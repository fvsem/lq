Отдельная папка для загрузки пользовательских дополнений.
Реализована с целью облегчить обновление сервера.
Пока поддерживается только загрузка скилов.

Измененные вами скилы, поместите в соответствующую папку, сервер загрузит их от туда, вместо основных.
Название файла роли не играет, в файле может содержаться любое количество скилов.