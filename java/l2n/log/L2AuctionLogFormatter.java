package l2n.log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;


public class L2AuctionLogFormatter extends Formatter
{
	private static final String CRLF = "\r\n";
	private static final String divider = "\t";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm:ss.SSS");

	@Override
	public String format(LogRecord record)
	{
		StringBuffer output = new StringBuffer();
		output.append(sdf.format(new Date(record.getMillis())));
		output.append(divider);
		output.append(record.getLevel().getName());
		output.append(divider);
		output.append(record.getSourceClassName());
		output.append(divider);
		output.append(record.getSourceMethodName());
		output.append(divider);
		output.append(record.getMessage());
		output.append(CRLF);
		return output.toString();
	}
}
