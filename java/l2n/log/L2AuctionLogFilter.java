package l2n.log;

import java.util.logging.Filter;
import java.util.logging.LogRecord;


public class L2AuctionLogFilter implements Filter
{
	@Override
	public boolean isLoggable(LogRecord record)
	{
		return record.getLoggerName().equals("l2auction");
	}
}
