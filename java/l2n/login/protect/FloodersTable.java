package l2n.login.protect;

import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.procedure.TLongObjectProcedure;
import gnu.trove.procedure.TObjectObjectProcedure;
import l2n.Config;
import l2n.commons.misc.INgObjectFactory;
import l2n.commons.misc.NgObjectBuffer;
import l2n.login.L2LoginThreadPools;

import java.net.InetAddress;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FloodersTable implements Runnable
{
	private final class AuthFloodReject implements TObjectObjectProcedure<String, FloodInformation>
	{
		@Override
		public final boolean execute(final String account, final FloodInformation fi)
		{
			if(fi.lastAttempTime + Config.LOGIN_CLIENT_AUTH_FLOOD_FORGET_TIME < System.currentTimeMillis())
			{
				recycleFloodInformation(fi);
				return false;
			}
			return true;
		}
	}

	private final class IPFloodReject implements TLongObjectProcedure<FloodInformation>
	{
		@Override
		public final boolean execute(final long ipHash, final FloodInformation fi)
		{
			if(fi.lastAttempTime + Config.LOGIN_CLIENT_IP_FLOOD_FORGET_TIME < System.currentTimeMillis())
			{
				recycleFloodInformation(fi);
				return false;
			}
			return true;
		}
	}

	private final void recycleFloodInformation(final FloodInformation fi)
	{
		synchronized (_floodInfoPool)
		{
			if(_floodInfoPool.put(fi))
			{
				fi.countTrys = 0;
				fi.blockTime = 0;
			}
		}
	}

	private static final Logger _log = Logger.getLogger(FloodersTable.class.getName());

	private final TLongObjectHashMap<FloodInformation> _loginIPFlooders;
	private final THashMap<String, FloodInformation> _loginAuthFlooders;

	private final IPFloodReject _ipFloodReject;
	private final AuthFloodReject _authFloodReject;

	private final NgObjectBuffer<FloodInformation> _floodInfoPool;

	private final ScheduledFuture<?> _cleanupTask;

	public FloodersTable()
	{
		_loginIPFlooders = new TLongObjectHashMap<FloodInformation>();
		_loginAuthFlooders = new THashMap<String, FloodInformation>();

		_ipFloodReject = new IPFloodReject();
		_authFloodReject = new AuthFloodReject();

		_floodInfoPool = new NgObjectBuffer<FloodInformation>(Config.LOGIN_CLIENT_FLOOD_INFO_POOL_MAX_SIZE, new INgObjectFactory<FloodInformation>()
		{
			@Override
			public final FloodInformation newInstance()
			{
				return new FloodInformation();
			}
		}, true);

		_cleanupTask = L2LoginThreadPools.getInstance().scheduleGeneralAtFixedRate(this, Config.LOGIN_CLIENT_CLEANUP_TIMEOUT, Config.LOGIN_CLIENT_CLEANUP_TIMEOUT);
	}

	public static FloodersTable getInstance()
	{
		return SingletonHolder._instance;
	}

	/**
	 * Проверяет и банит за слишком частые подключения к логину
	 */
	public boolean isIpFloodBanned(final long ipHash, final InetAddress address)
	{
		final long currentTime = System.currentTimeMillis();
		synchronized (_loginIPFlooders)
		{
			FloodInformation fi = _loginIPFlooders.get(ipHash);
			if(fi != null)
			{
				if(fi.blockTime > 0)
				{
					if(fi.blockTime > currentTime)
					{
						fi.lastAttempTime = currentTime;
						return true;
					}
					else
					{
						fi.lastAttempTime = currentTime;
						fi.countTrys = 0;
						fi.blockTime = 0;
					}
				}
				else if(fi.lastAttempTime + Config.LOGIN_CLIENT_LOGIN_IP_TRY_TIME > currentTime)
				{
					fi.lastAttempTime = currentTime;
					fi.countTrys++;

					if(fi.countTrys >= Config.LOGIN_CLIENT_LOGIN_IP_TRY_MAX_COUNT)
					{
						fi.blockTime = currentTime + Config.LOGIN_CLIENT_LOGIN_IP_TRY_BLOCK_TIME;
						_log.log(Level.INFO, "IpFloodBanned: banned [IP: " + (address == null ? "NULL" : address.getHostAddress()) + "] for " + Config.LOGIN_CLIENT_LOGIN_IP_TRY_BLOCK_TIME + " ms.");
						return true;
					}
				}
				else
				{
					fi.lastAttempTime = currentTime;
					fi.countTrys = 0;
					fi.blockTime = 0;
				}
			}
			else
			{
				synchronized (_floodInfoPool)
				{
					fi = _floodInfoPool.get();
				}

				fi.lastAttempTime = currentTime;
				_loginIPFlooders.put(ipHash, fi);
			}
		}

		return false;
	}

	public int isAuthFloodBanned(final String account, final InetAddress address)
	{
		synchronized (_loginAuthFlooders)
		{
			final long currentTime = System.currentTimeMillis();
			FloodInformation fi = _loginAuthFlooders.get(account);
			if(fi != null)
			{
				// если уже заблокирован
				if(fi.blockTime > 0)
				{
					// время ещё не вышло
					if(fi.blockTime > currentTime)
					{
						fi.lastAttempTime = currentTime;
						return 2;
					}
					else
					{
						fi.lastAttempTime = currentTime;
						fi.countTrys = 0;
						fi.blockTime = 0;
					}
				}
				// проверяем время последней попытки
				else if(fi.lastAttempTime + Config.LOGIN_CLIENT_LOGIN_AUTH_TRY_TIME > currentTime)
				{
					fi.lastAttempTime = currentTime;
					fi.countTrys++;

					if(fi.countTrys >= Config.LOGIN_CLIENT_LOGIN_AUTH_TRY_MAX_COUNT)
					{
						fi.blockTime = currentTime + Config.LOGIN_CLIENT_LOGIN_AUTH_TRY_BLOCK_TIME;
						_log.log(Level.INFO, "AuthFloodBanned: banned [ACC: " + account + " / IP: " + (address == null ? "NULL" : address.getHostAddress()) + "] for " + Config.LOGIN_CLIENT_LOGIN_AUTH_TRY_BLOCK_TIME + " ms.");
						return 1;
					}
				}
				else
				{
					fi.lastAttempTime = currentTime;
					fi.countTrys = 0;
					fi.blockTime = 0;
				}
			}
			else
			{
				synchronized (_floodInfoPool)
				{
					fi = _floodInfoPool.get();
				}

				fi.lastAttempTime = currentTime;
				_loginAuthFlooders.put(account, fi);
			}
		}
		return 0;
	}

	@Override
	public void run()
	{
		try
		{
			synchronized (_loginIPFlooders)
			{
				_loginIPFlooders.retainEntries(_ipFloodReject);
				_loginIPFlooders.compact();
			}

			synchronized (_loginAuthFlooders)
			{
				_loginAuthFlooders.retainEntries(_authFloodReject);
				_loginAuthFlooders.compact();
			}
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
	}

	public void shutdown()
	{
		_cleanupTask.cancel(false);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final FloodersTable _instance = new FloodersTable();
	}
}
