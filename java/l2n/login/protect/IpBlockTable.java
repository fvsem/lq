package l2n.login.protect;

import gnu.trove.map.hash.TLongObjectHashMap;
import gnu.trove.procedure.TLongObjectProcedure;
import l2n.Config;
import l2n.commons.network.utils.Net;
import l2n.commons.sql.SqlBatch;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.login.L2LoginThreadPools;
import l2n.login.network.L2LoginClient;
import l2n.util.StringUtil;

import java.net.InetAddress;
import java.sql.ResultSet;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IpBlockTable implements Runnable
{
	private static final class IpBannReject implements TLongObjectProcedure<BanInformation>
	{
		private static final IpBannReject STATIC_INSTANCE = new IpBannReject();

		@Override
		public final boolean execute(final long ipHash, final BanInformation bi)
		{
			if(bi.expireTime > 0 && bi.expireTime < System.currentTimeMillis())
				return false;
			return true;
		}
	}

	private static final Logger _log = Logger.getLogger(IpBlockTable.class.getName());

	private final TLongObjectHashMap<BanInformation> _bannedIPs;
	private final Lock _read_lock, _write_lock;
	private final ScheduledFuture<?> _cleanupTask;

	public static IpBlockTable getInstance()
	{
		return SingletonHolder._instance;
	}

	public IpBlockTable()
	{
		ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
		_read_lock = lock.readLock();
		_write_lock = lock.writeLock();

		_bannedIPs = new TLongObjectHashMap<BanInformation>();
		_cleanupTask = L2LoginThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 2 * Config.LOGIN_CLIENT_CLEANUP_TIMEOUT, 2 * Config.LOGIN_CLIENT_CLEANUP_TIMEOUT);

		loadBannedIPs();
	}

	public final boolean addIPBann(L2LoginClient lc, String admin, long time, String comments)
	{
		InetAddress address = null;
		try
		{
			address = lc.getConnection().getSocket().getInetAddress();
		}
		catch(Exception e)
		{}
		if(address != null)
		{
			_log.log(Level.INFO, "IpBanned: banned [IP: " + (address == null ? "NULL" : address.getHostAddress()) + "] for " + Config.BAN_SUSPICIOUS_BLOCK_TIME + " ms, reason: " + comments + ".");
			long ipHash = Net.getHashFromAddress(address);
			BanInformation banInfo = new BanInformation(ipHash, address.getHostAddress(), admin, time, comments);
			// закрываем коннект
			lc.closeNow(false);
			return addIPBann(banInfo);
		}
		return true;
	}

	public final boolean addIPBann(final long ipHash, String ip, String admin, long time, String comments)
	{
		BanInformation banInfo = new BanInformation(ipHash, ip, admin, time, comments);
		return addIPBann(banInfo);
	}

	public final boolean addIPBann(BanInformation banInfo)
	{
		_write_lock.lock();
		try
		{
			return _bannedIPs.put(banInfo.ipHash, banInfo) != null;
		}
		finally
		{
			_write_lock.unlock();
		}
	}

	public final BanInformation removeIPBann(final long ipHash)
	{
		BanInformation banInfo = null;
		_write_lock.lock();
		try
		{
			banInfo = _bannedIPs.remove(ipHash);
		}
		finally
		{
			_write_lock.unlock();
		}
		return banInfo;
	}

	public final BanInformation getBanInformation(final long ipHash)
	{
		_read_lock.lock();
		try
		{
			return _bannedIPs.get(ipHash);
		}
		finally
		{
			_read_lock.unlock();
		}
	}

	public final BanInformation[] getBanInformations()
	{
		BanInformation[] bans = null;
		_read_lock.lock();
		try
		{
			bans = _bannedIPs.values(new BanInformation[_bannedIPs.size()]);
		}
		finally
		{
			_read_lock.unlock();
		}
		return bans;
	}

	public final boolean isBanned(final long ipHash)
	{
		BanInformation banInfo;
		if((banInfo = getBanInformation(ipHash)) != null)
		{
			long currentTime = System.currentTimeMillis();
			if(banInfo.expireTime > 0)
			{
				// если время бана ещё не кончилось
				if(banInfo.expireTime > currentTime)
					return true;
				else
				{
					// если кончилось, то удаляем
					removeIPBann(ipHash);
					return false;
				}
			}
			// перманентный бан))
			else if(banInfo.expireTime <= 0)
				return true;

		}
		return false;
	}

	public final boolean isBanned(final InetAddress address)
	{
		return isBanned(Net.getHashFromAddress(address));
	}

	private void loadBannedIPs()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			long currentTime = System.currentTimeMillis();
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM `banned_ips` WHERE `expiretime` = 0 OR `expiretime` > ?");
			statement.setLong(1, currentTime);
			rset = statement.executeQuery();

			BanInformation banInfo;
			while (rset.next())
			{
				banInfo = new BanInformation(rset.getLong("iphash"), rset.getString("ip"), rset.getString("admin"), rset.getLong("expiretime"), rset.getString("comments"));
				addIPBann(banInfo);
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "error while reading banned_ips: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void saveBannedIPs()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM `banned_ips`");
			DbUtils.close(statement);

			if(_bannedIPs.isEmpty())
				return;

			final SqlBatch batch = new SqlBatch("INSERT INTO `banned_ips` (`iphash`,`ip`,`admin`,`expiretime`,`comments`) VALUES");
			StringBuilder sb;

			BanInformation[] banInfos = getBanInformations();
			for(BanInformation banInfo : banInfos)
				if(banInfo != null)
				{
					sb = StringUtil.startAppend(200, "(");
					StringUtil.append(sb, banInfo.ipHash, ",\"", banInfo.ip, "\",\"", banInfo.admin, "\",", banInfo.expireTime, ",\"", banInfo.comments, "\")");
					batch.write(sb.toString());
				}

			if(!batch.isEmpty())
			{
				statement = con.createStatement();
				statement.executeUpdate(batch.close());
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "error while save banned_ips: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void shutdown()
	{
		_cleanupTask.cancel(false);
	}

	public int size()
	{
		return _bannedIPs.size();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final IpBlockTable _instance = new IpBlockTable();
	}

	@Override
	public final void run()
	{
		_write_lock.lock();
		try
		{
			_bannedIPs.retainEntries(IpBannReject.STATIC_INSTANCE);
			_bannedIPs.compact();
		}
		finally
		{
			_write_lock.unlock();
		}
	}
}
