package l2n.login.protect;

import l2n.commons.network.utils.Net;

import java.net.InetAddress;

public class BanInformation
{
	public long ipHash;
	public String ip = "";
	public String admin;
	public long expireTime;
	public String comments;

	public BanInformation(long _ipHash, String _ip, String _admin, long _expireTime, String _comments)
	{
		ipHash = _ipHash;
		ip = _ip;
		admin = _admin;
		expireTime = _expireTime;
		comments = _comments;
	}

	public BanInformation(long _ipHash, String _admin, long _expireTime, String _comments)
	{
		ipHash = _ipHash;
		try
		{
			ip = InetAddress.getByAddress(Net.getIPFromHash(ipHash)).getHostAddress();
		}
		catch(Exception e)
		{}

		admin = _admin;
		expireTime = _expireTime;
		comments = _comments;
	}

	public String getAddress()
	{
		return ip;
	}
}
