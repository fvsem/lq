package l2n.login;

import l2n.commons.misc.L2ThreadPool;
import l2n.commons.threading.FIFOExecutableQueue;
import l2n.commons.threading.ThreadPoolFactory;
import l2n.commons.threading.ThreadPoolFactory.NextPriorityThreadFactory;
import l2n.login.network.L2LoginClient;
import l2n.login.network.clientpackets.L2LoginClientPacket;
import l2n.login.network.serverpackets.L2LoginServerPacket;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2LoginThreadPools implements L2ThreadPool<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>
{
	public static final Logger _log = Logger.getLogger(L2LoginThreadPools.class.getName());

	/** temp workaround for VM issue */
	private static final long MAX_DELAY = Long.MAX_VALUE / 1000000 / 2;

	public static long validateDelay(long delay)
	{
		if(delay < 0)
			delay = 0;
		else if(delay > MAX_DELAY)
		{
			_log.log(Level.WARNING, "ThreadPoolManager: big delay for task!", new Exception("validateDelay"));
			delay = MAX_DELAY;
		}
		return delay;
	}

	public static long validatePeriode(long periode)
	{
		if(periode < 1)
			periode = 1;
		else if(periode > MAX_DELAY)
		{
			_log.log(Level.WARNING, "ThreadPoolManager: big periode for task!", new Exception("validatePeriode"));
			periode = MAX_DELAY;
		}
		return periode;
	}

	private static L2LoginThreadPools _instance;

	private final ThreadPoolExecutor _executor;
	private final ScheduledThreadPoolExecutor _scheduledExecuter;

	private boolean _isShutdown;

	public static L2LoginThreadPools getInstance()
	{
		if(_instance == null)
			_instance = new L2LoginThreadPools();
		return _instance;
	}

	private L2LoginThreadPools()
	{
		_executor = ThreadPoolFactory.createThreadPoolExecutor(4, 6, 15L, TimeUnit.SECONDS, "LoginTPool", Thread.NORM_PRIORITY + 3);
		_scheduledExecuter = new ScheduledThreadPoolExecutor(8, new NextPriorityThreadFactory("LoginSTPool", Thread.NORM_PRIORITY));
	}

	public void execute(final Runnable r)
	{
		_executor.execute(r);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneral(final T command, final long delay)
	{
		return (ScheduledFuture<T>) _scheduledExecuter.schedule(command, validateDelay(delay), TimeUnit.MILLISECONDS);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneralAtFixedRate(final T command, final long initial, final long delay)
	{
		return (ScheduledFuture<T>) _scheduledExecuter.scheduleAtFixedRate(command, validateDelay(initial), validatePeriode(delay), TimeUnit.MILLISECONDS);
	}

	public final void shutdown()
	{
		if(_isShutdown)
			return;

		System.out.println("ThreadPoolManager: Shutting down all thread pools...");
		_isShutdown = true;
		try
		{
			_executor.awaitTermination(1, TimeUnit.SECONDS);
			_scheduledExecuter.awaitTermination(1, TimeUnit.SECONDS);
		}
		catch(final InterruptedException e)
		{}
	}

	public boolean isShutdown()
	{
		return _isShutdown;
	}

	@Override
	public void executeQueue(final FIFOExecutableQueue queue)
	{
		_executor.execute(queue);
	}
}
