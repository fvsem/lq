package l2n.login;

import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Base64;
import l2n.Config;
import l2n.commons.crypt.AlgorithmType;
import l2n.commons.crypt.ScrambledKeyPair;
import l2n.commons.crypt.algorithm.ICrypt;
import l2n.commons.list.GArray;
import l2n.commons.network.utils.Net;
import l2n.commons.network.utils.NetList;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.login.gameservercon.AttGameServer;
import l2n.login.gameservercon.GameServerInfo;
import l2n.login.network.L2LoginClient;
import l2n.login.protect.FloodersTable;
import l2n.login.protect.IpBlockTable;
import l2n.util.Log;
import l2n.util.Rnd;

import javax.crypto.Cipher;
import java.net.InetAddress;
import java.security.*;
import java.security.spec.RSAKeyGenParameterSpec;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginController implements Runnable
{
	private final static Logger _log = Logger.getLogger(LoginController.class.getName());

	/** Список клиентов на логине, которые ещё не назначены на аккаунты */
	private final TIntObjectHashMap<L2LoginClient> _clientsOnLogin;
	/** Список авторизованных клиентов, которые на ГеймСервере */
	private final THashMap<String, L2LoginClient> _accountsOnLogin;

	private final ScrambledKeyPair[] _keyPairs;

	private byte[][] _blowfishKeys;

	public final static ICrypt DEFAULT_CRYPT = Config.DEFAULT_PASSWORD_ENCODING.getInstance();
	private final ICrypt[] LEGACY_CRYPT;

	public static enum State
	{
		VALID,
		WRONG,
		FAKE_LOGIN,
		NOT_PAID,
		BANNED,
		IN_USE,
		IP_ACCESS_DENIED,
		AUTH_BANNED
	}

	public static LoginController getInstance()
	{
		return SingletonHolder._instance;
	}

	private LoginController()
	{
		_log.log(Level.INFO, "Loading LoginController...");

		final GArray<ICrypt> legacy = new GArray<ICrypt>();
		for(final String method : Config.LEGACY_PASSWORD_ENCODING.split(";"))
		{
			final AlgorithmType t = AlgorithmType.getAlgorithm(method);
			legacy.add(t.getInstance());
		}
		LEGACY_CRYPT = legacy.toArray(new ICrypt[legacy.size()]);

		_log.log(Level.INFO, "Loaded " + Config.DEFAULT_PASSWORD_ENCODING + " as default crypt.");

		_keyPairs = new ScrambledKeyPair[Config.LOGIN_RSA_KEYPAIRS];

		_clientsOnLogin = new TIntObjectHashMap<L2LoginClient>();
		_accountsOnLogin = new THashMap<String, L2LoginClient>();

		try
		{
			final KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
			final RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
			keygen.initialize(spec);

			// generate the initial set of keys
			for(int i = 0; i < _keyPairs.length; i++)
				_keyPairs[i] = new ScrambledKeyPair(keygen.generateKeyPair());

			testCipher(_keyPairs[0]._pair.getPrivate());

			// Store keys for blowfish communication
			generateBlowFishKeys();
		}
		catch(final NoSuchAlgorithmException e)
		{
			_log.log(Level.SEVERE, "Failed initializing LoginController. Reason: " + e.getMessage(), e);
		}
		catch(final InvalidAlgorithmParameterException e)
		{
			_log.log(Level.SEVERE, "Failed initializing LoginController. Reason: " + e.getMessage(), e);
		}
		catch(final GeneralSecurityException e)
		{
			_log.log(Level.SEVERE, "Failed initializing LoginController. Reason: " + e.getMessage(), e);
		}

		// FIXME
		L2LoginThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 20 * 60 * 1000, 20 * 60 * 1000);
	}

	/**
	 * This is mostly to force the initialization of the Crypto Implementation, avoiding it being done on runtime when its first needed.<BR>
	 * In short it avoids the worst-case execution time on runtime by doing it on loading.
	 * 
	 * @param privateKey
	 *            Any private RSA Key just for testing purposes.
	 * @throws GeneralSecurityException
	 *             if a underlying exception was thrown by the Cipher
	 */
	private void testCipher(final PrivateKey privateKey) throws GeneralSecurityException
	{
		// avoid worst-case execution, KenM
		final Cipher rsaCipher = Cipher.getInstance("RSA/ECB/nopadding");
		rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
	}

	private void generateBlowFishKeys()
	{
		_blowfishKeys = new byte[Config.LOGIN_BLOWFISH_KEYS][16];

		for(int i = 0; i < Config.LOGIN_BLOWFISH_KEYS; i++)
			for(int j = 0; j < _blowfishKeys[i].length; j++)
				_blowfishKeys[i][j] = (byte) (Rnd.get(255) + 1);
		_log.log(Level.INFO, "Stored " + _blowfishKeys.length + " keys for Blowfish communication");
	}

	/**
	 * @return Returns a random key
	 */
	public byte[] getBlowfishKey()
	{
		return _blowfishKeys[Rnd.get(_blowfishKeys.length)];
	}

	public void addLoginClient(final L2LoginClient client)
	{
		synchronized (_clientsOnLogin)
		{
			_clientsOnLogin.put(client.getSessionId(), client);
		}
	}

	public void removeLoginClient(final L2LoginClient client)
	{
		synchronized (_clientsOnLogin)
		{
			_clientsOnLogin.remove(client.getSessionId());
		}
	}

	public SessionKey assignSessionKeyToClient()
	{
		final SessionKey key = new SessionKey(Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt());
		return key;
	}

	public void addAuthedLoginClient(final String account, final L2LoginClient client)
	{
		synchronized (_accountsOnLogin)
		{
			_accountsOnLogin.put(account, client);
		}
	}

	public L2LoginClient removeAuthedLoginClient(final String account)
	{
		synchronized (_accountsOnLogin)
		{
			return _accountsOnLogin.remove(account);
		}
	}

	public boolean isAccountInLoginServer(final String account)
	{
		synchronized (_accountsOnLogin)
		{
			return _accountsOnLogin.containsKey(account);
		}
	}

	public L2LoginClient getAuthedClient(final String account)
	{
		synchronized (_accountsOnLogin)
		{
			return _accountsOnLogin.get(account);
		}
	}

	public State tryAuthLogin(final String account, final String password, final L2LoginClient client)
	{
		State ret = loginValid(account, password, client);
		if(ret != State.VALID)
			return ret;

		if(!isAccountInLoginServer(account) && !isAccountInAnyGameServer(account))
		{
			// dont allow 2 simultaneous login
			synchronized (_accountsOnLogin)
			{
				if(!_accountsOnLogin.containsKey(account))
					addAuthedLoginClient(account, client);
				else
					ret = State.IN_USE;
			}

			// если авторизация игрока прошла успешно
			if(ret == State.VALID)
				// то удаляем клиента из списка неавторизованных клиентов
				removeLoginClient(client);
		}
		else
			ret = State.IN_USE;
		return ret;
	}

	public boolean isBannedAddress(final InetAddress address)
	{
		final long ipHash = Net.getHashFromAddress(address);

		// проверяем основной список забаненных
		if(IpBlockTable.getInstance().isBanned(ipHash))
		{
			if(Config.LOGIN_CLIENT_LOGIN_IPBANN_SHOW_LOG)
				_log.log(Level.INFO, "Connect failed for [IP: " + address.getHostAddress() + "], reason: IP Banned.");
			return true;
		}

		// проверяем список флудеров
		if(FloodersTable.getInstance().isIpFloodBanned(ipHash, address))
		{
			if(Config.LOGIN_CLIENT_LOGIN_IP_TRY_SHOW_LOG)
				_log.log(Level.INFO, "Connect failed for [IP: " + address.getHostAddress() + "], reason: Flood blocked.");
			return true;
		}

		return false;
	}

	public SessionKey getKeyForAccount(final String account)
	{
		L2LoginClient client;
		synchronized (_accountsOnLogin)
		{
			client = _accountsOnLogin.get(account);
		}
		if(client != null)
			return client.getSessionKey();
		return null;
	}

	public int getOnlinePlayerCount(final int serverId)
	{
		final GameServerInfo gsi = GameServerTable.getInstance().getRegisteredGameServerById(serverId);
		if(gsi != null && gsi.isAuthed())
			return gsi.getCurrentPlayerCount();
		return 0;
	}

	public boolean isAccountInAnyGameServer(final String account)
	{
		AttGameServer gst;
		final Collection<GameServerInfo> serverList = GameServerTable.getInstance().getRegisteredGameServers().values();
		for(final GameServerInfo gsi : serverList)
			if((gst = gsi.getGameServer()) != null && gst.isAccountInGameServer(account))
				return true;
		return false;
	}

	public GameServerInfo getAccountOnGameServer(final String account)
	{
		AttGameServer gst;
		final Collection<GameServerInfo> serverList = GameServerTable.getInstance().getRegisteredGameServers().values();
		for(final GameServerInfo gsi : serverList)
			if((gst = gsi.getGameServer()) != null && gst.isAccountInGameServer(account))
				return gsi;
		return null;
	}

	public int getTotalOnlinePlayerCount()
	{
		int total = 0;
		final Collection<GameServerInfo> serverList = GameServerTable.getInstance().getRegisteredGameServers().values();
		for(final GameServerInfo gsi : serverList)
			if(gsi.isAuthed())
				total += gsi.getCurrentPlayerCount();
		return total;
	}

	public int getMaxAllowedOnlinePlayers(final int id)
	{
		final GameServerInfo gsi = GameServerTable.getInstance().getRegisteredGameServerById(id);
		if(gsi != null)
			return gsi.getMaxPlayers();
		return 0;
	}

	public boolean isLoginPossible(final L2LoginClient client, final int serverId)
	{
		final GameServerInfo gsi = GameServerTable.getInstance().getRegisteredGameServerById(serverId);
		final int access = client.getAccessLevel();
		final boolean loginOk = gsi != null && gsi.isAuthed() && (gsi.getCurrentPlayerCount() < gsi.getMaxPlayers() || access >= 50);
		return loginOk;
	}

	public void setAccountAccessLevel(final String user, final int banLevel, final String comments, final int banTime)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE accounts SET access_level = ?, comments = ?, ban_expire = ? WHERE login=?");
			statement.setInt(1, banLevel);
			statement.setString(2, comments);
			statement.setInt(3, banTime);
			statement.setString(4, user);
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.warning("Could not set accessLevel: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean isGM(final String user)
	{
		boolean ok = false;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT access_level FROM accounts WHERE login=?");
			statement.setString(1, user);
			rset = statement.executeQuery();
			if(rset.next())
			{
				final int accessLevel = rset.getInt(1);
				if(accessLevel >= 100)
					ok = true;
			}
		}
		catch(final Exception e)
		{
			// _log.warning("could not check gm state:"+e);
			ok = false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return ok;
	}

	/**
	 * <p>
	 * This method returns one of the cached {@link ScrambledKeyPair ScrambledKeyPairs} for communication with Login Clients.
	 * </p>
	 * 
	 * @return a scrambled keypair
	 */
	public ScrambledKeyPair getScrambledRSAKeyPair()
	{
		return _keyPairs[Rnd.get(_keyPairs.length)];
	}

	private void log(final String text)
	{
		if(Config.LOGIN_DEBUG)
			_log.log(Level.INFO, text);
	}

	private final State loginValid(final String user, final String password, final L2LoginClient client)
	{
		State client_state = State.WRONG;
		final InetAddress address = client.getConnection().getSocket().getInetAddress();
		Log.add("'" + (user == null ? "null" : user) + "' " + (address == null ? "null" : address.getHostAddress()), "logins_ip");

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			int lastServer = 1;
			boolean paid = false;
			boolean banned = false;
			String phash = "";

			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT password,pay_stat,access_level,bonus,bonus_expire,ban_expire,lastServer,allow_ips,allow_hwid FROM accounts WHERE login=?");
			statement.setString(1, user);
			rset = statement.executeQuery();
			if(rset.next())
			{
				client.account_fields = new StatsSet();
				final String allowedIps = rset.getString("allow_ips");
				if(allowedIps != null && !allowedIps.isEmpty() && !allowedIps.equals("*"))
				{
					final NetList allowedList = new NetList();
					allowedList.LoadFromString(allowedIps, ",");
					if(!allowedList.isIpInNets(client.getIpAddress()))
						return State.IP_ACCESS_DENIED;
				}

				phash = rset.getString("password");
				if(phash.equals(""))
					return State.WRONG;

				paid = rset.getInt(2) == 1;
				banned = rset.getInt(3) < 0;

				final long bonusTime = rset.getLong("bonus_expire");
				final double bonus = bonusTime > System.currentTimeMillis() / 1000 || bonusTime < 0 ? rset.getDouble("bonus") : 1;
				if(bonus > 1)
				{
					client.account_fields.set("bonus", bonus);
					client.account_fields.set("bonus_expire", bonusTime);
				}

				// задаём HWID к которому привязан аккаунт
				final String allow_hwid = rset.getString("allow_hwid");
				if(allow_hwid != null && !allow_hwid.isEmpty() && !allow_hwid.equals("*"))
					client.account_fields.set("allow_hwid", allow_hwid);

				final long banTime = rset.getLong("ban_expire");
				if(banTime == -1)
					banned = true;
				else if(banTime > 0)
					if(banTime < System.currentTimeMillis() / 1000)
						unBanAcc(user);
					else
						banned = true;

				log("account exists");
				lastServer = Math.max(rset.getInt("lastServer"), 1);
				client.setAccessLevel(rset.getInt("access_level"));
			}
			DbUtils.closeQuietly(statement, rset);

			// if account doesnt exists
			if(phash.equals(""))
				if(Config.AUTO_CREATE_ACCOUNTS)
				{
					if(user != null && user.length() >= 2 && user.length() <= 14)
					{
						statement = con.prepareStatement("INSERT INTO accounts (login,password,lastactive,access_level,lastIP,comments) values(?,?,?,?,?,?)");
						statement.setString(1, user);
						statement.setString(2, DEFAULT_CRYPT.encrypt(password));
						statement.setLong(3, System.currentTimeMillis() / 1000);
						statement.setInt(4, 0);
						statement.setString(5, address != null ? address.getHostAddress() : "");
						statement.setString(6, "");
						statement.execute();
						DbUtils.close(statement);

						log("created new account for " + user);
						return State.VALID;
					}

					log("Invalid username creation/use attempt: " + user);
					return State.WRONG;
				}
				else
				// учёт ошибок при попытке зайти на несуществующие аккаунты
				{
					log("Account missing for user " + user);
					// Проверяем на частые попытки авторизоваться
					int type;
					if((type = FloodersTable.getInstance().isAuthFloodBanned(user, address)) > 0)
					{
						if(Config.ENABLE_FAKE_LOGIN && type == 1)
							return State.FAKE_LOGIN;
						return State.AUTH_BANNED;
					}
				}

			client_state = State.VALID;

			// проверяем не зашифрован ли пароль одним из устаревших но поддерживаемых алгоритмов
			boolean oldcrypt = false;
			for(final ICrypt c : LEGACY_CRYPT)
				if(c.compare(password, phash)) // если да то заменяем на стандартный
				{
					statement = con.prepareStatement("UPDATE accounts SET password=? WHERE login=?");
					statement.setString(1, DEFAULT_CRYPT.encrypt(password));
					statement.setString(2, user);
					statement.execute();
					DbUtils.closeQuietly(statement);
					oldcrypt = true;
					break;
				}

			// если старые алгоритмы не подошли проверяем стандартным
			if(!oldcrypt && !DEFAULT_CRYPT.compare(password, phash))
				client_state = State.WRONG;

			// Настройка позволяющая зайти на любой аккаунт
			if(Config.SUPER_ACCESS_ENABLE && Config.SUPER_ACCESS_PASSWORD.length() >= 6)
				if(Config.SUPER_ACCESS_IP_LIST.isIpInNets(client.getIpAddress()) && Config.SUPER_ACCESS_PASSWORD.equals(password))
				{
					Log.add("'" + user + "' " + (address != null ? address.getHostAddress() : ""), "logins_super_access");
					client_state = State.VALID;
				}

			if(!paid)
				return State.NOT_PAID;
			if(banned)
				return State.BANNED;

			if(client_state == State.VALID)
			{
				updateLastIp(con, statement, user, address != null ? address.getHostAddress() : "");
				client.setLastServer(lastServer);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not check password: ", e);
			client_state = State.WRONG;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		// проверка антибрута для существующих аккаунтов
		if(client_state != State.VALID)
		{
			// Проверяем на частые попытки авторизоваться
			int type;
			if((type = FloodersTable.getInstance().isAuthFloodBanned(user, address)) > 0)
			{
				if(Config.ENABLE_FAKE_LOGIN && type == 1)
					return State.FAKE_LOGIN;
				return State.AUTH_BANNED;
			}
		}
		else
			Log.add("'" + user + "' " + (address != null ? address.getHostAddress() : ""), "logins_ip");

		return client_state;
	}

	private void updateLastIp(final ThreadConnection con, FiltredPreparedStatement statement, final String user, final String address)
	{
		try
		{
			statement = con.prepareStatement("UPDATE accounts SET lastactive=?, lastIP=? WHERE login=?");
			statement.setLong(1, System.currentTimeMillis() / 1000);
			statement.setString(2, address);
			statement.setString(3, user);
			statement.execute();
		}
		catch(final SQLException e)
		{
			_log.log(Level.WARNING, "Could not update lastIP: ", e);
		}
	}

	public void unBanAcc(final String name)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE accounts SET access_level = ?, ban_expire = ? WHERE login = ?");
			statement.setInt(1, 0);
			statement.setInt(2, 0);
			statement.setString(3, name);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.warning("Cant unban acc " + name + ", " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean setPassword(final String account, final String password)
	{
		boolean updated = true;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			final MessageDigest md = MessageDigest.getInstance("SHA");
			final byte[] raw = password.getBytes("UTF-8");
			final byte[] hash = md.digest(raw);
			statement = con.prepareStatement("UPDATE accounts SET password=? WHERE login=?");
			statement.setString(1, Base64.encodeToString(hash, false));
			statement.setString(2, account);
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			updated = false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		return updated;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final LoginController _instance = new LoginController();
	}

	// FIXME
// private final class LCIdleReject implements TObjectObjectProcedure<Object, L2LoginClient>
// {
// @Override
// public final boolean execute(final Object sessionId, final L2LoginClient lc)
// {
// if(lc.getState() != LoginClientState.AUTHED_LOGIN)
// {
// if(lc.getConnectionStartTime() + Config.LOGIN_CLIENT_EXPIRE_TIME < System.currentTimeMillis())
// {
// lc.close(LoginFailReason.REASON_ACCESS_FAILED);
// return false;
// }
// }
// else if(lc.getPendingTime() + Config.LOGIN_CLIENT_EXPIRE_TIME_PENDING < System.currentTimeMillis())
// {
// lc.close(LoginFailReason.REASON_ACCESS_FAILED);
// return false;
// }
// return true;
// }
// }

	@Override
	public void run()
	{
		try
		{
			synchronized (_clientsOnLogin)
			{
				// FIXME _clientsOnLogin.retainEntries(_lcIdleReject);
				_clientsOnLogin.compact();
			}

			synchronized (_accountsOnLogin)
			{
				// FIXME _accountsOnLogin.retainEntries(_lcIdleReject);
				_accountsOnLogin.compact();
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
	}
}
