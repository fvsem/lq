package l2n.login.network;

import l2n.Config;
import l2n.commons.crypt.LoginCrypt;
import l2n.commons.crypt.ScrambledKeyPair;
import l2n.commons.network.MMOClient;
import l2n.commons.network.MMOConnection;
import l2n.commons.threading.FIFORunnableQueue;
import l2n.commons.util.StatsSet;
import l2n.login.L2LoginThreadPools;
import l2n.login.LoginController;
import l2n.login.SessionKey;
import l2n.login.network.clientpackets.L2LoginClientPacket;
import l2n.login.network.serverpackets.AccountKicked;
import l2n.login.network.serverpackets.AccountKicked.AccountKickedReason;
import l2n.login.network.serverpackets.L2LoginServerPacket;
import l2n.login.protect.IpBlockTable;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.security.interfaces.RSAPrivateKey;
import java.util.Arrays;

public final class L2LoginClient extends MMOClient<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>
{
	public static enum LoginClientState
	{
		CONNECTED,
		AUTHED_GG,
		AUTHED_LOGIN,
		FAKE_LOGIN
	}

	private final static String EMPTY_STRING = "".intern();

	private int point;

	public int getPoint()
	{
		return point;
	}

	public void setPoint(final int point)
	{
		this.point = point;
	}

	private LoginClientState _state;
	public boolean PlayOK = false;

	// Crypt
	private LoginCrypt _loginCrypt;
	private ScrambledKeyPair _scrambledPair;
	private byte[] _blowfishKey;

	private String _account;
	private int _accessLevel;
	private int _lastServer;
	private SessionKey _sessionKey;

	private final int _sessionId;
	private byte[] _gameGuard;

	private final long _connectionTime;
	private long _pendingTime;

	public StatsSet account_fields = null;

	public L2LoginClient(final MMOConnection<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> con)
	{
		super(con);
		_state = LoginClientState.CONNECTED;

		_scrambledPair = LoginController.getInstance().getScrambledRSAKeyPair();
		_blowfishKey = LoginController.getInstance().getBlowfishKey();
		_connectionTime = System.currentTimeMillis();
		_loginCrypt = new LoginCrypt();
		_loginCrypt.setKey(_blowfishKey, false); // TODO выпилить
		_sessionId = con.hashCode();

		// По идее такого вообще не должно происходить, таких вообще пускать не будем)
		if(IpBlockTable.getInstance().isBanned(getConnection().getSocket().getInetAddress()))
			close(new AccountKicked(AccountKickedReason.REASON_PERMANENTLY_BANNED));
	}

	@Override
	public boolean decrypt(final ByteBuffer buf, final int size)
	{
		boolean ret;
		try
		{
			ret = _loginCrypt.decrypt(buf.array(), buf.position(), size);
		}
		catch(final IOException e)
		{
			e.printStackTrace();
			closeNow(false);
			return false;
		}

		if(!ret)
		{
			if(Config.ENABLE_BAN_SUSPICIOUS_ACTIVITY)
				IpBlockTable.getInstance().addIPBann(this, "auto-system", System.currentTimeMillis() + Config.BAN_SUSPICIOUS_BLOCK_TIME, "Suspicious Activity [Wrong checksum]");
			closeNow(false);
		}

		return ret;
	}

	@Override
	public boolean encrypt(final ByteBuffer buf, int size)
	{
		final int offset = buf.position();
		try
		{
			size = _loginCrypt.encrypt(buf.array(), offset, size);
		}
		catch(final IOException e)
		{
			e.printStackTrace();
			return false;
		}

		buf.position(offset + size);
		return true;
	}

	public LoginClientState getState()
	{
		return _state;
	}

	public void setState(final LoginClientState state)
	{
		_state = state;
	}

	public byte[] getBlowfishKey()
	{
		return _blowfishKey;
	}

	public byte[] getScrambledModulus()
	{
		if(_scrambledPair == null || _scrambledPair._scrambledModulus == null)
		{
			closeNow(true);
			return null;
		}

		return _scrambledPair._scrambledModulus;
	}

	public RSAPrivateKey getRSAPrivateKey()
	{
		return (RSAPrivateKey) _scrambledPair._pair.getPrivate();
	}

	public String getAccount()
	{
		return _account;
	}

	public void setAccount(final String account)
	{
		_account = account;
	}

	public void setAccessLevel(final int accessLevel)
	{
		_accessLevel = accessLevel;
	}

	public int getAccessLevel()
	{
		return _accessLevel;
	}

	public void setLastServer(final int lastServer)
	{
		_lastServer = lastServer;
	}

	public int getLastServer()
	{
		return _lastServer;
	}

	public int getSessionId()
	{
		return _sessionId;
	}

	public final void setGameGuard(final byte[] gameGuard)
	{
		_gameGuard = gameGuard;
	}

	public final boolean checkGameGuard(final byte[] gameGuard)
	{
		return Arrays.equals(_gameGuard, gameGuard);
	}

	public void setSessionKey(final SessionKey sessionKey)
	{
		_sessionKey = sessionKey;
	}

	public SessionKey getSessionKey()
	{
		return _sessionKey;
	}

	public final long getConnectionStartTime()
	{
		return _connectionTime;
	}

	public final long getPendingTime()
	{
		return _pendingTime;
	}

	public final void setPendingTime(final long pendingTime)
	{
		_pendingTime = pendingTime;
	}

	public String getAccountFields()
	{
		if(account_fields == null)
			return EMPTY_STRING;
		return account_fields.serialize();
	}

	public void sendPacket(final L2LoginServerPacket... lsp)
	{
		getConnection().sendPacket(lsp);
	}

	public void close(final L2LoginServerPacket lsp)
	{
		getConnection().close(lsp);
	}

	@Override
	public void onDisconnection()
	{
		final LoginController lc = LoginController.getInstance();
		if(getState() != LoginClientState.AUTHED_LOGIN)
			lc.removeLoginClient(this);
		else if(!PlayOK && _account != null && lc.isAccountInLoginServer(_account))
			lc.removeAuthedLoginClient(_account);

		_loginCrypt = null;
		_scrambledPair = null;
		_blowfishKey = null;

		super.onDisconnection();
	}

	@Override
	public void onForcedDisconnection()
	{
		final LoginController lc = LoginController.getInstance();
		if(getState() != LoginClientState.AUTHED_LOGIN)
			lc.removeLoginClient(this);
		else if(!PlayOK && _account != null && lc.isAccountInLoginServer(_account))
			lc.removeAuthedLoginClient(_account);

		_loginCrypt = null;
		_scrambledPair = null;
		_blowfishKey = null;

		super.onForcedDisconnection();
	}

	@Override
	public String toString()
	{
		final InetAddress address = getConnection().getSocket().getInetAddress();
		if(getState() == LoginClientState.AUTHED_LOGIN)
			return "[" + getAccount() + " (" + (address == null ? "disconnected" : address.getHostAddress()) + ")]";
		return "[" + (address == null ? "disconnected" : address.getHostAddress()) + "]";
	}

	public String getIpAddress()
	{
		try
		{
			return getConnection().getSocket().getInetAddress().getHostAddress();
		}
		catch(final Exception e)
		{
			return "Null IP";
		}
	}

	private FIFORunnableQueue<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> _packetQueue;

	@Override
	public FIFORunnableQueue<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> getPacketQueue()
	{
		if(_packetQueue == null)
			_packetQueue = new FIFORunnableQueue<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>(L2LoginThreadPools.getInstance())
			{

			};
		return _packetQueue;
	}

	@Override
	public void executePacket(final L2LoginClientPacket packet)
	{
		if(packet.blockReadingUntilExecutionIsFinished())
			getPacketQueue().executeNow(packet);
		else
			getPacketQueue().execute(packet);
	}
}
