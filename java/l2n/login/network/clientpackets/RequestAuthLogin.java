package l2n.login.network.clientpackets;

import l2n.Config;
import l2n.login.LoginController;
import l2n.login.LoginController.State;
import l2n.login.gameservercon.GameServerInfo;
import l2n.login.network.L2LoginClient;
import l2n.login.network.L2LoginClient.LoginClientState;
import l2n.login.network.serverpackets.AccountKicked;
import l2n.login.network.serverpackets.AccountKicked.AccountKickedReason;
import l2n.login.network.serverpackets.LoginFail;
import l2n.login.network.serverpackets.LoginOk;
import l2n.login.network.serverpackets.ServerList;
import l2n.login.protect.IpBlockTable;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.security.GeneralSecurityException;
import java.util.logging.Level;

public class RequestAuthLogin extends L2LoginClientPacket
{
	private byte[] _raw;
	private int _sessionId;
	private byte[] _gameGuard;

	public RequestAuthLogin()
	{
		_raw = new byte[128];
		_gameGuard = new byte[16];
	}

	@Override
	public boolean readImpl()
	{
		if(getAvaliableBytes() >= 128)
		{
			try
			{
				readB(_raw);
				_sessionId = readD();
				readB(_gameGuard);
				return true;
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "error readImpl: " + this.getClass().getSimpleName(), e);
				return false;
			}
		}
		return false;
	}

	@Override
	public void runImpl()
	{
		L2LoginClient client = getClient();
		if(_sessionId != client.getSessionId())
		{
			client.close(LoginFail.REASON_ACCESS_FAILED_TRYA1);
			return;
		}

		if(Config.LOGIN_GG_CHECK)
			if(!client.checkGameGuard(_gameGuard))
			{
				client.close(LoginFail.REASON_ACCESS_FAILED_TRYA1);
				return;
			}

		try
		{
			Cipher rsaCipher = Cipher.getInstance("RSA/ECB/nopadding");
			rsaCipher.init(Cipher.DECRYPT_MODE, client.getRSAPrivateKey());
			_raw = rsaCipher.doFinal(_raw, 0x00, 0x80);
		}
		catch(IllegalBlockSizeException e)
		{
			if(Config.ENABLE_BAN_SUSPICIOUS_ACTIVITY)
				IpBlockTable.getInstance().addIPBann(client, "auto-system", System.currentTimeMillis() + Config.BAN_SUSPICIOUS_BLOCK_TIME, "Suspicious Activity [IllegalBlockSizeException]");
			client.closeNow(false);
			return;
		}
		catch(BadPaddingException e)
		{
			if(Config.ENABLE_BAN_SUSPICIOUS_ACTIVITY)
				IpBlockTable.getInstance().addIPBann(client, "auto-system", System.currentTimeMillis() + Config.BAN_SUSPICIOUS_BLOCK_TIME, "Suspicious Activity [BadPaddingException]");
			client.closeNow(false);
			return;
		}
		catch(GeneralSecurityException e)
		{
			client.close(LoginFail.REASON_ACCESS_FAILED_TRYA1);
			return;
		}

		final String account = new String(_raw, 0x5E, 0x0E).trim().toLowerCase();
		final String password = new String(_raw, 0x6C, 0x10).trim();

		LoginController lc = LoginController.getInstance();

		State state = lc.tryAuthLogin(account, password, client);
		if(state == State.IN_USE)
		{
			L2LoginClient oldClient = lc.getAuthedClient(account);
			GameServerInfo gsi = lc.getAccountOnGameServer(account);

			// кикаем другого клиента, подключенного к логину
			if(oldClient != null)
				oldClient.close(LoginFail.REASON_ACCOUNT_IN_USE);

			// кикаем другого клиента из игры
			if(gsi != null && gsi.isAuthed())
				gsi.getGameServer().kickPlayer(account);

			if(lc.isAccountInLoginServer(account))
				lc.removeAuthedLoginClient(account).close(LoginFail.REASON_ACCOUNT_IN_USE);

			client.close(LoginFail.REASON_ACCOUNT_IN_USE);
		}
		else if(state == State.VALID)
		{
			client.setAccount(account);
			client.setState(LoginClientState.AUTHED_LOGIN);
			client.setSessionKey(lc.assignSessionKeyToClient());
			if(Config.SHOW_LICENCE)
				client.sendPacket(new LoginOk(client.getSessionKey()));
			else
				client.sendPacket(new ServerList(client));
		}
		else if(state == State.FAKE_LOGIN)
		{
			client.setState(LoginClientState.FAKE_LOGIN);
			client.setSessionKey(lc.assignSessionKeyToClient());
			if(Config.SHOW_LICENCE)
				client.sendPacket(new LoginOk(client.getSessionKey()));
			else
				client.sendPacket(new ServerList(client));
		}
		else if(state == State.WRONG)
			client.close(LoginFail.REASON_USER_OR_PASS_WRONG);
		else if(state == State.BANNED)
			client.close(new AccountKicked(AccountKickedReason.REASON_PERMANENTLY_BANNED));
		else if(state == State.IP_ACCESS_DENIED)
			client.close(LoginFail.REASON_ATTEMPTED_RESTRICTED_IP);
		// обработка после бана при подборе паролей
		else if(state == State.AUTH_BANNED)
			client.close(LoginFail.REASON_SERVER_OVERLOADED);
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}
}
