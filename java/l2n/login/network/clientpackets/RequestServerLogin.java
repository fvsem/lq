package l2n.login.network.clientpackets;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.login.LoginController;
import l2n.login.SessionKey;
import l2n.login.network.L2LoginClient;
import l2n.login.network.serverpackets.LoginFail;
import l2n.login.network.serverpackets.PlayOk;

import java.util.logging.Level;

public class RequestServerLogin extends L2LoginClientPacket
{
	private int _skey1;
	private int _skey2;
	private int _serverId;

	public int getSessionKey1()
	{
		return _skey1;
	}

	public int getSessionKey2()
	{
		return _skey2;
	}

	public int getServerID()
	{
		return _serverId;
	}

	@Override
	public boolean readImpl()
	{
		if(getAvaliableBytes() >= 9)
		{
			_skey1 = readD();
			_skey2 = readD();
			_serverId = readC();
			return true;
		}
		return false;
	}

	@Override
	public void runImpl()
	{
		L2LoginClient client = getClient();
		if(client == null)
			return;

		SessionKey sk = client.getSessionKey();

		// if we didnt showed the license we cant check these values
		if(!Config.SHOW_LICENCE || sk.checkLoginPair(_skey1, _skey2))
		{
			if(LoginController.getInstance().isLoginPossible(client, _serverId))
			{
				client.sendPacket(new PlayOk(sk));
				if(client.getLastServer() == _serverId)
					return;

				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("UPDATE accounts SET lastServer=? WHERE login=?");
					statement.setInt(1, _serverId);
					statement.setString(2, client.getAccount());
					statement.executeUpdate();
				}
				catch(Exception e)
				{
					_log.log(Level.WARNING, "", e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement);
				}

				client.setPendingTime(System.currentTimeMillis());
			}
			else
				client.close(LoginFail.REASON_ACCESS_FAILED);
		}
		else
			client.close(LoginFail.REASON_ACCESS_FAILED);
	}
}
