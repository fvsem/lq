package l2n.login.network.clientpackets;

import l2n.Config;
import l2n.login.network.L2LoginClient;
import l2n.login.network.L2LoginClient.LoginClientState;
import l2n.login.network.serverpackets.GGAuth;
import l2n.login.network.serverpackets.LoginFail;

public class RequestAuthGameGuard extends L2LoginClientPacket
{
	private int _sessionId;
	private byte[] _gameGuard;

	public RequestAuthGameGuard()
	{
		_gameGuard = new byte[16];
	}

	@Override
	protected boolean readImpl()
	{
		if(getAvaliableBytes() >= 20)
		{
			_sessionId = readD();
			readB(_gameGuard);
			return true;
		}
		return false;
	}

	@Override
	public void runImpl()
	{
		L2LoginClient lc = getClient();
		if(!Config.LOGIN_GG_CHECK || _sessionId == lc.getSessionId())
		{
			lc.setGameGuard(_gameGuard);
			lc.setState(LoginClientState.AUTHED_GG);
			lc.sendPacket(new GGAuth(_sessionId));
		}
		else
			lc.close(LoginFail.REASON_ACCESS_FAILED);
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}
}
