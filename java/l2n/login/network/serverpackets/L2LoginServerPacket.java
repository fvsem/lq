package l2n.login.network.serverpackets;

import l2n.commons.network.SendablePacket;
import l2n.login.network.L2LoginClient;
import l2n.login.network.clientpackets.L2LoginClientPacket;

public abstract class L2LoginServerPacket extends SendablePacket<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>
{
	@Override
	protected int getHeaderSize()
	{
		return 2;
	}

	@Override
	protected void writeHeader(int dataSize)
	{
		writeH(dataSize + getHeaderSize());
	}
}
