package l2n.login.network.serverpackets;

import javolution.util.FastList;
import l2n.game.loginservercon.AdvIP;
import l2n.login.GameServerTable;
import l2n.login.gameservercon.GameServerInfo;
import l2n.login.gameservercon.receive.ServerStatus;
import l2n.login.network.L2LoginClient;
import l2n.util.Util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

public final class ServerList extends L2LoginServerPacket
{
	private List<ServerData> _servers;
	private int _lastServer;

	public class ServerData
	{
		public String ip;
		public int port;
		public boolean pvp;
		public int currentPlayers;
		public int maxPlayers;
		public boolean testServer;
		public boolean brackets;
		public boolean clock;
		public int status;
		public int server_id;

		public ServerData(String pIp, int pPort, boolean pPvp, boolean pTestServer, int pCurrentPlayers, int pMaxPlayers, boolean pBrackets, boolean pClock, int pStatus, int pServer_id)
		{
			ip = pIp;
			port = pPort;
			pvp = pPvp;
			testServer = pTestServer;
			currentPlayers = pCurrentPlayers;
			maxPlayers = pMaxPlayers;
			brackets = pBrackets;
			clock = pClock;
			status = pStatus;
			server_id = pServer_id;
		}
	}

	public ServerList(L2LoginClient client)
	{
		_client = client;
		_servers = new FastList<ServerData>();
		_lastServer = client.getLastServer();

		for(GameServerInfo gsi : GameServerTable.getInstance().getRegisteredGameServers().values())
		{
			boolean added = false;

			String client_ip = _client.getIpAddress();
			if(client_ip == null || client_ip.equalsIgnoreCase("Null IP"))
				continue;

			String ipAddr = Util.isInternalIP(client_ip) ? gsi.getInternalHost() : gsi.getExternalHost();
			if(ipAddr == null || ipAddr.equalsIgnoreCase("Null IP"))
				continue;

			if(gsi.getAdvIP() != null)
			{
				for(AdvIP ip : gsi.getAdvIP())
				{
					if(!added && GameServerTable.getInstance().CheckSubNet(client.getConnection().getSocket().getInetAddress().getHostAddress(), ip))
					{
						added = true;
						addServer(ip.ipadress, gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), gsi.getStatus(), gsi.getId());
					}
				}
			}

			if(ipAddr.equals("*"))
				ipAddr = client.getConnection().getSocket().getLocalAddress().getHostAddress();

			if(!added)
			{
				if(gsi.getStatus() == ServerStatus.STATUS_GM_ONLY && client.getAccessLevel() >= 100)
					// Server is GM-Only but you've got GM Status
					addServer(ipAddr, gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), gsi.getStatus(), gsi.getId());
				else if(gsi.getStatus() != ServerStatus.STATUS_GM_ONLY)
					// Server is not GM-Only
					addServer(ipAddr, gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), gsi.getStatus(), gsi.getId());
				else
					// Server's GM-Only and you've got no GM-Status
					addServer("127.0.0.1", 0, gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), ServerStatus.STATUS_DOWN, gsi.getId());
			}
		}
	}

	public void addServer(String ip, int port, boolean pvp, boolean testServer, int currentPlayer, int maxPlayer, boolean brackets, boolean clock, int status, int server_id)
	{
		_servers.add(new ServerData(ip, port, pvp, testServer, currentPlayer, maxPlayer, brackets, clock, status, server_id));
	}

	@Override
	public void write()
	{
		writeC(0x04);
		writeC(_servers.size());
		writeC(_lastServer);
		for(ServerData server : _servers)
		{
			writeC(server.server_id); // server id

			try
			{
				InetAddress i4 = InetAddress.getByName(server.ip);
				byte[] raw = i4.getAddress();
				writeC(raw[0] & 0xff);
				writeC(raw[1] & 0xff);
				writeC(raw[2] & 0xff);
				writeC(raw[3] & 0xff);
			}
			catch(UnknownHostException e)
			{
				e.printStackTrace();
				writeC(127);
				writeC(0);
				writeC(0);
				writeC(1);
			}

			writeD(server.port); // do not show server port to non-GMs if GM Only
			writeC(0x00); // age limit
			writeC(server.pvp ? 0x01 : 0x00);
			writeH(server.currentPlayers);
			writeH(server.maxPlayers);
			writeC(server.status == ServerStatus.STATUS_DOWN ? 0x00 : 0x01);
			int bits = 0;
			if(server.testServer)
				bits |= 0x04;
			if(server.clock)
				bits |= 0x02;
			writeD(bits);
			writeC(server.brackets ? 0x01 : 0x00);
		}
	}
}
