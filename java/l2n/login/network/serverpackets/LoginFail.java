package l2n.login.network.serverpackets;

public final class LoginFail extends L2LoginServerPacket
{
	public static final LoginFail REASON_NO_MESSAGE = new LoginFail(0x00);
	public static final LoginFail REASON_SYSTEM_ERROR = new LoginFail(0x01); // There is a system error. Please log in again later
	public static final LoginFail REASON_PASS_WRONG = new LoginFail(0x02); // The password you have entered is incorrect. Confirm your ...
	public static final LoginFail REASON_USER_OR_PASS_WRONG = new LoginFail(0x03);
	public static final LoginFail REASON_ACCESS_FAILED_TRYA1 = new LoginFail(0x04); // Access failed. Please try again later.
	public static final LoginFail REASON_ACCOUNT_INFO_INCORR = new LoginFail(0x05); // Your account information is incorrect. For more details ...
	public static final LoginFail REASON_ACCESS_FAILED_TRYA2 = new LoginFail(0x06); // Access failed. Please try again later.
	public static final LoginFail REASON_ACCOUNT_IN_USE = new LoginFail(0x07); // Account is already in use. Unable to log in.
	public static final LoginFail REASON_MIN_AGE = new LoginFail(0x0c); // Lineage II game services may be used by individuals 15 years of age or older ...
	public static final LoginFail REASON_SERVER_OVERLOADED = new LoginFail(0x0f); // Due to high traffic, your login attempt has failed.
	public static final LoginFail REASON_SERVER_MAINTENANCE = new LoginFail(0x10); // Currently undergoing game server maintenance. Please log in again later
	public static final LoginFail REASON_CHANGE_TEMP_PASS = new LoginFail(0x11); // Please login after changing your temporary password.
	public static final LoginFail REASON_USAGE_TEMP_EXPIRED = new LoginFail(0x12); // Your usage term has expired. PlayNC website ...
	public static final LoginFail REASON_TIME_LEFT_EXPIRED = new LoginFail(0x13); // There is no time left on this account.
	public static final LoginFail REASON_SYS_ERR = new LoginFail(0x14); // System Error.
	public static final LoginFail REASON_ACCESS_FAILED = new LoginFail(0x15); // Access Filed.
	public static final LoginFail REASON_ATTEMPTED_RESTRICTED_IP = new LoginFail(0x16); // Game connection attempted through a restricted IP.
	public static final LoginFail REASON_WEEK_USAGE_TIME_END = new LoginFail(0x1e); // This week's usage time has finished.
	public static final LoginFail REASON_SECURITY_CARD_NUMB_I = new LoginFail(0x1f); // The security card number is invalid.
	public static final LoginFail REASON_VERIFY_AGE = new LoginFail(0x20); // Users who have not verified their age may not log in ...
	public static final LoginFail REASON_CANNOT_ACC_COUPON = new LoginFail(0x21); // This server cannot be accessed by the coupon you are using.
	public static final LoginFail REASON_DUAL_BOX = new LoginFail(0x23);
	public static final LoginFail REASON_ACCOUNT_INACTIVE = new LoginFail(0x24); // Your account is currently inactive because you have not logged ...
	public static final LoginFail REASON_USER_AGREEMENT_DIS = new LoginFail(0x25); // You must accept the User Agreement before this account ...
	public static final LoginFail REASON_GUARDIAN_CONSENT_REQ = new LoginFail(0x26); // A guardian's consent is required before this account ...
	public static final LoginFail REASON_USER_AGREEMENT_DEC = new LoginFail(0x27); // This account has declined the User Agreement or is pending ...
	public static final LoginFail REASON_ACCOUNT_SUSPENDED = new LoginFail(0x28); // This account has been suspended ...
	public static final LoginFail REASON_CHANGE_PASS_AND_QUIZ = new LoginFail(0x29); // Your account can only be used after changing your password and quiz ...
	public static final LoginFail REASON_LOGGED_INTO_10_ACCS = new LoginFail(0x2a); // You are currently logged into 10 of your accounts and can no longer ...
	private int reason_code;

	public LoginFail(int reason)
	{
		reason_code = reason;
	}

	@Override
	protected void write()
	{
		writeC(0x01);
		writeD(reason_code);
	}
}
