package l2n.login.network;

import l2n.Config;
import l2n.commons.network.IPacketHandler;
import l2n.login.network.L2LoginClient.LoginClientState;
import l2n.login.network.clientpackets.*;
import l2n.login.network.serverpackets.L2LoginServerPacket;
import l2n.login.network.serverpackets.LoginFail;
import l2n.login.protect.IpBlockTable;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2LoginPacketHandler implements IPacketHandler<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>
{
	private static final Logger _log = Logger.getLogger(L2LoginPacketHandler.class.getName());

	@Override
	public L2LoginClientPacket handlePacket(ByteBuffer buf, L2LoginClient client, final int opcode)
	{
		L2LoginClientPacket packet = null;
		final LoginClientState state = client.getState();
		switch (state)
		{
			case CONNECTED:
				if(opcode == 0x07)
					packet = new RequestAuthGameGuard();
				else
					debugOpcode(opcode, state, client);
				break;
			case AUTHED_GG:
				if(opcode == 0x00)
					packet = new RequestAuthLogin();
				else if(opcode != 0x05) // на случай когда клиент зажимает ентер
					debugOpcode(opcode, state, client);
				break;
			case AUTHED_LOGIN:
				if(opcode == 0x05)
					packet = new RequestServerList();
				else if(opcode == 0x02)
					packet = new RequestServerLogin();
				else
					debugOpcode(opcode, state, client);
				break;
			case FAKE_LOGIN:
				if(opcode == 0x05)
					packet = new RequestServerList();
				else if(opcode == 0x02)
					client.close(LoginFail.REASON_SYS_ERR);
				else
					debugOpcode(opcode, state, client);
				break;
		}
		return packet;
	}

	private void debugOpcode(int opcode, LoginClientState state, L2LoginClient client)
	{
		if(Config.ENABLE_BAN_SUSPICIOUS_ACTIVITY)
			IpBlockTable.getInstance().addIPBann(client, "auto-system", System.currentTimeMillis() + Config.BAN_SUSPICIOUS_BLOCK_TIME, "Suspicious Activity [Unknown Opcode]");
		_log.log(Level.INFO, "Unknown Opcode: " + opcode + " for state: " + state.name() + " from IP: " + client);
	}
}
