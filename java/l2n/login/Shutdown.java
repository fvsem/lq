package l2n.login;

import l2n.Server;
import l2n.commons.threading.RunnableStatsManager;
import l2n.commons.threading.RunnableStatsManager.SortBy;
import l2n.login.protect.FloodersTable;
import l2n.login.protect.IpBlockTable;

import java.util.logging.Logger;

public class Shutdown extends Thread
{
	private static final Logger _log = Logger.getLogger(Shutdown.class.getName());

	private static Shutdown _instance;
	private static Shutdown _counterInstance = null;

	private int secondsShut;
	private int shutdownMode;

	public static final int SIGTERM = 0;
	public static final int GM_SHUTDOWN = 1;
	public static final int GM_RESTART = 2;
	public static final int ABORT = 3;

	private static String[] _modeText = {
			"brought down",
			"brought down",
			"restarting",
			"aborting" };

	public int get_seconds()
	{
		if(_counterInstance != null)
			return _counterInstance.secondsShut;
		return -1;
	}

	public int get_mode()
	{
		if(_counterInstance != null)
			return _counterInstance.shutdownMode;
		return -1;
	}

	public void startShutdownH(final int hours, final boolean restart)
	{
		if(hours < 0)
			return;

		if(_counterInstance != null)
			_counterInstance._abort();

		_counterInstance = new Shutdown(hours * 3600, restart);
		_counterInstance.start();
	}

	public Shutdown()
	{
		secondsShut = -1;
		shutdownMode = SIGTERM;
	}

	public Shutdown(int seconds, final boolean restart)
	{
		if(seconds < 0)
			seconds = 0;
		secondsShut = seconds;

		_log.info("Restarting in " + secondsShut + " sec.");

		if(restart)
			shutdownMode = GM_RESTART;
		else
			shutdownMode = GM_SHUTDOWN;
	}

	public static Shutdown getInstance()
	{
		if(_instance == null)
			_instance = new Shutdown();
		return _instance;
	}

	@Override
	public void run()
	{
		if(this == _instance)
		{
			try
			{
				System.out.println("IpBlockTable: save data...");
				IpBlockTable.getInstance().saveBannedIPs();
				FloodersTable.getInstance().shutdown();
				IpBlockTable.getInstance().shutdown();

				System.out.println("RunnableStatsManager: Method stats successfully dumped.");
				RunnableStatsManager.dumpClassStats(SortBy.MAX);
			}
			catch(final Throwable t)
			{}

			try
			{
				L2LoginThreadPools.getInstance().shutdown();
			}
			catch(final Throwable t)
			{}

			// server will quit, when this function ends.
			System.out.println("Shutdown finished.");
			Server.halt(_instance.shutdownMode == GM_RESTART ? 2 : 0, "LS Shutdown");
		}
		else
		{
			// gm shutdown: send warnings and then call exit to start shutdown sequence
			countdown();
			// last point where logging is operational :(
			_log.info("Shutdown countdown is over. " + _modeText[shutdownMode] + " NOW!");
			switch (shutdownMode)
			{
				case GM_SHUTDOWN:
					_instance.setMode(GM_SHUTDOWN);
					Server.exit(0, "GM_SHUTDOWN");
					break;
				case GM_RESTART:
					_instance.setMode(GM_RESTART);
					Server.exit(2, "GM_RESTART");
					break;
			}
		}
	}

	public void startShutdown(final int seconds, final boolean restart)
	{
		if(_counterInstance != null)
			_counterInstance._abort();

		// the main instance should only run for shutdown hook, so we start a new instance
		_counterInstance = new Shutdown(seconds, restart);
		_counterInstance.start();
	}

	private void countdown()
	{
		try
		{
			while (secondsShut > 0)
			{
				secondsShut--;

				final int delay = 1000; // milliseconds
				Thread.sleep(delay);

				if(shutdownMode == ABORT)
					break;
			}
		}
		catch(final InterruptedException e)
		{
			// this will never happen
		}
	}

	private void setMode(final int mode)
	{
		shutdownMode = mode;
	}

	private void _abort()
	{
		shutdownMode = ABORT;
	}
}
