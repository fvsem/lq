package l2n.login;

import l2n.Config;
import l2n.Server;
import l2n.commons.network.SelectorConfig;
import l2n.commons.network.SelectorThread;
import l2n.commons.threading.ThreadPoolFactory.NextUncaughtExceptionHandler;
import l2n.database.L2DatabaseFactory;
import l2n.game.GameServer;
import l2n.login.gameservercon.GSConnection;
import l2n.login.network.L2LoginClient;
import l2n.login.network.L2LoginPacketHandler;
import l2n.login.network.clientpackets.L2LoginClientPacket;
import l2n.login.network.serverpackets.L2LoginServerPacket;
import l2n.login.protect.IpBlockTable;
import l2n.status.Status;
import l2n.util.Util;

import java.io.IOException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.logging.Logger;

public class L2LoginServer
{
	public static final int PROTOCOL_REV = 0x0102;

	private final static Logger _log = Logger.getLogger(L2LoginServer.class.getName());

	private static L2LoginServer _instance;
	private final GSConnection _gameServerListener;
	private SelectorThread<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> _selectorThread;
	public static Status statusServer;
	public LoginController loginController;

	public static void main(final String[] args) throws Throwable
	{
		Server.SERVER_MODE = Server.MODE_LOGINSERVER;

		// Load Config
		Config.initLogging();
		Config.load();

		if(Config.COMBO_MODE)
		{
			Server.SERVER_MODE = Server.MODE_COMBOSERVER;
			Config.load();
		}

		/* Accepting connections from players */
		Util.waitForFreePorts(Config.LOGIN_HOST, Config.PORT_LOGIN);

		L2DatabaseFactory.getInstance().getConnection().close();

		_instance = new L2LoginServer();
	}

	public static L2LoginServer getInstance()
	{
		return _instance;
	}

	public L2LoginServer()
	{
		Thread.setDefaultUncaughtExceptionHandler(NextUncaughtExceptionHandler.STATIC_INSTANCE);

		LoginController.getInstance();

		try
		{
			GameServerTable.load();
		}
		catch(final GeneralSecurityException e)
		{
			_log.severe("FATAL: Failed to load GameServerTable. Reason: " + e.getMessage());
			if(Config.LOGIN_DEBUG)
				e.printStackTrace();
			Server.exit(1, "FATAL: Failed to load GameServerTable. Reason: " + e.getMessage());
		}
		catch(final SQLException e)
		{
			_log.severe("FATAL: Failed to load GameServerTable. Reason: " + e.getMessage());
			if(Config.LOGIN_DEBUG)
				e.printStackTrace();
			Server.exit(1, "FATAL: Failed to load GameServerTable. Reason: " + e.getMessage());
		}

		InetAddress ad = null;
		try
		{
			ad = InetAddress.getByName(Config.LOGIN_HOST);
		}
		catch(final Exception e)
		{}

		final L2LoginPacketHandler loginPacketHandler = new L2LoginPacketHandler();
		final SelectorHelper sh = new SelectorHelper();
		final SelectorConfig<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> sc = new SelectorConfig<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>(sh);
		sc.setSelectorSleepTime(40);
		try
		{
			_selectorThread = new SelectorThread<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>(sc, loginPacketHandler, sh, sh);
		}
		catch(final IOException e)
		{
			_log.severe("FATAL: Failed to open Selector. Reason: " + e.getMessage());
			if(Config.LOGIN_DEBUG)
				e.printStackTrace();
			System.exit(1);
		}

		_gameServerListener = GSConnection.getInstance();
		_gameServerListener.start();
		_log.info("Listening for GameServers on " + Config.GAME_SERVER_LOGIN_HOST + ":" + Config.GAME_SERVER_LOGIN_PORT);

		if(Config.IS_LOGIN_TELNET_ENABLED)
			try
			{
				statusServer = new Status(Server.MODE_LOGINSERVER);
				statusServer.start();
			}
			catch(final IOException e)
			{
				_log.severe("Failed to start the Telnet Server. Reason: " + e.getMessage());
				if(Config.LOGIN_DEBUG)
					e.printStackTrace();
			}
		else
			_log.info("LoginServer Telnet server is currently disabled.");

		try
		{
			_selectorThread.setAntiFlood(Config.ANTIFLOOD_ENABLE);
			if(Config.ANTIFLOOD_ENABLE)
				_selectorThread.setAntiFloodSocketsConf(Config.MAX_UNHANDLED_SOCKETS_PER_IP, Config.UNHANDLED_SOCKET_MIN_TTL);
			_selectorThread.openServerSocket(ad, Config.PORT_LOGIN);
		}
		catch(final IOException e)
		{
			_log.severe("FATAL: Failed to open server socket on " + ad + ":" + Config.PORT_LOGIN + ". Reason: " + e.getMessage());
			if(Config.LOGIN_DEBUG)
				e.printStackTrace();
			System.exit(1);
		}
		_selectorThread.start();
		_log.info("Login Server ready on port " + Config.PORT_LOGIN);
		_log.info(IpBlockTable.getInstance().size() + " banned IPs defined");

		if(Config.COMBO_MODE)
			try
			{
				if(Config.IS_TELNET_ENABLED)
				{
					final Status _statusServer = new Status(Server.MODE_GAMESERVER);
					_statusServer.start();
				}
				else
					_log.info("GameServer Telnet server is currently disabled.");
				new GameServer();
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

		final Shutdown shutdownHandler = Shutdown.getInstance();
		Runtime.getRuntime().addShutdownHook(shutdownHandler);
		shutdownHandler.startShutdownH(Config.LRESTART_TIME, true);
		
		_log.info("-------------------- Login Server --------------------");
	}

	public GSConnection getGameServerListener()
	{
		return _gameServerListener;
	}

	public boolean setPassword(final String account, final String password)
	{
		return loginController.setPassword(account, password);
	}
}
