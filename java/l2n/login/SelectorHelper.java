package l2n.login;

import l2n.commons.network.*;
import l2n.login.network.L2LoginClient;
import l2n.login.network.clientpackets.L2LoginClientPacket;
import l2n.login.network.serverpackets.Init;
import l2n.login.network.serverpackets.L2LoginServerPacket;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class SelectorHelper extends TCPHeaderHandler<L2LoginClient> implements IClientFactory<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>, IAcceptFilter
{
	public SelectorHelper()
	{
		super(null);
	}

	@Override
	public L2LoginClient create(final MMOConnection<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket> con)
	{
		final L2LoginClient client = new L2LoginClient(con);
		client.sendPacket(new Init(client));
		return client;
	}

	@Override
	public boolean accept(final SocketChannel sc)
	{
		return !LoginController.getInstance().isBannedAddress(sc.socket().getInetAddress());
	}

	@SuppressWarnings("unchecked")
	@Override
	public HeaderInfo<L2LoginClient> handleHeader(final SelectionKey key, final ByteBuffer buf)
	{
		if(buf.remaining() >= 2)
		{
			final int dataPending = (buf.getShort() & 0xffff) - 2;
			final L2LoginClient client = ((MMOConnection<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>) key.attachment()).getClient();
			return getHeaderInfoReturn().set(0, dataPending, false, client);
		}
		final L2LoginClient client = ((MMOConnection<L2LoginClient, L2LoginClientPacket, L2LoginServerPacket>) key.attachment()).getClient();
		return getHeaderInfoReturn().set(2 - buf.remaining(), 0, false, client);
	}
}
