package l2n.login.gameservercon.send;

public class ChangePasswordResponse extends ServerBasePacket
{
	public ChangePasswordResponse(String account, boolean hasChanged)
	{
		writeC(0x06);
		writeS(account);
		writeD(hasChanged ? 1 : 0);
	}
}
