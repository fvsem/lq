package l2n.login.gameservercon.send;

public class LoginServerFail extends ServerBasePacket
{
	public static final LoginServerFail REASON_IP_BANNED = new LoginServerFail(1);
	public static final LoginServerFail REASON_IP_RESERVED = new LoginServerFail(2);
	public static final LoginServerFail REASON_WRONG_HEXID = new LoginServerFail(3);
	public static final LoginServerFail REASON_ID_RESERVED = new LoginServerFail(4);
	public static final LoginServerFail REASON_NO_FREE_ID = new LoginServerFail(5);
	public static final LoginServerFail NOT_AUTHED = new LoginServerFail(6);
	public static final LoginServerFail REASON_ALREADY_LOGGED8IN = new LoginServerFail(7);

	public LoginServerFail(int reason)
	{
		writeC(0x01);
		writeC(reason);
	}
}
