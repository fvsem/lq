package l2n.login.gameservercon.send;

import l2n.login.protect.BanInformation;
import l2n.login.protect.IpBlockTable;

public class BanIPList extends ServerBasePacket
{
	public BanIPList()
	{
		BanInformation[] baniplist = IpBlockTable.getInstance().getBanInformations();
		writeC(0x05);
		writeD(baniplist.length);
		for(BanInformation temp : baniplist)
		{
			writeS(temp.ip);
			writeS(temp.admin);
		}
		baniplist = null;
	}
}
