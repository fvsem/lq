package l2n.login.gameservercon;

import gnu.trove.set.hash.THashSet;
import l2n.commons.crypt.NewCrypt;
import l2n.login.GameServerTable;
import l2n.login.L2LoginThreadPools;
import l2n.login.LoginController;
import l2n.login.gameservercon.receive.ClientBasePacket;
import l2n.login.gameservercon.send.KickPlayer;
import l2n.login.gameservercon.send.ServerBasePacket;
import l2n.util.Util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayDeque;
import java.util.logging.Logger;

public class AttGameServer
{
	private static final Logger log = Logger.getLogger(AttGameServer.class.getName());

	private final ByteBuffer readBuffer = ByteBuffer.allocate(64 * 1024).order(ByteOrder.LITTLE_ENDIAN);
	private final ArrayDeque<ServerBasePacket> sendQueue = new ArrayDeque<ServerBasePacket>();

	private final SelectionKey key;
	private RSACrypt rsa;
	private NewCrypt crypt;
	private int serverId = -1, protocolVersion = 0;
	private boolean _isAuthed;
	private GameServerInfo gameServerInfo;

	private int _online = 0;

	private final THashSet<String> _accountsOnGameServer;

	public AttGameServer(final SelectionKey sc)
	{
		key = sc;
		new KeyTask(this).start();

		_accountsOnGameServer = new THashSet<String>();

		if(GSConnection.DEBUG_LS_GS)
			log.info("LS Debug: RSAKey task started");
	}

	public void sendPacket(final ServerBasePacket packet)
	{
		if(!key.isValid())
			return;

		if(GSConnection.DEBUG_LS_GS)
			log.info("LS Debug: adding packet to sendQueue: " + packet.getClass().getName());

		synchronized (sendQueue)
		{
			sendQueue.addLast(packet);
			key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
		}

		if(GSConnection.DEBUG_LS_GS)
			log.info("LS Debug: Packet added");
	}

	public void onClose()
	{
		try
		{
			if(isAuthed())
			{
				setAuthed(false);
				log.info("LoginServer: Connection with gameserver " + getServerId() + " [" + getName() + "] lost.");
				if(gameServerInfo.getHexId().length == 0)
				{
					log.info("LoginServer: delete from GameServerTable [ID: " + getServerId() + "]");
					GameServerTable.getInstance().deleteGameServer(getServerId(), gameServerInfo);
				}
			}

			GSConnection.getInstance().removeGameserver(this);
			sendQueue.clear();
			if(gameServerInfo != null)
			{
				for(final String account : _accountsOnGameServer)
					LoginController.getInstance().removeAuthedLoginClient(account);
				gameServerInfo.setDown();
			}
			gameServerInfo = null;
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public ByteBuffer getReadBuffer()
	{
		return readBuffer;
	}

	public ArrayDeque<ServerBasePacket> getSendQueue()
	{
		return sendQueue;
	}

	public byte[] encrypt(final byte[] data) throws IOException
	{
		if(crypt == null)
			return data;
		return crypt.crypt(data);
	}

	public byte[] decrypt(final byte[] data) throws IOException
	{
		if(crypt == null)
			return data;
		return crypt.decrypt(data);
	}

	public void processData()
	{
		try
		{
			final ByteBuffer buf = getReadBuffer();

			final int position = buf.position();
			if(position < 2) // У нас недостаточно данных для получения длинны пакета
				return;

			// Получаем длинну пакета
			int lenght = Util.getPacketLength(buf.get(0), buf.get(1));

			// Пакетик не дошел целиком, ждем дальше
			if(lenght > position)
				return;

			byte[] data = new byte[position];
			for(int i = 0; i < position; i++)
				data[i] = buf.get(i);

			buf.clear();

			while ((lenght = Util.getPacketLength(data[0], data[1])) <= data.length)
			{
				data = processPacket(data, lenght);
				if(data.length < 2)
					break;
			}

			buf.put(data);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	private byte[] processPacket(final byte[] data, final int lenght)
	{
		try
		{
			final byte[] remaining = new byte[data.length - lenght];
			final byte[] packet = new byte[lenght - 2];

			System.arraycopy(data, 2, packet, 0, lenght - 2);
			System.arraycopy(data, lenght, remaining, 0, remaining.length);

			final ClientBasePacket runnable = PacketHandler.handlePacket(packet, this);
			if(runnable != null)
			{
				if(GSConnection.DEBUG_LS_GS)
					log.info("LoginServer: Reading packet from GS [" + getServerId() + "]: " + runnable.getClass().getSimpleName());
				L2LoginThreadPools.getInstance().execute(runnable);
			}

			return remaining;
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			return new byte[] {};
		}
	}

	public void initBlowfish(final byte[] key)
	{
		crypt = new NewCrypt(key);
		log.info("Init connection crypt for gameserver " + getConnectionIpAddress() + ".");
	}

	public byte[] RSADecrypt(final byte[] data) throws Exception
	{
		return rsa.decryptRSA(data);
	}

	public byte[] RSAEncrypt(final byte[] data) throws Exception
	{
		return rsa.encryptRSA(data);
	}

	public byte[] getRSAPublicKey()
	{
		return rsa.getRSAPublicKey();
	}

	public void setRSA(final RSACrypt rsa)
	{
		this.rsa = rsa;
	}

	public int getServerId()
	{
		return serverId;
	}

	public void setServerId(final int serverId)
	{
		this.serverId = serverId;
	}

	public boolean isAuthed()
	{
		return _isAuthed;
	}

	public void setAuthed(final boolean authed)
	{
		_isAuthed = authed;
	}

	public final void addAccountInGameServer(String account)
	{
		account = account.toLowerCase();
		synchronized (_accountsOnGameServer)
		{
			if(_accountsOnGameServer.contains(account))
				return;
			_accountsOnGameServer.add(account);
		}
	}

	public final void addAccountsInGameServer(final String[] accounts)
	{
		for(int i = accounts.length; i-- > 0;)
			accounts[i] = accounts[i].toLowerCase();

		synchronized (_accountsOnGameServer)
		{
			for(int i = accounts.length; i-- > 0;)
				if(!_accountsOnGameServer.contains(accounts[i]))
					_accountsOnGameServer.add(accounts[i]);
		}
	}

	public void removeAccountFromGameServer(String account)
	{
		account = account.toLowerCase();
		synchronized (_accountsOnGameServer)
		{
			_accountsOnGameServer.remove(account);
		}
	}

	public boolean isAccountInGameServer(String account)
	{
		account = account.toLowerCase();
		synchronized (_accountsOnGameServer)
		{
			return _accountsOnGameServer.contains(account);
		}
	}

	public void clearAccountInGameServer()
	{
		synchronized (_accountsOnGameServer)
		{
			_accountsOnGameServer.clear();
		}
	}

	public int getPlayerCount()
	{
		return _online;
	}

	public void setPlayerCount(final int i)
	{
		_online = i;
	}

	public int getProtocolVersion()
	{
		return protocolVersion;
	}

	public void setProtocolVersion(final int ver)
	{
		protocolVersion = ver;
	}

	public GameServerInfo getGameServerInfo()
	{
		return gameServerInfo;
	}

	public void setGameServerInfo(final GameServerInfo gameServerInfo)
	{
		this.gameServerInfo = gameServerInfo;
	}

	public String getName()
	{
		return GameServerTable.getInstance().getServerNames().get(getServerId());
	}

	public String getConnectionIpAddress()
	{
		final SocketChannel channel = (SocketChannel) key.channel();
		return channel.socket().getInetAddress().getHostAddress();
	}

	public void kickPlayer(final String account)
	{
		sendPacket(new KickPlayer(account));
		removeAccountFromGameServer(account);
		LoginController.getInstance().removeAuthedLoginClient(account);
	}

	public boolean isCryptInitialized()
	{
		return crypt != null;
	}

	public SelectionKey getSelectionKey()
	{
		return key;
	}
}
