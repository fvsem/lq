package l2n.login.gameservercon;

import l2n.login.gameservercon.send.RSAKey;

public class KeyTask extends Thread
{
	private final AttGameServer gameserver;

	public KeyTask(AttGameServer gameserver)
	{
		this.gameserver = gameserver;
	}

	@Override
	public void run()
	{
		try
		{
			gameserver.setRSA(new RSACrypt());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		gameserver.sendPacket(new RSAKey(gameserver.getRSAPublicKey()));
	}
}
