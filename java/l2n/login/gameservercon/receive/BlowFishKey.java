package l2n.login.gameservercon.receive;

import l2n.login.gameservercon.AttGameServer;

public class BlowFishKey extends ClientBasePacket
{
	public BlowFishKey(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		int keyLength = readD();
		byte[] data = readB(keyLength);

		try
		{
			data = getGameServer().RSADecrypt(data);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		getGameServer().initBlowfish(data);
	}
}
