package l2n.login.gameservercon.receive;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.login.gameservercon.AttGameServer;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LockAccountIP extends ClientBasePacket
{
	private static final Logger _log = Logger.getLogger(LockAccountIP.class.getName());

	public LockAccountIP(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		String accname = readS();
		String IP = readS();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE `accounts` SET `allow_ips` = ? WHERE login = ?");
			statement.setString(1, IP);
			statement.setString(2, accname);
			statement.executeUpdate();
			DbUtils.close(statement);
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Failed to lock/unlock account '" + accname + "' for IP: " + IP, e);
		}
		finally
		{
			DbUtils.close(con);
		}
	}
}
