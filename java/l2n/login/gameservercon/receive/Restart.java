package l2n.login.gameservercon.receive;

import l2n.login.gameservercon.AttGameServer;

public class Restart extends ClientBasePacket
{
	public Restart(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		System.exit(2); // Полный рестарт логинсервера.
	}
}
