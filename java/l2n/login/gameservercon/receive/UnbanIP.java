package l2n.login.gameservercon.receive;

import l2n.commons.network.utils.Net;
import l2n.login.gameservercon.AttGameServer;
import l2n.login.gameservercon.GSConnection;
import l2n.login.gameservercon.send.BanIPList;
import l2n.login.gameservercon.send.IpAction;
import l2n.login.protect.IpBlockTable;

import java.net.InetAddress;

public class UnbanIP extends ClientBasePacket
{
	public UnbanIP(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		String ip = readS();
		try
		{
			InetAddress address = InetAddress.getByName(ip);
			if(address != null)
			{
				IpBlockTable.getInstance().removeIPBann(Net.getHashFromAddress(address));
				GSConnection.getInstance().broadcastPacket(new BanIPList());
				GSConnection.getInstance().broadcastPacket(new IpAction(ip, false, ""));
			}
		}
		catch(Exception e)
		{}
	}
}
