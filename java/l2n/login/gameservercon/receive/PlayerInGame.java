package l2n.login.gameservercon.receive;

import l2n.login.gameservercon.AttGameServer;

public class PlayerInGame extends ClientBasePacket
{
	public PlayerInGame(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		String acc = readS();
		if(acc == null || acc.isEmpty())
			getGameServer().clearAccountInGameServer();
		else
			getGameServer().addAccountInGameServer(acc);
		getGameServer().setPlayerCount(readH());
	}
}
