package l2n.login.gameservercon.receive;

import l2n.commons.network.utils.Net;
import l2n.login.gameservercon.AttGameServer;
import l2n.login.gameservercon.GSConnection;
import l2n.login.gameservercon.send.BanIPList;
import l2n.login.gameservercon.send.IpAction;
import l2n.login.protect.IpBlockTable;

import java.net.InetAddress;

public class BanIP extends ClientBasePacket
{
	public BanIP(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		String ip = readS();
		String admin = readS();
		try
		{
			InetAddress address = InetAddress.getByName(ip);
			if(address != null)
			{
				IpBlockTable.getInstance().addIPBann(Net.getHashFromAddress(address), ip, admin, 0, "");
				GSConnection.getInstance().broadcastPacket(new BanIPList());
				GSConnection.getInstance().broadcastPacket(new IpAction(ip, true, admin));
			}
		}
		catch(Exception e)
		{}
	}
}
