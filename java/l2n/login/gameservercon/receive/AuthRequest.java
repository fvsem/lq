package l2n.login.gameservercon.receive;

import javolution.util.FastList;
import l2n.Config;
import l2n.game.loginservercon.AdvIP;
import l2n.login.GameServerTable;
import l2n.login.gameservercon.AttGameServer;
import l2n.login.gameservercon.GameServerInfo;
import l2n.login.gameservercon.send.AuthResponse;
import l2n.login.gameservercon.send.BanIPList;
import l2n.login.gameservercon.send.LoginServerFail;

import java.util.Arrays;
import java.util.logging.Logger;

public class AuthRequest extends ClientBasePacket
{
	private final static Logger log = Logger.getLogger(AuthRequest.class.getName());

	public AuthRequest(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		int requestId = readC();
		boolean acceptAlternateID = readC() == 1;
		boolean reserveHostOnLogin = readC() == 1; // FIXME: оно всегда false
		String externalIp = readS();
		String internalIp = readS();
		int port = readH();
		int maxOnline = readD();
		int hexIdLenth = readD();
		byte[] hexId = readB(hexIdLenth);
		int advIpsSize = readD();

		FastList<AdvIP> advIpList = FastList.newInstance();
		for(int i = 0; i < advIpsSize; i++)
		{
			AdvIP ip = new AdvIP();
			ip.ipadress = readS();
			ip.ipmask = readS();
			ip.bitmask = readS();
			advIpList.add(ip);
		}

		int protocolVersion;
		try
		{
			protocolVersion = readH();
		}
		catch(Exception e)
		{
			protocolVersion = 0;
		}

		log.info("Trying to register server: " + requestId + ", " + getGameServer().getConnectionIpAddress());
		AttGameServer client = getGameServer();

		GameServerTable gameServerTable = GameServerTable.getInstance();

		GameServerInfo gsi = gameServerTable.getRegisteredGameServerById(requestId);
		// is there a gameserver registered with this id?
		if(gsi != null)
		{
			// does the hex id match?
			if(Arrays.equals(gsi.getHexId(), hexId))
				// check to see if this GS is already connected
				synchronized (gsi)
				{
					if(gsi.isAuthed())
						sendPacket(LoginServerFail.REASON_ALREADY_LOGGED8IN);
					else
					{
						getGameServer().setGameServerInfo(gsi);
						gsi.setGameServer(getGameServer());
						gsi.setPort(port);
						gsi.setGameHosts(externalIp, internalIp, advIpList);
						gsi.setMaxPlayers(maxOnline);
						gsi.setAuthed(true);
					}
				}
			else // there is already a server registered with the desired id and different hex id
			// try to register this one with an alternative id
			if(Config.ACCEPT_NEW_GAMESERVER && acceptAlternateID)
			{
				gsi = new GameServerInfo(requestId, hexId, client);
				if(gameServerTable.registerWithFirstAvailableId(gsi))
				{
					getGameServer().setGameServerInfo(gsi);
					gsi.setGameServer(getGameServer());
					gsi.setPort(port);
					gsi.setGameHosts(externalIp, internalIp, advIpList);
					gsi.setMaxPlayers(maxOnline);
					gsi.setAuthed(true);
					if(reserveHostOnLogin)
						gameServerTable.registerServerOnDB(gsi);
				}
				else
					sendPacket(LoginServerFail.REASON_NO_FREE_ID);
			}
			else
				// server id is already taken, and we cant get a new one for you
				sendPacket(LoginServerFail.REASON_WRONG_HEXID);
		}
		else if(Config.ACCEPT_NEW_GAMESERVER)
		{
			gsi = new GameServerInfo(requestId, hexId, client);
			if(gameServerTable.register(requestId, gsi))
			{
				getGameServer().setGameServerInfo(gsi);
				gsi.setGameServer(getGameServer());
				gsi.setPort(port);
				gsi.setGameHosts(externalIp, internalIp, advIpList);
				gsi.setMaxPlayers(maxOnline);
				gsi.setAuthed(true);
				if(reserveHostOnLogin)
					gameServerTable.registerServerOnDB(gsi);
			}
			else
				// some one took this ID meanwhile
				sendPacket(LoginServerFail.REASON_ID_RESERVED);
		}
		else
			sendPacket(LoginServerFail.REASON_WRONG_HEXID);

		if(gsi != null && gsi.isAuthed())
		{
			AuthResponse ar = new AuthResponse(gsi.getId());
			client.setProtocolVersion(protocolVersion);
			client.setAuthed(true);
			client.setServerId(gsi.getId());
			sendPacket(ar);
			sendPacket(new BanIPList());
			log.info("Server registration successful.");
		}
		else
			log.info("Server registration failed.");
	}
}
