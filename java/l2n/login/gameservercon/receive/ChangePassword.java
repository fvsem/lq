package l2n.login.gameservercon.receive;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.login.LoginController;
import l2n.login.gameservercon.AttGameServer;
import l2n.login.gameservercon.send.ChangePasswordResponse;
import l2n.login.network.L2LoginClient;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChangePassword extends ClientBasePacket
{
	private static final Logger log = Logger.getLogger(ChangePassword.class.getName());

	public ChangePassword(byte[] decrypt, AttGameServer gameserver)
	{
		super(decrypt, gameserver);
	}

	@Override
	public void read()
	{
		String accname = readS();
		String oldPass = readS();
		String newPass = readS();

		String dbPassword = null;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			try
			{
				statement = con.prepareStatement("SELECT password FROM accounts WHERE login = ?");
				statement.setString(1, accname);
				rs = statement.executeQuery();
				if(rs.next())
					dbPassword = rs.getString("password");
			}
			catch(Exception e)
			{
				log.log(Level.WARNING, "Can't recive old password for account " + accname + ", exciption: " + e, e);
			}
			finally
			{
				DbUtils.closeQuietly(statement, rs);
			}

			try
			{
				if(!LoginController.DEFAULT_CRYPT.compare(oldPass, dbPassword))
				{
					ChangePasswordResponse cp = new ChangePasswordResponse(accname, false);
					sendPacket(cp);
				}
				else
				{
					statement = con.prepareStatement("UPDATE accounts SET password = ? WHERE login = ?");
					statement.setString(1, LoginController.DEFAULT_CRYPT.encrypt(newPass));
					statement.setString(2, accname);
					int result = statement.executeUpdate();
					L2LoginClient client = LoginController.getInstance().getAuthedClient(accname);
					if(result != 0)
						Log.add("<acc=\"" + accname + "\" old=\"" + oldPass + "\" new=\"" + newPass + "\" ip=\"" + (client != null ? client.getIpAddress() : "0.0.0.0") + "\" />", "passwords");

					ChangePasswordResponse cp = new ChangePasswordResponse(accname, result != 0);
					sendPacket(cp);
				}
			}
			catch(Exception e1)
			{
				log.log(Level.WARNING, "", e1);
			}
			finally
			{
				DbUtils.close(statement);
			}
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
		finally
		{
			DbUtils.close(con);
		}
	}
}
