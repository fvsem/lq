package l2n;

import l2n.database.L2DatabaseFactory;
import l2n.game.GameServer;
import l2n.login.L2LoginServer;
import l2n.status.Status;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;


public class Server
{
	public static final int MODE_GAMESERVER = 1;
	public static final int MODE_LOGINSERVER = 2;
	public static final int MODE_COMBOSERVER = 3;
	public static int SERVER_MODE = 0;
	public static GameServer gameServer;
	public static L2LoginServer loginServer;
	public static Status statusServer;
	public static String L2sHomeDir = System.getProperty("user.dir") + "/";

	public static void main(final String[] args) throws Exception
	{
		String L2sHome = System.getProperty("l2n.home");
		if(L2sHome != null)
		{
			final File home = new File(L2sHome);
			if(!home.isAbsolute())
				try
				{
					L2sHome = home.getCanonicalPath();
				}
				catch(final IOException e)
				{
					L2sHome = home.getAbsolutePath();
				}
			L2sHomeDir = L2sHome + "/";
			System.setProperty("user.dir", L2sHome);
		}

		final String LOG_NAME = "/config/log.ini";
		final String CONF_LOC = "l2n.Config";

		Class.forName(CONF_LOC);

		FileUtils.forceMkdir(new File("./log"));

		final InputStream is = new FileInputStream(L2sHomeDir + LOG_NAME);
		LogManager.getLogManager().readConfiguration(is);
		is.close();

		L2DatabaseFactory.getInstance();

		gameServer = new GameServer();
		loginServer = L2LoginServer.getInstance();

		if(Config.MAT_BANCHAT)
			System.out.println("MAT AutoBANChat filter enable.");
		else
			System.out.println("MAT AutoBANChat filter disable.");
	}

	public static void exit(final int status, final String reason)
	{
		System.out.println("Server exiting [status=" + status + "] / Reason: " + reason);
		Runtime.getRuntime().exit(status);
	}

	public static void halt(final int status, final String reason)
	{
		System.out.println("Server halting [status=" + status + "] / Reason: " + reason);
		Runtime.getRuntime().halt(status);
	}
}
