package l2n.commons.text;

import l2n.Config;
import l2n.util.Files;
import l2n.util.StringUtil;

import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Strings
{
	private final static Logger _log = Logger.getLogger(Strings.class.getName());

	private static final char hex[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public static String bytesToString(final byte[] b)
	{
		String ret = "";
		for(final byte element : b)
		{
			ret += String.valueOf(hex[(element & 0xF0) >> 4]);
			ret += String.valueOf(hex[element & 0x0F]);
		}
		return ret;
	}

	public static String addSlashes(String s)
	{
		if(s == null)
			return "";

		s = s.replace("\\", "\\\\");
		s = s.replace("\"", "\\\"");
		s = s.replace("@", "\\@");
		s = s.replace("'", "\\'");
		return s;
	}

	public static String stripSlashes(String s)
	{
		if(s == null)
			return "";
		s = s.replace("\\'", "'");
		s = s.replace("\\\\", "\\");
		return s;
	}

	public static Integer parseInt(final Object x)
	{
		if(x == null)
			return 0;

		if(x instanceof Integer)
			return (Integer) x;

		if(x instanceof Double)
			return ((Double) x).intValue();

		if(x instanceof Boolean)
			return (Boolean) x ? -1 : 0;

		Integer res = 0;
		try
		{
			res = Integer.parseInt("" + x);
		}
		catch(final Exception e)
		{}
		return res;
	}

	public static Double parseFloat(final Object x)
	{
		if(x instanceof Double)
			return (Double) x;

		if(x instanceof Integer)
			return 0.0 + (Integer) x;

		if(x == null)
			return 0.0;

		Double res = 0.0;
		try
		{
			res = Double.parseDouble("" + x);
		}
		catch(final Exception e)
		{}
		return res;
	}

	public static Boolean parseBoolean(final Object x)
	{
		if(x instanceof Integer)
			return (Integer) x != 0;

		if(x == null)
			return false;

		if(x instanceof Boolean)
			return (Boolean) x;

		if(x instanceof Double)
			return Math.abs((Double) x) < 0.00001;

		return !("" + x).equals("");
	}

	private static String[] tr;
	private static String[] trb;
	private static String[] trcode;

	public static void reload()
	{
		Files.cacheClean();
		String[] pairs = Files.read("data/translit.txt").split("\n");
		tr = new String[pairs.length * 2];
		for(int i = 0; i < pairs.length; i++)
		{
			final String[] ss = pairs[i].split(" +");
			tr[i * 2] = ss[0];
			tr[i * 2 + 1] = ss[1];
		}

		pairs = Files.read("data/translit_back.txt").split("\n");
		trb = new String[pairs.length * 2];
		for(int i = 0; i < pairs.length; i++)
		{
			final String[] ss = pairs[i].split(" +");
			trb[i * 2] = ss[0];
			trb[i * 2 + 1] = ss[1];
		}

		pairs = Files.read("data/transcode.txt").split("\n");
		trcode = new String[pairs.length * 2];
		for(int i = 0; i < pairs.length; i++)
		{
			final String[] ss = pairs[i].split(" +");
			trcode[i * 2] = ss[0];
			trcode[i * 2 + 1] = ss[1];
		}
		_log.info("Loaded " + (tr.length + tr.length + trcode.length) + " translit entrys");
	}

	public static String translit(String s)
	{
		for(int i = 0; i < tr.length; i += 2)
			s = s.replace(tr[i], tr[i + 1]);

		return s;
	}

	public static String fromTranslit(String s, final int type)
	{
		if(type == 1)
			for(int i = 0; i < trb.length; i += 2)
				s = s.replace(trb[i], trb[i + 1]);
		else if(type == 2)
			for(int i = 0; i < trcode.length; i += 2)
				s = s.replace(trcode[i], trcode[i + 1]);

		return s;
	}

	public static String replace(final String str, final String regex, final int flags, final String replace)
	{
		return Pattern.compile(regex, flags).matcher(str).replaceAll(replace);
	}

	public static boolean matches(final String str, final String regex, final int flags)
	{
		return Pattern.compile(regex, flags).matcher(str).matches();
	}

	public static boolean matches(final String str, final Pattern patern)
	{
		return patern.matcher(str).matches();
	}

	public static String bbParse(String s)
	{
		if(s == null)
			return null;

		s = s.replace("\r", "");

		if(Config.DEBUG)
			_log.info("Parse string\n==========================\n" + s + "\n==========================\n");

		s = s.replaceAll("(\\s|\"|\'|\\(|^|\n)\\*(.*?)\\*(\\s|\"|\'|\\)|\\?|\\.|!|:|;|,|$|\n)", "$1<font color=\"LEVEL\">$2</font>$3");

		s = replace(s, "^!(.*?)$", Pattern.MULTILINE, "<font color=\"LEVEL\">$1</font>\n\n");

		s = s.replaceAll("%%\\s*\n", "<br1>");

		s = s.replaceAll("\n\n+", "<br>");

		s = replace(s, "\\[([^\\]\\|]*?)\\|([^\\]]*?)\\]", Pattern.DOTALL, "<a action=\"bypass -h $1\">$2</a>");

		s = s.replaceAll(" @", "\" msg=\"");

		if(Config.DEBUG)
			_log.info("to \n==========================\n" + s + "\n==========================\n");

		return s;
	}

	public static String utf2win(final String utfString)
	{
		String winString;
		try
		{
			winString = new String(utfString.getBytes("Cp1251"));
		}
		catch(final UnsupportedEncodingException uee)
		{
			winString = utfString;
		}
		return winString;
	}

	public static String joinStrings(final String glueStr, final String strings[], int startIdx, int maxCount)
	{
		String result = "";
		if(startIdx < 0)
		{
			startIdx += strings.length;
			if(startIdx < 0)
				return result;
		}
		for(; startIdx < strings.length && maxCount != 0; maxCount--)
		{
			if(!result.isEmpty() && glueStr != null && !glueStr.isEmpty())
				result = StringUtil.concat(result, glueStr);
			result = StringUtil.concat(result, strings[startIdx++]);
		}

		return result;
	}

	/**
	 * Из txt в html
	 * 
	 * @param txt
	 * @return
	 */
	public static String edtiPlayerTxT(String txt)
	{
		if(txt == null)
			return "";
		txt = txt.replace(">", "&gt;");
		txt = txt.replace("<", "&lt;");
		txt = txt.replace("\n", "<br1>");
		txt = txt.replace("$", "\\$");
		return txt;
	}

	/**
	 * Из html в txt
	 * 
	 * @param html
	 */
	public static String edtiSavedTxT(String html)
	{
		if(html == null)
			return "";
		html = html.replace("&gt;", ">");
		html = html.replace("&lt;", "<");
		html = html.replace("<br1>", "\n");
		html = html.replace("\\$", "$");
		return html;
	}

	public static String joinStrings(final String glueStr, final String strings[], final int startIdx)
	{
		return joinStrings(glueStr, strings, startIdx, -1);
	}

	public static String joinStrings(final String glueStr, final String... strings)
	{
		return joinStrings(glueStr, strings, 0);
	}

	public static String htmlButton(final String value, final String action, final int width)
	{
		return htmlButton(value, action, width, 22);
	}

	public static String htmlButton(final String value, final String action, final int width, final int height)
	{
		return String.format("<button value=\"%s\" action=\"%s\" back=\"L2UI_CT1.Button_DF_Small_Down\" width=%d height=%d fore=\"L2UI_CT1.Button_DF_Small\">", value, action, width, height);
	}
}
