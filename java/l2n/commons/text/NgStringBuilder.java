package l2n.commons.text;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;

public final class NgStringBuilder implements CharSequence, Appendable, Comparable<NgStringBuilder>
{
	public static final char[] LINE_SEPERATOR = System.getProperty("line.separator").toCharArray();

	public static final class IncStringWriter extends Writer
	{
		private final NgStringBuilder _isb;

		private IncStringWriter(final NgStringBuilder isb)
		{
			_isb = isb;
		}

		@Override
		public final void close() throws IOException
		{

		}

		@Override
		public final void flush() throws IOException
		{

		}

		@Override
		public final void write(final char[] cbuf, final int off, final int len) throws IOException
		{
			_isb.append(cbuf, off, off + len);
		}

		public final NgStringBuilder getStringBuilder()
		{
			return _isb;
		}

		public final IncPrintWriter asPrintWriter()
		{
			return new IncPrintWriter(this);
		}
	}

	public static final class IncPrintWriter extends PrintWriter
	{
		private final NgStringBuilder _isb;

		public IncPrintWriter(final IncStringWriter out)
		{
			super(out);
			_isb = out.getStringBuilder();
		}

		@Override
		public final void println(final String val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final char[] val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final Object val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final boolean val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final int val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final double val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final float val)
		{
			print(val);
			println();
		}

		@Override
		public final void println(final long val)
		{
			print(val);
			println();
		}

		@Override
		public final void print(final String val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final Object val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final boolean val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final int val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final double val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final float val)
		{
			_isb.append(val);
		}

		@Override
		public final void print(final long val)
		{
			_isb.append(val);
		}

		@Override
		public final void println()
		{
			_isb.append(LINE_SEPERATOR, 0, LINE_SEPERATOR.length);
		}

		@Override
		public final void write(final int val)
		{
			_isb.append(val);
		}

		@Override
		public final void write(final char[] cbuf, final int start, final int end)
		{
			_isb.append(cbuf, start, end);
		}

		@Override
		public final void write(final String str, final int start, final int end)
		{
			_isb.append(str, start, end);
		}

		@Override
		public final IncPrintWriter append(final char c)
		{
			_isb.append(c);
			return this;
		}

		@Override
		public final IncPrintWriter append(final CharSequence csq)
		{
			_isb.append(csq);
			return this;
		}

		@Override
		public final IncPrintWriter append(final CharSequence csq, final int start, final int end)
		{
			_isb.append(csq, start, end);
			return this;
		}

		@Override
		public final void close()
		{

		}

		@Override
		public final void flush()
		{

		}
	}

	private static final char[][] BYTE_NUMBERS;

	private static final String STRING_NULL = "null";
	private static final char[] CA_NULL = STRING_NULL.toCharArray();

	private static final char[] DIGIT_ONES = {
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9', };
	private static final char[] DIGIT_TENS = {
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'1',
			'2',
			'2',
			'2',
			'2',
			'2',
			'2',
			'2',
			'2',
			'2',
			'2',
			'3',
			'3',
			'3',
			'3',
			'3',
			'3',
			'3',
			'3',
			'3',
			'3',
			'4',
			'4',
			'4',
			'4',
			'4',
			'4',
			'4',
			'4',
			'4',
			'4',
			'5',
			'5',
			'5',
			'5',
			'5',
			'5',
			'5',
			'5',
			'5',
			'5',
			'6',
			'6',
			'6',
			'6',
			'6',
			'6',
			'6',
			'6',
			'6',
			'6',
			'7',
			'7',
			'7',
			'7',
			'7',
			'7',
			'7',
			'7',
			'7',
			'7',
			'8',
			'8',
			'8',
			'8',
			'8',
			'8',
			'8',
			'8',
			'8',
			'8',
			'9',
			'9',
			'9',
			'9',
			'9',
			'9',
			'9',
			'9',
			'9',
			'9', };

	private static final char[] DIGITS = {
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z' };

	private static final int INITIAL_SIZE = 16;

	private static final char[] INT_MIN_VALUE = Integer.toString(Integer.MIN_VALUE).toCharArray();

	private static final int[] INT_SIZE_TABLE = { 9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999, Integer.MAX_VALUE };
	private static final char[] LONG_MIN_VALUE = Long.toString(Long.MIN_VALUE).toCharArray();

	private static final long[] LONG_SIZE_TABLE = {
			9,
			99,
			999,
			9999,
			99999,
			999999,
			9999999,
			99999999,
			999999999,
			9999999999L,
			99999999999L,
			999999999999L,
			9999999999999L,
			99999999999999L,
			999999999999999L,
			9999999999999999L,
			99999999999999999L,
			999999999999999999L,
			Long.MAX_VALUE, };

	static
	{
		BYTE_NUMBERS = new char[256][];
		for(int i = 256; i-- > 0;)
		{
			BYTE_NUMBERS[i] = Byte.toString((byte) i).toCharArray();
		}
	}

	public static final NgStringBuilder concat(final CharSequence... css)
	{
		int len = 0;
		for(final CharSequence cs : css)
		{
			if(cs != null)
				len += cs.length();
			else
				len += 4; // CA_NULL.length
		}

		final NgStringBuilder is = new NgStringBuilder(len);
		for(final CharSequence cs : css)
		{
			if(cs != null)
			{
				len = cs.length();
				for(int i = 0; i < len; i++)
				{
					is._buf[is._len++] = cs.charAt(i);
				}
			}
			else
			{
				System.arraycopy(CA_NULL, 0, is._buf, is._len, 4);
				is._len += 4;
			}
		}

		return is;
	}

	public static final NgStringBuilder concat(final NgStringBuilder... iss)
	{
		int len = 0;
		for(final NgStringBuilder is : iss)
		{
			if(is != null)
				len += is._len;
			else
				len += 4; // CA_NULL.length
		}

		final NgStringBuilder is = new NgStringBuilder(len);
		for(final NgStringBuilder s : iss)
		{
			if(s != null)
			{
				System.arraycopy(s._buf, 0, is._buf, is._len, s._len);
				is._len += s._len;
			}
			else
			{
				System.arraycopy(CA_NULL, 0, is._buf, is._len, 4);
				is._len += 4;
			}
		}

		return is;
	}

	public static final NgStringBuilder concat(final String... ss)
	{
		int len = 0;
		for(final String s : ss)
		{
			if(s != null)
				len += s.length();
			else
				len += 4; // CA_NULL.length
		}

		final NgStringBuilder is = new NgStringBuilder(len);
		for(final String s : ss)
		{
			if(s != null)
			{
				len = s.length();
				s.getChars(0, len, is._buf, is._len);
				is._len += len;
			}
			else
			{
				System.arraycopy(CA_NULL, 0, is._buf, is._len, 4);
				is._len += 4;
			}
		}

		return is;
	}

	public static final NgStringBuilder concat(final Object... os)
	{
		int len = 0;
		for(int i = os.length; i-- > 0;)
		{
			if(os[i] != null)
			{
				len += ((String) (os[i] = os[i].toString())).length();
			}
			else
			{
				os[i] = STRING_NULL;
				len += 4; // CA_NULL.length
			}
		}

		final NgStringBuilder is = new NgStringBuilder(len);
		for(final Object o : os)
		{
			final String s = (String) o;
			len = s.length();
			s.getChars(0, len, is._buf, is._len);
			is._len += len;
		}

		return is;
	}

	private static final int stringSize(final int x)
	{
		for(int i = 0;; i++)
		{
			if(x <= INT_SIZE_TABLE[i])
				return i + 1;
		}
	}

	private static final int stringSize(final long x)
	{
		for(int i = 0;; i++)
		{
			if(x <= LONG_SIZE_TABLE[i])
				return i + 1;
		}
	}

	public static final int stringSizeOfInteger(final int x)
	{
		if(x < 0)
			return stringSize(-x) + 1;

		return stringSize(x);
	}

	public static final int stringSizeOfLong(final long x)
	{
		if(x < 0)
			return stringSize(-x) + 1;

		return stringSize(x);
	}

	private char[] _buf;

	private int _hash;

	private int _len;

	public NgStringBuilder()
	{
		_buf = new char[INITIAL_SIZE];
		_len = 0;
		_hash = 0;
	}

	public NgStringBuilder(final char[] buf)
	{
		this(buf, true);
	}

	public NgStringBuilder(final char[] buf, final boolean copy)
	{
		if(buf != null)
		{
			_len = buf.length;

			if(copy)
			{
				_buf = new char[_len + INITIAL_SIZE];
				System.arraycopy(buf, 0, _buf, 0, _len);
			}
			else
			{
				_buf = buf;
			}

			_hash = 0;
		}
		else
		{
			_buf = new char[INITIAL_SIZE];
			_len = 0;
			_hash = 0;
		}
	}

	public NgStringBuilder(final char[] buf, final int start, final int end)
	{
		checkBounds(buf.length, start, end);

		_len = end - start;
		_buf = new char[_len + INITIAL_SIZE];
		_hash = 0;
		System.arraycopy(buf, start, _buf, 0, _len);
	}

	public NgStringBuilder(final NgStringBuilder s)
	{
		_len = s._len;
		_buf = new char[_len + INITIAL_SIZE];
		_hash = 0;
		System.arraycopy(s._buf, 0, _buf, 0, _len);
	}

	public NgStringBuilder(final NgStringBuilder is, final int start, final int end)
	{
		checkBounds(is._len, start, end);

		_len = end - start;
		_buf = new char[_len + INITIAL_SIZE];
		_hash = 0;
		System.arraycopy(is._buf, start, _buf, 0, _len);
	}

	public NgStringBuilder(final int capacity)
	{
		_buf = new char[capacity];
		_len = 0;
		_hash = 0;
	}

	public NgStringBuilder(final String s)
	{
		if(s != null)
		{
			_len = s.length();
			_buf = new char[_len + INITIAL_SIZE];
			_hash = 0;
			s.getChars(0, _len, _buf, 0);
		}
		else
		{
			_buf = new char[INITIAL_SIZE];
			_len = 0;
			_hash = 0;
		}
	}

	public NgStringBuilder(final String s, final int start, final int end)
	{
		checkBounds(s.length(), start, end);

		_len = end - start;
		_buf = new char[_len];
		_hash = 0;
		s.getChars(start, end, _buf, 0);
	}

	public NgStringBuilder(final StringBuffer sb)
	{
		this(sb, 0, sb.length());
	}

	public NgStringBuilder(final StringBuffer sb, final int start, final int end)
	{
		checkBounds(sb.length(), start, end);

		_len = end - start;
		_buf = new char[_len + INITIAL_SIZE];
		_hash = 0;
		sb.getChars(start, end, _buf, 0);
	}

	public final NgStringBuilder append(final byte b)
	{
		final char[] number = BYTE_NUMBERS[b & 0xFF];
		return appendInternal(number, 0, number.length);
	}

	/**
	 * Appends the specified char to this character sequence.
	 * 
	 * @param ca
	 *            the char array to be appended.
	 * @return a reference to this object.
	 */
	@Override
	public final NgStringBuilder append(final char c)
	{
		final int newLen = _len + 1;
		ensureCapacity(newLen);
		_buf[_len] = c;
		_len = newLen;
		_hash = 0;
		return this;
	}

	/**
	 * Appends the specified char array to this character sequence.
	 * <p>
	 * If <code>ca</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param ca
	 *            the char array to be appended.
	 * @return a reference to this object.
	 */
	public final NgStringBuilder append(final char[] ca)
	{
		if(ca == null)
			return appendInternal(CA_NULL, 0, 4);

		return appendInternal(ca, 0, ca.length);
	}

	/**
	 * Appends the specified char array to this character sequence.
	 * <p>
	 * If <code>ca</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param ca
	 *            the char array to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 * @throws IndexOutOfBoundsException
	 *             if start < 0, end < start or end > length
	 */
	public final NgStringBuilder append(final char[] ca, final int start, final int end)
	{
		if(ca == null)
			return appendInternal(CA_NULL, 0, 4);

		checkBounds(ca.length, start, end);
		return appendInternal(ca, start, end - start);
	}

	/**
	 * Appends the specified CharSequence to this character sequence.
	 * <p>
	 * If <code>csq</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param csq
	 *            the CharSequence to be appended.
	 * @return a reference to this object.
	 */
	@Override
	public final NgStringBuilder append(final CharSequence csq)
	{
		if(csq == null)
		{
			return appendInternal(CA_NULL, 0, 4);
		}
		else if(csq instanceof NgStringBuilder)
		{
			return appendInternal((NgStringBuilder) csq, 0, csq.length());
		}
		else if(csq instanceof String)
		{
			return appendInternal((String) csq, 0, csq.length());
		}
		else if(csq instanceof StringBuilder)
		{
			return appendInternal((StringBuilder) csq, 0, csq.length());
		}
		else if(csq instanceof StringBuffer)
		{
			return appendInternal((StringBuffer) csq, 0, csq.length());
		}
		else
		{
			return appendInternal(csq, 0, csq.length());
		}
	}

	/**
	 * Appends the specified CharSequence to this character sequence.
	 * <p>
	 * If <code>csq</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param csq
	 *            the CharSequence to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 * @throws IndexOutOfBoundsException
	 *             if start < 0, end < start or end > length
	 */
	@Override
	public final NgStringBuilder append(final CharSequence csq, final int start, final int end)
	{
		if(csq == null)
		{
			return appendInternal(CA_NULL, 0, 4);
		}
		else
		{
			checkBounds(csq.length(), start, end);

			if(csq instanceof NgStringBuilder)
			{
				return appendInternal((NgStringBuilder) csq, start, end);
			}
			else if(csq instanceof StringBuilder)
			{
				return appendInternal((StringBuilder) csq, start, end);
			}
			else if(csq instanceof StringBuffer)
			{
				return appendInternal((StringBuffer) csq, start, end);
			}
			else
			{
				return appendInternal(csq.toString(), start, end);
			}
		}
	}

	/**
	 * Appends the specified double to this character sequence.
	 * 
	 * @param d
	 *            the double to be appended.
	 * @return a reference to this object.
	 * @see java.lang.Double#toString(double)
	 */
	public final NgStringBuilder append(final double d)
	{
		final String s = Double.toString(d);
		appendInternal(s, 0, s.length());
		return this;
	}

	/**
	 * Appends the specified float to this character sequence.
	 * 
	 * @param f
	 *            the float to be appended.
	 * @return a reference to this object.
	 * @see java.lang.Float#toString(float)
	 */
	public final NgStringBuilder append(final float f)
	{
		final String s = Float.toString(f);
		appendInternal(s, 0, s.length());
		return this;
	}

	/**
	 * Appends the specified IncStringBuilder to this character sequence.
	 * <p>
	 * If <code>isb</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param isb
	 *            the IncStringBuilder to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 */
	public final NgStringBuilder append(final NgStringBuilder isb)
	{
		if(isb == null)
			return appendInternal(CA_NULL, 0, 4);

		return appendInternal(isb, 0, isb._len);
	}

	/**
	 * Appends the specified IncStringBuilder to this character sequence.
	 * <p>
	 * If <code>isb</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param isb
	 *            the IncStringBuilder to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 * @throws IndexOutOfBoundsException
	 *             if start < 0, end < start or end > length
	 */
	public final NgStringBuilder append(final NgStringBuilder isb, final int start, final int end)
	{
		if(isb == null)
			return appendInternal(CA_NULL, 0, 4);

		checkBounds(isb._len, start, end);
		return appendInternal(isb, start, end - start);
	}

	/**
	 * Appends the specified integer to this character sequence.
	 * 
	 * @param i
	 *            the integer to be appended.
	 * @return a reference to this object.
	 * @see java.lang.Integer#toString(int)
	 */
	public final NgStringBuilder append(int i)
	{
		int size;

		if(i < 0)
		{
			if(i == Integer.MIN_VALUE)
				return appendInternal(INT_MIN_VALUE, 0, INT_MIN_VALUE.length);

			if(i >= -128)
			{
				final char[] number = BYTE_NUMBERS[i & 0xFF];
				return appendInternal(number, 0, number.length);
			}

			i = -i;
			size = _len + stringSize(i) + 1;
			ensureCapacity(size);
			_buf[_len] = '-';
		}
		else
		{
			if(i <= 127)
			{
				final char[] number = BYTE_NUMBERS[i];
				return appendInternal(number, 0, number.length);
			}

			size = _len + stringSize(i);
			ensureCapacity(size);
		}

		int r, q;
		_len = size;
		_hash = 0;

		while (i >= 65536)
		{
			q = i / 100;
			r = i - ((q << 6) + (q << 5) + (q << 2));
			i = q;
			_buf[--size] = DIGIT_ONES[r];
			_buf[--size] = DIGIT_TENS[r];
		}

		for(;;)
		{
			q = i * 52429 >>> 19;
			r = i - ((q << 3) + (q << 1));
			_buf[--size] = DIGITS[r];
			if(q == 0)
				break;
			i = q;
		}

		return this;
	}

	/**
	 * Appends the specified long to this character sequence.
	 * 
	 * @param l
	 *            the long to be appended.
	 * @return a reference to this object.
	 * @see java.lang.Long#toString(long)
	 */
	public final NgStringBuilder append(long l)
	{
		int size;

		if(l < 0)
		{
			if(l == Long.MIN_VALUE)
				return appendInternal(LONG_MIN_VALUE, 0, LONG_MIN_VALUE.length);

			if(l >= -128)
			{
				final char[] number = BYTE_NUMBERS[(int) l & 0xFF];
				return appendInternal(number, 0, number.length);
			}

			l = -l;
			size = _len + stringSize(l) + 1;
			ensureCapacity(size);
			_buf[_len] = '-';

		}
		else
		{
			if(l <= 127)
			{
				final char[] number = BYTE_NUMBERS[(int) l];
				return appendInternal(number, 0, number.length);
			}

			size = _len + stringSize(l);
			ensureCapacity(size);
		}

		long q;
		int r;
		_len = size;
		_hash = 0;

		while (l > Integer.MAX_VALUE)
		{
			q = l / 100;
			r = (int) (l - ((q << 6) + (q << 5) + (q << 2)));
			l = q;
			_buf[--size] = DIGIT_ONES[r];
			_buf[--size] = DIGIT_TENS[r];
		}

		int q2, i2 = (int) l;

		while (i2 >= 65536)
		{
			q2 = i2 / 100;
			r = i2 - ((q2 << 6) + (q2 << 5) + (q2 << 2));
			i2 = q2;
			_buf[--size] = DIGIT_ONES[r];
			_buf[--size] = DIGIT_TENS[r];
		}

		for(;;)
		{
			q2 = i2 * 52429 >>> 16 + 3;
			r = i2 - ((q2 << 3) + (q2 << 1));
			_buf[--size] = DIGITS[r];
			if(q2 == 0)
				break;
			i2 = q2;
		}

		return this;
	}

	public final NgStringBuilder append(final Object obj)
	{
		if(obj == null)
			return appendInternal(CA_NULL, 0, 4);

		final String s = obj.toString();
		return appendInternal(s, 0, s.length());
	}

	/**
	 * Appends the specified String to this character sequence.
	 * <p>
	 * If <code>str</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param str
	 *            the String to be appended.
	 * @return a reference to this object.
	 */
	public final NgStringBuilder append(final String str)
	{
		if(str == null)
			return appendInternal(CA_NULL, 0, 4);

		return appendInternal(str, 0, str.length());
	}

	/**
	 * Appends the specified String to this character sequence.
	 * <p>
	 * If <code>str</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param str
	 *            the String to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 * @throws IndexOutOfBoundsException
	 *             if start < 0, end < start or end > length
	 */
	public final NgStringBuilder append(final String str, final int start, final int end)
	{
		if(str == null)
			return appendInternal(CA_NULL, 0, 4);

		checkBounds(str.length(), start, end);
		return appendInternal(str, start, end);
	}

	public final NgStringBuilder append(final StringBuffer sb)
	{
		if(sb == null)
			return appendInternal(CA_NULL, 0, 4);

		return appendInternal(sb, 0, sb.length());
	}

	public final NgStringBuilder append(final StringBuffer sb, final int start, final int end)
	{
		if(sb == null)
			return appendInternal(CA_NULL, 0, 4);

		checkBounds(sb.length(), start, end);
		return appendInternal(sb, start, end);
	}

	/**
	 * Appends the specified StringBuilder to this character sequence.
	 * <p>
	 * If <code>sb</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param sb
	 *            the StringBuilder to be appended.
	 * @return a reference to this object.
	 */
	public final NgStringBuilder append(final StringBuilder sb)
	{
		if(sb == null)
			return appendInternal(CA_NULL, 0, 4);

		return appendInternal(sb, 0, sb.length());
	}

	/**
	 * Appends the specified StringBuilder to this character sequence.
	 * <p>
	 * If <code>sb</code> is <code>null</code>, then the four characters <code>"null"</code> are appended.
	 * 
	 * @param sb
	 *            the StringBuilder to be appended.
	 * @param start
	 *            the index of the first char to be appended.
	 * @param end
	 *            the index of the last char to be appended.
	 * @return a reference to this object.
	 * @throws IndexOutOfBoundsException
	 *             if start < 0, end < start or end > length
	 */
	public final NgStringBuilder append(final StringBuilder sb, final int start, final int end)
	{
		if(sb == null)
			return appendInternal(CA_NULL, 0, 4);

		checkBounds(sb.length(), start, end);
		return appendInternal(sb, start, end);
	}

	private final NgStringBuilder appendInternal(final char[] ca, final int offset, final int len)
	{
		final int newLen = _len + len;
		ensureCapacity(newLen);
		System.arraycopy(ca, offset, _buf, _len, len);
		_len = newLen;
		_hash = 0;
		return this;
	}

	private final NgStringBuilder appendInternal(final CharSequence cs, final int start, final int end)
	{
		ensureCapacity(_len + end - start);
		for(int i = start; i < end; i++)
		{
			_buf[_len++] = cs.charAt(i);
		}
		_hash = 0;
		return this;
	}

	private final NgStringBuilder appendInternal(final NgStringBuilder is, final int off, final int len)
	{
		final int newLen = _len + len;
		ensureCapacity(newLen);
		System.arraycopy(is._buf, off, _buf, _len, len);
		_len = newLen;
		_hash = 0;
		return this;
	}

	private final NgStringBuilder appendInternal(final String s, final int start, final int end)
	{
		final int newLen = _len + end - start;
		ensureCapacity(newLen);
		s.getChars(start, end, _buf, _len);
		_len = newLen;
		_hash = 0;
		return this;
	}

	private final NgStringBuilder appendInternal(final StringBuffer sb, final int start, final int end)
	{
		final int newLen = _len + end - start;
		ensureCapacity(newLen);
		sb.getChars(start, end, _buf, _len);
		_len = newLen;
		_hash = 0;
		return this;
	}

	private final NgStringBuilder appendInternal(final StringBuilder sb, final int start, final int end)
	{
		final int newLen = _len + end - start;
		ensureCapacity(newLen);
		sb.getChars(start, end, _buf, _len);
		_len = newLen;
		_hash = 0;
		return this;
	}

	public final NgStringBuilder appendUnsafe(final char c)
	{
		_buf[_len++] = c;
		_hash = 0;
		return this;
	}

	public final NgStringBuilder appendUnsafe(final char[] ca, final int offset, final int len)
	{
		System.arraycopy(ca, offset, _buf, _len, len);
		_len += len;
		_hash = 0;
		return this;
	}

	public final NgStringBuilder appendUnsafe(final String str)
	{
		final int len = str.length();
		str.getChars(0, len, _buf, _len);
		_len += len;
		_hash = 0;
		return this;
	}

	public final NgStringBuilder appendUnsafe(final String str, final int start, final int end)
	{
		final int len = end - start;
		str.getChars(start, end, _buf, _len);
		_len += len;
		_hash = 0;
		return this;
	}

	public final IncStringWriter asIncStringWriter()
	{
		return new IncStringWriter(this);
	}

	/**
	 * Get the current capacity of the character sequence char array.
	 * 
	 * @return the capacity of the character sequence char array
	 */
	public final int capacity()
	{
		return _buf.length;
	}

	@Override
	public final char charAt(final int index)
	{
		if(index < 0)
			throw new IndexOutOfBoundsException("index < 0");

		if(index >= _len)
			throw new IndexOutOfBoundsException("index >= len");

		return charAtUnsafe(index);
	}

	public final char charAtUnsafe(final int index)
	{
		return _buf[index];
	}

	private final void checkBounds(final int len, final int start, final int end)
	{
		if(start < 0)
			throw new IndexOutOfBoundsException("start < 0");

		if(end < start)
			throw new IndexOutOfBoundsException("end < start");

		if(end > len)
			throw new IndexOutOfBoundsException("end > len");
	}

	/**
	 * Clears the character sequence content.
	 */
	public final void clear()
	{
		_len = 0;
		_hash = 0;
	}

	@Override
	public final NgStringBuilder clone()
	{
		return new NgStringBuilder(this);
	}

	@Override
	public final int compareTo(final NgStringBuilder isb)
	{
		final int n = Math.min(_len, isb._len);
		for(int i = 0; i < n; i++)
		{
			if(_buf[i] != isb._buf[i])
				return _buf[i] - isb._buf[i];
		}
		return _len - isb._len;
	}

	/**
	 * @see #compareTo(NgStringBuilder)
	 */
	public final int compareToIgnoreCase(final NgStringBuilder isb)
	{
		final int n = Math.min(_len, isb._len);
		for(int i = 0; i < n; i++)
		{
			if(_buf[i] != isb._buf[i] && Character.toLowerCase(_buf[i]) != Character.toLowerCase(isb._buf[i]) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(isb._buf[i]))
				return _buf[i] - isb._buf[i];
		}
		return _len - isb._len;
	}

	public final boolean contains(final char reg)
	{
		for(int i = _len - 1; i >= 0; i--)
		{
			if(_buf[i] == reg)
				return true;
		}

		return false;
	}

	public final boolean contains(final char[] reg)
	{
		if(reg == null)
			return false;

		final int len = reg.length;
		switch (len)
		{
			case 0:
				return false;

			case 1:
				return contains(reg[0]);

			default:
				return containsInternal(reg, len);
		}
	}

	public final boolean contains(final String reg)
	{
		if(reg == null)
			return false;

		final int len = reg.length();
		switch (len)
		{
			case 0:
				return false;

			case 1:
				return contains(reg.charAt(0));

			default:
				return containsInternal(reg.toCharArray(), len);
		}
	}

	private final boolean containsInternal(final char[] reg, final int regLen)
	{
		for(int i = 0, k = _len - regLen + 1; i < k; i++)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int m = 1; m < regLen; m++)
				{
					if(_buf[i + m] != reg[m])
					{
						found = false;
						break;
					}
				}

				if(found)
					return true;
			}
		}

		return false;
	}

	public final boolean contentEquals(final String s)
	{
		if(s == null)
			return false;

		return contentEqualsInternal(s);
	}

	public final boolean contentEqualsIgnoreCase(final String s)
	{
		if(s == null)
			return false;

		if(_len != s.length())
			return false;

		for(int i = 0; i < _len; i++)
		{
			final char c = s.charAt(i);
			if(_buf[i] != c && Character.toLowerCase(_buf[i]) != Character.toLowerCase(c) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(c))
				return false;
		}

		return true;
	}

	private final boolean contentEqualsInternal(final String s)
	{
		if(_len != s.length())
			return false;

		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] != s.charAt(i))
				return false;
		}

		return true;
	}

	private final int countAllInternal(final char[] reg, final int regLen)
	{
		int val = 0;
		for(int i = 0, n = _len - regLen + 1; i < n;)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int k = 1; k < regLen; k++)
				{
					if(_buf[i + k] != reg[k])
					{
						found = false;
						break;
					}
				}

				if(found)
				{
					val++;
					i += regLen;
				}
				else
				{
					i++;
				}
			}
			else
			{
				i++;
			}
		}

		return val;
	}

	public final void ensureCapacity(final int spaceNeeded)
	{
		if(_buf.length < spaceNeeded)
		{
			final char[] buf = new char[newCapacity(spaceNeeded)];
			System.arraycopy(_buf, 0, buf, 0, _len);
			_buf = buf;
		}
	}

	public final boolean equals(final NgStringBuilder s)
	{
		if(s == null)
			return false;

		if(s == this)
			return true;

		return equalsInternal(s);
	}

	@Override
	public final boolean equals(final Object o)
	{
		if(o == this)
			return true;

		if(o instanceof NgStringBuilder)
			return equalsInternal((NgStringBuilder) o);

		// if (o instanceof String)
		// return contentEqualsInternal((String)o);

		return false;
	}

	public final boolean equalsIgnoreCase(final NgStringBuilder s)
	{
		if(s == this)
			return true;

		if(_len != s._len)
			return false;

		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] != s._buf[i] && Character.toLowerCase(_buf[i]) != Character.toLowerCase(s._buf[i]) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(s._buf[i]))
				return false;
		}

		return true;
	}

	private final boolean equalsInternal(final NgStringBuilder s)
	{
		if(_len != s._len)
			return false;

		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] != s._buf[i])
				return false;
		}

		return true;
	}

	public final int firstIndexOf(final char c)
	{
		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] == c)
				return i;
		}

		return -1;
	}

	public final int firstIndexOf(final char c, final int fromIndex)
	{
		if(fromIndex < 0)
			throw new IndexOutOfBoundsException("fromIndex < 0");

		if(fromIndex >= _len)
			throw new IndexOutOfBoundsException("fromIndex >= _len");

		return firstIndexOfUnsafe(c, fromIndex);
	}

	public final int firstIndexOfUnsafe(final char c, final int fromIndex)
	{
		for(int i = fromIndex; i < _len; i++)
		{
			if(_buf[i] == c)
				return i;
		}

		return -1;
	}

	public final byte[] getBytesUTF16BE()
	{
		final byte[] dst = new byte[_len * 2];
		getBytesUTF16BE(dst, 0);
		return dst;
	}

	public final void getBytesUTF16BE(final byte[] dst)
	{
		getBytesUTF16BE(dst, 0);
	}

	public final void getBytesUTF16BE(final byte[] dst, final int off)
	{
		for(int i = 0, j = off; i < _len; i++)
		{
			dst[j++] = (byte) (_buf[i] >> 8);
			dst[j++] = (byte) _buf[i];
		}
	}

	public final byte[] getBytesUTF16LE()
	{
		final byte[] dst = new byte[_len * 2];
		getBytesUTF16LE(dst, 0);
		return dst;
	}

	public final void getBytesUTF16LE(final byte[] dst)
	{
		getBytesUTF16LE(dst, 0);
	}

	public final void getBytesUTF16LE(final byte[] dst, final int off)
	{
		for(int i = 0, j = off; i < _len; i++)
		{
			dst[j++] = (byte) _buf[i];
			dst[j++] = (byte) (_buf[i] >> 8);
		}
	}

	public final void getBytesUTF8(final byte[] dst)
	{
		for(int i = 0; i < _len; i++)
		{
			dst[i] = (byte) _buf[i];
		}
	}

	public final void getBytesUTF8(final byte[] dst, final int off)
	{
		for(int i = 0, j = off; i < _len; i++, j++)
		{
			dst[j] = (byte) _buf[i];
		}
	}

	public final void getChars(final int srcBegin, final int srcEnd, final char[] dst, final int dstBegin)
	{
		if(srcBegin < 0)
			throw new IndexOutOfBoundsException("srcBegin < 0");

		if(srcBegin > srcEnd)
			throw new IndexOutOfBoundsException("srcBegin > srcEnd");

		if(srcEnd > _len)
			throw new IndexOutOfBoundsException("srcEnd > len");

		System.arraycopy(_buf, srcBegin, dst, dstBegin, srcEnd - srcBegin);
	}

	@Override
	public final int hashCode()
	{
		int h = _hash;
		if(h == 0)
		{
			final char[] buf = _buf;
			final int len = _len;

			for(int i = 0; i < len; i++)
			{
				h = 31 * h + buf[i];
			}

			_hash = h;
		}
		return h;
	}

	public final boolean isEmpty()
	{
		return _len == 0;
	}

	public final int lastIndexOf(final char c)
	{
		for(int i = _len; i-- > 0;)
		{
			if(_buf[i] == c)
				return i;
		}

		return -1;
	}

	public final int lastIndexOf(final char c, final int fromIndex)
	{
		if(fromIndex <= 0)
			throw new IndexOutOfBoundsException("fromIndex <= 0");

		if(fromIndex > _len)
			throw new IndexOutOfBoundsException("fromIndex > _len");

		for(int i = fromIndex; i-- > 0;)
		{
			if(_buf[i] == c)
				return i;
		}

		return -1;
	}

	@Override
	public final int length()
	{
		return _len;
	}

	private final int newCapacity(final int minimumCapacity)
	{
		final int newCapacity = _buf.length << 1;
		return newCapacity < 0 ? Integer.MAX_VALUE : newCapacity < minimumCapacity ? minimumCapacity : newCapacity;
	}

	public final NgStringBuilder println()
	{
		System.out.print(Arrays.copyOf(_buf, _len));
		System.out.println(new NgStringBuilder(" - DEBUG LEN: " + _len + ", CAP: " + _buf.length).toString());
		return this;
	}

	public final NgStringBuilder replaceAll(final char reg, final char rep)
	{
		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] == reg)
				_buf[i] = rep;
		}
		_hash = 0;
		return this;
	}

	public final NgStringBuilder replaceAll(final char reg, final NgStringBuilder rep)
	{
		if(rep == null)
			return this;

		final int repLen = rep._len;
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
				return replaceAll(reg, rep._buf[0]);

			default:
				return replaceAllInternal(reg, rep._buf, repLen);
		}
	}

	public final NgStringBuilder replaceAll(final char reg, final String rep)
	{
		if(rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
				return replaceAll(reg, rep.charAt(0));

			default:
				return replaceAllInternal(reg, rep.toCharArray(), repLen);
		}
	}

	public final NgStringBuilder replaceAll(final NgStringBuilder reg, final char rep)
	{
		if(reg == null)
			return this;

		final int regLen = reg.length();
		switch (regLen)
		{
			case 0:
				return this;

			case 1:
				return replaceAll(reg._buf[0], rep);

			default:
				return replaceAllInternal(reg._buf, regLen, rep);
		}
	}

	public final NgStringBuilder replaceAll(final NgStringBuilder reg, final NgStringBuilder rep)
	{
		if(reg == null || rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
			{
				final int regLen = reg.length();
				switch (regLen)
				{
					case 0:
						return this;

					case 1:
						return replaceAll(reg._buf[0], rep._buf[0]);

					default:
						return replaceAllInternal(reg._buf, regLen, rep._buf[0]);
				}
			}

			default:
				return replaceAllInternal(reg._buf, reg._len, rep._buf, repLen);
		}
	}

	public final NgStringBuilder replaceAll(final String reg, final char rep)
	{
		if(reg == null)
			return this;

		final int regLen = reg.length();
		switch (regLen)
		{
			case 0:
				return this;

			case 1:
				return replaceAll(reg.charAt(0), rep);

			default:
				return replaceAllInternal(reg.toCharArray(), regLen, rep);
		}
	}

	public final NgStringBuilder replaceAll(final String reg, final String rep)
	{
		if(reg == null || rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
			{
				switch (reg.length())
				{
					case 0:
						return this;

					case 1:
						return replaceAll(reg.charAt(0), rep.charAt(0));

					default:
						return replaceAllInternal(reg.toCharArray(), reg.length(), rep.charAt(0));
				}
			}

			default:
				return replaceAllInternal(reg.toCharArray(), reg.length(), rep.toCharArray(), repLen);
		}
	}

	private final NgStringBuilder replaceAllInternal(final char reg, final char[] rep, final int repLen)
	{
		int val = 0;
		for(int i = _len - 1; i >= 0; i--)
		{
			if(_buf[i] == reg)
				val++;
		}

		if(val == 0)
			return this;

		val = _len + (repLen - 1) * val;
		final char[] buf = _buf.length < val ? new char[newCapacity(val)] : new char[_buf.length];

		int j = 0, k = 0;
		for(int i = 0, l = 0; i < _len; i++)
		{
			if(_buf[i] == reg)
			{
				l = i - j;
				System.arraycopy(_buf, j, buf, k, l);
				k += l;
				System.arraycopy(rep, 0, buf, k, repLen);
				j = i + 1;
				k += repLen;
			}
		}

		if(j < _len)
			System.arraycopy(_buf, j, buf, k, _len - j);

		_buf = buf;
		_len = val;
		_hash = 0;

		return this;
	}

	private final NgStringBuilder replaceAllInternal(final char[] reg, final int regLen, final char rep)
	{
		int val = countAllInternal(reg, regLen);

		if(val == 0)
			return this;

		val = _len + (1 - regLen) * val;
		final char[] buf = _buf.length < val ? new char[newCapacity(val)] : new char[_buf.length];

		int j = 0, k = 0;
		for(int i = 0, l = 0, n = _len - regLen + 1; i < n;)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int m = 1; m < regLen; m++)
				{
					if(_buf[i + m] != reg[m])
					{
						found = false;
						break;
					}
				}

				if(found)
				{
					l = i - j;
					System.arraycopy(_buf, j, buf, k, l);
					buf[k += l + 1] = rep;
					j = i += regLen;
				}
				else
				{
					i++;
				}
			}
			else
			{
				i++;
			}
		}

		if(j < _len)
			System.arraycopy(_buf, j, buf, k, _len - j);

		_buf = buf;
		_len = val;
		_hash = 0;

		return this;
	}

	private final NgStringBuilder replaceAllInternal(final char[] reg, final int regLen, final char[] rep, final int repLen)
	{
		int val = countAllInternal(reg, regLen);

		if(val == 0)
			return this;

		val = _len + (repLen - regLen) * val;
		final char[] buf = _buf.length < val ? new char[newCapacity(val)] : new char[_buf.length];

		int j = 0, k = 0;
		for(int i = 0, l = 0, n = _len - regLen + 1; i < n;)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int m = 1; m < regLen; m++)
				{
					if(_buf[i + m] != reg[m])
					{
						found = false;
						break;
					}
				}

				if(found)
				{
					l = i - j;
					System.arraycopy(_buf, j, buf, k, l);
					k += l;
					System.arraycopy(rep, 0, buf, k, repLen);
					j = i += regLen;
					k += repLen;
				}
				else
				{
					i++;
				}
			}
			else
			{
				i++;
			}
		}

		if(j < _len)
			System.arraycopy(_buf, j, buf, k, _len - j);

		_buf = buf;
		_len = val;
		_hash = 0;

		return this;
	}

	public final NgStringBuilder replaceFirst(final char reg, final char rep)
	{
		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] == reg)
			{
				_buf[i] = rep;
				break;
			}
		}
		_hash = 0;
		return this;
	}

	public final NgStringBuilder replaceFirst(final char reg, final NgStringBuilder rep)
	{
		if(rep == null)
			return this;

		final int repLen = rep._len;
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
				return replaceFirst(reg, rep._buf[0]);

			default:
				return replaceFirstInternal(reg, rep._buf, repLen);
		}
	}

	public final NgStringBuilder replaceFirst(final char reg, final String rep)
	{
		if(rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
				return replaceFirst(reg, rep.charAt(0));

			default:
				return replaceFirstInternal(reg, rep.toCharArray(), repLen);
		}
	}

	public final NgStringBuilder replaceFirst(final NgStringBuilder reg, final char rep)
	{
		if(reg == null)
			return this;

		final int regLen = reg.length();
		switch (regLen)
		{
			case 0:
				return this;

			case 1:
				return replaceFirst(reg._buf[0], rep);

			default:
				return replaceFirstInternal(reg._buf, regLen, rep);
		}
	}

	public final NgStringBuilder replaceFirst(final NgStringBuilder reg, final NgStringBuilder rep)
	{
		if(reg == null || rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
			{
				final int regLen = reg.length();
				switch (regLen)
				{
					case 0:
						return this;

					case 1:
						return replaceFirst(reg._buf[0], rep._buf[0]);

					default:
						return replaceFirstInternal(reg._buf, regLen, rep._buf[0]);
				}
			}

			default:
				return replaceFirstInternal(reg._buf, reg._len, rep._buf, repLen);
		}
	}

	public final NgStringBuilder replaceFirst(final String reg, final char rep)
	{
		if(reg == null)
			return this;

		final int regLen = reg.length();
		switch (regLen)
		{
			case 0:
				return this;

			case 1:
				return replaceFirst(reg.charAt(0), rep);

			default:
				return replaceFirstInternal(reg.toCharArray(), regLen, rep);
		}
	}

	public final NgStringBuilder replaceFirst(final String reg, final String rep)
	{
		if(reg == null || rep == null)
			return this;

		final int repLen = rep.length();
		switch (repLen)
		{
			case 0:
				return this;

			case 1:
			{
				switch (reg.length())
				{
					case 0:
						return this;

					case 1:
						return replaceFirst(reg.charAt(0), rep.charAt(0));

					default:
						return replaceFirstInternal(reg.toCharArray(), reg.length(), rep.charAt(0));
				}
			}

			default:
				return replaceFirstInternal(reg.toCharArray(), reg.length(), rep.toCharArray(), repLen);
		}
	}

	private final NgStringBuilder replaceFirstInternal(final char reg, final char[] rep, final int repLen)
	{
		for(int i = 0; i < _len; i++)
		{
			if(_buf[i] == reg)
			{
				final int newLen = _len + repLen - 1;

				if(_buf.length < newLen)
				{
					final char[] buf = new char[newCapacity(newLen)];
					System.arraycopy(_buf, 0, buf, 0, i);
					System.arraycopy(_buf, i + 1, buf, i + repLen, _len - 1 - i);
					System.arraycopy(rep, 0, buf, i, repLen);
					_buf = buf;
				}
				else
				{
					System.arraycopy(_buf, i + 1, _buf, i + repLen, _len - 1 - i);
					System.arraycopy(rep, 0, _buf, i, repLen);
				}

				_len = newLen;
				_hash = 0;
				break;
			}
		}
		return this;
	}

	private final NgStringBuilder replaceFirstInternal(final char[] reg, final int regLen, final char rep)
	{
		final int len = _len - regLen + 1;
		for(int i = 0; i < len; i++)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int j = 1; j < regLen; j++)
				{
					if(_buf[i + j] != reg[j])
					{
						found = false;
						break;
					}
				}

				if(found)
				{
					System.arraycopy(_buf, i + regLen, _buf, i + 1, _len - regLen - i);
					_buf[i] = rep;
					_len = len;
					_hash = 0;
					break;
				}
			}
		}

		return this;
	}

	private final NgStringBuilder replaceFirstInternal(final char[] reg, final int regLen, final char[] rep, final int repLen)
	{
		for(int i = 0, n = _len - regLen + 1; i < n; i++)
		{
			if(_buf[i] == reg[0])
			{
				boolean found = true;
				for(int j = 1; j < regLen; j++)
				{
					if(_buf[i + j] != reg[j])
					{
						found = false;
						break;
					}
				}

				if(found)
				{
					final int newLen = _len - regLen + repLen;

					if(_buf.length < newLen)
					{
						final char[] buf = new char[newCapacity(newLen)];
						System.arraycopy(_buf, 0, buf, 0, i);
						System.arraycopy(_buf, i + regLen, buf, i + repLen, _len - regLen - i);
						System.arraycopy(rep, 0, buf, i, repLen);
						_buf = buf;
					}
					else
					{
						System.arraycopy(_buf, i + regLen, _buf, i + repLen, _len - regLen - i);
						System.arraycopy(rep, 0, _buf, i, repLen);
					}

					_len = newLen;
					_hash = 0;
					break;
				}
			}
		}

		return this;
	}

	public final NgStringBuilder setCharAt(final int index, final char rep)
	{
		if(index < 0)
			throw new IndexOutOfBoundsException("index < 0");

		if(index >= _len)
			throw new IndexOutOfBoundsException("index >= len");

		_buf[index] = rep;

		return this;
	}

	public final boolean startsWith(final char[] ca)
	{
		if(ca == null)
			return false;

		final int len = ca.length;
		if(_len < len)
			return false;

		for(int i = 0; i < len; i++)
		{
			if(_buf[i] != ca[i])
				return false;
		}

		return true;
	}

	public final boolean startsWith(final NgStringBuilder s)
	{
		if(s == null)
			return false;

		final int len = s.length();
		if(_len < len)
			return false;

		for(int i = 0; i < s._len; i++)
		{
			if(_buf[i] != s._buf[i])
				return false;
		}

		return true;
	}

	public final boolean startsWith(final String s)
	{
		if(s == null)
			return false;

		final int len = s.length();
		if(_len < len)
			return false;

		for(int i = 0; i < len; i++)
		{
			if(_buf[i] != s.charAt(i))
				return false;
		}

		return true;
	}

	public final boolean startsWithIgnoreCase(final char[] ca)
	{
		if(ca == null)
			return false;

		final int len = ca.length;
		if(_len < len)
			return false;

		for(int i = 0; i < ca.length; i++)
		{
			if(_buf[i] != ca[i] && Character.toLowerCase(_buf[i]) != Character.toLowerCase(ca[i]) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(ca[i]))
				return false;
		}

		return false;
	}

	public final boolean startsWithIgnoreCase(final NgStringBuilder s)
	{
		final int len = s.length();
		if(_len < len)
			return false;

		for(int i = 0; i < s._len; i++)
		{
			if(_buf[i] != s._buf[i] && Character.toLowerCase(_buf[i]) != Character.toLowerCase(s._buf[i]) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(s._buf[i]))
				return false;
		}

		return false;
	}

	public final boolean startsWithIgnoreCase(final String s)
	{
		if(s == null)
			return false;

		final int len = s.length();
		if(_len < len)
			return false;

		for(int i = 0; i < len; i++)
		{
			final char c = s.charAt(i);
			if(_buf[i] != c && Character.toLowerCase(_buf[i]) != Character.toLowerCase(c) && Character.toUpperCase(_buf[i]) != Character.toUpperCase(c))
				return false;
		}

		return true;
	}

	public final NgStringBuilder subSequence(final int start)
	{
		if(start < 0)
			throw new IndexOutOfBoundsException("start < 0");

		if(start >= _len)
			throw new IndexOutOfBoundsException("start >= len");

		final int len = _len - start;
		final NgStringBuilder isb = new NgStringBuilder(len);
		System.arraycopy(_buf, start, isb._buf, 0, len);
		isb._len = len;
		return isb;
	}

	@Override
	public final NgStringBuilder subSequence(final int start, final int end)
	{
		checkBounds(_len, start, end);
		final int len = end - start;
		final NgStringBuilder s = new NgStringBuilder(len);
		System.arraycopy(_buf, start, s._buf, 0, len);
		s._len = len;
		return s;
	}

	public final String subString(final int start)
	{
		if(start < 0)
			throw new IndexOutOfBoundsException("start < 0");

		if(start >= _len)
			throw new IndexOutOfBoundsException("start >= len");

		return subStringUnsafe(start);
	}

	public final String subString(final int start, final int end)
	{
		checkBounds(_len, start, end);
		return subStringUnsafe(start, end);
	}

	public final String subStringUnsafe(final int start)
	{
		return new String(_buf, start, _len - start);
	}

	public final String subStringUnsafe(final int start, final int end)
	{
		return new String(_buf, start, end - start);
	}

	public final NgStringBuilder toLowerCase()
	{
		for(int i = _len - 1; i >= 0; i--)
		{
			_buf[i] = Character.toLowerCase(_buf[i]);
		}
		_hash = 0;
		return this;
	}

	@Override
	public final String toString()
	{
		if(_len < _buf.length)
		{
			return new String(_buf, 0, _len);
		}
		else
		{
			return new String(_buf);
		}
	}

	public final NgStringBuilder toUpperCase()
	{
		for(int i = _len - 1; i >= 0; i--)
		{
			_buf[i] = Character.toUpperCase(_buf[i]);
		}
		_hash = 0;
		return this;
	}

	public final void trimToSize()
	{
		if(_len < _buf.length)
		{
			final char[] buf = new char[_len];
			System.arraycopy(_buf, 0, buf, 0, _len);
			_buf = buf;
		}
	}
}
