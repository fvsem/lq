package l2n.commons.crypt.algorithm;

import jonelo.jacksum.JacksumAPI;
import jonelo.jacksum.algorithm.AbstractChecksum;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SHA1 implements ICrypt
{
	private final static Logger _log = Logger.getLogger(SHA1.class.getName());

	public static SHA1 getInstance()
	{
		return SingletonHolder._instance;
	}

	@Override
	public boolean compare(String password, String expected)
	{
		try
		{
			return encrypt(password).equals(expected);
		}
		catch(NoSuchAlgorithmException nsee)
		{
			_log.log(Level.WARNING, "Could not check password, algorithm SHA1 not found! Check jacksum library!", nsee);
			return false;
		}
		catch(UnsupportedEncodingException uee)
		{
			_log.log(Level.WARNING, "Could not check password, UTF-8 is not supported!", uee);
			return false;
		}
	}

	@Override
	public String encrypt(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		AbstractChecksum sha1 = JacksumAPI.getChecksumInstance("sha1");
		sha1.setEncoding("BASE64");
		sha1.update(password.getBytes("UTF-8"));
		return sha1.format("#CHECKSUM");
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final SHA1 _instance = new SHA1();
	}
}
