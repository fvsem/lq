package l2n.commons.crypt.algorithm;

import jonelo.jacksum.JacksumAPI;
import jonelo.jacksum.algorithm.AbstractChecksum;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Whirlpool implements ICrypt
{
	private final static Logger _log = Logger.getLogger(Whirlpool.class.getName());

	public static Whirlpool getInstance()
	{
		return SingletonHolder._instance;
	}

	@Override
	public boolean compare(String password, String expected)
	{
		try
		{
			return encrypt(password).equals(expected);
		}
		catch(NoSuchAlgorithmException nsee)
		{
			_log.log(Level.WARNING, "Could not check password, algorithm Whirlpool not found! Check jacksum library!", nsee);
			return false;
		}
		catch(UnsupportedEncodingException uee)
		{
			_log.log(Level.WARNING, "Could not check password, UTF-8 is not supported!", uee);
			return false;
		}
	}

	@Override
	public String encrypt(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		AbstractChecksum whirlpool2 = JacksumAPI.getChecksumInstance("whirlpool2");
		whirlpool2.setEncoding("BASE64");
		whirlpool2.update(password.getBytes("UTF-8"));
		return whirlpool2.format("#CHECKSUM");
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final Whirlpool _instance = new Whirlpool();
	}
}
