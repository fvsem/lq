package l2n.commons.crypt.algorithm;

public interface ICrypt
{

	public boolean compare(String password, String hash);


	public String encrypt(String password) throws Exception;
}
