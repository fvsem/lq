package l2n.commons.crypt;

import l2n.commons.crypt.algorithm.DoubleWhirlpoolWithSalt;
import l2n.commons.crypt.algorithm.ICrypt;
import l2n.commons.crypt.algorithm.SHA1;
import l2n.commons.crypt.algorithm.Whirlpool;

import java.lang.reflect.Constructor;

public enum AlgorithmType
{
	DES(SHA1.class),
	SHA1(SHA1.class),
	Whirlpool(Whirlpool.class),
	DoubleWhirlpoolWithSalt(DoubleWhirlpoolWithSalt.class);

	private final Class<? extends ICrypt> clazz;

	private AlgorithmType(Class<? extends ICrypt> clazz)
	{
		this.clazz = clazz;
	}

	public static AlgorithmType getAlgorithm(String name)
	{
		for(AlgorithmType type : AlgorithmType.values())
			if(type.name().equals(name))
				return type;
		return Whirlpool;
	}

	public ICrypt getInstance()
	{
		try
		{
			Constructor<? extends ICrypt> c = clazz.getConstructor();
			return c.newInstance();
		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}
}
