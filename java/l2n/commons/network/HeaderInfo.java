package l2n.commons.network;

public final class HeaderInfo<T>
{
	private int _headerPending;
	private int _dataPending;
	private boolean _multiPacket;
	private T _client;

	public HeaderInfo()
	{}

	public HeaderInfo<T> set(final int headerPending, final int dataPending, final boolean multiPacket, final T client)
	{
		this.setHeaderPending(headerPending);
		this.setDataPending(dataPending);
		this.setMultiPacket(multiPacket);
		this.setClient(client);
		return this;
	}

	protected boolean headerFinished()
	{
		return getHeaderPending() == 0;
	}

	protected boolean packetFinished()
	{
		return getDataPending() == 0;
	}


	private void setDataPending(final int dataPending)
	{
		_dataPending = dataPending;
	}


	protected int getDataPending()
	{
		return _dataPending;
	}


	private void setHeaderPending(final int headerPending)
	{
		_headerPending = headerPending;
	}


	protected int getHeaderPending()
	{
		return _headerPending;
	}


	protected void setClient(final T client)
	{
		_client = client;
	}

	protected T getClient()
	{
		return _client;
	}


	private void setMultiPacket(final boolean multiPacket)
	{
		_multiPacket = multiPacket;
	}


	public boolean isMultiPacket()
	{
		return _multiPacket;
	}
}
