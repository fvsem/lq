package l2n.commons.network;

@SuppressWarnings("rawtypes")
public abstract class HeaderHandler<T extends MMOClient, H extends HeaderHandler<T, H>>
{
	private final H _subHeaderHandler;

	public HeaderHandler(final H subHeaderHandler)
	{
		_subHeaderHandler = subHeaderHandler;
	}

	public final H getSubHeaderHandler()
	{
		return _subHeaderHandler;
	}

	public final boolean isChildHeaderHandler()
	{
		return this.getSubHeaderHandler() == null;
	}
}
