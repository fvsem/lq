package l2n.commons.network;

import l2n.commons.threading.FIFORunnableQueue;

import java.nio.ByteBuffer;

@SuppressWarnings("unchecked")
public abstract class MMOClient<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	private MMOConnection<T, RP, SP> _connection;

	public boolean can_runImpl = true;

	public MMOClient(final MMOConnection<T, RP, SP> con)
	{
		_connection = con;
		con.setClient((T) this);
	}

	public void setConnection(final MMOConnection<T, RP, SP> con)
	{
		_connection = con;
	}

	public MMOConnection<T, RP, SP> getConnection()
	{
		return _connection;
	}

	public void closeNow(final boolean error)
	{
		if(_connection != null)
			_connection.closeNow(error);
	}

	public boolean isConnected()
	{
		return _connection == null ? false : !_connection.isClosed();
	}

	public boolean isGameClient()
	{
		return false;
	}

	public boolean stateIsConnected()
	{
		return false;
	}

	protected abstract FIFORunnableQueue<T, RP, SP> getPacketQueue();

	protected abstract void executePacket(final RP packet);

	public abstract boolean decrypt(ByteBuffer buf, int size);

	public abstract boolean encrypt(ByteBuffer buf, int size);

	protected void onDisconnection()
	{}

	protected void onForcedDisconnection()
	{}
}
