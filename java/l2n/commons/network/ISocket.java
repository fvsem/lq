package l2n.commons.network;

import java.net.InetAddress;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public interface ISocket
{
	public void close();

	public WritableByteChannel getWritableByteChannel();

	public ReadableByteChannel getReadableByteChannel();

	public InetAddress getInetAddress();

	public InetAddress getLocalAddress();
}
