package l2n.commons.network;

import java.nio.ByteBuffer;

public abstract class AbstractPacket<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	protected T _client;
	
	public T getClient()
	{
		return _client;
	}

	@SuppressWarnings("unchecked")
	protected SelectorThread<T, RP, SP> getCurrentSelectorThread()
	{
		final Thread result = Thread.currentThread();
		return result instanceof SelectorThread ? (SelectorThread<T, RP, SP>) result : null;
	}

	protected abstract ByteBuffer getByteBuffer();
}
