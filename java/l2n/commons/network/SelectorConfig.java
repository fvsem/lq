package l2n.commons.network;

import java.nio.ByteOrder;

public class SelectorConfig<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	private final TCPHeaderHandler<T> TCP_HEADER_HANDLER;

	private static final ByteOrder BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

	private final int CHAR_BUFFER_SIZE = 1024 * 32;

	private final int READ_BUFFER_SIZE = 64 * 1024;
	private final int WRITE_BUFFER_SIZE = 128 * 1024;
	private int MAX_SEND_PER_PASS = 1;
	private int SLEEP_TIME = 10;
	private final HeaderSize HEADER_TYPE = HeaderSize.SHORT_HEADER;
	private final int HELPER_BUFFER_SIZE = 128 * 1024;
	private final int HELPER_BUFFER_COUNT = 20;


	public static enum HeaderSize
	{
		BYTE_HEADER,
		SHORT_HEADER,
		INT_HEADER,
	}

	public SelectorConfig(final TCPHeaderHandler<T> tcpHeaderHandler)
	{
		TCP_HEADER_HANDLER = tcpHeaderHandler;
	}

	public int getReadBufferSize()
	{
		return READ_BUFFER_SIZE;
	}

	public int getWriteBufferSize()
	{
		return WRITE_BUFFER_SIZE;
	}

	public int getHelperBufferSize()
	{
		return HELPER_BUFFER_SIZE;
	}

	public int getHelperBufferCount()
	{
		return HELPER_BUFFER_COUNT;
	}

	public int getCharBufferSize()
	{
		return CHAR_BUFFER_SIZE;
	}

	public ByteOrder getByteOrder()
	{
		return BYTE_ORDER;
	}

	public HeaderSize getHeaderType()
	{
		return HEADER_TYPE;
	}

	public TCPHeaderHandler<T> getTCPHeaderHandler()
	{
		return TCP_HEADER_HANDLER;
	}

	public void setMaxSendPerPass(final int maxSendPerPass)
	{
		MAX_SEND_PER_PASS = maxSendPerPass;
	}


	public int getMaxSendPerPass()
	{
		return MAX_SEND_PER_PASS;
	}


	public void setSelectorSleepTime(final int sleepTime)
	{
		SLEEP_TIME = sleepTime;
	}

	public int getSelectorSleepTime()
	{
		return SLEEP_TIME;
	}
}
