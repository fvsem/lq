package l2n.commons.network;

import l2n.Config;
import l2n.commons.threading.ThreadPoolFactory;

import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MMOConnection<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	private static ScheduledThreadPoolExecutor interestPool;

	private final SelectorThread<T, RP, SP> _selectorThread;
	private T _client;

	private ISocket _socket;

	private ArrayDeque<SP> _sendQueue;
	private SelectionKey _selectionKey;

	private int _readHeaderPending;

	private ByteBuffer _readBuffer, _primaryWriteBuffer, _secondaryWriteBuffer;

	private boolean _pendingClose;

	private RunnableScheduledFuture<ScheduleInterest> interest;

	static
	{
		interestPool = ThreadPoolFactory.createScheduledThreadPoolExecutor(Config.INTEREST_MAX_THREAD, "InterestManagerPool", Thread.NORM_PRIORITY - 1);
	}

	@SuppressWarnings("unchecked")
	public MMOConnection(final SelectorThread<T, RP, SP> selectorThread, final ISocket socket, final SelectionKey key)
	{
		_selectorThread = selectorThread;
		setSocket(socket);
		setSelectionKey(key);

		interest = (RunnableScheduledFuture<ScheduleInterest>) interestPool.scheduleWithFixedDelay(new ScheduleInterest(), 10, 50, TimeUnit.MILLISECONDS);
	}

	public MMOConnection(final SelectorThread<T, RP, SP> selectorThread)
	{
		_selectorThread = selectorThread;
	}

	protected void setClient(final T client)
	{
		_client = client;
	}

	public final T getClient()
	{
		return _client;
	}

	/**
	 * Sends a packet to the client, by adding it to the queue, and enabling write interest.
	 * 
	 * @param packets
	 *            the packets to be sent
	 */
	public final void sendPacket(final SP... packets)
	{
		if(isClosed() || _selectionKey == null || packets == null || packets.length <= 0)
			return;

		if(SelectorThread.DEBUG_NETWORK)
			System.out.println("MMOConnection.sendPacket(SendablePacket<T>...) length = " + packets.length);

		synchronized (getSendQueue())
		{
			for(final SP sp : packets)
				if(sp != null)
					getSendQueue().addLast(sp);
		}

		if(!useInterestScheduler())
			enableWriteInterest();
	}

	public final void sendPackets(final Collection<SP> packets)
	{
		if(isClosed() || _selectionKey == null || packets == null || packets.isEmpty())
			return;

		if(SelectorThread.DEBUG_NETWORK)
			System.out.println("MMOConnection.sendPackets(Collection<? extends SendablePacket<T>>) length = " + packets.size());

		synchronized (getSendQueue())
		{
			getSendQueue().addAll(packets);
		}

		if(!useInterestScheduler())
			enableWriteInterest();
	}

	public final void sendPackets(final Deque<SP> packets)
	{
		if(isClosed() || _selectionKey == null || packets == null || packets.isEmpty())
			return;

		if(SelectorThread.DEBUG_NETWORK)
			System.out.println("MMOConnection.sendPackets(Deque<? extends SendablePacket<T>>) length = " + packets.size());

		synchronized (getSendQueue())
		{
			SP p;
			while ((p = packets.poll()) != null)
				getSendQueue().addLast(p);
		}

		if(!useInterestScheduler())
			enableWriteInterest();
	}

	protected SelectorThread<T, RP, SP> getSelectorThread()
	{
		return _selectorThread;
	}

	protected void setSelectionKey(final SelectionKey key)
	{
		_selectionKey = key;
	}

	protected SelectionKey getSelectionKey()
	{
		return _selectionKey;
	}

	protected final void enableReadInterest()
	{
		try
		{
			_selectionKey.interestOps(_selectionKey.interestOps() | SelectionKey.OP_READ);
		}
		catch(final CancelledKeyException e)
		{}
	}

	protected final void disableReadInterest()
	{
		try
		{
			_selectionKey.interestOps(_selectionKey.interestOps() & ~SelectionKey.OP_READ);
		}
		catch(final CancelledKeyException e)
		{}
	}

	protected final void enableWriteInterest()
	{
		try
		{
			_selectionKey.interestOps(_selectionKey.interestOps() | SelectionKey.OP_WRITE);
		}
		catch(final CancelledKeyException e)
		{}
	}

	protected final void disableWriteInterest()
	{
		try
		{
			_selectionKey.interestOps(_selectionKey.interestOps() & ~SelectionKey.OP_WRITE);
		}
		catch(final CancelledKeyException e)
		{}
	}

	/**
	 * @param socket
	 *            the socket to set
	 */
	protected void setSocket(final ISocket socket)
	{
		_socket = socket;
	}

	/**
	 * @return the socket
	 */
	public ISocket getSocket()
	{
		return _socket;
	}

	public WritableByteChannel getWritableChannel()
	{
		return _socket.getWritableByteChannel();
	}

	public ReadableByteChannel getReadableByteChannel()
	{
		return _socket.getReadableByteChannel();
	}

	protected ArrayDeque<SP> getSendQueue()
	{
		if(_sendQueue == null)
			_sendQueue = new ArrayDeque<SP>();
		return _sendQueue;
	}

	protected void createWriteBuffer(final ByteBuffer buf)
	{
		if(_primaryWriteBuffer == null)
		{
			// APPENDING FOR NULL
			_primaryWriteBuffer = _selectorThread.getPooledBuffer();
			_primaryWriteBuffer.put(buf);
		}
		else
		{
			final ByteBuffer temp = _selectorThread.getPooledBuffer();
			temp.put(buf);

			final int remaining = temp.remaining();
			_primaryWriteBuffer.flip();
			final int limit = _primaryWriteBuffer.limit();

			if(remaining >= _primaryWriteBuffer.remaining())
			{
				temp.put(_primaryWriteBuffer);
				_selectorThread.recycleBuffer(_primaryWriteBuffer);
				_primaryWriteBuffer = temp;
			}
			else
			{
				_primaryWriteBuffer.limit(remaining);
				temp.put(_primaryWriteBuffer);
				_primaryWriteBuffer.limit(limit);
				_primaryWriteBuffer.compact();
				_secondaryWriteBuffer = _primaryWriteBuffer;
				_primaryWriteBuffer = temp;
			}
		}
	}

	protected boolean hasPendingWriteBuffer()
	{
		return _primaryWriteBuffer != null;
	}

	protected void movePendingWriteBufferTo(final ByteBuffer dest)
	{
		_primaryWriteBuffer.flip();
		dest.put(_primaryWriteBuffer);
		_selectorThread.recycleBuffer(_primaryWriteBuffer);
		_primaryWriteBuffer = _secondaryWriteBuffer;
		_secondaryWriteBuffer = null;
	}

	protected ByteBuffer getWriteBuffer()
	{
		final ByteBuffer ret = _primaryWriteBuffer;
		if(_secondaryWriteBuffer != null)
		{
			_primaryWriteBuffer = _secondaryWriteBuffer;
			_secondaryWriteBuffer = null;
		}
		return ret;
	}

	protected void setPendingHeader(final int size)
	{
		_readHeaderPending = size;
	}

	protected int getPendingHeader()
	{
		return _readHeaderPending;
	}

	protected void setReadBuffer(final ByteBuffer buf)
	{
		_readBuffer = buf;
	}

	protected ByteBuffer getReadBuffer()
	{
		return _readBuffer;
	}

	public final boolean isClosed()
	{
		return _pendingClose;
	}

	/**
	 * Clears the packet queue, and closes the client.
	 */
	protected void closeNow(final boolean error)
	{
		boolean selectorThreadClose = false;
		synchronized (getSendQueue())
		{
			if(!isClosed())
			{
				_pendingClose = true;
				getSendQueue().clear();
				if(!error)
					disableWriteInterest();
				if(_selectorThread != null)
					selectorThreadClose = true;
			}
		}

		if(selectorThreadClose && _selectorThread != null)
			_selectorThread.closeConnection(this);
		cancelInterest();
	}

	@SuppressWarnings("unchecked")
	public void close(final SP packet)
	{
		synchronized (getSendQueue())
		{
			if(isClosed())
				return;
			getSendQueue().clear();
		}

		if(!isClosed() && _selectionKey.isValid())
		{
			sendPacket(packet);
			if(useInterestScheduler() && !getSendQueue().isEmpty())
				_selectionKey.interestOps(_selectionKey.interestOps() | SelectionKey.OP_WRITE);
		}
		synchronized (getSendQueue())
		{
			_pendingClose = true;
			if(_selectorThread != null)
				_selectorThread.closeConnection(this);
		}
		cancelInterest();
	}

	protected void releaseBuffers()
	{
		if(_primaryWriteBuffer != null)
		{
			_selectorThread.recycleBuffer(_primaryWriteBuffer);
			_primaryWriteBuffer = null;
			if(_secondaryWriteBuffer != null)
			{
				_selectorThread.recycleBuffer(_secondaryWriteBuffer);
				_secondaryWriteBuffer = null;
			}
		}
		if(_readBuffer != null)
		{
			_selectorThread.recycleBuffer(_readBuffer);
			_readBuffer = null;
		}
	}

	protected void onDisconnection()
	{
		getClient().onDisconnection();
		cancelInterest();
	}

	protected void onForcedDisconnection()
	{
		getClient().onForcedDisconnection();
		cancelInterest();
	}

	private synchronized void cancelInterest()
	{
		if(interest == null)
			return;
		interestPool.remove(interest);
		interest.cancel(false);
		interest = null;
	}

	private boolean useInterestScheduler()
	{
		return Config.INTEREST_ALT && _client != null && _client.isGameClient();
	}

	@Override
	public String toString()
	{
		return "MMOConnection: selector=" + _selectorThread + "; client=" + getClient();
	}

	private class ScheduleInterest implements Runnable
	{
		@Override
		public void run()
		{
			if(useInterestScheduler() && !getSendQueue().isEmpty())
				_selectionKey.interestOps(_selectionKey.interestOps() | SelectionKey.OP_WRITE);
		}
	}
}
