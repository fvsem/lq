package l2n.commons.network;

import l2n.commons.text.NgStringBuilder;

public final class NioNetCharBuffer
{
	private final char[] _buf;
	private int _len;

	public NioNetCharBuffer(final int size)
	{
		_buf = new char[size];
		_len = 0;
	}

	public final void clear()
	{
		_len = 0;
	}

	public final void appendUnsafe(final char c)
	{
		_buf[_len++] = c;
	}

	@Override
	public final String toString()
	{
		return new String(_buf, 0, _len);
	}

	public final String toString(final int maxLen)
	{
		return _len > maxLen ? toString().substring(0, maxLen) : toString();
	}

	public final NgStringBuilder toSB()
	{
		return new NgStringBuilder(_buf, 0, _len);
	}
}
