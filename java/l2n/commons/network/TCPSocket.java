package l2n.commons.network;

import org.apache.commons.io.IOUtils;

import java.net.InetAddress;
import java.net.Socket;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public class TCPSocket implements ISocket
{
	private final Socket _socket;

	public TCPSocket(final Socket socket)
	{
		_socket = socket;
	}

	@Override
	public void close()
	{
		IOUtils.closeQuietly(_socket);
	}

	@Override
	public ReadableByteChannel getReadableByteChannel()
	{
		return _socket.getChannel();
	}

	@Override
	public WritableByteChannel getWritableByteChannel()
	{
		return _socket.getChannel();
	}

	@Override
	public InetAddress getInetAddress()
	{
		return _socket.getInetAddress();
	}

	@Override
	public InetAddress getLocalAddress()
	{
		return _socket.getLocalAddress();
	}

	public Socket getSocket()
	{
		return _socket;
	}
}
