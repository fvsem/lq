package l2n.commons.network;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public abstract class ReceivablePacket<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>> extends AbstractPacket<T, RP, SP> implements Runnable
{
	protected ByteBuffer _buf;
	protected NioNetCharBuffer _cbuf;

	protected ReceivablePacket()
	{}

	protected void setByteBuffer(final ByteBuffer buf)
	{
		_buf = buf;
	}

	@Override
	protected ByteBuffer getByteBuffer()
	{
		return _buf;
	}

	private final void fillChars()
	{
		_cbuf.clear();
		char c;
		while ((c = _buf.getChar()) != 0)
			_cbuf.appendUnsafe(c);
	}

	protected int getAvaliableBytes()
	{
		return _buf.remaining();
	}

	public void skip(final int byteCount)
	{
		if(_buf.remaining() < byteCount)
			throw new BufferUnderflowException();

		_buf.position(_buf.position() + byteCount);
	}

	protected boolean isAvaliable()
	{
		return _buf.hasRemaining();
	}

	protected abstract boolean read();

	@Override
	public abstract void run();

	protected void readB(final byte[] dst)
	{
		_buf.get(dst);
	}

	protected void readB(final byte[] dst, final int offset, final int len)
	{
		_buf.get(dst, offset, len);
	}


	protected int readC()
	{
		return _buf.get() & 0xFF;
	}


	protected int readH()
	{
		return _buf.getShort() & 0xFFFF;
	}


	protected int readD()
	{
		return _buf.getInt();
	}


	protected long readQ()
	{
		return _buf.getLong();
	}


	protected double readF()
	{
		return _buf.getDouble();
	}


	protected String readS()
	{
		fillChars();
		return _cbuf.toString();
	}

	protected String readS(final int maxLen)
	{
		fillChars();
		return _cbuf.toString(maxLen);
	}


	// TODO it's still not the most optimal solution, because it requires manual override in order to work
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return false;
	}
}
