package l2n.commons.network;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

public abstract class SendablePacket<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>> extends AbstractPacket<T, RP, SP>
{
	@Override
	protected ByteBuffer getByteBuffer()
	{
		return getCurrentSelectorThread().getWriteBuffer();
	}

	@Override
	public T getClient()
	{
		final SelectorThread<T, RP, SP> selector = getCurrentSelectorThread();
		return selector == null ? null : selector.getWriteClient();
	}

	protected void putInt(final int value)
	{
		getByteBuffer().putInt(value);
	}

	protected void putDouble(final double value)
	{
		getByteBuffer().putDouble(value);
	}

	protected void putFloat(final float value)
	{
		getByteBuffer().putFloat(value);
	}

	protected void writeC(final int data)
	{
		getByteBuffer().put((byte) data);
	}

	protected void writeF(final double value)
	{
		getByteBuffer().putDouble(value);
	}

	protected void writeH(final int value)
	{
		getByteBuffer().putShort((short) value);
	}

	protected void writeD(final int value)
	{
		getByteBuffer().putInt(value);
	}

	protected void writeQ(final long value)
	{
		getByteBuffer().putLong(value);
	}

	protected void writeB(final byte[] data)
	{
		getByteBuffer().put(data);
	}

	protected void writeS(final CharSequence cs)
	{
		if(cs != null)
		{
			final ByteBuffer buf = getByteBuffer();
			final int len = cs.length();
			final byte[] array = buf.array();
			int pos = buf.position();

			if(buf.limit() - pos < len * 2 + 2)
				throw new BufferOverflowException();

			for(int i = 0; i < len; i++)
			{
				final char c = cs.charAt(i);
				array[pos++] = (byte) c;
				array[pos++] = (byte) (c >> 8);
			}

			array[pos++] = 0;
			array[pos++] = 0;

			buf.position(pos);
		}
		else
			getByteBuffer().putChar('\000');
	}

	protected abstract void write();

	protected abstract int getHeaderSize();

	protected abstract void writeHeader(int dataSize);
}
