package l2n.commons.network;

public interface IClientFactory<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	public T create(final MMOConnection<T, RP, SP> con);
}
