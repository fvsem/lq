package l2n.commons.network;

import javolution.util.FastList;
import l2n.Config;
import l2n.game.L2GameThreadPools;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.*;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class SelectorThread<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>> extends Thread
{
	private final static Logger _log = Logger.getLogger(SelectorThread.class.getName());

	public static final boolean DEBUG_NETWORK = false;

	private ScheduledFuture<UnauthedClientTimeoutChecker> _unauthedClientTimeoutChecker;

	private final Selector _selector = Selector.open();

	// Implementations
	private final IPacketHandler<T, RP, SP> _packetHandler;
	private final IClientFactory<T, RP, SP> _clientFactory;
	private final IAcceptFilter _acceptFilter;
	private final TCPHeaderHandler<T> _tcpHeaderHandler;

	private boolean _shutdown;

	// Pending Close
	private final FastList<MMOConnection<T, RP, SP>> _pendingClose = FastList.newInstance();

	// Configs
	private final int HELPER_BUFFER_SIZE;
	private final int HELPER_BUFFER_COUNT;
	private final int MAX_SEND_PER_PASS;
	private static final int PACKET_HEADER_SIZE = 2;
	private final ByteOrder BYTE_ORDER;
	private final long SLEEP_TIME;

	// MAIN BUFFERS
	private final ByteBuffer DIRECT_WRITE_BUFFER;
	private final ByteBuffer WRITE_BUFFER, READ_BUFFER;
	private final NioNetCharBuffer CHAR_BUFFER;

	private T WRITE_CLIENT;

	// ByteBuffers General Purpose Pool
	private final ConcurrentLinkedQueue<ByteBuffer> _bufferPool = new ConcurrentLinkedQueue<ByteBuffer>();

	private static int MAX_UNHANDLED_SOCKETS_PER_IP = 5;
	private static int UNHANDLED_SOCKET_TTL = 5000;

	private boolean enableAntiflood = false;
	private final Object antifloodLock = new Object();

	private final ConcurrentHashMap<String, Integer> _unhandledIPSockets = new ConcurrentHashMap<String, Integer>();
	private final ConcurrentHashMap<Socket, Long> _unhandledChannels = new ConcurrentHashMap<Socket, Long>();
	private final ConcurrentHashMap<T, Long> _unauthedClients = new ConcurrentHashMap<T, Long>();

	public SelectorThread(final SelectorConfig<T, RP, SP> sc, final IPacketHandler<T, RP, SP> packetHandler, final IClientFactory<T, RP, SP> clientFactory, final IAcceptFilter acceptFilter) throws IOException
	{
		HELPER_BUFFER_SIZE = sc.getHelperBufferSize();
		HELPER_BUFFER_COUNT = sc.getHelperBufferCount();
		MAX_SEND_PER_PASS = sc.getMaxSendPerPass();
		BYTE_ORDER = sc.getByteOrder();
		SLEEP_TIME = sc.getSelectorSleepTime();

		DIRECT_WRITE_BUFFER = ByteBuffer.allocate(sc.getWriteBufferSize()).order(BYTE_ORDER);
		WRITE_BUFFER = ByteBuffer.allocate(sc.getWriteBufferSize()).order(BYTE_ORDER);
		READ_BUFFER = ByteBuffer.allocate(sc.getReadBufferSize()).order(BYTE_ORDER);
		CHAR_BUFFER = new NioNetCharBuffer(sc.getCharBufferSize());

		_tcpHeaderHandler = sc.getTCPHeaderHandler();
		initBufferPool();
		_acceptFilter = acceptFilter;
		_packetHandler = packetHandler;
		_clientFactory = clientFactory;

		setPriority(MAX_PRIORITY - 1);

		_unauthedClientTimeoutChecker = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new UnauthedClientTimeoutChecker(), 10, 1000);
	}

	protected void initBufferPool()
	{
		for(int i = 0; i < HELPER_BUFFER_COUNT; i++)
			getFreeBuffers().add(ByteBuffer.wrap(new byte[HELPER_BUFFER_SIZE]).order(BYTE_ORDER));
	}

	public void openServerSocket(InetAddress address, final int tcpPort) throws IOException
	{
		final ServerSocketChannel selectable = ServerSocketChannel.open();
		selectable.configureBlocking(false);

		final ServerSocket ss = selectable.socket();
		ss.setReuseAddress(true);
		// FIXME нужно или нет? ss.setReceiveBufferSize(64 * 1024);

		address = address == null ? MMOSocket.getInstance(true) : address;
		if(address == null)
			ss.bind(new InetSocketAddress(tcpPort));
		else
			ss.bind(new InetSocketAddress(address, tcpPort));

		selectable.register(getSelector(), SelectionKey.OP_ACCEPT);
		setName("SelectorThread: " + tcpPort);
	}

	protected ByteBuffer getPooledBuffer()
	{
		final ByteBuffer buffer = getFreeBuffers().poll();
		if(buffer == null)
			return ByteBuffer.wrap(new byte[HELPER_BUFFER_SIZE]).order(BYTE_ORDER);
		else
			return buffer;
	}

	public void recycleBuffer(final ByteBuffer buf)
	{
		if(getFreeBuffers().size() < HELPER_BUFFER_COUNT)
		{
			buf.clear();
			getFreeBuffers().add(buf);
		}
	}

	public void freeBuffer(final ByteBuffer buf, final MMOConnection<T, RP, SP> con)
	{
		if(buf == READ_BUFFER)
			READ_BUFFER.clear();
		else
		{
			con.setReadBuffer(null);
			recycleBuffer(buf);
		}
	}

	private ConcurrentLinkedQueue<ByteBuffer> getFreeBuffers()
	{
		return _bufferPool;
	}

	public SelectionKey registerClientSocket(final SelectableChannel sc, final int interestOps) throws ClosedChannelException
	{
		SelectionKey sk = null;
		sk = sc.register(getSelector(), interestOps);
		return sk;
	}

	@Override
	public final void run()
	{
		int totalKeys = 0;
		Set<SelectionKey> keys = null;
		Iterator<SelectionKey> it = null;
		SelectionKey key = null;

		// main loop
		for(;;)
		{
			// final long begin = System.nanoTime();
			try
			{
				// check for shutdown
				if(isShuttingDown())
				{
					closeSelectorThread();
					return;
				}

				try
				{
					totalKeys = getSelector().selectNow();
				}
				catch(final IOException e)
				{
					_log.log(Level.WARNING, "", e);
				}

				if(totalKeys > 0)
				{
					keys = getSelector().selectedKeys();
					it = keys.iterator();
					while (it.hasNext())
					{
						key = it.next();
						it.remove();
						if(!key.isValid())
							continue;
						switch (key.readyOps())
						{
							case SelectionKey.OP_CONNECT:
								finishConnection(key);
								break;
							case SelectionKey.OP_ACCEPT:
								acceptConnection(key);
								break;
							case SelectionKey.OP_READ:
								readPacket(key);
								break;
							case SelectionKey.OP_WRITE:
								writePacket(key);
								break;
							case SelectionKey.OP_READ | SelectionKey.OP_WRITE:
								writePacket(key);
								// key might have been invalidated on writePacket
								if(key.isValid())
									readPacket(key);
								break;
						}
					}
					keys.clear();
				}

				closePendingConnections();
			}
			finally
			{
				// RunnableStatsManager.handleStats(getClass(), "selectNow()", System.nanoTime() - begin);
			}

			try
			{
				Thread.sleep(SLEEP_TIME);
			}
			catch(final InterruptedException e)
			{
				_log.log(Level.WARNING, "", e);
			}
		}
	}

	private void closePendingConnections()
	{
		// process pending close
		synchronized (_pendingClose)
		{
			MMOConnection<T, RP, SP> con;
			FastList.Node<MMOConnection<T, RP, SP>> temp;
			for(FastList.Node<MMOConnection<T, RP, SP>> n = _pendingClose.head(), end = _pendingClose.tail(); (n = n.getNext()) != end;)
			{
				con = n.getValue();
				if(con != null && con.getSendQueue().isEmpty())
				{
					temp = n.getPrevious();
					_pendingClose.delete(n);
					n = temp;
					closeConnectionImpl(con);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void finishConnection(final SelectionKey key)
	{
		try
		{
			((SocketChannel) key.channel()).finishConnect();
		}
		catch(final IOException e)
		{
			final MMOConnection<T, RP, SP> con = (MMOConnection<T, RP, SP>) key.attachment();
			final T client = con.getClient();
			client.getConnection().onForcedDisconnection();
			closeConnectionImpl(client.getConnection());
			return;
		}

		// key might have been invalidated on finishConnect()
		if(key.isValid())
		{
			key.interestOps(key.interestOps() | SelectionKey.OP_READ);
			key.interestOps(key.interestOps() & ~SelectionKey.OP_CONNECT);
		}
	}

	private void acceptConnection(final SelectionKey key)
	{
		final ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
		SocketChannel sc;
		SelectionKey clientKey;
		MMOConnection<T, RP, SP> con;
		T client;
		try
		{
			while ((sc = ssc.accept()) != null)
			{
				if(enableAntiflood)
					synchronized (antifloodLock)
					{
						floodCloseOld();
						if(!floodAccept(sc.socket()))
						{
							sc.socket().close();
							continue;
						}
					}
				if(getAcceptFilter() == null || getAcceptFilter().accept(sc))
				{
					sc.configureBlocking(false);
					clientKey = sc.register(getSelector(), SelectionKey.OP_READ);

					con = new MMOConnection<T, RP, SP>(this, new TCPSocket(sc.socket()), clientKey);
					client = getClientFactory().create(con);
					con.setClient(client);
					if(client.isGameClient())
						_unauthedClients.put(client, System.currentTimeMillis());
					clientKey.attach(con);
				}
				else
					sc.socket().close();
			}
		}
		catch(final IOException e)
		{
			_log.log(Level.WARNING, "", e);
		}
	}

	@SuppressWarnings("unchecked")
	private void readPacket(final SelectionKey key)
	{
		final MMOConnection<T, RP, SP> con = (MMOConnection<T, RP, SP>) key.attachment();
		final T client = con.getClient();

		ByteBuffer buf;
		if((buf = con.getReadBuffer()) == null)
		{
			buf = READ_BUFFER;
			buf.clear();
		}
		int result = -2;

		// if we try to to do a read with no space in the buffer it will read 0 bytes
		// going into infinite loop
		if(buf.position() == buf.limit())
		{
			closeConnectionImpl(con);
			return;
		}

		try
		{
			result = con.getReadableByteChannel().read(buf);
		}
		catch(final IOException e)
		{}

		if(result > 0)
		{
			// TODO this should be done before even reading
			if(con.isClosed())
				freeBuffer(buf, con);
			else
			{
				buf.flip();
				// try to read as many packets as possible
				while (tryReadPacket(key, client, buf)) // FIXME
				{}
			}
		}
		else if(result == 0)
		{
			// read interest but nothing to read? wtf?
			closeConnectionImpl(con);
			return;
		}
		else if(result == -1)
			closeConnectionImpl(con);
		else
		{
			con.onForcedDisconnection();
			closeConnectionImpl(con);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean tryReadPacket(final SelectionKey key, final T client, final ByteBuffer buf)
	{
		final MMOConnection<T, RP, SP> con = client.getConnection();

		if(buf.hasRemaining())
		{
			TCPHeaderHandler<T> handler = _tcpHeaderHandler;
			// parse all jeaders
			HeaderInfo<T> ret;
			while (!handler.isChildHeaderHandler())
			{
				handler.handleHeader(key, buf);
				handler = handler.getSubHeaderHandler();
			}
			// last header
			ret = handler.handleHeader(key, buf);

			if(ret != null)
			{
				final int result = buf.remaining();

				// then check if header was processed
				if(ret.headerFinished())
				{
					// get expected packet size
					final int size = ret.getDataPending();

					// do we got enough bytes for the packet?
					if(size <= result)
					{
						boolean parseRet = true;
						// avoid parsing dummy packets (packets without body)
						if(size > 0)
						{
							final int pos = buf.position();
							parseRet = parseClientPacket(getPacketHandler(), buf, size, client);
							buf.position(pos + size);
						}

						// if we are done with this buffer
						if(!buf.hasRemaining() || !parseRet)
						{
							freeBuffer(buf, con);
							return false;
						}

						return parseRet;
					}
					// we dont have enough bytes for the dataPacket so we need to read
					client.getConnection().enableReadInterest();

					// _log.log(Level.INFO, "SelectorThread: LIMIT "+buf.limit());
					if(buf == READ_BUFFER)
					{
						buf.position(buf.position() - PACKET_HEADER_SIZE);
						allocateReadBuffer(con);
					}
					else
					{
						buf.position(buf.position() - PACKET_HEADER_SIZE);
						buf.compact();
					}
					return false;
				}
				// we dont have enough data for header so we need to read
				client.getConnection().enableReadInterest();

				if(buf == READ_BUFFER)
					allocateReadBuffer(con);
				else
					buf.compact();
				return false;
			}
			// null ret means critical error
			// kill the connection
			closeConnectionImpl(con);
			return false;
		}
		// con.disableReadInterest();
		return false; // empty buffer
	}

	private void allocateReadBuffer(final MMOConnection<T, RP, SP> con)
	{
		con.setReadBuffer(getPooledBuffer().put(READ_BUFFER));
		READ_BUFFER.clear();
	}

	private boolean parseClientPacket(final IPacketHandler<T, RP, SP> handler, final ByteBuffer buf, final int dataSize, final T client)
	{
		if(enableAntiflood)
			synchronized (antifloodLock)
			{
				floodClose(((TCPSocket) client.getConnection().getSocket()).getSocket());
			}

		final int pos = buf.position();
		final boolean ret = client.decrypt(buf, dataSize);
		buf.position(pos);
		if(!ret)
			return false;

		if(buf.hasRemaining() && ret)
		{
			// apply limit
			final int limit = buf.limit();
			buf.limit(pos + dataSize);
			final int opcode = buf.get() & 0xFF;

			final RP cp = handler.handlePacket(buf, client, opcode);
			if(cp != null)
			{
				cp._buf = buf;
				cp._cbuf = CHAR_BUFFER;
				cp._client = client;

				// если пакет прочитался, то добавляем в очередь
				if(cp.read())
					client.executePacket(cp);

				cp._buf = null;
				cp._cbuf = null;
			}
			buf.limit(limit);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	private void writePacket(final SelectionKey key)
	{
		final MMOConnection<T, RP, SP> con = (MMOConnection<T, RP, SP>) key.attachment();

		prepareWriteBuffer(con);

		DIRECT_WRITE_BUFFER.flip();
		final int size = DIRECT_WRITE_BUFFER.remaining();

		int result = -1;
		try
		{
			result = con.getWritableChannel().write(DIRECT_WRITE_BUFFER);
		}
		catch(final IOException e)
		{}

		// check if no error happened
		if(result >= 0)
		{
			// check if we writed everything
			if(result == size)
				synchronized (con.getSendQueue())
				{
					if(con.getSendQueue().isEmpty() && !con.hasPendingWriteBuffer())
						con.disableWriteInterest();
				}
			// incomplete write
			else
				con.createWriteBuffer(DIRECT_WRITE_BUFFER);
		}
		else
		{
			con.onForcedDisconnection();
			closeConnectionImpl(con);
		}
	}

	private void prepareWriteBuffer(final MMOConnection<T, RP, SP> con)
	{
		DIRECT_WRITE_BUFFER.clear();

		// if theres pending content add it
		if(con.hasPendingWriteBuffer())
			con.movePendingWriteBufferTo(DIRECT_WRITE_BUFFER);

		if(DIRECT_WRITE_BUFFER.remaining() > 1 && !con.hasPendingWriteBuffer())
		{
			int i = 0;

			final ArrayDeque<SP> sendQueue = con.getSendQueue();
			SP sp;

			synchronized (sendQueue)
			{
				if(DEBUG_NETWORK)
					_log.log(Level.INFO, "SelectorThread: prepareWriteBuffer: sendQueue = " + sendQueue.size());

				WRITE_CLIENT = con.getClient();
				while (i++ < MAX_SEND_PER_PASS && (sp = sendQueue.poll()) != null)
				{
					if(DEBUG_NETWORK)
						_log.log(Level.INFO, "SelectorThread: prepareWriteBuffer: i = " + i);
					try
					{
						// put into WriteBuffer
						putPacketIntoWriteBuffer(con.getClient(), sp);
						WRITE_BUFFER.flip();
						if(DIRECT_WRITE_BUFFER.remaining() >= WRITE_BUFFER.limit())
							DIRECT_WRITE_BUFFER.put(WRITE_BUFFER);
						else
						// если не осталось места в DIRECT_WRITE_BUFFER для WRITE_BUFFER то мы его запишев в следующий раз
						{
							// there is no more space in the direct buffer
							con.createWriteBuffer(WRITE_BUFFER);
							break;
						}
					}
					catch(final Exception e)
					{
						_log.log(Level.WARNING, "", e);
						WRITE_BUFFER.clear();
						break;
					}
				}
			}
		}
	}

	private final void putPacketIntoWriteBuffer(final T client, final SP sp)
	{
		WRITE_BUFFER.clear();

		// reserve space for the size
		final int headerPos = WRITE_BUFFER.position();
		final int headerSize = sp.getHeaderSize();
		WRITE_BUFFER.position(headerPos + headerSize);

		// write content to buffer
		sp.write();

		// size (incl header)
		int dataSize = WRITE_BUFFER.position() - headerPos - headerSize;
		if(dataSize == 0)
		{
			WRITE_BUFFER.position(headerPos);
			return;
		}
		WRITE_BUFFER.position(headerPos + headerSize);

		client.encrypt(WRITE_BUFFER, dataSize);
		// recalculate size after encryption
		dataSize = WRITE_BUFFER.position() - headerPos - headerSize;

		// prepend header
		WRITE_BUFFER.position(headerPos);
		sp.writeHeader(dataSize);
		WRITE_BUFFER.position(headerPos + headerSize + dataSize);
	}

	public final Selector getSelector()
	{
		return _selector;
	}

	public final IPacketHandler<T, RP, SP> getPacketHandler()
	{
		return _packetHandler;
	}

	public final IClientFactory<T, RP, SP> getClientFactory()
	{
		return _clientFactory;
	}

	public IAcceptFilter getAcceptFilter()
	{
		return _acceptFilter;
	}

	public void closeConnection(final MMOConnection<T, RP, SP> con)
	{
		synchronized (_pendingClose)
		{
			_pendingClose.addLast(con);
		}
	}

	protected void closeConnectionImpl(final MMOConnection<T, RP, SP> con)
	{
		try
		{
			if(enableAntiflood)
				synchronized (antifloodLock)
				{
					floodClose(((TCPSocket) con.getSocket()).getSocket());
				}
			// notify connection
			con.onDisconnection();
		}
		finally
		{
			// close socket and the SocketChannel
			con.getSocket().close();
			con.releaseBuffers();
			// clear attachment
			con.getSelectionKey().attach(null);
			// cancel key
			con.getSelectionKey().cancel();
		}
	}

	public void shutdown()
	{
		_shutdown = true;
	}

	public boolean isShuttingDown()
	{
		return _shutdown;
	}

	protected void closeAllChannels()
	{
		final Set<SelectionKey> keys = getSelector().keys();
		for(final SelectionKey key : keys)
			IOUtils.closeQuietly(key.channel());
	}

	protected void closeSelectorThread()
	{
		closeAllChannels();
		if(_unauthedClientTimeoutChecker != null)
		{
			_unauthedClientTimeoutChecker.cancel(false);
			_unauthedClientTimeoutChecker = null;
		}
		try
		{
			getSelector().close();
		}
		catch(final IOException e)
		{
			// Ignore
		}
	}

	public ByteBuffer getWriteBuffer()
	{
		return WRITE_BUFFER;
	}

	public T getWriteClient()
	{
		return WRITE_CLIENT;
	}

	protected boolean floodAccept(final Socket sc)
	{
		final String _ip = sc.getInetAddress().getHostAddress();
		Integer cnt = _unhandledIPSockets.get(_ip);
		if(cnt == null)
		{
			_unhandledIPSockets.put(_ip, 1);
			_unhandledChannels.put(sc, System.currentTimeMillis());
			return true;
		}
		if(cnt < MAX_UNHANDLED_SOCKETS_PER_IP)
		{
			cnt++;
			_unhandledIPSockets.remove(_ip);
			_unhandledIPSockets.put(_ip, cnt);
			_unhandledChannels.put(sc, System.currentTimeMillis());
			return true;
		}
		return false;
	}

	protected void floodClose(final Socket sc)
	{
		if(sc == null)
			return;
		if(!_unhandledChannels.containsKey(sc))
			return;
		_unhandledChannels.remove(sc);
		if(sc.getInetAddress() == null)
			return;
		final String _ip = sc.getInetAddress().getHostAddress();
		if(_ip == null)
			return;
		Integer cnt = _unhandledIPSockets.get(_ip);
		if(cnt == null)
			return;
		cnt--;
		if(cnt < 0)
			cnt = 0;
		_unhandledIPSockets.remove(_ip);
		_unhandledIPSockets.put(_ip, cnt);
	}

	protected void floodCloseOld()
	{
		final Long now_time = System.currentTimeMillis();
		for(final Socket sc : _unhandledChannels.keySet())
		{
			final Long sc_time_diff = now_time - _unhandledChannels.get(sc);
			if(sc_time_diff >= UNHANDLED_SOCKET_TTL)
			{
				floodClose(sc);
				try
				{
					sc.close();
				}
				catch(final IOException e)
				{}
			}
		}
	}

	public void setAntiFlood(final boolean _enableAntiflood)
	{
		enableAntiflood = _enableAntiflood;
	}

	public boolean getAntiFlood()
	{
		return enableAntiflood;
	}

	public void setAntiFloodSocketsConf(final int MaxUnhandledSocketsPerIP, final int UnhandledSocketsMinTTL)
	{
		MAX_UNHANDLED_SOCKETS_PER_IP = MaxUnhandledSocketsPerIP;
		UNHANDLED_SOCKET_TTL = UnhandledSocketsMinTTL;
	}

	/**
	 * Closes invalid (non-authed, timeouted) connections.
	 */
	private class UnauthedClientTimeoutChecker implements Runnable
	{
		@Override
		public void run()
		{
			final long time = System.currentTimeMillis() - Config.TIMEOUT_CHECKER_CLIENT;
			for(final Entry<T, Long> e : _unauthedClients.entrySet())
				if(e.getValue() < time)
				{
					if(e.getKey().stateIsConnected())
						e.getKey().closeNow(false);
					_unauthedClients.remove(e.getKey());
				}
		}
	}
}
