package l2n.commons.compiler;

import java.util.HashMap;

public class MemoryClassLoader extends ClassLoader
{
	private final HashMap<String, MemoryJavaByteCode> byteCodes = new HashMap<String, MemoryJavaByteCode>();

	@Override
	protected Class<?> findClass(final String name) throws ClassNotFoundException
	{
		MemoryJavaByteCode byteCode = byteCodes.get(name);
		if(byteCode == null)
		{
			byteCode = byteCodes.get(name);
			if(byteCode == null)
			{
				System.out.println("MemoryClassLoader: class no found " + name);
				return super.findClass(name);
			}
		}
		final byte[] bytecode = byteCode.getBytes();
		return defineClass(name, bytecode, 0, bytecode.length);
	}

	public void addClass(final String name, final MemoryJavaByteCode mbc)
	{
		byteCodes.put(name.replace("/", "."), mbc);
	}

	public String[] getClasses()
	{
		return byteCodes.keySet().toArray(new String[byteCodes.size()]);
	}
}
