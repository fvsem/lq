package l2n.commons.compiler;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import java.io.IOException;

public class MemoryJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager>
{
	private final MemoryClassLoader classLoader;

	public MemoryJavaFileManager(StandardJavaFileManager fileManager, MemoryClassLoader specClassLoader)
	{
		super(fileManager);
		classLoader = specClassLoader;
	}

	@Override
	public JavaFileObject getJavaFileForOutput(Location location, String name, JavaFileObject.Kind kind, FileObject sibling) throws IOException
	{
		MemoryJavaByteCode byteCode = new MemoryJavaByteCode(name);
		classLoader.addClass(name, byteCode);
		return byteCode;
	}
}
