package l2n.commons.compiler;

import l2n.commons.list.GArray;
import l2n.util.Log;
import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import java.io.*;

public class Compiler
{
	private static final EclipseCompiler javac = new EclipseCompiler();

	// create a new memory JavaFileManager/ to collect errors, warnings etc.
	private static final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
	private static final StandardJavaFileManager standartFileManager = javac.getStandardFileManager(diagnostics, null, null);

	private MemoryClassLoader classLoader = null;

	public static Compiler getInstance()
	{
		return SingletonHolder._instance;
	}

	public boolean compile(final File[] files, final PrintStream err)
	{
		classLoader = new MemoryClassLoader();
		final MemoryJavaFileManager fileManager = new MemoryJavaFileManager(standartFileManager, classLoader);

		// javac options
		final GArray<String> options = new GArray<String>();
		options.add("-Xlint:all");
		options.add("-g");
		options.add("-nowarn");
		options.add("-deprecation");
		options.add("-time");

		Boolean result = null;
		try
		{
			final File file = new File("log/developer/scripts_error.txt");
			file.createNewFile();
			final OutputStream out = new FileOutputStream(file);

			final Writer fileOut = new PrintWriter(out);
			final CompilationTask task = javac.getTask(fileOut, fileManager, diagnostics, options, null, standartFileManager.getJavaFileObjects(files));
			result = task.call();
			out.close();
		}
		catch(final IOException e)
		{
			e.printStackTrace();
		}

		// if(err != null)
		for(final Diagnostic<?> dia : diagnostics.getDiagnostics())
			Log.addDev(dia.toString(), "scripts_error", false);

		return result;
	}

	public boolean compile(final GArray<File> files, final PrintStream err)
	{
		return compile(files.toArray(new File[files.size()]), err);
	}

	public boolean compile(final File file, final PrintStream err)
	{
		return compile(new File[] { file }, err);
	}

	public MemoryClassLoader getClassLoader()
	{
		return classLoader;
	}

	public void clearClassLoader()
	{
		classLoader = null;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final Compiler _instance = new Compiler();
	}
}
