package l2n.commons.list.primitive;

import gnu.trove.procedure.TLongProcedure;
import l2n.commons.list.procedure.primitive.INgLongProcedure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class LongArrayList implements Iterable<Long>
{
	private final class IncIterator implements Iterator<Long>
	{
		private int _off = 0;

		@Override
		public final boolean hasNext()
		{
			return _off < _size;
		}

		@Override
		public final Long next()
		{
			return _values[_off++];
		}

		@Override
		public final void remove()
		{
			removeUnsafeVoid(--_off);
		}
	}

	private long[] _values;
	private int _size;
	private int _no_entry_value;

	public LongArrayList()
	{
		this(10, -1);
	}

	public LongArrayList(final int initialCapacity)
	{
		this(initialCapacity, -1);
	}

	public LongArrayList(int initialCapacity, int no_entry_value)
	{
		_values = new long[initialCapacity];
		_size = 0;
		_no_entry_value = no_entry_value;
	}

	public LongArrayList(final long[] values, final boolean copy)
	{
		if(copy)
		{
			_values = new long[values.length];
			System.arraycopy(values, 0, _values, 0, values.length);
		}
		else
			_values = values;
	}

	public final boolean add(final long value)
	{
		addLast(value);
		return true;
	}

	public final void add(final int index, final long value)
	{
		if(index > _size || index < 0)
			throw new IndexOutOfBoundsException();

		addUnsafe(index, value);
	}

	public final boolean addAll(final Collection<Long> collection)
	{
		if(collection.isEmpty())
			return false;

		ensureCapacity(_size + collection.size());

		final Iterator<Long> iter = collection.iterator();
		Long value;
		while (iter.hasNext())
		{
			value = iter.next();
			if(value != null)
				addLastUnsafe(value);
		}

		return true;
	}

	public final boolean addAll(final LongArrayList list)
	{
		if(list._size == 0)
			return false;

		ensureCapacity(_size + list._size);
		System.arraycopy(list._values, 0, _values, _size, list._size);
		_size += list._size;

		return true;
	}

	public final boolean addAll(final long[] array)
	{
		final int lenght = array.length;
		if(lenght == 0)
			return false;

		final int newSize = _size + lenght;
		ensureCapacity(newSize);
		System.arraycopy(array, 0, _values, _size, lenght);
		_size = newSize;
		return true;
	}

	public final void addFirst(final long value)
	{
		ensureCapacity(_size + 1);
		addFirstUnsafe(value);
	}

	public final void addFirstUnsafe(final long value)
	{
		System.arraycopy(_values, 0, _values, 1, _size);
		_values[0] = value;
	}

	public final void addLast(final long value)
	{
		ensureCapacity(_size + 1);
		addLastUnsafe(value);
	}

	public final void addLastUnsafe(final long value)
	{
		_values[_size++] = value;
	}

	public final void addUnsafe(final int index, final long value)
	{
		final int spaceNeeded = _size + 1;

		if(_values.length < spaceNeeded)
		{
			final long[] values = new long[spaceNeeded];
			System.arraycopy(_values, 0, values, 0, index);
			values[index] = value;
			System.arraycopy(_values, index, values, index + 1, _size - index);
			_values = values;
		}
		else
		{
			System.arraycopy(_values, index, _values, index + 1, _size - index);
			_values[index] = value;
		}

		_size = spaceNeeded;
	}

	public final void clear()
	{
		for(int i = _size; i-- > 0;)
			_values[i] = 0;
		_size = 0;
	}

	public final boolean contains(final long value)
	{
		for(int i = _size; i-- > 0;)
			if(value == _values[i])
				return true;

		return false;
	}

	public final long[] directArray()
	{
		return _values;
	}

	public final void ensureCapacity(final int capacity)
	{
		if(_values.length < capacity)
		{
			final int newCapacity = capacity << 1;
			final long[] values = new long[newCapacity > capacity ? newCapacity : capacity];
			System.arraycopy(_values, 0, values, 0, _size);
			_values = values;
		}
	}

	public final int firstIndexOf(final int value)
	{
		for(int i = 0; i < _size; i++)
			if(value == _values[i])
				return i;
		return -1;
	}

	public final boolean forEach(final TLongProcedure procedure)
	{
		for(int i = _size; i-- > 0;)
			if(!procedure.execute(_values[i]))
				return false;
		return true;
	}

	public final void forEachVoid(final INgLongProcedure procedure)
	{
		for(int i = _size; i-- > 0;)
			procedure.execute(_values[i]);
	}

	public final long get(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return getUnsafe(index);
	}

	public final long getUnsafe(final int index)
	{
		return _values[index];
	}

	public final int indexOf(final long value)
	{
		return lastIndexOf(value);
	}

	public boolean isEmpty()
	{
		return _size == 0;
	}

	@Override
	public final Iterator<Long> iterator()
	{
		return new IncIterator();
	}

	public final int lastIndexOf(final long value)
	{
		for(int i = _size; i-- > 0;)
			if(value == _values[i])
				return i;
		return -1;
	}

	public final long removeFirst()
	{
		if(_size == 0)
			throw new NoSuchElementException();

		return removeFirstUnsafe();
	}

	public final long removeFirstUnsafe()
	{
		final long value = _values[0];
		System.arraycopy(_values, 1, _values, 0, --_size);
		return value;
	}

	public final long removeIndex(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return removeUnsafeIndex(index);
	}

	public final long removeLast()
	{
		if(_size == 0)
			return _no_entry_value;

		return removeLastUnsafe();
	}

	public final long removeLastUnsafe()
	{
		return _values[--_size];
	}

	public final long removeUnsafeIndex(final int index)
	{
		final long value = _values[index];
		System.arraycopy(_values, index + 1, _values, index, --_size - index);
		return value;
	}

	public final boolean removeUnsafeValue(final long value)
	{
		final int index = lastIndexOf(value);
		if(index >= 0)
		{
			removeUnsafeIndex(index);
			return true;
		}

		return false;
	}

	public final void removeUnsafeVoid(final int index)
	{
		if(index < --_size)
			System.arraycopy(_values, index + 1, _values, index, _size - index);
	}

	public final boolean removeValue(final long value)
	{
		if(_size == 0)
			return false;

		return removeUnsafeValue(value);
	}

	public final void removeVoid(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		removeUnsafeVoid(index);
	}

	public final long set(final int index, final long value)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return setUnsafe(index, value);
	}

	public final long setUnsafe(final int index, final long value)
	{
		final long old = _values[index];
		_values[index] = value;
		return old;
	}

	public final void setUnsafeVoid(final int index, final long value)
	{
		_values[index] = value;
	}

	public final void setVoid(final int index, final long value)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		setUnsafeVoid(index, value);
	}

	public final int size()
	{
		return _size;
	}

	public final void sort()
	{
		Arrays.sort(_values, 0, _size);
	}

	public final long[] toArray()
	{
		return toArray(new long[_size]);
	}

	public final long[] toArray(final long[] array)
	{
		System.arraycopy(_values, 0, array, 0, _size);
		return array;
	}

	public final long[] toArray(final long[] array, final int offset)
	{
		System.arraycopy(_values, 0, array, offset, _size);
		return array;
	}

	@Override
	public final String toString()
	{
		if(isEmpty())
			return "[]";

		final StringBuilder isb = new StringBuilder(128);
		isb.append('[');
		for(int i = _size; i-- > 0;)
		{
			isb.append(_values[i]);
			if(i != 0)
			{
				isb.append(' ');
				isb.append(',');
			}
		}
		isb.append(']');
		return isb.toString();
	}

	public final void trimToSize()
	{
		if(_values.length != _size)
		{
			final long[] values = new long[_size];
			System.arraycopy(_values, 0, values, 0, _size);
			_values = values;
		}
	}
}
