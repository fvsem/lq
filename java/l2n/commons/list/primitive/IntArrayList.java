package l2n.commons.list.primitive;

import gnu.trove.procedure.TIntProcedure;
import l2n.commons.list.procedure.primitive.INgIntVoidProcedure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class IntArrayList implements Iterable<Integer>
{
	private final class IncIterator implements Iterator<Integer>
	{
		private int _off;

		@Override
		public final boolean hasNext()
		{
			return _off < _size;
		}

		@Override
		public final Integer next()
		{
			return _values[_off++];
		}

		@Override
		public final void remove()
		{
			removeUnsafeVoid(--_off);
		}
	}

	private int[] _values;
	private int _size;
	private int _no_entry_value;

	public IntArrayList()
	{
		this(10, -1);
	}

	public IntArrayList(final int initialCapacity)
	{
		this(initialCapacity, -1);
	}

	public IntArrayList(int initialCapacity, int no_entry_value)
	{
		_values = new int[initialCapacity];
		_size = 0;
		_no_entry_value = no_entry_value;
	}

	public IntArrayList(final int[] values, final boolean copy)
	{
		if(copy)
		{
			_values = new int[values.length];
			System.arraycopy(values, 0, _values, 0, values.length);
		}
		else
			_values = values;
		_size = _values.length;
	}

	public final boolean add(final int value)
	{
		addLast(value);
		return true;
	}

	public final void add(final int index, final int value)
	{
		if(index > _size || index < 0)
			throw new IndexOutOfBoundsException();

		addUnsafe(index, value);
	}

	public final boolean addAll(final Collection<Integer> collection)
	{
		if(collection.isEmpty())
			return false;

		ensureCapacity(_size + collection.size());

		final Iterator<Integer> iter = collection.iterator();
		Integer value;
		while (iter.hasNext())
		{
			value = iter.next();
			if(value != null)
				addLastUnsafe(value);
		}

		return true;
	}

	public final boolean addAll(final IntArrayList list)
	{
		if(list._size == 0)
			return false;

		ensureCapacity(_size + list._size);
		System.arraycopy(list._values, 0, _values, _size, list._size);
		_size += list._size;

		return true;
	}

	public final boolean addAll(final int[] array)
	{
		final int lenght = array.length;
		if(lenght == 0)
			return false;

		final int newSize = _size + lenght;
		ensureCapacity(newSize);
		System.arraycopy(array, 0, _values, _size, lenght);
		_size = newSize;
		return true;
	}

	public final void addFirst(final int value)
	{
		ensureCapacity(_size + 1);
		addFirstUnsafe(value);
	}

	public final void addFirstUnsafe(final int value)
	{
		System.arraycopy(_values, 0, _values, 1, _size);
		_values[0] = value;
	}

	public final void addLast(final int value)
	{
		ensureCapacity(_size + 1);
		addLastUnsafe(value);
	}

	public final void addLastUnsafe(final int value)
	{
		_values[_size++] = value;
	}

	public final void addUnsafe(final int index, final int value)
	{
		final int spaceNeeded = _size + 1;

		if(_values.length < spaceNeeded)
		{
			final int[] values = new int[spaceNeeded];
			System.arraycopy(_values, 0, values, 0, index);
			values[index] = value;
			System.arraycopy(_values, index, values, index + 1, _size - index);
			_values = values;
		}
		else
		{
			System.arraycopy(_values, index, _values, index + 1, _size - index);
			_values[index] = value;
		}

		_size = spaceNeeded;
	}

	public final void clear()
	{
		_size = 0;
	}

	public final boolean contains(final int value)
	{
		for(int i = _size; i-- > 0;)
			if(value == _values[i])
				return true;
		return false;
	}

	public final int[] directArray()
	{
		return _values;
	}

	public final void ensureCapacity(final int capacity)
	{
		if(_values.length < capacity)
		{
			final int newCapacity = capacity << 1;
			final int[] values = new int[newCapacity > capacity ? newCapacity : capacity];
			System.arraycopy(_values, 0, values, 0, _size);
			_values = values;
		}
	}

	public final int firstIndexOf(final int value)
	{
		for(int i = 0; i < _size; i++)
			if(value == _values[i])
				return i;
		return -1;
	}

	public final boolean forEach(final TIntProcedure procedure)
	{
		for(int i = _size; i-- > 0;)
			if(!procedure.execute(_values[i]))
				return false;
		return true;
	}

	public final void forEachVoid(final INgIntVoidProcedure procedure)
	{
		for(int i = _size; i-- > 0;)
			procedure.execute(_values[i]);
	}

	public final int get(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return getUnsafe(index);
	}

	public final int getUnsafe(final int index)
	{
		return _values[index];
	}

	public final int indexOf(final int value)
	{
		return lastIndexOf(value);
	}

	public boolean isEmpty()
	{
		return _size == 0;
	}

	@Override
	public final Iterator<Integer> iterator()
	{
		return new IncIterator();
	}

	public final int lastIndexOf(final int value)
	{
		for(int i = _size; i-- > 0;)
			if(value == _values[i])
				return i;
		return -1;
	}

	public final int removeFirst()
	{
		if(_size == 0)
			throw new NoSuchElementException();

		return removeFirstUnsafe();
	}

	public final int removeFirstUnsafe()
	{
		final int value = _values[0];
		System.arraycopy(_values, 1, _values, 0, --_size);
		return value;
	}

	public final int removeIndex(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return removeUnsafeIndex(index);
	}

	public final int removeLast()
	{
		if(_size == 0)
			return _no_entry_value;

		return removeLastUnsafe();
	}

	public final int removeLastUnsafe()
	{
		return _values[--_size];
	}

	public final int removeUnsafeIndex(final int index)
	{
		final int value = _values[index];
		System.arraycopy(_values, index + 1, _values, index, --_size - index);
		return value;
	}

	public final boolean removeUnsafeValue(final int value)
	{
		final int index = lastIndexOf(value);
		if(index >= 0)
		{
			removeUnsafeIndex(index);
			return true;
		}

		return false;
	}

	public final void removeUnsafeVoid(final int index)
	{
		if(index < --_size)
			System.arraycopy(_values, index + 1, _values, index, _size - index);
	}

	public final boolean removeValue(final int value)
	{
		if(_size == 0)
			return false;

		return removeUnsafeValue(value);
	}

	public final void removeVoid(final int index)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		removeUnsafeVoid(index);
	}

	public final int set(final int index, final int value)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		return setUnsafe(index, value);
	}

	public final int setUnsafe(final int index, final int value)
	{
		final int old = _values[index];
		_values[index] = value;
		return old;
	}

	public final void setUnsafeVoid(final int index, final int value)
	{
		_values[index] = value;
	}

	public final void setVoid(final int index, final int value)
	{
		if(index >= _size || index < 0)
			throw new IndexOutOfBoundsException();

		setUnsafeVoid(index, value);
	}

	public final int size()
	{
		return _size;
	}

	public final void sort()
	{
		Arrays.sort(_values, 0, _size);
	}

	public final int[] toArray()
	{
		return toArray(new int[_size]);
	}

	public final int[] toArray(final int[] array)
	{
		System.arraycopy(_values, 0, array, 0, _size);
		return array;
	}

	public final int[] toArray(final int[] array, final int offset)
	{
		System.arraycopy(_values, 0, array, offset, _size);
		return array;
	}

	@Override
	public final String toString()
	{
		if(isEmpty())
			return "[]";

		final StringBuilder isb = new StringBuilder(128);
		isb.append('[');
		for(int i = _size; i-- > 0;)
		{
			isb.append(_values[i]);
			if(i != 0)
			{
				isb.append(',');
				isb.append(' ');
			}
		}
		isb.append(']');
		return isb.toString();
	}

	public final void trimToSize()
	{
		if(_values.length != _size)
		{
			final int[] values = new int[_size];
			System.arraycopy(_values, 0, values, 0, _size);
			_values = values;
		}
	}
}
