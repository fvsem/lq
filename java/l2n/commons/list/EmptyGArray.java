package l2n.commons.list;

import l2n.commons.list.procedure.INgObjectProcedure;
import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.commons.util.CollectionUtils;

import java.util.Collection;
import java.util.Iterator;


public class EmptyGArray<E> extends GArray<E>
{
	@Override
	public final Iterator<E> iterator()
	{
		return CollectionUtils.emptyIterator();
	}

	private static final Object[] EMPTY_ARRAY = new Object[0];

	@Override
	public int size()
	{
		return 0;
	}

	@Override
	public boolean contains(final Object obj)
	{
		return false;
	}

	@Override
	public E get(final int index)
	{
		return null;
	}

	@Override
	public boolean isEmpty()
	{
		return true;
	}

	@Override
	public Object[] toArray()
	{
		return EMPTY_ARRAY;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a)
	{
		return (T[]) EMPTY_ARRAY;
	}

	@Override
	public boolean add(final E e)
	{
		return false;
	}

	@Override
	public boolean remove(final Object o)
	{
		return false;
	}

	@Override
	public boolean containsAll(final Collection<?> c)
	{
		return false;
	}

	@Override
	public boolean addAll(final Collection<? extends E> c)
	{
		return false;
	}

	@Override
	public boolean removeAll(final Collection<?> c)
	{
		return false;
	}

	@Override
	public boolean retainAll(final Collection<?> c)
	{
		return false;
	}

	@Override
	public void clear()
	{}

	@Override
	public final boolean forEach(final INgObjectProcedure<E> procedure)
	{
		return true;
	}

	@Override
	public final void forEach(final INgVoidProcedure<E> procedure)
	{}

	@Override
	public final void forEachThread(final INgVoidProcedure<E> procedure)
	{}
}
