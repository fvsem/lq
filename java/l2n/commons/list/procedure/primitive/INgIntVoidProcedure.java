package l2n.commons.list.procedure.primitive;

public interface INgIntVoidProcedure
{
	public void execute(final int value);
}
