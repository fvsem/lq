package l2n.commons.list.procedure.primitive;

public interface INgCharProcedure
{
	public boolean execute(final char value);
}
