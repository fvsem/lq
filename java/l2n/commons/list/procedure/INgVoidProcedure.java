package l2n.commons.list.procedure;

public interface INgVoidProcedure<E>
{
	public void execute(final E value);
}
