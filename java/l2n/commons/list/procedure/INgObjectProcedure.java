package l2n.commons.list.procedure;

import gnu.trove.procedure.TObjectProcedure;

public interface INgObjectProcedure<E> extends TObjectProcedure<E>
{}
