package l2n.commons.list;

import l2n.commons.list.procedure.INgObjectProcedure;
import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.game.L2GameThreadPools;

import java.util.*;


public class GArray<E> implements Collection<E>, RandomAccess
{
	@SuppressWarnings("rawtypes")
	private static final GArray EMPTY_COLLECTION = new EmptyGArray();

	protected transient E[] elementData;
	protected transient int modCount = 0;
	protected int size;

	@SuppressWarnings("unchecked")
	public GArray(int initialCapacity)
	{
		if(initialCapacity < 0)
			throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
		elementData = (E[]) new Object[initialCapacity];
	}

	public GArray()
	{
		this(10);
	}

	public int getCapacity()
	{
		return elementData.length;
	}

	public void ensureCapacity(int minCapacity)
	{
		modCount++;
		int oldCapacity = elementData.length;
		if(minCapacity > oldCapacity)
		{
			int newCapacity = oldCapacity * 3 / 2 + 1;
			if(newCapacity < minCapacity)
				newCapacity = minCapacity;
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	public boolean forEach(final INgObjectProcedure<E> procedure)
	{
		for(int i = size; i-- > 0;)
			if(!procedure.execute(elementData[i]))
				return false;
		return true;
	}

	public void forEach(final INgVoidProcedure<E> procedure)
	{
		for(int i = size; i-- > 0;)
			procedure.execute(elementData[i]);
	}

	public void forEachThread(final INgVoidProcedure<E> procedure)
	{
		L2GameThreadPools.getInstance().executeGeneral(new Runnable()
		{
			@Override
			public final void run()
			{
				forEach(procedure);
			}
		});
	}

	@Override
	public int size()
	{
		return size;
	}

	@Override
	public boolean isEmpty()
	{
		return size == 0;
	}

	public E[] toNativeArray()
	{
		return Arrays.copyOf(elementData, size);
	}

	@Override
	public Object[] toArray()
	{
		return Arrays.copyOf(elementData, size);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T[] toArray(T[] a)
	{
		if(a.length < size)
			return (T[]) Arrays.copyOf(elementData, size, a.getClass());
		System.arraycopy(elementData, 0, a, 0, size);
		if(a.length > size)
			a[size] = null;
		return a;
	}

	public final <T> T[] toArray(final T[] array, final int offset)
	{
		System.arraycopy(elementData, 0, array, offset, size);
		return array;
	}

	/**
	 * Returns the value at the specified position in this collection.
	 */
	public E get(int index)
	{
		RangeCheck(index);
		return getUnsafe(index);
	}

	public final E getUnsafe(final int index)
	{
		return elementData[index];
	}

	/**
	 * Returns the first value of this collection.
	 */
	public E getFirst()
	{
		return get(0);
	}

	/**
	 * Returns the last value of this collection.
	 */
	public E getLast()
	{
		return get(size - 1);
	}

	@Override
	public boolean add(E value)
	{
		ensureCapacity(size + 1);
		addLastUnsafe(value);
		return true;
	}

	public final void addLastUnsafe(final E value)
	{
		elementData[size++] = value;
	}

	/**
	 * Inserts the specified element at the specified position in this list. Shifts the element currently at that position (if any) and any subsequent elements to the right (adds one to their indices).
	 * 
	 * @param index
	 *            index at which the specified element is to be inserted
	 * @param element
	 *            element to be inserted
	 * @throws IndexOutOfBoundsException
	 *             {@inheritDoc}
	 */
	public void add(int index, E element)
	{
		if(index > size || index < 0)
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

		ensureCapacity(size + 1); // Increments modCount!!
		System.arraycopy(elementData, index, elementData, index + 1, size - index);
		elementData[index] = element;
		size++;
	}

	@Override
	public boolean remove(Object o)
	{
		if(o == null)
		{
			for(int index = 0; index < size; index++)
				if(elementData[index] == null)
				{
					fastRemove(index);
					return true;
				}
		}
		else
			for(int index = 0; index < size; index++)
				if(o.equals(elementData[index]))
				{
					fastRemove(index);
					return true;
				}
		return false;
	}

	/*
	 * Private remove method that skips bounds checking and does not
	 * return the value removed.
	 */
	private void fastRemove(int index)
	{
		modCount++;
		int numMoved = size - index - 1;
		if(numMoved > 0)
			System.arraycopy(elementData, index + 1, elementData, index, numMoved);
		elementData[--size] = null; // Let gc do its work
	}

	public E remove(int index)
	{
		RangeCheck(index);

		modCount++;
		E oldValue = getUnsafe(index);

		int numMoved = size - index - 1;
		if(numMoved > 0)
			System.arraycopy(elementData, index + 1, elementData, index, numMoved);
		elementData[--size] = null; // Let gc do its work

		return oldValue;
	}

	public final void removeUnsafeVoid(final int index)
	{
		if(index < --size)
			System.arraycopy(elementData, index + 1, elementData, index, size - index);
		elementData[size] = null;
	}

	public final void removeVoid(final int index)
	{
		if(index >= size || index < 0)
			throw new IndexOutOfBoundsException();

		removeUnsafeVoid(index);
	}

	public E removeFirst()
	{
		return size > 0 ? remove(0) : null;
	}

	public E removeLast()
	{
		if(size > 0)
		{
			modCount++;
			size--;
			E old = elementData[size];
			elementData[size] = null;
			return old;
		}
		return null;
	}

	public E set(int index, E element)
	{
		RangeCheck(index);
		E oldValue = elementData[index];
		elementData[index] = element;
		return oldValue;
	}

	public final void setUnsafeVoid(final int index, final E value)
	{
		elementData[index] = value;
	}

	public int indexOf(Object o)
	{
		if(o == null)
		{
			for(int i = 0; i < size; i++)
				if(elementData[i] == null)
					return i;
		}
		else
			for(int i = 0; i < size; i++)
				if(o.equals(elementData[i]))
					return i;
		return -1;
	}

	@Override
	public boolean contains(Object o)
	{
		if(o == null)
		{
			for(int i = 0; i < size; i++)
				if(elementData[i] == null)
					return true;
		}
		else
			for(int i = 0; i < size; i++)
				if(o.equals(elementData[i]))
					return true;
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends E> c)
	{
		if(c == null)
			return false;
		boolean modified = false;
		Iterator<? extends E> e = c.iterator();
		while (e.hasNext())
			if(add(e.next()))
				modified = true;
		return modified;
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		boolean modified = false;
		for(int i = 0; i < size; i++)
			if(c.contains(elementData[i]))
			{
				modCount++;
				elementData[i] = elementData[size - 1];
				elementData[--size] = null;
				modified = true;
			}
		return modified;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		boolean modified = false;
		for(int i = 0; i < size; i++)
			if(!c.contains(elementData[i]))
			{
				modCount++;
				elementData[i] = elementData[size - 1];
				elementData[--size] = null;
				modified = true;
			}
		return modified;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		for(int i = 0; i < size; i++)
			if(!contains(elementData[i]))
				return false;
		return true;
	}

	private void RangeCheck(int index)
	{
		if(index >= size || index < 0)
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void clear()
	{
		modCount++;
		int oldSize = size;
		size = 0;
		if(oldSize > 1000)
			elementData = (E[]) new Object[10];
		else
			for(int i = 0; i < oldSize; i++)
				elementData[i] = null;
		size = 0;
	}

	/**
	 * Осторожно, при таком очищении в массиве могут оставаться ссылки на обьекты,
	 * удерживающие эти обьекты в памяти!
	 */
	public void clearSize()
	{
		modCount++;
		size = 0;
	}

	@Override
	public Iterator<E> iterator()
	{
		return new Itr();
	}

	private final class Itr implements Iterator<E>
	{
		int cursor = 0;

		@Override
		public final boolean hasNext()
		{
			return cursor < size();
		}

		@Override
		public final E next()
		{
			try
			{
				return elementData[cursor++];
			}
			catch(IndexOutOfBoundsException e)
			{
				throw new NoSuchElementException();
			}
		}

		@Override
		public final void remove()
		{
			try
			{
				fastRemove(--cursor);
			}
			catch(IndexOutOfBoundsException e)
			{
				throw new ConcurrentModificationException();
			}
		}
	}

	@Override
	public String toString()
	{
		StringBuffer bufer = new StringBuffer();
		for(int i = 0; i < size; i++)
		{
			if(i != 0)
				bufer.append(", ");
			bufer.append(elementData[i]);
		}
		return "<" + bufer + ">";
	}

	@SuppressWarnings({ "unchecked" })
	public static final <E> GArray<E> emptyCollection()
	{
		return EMPTY_COLLECTION;
	}
}
