package l2n.commons.math.random;

import l2n.util.Rnd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RndSelector<E>
{
	private class RndNode<T> implements Comparable<RndNode<T>>
	{
		private final T value;
		private final int weight;

		public RndNode(final T value, final int weight)
		{
			this.value = value;
			this.weight = weight;
		}

		@Override
		public int compareTo(final RndNode<T> o)
		{
			return this.weight - weight;
		}
	}

	private int totalWeight = 0;
	private final List<RndNode<E>> nodes;

	public RndSelector()
	{
		nodes = new ArrayList<RndNode<E>>();
	}

	public RndSelector(final int initialCapacity)
	{
		nodes = new ArrayList<RndNode<E>>(initialCapacity);
	}

	public void add(final E value, final int weight)
	{
		if(value == null || weight <= 0)
			return;
		totalWeight += weight;
		nodes.add(new RndNode<E>(value, weight));
	}

	public E chance(final int maxWeight)
	{
		if(maxWeight <= 0)
			return null;

		Collections.sort(nodes);

		final int r = Rnd.get(maxWeight);
		int weight = 0;
		for(int i = 0; i < nodes.size(); i++)
			if((weight += nodes.get(i).weight) > r)
				return nodes.get(i).value;
		return null;
	}

	public E chance()
	{
		return chance(100);
	}

	public E select()
	{
		return chance(totalWeight);
	}

	public void clear()
	{
		totalWeight = 0;
		nodes.clear();
	}

	@Override
	public String toString()
	{
		final StringBuilder ret = new StringBuilder();
		for(int i = 0; i < nodes.size(); i++)
		{
			final RndNode<E> node = nodes.get(i);
			ret.append(node.value.toString()).append(" - ").append(node.weight).append("\r\n");
		}
		return ret.toString();
	}
}
