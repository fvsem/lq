package l2n.commons.misc;

public interface INgObjectFactory<T>
{
	public T newInstance();
}
