package l2n.commons.misc;

import l2n.commons.network.MMOClient;
import l2n.commons.network.ReceivablePacket;
import l2n.commons.network.SendablePacket;
import l2n.commons.threading.FIFOExecutableQueue;

public interface L2ThreadPool<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>>
{
	public void executeQueue(final FIFOExecutableQueue queue);
}
