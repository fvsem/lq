package l2n.commons.misc;

public final class NgObjectBuffer<E>
{
	private final INgObjectFactory<E> _factory;
	private final E[] _buf;
	private int _pos;

	@SuppressWarnings("unchecked")
	public NgObjectBuffer(final int capacity, final INgObjectFactory<E> factory, final boolean fill)
	{
		if(factory == null)
			throw new IllegalArgumentException("Factory is null");

		_factory = factory;
		_buf = (E[]) new Object[capacity];

		if(fill)
		{
			for(; _pos < _buf.length; _pos++)
			{
				_buf[_pos] = _factory.newInstance();
			}
		}
	}

	public final E get()
	{
		if(_pos == 0)
			return _factory.newInstance();

		return _buf[--_pos];
	}

	public final E getIfHasRemaining()
	{
		if(_pos == 0)
			return null;

		return _buf[--_pos];
	}

	public final E getNewInstance()
	{
		return _factory.newInstance();
	}

	public final boolean put(final E val)
	{
		if(_pos < _buf.length)
		{
			_buf[_pos++] = val;
			return true;
		}

		return false;
	}
}
