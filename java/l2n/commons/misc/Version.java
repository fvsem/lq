package l2n.commons.misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

public class Version
{
	private final String _revisionNumber;
	private final String _versionNumber;
	private final String _buildDate;
	private final String _buildBy;
	private final String _hash;

	public Version(final Class<?> c)
	{
		Attributes attrs = null;
		File jar = null;
		try
		{
			jar = Locator.getClassSource(c);
			final JarFile jarFile = new JarFile(jar);
			attrs = jarFile.getManifest().getMainAttributes();
		}
		catch(final Exception e)
		{}

		_buildBy = getValue(attrs, "Build-By", "undefined");
		_buildDate = getValue(attrs, "Build-Date", "undefined");
		_versionNumber = getValue(attrs, "Version", "-1");
		_revisionNumber = getValue(attrs, "Revision", "exported");
		_hash = getHash(jar, "FXW4TP2OAXOTO0OK3T79ZAR0Y6TLIPKNIYL81OXT");
	}

	private String getHash(final File file, final String def)
	{
		if(file == null || !file.exists() || file.isDirectory() || !file.canRead())
			return def;

		InputStream input = null;
		try
		{
			input = new FileInputStream(file);
			final int size = (int) file.length();
			final byte[] data = new byte[size];
			int offset = 0;
			int readed;

			while (offset < size && (readed = input.read(data, offset, size - offset)) != -1)
				offset += readed;

			final MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(data);
			return toHexString(md.digest());
		}
		catch(final Exception e)
		{}
		finally
		{
			if(input != null)
				try
				{
					input.close();
				}
				catch(final IOException e)
				{}
		}

		return def;
	}

	private void byte2hex(final byte b, final StringBuilder buf)
	{
		final char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		final int high = (b & 0xf0) >> 4;
		final int low = b & 0x0f;
		buf.append(hexChars[high]);
		buf.append(hexChars[low]);
	}

	private String toHexString(final byte[] block)
	{
		final StringBuilder buf = new StringBuilder();
		final int len = block.length;
		for(int i = 0; i < len; i++)
			byte2hex(block[i], buf);
		return buf.toString();
	}

	private final String getValue(final Attributes attrs, final String name, final String def)
	{
		String versionNumber = null;
		if(attrs != null && (versionNumber = attrs.getValue(name)) != null)
			return versionNumber;
		return def;
	}

	public String getRevisionNumber()
	{
		return _revisionNumber;
	}

	public String getVersionNumber()
	{
		return _versionNumber;
	}

	public String getBuildDate()
	{
		return _buildDate;
	}

	public String getBuildJdk()
	{
		return _buildBy;
	}

	public String getHash()
	{
		return _hash;
	}
}
