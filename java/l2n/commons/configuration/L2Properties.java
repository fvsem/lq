package l2n.commons.configuration;

import javolution.text.TypeFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.logging.Logger;

public class L2Properties extends Properties
{
	private static final Logger _log = Logger.getLogger(L2Properties.class.getName());
	private static final long serialVersionUID = 1L;
	private final String _configFile;

	public L2Properties(String configFile)
	{
		this(configFile, true);
	}

	public L2Properties(String configFile, boolean msg)
	{
		_configFile = configFile;

		if(msg)
			_log.config("Loading: " + configFile + ".");

		InputStream is = null;
		try
		{
			is = new FileInputStream(new File(configFile));
			InputStreamReader reader = new InputStreamReader(is, "UTF-8");
			load(reader);
		}
		catch(Exception e)
		{
			_log.warning("Configuration file : " + configFile + " cannot be loaded!");
		}
		finally
		{
			if(is != null)
				try
				{
					is.close();
				}
				catch(Exception e)
				{}
		}
	}

	@Override
	public String getProperty(String name, String defaultValue)
	{
		String value = getProperty(name);
		if(value == null)
		{
			_log.config("Value for \"" + name + "\" in file " + _configFile + " is not set, using default : " + defaultValue + ".");
			value = defaultValue;
		}

		if(value != null)
			value = value.trim();
		return value;
	}

	public boolean getBoolean(String name, boolean defaultValue)
	{
		boolean val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = TypeFormat.parseBoolean(value);
		}

		return val;
	}

	public int getInteger(String name, int defaultValue)
	{
		int val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = TypeFormat.parseInt(value);
		}

		return val;
	}

	public byte getByte(String name, int defaultValue)
	{
		int val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = TypeFormat.parseByte(value);
		}

		return (byte) val;
	}

	public long getLong(String name, long defaultValue)
	{
		long val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = Long.parseLong(value);
		}

		return val;
	}

	public double getDouble(String name, double defaultValue)
	{
		double val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = TypeFormat.parseDouble(value);
		}

		return val;
	}

	public float getFloat(String name, float defaultValue)
	{
		float val = defaultValue;
		String value;
		if((value = getProperty(name, String.valueOf(defaultValue))) != null)
		{
			value = value.trim();
			val = TypeFormat.parseFloat(value);
		}

		return val;
	}

	public int[] getIntArray(String name, int... defaultValue)
	{
		String value = getProperty(name);
		return value == null ? defaultValue : parseCommaSeparatedIntegerArray(value.trim());
	}

	public long[] getLongArray(String name, long... defaultValue)
	{
		String value = getProperty(name);
		return value == null ? defaultValue : parseCommaSeparatedLongArray(value.trim());
	}

	public float[] getFloatArray(String name, float... defaultValue)
	{
		String value = getProperty(name);
		return value == null ? defaultValue : parseCommaSeparatedFloatArray(value.trim());
	}

	public double[] getDoubleArray(String name, double... defaultValue)
	{
		String value = getProperty(name);
		return value == null ? defaultValue : parseCommaSeparatedDoubleArray(value.trim());
	}

	public String[] getStringArray(String name, String... defaultValue)
	{
		String value = getProperty(name);
		return value == null ? defaultValue : parseCommaSeparatedStringArray(value.trim());
	}

	public static float[] parseCommaSeparatedFloatArray(String s)
	{
		if(s.isEmpty())
			return new float[0];
		String[] tmp = s.replaceAll(",", ";").split(";");
		float[] ret = new float[tmp.length];
		for(int i = 0; i < tmp.length; i++)
			ret[i] = TypeFormat.parseFloat(tmp[i]);
		return ret;
	}

	public static double[] parseCommaSeparatedDoubleArray(String s)
	{
		if(s.isEmpty())
			return new double[0];
		String[] tmp = s.replaceAll(",", ";").split(";");
		double[] ret = new double[tmp.length];
		for(int i = 0; i < tmp.length; i++)
			ret[i] = TypeFormat.parseDouble(tmp[i]);
		return ret;
	}

	public static int[] parseCommaSeparatedIntegerArray(String s)
	{
		if(s.isEmpty())
			return new int[0];
		String[] tmp = s.replaceAll(",", ";").split(";");
		int[] ret = new int[tmp.length];
		for(int i = 0; i < tmp.length; i++)
			ret[i] = TypeFormat.parseInt(tmp[i]);
		return ret;
	}

	public static long[] parseCommaSeparatedLongArray(String s)
	{
		if(s.isEmpty())
			return new long[0];
		String[] tmp = s.replaceAll(",", ";").split(";");
		long[] ret = new long[tmp.length];
		for(int i = 0; i < tmp.length; i++)
			ret[i] = TypeFormat.parseLong(tmp[i]);
		return ret;
	}

	public static String[] parseCommaSeparatedStringArray(String s)
	{
		if(s.isEmpty())
			return new String[0];
		String[] tmp = s.replaceAll(",", ";").split(";");
		String[] ret = new String[tmp.length];
		for(int i = 0; i < tmp.length; i++)
			ret[i] = tmp[i].trim();
		return ret;
	}
}
