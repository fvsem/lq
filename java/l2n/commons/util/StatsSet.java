package l2n.commons.util;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.skills.EffectType;


public final class StatsSet {
    private static final String items_delimiter = "\r\n<next>\r\n";
    private static final String value_delimiter = "=value=";

    private static final StatsSet EMPTY_STATS_SET = new StatsSet();

    private final TIntObjectHashMap<Object> _set;

    public StatsSet() {
        _set = new TIntObjectHashMap<Object>();
    }

    public StatsSet(final TIntObjectHashMap<Object> map) {
        _set = new TIntObjectHashMap<Object>(map);
    }

    public final TIntObjectHashMap<Object> getSet() {
        return _set;
    }

    public Object getObject(final String name) {
        return _set.get(name.hashCode());
    }

    public boolean getBool(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Boolean value required, but not specified");
        if (val instanceof Boolean)
            return (Boolean) val;
        try {
            return Boolean.parseBoolean((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Boolean value required, but found: " + val);
        }
    }

    public Boolean getBool(final String name, final Boolean deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Boolean)
            return ((Boolean) val).booleanValue();
        try {
            return Boolean.parseBoolean((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Boolean value required, but found: " + val);
        }
    }

    public boolean getBool(final String name, final boolean deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Boolean)
            return ((Boolean) val).booleanValue();
        try {
            return Boolean.parseBoolean((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Boolean value required, but found: " + val);
        }
    }

    public int getInteger(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Integer value required, but not specified for " + name);
        if (val instanceof Number)
            return ((Number) val).intValue();
        try {
            return Integer.parseInt((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public short getShort(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Integer value required, but not specified");
        if (val instanceof Number)
            return ((Number) val).shortValue();
        try {
            return Short.parseShort((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public Short getShort(final String name, final Short deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).shortValue();
        try {
            return Short.parseShort((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public byte getByte(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Integer value required, but not specified");
        if (val instanceof Number)
            return ((Number) val).byteValue();
        try {
            return Byte.parseByte((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public Byte getByte(final String name, final Byte deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).byteValue();
        try {
            return Byte.parseByte((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public int getInteger(final String name, final int deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).intValue();
        try {
            return Integer.parseInt((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public float getFloat(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Float value required, but not specified");
        if (val instanceof Number)
            return ((Number) val).floatValue();
        try {
            return (float) Double.parseDouble((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Float value required, but found: " + val);
        }
    }

    public float getFloat(final String name, final float deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).floatValue();
        try {
            return (float) Double.parseDouble((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Float value required, but found: " + val);
        }
    }

    public double getDouble(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Float value required, but not specified");
        if (val instanceof Number)
            return ((Number) val).doubleValue();
        try {
            return Double.parseDouble((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Float value required, but found: " + val);
        }
    }

    public double getDouble(final String name, final double deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).doubleValue();
        try {
            return Double.parseDouble((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Float value required, but found: " + val);
        }
    }

    public String getString(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("String value required, but not specified");
        return String.valueOf(val);
    }

    public String getString(final String name, final String deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        return String.valueOf(val);
    }

    public long getLong(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Integer value required, but not specified");
        if (val instanceof Number)
            return ((Number) val).longValue();
        try {
            return Long.parseLong((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    public long getLong(final String name, final long deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (val instanceof Number)
            return ((Number) val).longValue();
        try {
            return Long.parseLong((String) val);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Integer value required, but found: " + val);
        }
    }

    @SuppressWarnings(value = {"unchecked"})

    public <T extends Enum<T>> T getEnum(final String name, final Class<T> enumClass) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but not specified");
        if (enumClass.isInstance(val))
            return (T) val;
        try {
            return Enum.valueOf(enumClass, String.valueOf(val));
        } catch (final Exception e) {
            throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but found: " + val);
        }
    }

    @SuppressWarnings(value = {"unchecked"})
    public <T extends Enum<T>> T getEnum(final String name, final Class<T> enumClass, final T deflt) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return deflt;
        if (enumClass.isInstance(val))
            return (T) val;
        try {
            return Enum.valueOf(enumClass, String.valueOf(val));
        } catch (final Exception e) {
            throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but found: " + val);
        }
    }

    public EffectType getEnum(final String name) {
        final Object val = _set.get(name.hashCode());
        if (val == null)
            return EffectType.ERROR;
        if (EffectType.class.isInstance(val))
            return (EffectType) val;
        try {
            return Enum.valueOf(EffectType.class, String.valueOf(val));
        } catch (final Exception e) {
            return EffectType.ERROR;
        }
    }

    public void set(final String name, final String value) {
        _set.put(name.hashCode(), value);
    }

    public void set(final String name, final boolean value) {
        _set.put(name.hashCode(), value);
    }

    public void set(final String name, final int value) {
        _set.put(name.hashCode(), value);
    }

    public void set(final String name, final Object value) {
        _set.put(name.hashCode(), value);
    }

    public void set(final String name, final double value) {
        _set.put(name.hashCode(), value);
    }


    @SuppressWarnings("rawtypes")
    public void set(final String name, final Enum value) {
        _set.put(name.hashCode(), value);
    }

    public void set(final String name, final long value) {
        _set.put(name.hashCode(), value);
    }

    @Override
    public StatsSet clone() {
        return new StatsSet(_set);
    }

    public void unset(final String name) {
        _set.remove(name.hashCode());
    }

    public String serialize() {
        String result = "";
        for (final int name : _set.keys()) {
            if (!result.isEmpty())
                result += items_delimiter;
            result += name + value_delimiter + String.valueOf(_set.get(name));
        }
        return result;
    }

    public static StatsSet unserialize(final String serialized) {
        if (serialized == null || serialized.isEmpty())
            return EMPTY_STATS_SET;

        final StatsSet result = new StatsSet();
        final String[] items = serialized.split(items_delimiter);
        for (final String item : items) {
            final String[] name_n_value = item.split(value_delimiter);
            if (name_n_value.length == 2)
                result.getSet().put(Integer.parseInt(name_n_value[0]), name_n_value[1]);
        }

        return result;
    }
}
