package l2n.commons.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class EnumValues<E extends Enum<E>> implements Iterable<E>
{
	private final E[] _values;

	public EnumValues(final Class<E> clazz)
	{
		_values = clazz.getEnumConstants();
	}

	public final int length()
	{
		return _values.length;
	}

	public final E get(final int index)
	{
		return _values[index];
	}

	public final E valueOf(final int index)
	{
		if(index < 0 || _values.length <= index)
			return defaultValue();

		return _values[index];
	}

	protected E defaultValue()
	{
		return null;
	}

	@Override
	public final Iterator<E> iterator()
	{
		return new EnumValuesIterator<E>(_values);
	}

	private static final class EnumValuesIterator<E> implements Iterator<E>
	{
		private final E[] _values;

		private int _index;

		private EnumValuesIterator(final E[] values)
		{
			_values = values;
		}

		@Override
		public boolean hasNext()
		{
			return _index < _values.length;
		}

		@Override
		public E next()
		{
			if(!hasNext())
				throw new NoSuchElementException();

			return _values[_index++];
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}
