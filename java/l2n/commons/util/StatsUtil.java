package l2n.commons.util;

import l2n.commons.text.StrTable;
import l2n.game.taskmanager.MemoryWatchDog;
import l2n.util.Util;

import java.lang.management.*;

public class StatsUtil
{
	private static final ThreadMXBean threadMXbean = ManagementFactory.getThreadMXBean();

	public static String getMemUsage()
	{
		final long maxMem = MemoryWatchDog.getMemMax();
		final long allocatedMem = MemoryWatchDog.getMemCommitted();
		final long usedMem = MemoryWatchDog.getMemUsed();
		final long nonAllocatedMem = maxMem - allocatedMem;
		final long cachedMem = allocatedMem - usedMem;
		final long useableMem = maxMem - usedMem;

		final StringBuilder list = new StringBuilder();

		list.append("AllowedMemory: ........... ").append(Util.formatBytes(maxMem)).append("\n");
		list.append("\tAllocated: .......... ").append(Util.formatBytes(allocatedMem)).append(" (").append(Math.round((double) allocatedMem / (double) maxMem * 100)).append("%)").append("\n");
		list.append("\tNon-Allocated: ...... ").append(Util.formatBytes(nonAllocatedMem)).append(" (").append(Math.round((double) nonAllocatedMem / (double) maxMem * 100)).append("%)").append("\n");
		list.append("AllocatedMemory: ......... ").append(Util.formatBytes(allocatedMem)).append("\n");
		list.append("\tUsed: ............... ").append(Util.formatBytes(usedMem)).append(" (").append(Math.round((double) usedMem / (double) maxMem * 100)).append("%)").append("\n");
		list.append("\tUnused (cached): .... ").append(Util.formatBytes(cachedMem)).append(" (").append(Math.round((double) cachedMem / (double) maxMem * 100)).append("%)").append("\n");
		list.append("UseableMemory: ........... ").append(Util.formatBytes(useableMem)).append(" (").append(Math.round((double) useableMem / (double) maxMem * 100)).append("%)").append("\n");

		return list.toString();
	}

	public static String getThreadStats(final boolean lockedMonitors, final boolean lockedSynchronizers, final boolean stackTrace)
	{
		final StringBuilder list = new StringBuilder();

		final int threadCount = threadMXbean.getThreadCount();
		final int daemonCount = threadMXbean.getDaemonThreadCount();
		final int nonDaemonCount = threadCount - daemonCount;
		final int peakCount = threadMXbean.getPeakThreadCount();
		final long totalCount = threadMXbean.getTotalStartedThreadCount();

		list.append("Live: .................... ").append(threadCount).append(" threads").append("\n");
		list.append("\tNon-Daemon: ......... ").append(nonDaemonCount).append(" threads").append("\n");
		list.append("\tDaemon: ............. ").append(daemonCount).append(" threads").append("\n");
		list.append("Peak: .................... ").append(peakCount).append(" threads").append("\n");
		list.append("Total started: ........... ").append(totalCount).append(" threads").append("\n");
		list.append("=================================================").append("\n");

		for(final ThreadInfo info : threadMXbean.dumpAllThreads(lockedMonitors, lockedSynchronizers))
		{
			list.append("Thread #").append(info.getThreadId()).append(" (").append(info.getThreadName()).append(")").append("\n");
			list.append("=================================================\n");
			list.append("\tgetThreadState: ...... ").append(info.getThreadState()).append("\n");
			list.append("\tgetWaitedTime: ....... ").append(info.getWaitedTime()).append("\n");
			list.append("\tgetBlockedTime: ...... ").append(info.getBlockedTime()).append("\n");
			for(final MonitorInfo monitorInfo : info.getLockedMonitors())
			{
				list.append("\tLocked monitor: ....... ").append(monitorInfo).append("\n");
				list.append("\t\t[").append(monitorInfo.getLockedStackDepth()).append(".]: at ").append(monitorInfo.getLockedStackFrame()).append("\n");
			}

			for(final LockInfo lockInfo : info.getLockedSynchronizers())
				list.append("\tLocked synchronizer: ...").append(lockInfo).append("\n");
			if(stackTrace)
			{
				list.append("\tgetStackTace: ..........\n");
				for(final StackTraceElement trace : info.getStackTrace())
					list.append("\t\tat ").append(trace).append("\n");
			}
			list.append("=================================================\n");
		}

		return list.toString();
	}

	public static StrTable getThreadStats()
	{
		final int threadCount = threadMXbean.getThreadCount();
		final int daemonCount = threadMXbean.getDaemonThreadCount();
		final int nonDaemonCount = threadCount - daemonCount;
		final int peakCount = threadMXbean.getPeakThreadCount();
		final long totalCount = threadMXbean.getTotalStartedThreadCount();
		final StrTable table = new StrTable("Threads Stats");

		table.set(0, "Name", "Live");
		table.set(0, "Count", Integer.toString(threadCount));
		table.set(1, "Name", "Non-Daemon");
		table.set(1, "Count", Integer.toString(nonDaemonCount));
		table.set(2, "Name", "Daemon");
		table.set(2, "Count", Integer.toString(daemonCount));
		table.set(3, "Name", "Peak");
		table.set(3, "Count", Integer.toString(peakCount));
		table.set(4, "Name", "Total started");
		table.set(4, "Count", Long.toString(totalCount));

		return table;
	}

	public static String getShortUptime()
	{
		final long uptimeInSec = (long) Math.ceil(ManagementFactory.getRuntimeMXBean().getUptime() / 1000.0);

		final long s = uptimeInSec / 1 % 60;
		final long m = uptimeInSec / 60 % 60;
		final long h = uptimeInSec / 3600 % 24;
		final long d = uptimeInSec / 86400;

		final StringBuilder tb = new StringBuilder();
		if(d > 0)
			tb.append(d + "d");

		if(h > 0 || tb.length() != 0)
			tb.append(h + "h");

		if(m > 0 || tb.length() != 0)
			tb.append(m + "m");

		if(s > 0 || tb.length() != 0)
			tb.append(s + "s");

		return tb.toString();
	}

	public static String getFullThreadStats()
	{
		return getThreadStats(true, true, true);
	}
}
