package l2n.commons.util;

import l2n.commons.list.GArray;
import l2n.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.TreeMap;

public class GetImport
{
	private static TreeMap<String, String> entries = new TreeMap<String, String>();

	public static void main(String[] args)
	{
		parseData();
	}

	private static void parseData()
	{
		entries.clear();
		parse();
		for(String classs : entries.keySet())
		{
			Log.add(classs, "import_class", false);
		}
	}

	private static void parse()
	{
		GArray<File> files = new GArray<File>();
		hashFiles(new File("./data/scripts"), files);

		for(File f : files)
			parseFile(f);

		files.clear();
	}

	private static void hashFiles(File file, GArray<File> hash)
	{
		if(!file.exists())
		{
			System.out.println("Dir " + file.getAbsolutePath() + " not exists");
			return;
		}
		File[] files = file.listFiles();
		for(File f : files)
			if(f.getName().endsWith(".java"))
				hash.add(f);
			else if(f.isDirectory() && !f.getName().equals(".svn"))
				try
				{
					hashFiles(f, hash);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
	}

	public static void parseFile(File f)
	{
		LineNumberReader lnr = null;
		try
		{
			lnr = new LineNumberReader(new BufferedReader(new FileReader(f)));
			String line = null;
			String claz;
			while ((line = lnr.readLine()) != null)
			{
				if(line.trim().length() == 0 || line.startsWith("//"))
					continue;

				if(line.startsWith("import l2n.") || line.startsWith("impor statict l2n."))
				{
					claz = line.replace("import static", "");
					claz = claz.replace("import ", "");
					claz = claz.replace(";", "");
					claz = claz.trim();
					entries.put(claz, claz);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(lnr != null)
					lnr.close();
			}
			catch(Exception e1)
			{}
		}
	}
}
