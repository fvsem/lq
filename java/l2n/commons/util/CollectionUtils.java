package l2n.commons.util;

import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 17.09.2011
 * @time 5:08:36
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public final class CollectionUtils
{
	/**
	 * copy from {@link java.util.AbstractList}
	 * 
	 * @param collection
	 * @param <E>
	 * @return hash
	 */
	public static <E> int hashCode(final Collection<E> collection)
	{
		int hashCode = 1;
		final Iterator<E> i = collection.iterator();
		while (i.hasNext())
		{
			final E obj = i.next();
			hashCode = 31 * hashCode + (obj == null ? 0 : obj.hashCode());
		}
		return hashCode;
	}

	/**
	 * Returns an empty, unmodifiable, Serializable Queue.
	 * 
	 * @return an empty, unmodifiable, Serializable Queue.
	 */
	public static <T> Queue<T> emptyQueue()
	{
		return _EMPTY_QUEUE;
	}

	public static final <T> Collection<T> emptyCollection()
	{
		return _EMPTY_COLLECTION;
	}

	public static <V> ConcurrentLinkedQueue<V> emptyConcurrentQueue()
	{
		return _EMPTY_CONCURRENT_QUEUE;
	}

	/**
	 * Returns an empty, unmodifiable, Iterator.
	 * 
	 * @return an empty, unmodifiable, Iterator.
	 */
	public static <T> Iterator<T> emptyIterator()
	{
		return _EMPTY_ITERATOR;
	}

	/**
	 * Returns an empty, unmodifiable, ListIterator.
	 * 
	 * @return an empty, unmodifiable, ListIterator.
	 */
	public static <T> ListIterator<T> emptyListIterator()
	{
		return (ListIterator<T>) _EMPTY_LIST_ITERATOR;
	}

	/**
	 * Returns a minimal Set containing the elements passed in. There is no guarantee that the
	 * returned set is mutable or immutable.
	 * 
	 * @param <T>
	 * @param a
	 *            The elements to add to the Set
	 * @return A set containing the elements passed in.
	 * @see #asUnmodifiableSet
	 */
	public static <T> Set<T> asSet(final T... a)
	{
		return _asSet(a, false);
	}

	/**
	 * Returns an unmodifiable Set containing the elements passed in.
	 * 
	 * @param <T>
	 * @param a
	 *            The elements to add to the Set
	 * @return A set containing the elements passed in.
	 * @see #asSet
	 */
	public static <T> Set<T> asUnmodifiableSet(final T... a)
	{
		return _asSet(a, true);
	}

	/**
	 * Returns an unmodifiable versions of the Set of Enums. If the contents of the set are known
	 * to be unmodifiable by the caller in any way, the set itself will be retured, otherwise an
	 * unmodifiable copy of the Set will be returned.
	 * 
	 * @param s
	 *            Set to get the tamper-proof version of
	 * @return An unmodifiable tamper-proof version of the set
	 */
	public static <E extends Enum<E>> Set<E> unmodifiableCopyOfEnumSet(final Set<E> s)
	{
		final Class<? extends Set> copyClass = s.getClass();

		if(_EMPTY_SET == copyClass || _SINGLETON_SET == copyClass)
			// these classes are already unmodifiable, so just return
			return s;
		else
			return Collections.unmodifiableSet(EnumSet.copyOf(s));
	}

	private static <T> Set<T> _asSet(final T[] a, final boolean makeImmutable)
	{
		final int count = a != null ? a.length : 0;

		Set<T> outSet;

		if(count == 0)
			outSet = Collections.emptySet();
		else if(count == 1)
			outSet = Collections.singleton(a[0]);
		else
		{
			// make the initial size big enough that we don't have to rehash to fit the array, for
			// the .75 load factor we pass in
			final int initialSize = (int) Math.ceil(count / .75d);

			outSet = new HashSet<T>(initialSize, .75f);

			for(int i = 0; i < count; i++)
				outSet.add(a[i]);

			if(makeImmutable)
				outSet = Collections.unmodifiableSet(outSet);
		}

		return outSet;
	}

	private static class EmptyIterator implements Iterator
	{
		@Override
		public boolean hasNext()
		{
			return false;
		}

		@Override
		public Object next()
		{
			throw new NoSuchElementException();
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	private static final class EmptyListIterator extends EmptyIterator implements ListIterator
	{
		@Override
		public boolean hasPrevious()
		{
			return false;
		}

		@Override
		public Object previous()
		{
			throw new NoSuchElementException();
		}

		@Override
		public int nextIndex()
		{
			return 0;
		}

		@Override
		public int previousIndex()
		{
			return -1;
		}

		@Override
		public void set(final Object e)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public void add(final Object e)
		{
			throw new UnsupportedOperationException();
		}
	}

	private static final class EmptyQueue extends AbstractQueue implements Serializable, Queue
	{
		@Override
		public Iterator iterator()
		{
			return _EMPTY_ITERATOR;
		}

		@Override
		public int size()
		{
			return 0;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean contains(final Object o)
		{
			return false;
		}

		@Override
		public boolean offer(final Object e)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Object poll()
		{
			return null;
		}

		@Override
		public Object peek()
		{
			return null;
		}

		@Override
		public boolean add(final Object e)
		{
			return false;
		}

		@Override
		public boolean addAll(final Collection c)
		{
			return false;
		}

		@Override
		public Object remove()
		{
			return null;
		}

		private Object readResolve()
		{
			return _EMPTY_QUEUE;
		}

		private static final long serialVersionUID = 0L;
	}

	private static final class EmptyConcurrentQueue extends ConcurrentLinkedQueue
	{
		@Override
		public Iterator iterator()
		{
			return _EMPTY_ITERATOR;
		}

		@Override
		public int size()
		{
			return 0;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean contains(final Object o)
		{
			return false;
		}

		@Override
		public boolean offer(final Object e)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Object poll()
		{
			return null;
		}

		@Override
		public Object peek()
		{
			return null;
		}

		@Override
		public boolean add(final Object e)
		{
			return false;
		}

		@Override
		public boolean addAll(final Collection c)
		{
			return false;
		}

		@Override
		public Object remove()
		{
			return null;
		}

		private Object readResolve()
		{
			return _EMPTY_QUEUE;
		}

		private static final long serialVersionUID = 0L;
	}

	private static final class EmptyCollection<E> implements Collection<E>
	{
		@Override
		public int size()
		{
			return 0;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean contains(final Object o)
		{
			return false;
		}

		@Override
		public Iterator iterator()
		{
			return emptyIterator();
		}

		@Override
		public Object[] toArray()
		{
			return ArrayUtils.EMPTY_OBJECT_ARRAY;
		}

		@Override
		public Object[] toArray(final Object[] a)
		{
			return ArrayUtils.EMPTY_OBJECT_ARRAY;
		}

		@Override
		public boolean add(final Object e)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean remove(final Object o)
		{
			return false;
		}

		@Override
		public boolean containsAll(final Collection c)
		{
			return false;
		}

		@Override
		public boolean addAll(final Collection c)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean removeAll(final Collection c)
		{
			return false;
		}

		@Override
		public boolean retainAll(final Collection c)
		{
			return false;
		}

		@Override
		public void clear()
		{}
	}

	private static final Class<? extends Set> _EMPTY_SET = Collections.emptySet().getClass();
	private static final Class<? extends Set> _SINGLETON_SET = Collections.singleton(null).getClass();
	private static final Queue _EMPTY_QUEUE = new EmptyQueue();
	private static final ConcurrentLinkedQueue _EMPTY_CONCURRENT_QUEUE = new EmptyConcurrentQueue();
	private static final Iterator _EMPTY_ITERATOR = new EmptyIterator();
	private static final Iterator _EMPTY_LIST_ITERATOR = new EmptyListIterator();
	private static final Collection _EMPTY_COLLECTION = new EmptyCollection();
}
