package l2n.commons.util;

import java.util.Calendar;
import java.util.TimeZone;

public abstract class DateGenerator
{
	public static final String FIELD_GENERATORS = "generators";
	public static final String FIELD_GENERATOR_TYPE = "generator_type";
	public static final String FIELD_TIME_ZONE = "time_zone";
	public static final String FIELD_DAY_OF_MONTH = "day_of_month";
	public static final String FIELD_WEEK_OF_YEAR = "week_of_year";
	public static final String FIELD_DAY_OF_WEEK = "day_of_week";
	public static final String FIELD_HOUR_OF_DAY = "hour_of_day";
	public static final String FIELD_MINUTE_OF_HOUR = "minute_of_hour";

	public static final String FIELD_WEEKS = "weeks";
	public static final String FIELD_DAYS = "days";
	public static final String FIELD_HOURS = "hours";
	public static final String FIELD_MINUTES = "minutes";

	private static final long MILLIS_IN_WEEK = 1000 * 60 * 60 * 24 * 7;
	private static final long MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
	private static final long MILLIS_IN_HOUR = 1000 * 60 * 60;
	private static final long MILLIS_IN_MINUTE = 1000 * 60;

	public static final DateGenerator newGenerator(final StatsSet config) throws IllegalArgumentException
	{
		final int count = config.getInteger(FIELD_GENERATORS, 1);
		if(count == 1)
			return new SingleDateGenerator(config, -1);
		else if(count > 1)
			return new MultiDateGenerator(config, count);

		throw new IllegalArgumentException("Illegal params for ' + FIELD_GENERATORS + ': " + count);
	}

	public final int getDaysInMonth(final int month, final int year)
	{
		switch (month)
		{
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;

			case 4:
			case 6:
			case 9:
			case 11:
				return 30;

			case 2:
				if(year % 4 == 0 && (year % 100 != 0 || year % 400 == 0))
					return 29;
				else
					return 28;
		}

		throw new IllegalArgumentException();
	}

	public abstract long nextDate(final long date);

	private static enum DateGeneratorType
	{
		MONTHLY,
		WEEKLY,
		DAILY,
		INTERVAL;
	}

	private static final class MultiDateGenerator extends DateGenerator
	{
		private final SingleDateGenerator[] _generators;

		private MultiDateGenerator(final StatsSet config, final int count)
		{
			_generators = new SingleDateGenerator[count];
			for(int i = count; i-- > 0;)
				_generators[i] = new SingleDateGenerator(config, i + 1);
		}

		@Override
		public final long nextDate(final long date)
		{
			long closestDate = Long.MAX_VALUE;
			for(final SingleDateGenerator schedule : _generators)
				closestDate = Math.min(closestDate, schedule.nextDate(date));
			return closestDate;
		}
	}

	private static final class SingleDateGenerator extends DateGenerator
	{
		private final DateGeneratorType _generatorType;
		private final Calendar _calendar;

		private int _day_of_month;
		private int _week_of_year;
		private int _day_of_week;
		private int _hour_of_day;
		private int _minute_of_hour;
		private int _weeks;
		private int _days;
		private int _hours;
		private int _minutes;

		private SingleDateGenerator(final StatsSet config, final int multiId) throws IllegalArgumentException
		{
			final String entry = getMultiIdAppendedEntry(FIELD_GENERATOR_TYPE, multiId);
			_generatorType = config.getEnum(entry, DateGeneratorType.class, null);
			if(_generatorType == null)
				throw new IllegalArgumentException("Illegal params for '" + entry + "': not found");

			final String timeZone = config.getString(getMultiIdAppendedEntry(FIELD_TIME_ZONE, multiId), null);
			if(timeZone == null)
				_calendar = Calendar.getInstance();
			else
			{
				final TimeZone tz = TimeZone.getTimeZone(timeZone);
				if(tz == null)
					throw new IllegalArgumentException("Unsupported time zone: '" + timeZone + "'");

				_calendar = Calendar.getInstance(tz);
			}

			_day_of_month = config.getInteger(getMultiIdAppendedEntry(FIELD_DAY_OF_MONTH, multiId), -1);
			_week_of_year = config.getInteger(getMultiIdAppendedEntry(FIELD_WEEK_OF_YEAR, multiId), -1);
			_day_of_week = config.getInteger(getMultiIdAppendedEntry(FIELD_DAY_OF_WEEK, multiId), -1);
			_hour_of_day = config.getInteger(getMultiIdAppendedEntry(FIELD_HOUR_OF_DAY, multiId), -1);
			_minute_of_hour = config.getInteger(getMultiIdAppendedEntry(FIELD_MINUTE_OF_HOUR, multiId), -1);
			_weeks = config.getInteger(getMultiIdAppendedEntry(FIELD_WEEKS, multiId), -1);
			_days = config.getInteger(getMultiIdAppendedEntry(FIELD_DAYS, multiId), -1);
			_hours = config.getInteger(getMultiIdAppendedEntry(FIELD_HOURS, multiId), -1);
			_minutes = config.getInteger(getMultiIdAppendedEntry(FIELD_MINUTES, multiId), -1);

			switch (_generatorType)
			{
				case MONTHLY:
				{
					checkDayOfMonth(multiId, -1);
					checkHourOfDay(multiId, -1);
					checkMinuteOfHour(multiId, 0);
					break;
				}

				case WEEKLY:
				{
					checkDayOfWeek(multiId, -1);
					checkHourOfDay(multiId, -1);
					checkMinuteOfHour(multiId, 0);
					break;
				}

				case DAILY:
				{
					checkHourOfDay(multiId, -1);
					checkMinuteOfHour(multiId, 0);
					break;
				}

				case INTERVAL:
				{
					checkWeekOfYear(multiId, 1);
					checkDayOfWeek(multiId, 1);
					checkHourOfDay(multiId, 0);
					checkMinuteOfHour(multiId, 0);
					checkWeeks(multiId, 0);
					checkDays(multiId, 0);
					checkHours(multiId, 0);
					checkMinutes(multiId, 0);

					if(_weeks + _days + _hours + _minutes == 0)
						throw new IllegalArgumentException("All combined params are equal to 0");
					break;
				}
			}
		}

		@Override
		public synchronized final long nextDate(final long date)
		{
			_calendar.setTimeInMillis(date);
			_calendar.set(Calendar.MILLISECOND, 0);
			_calendar.set(Calendar.SECOND, 0);

			switch (_generatorType)
			{
				case MONTHLY:
				{
					_calendar.set(Calendar.MINUTE, _minute_of_hour);
					_calendar.set(Calendar.HOUR_OF_DAY, _hour_of_day);

					if(_day_of_month == 0)
					{
						_calendar.set(Calendar.DAY_OF_MONTH, _calendar.get(_calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));

						while (_calendar.getTimeInMillis() < date)
						{
							_calendar.add(Calendar.MONTH, 1);
							_calendar.set(Calendar.DAY_OF_MONTH, _calendar.get(_calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
						}
					}
					else
					{
						_calendar.set(Calendar.DAY_OF_MONTH, _day_of_month);

						while (_calendar.getTimeInMillis() < date)
							_calendar.add(Calendar.MONTH, 1);
					}

					return _calendar.getTimeInMillis();
				}

				case WEEKLY:
				{
					_calendar.set(Calendar.MINUTE, _minute_of_hour);
					_calendar.set(Calendar.HOUR_OF_DAY, _hour_of_day);
					_calendar.set(Calendar.DAY_OF_WEEK, _day_of_week);
					return calcDestTime(_calendar.getTimeInMillis(), date, MILLIS_IN_WEEK);
				}

				case DAILY:
				{
					_calendar.set(Calendar.MINUTE, _minute_of_hour);
					_calendar.set(Calendar.HOUR_OF_DAY, _hour_of_day);
					return calcDestTime(_calendar.getTimeInMillis(), date, MILLIS_IN_DAY);
				}

				default:
				{
					_calendar.set(Calendar.MINUTE, _minute_of_hour);
					_calendar.set(Calendar.HOUR_OF_DAY, _hour_of_day);
					_calendar.set(Calendar.DAY_OF_WEEK, _day_of_week);
					_calendar.set(Calendar.WEEK_OF_YEAR, _week_of_year);

					return calcDestTime(_calendar.getTimeInMillis(), date, _weeks * MILLIS_IN_WEEK + _days * MILLIS_IN_DAY + _hours * MILLIS_IN_HOUR + _minutes * MILLIS_IN_MINUTE);
				}
			}
		}

		private final long calcDestTime(long time, final long date, final long add)
		{
			if(time < date)
			{
				time += (date - time) / add * add;
				if(time < date)
					time += add;
			}
			return time;
		}

		private final void checkDayOfMonth(final int multiId, final int defaultValue)
		{
			if(_day_of_month < 1 || _day_of_month > 31)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_DAY_OF_MONTH, multiId) + "': " + (_day_of_month == -1 ? "not found" : _day_of_month));
				_day_of_month = defaultValue;
			}
		}

		private final void checkWeekOfYear(final int multiId, final int defaultValue)
		{
			if(_week_of_year < 1 || _week_of_year > 52)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_WEEK_OF_YEAR, multiId) + "': " + (_week_of_year == -1 ? "not found" : _week_of_year));
				_week_of_year = defaultValue;
			}
		}

		private final void checkDayOfWeek(final int multiId, final int defaultValue)
		{
			if(_day_of_week < 1 || _day_of_week > 7)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_DAY_OF_WEEK, multiId) + "': " + (_day_of_week == -1 ? "not found" : _day_of_week));
				_day_of_week = defaultValue;
			}
		}

		private final void checkHourOfDay(final int multiId, final int defaultValue)
		{
			if(_hour_of_day < 0 || _hour_of_day > 23)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_HOUR_OF_DAY, multiId) + "': " + (_hour_of_day == -1 ? "not found" : _hour_of_day));
				_hour_of_day = defaultValue;
			}
		}

		private final void checkMinuteOfHour(final int multiId, final int defaultValue)
		{
			if(_minute_of_hour < 0 || _minute_of_hour > 59)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_MINUTE_OF_HOUR, multiId) + "': " + (_minute_of_hour == -1 ? "not found" : _minute_of_hour));
				_minute_of_hour = defaultValue;
			}
		}

		private final void checkWeeks(final int multiId, final int defaultValue)
		{
			if(_weeks < 0)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_WEEKS, multiId) + "': " + (_weeks == -1 ? "not found" : _weeks));
				_weeks = defaultValue;
			}
		}

		private final void checkDays(final int multiId, final int defaultValue)
		{
			if(_days < 0)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_DAYS, multiId) + "': " + (_days == -1 ? "not found" : _days));
				_days = defaultValue;
			}
		}

		private final void checkHours(final int multiId, final int defaultValue)
		{
			if(_hours < 0)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_HOURS, multiId) + "': " + (_hours == -1 ? "not found" : _hours));
				_hours = defaultValue;
			}
		}

		private final void checkMinutes(final int multiId, final int defaultValue)
		{
			if(_minutes < 0)
			{
				if(defaultValue == -1)
					throw new IllegalArgumentException("Illegal params for '" + getMultiIdAppendedEntry(FIELD_MINUTES, multiId) + "': " + (_minutes == -1 ? "not found" : _minutes));
				_minutes = defaultValue;
			}
		}
	}

	private static final String getMultiIdAppendedEntry(final String entry, final int multiId)
	{
		if(multiId == -1)
			return entry;
		else
			return entry + multiId;
	}
}
