package l2n.commons.lang.reference;


public interface IHardReference<T>
{

	public T get();

	public void clear();
}
