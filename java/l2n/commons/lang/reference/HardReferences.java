package l2n.commons.lang.reference;

import l2n.game.model.L2Object;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class HardReferences
{
	private HardReferences()
	{}

	private static class EmptyReferencedHolder extends AbstractHardReference<L2Object>
	{
		public EmptyReferencedHolder(final L2Object reference)
		{
			super(reference);
		}
	}

	private static final IHardReference<? extends L2Object> EMPTY_REF = new EmptyReferencedHolder(null);

	@SuppressWarnings("unchecked")
	public static <T> IHardReference<T> emptyRef()
	{
		return (IHardReference<T>) EMPTY_REF;
	}

	/**
	 * Получить список объектов, исходя из коллекции ссылок. Нулевые ссылки будут отфильтрованы.
	 * 
	 * @param <T>
	 * @param refs
	 *            коллекция ссылок
	 * @return коллекцию объектов, на которые указываю ссылки
	 */
	public static <T> Collection<T> unwrap(Collection<IHardReference<T>> refs)
	{
		List<T> result = new ArrayList<T>(refs.size());
		for(IHardReference<T> ref : refs)
		{
			T obj = ref.get();
			if(obj != null)
				result.add(obj);
		}
		return result;
	}

	private static class WrappedIterable<T> implements Iterable<T>
	{
		final Iterable<IHardReference<T>> refs;

		WrappedIterable(Iterable<IHardReference<T>> refs)
		{
			this.refs = refs;
		}

		private static class WrappedIterator<T> implements Iterator<T>
		{
			final Iterator<IHardReference<T>> iterator;

			WrappedIterator(Iterator<IHardReference<T>> iterator)
			{
				this.iterator = iterator;
			}

			@Override
			public boolean hasNext()
			{
				return iterator.hasNext();
			}

			@Override
			public T next()
			{
				return iterator.next().get();
			}

			@Override
			public void remove()
			{
				iterator.remove();
			}
		}

		@Override
		public Iterator<T> iterator()
		{
			return new WrappedIterator<T>(refs.iterator());
		}
	}

	/**
	 * Итерация по коллекции ссылок на объекты.
	 * 
	 * @param <T>
	 * @param refs
	 *            коллекция ссылок на объекты
	 * @return враппер, который будет возвращать при итерации объекты, на которые указывают ссылки
	 */
	public static <T> Iterable<T> iterate(Iterable<IHardReference<T>> refs)
	{
		return new WrappedIterable<T>(refs);
	}
}
