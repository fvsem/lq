package l2n.commons.lang;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;

public final class ByteBufferPool
{
	private final ArrayDeque<ByteBuffer> _pool;

	private final int _poolSize;
	private final int _bufferSize;
	private final boolean _allocateDirect;
	private final ByteOrder _byteOrder;

	private long create = 0;
	private long release = 0;

	public ByteBufferPool(final int poolSize, final int bufferSize, final boolean allocateDirect, final ByteOrder byteOrder)
	{
		_pool = new ArrayDeque<ByteBuffer>(poolSize);
		_poolSize = poolSize;
		_bufferSize = bufferSize;
		_allocateDirect = allocateDirect;
		_byteOrder = byteOrder;
	}

	public final void fillPool()
	{
		for(int i = 0; i < _poolSize; i++)
			release(createNewInstance());
	}

	private final ByteBuffer createNewInstance()
	{
		create++;
		if(_allocateDirect)
			return ByteBuffer.allocateDirect(_bufferSize).order(_byteOrder);
		return ByteBuffer.allocate(_bufferSize).order(_byteOrder);
	}

	/**
	 * Extract a buffer from the pool
	 * 
	 * @return буфер из пула
	 */
	public final ByteBuffer acquire()
	{
		if(_pool.isEmpty())
			return createNewInstance();
		return _pool.pollFirst();
	}

	/**
	 * Returns the buffer to the pool
	 * 
	 * @param buffer
	 */
	public final void release(ByteBuffer buffer)
	{
		if(_pool.size() < _poolSize)
		{
			release++;
			buffer.clear();
			_pool.addLast(buffer);
		}
		else
		{
			buffer.clear();
			buffer = null;
		}
	}

	public final int size()
	{
		return _pool.size();
	}

	public final long getCreate()
	{
		return create;
	}

	public final long getRelease()
	{
		return release;
	}
}
