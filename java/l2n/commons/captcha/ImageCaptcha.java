package l2n.commons.captcha;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.PledgeCrest;
import l2n.util.Rnd;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImageCaptcha implements ICaptcha
{
	private static final Logger _log = Logger.getLogger(ImageCaptcha.class.getName());
	private static final String chars = "abcdefghjijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ1234567890";
	private static final String[] words = Config.CAPTCHA_IMAGE_WORDS;

	public ImageCaptcha()
	{
		_log.info("ImageCaptcha: loaded.");
	}

	@Override
	public void sendCaptchaPage(L2Player activeChar)
	{
		final int imgId = Rnd.get(1000000);
		final String html = "Crest.crest_1_" + imgId;
		try
		{
			String captcha = generateRandomString(4);
			ByteArrayOutputStream bts = new ByteArrayOutputStream();

			ImageIO.write(generateCaptcha(captcha), "png", bts);

			byte[] buffer = bts.toByteArray();
			ByteInputStream bis = new ByteInputStream(buffer, 0, buffer.length);
			PledgeCrest packet = new PledgeCrest(imgId, DDSConverter.convertToDDS(bis).array());
			activeChar.sendPacket(packet);

			NpcHtmlMessage captchapage = new NpcHtmlMessage(0);
			captchapage.setFile("data/html/common/captcha_image.htm");
			captchapage.replace("%captchatime%", Config.CAPTCHA_TIME + "");
			captchapage.replace("%captcha%", html);
	        captchapage.replace("%countCaptcha%", Integer.toString(Config.CAPTCHA_COUNT_ERROR - activeChar.getCaptchaCountError()));

			activeChar.sendPacket(captchapage);
			activeChar.setCaptcha(captcha);
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
	}

	private String generateRandomString(int length)
	{
		Random random = new Random();
		String rndString = "";
		for(int i = 0; i < length; i++)
			rndString += chars.charAt(random.nextInt(chars.length()));
		return rndString;
	}

	private BufferedImage generateCaptcha(String randomString)
	{
		char[] charString = randomString.toCharArray();
		final int width = 256;
		final int height = 64;
		final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		final Graphics2D g2d = bufferedImage.createGraphics();
		final Font font = new Font("verdana", Font.BOLD, 36);
		final Font fontSmall = new Font("Arial", Font.PLAIN, 10);
		final Font fontUpper = new Font("verdana", Font.BOLD | Font.ITALIC, 36);
		RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHints(renderingHints);
		g2d.setFont(font);
		g2d.setColor(new Color(255, 255, 0));
		final GradientPaint gradientPaint = new GradientPaint(0, 0, Color.black, 0, height / 2, Color.black, true);
		g2d.setPaint(gradientPaint);
		g2d.fillRect(0, 0, width, height);
		g2d.setColor(new Color(255, 153, 0));
		int xCordinate = 0;
		int yCordinate = 0;
		for(int i = 0; i < charString.length; i++)
		{
			xCordinate = 55 * i + Rnd.get(25) + 10;
			if(xCordinate >= width - 5)
				xCordinate = 0;
			yCordinate = 30 + Rnd.get(34);
			if(Character.isDigit(charString[i]) || Character.isLowerCase(charString[i]))
				g2d.setFont(font);
			else
				g2d.setFont(fontUpper);

			g2d.drawChars(charString, i, 1, xCordinate, yCordinate);
		}
		for(int i = 0; i < 10; i++)
		{
			g2d.setColor(new Color(Rnd.get(255), Rnd.get(255), Rnd.get(255)));
			g2d.drawLine(Rnd.get(width), Rnd.get(height), Rnd.get(width), Rnd.get(height));
		}

		for(int i = 0; i < words.length * 2; i++)
		{
			g2d.setFont(fontSmall);
			xCordinate = 55 * i + Rnd.get(25) + 10;
			if(xCordinate >= width - 5)
				xCordinate = 0;
			yCordinate = 30 + Rnd.get(34);
			g2d.setColor(new Color(Rnd.get(255), Rnd.get(255), Rnd.get(255)));
			g2d.drawString(words[Rnd.get(words.length)], xCordinate, yCordinate);
		}

		g2d.dispose();
		return bufferedImage;
	}
}
