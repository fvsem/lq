package l2n.commons.captcha;

import l2n.Config;
import l2n.extensions.Bonus;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.EnterWorld;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.util.Log;
import l2n.util.Util;

import javax.realtime.MemoryArea;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CaptchaValidator
{
	private static final Logger _log = Logger.getLogger(CaptchaValidator.class.getName());

	private class SchedulePlayerCaptchaValidation implements Runnable
	{
		private final long _playerStoredId;

		public SchedulePlayerCaptchaValidation(final L2Player player)
		{
			_playerStoredId = player.getStoredId();
		}

		@Override
		public void run()
		{
			final L2Player player = L2ObjectsStorage.getAsPlayer(_playerStoredId);
			if(player != null && !player.getCaptcha().equals(""))
				kickPlayer(player);
		}
	}

	private static CaptchaValidator instance = null;

	private ICaptcha _captchaEngine;

	public static CaptchaValidator getInstance()
	{
		if(instance == null)
			instance = new CaptchaValidator();
		return instance;
	}

	private CaptchaValidator()
	{
		if(Config.CAPTCHA_ENABLE || Config.CAPTCHA_ANTIBOT_ENABLE || Config.CAPTCHA_COMMAND_ENABLE)
			try
			{
				if(Config.CAPTCHA_TYPE.equalsIgnoreCase("IMAGE"))
					_captchaEngine = new ImageCaptcha();
				else
					_captchaEngine = new ASCIICaptcha();
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "CaptchaValidator: ", e);
			}
		else
			_captchaEngine = null;
	}

	private static void sendNextPage(final L2Player activeChar, final boolean actionPerformed)
	{
		if(activeChar.hasPremiumAccount())
			sendPremiumAccountInfoPage(activeChar, activeChar.getAccountPremiumEndDate());
		else if(Config.SHOW_HTML_WELCOME && !(activeChar.getClan() != null && activeChar.getClan().isNoticeEnabled() && activeChar.getClan().getNotice() != "") && !actionPerformed)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile("data/html/welcome/index.htm");
			activeChar.sendPacket(html);
		}
		else if(actionPerformed)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile("data/html/common/captcha_successful.htm");
			activeChar.sendPacket(html);
		}
	}

	public void sendCaptcha(final L2Player activeChar)
	{
		/**if(!Config.CAPTCHA_ENABLE || !Config.CAPTCHA_SHOW_PLAYERS_WITH_PA && activeChar.hasPremiumAccount() || !Config.CAPTCHA_ANTIBOT_ENABLE)
		{
			sendNextPage(activeChar, false);
			return;
		}*/

		_captchaEngine.sendCaptchaPage(activeChar);

		L2GameThreadPools.getInstance().scheduleGeneral(new SchedulePlayerCaptchaValidation(activeChar), Config.CAPTCHA_TIME * 1000);
	}

	public static void processCaptchaBypass(final String command, final L2Player player)
	{
		if(command.length() <= 9)
		{
			Log.add(player.toString() + " write empty captcha", "captcha");
			kickPlayer(player);
		}
		else
		{
			final String playercaptcha = command.substring(9);
			if(player.getCaptcha().compareTo(playercaptcha) == 0)
			{
				player.setCaptcha("");
                player.setCaptchaCountError(0);
				sendNextPage(player, true);
			}
			else
			{
				Log.add(player.toString() + " write '" + playercaptcha + "', need '" + player.getCaptcha() + "'", "captcha");
                if(player.getCaptchaCountError() >= (Config.CAPTCHA_COUNT_ERROR - 1)){
                    kickPlayer(player);
                }else{
                    player.setCaptchaCountError(player.getCaptchaCountError() + 1);
                    CaptchaValidator.getInstance().sendCaptcha(player);
                }
			}
		}
	}

	private static void kickPlayer(final L2Player player)
	{
		player.logout(false, false, true, true);
	}

	private static void sendPremiumAccountInfoPage(final L2Player activeChar, final long bonusTime)
	{
		final Bonus bonusConfig = activeChar.getBonus();
		if(bonusConfig != null && bonusTime > 0)
			MemoryArea.getMemoryArea(EnterWorld.class).executeInArea(new Runnable()
			{
				@Override
				public void run()
				{
				
					if(Config.SHOW_BONUS_INFO_PAGE)
					{
						final NpcHtmlMessage infopage = new NpcHtmlMessage(1);
						infopage.setFile("data/html/common/premium_welcome.htm");
						if(infopage != null)
						{
							
							infopage.replace("%playername%", activeChar.getName());
							
							final Date resultdate = new Date(bonusTime * 1000);
							infopage.replace("%exptime%", Util.datetimeFormatter.format(resultdate));
							
							infopage.replace("%exprate%", String.valueOf(bonusConfig.RATE_XP));
							infopage.replace("%sprate%", String.valueOf(bonusConfig.RATE_SP));
							infopage.replace("%adenarate%", String.valueOf(bonusConfig.RATE_DROP_ADENA));
							infopage.replace("%itemsrate%", String.valueOf(bonusConfig.RATE_DROP_ITEMS));
							infopage.replace("%spoilrate%", String.valueOf(bonusConfig.RATE_DROP_SPOIL));
							infopage.replace("%questreward%", String.valueOf(bonusConfig.RATE_QUESTS_REWARD));
							infopage.replace("%questdrop%", String.valueOf(bonusConfig.RATE_QUESTS_DROP));
							activeChar.sendPacket(infopage);
						}
					}
				}
			});
	}
}
