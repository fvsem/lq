package l2n.commons.captcha;

import java.io.ByteArrayInputStream;
import java.io.IOException;


public class ByteInputStream extends ByteArrayInputStream
{
	private static final byte[] EMPTY_ARRAY = new byte[0];

	public ByteInputStream()
	{
		this(EMPTY_ARRAY, 0);
	}

	public ByteInputStream(byte buf[], int length)
	{
		super(buf, 0, length);
	}

	public ByteInputStream(byte buf[], int offset, int length)
	{
		super(buf, offset, length);
	}

	public byte[] getBytes()
	{
		return buf;
	}

	public int getCount()
	{
		return count;
	}

	@Override
	public void close() throws IOException
	{
		reset();
	}

	public void setBuf(byte[] buf)
	{
		this.buf = buf;
		pos = 0;
		count = buf.length;
	}
}
