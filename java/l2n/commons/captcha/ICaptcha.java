package l2n.commons.captcha;

import l2n.game.model.actor.L2Player;

public interface ICaptcha
{
	public void sendCaptchaPage(final L2Player activeChar);
}
