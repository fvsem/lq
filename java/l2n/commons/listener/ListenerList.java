package l2n.commons.listener;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class ListenerList<T>
{
	protected Set<Listener<T>> listeners = new CopyOnWriteArraySet<Listener<T>>();

	public Collection<Listener<T>> getListeners()
	{
		return listeners;
	}

	public boolean add(Listener<T> listener)
	{
		return listeners.add(listener);
	}


	public boolean remove(Listener<T> listener)
	{
		return listeners.remove(listener);
	}

}
