package l2n.commons.threading;

import l2n.game.L2GameThreadPools;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

/**
 * @<a href="L2System Project">L2s</a>
 * @date 28.04.2013
 * @time 10:16:57
 */
public class ThreadPoolFactory
{
	public static class NextUncaughtExceptionHandler implements UncaughtExceptionHandler
	{
		public final static NextUncaughtExceptionHandler STATIC_INSTANCE = new NextUncaughtExceptionHandler();

		@Override
		public final void uncaughtException(final Thread t, final Throwable e)
		{
			L2GameThreadPools._log.log(Level.SEVERE, "Exception in thread " + t.getName(), e);
		}
	}

	public static class NextRejectedExecutionHandler implements RejectedExecutionHandler
	{
		public final static NextRejectedExecutionHandler STATIC_INSTANCE = new NextRejectedExecutionHandler();

		@Override
		public void rejectedExecution(final Runnable r, final ThreadPoolExecutor executor)
		{
			if(executor.isShutdown())
				return;
			L2GameThreadPools._log.log(Level.SEVERE, r + " from " + executor, new RejectedExecutionException());
		}
	}

	public static class NextPriorityThreadFactory implements ThreadFactory
	{
		private final int _threadPriority;
		private final String _threadGroupName;
		private final ThreadGroup _threadGroup;
		private final AtomicInteger _threadId;

		public NextPriorityThreadFactory(final String threadGroupName, final int threadPriority)
		{
			_threadGroupName = threadGroupName;
			_threadPriority = threadPriority;
			_threadGroup = new ThreadGroup(_threadGroupName);
			_threadId = new AtomicInteger();
		}

		@Override
		public Thread newThread(final Runnable r)
		{
			final Thread t = new Thread(_threadGroup, r, _threadGroupName + '[' + _threadId.getAndIncrement() + ']');
			t.setPriority(_threadPriority);
			t.setUncaughtExceptionHandler(NextUncaughtExceptionHandler.STATIC_INSTANCE);
			return t;
		}

		public int getCreatedThreadsCount()
		{
			return _threadId.get();
		}

		public ThreadGroup getThreadGroup()
		{
			return _threadGroup;
		}
	}

	@SuppressWarnings("serial")
	public static class NextLinkedBlockingQueue extends LinkedBlockingQueue<Runnable>
	{
		private int _maxPoolSize;

		@Override
		public boolean offer(final Runnable e)
		{
			final int size = super.size() + 1;
			if(_maxPoolSize < size)
				_maxPoolSize = size;

			return super.offer(e);
		}

		public final int getMaxSize()
		{
			return _maxPoolSize;
		}
	}

	public static ThreadPoolExecutor createThreadPoolExecutor(final int corePoolSize, final String threadGroupName, final int threadPriority)
	{
		return new ThreadPoolExecutor(corePoolSize, corePoolSize, 1L, TimeUnit.SECONDS, new NextLinkedBlockingQueue(), new NextPriorityThreadFactory(threadGroupName, threadPriority), NextRejectedExecutionHandler.STATIC_INSTANCE);
	}

	public static ThreadPoolExecutor createThreadPoolExecutor(final int corePoolSize, final int maxPoolSize, final long keepAliveTime, final TimeUnit unit, final String threadGroupName, final int threadPriority)
	{
		return new ThreadPoolExecutor(corePoolSize, maxPoolSize, 1L, TimeUnit.SECONDS, new NextLinkedBlockingQueue(), new NextPriorityThreadFactory(threadGroupName, threadPriority), NextRejectedExecutionHandler.STATIC_INSTANCE);
	}

	public static ThreadPoolExecutor createThreadPoolExecutor(final int corePoolSize, final int maxPoolSize, final long keepAliveTime, final TimeUnit unit, final BlockingQueue<Runnable> linkedBlockingQueue, final String threadGroupName, final int threadPriority)
	{
		return new ThreadPoolExecutor(corePoolSize, maxPoolSize, 1L, TimeUnit.SECONDS, linkedBlockingQueue, new NextPriorityThreadFactory(threadGroupName, threadPriority), NextRejectedExecutionHandler.STATIC_INSTANCE);
	}

	public static ScheduledThreadPoolExecutor createScheduledThreadPoolExecutor(final int corePoolSize, final String threadGroupName, final int threadPriority)
	{
		ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor(corePoolSize, new NextPriorityThreadFactory(threadGroupName, threadPriority), NextRejectedExecutionHandler.STATIC_INSTANCE);
		// устанавливаем временной лимит для потоков, в течении которого они могут бездействовать
		pool.setKeepAliveTime(1, TimeUnit.SECONDS);
		// pool.allowCoreThreadTimeOut(true);
		return pool;
	}
}
