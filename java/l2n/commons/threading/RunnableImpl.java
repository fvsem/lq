package l2n.commons.threading;

import l2n.commons.util.LoggerObject;

public abstract class RunnableImpl extends LoggerObject implements Runnable
{
	public abstract void runImpl() throws Exception;

	@Override
	public final void run()
	{
		try
		{
			runImpl();
		}
		catch(Exception e)
		{
			error("Exception: RunnableImpl.run(): " + e, e);
		}
	}
}
