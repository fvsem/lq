package l2n.commons.threading;

import l2n.commons.list.LazyArrayList;
import l2n.commons.util.LoggerObject;
import org.apache.commons.lang3.mutable.MutableLong;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class SteppingRunnableQueueManager extends LoggerObject implements Runnable
{
	private final long tickPerStepInMillis;
	private final List<SteppingScheduledFuture<?>> queue = new CopyOnWriteArrayList<SteppingScheduledFuture<?>>();
	private final AtomicBoolean isRunning = new AtomicBoolean();

	public SteppingRunnableQueueManager(final long tickPerStepInMillis)
	{
		this.tickPerStepInMillis = tickPerStepInMillis;
	}

	public class SteppingScheduledFuture<V> implements RunnableScheduledFuture<V>
	{
		private final Runnable r;
		private final long stepping;
		private final boolean isPeriodic;

		private long step;
		private boolean isCancelled;

		public SteppingScheduledFuture(final Runnable r, final long initial, final long stepping, final boolean isPeriodic)
		{
			this.r = r;
			this.step = initial;
			this.stepping = stepping;
			this.isPeriodic = isPeriodic;
		}

		@Override
		public void run()
		{
			if(--step == 0)
				try
				{
					r.run();
				}
				catch(final Exception e)
				{
					error("Exception in a Runnable execution: ", e);
				}
				finally
				{
					if(isPeriodic)
						step = stepping;
				}
		}

		@Override
		public boolean isDone()
		{
			return isCancelled || !isPeriodic && step == 0;
		}

		@Override
		public boolean isCancelled()
		{
			return isCancelled;
		}

		@Override
		public boolean cancel(final boolean mayInterruptIfRunning)
		{
			return isCancelled = true;
		}

		@Override
		public V get() throws InterruptedException, ExecutionException
		{
			return null;
		}

		@Override
		public V get(final long timeout, final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException
		{
			return null;
		}

		@Override
		public long getDelay(final TimeUnit unit)
		{
			return unit.convert(step * tickPerStepInMillis, TimeUnit.MILLISECONDS);
		}

		@Override
		public int compareTo(final Delayed o)
		{
			return 0;
		}

		@Override
		public boolean isPeriodic()
		{
			return isPeriodic;
		}
	}

	/**
	 * Запланировать выполнение задачи через промежуток времени
	 * 
	 * @param r
	 *            задача для выполнения
	 * @param delay
	 *            задержка в миллисекундах
	 * @return SteppingScheduledFuture управляющий объект, отвечающий за выполенение задачи
	 */
	public <T extends Runnable> SteppingScheduledFuture<T> schedule(final T r, final long delay)
	{
		return schedule(r, delay, delay, false);
	}

	/**
	 * Запланировать выполнение задачи через равные промежутки времени, с начальной задержкой
	 * 
	 * @param r
	 *            задача для выполнения
	 * @param initial
	 *            начальная задержка в миллисекундах
	 * @param delay
	 *            период выполенения в силлисекундах
	 * @return SteppingScheduledFuture управляющий объект, отвечающий за выполенение задачи
	 */
	public <T extends Runnable> SteppingScheduledFuture<T> scheduleAtFixedRate(final T r, final long initial, final long delay)
	{
		return schedule(r, initial, delay, true);
	}

	private <T extends Runnable> SteppingScheduledFuture<T> schedule(final T r, final long initial, final long delay, final boolean isPeriodic)
	{
		SteppingScheduledFuture<T> sr;

		final long initialStepping = getStepping(initial);
		final long stepping = getStepping(delay);

		queue.add(sr = new SteppingScheduledFuture<T>(r, initialStepping, stepping, isPeriodic));

		return sr;
	}

	/**
	 * Выбираем "степпинг" для работы задачи:
	 * если delay меньше шага выполнения, результат будет равен 1
	 * если delay больше шага выполнения, результат будет результатом округления от деления delay / step
	 */
	private long getStepping(long delay)
	{
		delay = Math.max(0, delay);
		return delay % tickPerStepInMillis > tickPerStepInMillis / 2 ? delay / tickPerStepInMillis + 1 : delay < tickPerStepInMillis ? 1 : delay / tickPerStepInMillis;
	}

	@Override
	public void run()
	{
		if(!isRunning.compareAndSet(false, true))
		{
			warn("Slow running queue, managed by " + this + ", queue size: " + queue.size() + "!");
			return;
		}

		try
		{
			if(queue.isEmpty())
				return;

			for(final SteppingScheduledFuture<?> sr : queue)
				if(!sr.isDone())
					sr.run();
		}
		finally
		{
			isRunning.set(false);
		}
	}

	/**
	 * Очистить очередь от выполенных и отмененных задач.
	 */
	public void purge()
	{
		final LazyArrayList<SteppingScheduledFuture<?>> purge = LazyArrayList.newInstance();

		for(final SteppingScheduledFuture<?> sr : queue)
			if(sr.isDone())
				purge.add(sr);

		queue.removeAll(purge);
		LazyArrayList.recycle(purge);
	}

	protected abstract String getName();

	public CharSequence getStats()
	{
		final StringBuilder list = new StringBuilder();

		final Map<String, MutableLong> stats = new TreeMap<String, MutableLong>();
		int total = 0;
		int done = 0;

		for(final SteppingScheduledFuture<?> sr : queue)
		{
			if(sr.isDone())
			{
				done++;
				continue;
			}
			total++;
			MutableLong count = stats.get(sr.r.getClass().getName());
			if(count == null)
				stats.put(sr.r.getClass().getName(), count = new MutableLong(1L));
			else
				count.increment();
		}

		for(final Map.Entry<String, MutableLong> e : stats.entrySet())
			list.append("\t").append(e.getKey()).append(" : ").append(e.getValue().longValue()).append("\n");

		list.append("Scheduled: ....... ").append(total).append("\n");
		list.append("Done/Cancelled: .. ").append(done).append("\n");

		return list;
	}
}
