package l2n.commons.threading;

import l2n.commons.misc.L2ThreadPool;
import l2n.commons.network.MMOClient;
import l2n.commons.network.ReceivablePacket;
import l2n.commons.network.SendablePacket;

import java.util.ArrayDeque;
import java.util.Collection;

public abstract class FIFOSimpleExecutableQueue<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>> extends FIFOExecutableQueue
{
	private final ArrayDeque<RP> _queue = new ArrayDeque<RP>();

	public final void add(final RP t)
	{
		synchronized (_queue)
		{
			_queue.addLast(t);
		}
	}

	public final void addAll(final Collection<RP> c)
	{
		synchronized (_queue)
		{
			_queue.addAll(c);
		}
	}

	public final void execute(final RP t)
	{
		add(t);
		execute();
	}

	public final void executeAll(final Collection<RP> c)
	{
		addAll(c);
		execute();
	}

	public final void executeNow(final RP t)
	{
		add(t);
		executeNow();
	}

	public final void executeAllNow(final Collection<RP> c)
	{
		addAll(c);
		executeNow();
	}

	public final void remove(final RP t)
	{
		synchronized (_queue)
		{
			_queue.remove(t);
		}
	}

	@Override
	protected final boolean isEmpty()
	{
		synchronized (_queue)
		{
			return _queue.isEmpty();
		}
	}

	protected final RP removeFirst()
	{
		synchronized (_queue)
		{
			return _queue.pollFirst();
		}
	}

	@Override
	protected abstract void removeAndExecuteAll();

	@Override
	protected abstract L2ThreadPool<T, RP, SP> getPool();
}
