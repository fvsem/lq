package l2n.commons.threading;

import l2n.commons.misc.L2ThreadPool;
import l2n.commons.network.MMOClient;
import l2n.commons.network.ReceivablePacket;
import l2n.commons.network.SendablePacket;

public abstract class FIFORunnableQueue<T extends MMOClient<T, RP, SP>, RP extends ReceivablePacket<T, RP, SP>, SP extends SendablePacket<T, RP, SP>> extends FIFOSimpleExecutableQueue<T, RP, SP>
{
	private final L2ThreadPool<T, RP, SP> _pool;

	public FIFORunnableQueue(final L2ThreadPool<T, RP, SP> pool)
	{
		_pool = pool;
	}

	@Override
	protected final void removeAndExecuteAll()
	{
		for(RP t; (t = removeFirst()) != null;)
			ExecuteWrapper.execute(t);
	}

	@Override
	protected L2ThreadPool<T, RP, SP> getPool()
	{
		return _pool;
	}
}
