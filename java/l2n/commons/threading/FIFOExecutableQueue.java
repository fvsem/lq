package l2n.commons.threading;

import l2n.commons.misc.L2ThreadPool;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class FIFOExecutableQueue implements Runnable
{
	private final static Logger _log = Logger.getLogger(FIFOExecutableQueue.class.getName());

	private static final byte NONE = 0;
	private static final byte QUEUED = 1;
	private static final byte RUNNING = 2;

	private final ReentrantLock _lock = new ReentrantLock();
	private final Condition _condition = _lock.newCondition();

	private volatile byte _state = NONE;

	public final void execute()
	{
		lock();
		try
		{
			if(_state != NONE)
				return;

			_state = QUEUED;
		}
		finally
		{
			unlock();
		}

		getPool().executeQueue(this);
	}

	public final void executeNow()
	{
		lock();
		try
		{
			if(_state != NONE)
			{
				_condition.awaitUninterruptibly();
				return;
			}

			_state = QUEUED;
		}
		finally
		{
			unlock();
		}
		run();
	}

	public final void lock()
	{
		_lock.lock();
	}

	public final void unlock()
	{
		_lock.unlock();
	}

	@Override
	public final void run()
	{
		try
		{
			while (!isEmpty())
			{
				setState(QUEUED, RUNNING);
				try
				{
					removeAndExecuteAll();
				}
				catch(final RuntimeException e)
				{
					_log.log(Level.WARNING, "", e);
				}
				finally
				{
					setState(RUNNING, QUEUED);
				}
			}
		}
		catch(final RuntimeException e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			setState(QUEUED, NONE);
		}
	}

	private void setState(final byte expected, final byte value)
	{
		lock();
		try
		{
			if(value == NONE)
				_condition.signalAll();

			if(_state != expected)
				throw new IllegalStateException("state: " + _state + ", expected: " + expected);
		}
		finally
		{
			_state = value;

			unlock();
		}
	}

	protected abstract boolean isEmpty();

	protected abstract void removeAndExecuteAll();

	protected abstract L2ThreadPool getPool();
}
