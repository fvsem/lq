package l2n.commons.threading;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class ExecuteWrapper
{
	private final static Logger _log = Logger.getLogger(ExecuteWrapper.class.getName());

	public static Runnable wrap(final Runnable r)
	{
		return new TaskWrapper(r);
	}

	public static Runnable wrapLongRunning(final Runnable r)
	{
		return new LongRunningTaskWrapper(r);
	}

	public static final class TaskWrapper implements Runnable
	{
		private final Runnable _runnable;

		public TaskWrapper(final Runnable runnable)
		{
			_runnable = runnable;
		}

		@Override
		public void run()
		{
			ExecuteWrapper.execute(_runnable);
		}
	}

	public static final class LongRunningTaskWrapper implements Runnable
	{
		private final Runnable _runnable;

		public LongRunningTaskWrapper(final Runnable runnable)
		{
			_runnable = runnable;
		}

		@Override
		public void run()
		{
			ExecuteWrapper.executeLongRunning(_runnable);
		}
	}

	public static void execute(final Runnable runnable)
	{
		final long begin = System.nanoTime();
		try
		{
			runnable.run();
		}
		catch(final RuntimeException e)
		{
			_log.log(Level.WARNING, "Exception in a Runnable execution:", e);
		}
		finally
		{
			RunnableStatsManager.handleStats(runnable.getClass(), System.nanoTime() - begin);
		}
	}

	public static void executeLongRunning(final Runnable runnable)
	{
		final long begin = System.nanoTime();
		try
		{
			runnable.run();
		}
		catch(final RuntimeException e)
		{
			_log.log(Level.WARNING, "Exception in a Runnable execution:", e);
		}
		finally
		{
			RunnableStatsManager.handleStats(runnable.getClass(), System.nanoTime() - begin, RunnableStatsManager.MAXIMUM_RUNTIME_IN_MILLISEC_WITHOUT_WARNING_FOR_LONG_RUNNING_TASKS);
		}
	}
}
