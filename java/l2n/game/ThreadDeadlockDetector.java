package l2n.game;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsUtil;
import l2n.util.Log;

import java.lang.management.*;
import java.util.logging.Logger;

public class ThreadDeadlockDetector extends Thread
{
	private static final Logger _log = Logger.getLogger(ThreadDeadlockDetector.class.getName());
	private final ThreadMXBean mbean = ManagementFactory.getThreadMXBean();

	private long[] locked_ids;
	private ThreadInfo[] locked_infos;

	private static final GArray<Long> all_locked_ids = new GArray<Long>();

	public ThreadDeadlockDetector()
	{
		super();
		setName("Thread Deadlock Detector");
		setDaemon(true);
		setPriority(MIN_PRIORITY);
	}

	@Override
	public void run()
	{
		for(;;)
		{
			checkDeadLock();

			try
			{
				Thread.sleep(Config.DEADLOCK_CHECK_PERIOD);
			}
			catch(InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	private void checkDeadLock()
	{
		if((locked_ids = findDeadlockedThreadIDs()) != null)
		{
			locked_infos = mbean.getThreadInfo(locked_ids, true, true);
			fireDeadlockDetected(locked_infos);
		}
	}

	private void fireDeadlockDetected(ThreadInfo[] tis)
	{
		long time = System.currentTimeMillis();
		String text1 = "", text2 = "";

		int count = 0;
		ThreadInfo ti;
		for(int i = 0; i < locked_infos.length; i++)
		{
			ti = locked_infos[i];
			if(all_locked_ids.contains(ti.getThreadId()))
				continue;

			all_locked_ids.add(ti.getThreadId());

			text1 += ti.toString();
			count++;

			final LockInfo[] locks = ti.getLockedSynchronizers();
			final MonitorInfo[] monitors = ti.getLockedMonitors();
			if(locks.length == 0 && monitors.length == 0)
				continue;

			text2 += "Thread " + ti.getThreadName() + " is waiting to lock " + ti.getLockInfo().getClassName() + " which is held by Thread " + ti.getLockOwnerName() + "\n";
			while ((ti = mbean.getThreadInfo(new long[] { ti.getLockOwnerId() }, true, true)[0]).getThreadId() != ti.getThreadId())
				text2 += "Thread " + ti.getThreadName() + " is waiting to lock " + ti.getLockInfo().getClassName() + " which is held by Thread " + ti.getLockOwnerName() + "\n";
		}

		if(text1.length() > 1)
		{
			System.out.println("Deadlocked Thread:");
			System.out.println("------------------");
			System.out.println(text1);

			Log.addDev(text1, time + "_deadlockThread_" + count, false);
			Log.addDev("", time + "_deadlockThread_" + count, false);
			Log.addDev(StatsUtil.getFullThreadStats(), time + "_deadlockThread_" + count, false);
		}

		if(text2.length() > 1)
		{
			System.out.println("Java-level deadlock:");
			System.out.println("------------------");
			System.out.println(text2);

			Log.addDev(text2, time + "_deadlockJavaLevel", false);
		}

		if(count > 0 && Config.DEADLOCKFOUND_RESTART)
		{
			_log.info("DeadLockDetector - Auto Restart");
			Announcements.announceToAll("Server has stability issues - restarting now.");
			Shutdown.getInstance().startTelnetShutdown("DeadLockDetector - Auto Restart", 10, true);
		}
	}

	private long[] findDeadlockedThreadIDs()
	{
		return mbean.findDeadlockedThreads();
	}
}
