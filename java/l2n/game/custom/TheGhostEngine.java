package l2n.game.custom;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.idfactory.IdFactory;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2FakeMonsterInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.ExCaptureOrc;
import l2n.game.network.serverpackets.ExFuckingHacksPacket;
import l2n.game.network.serverpackets.ExRequestHackShield;
import l2n.game.tables.GmListTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.logging.Logger;

public class TheGhostEngine
{
	private static final Logger _log = Logger.getLogger(TheGhostEngine.class.getName());

	private final static GArray<ThirdPartySoftwareAbuser> _allAbusers = new GArray<ThirdPartySoftwareAbuser>();

	public final static void onSpawnFakeMonsterForPlayer(final L2Player activeChar, final L2NpcTemplate aTemplate)
	{
		L2FakeMonsterInstance aFakeMonster = new L2FakeMonsterInstance(IdFactory.getInstance().getNextId(), aTemplate);

		aFakeMonster.setCurrentHpMp(aFakeMonster.getMaxHp(), aFakeMonster.getMaxMp(), true);
		aFakeMonster.setOwner(activeChar);
		aFakeMonster.setCollisionHeight(-20);
		aFakeMonster.setCollisionRadius(-20);
		aFakeMonster.decayMe();
		aFakeMonster.setXYZ(activeChar.getX() - 50, activeChar.getY() + 50, activeChar.getZ() - 200);
		aFakeMonster.spawnMe();

		activeChar.setLastBotTrackerCheck(System.currentTimeMillis());

		if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
			GmListTable.broadcastMessageToGMs("A fake monster (" + aFakeMonster.getName() + ") has been spawned for " + activeChar.getName() + ".");

		L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleFakeMonsterRemoval(aFakeMonster), Config.GHOST_ENGINE_INVISIBLE_NPC_EXPIRATION_TIME);
	}

	public final static void onSpawnWonderfulRabbit(final L2Player activeChar)
	{
		final L2NpcTemplate aTemplate = NpcTable.getTemplate(Config.GHOST_ENGINE_RABBIT_TEMPLATE_ID);
		if(aTemplate == null)
		{
			_log.warning("TheGhostEngine: Rabbit Template could not be found!!!");
			return;
		}

		final L2MonsterInstance aFakeRabbit = new L2MonsterInstance(IdFactory.getInstance().getNextId(), aTemplate);

		aFakeRabbit.setInvul(true);
		aFakeRabbit.setCurrentHpMp(aFakeRabbit.getMaxHp(), aFakeRabbit.getMaxMp(), true);
		aFakeRabbit.decayMe();
		aFakeRabbit.setXYZ(activeChar.getX(), activeChar.getY(), activeChar.getZ());
		aFakeRabbit.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, activeChar, Config.FOLLOW_RANGE);
		aFakeRabbit.spawnMe();
		aFakeRabbit.setRunning();

		activeChar.sendPacket(new ExRequestHackShield());

		L2GameThreadPools.getInstance().scheduleGeneral(new SchedulWonderfulRabbitRemoval(aFakeRabbit), Rnd.get(10000, 30000));
	}

	public final static void onFloodSuperUberAntiHacksPackets(final L2Player activeChar, final int sendIn)
	{
		if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
			GmListTable.broadcastMessageToGMs("TheGhostEngine: Sending and Scheduling HackShield Packets Rain on " + activeChar.getName() + ".");

		activeChar.sendPacket(new ExRequestHackShield());
		activeChar.sendPacket(new ExFuckingHacksPacket());

		L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleSuperAntiHacksPacketsRain(activeChar), sendIn);
	}

	public final static void onScheduleRabbitPackets(final L2Player activeChar, final int sendIn)
	{
		onFloodSuperUberAntiHacksPackets(activeChar, sendIn);

	}


	public final static void onScheduleAbusesPunishments()
	{
		L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleAbusesPunishments(), Config.GHOST_ENGINE_DEVELOPMENT_MODE ? 300000 : 21600000);
	}

	public final static void onAbusersPunishment()
	{
		final int allBans = _allAbusers.size();

		_log.info("TheGhostEngine: Rabbits caught and banned " + allBans + " abusers over the past 6 hours.");


		GmListTable.broadcastMessageToGMs("Rabbits says that " + _allAbusers.size() + " player(s) has been found using third party software over the past 6hours!");

		if(!_allAbusers.isEmpty())
		{
			for(ThirdPartySoftwareAbuser abuser : _allAbusers)
			{
				_log.info("TheGhostEngine: " + abuser.getName() + " has been banned.");

				L2Player player = abuser.getPlayer();
				if(player != null && player.isOnline())
				{
					player.setAccessLevel(-100);
					LSConnection.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), -100, "AutoBan - TheGhostEngine - Bot[0]", -1));
					player.logout(false, false, true, true);
				}
				else
					LSConnection.getInstance().sendPacket(new ChangeAccessLevel(abuser.getAccountName(), -100, "AutoBan - TheGhostEngine - Bot[1]", -1));

				if(Config.GHOST_ENGINE_LOG_BOTS_DETECTIONS_AND_PUNISHMENTS)
					Log.add("Character: " + abuser.getName() + ", Account: " + abuser.getAccountName() + " has been banned.", "TheGhostEngineBans");
			}


			_allAbusers.clear();
		}


		onScheduleAbusesPunishments();
	}

	public final static void onLogin(final L2Player activeChar)
	{

		onScheduleRabbitPackets(activeChar, Config.GHOST_ENGINE_DEVELOPMENT_MODE ? 30000 : Rnd.get(900000, 2700000));

		if(_allAbusers.isEmpty())
			return;

		for(ThirdPartySoftwareAbuser p : _allAbusers)
			if(activeChar.getName().equals(p.getName()))
			{
				if(Config.GHOST_ENGINE_DEVELOPMENT_MODE)
					GmListTable.broadcastMessageToGMs("Re-adding detected player (" + activeChar.getName() + ").");


				activeChar.setCaughtByRabbits(true);


				_allAbusers.remove(p);
				ThirdPartySoftwareAbuser abuser = new TheGhostEngine().new ThirdPartySoftwareAbuser(activeChar);
				_allAbusers.add(abuser);
			}
	}

	public static void addThirdPartySoftwareAbuser(final L2Player o, final String s)
	{
		ThirdPartySoftwareAbuser abuser = new TheGhostEngine().new ThirdPartySoftwareAbuser(o);
		_allAbusers.add(abuser);
	}

	public static final class ScheduleAbusesPunishments implements Runnable
	{
		@Override
		public final void run()
		{
			onAbusersPunishment();
		}
	}

	private static final class SchedulWonderfulRabbitRemoval implements Runnable
	{
		private final long _activeFakeStoredId;

		SchedulWonderfulRabbitRemoval(final L2MonsterInstance activeFake)
		{
			_activeFakeStoredId = activeFake.getStoredId();
		}

		@Override
		public final void run()
		{
			L2MonsterInstance fake;
			if((fake = L2ObjectsStorage.getAsMonster(_activeFakeStoredId)) != null)
				fake.deleteMe();
		}
	}

	public static final class ScheduleRabbitPackets implements Runnable
	{
		private final long _playerStoredId;

		public ScheduleRabbitPackets(final L2Player activeChar)
		{
			_playerStoredId = activeChar.getStoredId();
		}

		@Override
		public final void run()
		{
			L2Player activeChar;
			if((activeChar = L2ObjectsStorage.getAsPlayer(_playerStoredId)) != null && activeChar.isOnline())
				if(activeChar.isTeleporting())
					L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleRabbitPackets(activeChar), 30000);
				else
				{
					activeChar.sendPacket(new ExCaptureOrc());
					L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleRabbitPacketsCheck(activeChar), 30000);
				}
		}
	}

	private static final class ScheduleRabbitPacketsCheck implements Runnable
	{
		private final long _playerStoredId;

		ScheduleRabbitPacketsCheck(final L2Player activeChar)
		{
			_playerStoredId = activeChar.getStoredId();
		}

		@Override
		public final void run()
		{
			L2Player activeChar;
			if((activeChar = L2ObjectsStorage.getAsPlayer(_playerStoredId)) != null && activeChar.isOnline())
			{

				if(activeChar.getLastRabbitsOkay() < System.currentTimeMillis() - 300000)
					GmListTable.broadcastMessageToGMs("TGE: " + activeChar.getName() + " is connected with an OOG Bot. [KEY: FRP]");

				onScheduleRabbitPackets(activeChar, Config.GHOST_ENGINE_DEVELOPMENT_MODE ? 30000 : Rnd.get(900000, 2700000));
			}
		}
	}

	private static final class ScheduleFakeMonsterRemoval implements Runnable
	{
		private final long _fakeMonsterStoredId;

		ScheduleFakeMonsterRemoval(final L2MonsterInstance fakeMonster)
		{
			_fakeMonsterStoredId = fakeMonster.getStoredId();
		}

		@Override
		public final void run()
		{
			L2MonsterInstance fakeMonster;
			if((fakeMonster = L2ObjectsStorage.getAsMonster(_fakeMonsterStoredId)) != null)
				fakeMonster.deleteMe();
		}
	}

	private static final class ScheduleSuperAntiHacksPacketsRain implements Runnable
	{
		private final long _playerStoredId;

		ScheduleSuperAntiHacksPacketsRain(final L2Player activeChar)
		{
			_playerStoredId = activeChar.getStoredId();
		}

		@Override
		public final void run()
		{
			L2Player activeChar;
			if((activeChar = L2ObjectsStorage.getAsPlayer(_playerStoredId)) != null)
			{
				activeChar.sendPacket(new ExRequestHackShield());
				for(int i = 10; i-- > 0;)
					activeChar.sendPacket(new ExFuckingHacksPacket());
			}
		}
	}

	private final class ThirdPartySoftwareAbuser
	{
		private final long _playerStoredId;
		private final String _name;
		private final String _accountName;

		public ThirdPartySoftwareAbuser(L2Player player)
		{
			_playerStoredId = player.getStoredId();
			_name = player.getName();
			_accountName = player.getAccountName();
		}

		public final L2Player getPlayer()
		{
			L2Player player;
			if((player = L2ObjectsStorage.getAsPlayer(_playerStoredId)) != null)
				return player;
			return null;
		}

		public final String getName()
		{
			return _name;
		}

		public final String getAccountName()
		{
			return _accountName;
		}
	}
}
