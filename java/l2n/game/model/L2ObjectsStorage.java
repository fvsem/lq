package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.commons.text.StrTable;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2PetInstance;

public class L2ObjectsStorage
{
	private static final int STORAGE_PLAYERS = 0x00;
	private static final int STORAGE_SUMMONS = 0x01;
	private static final int STORAGE_NPCS = 0x02;
	private static final int STORAGE_ITEMS = 0x03;
	/** ......................................... */
	private static final int STORAGE_OTHER = 0x1E;
	private static final int STORAGE_NONE = 0x1F;

	@SuppressWarnings("rawtypes")
	private static final L2ObjectArray[] storages = new L2ObjectArray[STORAGE_NONE];

	private static long offline_refresh = 0;
	private static int offline_count = 0;

	static
	{
		storages[STORAGE_PLAYERS] = new L2ObjectArray<L2Player>("PLAYERS", Config.MAXIMUM_ONLINE_USERS, 100);
		storages[STORAGE_SUMMONS] = new L2ObjectArray<L2Playable>("SUMMONS", Config.MAXIMUM_ONLINE_USERS, 100);
		storages[STORAGE_NPCS] = new L2ObjectArray<L2NpcInstance>("NPCS", 60000, 5000);
		storages[STORAGE_ITEMS] = new L2ObjectArray<L2ItemInstance>("ITEMS", Config.MAXIMUM_ONLINE_USERS * Config.INVENTORY_MAXIMUM_NO_DWARF / 3, Config.MAXIMUM_ONLINE_USERS);
		storages[STORAGE_OTHER] = new L2ObjectArray<L2Object>("OTHER", 2000, 1000);
	}

	@SuppressWarnings("unchecked")
	public static L2ObjectArray<L2Player> getStoragePlayers()
	{
		return storages[STORAGE_PLAYERS];
	}

	@SuppressWarnings("unchecked")
	public static L2ObjectArray<L2Playable> getStorageSummons()
	{
		return storages[STORAGE_SUMMONS];
	}

	@SuppressWarnings("unchecked")
	public static L2ObjectArray<L2NpcInstance> getStorageNpcs()
	{
		return storages[STORAGE_NPCS];
	}

	@SuppressWarnings("unchecked")
	public static L2ObjectArray<L2ItemInstance> getStorageItems()
	{
		return storages[STORAGE_ITEMS];
	}

	@SuppressWarnings({ "unchecked" })
	public static L2ObjectArray<L2Object> getStorageOther()
	{
		return storages[STORAGE_OTHER];
	}

	private static int selectStorageID(final L2Object o)
	{
		if(o.isItem())
			return STORAGE_ITEMS;
		if(o.isNpc())
			return STORAGE_NPCS;
		if(o.isPlayable())
			return o.isPlayer() ? STORAGE_PLAYERS : STORAGE_SUMMONS;
		return STORAGE_OTHER;
	}

	public static L2Object get(final long storedId)
	{
		int STORAGE_ID;
		if(storedId == 0 || (STORAGE_ID = getStorageID(storedId)) == STORAGE_NONE)
			return null;
		final L2Object result = storages[STORAGE_ID].get(getStoredIndex(storedId));
		return result != null && result.getObjectId() == getStoredObjectId(storedId) ? result : null;
	}

	public static L2Object get(final Long storedId)
	{
		int STORAGE_ID;
		if(storedId == null || storedId == 0 || (STORAGE_ID = getStorageID(storedId)) == STORAGE_NONE)
			return null;
		final L2Object result = storages[STORAGE_ID].get(getStoredIndex(storedId));
		return result != null && result.getObjectId() == getStoredObjectId(storedId) ? result : null;
	}

	public static boolean isStored(final long storedId)
	{
		int STORAGE_ID;
		if(storedId == 0 || (STORAGE_ID = getStorageID(storedId)) == STORAGE_NONE)
			return false;
		final L2Object o = storages[STORAGE_ID].get(getStoredIndex(storedId));
		return o != null && o.getObjectId() == getStoredObjectId(storedId);
	}

	public static L2NpcInstance getAsNpc(final long storedId)
	{
		return (L2NpcInstance) get(storedId);
	}

	public static L2NpcInstance getAsNpc(final Long storedId)
	{
		return (L2NpcInstance) get(storedId);
	}

	public static L2Player getAsPlayer(final long storedId)
	{
		return (L2Player) get(storedId);
	}

	public static L2Playable getAsPlayable(final long storedId)
	{
		return (L2Playable) get(storedId);
	}

	public static L2Character getAsCharacter(final long storedId)
	{
		return (L2Character) get(storedId);
	}

	public static L2MonsterInstance getAsMonster(final long storedId)
	{
		return (L2MonsterInstance) get(storedId);
	}

	public static L2PetInstance getAsPet(final long storedId)
	{
		return (L2PetInstance) get(storedId);
	}

	public static L2ItemInstance getAsItem(final long storedId)
	{
		return (L2ItemInstance) get(storedId);
	}

	public static boolean contains(final long storedId)
	{
		return get(storedId) != null;
	}

	public static final void forEachPlayer(final INgVoidProcedure<L2Player> procedure)
	{
		getStoragePlayers().forEach(procedure);
	}

	public static L2Player getPlayer(final String name)
	{
		return getStoragePlayers().findByName(name);
	}

	public static L2Player getPlayer(final int objId)
	{
		return getStoragePlayers().findByObjectId(objId);
	}

	public static GArray<L2Player> getAllPlayers()
	{
		return getStoragePlayers().getAll();
	}

	public static Iterable<L2Player> getAllPlayersForIterate()
	{
		return getStoragePlayers();
	}

	public static int getAllPlayersCount()
	{
		return getStoragePlayers().getRealSize();
	}

	public static int getAllObjectsCount()
	{
		int result = 0;
		for(final L2ObjectArray<?> storage : storages)
			if(storage != null)
				result += storage.getRealSize();
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static GArray<L2Object> getAllObjects()
	{
		final GArray<L2Object> result = new GArray<L2Object>(getAllObjectsCount());
		for(final L2ObjectArray storage : storages)
			if(storage != null)
				storage.getAll(result);
		return result;
	}

	public static L2Object findObject(final int objId)
	{
		L2Object result;
		for(final L2ObjectArray<?> storage : storages)
			if(storage != null && (result = storage.findByObjectId(objId)) != null)
				return result;
		return null;
	}

	public static int getAllOfflineCount()
	{
		if(!Config.SERVICES_OFFLINE_TRADE_ALLOW)
			return 0;

		final long now = System.currentTimeMillis();
		if(now > offline_refresh)
		{
			offline_refresh = now + 10000;
			offline_count = 0;
			for(final L2Player player : getAllPlayersForIterate())
				if(player.isInOfflineMode())
					offline_count++;
		}
		return offline_count;
	}

	public static GArray<Reflection> getAllReflections()
	{
		final GArray<Reflection> result = new GArray<Reflection>();
		for(final L2Object obj : getStorageOther())
			if(obj instanceof Reflection)
				result.add((Reflection) obj);
		return result;
	}

	public static GArray<L2NpcInstance> getAllNpcs()
	{
		return getStorageNpcs().getAll();
	}

	public static Iterable<L2NpcInstance> getAllNpcsForIterate()
	{
		return getStorageNpcs();
	}

	public static L2ItemInstance getItemByObjId(final int objId)
	{
		return getStorageItems().findByObjectId(objId);
	}

	public static L2NpcInstance getByNpcId(final int npc_id)
	{
		L2NpcInstance result = null;
		for(final L2NpcInstance temp : getStorageNpcs())
			if(npc_id == temp.getNpcId())
			{
				if(!temp.isDead())
					return temp;
				result = temp;
			}
		return result;
	}

	public static GArray<L2NpcInstance> getAllByNpcId(final int npc_id, final boolean justAlive)
	{
		final GArray<L2NpcInstance> result = new GArray<L2NpcInstance>(0);
		for(final L2NpcInstance temp : getStorageNpcs())
			if(temp.getTemplate() != null && npc_id == temp.getTemplate().getNpcId() && (!justAlive || !temp.isDead()))
				result.add(temp);
		return result;
	}

	public static GArray<L2NpcInstance> getAllByNpcId(final int[] npc_ids, final boolean justAlive)
	{
		final GArray<L2NpcInstance> result = new GArray<L2NpcInstance>(0);
		for(final L2NpcInstance temp : getStorageNpcs())
			if(!justAlive || !temp.isDead())
				for(final int npc_id : npc_ids)
					if(npc_id == temp.getNpcId())
						result.add(temp);
		return result;
	}

	public static L2NpcInstance getNpc(final String name)
	{
		final GArray<L2NpcInstance> npcs = getStorageNpcs().findAllByName(name);
		if(npcs.size() == 0)
			return null;
		for(final L2NpcInstance temp : npcs)
			if(!temp.isDead())
				return temp;
		if(npcs.size() > 0)
			return npcs.remove(npcs.size() - 1);
		return null;
	}

	public static L2NpcInstance getNpc(final int objId)
	{
		return getStorageNpcs().findByObjectId(objId);
	}

	@SuppressWarnings("unchecked")
	public static long put(final L2Object o)
	{
		final int STORAGE_ID = selectStorageID(o);
		return o.getObjectId() & 0xFFFFFFFFL | (STORAGE_ID & 0x1FL) << 32 | (storages[STORAGE_ID].add(o) & 0xFFFFFFFFL) << 37;
	}

	public static long putDummy(final L2Object o)
	{
		return objIdNoStore(o.getObjectId());
	}

	public static long objIdNoStore(final int objId)
	{
		return objId & 0xFFFFFFFFL | (STORAGE_NONE & 0x1FL) << 32;
	}

	public static long refreshId(final L2Object o)
	{
		return o.getObjectId() & 0xFFFFFFFFL | o.getStoredId() >> 32 << 32;
	}

	public static L2Object remove(final long storedId)
	{
		final int STORAGE_ID = getStorageID(storedId);
		return STORAGE_ID == STORAGE_NONE ? null : storages[STORAGE_ID].remove(getStoredIndex(storedId), getStoredObjectId(storedId));
	}

	private static int getStorageID(final long storedId)
	{
		return (int) (storedId >> 32) & 0x1F;
	}

	private static int getStoredIndex(final long storedId)
	{
		return (int) (storedId >> 37);
	}

	public static int getStoredObjectId(final long storedId)
	{
		return (int) storedId;
	}

	public static StrTable getStats()
	{
		final StrTable table = new StrTable("L2 Objects Storage Stats");

		L2ObjectArray<?> storage;
		for(int i = 0; i < storages.length; i++)
		{
			if((storage = storages[i]) == null)
				continue;
			synchronized (storage)
			{
				table.set(i, "Name", storage.name);
				table.set(i, "Size / Real", storage.size() + " / " + storage.getRealSize());
				table.set(i, "Capacity / init", storage.capacity() + " / " + storage.initCapacity);
			}
		}

		return table;
	}
}
