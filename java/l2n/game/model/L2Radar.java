package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.RadarControl;
import l2n.util.Location;

public final class L2Radar
{
	private final L2Player _player;
	private final GArray<RadarMarker> _markers;

	public L2Radar(final L2Player player)
	{
		_player = player;
		_markers = new GArray<RadarMarker>();
	}

	// Add a marker to player's radar
	public void addMarker(final int x, final int y, final int z)
	{
		final RadarMarker newMarker = new RadarMarker(x, y, z);
		_markers.add(newMarker);
		_player.sendPacket(new RadarControl(2, 2, newMarker));
		_player.sendPacket(new RadarControl(0, 1, newMarker));
	}

	// Remove a marker from player's radar
	public void removeMarker(final int x, final int y, final int z)
	{
		final RadarMarker newMarker = new RadarMarker(x, y, z);
		_markers.remove(newMarker);
		_player.sendPacket(new RadarControl(1, 1, newMarker));
	}

	public void removeAllMarkers()
	{
		for(final RadarMarker tempMarker : _markers)
			_player.sendPacket(new RadarControl(2, 2, tempMarker));
		_markers.clear();
	}

	public void loadMarkers()
	{
		_player.sendPacket(new RadarControl(2, 2, _player.getX(), _player.getY(), _player.getZ()));
		for(final RadarMarker tempMarker : _markers)
			_player.sendPacket(new RadarControl(0, 1, tempMarker));
	}

	public static class RadarMarker extends Location
	{
		// Simple class to model radar points.
		public int type;

		public RadarMarker(final int type_, final int x_, final int y_, final int z_)
		{
			super(x_, y_, z_);
			type = type_;
		}

		public RadarMarker(final int x_, final int y_, final int z_)
		{
			super(x_, y_, z_);
			type = 1;
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + type;
			result = prime * result + x;
			result = prime * result + y;
			result = prime * result + z;
			return result;
		}

		@Override
		public boolean equals(final Object obj)
		{
			if(this == obj)
				return true;
			if(obj == null)
				return false;
			if(!(obj instanceof RadarMarker))
				return false;
			final RadarMarker other = (RadarMarker) obj;
			if(type != other.type)
				return false;
			if(x != other.x)
				return false;
			if(y != other.y)
				return false;
			if(z != other.z)
				return false;
			return true;
		}
	}
}
