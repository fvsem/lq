package l2n.game.model.base;

public class ItemToDrop
{
	public int itemId;
	public long count;
	public boolean isSpoil;
	public boolean isAdena;

	public ItemToDrop(int id)
	{
		itemId = id;
	}

	@Override
	public String toString()
	{
		return itemId + "-" + count;
	}
}
