package l2n.game.model.base;

import l2n.game.model.actor.L2Player;
import l2n.game.tables.SkillTable;

import static l2n.game.tables.EnchantChanceSkillsTable._chance15;
import static l2n.game.tables.EnchantChanceSkillsTable._chance30;

public final class L2EnchantSkillLearn
{
	private final int _id;
	private final int _level;
	private final String _name;
	private final int _baseLvl;
	private final int _maxLvl;
	private final int _minSkillLevel;
	private int _costMul;

	public static final int[] ench_sp = { 0,
			306000, // 1 lvl
			315000, // 2 lvl
			325000, // 3 lvl
			346000, // 4 lvl
			357000, // 5 lvl
			368000, // 6 lvl
			390000, // 7 lvl
			402000, // 8 lvl
			414000, // 9 lvl
			507000, // 10 lvl
			523000, // 11 lvl
			538000, // 12 lvl
			659000, // 13 lvl
			680000, // 14 lvl
			699000, // 15 lvl
			857000, // 16 lvl
			884000, // 17 lvl
			909000, // 18 lvl
			1114000, // 19 lvl
			1149000, // 20 lvl
			1182000, // 21 lvl
			1448000, // 22 lvl
			1494000, // 23 lvl
			1537000, // 24 lvl
			1882000, // 25 lvl
			1942000, // 26 lvl
			1998000, // 27 lvl
			2447000, // 28 lvl
			2525000, // 29 lvl
			2597000 // 30 lvl
	};
	public static final byte[] elevels30 = { 0, 76, 76, 76, 77, 77, 77, 78, 78, 78, 79, 79, 79, 80, 80, 80, 81, 81, 81, 82, 82, 82, 83, 83, 83, 84, 84, 84, 85, 85, 85 };
	public static final byte[] elevels15 = { 0, 81, 81, 81, 82, 82, 82, 83, 83, 83, 84, 84, 84, 85, 85, 85 };

	private static final int[][] _priceBuff = {
			new int[0],
			{ 74250, 503980 },
			{ 74250, 503980 },
			{ 74250, 503980 },
			{ 112050, 528970 },
			{ 112050, 528970 },
			{ 112050, 528970 },
			{ 150300, 554700 },
			{ 150300, 554700 },
			{ 150300, 554700 },
			{ 189000, 595020 },
			{ 189000, 595020 },
			{ 189000, 595020 },
			{ 228150, 622095 },
			{ 228150, 622095 },
			{ 228150, 622095 },
			{ 267750, 666350 },
			{ 267750, 666350 },
			{ 267750, 666350 },
			{ 307800, 696405 },
			{ 307800, 696405 },
			{ 307800, 696405 },
			{ 348300, 743165 },
			{ 348300, 743165 },
			{ 348300, 743165 },
			{ 389250, 775470 },
			{ 389250, 775470 },
			{ 389250, 775470 },
			{ 435000, 809180 },
			{ 435000, 809180 },
			{ 435000, 809180 } };

	private static final int[][] _priceCombat = {
			new int[0],
			{ 133650, 907164 },
			{ 133650, 907164 },
			{ 133650, 907164 },
			{ 201690, 952146 },
			{ 201690, 952146 },
			{ 201690, 952146 },
			{ 270540, 998586 },
			{ 270540, 998586 },
			{ 270540, 998586 },
			{ 340200, 1071036 },
			{ 340200, 1071036 },
			{ 340200, 1071036 },
			{ 410670, 1281402 },
			{ 410670, 1281402 },
			{ 410670, 1281402 },
			{ 481950, 1370772 },
			{ 481950, 1370772 },
			{ 481950, 1370772 },
			{ 554040, 1432602 },
			{ 554040, 1432602 },
			{ 554040, 1432602 },
			{ 626940, 1528794 },
			{ 626940, 1528794 },
			{ 626940, 1528794 },
			{ 709430, 1631444 },
			{ 709430, 1631444 },
			{ 709430, 1631444 },
			{ 802770, 1740986 },
			{ 802770, 1740986 },
			{ 802770, 1740986 } };

	public L2EnchantSkillLearn(int id, int lvl, String name, int minSkillLvl, int baseLvl, int maxLvl)
	{
		_id = id;
		_level = lvl;
		_baseLvl = baseLvl;
		_maxLvl = maxLvl;
		_minSkillLevel = minSkillLvl;
		_name = name.intern();
		_costMul = _maxLvl == 15 ? 5 : 1;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId()
	{
		return _id;
	}

	public int getLevel()
	{
		return _level;
	}

	/**
	 * @return Returns the minLevel.
	 */
	public int getBaseLevel()
	{
		return _baseLvl;
	}

	public int getMinSkillLevel()
	{
		return _minSkillLevel;
	}

	public String getName()
	{
		return _name;
	}

	public int getCostMult()
	{
		return _costMul;
	}

	public int[] getCost()
	{
		return SkillTable.getInstance().getInfo(_id, 1).isOffensive() ? _priceCombat[_level % 100] : _priceBuff[_level % 100];
	}

	/**
	 * Шанс успешной заточки
	 */
	public int getRate(L2Player ply)
	{
		int level = _level % 100 - 1;
		int chance = Math.min(_chance30[level].length - 1, ply.getLevel() - 76);
		return _maxLvl == 15 ? _chance15[level][chance] : _chance30[level][chance];
	}

	public int getMaxLevel()
	{
		return _maxLvl;
	}

	@Override
	public int hashCode()
	{
		int result = 1;
		result = 31 * result + _id;
		result = 31 * result + _level;
		return result;
	}
}
