package l2n.game.model.base;

/**
 * Клас для переменных игроков
 * 
 * @author L2System Project
 */
public final class CharVariables
{
	public enum CharVariable
	{
		lost_exp("lostexp"),
		recom_chars("recomChars"),
		expand_inventory("ExpandInventory"),
		expand_warehouse("ExpandWarehouse"),
		store_mode("storemode"),
		offline("offline"),
		reflection("reflection"),
		back_coords("backCoords"),
		jailed("jailed"),
		jailed_from("jailedFrom"),
		wyvern_moneyback("wyvern_moneyback"),

		lang("lang@"),
		autoloot("@autoloot"),
		title_color("titleColor"),
		name_color("namecolor"),

		// trade vars
		sell_list("selllist"),
		sell_store_name("sellstorename"),
		buy_list("buylist"),
		buy_store_name("buystorename"),
		create_list("createlist"),
		manufacture_name("manufacturename"),

		// GM vars
		gm_gmspeed("gm_gmspeed"),
		gm_silence("gm_silence"),
		gm_invul("gm_invul");

		private final String var;

		private CharVariable(final String var)
		{
			this.var = var;
		}

		public final String getName()
		{
			return var;
		}

		@Override
		public final String toString()
		{
			return var;
		}
	}

	private final String _type;
	private final String _name;
	private final String _value;
	private final long _expire_time;

	public CharVariables(final String type, final String name, final String value, final long expire_time)
	{
		_type = type;
		_name = name;
		_value = value;
		_expire_time = expire_time;
	}

	/** Имя инстанса */
	public String getName()
	{
		return _name;
	}

	public long getExpireTime()
	{
		return _expire_time;
	}

	/** @return true если время действия переменной истекло */
	public boolean isExpired()
	{
		return System.currentTimeMillis() / 1000 >= _expire_time && isTimeLimited();
	}

	/** @return true если переменная с ограничением по времени */
	public boolean isTimeLimited()
	{
		return _expire_time > 0;
	}

	public String getType()
	{
		return _type;
	}

	public String getValue()
	{
		return _value;
	}
}
