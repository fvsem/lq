package l2n.game.model.base;

/**
 * This class defines all races (human, elf, darkelf, orc, dwarf) that a player can chose.
 */
public enum Race
{
	human,
	elf,
	darkelf,
	orc,
	dwarf,
	kamael;

	public final static Race getRaceByName(String name)
	{
		for(Race race : Race.values())
		{
			if(race.name().equals(name))
				return race;
		}

		return Race.human;
	}
}
