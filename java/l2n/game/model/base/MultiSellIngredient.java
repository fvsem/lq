package l2n.game.model.base;

import l2n.game.tables.ItemTable;

public class MultiSellIngredient implements Cloneable
{
	private int _itemId;
	private long _itemCount;
	private int _itemEnchant;
	private int _itemElement;
	private int _itemElementValue;

	public MultiSellIngredient(int itemId, long itemCount, int itemEnchant, int element, int elementValue)
	{
		_itemId = itemId;
		_itemCount = itemCount;
		_itemEnchant = itemEnchant;
		_itemElement = element;
		_itemElementValue = elementValue;
	}

	public MultiSellIngredient(int itemId, long itemCount, int itemEnchant)
	{
		this(itemId, itemCount, itemEnchant, -2, 0);
	}

	public MultiSellIngredient(int itemId, long itemCount)
	{
		this(itemId, itemCount, 0, -2, 0);
	}

	@Override
	public MultiSellIngredient clone()
	{
		return new MultiSellIngredient(_itemId, _itemCount, _itemEnchant, _itemElement, _itemElementValue);
	}

	/**
	 * @return Returns the itemId.
	 */
	public int getItemId()
	{
		return _itemId;
	}

	/**
	 * @return Returns the itemCount.
	 */
	public long getItemCount()
	{
		return _itemCount;
	}

	/**
	 * @return Returns the itemEnchant.
	 */
	public int getItemEnchant()
	{
		return _itemEnchant;
	}

	public byte getElement()
	{
		return (byte) _itemElement;
	}

	public int getElementValue()
	{
		return _itemElementValue;
	}

	/**
	 * @param itemCount
	 *            The itemCount to set.
	 */
	public void setItemCount(long itemCount)
	{
		_itemCount = itemCount;
	}

	/**
	 * @param itemId
	 *            The itemId to set.
	 */
	public void setItemId(int itemId)
	{
		_itemId = itemId;
	}

	/**
	 * @param itemEnchant
	 *            The itemEnchant to set.
	 */
	public void setItemEnchant(int itemEnchant)
	{
		_itemEnchant = itemEnchant;
	}

	/**
	 * Returns if item is stackable
	 * 
	 * @return boolean
	 */
	public boolean isStackable()
	{
		return _itemId <= 0 || ItemTable.getInstance().getTemplate(_itemId).isStackable();
	}

	@Override
	public int hashCode()
	{
		final int PRIME = 31;
		int result = 1;
		result = (int) (PRIME * result + _itemCount);
		result = PRIME * result + _itemEnchant;
		result = PRIME * result + _itemElementValue;
		result = PRIME * result + _itemEnchant;
		result = PRIME * result + _itemId;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		MultiSellIngredient other = (MultiSellIngredient) obj;
		if(_itemId != other._itemId)
			return false;
		if(_itemCount != other._itemCount)
			return false;
		if(_itemEnchant != other._itemEnchant)
			return false;
		if(_itemElement != other._itemElement)
			return false;
		return _itemElementValue == other._itemElementValue;
	}

	@Override
	public String toString()
	{
		return _itemId + " - " + _itemCount;
	}
}
