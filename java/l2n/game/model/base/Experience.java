package l2n.game.model.base;

import l2n.Config;

public class Experience
{

	public final static long LEVEL[] = {
			-1L, // level 0 (unreachable)
			0L, // level 1
			68L, // level 2
			363L, // level 3
			1168L, // level 4
			2884L, // level 5
			6038L, // level 6
			11287L, // level 7
			19423L, // level 8
			31378L, // level 9
			48229L, // level 10
			71201L, // level 11
			101676L, // level 12
			141192L, // level 13
			191452L, // level 14
			254327L, // level 15
			331864L, // level 16
			426284L, // level 17
			539995L, // level 18
			675590L, // level 19
			835854L, // level 20
			1023775L, // level 20
			1242536L, // level 21
			1495531L, // level 22
			1786365L, // level 23
			2118860L, // level 24
			2497059L, // level 25
			2925229L, // level 26
			3407873L, // level 27
			3949727L, // level 28
			4555766L, // level 30
			5231213L, // level 31
			5981539L, // level 32
			6812472L, // level 33
			7729999L, // level 34
			8740372L, // level 35
			9850111L, // level 36
			11066012L, // level 37
			12395149L, // level 38
			13844879L, // level 39
			15422851L, // level 40
			17137002L, // level 41
			18995573L, // level 42
			21007103L, // level 43
			23180442L, // level 44
			25524751L, // level 45
			28049509L, // level 46
			30764519L, // level 47
			33679907L, // level 48
			36806133L, // level 49
			40153995L, // level 50
			45524865L, // level 51
			51262204L, // level 52
			57383682L, // level 53
			63907585L, // level 54
			70852742L, // level 55
			80700339L, // level 56
			91162131L, // level 57
			102265326L, // level 58
			114038008L, // level 59
			126509030L, // level 60
			146307211L, // level 61
			167243291L, // level 62
			189363788L, // level 63
			212716741L, // level 64
			237351413L, // level 65
			271973532L, // level 66
			308441375L, // level 67
			346825235L, // level 68
			387197529L, // level 69
			429632402L, // level 70
			474205751L, // level 71
			532692055L, // level 72
			606319094L, // level 73
			696376867L, // level 74
			804219972L, // level 75
			931275828L, // level 76
			1151275834L, // level 77
			1511275834L, // level 78
			2099275834L, // level 79
			4200000000L, // level 80
			6300000000L, // level 81
			8820000000L, // level 82
			11844000000L, // level 83
			15472800000L, // level 84
			19827360000L, // level 85
			25314000000L, // level 86
	};

	/**
	 * Return PenaltyModifier (can use in all cases)
	 * 
	 * @param count
	 *            - how many times <percents> will be substructed
	 * @param percents
	 *            - percents to substruct
	 */
	public static double penaltyModifier(long count, double percents)
	{
		return Math.max(1. - count * percents / 100., 0);
	}

	/**
	 * This is the first UNREACHABLE level.<BR>
	 * ex: If you want a max at 85 & 100.00%, you have to put 86.<BR>
	 * <BR>
	 */
	public final static byte MAX_LEVEL = 86;

	/**
	 * Максимальный достижимый уровень
	 */
	public static int getMaxLevel()
	{
		return Config.ALT_MAX_LEVEL;
	}

	/**
	 * Максимальный уровень для саба
	 */
	public static int getMaxSubLevel()
	{
		return Config.ALT_MAX_SUB_LEVEL;
	}
}
