package l2n.game.model.base;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncAdd;
import l2n.game.tables.AugmentationData;
import l2n.game.tables.AugmentationData.AugStat;
import l2n.game.tables.SkillTable;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used to store an augmentation and its boni
 * 
 * @author L2NextGen (bloodshed)
 */
public final class L2Augmentation
{
	private static final Logger _log = Logger.getLogger(L2Augmentation.class.getName());

	private int _itemObjectId;
	private int _effectsId = 0;
	private AugmentationStatBoni _boni = null;
	private L2Skill _skill = null;
	private boolean _isLoaded = false;

	public boolean isLoaded()
	{
		return _isLoaded;
	}

	public L2Augmentation(L2ItemInstance item, int effects, L2Skill skill, boolean save)
	{
		_itemObjectId = item.getObjectId();
		_effectsId = effects;
		_boni = new AugmentationStatBoni(_effectsId);
		_skill = skill;

		if(save)
			saveAugmentationData();
	}

	public L2Augmentation(L2ItemInstance item, int effects, int skill, int skillLevel, boolean save)
	{
		this(item, effects, SkillTable.getInstance().getInfo(skill, skillLevel), save);
	}

	public class AugmentationStatBoni
	{
		private Stats _stats[];
		private float _values[];
		private boolean _active;

		public AugmentationStatBoni(int augmentationId)
		{
			_active = false;
			GArray<AugStat> as = AugmentationData.getInstance().getAugStatsById(augmentationId);
			if(as == null)
				return;

			_stats = new Stats[as.size()];
			_values = new float[as.size()];

			int i = 0;
			for(AugStat aStat : as)
			{
				_stats[i] = aStat.getStat();
				_values[i] = aStat.getValue();
				i++;
			}

			_isLoaded = true;
		}

		public void applyBoni(L2Player player)
		{
			// make sure the boni are not applyed twice..
			if(_active)
				return;

			for(int i = 0; i < _stats.length; i++)
				player.addStatFunc(new FuncAdd(_stats[i], 0x60, this, _values[i]));

			_active = true;
		}

		public void removeBoni(L2Player player)
		{
			// make sure the boni is not removed twice
			if(!_active)
				return;

			player.removeStatsOwner(this);

			_active = false;
		}

		@Override
		public String toString()
		{
			return "AugmentationStat{id:" + _effectsId + ",objId:" + _itemObjectId + "}";
		}
	}

	private void saveAugmentationData()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("INSERT INTO augmentations (item_id,attributes,skill,level) VALUES (?,?,?,?)");
			statement.setInt(1, _itemObjectId);
			statement.setInt(2, _effectsId);
			if(_skill != null)
			{
				statement.setInt(3, _skill.getId());
				statement.setInt(4, _skill.getLevel());
			}
			else
			{
				statement.setInt(3, 0);
				statement.setInt(4, 0);
			}
			statement.executeUpdate();
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Could not save augmentation for item: " + _itemObjectId + " from DB:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void deleteAugmentationData()
	{
		// delete the augmentation from the database
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM augmentations WHERE item_id=?");
			statement.setInt(1, _itemObjectId);
			statement.executeUpdate();
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Could not delete augmentation for item: " + _itemObjectId + " from DB:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Get the augmentation "id" used in serverpackets.
	 * 
	 * @return augmentationId
	 */
	public int getAugmentationId()
	{
		return _effectsId;
	}

	public L2Skill getSkill()
	{
		return _skill;
	}

	/**
	 * Applys the boni to the player.
	 * 
	 * @param player
	 */
	public void applyBoni(L2Player player)
	{
		if(player.getArmorExpertisePenalty() > 0 || player.getWeaponsExpertisePenalty() > 0)
			return;

		_boni.applyBoni(player);

		// add the skill if any
		if(_skill != null)
		{
			player.addSkill(_skill);
			player.sendPacket(new SkillList(player));
		}
	}

	/**
	 * Removes the augmentation boni from the player.
	 * 
	 * @param player
	 */
	public void removeBoni(L2Player player)
	{
		_boni.removeBoni(player);

		// remove the skill if any
		if(_skill != null)
		{
			player.removeSkill(_skill);
			player.sendPacket(new SkillList(player));
		}
	}

	public void setItem(L2ItemInstance item)
	{
		if(item == null)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("UPDATE augmentations SET item_id=? WHERE item_id=?");
			statement.setInt(1, item.getObjectId());
			statement.setInt(2, _itemObjectId);
			statement.executeUpdate();
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Could not save augmentation for item: " + _itemObjectId + " from DB:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		_itemObjectId = item.getObjectId();
	}
}
