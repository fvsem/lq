package l2n.game.model.base;

import l2n.commons.list.GArray;

public class PlayerAccess
{
	public int PlayerID;
	public boolean IsGM = false;
	public boolean CanSeeInvisible = false;
	public boolean Admin = false;
	public boolean Announcements = false;
	public boolean Ban = false;

	public boolean CanBanChat = false;
	public boolean CanUnBanChat = false;
	public boolean CanChatPenalty = false;
	public int BanChatMaxValue = -1;
	public int BanChatCountPerDay = -1;
	public int BanChatBonusId = -1;
	public int BanChatBonusCount = -1;

	public boolean Cancel = false;
	public boolean ShowServerVersion = false;
	public boolean ClanHall = false;
	public boolean CreateItem = false;
	public boolean CursedWeapons = false;
	public boolean Delete = false;
	public boolean Kick = false;
	public boolean Door = false;
	public boolean EditChar = false;
	public boolean GodMode = false;
	public boolean CanEnchant = false;
	public boolean IsEventGm = false;
	public boolean CanReload = false;
	public boolean HealKillRes = false;
	public boolean MonsterRace = false;
	public boolean Clan = false;
	public boolean CanPolymorph = false;
	public boolean Rider = false;
	public boolean UseGMShop = false;
	public boolean CanRestart = false;
	public boolean Siege = false;
	public boolean Skills = false;
	public boolean Spawn = false;
	public boolean UseGMComand = false;
	public boolean CanTeleport = false;
	public boolean CanEditZone = false;
	public boolean Unblock = false;
	public boolean FastUnstuck = false;
	public boolean ResurectFixed = false;
	public boolean PeaceAttack = false;
	public boolean AllowWalker = false;
	public boolean CanChangeClass = false;
	public boolean BlockInventory = false;
	public boolean DisableReuseSkill = false;
	public boolean UseInventory = true;
	public boolean UseTrade = true;
	public boolean CanAttack = true;
	public boolean CanEvaluate = true;
	public boolean CanJoinParty = true;
	public boolean CanJoinClan = true;
	public boolean UseWarehouse = true;
	public boolean UseShop = true;
	public boolean UseTeleport = true;
	public GArray<Integer> ProhibitedItems = new GArray<Integer>(0);

	public PlayerAccess()
	{}
}
