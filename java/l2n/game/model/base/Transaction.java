package l2n.game.model.base;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.SendTradeDone;
import l2n.util.IllegalPlayerAction;
import l2n.util.Log;
import l2n.util.Util;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * Контейнер для обмена между игроками и транзакций с запросами и ответами.
 * Отвечает за разные типы операций. Создаёт, отменяет и тд.
 * 
 * @author L2System Project
 * @date 07.07.2010
 * @time 18:45:07
 */
public class Transaction
{
	public static enum TransactionType
	{
		NONE,
		PARTY,
		PARTY_ROOM,
		CLAN,
		ALLY,
		TRADE,
		TRADE_REQUEST,
		FRIEND,
		CHANNEL,
		DUEL;
	}

	private final static Logger _log = Logger.getLogger(Transaction.class.getName());

	private final long _player1, _player2, _timeout;
	private final TransactionType _type;
	private ConcurrentLinkedQueue<TradeItem> _exchangeList1, _exchangeList2;
	private byte _confirmed1, _confirmed2;
	/** для запросов на принятие в пати, хранит в себе itemDistribution если нужно создавать новую группу */
	private int _createParty = -1;

	/**
	 * Создает транзакцию с ограничением времени.
	 */
	public Transaction(final TransactionType type, final L2Player player1, final L2Player player2, final long timeout)
	{
		_player1 = player1.getStoredId();
		_player2 = player2.getStoredId();
		_timeout = timeout > 0 ? System.currentTimeMillis() + timeout : -1;
		_type = type;
		player1.setTransaction(this);
		player2.setTransaction(this);
	}

	/**
	 * Создает транзакцию без ограничения времени.
	 */
	public Transaction(final TransactionType type, final L2Player player1, final L2Player player2)
	{
		this(type, player1, player2, 0);
	}

	/**
	 * Заканчивает транзакцию и очищает соответствующее поле у участников.
	 */
	public void cancel()
	{
		L2Player player = L2ObjectsStorage.getAsPlayer(_player1);
		if(player != null && player.getTransaction() == this)
		{
			player.setTransaction(null);
			if(_type == TransactionType.TRADE)
				player.sendPacket(SendTradeDone.Fail);
		}
		player = L2ObjectsStorage.getAsPlayer(_player2);
		if(player != null && player.getTransaction() == this)
		{
			player.setTransaction(null);
			if(_type == TransactionType.TRADE)
				player.sendPacket(SendTradeDone.Fail);
		}
	}

	public boolean isTrade()
	{
		return _type == TransactionType.TRADE || _type == TransactionType.TRADE_REQUEST;
	}

	/**
	 * Проверяет, участвует ли игрок в этой транзакции.
	 */
	public boolean isParticipant(final L2Player player)
	{
		if(player.getStoredId() == _player1 || player.getStoredId() == _player2)
			return true;
		return false;
	}

	/**
	 * Проверяет не просрочена ли транзакция. Если просрочена - отменяет ее.
	 */
	public boolean isInProgress()
	{
		if(_timeout < 0)
			return true;
		if(_timeout > System.currentTimeMillis())
			return true;
		cancel();
		return false;
	}

	/**
	 * Проверяет тип транзакции.
	 */
	public boolean isTypeOf(final TransactionType type)
	{
		return _type == type;
	}

	/**
	 * Помечает участника как согласившегося.
	 */
	public void confirm(final L2Player player)
	{
		if(player.getStoredId() == _player1)
			_confirmed1 = 1;
		else if(player.getStoredId() == _player2)
			_confirmed2 = 1;
	}

	/**
	 * Проверяет согласились ли оба игрока на транзакцию.
	 */
	public boolean isConfirmed(final L2Player player)
	{
		if(player.getStoredId() == _player1)
			return _confirmed1 == 1;
		if(player.getStoredId() == _player2)
			return _confirmed2 == 1;
		return false; // WTF???
	}

	public int getCreateNewParty()
	{
		return _createParty;
	}

	public void setCreateNewParty(final int val)
	{
		_createParty = val;
	}

	/**
	 * Проверяет, оба ли игрока в игре и не сброшена ли у одного из них транзакция.
	 */
	public boolean isValid()
	{
		L2Player player = L2ObjectsStorage.getAsPlayer(_player1);
		if(player == null || player.getTransaction() != this)
			return false;
		player = L2ObjectsStorage.getAsPlayer(_player2);
		if(player == null || player.getTransaction() != this)
			return false;
		return true;
	}

	public L2Player getOtherPlayer(final L2Player player)
	{
		if(player.getStoredId() == _player1)
			return L2ObjectsStorage.getAsPlayer(_player2);
		if(player.getStoredId() == _player2)
			return L2ObjectsStorage.getAsPlayer(_player1);
		return null;
	}

	public ConcurrentLinkedQueue<TradeItem> getExchangeList(final L2Player player)
	{
		if(player.getStoredId() == _player1)
		{
			if(_exchangeList1 == null)
				_exchangeList1 = new ConcurrentLinkedQueue<TradeItem>();
			return _exchangeList1;
		}
		else if(player.getStoredId() == _player2)
		{
			if(_exchangeList2 == null)
				_exchangeList2 = new ConcurrentLinkedQueue<TradeItem>();
			return _exchangeList2;
		}

		return null; // WTF?
	}

	/**
	 * Производит обмен между игроками. Списки надо предварительно проверить на валидность.
	 */
	public void tradeItems()
	{
		final L2Player player1 = L2ObjectsStorage.getAsPlayer(_player1);
		final L2Player player2 = L2ObjectsStorage.getAsPlayer(_player2);

		if(player1 == null || player2 == null)
			return;

		tradeItems(player1, player2);
		tradeItems(player2, player1);
	}

	private void tradeItems(final L2Player player, final L2Player reciever)
	{
		final ConcurrentLinkedQueue<TradeItem> exchangeList = getExchangeList(player);

		final Inventory playersInv = player.getInventory();
		final Inventory recieverInv = reciever.getInventory();
		L2ItemInstance recieverItem, transferItem;

		for(final TradeItem temp : exchangeList)
		{
			if(temp.getObjectId() == 0)
			{
				_log.warning("Transaction: null object id item, player " + player);
				Util.handleIllegalPlayerAction(player, "Transaction[206]", "trade with item objectId = 0", IllegalPlayerAction.WARNING);
				continue;
			}
			else if(temp.getCount() <= 0)
			{
				_log.warning("Transaction: null item count, player " + player);
				Util.handleIllegalPlayerAction(player, "Transaction[212]", "trade with item count <= 0", IllegalPlayerAction.WARNING);
				continue;
			}

			// If player trades the enchant scroll he was using remove its effect
			if(player.getActiveEnchantItem() != null && temp.getObjectId() == player.getActiveEnchantItem().getObjectId())
				player.setActiveEnchantItem(null);

			if(player.getActiveEnchantAttrItem() != null && temp.getObjectId() == player.getActiveEnchantAttrItem().getObjectId())
				player.setActiveEnchantAttrItem(null);

			transferItem = playersInv.dropItem(temp.getObjectId(), temp.getCount(), false);
			if(transferItem == null)
			{
				Util.handleIllegalPlayerAction(player, "Transaction[250]", "trade with null item! not unbanned!!", IllegalPlayerAction.CRITICAL);
				continue;
			}

			recieverItem = recieverInv.addItem(transferItem);
			Log.LogItem(player, reciever, Log.TradeGive, transferItem);
			Log.LogItem(player, reciever, Log.TradeGet, recieverItem);
		}

		player.sendChanges();
		reciever.sendChanges();
	}
}
