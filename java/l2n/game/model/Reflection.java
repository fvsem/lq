package l2n.game.model;

import gnu.trove.set.hash.TLongHashSet;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.SpawnInfo;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.network.serverpackets.ExSendUIEvent;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.TerritoryTable;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Reflection extends L2Object
{
	private final class CollapseTask implements Runnable
	{
		@Override
		public void run()
		{
			collapse();
		}
	}

	private final class CheckTimeUpTask implements Runnable
	{
		private final long _remaining;
		private final boolean _showMsg;

		public CheckTimeUpTask(final boolean showMsg, final long remaining)
		{
			_remaining = remaining;
			_showMsg = showMsg;
		}

		@Override
		public void run()
		{
			doCheckTimeUp(_showMsg, _remaining);
		}
	}

	protected static final Logger _log = Logger.getLogger("l2n.game.model.Reflection");

	private static final int[] TIMER_DISABLED_INSTANCES = new int[] { 115, 116, 119, 120, 121, 122 };

	protected static final long MILLISECONDS_IN_1_MINUTE = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
	protected static final long MILLISECONDS_IN_5_MINUTE = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);

	protected long _id = Integer.MAX_VALUE;
	private Instance _instance = null;
	private int _instancedZoneId = 0;

	private Location _coreLoc;
	private Location _returnLoc;
	private Location _teleportLoc;

	protected GArray<L2Spawn> _spawns = new GArray<L2Spawn>();
	protected GArray<L2DoorInstance> _doors = new GArray<L2DoorInstance>();
	protected TLongHashSet _objects = new TLongHashSet();

	protected int _playerCount = 0;

	private ScheduledFuture<?> _сheckTimeOutTask;

	private L2Party _party;
	private L2CommandChannel _commandChannel;
	private final AtomicBoolean _isCollapseStarted = new AtomicBoolean(false); // Default FALSE

	private String _name = "";
	private long _emptyDestroyTime = 0;
	private long _lastLeft = -1;
	private long _instanceEndTime = -1;

	private boolean _showTimer = true;

	public int getInstancedZoneId()
	{
		return _instancedZoneId;
	}

	public void setInstancedZoneId(final int id)
	{
		_instancedZoneId = id;
		if(ArrayUtil.arrayContains(TIMER_DISABLED_INSTANCES, id))
			_showTimer = false;
	}

	public Reflection(final long id, final boolean inTable)
	{
		super(IdFactory.getInstance().getNextId());
		_id = id;
		if(inTable)
			ReflectionTable.getInstance().addReflection(this);
	}

	public Reflection(final long id)
	{
		super(IdFactory.getInstance().getNextId());
		_id = id;
		ReflectionTable.getInstance().addReflection(this);
	}

	public Reflection(final String name)
	{
		super(IdFactory.getInstance().getNextId());
		_name = name;
		ReflectionTable.getInstance().addReflection(this);
	}

	public Reflection(final Instance iz)
	{
		super(IdFactory.getInstance().getNextId());
		_instance = iz;
		_name = iz.getName();
		ReflectionTable.getInstance().addReflection(this);
	}

	public void addSpawn(final L2Spawn spawn)
	{
		if(spawn != null)
			_spawns.add(spawn);
	}

	public void addDoor(final L2DoorInstance door)
	{
		_doors.add(door);
	}

	public void setParty(final L2Party party)
	{
		_party = party;
	}

	public L2Party getParty()
	{
		return _party;
	}

	public void setCommandChannel(final L2CommandChannel commandChannel)
	{
		_commandChannel = commandChannel;
	}

	public L2CommandChannel getCommandChannel()
	{
		return _commandChannel;
	}

	public void setEmptyDestroyTime(final long time)
	{
		_emptyDestroyTime = time;
	}

	private final void stopAllTimers(final boolean mayInterruptIfRunning)
	{
		if(_сheckTimeOutTask != null)
			_сheckTimeOutTask.cancel(mayInterruptIfRunning);
		_сheckTimeOutTask = null;
	}

	public void startCollapseTimer(final long duration)
	{
		if(_id <= 0)
		{
			_log.log(Level.WARNING, "Reflection: basic reflection " + _id + " could not be collapsed!", new Exception());
			return;
		}

		if(isCollapseStarted())
			return;

		stopAllTimers(true);

		_сheckTimeOutTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckTimeUpTask(false, duration), 500);
		_instanceEndTime = System.currentTimeMillis() + duration + 500;
	}

	private final void sendTimeBeforeCollapse(final int minuts)
	{
		if(isCollapseStarted())
			return;

		final GArray<L2Player> players = getPlayers();
		for(final L2Player player : players)
			player.sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(minuts));
	}

	private final void doCheckTimeUp(final boolean showMsg, long remaining)
	{
		if(isCollapseStarted())
			return;


		long interval;

		if(_playerCount < 1 && _emptyDestroyTime == 0)
		{
			interval = 100;
			remaining = 0;
		}
		else if(_playerCount < 1 && _emptyDestroyTime > 0)
		{
			final long emptyTimeLeft = _lastLeft + _emptyDestroyTime - System.currentTimeMillis();
			if(emptyTimeLeft <= 0)
			{
				interval = 100;
				remaining = 0;
			}
			else if(remaining > MILLISECONDS_IN_5_MINUTE && emptyTimeLeft > MILLISECONDS_IN_5_MINUTE)
			{
				interval = MILLISECONDS_IN_5_MINUTE;
				remaining -= MILLISECONDS_IN_5_MINUTE;
			}
			else if(remaining > MILLISECONDS_IN_1_MINUTE && emptyTimeLeft > MILLISECONDS_IN_1_MINUTE)
			{
				interval = MILLISECONDS_IN_1_MINUTE;
				remaining -= MILLISECONDS_IN_1_MINUTE;
			}
			else if(remaining > emptyTimeLeft)
			{
				stopAllTimers(false);
				_сheckTimeOutTask = L2GameThreadPools.getInstance().scheduleGeneral(new CollapseTask(), emptyTimeLeft);
				return;
			}
			else
			{
				interval = 100;
				remaining = 0;
			}
		}
		else if(remaining > MILLISECONDS_IN_5_MINUTE)
		{
			if(showMsg && _showTimer)
				if(!isDimensionalRift() && !isDelusionChamber() && !isDarknessFestival())
				{
					final int timeLeft = (int) (remaining / MILLISECONDS_IN_1_MINUTE);
					if(timeLeft % 5 == 0)
						sendTimeBeforeCollapse(timeLeft);
				}

			interval = MILLISECONDS_IN_5_MINUTE;
			remaining -= MILLISECONDS_IN_5_MINUTE;
		}
		else if(remaining > MILLISECONDS_IN_1_MINUTE)
		{
			if(showMsg && _showTimer)
				if(!isDimensionalRift() && !isDelusionChamber() && !isDarknessFestival())
				{
					final int timeLeft = (int) (remaining / MILLISECONDS_IN_1_MINUTE);
					sendTimeBeforeCollapse(timeLeft);
				}

			interval = MILLISECONDS_IN_1_MINUTE;
			remaining -= MILLISECONDS_IN_1_MINUTE;
		}
		else
		{
			interval = remaining;
			remaining = 0;
		}


		stopAllTimers(false);
		if(remaining > 0)
			_сheckTimeOutTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckTimeUpTask(true, remaining), interval);
		else
			_сheckTimeOutTask = L2GameThreadPools.getInstance().scheduleGeneral(new CollapseTask(), interval);
	}

	/** Закрываем инстансе */
	public void collapse()
	{
		if(_id <= 0)
		{
			_log.log(Level.WARNING, "Reflection: basic reflection " + _id + " could not be collapsed!", new Exception());
			return;
		}

		if(isCollapseStarted())
			return;

		_isCollapseStarted.set(true);

		stopAllTimers(true);

		try
		{
			_spawns.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);

			for(final L2DoorInstance d : _doors)
				d.deleteMe();

			final GArray<L2Player> teleport_list = new GArray<L2Player>();
			final GArray<L2Object> delete_list = new GArray<L2Object>();

			final GArray<L2Object> all_objects = getObjects();
			for(final L2Object o : all_objects)
				if(o != null)
					if(o.isPlayer())
						teleport_list.add((L2Player) o);
					else if(!o.isSummon() && !o.isPet())
						delete_list.add(o);

			processTeleportList(teleport_list);

			if(_commandChannel != null)
				_commandChannel.setReflection(null);

			if(_party != null)
				_party.setReflection(null);

			delete_list.forEach(L2ObjectProcedures.PROC_DELETE_L2OBJECT);

			_spawns = null;
			_doors = null;
			_objects = null;
			_commandChannel = null;
			_party = null;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "ReflectionTable: collapse reflection error: ", e);
		}
		finally
		{
			L2ObjectsStorage.remove(getId());
			InstanceManager.getInstance().removeWorld(getId());
			IdFactory.getInstance().releaseId(getObjectId());
		}
	}

	public long getId()
	{
		return _id;
	}

	public void setId(final long newId)
	{
		_id = newId;
	}

	public void addObject(final L2Object o)
	{
		if(isCollapseStarted())
			return;

		synchronized (_objects)
		{
			_objects.add(o.getStoredId());
			if(o.isPlayer())
				_playerCount++;
		}
	}

	public void removeObject(final L2Object o)
	{
		if(isCollapseStarted())
			return;

		synchronized (_objects)
		{
			_objects.remove(o.getStoredId());
			if(o.isPlayer())
			{
				_playerCount--;
				if(_playerCount < 1 && _id > 0 && _emptyDestroyTime >= 0)
				{
					_lastLeft = System.currentTimeMillis();
					startCollapseTimer(_instanceEndTime - System.currentTimeMillis() - 500);
				}
			}
		}
	}

	public void setCoreLoc(final Location location)
	{
		_coreLoc = location;
	}

	public Location getCoreLoc()
	{
		return _coreLoc;
	}

	public void setReturnLoc(final Location location)
	{
		_returnLoc = location;
	}

	public Location getReturnLoc()
	{
		return _returnLoc;
	}

	public void setTeleportLoc(final Location location)
	{
		_teleportLoc = location;
	}

	public Location getTeleportLoc()
	{
		return _teleportLoc;
	}

	public void setName(final String name)
	{
		_name = name;
	}

	public Instance getInstancedZone()
	{
		return _instance;
	}

	public GArray<L2Spawn> getSpawns()
	{
		return _spawns;
	}

	protected void processTeleportList(final GArray<L2Player> teleport_list)
	{
		for(final L2Player player : teleport_list)
		{
			if(player.getParty() != null)
			{
				if(equals(player.getParty().getReflection()))
					player.getParty().setReflection(null);
				if(player.getParty().getCommandChannel() != null && equals(player.getParty().getCommandChannel().getReflection()))
					player.getParty().getCommandChannel().setReflection(null);
			}

			if(equals(player.getReflection()))
				if(getReturnLoc() != null)
					player.teleToLocation(getReturnLoc(), 0);
				else
					player.setReflection(0);

			player.sendPacket(new ExSendUIEvent(player, true, true, 0, 10, _name));
		}
	}

	public GArray<L2Player> getPlayers()
	{
		if(_playerCount < 1 || _objects == null)
			return GArray.emptyCollection();

		long[] objects = null;
		synchronized (_objects)
		{
			objects = _objects.toArray();
		}

		if(objects != null)
		{
			final GArray<L2Player> result = new GArray<L2Player>(_playerCount);
			L2Object o;
			for(final long storedId : objects)
			{
				o = L2ObjectsStorage.get(storedId);
				if(o != null && o.isPlayer())
					result.add((L2Player) o);
			}
			return result;
		}
		return GArray.emptyCollection();
	}

	public GArray<L2NpcInstance> getNpcs()
	{
		if(_objects == null)
			return GArray.emptyCollection();

		long[] objects = null;
		synchronized (_objects)
		{
			objects = _objects.toArray();
		}

		if(objects != null)
		{
			final GArray<L2NpcInstance> result = new GArray<L2NpcInstance>();
			L2Object o;
			for(final long storedId : objects)
			{
				o = L2ObjectsStorage.get(storedId);
				if(o != null && o.isNpc())
					result.add((L2NpcInstance) o);
			}
			return result;
		}
		return GArray.emptyCollection();
	}

	public GArray<L2Object> getObjects()
	{
		if(_objects == null)
			return GArray.emptyCollection();

		long[] objects = null;
		synchronized (_objects)
		{
			objects = _objects.toArray();
		}

		if(objects != null)
		{
			final GArray<L2Object> result = new GArray<L2Object>();
			L2Object o;
			for(final long storedId : objects)
			{
				o = L2ObjectsStorage.get(storedId);
				if(o != null)
					result.add(o);
			}
			return result;
		}
		return GArray.emptyCollection();
	}

	public void fillSpawns(final GArray<SpawnInfo> si)
	{
		if(si == null)
			return;
		for(final SpawnInfo s : si)
		{
			L2Spawn c;
			final GArray<int[]> points = TerritoryTable.getInstance().getLocation(s.getLocationId()).getCoords();
			switch (s.getType())
			{
				case 0:
					for(final int[] point : points)
					{
						c = s.getSpawn().clone();
						addSpawn(c);
						c.setReflection(getId());
						c.setRespawnDelay(s.getSpawn().getRespawnDelay(), s.getSpawn().getRespawnDelayRandom());
						c.setLocation(0);
						c.setLoc(new Location(point));
						if(!NpcTable.getTemplate(c.getNpcId()).isInstanceOf(L2ReflectionBossInstance.class))
							c.startRespawn();
						c.doSpawn(true);
						if(s.getSpawn().getNativeRespawnDelay() == 0)
							c.stopRespawn();
					}
					break;
				case 1:
					c = s.getSpawn().clone();
					addSpawn(c);
					c.setReflection(getId());
					c.setRespawnDelay(s.getSpawn().getRespawnDelay(), s.getSpawn().getRespawnDelayRandom());
					c.setLocation(0);
					c.setLoc(new Location(points.get(Rnd.get(points.size()))));
					if(!NpcTable.getTemplate(c.getNpcId()).isInstanceOf(L2ReflectionBossInstance.class))
						c.startRespawn();
					c.doSpawn(true);
					if(s.getSpawn().getNativeRespawnDelay() == 0)
						c.stopRespawn();
					break;
				case 2:
					c = s.getSpawn().clone();
					addSpawn(c);
					c.setReflection(getId());
					c.setRespawnDelay(s.getSpawn().getRespawnDelay(), s.getSpawn().getRespawnDelayRandom());
					if(!NpcTable.getTemplate(c.getNpcId()).isInstanceOf(L2ReflectionBossInstance.class))
						c.startRespawn();
					for(int j = 0; j < c.getAmount(); j++)
						c.doSpawn(true);
					if(s.getSpawn().getNativeRespawnDelay() == 0)
						c.stopRespawn();
			}
		}
	}

	public void fillDoors(final GArray<L2DoorInstance> doors)
	{
		if(doors == null)
			return;
		for(final L2DoorInstance d : doors)
		{
			final L2DoorInstance door = d.clone();
			door.setReflection(this);
			addDoor(door);
			door.spawnMe();
			if(d.isOpen())
				door.openMe();
		}
	}

	public GArray<L2DoorInstance> getDoors()
	{
		return _doors;
	}

	public boolean canChampions()
	{
		return _id <= 0;
	}

	public boolean isAutoLootForced()
	{
		return false;
	}

	@Override
	public String toString()
	{
		return getName() + " [" + getId() + "]";
	}

	public void clearReflection(final int collapseTime, final boolean message)
	{
		if(getId() <= 0)
			return;

		L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				_spawns.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);
			}
		}, 1000);

		startCollapseTimer(collapseTime * 60 * 1000);

		if(message)
			sendTimeBeforeCollapse(collapseTime);
	}

	public void openDoor(final int doorId)
	{
		for(final L2DoorInstance door : getDoors())
			if(door.getDoorId() == doorId)
				door.openMe();
	}

	public void openDoors(final int... doors)
	{
		for(final int id : doors)
			openDoor(id);
	}

	public void closeDoor(final int doorId)
	{
		for(final L2DoorInstance door : getDoors())
			if(door.getDoorId() == doorId)
				door.closeMe();
	}

	public void closeDoors(final int... doors)
	{
		for(final int id : doors)
			closeDoor(id);
	}

	@Override
	protected void finalize()
	{
		collapse();
	}

	public boolean isCollapseStarted()
	{
		return _isCollapseStarted.get();
	}

	@Override
	public String getName()
	{
		return _name;
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return false;
	}

	public boolean isDarknessFestival()
	{
		return false;
	}

	public boolean isDimensionalRift()
	{
		return false;
	}

	public boolean isDelusionChamber()
	{
		return false;
	}

	public boolean isKamalokaNightmare()
	{
		return false;
	}

	public boolean isKamalokaAbyssLabyrinth()
	{
		return false;
	}
}
