package l2n.game.model;

import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2TrapInstance;

public class L2RoundTerritoryWithSkill extends L2RoundTerritory
{
	private final long _effector;
	private final L2Skill _skill;

	public L2RoundTerritoryWithSkill(final int id, final int centerX, final int centerY, final int radius, final int zMin, final int zMax, final L2Character effector, final L2Skill skill)
	{
		super(id, centerX, centerY, radius, zMin, zMax);
		_effector = effector.getStoredId();
		_skill = skill;
		if(_skill == null)
			_log.warning("L2RoundTerritoryWithSkill with null skill: " + getId());
	}

	@Override
	public void doEnter(final L2Character character)
	{
		super.doEnter(character);

		L2Character effector = getEffector();
		if(effector == null || character == null || _skill == null || !isInside(character.getLoc()))
			return;

		if(character.getReflection().getId() == effector.getReflection().getId())
		{
			if(effector.isTrap())
				((L2TrapInstance) effector).detonate(character);
			else if(_skill.checkTarget(effector, character, null, false, false) == null)
				_skill.getEffects(effector, character, false, false);
		}
	}

	public L2Character getEffector()
	{
		return L2ObjectsStorage.getAsCharacter(_effector);
	}
}
