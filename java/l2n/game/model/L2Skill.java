package l2n.game.model;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.DefaultAI;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.entity.Duel;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.instances.*;
import l2n.game.model.items.Inventory;
import l2n.game.model.restrictions.PlayerRestrictionType;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.serverpackets.FlyToLocation.FlyType;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Formulas;
import l2n.game.skills.Stats;
import l2n.game.skills.conditions.Condition;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.game.skills.skillclasses.DeathPenalty;
import l2n.game.skills.skillclasses.*;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.*;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class L2Skill
{
	protected final static Logger _log = Logger.getLogger(L2Skill.class.getName());

	private final static TIntObjectHashMap<GArray<Integer>> _reuseGroups = new TIntObjectHashMap<GArray<Integer>>();

	public static final L2Character[] EMPTY_TARGETS = new L2Character[0];
	public static final int SKILL_CUBIC_MASTERY = 143;
	public static final int SKILL_CRAFTING = 172;
	public static final int SKILL_POLEARM_MASTERY = 216;
	public static final int SKILL_CRYSTALLIZE = 248;
	public static final int SKILL_BLINDING_BLOW = 321;
	public static final int SKILL_STRIDER_ASSAULT = 325;
	public static final int SKILL_BLUFF = 358;
	public static final int SKILL_CRITICAL_BLOW = 409;
	public static final int SKILL_SOUL_MASTERY = 467;

	public static final int SKILL_TRANSFOR_DISPELL = 619;
	public static final int SKILL_FINAL_FLYING_FORM = 840;
	public static final int SKILL_AURA_BIRD_FALCON = 841;
	public static final int SKILL_AURA_BIRD_OWL = 842;

	public static final int SKILL_DUAL_DAGGER_MASTERY = 923;
	public static final int SKILL_DETECTION = 933;
	public static final int SKILL_RESTORE_LIFE = 1258;
	public static final int SKILL_TRANSFER_PAIN = 1262;
	public static final int SKILL_FISHING_MASTERY = 1315;
	public static final int SKILL_CREATE_COMMON = 1320;
	public static final int SKILL_DWARVEN_CRAFT = 1321;
	public static final int SKILL_COMMON_CRAFT = 1322;
	public static final int SKILL_BLESSING_OF_NOBLESSE = 1323;
	public static final int SKILL_FORTUNE_OF_NOBLESSE = 1325;
	public static final int SKILL_DIVINE_INSPIRATION = 1405;
	public static final int SKILL_MYSTIC_IMMUNITY = 1411;
	public static final int SKILL_STEAL_DIVINITY = 1440;
	public static final int SKILL_RAID_BLESSING = 2168;
	public static final int SKILL_DISMISS_AGATHION = 3267;
	public static final int SKILL_HINDER_STRIDER = 4258;
	public static final int SKILL_WYVERN_BREATH = 4289;
	public static final int SKILL_RAID_CURSE = 4515;
	public static final int SKILL_CHARM_OF_COURAGE = 5041;
	public static final int SKILL_EVENT_TIMER = 5239;
	public static final int SKILL_BATTLEFIELD_DEATH_SYNDROME = 5660;

	// save vs
	public final static int SAVEVS_INT = 1;
	public final static int SAVEVS_WIT = 2;
	public final static int SAVEVS_MEN = 3;
	public final static int SAVEVS_CON = 4;
	public final static int SAVEVS_DEX = 5;
	public final static int SAVEVS_STR = 6;

	public final static int HEIGHT = 128;

	public static enum SkillOpType
	{
		OP_ACTIVE,
		OP_PASSIVE,
		OP_TOGGLE,
		OP_ON_ACTION;
	}

	public static enum TriggerActionType
	{
		/** скилл срабатывает при добавлении в лист */
		ADD,
		/** при атаке любой */
		ATTACK,
		/** при крите */
		CRIT,

		/** при использовании любого скила */
		// TODO SKILL_USE,
		/** при использовании маг скилов */
		// TODO MAGIC_SKILL_USE,
		/** при использовании игроком физ скилов */
		// TODO PHYSICAL_SKILL_USE,

		/** при использовании игроком атакающих физ скилов */
		OFFENSIVE_PHYSICAL_SKILL_USE,
		/** при использовании игроком атакающих маг скилов */
		OFFENSIVE_MAGICAL_SKILL_USE,
		/** при использовании скилов поддержки (бафов, хилов) */
		SUPPORT_MAGICAL_SKILL_USE,

		/** вы атакованы простыми атаками наносящими урон! */
		UNDER_ATTACK(true),
		/** вы атакованы физ/маг скилами наносящими урон! */
		UNDER_SKILL_ATTACK(true),
		/**  */
		UNDER_MISSED_ATTACK,
		/** вы атакованы атакующими скилами */
		UNDER_OFFENSIVE_SKILL_ATTACK,

		// TODO UNDER_MAGIC_SKILL_ATTACK,
		// TODO UNDER_MAGIC_SUPPORT,

		DIE; // срабатывает при смерти

		private final boolean checkDamage;

		private TriggerActionType()
		{
			checkDamage = false;
		}

		private TriggerActionType(final boolean check)
		{
			checkDamage = check;
		}

		public boolean isCheckDamage()
		{
			return checkDamage;
		}
	}

	public static enum NextAction
	{
		DEFAULT,
		NONE,
		ATTACK,
		CAST,
		MOVE
	}

	// elements
	public static enum Element
	{
		FIRE(0, Stats.ATTACK_ELEMENT_FIRE, Stats.FIRE_RECEPTIVE),
		WATER(1, Stats.ATTACK_ELEMENT_WATER, Stats.WATER_RECEPTIVE),
		WIND(2, Stats.ATTACK_ELEMENT_WIND, Stats.WIND_RECEPTIVE),
		EARTH(3, Stats.ATTACK_ELEMENT_EARTH, Stats.EARTH_RECEPTIVE),
		SACRED(4, Stats.ATTACK_ELEMENT_SACRED, Stats.SACRED_RECEPTIVE),
		UNHOLY(5, Stats.ATTACK_ELEMENT_UNHOLY, Stats.UNHOLY_RECEPTIVE),
		NONE(-2, null, null);

		/** Массив элементов без NONE **/
		public final static Element[] VALUES = Arrays.copyOf(values(), 6);

		private final int _id;
		private final Stats _attack;
		private final Stats _defence;

		private Element(final int id, final Stats attack, final Stats defence)
		{
			_id = id;
			_attack = attack;
			_defence = defence;
		}

		public int getId()
		{
			return _id;
		}

		public Stats getAttack()
		{
			return _attack;
		}

		public Stats getDefence()
		{
			return _defence;
		}

		public static Element getElementById(final int id)
		{
			for(final Element e : values())
				if(e.getId() == id)
					return e;
			return NONE;
		}
	}

	public static enum SkillTargetType
	{
		TARGET_NONE,
		TARGET_ONE,
		TARGET_SELF,

		TARGET_ALLY,
		TARGET_AREA,
		TARGET_AREA_AIM_CORPSE,
		TARGET_AURA,
		TARGET_AURA_CORPSE,
		TARGET_CHEST,
		TARGET_CLAN,
		TARGET_CORPSE,
		TARGET_CORPSE_PLAYER,
		TARGET_PET,

		TARGET_ENEMY_PET,
		TARGET_ENEMY_SUMMON,
		TARGET_ENEMY_SERVITOR,
		TARGET_FLAGPOLE,
		TARGET_HOLY,
		TARGET_ITEM,

		/** конус 120 градусов перед собой */
		TARGET_MULTIFACE,
		TARGET_MULTIFACE_AURA,
		/** цилиндр */
		TARGET_TUNNEL,

		TARGET_OWNER,
		TARGET_PARTY,
		TARGET_PARTY_ONE,
		TARGET_PARTY_CLAN,
		TARGET_SIEGE,
		TARGET_TYRANNOSAURUS,
		TARGET_UNLOCKABLE;

		private ISkillTargetTypeHandler _targetHandler;

		public void setHandler(final ISkillTargetTypeHandler handler)
		{
			_targetHandler = handler;
			if(_targetHandler == null)
				_log.warning("SkillTargetType: not set handler for " + name());
		}

		public final L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
		{
			if(_targetHandler == null)
			{
				activeChar.sendPacket(CustomSystemMessageId.L2SKILL_SKILL_TARGET_NOT_HANDLED.getPacket());
				return ArrayUtil.EMPTY_TARGETS;
			}
			return _targetHandler.getTargetList(skill, activeChar, aimingTarget, forceUse);
		}
	}

	public static enum SkillType
	{
		AGGDAMAGE,
		AGGRESSION(Aggression.class),
		AIEFFECTS(AIeffects.class),
		BALANCE(Balance.class),
		BALLISTA(BallistaBomb.class),
		BEAST_FEED(BeastFeed.class),
		BLEED(Continuous.class),
		BUFF(Continuous.class),
		BUFF_CHARGER(BuffCharger.class),
		CALL(Call.class),
		CANCEL(Cancel.class, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER),
		CHARGE(Charge.class),
		CHARGE_SOUL(ChargeSoul.class),
		CHARGEDAM(PDam.class),
		CLAN_GATE(ClanGate.class),
		COMBATPOINTHEAL(CombatPointHeal.class),
		CONT(Toggle.class),
		CPDAM(CPDam.class),
		CPHOT(Continuous.class),
		CRAFT(Craft.class),
		DANCE(Continuous.class),
		DEATH_PENALTY(DeathPenalty.class),
		DEATHLINK(Deathlink.class),
		DEBUFF(Continuous.class),
		DELETE_HATE(DeleteHate.class, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER),
		DELETE_HATE_OF_ME(DeleteHateOfMe.class, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER),
		DESTROY_SUMMON(DestroySummon.class, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER),
		DEFUSE_TRAP(DefuseTrap.class),
		DETECT_TRAP(DetectTrap.class),
		DISCORD(Continuous.class),
		DOT(Continuous.class),
		DRAIN(Drain.class),
		DRAIN_SOUL(DrainSoul.class),
		EFFECTS_FROM_SKILLS(EffectsFromSkills.class),
		ENCHANT_ARMOR,
		ENCHANT_WEAPON,
		EXTRACT_STONE(ExtractStone.class),
		FEED_PET,
		FISHING(Fishing.class),
		FORCE_BUFF(Continuous.class),
		HARVESTING(Harvesting.class),
		HEAL(Heal.class),
		HEAL_PERCENT(HealPercent.class),
		HEALCOMBATPOINT_PERCENT(HealCombatPointPercent.class),
		HOT(Continuous.class),
		KAMAEL_WEAPON_EXCHANGE(KamaelWeaponExchange.class),
		LETHAL_SHOT(LethalShot.class),
		LUCK,
		MANADAM(ManaDam.class),
		MANAHEAL(ManaHeal.class),
		MANAHEAL_PERCENT(ManaHealPercent.class),
		MDAM(MDam.class),
		MDOT(Continuous.class),
		MPHOT(Continuous.class),
		MUTE(Disablers.class),
		NEGATE_EFFECTS(NegateEffects.class, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER),
		NEGATE_STATS(NegateStats.class, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER),
		OUTPOST(Outpost.class),
		PARALYZE(Disablers.class),
		PASSIVE,
		PDAM(PDam.class),
		POISON(Continuous.class),
		PUMPING(ReelingPumping.class),
		RECALL(Recall.class),
		REELING(ReelingPumping.class),
		REFILL(Refill.class),
		RESURRECT(Resurrect.class),
		RIDE(Ride.class),
		ROOT(Disablers.class),
		RUSH(Rush.class),
		SHIFT_AGGRESSION(ShiftAggression.class),
		SIEGEFLAG(SiegeFlag.class),
		SLEEP(Disablers.class),
		SONG(Continuous.class),
		SOULSHOT,
		SOWING(Sowing.class),
		SPHEAL(SPHeal.class),
		SPIRITSHOT,
		SPOIL(Spoil.class),
		STEAL_BUFF(StealBuff.class, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER),
		STUN(Disablers.class),
		SUMMON(Summon.class),
		SUMMON_ITEM(SummonItem.class),
		SWEEP(Sweep.class),
		TAKECASTLE(TakeCastle.class),
		TAKEFORT(TakeFortress.class),
		TAKEFLAG(TakeFlag.class),
		TELEPORT_NPC(TeleportNpc.class),
		TRANSFORMATION(Transformation.class),
		TRANSFORMDISPEL(TransformDispel.class),
		UNLOCK(Unlock.class),
		WATCHER_GAZE(Continuous.class),
		GIVE_VITALITY(Vitality.class),

		// TODO Удалить
		LIGHTNING_SHOCK(PDam.class, Stats.PARALYZE_RECEPTIVE, Stats.PARALYZE_POWER),

		// effect based
		EFFECT(Effect.class),
		HARDCODED(Effect.class),
		// unimplemented
		NOTDONE,
		NOTUSED;

		private final Class<? extends L2Skill> clazz;
		private final Stats _resistType;
		private final Stats _attackType;

		public L2Skill makeSkill(final StatsSet set)
		{
			try
			{
				final Constructor<? extends L2Skill> c = clazz.getConstructor(StatsSet.class);
				return c.newInstance(set);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "L2Skill: error on skill id = " + set.getInteger("skill_id", 0) + ", name = " + set.getString("name", "NONE"), e);
				Log.addDev("L2Skill: error on skill id = " + set.getInteger("skill_id", 0) + ", name = " + set.getString("name", "NONE"), "skills_load_errors", false);
				throw new RuntimeException(e);
			}
		}

		private SkillType()
		{
			this(Default.class, null, null);
		}

		private SkillType(final Class<? extends L2Skill> clazz)
		{
			this(clazz, null, null);
		}

		private SkillType(final Class<? extends L2Skill> clazz, final Stats resistType, final Stats attibuteType)
		{
			this.clazz = clazz;
			_resistType = resistType;
			_attackType = attibuteType;
		}

		/** @return тип статов который увеличивает шанс прохождения эффекта */
		private Stats getAttackType()
		{
			return _attackType;
		}

		/** @return тип статов который уменьшает шанс прохождения (разные сопротивляемости) */
		private Stats getResistType()
		{
			return _resistType;
		}

		public final boolean isPvpSkill()
		{
			switch (this)
			{
				case BLEED:
				case CANCEL:
				case AGGRESSION:
				case DEBUFF:
				case DOT:
				case MDOT:
				case MUTE:
				case PARALYZE:
				case POISON:
				case ROOT:
				case SLEEP:
				case MANADAM:
				case DESTROY_SUMMON:
				case NEGATE_STATS:
				case NEGATE_EFFECTS:
				case STEAL_BUFF:
				case DELETE_HATE:
				case DELETE_HATE_OF_ME:
					return true;
				default:
					return false;
			}
		}

		public final boolean isDebuff()
		{
			switch (this)
			{
				case MUTE:
				case PARALYZE:
				case ROOT:
				case SLEEP:
				case STUN:
				case DEBUFF:
				case TELEPORT_NPC:
				case POISON:
				case BLEED:
				case DOT:
				case SOWING:
					return true;
				default:
					return false;
			}
		}

		public boolean isOffensive()
		{
			switch (this)
			{
				case AGGRESSION:
				case AIEFFECTS:
				case BALLISTA:
				case BLEED:
				case CANCEL:
				case CHARGEDAM:
				case DEBUFF:
				case DOT:
				case DRAIN:
				case DEATHLINK:
				case DRAIN_SOUL:
				case LETHAL_SHOT:
				case MANADAM:
				case MDAM:
				case MDOT:
				case MUTE:
				case PARALYZE:
				case PDAM:
				case LIGHTNING_SHOCK: // TODO Удалить
				case CPDAM:
				case POISON:
				case ROOT:
				case SLEEP:
				case SOULSHOT:
				case SPIRITSHOT:
				case SPOIL:
				case STUN:
				case SWEEP:
				case HARVESTING:
				case TELEPORT_NPC:
				case SOWING:
				case DELETE_HATE:
				case DELETE_HATE_OF_ME:
				case SHIFT_AGGRESSION:
				case DESTROY_SUMMON:
				case STEAL_BUFF:
				case DISCORD:
				case UNLOCK:
				case RUSH:
				case AGGDAMAGE:
					return true;
				default:
					return false;
			}
		}

		/**
		 * Работают только против npc
		 */
		public boolean isPvM()
		{
			switch (this)
			{
				case DISCORD:
					return true;
				default:
					return false;
			}
		}

		/**
		 * Такие скиллы не аггрят цель, и не флагают чара, но являются "плохими"
		 */
		public boolean isAI()
		{
			switch (this)
			{
				case AGGRESSION:
				case AIEFFECTS:
				case SOWING:
				case DELETE_HATE:
				case DELETE_HATE_OF_ME:
				case SHIFT_AGGRESSION:
					return true;
				default:
					return false;
			}
		}
	}

	public final static class AddedSkill
	{
		public int id;
		public int level;

		public AddedSkill(final int Id, final int lvl)
		{
			id = Id;
			level = lvl;
		}

		public L2Skill getSkill()
		{
			return SkillTable.getInstance().getInfo(id, level);
		}

		@Override
		public String toString()
		{
			return id + "-" + level;
		}
	}

	protected AddedSkill[] _addedSkills;

	// these two build the primary key
	protected Integer _id;
	protected Short _level;
	protected Short _baseLevel;

	/** Identifier for a skill that client can't display */
	protected Integer _displayId;
	protected Short _displayLevel;

	/** Отвечает за анимацию при использовании скила */
	protected int _delayedEffect;

	// not needed, just for easier debug
	protected String _name;
	protected String _icon;

	protected SkillOpType _operateType;
	protected Map<TriggerActionType, double[]> _triggerActions;

	protected int _mpInitialConsume; // mpConsume1
	protected int _mpConsume; // mpConsume2

	protected int _hpConsume;
	protected int[] _itemConsumeCounts;
	protected int[] _itemConsumeIDs;
	protected int _soulsConsume;

	protected final Boolean _isCommon;
	protected final Boolean _isSaveable;
	protected final Boolean _isSuicideAttack;
	protected final Boolean _isShieldignore;
	protected final Boolean _isSkillTimeStatic;
	protected final Boolean _isSkillReuseStatic;
	protected final boolean _isOffensive;
	protected final boolean _isPvpSkill;
	protected final boolean _isPvM;
	protected final Boolean _isHealAggro;
	protected final Boolean _isForceUse;
	protected final Boolean _isNoEffectWithoutTargets;
	protected final boolean _isItemSkill;
	protected final boolean _isTalismanSkill;
	protected final boolean _isDanceSong;
	protected boolean _isMagic;
	protected final boolean _isItemHandler;
	protected final boolean _isUndeadOnly;
	protected final boolean _isCorpse;
	protected final boolean _isOlympiadEnabled;
	protected final boolean _isAltUse;
	protected final boolean _isCancelable;
	protected final boolean _isShieldIgnore;
	protected boolean _isOverhit;
	protected final boolean _isBehind;
	protected final boolean _isValidateable;
	protected final Boolean _isUseSS;
	protected final boolean _isSoulBoost;
	protected final boolean _isChargeBoost;
	protected final boolean _isUsingWhileCasting;
	protected boolean _skillInterrupt;
	protected final boolean _deathlink;
	protected final boolean _isNotUsedByAI;
	protected final boolean _isIgnoreResists;
	protected final boolean _isNotAffectedByMute;
	protected final boolean _flyingTransformUsage;
	protected final boolean _isReflectable;
	protected final boolean _hideStartMessage;
	protected final boolean _hideUseMessage;

	protected int _castRange;

	protected int _mAtak;
	protected int _cancelTarget;

	protected int[] _hitTimings;
	protected int _hitCount;

	protected int _reuseGroupId;
	protected int _enchantLevelCount;

	// all times in milliseconds
	protected int _hitTime;
	protected int _coolTime;
	protected int _skillInterruptTime;
	protected int _reuseDelay;

	protected final SkillType _skillType;
	protected final SkillTargetType _targetType;

	protected double _power;
	protected final double _powerPvP;
	protected final double _powerPvE;

	protected final double _lethal1;
	protected final double _lethal2;
	protected final double _absorbPart;
	protected final double _dependOnTargetBuff;

	protected final int _aggroPoints;
	protected final int _skillRadius;

	protected int _savevs;
	protected final int _activateRate;
	protected int _levelModifier;
	protected byte _magicLevel;
	protected int _criticalRate;
	protected final int _weaponsAllowed;

	protected int _transformId;

	protected int _symbolId;
	protected int _minPledgeClass;
	protected int _negateSkill;
	protected int _negatePower;
	protected int _numCharges;
	protected int _condCharges;
	protected int _npcId;
	protected Element _element;
	protected int _elementPower;
	protected int _flyRadius;
	protected FlyType _flyType;
	protected Boolean _flyToBack;

	protected GArray<ClassId> _canLearn; // which classes can learn
	protected TIntArrayList _teachers; // which NPC teaches

	protected Condition[] _preCondition;
	protected FuncTemplate[] _funcTemplates;
	protected EffectTemplate[] _effectTemplates;

	protected NextAction _nextAction;

	protected final String _toString;
	private final int hashCode;

	/**
	 * Внимание!!! У наследников вручную надо поменять тип на public
	 *
	 * @param set
	 *            парамерты скилла
	 */
	protected L2Skill(final StatsSet set)
	{
		_id = set.getInteger("skill_id");
		_level = set.getShort("level");
		_displayId = set.getInteger("displayId", _id);
		_displayLevel = set.getShort("displayLevel", _level);
		_delayedEffect = set.getInteger("delayedEffect", 0);
		_name = set.getString("name");
		_icon = set.getString("icon", "");

		_operateType = set.getEnum("operateType", SkillOpType.class);
		_targetType = set.getEnum("target", SkillTargetType.class);
		_skillType = set.getEnum("skillType", SkillType.class);

		_isMagic = set.getBool("isMagic", false);
		_isAltUse = set.getBool("altUse", false);
		_isOlympiadEnabled = set.getBool("isOlympiadEnabled", false);
		_mpInitialConsume = set.getInteger("mpInitialConsume", 0);
		_mpConsume = set.getInteger("mpConsume", 0);
		_hpConsume = set.getInteger("hpConsume", 0);
		_soulsConsume = set.getInteger("soulsConsume", 0);
		_mAtak = set.getInteger("mAtk", 0);
		_isUseSS = set.getBool("useSS", null);
		_isSoulBoost = set.getBool("soulBoost", false);
		_isChargeBoost = set.getBool("chargeBoost", false);
		_isUsingWhileCasting = set.getBool("isUsingWhileCasting", false);
		_cancelTarget = set.getInteger("cancelTarget", 0);
		_skillInterrupt = set.getBool("skillInterrupt", false);
		_deathlink = set.getBool("deathlink", false);
		_dependOnTargetBuff = set.getDouble("dependOnTargetBuff", 0);
		_isIgnoreResists = set.getBool("isIgnoreResists", false);
		_isNotAffectedByMute = set.getBool("isNotAffectedByMute", false);
		_isNotUsedByAI = set.getBool("isNotUsedByAI", false);
		_flyingTransformUsage = set.getBool("flyingTransformUsage", false);

		final String hitTimings = set.getString("hitTimings", null);
		if(hitTimings != null)
			try
			{
				final String[] valuesSplit = hitTimings.split(";");
				_hitTimings = new int[valuesSplit.length];
				for(int i = 0; i < valuesSplit.length; i++)
					_hitTimings[i] = Integer.parseInt(valuesSplit[i]);
			}
			catch(final Exception e)
			{
				throw new IllegalArgumentException("SkillId: " + _id + " invalid hitTimings value: " + hitTimings + ", \"percent,percent,...percent\" required");
			}
		else
			_hitTimings = ArrayUtil.EMPTY_INT_ARRAY;
		_hitCount = _hitTimings.length;

		_symbolId = set.getInteger("symbolId", 0);
		// ItemConsume quick hack
		final String s1 = set.getString("itemConsumeCount", "");
		final String s2 = set.getString("itemConsumeId", "");

		if(s1.length() == 0)
			_itemConsumeCounts = ArrayUtil.EMPTY_INT_ARRAY;
		else
		{
			final String[] s = s1.split(" ");
			_itemConsumeCounts = new int[s.length];
			for(int i = 0; i < s.length; i++)
				_itemConsumeCounts[i] = Integer.parseInt(s[i]);
		}

		if(s2.length() == 0)
			_itemConsumeIDs = ArrayUtil.EMPTY_INT_ARRAY;
		else
		{
			final String[] s = s2.split(" ");
			_itemConsumeIDs = new int[s.length];
			for(int i = 0; i < s.length; i++)
				_itemConsumeIDs[i] = Integer.parseInt(s[i]);
		}

		_isItemHandler = set.getBool("isHandler", false);
		_reuseGroupId = set.getInteger("reuseGroup", 0);
		if(_reuseGroupId > 0)
		{
			if(_reuseGroups.get(_reuseGroupId) == null)
				_reuseGroups.put(_reuseGroupId, new GArray<Integer>());
			if(!_reuseGroups.get(_reuseGroupId).contains(_id))
				_reuseGroups.get(_reuseGroupId).add(_id);
		}
		_isCommon = set.getBool("isCommon", null);
		_isSaveable = set.getBool("isSaveable", null);
		_isValidateable = set.getBool("isValidateable", !_name.contains("Item Skill"));

		_hitTime = set.getInteger("hitTime", 0);
		_coolTime = set.getInteger("coolTime", 0);
		_skillInterruptTime = set.getInteger("skillInterruptTime", 0);
		_reuseDelay = set.getInteger("reuseDelay", 0);
		_skillRadius = set.getInteger("skillRadius", 80); // l2jsf effectRange

		_isUndeadOnly = set.getBool("undeadOnly", false);
		_hideStartMessage = set.getBool("isHideStartMessage", false);
		_hideUseMessage = set.getBool("isHideUseMessage", false);
		_isCorpse = set.getBool("corpse", false);

		_power = set.getDouble("power", 0.);
		_powerPvP = set.getDouble("duelPower", 0.);
		_powerPvE = set.getDouble("powerPvE", 0.);

		_lethal1 = set.getDouble("lethal1", 0.);
		_lethal2 = set.getDouble("lethal2", 0.);
		_absorbPart = set.getFloat("absorbPart", 0.f);

		_aggroPoints = set.getInteger("aggroPoints", 0);
		_nextAction = NextAction.valueOf(set.getString("nextAction", "DEFAULT").toUpperCase());
		_isSuicideAttack = set.getBool("isSuicideAttack", null);
		_isShieldignore = set.getBool("shieldignore", null);
		_isSkillTimeStatic = set.getBool("isSkillTimePermanent", null);
		_isSkillReuseStatic = set.getBool("isSkillReusePermanent", null);

		_element = Element.valueOf(set.getString("element", "NONE").toUpperCase());
		_elementPower = set.getInteger("elementPower", 0);

		if(Util.isNumber(set.getString("save", "0")))
			_savevs = set.getInteger("save", 0);
		else
			try
			{
				_savevs = L2Skill.class.getField("SAVEVS_" + set.getString("save").toUpperCase()).getInt(null);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Invalid savevs value: " + set.getString("save"), e);
			}

		_activateRate = set.getInteger("activateRate", -1);
		_levelModifier = set.getInteger("levelModifier", 1);
		_magicLevel = set.getByte("magicLvl", (byte) 0);
		_isCancelable = set.getBool("cancelable", true);
		_isShieldIgnore = set.getBool("shieldignore", false);
		_criticalRate = set.getInteger("criticalRate", 0);
		_transformId = set.getInteger("transformId", 0);
		_isOverhit = set.getBool("overHit", false);
		_weaponsAllowed = set.getInteger("weaponsAllowed", 0);

		_minPledgeClass = set.getInteger("minPledgeClass", 0);

		_isForceUse = set.getBool("isForceUse", null);
		_isOffensive = set.getBool("isOffensive", _skillType.isOffensive());
		_isPvpSkill = set.getBool("isPvpSkill", _skillType.isPvpSkill());
		_isPvM = set.getBool("isPvm", _skillType.isPvM());
		_isHealAggro = set.getBool("isHealAggro", null);
		_isBehind = set.getBool("behind", false);
		_npcId = set.getInteger("npcId", 0);

		_flyType = FlyType.valueOf(set.getString("flyType", "NONE").toUpperCase());
		_flyToBack = set.getBool("flyToBack", null);
		_flyRadius = set.getInteger("flyRadius", 200);

		_negateSkill = set.getInteger("negateSkill", 0);
		_negatePower = set.getInteger("negatePower", Integer.MAX_VALUE);

		_numCharges = set.getInteger("num_charges", 0);
		_condCharges = set.getInteger("cond_charges", 0);

		// id 914 Song of Purification
		// id 915 Dance of Berserker
		_isDanceSong = _skillType == SkillType.DANCE || _skillType == SkillType.SONG || _id == 914 || _id == 915 ? true : false;
		_isItemSkill = _name.contains("Item Skill") ? true : false;
		_isTalismanSkill = _name.contains("Talisman") ? true : false;

		if(_operateType == SkillOpType.OP_ON_ACTION)
		{
			final StringTokenizer st = new StringTokenizer(set.getString("triggerActions", ""), ";");
			if(st.hasMoreTokens())
			{
				_triggerActions = new HashMap<TriggerActionType, double[]>();
				while (st.hasMoreTokens())
				{
					final TriggerActionType type = Enum.valueOf(TriggerActionType.class, st.nextToken());

					final StringTokenizer conditions = new StringTokenizer(st.nextToken(), ",");
					final double chance = Double.parseDouble(conditions.nextToken());
					final int min_damage = conditions.hasMoreTokens() ? Integer.parseInt(conditions.nextToken()) : 1;

					_triggerActions.put(type, new double[] { chance, min_damage });
				}
			}
		}

		if(_triggerActions == null)
			_triggerActions = Collections.emptyMap();

		StringTokenizer st = new StringTokenizer(set.getString("addSkills", ""), ";");
		while (st.hasMoreTokens())
		{
			final int id = Integer.valueOf(st.nextToken());
			int level = Integer.valueOf(st.nextToken());
			if(level == -1)
				level = _level;

			if(level > 0)
				_addedSkills = ArrayUtil.arrayAdd(_addedSkills, new AddedSkill(id, level), AddedSkill.class);
		}

		_toString = _name + "[id=" + _id + ", lvl=" + _level + "]";

		if(_nextAction == NextAction.DEFAULT)
			switch (_skillType)
			{
				case PDAM:
				case LIGHTNING_SHOCK: // TODO Удалить
				case CPDAM:
				case LETHAL_SHOT:
				case CHARGEDAM:
				case SPOIL:
				case SOWING:
				case STUN:
				case DRAIN_SOUL:
					_nextAction = NextAction.ATTACK;
					break;
				default:
					_nextAction = NextAction.NONE;
			}

		if(_savevs == 0)
			switch (_skillType)
			{
				case BLEED:
				case DOT:
				case MDOT:
				case LETHAL_SHOT:
				case PDAM:
				case LIGHTNING_SHOCK: // TODO Удалить
				case CHARGEDAM:
				case CPDAM:
				case POISON:
				case STUN:
					_savevs = SAVEVS_CON;
					break;
				case CANCEL:
				case MANADAM:
				case DEBUFF:
				case MDAM:
				case MUTE:
				case PARALYZE:
				case ROOT:
				case SLEEP:
					_savevs = SAVEVS_MEN;
					break;
				default:
					break;
			}

		final String canLearn = set.getString("canLearn", null);
		if(canLearn == null)
			_canLearn = null;
		else
		{
			_canLearn = new GArray<ClassId>();
			st = new StringTokenizer(canLearn, " \r\n\t,;");
			while (st.hasMoreTokens())
			{
				final String cls = st.nextToken();
				try
				{
					_canLearn.add(ClassId.valueOf(cls));
				}
				catch(final Throwable t)
				{
					_log.log(Level.SEVERE, "Bad class " + cls + " to learn skill", t);
				}
			}
		}

		final String teachers = set.getString("teachers", null);
		if(teachers == null)
			_teachers = null;
		else
		{
			_teachers = new TIntArrayList();
			st = new StringTokenizer(teachers, " \r\n\t,;");
			while (st.hasMoreTokens())
			{
				final String npcid = st.nextToken();
				try
				{
					_teachers.add(Integer.parseInt(npcid));
				}
				catch(final Throwable t)
				{
					_log.log(Level.SEVERE, "Bad teacher id " + npcid + " to teach skill", t);
				}
			}
			_teachers.sort();
		}
		_isReflectable = set.getBool("canBeReflected", true);
		_isNoEffectWithoutTargets = set.getBool("isNoEffectWithoutTargets", false);

		hashCode = _id * 1023 + _level;
	}

	public abstract void useSkill(final L2Character activeChar, final L2Character... targets);

	public void displayHitMessage(final L2Character attacker, final L2Character target, final long damage, final boolean crit)
	{
		if(attacker.isPlayer())
		{
			if(crit)
				attacker.sendPacket(Msg.MAGIC_CRITICAL_HIT);
			attacker.sendPacket(new SystemMessage(SystemMessage.S1_HAS_GIVEN_S2_DAMAGE_OF_S3).addName(attacker).addName(target).addNumber(damage));
		}
	}

	public final boolean isPvpSkill()
	{
		return _isPvpSkill;
	}

	public final boolean isDanceSong()
	{
		return _isDanceSong;
	}

	public final boolean isDebuff()
	{
		switch (_skillType)
		{
			case MUTE:
			case PARALYZE:
			case ROOT:
			case SLEEP:
			case STUN:
			case DEBUFF:
			case TELEPORT_NPC:
			case POISON:
			case BLEED:
			case DOT:
			case SOWING:
				return true;
			default:
				return false;
		}
	}

	public boolean isOffensive()
	{
		return _isOffensive;
	}

	/**
	 * Работают только против npc
	 */
	public boolean isPvM()
	{
		return _isPvM;
	}

	public boolean isForceUse()
	{
		return _isForceUse == null ? false : _isForceUse;
	}

	/**
	 * Такие скиллы не аггрят цель, и не флагают чара, но являются "плохими"
	 */
	public boolean isAI()
	{
		switch (_skillType)
		{
			case AGGRESSION:
			case AIEFFECTS:
			case SOWING:
			case DELETE_HATE:
			case DELETE_HATE_OF_ME:
			case SHIFT_AGGRESSION:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Area of Effect - заклинания и способности, которые действуют по площади, захватывая все цели в некотором радиусе от эпицентра.
	 */
	public boolean isAoE()
	{
		switch (_targetType)
		{
			case TARGET_AREA:
			case TARGET_AREA_AIM_CORPSE:
			case TARGET_AURA:
			case TARGET_AURA_CORPSE:
			case TARGET_MULTIFACE:
			case TARGET_MULTIFACE_AURA:
			case TARGET_TUNNEL:
				return true;
			default:
				return false;
		}
	}

	public boolean isNotTargetAoE()
	{
		switch (_targetType)
		{
			case TARGET_AURA:
			case TARGET_AURA_CORPSE:
			case TARGET_MULTIFACE_AURA:
			case TARGET_ALLY:
			case TARGET_CLAN:
			case TARGET_PARTY:
			case TARGET_PARTY_CLAN:
				return true;
			default:
				return false;
		}
	}

	public boolean oneTarget()
	{
		switch (_targetType)
		{
			case TARGET_CORPSE:
			case TARGET_CORPSE_PLAYER:
			case TARGET_HOLY:
			case TARGET_ITEM:
			case TARGET_NONE:
			case TARGET_ONE:
			case TARGET_PARTY_ONE:
			case TARGET_PET:
			case TARGET_TYRANNOSAURUS:
			case TARGET_OWNER:
			case TARGET_ENEMY_PET:
			case TARGET_ENEMY_SUMMON:
			case TARGET_ENEMY_SERVITOR:
			case TARGET_SELF:
			case TARGET_UNLOCKABLE:
			case TARGET_CHEST:
			case TARGET_SIEGE:
			case TARGET_FLAGPOLE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * @return true если скил является селф-бафом
	 */
	public boolean isSelfBuf()
	{
		return _targetType == SkillTargetType.TARGET_SELF && _skillType == SkillType.BUFF;
	}

	public boolean isBuff()
	{
		return _skillType == SkillType.BUFF;
	}

	public final boolean isAltUse()
	{
		return _isAltUse;
	}

	public final SkillType getSkillType()
	{
		return _skillType;
	}

	public final boolean isNotDone()
	{
		return _skillType == SkillType.NOTDONE;
	}

	/**
	 * @return тип статов который увеличивает шанс прохождения эффекта
	 */
	public Stats getAttackType()
	{
		return _skillType.getAttackType();
	}

	/**
	 * @return тип статов который уменьшает шанс прохождения (разные сопротивляемости)
	 */
	public Stats getResistType()
	{
		return _skillType.getResistType();
	}

	public final int getSavevs()
	{
		return _savevs;
	}

	public final int getActivateRate()
	{
		return _activateRate;
	}

	public final int getLevelModifier()
	{
		return _levelModifier;
	}

	public final byte getMagicLevel()
	{
		return _magicLevel;
	}

	public final boolean isCancelable()
	{
		return _isCancelable && getSkillType() != SkillType.TRANSFORMATION && !isToggle();
	}

	public final boolean isShieldIgnore()
	{
		return _isShieldIgnore;
	}

	public final int getCriticalRate()
	{
		return _criticalRate;
	}

	public final Element getElement()
	{
		return _element;
	}

	public final SkillTargetType getTargetType()
	{
		return _targetType;
	}

	public final boolean isOverhit()
	{
		return _isOverhit;
	}

	public final boolean isSuicideAttack()
	{
		return _isSuicideAttack == null ? false : _isSuicideAttack;
	}

	public final boolean getShieldIgnore()
	{
		return _isShieldignore == null ? false : _isShieldignore;
	}

	/**
	 * На некоторые скиллы и хендлеры предметов скорости каста/атаки не влияет
	 */
	public final boolean isSkillTimePermanent()
	{
		return (_isSkillTimeStatic == null ? false : _isSkillTimeStatic) || _isItemHandler || _name.contains("Talisman");
	}

	public final boolean isSkillReusePermanent()
	{
		return (_isSkillReuseStatic == null ? false : _isSkillReuseStatic) || _isItemHandler;
	}

	public double getDependOnTargetBuff()
	{
		return _dependOnTargetBuff;
	}

	public final double getPower(final L2Character target)
	{
		if(target != null)
		{
			if(target.isPlayable())
				return getPowerPvP();
			if(target.isMonster())
				return getPowerPvE();
		}
		return getPower();
	}

	/**
	 * Return the power of the skill.<BR>
	 * <BR>
	 */
	public final double getPower()
	{
		return _power;
	}

	public final double getPowerPvP()
	{
		return _powerPvP > 0 ? _powerPvP : _power;
	}

	public final double getPowerPvE()
	{
		return _powerPvE > 0 ? _powerPvE : _power;
	}

	/** уменьшает CP до 0 в ПвП, HP на 50% от имеющегося в ПвЕ. Щанс нанести half kill имеют все дамажащие aбилы дагера. */
	public double getLethal1()
	{
		return _lethal1;
	}

	/** уменьшает СP и HP до 1. В данный момент нет ни в одной абиле дагера, но заявлено в следующем обновлении High Five, скил Dual Blow. */
	public double getLethal2()
	{
		return _lethal2;
	}

	public double getAbsorbPart()
	{
		return _absorbPart;
	}

	public final int getElementPower()
	{
		return _elementPower;
	}

	/**
	 * @return Returns the castRange.
	 */
	public final int getCastRange()
	{
		return _castRange;
	}

	public final int getAOECastRange()
	{
		return Math.max(_castRange, _skillRadius);
	}

	/**
	 * @return Returns the hpConsume.
	 */
	public final int getHpConsume()
	{
		return _hpConsume;
	}

	/**
	 * @return Returns the id.
	 */
	public final int getId()
	{
		return _id;
	}

	public final int getDisplayId()
	{
		return _displayId;
	}

	public final int getDisplayLevel()
	{
		return _displayLevel;
	}

	public int getMinPledgeClass()
	{
		return _minPledgeClass;
	}

	/**
	 * @return Returns the itemConsume.
	 */
	public final int[] getItemConsumeCounts()
	{
		return _itemConsumeCounts;
	}

	/**
	 * @return Returns the itemConsumeId.
	 */
	public final int[] getItemConsumeIDs()
	{
		return _itemConsumeIDs;
	}

	public final int getItemConsumeId()
	{
		return _itemConsumeIDs[0];
	}

	/**
	 * использование имени потребляемой вещи вместо имени самого скилла
	 */
	public final boolean isHandler()
	{
		return _isItemHandler;
	}

	/**
	 * Проверяет, является ли скилл Item скиллом (аугментации)
	 *
	 * @return boolean true/false
	 */
	public boolean isItemSkill()
	{
		return _isItemSkill || _isTalismanSkill;
	}

	/**
	 * Проверяет, является ли скиллом Талисмана
	 *
	 * @return boolean true/false
	 */
	public boolean isTalismanSkill()
	{
		return _isTalismanSkill && isActive();
	}

	/** Возвращает true если скил пассивный или от предмета (они должны оставаться при трансформации) */
	public boolean isAllowInTransform()
	{
		return isItemSkill() || isPassive() || isCommon();
	}

	/**
	 * Является ли скилл общим
	 */
	public final boolean isCommon()
	{
		return _isCommon == null ? false : _isCommon;
	}

	public final int getReuseGroupId()
	{
		return _reuseGroupId;
	}

	public final GArray<Integer> getReuseGroup()
	{
		return _reuseGroups.get(_reuseGroupId);
	}

	/**
	 * @return Returns the level.
	 */
	public final int getLevel()
	{
		return _level;
	}

	public final short getBaseLevel()
	{
		return _baseLevel;
	}

	public final void setBaseLevel(final short baseLevel)
	{
		_baseLevel = baseLevel;
	}

	/**
	 * @return Returns true if skill is magic.
	 */
	public final boolean isMagic()
	{
		return _isMagic;
	}

	/**
	 * @return Returns the mpConsume as _mpInitialConsume + _mpConsume.
	 */
	public final int getMpConsumeAll()
	{
		return _mpInitialConsume + _mpConsume;
	}

	/**
	 * Возвращает количество манны требуемое перед кастом скилла
	 */
	public final int getMpInitialConsume()
	{
		return _mpInitialConsume;
	}

	/**
	 * Возвращает количество манны требуемое после каста скилла
	 */
	public final int getMpConsume()
	{
		return _mpConsume;
	}

	/**
	 * @return Returns the name.
	 */
	public final String getName()
	{
		return _name;
	}

	/**
	 * @return Returns the reuseDelay.
	 */
	public final int getReuseDelay()
	{
		return _reuseDelay;
	}

	/**
	 * Set the reuseDelay.
	 */
	public final void setReuseDelay(final int newReuseDelay)
	{
		_reuseDelay = newReuseDelay;
	}

	public final int getHitTime()
	{
		return _hitTime;
	}

	public final int getCoolTime()
	{
		return _coolTime;
	}

	public final int getSkillInterruptTime()
	{
		return _skillInterruptTime;
	}

	public final int getSkillRadius()
	{
		return _skillRadius;
	}

	public void setOperateType(final SkillOpType type)
	{
		_operateType = type;
	}

	/** Возвращает шанс */
	public double getChanceForAction(final TriggerActionType action)
	{
		return _triggerActions.get(action)[0];
	}

	/** Возвращает минимальный урон, при котором возможно действие */
	public double getMinDamageForAction(final TriggerActionType action)
	{
		return _triggerActions.get(action)[1];
	}

	public Map<TriggerActionType, double[]> getTriggerActions()
	{
		return _triggerActions;
	}

	public final boolean isOnAction()
	{
		return _operateType == SkillOpType.OP_ON_ACTION;
	}

	public final boolean isActive()
	{
		return _operateType == SkillOpType.OP_ACTIVE;
	}

	public final boolean isPassive()
	{
		return _operateType == SkillOpType.OP_PASSIVE;
	}

	public final boolean isLikePassive()
	{
		return _operateType == SkillOpType.OP_PASSIVE || _operateType == SkillOpType.OP_ON_ACTION;
	}

	public final boolean isToggle()
	{
		return _operateType == SkillOpType.OP_TOGGLE;
	}

	public void setCastRange(final int castRange)
	{
		_castRange = castRange;
	}

	public void setHitTime(final int hitTime)
	{
		_hitTime = hitTime;
	}

	public void setHpConsume(final int hpConsume)
	{
		_hpConsume = hpConsume;
	}

	public void setIsMagic(final boolean isMagic)
	{
		_isMagic = isMagic;
	}

	public void setMagicLevel(final byte newlevel)
	{
		_magicLevel = newlevel;
	}

	public void setMpInitialConsume(final int mpInitialConsume)
	{
		_mpInitialConsume = mpInitialConsume;
	}

	public void setMpConsume(final int mpConsume)
	{
		_mpConsume = mpConsume;
	}

	public void setName(final String name)
	{
		_name = name;
	}

	public void setOverhit(final boolean isOverhit)
	{
		_isOverhit = isOverhit;
	}

	public final void setPower(final double power)
	{
		_power = power;
	}

	public void setSkillInterruptTime(final int skillInterruptTime)
	{
		_skillInterruptTime = skillInterruptTime;
	}

	public final int getWeaponsAllowed()
	{
		return _weaponsAllowed;
	}

	public final boolean getCanLearn(final ClassId cls)
	{
		return _canLearn == null || _canLearn.contains(cls);
	}

	public final boolean canTeachBy(final int npcId)
	{
		return _teachers == null || _teachers.binarySearch(npcId) >= 0;
	}

	public final boolean getWeaponDependancy(final L2Character activeChar)
	{
		// check to see if skill has a weapon dependency.
		if(_weaponsAllowed == 0)
			return true;

		if(activeChar.getActiveWeaponInstance() != null && activeChar.getActiveWeaponItem() != null && (activeChar.getActiveWeaponItem().getItemType().mask() & _weaponsAllowed) != 0)
			return true;
		// can be on the secondary weapon
		if(activeChar.getSecondaryWeaponInstance() != null && activeChar.getSecondaryWeaponItem() != null && (activeChar.getSecondaryWeaponItem().getItemType().mask() & _weaponsAllowed) != 0)
			return true;

		if(isActive())
		{
			final StringBuffer skillmsg = new StringBuffer();
			skillmsg.append(_name);
			skillmsg.append(" can only be used with weapons of type ");
			for(final WeaponType wt : WeaponType.values())
				if((wt.mask() & _weaponsAllowed) != 0)
					skillmsg.append(wt).append('/');
			skillmsg.setCharAt(skillmsg.length() - 1, '.');
			activeChar.sendMessage(skillmsg.toString());
		}

		return false;
	}

	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(activeChar.isDead())
			return false;

		if(target != null && (activeChar.getReflection() != target.getReflection() || target != activeChar && target.isInvisible() && getId() != SKILL_DETECTION))
		{
			activeChar.sendPacket(Msg.CANNOT_SEE_TARGET);
			return false;
		}

		if(!getWeaponDependancy(activeChar))
			return false;

		if(first && activeChar.isSkillDisabled(this))
		{
			// не отображаем реюз для триггерных скилов
			if(!isOnAction())
				activeChar.sendReuseMessage(this);
			return false;
		}

		double mpCost = 0;
		if(isDanceSong())
		{
			int danceCount = activeChar.getEffectList().getDanceCount();
			danceCount = Math.min(danceCount, 8);
			mpCost = activeChar.calcStat(Stats.MP_DANCE_SKILL_CONSUME, _mpInitialConsume + _mpConsume + 30 * danceCount, null, this);
		}
		else
			mpCost = isMagic() ? activeChar.calcStat(Stats.MP_MAGIC_SKILL_CONSUME, _mpInitialConsume + _mpConsume, null, this) : activeChar.calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, _mpInitialConsume + _mpConsume, null, this);

		if(first && activeChar.getCurrentMp() < mpCost)
		{
			activeChar.sendPacket(Msg.NOT_ENOUGH_MP);
			return false;
		}

		if(activeChar.getCurrentHp() < _hpConsume + 1)
		{
			activeChar.sendPacket(Msg.NOT_ENOUGH_HP);
			return false;
		}

		if(!_isItemHandler && !_isAltUse && activeChar.isMuted(this))
			return false;

		if(_soulsConsume > activeChar.getConsumedSouls())
		{
			activeChar.sendPacket(Msg.THERE_IS_NOT_ENOUGHT_SOUL);
			return false;
		}

		if(activeChar.getIncreasedForce() < _condCharges || activeChar.getIncreasedForce() < _numCharges)
		{
			activeChar.sendPacket(Msg.YOUR_FORCE_HAS_REACHED_MAXIMUM_CAPACITY);
			return false;
		}

		final L2Player player = activeChar.getPlayer();
		if(player != null)
		{
			if(!isOlympiadEnabled() && player.isInOlympiadMode())
			{
				player.sendPacket(Msg.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT);
				return false;
			}

			if(player.isInFlyingTransform() && _isItemHandler && !flyingTransformUsage())
			{
				player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(getItemConsumeIDs()[0]));
				return false;
			}

			if(player.isInVehicle())
			{
				// На воздушных кораблях можно использовать скилы-хэндлеры
				if(player.getVehicle().isAirShip() && !_isItemHandler)
					return false;

				// С морских кораблей можно ловить рыбу
				if(player.getVehicle().isShip() && !(this instanceof Fishing || this instanceof ReelingPumping))
					return false;
			}

			if(player.inObserverMode())
			{
				activeChar.sendPacket(Msg.OBSERVERS_CANNOT_PARTICIPATE);
				return false;
			}

			// проверяем условия 'SKILL_USE'
			if(!player.getRestrictions().test(PlayerRestrictionType.SKILL_USE, player, target, this, null))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addSkillName(_id, 1));
				return false;
			}

			// If summon siege golem, hog cannon, Swoop Cannon, check its ok to place the flag
			if(_id == 13 || _id == 299 || _id == 448)
			{
				final Siege siege = SiegeManager.getSiege(player, true);
				if(siege == null)
				{
					player.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
					return false;
				}
				else if(!siege.isInProgress())
				{
					player.sendPacket(Msg.YOU_ARE_NOT_IN_SIEGE);
					return false;
				}
				else if(player.getClanId() != 0 && siege.getAttackerClan(player.getClan()) == null)
				{
					player.sendPacket(Msg.OBSERVATION_IS_ONLY_POSSIBLE_DURING_A_SIEGE);
					return false;
				}
			}

			if(first && _itemConsumeCounts.length > 0)
				for(int i = 0; i < _itemConsumeCounts.length; i++)
				{
					Inventory inv = ((L2Playable) activeChar).getInventory();
					if(inv == null)
						inv = player.getInventory();
					final L2ItemInstance requiredItems = inv.findItemByItemId(_itemConsumeIDs[i]);
					if(requiredItems == null || requiredItems.getCount() < _itemConsumeCounts[i])
					{
						if(activeChar == player)
							player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
						return false;
					}
				}

			if(player.isFishing() && _id != 1312 && _id != 1313 && _id != 1314)
			{
				player.sendPacket(Msg.ONLY_FISHING_SKILLS_ARE_AVAILABLE);
				return false;
			}

            if(player.isTerritoryFlagEquipped() && _id!=847)
            {
            	
                player.sendMessage(player.isLangRus() ? "Вы не может атаковать и использовать умения." : "The ward capturing skill can be used instead.");
                return false;
            }
		}

		// You cannot jump while rooted right ;)
		// Warp (628) && Shadow Step (821) can be used while rooted
		if(getFlyType() != FlyType.NONE && getId() != 628 && getId() != 821)
			if((getFlyType() == FlyType.DUMMY && isFlyToBack() || getFlyType() != FlyType.DUMMY) && (activeChar.isImobilised() || activeChar.isRooted()))
			{
				if(activeChar.isPlayer())
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addSkillName(_id, 1));
				return false;
			}

		// Fly скиллы нельзя использовать слишком близко
		if(first && target != null && target != activeChar && getFlyType() == FlyType.CHARGE && activeChar.isInRange(target.getLoc(), Math.min(150, getFlyRadius())))
		{
			if(activeChar.isPlayer())
				activeChar.sendPacket(Msg.THERE_IS_NOT_ENOUGH_SPACE_TO_MOVE_THE_SKILL_CANNOT_BE_USED);
			return false;
		}

		final SystemMessage msg = checkTarget(activeChar, target, target, forceUse, first);
		if(msg != null && activeChar.getPlayer() != null)
		{
			activeChar.getPlayer().sendPacket(msg);
			return false;
		}

		if(_preCondition == null || _preCondition.length == 0)
			return true;

		if(first)
		{
			final Env env = new Env();
			env.character = activeChar;
			env.skill = this;
			env.target = target;

			for(final Condition с : _preCondition)
				if(с != null && !с.test(env))
				{
					// Не отправляем сообщение при неудачной проверки различных условий для скила, скил срабатывает при действии.
					if(!isOnAction())
					{
						final String cond_msg = с.getMessage();
						final int cond_msgId = с.getMessageId();
						if(cond_msgId != 0)
						{
							final SystemMessage sm = new SystemMessage(cond_msgId);
							if(с.isAddName())
								sm.addSkillName(_id, 1);
							activeChar.sendPacket(sm);
						}
						else if(cond_msg != null)
							activeChar.sendMessage(cond_msg);
					}
					return false;
				}
		}
		return true;
	}

	public final SystemMessage checkTarget(final L2Character activeChar, final L2Character target, final L2Character aimingTarget, final boolean forceUse, final boolean first)
	{
		// _log.info("<<-checkTarget->>");
		// _log.info("skill: " + this);
		// _log.info("activeChar: " + activeChar);
		// _log.info("target: " + target);
		// _log.info("aimingTarget: " + aimingTarget);
		// _log.info("forceUse: " + forceUse);

		if(target == activeChar && isNotTargetAoE()/* || target == activeChar.getPet() && _targetType == SkillTargetType.TARGET_PET_AURA */)
			return null;
		if(target == null || isOffensive() && target == activeChar)
			return Msg.TARGET_IS_INCORRECT;

		// Массовые атаки должны попадать по дагерам в Hide. Если потребуется убрать - раскомментировать. || target != activeChar && target.isInvisible() && getId() != SKILL_DETECTION)
		if(activeChar.getReflection() != target.getReflection())
			return Msg.CANNOT_SEE_TARGET;

		// Попадает ли цель в радиус действия в конце каста
		if(!first && target != activeChar && target == aimingTarget && getCastRange() > 0 && getCastRange() != Short.MAX_VALUE && !activeChar.isInRange(target.getLoc(), getCastRange() + (getCastRange() < 200 ? 400 : 500)))
			return Msg.YOUR_TARGET_IS_OUT_OF_RANGE;

		// Для этих скиллов дальнейшие проверки не нужны
		if(_skillType == SkillType.TAKECASTLE || _skillType == SkillType.TAKEFORT || _skillType == SkillType.TAKEFLAG)
			return null;

		// Конусообразные скиллы
		if(!first && target != activeChar && (_targetType == SkillTargetType.TARGET_MULTIFACE || _targetType == SkillTargetType.TARGET_MULTIFACE_AURA || _targetType == SkillTargetType.TARGET_TUNNEL) && (_isBehind ? activeChar.isInFront(target, 120) : !activeChar.isInFront(target, 60)))
			return Msg.YOUR_TARGET_IS_OUT_OF_RANGE;

		// Проверка на каст по трупу
		if(target.isDead() != _isCorpse && _targetType != SkillTargetType.TARGET_AREA_AIM_CORPSE || _isUndeadOnly && !target.isUndead())
			return Msg.INCORRECT_TARGET;

		if(target.isMonster() && ((L2MonsterInstance) target).isDying())
			return Msg.INCORRECT_TARGET;

		if(_targetType != SkillTargetType.TARGET_UNLOCKABLE && target.isDoor() && !((L2DoorInstance) target).isAttackable(activeChar))
			return Msg.INCORRECT_TARGET;

		// Для различных бутылок, и для скилла кормления, дальнейшие проверки ненужны
		if(_isAltUse || _skillType == SkillType.BEAST_FEED || _targetType == SkillTargetType.TARGET_UNLOCKABLE || _targetType == SkillTargetType.TARGET_CHEST)
			return null;

		if(activeChar.isPlayable())
		{
			final L2Player player = activeChar.getPlayer();
			if(player == null)
				return Msg.TARGET_IS_INCORRECT;

			// Запрет на атаку мирных NPC в осадной зоне на TW. Иначе таким способом набивают очки.
			if(player.getTerritorySiege() > -1 && target.isNpc() && !(target instanceof L2TerritoryFlagInstance) && !(target.getAI() instanceof DefaultAI) && player.isInZone(ZoneType.Siege))
				return Msg.INCORRECT_TARGET;

			if(target.isPlayable())
			{
				if(isPvM())
					return Msg.TARGET_IS_INCORRECT;

				final L2Player pcTarget = target.getPlayer();
				if(pcTarget == null)
					return Msg.TARGET_IS_INCORRECT;

				if(player.isInZone(ZoneType.epic) != pcTarget.isInZone(ZoneType.epic))
					return Msg.TARGET_IS_INCORRECT;

				if(pcTarget.isInOlympiadMode() && (!player.isInOlympiadMode() || player.getOlympiadGameId() != pcTarget.getOlympiadGameId())) // На всякий случай
					return Msg.TARGET_IS_INCORRECT;

				if(player.getTeam() > 0 && player.isChecksForTeam() && pcTarget.getTeam() == 0) // Запрет на атаку/баф участником эвента незарегистрированного игрока
					return Msg.TARGET_IS_INCORRECT;
				if(pcTarget.getTeam() > 0 && pcTarget.isChecksForTeam() && player.getTeam() == 0) // Запрет на атаку/баф участника эвента незарегистрированным игроком
					return Msg.TARGET_IS_INCORRECT;

				if(isOffensive())
				{
					if(player.isInOlympiadMode() && !player.isOlympiadCompStart()) // Бой еще не начался
						return Msg.INCORRECT_TARGET;

					if(player.isInOlympiadMode() && player.isOlympiadCompStart() && player.getOlympiadSide() == pcTarget.getOlympiadSide()) // Свою команду атаковать нельзя
					{
						// разрешаем атаку суммона скилами
						if(!isDebuff() && oneTarget() && target == player.getPet())
							return null;
						return Msg.TARGET_IS_INCORRECT;
					}

					if(player.getTeam() > 0 && player.isChecksForTeam() && pcTarget.getTeam() > 0 && pcTarget.isChecksForTeam() && player.getTeam() == pcTarget.getTeam()) // Свою команду атаковать нельзя
					{
						// разрешаем атаку суммона скилами
						if(!isDebuff() && oneTarget() && target == player.getPet())
							return null;
						return Msg.TARGET_IS_INCORRECT;
					}

					if(isAoE() && getCastRange() < Integer.MAX_VALUE && !GeoEngine.canSeeTarget(activeChar, target, activeChar.isFlying()))
						return Msg.CANNOT_SEE_TARGET;

					if(activeChar.isInZoneBattle() != target.isInZoneBattle() && !player.getPlayerAccess().PeaceAttack)
						return Msg.YOU_CANNOT_ATTACK_THE_TARGET_IN_THE_PEACE_ZONE;

					if((activeChar.isInZonePeace() || target.isInZonePeace()) && !player.getPlayerAccess().PeaceAttack)
					{
						if(Config.ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE && target.getKarma() > 0 && player.getKarma() == 0)
							return null;
						return Msg.YOU_CANNOT_ATTACK_THE_TARGET_IN_THE_PEACE_ZONE;
					}

					if(activeChar.isInZoneBattle())
					{
						if(!forceUse && player.isInSameParty(pcTarget))
							return Msg.INCORRECT_TARGET;
						return null; // Остальные условия на аренах и на олимпиаде проверять не требуется
					}

					// Только враг и только если он еше не проиграл.
					final Duel duel1 = player.getDuel();
					final Duel duel2 = pcTarget.getDuel();
					if(player != pcTarget && duel1 != null && duel1 == duel2)
					{
						if(duel1.getTeamForPlayer(pcTarget) == duel1.getTeamForPlayer(player))
							return Msg.INCORRECT_TARGET;
						if(duel1.getDuelState(player.getStoredId()) != Duel.DuelState.Fighting)
							return Msg.INCORRECT_TARGET;
						if(duel1.getDuelState(pcTarget.getStoredId()) != Duel.DuelState.Fighting)
							return Msg.INCORRECT_TARGET;
						return null;
					}

					if(isPvpSkill() || !forceUse || isAoE())
					{
						if(player == pcTarget)
							return Msg.INCORRECT_TARGET;
						if(player.isInSameParty(pcTarget))
							return Msg.INCORRECT_TARGET;
						if(player.isInSameClan(pcTarget))
							return Msg.INCORRECT_TARGET;
						if(player.isInSameAlly(pcTarget))
							return Msg.INCORRECT_TARGET;
						if(player.getDuel() != null && pcTarget.getDuel() != player.getDuel())
							return Msg.INCORRECT_TARGET;
					}

					if(activeChar.isInZone(ZoneType.Siege) && target.isInZone(ZoneType.Siege))
					{
						// своих во время ТВ бить нельзя
						if(player.getTerritorySiege() > -1 && player.getTerritorySiege() == pcTarget.getTerritorySiege())
							return Msg.INCORRECT_TARGET;

						final L2Clan clan1 = player.getClan();
						final L2Clan clan2 = pcTarget.getClan();
						if(clan1 == null || clan2 == null)
							return null;
						if(clan1.getSiege() == null || clan2.getSiege() == null)
							return null;
						if(clan1.getSiege() != clan2.getSiege())
							return null;
						if(clan1.isDefender() && clan2.isDefender())
							return Msg.INCORRECT_TARGET;
						if(clan1.getSiege().isMidVictory())
							return null;
						if(clan1.isAttacker() && clan2.isAttacker())
							return Msg.INCORRECT_TARGET;
						return null;
					}

					if(player.atMutualWarWith(pcTarget))
						return null;
					// Защита от развода на флаг с копьем
					if(!forceUse && player.getPvpFlag() == 0 && pcTarget.getPvpFlag() != 0 && aimingTarget != target)
						return Msg.INCORRECT_TARGET;
					if(pcTarget.getPvpFlag() != 0)
						return null;
					if(pcTarget.getKarma() > 0)
						return null;
					// атакующие скилы которые могут использоваться без флага
					if(forceUse && !isPvpSkill() && (isForceUse() || !isAoE() || aimingTarget == target))
						return null;

					return Msg.INCORRECT_TARGET;
				}

				if(pcTarget == player)
					return null;
				if(!forceUse && player.isInOlympiadMode() && player.getOlympiadSide() != pcTarget.getOlympiadSide()) // Чужой команде помогать нельзя	
					return Msg.INCORRECT_TARGET;
				if(player.getTeam() > 0 && player.isChecksForTeam() && pcTarget.getTeam() > 0 && pcTarget.isChecksForTeam() && player.getTeam() != pcTarget.getTeam()) // Чужой команде помогать нельзя
					return Msg.INCORRECT_TARGET;
				if(!activeChar.isInZoneBattle() && target.isInZoneBattle())
					return Msg.INCORRECT_TARGET;
				if(activeChar.isInZonePeace() && !target.isInZonePeace())
					return Msg.INCORRECT_TARGET;
				if(forceUse)
					return null;
				if(player.getDuel() != null && pcTarget.getDuel() != player.getDuel())
					return Msg.INCORRECT_TARGET;
				if(player != pcTarget && player.getDuel() != null && pcTarget.getDuel() != null && player.getDuel() == pcTarget.getDuel() && player.getDuel().getTeamForPlayer(player) != pcTarget.getDuel().getTeamForPlayer(pcTarget))					
				return Msg.INCORRECT_TARGET;
				if(player.isInSameParty(pcTarget))
					return null;
				if(player.isInSameClan(pcTarget))
					return null;
				if(player.atMutualWarWith(pcTarget))
					return Msg.INCORRECT_TARGET;
				if(pcTarget.getPvpFlag() != 0)
					return Msg.INCORRECT_TARGET;
				if(pcTarget.getKarma() > 0)
					return Msg.INCORRECT_TARGET;
				return null;
			}
		}

		if(isAoE() && isOffensive() && getCastRange() < Integer.MAX_VALUE && !GeoEngine.canSeeTarget(activeChar, target, activeChar.isFlying()))
			return Msg.CANNOT_SEE_TARGET;
		if(!forceUse && !isOffensive() && target.isAutoAttackable(activeChar))
			return Msg.INCORRECT_TARGET;
		if(!forceUse && isOffensive() && !target.isAutoAttackable(activeChar))
			return Msg.INCORRECT_TARGET;
		if(!target.isAttackable(activeChar))
			return Msg.INCORRECT_TARGET;

		return null;
	}

	public final L2Character getAimingTarget(final L2Character activeChar, final L2Object obj)
	{
		L2Character target = obj != null && obj.isCharacter() ? (L2Character) obj : null;
		switch (_targetType)
		{
			case TARGET_ALLY:
			case TARGET_CLAN:
			case TARGET_PARTY:
			case TARGET_PARTY_CLAN:
			case TARGET_SELF:
				return activeChar;
			case TARGET_AURA:
			case TARGET_MULTIFACE_AURA:
			case TARGET_AURA_CORPSE:
				return activeChar;
			case TARGET_HOLY:
				return target != null && activeChar.isPlayer() && target.isArtefact() ? target : null;
			case TARGET_FLAGPOLE:
				return obj != null && activeChar.isPlayer() && obj.isStaticObject() && ((L2StaticObjectInstance) obj).getType() == 3 ? activeChar : null;
			case TARGET_UNLOCKABLE:
				return target != null && (target.isDoor() || target.isChest()) ? target : null;
			case TARGET_CHEST:
				return target != null && target.isChest() ? target : null;
			case TARGET_PET:
				target = activeChar.getPet() != null ? activeChar.getPet() : null;
				return target != null && target.isDead() == _isCorpse ? target : null;
			case TARGET_TYRANNOSAURUS:
				return target.isMonster() && ((L2MonsterInstance) target).getNpcId() == 22217 || ((L2MonsterInstance) target).getNpcId() == 22216 || ((L2MonsterInstance) target).getNpcId() == 22215 ? target : null;
			case TARGET_OWNER:
				if(activeChar.isSummon())
					target = activeChar.getPlayer();
				else
					return null;
				return target != null && target.isDead() == _isCorpse ? target : null;
			case TARGET_ENEMY_PET:
				if(target == null || target == activeChar.getPet() || !target.isPet())
					return null;
				return target;
			case TARGET_ENEMY_SUMMON:
				if(target == null || target == activeChar.getPet() || !target.isSummon())
					return null;
				return target;
			case TARGET_ENEMY_SERVITOR:
				if(target == null || target == activeChar.getPet() || !target.isSummon0())
					return null;
				return target;
			case TARGET_ONE:
				return target != null && target.isDead() == _isCorpse && !(target == activeChar && isOffensive()) && (!_isUndeadOnly || target.isUndead()) ? target : null;
			case TARGET_PARTY_ONE:
				if(target == null)
					return null;
				// self or self pet.
				if(target.getPlayer() != null && target.getPlayer().equals(activeChar))
					return target;
				// party member or party member pet.
				if(target.getPlayer() != null && activeChar.getPlayer() != null && activeChar.getPlayer().getParty() != null && activeChar.getPlayer().getParty().containsMember(target.getPlayer()) && target.isDead() == _isCorpse && !(target == activeChar && isOffensive()) && (!_isUndeadOnly || target.isUndead()))
					return target;
				return null;
			case TARGET_AREA:
			case TARGET_MULTIFACE:
			case TARGET_TUNNEL:
				return target != null && target.isDead() == _isCorpse && !(target == activeChar && isOffensive()) && (!_isUndeadOnly || target.isUndead()) ? target : null;
			case TARGET_AREA_AIM_CORPSE:
				return target != null && target.isDead() ? target : null;
			case TARGET_CORPSE:
				return target != null && target.isNpc() && target.isDead() ? target : null;
			case TARGET_CORPSE_PLAYER:
				return target != null && target.isPlayable() && target.isDead() ? target : null;
			case TARGET_SIEGE:
				return target != null && !target.isDead() && (target.isDoor() || target instanceof L2ControlTowerInstance) ? target : null;
			default:
				// activeChar.sendMessage("Target type of skill is not currently handled");
				// _log.info("Target type '" + _targetType + "' of skill " + this + " is not currently handled");
				return null;
		}
	}

	public L2Character[] getTargets(final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		return _targetType.getTargetList(this, activeChar, aimingTarget, forceUse);
	}

	public final Func[] getStatFuncs(final L2Character player)
	{
		if(_funcTemplates == null)
			return Func.EMPTY_FUNCTION_SET;
		final GArray<Func> funcs = new GArray<Func>();
		for(final FuncTemplate t : _funcTemplates)
		{
			final Env env = new Env();
			env.character = player;
			env.skill = this;
			final Func f = t.getFunc(env, this); // skill is owner
			if(f != null)
				funcs.add(f);
		}
		if(funcs.size() == 0)
			return Func.EMPTY_FUNCTION_SET;
		return funcs.toArray(new Func[funcs.size()]);
	}

	/**
	 * @return разрешено ли использовать скил на олимпиаде
	 */
	public boolean isOlympiadEnabled()
	{
		return _isOlympiadEnabled || !_isItemHandler && !_isAltUse;
	}

	/**
	 * Создает и применяет эффекты скилла. Выполняется в отдельном потоке.
	 *
	 * @param effector
	 *            тот кто наносит эффект
	 * @param effected
	 *            тот на кого наносят эффект
	 * @param calcChance
	 *            расчитывать шанс прохождения
	 * @param applyOnCaster
	 *            применять эффект на кастующего
	 */
	public final void getEffects(final L2Character effector, final L2Character effected, final boolean calcChance, final boolean applyOnCaster)
	{
		if(isPassive() || !hasEffects() || effector == null || effected == null)
			return;

		// Mystic Immunity Makes a target temporarily immune to buffs/debuffs
		if(_id != 4215 && _id != 4515) // скилы RAID_CURSE всё равно накладываем
			if(effected.getEffectList().getFirstEffect(SKILL_MYSTIC_IMMUNITY) != null)
			{
				if(effector.isPlayer())
					effector.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addName(effected).addSkillName(getDisplayId(), getDisplayLevel()));
				return;
			}

		// No effect on invulnerable characters unless they cast it themselves.
		if((/* TODO effected.isEffectImmune() || */effected.isInvul() && isOffensive()) && effector != effected)
		{
			if(effector.isPlayer())
				effector.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addName(effected).addSkillName(getDisplayId(), getDisplayLevel()));
			return;
		}

		// No effect on doors/walls
		if(effected.isDoor())
			return;

		final int sps = effector.getChargedSpiritShot();

		L2GameThreadPools.getInstance().executeGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				boolean success = false;
				boolean skillMastery = false;
				// Check for skill mastery duration time increase
				if(effector.getSkillMastery(getId()) == 2)
				{
					skillMastery = true;
					effector.removeSkillMastery(getId());
				}

				for(final EffectTemplate et : getEffectTemplates())
				{
					if(applyOnCaster != et._applyOnCaster || et._ticks <= 0)
						continue;

					final int activateRate = et._activateRate != 0 ? et._activateRate : _activateRate;

					L2Character target = et._applyOnCaster ? effector : effected;

					if(et._stackOrder == -1)
						if(et._stackType != StringUtil.NO_STACK)
						{
							if(target.getEffectList().getFirstEffect(et._stackType) != null)
								continue;
						}
						else if(target.getEffectList().getFirstEffect(getId()) != null)
							continue;

					if(target.isRaid() && et.getEffectType().isRaidImmune())
						continue;

					if(effected.isDebuffImmune() && isOffensive() && et.getPeriod() > 0 && effector != effected)
						continue;

					if(isBlockedByChar(target, et))
						continue;

					final Env env = new Env(effector, target, L2Skill.this);
					if(calcChance && !et._applyOnCaster)
					{
						env.value = activateRate;
						if(!Formulas.calcSkillSuccess(env, et.getEffectType().getResistType(), et.getEffectType().getAttackType(), sps))
							continue;
					}

					if(target != effector && isReflectable() && et._isReflectable && isOffensive() && !effector.isTrap())
						if(Rnd.chance(target.calcStat(isMagic() ? Stats.REFLECT_MAGIC_DEBUFF : Stats.REFLECT_PHYSIC_DEBUFF, 0, effector, L2Skill.this)))
						{
							target.sendPacket(new SystemMessage(SystemMessage.COUNTERED_S1_ATTACK).addName(effector));
							effector.sendPacket(new SystemMessage(SystemMessage.S1_DODGES_ATTACK).addName(target));
							target = effector;
							env.target = target;
						}

					if(success) // больше это значение не используется, поэтому заюзываем его для ConditionFirstEffectSuccess
						env.value = Integer.MAX_VALUE;

					final L2Effect e = et.getEffect(env);
					if(e != null)
					{
						if(activateRate > 0)
							success = true;

						// Эффекты однократного действия не шедулятся, а применяются немедленно
						// Как правило это побочные эффекты для скиллов моментального действия
						if(e.isOneTime())
						{
							if(e.checkCondition())
							{
								e.onStart();
								e.onActionTime();
								e.onExit();
							}
						}
						else
						{
							int count = e.getCount();
							long period = e.getPeriod();

							// Check for skill mastery duration time increase
							if(skillMastery)
								if(count > 1)
									count *= 2;
								else
									period *= 2;

							// Считаем влияние резистов
							if(!et._applyOnCaster && isOffensive() && !isIgnoreResists() && !effector.isRaid())
							{
								double res = 0;
								if(et.getEffectType().getResistType() != null)
									res += effected.calcStat(et.getEffectType().getResistType(), 0, effector, L2Skill.this);
								if(et.getEffectType().getAttackType() != null)
									res -= effector.calcStat(et.getEffectType().getAttackType(), 0, effected, L2Skill.this);
								res += effected.calcStat(Stats.DEBUFF_RECEPTIVE, 0, effector, L2Skill.this);

								if(res != 0)
								{
									double mod = 1 + Math.abs(0.01 * res);
									if(res > 0)
										mod = 1. / mod;

									if(count > 1)
										count = (int) Math.floor(Math.max(count * mod, 1));
									else
										period = (long) Math.floor(Math.max(period * mod, 1));
								}
							}

							e.setCount(count);
							e.setPeriod(period);
							e.schedule();
						}
					}
				}

				if(calcChance)
					if(success)
						effector.sendPacket(new SystemMessage(SystemMessage.S1_HAS_SUCCEEDED).addSkillName(getDisplayId(), getDisplayLevel()));
					else
						effector.sendPacket(new SystemMessage(SystemMessage.S1_HAS_FAILED).addSkillName(getDisplayId(), getDisplayLevel()));
			}
		});
	}

	public final void getEffectsSelf(final L2Character effected, final long time)
	{
		if(isPassive() || !hasEffects() || effected == null)
			return;

		// Mystic Immunity Makes a target temporarily immune to buffs/debuffs
		if(effected.getEffectList().getFirstEffect(SKILL_MYSTIC_IMMUNITY) != null)
			return;

		for(final EffectTemplate et : _effectTemplates)
		{
			if(et._ticks <= 0)
				continue;

			if(et._stackOrder == -1)
				if(et._stackType != StringUtil.NO_STACK)
				{
					if(effected.getEffectList().getFirstEffect(et._stackType) != null)
						continue;
				}
				else if(effected.getEffectList().getFirstEffect(getId()) != null)
					continue;

			if(isBlockedByChar(effected, et))
				continue;

			final Env env = new Env(effected, effected, L2Skill.this);
			final L2Effect e = et.getEffect(env);
			if(e != null)
				// Эффекты однократного действия не шедулятся, а применяются немедленно
				// Как правило это побочные эффекты для скиллов моментального действия
				if(e.isOneTime())
				{
					if(e.checkCondition())
					{
						e.onStart();
						e.onActionTime();
						e.onExit();
					}
				}
				else
				{
					if(time > 0)
						e.setPeriod(time);
					e.schedule();
				}
		}
		effected.updateStats();
		effected.updateEffectIcons();
	}

	public boolean isBlockedByChar(final L2Character effected, final EffectTemplate et)
	{
		if(et._funcTemplates == null)
			return false;
		for(final FuncTemplate func : et._funcTemplates)
			if(func != null && effected.checkBlockedStat(func._stat))
				return true;
		return false;
	}

	public final void attach(final FuncTemplate f)
	{
		if(_funcTemplates == null)
			_funcTemplates = new FuncTemplate[] { f };
		else
		{
			final int len = _funcTemplates.length;
			final FuncTemplate[] tmp = new FuncTemplate[len + 1];
			System.arraycopy(_funcTemplates, 0, tmp, 0, len);
			tmp[len] = f;
			_funcTemplates = tmp;
		}
	}

	public final void attach(final EffectTemplate effect)
	{
		if(_effectTemplates == null)
			_effectTemplates = new EffectTemplate[] { effect };
		else
		{
			final int len = _effectTemplates.length;
			final EffectTemplate[] tmp = new EffectTemplate[len + 1];
			System.arraycopy(_effectTemplates, 0, tmp, 0, len);
			tmp[len] = effect;
			_effectTemplates = tmp;
		}
	}

	public final void attach(final Condition c)
	{
		_preCondition = ArrayUtil.arrayAdd(_preCondition, c, Condition.class);
	}

	@Override
	public String toString()
	{
		return _toString;
	}

	public int getAggroPoints()
	{
		return _aggroPoints;
	}

	public NextAction getNextAction()
	{
		return _nextAction;
	}

	public boolean isCorpse()
	{
		return _isCorpse;
	}

	public boolean isUndeadOnly()
	{
		return _isUndeadOnly;
	}

	public int getDelayedEffect()
	{
		return _delayedEffect;
	}

	@Override
	public int hashCode()
	{
		return hashCode;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;

		return hashCode() == ((L2Skill) obj).hashCode();
	}

	public EffectTemplate[] getEffectTemplates()
	{
		return _effectTemplates;
	}

	public boolean hasEffects()
	{
		return _effectTemplates != null && _effectTemplates.length > 0;
	}

	public L2Effect getSameByStackType(final Queue<L2Effect> queue)
	{
		if(_effectTemplates == null)
			return null;

		L2Effect ret;
		for(final EffectTemplate et : _effectTemplates)
			if(et != null && (ret = et.getSameByStackType(queue)) != null)
				return ret;
		return null;
	}

	public L2Effect getSameByStackType(final L2Character actor)
	{
		return getSameByStackType(actor.getEffectList().getEffects());
	}

	public boolean isSaveable()
	{
		if(!Config.ALT_SAVE_UNSAVEABLE && (isDanceSong() || _name.startsWith("Herb of")))
			return false;
		return _isSaveable == null ? true : _isSaveable;
	}

	/**
	 * Скилл подлежит удалению из ярлыков при валидейте, в случае отсутсвия данного скила у чара
	 *
	 * @return true, если скилл подлежит удалению
	 */
	public boolean isValidateable()
	{
		return _isValidateable;
	}

	public int getSoulsConsume()
	{
		return _soulsConsume;
	}

	public AddedSkill[] getAddedSkills()
	{
		return _addedSkills == null ? ArrayUtil.EMPTY_ADDED_SKILLS : _addedSkills;
	}

	public L2Skill getFirstAddedSkill()
	{
		if(_addedSkills == null)
			return null;
		return _addedSkills[0].getSkill();
	}

	public int getNpcId()
	{
		return _npcId;
	}

	public int getNegateSkill()
	{
		return _negateSkill;
	}

	public int getNegatePower()
	{
		return _negatePower;
	}

	public int getNumCharges()
	{
		return _numCharges;
	}

	public int getMatak()
	{
		return _mAtak;
	}

	public boolean isBehind()
	{
		return _isBehind;
	}

	public boolean isHideStartMessage()
	{
		return _hideStartMessage;
	}

	public boolean isHideUseMessage()
	{
		return _hideUseMessage;
	}

	/**
	 * Может ли скилл тратить шоты, для хендлеров всегда false
	 */
	public boolean isSSPossible()
	{
		return Boolean.TRUE.equals(_isUseSS) || _isUseSS == null && !_isItemHandler && !isDanceSong() && isActive() && !(getTargetType() == SkillTargetType.TARGET_SELF && !isMagic());
	}

	public boolean isSoulBoost()
	{
		return _isSoulBoost;
	}

	public boolean isChargeBoost()
	{
		return _isChargeBoost;
	}

	public boolean isUsingWhileCasting()
	{
		return _isUsingWhileCasting;
	}

	public boolean isDeathlink()
	{
		return _deathlink;
	}

	public int getCancelTarget()
	{
		return _cancelTarget;
	}

	public boolean isSkillInterrupt()
	{
		return _skillInterrupt;
	}

	public boolean isNotUsedByAI()
	{
		return _isNotUsedByAI;
	}

	public boolean isHealAggro()
	{
		return _isHealAggro == null ? true : _isHealAggro;
	}

	public final int getTransformId()
	{
		return _transformId;
	}

	public FlyType getFlyType()
	{
		return _flyType;
	}

	public int getFlyRadius()
	{
		return _flyRadius;
	}

	public boolean isFlyToBack()
	{
		return _flyToBack == null ? false : _flyToBack;
	}

	public int getSymbolId()
	{
		return _symbolId;
	}

	public int getEnchantLevelCount()
	{
		return _enchantLevelCount;
	}

	public void setEnchantLevelCount(final int count)
	{
		_enchantLevelCount = count;
	}

	public boolean flyingTransformUsage()
	{
		return _flyingTransformUsage;
	}

	public final int getHitCounts()
	{
		return _hitCount;
	}

	public final int[] getHitTimings()
	{
		return _hitTimings;
	}

	public final boolean is7Signs()
	{
		if(getId() > 4360 && getId() < 4367)
			return true;
		return false;
	}

	public boolean isBaseTransformation() // Inquisitor, Vanguard, Final Form...
	{
		return _id >= 810 && _id <= 813 || _id >= 1520 && _id <= 1522 || _id == 538;
	}

	public boolean isSummonerTransformation() // Spirit of the Cat etc
	{
		return _id >= 929 && _id <= 931;
	}

	/**
	 * @param actor
	 * @param target
	 * @return
	 */
	public double getSimpleDamage(final L2Character attacker, final L2Character target)
	{
		if(isMagic())
		{
			// магический урон
			final double mAtk = attacker.getMAtk(target, this);
			final double mdef = target.getMDef(null, this);
			final double power = getPower();
			final int sps = attacker.getChargedSpiritShot() > 0 && isSSPossible() ? attacker.getChargedSpiritShot() * 2 : 1;
			return 91 * power * Math.sqrt(sps * mAtk) / mdef;
		}
		// физический урон
		final double pAtk = attacker.getPAtk(target);
		final double pdef = target.getPDef(attacker);
		final double power = getPower();
		final int ss = attacker.getChargedSoulShot() && isSSPossible() ? 2 : 1;
		return ss * (pAtk + power) * 70. / pdef;
	}

	/**
	 * @return reuse skill time for monster
	 */
	public int getReuseForMonsters()
	{
		int min = 1000;
		switch (_skillType)
		{
			case PARALYZE:
			case DEBUFF:
			case CANCEL:
			case NEGATE_EFFECTS:
			case NEGATE_STATS:
			case STEAL_BUFF:
				min = 10000;
				break;
			case MUTE:
			case ROOT:
			case SLEEP:
			case STUN:
				min = 5000;
				break;
			default:
				break;
		}
		return Math.max(Math.max(_hitTime + _coolTime, _reuseDelay), min);
	}

	public boolean isReflectable()
	{
		return _isReflectable;
	}

	/**
	 * Return true if skill should ignore all resistances
	 */
	public final boolean isIgnoreResists()
	{
		return _isIgnoreResists;
	}

	public boolean isNotAffectedByMute()
	{
		return _isNotAffectedByMute;
	}

	/**
	 * @return для скилов, которые не дают эффекта если подходящих целей нету
	 */
	public boolean isNoEffectWOTargets()
	{
		return _isNoEffectWithoutTargets;
	}

	public void setIcon(final String icon)
	{
		_icon = icon == null ? StringUtil.EMPTY_ICON : icon;
	}

	public String getIcon()
	{
		return _icon;
	}

	public final String getHtmlInfo()
	{
		final StringBuilder replyMSG = new StringBuilder();
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td>Name: " + _name + "</td></tr>");
		replyMSG.append("<tr><td>Id: " + _id + "</td></tr>");
		replyMSG.append("<tr><td>Level: " + _level + "</td></tr>");
		replyMSG.append("<tr><td>MagicLevel: " + _magicLevel + "</td></tr>");
		replyMSG.append("<tr><td>MpInitialConsume: " + _mpInitialConsume + "</td></tr>");
		replyMSG.append("<tr><td>MpConsume: " + _mpConsume + "</td></tr>");
		replyMSG.append("<tr><td>HpConsume: " + _hpConsume + "</td></tr>");
		replyMSG.append("<tr><td>EnchantLevelCount: " + _enchantLevelCount + "</td></tr>");
		replyMSG.append("<tr><td>ReuseDelay: " + _reuseDelay + "</td></tr>");
		replyMSG.append("<tr><td>CastRange: " + _castRange + "</td></tr>");
		replyMSG.append("<tr><td>SkillType: " + _skillType + "</td></tr>");
		replyMSG.append("<tr><td>TargetType: " + _targetType + "</td></tr>");
		replyMSG.append("<tr><td>Power: " + _power + "</td></tr>");
		replyMSG.append("<tr><td>SkillRadius: " + _skillRadius + "</td></tr>");
		replyMSG.append("<tr><td>Element: " + _element + "</td></tr>");
		replyMSG.append("<tr><td>ElementPower: " + _elementPower + "</td></tr>");
		replyMSG.append("<tr><td>ActivateRate: " + _activateRate + "</td></tr>");
		replyMSG.append("<tr><td>is Offensive: " + isOffensive() + "</td></tr>");
		replyMSG.append("<tr><td>AddedSkills: " + getFirstAddedSkill() + "</td></tr>");
		replyMSG.append("<tr><td></td>");

		if(_effectTemplates != null && _effectTemplates.length > 0)
		{
			replyMSG.append("<tr><td>Effects:</td></tr>");
			replyMSG.append("<tr><td>name|counter|period|stackType|stackOrder</td></tr>");
			for(final EffectTemplate et : _effectTemplates)
				replyMSG.append("<tr><td>" + et.getName() + "|" + et._ticks + "|" + et._period + "|" + et._stackType + "|" + et._stackOrder + "</td></tr>");
		}

		replyMSG.append("</table>");
		return replyMSG.toString();
	}
}
