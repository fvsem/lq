package l2n.game.model;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.primitive.IntArrayList;
import l2n.commons.list.procedure.INgObjectProcedure;
import l2n.commons.util.CollectionUtils;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.EffectType;
import l2n.game.skills.Stats;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.skillclasses.Transformation;
import l2n.util.ArrayUtil;
import l2n.util.StringUtil;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public class EffectList
{
	private static final Logger _log = Logger.getLogger(EffectList.class.getName());

	private static final int INITIAL_LIST_CAPACITY = Config.ALT_DANCE_SONG_LIMIT > Config.ALT_BUFF_LIMIT ? Config.ALT_BUFF_LIMIT : Config.ALT_DANCE_SONG_LIMIT;

	private final ReentrantLock _lock = new ReentrantLock();

	private IHardReference<? extends L2Character> ownerRef = HardReferences.emptyRef();

	private ConcurrentLinkedQueue<L2Effect> _effects;

	public EffectList(final L2Character owner)
	{
		setOwner(owner);
	}

	public final int getEffectsCount(final int skillId)
	{
		if(isEmpty())
			return 0;

		int count = 0;
		for(final L2Effect effect : _effects)
			if(effect != null && effect.getSkill().getId() == skillId)
				count++;
		return count;
	}

	public final L2Effect getEffectByType(final EffectType effectType)
	{
		if(isEmpty())
			return null;

		for(final L2Effect effect : _effects)
			if(effect != null && effect.getEffectType() == effectType)
				return effect;
		return null;
	}

	public final L2Effect getFirstEffect(final int skillId)
	{
		return getFirstEffectInternal(skillId);
	}

	public final L2Effect getFirstEffect(final L2Skill skill)
	{
		if(skill == null)
			return null;
		return getFirstEffectInternal(skill.getId());
	}

	private final L2Effect getFirstEffectInternal(final int skillId)
	{
		if(isEmpty())
			return null;

		for(final L2Effect effect : _effects)
			if(effect != null && effect.getSkill().getId() == skillId)
				return effect;

		return null;
	}

	public final L2Effect getFirstEffect(final String stackType)
	{
		if(isEmpty())
			return null;

		for(final L2Effect effect : _effects)
			if(effect != null && effect.getStackType().equalsIgnoreCase(stackType))
				return effect;

		return null;
	}

	public final L2Effect[] getAllEffects()
	{
		if(isEmpty())
			return ArrayUtil.EMPTY_EFFECT_ARRAY;
		return _effects.toArray(new L2Effect[_effects.size()]);
	}

	public final Queue<L2Effect> getEffects()
	{
		if(isEmpty())
			return CollectionUtils.emptyQueue();
		return _effects;
	}

	public final int getAllCancelableEffectsCount(final int offensive)
	{
		if(isEmpty())
			return 0;

		int ret = 0;
		for(final L2Effect effect : _effects)
			if(effect != null && effect.getSkill().isCancelable())
				if(offensive == 0 || effect.getSkill().isOffensive() == offensive > 0)
					ret++;
		return ret;
	}

	public boolean isEmpty()
	{
		return _effects == null || _effects.isEmpty();
	}

	public L2Effect[] getAllFirstEffects()
	{
		if(isEmpty())
			return ArrayUtil.EMPTY_EFFECT_ARRAY;
		final TIntObjectHashMap<L2Effect> temp = new TIntObjectHashMap<L2Effect>(_effects.size());
		for(final L2Effect ef : _effects)
			if(ef != null && !ef.isHidden())
				temp.put(ef.getSkill().getId(), ef);
		return temp.values(new L2Effect[temp.size()]);
	}

	private final static boolean isNotUsedBuffSlot(final L2Effect ef)
	{
		return ef.getSkill().isLikePassive() || ef.getSkill().isOffensive() || ef.getSkill().isToggle() || ef.getSkill() instanceof Transformation || ef.getStackType().equalsIgnoreCase("HpRecoverCast");
	}

	private final void checkBuffSlots(final ConcurrentLinkedQueue<L2Effect> effects, final L2Effect newEffect)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		if(effects == null || effects.size() < INITIAL_LIST_CAPACITY)
			return;

		if(isNotUsedBuffSlot(newEffect))
			return;

		int buffs = 0;
		int songdance = 0;
		final IntArrayList skills = new IntArrayList(effects.size());
		for(final L2Effect ef : effects)
			if(ef != null && ef.isInUse())
			{
				if(ef.getSkill().equals(newEffect.getSkill()))
					return;

				if(!isNotUsedBuffSlot(ef) && !skills.contains(ef.getSkill().getId()))
				{
					if(ef.getSkill().isDanceSong())
						songdance++;
					else
						buffs++;
					skills.add(ef.getSkill().getId());
				}
			}

		if(newEffect.getSkill().isDanceSong() ? songdance < owner.getDanceSongLimit() : buffs < owner.getBuffLimit())
			return;

		for(final L2Effect ef : effects)
			if(ef != null && ef.isInUse())
				if(!isNotUsedBuffSlot(ef) && ef.getSkill().isDanceSong() == newEffect.getSkill().isDanceSong())
				{
					stopEffectsInternalNoLock(new ForEachEffectStopMeBySkillId(ef.getSkill().getId()));
					break;
				}
	}

	public static boolean checkStackType(final EffectTemplate ef1, final EffectTemplate ef2)
	{
		if(ef1._stackType != StringUtil.NO_STACK && ef1._stackType.equalsIgnoreCase(ef2._stackType))
			return true;
		if(ef1._stackType != StringUtil.NO_STACK && ef1._stackType.equalsIgnoreCase(ef2._stackType2))
			return true;
		if(ef1._stackType2 != StringUtil.NO_STACK && ef1._stackType2.equalsIgnoreCase(ef2._stackType))
			return true;
		if(ef1._stackType2 != StringUtil.NO_STACK && ef1._stackType2.equalsIgnoreCase(ef2._stackType2))
			return true;
		return false;
	}

	public final void addEffect(final L2Effect newEffect)
	{
		final L2Character owner = getOwner();
		if(owner == null || owner.isDead())
			return;

		boolean add = false;

		final double hp = owner.getCurrentHp(), mp = owner.getCurrentMp(), cp = owner.getCurrentCp();

		_lock.lock();
		try
		{
			if(newEffect == null)
				return;

			if(Config.HERBS_DIVIDE && (owner.isSummon() || owner.getPet() != null && !owner.getPet().isDead() && owner.getPet().isSummon()) && (newEffect.getSkill().getId() >= 2278 && newEffect.getSkill().getId() <= 2285 || newEffect.getSkill().getId() >= 2512 && newEffect.getSkill().getId() <= 2514))
			{
				newEffect.setPeriod(newEffect.getPeriod() / 2);
				if(!owner.isSummon())
					owner.getPet().altUseSkill(newEffect.getSkill(), owner.getPet());
			}

			if(_effects == null)
				_effects = new ConcurrentLinkedQueue<L2Effect>();

			if(owner.isBuffImmunity() && newEffect.getEffectType() != EffectType.BuffImmunity)
			{
				final SkillType st = newEffect.getSkill().getSkillType();
				if(st == SkillType.BUFF || st == SkillType.DEBUFF)
					return;
			}

			final String stackType = newEffect.getStackType();
			if(stackType.equals(StringUtil.NO_STACK))
				for(final L2Effect e : _effects)
				{
					if(e == null || !e.isInUse())
						continue;

					if(e.getStackType().equals(StringUtil.NO_STACK) && e.getSkill().getId() == newEffect.getSkill().getId() && e.getEffectType() == newEffect.getEffectType())
						if(newEffect.getTimeLeft() > e.getTimeLeft())
							e.exit();
						else
							return;
				}
			else
				for(final L2Effect ef : _effects)
					if(ef != null)
					{
						if(!ef.isInUse())
							continue;

						if(!checkStackType(ef.getTemplate(), newEffect.getTemplate()))
							continue;

						if(ef.getSkill().getId() == newEffect.getSkill().getId() && ef.getEffectType() != newEffect.getEffectType())
							break;

						if(ef.getStackOrder() == -1)
							return;

						if(!ef.maybeScheduleNext(newEffect))
							return;
					}

			checkBuffSlots(_effects, newEffect);

			if(add = _effects.add(newEffect))
				newEffect.setInUse(true);
		}
		finally
		{
			_lock.unlock();
		}

		if(!add)
			return;

		if(newEffect.getEffector().isPlayer() && newEffect.getEffector().getDuel() != null)
			newEffect.getEffector().getDuel().onBuff((L2Player) newEffect.getEffector(), newEffect);

		newEffect.start();

		for(final Func f : newEffect.getStatFuncs())
			if(f._stat == Stats.MAX_HP)
				owner.setCurrentHp(hp, false, false);
			else if(f._stat == Stats.MAX_MP)
				owner.setCurrentMp(mp, false);
			else if(f._stat == Stats.MAX_CP)
				owner.setCurrentCp(cp, false);

		if(!owner.isMassUpdating())
		{
			owner.updateStats();
			owner.updateEffectIcons();
		}
	}

	public final void removeEffect(final L2Effect effect)
	{
		final L2Character owner = getOwner();
		if(owner == null || effect == null)
			return;

		boolean remove = false;
		_lock.lock();
		try
		{
			if(_effects == null)
				return;

			if((remove = _effects.remove(effect)) == false)
				return;
		}
		finally
		{
			_lock.unlock();
		}

		if(!remove)
			return;

		// Обновляем иконки
		if(!owner.isMassUpdating())
		{
			owner.updateStats();
			owner.updateEffectIcons();
		}
	}

	public final int getDanceCount()
	{
		if(isEmpty())
			return 0;

		int cnt = 0;
		for(final L2Effect e : _effects)
			if(e != null && e.getSkill().isDanceSong())
				cnt++;
		return cnt;
	}

	public final void stopAllEffects()
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		_lock.lock();
		try
		{
			owner.setMassUpdating(true);
			stopEffectsInternalNoLock(PROC_STOP_ALL_EFFECTS);
			owner.setMassUpdating(false);
		}
		finally
		{
			_lock.unlock();
		}

		owner.updateStats();
		owner.updateEffectIcons();
	}

	public final void stopEffects(final INgObjectProcedure<L2Effect> proc)
	{
		if(proc == null)
			throw new NullPointerException();

		final L2Character owner = getOwner();
		if(owner == null)
			return;

		owner.setMassUpdating(true);
		stopEffectsInternalNoLock(proc);
		owner.setMassUpdating(false);

		owner.updateStats();
		owner.updateEffectIcons();
	}

	public final void stopEffects(final EffectType type)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		owner.setMassUpdating(true);
		stopEffectsInternalNoLock(new ForEachEffectStopMeByEffectType(type));
		owner.setMassUpdating(false);

		owner.updateStats();
		owner.updateEffectIcons();
	}

	public final void stopEffects(final EffectType... type)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		owner.setMassUpdating(true);
		stopEffectsInternalNoLock(new ForEachEffectStopMeByEffectsType(type));
		owner.setMassUpdating(false);

		owner.updateStats();
		owner.updateEffectIcons();
	}

	public final void stopEffect(final int skillId)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		owner.setMassUpdating(true);
		stopEffectsInternalNoLock(new ForEachEffectStopMeBySkillId(skillId));
		owner.setMassUpdating(false);

		owner.updateStats();
		owner.updateEffectIcons();
	}

	public final void stopEffect(final L2Skill skill)
	{
		final L2Character owner = getOwner();
		if(owner == null)
			return;

		owner.setMassUpdating(true);
		stopEffectsInternalNoLock(new ForEachEffectStopMeBySkillId(skill.getId()));
		owner.setMassUpdating(false);

		owner.updateStats();
		owner.updateEffectIcons();
	}

	private final void stopEffectsInternalNoLock(final INgObjectProcedure<L2Effect> proc)
	{
		if(_effects != null)
			stopEffects0(proc);
	}

	private final void stopEffects0(final INgObjectProcedure<L2Effect> proc)
	{
		for(final L2Effect e : _effects)
			if(proc.execute(e))
				if(e != null)
					e.exit();
	}

	public static final class ForEachEffectStopMeByEffectType implements INgObjectProcedure<L2Effect>
	{
		private final EffectType _effectType;

		public ForEachEffectStopMeByEffectType(final EffectType effectType)
		{
			_effectType = effectType;
		}

		@Override
		public final boolean execute(final L2Effect e)
		{
			return e.getEffectType() == _effectType;
		}
	}

	public static final class ForEachEffectStopMeByEffectsType implements INgObjectProcedure<L2Effect>
	{
		private final EffectType[] _effectType;

		public ForEachEffectStopMeByEffectsType(final EffectType[] effectType)
		{
			_effectType = effectType;
		}

		@Override
		public final boolean execute(final L2Effect e)
		{
			for(final EffectType type : _effectType)
				if(e.getEffectType() == type)
					return true;
			return false;
		}
	}

	public static final class ForEachEffectStopMeBySkillId implements INgObjectProcedure<L2Effect>
	{
		private final int _skillId;

		public ForEachEffectStopMeBySkillId(final int skillId)
		{
			_skillId = skillId;
		}

		@Override
		public final boolean execute(final L2Effect e)
		{
			return e.getSkill().getId() == _skillId;
		}
	}

	public static final class ForEachEffectStopMeditation implements INgObjectProcedure<L2Effect>
	{
		private final int _skillId;

		public ForEachEffectStopMeditation(final int skillId)
		{
			_skillId = skillId;
		}

		@Override
		public final boolean execute(final L2Effect e)
		{
			return e.getSkill().getId() == _skillId && e.getEffectType() != EffectType.Debuff;
		}
	}

	public static final INgObjectProcedure<L2Effect> PROC_STOP_AFTER_DEATH = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect e)
		{
			final int skillId = e.getSkill().getId();
			switch (skillId)
			{
				case L2Skill.SKILL_FINAL_FLYING_FORM: // Final Flying Form
				case L2Skill.SKILL_AURA_BIRD_FALCON: // Aura Bird - Falcon
				case L2Skill.SKILL_AURA_BIRD_OWL: // Aura Bird - Owl
				case L2Skill.SKILL_CHARM_OF_COURAGE:
				case L2Skill.SKILL_BATTLEFIELD_DEATH_SYNDROME:
					return false;
			}

			return e.getEffectType() != EffectType.Transformation;
		}
	};

	public static final INgObjectProcedure<L2Effect> PROC_STOP_NOBLESSE = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect e)
		{
			final int skillId = e.getSkill().getId();
			switch (skillId)
			{
				case L2Skill.SKILL_BLESSING_OF_NOBLESSE:
				case L2Skill.SKILL_FORTUNE_OF_NOBLESSE:
				case L2Skill.SKILL_RAID_BLESSING:
					return true;
			}
			return false;
		}
	};

	public static final INgObjectProcedure<L2Effect> PROC_STOP_PVPEVENT = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect e)
		{
			final int skillId = e.getSkill().getId();
			switch (skillId)
			{
				case L2Skill.SKILL_RAID_CURSE:
					return false;
			}
			switch (e.getEffectType())
			{
				case Petrification:
				case BuffImmunity:
					return true;
			}
			return false;
		}
	};

	public static final INgObjectProcedure<L2Effect> PROC_DISPELL_ADVENTURERS_BUFFS = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect effect)
		{
			return effect.getSkill() != null && !effect.getSkill().isOffensive() && !effect.getSkill().getName().startsWith("Adventurer's ");
		}
	};

	public static final INgObjectProcedure<L2Effect> PROC_STOP_ALL_EFFECTS = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect effect)
		{
			return true;
		}
	};

	public static final INgObjectProcedure<L2Effect> PROC_STOP_ALL_TOGGLES = new INgObjectProcedure<L2Effect>()
	{
		@Override
		public final boolean execute(final L2Effect effect)
		{
			return effect.getSkill().isToggle();
		}
	};

	public void setOwner(final L2Character owner)
	{
		ownerRef = owner == null ? HardReferences.<L2Character> emptyRef() : owner.getRef();
	}

	private L2Character getOwner()
	{
		return ownerRef.get();
	}

	public static final boolean checkEffect(final L2Effect ef, final L2Skill skill)
	{
		if(ef == null || !ef.isInUse()) // такого скилла нет
			return false;
		if(!checkStackType(ef._template, skill.getEffectTemplates()[0]) && (ef.getSkill() == null || ef.getSkill().getId() != skill.getId())) // такого скилла нет
			return false;
		if(ef.getStackOrder() < skill.getEffectTemplates()[0]._stackOrder) // старый слабее
			return false;
		if(ef.getTimeLeft() > 10000) // старый не слабее и еще не кончается - ждем
			return true;
		if(ef.getSkill() != null && ef.getSkill().getId() == skill.getId() && ef.getTimeLeft() > 10000) // эффект от этого скила уже есть
			return true;
		if(ef.getNext() != null) // старый не слабее но уже кончается - проверить рекурсией что там зашедулено
			return checkEffect(ef.getNext(), skill);
		return false;
	}
}
