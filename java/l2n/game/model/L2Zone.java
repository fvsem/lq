package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.DefaultListenerEngine;
import l2n.extensions.listeners.engine.ListenerEngine;
import l2n.extensions.listeners.events.L2Zone.L2ZoneEnterLeaveEvent;
import l2n.extensions.scripts.Functions;
import l2n.game.GameTimeController;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncAdd;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

public class L2Zone
{
	private final static Logger _log = Logger.getLogger(L2Zone.class.getName());

	public static final L2Zone[] EMPTY_ARRAY = new L2Zone[0];

	/** Ордер в зонах, с ним мы и добавляем/убираем статы. TODO: сравнить ордер с оффом, пока от фонаря */
	public final static int ZONE_STATS_ORDER = 0x40;

	public static final String BLOCKED_ACTION_PRIVATE_STORE = "private store";
	public static final String BLOCKED_ACTION_PRIVATE_WORKSHOP = "private workshop";

	public static enum ZoneTarget
	{
		pc,
		npc,
		only_pc
	}

	public static enum ZoneType
	{
		Town(0),
		Castle(1),
		epic(2),
		no_water(3),
		unblock_actions(4),
		ClanHall(5),
		Fortress(6),
		OlympiadStadia(7),
		Underground(8),
		Siege(9),
		water(10),
		battle_zone(11),
		pvp_zone(12),

		damage(13),
		instant_skill(14),
		swamp(15),
		poison(16),

		mother_tree(17),
		peace_zone(18),
		ssq_zone(19),
		no_escape(20),
		no_landing(21),
		no_restart(22),
		no_spawn(23),
		no_summon(24),
		siege_residense(25),
		dummy(26),
		offshore(27),
		monster_trap(28),
		UnderGroundColiseum(29),
		landing(30),
		no_clicker_zone(32);

		public final int mask;

		private ZoneType(final int id)
		{
			mask = 1 << id;
		}

		public final int getTypeId()
		{
			return mask;
		}
	}

	private final int _id;
	private final ZoneType _type;
	private final String _name;

	private final Lock _readLock;
	private final Lock _writeLock;
	private final GArray<L2Character> _characters;

	private L2Territory _loc;
	private L2Territory _restart_points;
	private L2Territory _pk_restart_points;
	private int _entering_message_no;
	private int _leaving_message_no;
	private boolean _active;

	private L2Skill _skill;
	private int _chance;
	private long _reuse;
	private long _initialDelay;
	private ZoneTarget _target;
	private Future<ZoneSkillTimer> skillTimer;
	private Future<DamageTask> damageTask;

	private int _index;
	private int _taxById;
	private long _restartTime;
	private String _blockedActions;

	private ListenerEngine<L2Zone> listenerEngine;

	private String _event;
	private long _lastEventTime;
	private long _reflectionId;

	private int messageNumber;
	private int damageOnHP;
	private int damageOnMP;
	private int moveBonus;
	private int regenBonusHP;
	private int regenBonusMP;
	private String affectRace;

	public L2Zone(final int id, final ZoneType type, final String name)
	{
		_id = id;
		_type = type;
		_name = name;

		final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
		_readLock = lock.readLock();
		_writeLock = lock.writeLock();
		_characters = new GArray<L2Character>(0);
	}

	public final int getId()
	{
		return _id;
	}

	@Override
	public final String toString()
	{
		return "ID " + _id + " [name: " + _name + ", type: " + _type + "]";
	}

	public ZoneType getType()
	{
		return _type;
	}

	public final int getTypeId()
	{
		return _type.mask;
	}

	public void setLoc(final L2Territory t)
	{
		_loc = t;
	}

	public void setRestartPoints(final L2Territory t)
	{
		_restart_points = t;
	}

	public void setPKRestartPoints(final L2Territory t)
	{
		_pk_restart_points = t;
	}

	public L2Territory getLoc()
	{
		return _loc;
	}

	public L2Territory getRestartPoints()
	{
		return _restart_points;
	}

	public L2Territory getPKRestartPoints()
	{
		return _pk_restart_points;
	}

	public Location getSpawn()
	{
		if(_restart_points == null)
		{
			_log.warning("L2Zone: restartPoint not found for " + toString());
			return new Location(17817, 170079, -3530);
		}
		final GArray<int[]> coords = _restart_points.getCoords();
		final int[] coord = coords.get(Rnd.get(coords.size()));
		return new Location(coord[0], coord[1], coord[2]);
	}

	public GArray<int[]> getSpawns()
	{
		return _restart_points.getCoords();
	}

	public Location getPKSpawn()
	{
		if(_pk_restart_points == null)
			return getSpawn();
		final GArray<int[]> coords = _pk_restart_points.getCoords();
		final int rnd = Rnd.get(coords.size());
		final int[] coord = coords.get(rnd);
		return new Location(coord[0], coord[1], coord[2]);
	}

	public boolean checkIfInZone(final int x, final int y)
	{
		return _loc != null && _loc.isInside(x, y);
	}

	public boolean checkIfInZone(final int x, final int y, final int z)
	{
		return _loc != null && _loc.isInside(x, y, z);
	}

	public boolean checkIfInZone(final L2Object obj)
	{
		return _loc != null && _loc.isInside(obj.getX(), obj.getY(), obj.getZ());
	}

	public final double findDistanceToZone(final L2Object obj, final boolean includeZAxis)
	{
		return findDistanceToZone(obj.getX(), obj.getY(), obj.getZ(), includeZAxis);
	}

	public final double findDistanceToZone(final int x, final int y, final int z, final boolean includeZAxis)
	{
		double closestDistance = 99999999;
		if(_loc == null)
			return closestDistance;
		double distance;
		for(final int[] coord : _loc.getCoords())
		{
			distance = Util.calculateDistance(x, y, z, coord[0], coord[1], (coord[2] + coord[3]) / 2, includeZAxis);
			if(distance < closestDistance)
				closestDistance = distance;
		}
		return closestDistance;
	}

	public GArray<int[]> getCoords()
	{
		return _loc.getCoords();
	}
	public void doEnter(final L2Character character)
	{
		_writeLock.lock();
		try
		{
			if(!_characters.contains(character))
				_characters.add(character);
			else
				_log.severe("Attempt of double object add to zone " + _name + " id " + _id + " [cha: " + character + "]");
		}
		finally
		{
			_writeLock.unlock();
		}

		if(!isActive())
			return;

		onZoneEnter(character);
	}

	public void doLeave(final L2Character character, final boolean notify)
	{
		_writeLock.lock();
		try
		{
			final boolean remove = _characters.remove(character);
			if(!remove)
				_log.severe("Attempt remove object from zone " + _name + " id " + _id + " where it's absent [cha: " + character + "]");
		}
		finally
		{
			_writeLock.unlock();
		}

		if(!isActive())
			return;

		if(notify)
			onZoneLeave(character);
	}

	private void onZoneEnter(final L2Character character)
	{
		if(character.inObserverMode())
			return;

		if(character.getPlayer() != null && getType() == ZoneType.epic 
				&& getId() == 702102
				&& character.getPlayer().getLevel() > 48)
		{
			character.getPlayer().teleToClosestTown();
		}
		
		character.addZone(this);

		getListenerEngine().fireMethodInvoked(new L2ZoneEnterLeaveEvent(MethodCollection.L2ZoneObjectEnter, this, character));

		checkEffects(character, true);
		addZoneStats(character);
		character.doZoneCheck(_entering_message_no);
	}

	private void onZoneLeave(final L2Character character)
	{
		if(character.inObserverMode())
			return;

		character.removeZone(this);

		getListenerEngine().fireMethodInvoked(new L2ZoneEnterLeaveEvent(MethodCollection.L2ZoneObjectLeave, this, character));

		checkEffects(character, false);
		removeZoneStats(character);
		character.doZoneCheck(_leaving_message_no);
	}

	private void addZoneStats(final L2Character object)
	{
		if(_target != null)
			if(!checkTarget(object))
				return;

		if(getMoveBonus() != 0)
			if(object.isPlayable())
			{
				object.addStatFunc(new FuncAdd(Stats.RUN_SPEED, ZONE_STATS_ORDER, this, getMoveBonus()));
				object.sendChanges();
			}

		if(affectRace != null && !object.isPlayer())
			return;

		if(affectRace != null && !affectRace.equalsIgnoreCase("all"))
		{
			final L2Player player = object.getPlayer();
			if(!player.getRace().toString().equalsIgnoreCase(affectRace))
				return;
		}

		if(getRegenBonusHP() != 0)
			object.addStatFunc(new FuncAdd(Stats.REGENERATE_HP_RATE, ZONE_STATS_ORDER, this, getRegenBonusHP()));

		if(getRegenBonusMP() != 0)
			object.addStatFunc(new FuncAdd(Stats.REGENERATE_MP_RATE, ZONE_STATS_ORDER, this, getRegenBonusMP()));
	}

	private void removeZoneStats(final L2Character object)
	{
		if(getRegenBonusHP() == 0 && getRegenBonusMP() == 0 && getMoveBonus() == 0)
			return;

		object.removeStatsOwner(this);

		if(getMoveBonus() > 0)
			object.sendChanges();
	}

	private void checkEffects(final L2Character character, final boolean enter)
	{
		if(_event != null && _event.startsWith("script"))
		{
			final String[] param = _event.split(";");
			Functions.callScripts(param[1], param[2], new Object[] { this, character, Boolean.valueOf(enter) });
		}

		if(_skill == null || _target == null)
			return;

		if(!checkTarget(character))
			return;

		if(enter)
		{
			if(skillTimer == null)
				synchronized (this)
				{
					if(skillTimer == null)
						skillTimer = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new ZoneSkillTimer(), _initialDelay, _reuse);
				}
		}
		else
		{
			final L2Character[] characters = getObjectsInside();
			for(final L2Character cha : characters)
				if(checkTarget(cha))
					return;

			stopSkillTimer();
		}
	}

	private boolean checkTarget(final L2Character character)
	{
		switch (_target)
		{
			case pc:
				return character.isPlayable();
			case only_pc:
				return character.isPlayer();
			case npc:
				return character.isNpc();
		}
		return false;
	}

	public L2Character[] getObjectsInside()
	{
		_readLock.lock();
		try
		{
			return _characters.toArray(new L2Character[_characters.size()]);
		}
		finally
		{
			_readLock.unlock();
		}
	}

	public void setActive(final boolean value)
	{
		if(_active == value)
			return;

		_readLock.lock();
		try
		{
			_active = value;

			for(final L2Character cha : _characters)
				if(_active)
					onZoneEnter(cha);
				else
					onZoneLeave(cha);
		}
		finally
		{
			_readLock.unlock();
		}

		setDamageTaskActive(value);
	}

	private void setDamageTaskActive(final boolean value)
	{
		if(damageTask == null && value && (damageOnHP > 0 || damageOnMP > 0))
			damageTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new DamageTask(), _initialDelay, _reuse);
		else
			stopDamageTask();
	}

	public GArray<L2Player> getInsidePlayers()
	{
		if(_characters.isEmpty())
			return GArray.emptyCollection();

		final L2Character[] all_characters = getObjectsInside();
		final GArray<L2Player> result = new GArray<L2Player>();
		for(final L2Character obj : all_characters)
			if(obj != null && obj.isPlayer())
				result.add((L2Player) obj);
		return result;
	}

	public GArray<L2Player> getInsidePlayersIncludeZ()
	{
		if(_characters.isEmpty())
			return GArray.emptyCollection();

		final L2Character[] all_characters = getObjectsInside();
		final GArray<L2Player> result = new GArray<L2Player>();
		for(final L2Character obj : all_characters)
			if(obj != null && obj.isPlayer() && _loc.isInside(obj))
				result.add((L2Player) obj);
		return result;
	}

	public void broadcastPacket(final L2GameServerPacket... packets)
	{
		if(packets.length == 0 || !isActive() || _characters.isEmpty())
			return;

		final L2Character[] all_characters = getObjectsInside();
		for(final L2Character obj : all_characters)
			if(obj != null && obj.isPlayer() && _loc.isInside(obj))
				((L2Player) obj).sendPacket(packets);
	}

	public boolean isActive()
	{
		return _active;
	}

	public final String getName()
	{
		return _name;
	}

	public final int getEnteringMessageId()
	{
		return _entering_message_no;
	}

	public final void setEnteringMessageId(final int id)
	{
		_entering_message_no = id;
	}

	public final int getLeavingMessageId()
	{
		return _leaving_message_no;
	}

	public final void setLeavingMessageId(final int id)
	{
		_leaving_message_no = id;
	}

	public final void setIndex(final int index)
	{
		_index = index;
	}

	public final int getIndex()
	{
		return _index;
	}

	public final void setTaxById(final int id)
	{
		_taxById = id;
	}

	public final Integer getTaxById()
	{
		return _taxById;
	}

	public void setSkill(final String skill)
	{
		if(skill == null || skill.equalsIgnoreCase("null"))
			return;
		final String[] data = skill.split(";");
		final L2Skill sk = SkillTable.getInstance().getInfo(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
		if(sk == null)
			Log.addDev(skill + " is NULL for zone " + toString(), "dev_zoneskills", false);
		else if(sk.getTargetType() == SkillTargetType.TARGET_NONE || sk.getSkillType() == SkillType.NOTDONE)
			Log.addDev(sk.toString() + " for zone " + toString(), "dev_zoneskills", false);

		setSkill(sk);
	}

	private void setSkill(final L2Skill skill)
	{
		_skill = skill;
	}

	public void setSkillProb(final String chance)
	{
		if(chance == null)
		{
			_chance = -1;
			return;
		}
		_chance = Integer.parseInt(chance);
	}

	public void setUnitTick(final String delay)
	{
		if(delay == null)
		{
			_reuse = 1000;
			return;
		}
		_reuse = Integer.parseInt(delay) * 1000;
	}

	public void setInitialDelay(final String delay)
	{
		if(delay == null)
		{
			_initialDelay = 0;
			return;
		}
		_initialDelay = Integer.parseInt(delay) * 1000;
	}

	public void setTarget(final String target)
	{
		if(target == null)
			return;
		_target = ZoneTarget.valueOf(target);
	}

	public long getRestartTime()
	{
		return _restartTime;
	}

	public L2Skill getZoneSkill()
	{
		return _skill;
	}

	public ZoneTarget getZoneTarget()
	{
		return _target;
	}

	public void setRestartTime(final long restartTime)
	{
		_restartTime = restartTime;
	}

	public void setBlockedActions(final String blockedActions)
	{
		_blockedActions = blockedActions;
	}

	public boolean isActionBlocked(final String action)
	{
		return _blockedActions != null && _blockedActions.contains(action);
	}

	public ListenerEngine<L2Zone> getListenerEngine()
	{
		if(listenerEngine == null)
			listenerEngine = new DefaultListenerEngine<L2Zone>(this);
		return listenerEngine;
	}

	public int getMessageNumber()
	{
		return messageNumber;
	}

	public void setMessageNumber(final int messageNumber)
	{
		this.messageNumber = messageNumber;
	}

	public void setMessageNumber(final String number)
	{
		setMessageNumber(number == null ? 0 : Integer.parseInt(number));
	}

	public int getDamageOnHP()
	{
		return damageOnHP;
	}

	private void setDamageOnHP(final int damageOnHP)
	{
		this.damageOnHP = damageOnHP;
	}

	public void setDamageOnHP(final String damage)
	{
		setDamageOnHP(damage == null ? 0 : Integer.parseInt(damage));
	}

	public int getDamageOnMP()
	{
		return damageOnMP;
	}

	private void setDamageOnMP(final int damageOnMP)
	{
		this.damageOnMP = damageOnMP;
	}

	public void setDamageOnМP(final String damage)
	{
		setDamageOnMP(damage == null ? 0 : Integer.parseInt(damage));
	}

	public int getMoveBonus()
	{
		return moveBonus;
	}

	public void setMoveBonus(final int moveBonus)
	{
		this.moveBonus = moveBonus;
	}

	public void setMoveBonus(final String moveBonus)
	{
		setMoveBonus(moveBonus == null ? 0 : Integer.parseInt(moveBonus));
	}

	public int getRegenBonusHP()
	{
		return regenBonusHP;
	}

	public void setRegenBonusHP(final String bonus)
	{
		setRegenBonusHP(bonus == null ? 0 : Integer.parseInt(bonus));
	}

	public void setRegenBonusHP(final int regenBonusHP)
	{
		this.regenBonusHP = regenBonusHP;
	}

	public int getRegenBonusMP()
	{
		return regenBonusMP;
	}

	public void setRegenBonusMP(final int regenBonusMP)
	{
		this.regenBonusMP = regenBonusMP;
	}

	public void setRegenBonusMP(final String bonus)
	{
		setRegenBonusMP(bonus == null ? 0 : Integer.parseInt(bonus));
	}

	public String getAffectRace()
	{
		return affectRace;
	}

	public void setAffectRace(String affectRace)
	{
		if(affectRace != null)
			affectRace = affectRace.toLowerCase().replace(" ", "");

		this.affectRace = affectRace;
	}

	public String getEvent()
	{
		return _event;
	}

	public void setEvent(final String event)
	{
		_event = event;
	}

	public long getLastEventTime()
	{
		return _lastEventTime;
	}

	public void setLastEventTime(final long lastEventTime)
	{
		_lastEventTime = lastEventTime;
	}

	public void setReflectionId(final long val)
	{
		_reflectionId = val;
	}

	public long getReflectionId()
	{
		return _reflectionId;
	}

	public void setParam(final String name, final String value)
	{}

	private void stopSkillTimer()
	{
		try
		{
			if(skillTimer != null)
				skillTimer.cancel(false);
		}
		catch(final NullPointerException e)
		{}
		skillTimer = null;
	}

	private void stopDamageTask()
	{
		try
		{
			if(damageTask != null)
				damageTask.cancel(false);
		}
		catch(final NullPointerException e)
		{}
		damageTask = null;
	}

	private class ZoneSkillTimer implements Runnable
	{
		@Override
		public void run()
		{
			if(!isActive() || _target == null)
			{
				stopSkillTimer();
				return;
			}

			if(_skill.getId() == 5399 && GameTimeController.getInstance().isNowNight())
				return;

			final L2Character[] all_characters = getObjectsInside();
			for(final L2Character character : all_characters)
				if(checkTarget(character))
					if(!character.isDead() && Rnd.chance(_chance))
						_skill.getEffects(character, character, false, false);
		}
	}

	private class DamageTask implements Runnable
	{
		@Override
		public void run()
		{
			if(!isActive())
				return;

			final L2Character[] all_characters = getObjectsInside();
			for(final L2Character target : all_characters)
			{
				if(target == null || _target != null && !checkTarget(target))
					continue;

				if(_target == null && !target.isPlayable())
					continue;

				if(target.isDead())
					continue;

				if(damageOnHP > 0)
				{
					target.reduceCurrentHp(damageOnHP, target, null, true, true, true, false, false);
					if(messageNumber > 0)
						target.sendPacket(new SystemMessage(messageNumber).addNumber(damageOnHP));
				}

				if(damageOnMP > 0)
				{
					target.reduceCurrentMp(damageOnMP, null);
					if(messageNumber > 0)
						target.sendPacket(new SystemMessage(messageNumber).addNumber(damageOnMP));
				}
			}
		}
	}
}
