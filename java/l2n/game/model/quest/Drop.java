package l2n.game.model.quest;

import l2n.commons.list.primitive.IntArrayList;

public class Drop
{
	public int condition;
	public int maxcount;
	public int chance;

	public IntArrayList itemList = new IntArrayList();

	public Drop(int condition, int maxcount, int chance)
	{
		this.condition = condition;
		this.maxcount = maxcount;
		this.chance = chance;
	}

	public Drop addItem(int item)
	{
		itemList.add(item);
		return this;
	}
}
