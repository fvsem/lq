package l2n.game.model.quest;

import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;

import java.util.concurrent.ScheduledFuture;

public class QuestTimer
{
	public class ScheduleTimerTask implements Runnable
	{
		@Override
		public void run()
		{
			if(!isActive())
				return;

			L2Player pl = getPlayer();
			if(pl != null && getQuest() != null && getQuest().getName() != null && getName() != null)
				pl.processQuestEvent(getQuest().getName(), getName(), getNpc());
			cancel();
		}
	}

	private boolean _isActive = true;
	private String _name;
	private L2NpcInstance _npc;
	private long _playerStoreId;
	private Quest _quest;
	private ScheduledFuture<ScheduleTimerTask> _schedular;

	public QuestTimer(Quest quest, String name, long time, L2NpcInstance npc, L2Player player)
	{
		_name = name;
		_quest = quest;
		_playerStoreId = player == null ? 0 : player.getStoredId();
		_npc = npc;
		_schedular = L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleTimerTask(), time); // Prepare auto end task
	}

	public void cancel()
	{
		_isActive = false;

		if(_schedular != null)
			_schedular.cancel(false);

		getQuest().removeQuestTimer(this);
	}

	public final boolean isActive()
	{
		return _isActive;
	}

	public final String getName()
	{
		return _name;
	}

	public final L2NpcInstance getNpc()
	{
		return _npc;
	}

	public final L2Player getPlayer()
	{
		return L2ObjectsStorage.getAsPlayer(_playerStoreId);
	}

	public final Quest getQuest()
	{
		return _quest;
	}

	// Проверяет, совпадают ли указанные параметры с параметрами этого таймера
	public boolean isMatch(Quest quest, String name, L2NpcInstance npc, L2Player player)
	{
		return quest != null && name != null && quest == _quest && name.equalsIgnoreCase(_name) && player == getPlayer();
	}

	@Override
	public final String toString()
	{
		return _name;
	}
}
