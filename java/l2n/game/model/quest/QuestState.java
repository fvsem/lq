package l2n.game.model.quest;

import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SpawnTable;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.game.templates.L2Item;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public final class QuestState
{
	private final static Logger _log = Logger.getLogger(Quest.class.getName());

	/** Player who engaged the quest */
	private IHardReference<L2Player> ownerRef = HardReferences.emptyRef();

	/** Quest associated to the QuestState */
	private final Quest quest;

	/** state of the quest */
	private int _state;

	/** List of couples (variable for quest,value of the variable for quest) */
	private ConcurrentHashMap<String, String> _vars;

	/**
	 * Constructor of the QuestState : save the quest in the list of quests of the player.<BR/>
	 * <BR/>
	 * <U><I>Actions :</U></I><BR/>
	 * <LI>Save informations in the object QuestState created (Quest, Player, Completion, State)</LI> <LI>Add the QuestState in the player's list of quests by using setQuestState()</LI> <LI>Add drops gotten by the quest</LI> <BR/>
	 * 
	 * @param quest
	 *            : quest associated with the QuestState
	 * @param player
	 *            : L2Player pointing out the player
	 * @param state
	 *            : state of the quest
	 */
	public QuestState(final Quest q, final L2Player player, final int state)
	{
		quest = q;
		ownerRef = player.getRef();

		// Save the state of the quest for the player in the player's list of quest onwed
		player.setQuestState(this);

		// set the state of the quest
		_state = state;
	}

	/**
	 * Add XP and SP as quest reward <br>
	 * <br>
	 * Метод учитывает рейты!
	 */
	public void addExpAndSp(final long exp, final long sp)
	{
		addExpAndSp(exp, sp, false);
	}

	/**
	 * Add XP and SP as quest reward <br>
	 * <br>
	 * Метод учитывает рейты!
	 * 3-ий параметр true/false показывает является ли квест на профессию
	 * и рейты учитываются в завимисомти от параметра RateQuestsRewardOccupationChange
	 */
	public void addExpAndSp(long exp, long sp, final boolean isOccupationChange)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;

		// Применяем рейты если
		// 1 - квест не на профу
		// 2 - квест на профу и если включен параметр RateQuestsRewardOccupationChange
		if(!isOccupationChange || isOccupationChange && Config.RATE_QUESTS_OCCUPATION_CHANGE)
		{
			final double bonus = player == null ? 1 : player.getBonus().RATE_QUESTS_REWARD;
			exp *= Config.RATE_QUESTS_REWARD_XP * bonus;
			sp *= Config.RATE_QUESTS_REWARD_SP * bonus;
			player.addExpAndSp(exp, sp, false, false);
		}
		else
			player.addExpAndSp(exp, sp, false, false);
	}

	/**
	 * Add player to get notification of characters death
	 * 
	 * @param playable
	 *            : L2Playable of the character to get notification of death
	 */
	public void addNotifyOfDeath(final L2Playable playable)
	{
		if(playable != null)
			playable.addNotifyQuestOfDeath(this);
	}

	public void addNotifyOfPlayerKill()
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.addNotifyOfPlayerKill(this);
	}

	public void removeNotifyOfPlayerKill()
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.removeNotifyOfPlayerKill(this);
	}

	public void addRadar(final int x, final int y, final int z)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.radar.addMarker(x, y, z);
	}

	public void clearRadar()
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.radar.removeAllMarkers();
	}

	/**
	 * Destroy element used by quest when quest is exited
	 * 
	 * @param repeatable
	 *            - если true удаляет все квестовые предметы ;(
	 * @return QuestState
	 */
	public QuestState exitCurrentQuest(final boolean repeatable)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return this;

		// Clean drops
		if(quest.getItems() != null)
			// Go through values of class variable "drops" pointing out mobs that drop for quest
			for(final Integer itemId : quest.getItems())
			{
				// Get [item from] / [presence of the item in] the inventory of the player
				final L2ItemInstance item = player.getInventory().getItemByItemId(itemId);
				if(item == null || itemId == 57)
					continue;
				final long count = item.getCount();
				// If player has the item in inventory, destroy it (if not gold)
				player.getInventory().destroyItemByItemId(itemId, count, true);
				player.getWarehouse().destroyItem(itemId, count);
			}

		// If quest is repeatable, delete quest from list of quest of the player and from database (quest CAN be created again => repeatable)
		if(repeatable)
		{
			player.delQuestState(quest.getName());
			Quest.deleteQuestInDb(this);
			_vars = null;
		}
		else
		{
			// Otherwise, delete variables for quest and update database (quest CANNOT be created again => not repeatable)
			if(_vars != null && !_vars.isEmpty())
				for(final String var : _vars.keySet())
					if(var != null)
						unset(var);
			setState(Quest.COMPLETED);
			Quest.updateQuestInDb(this);
		}
		player.sendPacket(new QuestList(player));
		return this;
	}

	public void abortQuest()
	{
		quest.onAbort(this);
		exitCurrentQuest(true);
	}

	/**
	 * <font color=red>Не использовать для получения кондов!</font><br>
	 * <br>
	 * Return the value of the variable of quest represented by "var"
	 * 
	 * @param var
	 *            : name of the variable of quest
	 * @return Object
	 */
	public String get(final String var)
	{
		if(_vars == null)
			return null;
		return _vars.get(var);
	}

	public ConcurrentHashMap<String, String> getVars()
	{
		final ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
		if(_vars != null)
			result.putAll(_vars);
		return result;
	}

	/**
	 * Возвращает переменную в виде целого числа. Для кондов вызывает getCond.
	 * 
	 * @param var
	 *            : String designating the variable for the quest
	 * @return int
	 */
	public int getInt(final String var)
	{
		if(var.equalsIgnoreCase("cond"))
			return getCond();

		return getRawInt(var);
	}

	/**
	 * Возвращает переменную в виде целого числа.
	 * 
	 * @param var
	 *            : String designating the variable for the quest
	 * @return int
	 */
	public int getRawInt(final String var)
	{
		int varint = 0;
		try
		{
			final String val = get(var);
			if(val == null)
				return 0;
			varint = Integer.parseInt(val);
		}
		catch(final Exception e)
		{
			_log.finer(getPlayer().getName() + ": variable " + var + " isn't an integer: " + varint + e);
			e.printStackTrace();
		}
		return varint;
	}

	/**
	 * Return item number which is equipped in selected slot
	 * 
	 * @return int
	 */
	public int getItemEquipped(final int loc)
	{
		return getPlayer().getInventory().getPaperdollItemId(loc);
	}

	/**
	 * Return the L2Player
	 * 
	 * @return L2Player
	 */
	public L2Player getPlayer()
	{
		return ownerRef.get();
	}

	/**
	 * Return the quest
	 * 
	 * @return Quest
	 */
	public Quest getQuest()
	{
		return quest;
	}

	/**
	 * Return the quantity of one sort of item hold by the player
	 * 
	 * @param itemId
	 *            : ID of the item wanted to be count
	 * @return int
	 */
	public long getQuestItemsCount(final int itemId)
	{
		final L2Player player = getPlayer();
		return player == null ? 0 : player.getInventory().getCountOf(itemId);
	}

	public long getQuestItemsCount(final int... itemsIds)
	{
		long result = 0;
		for(final int id : itemsIds)
			result = result + getQuestItemsCount(id);
		return result;
	}

	/**
	 * Return the QuestTimer object with the specified name
	 * 
	 * @return QuestTimer<BR>
	 *         Return null if name does not exist
	 */
	public final QuestTimer getQuestTimer(final String name)
	{
		return getQuest().getQuestTimer(name, null, getPlayer());

	}

	/**
	 * Return the state of the quest
	 * 
	 * @return State
	 */
	public int getState()
	{
		return _state;
	}

	public String getStateName()
	{
		return Quest.getStateName(_state);
	}

	/**
	 * Добавить предмет игроку
	 * By default if item is adena rates 'll be applyed, else no
	 * 
	 * @param itemId
	 * @param count
	 */
	public void giveItems(final int itemId, final long count)
	{
		giveItems(itemId, count, 0, false);
	}

	/**
	 * Добавить предмет игроку
	 * 
	 * @param itemId
	 * @param count
	 * @param rate
	 *            - учет квестовых рейтов
	 */
	public void giveItems(final int itemId, final long count, final boolean rate)
	{
		giveItems(itemId, count, 0, rate);
	}

	/**
	 * Добавить предмет игроку
	 * 
	 * @param itemId
	 * @param count
	 * @param enchantlevel
	 * @param rate
	 *            - учет квестовых рейтов
	 */
	public void giveItems(final int itemId, long count, final int enchantlevel, final boolean rate)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;

		if(count <= 0)
			count = 1;

		// Применяем рейты
		if(rate || itemId == Quest.ADENA_ID)
			// дополнительные модификаторы
			if(Config.RATE_QUESTS_ALT_MULTIPLIERS_REWARD)
				count *= getRateQuestsReward(itemId);
			else
				count *= getRateQuestsReward();

		// Get template of item
		final L2Item template = ItemTable.getInstance().getTemplate(itemId);
		if(template == null)
			return;

		if(template.isStackable())
		{
			final L2ItemInstance item = ItemTable.getInstance().createItem(itemId, player.getObjectId(), 0, "Quest[ID:" + quest._questId + "]");

			// Set quantity of item
			item.setCount(count);

			// Add items to player's inventory
			player.getInventory().addItem(item);

			if(enchantlevel > 0)
				item.setEnchantLevel(enchantlevel);

			Log.LogItem(player, Log.GetQuestItem, item);
		}
		else
		{
			L2ItemInstance item;
			for(int i = 0; i < count; i++)
			{
				item = ItemTable.getInstance().createItem(itemId, player.getObjectId(), 0, "Quest[ID:" + quest._questId + "]");

				// Set quantity of item
				item.setCount(1);

				// Add items to player's inventory
				player.getInventory().addItem(item);

				if(enchantlevel > 0)
					item.setEnchantLevel(enchantlevel);

				Log.LogItem(player, Log.GetQuestItem, item);
			}
		}

		// If item for reward is gold, send message of gold reward to client
		player.sendPacket(SystemMessage.obtainItems(template.getItemId(), count, enchantlevel));
		player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
	}

	public void giveItems(final int itemId, long count, final byte attributeId, final int attributeLevel)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;

		if(count <= 0)
			count = 1;

		final L2Item template = ItemTable.getInstance().getTemplate(itemId);
		if(template == null)
			return;

		L2ItemInstance item;
		for(int i = 0; i < count; i++)
		{
			item = ItemTable.getInstance().createItem(itemId, player.getObjectId(), 0, "Quest[ID:" + quest._questId + "]");
			if(attributeId >= 0 && attributeLevel > 0)
				item.setAttributeElement(attributeId, attributeLevel, true);

			player.getInventory().addItem(item);
			Log.LogItem(player, Log.GetQuestItem, item);
		}

		player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param count
	 *            количество при рейтах 1х
	 * @param calcChance
	 *            шанс при рейтах 1х, в процентах
	 * @return количество вещей для дропа, может быть 0
	 */
	public int rollDrop(final int count, final double calcChance, final boolean prof)
	{
		if(calcChance <= 0 || count <= 0)
			return 0;
		return rollDrop(count, count, calcChance, prof);
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param min
	 *            минимальное количество при рейтах 1х
	 * @param max
	 *            максимальное количество при рейтах 1х
	 * @param calcChance
	 *            шанс при рейтах 1х, в процентах
	 * @param prof
	 *            - учитывать дроп по параметру "рейт дропа для квестов на профу"
	 * @return количество вещей для дропа, может быть 0
	 */
	public int rollDrop(final int min, final int max, double calcChance, final boolean prof)
	{
		if(calcChance <= 0 || min <= 0 || max <= 0)
			return 0;
		int dropmult = 1;
		calcChance *= getRateQuestsDrop(prof);
		if(calcChance > 100)
		{
			dropmult = (int) (calcChance / 100);
			calcChance = calcChance / dropmult;
		}

		return Rnd.chance(calcChance) ? Rnd.get(min * dropmult, max * dropmult) : 0;
	}

	/**
	 * @param prof
	 *            - учитывать дроп по параметру "рейт дропа для квестов на профу"
	 * @return рейты на дроп квестовых предметов, с учётом личного бонуса
	 */
	public double getRateQuestsDrop(final boolean prof)
	{
		final L2Player player = getPlayer();
		final double bonus = player == null ? 1 : player.getBonus().RATE_QUESTS_DROP;
		return (prof ? Config.RATE_QUESTS_DROP_PROF : Config.RATE_QUESTS_DROP) * bonus;
	}

	/**
	 * @return
	 */
	public double getRateQuestsReward(final int itemId)
	{
		final L2Player player = getPlayer();
		final double bonus = player == null ? 1 : player.getBonus().RATE_QUESTS_REWARD;

		double mod = 1;
		if(itemId <= 0)
			mod = Config.RATE_QUESTS_REWARD;
		else if(itemId == L2Item.ITEM_ID_ADENA)
			mod = Config.RATE_QUESTS_REWARD_ADENA;
		else
		{
			final L2Item item = ItemTable.getInstance().getTemplate(itemId);
			if(item == null)
				mod = Config.RATE_QUESTS_REWARD;
			else if(item.getItemType() == EtcItemType.SCROLL)
				mod = Config.RATE_QUESTS_REWARD_SCROLL;
			else if(item.getItemType() == EtcItemType.RECIPE)
				mod = Config.RATE_QUESTS_REWARD_RECIPE;
			else if(item.getItemType() == EtcItemType.MATERIAL)
				mod = Config.RATE_QUESTS_REWARD_MATERIAL;
			else if(item.getItemType() == EtcItemType.POTION)
				mod = Config.RATE_QUESTS_REWARD_POTION;
		}
		return mod * bonus;
	}

	/**
	 * @return
	 */
	public double getRateQuestsReward()
	{
		final L2Player player = getPlayer();
		final double bonus = player == null ? 1 : player.getBonus().RATE_QUESTS_REWARD;
		return Config.RATE_QUESTS_REWARD * bonus;
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * проверяет максимум, а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param min
	 *            минимальное количество при рейтах 1х
	 * @param max
	 *            максимальное количество при рейтах 1х
	 * @param limit
	 *            максимум таких вещей
	 * @param calcChance
	 * @return true если после выполнения количество достигло лимита
	 */
	public boolean rollAndGive(final int itemId, final int min, final int max, final int limit, final double calcChance)
	{
		if(calcChance <= 0 || min <= 0 || max <= 0 || limit <= 0 || itemId <= 0)
			return false;
		return rollAndGive(itemId, min, max, limit, calcChance, false);
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * проверяет максимум, а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param min
	 *            минимальное количество при рейтах 1х
	 * @param max
	 *            максимальное количество при рейтах 1х
	 * @param limit
	 *            максимум таких вещей
	 * @param calcChance
	 * @param QuestProf
	 * @return true если после выполнения количество достигло лимита
	 */
	public boolean rollAndGive(final int itemId, final int min, final int max, final int limit, final double calcChance, final boolean prof)
	{
		if(calcChance <= 0 || min <= 0 || max <= 0 || limit <= 0 || itemId <= 0)
			return false;
		long count = rollDrop(min, max, calcChance, prof);
		if(count > 0)
		{
			final long alreadyCount = getQuestItemsCount(itemId);
			if(alreadyCount + count > limit)
				count = limit - alreadyCount;
			if(count > 0)
			{
				giveItems(itemId, count, false);
				if(count + alreadyCount < limit)
					playSound(Quest.SOUND_ITEMGET);
				else
				{
					playSound(Quest.SOUND_MIDDLE);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param min
	 *            минимальное количество при рейтах 1х
	 * @param max
	 *            максимальное количество при рейтах 1х
	 * @param calcChance
	 */
	public void rollAndGive(final int itemId, final int min, final int max, final double calcChance)
	{
		if(calcChance <= 0 || min <= 0 || max <= 0 || itemId <= 0)
			return;
		rollAndGive(itemId, min, max, calcChance, false);
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param min
	 *            минимальное количество при рейтах 1х
	 * @param max
	 *            максимальное количество при рейтах 1х
	 * @param prof
	 *            - учитывать дроп по параметру "рейт дропа для квестов на профу"
	 * @param calcChance
	 */
	public void rollAndGive(final int itemId, final int min, final int max, final double calcChance, final boolean prof)
	{
		if(calcChance <= 0 || min <= 0 || max <= 0 || itemId <= 0)
			return;
		final int count = rollDrop(min, max, calcChance, prof);
		if(count > 0)
		{
			giveItems(itemId, count, false);
			playSound(Quest.SOUND_ITEMGET);
		}
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param count
	 *            количество при рейтах 1х
	 * @param calcChance
	 */
	public boolean rollAndGive(final int itemId, final int count, final double calcChance)
	{
		if(calcChance <= 0 || count <= 0 || itemId <= 0)
			return false;
		return rollAndGive(itemId, count, calcChance, false);
	}

	/**
	 * Этот метод рассчитывает количество дропнутых вещей в зависимости от рейтов и дает их,
	 * а так же проигрывает звук получения вещи. <br>
	 * <br>
	 * Следует учесть, что контроль за верхним пределом вещей в квестах, в которых
	 * нужно набить определенное количество предметов не осуществляется. <br>
	 * <br>
	 * Ни один из передаваемых параметров не должен быть равен 0
	 * 
	 * @param itemId
	 *            id вещи
	 * @param count
	 *            количество при рейтах 1х
	 * @param calcChance
	 * @param prof
	 *            - учитывать дроп по параметру "рейт дропа для квестов на профу"
	 */
	public boolean rollAndGive(final int itemId, final int count, final double calcChance, final boolean prof)
	{
		if(calcChance <= 0 || count <= 0 || itemId <= 0)
			return false;
		final int countToDrop = rollDrop(count, calcChance, prof);
		if(countToDrop > 0)
		{
			giveItems(itemId, countToDrop, false);
			playSound(Quest.SOUND_ITEMGET);
			return true;
		}
		return false;
	}

	/**
	 * Return true if quest completed, false otherwise
	 * 
	 * @return boolean
	 */
	public boolean isCompleted()
	{
		return getState() == Quest.COMPLETED;
	}

	/**
	 * Return true if quest started, false otherwise
	 * 
	 * @return boolean
	 */
	public boolean isStarted()
	{
		return getState() != Quest.CREATED && getState() != Quest.COMPLETED;
	}

	public void killNpcByObjectId(final int _objId)
	{
		final L2Object obj = L2ObjectsStorage.findObject(_objId);
		if(obj instanceof L2NpcInstance)
			((L2NpcInstance) obj).doDie(null);
		else
			_log.warning("Attemp to kill object that is not npc in quest " + getQuest().getQuestIntId());
	}

	/**
	 * Аналог set с флагом true, но если получает cond проверяет нотацию (для совместимости).
	 */
	public String set(final String var, final String val)
	{
		if(var.equalsIgnoreCase("cond"))
			return setCond(Integer.parseInt(val));

		return set(var, val, true);
	}

	/**
	 * Аналог set с флагом true, но если получает cond проверяет нотацию (для совместимости).
	 */
	public String set(final String var, final int intval)
	{
		if(var.equalsIgnoreCase("cond"))
			return setCond(intval);

		return set(var, String.valueOf(intval), true);
	}

	/**
	 * <font color=red>Использовать осторожно! Служебная функция!</font><br>
	 * <br>
	 * Устанавливает переменную и сохраняет в базу, если установлен флаг. Если получен cond обновляет список квестов игрока (только с флагом).
	 * 
	 * @param var
	 *            : String pointing out the name of the variable for quest
	 * @param val
	 *            : String pointing out the value of the variable for quest
	 * @param store
	 *            : Сохраняет в базу и если var это cond обновляет список квестов игрока.
	 * @return String (equal to parameter "val")
	 */
	public String set(final String var, String val, final boolean store)
	{
		if(_vars == null)
			_vars = new ConcurrentHashMap<String, String>();
		if(val == null)
			val = "";
		_vars.put(var, val);

		if(store)
		{
			final L2Player player = getPlayer();
			if(player == null)
				return null;
			Quest.updateQuestVarInDb(this, var, val);
			if(var.equalsIgnoreCase("cond"))
			{
				player.sendPacket(new QuestList(player));
				if(!val.equals("0") && (getQuest().getQuestIntId() < 999 || getQuest().getQuestIntId() > 10000) && isStarted())
					player.sendPacket(new ExShowQuestMark(getQuest().getQuestIntId()));
			}
		}

		return val;
	}

	/**
	 * Return state of the quest after its initialization.<BR>
	 * <BR>
	 * <U><I>Actions :</I></U> <LI>Remove drops from previous state</LI> <LI>Set new state of the quest</LI> <LI>Add drop for new state</LI> <LI>Update information in database</LI> <LI>Send packet QuestList to client</LI>
	 * 
	 * @param state
	 * @return object
	 */
	public Object setState(final int state)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return null;

		// set new state if it is not already in that state
		if(_state != state)
		{
			// set new state
			_state = state;

			if((getQuest().getQuestIntId() < 999 || getQuest().getQuestIntId() > 10000) && isStarted())
				player.sendPacket(new ExShowQuestMark(getQuest().getQuestIntId()));

			Quest.updateQuestInDb(this);
			player.sendPacket(new QuestList(player));
		}

		return state;
	}

	public Object setStateAndNotSave(final int state)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return null;

		// set new state if it is not already in that state
		if(_state != state)
		{
			// set new state
			_state = state;
			player.sendPacket(new QuestList(player));
		}

		return state;
	}

	public void removeRadar(final int x, final int y, final int z)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.radar.removeMarker(x, y, z);
	}

	/**
	 * Send a packet in order to play sound at client terminal
	 * 
	 * @param sound
	 */
	public void playSound(final String sound)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.sendPacket(new PlaySound(sound));
	}

	public void playSound(final PlaySound sound)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.sendPacket(sound);
	}

	public void giveReward()
	{
		quest.giveReward(getPlayer());
	}

	public void playTutorialVoice(final String voice)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.sendPacket(new PlaySound(2, voice, 0, 0, player.getLoc()));
	}

	public void onTutorialClientEvent(final int number)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.sendPacket(new TutorialEnableClientEvent(number));
	}

	public void showQuestionMark(final int number)
	{
		final L2Player player = getPlayer();
		if(player != null)
			player.sendPacket(new TutorialShowQuestionMark(number));
	}

	public void showTutorialHTML(final String html)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;
		String text = Files.read("data/scripts/quests/_255_Tutorial/" + html, player);
		if(text == null || text.equalsIgnoreCase(""))
			text = "<html><body>File data/scripts/quests/_255_Tutorial/" + html + " not found or file is empty.</body></html>";
		player.sendPacket(new TutorialShowHtml(text));
	}

	/**
	 * Start a timer for quest.<BR>
	 * <BR>
	 * 
	 * @param name
	 * <BR>
	 *            The name of the timer. Will also be the value for event of onEvent
	 * @param time
	 * <BR>
	 *            The milisecond value the timer will elapse
	 */
	public void startQuestTimer(final String name, final long time)
	{
		getQuest().startQuestTimer(name, time, null, getPlayer());
	}

	/**
	 * Удаляет указанные предметы из инвентаря игрока, и обновляет инвентарь
	 * 
	 * @param itemId
	 *            : id удаляемого предмета
	 * @param count
	 *            : число удаляемых предметов<br>
	 *            Если count передать -1, то будут удалены все указанные предметы.
	 * @return Количество удаленных предметов
	 */
	public long takeItems(final int itemId, long count)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return 0;

		// Get object item from player's inventory list
		final L2ItemInstance item = player.getInventory().getItemByItemId(itemId);
		if(item == null)
			return 0;
		// Tests on count value in order not to have negative value
		if(count < 0 || count > item.getCount())
			count = item.getCount();

		// Destroy the quantity of items wanted
		player.getInventory().destroyItemByItemId(itemId, count, true);
		// Send message of destruction to client
		player.sendPacket(SystemMessage.removeItems(itemId, count));

		return count;
	}

	public long takeAllItems(final int itemId)
	{
		return takeItems(itemId, -1);
	}

	public long takeAllItems(final int... itemsIds)
	{
		long result = 0L;
		for(final int id : itemsIds)
			result = result + takeAllItems(id);
		return result;
	}

	public long takeAllItems(final Collection<Integer> itemsIds)
	{
		long result = 0L;
		for(final int id : itemsIds)
			result = result + takeAllItems(id);
		return result;
	}

	/**
	 * Remove the variable of quest from the list of variables for the quest.<BR>
	 * <BR>
	 * <U><I>Concept : </I></U>
	 * Remove the variable of quest represented by "var" from the class variable FastMap "vars" and from the database.
	 * 
	 * @param var
	 *            : String designating the variable for the quest to be deleted
	 * @return String pointing out the previous value associated with the variable "var"
	 */
	public String unset(final String var)
	{
		if(_vars == null || var == null)
			return null;
		final String old = _vars.remove(var);
		if(old != null)
			Quest.deleteQuestVarInDb(this, var);
		return old;
	}

	private boolean checkPartyMember(final L2Player member, final int st, final int maxrange, final L2Object rangefrom)
	{
		if(member == null)
			return false;
		if(rangefrom != null && maxrange > 0 && !member.isInRange(rangefrom, maxrange))
			return false;
		final QuestState qs = member.getQuestState(getQuest().getName());
		if(qs == null || qs.getState() != st)
			return false;
		return true;
	}

	public GArray<L2Player> getPartyMembers(final int st, final int maxrange, final L2Object rangefrom)
	{
		final GArray<L2Player> result = new GArray<L2Player>();
		final L2Party party = getPlayer().getParty();
		if(party == null)
		{
			if(checkPartyMember(getPlayer(), st, maxrange, rangefrom))
				result.add(getPlayer());
			return result;
		}

		for(final L2Player _member : party.getPartyMembers())
			if(checkPartyMember(_member, st, maxrange, rangefrom))
				result.add(getPlayer());

		return result;
	}

	public L2Player getRandomPartyMember(final int st)
	{
		return getRandomPartyMember(st, 0, null);
	}

	public L2Player getRandomPartyMember(final int st, final int maxrangefromplayer)
	{
		return getRandomPartyMember(st, maxrangefromplayer, getPlayer());
	}

	public L2Player getRandomPartyMember(final int st, final int maxrange, final L2Object rangefrom)
	{
		final GArray<L2Player> list = getPartyMembers(st, maxrange, rangefrom);
		if(list.size() == 0)
			return null;
		return list.get(Rnd.get(list.size()));
	}

	public L2NpcInstance addSpawn(final int npcId)
	{
		return addSpawn(npcId, getPlayer().getX(), getPlayer().getY(), getPlayer().getZ(), 0, false, 0);
	}

	public L2NpcInstance addSpawn(final int npcId, final int despawnDelay)
	{
		return addSpawn(npcId, getPlayer().getX(), getPlayer().getY(), getPlayer().getZ(), 0, false, despawnDelay);
	}

	public L2NpcInstance addSpawn(final int npcId, final int x, final int y, final int z)
	{
		return addSpawn(npcId, x, y, z, 0, false, 0);
	}

	public L2NpcInstance addSpawn(final int npcId, final int x, final int y, final int z, final int despawnDelay)
	{
		return addSpawn(npcId, x, y, z, 0, false, despawnDelay);
	}

	public L2NpcInstance addSpawn(final int npcId, final int x, final int y, final int z, final int heading, final boolean randomOffset, final int despawnDelay)
	{
		return getQuest().addSpawn(npcId, x, y, z, heading, randomOffset, despawnDelay);
	}

	public L2NpcInstance findTemplate(final int npcId)
	{
		for(final L2Spawn spawn : SpawnTable.getInstance().getSpawnTable())
			if(spawn != null && spawn.getNpcId() == npcId)
				return spawn.getLastSpawn();
		return null;
	}

	public int calculateLevelDiffForDrop(final int mobLevel, final int player)
	{
		if(!Config.DEEPBLUE_DROP_RULES)
			return 0;
		return Math.max(player - mobLevel - Config.DEEPBLUE_DROP_MAXDIFF, 0);
	}

	/**
	 * Возвращает текущий номер конда в стандартной нотации. Если не определен возвращает 0.
	 */
	public int getCond()
	{
		final int value = getRawInt("cond");
		if(value < 0) // новая побитовая нотация
			return bitToInt(value);
		return value;
	}

	private int bitToInt(final int value)
	{
		// перебираем биты начиная с 30, поскольку 31 это бит знака
		for(int i = 30; i >= 0; i--)
			if((value & 1 << i) > 0) // если бит под этим номером определен
				return i + 1; // нумерация битов начинается с 0, а кондов с 1
		return 0;
	}

	/** Первый конд пропускать нельзя, Integer.MIN_VALUE флаг новой нотации */
	private static final int mask = 1 | Integer.MIN_VALUE;

	/**
	 * Записывает номер конда с проверкой нотации и сохраняет в базу используя set(String, String, true).
	 */
	public String setCond(final int newCond)
	{
		int oldCond = getRawInt("cond");

		if(oldCond < 0)
		{
			// это уже новая нотация
			final int oldCondStd = bitToInt(oldCond);
			if(newCond < oldCondStd)
			{
				// возвращаемся назад
				for(int i = oldCondStd - 1; i >= newCond; i--)
					if((oldCond & 1 << i) > 0)
						oldCond ^= 1 << i;
				return set("cond", String.valueOf(oldCond), true);
			}
			// добавляем бит нового конда
			return set("cond", String.valueOf(oldCond | 1 << newCond - 1), true);
		}
		if(oldCond >= newCond - 1)
			// новый конд больше старого на 1 или вообще меньше старого, нет нужды в новой нотации
			return set("cond", String.valueOf(newCond), true);
		// это старый квест, но пропущен конд, меняем на новую нотацию
		return set("cond", String.valueOf((1 << oldCond) - 1 | 1 << newCond - 1 | mask), true);
	}
}
