package l2n.game.model.quest;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.ArrayUtil;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuestRewardTable
{
	private final static Logger _log = Logger.getLogger(QuestRewardTable.class.getName());

	public static class Reward
	{
		private final L2Item item;
		private final long count;

		public Reward(final L2Item template, final long value)
		{
			item = template;
			count = value;
		}

		public final int getItemId()
		{
			return item.getItemId();
		}

		public final long getCount()
		{
			return count;
		}

		public final L2Item getTemplate()
		{
			return item;
		}

		public final boolean isStackable()
		{
			return item.isStackable();
		}
	}

	public static class QuestReward
	{
		public final int questId;
		public final String name;
		private Reward[] items = new Reward[0];

		public long exp = 0;
		public long sp = 0;

		public QuestReward(final int id, final String n)
		{
			questId = id;
			name = n;
		}

		public Reward[] getItems()
		{
			return items;
		}

		public void addReward(final Reward r)
		{
			items = ArrayUtil.arrayAdd(items, r);
		}
	}

	private final TIntObjectHashMap<QuestReward> rewards;

	public QuestRewardTable()
	{
		rewards = new TIntObjectHashMap<QuestReward>();
		load();
		_log.info("QuestRewardTable: loaded " + rewards.size() + " rewards.");
	}

	public static QuestRewardTable getInstance()
	{
		return SingletonHolder._instance;
	}

	public void reload()
	{
		rewards.clear();
		load();
		_log.info("QuestRewardTable: reloaded " + rewards.size() + " rewards.");
	}

	private void load()
	{
		final File file = new File(Config.DATAPACK_ROOT, "data/quest_reward.xml");
		if(!file.exists())
		{
			_log.warning("File " + file.getAbsolutePath() + " not exists");
			return;
		}

		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			QuestReward quest;
			Reward r;
			L2Item template;
			final Document doc = factory.newDocumentBuilder().parse(file);
			for(Node iz = doc.getFirstChild(); iz != null; iz = iz.getNextSibling())
				if("list".equalsIgnoreCase(iz.getNodeName()))
					for(Node area = iz.getFirstChild(); area != null; area = area.getNextSibling())
						if("quest".equalsIgnoreCase(area.getNodeName()))
						{
							final int questId = XMLUtil.getAttributeIntValue(area, "id");
							final String questName = XMLUtil.getAttributeValue(area, "name");

							quest = new QuestReward(questId, questName);

							for(Node rewards = area.getFirstChild(); rewards != null; rewards = rewards.getNextSibling())
								if("reward".equalsIgnoreCase(rewards.getNodeName()))
								{
									int itemId = 0;
									try
									{
										itemId = XMLUtil.getAttributeIntValue(rewards, "itemId", 0);
									}
									catch(final StringIndexOutOfBoundsException e)
									{
										continue;
									}

									final long count = XMLUtil.getAttributeIntValue(rewards, "count", 0);

									// exp
									if(itemId == -1000)
									{
										quest.exp = count;
										continue;
									}
									// sp
									else if(itemId == -1001)
									{
										quest.sp = count;
										continue;
									}

									template = ItemTable.getInstance().getTemplate(itemId);
									if(template == null)
									{
										// _log.log(Level.WARNING, "Not found itemdId:" + itemId + " for Q[" + questId + "]");
										continue;
									}

									r = new Reward(template, count);
									quest.addReward(r);
								}

							rewards.put(questId, quest);
						}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error on loading file - " + file.getName() + ": " + e.getMessage(), e);
		}
	}

	public QuestReward getReward(final int qId)
	{
		return rewards.get(qId);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final QuestRewardTable _instance = new QuestRewardTable();
	}
}
