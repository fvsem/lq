package l2n.game.model;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TObjectProcedure;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.database.utils.mysql;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.CrestCache;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.AuctionManager;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.instances.L2TerritoryFlagInstance;
import l2n.game.model.items.ClanWarehouse;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.ClanTable;
import l2n.game.tables.SkillTable;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2Clan
{
	private static final Logger _log = Logger.getLogger(L2Clan.class.getName());

	private static final TIntObjectHashMap<TIntObjectHashMap<L2Skill>> EMPTY_SQUAD_SKILLS = new TIntObjectHashMap<TIntObjectHashMap<L2Skill>>(0);

	// Clan Privileges
	/** No privilege to manage any clan activity */
	public static final int CP_NOTHING = 0 << 0;
	/** Privilege to join clan */
	public static final int CP_CL_JOIN_CLAN = 2;
	/** Privilege to give a title */
	public static final int CP_CL_GIVE_TITLE = 4;
	/** Privilege to view warehouse content */
	public static final int CP_CL_VIEW_WAREHOUSE = 8;
	/** Privilege to manage clan ranks */
	public static final int CP_CL_MANAGE_RANKS = 16;
	public static final int CP_CL_PLEDGE_WAR = 32;
	public static final int CP_CL_DISMISS = 64;
	/** Privilege to register clan crest */
	public static final int CP_CL_REGISTER_CREST = 128;
	public static final int CP_CL_APPRENTICE = 256;
	public static final int CP_CL_TROOPS_FAME = 512;
	public static final int CP_CL_SUMMON_AIRSHIP = 1024;
	/** Privilege to open a door */
	public static final int CP_CH_OPEN_DOOR = 2048;
	public static final int CP_CH_OTHER_RIGHTS = 4096;
	public static final int CP_CH_AUCTION = 8192;
	public static final int CP_CH_DISMISS = 16384;
	public static final int CP_CH_SET_FUNCTIONS = 32768;
	/** Privileges: castle/fotress */
	public static final int CP_CS_OPEN_DOOR = 65536;
	public static final int CP_CS_MANOR_ADMIN = 131072;
	public static final int CP_CS_MANAGE_SIEGE = 262144;
	public static final int CP_CS_USE_FUNCTIONS = 524288;
	public static final int CP_CS_DISMISS = 1048576;
	public static final int CP_CS_TAXES = 2097152;
	public static final int CP_CS_MERCENARIES = 4194304;
	public static final int CP_CS_SET_FUNCTIONS = 8388608;
	/** Privilege to manage all clan activity */
	public static final int CP_ALL = 16777214;

	public static final int RANK_FIRST = 1;
	public static final int RANK_LAST = 9;

	// Sub-unit types
	public static final int SUBUNIT_NONE = 0;
	/** Clan subunit type of Academy */
	public static final int SUBUNIT_ACADEMY = -1;
	/** Clan subunit type of Royal Guard A */
	public static final int SUBUNIT_ROYAL1 = 100;
	/** Clan subunit type of Royal Guard B */
	public static final int SUBUNIT_ROYAL2 = 200;
	/** Clan subunit type of Order of Knights A-1 */
	public static final int SUBUNIT_KNIGHT1 = 1001;
	/** Clan subunit type of Order of Knights A-2 */
	public static final int SUBUNIT_KNIGHT2 = 1002;
	/** Clan subunit type of Order of Knights B-1 */
	public static final int SUBUNIT_KNIGHT3 = 2001;
	/** Clan subunit type of Order of Knights B-2 */
	public static final int SUBUNIT_KNIGHT4 = 2002;

	private static final int ROYAL1 = Config.CLANREP_CREATE_ROYAL1;
	private static final int ROYAL2 = Config.CLANREP_CREATE_ROYAL2;
	private static final int KNIGHT1 = Config.CLANREP_CREATE_KNIGHT1;
	private static final int KNIGHT2 = Config.CLANREP_CREATE_KNIGHT2;
	private static final int KNIGHT3 = Config.CLANREP_CREATE_KNIGHT3;
	private static final int KNIGHT4 = Config.CLANREP_CREATE_KNIGHT4;

	private static final ClanReputationComparator REPUTATION_COMPARATOR = new ClanReputationComparator();

	/** Количество мест в таблице рангов кланов */
	private static final int REPUTATION_PLACES = 100;

	public static final int MAX_NOTICE_LENGTH = 512;

	// all these in milliseconds
	public static final long EXPELLED_MEMBER_PENALTY = Config.DAYS_BEFORE_CLAN_INVITE * 60 * 60 * 1000;
	public static final long LEAVED_ALLY_PENALTY = 24 * 60 * 60 * 1000;
	public static final long DISSOLVED_ALLY_PENALTY = Config.DAYS_BEFORE_CREATE_NEW_ALLY_WHEN_DISSOLVED * 60 * 60 * 1000;

	private String _name;
	private int _clanId;
	private L2ClanMember _leader = null;
	private final ConcurrentHashMap<Integer, L2ClanMember> _members = new ConcurrentHashMap<Integer, L2ClanMember>();

	private int _allyId;
	private byte _level;
	private int _hasCastle = 0;
	private int _hasFortress = 0;
	private int _hiredGuards;
	private int _hasHideout = 0;
	private int _crestId;
	private int _crestLargeId;

	private long _expelledMemberTime;
	private long _leavedAllyTime;
	private long _dissolvedAllyTime;

	// all these in milliseconds
	private L2AirShip _airship;
	private boolean _airshipLicense;
	private int _airshipFuel;

	private final ClanWarehouse _warehouse = new ClanWarehouse(this);
	private int _whBonus = -1;

	private final GArray<L2Clan> _atWarWith = new GArray<L2Clan>();
	private final GArray<L2Clan> _underAttackFrom = new GArray<L2Clan>();

	/** FastMap(Integer, L2Skill) containing all skills of the L2Clan */
	protected final TIntObjectHashMap<L2Skill> _skills = new TIntObjectHashMap<L2Skill>();
	/** ключ - номер отряда (0 - основной клан), значение - списко скилов для отряда */
	protected final TIntObjectHashMap<TIntObjectHashMap<L2Skill>> _squadSkills = new TIntObjectHashMap<TIntObjectHashMap<L2Skill>>();

	protected TIntObjectHashMap<RankPrivs> _privs = new TIntObjectHashMap<RankPrivs>();
	protected TIntObjectHashMap<SubPledge> _subPledges = new TIntObjectHashMap<SubPledge>();

	private int _reputation = 0;

	private L2ClanGate _clanGate = null; // врата клансуммона
	private Siege _siege;
	private boolean _isDefender;
	private boolean _isAttacker;
	private int _territorySide = -1;

	private String _notice;
	@SuppressWarnings("unused")
	private boolean _noticeEnabled = false;

	/**
	 * Конструктор используется только внутри для восстановления из базы
	 */
	private L2Clan(final int clanId)
	{
		_clanId = clanId;
		initializePrivs();
	}

	public L2Clan(final int clanId, final String clanName, final L2ClanMember leader)
	{
		_clanId = clanId;
		_name = clanName;
		initializePrivs();
		setLeader(leader);
		insertNotice();
	}

	public int getClanId()
	{
		return _clanId;
	}

	public void setClanId(final int clanId)
	{
		_clanId = clanId;
	}

	public int getLeaderId()
	{
		return _leader != null ? _leader.getObjectId() : 0;
	}

	public L2ClanMember getLeader()
	{
		return _leader;
	}

	public void setLeader(final L2ClanMember leader)
	{
		_leader = leader;
		_members.put(leader.getObjectId(), leader);
	}

	public String getLeaderName()
	{
		return _leader.getName();
	}

	public String getName()
	{
		return _name;
	}

	public void setName(final String name)
	{
		_name = name;
	}

	private void addClanMember(final L2ClanMember member)
	{
		_members.put(member.getObjectId(), member);
	}

	public void addClanMember(final L2Player player)
	{
		addClanMember(new L2ClanMember(this, player.getName(), player.getTitle(), player.getLevel(), player.getClassId().getId(), player.getObjectId(), player.getPledgeType(), player.getPowerGrade(), player.getApprentice(), false));
	}

	public L2ClanMember getClanMember(final int id)
	{
		return _members.get(id);
	}

	public L2ClanMember getClanMember(final String name)
	{
		for(final L2ClanMember member : _members.values())
			if(member.getName().equals(name))
				return member;
		return null;
	}

	public int getMembersCount()
	{
		return _members.size();
	}

	public void flush()
	{
		for(final L2ClanMember member : getMembers())
			removeClanMember(member.getObjectId());
		for(final L2ItemInstance item : _warehouse.listItems(ItemClass.ALL))
			_warehouse.destroyItem(item.getItemId(), item.getCount());
		if(_hasCastle != 0)
			CastleManager.getInstance().getCastleByIndex(_hasCastle).changeOwner(null);
		if(_hasFortress != 0)
			FortressManager.getInstance().getFortressByIndex(_hasFortress).changeOwner(null);
	}

	public void removeClanMember(final int id)
	{
		if(id == getLeaderId())
			return;
		final L2ClanMember exMember = _members.remove(id);
		if(exMember == null)
			return;
		final SubPledge sp = _subPledges.get(exMember.getPledgeType());
		if(sp != null && sp.getLeaderId() == exMember.getObjectId()) // subpledge leader
			sp.setLeaderId(0); // clan leader has to assign another one, via villagemaster
		if(exMember.hasSponsor())
			getClanMember(exMember.getSponsor()).setApprentice(0);
		removeMemberInDatabase(exMember);
        final L2Player cha = exMember.getPlayer();
        if(cha.isTerritoryFlagEquipped()) //РєРѕСЃС‚С‹Р»СЊ РЅРµР¶РµР»Рё СЂР°С†РёРѕРЅР°Р»СЊРЅС‹Р№ СЃРїРѕСЃРѕР±Рѕ С„РёРєСЃР°
{
	final L2ItemInstance flag = cha.getActiveWeaponInstance();
	if(flag != null && flag.getCustomType1() != 77) // 77 СЌС‚Рѕ СЌРІРµРЅС‚РѕРІС‹Р№ С„Р»Р°Рі
	{
		final L2TerritoryFlagInstance flagNpc = TerritorySiege.getNpcFlagByItemId(flag.getItemId());
		flagNpc.drop(cha);

		cha.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(flag.getItemId()));
		final String terrName = CastleManager.getInstance().getCastleByIndex(flagNpc.getBaseTerritoryId()).getName();
		TerritorySiege.announceToPlayer(new SystemMessage(2751).addString(terrName), true);
	}
}
	}

	public L2ClanMember[] getMembers()
	{
		return _members.values().toArray(new L2ClanMember[_members.size()]);
	}

	public L2Player[] getOnlineMembers(final String exclude)
	{
		final GArray<L2Player> result = new GArray<L2Player>();
		for(final L2ClanMember temp : _members.values())
			if(temp.isOnline() && (exclude == null || !temp.getName().equals(exclude)))
				result.add(temp.getPlayer());

		return result.toArray(new L2Player[result.size()]);
	}

	public L2Player[] getOnlineMembers(final int exclude)
	{
		final GArray<L2Player> result = new GArray<L2Player>(_members.values().size());
		for(final L2ClanMember temp : _members.values())
			if(temp.isOnline() && temp.getObjectId() != exclude)
				result.add(temp.getPlayer());

		return result.toArray(new L2Player[result.size()]);
	}

	public int getAllyId()
	{
		return _allyId;
	}

	public byte getLevel()
	{
		return _level;
	}

	public int getHasCastle()
	{
		return _hasCastle;
	}

	public int getHasFortress()
	{
		return _hasFortress;
	}

	public int getHasHideout()
	{
		return _hasHideout;
	}

	public void setAllyId(final int allyId)
	{
		_allyId = allyId;
	}

	public void setHasCastle(final int castle)
	{
		if(_hasFortress == 0)
			_hasCastle = castle;
	}

	public void setHasFortress(final int fortress)
	{
		if(_hasCastle == 0)
			_hasFortress = fortress;
	}

	public void setHasHideout(final int hasHideout)
	{
		_hasHideout = hasHideout;
	}

	public void setLevel(final byte level)
	{
		_level = level;
	}

	public boolean isMember(final int id)
	{
		return _members.containsKey(id);
	}

	public void updateClanInDB()
	{
		if(getLeaderId() == 0)
		{
			_log.warning("updateClanInDB with empty LeaderId");
			Thread.dumpStack();
			return;
		}

		if(getClanId() == 0)
		{
			_log.warning("updateClanInDB with empty ClanId");
			Thread.dumpStack();
			return;
		}
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET leader_id=?,ally_id=?,reputation_score=?,expelled_member=?,leaved_ally=?,dissolved_ally=?,clan_level=?,warehouse=?,clan_name=?,airship=? WHERE clan_id=?");
			statement.setInt(1, getLeaderId());
			statement.setInt(2, getAllyId());
			statement.setInt(3, getReputationScore());
			statement.setLong(4, getExpelledMemberTime() / 1000);
			statement.setLong(5, getLeavedAllyTime() / 1000);
			statement.setLong(6, getDissolvedAllyTime() / 1000);
			statement.setInt(7, _level);
			statement.setInt(8, getWhBonus());
			statement.setString(9, getName());
			statement.setInt(10, isHaveAirshipLicense() ? getAirshipFuel() : -1);
			statement.setInt(11, getClanId());
			statement.execute();

			if(Config.DEBUG)
				_log.info("Clan data saved in db: " + getClanId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while updating clan '" + getClanId() + "' data in db: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void store()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO clan_data (clan_id,clan_name,clan_level,hasCastle,hasFortress,hasHideout,ally_id,leader_id,expelled_member,leaved_ally,dissolved_ally,airship) values (?,?,?,?,?,?,?,?,?,?,?,?)");
			statement.setInt(1, _clanId);
			statement.setString(2, _name);
			statement.setInt(3, _level);
			statement.setInt(4, _hasCastle);
			statement.setInt(5, _hasFortress);
			statement.setInt(6, _hasHideout);
			statement.setInt(7, _allyId);
			statement.setInt(8, getLeaderId());
			statement.setLong(9, getExpelledMemberTime() / 1000);
			statement.setLong(10, getLeavedAllyTime() / 1000);
			statement.setLong(11, getDissolvedAllyTime() / 1000);
			statement.setInt(12, isHaveAirshipLicense() ? getAirshipFuel() : -1);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE characters SET clanid=?,pledge_type=0 WHERE obj_Id=?");
			statement.setInt(1, getClanId());
			statement.setInt(2, getLeaderId());
			statement.execute();

			if(Config.DEBUG)
				_log.info("New clan saved in db: " + getClanId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while saving new clan to db: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void removeMemberInDatabase(final L2ClanMember member)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET clanid=0, pledge_type=0, pledge_rank=0, lvl_joined_academy=0, apprentice=0, title='', leaveclan=? WHERE obj_Id=?");
			statement.setLong(1, member.getPledgeType() == SUBUNIT_ACADEMY ? 0 : System.currentTimeMillis() / 1000);
			statement.setInt(2, member.getObjectId());
			statement.execute();

			if(Config.DEBUG)
				_log.info("clan member removed in db: " + getClanId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while removing clan member in db: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static L2Clan restore(final int clanId)
	{
		if(clanId == 0) // no clan
			return null;

		L2Clan clan = null;
		int leaderId = 0;

		ThreadConnection con1 = null;
		FiltredPreparedStatement statement1 = null;
		ResultSet clanData = null;
		try
		{
			con1 = L2DatabaseFactory.getInstance().getConnection();
			statement1 = con1.prepareStatement("SELECT clan_name,clan_level,hasCastle,hasFortress,hasHideout,ally_id,leader_id,reputation_score,expelled_member,leaved_ally,dissolved_ally,auction_bid_at,warehouse,airship FROM clan_data where clan_id=?");
			statement1.setInt(1, clanId);
			clanData = statement1.executeQuery();

			if(clanData.next())
			{
				clan = new L2Clan(clanId);
				clan.setName(clanData.getString("clan_name"));
				clan.setLevel(clanData.getByte("clan_level"));
				clan.setHasCastle(clanData.getByte("hasCastle"));
				clan.setHasFortress(clanData.getByte("hasFortress"));
				clan.setHasHideout(clanData.getInt("hasHideout"));
				clan.setAllyId(clanData.getInt("ally_id"));
				clan._reputation = clanData.getInt("reputation_score");
				clan.setAuctionBiddedAt(clanData.getInt("auction_bid_at"));
				clan.setExpelledMemberTime(clanData.getLong("expelled_member") * 1000);
				clan.setLeavedAllyTime(clanData.getLong("leaved_ally") * 1000);
				clan.setDissolvedAllyTime(clanData.getLong("dissolved_ally") * 1000);
				clan.setWhBonus(clanData.getInt("warehouse"));
				clan.setAirshipLicense(clanData.getInt("airship") == -1 ? false : true);
				if(clan.isHaveAirshipLicense())
					clan.setAirshipFuel(clanData.getInt("airship"));

				leaderId = clanData.getInt("leader_id");
			}
			else
			{
				_log.warning("L2Clan clan " + clanId + " does't exist");
				return null;
			}

			if(clan.getName() == null)
				_log.warning("null name for clan?? " + clanId);

			if(clan.getAuctionBiddedAt() > 0 && AuctionManager.getInstance().getAuction(clan.getAuctionBiddedAt()) == null)
				clan.setAuctionBiddedAt(0);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while restoring clan: " + clanId, e);
		}
		finally
		{
			DbUtils.closeQuietly(con1, statement1, clanData);
		}

		if(clan == null)
		{
			_log.warning("Clan " + clanId + " does't exist");
			return null;
		}

		if(leaderId == 0)
		{
			_log.warning("Not found leader for clan: " + clanId);
			return null;
		}

		ThreadConnection con2 = null;
		FiltredPreparedStatement statement2 = null;
		ResultSet clanMembers = null;
		try
		{
			con2 = L2DatabaseFactory.getInstance().getConnection();
			statement2 = con2.prepareStatement("SELECT `c`.`char_name` AS `char_name`,`s`.`level` AS `level`,`s`.`class_id` AS `classid`,`c`.`obj_Id` AS `obj_id`,`c`.`title` AS `title`,`c`.`pledge_type` AS `pledge_type`,`c`.`pledge_rank` AS `pledge_rank`,`c`.`apprentice` AS `apprentice` FROM `characters` `c` LEFT JOIN `character_subclasses` `s` ON (`s`.`char_obj_id` = `c`.`obj_Id` AND `s`.`isBase` = '1') WHERE `c`.`clanid`=? ORDER BY `c`.`lastaccess` DESC");

			statement2.setInt(1, clanId);
			clanData = statement2.executeQuery();

			statement2.setInt(1, clan.getClanId());
			clanMembers = statement2.executeQuery();

			while (clanMembers.next())
			{
				final L2ClanMember member = new L2ClanMember(clan, clanMembers.getString("char_name"), clanMembers.getString("title"), clanMembers.getInt("level"), clanMembers.getInt("classid"), clanMembers.getInt("obj_id"), clanMembers.getInt("pledge_type"), clanMembers.getInt("pledge_rank"), clanMembers.getInt("apprentice"), Boolean.valueOf(clanMembers.getInt("obj_id") == leaderId));
				if(member.getObjectId() == leaderId)
					clan.setLeader(member);
				else
					clan.addClanMember(member);
			}
			if(clan.getLeader() == null)
				_log.severe("Clan " + clan.getName() + " have no leader!");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while restoring clan members for clan: " + clanId, e);
		}
		finally
		{
			DbUtils.closeQuietly(con2, statement2, clanMembers);
		}

		clan.restoreSkills();
		clan.restoreSubPledges();
		clan.restoreRankPrivs();
		clan.setCrestId(CrestCache.getPledgeCrestId(clanId));
		clan.setCrestLargeId(CrestCache.getPledgeCrestLargeId(clanId));

		return clan;
	}

	public void broadcastToOnlineMembers(final L2GameServerPacket... packets)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline())
				member.getPlayer().sendPacket(packets);
	}

	public void broadcastToOtherOnlineMembers(final L2GameServerPacket packet, final L2Player player)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline() && member.getPlayer() != player)
				member.getPlayer().sendPacket(packet);
	}

	@Override
	public String toString()
	{
		return getName();
	}

	public void setCrestId(final int newcrest)
	{
		_crestId = newcrest;
	}

	public int getCrestId()
	{
		return _crestId;
	}

	public boolean hasCrest()
	{
		return _crestId > 0;
	}

	public int getCrestLargeId()
	{
		return _crestLargeId;
	}

	public void setCrestLargeId(final int newcrest)
	{
		_crestLargeId = newcrest;
	}

	public boolean hasCrestLarge()
	{
		return _crestLargeId > 0;
	}

	public ClanWarehouse getWarehouse()
	{
		return _warehouse;
	}

	public long getAdenaCount()
	{
		return _warehouse.getAdenaCount();
	}

	public int getHiredGuards()
	{
		return _hiredGuards;
	}

	public void incrementHiredGuards()
	{
		_hiredGuards++;
	}

	public int isAtWar()
	{
		if(_atWarWith != null && !_atWarWith.isEmpty())
			return 1;
		return 0;
	}

	public int isAtWarOrUnderAttack()
	{
		if(_atWarWith != null && !_atWarWith.isEmpty() || _underAttackFrom != null && !_underAttackFrom.isEmpty())
			return 1;
		return 0;
	}

	public boolean isAtWarWith(final int id)
	{
		final L2Clan clan = ClanTable.getInstance().getClan(id);
		return _atWarWith != null && !_atWarWith.isEmpty() && _atWarWith.contains(clan);
	}

	public boolean isUnderAttackFrom(final int id)
	{
		final L2Clan clan = ClanTable.getInstance().getClan(id);
		return _underAttackFrom != null && !_underAttackFrom.isEmpty() && _underAttackFrom.contains(clan);
	}

	public void setEnemyClan(final L2Clan clan)
	{
		_atWarWith.add(clan);
	}

	public void deleteEnemyClan(final L2Clan clan)
	{
		_atWarWith.remove(clan);
	}

	// clans that are attacking this clan
	public void setAttackerClan(final L2Clan clan)
	{
		_underAttackFrom.add(clan);
	}

	public void setAttackerClan(final int clan)
	{
		final L2Clan Clan = ClanTable.getInstance().getClan(clan);
		_underAttackFrom.add(Clan);
	}

	public void deleteAttackerClan(final L2Clan clan)
	{
		_underAttackFrom.remove(clan);
	}

	public GArray<L2Clan> getEnemyClans()
	{
		return _atWarWith;
	}

	public int getWarsCount()
	{
		return _atWarWith.size();
	}

	public GArray<L2Clan> getAttackerClans()
	{
		return _underAttackFrom;
	}

	public void broadcastClanStatus(final boolean updateList, final boolean needUserInfo, final boolean relation)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline())
			{
				if(updateList)
					member.getPlayer().sendPacket(Msg.PledgeShowMemberListDeleteAll, new PledgeShowMemberListAll(this, member.getPlayer()));
				member.getPlayer().sendPacket(new PledgeShowInfoUpdate(this));
				if(needUserInfo)
					member.getPlayer().broadcastUserInfo(true);
				if(relation)
					member.getPlayer().broadcastRelationChanged();
			}
	}

	public L2Alliance getAlliance()
	{
		return _allyId == 0 ? null : ClanTable.getInstance().getAlliance(_allyId);
	}

	public void setExpelledMemberTime(final long time)
	{
		_expelledMemberTime = time;
	}

	public long getExpelledMemberTime()
	{
		return _expelledMemberTime;
	}

	public void setExpelledMember()
	{
		_expelledMemberTime = System.currentTimeMillis();
		updateClanInDB();
	}

	public void setLeavedAllyTime(final long time)
	{
		_leavedAllyTime = time;
	}

	public long getLeavedAllyTime()
	{
		return _leavedAllyTime;
	}

	public void setLeavedAlly()
	{
		_leavedAllyTime = System.currentTimeMillis();
		updateClanInDB();
	}

	public void setDissolvedAllyTime(final long time)
	{
		_dissolvedAllyTime = time;
	}

	public long getDissolvedAllyTime()
	{
		return _dissolvedAllyTime;
	}

	public void setDissolvedAlly()
	{
		_dissolvedAllyTime = System.currentTimeMillis();
		updateClanInDB();
	}

	public boolean canInvite()
	{
		return System.currentTimeMillis() - _expelledMemberTime >= EXPELLED_MEMBER_PENALTY;
	}

	public boolean canJoinAlly()
	{
		return System.currentTimeMillis() - _leavedAllyTime >= LEAVED_ALLY_PENALTY;
	}

	public boolean canCreateAlly()
	{
		return System.currentTimeMillis() - _dissolvedAllyTime >= DISSOLVED_ALLY_PENALTY;
	}

	public int getRank()
	{
		final L2Clan[] clans = ClanTable.getInstance().getClans();
		Arrays.sort(clans, REPUTATION_COMPARATOR);

		final int place = 1;
		for(int i = 0; i < clans.length; i++)
		{
			if(i == REPUTATION_PLACES)
				return 0;

			final L2Clan clan = clans[i];
			if(clan == this)
				return place + i;
		}

		return 0;
	}

	public int getReputationScore()
	{
		return _reputation;
	}

	public void setReputationScore(final int rep)
	{
		if(_reputation >= 0 && rep < 0)
		{
			broadcastToOnlineMembers(Msg.SINCE_THE_CLAN_REPUTATION_SCORE_HAS_DROPPED_TO_0_OR_LOWER_YOUR_CLAN_SKILLS_WILL_BE_DE_ACTIVATED);
			final L2Skill[] skills = getAllSkills();
			for(final L2ClanMember member : _members.values())
				if(member.isOnline())
					for(final L2Skill sk : skills)
						member.getPlayer().removeSkill(sk, false);
		}
		else if(_reputation < 0 && rep >= 0)
		{
			broadcastToOnlineMembers(Msg.THE_CLAN_SKILL_WILL_BE_ACTIVATED_BECAUSE_THE_CLANS_REPUTATION_SCORE_HAS_REACHED_TO_0_OR_HIGHER);
			final L2Skill[] skills = getAllSkills();
			for(final L2ClanMember member : _members.values())
				if(member.isOnline())
					for(final L2Skill sk : skills)
					{
						member.getPlayer().sendPacket(new PledgeSkillListAdd(sk.getId(), sk.getLevel()));
						if(sk.getMinPledgeClass() <= member.getPlayer().getPledgeClass())
							member.getPlayer().addSkill(sk, false);
					}
		}

		if(_reputation != rep)
		{
			_reputation = rep;
			broadcastToOnlineMembers(new PledgeShowInfoUpdate(this));
		}

		updateClanInDB();
	}

	public int incReputation(int inc, final boolean Rate, final String source)
	{
		if(_level < 5)
			return 0;

		if(Rate && Math.abs(inc) <= Config.RATE_CLAN_REP_SCORE_MAX_AFFECTED)
			inc = (int) Math.round(inc * Config.RATE_CLAN_REP_SCORE);

		setReputationScore(_reputation + inc);
		Log.add(_name + "|" + inc + "|" + _reputation + "|" + source, "clan_reputation");

		return inc;
	}

	/* ============================ clan skills stuff ============================ */

	private void restoreSkills()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			// Retrieve all skills of this L2Player from the database
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT skill_id, skill_level, squad_index FROM clan_skills WHERE clan_id=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();

			// Go though the recordset of this SQL query
			while (rset.next())
			{
				final int id = rset.getInt("skill_id");
				final int level = rset.getInt("skill_level");
				final int squad_index = rset.getInt("squad_index");
				// Create a L2Skill object for each record
				final L2Skill skill = SkillTable.getInstance().getInfo(id, level);
				if(squad_index > -1)
				{
					TIntObjectHashMap<L2Skill> skills = _squadSkills.get(squad_index);
					if(skills == null)
						skills = new TIntObjectHashMap<L2Skill>();

					skills.put(id, skill);
					_squadSkills.put(squad_index, skills);
				}
				else
					// Add the L2Skill object to the L2Clan _skills
					_skills.put(skill.getId(), skill);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore clan skills: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(statement, rset);
			DbUtils.closeQuietly(con);
		}
	}

	/**
	 * used to retrieve all skills
	 */
	public final L2Skill[] getAllSkills()
	{
		if(_reputation < 0)
			return SkillTable.EMPTY_ARRAY;
		return _skills.values(new L2Skill[_skills.valueCollection().size()]);
	}
	public final int getSkillLevel(final int skillId)
	{
		L2Skill skill = _skills.get(skillId);
		return skill == null ? -1 : skill.getLevel();
	}
	/**
	 * @return the squad skills of the main clan
	 */
	public TIntObjectHashMap<TIntObjectHashMap<L2Skill>> getSquadSkills()
	{
		if(_squadSkills == null)
			return EMPTY_SQUAD_SKILLS;
		return _squadSkills;
	}

	public L2Skill addNewSkill(final L2Skill newSkill, final boolean store)
	{
		return addNewSkill(newSkill, store, -1);
	}

	/**
	 * used to add a new skill to the list, send a packet to all online clan members, update their stats and store it in db
	 */
	public L2Skill addNewSkill(final L2Skill newSkill, final boolean store, final int squadIndex)
	{
		L2Skill oldSkill = null;
		if(newSkill != null)
		{
			// Replace oldSkill by newSkill or Add the newSkill
			if(squadIndex == -1)
				oldSkill = _skills.put(newSkill.getId(), newSkill);
			else if(squadIndex >= 0)
			{
				TIntObjectHashMap<L2Skill> map = _squadSkills.get(squadIndex);
				if(map == null)
					map = new TIntObjectHashMap<L2Skill>();
				oldSkill = map.put(newSkill.getId(), newSkill);
				_squadSkills.put(squadIndex, map);
			}
			else
			{
				_log.warning("Player " + getLeaderName() + " tried to add a Squad Skill to a squad that doesn't exist, ban him!");
				return null;
			}

			if(store)
			{
				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();

					if(oldSkill != null)
					{
						statement = con.prepareStatement("UPDATE clan_skills SET skill_level=? WHERE skill_id=? AND clan_id=?");
						statement.setInt(1, newSkill.getLevel());
						statement.setInt(2, oldSkill.getId());
						statement.setInt(3, getClanId());
						statement.execute();
						DbUtils.closeQuietly(statement);
					}
					else
					{
						statement = con.prepareStatement("INSERT INTO clan_skills (clan_id,skill_id,skill_level,skill_name,squad_index) VALUES (?,?,?,?,?)");
						statement.setInt(1, getClanId());
						statement.setInt(2, newSkill.getId());
						statement.setInt(3, newSkill.getLevel());
						statement.setString(4, newSkill.getName());
						statement.setInt(5, squadIndex);
						statement.execute();
						DbUtils.closeQuietly(statement);
					}
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "Error could not store char skills: ", e);
				}
				finally
				{
					DbUtils.closeQuietly(con);
				}
			}

			for(final L2ClanMember temp : _members.values())
				if(temp.isOnline() && temp.getPlayer() != null)
					if(squadIndex == -1 && newSkill.getMinPledgeClass() <= temp.getPlayer().getPledgeClass())
					{
						temp.getPlayer().addSkill(newSkill, false); // Skill is not saved to player DB
						temp.getPlayer().sendPacket(new PledgeSkillListAdd(newSkill.getId(), newSkill.getLevel()));
					}
					else if(temp.getPledgeType() == squadIndex)
					{
						temp.getPlayer().addSkill(newSkill, false); // Skill is not saved to player DB
						temp.getPlayer().sendPacket(new ExSubPledgeSkillAdd(newSkill.getId(), newSkill.getLevel(), squadIndex));
					}
		}
		return oldSkill;
	}

	public void removeSkill(final L2Skill skill)
	{
		_skills.remove(skill.getId());
		for(final L2ClanMember temp : _members.values())
			if(temp.isOnline() && temp.getPlayer() != null)
				temp.getPlayer().removeSkill(skill);
	}

	/** Делает то же самое что и след метод, только для всех членов клана */
	public void boarcastSkillListToOnlineMembers()
	{
		for(final L2ClanMember temp : _members.values())
			if(temp.isOnline() && temp.getPlayer() != null)
				addAndShowSkillsToPlayer(temp.getPlayer());
	}

	/** Добавляет игроку клан скилы и посылает пакет SkillList (при загрузке чара) */
	public void addAndShowSkillsToPlayer(final L2Player activeChar)
	{
		if(_reputation < 0)
			return;

		final GArray<L2GameServerPacket> packets = new GArray<L2GameServerPacket>();
		activeChar.sendPacket(new PledgeSkillList(this));
		_skills.forEachValue(new TObjectProcedure<L2Skill>()
		{
			@Override
			public boolean execute(final L2Skill s)
			{
				if(s != null)
				{
					packets.add(new PledgeSkillListAdd(s.getId(), s.getLevel()));
					if(s.getMinPledgeClass() <= activeChar.getPledgeClass())
						activeChar.addSkill(s, false);
				}
				return true;
			}
		});

		activeChar.sendPackets(packets);
		packets.clear();

		// проверяем скилы отрядов
		if(_squadSkills != null && !_squadSkills.isEmpty())
		{
			_squadSkills.forEachEntry(new TIntObjectProcedure<TIntObjectHashMap<L2Skill>>()
			{
				@Override
				public boolean execute(final int squadIndex, final TIntObjectHashMap<L2Skill> skills)
				{
					if(skills == null || skills.isEmpty())
						return true;

					for(final L2Skill skill : skills.valueCollection())
						if(squadIndex == activeChar.getPledgeType())
						{
							activeChar.addSkill(skill, false); // Skill is not saved to player DB
							packets.add(new ExSubPledgeSkillAdd(skill.getId(), skill.getLevel(), squadIndex));
						}
					return true;
				}
			});

			activeChar.sendPackets(packets);
			activeChar.sendPacket(new SkillList(activeChar));
		}
	}

	/**
	 * Обновляет инфу об умениях отряда для игрока (добавляет скил если нужно)
	 * 
	 * @param activeChar
	 */
	public void showSquadSkillsToPlayer(final L2Player activeChar)
	{
		if(_squadSkills == null || _squadSkills.isEmpty())
			return;

		final GArray<L2GameServerPacket> packets = new GArray<L2GameServerPacket>();
		_squadSkills.forEachEntry(new TIntObjectProcedure<TIntObjectHashMap<L2Skill>>()
		{
			@Override
			public boolean execute(final int squadIndex, final TIntObjectHashMap<L2Skill> skills)
			{
				if(skills == null || skills.isEmpty())
					return true;

				for(final L2Skill skill : skills.valueCollection())
				{
					packets.add(new ExSubPledgeSkillAdd(skill.getId(), skill.getLevel(), squadIndex));
					if(squadIndex == activeChar.getPledgeType())
						activeChar.addSkill(skill, false); // Skill is not saved to player DB
				}
				return true;
			}
		});

		activeChar.sendPackets(packets);
	}

	/* ============================ clan subpledges stuff ============================ */

	public class SubPledge
	{
		private final int _type;
		private int _leaderId;
		private final String _name;

		public SubPledge(final int type, final int leaderId, final String name)
		{
			_type = type;
			_leaderId = leaderId;
			_name = name;
		}

		public int getType()
		{
			return _type;
		}

		public String getName()
		{
			return _name;
		}

		public int getLeaderId()
		{
			return _leaderId;
		}

		public void setLeaderId(final int leaderId)
		{
			_leaderId = leaderId;
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("UPDATE clan_subpledges SET leader_id=? WHERE clan_id=? and type=?");
				statement.setInt(1, _leaderId);
				statement.setInt(2, getClanId());
				statement.setInt(3, _type);
				statement.execute();
			}
			catch(final Exception e)
			{}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}

		public String getLeaderName()
		{
			for(final L2ClanMember member : _members.values())
				if(member.getObjectId() == _leaderId)
					return member.getName();
			return "";
		}
	}

	public final boolean isAcademy(final int pledgeType)
	{
		return pledgeType == SUBUNIT_ACADEMY;
	}

	public final boolean isRoyalGuard(final int pledgeType)
	{
		return pledgeType == SUBUNIT_ROYAL1 || pledgeType == SUBUNIT_ROYAL2;
	}

	public final boolean isOrderOfKnights(final int pledgeType)
	{
		return pledgeType == SUBUNIT_KNIGHT1 || pledgeType == SUBUNIT_KNIGHT2 || pledgeType == SUBUNIT_KNIGHT3 || pledgeType == SUBUNIT_KNIGHT4;
	}

	public int getAffiliationRank(final int pledgeType)
	{
		if(isAcademy(pledgeType))
			return 9;
		else if(isOrderOfKnights(pledgeType))
			return 8;
		else if(isRoyalGuard(pledgeType))
			return 7;
		return 6;
	}

	/** used to retrieve subPledge by type */
	public final SubPledge getSubPledge(final int pledgeType)
	{
		if(_subPledges == null)
			return null;

		return _subPledges.get(pledgeType);
	}

	public final void addSubPledge(final SubPledge sp, final boolean updateDb)
	{
		_subPledges.put(sp.getType(), sp);

		if(updateDb)
		{
			broadcastToOnlineMembers(new PledgeReceiveSubPledgeCreated(sp));
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("INSERT INTO `clan_subpledges` (clan_id,type,leader_id,name) VALUES (?,?,?,?)");
				statement.setInt(1, getClanId());
				statement.setInt(2, sp.getType());
				statement.setInt(3, sp.getLeaderId());
				statement.setString(4, sp.getName());
				statement.execute();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Could not store clan Sub pledges: ", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
	}

	public int createSubPledge(final L2Player player, int pledgeType, final int leaderId, final String name)
	{
		final int temp = pledgeType;
		pledgeType = getAvailablePledgeTypes(pledgeType);

		if(pledgeType == SUBUNIT_NONE)
		{
			if(temp == SUBUNIT_ACADEMY)
				player.sendPacket(Msg.YOUR_CLAN_HAS_ALREADY_ESTABLISHED_A_CLAN_ACADEMY);
			else
				player.sendMessage("You can't create any more sub-units of this type");
			return SUBUNIT_NONE;
		}

		switch (pledgeType)
		{
			case SUBUNIT_ACADEMY:
				break;
			case SUBUNIT_ROYAL1:
				if(getReputationScore() < ROYAL1)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-ROYAL1, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
			case SUBUNIT_ROYAL2:
				if(getReputationScore() < ROYAL2)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-ROYAL2, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
			case SUBUNIT_KNIGHT1:
				if(getReputationScore() < KNIGHT1)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-KNIGHT1, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
			case SUBUNIT_KNIGHT2:
				if(getReputationScore() < KNIGHT2)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-KNIGHT2, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
			case SUBUNIT_KNIGHT3:
				if(getReputationScore() < KNIGHT3)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-KNIGHT3, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
			case SUBUNIT_KNIGHT4:
				if(getReputationScore() < KNIGHT4)
				{
					player.sendPacket(Msg.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW);
					return SUBUNIT_NONE;
				}
				incReputation(-KNIGHT4, Config.USE_REDUCE_REPSCORE_RATE, "SubunitCreate");
				break;
		}
		addSubPledge(new SubPledge(pledgeType, leaderId, name), true);
		return pledgeType;
	}

	public int getAvailablePledgeTypes(int pledgeType)
	{
		if(pledgeType == SUBUNIT_NONE)
			return SUBUNIT_NONE;

		if(_subPledges.get(pledgeType) != null)
			switch (pledgeType)
			{
				case SUBUNIT_ACADEMY:
					return 0;
				case SUBUNIT_ROYAL1:
					pledgeType = getAvailablePledgeTypes(SUBUNIT_ROYAL2);
					break;
				case SUBUNIT_ROYAL2:
					return 0;
				case SUBUNIT_KNIGHT1:
					pledgeType = getAvailablePledgeTypes(SUBUNIT_KNIGHT2);
					break;
				case SUBUNIT_KNIGHT2:
					pledgeType = getAvailablePledgeTypes(SUBUNIT_KNIGHT3);
					break;
				case SUBUNIT_KNIGHT3:
					pledgeType = getAvailablePledgeTypes(SUBUNIT_KNIGHT4);
					break;
				case SUBUNIT_KNIGHT4:
					return 0;
			}
		return pledgeType;
	}

	private void restoreSubPledges()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM clan_subpledges WHERE clan_id=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();

			// Go though the recordset of this SQL query
			while (rset.next())
			{
				final int type = rset.getInt("type");
				final int leaderId = rset.getInt("leader_id");
				final String name = rset.getString("name");
				final SubPledge pledge = new SubPledge(type, leaderId, name);
				addSubPledge(pledge, false);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore clan SubPledges: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * used to retrieve all subPledges
	 */
	public final SubPledge[] getAllSubPledges()
	{
		return _subPledges.values(new SubPledge[_subPledges.valueCollection().size()]);
	}

	public int getSubPledgeLimit(final int pledgeType)
	{
		int limit;
		switch (_level)
		{
			case 0:
				limit = Config.CLAN_MAX_USER_IN_CLAN_0;
				break;
			case 1:
				limit = Config.CLAN_MAX_USER_IN_CLAN_1;
				break;
			case 2:
				limit = Config.CLAN_MAX_USER_IN_CLAN_2;
				break;
			case 3:
				limit = Config.CLAN_MAX_USER_IN_CLAN_3;
				break;
			case 4:
				limit = Config.CLAN_MAX_USER_IN_CLAN_4;
				break;
			case 5:
				limit = Config.CLAN_MAX_USER_IN_CLAN_5;
				break;
			case 6:
				limit = Config.CLAN_MAX_USER_IN_CLAN_6;
				break;
			case 7:
				limit = Config.CLAN_MAX_USER_IN_CLAN_7;
				break;
			case 8:
				limit = Config.CLAN_MAX_USER_IN_CLAN_8;
				break;
			case 9:
				limit = Config.CLAN_MAX_USER_IN_CLAN_9;
				break;
			case 10:
				limit = Config.CLAN_MAX_USER_IN_CLAN_10;
				break;
			case 11:
				limit = Config.CLAN_MAX_USER_IN_CLAN_11;
				break;
			default:
			{
				limit = Config.CLAN_MAX_USER_IN_CLAN_11;
			}
		}
		switch (pledgeType)
		{
			case SUBUNIT_ACADEMY:
				limit = Config.CLAN_MAX_USER_IN_AKADEM;
				break;
			case SUBUNIT_ROYAL1:
				switch (getLevel())
				{
					case 5:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_5_LVL;
						break;
					case 6:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_6_LVL;
						break;
					case 7:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_7_LVL;
						break;
					case 8:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_8_LVL;
						break;
					case 9:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_9_LVL;
						break;
					case 10:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_10_LVL;
						break;
					case 11:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_11_LVL;
						break;
				}
				break;
			case SUBUNIT_ROYAL2:
				switch (getLevel())
				{
					case 6:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_6_LVL;
						break;
					case 7:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_7_LVL;
						break;
					case 8:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_8_LVL;
						break;
					case 9:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_9_LVL;
						break;
					case 10:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_10_LVL;
						break;
					case 11:
						limit = Config.CLAN_MAX_USER_IN_ROYAL_11_LVL;
						break;
				}
				break;
			case SUBUNIT_KNIGHT1:
			case SUBUNIT_KNIGHT2:
			case SUBUNIT_KNIGHT3:
			case SUBUNIT_KNIGHT4:
				switch (getLevel())
				{
					case 7:
						limit = Config.CLAN_MAX_USER_IN_KNIGHT_7_LVL;
						break;
					case 8:
						limit = Config.CLAN_MAX_USER_IN_KNIGHT_8_LVL;
						break;
					case 9:
						limit = Config.CLAN_MAX_USER_IN_KNIGHT_9_LVL;
						break;
					case 10:
						limit = Config.CLAN_MAX_USER_IN_KNIGHT_10_LVL;
						break;
					case 11:
						limit = Config.CLAN_MAX_USER_IN_KNIGHT_11_LVL;
						break;
				}
				break;
		}
		return limit;
	}

	public int getSubPledgeMembersCount(final int pledgeType)
	{
		int result = 0;
		for(final L2ClanMember temp : _members.values())
			if(temp.getPledgeType() == pledgeType)
				result++;
		return result;
	}

	public int getSubPledgeLeaderId(final int pledgeType)
	{
		return _subPledges.get(pledgeType).getLeaderId();
	}

	/* ============================ clan privilege ranks stuff ============================ */

	public class RankPrivs
	{
		private final int _rank;
		private int _party;
		private int _privs;

		public RankPrivs(final int rank, final int party, final int privs)
		{
			_rank = rank;
			_party = party;
			_privs = privs;
		}

		public int getRank()
		{
			return _rank;
		}

		public int getParty()
		{
			return _party;
		}

		public void setParty(final int party)
		{
			_party = party;
		}

		public int getPrivs()
		{
			return _privs;
		}

		public void setPrivs(final int privs)
		{
			_privs = privs;
		}
	}

	private void restoreRankPrivs()
	{
		if(_privs == null)
			initializePrivs();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			// Retrieve all skills of this L2Player from the database
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT privilleges,rank FROM clan_privs WHERE clan_id=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();

			// Go though the recordset of this SQL query
			while (rset.next())
			{
				final int rank = rset.getInt("rank");
				// int party = rset.getInt("party"); - unused?
				final int privileges = rset.getInt("privilleges");
				// noinspection ConstantConditions
				final RankPrivs p = _privs.get(rank);
				if(p != null)
					p.setPrivs(privileges);
				else
					_log.warning("Invalid rank value (" + rank + "), please check clan_privs table");
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Error restoring clan privs by rank.", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void initializePrivs()
	{
		for(int i = RANK_FIRST; i <= RANK_LAST; i++)
			_privs.put(i, new RankPrivs(i, 0, CP_NOTHING));
	}

	public void updatePrivsForRank(final int rank)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline() && member.getPlayer() != null && member.getPlayer().getPowerGrade() == rank)
			{
				if(member.getPlayer().isClanLeader())
					continue;
				member.getPlayer().sendUserInfo();
			}
	}

	public RankPrivs getRankPrivs(final int rank)
	{
		if(rank < RANK_FIRST || rank > RANK_LAST)
		{
			_log.log(Level.WARNING, "Requested invalid rank value: " + rank, new Exception("getRankPrivs"));
			return null;
		}

		if(_privs.get(rank) == null)
		{
			_log.log(Level.WARNING, "Request of rank before init: " + rank, new Exception("getRankPrivs"));
			setRankPrivs(rank, CP_NOTHING);
		}
		return _privs.get(rank);
	}

	public int getRankPrivsForPacket(final int rank)
	{
		final RankPrivs privs = _privs.get(rank);
		if(privs != null)
			return privs.getPrivs();
		else
			return CP_NOTHING;
	}

	public int countMembersByRank(final int rank)
	{
		int ret = 0;
		for(final L2ClanMember m : getMembers())
			if(m.getPowerGrade() == rank)
				ret++;
		return ret;
	}

	public void setRankPrivs(final int rank, final int privs)
	{
		if(rank < RANK_FIRST || rank > RANK_LAST)
		{
			_log.log(Level.WARNING, "Request set of invalid rank value: " + rank, new Exception("setRankPrivs"));
			return;
		}

		if(_privs.get(rank) != null)
			_privs.get(rank).setPrivs(privs);
		else
			_privs.put(rank, new RankPrivs(rank, countMembersByRank(rank), privs));

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			// _log.warning("requested store clan privs in db for rank: " + rank + ", privs: " + privs);
			// Retrieve all skills of this L2Player from the database
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO clan_privs (clan_id,rank,privilleges) VALUES (?,?,?)");
			statement.setInt(1, getClanId());
			statement.setInt(2, rank);
			statement.setInt(3, privs);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not store clan privs for rank: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * used to retrieve all privilege ranks
	 */
	public final RankPrivs[] getAllRankPrivs()
	{
		if(_privs == null)
			return new RankPrivs[0];
		return _privs.values(new RankPrivs[_privs.valueCollection().size()]);
	}

	private int _auctionBiddedAt = 0;

	public int getAuctionBiddedAt()
	{
		return _auctionBiddedAt;
	}

	public void setAuctionBiddedAt(final int id)
	{
		_auctionBiddedAt = id;
		// store changes to DB
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET auction_bid_at=? WHERE clan_id=?");
			statement.setInt(1, id);
			statement.setInt(2, getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not store auction for clan: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void sendMessageToAll(final String message)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline() && member.getPlayer() != null)
				member.getPlayer().sendMessage(message);
	}

	public void sendMessageToAll(final String message, final String message_ru)
	{
		for(final L2ClanMember member : _members.values())
			if(member.isOnline() && member.getPlayer() != null)
			{
				final L2Player player = member.getPlayer();
				if(player.getVar("lang@") == null || player.getVar("lang@").equalsIgnoreCase("en") || message_ru.equals(""))
					player.sendMessage(message);
				else
					player.sendMessage(message_ru);
			}
	}

	public void setSiege(final Siege siege)
	{
		_siege = siege;
	}

	public Siege getSiege()
	{
		return _siege;
	}

	public void setDefender(final boolean b)
	{
		_isDefender = b;
	}

	public void setAttacker(final boolean b)
	{
		_isAttacker = b;
	}

	public boolean isDefender()
	{
		return _isDefender;
	}

	public boolean isAttacker()
	{
		return _isAttacker;
	}

	public void setTerritorySiege(final int side)
	{
		_territorySide = side;
	}

	public int getTerritorySiege()
	{
		return _territorySide;
	}

	private static class ClanReputationComparator implements Comparator<L2Clan>
	{
		@Override
		public int compare(final L2Clan o1, final L2Clan o2)
		{
			if(o1 == null || o2 == null)
				return 0;
			return o2.getReputationScore() - o1.getReputationScore();
		}
	}

	public void insertNotice()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO clan_notices (clanID, notice, enabled) values (?,?,?)");
			statement.setInt(1, getClanId());
			statement.setString(2, "Change me");
			statement.setString(3, "false");
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while creating notice for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public String getNotice()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT notice FROM clan_notices WHERE clanID=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();
			while (rset.next())
				_notice = rset.getString("notice");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while getting notice from DB for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return _notice;
	}

	public String getNoticeForBBS()
	{
		String notice = "";
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT notice FROM clan_notices WHERE clanID=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();
			while (rset.next())
				notice = rset.getString("notice");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while getting notice from DB for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return Strings.edtiSavedTxT(notice);
	}

	/**
	 * Назначить новое сообщение
	 */
	public void setNotice(String notice)
	{
		notice = Strings.edtiPlayerTxT(notice);
		if(notice.length() > MAX_NOTICE_LENGTH)
			notice = notice.substring(0, MAX_NOTICE_LENGTH - 1);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_notices SET notice=? WHERE clanID=?");
			statement.setString(1, notice);
			statement.setInt(2, getClanId());
			statement.execute();
			_notice = notice;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while saving notice for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean isNoticeEnabled()
	{
		String result = "";
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT enabled FROM clan_notices WHERE clanID=?");
			statement.setInt(1, getClanId());
			rset = statement.executeQuery();

			while (rset.next())
				result = rset.getString("enabled");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while reading notice for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		if(result.isEmpty())
			insertNotice();
		else if(result.compareToIgnoreCase("true") == 0)
			return true;
		return false;
	}

	/**
	 * Включить/выключить
	 */
	public void setNoticeEnabled(final boolean noticeEnabled)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_notices SET enabled=? WHERE clanID=?");
			if(noticeEnabled)
				statement.setString(1, "true");
			else
				statement.setString(1, "false");
			statement.setInt(2, getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2Clan: error while updating notice for clan " + getClanId(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		_noticeEnabled = noticeEnabled;
	}

	public int getWhBonus()
	{
		return _whBonus;
	}

	public void setWhBonus(final int i)
	{
		if(_whBonus != -1)
			mysql.set("UPDATE `clan_data` SET `warehouse` = '" + i + "' WHERE `clan_id`=" + getClanId());
		_whBonus = i;
	}

	/**
	 * @return Возвратит врата клансуммона, или null, если нет врат
	 */
	public L2ClanGate getClanGate()
	{
		return _clanGate;
	}

	/**
	 * истанавливает врата клансуммона клану
	 * 
	 * @param clanGate
	 *            - инстанс врат для установки
	 */
	public void setClanGate(final L2ClanGate clanGate)
	{
		_clanGate = clanGate;
		if(_clanGate != null)
			for(final L2ClanMember temp : _members.values())
				if(temp.isOnline())
				{
					final L2Player tempPlayer = temp.getPlayer();
					tempPlayer.sendMessage(new CustomMessage("l2n.game.model.L2Clan.L2ClanGate.Activated", tempPlayer));
				}
	}

	public void setAirshipLicense(final boolean val)
	{
		_airshipLicense = val;
	}

	public boolean isHaveAirshipLicense()
	{
		return _airshipLicense;
	}

	public L2AirShip getAirship()
	{
		return _airship;
	}

	public void setAirship(final L2AirShip airship)
	{
		_airship = airship;
	}

	public int getAirshipFuel()
	{
		return _airshipFuel;
	}

	public void setAirshipFuel(final int fuel)
	{
		_airshipFuel = fuel;
	}
}
