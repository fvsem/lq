package l2n.game.model;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastList;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.TownManager;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AutoSpawnHandler
{
	protected static Logger _log = Logger.getLogger(AutoSpawnHandler.class.getName());
	private static AutoSpawnHandler _instance;

	private static final int DEFAULT_INITIAL_SPAWN = 30000;
	private static final int DEFAULT_RESPAWN = 3600000;
	private static final int DEFAULT_DESPAWN = 3600000;

	protected TIntObjectHashMap<AutoSpawnInstance> _registeredSpawns;
	protected TIntObjectHashMap<ScheduledFuture<AutoSpawner>> _runningSpawns;
	protected boolean _activeState = true;

	public AutoSpawnHandler()
	{
		_registeredSpawns = new TIntObjectHashMap<AutoSpawnInstance>();
		_runningSpawns = new TIntObjectHashMap<ScheduledFuture<AutoSpawner>>();

		restoreSpawnData();
	}

	public static AutoSpawnHandler getInstance()
	{
		if(_instance == null)
			_instance = new AutoSpawnHandler();
		return _instance;
	}

	public final int size()
	{
		return _registeredSpawns.size();
	}

	private void restoreSpawnData()
	{
		int numLoaded = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		FiltredPreparedStatement statement2 = null;
		ResultSet rset = null, rset2 = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM random_spawn ORDER BY groupId ASC");
			statement2 = con.prepareStatement("SELECT * FROM random_spawn_loc WHERE groupId=?");

			rset = statement.executeQuery();
			while (rset.next())
			{
				AutoSpawnInstance spawnInst = registerSpawn(rset.getInt("npcId"), rset.getInt("initialDelay"), rset.getInt("respawnDelay"), rset.getInt("despawnDelay"));
				spawnInst.setSpawnCount(rset.getByte("count"));
				spawnInst.setBroadcast(rset.getBoolean("broadcastSpawn"));
				spawnInst.setRandomSpawn(rset.getBoolean("randomSpawn"));
				numLoaded++;


				statement2.setInt(1, rset.getInt("groupId"));
				rset2 = statement2.executeQuery();
				while (rset2.next())

					spawnInst.addSpawnLocation(rset2.getInt("x"), rset2.getInt("y"), rset2.getInt("z"), rset2.getInt("heading"));
				DbUtils.close(rset2);
			}

			if(Config.DEBUG)
				_log.config("AutoSpawnHandler: Loaded " + numLoaded + " spawn group(s) from the database.");
		}
		catch(Exception e)
		{
			_log.warning("AutoSpawnHandler: Could not restore spawn data: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(statement2, rset2);
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public AutoSpawnInstance registerSpawn(int npcId, int[][] spawnPoints, int initialDelay, int respawnDelay, int despawnDelay)
	{
		if(initialDelay < 0)
			initialDelay = DEFAULT_INITIAL_SPAWN;

		if(respawnDelay < 0)
			respawnDelay = DEFAULT_RESPAWN;

		if(despawnDelay < 0)
			despawnDelay = DEFAULT_DESPAWN;

		AutoSpawnInstance newSpawn = new AutoSpawnInstance(npcId, initialDelay, respawnDelay, despawnDelay);

		if(spawnPoints != null)
			for(int[] spawnPoint : spawnPoints)
				newSpawn.addSpawnLocation(spawnPoint);

		int newId = IdFactory.getInstance().getNextId();
		newSpawn._objectId = newId;
		_registeredSpawns.put(newId, newSpawn);

		setSpawnActive(newSpawn, true);

		if(Config.DEBUG)
			_log.config("AutoSpawnHandler: Registered auto spawn for NPC ID " + npcId + " (Object ID = " + newId + ").");

		return newSpawn;
	}

	public AutoSpawnInstance registerSpawn(int npcId, int initialDelay, int respawnDelay, int despawnDelay)
	{
		return registerSpawn(npcId, null, initialDelay, respawnDelay, despawnDelay);
	}

	public boolean removeSpawn(AutoSpawnInstance spawnInst)
	{
		if(!isSpawnRegistered(spawnInst))
			return false;

		try
		{
			_registeredSpawns.remove(spawnInst.getNpcId());

			ScheduledFuture<AutoSpawner> respawnTask = _runningSpawns.remove(spawnInst._objectId);
			respawnTask.cancel(false);

			if(Config.DEBUG)
				_log.config("AutoSpawnHandler: Removed auto spawn for NPC ID " + spawnInst._npcId + " (Object ID = " + spawnInst._objectId + ").");
		}
		catch(Exception e)
		{
			_log.warning("AutoSpawnHandler: Could not auto spawn for NPC ID " + spawnInst._npcId + " (Object ID = " + spawnInst._objectId + "): " + e);
			return false;
		}

		return true;
	}

	public void removeSpawn(int objectId)
	{
		removeSpawn(_registeredSpawns.get(objectId));
	}

	public void setSpawnActive(AutoSpawnInstance spawnInst, boolean isActive)
	{
		int objectId = spawnInst._objectId;

		if(isSpawnRegistered(objectId))
		{
			ScheduledFuture<AutoSpawner> spawnTask = null;
			if(isActive)
			{
				AutoSpawner rset = new AutoSpawner(objectId);
				if(spawnInst._desDelay > 0)
					spawnTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(rset, spawnInst._initDelay, spawnInst._resDelay);
				else
					spawnTask = L2GameThreadPools.getInstance().scheduleGeneral(rset, spawnInst._initDelay);
				// spawnTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(rset, spawnInst._initDelay, spawnInst._resDelay);
				_runningSpawns.put(objectId, spawnTask);
			}
			else
			{
				spawnTask = _runningSpawns.remove(objectId);

				if(spawnTask != null)
					spawnTask.cancel(false);
			}

			spawnInst.setSpawnActive(isActive);
		}
	}

	public final long getTimeToNextSpawn(AutoSpawnInstance spawnInst)
	{
		int objectId = spawnInst._objectId;

		if(!isSpawnRegistered(objectId))
			return -1;

		return _runningSpawns.get(objectId).getDelay(TimeUnit.MILLISECONDS);
	}

	public final AutoSpawnInstance getAutoSpawnInstance(int id, boolean isObjectId)
	{
		if(isObjectId)
		{
			if(isSpawnRegistered(id))
				return _registeredSpawns.get(id);
		}
		else
			for(AutoSpawnInstance spawnInst : _registeredSpawns.valueCollection())
				if(spawnInst._npcId == id)
					return spawnInst;

		return null;
	}

	public Map<Integer, AutoSpawnInstance> getAllAutoSpawnInstance(int id)
	{
		HashMap<Integer, AutoSpawnInstance> spawnInstList = new HashMap<Integer, AutoSpawnInstance>();
		for(AutoSpawnInstance spawnInst : _registeredSpawns.valueCollection())
			if(spawnInst._npcId == id)
				spawnInstList.put(spawnInst._objectId, spawnInst);

		return spawnInstList;
	}

	public final boolean isSpawnRegistered(int objectId)
	{
		return _registeredSpawns.containsKey(objectId);
	}

	public final boolean isSpawnRegistered(AutoSpawnInstance spawnInst)
	{
		return _registeredSpawns.containsValue(spawnInst);
	}

	private class AutoSpawner implements Runnable
	{
		private int _objectId;

		AutoSpawner(int objectId)
		{
			_objectId = objectId;
		}

		@Override
		public void run()
		{
			try
			{
				AutoSpawnInstance spawnInst = _registeredSpawns.get(_objectId);

				if(!spawnInst.isSpawnActive() || Config.DONTLOADSPAWN)
					return;

				Location[] locationList = spawnInst.getLocationList();

				if(locationList.length == 0)
				{
					_log.info("AutoSpawnHandler: No location co-ords specified for spawn instance (Object ID = " + _objectId + ").");
					return;
				}

				int locationCount = locationList.length;
				int locationIndex = Rnd.get(locationCount);

				if(!spawnInst.isRandomSpawn())
				{
					locationIndex = spawnInst._lastLocIndex;
					locationIndex++;

					if(locationIndex == locationCount)
						locationIndex = 0;

					spawnInst._lastLocIndex = locationIndex;
				}

				final int x = locationList[locationIndex].x;
				final int y = locationList[locationIndex].y;
				final int z = locationList[locationIndex].z;
				final int heading = locationList[locationIndex].h;

				L2NpcTemplate npcTemp = NpcTable.getTemplate(spawnInst.getNpcId());
				L2Spawn newSpawn = new L2Spawn(npcTemp);

				newSpawn.setLocx(x);
				newSpawn.setLocy(y);
				newSpawn.setLocz(z);
				if(heading != -1)
					newSpawn.setHeading(heading);
				newSpawn.setAmount(spawnInst.getSpawnCount());
				if(spawnInst._desDelay == 0)
					newSpawn.setRespawnDelay(spawnInst._resDelay);

				L2NpcInstance npcInst = null;

				for(int i = 0; i < spawnInst._spawnCount; i++)
				{
					npcInst = newSpawn.doSpawn(true);

					npcInst.setXYZ(npcInst.getX() + Rnd.get(50), npcInst.getY() + Rnd.get(50), npcInst.getZ());

					spawnInst.addAttackable(npcInst);
				}

				String nearestTown = TownManager.getInstance().getClosestTownName(npcInst);


				if(spawnInst.isBroadcasting() && npcInst != null)
					Announcements.announceToAll("" + npcInst.getName() + " появился в районе  " + nearestTown + "!");

				if(Config.DEBUG)
					_log.info("AutoSpawnHandler: Spawned NPC ID " + spawnInst.getNpcId() + " at " + x + ", " + y + ", " + z + " (Near " + nearestTown + ") for " + spawnInst.getRespawnDelay() / 1000 / 60 + " minutes(s).");

				if(spawnInst.getDespawnDelay() > 0)
				{
					AutoDespawner rd = new AutoDespawner(_objectId);
					L2GameThreadPools.getInstance().scheduleGeneral(rd, spawnInst.getDespawnDelay() - 1000);
				}
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "AutoSpawnHandler: An error occurred while initializing spawn instance (Object ID = " + _objectId + "): ", e);
			}
		}
	}

	private class AutoDespawner implements Runnable
	{
		private int _objectId;

		AutoDespawner(int objectId)
		{
			_objectId = objectId;
		}

		@Override
		public void run()
		{
			try
			{
				AutoSpawnInstance spawnInst = _registeredSpawns.get(_objectId);

				for(L2NpcInstance npcInst : spawnInst.getAttackableList())
				{
					npcInst.deleteMe();
					spawnInst.removeAttackable(npcInst);

					if(Config.DEBUG)
						_log.info("AutoSpawnHandler: Spawns removed for spawn instance (Object ID = " + _objectId + ").");
				}
			}
			catch(Exception e)
			{
				_log.warning("AutoSpawnHandler: An error occurred while despawning spawn (Object ID = " + _objectId + "): " + e);
			}
		}
	}

	public class AutoSpawnInstance
	{
		protected int _objectId;
		protected int _spawnIndex;

		protected int _npcId;
		protected int _initDelay;
		protected int _resDelay;
		protected int _desDelay;
		protected byte _spawnCount = 1;
		protected int _lastLocIndex = -1;

		private GArray<L2NpcInstance> _npcList = new GArray<L2NpcInstance>();
		private GArray<Location> _locList = new GArray<Location>();

		private boolean _spawnActive;
		private boolean _randomSpawn = false;
		private boolean _broadcastAnnouncement = false;

		protected AutoSpawnInstance(int npcId, int initDelay, int respawnDelay, int despawnDelay)
		{
			_npcId = npcId;
			_initDelay = initDelay;
			_resDelay = respawnDelay;
			_desDelay = despawnDelay;
		}

		void setSpawnActive(boolean activeValue)
		{
			_spawnActive = activeValue;
		}

		boolean addAttackable(L2NpcInstance npcInst)
		{
			return _npcList.add(npcInst);
		}

		boolean removeAttackable(L2NpcInstance npcInst)
		{
			return _npcList.remove(npcInst);
		}

		public int getObjectId()
		{
			return _objectId;
		}

		public int getInitialDelay()
		{
			return _initDelay;
		}

		public int getRespawnDelay()
		{
			return _resDelay;
		}

		public int getDespawnDelay()
		{
			return _desDelay;
		}

		public int getNpcId()
		{
			return _npcId;
		}

		public int getSpawnCount()
		{
			return _spawnCount;
		}

		public Location[] getLocationList()
		{
			return _locList.toArray(new Location[_locList.size()]);
		}

		public L2NpcInstance[] getAttackableList()
		{
			return _npcList.toArray(new L2NpcInstance[_npcList.size()]);
		}

		public L2Spawn[] getSpawns()
		{
			List<L2Spawn> npcSpawns = new FastList<L2Spawn>();

			for(L2NpcInstance npcInst : _npcList)
				npcSpawns.add(npcInst.getSpawn());

			return npcSpawns.toArray(new L2Spawn[npcSpawns.size()]);
		}

		public void setSpawnCount(byte spawnCount)
		{
			_spawnCount = spawnCount;
		}

		public void setRandomSpawn(boolean randValue)
		{
			_randomSpawn = randValue;
		}

		public void setBroadcast(boolean broadcastValue)
		{
			_broadcastAnnouncement = broadcastValue;
		}

		public boolean isSpawnActive()
		{
			return _spawnActive;
		}

		public boolean isRandomSpawn()
		{
			return _randomSpawn;
		}

		public boolean isBroadcasting()
		{
			return _broadcastAnnouncement;
		}

		public boolean addSpawnLocation(int x, int y, int z, int heading)
		{
			return _locList.add(new Location(x, y, z, heading));
		}

		public boolean addSpawnLocation(int[] spawnLoc)
		{
			if(spawnLoc.length != 3)
				return false;

			return addSpawnLocation(spawnLoc[0], spawnLoc[1], spawnLoc[2], -1);
		}

		public Location removeSpawnLocation(int locIndex)
		{
			try
			{
				return _locList.remove(locIndex);
			}
			catch(IndexOutOfBoundsException e)
			{
				return null;
			}
		}
	}
}
