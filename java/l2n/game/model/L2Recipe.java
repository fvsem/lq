package l2n.game.model;

public class L2Recipe
{
	private int _itemId;

	private int _quantity;

	public L2Recipe(int itemId, int quantity)
	{
		_itemId = itemId;
		_quantity = quantity;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public int getQuantity()
	{
		return _quantity;
	}
}
