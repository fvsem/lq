package l2n.game.model;

import l2n.commons.util.StatsSet;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

public class L2RecipeList
{
	private L2Recipe[] _recipes;

	private int _id;

	private int _level;

	private int _recipeId;

	private String _recipeName;

	private int _successRate;

	private int _mpCost;

	private int _itemId;

	private int _count;

	private int _rareItemId;

	private int _rareCount;

	private int _rarity;

	private boolean _isDwarvencraft;

	public L2RecipeList(StatsSet set, boolean haveRare)
	{
		_recipes = new L2Recipe[0];
		_id = set.getInteger("id");
		_level = set.getInteger("craftLevel");
		_recipeId = set.getInteger("recipeId");
		_recipeName = set.getString("recipeName");
		_successRate = set.getInteger("successRate");
		_itemId = set.getInteger("itemId");
		_count = set.getInteger("count");
		_mpCost = set.getInteger("mpCost");
		if(haveRare)
		{
			_rareItemId = set.getInteger("rareItemId");
			_rareCount = set.getInteger("rareCount");
			_rarity = set.getInteger("rarity");
		}
		_isDwarvencraft = set.getBool("isDwarvenRecipe");
	}

	public void addRecipe(L2Recipe recipe)
	{
		int len = _recipes.length;
		L2Recipe[] tmp = new L2Recipe[len + 1];
		System.arraycopy(_recipes, 0, tmp, 0, len);
		tmp[len] = recipe;
		_recipes = tmp;
	}

	public int getId()
	{
		return _id;
	}

	public int getLevel()
	{
		return _level;
	}

	public int getRecipeId()
	{
		return _recipeId;
	}

	public String getRecipeName()
	{
		return _recipeName;
	}

	public int getSuccessRate()
	{
		return _successRate;
	}

	public int getMpCost()
	{
		return _mpCost;
	}

	public boolean isCriticalAffected()
	{
		L2Item item = ItemTable.getInstance().getTemplate(_itemId);
		if(item == null)
			return false;

		return item.getCrystalType().cry < L2Item.CRYSTAL_B;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public int getCount()
	{
		return _count;
	}

	public L2Recipe[] getRecipes()
	{
		return _recipes;
	}

	public int getRareItemId()
	{
		return _rareItemId;
	}

	public int getRareCount()
	{
		return _rareCount;
	}

	public int getRarity()
	{
		return _rarity;
	}

	public boolean isDwarvenRecipe()
	{
		return _isDwarvencraft;
	}
}
