package l2n.game.model;

import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2TerritoryFlagInstance;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 
 * @date 16.08.2010
 * @time 3:54:19
 */
public class L2ObjectTasks
{
	private static final Logger _log = Logger.getLogger(L2ObjectTasks.class.getName());

	public static class DeleteTask implements Runnable
	{
		private final IHardReference<? extends L2Character> _ref;

		public DeleteTask(final L2Character c)
		{
			_ref = c.getRef();
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character == null)
				return;
			character.deleteMe();
		}
	}

	public static class CastEndTimeTask implements Runnable
	{
		private final IHardReference<? extends L2Character> _ref;

		public CastEndTimeTask(final L2Character character)
		{
			_ref = character.getRef();
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character == null)
				return;
			character.onCastEndTime();
		}
	}

	/** Task lauching the function onMagicUseTimer() */
	public static class MagicUseTask implements Runnable
	{
		private final boolean _forceUse;
		private final IHardReference<? extends L2Character> _ref;

		public MagicUseTask(final L2Character cha, final boolean forceUse)
		{
			_ref = cha.getRef();
			_forceUse = forceUse;
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character == null)
				return;

			if((character.isPet() || character.isSummon()) && character.getPlayer() == null)
			{
				character.clearCastVars();
				return;
			}

			if(character._scheduledCastCount > 0)
				character._scheduledCastCount--;

			character.onMagicUseTimer(character.getCastingTarget(), character.getCastingSkill(), _forceUse);
		}
	}

	public static class MagicLaunchedTask implements Runnable
	{
		private final boolean _forceUse;
		private final IHardReference<? extends L2Character> _ref;

		public MagicLaunchedTask(final L2Character cha, final boolean forceUse)
		{
			_ref = cha.getRef();
			_forceUse = forceUse;
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character == null)
				return;

			final L2Skill castingSkill = character.getCastingSkill();
			if(castingSkill == null || (character.isPet() || character.isSummon()) && character.getPlayer() == null)
			{
				character.clearCastVars();
				return;
			}

			L2Character[] targets = castingSkill.getTargets(character, character.getCastingTarget(), _forceUse);

			// затычка для фьюжен скилов, удаляем себя из целей
			if(castingSkill.oneTarget() && castingSkill.getSymbolId() > 0)
				targets = ArrayUtil.arrayRemoveFirst(targets, character);

			character.broadcastPacket(new MagicSkillLaunched(character.getObjectId(), castingSkill.getDisplayId(), castingSkill.getLevel(), castingSkill.isOffensive(), targets));
		}
	}

	/** Task of AI notification */
	public static class NotifyAITask implements Runnable
	{
		private final CtrlEvent _evt;
		private final Object _agr0;
		private final Object _agr1;
		private final IHardReference<? extends L2Character> _ref;

		public NotifyAITask(final L2Character cha, final CtrlEvent evt, final Object agr0, final Object agr1)
		{
			_ref = cha.getRef();
			_evt = evt;
			_agr0 = agr0;
			_agr1 = agr1;
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character == null || (character.isPet() || character.isSummon()) && character.getPlayer() == null)
				return;
			try
			{
				character.getAI().notifyEvent(_evt, _agr0, _agr1);
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "", t);
			}
		}
	}

	public static class NotifyReadyToAct implements Runnable
	{
		private final IHardReference<? extends L2Character> _ref;

		public NotifyReadyToAct(final L2Character cha)
		{
			_ref = cha.getRef();
		}

		@Override
		public void run()
		{
			final L2Character character = _ref.get();
			if(character != null)
				try
				{
					character.setAttackEndTime(0);
					character.getAI().notifyEvent(CtrlEvent.EVT_READY_TO_ACT);
				}
				catch(final Throwable t)
				{
					_log.log(Level.WARNING, "", t);
				}
		}
	}

	public static class HitTask implements Runnable
	{
		private final int _damage;
		private final boolean _crit, _miss, _shld, _soulshot, _unchargeSS, _notify;

		private final IHardReference<? extends L2Character> _chaRef;
		private final IHardReference<? extends L2Character> _targetRef;

		public HitTask(final L2Character cha, final L2Character target, final int damage, final boolean crit, final boolean miss, final boolean soulshot, final boolean shld, final boolean unchargeSS, final boolean notify)
		{
			_chaRef = cha.getRef();
			_targetRef = target.getRef();
			_damage = damage;
			_crit = crit;
			_shld = shld;
			_miss = miss;
			_soulshot = soulshot;
			_unchargeSS = unchargeSS;
			_notify = notify;
		}

		@Override
		public void run()
		{
			L2Character character, target;
			if((character = _chaRef.get()) == null || (target = _targetRef.get()) == null)
				return;

			try
			{
				if(character.isAttackAborted())
				{
					character.setAttackEndTime(0);
					return;
				}

				character.onHitTimer(target, _damage, _crit, _miss, _soulshot, _shld, _unchargeSS);
				if(_notify)
				{
					character.setAttackEndTime(0);
					character.getAI().notifyEvent(CtrlEvent.EVT_READY_TO_ACT);
				}
			}
			catch(final Throwable e)
			{
				_log.log(Level.WARNING, "", e);
			}
		}
	}

	public static class AltMagicUseTask implements Runnable
	{
		private final L2Skill _skill;
		private final IHardReference<? extends L2Character> _chaRef;
		private final IHardReference<? extends L2Character> _targetRef;

		public AltMagicUseTask(final L2Character character, final L2Character target, final L2Skill skill)
		{
			_chaRef = character.getRef();
			_targetRef = target.getRef();
			_skill = skill;
		}

		@Override
		public void run()
		{
			L2Character character, target;
			if((character = _chaRef.get()) == null || (target = _targetRef.get()) == null)
				return;

			character.altOnMagicUseTimer(target, _skill);
		}
	}

	/** CancelAttackStanceTask */
	public static class CancelAttackStanceTask implements Runnable
	{
		private final IHardReference<? extends L2Character> _chaRef;

		public CancelAttackStanceTask(final L2Character character)
		{
			_chaRef = character.getRef();
		}

		@Override
		public void run()
		{
			final L2Character character = _chaRef.get();
			if(character == null)
				return;

			character.stopAttackStanceTask();
		}
	}

	public static class EmptyMoveNextTask extends MoveNextTask
	{
		public EmptyMoveNextTask(final L2Character _character)
		{
			super(null);
		}

		@Override
		public void run()
		{}

		@Override
		public void updateStoreId(final L2Character cha)
		{}

		@Override
		public MoveNextTask setDist(final double dist)
		{
			return this;
		}

		@Override
		public final L2Character getActor()
		{
			return null;
		}

		@Override
		public boolean isEmpty()
		{
			return true;
		}
	}

	public static class MoveNextTask implements Runnable
	{
		private float alldist, donedist;
		private IHardReference<? extends L2Character> _chaRef;

		public MoveNextTask(final L2Character character)
		{
			_chaRef = character == null ? HardReferences.<L2Character> emptyRef() : character.getRef();
		}

		public void updateStoreId(final L2Character cha)
		{
			_chaRef = cha.getRef();
		}

		public MoveNextTask setDist(final double dist)
		{
			alldist = (float) dist;
			donedist = 0;
			return this;
		}

		public L2Character getActor()
		{
			return _chaRef.get();
		}

		public boolean isEmpty()
		{
			return false;
		}

		@Override
		public void run()
		{
			final L2Character character = _chaRef.get();
			if(character == null || !character.isMoving)
				return;

			try
			{
				synchronized (character._targetRecorder)
				{
					if(!character.isMoving)
						return;

					if(character.isMovementDisabled())
					{
						character.stopMove();
						return;
					}

					L2Character follow_target = null;
					final int speed = character.getMoveSpeed();
					if(speed <= 0)
					{
						character.stopMove();
						return;
					}
					final long now = System.currentTimeMillis();

					if(character.isFollow)
					{
						follow_target = character.getFollowTarget();
						if(follow_target == null)
						{
							character.stopMove();
							return;
						}
						if(character.isInRangeZ(follow_target, character.getOffset()) && GeoEngine.canSeeTarget(character, follow_target, false))
						{
							character.stopMove();
							L2GameThreadPools.getInstance().executeAi(new NotifyAITask(character, CtrlEvent.EVT_ARRIVED_TARGET, null, null), character.isPlayable());
							return;
						}
					}

					if(alldist <= 0)
					{
						character.moveNext(false);
						return;
					}

					donedist += (now - character.getStartMoveTime()) * character.getPreviousSpeed() / 1000.;
					double done = donedist / alldist;

					if(done < 0)
						done = 0;
					if(done >= 1)
					{
						character.moveNext(false);
						return;
					}

					if(character.isMovementDisabled())
					{
						character.stopMove();
						return;
					}

					Location loc = null;

					int index = (int) (character.moveList.size() * done);
					if(index >= character.moveList.size())
						index = character.moveList.size() - 1;
					if(index < 0)
						index = 0;

					loc = character.moveList.get(index).clone().geo2world();

					if(!character.isFlying() && !character.isInVehicle() && !character.isInWater() && !character.isVehicle())
						if(loc.z - character.getZ() > 256)
						{
							final String bug_text = "geo bug 1 at: " + character.getLoc() + " => " + loc.x + "," + loc.y + "," + loc.z + "\tAll path: " + character.moveList.get(0) + " => " + character.moveList.get(character.moveList.size() - 1);
							Log.add(bug_text, "geo");
							if(character.isPlayer() && character.getAccessLevel() >= 100)
								character.sendMessage(bug_text);
							character.stopMove();
							return;
						}

					// Проверяем, на всякий случай
					if(loc == null || character.isMovementDisabled())
					{
						character.stopMove();
						return;
					}

					character.setLoc(loc, true);

					// В процессе изменения координат, мы остановились
					if(character.isMovementDisabled())
					{
						character.stopMove();
						return;
					}

					if(character.isFollow && now - character.getFollowTimestamp() > (character.isForestalling() ? 500 : 1000) && follow_target != null && !follow_target.isInRange(character.getMoveDestPos(), Math.max(100, character.getOffset())))
					{
						if(Math.abs(character.getZ() - loc.z) > 1000 && !character.isFlying())
						{
							character.sendPacket(Msg.CANNOT_SEE_TARGET);
							character.stopMove();
							return;
						}
						if(character.buildPathTo(follow_target.getX(), follow_target.getY(), follow_target.getZ(), character.getOffset(), follow_target, true, true))
							character.getMoveDestPos().set(follow_target.getX(), follow_target.getY(), follow_target.getZ());
						else
						{
							character.stopMove();
							return;
						}
						character.moveNext(true);
						return;
					}

					character.setPreviousSpeed(speed);
					character.setStartMoveTime(now);
					character.runMoveTask();
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
		}
	}

	public static class EndStandUpTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public EndStandUpTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;

			player.sittingTaskLaunched = false;
			player.setSitting(false);
			if(!player.getAI().setNextIntention())
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		}
	}

	public static class EndSitDownTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public EndSitDownTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;

			player.sittingTaskLaunched = false;
			player.getAI().clearNextAction();
		}
	}

	public static class NotifyFaction implements Runnable
	{
		private final IHardReference<? extends L2NpcInstance> _npcRef;
		private final IHardReference<? extends L2Character> _attackerRef;
		private final int _damage;

		public NotifyFaction(final L2NpcInstance npc, final L2Character attacker, final int damage)
		{
			_npcRef = npc.getRef();
			_attackerRef = attacker.getRef();
			_damage = damage;
		}

		@Override
		public void run()
		{
			L2NpcInstance npc;
			L2Character attacker;
			if((npc = _npcRef.get()) == null || (attacker = _attackerRef.get()) == null)
				return;
			try
			{
				final String faction_id = npc.getFactionId();
				if(faction_id == null || faction_id.isEmpty())
					return;

				for(final L2NpcInstance friend : npc.getActiveFriends(false))
					if(friend != null && faction_id.equalsIgnoreCase(friend.getFactionId()))
						friend.onClanAttacked(npc, attacker, _damage);
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "", t);
			}
		}
	}

	public static class ReturnTerritoryFlagTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public ReturnTerritoryFlagTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;

			if(player.isTerritoryFlagEquipped())
			{
				final L2ItemInstance flag = player.getActiveWeaponInstance();
				if(flag != null && flag.getCustomType1() != 77) // 77 это эвентовый флаг
				{
					final L2TerritoryFlagInstance flagNpc = TerritorySiege.getNpcFlagByItemId(flag.getItemId());
					flagNpc.returnToCastle(player);
				}
			}
		}
	}

	/** PvPFlagTask */
	public static class PvPFlagTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public PvPFlagTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;
			try
			{
				final long diff = Math.abs(System.currentTimeMillis() - player.getlastPvpAttack());
				if(diff > Config.PVP_TIME)
					player.stopPvPFlag();
				else if(diff > Config.PVP_TIME - 20000)
					player.updatePvPFlag(2);
				else
					player.updatePvPFlag(1);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "error in pvp flag task:", e);
			}
		}
	}

	public static class SoulConsumeTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public SoulConsumeTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player != null)
				player.setConsumedSouls(player.getConsumedSouls() + 1, null);
		}
	}

	public static class TeleportTask implements Runnable
	{
		private final IHardReference<? extends L2Character> _charRef;
		private final Location _loc;
		private final long _reflection;

		public TeleportTask(final L2Character cha, final Location p, final long reflection)
		{
			_charRef = cha.getRef();
			_loc = p;
			_reflection = reflection;
		}

		@Override
		public void run()
		{
			final L2Character cha = _charRef.get();
			if(cha == null)
				return;
			cha.teleToLocation(_loc, _reflection);
		}
	}

	public static class UnJailTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;
		private final Location loc;

		public UnJailTask(final L2Player player, final Location p)
		{
			_playerRef = player.getRef();
			loc = p;
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;
			player.unsetVar("jailedFrom");
			player.unsetVar("jailed");
			player._isInJail = false;
			player.teleToLocation(loc, 0);
		}
	}

	/** WaterTask */
	public static class WaterTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;

		public WaterTask(final L2Player player)
		{
			_playerRef = player.getRef();
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;
			if(player.isDead() || !player.isInZone(ZoneType.water))
			{
				player.stopWaterTask();
				return;
			}

			final double reduceHp = player.getMaxHp() < 100 ? 1 : player.getMaxHp() / 100;
			player.reduceCurrentHp(reduceHp, player, null, false, false, true, false, false);
			player.sendPacket(new SystemMessage(SystemMessage.YOU_RECEIVED_S1_DAMAGE_BECAUSE_YOU_WERE_UNABLE_TO_BREATHE).addNumber((long) reduceHp));
		}
	}

	public static class BanTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;
		private final String _reason;

		public BanTask(final L2Player player, final String reason)
		{
			_playerRef = player.getRef();
			_reason = reason;
		}

		@Override
		public void run()
		{
			// Player logged out. Lucky Ones.
			final L2Player player = _playerRef.get();
			if(player == null)
				return;

			LSConnection.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), -100, _reason, -1));
			player.logout(false, false, true, true);
		}
	}

	public static class LookingForFishTask implements Runnable
	{
		private final IHardReference<? extends L2Player> _playerRef;
		private final boolean _isNoob;
		private final boolean _isUpperGrade;
		private final int _fishType;
		private final int _fishGutsCheck;
		private final long _endTaskTime;

		public LookingForFishTask(final L2Player player, final int fishWaitTime, final int fishGutsCheck, final int fishType, final boolean isNoob, final boolean isUpperGrade)
		{
			_playerRef = player.getRef();
			_fishGutsCheck = fishGutsCheck;
			_endTaskTime = System.currentTimeMillis() + fishWaitTime + 10000;
			_fishType = fishType;
			_isNoob = isNoob;
			_isUpperGrade = isUpperGrade;
		}

		@Override
		public void run()
		{
			final L2Player player = _playerRef.get();
			if(player == null)
				return;

			if(System.currentTimeMillis() >= _endTaskTime)
			{
				player.endFishing(false);
				return;
			}
			if(_fishType == -1)
				return;
			final int check = Rnd.get(1000);
			if(_fishGutsCheck > check)
			{
				player.stopLookingForFishTask();
				player.startFishCombat(_isNoob, _isUpperGrade);
			}
		}
	}
}
