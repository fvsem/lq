package l2n.game.model.items;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ItemList;

import java.util.logging.Logger;

public class ClanWarehousePool
{
	private static final Logger _log = Logger.getLogger(ClanWarehousePool.class.getName());

	private GArray<ClanWarehouseWork> _works;
	private boolean inWork;

	private class ClanWarehouseWork
	{
		private L2Player activeChar;
		private int[] items;
		private long[] counts;
		public boolean complete;

		public ClanWarehouseWork(L2Player _activeChar, int[] _items, long[] _counts)
		{
			activeChar = _activeChar;
			items = _items;
			counts = _counts;
			complete = false;
		}

		public synchronized void RunWork()
		{
			Warehouse warehouse = activeChar.getClan().getWarehouse();
			L2ItemInstance item;
			for(int i = 0; i < items.length; i++)
			{
				if(counts[i] <= 0)
				{
					_log.warning("Warning char:" + activeChar.getName() + " get Item from ClanWarhouse count <= 0: objid=" + items[i]);
					return;
				}
				item = warehouse.getItemByObj(items[i], counts[i]);
				if(item == null)
				{
					_log.warning("Warning char:" + activeChar.getName() + " get null Item from ClanWarhouse: objid=" + items[i]);
					continue;
				}
				activeChar.getInventory().addItem(item);
			}

			activeChar.sendChanges();
			activeChar.sendPacket(new ItemList(activeChar, false));

			complete = true;
		}
	}

	public static ClanWarehousePool getInstance()
	{
		return SingletonHolder._instance;
	}

	public ClanWarehousePool()
	{
		_works = new GArray<ClanWarehouseWork>();
		inWork = false;
	}

	public void AddWork(L2Player _activeChar, int[] _items, long[] _counts)
	{
		ClanWarehouseWork cww = new ClanWarehouseWork(_activeChar, _items, _counts);
		_works.add(cww);
		if(Config.DEBUG)
			_log.warning("ClanWarehousePool: add work, work count " + _works.size());
		RunWorks();
	}

	private void RunWorks()
	{
		if(inWork)
		{
			if(Config.DEBUG)
				_log.warning("ClanWarehousePool: work in progress, work count " + _works.size());
			return;
		}

		inWork = true;
		try
		{
			if(_works.size() > 0)
			{
				ClanWarehouseWork cww = _works.get(0);
				if(!cww.complete)
				{
					if(Config.DEBUG)
						_log.warning("ClanWarehousePool: run work, work count " + _works.size());
					cww.RunWork();
				}
				_works.remove(0);
			}
		}
		catch(Exception e)
		{
			_log.warning("Error ClanWarehousePool: " + e);
		}
		inWork = false;

		if(_works.size() > 0)
			RunWorks();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final ClanWarehousePool _instance = new ClanWarehousePool();
	}
}
