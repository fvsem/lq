package l2n.game.model.items.listeners;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

public final class FormalWearListener implements PaperdollListener
{
	public static final FormalWearListener STATIC_INSTANCE = new FormalWearListener();

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer())
			return;

		if(item.getItemId() == 6408)
			((L2Player) owner).setIsWearingFormalWear(false);
	}

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer())
			return;

		// If player equip Formal Wear unequip weapons and abort cast/attack
		if(item.getItemId() == 6408)
			((L2Player) owner).setIsWearingFormalWear(true);
	}
}
