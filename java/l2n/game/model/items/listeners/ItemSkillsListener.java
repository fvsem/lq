package l2n.game.model.items.listeners;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SkillCoolTime;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.skills.Formulas;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Item;

/**
 * Добавление\удалениe скилов, прописанных предметам в sql или в xml.
 */
public final class ItemSkillsListener implements PaperdollListener
{
	public static final ItemSkillsListener STATIC_INSTANCE = new ItemSkillsListener();

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer())
			return;

		final L2Player player = owner.getPlayer();

		final L2Item it = item.getItem();
		final L2Skill[] itemSkills = it.getAttachedSkills();
		final L2Skill enchant4Skill = it.getEnchant4Skill();

		if(itemSkills != null)
			for(final L2Skill itemSkill : itemSkills)
				// Обработка скилов от Рун
				if(itemSkill.getId() >= 26046 && itemSkill.getId() <= 26048)
				{
					final int level = player.getSkillLevel(itemSkill.getId());
					final int newlevel = level - 1;
					if(newlevel > 0)
						player.addSkill(SkillTable.getInstance().getInfo(itemSkill.getId(), newlevel), false);
					else
						player.removeSkillById(itemSkill.getId());
				}
				else
					player.removeSkill(itemSkill, false);

		if(enchant4Skill != null)
			player.removeSkill(enchant4Skill, false);

		if(itemSkills != null || enchant4Skill != null)
		{
			player.sendPacket(new SkillList(player));
			player.updateStats();
		}
	}

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer())
			return;

		final L2Player player = owner.getPlayer();

		final L2Item it = item.getItem();
		L2Skill[] itemSkills = it.getAttachedSkills();
		L2Skill enchant4Skill = null;

		if(item.getEnchantLevel() >= 4)
			enchant4Skill = it.getEnchant4Skill();

		// Для оружия при несоотвествии грейда скилы не выдаем
		if(it.getType2() == L2Item.TYPE2_WEAPON && player.getWeaponsExpertisePenalty() > 0)
		{
			itemSkills = null;
			enchant4Skill = null;
		}

		boolean update = false;

		if(itemSkills != null && itemSkills.length > 0)
			for(final L2Skill itemSkill : itemSkills)
				// Обработка скилов от Рун
				if(itemSkill.getId() >= 26046 && itemSkill.getId() <= 26048)
				{
					final int level = player.getSkillLevel(itemSkill.getId());
					int newlevel = level;
					if(level > 0)
					{
						if(SkillTable.getInstance().getInfo(itemSkill.getId(), level + 1) != null)
							newlevel = level + 1;
					}
					else
						newlevel = 1;
					if(newlevel != level)
					{
						player.addSkill(SkillTable.getInstance().getInfo(itemSkill.getId(), newlevel), false);
						update = true;
					}
				}
				// Остальных скилов
				else if(player.getSkillLevel(itemSkill.getId()) < itemSkill.getLevel())
				{
					player.addSkill(itemSkill, false);
					if(itemSkill.isActive())
					{
						// добавляем скил и талисман в список
						if(item.isTalisman() && itemSkill.isTalismanSkill())
							player.addTalismanSkill(itemSkill, item);

						int reuseDelay = Formulas.calcSkillReuseDelay(player, itemSkill);
						reuseDelay = Math.min(reuseDelay, 30000);
						if(reuseDelay > 0 && !player.isSkillDisabled(itemSkill))
							player.disableSkill(itemSkill, reuseDelay);
					}
					update = true;
				}

		if(enchant4Skill != null)
		{
			player.addSkill(enchant4Skill, false);
			update = true;
		}

		if(itemSkills != null || enchant4Skill != null)
		{
			if(update)
				player.sendPacket(new SkillList(player), new SkillCoolTime(player));
			player.updateStats();
		}
	}
}
