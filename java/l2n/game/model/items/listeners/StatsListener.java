package l2n.game.model.items.listeners;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;

public final class StatsListener implements PaperdollListener
{
	public static final StatsListener STATIC_INSTANCE = new StatsListener();

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner != null)
		{
			owner.removeStatsOwner(item);
			owner.updateStats();
			if(owner.getPet() != null)
			{
				owner.getPet().removeStatsOwner(item);
				owner.getPet().updateStats();
			}
		}
	}

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner != null)
		{
			owner.addStatFuncs(item.getStatFuncs(owner));
			owner.updateStats();
		}
	}
}
