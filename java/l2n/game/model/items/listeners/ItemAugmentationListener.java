package l2n.game.model.items.listeners;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

public final class ItemAugmentationListener implements PaperdollListener
{
	public static final ItemAugmentationListener STATIC_INSTANCE = new ItemAugmentationListener();

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		if(item.isAugmented())
		{
			item.getAugmentation().removeBoni((L2Player) owner);
			owner.updateStats();
		}
	}

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		if(item.isAugmented())
		{
			item.getAugmentation().applyBoni((L2Player) owner);
			owner.updateStats();
		}
	}
}
