package l2n.game.model.items.listeners;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.templates.L2Item;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 24.11.2011
 * @time 15:15:58
 */
public final class AccessoryListener implements PaperdollListener
{
	public static final AccessoryListener STATIC_INSTANCE = new AccessoryListener();

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		final L2Player player = (L2Player) owner;
		if(item.isAccessory() || item.isTalisman() || item.isBracelet())
		{
			player.sendUserInfo(true);
			// TODO отладить отображение аксессуаров
			player.sendPacket(new ItemList(player, false));
		}
		else
			player.broadcastCharInfo();
	}

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		final L2Player player = (L2Player) owner;
		if(item.getBodyPart() == L2Item.SLOT_L_BRACELET && item.getItem().getAttachedSkills() != null)
		{
			final int agathionId = player.getAgathion() == null ? 0 : player.getAgathion().getId();
			final int mountNpcId = player.getMountNpcId();
			for(final L2Skill skill : item.getItem().getAttachedSkills())
			{
				// При снятии браслета с агнишеном, удаляем агришена
				if(agathionId > 0 && skill.getNpcId() == agathionId)
					player.setAgathion(0);
				// При снятии браслета с волком, удаляем волка
				if(mountNpcId > 0 && skill.getNpcId() == mountNpcId)
					player.setMount(0, 0);
			}
		}

		// При снятии браслета снимаем все талисманы
		// за исключением случая, когда происходит исскусственное снятие/одевание всех предметов,
		// для пересчета всех стат игрока, а бывает это только при входе в игру,
		// смене субкласса, энчанте на атрибут/удалении атрибута
		if(item.getBodyPart() == L2Item.SLOT_R_BRACELET)
			if(!owner.getInventory().isRefreshingListeners())
			{
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO1);
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO2);
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO3);
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO4);
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO5);
				owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_DECO6);
			}

		if(item.isAccessory() || item.isTalisman() || item.isBracelet())
		{
			player.sendUserInfo(true);
			// TODO отладить отображение аксессуаров
			player.sendPacket(new ItemList(player, false));
		}
		else
			player.broadcastCharInfo();
	}
}
