package l2n.game.model.items.listeners;

import l2n.game.cache.Msg;
import l2n.game.model.L2ArmorSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.tables.ArmorSetsTable;
import l2n.game.tables.SkillTable;

import java.util.logging.Logger;

public final class ArmorSetListener implements PaperdollListener
{
	public static final ArmorSetListener STATIC_INSTANCE = new ArmorSetListener();

	private static final L2Skill COMMON_SET_SKILL = SkillTable.getInstance().getInfo(3006, 1);

	protected static final Logger _log = Logger.getLogger(ArmorSetListener.class.getName());

	@Override
	public void onEquip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		final L2Player player = (L2Player) owner;

		// checks if player worns chest item
		final L2ItemInstance chestItem = owner.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		if(chestItem == null)
			return;

		// checks if there is armorset for chest item that player worns
		final L2ArmorSet armorSet = ArmorSetsTable.getInstance().getSet(chestItem.getItemId());
		if(armorSet == null)
			return;

		boolean update = false;
		// checks if equipped item is part of set
		if(armorSet.containItem(slot, item.getItemId()))
		{
			if(armorSet.containAll(player))
			{
				for(final L2Skill skill : armorSet.getSkills())
					if(skill != null)
					{
						player.addSkill(skill, false);
						player.addSkill(COMMON_SET_SKILL, false);
						update = true;
					}

				if(armorSet.containShield(player)) // has shield from set
				{
					final L2Skill skills = armorSet.getShieldSkill();
					if(skills != null)
					{
						player.addSkill(skills, false);
						update = true;
					}
				}

				if(armorSet.isEnchanted6(player)) // has all parts of set enchanted to 6 or more
				{
					final L2Skill skills = armorSet.getEnchant6skill();
					if(skills != null)
					{
						player.addSkill(skills, false);
						update = true;
					}
				}
			}
		}
		else if(armorSet.containShield(item.getItemId()))
			if(armorSet.containAll(player))
			{
				final L2Skill skills = armorSet.getShieldSkill();
				if(skills != null)
				{
					player.addSkill(skills, false);
					update = true;
				}
			}
		if(update)
		{
			player.sendPacket(new SkillList(player));
			player.updateStats();
		}
	}

	@Override
	public void onUnequip(final int slot, final L2ItemInstance item, final L2Playable owner)
	{
		if(owner == null || !owner.isPlayer() || !item.isEquipable())
			return;

		boolean remove = false;
		L2Skill[] removeSkillId1 = null; // set skill
		L2Skill removeSkillId2 = null; // shield skill
		L2Skill removeSkillId3 = null; // enchant +6 skill

		if(slot == Inventory.PAPERDOLL_CHEST)
		{
			final L2ArmorSet armorSet = ArmorSetsTable.getInstance().getSet(item.getItemId());
			if(armorSet == null)
				return;

			remove = true;
			removeSkillId1 = armorSet.getSkills();
			removeSkillId2 = armorSet.getShieldSkill();
			removeSkillId3 = armorSet.getEnchant6skill();

		}
		else
		{
			final L2ItemInstance chestItem = owner.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
			if(chestItem == null)
				return;

			final L2ArmorSet armorSet = ArmorSetsTable.getInstance().getSet(chestItem.getItemId());
			if(armorSet == null)
				return;

			if(armorSet.containItem(slot, item.getItemId())) // removed part of set
			{
				remove = true;
				removeSkillId1 = armorSet.getSkills();
				removeSkillId2 = armorSet.getShieldSkill();
				removeSkillId3 = armorSet.getEnchant6skill();
			}
			else if(armorSet.containShield(item.getItemId())) // removed shield
			{
				remove = true;
				removeSkillId2 = armorSet.getShieldSkill();
			}
		}

		final L2Player player = owner.getPlayer();
		boolean update = false;
		if(remove)
		{
			if(removeSkillId1 != null)
				for(final L2Skill skill : removeSkillId1)
				{
					if(skill == null)
						continue;

					player.removeSkill(skill, false);
					player.removeSkill(COMMON_SET_SKILL, false);
					update = true;
				}

			if(removeSkillId2 != null)
			{
				player.removeSkill(removeSkillId2);
				update = true;
			}

			if(removeSkillId3 != null)
			{
				player.removeSkill(removeSkillId3);
				update = true;
			}
		}

		if(update)
		{
			// При снятии вещей из состава S80 или S84 сета снимаем плащ
			if(!owner.getInventory().isRefreshingListeners())
				if(owner.getInventory().getCloakStatus() == 0 && owner.getInventory().unEquipItemInSlot(Inventory.PAPERDOLL_BACK) != null)
					player.sendPacket(Msg.THE_CLOAK_EQUIP_HAS_BEEN_REMOVED_BECAUSE_THE_ARMOR_SET_EQUIP_HAS_BEEN_REMOVED);

			player.sendPacket(new SkillList(player));
			player.updateStats();
		}
	}
}
