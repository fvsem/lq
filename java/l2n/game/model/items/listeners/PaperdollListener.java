package l2n.game.model.items.listeners;

import l2n.commons.listener.Listener;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;

public interface PaperdollListener extends Listener<L2Playable>
{
	public void onEquip(int slot, L2ItemInstance inst, L2Playable owner);

	public void onUnequip(int slot, L2ItemInstance inst, L2Playable owner);
}
