package l2n.game.model.items;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.PetInventoryUpdate;

public class PetInventory extends Inventory
{
	private L2PetInstance _owner;

	public PetInventory(L2PetInstance owner)
	{
		_owner = owner;
	}

	@Override
	public L2PetInstance getOwner()
	{
		return _owner;
	}

	@Override
	protected ItemLocation getBaseLocation()
	{
		return ItemLocation.PET;
	}

	@Override
	protected ItemLocation getEquipLocation()
	{
		return ItemLocation.PET_EQUIP;
	}

	@Override
	protected void onRefreshWeight()
	{
		getOwner().sendPetInfo();
	}

	protected L2Player getPlayer()
	{
		return _owner.getPlayer();
	}

	@Override
	protected void sendNewItem(L2ItemInstance item)
	{
		getPlayer().sendPacket(new PetInventoryUpdate(item, L2ItemInstance.ADDED));
	}

	@Override
	protected void sendModifyItem(L2ItemInstance item)
	{
		getPlayer().sendPacket(new PetInventoryUpdate(item, L2ItemInstance.MODIFIED));
	}

	@Override
	protected void sendRemoveItem(L2ItemInstance item)
	{
		getPlayer().sendPacket(new PetInventoryUpdate(item, L2ItemInstance.REMOVED));
	}
}
