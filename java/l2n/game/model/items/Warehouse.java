package l2n.game.model.items;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.IllegalPlayerAction;
import l2n.util.Log;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ХранУлУще....наше всё.
 */
public abstract class Warehouse
{
	/**
	 * ТУп хранУлУща
	 */
	public static enum WarehouseType
	{
		PRIVATE(1, "PcWarehouse"), // пользовательское
		CLAN(2, "ClanWarehouse"), // клановое
		CASTLE(3, "CastleWarehouse"), // замковое
		FREIGHT(4, "FreightWarehouse"); // я так понял что фрахтовое

		private final int _type;
		private final String _name;

		private WarehouseType(final int type, String name)
		{
			_type = type;
			_name = name;
		}

		public int getPacketValue()
		{
			return _type;
		}

		@Override
		public String toString()
		{
			return _name;
		}
	}

	private static final Logger _log = Logger.getLogger(Warehouse.class.getName());

	public abstract int getOwnerId();

	public abstract ItemLocation getLocationType();

	public abstract WarehouseType getWarehouseType();

	public abstract String getLocationId();

	public abstract byte getLocationId(boolean addItem);

	public abstract void setLocationId(L2Player activeChar);

	private static final String query = "SELECT * FROM items WHERE owner_id=? AND loc=? ORDER BY name ASC";
	private static final String query_class = "SELECT * FROM items WHERE owner_id=? AND loc=? AND class=? ORDER BY name ASC";

	/**
	 * Получаем спУсок вещей в хранУлУще по заданному классу
	 * 
	 * @param clss
	 *            - класс предметов
	 * @return массУв предметов
	 */
	public L2ItemInstance[] listItems(ItemClass clss)
	{
		final GArray<L2ItemInstance> items = new GArray<L2ItemInstance>();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(clss == ItemClass.ALL ? query : query_class);
			statement.setInt(1, getOwnerId());
			statement.setString(2, getLocationType().name());
			if(clss != ItemClass.ALL)
				statement.setString(3, clss.name());
			rset = statement.executeQuery();

			L2ItemInstance item;
			while (rset.next())
				if((item = L2ItemInstance.restoreFromDb(rset, con, false)) != null)
					items.add(item);
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "could not restore warehouse:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return items.toArray(new L2ItemInstance[items.size()]);
	}

	public synchronized void addItem(int id, long count, String comment)
	{
		L2ItemInstance i = ItemTable.getInstance().createItem(id, 0, 0, comment);
		i.setCount(count);
		addItem(i, comment);
	}

	/**
	 * Добавляем новую вещь в хранУлУще
	 * 
	 * @param newItem
	 *            Вещь, которую добавляем
	 * @param comment
	 *            TODO
	 */
	public synchronized void addItem(L2ItemInstance newItem, String comment)
	{
		// на всякУй случай
		if(newItem == null)
			return;

		L2ItemInstance item;
		// еслУ элемент кучкуется У он есть в хранУлУще
		if(newItem.isStackable() && (item = findItemId(newItem.getItemId())) != null)
		{
			// добавляем к предмету, который у нас уже есть еще нужное колУчество
			item.setCount(item.getCount() + newItem.getCount());
			// делаем апдейт базы данных для этого предмета
			item.updateDatabase(true);
			// логУруем это знаменательное событУе
			Log.add(getWarehouseType().toString() + "|" + getOwnerId() + "|add|" + item.getItemId() + "|" + item.getObjectId() + "|" + item.getCount() + (comment == null ? "" : "|" + comment), "items");
		}
		// еслУ элемент не кучкуется УлУ его тупо еще нет в хранУлУще - просто добавляем его туда
		else
		{
			// проставляем нового владельца
			newItem.setOwnerId(getOwnerId());
			// проставляем локацУю
			newItem.setLocation(getLocationType(), getLocationId(true));
			// делаем апдейт базы данных для этого предмета
			newItem.updateDatabase(true);

			// логУруем это знаменательное событУе
			Log.add(getWarehouseType().toString() + "|" + getOwnerId() + "|add|" + newItem.getItemId() + "|" + newItem.getObjectId() + "|" + newItem.getCount() + (comment == null ? "" : "|" + comment), "items");
		}
		// Грохаем экземпляр объекта в Угре
		newItem.deleteMe();
	}

	/**
	 * Берем из хранилица предмет
	 * 
	 * @param objectId
	 *            идентификатор объекта
	 * @param count
	 *            количество, которое хотим взять
	 * @return новый объект, который взяли
	 */
	public synchronized L2ItemInstance getItemByObj(int objectId, long count)
	{
		// ищем объект
		L2ItemInstance item = L2ItemInstance.restoreFromDb(objectId, true);
		if(item == null)
		{
			_log.warning("Warehouse.destroyItem: can't destroy objectId: " + objectId + ", count: " + count);
			return null;
		}

		if(count <= 0)
		{
			item.deleteMe();
			_log.warning("WARNING get item not in " + item.getLocation() + " via " + getLocationType() + ": item objid=" + item.getObjectId() + " ownerid=" + item.getOwnerId());

			L2Player owner;
			if((owner = L2ObjectsStorage.getPlayer(getOwnerId())) != null)
				Util.handleIllegalPlayerAction(owner, "Warehouse.getItemByObj(int, long)", "get item with count <= 0", IllegalPlayerAction.CRITICAL);
			return null;
		}

		// Проверяем, чтоб локация предмета, совпадала с типом ВХ
		if(item.getLocation() != getLocationType())
		{
			item.deleteMe();
			_log.warning("WARNING get item not in " + item.getLocation() + " via " + getLocationType() + ": item objid=" + item.getObjectId() + " ownerid=" + item.getOwnerId());

			L2Player owner;
			if((owner = L2ObjectsStorage.getPlayer(getOwnerId())) != null)
				Util.handleIllegalPlayerAction(owner, "Warehouse.getItemByObj(int, long)", "get item not in " + item.getLocation() + " via " + getLocationType() + ": item objid=" + item.getObjectId(), IllegalPlayerAction.CRITICAL);
			return null;
		}

		// если предмет имеется в меньшем количестве, чем нам требуется
		if(item.getCount() <= count)
		{
			// сбрасываем локацию объекта
			item.setLocation(ItemLocation.VOID, getLocationId(true));
			// зачем-то выставляем флаг, что он хранимый.
			item.setWhFlag(true);
			// сохраняем такое положение в базе данных
			item.updateDatabase(true);
			// логируем инфу о том, что из хранилища предмет ушел
			Log.add(getWarehouseType() + "|" + getOwnerId() + "|withdraw|" + item.getItemId() + "|" + item.getObjectId() + "|" + item.getCount(), "items");
			return item;
		}

		// уменьшаем количество
		item.setCount(item.getCount() - count);
		// сохраняем в базе данных
		item.updateDatabase(true);
		// создаем новый инстанц
		L2ItemInstance Newitem = ItemTable.getInstance().createItem(item.getItem().getItemId(), getOwnerId(), objectId, "Warehouse.getItemByObj");
		// и задаем ему количество
		Newitem.setCount(count);

		// логируем инфу о том, что из хранилища предмет немного позаимствовали
		Log.add(getWarehouseType() + "|" + getOwnerId() + "|withdraw|" + item.getItemId() + "|" + item.getObjectId() + "|" + Newitem.getObjectId() + "|" + count, "items");

		return Newitem;
	}

	/**
	 * иничтожаем предмет в хранилице
	 * 
	 * @param itemId
	 *            идентификатор предмета
	 * @param count
	 *            количество
	 */
	public synchronized void destroyItem(int itemId, long count)
	{
		// ищем предмет
		L2ItemInstance item = findItemId(itemId);
		// не нашли
		if(item == null)
		{
			_log.fine("Warehouse.destroyItem: can't destroy itemId: " + itemId + ", count: " + count);
			return;
		}

		if(count <= 0)
		{
			item.deleteMe();
			L2Player owner;
			if((owner = L2ObjectsStorage.getPlayer(getOwnerId())) != null)
				Util.handleIllegalPlayerAction(owner, "destroyItem(int itemId, long count)", "destroy item with count <= 0", IllegalPlayerAction.CRITICAL);
			return;
		}

		// его меньше чем нужно
		if(item.getCount() < count)
			count = item.getCount();

		if(item.getCount() == count)
		{
			item.setCount(0);
			// удаляем предмет
			item.removeFromDb();
		}
		else
		{
			// уменьшаем количество
			item.setCount(item.getCount() - count);
			// сохраняем изменения в базе
			item.updateDatabase(true);
		}
		// логируем эту операцию
		Log.add(getWarehouseType().toString() + "|" + getOwnerId() + "|destroy item|" + item.getItemId() + "|" + item.getObjectId() + "|" + count, "items");
	}

	/**
	 * Ищем объект который представляет данный предмет в хранилище
	 * 
	 * @param itemId
	 *            идентификатор предмета
	 * @return инстанц объекта, представляющего предмет
	 */
	public L2ItemInstance findItemId(final int itemId)
	{
		L2ItemInstance foundItem = null;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT object_id FROM items WHERE owner_id=? AND loc=? AND item_id=? ");
			statement.setInt(1, getOwnerId());
			statement.setString(2, getLocationType().name());
			statement.setInt(3, itemId);
			rset = statement.executeQuery();

			if(rset.next())
				foundItem = L2ItemInstance.restoreFromDb(rset.getInt(1), false);
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "could not list warehouse: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return foundItem;
	}

	public long countOf(int itemId)
	{
		L2ItemInstance foundItem = findItemId(itemId);
		return foundItem == null ? 0 : foundItem.getCount();
	}

	/**
	 * @return
	 */
	public long getAdenaCount()
	{
		return countOf(L2Item.ITEM_ID_ADENA);
	}
}
