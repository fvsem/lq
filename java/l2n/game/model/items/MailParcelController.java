package l2n.game.model.items;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.network.serverpackets.ExChangePostState;
import l2n.game.network.serverpackets.ExShowReceivedPostList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.IllegalPlayerAction;
import l2n.util.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Контроллер почты
 * 
 * @author L2System Project
 * @date 28.07.2010
 * @time 12:14:47
 */
public final class MailParcelController
{
	private final static Logger _log = Logger.getLogger(MailParcelController.class.getName());

	private static final long AUTO_MAP_COMPACT_INTERVAL = 1000L * 60L * 60L;

	public static class Letter
	{
		public int id;
		public int type;
		public int system;
		public int unread;
		public int attachments;
		public int validtime;
		public int senderId;
		public int receiverId;
		public String senderName;
		public String receiverName;
		public String topic;
		public String body;
		public long price;
		public GArray<TradeItem> attached = GArray.emptyCollection();

		/** Если письмо требует оплаты */
		public boolean isRequiresPayment()
		{
			return type == 1;
		}
	}

	/** Проверяет время жизни писем */
	private class TimeOutChecker implements Runnable
	{
		@Override
		public void run()
		{
			lock.lock();
			try
			{
				final ArrayDeque<Letter> toRemove = new ArrayDeque<Letter>();
				for(final Letter letter : lettersByIdCache.valueCollection())
					if(isExpired(letter)) // если время закончилось - добавляем в список на удаление
						toRemove.add(letter);

				Letter letter;
				while ((letter = toRemove.poll()) != null)
					if(letter.attachments > 0) // если есть прикрепл файлы, то возвращаем их
						returnLetter(letter.id, 3);
					else
						deleteLetter(letter.id); // если нет - просто удаляем письмо
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				lock.unlock();
			}
		}
	}

	private static final ReentrantLock lock = new ReentrantLock();
	private static final ReentrantLock sendLock = new ReentrantLock();

	private final TIntObjectHashMap<Letter> lettersByIdCache;
	private final TIntObjectHashMap<GArray<Letter>> lettersByReceiverCache;
	private final TIntObjectHashMap<GArray<Letter>> lettersBySenderCache;

	private ScheduledFuture<TimeOutChecker> _scheduled;

	/** Максимальное количество вложений */
	public static final int MAX_ATTACHMENTS = 8;
	/** Максимально разрешённый размер ящика для входящих писем */
	public static final int INBOX_SIZE = 240;
	/** Максимально разрешённый размер ящика для исходящих писем */
	public static final int OUTBOX_SIZE = 240;
	/** Время жизни для писем требующих оплату (в часах) - 12 часов */
	public static final int LIFE_TIME_REQUIRES_PAYMENT_MAIL = 12;
	/** Время жизни для писем <font color=red>не</font> требующих оплату (в часах) 15 дней */
	public static final int LIFE_TIME_MAIL = 15 * 24;

	// post state
	public static final int DELETED = 0;
	public static final int READED = 1;
	public static final int REJECTED = 2;

	public static MailParcelController getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final MailParcelController _instance = new MailParcelController();
	}

	private MailParcelController()
	{
		lettersByIdCache = new TIntObjectHashMap<Letter>();
		lettersByReceiverCache = new TIntObjectHashMap<GArray<Letter>>();
		lettersBySenderCache = new TIntObjectHashMap<GArray<Letter>>();

		cleanupBD();
		loadCache();

		if(_scheduled != null)
		{
			_scheduled.cancel(true);
			_scheduled = null;
		}
		_scheduled = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new TimeOutChecker(), 60000, 60000);

		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public final void run()
			{
				cleanupBD();
				compactMaps();
			}
		}, AUTO_MAP_COMPACT_INTERVAL, AUTO_MAP_COMPACT_INTERVAL);
	}

	private final void compactMaps()
	{
		lock.lock();
		try
		{
			lettersByIdCache.compact();
			lettersByReceiverCache.compact();
			lettersBySenderCache.compact();
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Проверяет закончилось время жизни письма
	 */
	public boolean isExpired(final Letter letter)
	{
		return letter.validtime < System.currentTimeMillis() / 1000;
	}

	/**
	 * Помечает сообщение прочитанным.
	 */
	public void markMailRead(final int mailId)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("UPDATE mail SET mail.unread=0 WHERE mail.messageId=" + mailId + " LIMIT 1");
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void sendLetter(final Letter letter)
	{
		sendLetter(letter, null, null, null);
	}

	/**
	 * Отправляет письмо. attachments может быть null. sender может быть null если attachments == null или пуст.
	 * Для письма обязательно должны быть определены поля validtime (unixtime просрочки), topic и body. Остальные поля можно опустить, если установить флаг system.
	 */
	public void sendLetter(final Letter letter, final int[] attachments, final long[] attItemsQ, final L2Player sender)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement stmnt = null;
		ResultSet rs = null;
		try
		{
			sendLock.lock();
			con = L2DatabaseFactory.getInstance().getConnection();
			stmnt = con.prepareStatement("INSERT INTO `mail` (`sender`, `receiver`, `topic`, `body`, `attachments`, `needsPayment`, `price`, `expire`, `system`) VALUES (?,?,?,?,?,?,?,FROM_UNIXTIME(?),?)");
			stmnt.setInt(1, letter.senderId);
			stmnt.setInt(2, letter.receiverId);
			stmnt.setString(3, letter.topic);
			stmnt.setString(4, letter.body);
			stmnt.setInt(5, attachments == null ? 0 : attachments.length);
			stmnt.setInt(6, letter.type);
			stmnt.setLong(7, letter.price);
			stmnt.setLong(8, letter.validtime);
			stmnt.setInt(9, letter.system);
			stmnt.executeUpdate();

			DbUtils.close(stmnt);

			stmnt = con.prepareStatement("SELECT LAST_INSERT_ID()");
			rs = stmnt.executeQuery();

			if(rs.next())
				letter.id = rs.getInt(1);

			if(letter.id == 0) // письмо не добавилось в базу?
				return;

			if(attachments != null && attachments.length > 0)
			{
				final L2ItemInstance[] att = new L2ItemInstance[attachments.length];
				for(int i = 0; i < attachments.length; ++i)
					att[i] = sender.getInventory().dropItem(attachments[i], attItemsQ[i], false);
				attach(letter, att);
			}
			cache(letter);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			sendLock.unlock();
			DbUtils.closeQuietly(con, stmnt, rs);
		}
	}

	public void attach(final Letter letter, final L2ItemInstance[] items)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement stmnt = null;
		try
		{
			sendLock.lock();
			if(items != null && items.length > 0)
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				stmnt = con.prepareStatement("INSERT INTO `mail_attachments` (`messageId`, `itemId`) VALUES (?,?)");

				letter.attached = new GArray<TradeItem>(items.length);
				L2ItemInstance item;
				for(int i = 0; i < items.length; i++)
				{
					item = items[i];
					if(item == null)
						continue;

					item.setOwnerId(letter.senderId);
					item.setLocation(ItemLocation.MAIL);
					item.updateDatabase(true);
					letter.attached.add(new TradeItem(item));
					stmnt.setInt(1, letter.id);
					stmnt.setInt(2, item.getObjectId());
					stmnt.executeUpdate();
				}
				letter.attachments = letter.attached.size();
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			sendLock.unlock();
			DbUtils.closeQuietly(con, stmnt);
		}
	}

	/**
	 * Возвращает письмо из кеша.
	 */
	public Letter getLetter(final int mailId)
	{
		return lettersByIdCache.get(mailId);
	}

	/**
	 * Возвращает из кеша все входящие для чара.
	 */
	public GArray<Letter> getReceived(final int charId)
	{
		return lettersByReceiverCache.get(charId);
	}

	/**
	 * Возвращает из кеша все исходящие для чара.
	 */
	public GArray<Letter> getSent(final int charId)
	{
		return lettersBySenderCache.get(charId);
	}

	/**
	 * Вовзращает список прикрепленных вещей.
	 */
	public GArray<L2ItemInstance> listAttachedItems(final int mailId)
	{
		final GArray<L2ItemInstance> ret = new GArray<L2ItemInstance>(8);
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		ThreadConnection con = null;
		lock.lock();
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT itemId FROM mail_attachments WHERE messageId = ?");
			statement.setInt(1, mailId);
			rs = statement.executeQuery();
			L2ItemInstance attach;
			while (rs.next())
			{
				attach = L2ItemInstance.restoreFromDb(rs.getInt("itemId"), false);
				if(attach != null)
					ret.add(attach);
			}
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			lock.unlock();
			DbUtils.closeQuietly(con, statement, rs);
		}
		return ret;
	}

	/**
	 * Возвращает письмо с приложениями написавшему. Вызывается при нажатии кнопки возврата написавшим, при отказе от письма адресатом либо по таймауту. Вещи попадают на склад.
	 * 
	 * @param mailId
	 *            - id письма
	 * @param type
	 *            - причина возврата 1 - возврат, 2 - отмена, 3 - время вышло
	 */
	public void returnLetter(final int mailId, final int type)
	{
		lock.lock();
		try
		{
			final Letter letter = getLetter(mailId);
			if(letter == null)
				return;

			if(letter.attachments == 0)
			{
				deleteLetter(mailId);
				return;
			}

			final GArray<L2ItemInstance> templist = listAttachedItems(mailId);
			if(templist.isEmpty())
			{
				deleteLetter(mailId);
				return;
			}

			// Отправка уведомления об отмене
			if(!templist.isEmpty() && type > 0)
			{
				final L2Player sender = L2ObjectsStorage.getPlayer(letter.senderId);
				if(sender != null && !sender.isInOfflineMode())
					if(type == 1) // reject
						sender.sendPacket(new SystemMessage(SystemMessage.S1_RETURNED_THE_MAIL).addString(letter.receiverName));
					else if(type == 2) // canceled
						sender.sendPacket(new SystemMessage(SystemMessage.S1_CANCELED_THE_SENT_MAIL).addString(letter.receiverName));
					else if(type == 3) // expire
						sender.sendPacket(new SystemMessage(SystemMessage.S1_DID_NOT_RECEIVE_IT_DURING_THE_WAITING_TIME_SO_IT_WAS_RETURNED_AUTOMATICALLY_TNTLS).addString(letter.receiverName));
			}

			for(final L2ItemInstance item : templist)
				if(item.getLocation() == ItemLocation.MAIL)
					returnItem(item);

			deleteLetter(mailId);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Удаляет письмо. Перед этим следует вернуть приложенные вещи.
	 */
	public void deleteLetter(final int... mailIds)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement stmnt = null;
		lock.lock();
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			stmnt = con.prepareStatement("DELETE FROM mail WHERE messageId=? LIMIT 1");
			final GArray<Letter> removed = new GArray<Letter>(mailIds.length);
			for(final int id : mailIds)
			{
				final Letter letter = lettersByIdCache.remove(id);
				if(letter == null)
					continue;

				GArray<Letter> cached = lettersBySenderCache.get(letter.senderId);
				if(cached != null)
				{
					cached.remove(letter);
					if(cached.isEmpty())
						lettersBySenderCache.remove(letter.senderId);
				}
				cached = lettersByReceiverCache.get(letter.receiverId);
				if(cached != null)
				{
					cached.remove(letter);
					if(cached.isEmpty())
						lettersByReceiverCache.remove(letter.receiverId);
				}
				removed.add(letter);
				stmnt.setInt(1, id);
				stmnt.executeUpdate();
			}
			DbUtils.close(stmnt);

			stmnt = con.prepareStatement("DELETE FROM mail_attachments WHERE messageId=? LIMIT ?");
			for(final Letter letter : removed)
			{
				stmnt.setInt(1, letter.id);
				stmnt.setInt(2, letter.attachments);
				stmnt.executeUpdate();
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, stmnt);
			lock.unlock();
		}
	}

	/**
	 * Игрок забирает предметы из приложений, платит деньги если письмо платное и удаляет после этого письмо. Подразумевается что игрок онлайн.
	 * 
	 * @param postId
	 * @param cha
	 */
	public void receivePost(final int postId, final L2Player cha)
	{
		SystemMessage msg = null;
		if((msg = canReceivePost(cha)) != null)
		{
			cha.sendPacket(msg);
			return;
		}

		final Letter letter = getLetter(postId);
		if(letter == null)
			return;
		try
		{
			lock.lock();

			if(cha.getObjectId() != letter.receiverId)
			{
				Util.handleIllegalPlayerAction(cha, "MailParcelController[506]", "Player [" + cha.toString() + "] tried to read the mail does not belong to him!", IllegalPlayerAction.WARNING);
				return;
			}

			if(cha.getAdena() < letter.price)
			{
				cha.sendPacket(Msg.YOU_CANNOT_RECEIVE_BECAUSE_YOU_DON_T_HAVE_ENOUGH_ADENA);
				return;
			}

			if(letter.attached.isEmpty()) // приложений нет?
				return;

			final Inventory inv = cha.getInventory();

			// проверяем слоты
			int slots = inv.getSize();
			for(final TradeItem item : letter.attached)
				if(!(item.getItem().isStackable() && inv.getItemByItemId(item.getItemId()) != null))
					slots++;

			if(cha.getInventoryLimit() < slots)
			{
				cha.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
				return;
			}

			// проверяем вес
			long weight = 0;
			for(final TradeItem item : letter.attached)
				weight += item.getItem().getWeight() * item.getCount();
			if(inv.getTotalWeight() + weight > cha.getMaxLoad())
			{
				cha.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
				return;
			}

			if(letter.price > 0)
			{
				cha.reduceAdena(letter.price, true);
				givePayPrice(letter.senderId, 57, letter.price);
			}

			L2ItemInstance transferItem;
			for(final TradeItem item : letter.attached)
			{
				transferItem = L2ItemInstance.restoreFromDb(item.getObjectId(), true);
				if(transferItem != null && transferItem.getLocation() == ItemLocation.MAIL)
				{
					cha.sendPacket(SystemMessage.obtainItems(transferItem));
					cha.getInventory().addItem(transferItem).updateDatabase(true);
				}
			}

			cha.sendPacket(new ExShowReceivedPostList(cha.getObjectId()), new ExChangePostState(true, postId, MailParcelController.READED), Msg.MAIL_SUCCESSFULLY_RECEIVED);

			// FIXME если есть текст при получении итема, то письмо остается
			// if(letter.body != null && !letter.body.isEmpty())
			// {
			// // удаляем аттачи из базы
			// ThreadConnection con = null;
			// FiltredStatement stmnt = null;
			// try
			// {
			// con = L2DatabaseFactory.getInstance().getConnection();
			// stmnt = con.createStatement();
			// stmnt.executeUpdate("DELETE FROM mail_attachments WHERE messageId=" + letter.id);
			// }
			// catch(final Exception e)
			// {
			// _log.log(Level.SEVERE, "", e);
			// }
			// finally
			// {
			// DbUtils.closeQuietly(con, stmnt);
			// }
			// letter.attached = GArray.emptyCollection();
			// letter.attachments = 0;
			// return;
			// }

			deleteLetter(postId);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			lock.unlock();
		}
	}

	/**
	 * Дает игроку адену. Сперва пытаемся найти его в игре, потом добавить через items, в крайнем случае использум items_delayed.
	 */
	public static void givePayPrice(final int player, final int item, final long count)
	{
		final L2Player sender = L2ObjectsStorage.getPlayer(player);
		if(sender != null) // цель в игре? отлично
		{
			Functions.addItem(sender, item, count);
			sender.sendPacket(new SystemMessage(SystemMessage.S1_ACQUIRED_THE_ATTACHED_ITEM_TO_YOUR_MAIL));
		}
		else
		{
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT object_id FROM items WHERE owner_id = ? AND item_id = ? AND loc = 'INVENTORY' LIMIT 1"); // сперва пробуем найти в базе его адену в инвентаре
				statement.setInt(1, player);
				statement.setInt(2, item);
				rs = statement.executeQuery();
				if(rs.next())
				{
					final int id = rs.getInt("object_id");
					DbUtils.close(statement);
					statement = con.prepareStatement("UPDATE items SET count=count+? WHERE object_id = ? LIMIT 1"); // если нашли увеличиваем ее количество
					statement.setLong(1, count);
					statement.setInt(2, id);
					statement.executeUpdate();
				}
				else
				{
					DbUtils.close(statement);
					statement = con.prepareStatement("INSERT INTO items_delayed (owner_id,item_id,`count`,description) VALUES (?,?,?,'mail')"); // иначе используем items_delayed
					statement.setLong(1, player);
					statement.setLong(2, item);
					statement.setLong(3, count);
					statement.executeUpdate();
				}
			}
			catch(final SQLException e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
	}

	/**
	 * Возвращает вещь игроку на склад. Игрок может быть оффлайн.
	 */
	public static void returnItem(L2ItemInstance item)
	{
		final L2Player sender = L2ObjectsStorage.getPlayer(item.getOwnerId());
		if(sender != null) // цель в игре? отлично, используем стандартный механизм
		{
			item = L2ItemInstance.restoreFromDb(item.getObjectId(), true);
			sender.getWarehouse().addItem(item, null);
		}
		else if(!item.isStackable()) // нестекуемые вещи можно возвращать без проблем
		{
			item.setLocation(ItemLocation.WAREHOUSE);
			item.updateDatabase(true);
		}
		else
		{
			// стекуемые проверяем на коллизии, хотя обрабатывает он корректно и несколько одинаковых, но это некрасиво
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			ThreadConnection con = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT object_id FROM items WHERE owner_id = ? AND item_id = ? AND loc = 'WAREHOUSE' LIMIT 1"); // сперва пробуем найти в базе его вещь на складе
				statement.setInt(1, item.getOwnerId());
				statement.setInt(2, item.getItemId());
				rs = statement.executeQuery();
				if(rs.next())
				{
					final int id = rs.getInt("object_id");
					DbUtils.close(statement);
					statement = con.prepareStatement("UPDATE items SET count=count+? WHERE object_id = ? LIMIT 1"); // если нашли увеличиваем ее количество
					statement.setLong(1, item.getCount());
					statement.setInt(2, id);
					statement.executeUpdate();
				}
				else
				{
					item.setLocation(ItemLocation.WAREHOUSE);
					item.updateDatabase(true);
				}
			}
			catch(final SQLException e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
	}

	/**
	 * Вызывается при старте сервера, чистит базу от фигни
	 */
	public void cleanupBD()
	{
		ThreadConnection con = null;
		FiltredStatement stmt = null;
		ResultSet rs = null;
		int totalDeleted = 0;
		try
		{
			lock.lock();
			con = L2DatabaseFactory.getInstance().getConnection();

			// удаляем почту у удаленных чаров
			stmt = con.createStatement();
			totalDeleted += stmt.executeUpdate("DELETE mail FROM mail LEFT JOIN characters ON mail.sender = characters.obj_Id WHERE characters.obj_Id IS NULL");
			DbUtils.close(stmt);
			stmt = con.createStatement();
			totalDeleted += stmt.executeUpdate("DELETE mail FROM mail LEFT JOIN characters ON mail.receiver = characters.obj_Id WHERE characters.obj_Id IS NULL");
			DbUtils.close(stmt);

			// удаляем протухшие письма
			stmt = con.createStatement();
			totalDeleted += stmt.executeUpdate("DELETE FROM mail WHERE UNIX_TIMESTAMP(expire) < UNIX_TIMESTAMP()");
			DbUtils.close(stmt);

			// удаляем некорректные аттачи
			stmt = con.createStatement();
			totalDeleted += stmt.executeUpdate("DELETE mail_attachments FROM mail_attachments LEFT JOIN items ON mail_attachments.itemId = items.object_id WHERE items.object_id IS NULL");
			DbUtils.close(stmt);

			// чистим письма с потерянными аттачами
			stmt = con.createStatement();
			stmt.executeUpdate("UPDATE mail LEFT JOIN mail_attachments ON mail.messageId = mail_attachments.messageId SET needsPayment=0,price=0,attachments=0 WHERE mail_attachments.messageId IS NULL");
			DbUtils.close(stmt);

			// чистим от мусора в mail_attachments, возвращая вещи владельцам
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT itemId FROM mail_attachments LEFT JOIN mail ON mail.messageId = mail_attachments.messageId WHERE mail.messageId IS NULL");
			while (rs.next())
			{
				final L2ItemInstance item = L2ItemInstance.restoreFromDb(rs.getInt("itemId"), false);
				if(item.getOwnerId() == 0)
					item.removeFromDb();
				else
					returnItem(item);
			}
			DbUtils.close(stmt, rs);

			stmt = con.createStatement();
			totalDeleted += stmt.executeUpdate("DELETE mail_attachments FROM mail_attachments LEFT JOIN mail ON mail.messageId = mail_attachments.messageId WHERE mail.messageId IS NULL");
			DbUtils.close(stmt);
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, stmt, rs);
			lock.unlock();
			if(totalDeleted > 0)
				_log.info("MailParcelController: cleaned " + totalDeleted + " elements from DB.");
		}
	}

	private void loadCache()
	{
		ThreadConnection con = null;
		FiltredStatement stmt = null;
		FiltredStatement stmt2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;

		lock.lock();
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery("SELECT m.messageId, m.topic, UNIX_TIMESTAMP(m.expire) AS lifetime, m.body, m.price, m.attachments, m.needsPayment, m.unread, m.system, m.sender, m.receiver, cs.char_name, cr.char_name FROM mail m LEFT JOIN characters cs ON ( m.sender = cs.obj_Id ) LEFT JOIN characters cr ON ( m.receiver = cr.obj_Id )");
			while (rs.next())
			{
				final Letter ret = new Letter();
				ret.id = rs.getInt("m.messageId");
				ret.type = rs.getInt("m.needsPayment");
				ret.system = rs.getInt("m.system");
				ret.attachments = rs.getInt("m.attachments");
				ret.unread = rs.getInt("m.unread");
				ret.senderId = rs.getInt("m.sender");
				ret.receiverId = rs.getInt("m.receiver");
				ret.validtime = rs.getInt("lifetime");
				ret.senderName = rs.getString("cs.char_name");
				ret.receiverName = rs.getString("cr.char_name");
				ret.topic = rs.getString("m.topic");
				ret.body = rs.getString("m.body");
				ret.price = rs.getLong("m.price");

				if(ret.attachments > 0)
				{
					stmt2 = con.createStatement();
					rs2 = stmt2.executeQuery("SELECT itemId FROM mail_attachments WHERE messageId=" + ret.id);
					final GArray<TradeItem> items = new GArray<TradeItem>(ret.attachments);
					while (rs2.next())
					{
						final TradeItem ti = TradeItem.restoreFromDb(rs2.getInt("itemId"), ItemLocation.MAIL);
						if(ti != null)
							items.add(ti);
					}
					ret.attached = items;
					DbUtils.close(stmt2, rs2);
				}

				cache(ret);
			}
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, stmt, rs);
			DbUtils.closeQuietly(stmt2, rs2);
			lock.unlock();
		}
	}

	public void cache(final Letter letter)
	{
		try
		{
			lock.lock();
			lettersByIdCache.put(letter.id, letter);

			GArray<Letter> arr = lettersByReceiverCache.get(letter.receiverId);
			if(arr == null)
			{
				arr = new GArray<Letter>();
				lettersByReceiverCache.put(letter.receiverId, arr);
			}
			arr.add(letter);

			arr = lettersBySenderCache.get(letter.senderId);
			if(arr == null)
			{
				arr = new GArray<Letter>();
				lettersBySenderCache.put(letter.senderId, arr);
			}
			arr.add(letter);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			lock.unlock();
		}
	}

	public static SystemMessage canSentPost(final L2Player cha)
	{
		// если игрок не контролирует персонажа своего
		if(cha.isOutOfControl())
			return Msg.YOU_CANNOT_EXCHANGE_WHILE_BLOCKING_EVERYTHING;

		// Отправка невозможна при открытой торговой лавке или мастерской.
		if(cha.isInStoreMode())
			return Msg.YOU_CANNOT_FORWARD_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS;

		// Во время обмена отправка невозможна.
		if(cha.isInTransaction() && cha.getTransaction().isTrade())
			return Msg.YOU_CANNOT_FORWARD_DURING_AN_EXCHANGE;

		// Отправка возможна только из мирных зон.
		if(!cha.isInPeaceZone())
			return Msg.YOU_CANNOT_FORWARD_IN_A_NON_PEACE_ZONE_LOCATION;

		// Во время улучшния предмета отправка невозможна.
		if(cha.getActiveEnchantItem() != null || cha.getActiveEnchantAttrItem() != null)
			return Msg.YOU_CANNOT_FORWARD_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT;

		return null;
	}

	private static SystemMessage canReceivePost(final L2Player cha)
	{
		// если игрок не контролирует персонажа своего
		if(cha.isOutOfControl())
			return Msg.YOU_CANNOT_EXCHANGE_WHILE_BLOCKING_EVERYTHING;

		// Отправка невозможна при открытой торговой лавке или мастерской.
		if(cha.isInStoreMode())
			return Msg.YOU_CANNOT_RECEIVE_BECAUSE_THE_PRIVATE_SHOP_OR_WORKSHOP_IS_IN_PROGRESS;

		// Во время обмена отправка невозможна.
		if(cha.isInTransaction() && cha.getTransaction().isTypeOf(TransactionType.TRADE))
			return Msg.YOU_CANNOT_RECEIVE_DURING_AN_EXCHANGE;

		// Отправка возможна только из мирных зон.
		if(!cha.isInPeaceZone())
			return Msg.YOU_CANNOT_RECEIVE_IN_A_NON_PEACE_ZONE_LOCATION;

		// Во время улучшния предмета отправка невозможна.
		if(cha.getActiveEnchantItem() != null || cha.getActiveEnchantAttrItem() != null)
			return Msg.YOU_CANNOT_RECEIVE_DURING_AN_ITEM_ENHANCEMENT_OR_ATTRIBUTE_ENHANCEMENT;

		return null;
	}
}
