package l2n.game.model.items;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;

public final class ClanWarehouse extends Warehouse
{
	private L2Clan _clan;

	public ClanWarehouse(L2Clan clan)
	{
		_clan = clan;
	}

	@Override
	public int getOwnerId()
	{
		return _clan.getClanId();
	}

	@Override
	public ItemLocation getLocationType()
	{
		return ItemLocation.CLANWH;
	}

	@Override
	public String getLocationId()
	{
		return "0";
	}

	@Override
	public byte getLocationId(boolean dummy)
	{
		return 0;
	}

	@Override
	public void setLocationId(L2Player dummy)
	{}

	@Override
	public final WarehouseType getWarehouseType()
	{
		return WarehouseType.CLAN;
	}
}
