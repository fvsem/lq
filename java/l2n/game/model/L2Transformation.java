package l2n.game.model;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.tables.SkillTable;
import l2n.util.ArrayUtil;

import java.util.logging.Logger;

public abstract class L2Transformation
{
	protected final static Logger _log = Logger.getLogger(L2Transformation.class.getName());

	private final int _id;
	private final double _collisionRadius;
	private final double _collisionHeight;

	public static final int TRANSFORM_ZARICHE = 301;
	public static final int TRANSFORM_AKAMANAH = 302;

	protected int[] allowedSkills = null;

	public L2Transformation(int id, double collisionRadius, double collisionHeight)
	{
		_id = id;
		_collisionRadius = collisionRadius;
		_collisionHeight = collisionHeight;
	}

	public int getId()
	{
		return _id;
	}

	public double getCollisionRadius(L2Player player)
	{
		if(isDefaultActionListTransform())
			return player.getBaseTemplate().collisionRadius;
		return _collisionRadius;
	}

	public double getCollisionHeight(L2Player player)
	{
		if(isDefaultActionListTransform())
			return player.getBaseTemplate().collisionHeight;
		return _collisionHeight;
	}

	public abstract void onTransform(L2Player player);

	public abstract void onUntransform(L2Player player);

	public void onLevelUp(L2Player player)
	{
		if(isDefaultActionListTransform())
		{
			onTransform(player);
			for(L2Skill s : player.getTransformationSkills())
				player.addSkill(s, false);
			player.sendPacket(new SkillList(player));
		}
	}

	protected void addSkill(L2Player player, int skillId, int skillLevel)
	{
		L2Skill skill = SkillTable.getInstance().getInfoSafe(skillId, skillLevel);
		if(skill == null)
			_log.warning("L2Transformation: " + _id + " skill " + skillId + "-" + skillLevel + " (max: " + SkillTable.getInstance().getBaseLevel(skillId) + ") not found!");
		else
			player.addTransformationSkill(skill);
	}

	protected void removeSkill(L2Player player, int skillId)
	{
	}

	protected void checkPlayerSkill(L2Player player)
	{
		for(L2Skill sk : player.getAllSkillsArray())
			if(sk != null && !sk.isAllowInTransform() && isAllowedSkill(sk.getId()))
				player.addTransformationSkill(sk);
	}

	private boolean isAllowedSkill(int id)
	{
		if(allowedSkills == null || allowedSkills.length == 0)
			return false;
		return ArrayUtil.arrayContains(allowedSkills, id);
	}

	public boolean isDefaultActionListTransform()
	{
		return getId() >= 312 && getId() <= 318;
	}

	public static boolean isDefaultActionListTransform(int id)
	{
		return id >= 312 && id <= 318;
	}
}
