package l2n.game.model;

import l2n.Config;
import l2n.commons.threading.RunnableImpl;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.AbnormalEffect;
import l2n.game.skills.EffectType;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.game.tables.SkillTable;
import l2n.game.taskmanager.EffectTaskManager;
import l2n.util.ArrayUtil;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

public abstract class L2Effect extends RunnableImpl implements Comparable<L2Effect>
{
	protected static final Logger _log = Logger.getLogger(L2Effect.class.getName());

	// Состояние, при котором работает задача запланированного эффекта
	private final static int STATE_SUSPENDED = -1;

	private final static int STATE_STARTING = 0;
	private final static int STATE_STARTED = 1;
	private final static int STATE_ACTING = 2;
	private final static int STATE_FINISHING = 3;
	private final static int STATE_FINISHED = 4;

	private final static String getState(final int state)
	{
		switch (state)
		{
			case STATE_SUSPENDED:
				return "SUSPENDED";
			case STATE_STARTING:
				return "STARTING";
			case STATE_STARTED:
				return "STARTED";
			case STATE_ACTING:
				return "ACTING";
			case STATE_FINISHING:
				return "FINISHING";
			case STATE_FINISHED:
				return "FINISHED";
		}
		return "UNKNOW";
	}

	private class ActionDispelListener implements MethodInvokeListener, MethodCollection
	{
		@Override
		public boolean accept(final MethodEvent event)
		{
			return event.getMethodName().equals(onStartAttack) || event.getMethodName().equals(onStartCast) || event.getMethodName().equals(onStartAltCast);
		}

		@Override
		public void methodInvoked(final MethodEvent e)
		{
			exit();
		}
	}

	private static final int[] summonModifSkills =
	{
			1476, // Appetite for Destruction
			1477, // Vampiric Impulce
			1478, // Protection Instinct
			1479, // Magic Impulce
			4699, // Blessing of Queen
			4700, // Gift of Queen
			4702, // Blessing of Seraphim
			4703 // Gift of Seraphim
	};

	/** Накладывающий эффект */
	protected final L2Character _effector;
	/** Тот, на кого накладывают эффект */
	protected final L2Character _effected;

	protected final L2Skill _skill;
	protected final int _displayId;
	protected final int _displayLevel;

	// the value of an update
	private final double _value;

	// the current state
	private final AtomicInteger _state;

	private final ReentrantLock lock = new ReentrantLock();

	// counter
	private int _count;
	// period, milliseconds
	private long _period;
	private long _startTimeMillis;
	private long _duration;

	public EffectTemplate _template;
	private ActionDispelListener _listener;

	// function templates
	private final FuncTemplate[] _funcTemplates;

	private boolean _inUse = false;
	private L2Effect _next = null;
	private boolean _active = false;

	private final String _options;
	private Future<?> _effectTask;

	protected L2Effect(final Env env, final EffectTemplate template)
	{
		_template = template;
		_skill = env.skill;
		_effector = env.character;
		_effected = env.target;
		_value = template._value;
		_funcTemplates = template._funcTemplates;

		_options = template._options;

		_count = template._ticks;
		_period = template._period;
		_displayId = template._displayId != 0 ? template._displayId : _skill.getDisplayId();
		_displayLevel = template._displayLevel != 0 ? template._displayLevel : _skill.getDisplayLevel();

		if(Config.UNAFFECTED_SKILL_LIST == null || !Config.UNAFFECTED_SKILL_LIST.contains(_skill.getId()))
		{
			// Применяем модификатор времени песен и танцев
			if(_skill.isDanceSong())
				_period *= Config.SONGDANCETIME_MODIFIER;
			// Применяем модификатор времени баффов
			// пропускаем 396 Heroic Berserker и 1374 Heroic Valor
			else if(_skill.getSkillType() == SkillType.BUFF && !_skill.isSelfBuf() && _skill.getId() != 396 && _skill.getId() != 1374)
				_period *= Config.BUFFTIME_MODIFIER;
			// Применяем модификатор времени селф-баффов
			else if(_skill.isSelfBuf())
				_period *= Config.SELF_BUFFTIME_MODIFIER;

			// Применяем модификатор времени баффов суммонов
			switch (Config.SUMMON_SET_BUFF_TYPE)
			{
				case 1:
					for(final int skillId : summonModifSkills)
						if(_skill.getId() == skillId)
						{
							_period *= Config.SUMMON_BUFF_MODIFIER;
							break;
						}
					break;
				case 2:
				{
					for(final int skillId : summonModifSkills)
						if(_skill.getId() == skillId)
						{
							_period = Config.SUMMON_BUFF_TIME;
							break;
						}
				}
			}
		}

		_duration = _period * _count;
		_state = new AtomicInteger(STATE_STARTING);
	}

	public long getPeriod()
	{
		return _period;
	}

	public void setPeriod(final long time)
	{
		_period = time;
		_duration = _period * _count;
	}

	public int getCount()
	{
		return _count;
	}

	public void setCount(final int newcount)
	{
		_count = newcount;
		_duration = _period * _count;
	}

	/**
	 * Эффект однократного действия (Как правило это побочные эффекты для скиллов моментального действия)
	 */
	public boolean isOneTime()
	{
		return _period == 0;
	}

	/**
	 * Возвращает время старта эффекта, если время не установлено, возвращается текущее
	 */
	public long getStartTime()
	{
		if(_startTimeMillis == 0L)
			return System.currentTimeMillis();
		return _startTimeMillis;
	}

	/** Возвращяет время в ms с момента старта эффекта */
	public long getTime()
	{
		return System.currentTimeMillis() - getStartTime();
	}

	/** Возвращает длительность эффекта в миллисекундах. */
	public long getDuration()
	{
		return _duration;
	}

	/** Возвращает оставшееся время в миллисекундах. */
	public long getTimeLeft()
	{
		return getDuration() - getTime();
	}

	/** Возвращает true, если осталось время для действия эффекта */
	public boolean isTimeLeft()
	{
		return getDuration() - getTime() > 0L;
	}

	public final boolean isInUse()
	{
		return _inUse;
	}

	public void setInUse(final boolean inUse)
	{
		_inUse = inUse;
	}

	public boolean isActive()
	{
		return _active;
	}

	/**
	 * Для неактивных эфектов не вызывается onActionTime.
	 */
	private void setActive(final boolean set)
	{
		_active = set;
	}

	public EffectTemplate getTemplate()
	{
		return _template;
	}

	/** The Identifier of the stack group 1 */
	public String getStackType()
	{
		return getTemplate()._stackType;
	}

	/** The Identifier of the stack group 2 */
	public String getStackType2()
	{
		return getTemplate()._stackType2;
	}

	public boolean checkStackType(final String param)
	{
		return getStackType().equalsIgnoreCase(param) || getStackType2().equalsIgnoreCase(param);
	}

	public boolean checkStackType(final L2Effect effect)
	{
		return checkStackType(effect.getStackType()) || checkStackType(effect.getStackType2());
	}

	/** The position of the effect in the stack group */
	public int getStackOrder()
	{
		return getTemplate()._stackOrder;
	}

	public L2Skill getSkill()
	{
		return _skill;
	}

	/** Накладывающий эффект */
	public L2Character getEffector()
	{
		return _effector;
	}

	/** Тот, на кого накладывают эффект */
	public L2Character getEffected()
	{
		return _effected;
	}

	public double calc()
	{
		return _value;
	}

	public boolean isEnded()
	{
		return isFinished() || isFinishing();
	}

	public boolean isFinishing()
	{
		return getState() == STATE_FINISHING;
	}

	public boolean isFinished()
	{
		return getState() == STATE_FINISHED;
	}

	private int getState()
	{
		return _state.get();
	}

	private boolean setState(final int oldState, final int newState)
	{
		return _state.compareAndSet(oldState, newState);
	}

	/** returns effect type */
	public EffectType getEffectType()
	{
		return getTemplate()._effectType;
	}

	/** Notify started */
	public void onStart()
	{
		// Применяем эффект к параметрам персонажа
		getEffected().addStatFuncs(getStatFuncs());
		if(getTemplate()._abnormalEffect != AbnormalEffect.NULL)
			getEffected().startAbnormalEffect(getTemplate()._abnormalEffect);
		else if(getEffectType().getAbnormal() != null)
			getEffected().startAbnormalEffect(getEffectType().getAbnormal());

		if(getTemplate()._specialEffect != AbnormalEffect.NULL)
			getEffected().startAbnormalEffect(getTemplate()._specialEffect);
		if(getTemplate()._cancelOnAction)
			getEffected().addMethodInvokeListener(_listener = new ActionDispelListener());
	}

	/**
	 * Cancel the effect in the the abnormal effect map of the effected L2Character.<BR>
	 * <BR>
	 */
	public void onExit()
	{
		getEffected().removeStatsOwner(this);
		if(getTemplate()._abnormalEffect != AbnormalEffect.NULL)
			getEffected().stopAbnormalEffect(getTemplate()._abnormalEffect);
		else if(getEffectType().getAbnormal() != null)
			getEffected().stopAbnormalEffect(getEffectType().getAbnormal());

		if(getTemplate()._specialEffect != AbnormalEffect.NULL)
			getEffected().stopAbnormalEffect(getTemplate()._specialEffect);
		if(getTemplate()._cancelOnAction)
			getEffected().removeMethodInvokeListener(_listener);
		if(getEffected().isPlayer() && getStackType().equals(EffectTemplate.HP_RECOVER_CAST))
			getEffected().sendPacket(new ShortBuffStatusUpdate());
	}

	private void stopEffectTask()
	{
		if(_effectTask != null)
			_effectTask.cancel(false);
	}

	private void startEffectTask()
	{
		if(_effectTask == null)
		{
			_startTimeMillis = System.currentTimeMillis();
			_effectTask = EffectTaskManager.getInstance().scheduleAtFixedRate(this, _period, _period);
		}
	}

	/** Return true for continuation of this effect */
	public abstract boolean onActionTime();

	/**
	 * Добавляет эффект в список эффектов, в случае успешности вызывается метод start
	 */
	public final void schedule()
	{
		final L2Character effected = getEffected();
		if(effected == null)
			return;

		if(!checkCondition())
			return;

		getEffected().getEffectList().addEffect(this);
	}

	/**
	 * Завершает эффект и все связанные, удаляет эффект из списка эффектов
	 */
	public final void exit()
	{
		final L2Effect next = getNext();
		if(next != null)
			next.exit();
		removeNext();

		// Эффект запланирован на запуск, удаляем
		if(setState(STATE_STARTING, STATE_FINISHED))
		{
			getEffected().removeStatsOwner(this);
			getEffected().getEffectList().removeEffect(this);
		}
		// Эффект работает в "фоне", останавливаем задачу в планировщике
		else if(setState(STATE_SUSPENDED, STATE_FINISHED))
			stopEffectTask();
		else if(setState(STATE_STARTED, STATE_FINISHED) || setState(STATE_ACTING, STATE_FINISHED))
		{
			lock.lock();
			try
			{
				if(isInUse())
				{
					setInUse(false);
					setActive(false);
					stopEffectTask();
					onExit();
				}
			}
			finally
			{
				lock.unlock();
			}
			getEffected().getEffectList().removeEffect(this);
		}
	}

	/**
	 * Переводит эффект в "фоновый" режим, эффект может быть запущен методом schedule
	 */
	private final void suspend()
	{
		// Эффект создан, запускаем задачу в фоне
		if(setState(STATE_STARTING, STATE_SUSPENDED))
			startEffectTask();
		else if(setState(STATE_STARTED, STATE_SUSPENDED) || setState(STATE_ACTING, STATE_SUSPENDED))
		{
			lock.lock();
			try
			{
				if(isInUse())
				{
					setInUse(false);
					setActive(false);
					onExit();
				}
			}
			finally
			{
				lock.unlock();
			}
			getEffected().getEffectList().removeEffect(this);
		}
	}

	/**
	 * Запускает задачу эффекта, в случае если эффект успешно добавлен в список
	 */
	public final void start()
	{
		if(setState(STATE_STARTING, STATE_STARTED))
		{
			lock.lock();
			try
			{
				if(isInUse())
				{
					setActive(true);
					onStart();
					startEffectTask();
				}
			}
			finally
			{
				lock.unlock();
			}
		}
		run();
	}

	@Override
	public final void runImpl() throws Exception
	{
		if(setState(STATE_STARTED, STATE_ACTING))
		{
			// Отображать сообщение только для первого эффекта скилла
			if(!getSkill().isHideStartMessage() && !isHidden() && getEffected().getEffectList().getEffectsCount(getSkill().getId()) == 1)
				getEffected().sendPacket(new SystemMessage(SystemMessage.YOU_CAN_FEEL_S1S_EFFECT).addSkillName(_displayId, _displayLevel));

			return;
		}

		if(getState() == STATE_SUSPENDED)
		{
			if(isTimeLeft())
			{
				_count--;
				if(isTimeLeft())
					return;
			}

			exit();
			return;
		}

		if(getState() == STATE_ACTING)
			if(isTimeLeft())
			{
				_count--;
				if((!isActive() || onActionTime()) && isTimeLeft())
					return;
			}

		if(setState(STATE_ACTING, STATE_FINISHING))
			setInUse(false);

		if(setState(STATE_FINISHING, STATE_FINISHED))
		{
			lock.lock();
			try
			{
				setActive(false);
				stopEffectTask();
				onExit();
			}
			finally
			{
				lock.unlock();
			}

			boolean autoBuff = true;
			// Добавляем следующий запланированный эффект
			final L2Effect next = getNext();
			if(next != null)
				if(next.setState(STATE_SUSPENDED, STATE_STARTING))
				{
					autoBuff = false;
					next.schedule();
				}

			// отложенный запуск другого эффекта у скила
			if(getSkill().getDelayedEffect() > 0)
				SkillTable.getInstance().getInfo(getSkill().getDelayedEffect(), 1).getEffects(_effector, _effected, false, false);

			final boolean msg = !isHidden() && getEffected().getEffectList().getEffectsCount(getSkill().getId()) == 1;

			getEffected().getEffectList().removeEffect(this);

			// Отображать сообщение только для последнего оставшегося эффекта скилла
			if(msg)
				getEffected().sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_WORN_OFF).addSkillName(_displayId, _displayLevel));

			if(autoBuff && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF && Config.COMMUNITY_BOARD_BUFFER_ALLOW_AUTOBUFF_EFFECT_EXIT && getEffected() == getEffected().getPlayer() && getEffected().getPlayer().isCommunityAutoBuff())
				getEffected().fireMethodInvoked(MethodCollection.AutoBuff, new Object[] { 1 });
		}
	}

	public Func[] getStatFuncs()
	{
		if(_funcTemplates.length == 0)
			return ArrayUtil.EMPTY_FUNCTION_SET;

		final Func[] funcs = new Func[_funcTemplates.length];
		for(int i = 0; i < funcs.length; i++)
		{
			final Env env = new Env();
			env.character = _effector;
			env.target = _effected;
			env.skill = _skill;
			final Func f = _funcTemplates[i].getFunc(env, this); // effect is owner
			funcs[i] = f;
		}
		return funcs;
	}

	public void addIcon(final AbnormalStatusUpdate mi)
	{
		if(!isActive() || isHidden())
			return;
		final int duration = _skill.isToggle() ? AbnormalStatusUpdate.INFINITIVE_EFFECT : (int) (getTimeLeft() / 1000);
		mi.addEffect(_displayId, _displayLevel, duration);
	}

	public void addPartySpelledIcon(final PartySpelled ps)
	{
		if(!isActive() || isHidden())
			return;
		final int duration = _skill.isToggle() ? AbnormalStatusUpdate.INFINITIVE_EFFECT : (int) (getTimeLeft() / 1000);
		ps.addPartySpelledEffect(_displayId, _displayLevel, duration);
	}

	public void addOlympiadSpelledIcon(final L2Player player, final ExOlympiadSpelledInfo os)
	{
		if(!isActive() || isHidden())
			return;
		final int duration = _skill.isToggle() ? AbnormalStatusUpdate.INFINITIVE_EFFECT : (int) (getTimeLeft() / 1000);
		os.addSpellRecivedPlayer(player);
		os.addEffect(_displayId, _displayLevel, duration);
	}

	public boolean containsStat(final Stats stat)
	{
		if(_funcTemplates.length != 0)
			for(int i = 0; i < _funcTemplates.length; i++)
				if(_funcTemplates[i]._stat == stat)
					return true;
		return false;
	}

	@Override
	public int compareTo(final L2Effect obj)
	{
		if(obj.equals(this))
			return 0;
		return 1;
	}

	public void removeNext()
	{
		_next = null;
	}

	/**
	 * Поставить в очередь эффект
	 * 
	 * @param e
	 * @return true, если эффект поставлен в очередь
	 */
	private final boolean scheduleNext(final L2Effect e)
	{
		if(e == null || e.isEnded())
			return false;

		final L2Effect next = getNext();
		if(next != null && !next.maybeScheduleNext(e))
			return false;

		_next = e;
		return true;
	}

	/**
	 * Проверяет, может ли новый эффект заменить текущий
	 * 
	 * @return false - игнорировать новый эффект, true - использовать новый эффект
	 */
	public boolean maybeScheduleNext(final L2Effect newEffect)
	{
		if(newEffect.getStackOrder() < getStackOrder()) // новый эффект слабее
		{
			if(newEffect.getTimeLeft() > getTimeLeft()) // новый эффект длинее
			{
				newEffect.suspend();
				scheduleNext(newEffect); // пробуем пристроить новый эффект в очередь
			}

			return false; // более слабый эффект всегда игнорируется, даже если не попал в очередь
		}
		// если старый не дольше, то просто остановить его
		else if(newEffect.getTimeLeft() >= getTimeLeft())
		{
			// наследуем зашедуленый старому, если есть смысл
			if(getNext() != null && getNext().getTimeLeft() > newEffect.getTimeLeft())
			{
				newEffect.scheduleNext(getNext());
				// отсоединяем зашедуленные от текущего
				removeNext();
			}
			exit();
		}
		// если новый короче то зашедулить старый
		else
		{
			suspend();
			newEffect.scheduleNext(this);
		}

		return true;
	}

	public L2Effect getNext()
	{
		return _next;
	}

	public boolean isSaveable()
	{
		return getTimeLeft() >= 15000 && getSkill().isSaveable();
	}

	public int getDisplayId()
	{
		return _displayId;
	}

	public int getDisplayLevel()
	{
		return _displayLevel;
	}

	@Override
	public String toString()
	{
		return "Effect: " + _skill;
	}

	public String effectToString()
	{
		return "Effect: [" + _skill.toString() + "], state: " + getState(_state.get()) + ", count: " + _count + ", period: " + _period + ", inUse: " + _inUse + ", active: " + _active + ", next:[" + _next + "]";
	}

	public boolean isHidden()
	{
		return _template.isHidden() || _displayId < 0;
	}

	public String getOptions()
	{
		return _options;
	}

	public boolean checkCondition()
	{
		return true;
	}

	public boolean isOffensive()
	{
		final Boolean template = _template.getParam().getBool("isOffensive", null);
		if(template != null)
			return template;
		return getSkill().isOffensive();
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
