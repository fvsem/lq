package l2n.game.model;

import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.DeleteObject;
import l2n.game.network.serverpackets.L2GameServerPacket;

import java.util.ArrayDeque;

public class L2ObjectProcedures
{
	public static final class RemoveObjectsFromPlayerProc implements INgVoidProcedure<L2Object>
	{
		private final L2Player _player;
		private final ArrayDeque<L2GameServerPacket> _result;
		private final DeleteObject _packet;
		private final boolean _move;

		public RemoveObjectsFromPlayerProc(final L2Player player, final ArrayDeque<L2GameServerPacket> result, final DeleteObject packet, final boolean move)
		{
			_player = player;
			_result = result;
			_move = move;
			_packet = packet;
		}

		private L2GameServerPacket p;

		@Override
		public void execute(final L2Object obj)
		{
			p = _player.removeVisibleObject(obj, _packet, _move);
			if(p != null)
				_result.add(p);
		}
	}

	public static final class RemovePlayerFromOtherPlayersProc implements INgVoidProcedure<L2Player>
	{
		private final L2Object _object;
		private final DeleteObject _packet;
		private final boolean _move;

		public RemovePlayerFromOtherPlayersProc(final L2Object object, final DeleteObject packet, final boolean move)
		{
			_object = object;
			_move = move;
			_packet = packet;
		}

		private L2GameServerPacket p;

		@Override
		public void execute(final L2Player player)
		{
			p = player.removeVisibleObject(_object, _packet, _move);
			if(p != null)
				player.sendPacket(p);
		}
	}

	public static final class SendPacketProc implements INgVoidProcedure<L2Player>
	{
		private final L2GameServerPacket _paket;

		public SendPacketProc(final L2GameServerPacket p)
		{
			_paket = p;
		}

		@Override
		public void execute(final L2Player player)
		{
			if(player != null && !player.isInOfflineMode())
				player.sendPacket(_paket);
		}
	}

	public static final class SendPacketsProc implements INgVoidProcedure<L2Player>
	{
		private final L2GameServerPacket[] _paket;

		public SendPacketsProc(final L2GameServerPacket... p)
		{
			_paket = p;
		}

		@Override
		public void execute(final L2Player player)
		{
			if(player != null && !player.isInOfflineMode())
				player.sendPacket(_paket);
		}
	}

	public static final class SendCustomMessageProc implements INgVoidProcedure<L2Player>
	{
		private final String _address;
		private final String[] _replacements;
		private final int _type;

		private CustomMessage cm;

		public SendCustomMessageProc(final String address, final String[] replacements, final int type)
		{
			_address = address;
			_replacements = replacements;
			_type = type;
		}

		@Override
		public void execute(final L2Player player)
		{
			if(player != null && !player.isInOfflineMode())
			{
				cm = new CustomMessage(_address, player);
				if(_replacements != null && _replacements.length > 0)
					for(final String s : _replacements)
						cm.addString(s);
				player.sendPacket(new CreatureSay(0, _type, "", cm.toString()));
			}
		}
	}

	public static final class CheckAggresionProc implements INgVoidProcedure<L2NpcInstance>
	{
		private final L2Playable _playable;

		public CheckAggresionProc(final L2Playable p)
		{
			_playable = p;
		}

		@Override
		public final void execute(final L2NpcInstance npc)
		{
			if(npc != null && npc.getAI() != null)
				npc.getAI().checkAggression(_playable);
		}
	}

	public static final INgVoidProcedure<L2NpcInstance> PROC_START_AI = new INgVoidProcedure<L2NpcInstance>()
	{
		@Override
		public final void execute(final L2NpcInstance npc)
		{
			if(npc != null && npc.getAI() != null)
				npc.getAI().startAITask();
		}
	};

	public static final INgVoidProcedure<L2NpcInstance> PROC_DELETE_NPC = new INgVoidProcedure<L2NpcInstance>()
	{
		@Override
		public final void execute(final L2NpcInstance npc)
		{
			if(npc != null)
				npc.deleteMe();
		}
	};

	public static final INgVoidProcedure<L2Object> PROC_DELETE_L2OBJECT = new INgVoidProcedure<L2Object>()
	{
		@Override
		public final void execute(final L2Object object)
		{
			if(object != null)
				object.deleteMe();
		}
	};

	public static final INgVoidProcedure<L2ItemInstance> PROC_DELETE_ITEMS = new INgVoidProcedure<L2ItemInstance>()
	{
		@Override
		public final void execute(final L2ItemInstance item)
		{
			if(item != null)
				item.deleteMe();
		}
	};

	public static final INgVoidProcedure<L2Spawn> PROC_DESPAWN_ALL = new INgVoidProcedure<L2Spawn>()
	{
		@Override
		public void execute(final L2Spawn spawn)
		{
			if(spawn != null)
				spawn.despawnAll();
		}
	};
}
