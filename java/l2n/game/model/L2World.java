package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.instancemanager.CatacombSpawnManager;
import l2n.game.instancemanager.DayNightSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;

import java.util.logging.Logger;

public class L2World
{
	private static final Logger _log = Logger.getLogger(L2World.class.getName());

	/** Map dimensions */
	public static final int MAP_MIN_X = Config.GEO_X_FIRST - 20 << 15;
	public static final int MAP_MAX_X = (Config.GEO_X_LAST - 19 << 15) - 1;
	public static final int MAP_MIN_Y = Config.GEO_Y_FIRST - 18 << 15;
	public static final int MAP_MAX_Y = (Config.GEO_Y_LAST - 17 << 15) - 1;
	public static final int MAP_MIN_Z = -32768;
	public static final int MAP_MAX_Z = 32767;

	public static final int WORLD_SIZE_X = Config.GEO_X_LAST - Config.GEO_X_FIRST + 1;
	public static final int WORLD_SIZE_Y = Config.GEO_Y_LAST - Config.GEO_Y_FIRST + 1;

	public static final int DIV_BY = Config.DIV_BY;
	public static final int DIV_BY_FOR_Z = Config.DIV_BY_FOR_Z;
	private static final int SMALL_DIV_BY = Config.DIV_BY / 2;

	public static final int OFFSET_X = Math.abs(MAP_MIN_X / DIV_BY);
	public static final int OFFSET_Y = Math.abs(MAP_MIN_Y / DIV_BY);
	public static final int OFFSET_Z = Math.abs(MAP_MIN_Z / DIV_BY_FOR_Z);

	private static final int SMALL_OFFSET_X = Math.abs(MAP_MIN_X / DIV_BY);
	private static final int SMALL_OFFSET_Y = Math.abs(MAP_MIN_Y / DIV_BY);

	private static final int REGIONS_X = MAP_MAX_X / DIV_BY + OFFSET_X;
	private static final int REGIONS_Y = MAP_MAX_Y / DIV_BY + OFFSET_Y;
	private static final int REGIONS_Z = MAP_MAX_Z / DIV_BY_FOR_Z + OFFSET_Z;

	private static final int SMALL_REGIONS_X = MAP_MAX_X / SMALL_DIV_BY + SMALL_OFFSET_X;
	private static final int SMALL_REGIONS_Y = MAP_MAX_Y / SMALL_DIV_BY + SMALL_OFFSET_Y;

	private final static L2WorldRegion[][][] _worldRegions = new L2WorldRegion[REGIONS_X + 1][REGIONS_Y + 1][];
	private final static L2WorldRegion[][] _smallWorldRegions = new L2WorldRegion[SMALL_REGIONS_X + 1][SMALL_REGIONS_Y + 1];


	static
	{
		int count = 0;
		final String[] split_regions = Config.VERTICAL_SPLIT_REGIONS.split(";");
		for(int x = 1; x <= REGIONS_X; x++)
			for(int y = 1; y <= REGIONS_Y; y++)
			{
				final int wx = (x - OFFSET_X) * DIV_BY;
				final int wy = (y - OFFSET_Y) * DIV_BY;

				final byte rx = (byte) (Math.floor((float) wx / (float) 32768) + 20);
				final byte ry = (byte) (Math.floor((float) wy / (float) 32768) + 18);

				boolean vertical_split = false;
				final String square = rx + "_" + ry;
				for(final String split_region : split_regions)
					if(split_region.equals(square))
					{
						vertical_split = true;
						count++;
						break;
					}

				if(vertical_split)
					_worldRegions[x][y] = new L2WorldRegion[REGIONS_Z + 1];
				else
					_worldRegions[x][y] = new L2WorldRegion[1];
			}

		_log.info("L2World: vertical split regions " + count + ".");
		_log.info("L2World: (" + L2World.REGIONS_X + " by " + L2World.REGIONS_Y + ") World Region Grid set up.");
	}

	public static GArray<L2WorldRegion> getNeighbors(final int x, final int y, final int z, final boolean small)
	{
		final GArray<L2WorldRegion> neighbors = new GArray<L2WorldRegion>();
		if(small)
		{
			for(int a = -Config.VIEW_OFFSET; a <= Config.VIEW_OFFSET; a++)
				for(int b = -Config.VIEW_OFFSET; b <= Config.VIEW_OFFSET; b++)
					if(validRegion(x + a, y + b, 0, true) && _smallWorldRegions[x + a][y + b] != null)
						neighbors.add(_smallWorldRegions[x + a][y + b]);
		}
		else
			for(int a = -Config.VIEW_OFFSET; a <= Config.VIEW_OFFSET; a++)
				for(int b = -Config.VIEW_OFFSET; b <= Config.VIEW_OFFSET; b++)
					if(validRegion(x + a, y + b, 0, false))
						if(_worldRegions[x + a][y + b].length > 1)
						{
							for(int c = -Config.VIEW_OFFSET; c <= Config.VIEW_OFFSET; c++)
								if(validRegion(x + a, y + b, z + c, false) && _worldRegions[x + a][y + b][z + c] != null)
									neighbors.add(_worldRegions[x + a][y + b][z + c]);
						}
						else if(_worldRegions[x + a][y + b][0] != null)
							neighbors.add(_worldRegions[x + a][y + b][0]);
		return neighbors;
	}

	public static GArray<L2WorldRegion> getNeighborsZ(final int x, final int y, final int z1, final int z2)
	{
		final GArray<L2WorldRegion> neighbors = new GArray<L2WorldRegion>();

		final int _x = x / DIV_BY + OFFSET_X;
		final int _y = y / DIV_BY + OFFSET_Y;
		final int _z1 = z1 / DIV_BY_FOR_Z + OFFSET_Z;
		final int _z2 = z2 / DIV_BY_FOR_Z + OFFSET_Z;

		for(int a = -1; a <= 1; a++)
			for(int b = -1; b <= 1; b++)
				if(validRegion(_x + a, _y + b, 0, false))
					if(_worldRegions[_x + a][_y + b].length > 1)
					{
						for(int c = _z1; c <= _z2; c++)
							if(validRegion(_x + a, _y + b, c, false) && _worldRegions[_x + a][_y + b][c] != null)
								neighbors.add(_worldRegions[_x + a][_y + b][c]);
					}
					else if(_worldRegions[_x + a][_y + b][0] != null)
						neighbors.add(_worldRegions[_x + a][_y + b][0]);

		return neighbors;
	}

	public static boolean validRegion(final int x, final int y, final int z, final boolean small)
	{
		if(small)
			return x >= 1 && x <= SMALL_REGIONS_X && y >= 1 && y <= SMALL_REGIONS_Y;
		return x >= 1 && x <= REGIONS_X && y >= 1 && y <= REGIONS_Y && z >= 0 && z < _worldRegions[x][y].length;
	}

	public static L2WorldRegion getRegion(final L2Object obj)
	{
		return getRegion(obj.getX(), obj.getY(), obj.getZ(), obj.getReflection().getId() == -2);
	}

	public static L2WorldRegion getRegion(final Location loc, final long refId)
	{
		return getRegion(loc.x, loc.y, loc.z, refId == -2);
	}

	public static L2WorldRegion getRegion(final int x, final int y, final int z, final boolean small)
	{
		if(small)
		{
			final int _x = x / SMALL_DIV_BY + SMALL_OFFSET_X;
			final int _y = y / SMALL_DIV_BY + SMALL_OFFSET_Y;
			if(validRegion(_x, _y, 0, true))
			{
				if(_smallWorldRegions[_x][_y] == null)
					_smallWorldRegions[_x][_y] = new L2WorldRegion(_x, _y, 0, true);
				return _smallWorldRegions[_x][_y];
			}
		}
		else
		{
			final int _x = x / DIV_BY + OFFSET_X;
			final int _y = y / DIV_BY + OFFSET_Y;

			if(validRegion(_x, _y, 0, false))
			{
				int _z = 0;
				if(_worldRegions[_x][_y].length > 1)
				{
					_z = z / DIV_BY_FOR_Z + OFFSET_Z;
					if(!validRegion(_x, _y, _z, false))
						return null;
				}

				if(_worldRegions[_x][_y][_z] == null)
					_worldRegions[_x][_y][_z] = new L2WorldRegion(_x, _y, _z, false);
				return _worldRegions[_x][_y][_z];
			}
		}
		return null;
	}

	public static void deleteRegion(final int tileX, final int tileY, final int tileZ, final boolean small)
	{
		if(small)
			_smallWorldRegions[tileX][tileY] = null;
		else
			_worldRegions[tileX][tileY][tileZ] = null;
	}

	public static void removeObject(final L2Object object)
	{
		if(object != null)
			object.clearTerritories();
	}

	public static void addVisibleObject(final L2Object object, final L2Character dropper)
	{
		if(object == null || !object.isVisible() || object.inObserverMode())
			return;

		if(object.isPet() || object.isSummon())
		{
			final L2Player owner = object.getPlayer();
			if(owner != null && object.getReflection() != owner.getReflection())
				object.setReflection(owner.getReflection());
		}

		final L2WorldRegion region = getRegion(object);
		final L2WorldRegion currentRegion = object.getCurrentRegion();

		if(region == null || currentRegion != null && currentRegion.equals(region))
			return;

		if(object.isNpc())
			((L2NpcInstance) object).setShowSpawnAnimation(((L2NpcInstance) object).getTemplate().getAIParams().getBool(AiOptionsType.SPAWN_ANIMATION_DISABLED, false) ? 0 : 2);

		if(currentRegion == null)
		{
			for(final L2WorldRegion neighbor : region.getNeighbors())
				neighbor.addToPlayers(object, dropper);

			region.addObject(object);
			object.setCurrentRegion(region);
		}
		else
		{
			currentRegion.removeObject(object, true);
			region.addObject(object);
			object.setCurrentRegion(region);

			final GArray<L2WorldRegion> oldNeighbors = currentRegion.getNeighbors();
			final GArray<L2WorldRegion> newNeighbors = region.getNeighbors();

			for(final L2WorldRegion neighbor : oldNeighbors)
			{
				boolean flag = true;
				for(final L2WorldRegion newneighbor : newNeighbors)
					if(newneighbor != null && newneighbor.equals(neighbor))
					{
						flag = false;
						break;
					}
				if(flag)
					neighbor.removeFromPlayers(object, true);
			}

			for(final L2WorldRegion neighbor : newNeighbors)
			{
				boolean flag = true;
				for(final L2WorldRegion oldneighbor : oldNeighbors)
					if(oldneighbor != null && oldneighbor.equals(neighbor))
					{
						flag = false;
						break;
					}
				if(flag)
					neighbor.addToPlayers(object, dropper);
			}
		}

		if(object.isNpc())
			((L2NpcInstance) object).setShowSpawnAnimation(0);
	}

	public static void removeVisibleObject(final L2Object object)
	{
		if(object == null || object.isVisible() || object.inObserverMode())
			return;
		if(object.getCurrentRegion() != null)
		{
			object.getCurrentRegion().removeObject(object, false);
			if(object.getCurrentRegion() != null)
				for(final L2WorldRegion neighbor : object.getCurrentRegion().getNeighbors())
					neighbor.removeFromPlayers(object, false);
			object.setCurrentRegion(null);
		}
	}

	public static boolean validCoords(final int x, final int y)
	{
		return x > MAP_MIN_X && x < MAP_MAX_X && y > MAP_MIN_Y && y < MAP_MAX_Y;
	}

	public static synchronized void deleteVisibleNpcSpawns()
	{
		RaidBossSpawnManager.getInstance().cleanUp();
		DayNightSpawnManager.getInstance().cleanUp();
		CatacombSpawnManager.getInstance().cleanUp();

		_log.info("Deleting all visible NPC's...");

		for(int i = 1; i <= REGIONS_X; i++)
			for(int j = 1; j <= REGIONS_Y; j++)
				if(_worldRegions[i][j].length > 1)
				{
					for(int k = 0; k < REGIONS_Z; k++)
						if(_worldRegions[i][j][k] != null)
							_worldRegions[i][j][k].deleteVisibleNpcSpawns();
				}
				else if(_worldRegions[i][j][0] != null)
					_worldRegions[i][j][0].deleteVisibleNpcSpawns();

		for(int i = 1; i <= SMALL_REGIONS_X; i++)
			for(int j = 1; j <= SMALL_REGIONS_Y; j++)
				if(_smallWorldRegions[i][j] != null)
					_smallWorldRegions[i][j].deleteVisibleNpcSpawns();

		_log.info("All visible NPC's deleted.");
	}

	public static L2Object getAroundObjectById(final L2Object object, final int id)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return null;

		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();
		for(final L2WorldRegion region : neighbors)
			for(final L2Object o : region.getObjectsList(new GArray<L2Object>(size), object.getObjectId(), object.getReflection()))
				if(o != null && o.getObjectId() == id)
					return o;
		return null;
	}

	public static GArray<L2Object> getAroundObjects(final L2Object object)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2Object> result = new GArray<L2Object>(size);
		for(final L2WorldRegion region : neighbors)
			region.getObjectsList(result, oid, object.getReflection());
		return result;
	}

	public static GArray<L2Object> getAroundObjects(final L2Object object, final int radius, final int height)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2Object> result = new GArray<L2Object>(size);
		for(final L2WorldRegion region : neighbors)
			region.getObjectsList(result, oid, object.getReflection(), object.getX(), object.getY(), object.getZ(), radius * radius, height);
		return result;
	}

	public static GArray<L2Character> getAroundCharacters(final L2Object object)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2Character> result = new GArray<L2Character>(size);
		for(final L2WorldRegion region : neighbors)
			region.getCharactersList(result, oid, object.getReflection().getId());
		return result;
	}

	public static GArray<L2Character> getAroundCharacters(final L2Object object, final int radius, final int height)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2Character> result = new GArray<L2Character>(size);
		for(final L2WorldRegion region : neighbors)
			region.getCharactersList(result, oid, object.getReflection().getId(), object.getX(), object.getY(), object.getZ(), radius * radius, height);
		return result;
	}

	public static GArray<L2NpcInstance> getAroundNpc(final L2Object object)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2NpcInstance> result = new GArray<L2NpcInstance>(size);
		for(final L2WorldRegion region : neighbors)
			region.getNpcsList(result, oid, object.getReflection().getId());
		return result;
	}

	public static GArray<L2NpcInstance> getAroundNpc(final L2Object object, final int radius, final int height)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getObjectsSize();

		final GArray<L2NpcInstance> result = new GArray<L2NpcInstance>(size);
		for(final L2WorldRegion region : neighbors)
			region.getNpcsList(result, oid, object.getReflection().getId(), object.getX(), object.getY(), object.getZ(), radius * radius, height);
		return result;
	}

	public static GArray<L2Playable> getAroundPlayables(final L2Object object)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getPlayersSize();

		final GArray<L2Playable> result = new GArray<L2Playable>(size * 2);
		for(final L2WorldRegion region : neighbors)
			region.getPlayablesList(result, oid, object.getReflection());
		return result;
	}

	public static GArray<L2Playable> getAroundPlayables(final L2Object object, final int radius, final int height)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 0;
		for(final L2WorldRegion region : neighbors)
			size += region.getPlayersSize();

		final GArray<L2Playable> result = new GArray<L2Playable>(size * 2);
		for(final L2WorldRegion region : neighbors)
			region.getPlayablesList(result, oid, object.getReflection(), object.getX(), object.getY(), object.getZ(), radius * radius, height);
		return result;
	}

	public static GArray<L2Player> getAroundPlayers(final L2Object object)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 10;
		for(final L2WorldRegion region : neighbors)
			size += region.getPlayersSize();

		final GArray<L2Player> result = new GArray<L2Player>(size);
		for(final L2WorldRegion region : neighbors)
			region.getPlayersList(result, oid, object.getReflection());
		return result;
	}

	public static GArray<L2Player> getAroundPlayers(final L2Object object, final int radius, final int height)
	{
		final L2WorldRegion currentRegion = object.getCurrentRegion();
		if(currentRegion == null)
			return GArray.emptyCollection();

		final int oid = object.getObjectId();
		final GArray<L2WorldRegion> neighbors = currentRegion.getNeighbors();
		int size = 10;
		for(final L2WorldRegion region : neighbors)
			size += region.getPlayersSize();

		final GArray<L2Player> result = new GArray<L2Player>(size);
		for(final L2WorldRegion region : neighbors)
			region.getPlayersList(result, oid, object.getReflection(), object.getX(), object.getY(), object.getZ(), radius * radius, height);
		return result;
	}

	public static void addTerritory(final L2Territory territory)
	{
		final int xmin = territory.getXmin() / DIV_BY + OFFSET_X;
		final int ymin = territory.getYmin() / DIV_BY + OFFSET_Y;
		final int zmin = territory.getZmin() / DIV_BY_FOR_Z + OFFSET_Z;

		final int xmax = territory.getXmax() / DIV_BY + OFFSET_X;
		final int ymax = territory.getYmax() / DIV_BY + OFFSET_Y;
		final int zmax = territory.getZmax() / DIV_BY_FOR_Z + OFFSET_Z;

		for(int x = xmin; x <= xmax; x++)
			for(int y = ymin; y <= ymax; y++)
				if(validRegion(x, y, 0, false))
					if(_worldRegions[x][y].length > 1)
					{
						for(int z = zmin; z <= zmax; z++)
							if(validRegion(x, y, z, false))
							{
								if(_worldRegions[x][y][z] == null)
									_worldRegions[x][y][z] = new L2WorldRegion(x, y, z, false);
								_worldRegions[x][y][z].addTerritory(territory);
							}
					}
					else
					{
						if(_worldRegions[x][y][0] == null)
							_worldRegions[x][y][0] = new L2WorldRegion(x, y, 0, false);
						_worldRegions[x][y][0].addTerritory(territory);
					}
	}

	public static void removeTerritory(final L2Territory territory)
	{
		final int xmin = territory.getXmin() / DIV_BY + OFFSET_X;
		final int ymin = territory.getYmin() / DIV_BY + OFFSET_Y;
		final int zmin = territory.getZmin() / DIV_BY_FOR_Z + OFFSET_Z;

		final int xmax = territory.getXmax() / DIV_BY + OFFSET_X;
		final int ymax = territory.getYmax() / DIV_BY + OFFSET_Y;
		final int zmax = territory.getZmax() / DIV_BY_FOR_Z + OFFSET_Z;

		for(int x = xmin; x <= xmax; x++)
			for(int y = ymin; y <= ymax; y++)
				if(validRegion(x, y, 0, false))
					if(_worldRegions[x][y].length > 1)
					{
						for(int z = zmin; z <= zmax; z++)
							if(validRegion(x, y, z, false) && _worldRegions[x][y][z] != null)
								_worldRegions[x][y][z].removeTerritory(territory);
					}
					else if(_worldRegions[x][y][0] != null)
						_worldRegions[x][y][0].removeTerritory(territory);
	}

	public static GArray<L2Territory> getTerritories(final int x, final int y, final int z)
	{
		final L2WorldRegion region = L2World.getRegion(x, y, z, false); // Здесь важно именно false
		return region == null ? null : region.getTerritories(x, y, z);
	}

	public static L2Territory getTerritoryByZoneType(final Location loc, final ZoneType type)
	{
		final GArray<L2Territory> territories = getTerritories(loc.x, loc.y, loc.z);
		if(territories != null)
			for(final L2Territory terr : territories)
				if(terr != null && terr.getZone() != null && terr.getZone().getType() == type)
					return terr;
		return null;
	}

	public static boolean isWater(final Location loc)
	{
		return getTerritoryByZoneType(loc, ZoneType.water) != null;
	}

	public static String getOnline()
	{
		return "R:" + (L2ObjectsStorage.getAllPlayersCount() - L2ObjectsStorage.getAllOfflineCount()) + "/Off:" + L2ObjectsStorage.getAllOfflineCount();
	}

	public static L2WorldRegion[][][] getRegions()
	{
		return _worldRegions;
	}
}
