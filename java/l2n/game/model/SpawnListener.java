package l2n.game.model;

import l2n.game.model.instances.L2NpcInstance;

public interface SpawnListener
{
	public void npcSpawned(L2NpcInstance npc);

	public void npcDeSpawned(L2NpcInstance npc);
}
