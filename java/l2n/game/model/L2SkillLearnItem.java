package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.*;

public class L2SkillLearnItem
{
	private final int _itemId;
	// give skills
	private GArray<int[]> _giveSkills;
	private static final GArray<int[]> _EMPTY_SKILLS = new GArray<int[]>(0);

	private ClassId[] _playerClassIds;
	private int _playerMinLevel;
	private ClassLevel[] _playerClassLevel;
	private ClassType[] _playerClassType;
	private Race[] _playerRace;
	private int _playerKnownSkillId;
	private int _playerKnownSkillLv;
	private boolean _playerKnownSkillEx;

	private int playerSpReq;

	public int getPlayerSpReq()
	{
		return playerSpReq;
	}

	public void setPlayerSpReq(final int _playerSpReq)
	{
		playerSpReq = _playerSpReq;
	}

	public L2SkillLearnItem(final int itemId)
	{
		_itemId = itemId;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public void addGiveSkill(final int skillId, final int skillLv)
	{
		if(_giveSkills == null)
			_giveSkills = new GArray<int[]>();
		_giveSkills.add(new int[] { skillId, skillLv });
	}

	public boolean checkConditions(final L2Player activeChar, final boolean alt)
	{
		if(_playerClassIds != null && _playerClassIds.length > 0)
		{
			boolean _checked = false;
			for(final ClassId cid : _playerClassIds)
				if(activeChar.getClassId().getId() == cid.getId() || activeChar.getClassId().childOf(cid))
				{
					_checked = true;
					break;
				}
			if(!_checked)
				return false;
		}

		if(!alt && _playerMinLevel > activeChar.getLevel())
			return false;
		else if(alt && _playerMinLevel > activeChar.getLevel() + 10)
			return false;

		if(_playerClassLevel != null && _playerClassLevel.length > 0)
		{
			boolean _checked = false;
			final ClassLevel playerClass = PlayerClass.values()[activeChar.getClassId().getId()].getLevel();
			for(final ClassLevel cid : _playerClassLevel)
				if(playerClass.ordinal() == cid.ordinal())
				{
					_checked = true;
					break;
				}
			if(!_checked)
				return false;
		}
		if(_playerClassType != null && _playerClassType.length > 0)
		{
			boolean _checked = false;
			final ClassType playerClass = PlayerClass.values()[activeChar.getClassId().getId()].getType();
			for(final ClassType cid : _playerClassType)
				if(playerClass.ordinal() == cid.ordinal())
				{
					_checked = true;
					break;
				}
			if(!_checked)
				return false;
		}
		if(_playerRace != null && _playerRace.length > 0)
		{
			boolean _checked = false;
			final Race playerClass = activeChar.getRace();
			for(final Race cid : _playerRace)
				if(playerClass.ordinal() == cid.ordinal())
				{
					_checked = true;
					break;
				}
			if(!_checked)
				return false;
		}
		if(_playerKnownSkillId > 0 && _playerKnownSkillLv > 0)
		{
			final L2Skill knownSkill = activeChar.getKnownSkill(_playerKnownSkillId);
			if(knownSkill == null)
				return false;
			if(_playerKnownSkillEx && knownSkill.getLevel() != _playerKnownSkillLv)
				return false;
			if(!_playerKnownSkillEx && knownSkill.getLevel() < _playerKnownSkillLv)
				return false;
		}
		if(!alt && playerSpReq > activeChar.getSp())
			return false;
		return true;
	}

	public GArray<int[]> getGiveSkills()
	{
		return _giveSkills == null ? _EMPTY_SKILLS : _giveSkills;
	}

	public void setPlayerClassIDs(final ClassId[] classIds)
	{
		_playerClassIds = classIds;
	}

	public void setPlayerMinLevel(final int val)
	{
		_playerMinLevel = val;
	}

	public int getPlayerMinLevel()
	{
		return _playerMinLevel;
	}

	public void setPlayerClassLevel(final ClassLevel[] array)
	{
		_playerClassLevel = array;
	}

	public void setPlayerClassType(final ClassType[] array)
	{
		_playerClassType = array;
	}

	public void setPlayerRace(final Race[] array)
	{
		_playerRace = array;
	}

	public void setPlayerKnownSkill(final int id, final int lv, final boolean ex)
	{
		_playerKnownSkillId = id;
		_playerKnownSkillLv = lv;
		_playerKnownSkillEx = ex;
	}
}
