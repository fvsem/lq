package l2n.game.model.actor;

import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Events;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.L2SummonAI;
import l2n.game.cache.Msg;
import l2n.game.model.L2Object;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.recorder.SummonStatsChangeRecorder;
import l2n.game.model.base.Experience;
import l2n.game.model.entity.Duel;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.PetInventory;
import l2n.game.network.serverpackets.*;
import l2n.game.taskmanager.DecayTaskManager;
import l2n.game.taskmanager.DecayTaskManager.DecayTask;
import l2n.game.templates.L2NpcTemplate;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

public abstract class L2Summon extends L2Playable
{
	protected long _exp = 0;
	protected int _sp = 0;
	private int _attackRange = 36; // Melee range
	private boolean _follow = true;
	private int _maxLoad;
	private boolean _posessed = false; // betrayed?
	private int _showSpawnAnimation = 2;

	private boolean _ssCharged = false;
	private int _spsCharged = 0;

	private Future<DecayTask> _decayTask;

	private IHardReference<L2Player> ownerRef = HardReferences.emptyRef();

	public static final int SIEGE_GOLEM_ID = 14737;
	public static final int SIEGE_CANNON_ID = 14768;
	public static final int SWOOP_CANNON_ID = 14839;

	private static final int SUMMON_DISAPPEAR_RANGE = 2500;

	// для петов устанавливается при одевании клыков, для суммонов скилом
	private WeaponType _weaponType = WeaponType.NONE;

	public L2Summon(final int objectId, final L2NpcTemplate template, final L2Player owner)
	{
		super(objectId, template, owner);
		ownerRef = owner.getRef();

		final GArray<L2ItemInstance> items = new GArray<L2ItemInstance>();
		for(final L2ItemInstance item : owner.getInventory().getPaperdollItems())
			if(item != null && !items.contains(item)) // проверяем на дубли, т.к. один предмет может быть в двух слотах сразу
				items.add(item);

		setXYZInvisible(owner.getX() + Rnd.get(-100, 100), owner.getY() + Rnd.get(-100, 100), owner.getZ());
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();
		onSpawn();
	}

	@Override
	public void onSpawn()
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		final L2Party party = owner.getParty();
		if(party != null)
			party.broadcastToPartyMembers(owner, new ExPartyPetWindowAdd(this));
	}

	@Override
	public L2SummonAI getAI()
	{
		if(_ai == null)
			_ai = new L2SummonAI(this);
		return (L2SummonAI) _ai;
	}

	@Override
	public L2NpcTemplate getTemplate()
	{
		return (L2NpcTemplate) _template;
	}

	@Override
	public boolean isUndead()
	{
		return getTemplate().isUndead();
	}

	// this defines the action buttons, 1 for Summon, 2 for Pets
	public abstract int getSummonType();

	/**
	 * @return Returns the mountable.
	 */
	public boolean isMountable()
	{
		return false;
	}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		final L2Player _owner = getPlayer();
		if(_owner == null)
		{
			player.sendActionFailed();
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}
		if(player.isConfused() || player.isBlocked())
			player.sendActionFailed();
		if(player.getTarget() != this)
		{
			// Set the target of the player
			player.setTarget(this);
			// The color to display in the select window is White
			player.sendPacket(new MyTargetSelected(getObjectId(), 0), makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
		}
		else if(player == _owner)
		player.sendPacket(new PetStatusShow(this), Msg.ActionFail);
		else if(isAutoAttackable(player))
		{
			// Player with lvl < 21 can't attack a cursed weapon holder
			// And a cursed weapon holder can't attack players with lvl < 21
			if(_owner.isCursedWeaponEquipped() && player.getLevel() < 21 || player.isCursedWeaponEquipped() && _owner.getLevel() < 21)
				player.sendActionFailed();
			else
				player.getAI().Attack(this, false, shift);
		}
		else if(player != _owner)
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, this, Config.FOLLOW_RANGE);
		else
			sendActionFailed();
	}

	public long getExpForThisLevel()
	{
		if(getLevel() >= Experience.LEVEL.length)
			return 0;
		return Experience.LEVEL[getLevel()];
	}

	public long getExpForNextLevel()
	{
		if(getLevel() + 1 >= Experience.LEVEL.length)
			return 0;
		return Experience.LEVEL[getLevel() + 1];
	}

	@Override
	public int getNpcId()
	{
		return getTemplate().npcId;
	}

	public final long getExp()
	{
		return _exp;
	}

	public final void setExp(final long exp)
	{
		_exp = exp;
	}

	public final int getSp()
	{
		return _sp;
	}

	public void setSp(final int sp)
	{
		_sp = sp;
	}

	public int getMaxLoad()
	{
		return _maxLoad;
	}

	public void setMaxLoad(final int maxLoad)
	{
		_maxLoad = maxLoad;
	}

	public abstract int getCurrentFed();

	public abstract int getMaxFed();

	@Override
	public void doDie(L2Character killer)
	{
		super.doDie(killer);

		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		if(killer == null || killer == owner || killer.getObjectId() == _objectId || isInZoneBattle() || killer.isInZoneBattle())
			return;

		if(killer.isSummon0())
			killer = killer.getPlayer();

		if(killer == null)
			return;

		if(killer.isPlayer())
		{
			final L2Player pk = (L2Player) killer;

			if(isInZone(ZoneType.Siege))
				return;

			if(owner.getPvpFlag() > 0 || owner.atMutualWarWith(pk))
				pk.setPvpKills(pk.getPvpKills() + 1);
			else if((getDuel() == null || getDuel() != pk.getDuel()) && getKarma() <= 0)
			{
				final int pkCountMulti = Math.max(pk.getPkKills() / 2, 1);
				pk.increaseKarma(Config.KARMA_MIN_KARMA * pkCountMulti);
			}

			// Send a Server->Client UserInfo packet to attacker with its PvP Kills Counter
			pk.sendChanges();
		}
	}

	protected void startDecay(final long delay)
	{
		stopDecay();
		_decayTask = DecayTaskManager.getInstance().addDecayTask(this, delay);
	}

	protected void stopDecay()
	{
		if(_decayTask != null)
		{
			_decayTask.cancel(false);
			_decayTask = null;
		}
	}

	public void endDecayTask()
	{
		stopDecay();
		onDecay();
	}

	@Override
	public void onDecay()
	{
		deleteMe();
	}

	@Override
	public void broadcastStatusUpdate()
	{
		if(!needStatusUpdate())
			return;

		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		final StatusUpdate su = makeStatusUpdate(StatusUpdate.MAX_HP, StatusUpdate.CUR_HP);
		broadcastToStatusListeners(su);
		sendStatusUpdate();

		final L2Party party = owner.getParty();
		if(party != null)
			party.broadcastToPartyMembers(owner, new ExPartyPetWindowUpdate(this));
	}

	public void sendStatusUpdate()
	{
		final L2Player owner = getPlayer();
		if(owner != null)
			owner.sendPacket(new PetStatusUpdate(this));
	}

	@Override
	public void deleteMe()
	{
		final L2Player owner = getPlayer();
		if(owner != null)
		{
			final L2Party party = owner.getParty();
			if(party != null)
				party.broadcastToPartyMembers(owner, new ExPartyPetWindowDelete(this));
			owner.sendPacket(new PetDelete(getObjectId(), 2));
			owner.setPet(null);
		}

		ownerRef = HardReferences.emptyRef();
		setEffectList(null);
		super.deleteMe();
	}

	public void unSummon()
	{
		deleteMe();
	}

	public int getAttackRange()
	{
		return _attackRange;
	}

	public void setAttackRange(int range)
	{
		if(range < 36)
			range = 36;
		_attackRange = range;
	}

	@Override
	public void setFollowStatus(final boolean state, final boolean changeIntention)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		_follow = state;
		if(changeIntention)
			if(_follow)
				getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, owner, Config.FOLLOW_RANGE);
			else
				getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
	}

	public boolean isFollow()
	{
		return _follow;
	}

	private Future<?> _updateEffectIconsTask;

	private class UpdateEffectIcons implements Runnable
	{
		@Override
		public void run()
		{
			updateEffectIconsImpl();
			_updateEffectIconsTask = null;
		}
	}

	@Override
	public void updateEffectIcons()
	{
		if(Config.USER_INFO_INTERVAL == 0)
		{
			if(_updateEffectIconsTask != null)
			{
				_updateEffectIconsTask.cancel(false);
				_updateEffectIconsTask = null;
			}
			updateEffectIconsImpl();
			return;
		}

		if(_updateEffectIconsTask != null)
			return;

		_updateEffectIconsTask = L2GameThreadPools.getInstance().scheduleGeneral(new UpdateEffectIcons(), Config.USER_INFO_INTERVAL);
	}

	public void updateEffectIconsImpl()
	{
		final L2Player owner = getPlayer();
		final PartySpelled ps = new PartySpelled(this, true);
		final L2Party party = owner.getParty();
		if(party != null)
			party.broadcastToPartyMembers(ps);
		else
			owner.sendPacket(ps);
	}

	/**
	 * @return Returns the showSpawnAnimation.
	 */
	public int getShowSpawnAnimation()
	{
		return _showSpawnAnimation;
	}
	public void setShowSpawnAnimation(final int showSpawnAnimation)
	{
		_showSpawnAnimation = showSpawnAnimation;
	}
	public int getControlItemId()
	{
		return 0;
	}

	public long getWearedMask()
	{
		return _weaponType.mask();
	}

	public void setWearedWeaponType(final WeaponType type)
	{
		// для петов устанавливается при одевании клыков
		if(type != null)
			_weaponType = type;
		else
		// для суммонов скилом
		{
			final int level = getSkillLevel(4415, 0);
			if(level > 0)
				switch (level)
				{
					case 1:
					case 2:
						_weaponType = WeaponType.NONE;
						break;
					case 3:
					case 4:
						_weaponType = WeaponType.SWORD;
						break;
					case 5:
					case 6:
						_weaponType = WeaponType.BLUNT;
						break;
					case 7:
					case 8:
						_weaponType = WeaponType.DAGGER;
						break;
					case 9:
					case 10:
						_weaponType = WeaponType.BOW;
						break;
					case 11:
					case 12:
						_weaponType = WeaponType.POLE;
						break;
					case 13:
					case 14:
						_weaponType = WeaponType.BIGSWORD;
						break;
					case 15:
					case 16:
						_weaponType = WeaponType.BIGBLUNT;
						break;
					case 17:
					case 18:
						_weaponType = WeaponType.DUALFIST;
						break;
					case 19:
					case 20:
						_weaponType = WeaponType.DUAL;
						break;
					default:
						_weaponType = WeaponType.NONE;
						break;
				}
		}
	}

	public L2Weapon getActiveWeapon()
	{
		return null;
	}

	@Override
	public PetInventory getInventory()
	{
		return null;
	}

	@Override
	public void doPickupItem(final L2Object object)
	{}

	public void giveAllToOwner()
	{}

	/**
	 * Return null.<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getActiveWeaponItem()
	{
		return null;
	}

	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		return null;
	}

	@Override
	public boolean unChargeShots(final boolean spirit)
	{
		if(getPlayer() == null)
			return false;

		if(spirit && _spsCharged != 0)
		{
			_spsCharged = 0;
			getPlayer().rechargeAutoSoulShot();
			return true;
		}

		if(_ssCharged)
		{
			_ssCharged = false;
			getPlayer().rechargeAutoSoulShot();
			return true;
		}

		getPlayer().rechargeAutoSoulShot();
		return false;
	}

	@Override
	public boolean getChargedSoulShot()
	{
		return _ssCharged;
	}

	@Override
	public int getChargedSpiritShot()
	{
		return _spsCharged;
	}

	public void chargeSoulShot()
	{
		_ssCharged = true;
	}

	public void chargeSpiritShot(final int state)
	{
		_spsCharged = state;
	}

	public int getSoulshotConsumeCount()
	{
		return getLevel() / 27 + 1;
	}

	public int getSpiritshotConsumeCount()
	{
		return getLevel() / 58 + 1;
	}

	@Override
	public void doAttack(final L2Character target)
	{
		super.doAttack(target);
	}

	public void doAttack(final L2Object target, final boolean forceUse, final boolean dontMove)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		if(target == null || this == target || isDead() || !target.isCharacter())
		{
			owner.sendActionFailed();
			return;
		}

		if(owner.isInOlympiadMode() && !owner.isOlympiadCompStart())
		{
			owner.sendActionFailed();
			return;
		}

		// Sin Eater
		if(getNpcId() == 12564)
			return;

		// проверка уровня - нельзя управлять петами на 20 уровней выше уровня чара
		if(owner.getLevel() + 20 <= getLevel())
		{
			owner.sendPacket(Msg.THE_PET_IS_TOO_HIGH_LEVEL_TO_CONTROL);
			return;
		}

		// атака петом хозяина - аналогично команде следования за хозияном
		if(target.getObjectId() == owner.getObjectId())
		{
			if(!isMovementDisabled())
			{
				setFollowTarget(owner);
				setFollowStatus(true, true);
			}
			return;
		}

		if(!target.isMonster() && (isInZonePeace() || ((L2Character) target).isInZonePeace()))
		{
			if(forceUse && !Config.ALT_ALL_CAN_KILL_IN_PEACE)
				// force pet attack used in peace zone
				owner.sendPacket(Msg.YOU_CANNOT_ATTACK_THE_TARGET_IN_THE_PEACE_ZONE);
			else if(!forceUse && !Config.ALT_ALL_CAN_KILL_IN_PEACE)
				// pet attack used in peace zone - pet follow target
				if(!isMovementDisabled())
					if(target.isPlayer())
					{
						setFollowStatus(true, true);
						setFollowTarget((L2Character) target);
					}
					else
					// just move to target
					{
						setFollowStatus(false, true);
						moveToLocation(target.getLoc(), 100, true);
					}
			return;
		}

		if(!target.isDoor() && (getNpcId() == L2Summon.SIEGE_GOLEM_ID || getNpcId() == L2Summon.SIEGE_CANNON_ID || getNpcId() == L2Summon.SWOOP_CANNON_ID))
		{
			owner.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		getAI().Attack(target, forceUse, dontMove);
	}

	@Override
	public void doCast(final L2Skill skill, final L2Character target, final boolean forceUse)
	{
		super.doCast(skill, target, forceUse);
	}

	public boolean isPosessed()
	{
		return _posessed;
	}

	public void setPossessed(final boolean possessed)
	{
		_posessed = possessed;
	}

	public boolean isInRange()
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return false;

		return Util.checkIfInRange(SUMMON_DISAPPEAR_RANGE, owner.getX(), owner.getY(), owner.getZ(), getX(), getY(), getZ(), false);
	}

	public void teleportToOwner()
	{
		// на всякий случай, хотя всё равно саммон удаляется при смерти
		if(isDead() && !isPet())
			return;

		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		setXYZ(owner.getLoc().x + Rnd.get(-70, 70), owner.getLoc().y + Rnd.get(-70, 70), owner.getLoc().z);
		setReflection(owner.getReflection());

		broadcastCharInfo();
		if(_follow && !isDead())
			getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, owner, Config.FOLLOW_RANGE);
		updateEffectIcons();
	}

	@Override
	public SummonStatsChangeRecorder getStatsRecorder()
	{
		if(_statsRecorder == null)
			synchronized (this)
			{
				if(_statsRecorder == null)
					_statsRecorder = new SummonStatsChangeRecorder(this);
			}

		return (SummonStatsChangeRecorder) _statsRecorder;
	}

	private ScheduledFuture<BroadcastCharInfoTask> _broadcastCharInfoTask;

	private class BroadcastCharInfoTask implements Runnable
	{
		@Override
		public void run()
		{
			broadcastCharInfoImpl();
			_broadcastCharInfoTask = null;
		}
	}

	@Override
	public void broadcastCharInfo()
	{
		if(_broadcastCharInfoTask != null)
			return;
		_broadcastCharInfoTask = L2GameThreadPools.getInstance().scheduleAi(new BroadcastCharInfoTask(), Config.BROADCAST_CHAR_INFO_INTERVAL, false);
	}

	public void broadcastCharInfoImpl()
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		for(final L2Player player : L2World.getAroundPlayers(this))
			if(player != null)
				if(player == owner)
					player.sendPacket(new PetInfo(this, getShowSpawnAnimation()));
				else
					player.sendPacket(new NpcInfo(this, player, getShowSpawnAnimation()));
	}

	private Future<?> _petInfoTask;

	private class PetInfoTask implements Runnable
	{
		@Override
		public void run()
		{
			sendPetInfoImpl();
			_petInfoTask = null;
		}
	}

	private void sendPetInfoImpl()
	{
		final L2Player owner = getPlayer();
		owner.sendPacket(new PetInfo(this));
	}

	public void sendPetInfo()
	{
		if(Config.USER_INFO_INTERVAL == 0)
		{
			if(_petInfoTask != null)
			{
				_petInfoTask.cancel(false);
				_petInfoTask = null;
			}
			sendPetInfoImpl();
			return;
		}

		if(_petInfoTask != null)
			return;

		_petInfoTask = L2GameThreadPools.getInstance().scheduleAi(new PetInfoTask(), Config.USER_INFO_INTERVAL, false);
	}

	@Override
	public void startPvPFlag(final L2Character target)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;
		owner.startPvPFlag(target);
	}

	@Override
	public int getPvpFlag()
	{
		final L2Player owner = getPlayer();
		return owner == null ? 0 : owner.getPvpFlag();
	}

	@Override
	public int getKarma()
	{
		final L2Player owner = getPlayer();
		return owner == null ? 0 : owner.getKarma();
	}

	@Override
	public Duel getDuel()
	{
		final L2Player owner = getPlayer();
		return owner == null ? null : owner.getDuel();
	}

	@Override
	public int getTeam()
	{
		final L2Player owner = getPlayer();
		return owner == null ? 0 : owner.getTeam();
	}

	public boolean isSiegeWeapon()
	{
		return getNpcId() == SIEGE_GOLEM_ID || getNpcId() == SIEGE_CANNON_ID || getNpcId() == SWOOP_CANNON_ID;
	}

	@Override
	public L2Player getPlayer()
	{
		return ownerRef.get();
	}

	@Override
	public double getLevelMod()
	{
		return (89. + getLevel()) / 100.0;
	}

	public abstract void displayHitMessage(L2Character target, int damage, boolean crit, boolean miss);

	public abstract float getExpPenalty();

	public boolean isHungry()
	{
		return false;
	}

	@Override
	public boolean isSummon0()
	{
		return true;
	}
}
