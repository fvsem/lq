package l2n.game.model.actor;

import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectProcedures.CheckAggresionProc;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.L2World;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.entity.Duel;
import l2n.game.model.entity.Duel.DuelState;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.Inventory;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.scripts.npc.PailakaHelpersInstance;
import l2n.game.skills.EffectType;
import l2n.game.skills.Stats;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2CharTemplate;
import l2n.game.templates.L2EtcItem;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Rnd;

import java.util.Map.Entry;

import static l2n.game.model.L2Zone.ZoneType.Siege;
import static l2n.game.model.L2Zone.ZoneType.peace_zone;

public abstract class L2Playable extends L2Character
{
	private byte _isSilentMoving;
	private long _checkAggroTimestamp = 0;

	private boolean _protectionBlessing = false;

	private GCSArray<QuestState> _notifyQuestOfDeathList;
	private GCSArray<QuestState> _notifyQuestOfPlayerKillList;

	public L2Playable(final int objectId, final L2CharTemplate template, final L2Player owner)
	{
		super(objectId, template);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IHardReference<? extends L2Playable> getRef()
	{
		return (IHardReference<? extends L2Playable>) super.getRef();
	}

	public abstract Inventory getInventory();

	@Override
	public boolean checkPvP(final L2Character target, final L2Skill skill)
	{
		final L2Player player = getPlayer();

		if(isDead() || target == null || player == null || target == this || target == player || target == player.getPet() || player.getKarma() > 0)
			return false;

		if(skill != null)
		{
			if(skill.isAltUse())
				return false;
			if(skill.getSkillType().equals(SkillType.BEAST_FEED))
				return false;
			if(skill.getTargetType() == SkillTargetType.TARGET_UNLOCKABLE)
				return false;
			if(skill.getTargetType() == SkillTargetType.TARGET_CHEST)
				return false;
		}

		// Проверка на дуэли... Мэмбэры одной дуэли не флагаются
		if(getDuel() != null && getDuel() == target.getDuel())
			return false;

		if(isInZone(peace_zone) || target.isInZone(peace_zone) || isInZoneBattle() || target.isInZoneBattle())
			return false;
		if(isInZone(Siege) && target.isInZone(Siege))
			return false;
		if(skill == null || skill.isOffensive())
		{
			if(target.getKarma() > 0)
				return false;
			else if(target.isPlayable())
				return true;
		}
		else if(target.getPvpFlag() > 0 || target.getKarma() > 0 || target.isMonster())
			return true;

		return false;
	}

	public boolean checkAttack(final L2Character target)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return false;

		if(target == null || target.isDead())
		{
			player.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		if(!isInRange(target, 2000))
		{
			player.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
			return false;
		}

		if(target.isDoor() && !((L2DoorInstance) target).isAttackable(this))
		{
			player.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		if(player.inObserverMode())
		{
			player.sendMessage(new CustomMessage("l2n.game.model.L2Playable.OutOfControl.ObserverNoAttack", player));
			return false;
		}

		if(target.paralizeOnAttack(player))
		{
			if(Config.PARALIZE_ON_RAID_DIFF)
				paralizeMe(target);
			return false;
		}

		if(!GeoEngine.canSeeTarget(this, target, false) || getReflection() != target.getReflection())
		{
			player.sendPacket(Msg.CANNOT_SEE_TARGET);
			return false;
		}

		if(target.isPlayable())
		{
			// Нельзя атаковать того, кто находится на арене, если ты сам не на арене
			if(isInZoneBattle() != target.isInZoneBattle())
			{
				player.sendPacket(Msg.INCORRECT_TARGET);
				return false;
			}

			// Если цель либо атакующий находится в мирной зоне - атаковать нельзя
			if((isInZonePeace() || target.isInZonePeace()) && !player.getPlayerAccess().PeaceAttack)
				if(Config.ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE && player.getKarma() == 0 && target.getKarma() > 0)
					return true;
				else
				{
					player.sendPacket(Msg.YOU_CANNOT_ATTACK_THE_TARGET_IN_THE_PEACE_ZONE);
					return false;
				}

			if(player.isInOlympiadMode() && !player.isOlympiadCompStart())
				return false;
		}

		return true;
	}

	@Override
	public void doAttack(final L2Character target)
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;

		if(isAttackingNow() || isAttackMuted())
		{
			player.sendActionFailed();
			return;
		}

		if(player.inObserverMode())
		{
			player.sendMessage(new CustomMessage("l2n.game.model.L2Playable.OutOfControl.ObserverNoAttack", player));
			return;
		}

		if(!checkAttack(target))
		{
			getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
			player.sendActionFailed();
			return;
		}

		// Прерывать дуэли если цель не дуэлянт
		if(getDuel() != null)
			if(target.getDuel() != getDuel())
				getDuel().setDuelState(player.getStoredId(), Duel.DuelState.Interrupted);
			else if(getDuel().getDuelState(player.getStoredId()) == Duel.DuelState.Interrupted)
			{
				player.sendPacket(Msg.INCORRECT_TARGET);
				return;
			}

		// Get the active weapon item corresponding to the active weapon instance (always equipped in the right hand)
		final L2Weapon weaponItem = getActiveWeaponItem();

		if(weaponItem != null && (weaponItem.getItemType() == WeaponType.BOW || weaponItem.getItemType() == WeaponType.CROSSBOW))
		{
			double bowMpConsume = weaponItem.getMpConsume();
			if(bowMpConsume > 0)
			{
				// cheap shot SA
				final double chance = calcStat(Stats.MP_USE_BOW_CHANCE, 0., null, null);
				if(chance > 0 && Rnd.chance(chance))
					bowMpConsume = calcStat(Stats.MP_USE_BOW, bowMpConsume, null, null);

				if(_currentMp < bowMpConsume)
				{
					getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
					player.sendPacket(Msg.NOT_ENOUGH_MP);
					player.sendActionFailed();
					return;
				}

				reduceCurrentMp(bowMpConsume, null);
			}

			if(!player.checkAndEquipArrows())
			{
				getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
				player.sendPacket(player.getActiveWeaponInstance().getItemType() == WeaponType.BOW ? Msg.NOT_ENOUGH_ARROWS : Msg.NOT_ENOUGH_BOLTS);
				player.sendActionFailed();
				return;
			}
		}

		super.doAttack(target);
	}

	public void addNotifyQuestOfDeath(final QuestState qs)
	{
		if(qs == null || _notifyQuestOfDeathList != null && _notifyQuestOfDeathList.contains(qs))
			return;
		if(_notifyQuestOfDeathList == null)
			_notifyQuestOfDeathList = new GCSArray<QuestState>();
		_notifyQuestOfDeathList.add(qs);
	}

	public void addNotifyOfPlayerKill(final QuestState qs)
	{
		if(qs == null || _notifyQuestOfPlayerKillList != null && _notifyQuestOfPlayerKillList.contains(qs))
			return;
		if(_notifyQuestOfPlayerKillList == null)
			_notifyQuestOfPlayerKillList = new GCSArray<QuestState>();
		_notifyQuestOfPlayerKillList.add(qs);
	}

	public void removeNotifyOfPlayerKill(final QuestState qs)
	{
		if(qs == null || _notifyQuestOfPlayerKillList == null)
			return;
		_notifyQuestOfPlayerKillList.remove(qs);
		if(_notifyQuestOfPlayerKillList.isEmpty())
			_notifyQuestOfPlayerKillList = null;
	}

	public GCSArray<QuestState> getNotifyOfPlayerKillList()
	{
		return _notifyQuestOfPlayerKillList;
	}

	@Override
	public void doDie(final L2Character killer)
	{
		super.doDie(killer);

		// Уведомление в квестах метода "onPlayerKill"
		if(killer != null)
		{
			final L2Player pk = killer.getPlayer();
			final L2Player player = getPlayer();

			if(pk != null && player != null)
			{
				final L2Party party = pk.getParty();
				if(party == null)
				{
					final GCSArray<QuestState> killList = pk.getNotifyOfPlayerKillList();
					if(killList != null)
						for(final QuestState qs : killList)
							qs.getQuest().notifyPlayerKill(player, qs);
				}
				else
					for(final L2Player member : party.getPartyMembers())
						if(member != null && member.isInRange(pk, 2000))
						{
							final GCSArray<QuestState> killList = member.getNotifyOfPlayerKillList();
							if(killList != null)
								for(final QuestState qs : killList)
									qs.getQuest().notifyPlayerKill(player, qs);
						}
			}
		}

		// Уведомление в квестах метода "onDeath"
		if(_notifyQuestOfDeathList != null)
		{
			for(final QuestState qs : _notifyQuestOfDeathList)
				qs.getQuest().notifyDeath(killer, this, qs);
			_notifyQuestOfDeathList = null;
		}
	}

	/** Return the MAtk Speed (base+modifier) of the L2Character in function of the Armour Expertise Penalty. */
	@Override
	public int getMAtkSpd()
	{
		int val = (int) (calcStat(Stats.MAGIC_ATTACK_SPEED, _template.baseMAtkSpd, null, null));
		if(Config.LIM_MATK_SPD > 0 && val > Config.LIM_MATK_SPD)
			val = Config.LIM_MATK_SPD;
		return val;
	}

	/** Return the PAtk Speed (base+modifier) of the L2Character in function of the Armour Expertise Penalty */
	@Override
	public int getPAtkSpd()
	{
		int val = Math.max((int) (calcStat(Stats.POWER_ATTACK_SPEED, calcStat(Stats.ATK_BASE, _template.basePAtkSpd, null, null), null, null)), 1);
		if(Config.LIM_PATK_SPD > 0)
			val = Math.min(val, Config.LIM_PATK_SPD);
		return val;
	}

	@Override
	public int getPAtk(final L2Character target)
	{
		final double init = getActiveWeaponInstance() == null ? _template.basePAtk : 0;
		int val = (int) calcStat(Stats.POWER_ATTACK, init, target, null);
		if(Config.LIM_PATK > 0 && val > Config.LIM_PATK)
			val = Config.LIM_PATK;
		return val;
	}

	@Override
	public int getMAtk(final L2Character target, final L2Skill skill)
	{
		if(skill != null && skill.getMatak() > 0)
			return skill.getMatak();

		final double init = getActiveWeaponInstance() == null ? _template.baseMAtk : 0;
		int val = (int) calcStat(Stats.MAGIC_ATTACK, init, target, skill);
		if(Config.LIM_MATK > 0 && val > Config.LIM_MATK)
			val = Config.LIM_MATK;
		return val;
	}

	private boolean checkTarget(final L2Character attacker, final boolean force)
	{
		final L2Player player = getPlayer();
		if(attacker == null || player == null || attacker == this || attacker == player && !isSummon() || isDead() || attacker.isAlikeDead())
			return false;

		if(!GeoEngine.canSeeTarget(attacker, this, false) || getReflection() != attacker.getReflection() || isInvisible())
			return false;

		if(isInVehicle())
			return false;

		final L2Player pcAttacker = attacker.getPlayer();
		final L2Clan clan1 = player.getClan();
		if(pcAttacker != null)
		{
			if(pcAttacker.isInVehicle())
				return false;

			// Только враг и только если он еше не проиграл.
			final Duel duel1 = player.getDuel();
			final Duel duel2 = pcAttacker.getDuel();
			if(player != pcAttacker && duel1 != null && duel1 == duel2)
			{
				if(duel1.getTeamForPlayer(pcAttacker) == duel1.getTeamForPlayer(player))
					return false;
				if(duel1.getDuelState(player.getStoredId()) != DuelState.Fighting)
					return false;
				if(duel1.getDuelState(pcAttacker.getStoredId()) != DuelState.Fighting)
					return false;
				return true;
			}

			if(!force && duel1 != null && duel1 != duel2)
				return false;

			if(player.isInZone(ZoneType.epic) != pcAttacker.isInZone(ZoneType.epic))
				return false;

			// На всякий случай
			if((player.isInOlympiadMode() || pcAttacker.isInOlympiadMode()) && player.getOlympiadGameId() != pcAttacker.getOlympiadGameId())
				return false;

			// Бой еще не начался
			if(player.isInOlympiadMode() && !player.isOlympiadCompStart())
				return false;

			// Разрешаем атаку своего самона
			if(player == pcAttacker && isSummon() && force)
				return true;

			// Свою команду атаковать нельзя
			if(player.isInOlympiadMode() && player.isOlympiadCompStart() && player.getOlympiadSide() == pcAttacker.getOlympiadSide())
				return false;

			// Запрет на атаку/баф участником эвента незарегистрированного игрока
			if(pcAttacker.getTeam() > 0 && pcAttacker.isChecksForTeam() && player.getTeam() == 0)
				return false;

			// Запрет на атаку/баф участника эвента незарегистрированным игроком
			if(player.getTeam() > 0 && player.isChecksForTeam() && pcAttacker.getTeam() == 0)
				return false;

			// Свою команду атаковать нельзя
			if(player.getTeam() > 0 && player.isChecksForTeam() && pcAttacker.getTeam() > 0 && pcAttacker.isChecksForTeam() && player.getTeam() == pcAttacker.getTeam())
				return false;

			if(isInZoneBattle() != attacker.isInZoneBattle() && !player.getPlayerAccess().PeaceAttack)
				return false;

			// Если цель либо атакующий находится в мирной зоне - атаковать нельзя
			if((isInZonePeace() || pcAttacker.isInZonePeace()) && !player.getPlayerAccess().PeaceAttack)
				if(Config.ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE && getKarma() > 0 && pcAttacker.getKarma() == 0)
					return true;
				else
					return false;

			if(!force && player.getParty() != null && player.getParty() == pcAttacker.getParty())
				return false;

			if(isInZoneBattle())
				return true; // Остальные условия на аренах и на олимпиаде проверять не требуется

			if(!force && player.getClanId() != 0 && player.getClanId() == pcAttacker.getClanId())
				return false;

			if(isInZone(ZoneType.Siege) && attacker.isInZone(ZoneType.Siege))
			{
				if(player.getTerritorySiege() > -1 && player.getTerritorySiege() == pcAttacker.getTerritorySiege())
					return false;
				final L2Clan clan2 = pcAttacker.getClan();
				if(clan1 == null || clan2 == null)
					return true;
				if(clan1.getSiege() == null || clan2.getSiege() == null)
					return true;
				if(clan1.getSiege() != clan2.getSiege())
					return true;
				if(clan1.isDefender() && clan2.isDefender())
					return false;
				if(clan1.getSiege().isMidVictory())
					return true;
				return !clan1.isAttacker() || !clan2.isAttacker();
			}

			if(pcAttacker.atMutualWarWith(player))
				return true;

			if(player.getKarma() > 0 || player.getPvpFlag() != 0)
				return true;

			// Защита от развода на флаг с копьем
			if(!force && pcAttacker.getPvpFlag() == 0 && getPvpFlag() != 0 && pcAttacker.getAI() != null && pcAttacker.getAI().getAttackTarget() != this)
				return false;

			return force;
		}

		if(attacker.isSiegeGuard() && clan1 != null && clan1.isDefender() && SiegeManager.getSiege(this, true) == clan1.getSiege())
			return false;
		// для квестов {@link l2n.game.scripts.quests._726_LightWithinTheDarkness} и {@link l2n.game.scripts.quests._727_HopeWithinTheDarkness}
		if(attacker instanceof PailakaHelpersInstance)
			return false;
		if(!force && isInZonePeace()) // Гварды с пикой, будут атаковать только одиночные цели в городе
			return false;

		return true;
	}

	@Override
	public boolean isAttackable(final L2Character attacker)
	{
		return checkTarget(attacker, true);
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return checkTarget(attacker, false);
	}

	@Override
	public int getKarma()
	{
		final L2Player player = getPlayer();
		return player == null ? 0 : player.getKarma();
	}

	@Override
	public void callSkill(final L2Skill skill, final boolean useActionSkills, L2Character... targets)
	{
		final L2Player player = getPlayer();
		if(player == null || skill == null)
			return;

		if(useActionSkills && !skill.isAltUse() && !skill.getSkillType().equals(SkillType.BEAST_FEED))
		{
			boolean add;
			final GArray<L2Character> newTargets = new GArray<L2Character>(targets.length);
			for(final L2Character target : targets)
			{
				add = true;
				if(target == null || target.isInvul() && skill.isOffensive() && skill.getSkillType() != SkillType.STEAL_BUFF // для стила под инвулом
						&& skill.getSkillType() != SkillType.CANCEL && !target.isParalyzed() // для кансела под медузой
						&& !target.isArtefact())
					add = false;

				if(add)
					newTargets.addLastUnsafe(target);

				if(!skill.isOffensive() && skill.isHealAggro())
				{
					if(target.isPlayable() && target != getPet() && !(isSummon0() && target == player))
					{
						final int aggro = skill.getAggroPoints() != 0 ? skill.getAggroPoints() : Math.max(1, (int) skill.getPower());
						for(final Entry<L2NpcInstance, HateInfo> entry : target.getHateList().entrySet())
							if(entry.getKey() != null && !entry.getKey().isDead() && entry.getValue().hate > 0 && entry.getKey().isInRange(this, 2000) && entry.getKey().getAI().getIntention() == CtrlIntention.AI_INTENTION_ATTACK)
							{
								if(!skill.isHandler() && entry.getKey().paralizeOnAttack(player))
								{
									if(Config.PARALIZE_ON_RAID_DIFF)
										paralizeMe(entry.getKey());
									return;
								}
								entry.getKey().getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, skill, this);
								if(GeoEngine.canSeeTarget(entry.getKey(), target, false)) // Моб агрится только если видит цель, которую лечишь/бафаешь.
									entry.getKey().getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, this, aggro);
							}
					}
				}
				else if(target.isNpc())
				{
					// mobs will hate on debuff
					if(target.paralizeOnAttack(player))
					{
						if(Config.PARALIZE_ON_RAID_DIFF)
							paralizeMe(target);
						return;
					}
					target.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, skill, this);
					if(!skill.isAI())
					{
						final int damage = skill.getAggroPoints() != 0 ? skill.getAggroPoints() : 1;
						target.getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, this, damage);
						target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, this, damage);
					}
				}
				// Check for PvP Flagging / Drawing Aggro
				if(checkPvP(target, skill))
					startPvPFlag(target);
			}

			targets = newTargets.toArray(new L2Character[newTargets.size()]);
		}

		super.callSkill(skill, useActionSkills, targets);
	}

	@Override
	public void setXYZ(final int x, final int y, final int z, final boolean MoveTask)
	{
		super.setXYZ(x, y, z, MoveTask);
		final L2Player player = getPlayer();

		if(!MoveTask || player == null || isAlikeDead() || isInvul() || !isVisible() || getCurrentRegion() == null)
			return;

		final long now = System.currentTimeMillis();
		if(now - _checkAggroTimestamp < Config.AGGRO_CHECK_INTERVAL || player.getNonAggroTime() > now)
			return;
		_checkAggroTimestamp = now;

		if(getAI().getIntention() == CtrlIntention.AI_INTENTION_FOLLOW && (!isPlayer() || getFollowTarget() != null && getFollowTarget().getPlayer() != null && !getFollowTarget().getPlayer().isSilentMoving()))
			return;

		final GArray<L2NpcInstance> npcs = L2World.getAroundNpc(this);
		if(!npcs.isEmpty())
			npcs.forEachThread(new CheckAggresionProc(this));
	}

	/**
	 * Оповещает других игроков о поднятии вещи
	 * 
	 * @param item
	 *            предмет который был поднят
	 */
	public void broadcastPickUpMsg(final L2ItemInstance item)
	{
		final L2Player player = getPlayer();
		if(item == null || player == null || player.isInvisible())
			return;
		if(!item.isEquipable() || item.getItem() instanceof L2EtcItem)
			return;

		SystemMessage msg = null;
		final String player_name = player.getName();
		if(item.getEnchantLevel() > 0)
		{
			final int msg_id = isPlayer() ? SystemMessage.ATTENTION_S1_PICKED_UP__S2_S3 : SystemMessage.ATTENTION_S1_PET_PICKED_UP__S2_S3;
			msg = new SystemMessage(msg_id).addString(player_name).addNumber(item.getEnchantLevel()).addItemName(item.getItemId());
		}
		else
		{
			final int msg_id = isPlayer() ? SystemMessage.ATTENTION_S1_PICKED_UP_S2 : SystemMessage.ATTENTION_S1_PET_PICKED_UP_S2;
			msg = new SystemMessage(msg_id).addString(player_name).addItemName(item.getItemId());
		}
		player.broadcastPacket(msg);
	}

	public void paralizeMe(final L2Character effector)
	{
		final L2Skill revengeSkill = SkillTable.FrequentSkill.RAID_CURSE2.getSkill();
		final L2Player player = getPlayer();
		if(player != this)
			revengeSkill.getEffects(effector, this, false, false);
		if(player != null)
			revengeSkill.getEffects(effector, player, false, false);
	}

	// for Newbie Protection Blessing skill, keeps you safe from an attack by a chaotic character >= 10 levels apart from you
	@Override
	public final boolean isProtectionBlessing()
	{
		return _protectionBlessing;
	}

	@Override
	public final void setProtectionBlessing(final boolean value)
	{
		_protectionBlessing = value;
	}

	@Override
	public void startProtectionBlessing()
	{
		setProtectionBlessing(true);
		broadcastCharInfo();
	}

	@Override
	public void stopProtectionBlessing()
	{
		getEffectList().stopEffects(EffectType.ProtectionBlessing);
		setProtectionBlessing(false);
		broadcastCharInfo();
	}

	/**
	 * Set the Silent Moving mode Flag.<BR>
	 * <BR>
	 */
	public void setSilentMoving(final boolean flag)
	{
		if(flag)
			_isSilentMoving++;
		else
			_isSilentMoving--;
	}

	/**
	 * @return True if the Silent Moving mode is active.<BR>
	 * <BR>
	 */
	public boolean isSilentMoving()
	{
		return _isSilentMoving > 0;
	}

	@Override
	public boolean isPlayable()
	{
		return true;
	}
}
