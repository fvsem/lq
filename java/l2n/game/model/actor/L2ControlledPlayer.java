package l2n.game.model.actor;

import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.templates.L2CharTemplate;
import l2n.game.templates.L2Weapon;

import java.util.logging.Logger;

public class L2ControlledPlayer extends L2Playable
{
	private static final Logger _log = Logger.getLogger(L2ControlledPlayer.class.getName());

	private byte _level;

	private int _karma;

	private int _nameColor;

	private int _title_color;

	private int _pkKills;

	private int _pvpKills;

	public L2ControlledPlayer(final int objectId, final L2CharTemplate template, final L2Player owner)
	{
		super(objectId, template, owner);
	}

	@Override
	public Inventory getInventory()
	{
		return null;
	}

	@Override
	public byte getLevel()
	{
		return 0;
	}

	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getActiveWeaponItem()
	{
		return null;
	}

	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		return null;
	}

}
