package l2n.game.model.actor.recorder;

import l2n.game.model.actor.L2Summon;

public class SummonStatsChangeRecorder extends CharStatsChangeRecorder<L2Summon>
{
	public SummonStatsChangeRecorder(final L2Summon actor)
	{
		super(actor);
	}

	@Override
	protected void onSendChanges()
	{
		super.onSendChanges();

		if((_changes & SEND_CHAR_INFO) == SEND_CHAR_INFO)
			_activeChar.sendPetInfo();
		else if((_changes & BROADCAST_CHAR_INFO) == BROADCAST_CHAR_INFO)
			_activeChar.broadcastCharInfo();
	}
}
