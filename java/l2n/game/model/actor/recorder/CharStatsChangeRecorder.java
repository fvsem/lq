package l2n.game.model.actor.recorder;

import l2n.game.model.actor.L2Character;

import java.util.logging.Logger;

public class CharStatsChangeRecorder<T extends L2Character>
{
	protected static final Logger _log = Logger.getLogger("l2n.game.model.actor.recorder.CharStatsChangeRecorder");

	protected static final int BROADCAST_CHAR_INFO = 1 << 0; // Требуется обновить состояние персонажа у окружающих
	protected static final int SEND_CHAR_INFO = 1 << 1; // Требуется обновить состояние только самому персонажу
	protected static final int SEND_STATUS_INFO = 1 << 2; // Требуется обновить статус HP/MP/CP

	protected final T _activeChar;

	protected int _level;

	protected int _accuracy;
	protected int _attackSpeed;
	protected int _castSpeed;
	protected int _criticalHit;
	protected int _evasion;
	protected int _magicAttack;
	protected int _magicDefence;
	protected int _maxHp;
	protected int _maxMp;
	protected int _physicAttack;
	protected int _physicDefence;
	protected int _runSpeed;

	protected int _abnormalEffects;
	protected int _specialEffects;

	protected int _team;
	protected int _changes;

	public CharStatsChangeRecorder(final T actor)
	{
		this._activeChar = actor;
	}

	protected int set(final int flag, final int oldValue, final int newValue)
	{
		if(oldValue != newValue)
			_changes |= flag;
		return newValue;
	}

	protected long set(final int flag, final long oldValue, final long newValue)
	{
		if(oldValue != newValue)
			_changes |= flag;
		return newValue;
	}

	protected String set(final int flag, final String oldValue, final String newValue)
	{
		if(!oldValue.equals(newValue))
			_changes |= flag;
		return newValue;
	}

	protected <E extends Enum<E>> E set(final int flag, final E oldValue, final E newValue)
	{
		if(oldValue != newValue)
			_changes |= flag;
		return newValue;
	}

	protected void refreshStats()
	{
		_accuracy = set(SEND_CHAR_INFO, _accuracy, _activeChar.getAccuracy());
		_attackSpeed = set(BROADCAST_CHAR_INFO, _attackSpeed, _activeChar.getPAtkSpd());
		_castSpeed = set(BROADCAST_CHAR_INFO, _castSpeed, _activeChar.getMAtkSpd());
		_criticalHit = set(SEND_CHAR_INFO, _criticalHit, _activeChar.getCriticalHit(null, null));
		_evasion = set(SEND_CHAR_INFO, _evasion, _activeChar.getEvasionRate(null));
		_runSpeed = set(BROADCAST_CHAR_INFO, _runSpeed, _activeChar.getRunSpeed());

		_physicAttack = set(SEND_CHAR_INFO, _physicAttack, _activeChar.getPAtk(null));
		_physicDefence = set(SEND_CHAR_INFO, _physicDefence, _activeChar.getPDef(null));
		_magicAttack = set(SEND_CHAR_INFO, _magicAttack, _activeChar.getMAtk(null, null));
		_magicDefence = set(SEND_CHAR_INFO, _magicDefence, _activeChar.getMDef(null, null));

		_maxHp = set(SEND_STATUS_INFO, _maxHp, _activeChar.getMaxHp());
		_maxMp = set(SEND_STATUS_INFO, _maxMp, _activeChar.getMaxMp());

		_level = set(SEND_CHAR_INFO, _level, _activeChar.getLevel());

		_abnormalEffects = set(BROADCAST_CHAR_INFO, _abnormalEffects, _activeChar.getAbnormalEffect());
		_specialEffects = set(BROADCAST_CHAR_INFO, _specialEffects, _activeChar.getSpecialEffect());

		_team = set(BROADCAST_CHAR_INFO, _team, _activeChar.getTeam());
	}

	public final void sendChanges()
	{
		refreshStats();
		onSendChanges();
		_changes = 0;
	}

	protected void onSendChanges()
	{
		if((_changes & SEND_STATUS_INFO) == SEND_STATUS_INFO)
			_activeChar.broadcastStatusUpdate();
	}
}
