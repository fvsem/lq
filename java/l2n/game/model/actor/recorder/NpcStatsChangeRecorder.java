package l2n.game.model.actor.recorder;

import l2n.game.model.instances.L2NpcInstance;

public class NpcStatsChangeRecorder extends CharStatsChangeRecorder<L2NpcInstance>
{
	public NpcStatsChangeRecorder(final L2NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onSendChanges()
	{
		super.onSendChanges();

		if((_changes & BROADCAST_CHAR_INFO) == BROADCAST_CHAR_INFO)
			_activeChar.broadcastCharInfo();
	}
}
