package l2n.game.model.zone;

import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

public class L2MonsterTrapZone extends L2ZoneEnterLeaveListener
{
	private static final long MONSTER_TRAP_DESPAWN_TIME = 5 * 60 * 1000L; // 5 min

	@Override
	public void objectEntered(final L2Zone zone, final L2Character character)
	{
		try
		{
			if(zone == null || character == null)
				return;

			final L2Player player = character.getPlayer();
			if(player != null && zone.getEvent() != null)
			{
				final Reflection r = player.getReflection();
				if(r.getInstancedZoneId() == zone.getReflectionId())
				{
					// Структура: reuse;chance1,id11,id12,id1N;chance2,id221,id22,id2N;chanceM,idM1,idM2,idMN; .....
					final String[] params = zone.getEvent().split(";");
					final int reuse = Integer.parseInt(params[0]); // В секундах

					final long zoneReuse = ZoneManager.getInstance().getReuseMonsterTrapZone(zone.getId(), r.getId());
					if(zoneReuse != 0 && zoneReuse + reuse * 1000L > System.currentTimeMillis())
						return;

					ZoneManager.getInstance().setReuseMonsterTrapZone(zone.getId(), r.getId(), System.currentTimeMillis());
					final int[] chances = new int[params.length - 1];
					final int[][] groups = new int[params.length - 1][];
					for(int i = 1; i < params.length; i++)
					{
						// Структура: chance,id1,id2,idN
						final String[] group = params[i].split(",");
						chances[i - 1] = Integer.parseInt(group[0]);
						final int[] mobs = new int[group.length - 1];
						for(int j = 1; j < group.length; j++)
							mobs[j - 1] = Integer.parseInt(group[j]);
						groups[i - 1] = mobs;
					}
					final int[] monsters = groups[chooseGroup(chances)];
					for(final int monster : monsters)
					{
						final L2NpcTemplate template = NpcTable.getTemplate(monster);
						if(template == null)
							continue;

						final L2Spawn spawn = new L2Spawn(template);
						spawn.setLocation(zone.getLoc().getId());
						spawn.setHeading(-1);
						spawn.setAmount(1);
						spawn.setReflection(r.getId());
						spawn.stopRespawn();
						final L2NpcInstance mob = spawn.doSpawn(true);
						if(mob != null)
						{
							L2GameThreadPools.getInstance().scheduleAi(new UnSpawnTask(mob), MONSTER_TRAP_DESPAWN_TIME, false);
							if(mob.isAggressive() && mob.getAI().canSeeInSilentMove(player))
								mob.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, player, 1);
						}
					}
				}
			}

		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void objectLeaved(final L2Zone zone, final L2Character character)
	{}

	private static int chooseGroup(final int[] chances)
	{
		int sum = 0;

		for(int i = 0; i < chances.length; i++)
			sum += chances[i];
		final int[] table = new int[sum];
		int k = 0;

		for(int i = 0; i < chances.length; i++)
			for(int j = 0; j < chances[i]; j++)
			{
				table[k] = i;
				k++;
			}
		return table[Rnd.get(table.length)];
	}

	public class UnSpawnTask implements Runnable
	{
		L2NpcInstance _monster;

		public UnSpawnTask(final L2NpcInstance npc)
		{
			_monster = npc;
		}

		@Override
		public void run()
		{
			if(_monster != null)
				_monster.deleteMe();
			_monster = null;
		}
	}
}
