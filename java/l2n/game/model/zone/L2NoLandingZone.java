package l2n.game.model.zone;

import l2n.Config;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.Siege;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.PetDataTable;
import l2n.util.Location;

public class L2NoLandingZone extends L2ZoneEnterLeaveListener
{
	@Override
	public void objectEntered(final L2Zone zone, final L2Character character)
	{
		if(zone == null || character == null)
			return;

		final L2Player player = character.getPlayer();
		if(player != null)
			if(player.isFlying() && !player.isBlocked() && player.getMountNpcId() == PetDataTable.WYVERN_ID)
			{
				final Siege siege = SiegeManager.getSiege(player, false);
				if(siege != null)
				{
					final Residence unit = siege.getSiegeUnit();
					if(unit != null && player.getClan() != null && player.isClanLeader() && (player.getClan().getHasCastle() == unit.getId() || player.getClan().getHasFortress() == unit.getId()))
						return;
				}

				player.stopMove();
				player.sendPacket(new SystemMessage(SystemMessage.THIS_AREA_CANNOT_BE_ENTERED_WHILE_MOUNTED_ATOP_OF_A_WYVERN_YOU_WILL_BE_DISMOUNTED_FROM_YOUR_WYVERN_IF_YOU_DO_NOT_LEAVE));

				Integer enterCount = (Integer) player.getProperty(PropertyCollection.ZoneEnteredNoLandingFlying);
				if(enterCount == null)
					enterCount = 0;

				final Location loc = player.getLastServerPosition();
				if(loc == null || enterCount >= 5)
				{
					player.setMount(0, 0);
					player.addProperty(PropertyCollection.ZoneEnteredNoLandingFlying, 0);
					return;
				}

				player.teleToLocation(loc);
				player.addProperty(PropertyCollection.ZoneEnteredNoLandingFlying, enterCount + 1);
			}
			else if(Config.ALT_DONT_ALLOW_PETS_ON_SIEGE && player.getPet() != null)
			{
				final int id = player.getPet().getNpcId();

				if((PetDataTable.isBabyPet(id) || PetDataTable.isImprovedBabyPet(id)) && SiegeManager.getSiege(player, true) != null)
				{
					player.getPet().unSummon();
					player.sendMessage("Этих питомцев запрещено использовать в зонах осад.");
				}
			}
	}

	@Override
	public void objectLeaved(final L2Zone zone, final L2Character character)
	{}
}
