package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.ai.DefaultAI;
import l2n.game.model.L2ObjectProcedures.RemoveObjectsFromPlayerProc;
import l2n.game.model.L2ObjectProcedures.RemovePlayerFromOtherPlayersProc;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.DeleteObject;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.tables.SpawnTable;
import l2n.game.tables.TerritoryTable;
import l2n.util.Location;

import java.util.ArrayDeque;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2WorldRegion
{
	private static final Logger _log = Logger.getLogger(L2WorldRegion.class.getName());

	private L2Object[] _objects = null;
	private final Lock objects_read_lock;
	private final Lock objects_write_lock;

	private GArray<L2Territory> territories = null;
	private final Lock territories_read_lock;
	private final Lock territories_write_lock;

	private final int tileX, tileY, tileZ;

	private int _objectsSize = 0;
	private int _playersSize = 0;

	private boolean _spawned = false;
	private boolean _small = false;

	private final ReentrantLock spawn_lock = new ReentrantLock();

	public L2WorldRegion(final int pTileX, final int pTileY, final int pTileZ, final boolean small)
	{
		final ReentrantReadWriteLock object_lock = new ReentrantReadWriteLock();
		objects_read_lock = object_lock.readLock();
		objects_write_lock = object_lock.writeLock();

		final ReentrantReadWriteLock territory_lock = new ReentrantReadWriteLock();
		territories_read_lock = territory_lock.readLock();
		territories_write_lock = territory_lock.writeLock();

		tileX = pTileX;
		tileY = pTileY;
		tileZ = pTileZ;
		_small = small;
	}

	private void spawn()
	{
		if(!_spawned && Config.DELAYED_SPAWN)
		{
			spawn_lock.lock();
			try
			{
				if(_spawned)
					return;
				_spawned = true;

				for(final L2Spawn spawn : SpawnTable.getInstance().getSpawnTable())
				{
					Location loc = null;
					if(spawn.getLocx() == 0)
					{
						if(spawn.getLocation() <= 0)
							continue;
						final L2Territory terr = TerritoryTable.getInstance().getLocation(spawn.getLocation());
						if(terr == null)
							continue;
						loc = terr.getCenter();
					}
					else
						loc = spawn.getLoc();

					final int x = loc.x / L2World.DIV_BY + L2World.OFFSET_X;
					final int y = loc.y / L2World.DIV_BY + L2World.OFFSET_Y;

					if(L2World.validRegion(x, y, 0, false))
					{
						int z = 0;
						if(L2World.getRegions()[x][y].length > 1)
						{
							z = loc.z / L2World.DIV_BY_FOR_Z + L2World.OFFSET_Z;
							if(!L2World.validRegion(x, y, z, false))
								continue;
						}

						if(x == tileX && y == tileY && z == tileZ)
							spawn.init();
					}
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				spawn_lock.unlock();
			}
		}
	}

	public void addToPlayers(final L2Object object, final L2Character dropper)
	{
		if(_objects == null)
		{
			_objectsSize = 0;
			_playersSize = 0;
			return;
		}

		L2Player player = null;
		if(object.isPlayer())
			player = (L2Player) object;

		if(player != null)
		{
			final ArrayDeque<L2GameServerPacket> result = new ArrayDeque<L2GameServerPacket>(_objectsSize);
			for(final L2Object obj : getObjectsList(new GArray<L2Object>(_objectsSize), object.getObjectId(), object.getReflection()))
				if(obj != null)
				{
					if(obj.inObserverMode() && !obj.isInOlympiadMode() && (obj.getCurrentRegion() == null || !obj.getCurrentRegion().equals(this)))
						continue;
					result.addAll(player.addVisibleObject(obj, dropper));
				}
			player.sendPackets2(result);
		}

		for(final L2Player pl : getPlayersList(new GArray<L2Player>(_playersSize), object.getObjectId(), object.getReflection()))
			pl.sendPackets(pl.addVisibleObject(object, dropper));
	}

	public void removeFromPlayers(final L2Object object, final boolean move)
	{
		if(_objects == null)
		{
			_objectsSize = 0;
			_playersSize = 0;
			return;
		}

		L2Player player = null;
		if(object.isPlayer())
			player = (L2Player) object;

		if(player != null)
		{
			final ArrayDeque<L2GameServerPacket> result = new ArrayDeque<L2GameServerPacket>(_objectsSize);
			final GArray<L2Object> objectList = getObjectsList(new GArray<L2Object>(_objectsSize), object.getObjectId(), object.getReflection());
			objectList.forEach(new RemoveObjectsFromPlayerProc(player, result, null, move));
			player.sendPackets2(result);
		}

		final GArray<L2Player> players = getPlayersList(new GArray<L2Player>(_playersSize), object.getObjectId(), object.getReflection());
		players.forEach(new RemovePlayerFromOtherPlayersProc(object, new DeleteObject(object), move));
	}

	private L2Object[] getObjects()
	{
		objects_write_lock.lock();
		try
		{
			if(_objects == null)
			{
				_objects = new L2Object[50];
				_objectsSize = 0;
				_playersSize = 0;
			}
			return _objects;
		}
		finally
		{
			objects_write_lock.unlock();
		}
	}

	public void addObject(final L2Object obj)
	{
		if(obj == null)
			return;

		objects_write_lock.lock();
		try
		{
			if(_objects == null)
			{
				_objects = new L2Object[50];
				_objectsSize = 0;
			}
			else if(_objectsSize >= _objects.length)
			{
				final L2Object[] temp = new L2Object[_objects.length * 2];
				for(int i = _objectsSize; i-- > 0;)
					temp[i] = _objects[i];
				_objects = temp;
			}

			_objects[_objectsSize] = obj;
			_objectsSize++;

			if(obj.isPlayer())
			{
				_playersSize++;
				if(Config.DELAYED_SPAWN)
					for(final L2WorldRegion neighbor : getNeighbors())
						neighbor.spawn();
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_write_lock.unlock();
		}

		if(obj.isNpc() && obj.getAI() instanceof DefaultAI && obj.getAI().isGlobalAI() && !obj.getAI().isActive())
			obj.getAI().startAITask();
	}

	public void removeObject(final L2Object obj, final boolean move)
	{
		if(obj == null)
			return;

		objects_write_lock.lock();
		try
		{
			if(_objects == null)
			{
				_objectsSize = 0;
				_playersSize = 0;
				return;
			}

			if(_objectsSize > 1)
			{
				int k = -1;
				for(int i = _objectsSize; i-- > 0;)
					if(_objects[i] == obj)
					{
						k = i;
						break;
					}
				if(k > -1)
				{
					_objects[k] = _objects[_objectsSize - 1];
					_objects[_objectsSize - 1] = null;
					_objectsSize--;
				}
			}
			else if(_objectsSize == 1 && _objects[0] == obj)
			{
				_objects[0] = null;
				_objects = null;
				_objectsSize = 0;
				_playersSize = 0;
			}

			if(obj.isPlayer())
			{
				_playersSize--;
				if(_playersSize <= 0)
					_playersSize = 0;
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_write_lock.unlock();
		}

		if(!move && obj.isNpc() && obj.getAI() instanceof DefaultAI && !obj.getAI().isGlobalAI())
			obj.getAI().stopAITask();
	}

	public GArray<L2Object> getObjectsList(final GArray<L2Object> result, final int exclude, final Reflection reflection)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj != null && obj.getObjectId() != exclude && (reflection.getId() == -1 || obj.getReflection() == reflection))
					result.add(obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Object> getObjectsList(final GArray<L2Object> result, final int exclude, final Reflection reflection, final int x, final int y, final int z, final long sqrad, final int height)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj == null || obj.getObjectId() == exclude || reflection.getId() != -1 && obj.getReflection() != reflection)
					continue;
				if(Math.abs(obj.getZ() - z) > height)
					continue;
				long dx = obj.getX() - x;
				dx *= dx;
				if(dx > sqrad)
					continue;
				long dy = obj.getY() - y;
				dy *= dy;
				if(dx + dy < sqrad)
					result.add(obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Character> getCharactersList(final GArray<L2Character> result, final int exclude, final long reflection)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj != null && obj.isCharacter() && obj.getObjectId() != exclude && (reflection == -1 || obj.getReflection().getId() == reflection))
					result.add((L2Character) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Character> getCharactersList(final GArray<L2Character> result, final int exclude, final long reflection, final int x, final int y, final int z, final long sqrad, final int height)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj == null || !obj.isCharacter() || obj.getObjectId() == exclude || reflection != -1 && obj.getReflection().getId() != reflection)
					continue;
				if(Math.abs(obj.getZ() - z) > height)
					continue;
				long dx = obj.getX() - x;
				dx *= dx;
				if(dx > sqrad)
					continue;
				long dy = obj.getY() - y;
				dy *= dy;
				if(dx + dy < sqrad)
					result.add((L2Character) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2NpcInstance> getNpcsList(final GArray<L2NpcInstance> result, final int exclude, final long reflection)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj != null && obj.isNpc() && obj.getObjectId() != exclude && (reflection == -1 || obj.getReflection().getId() == reflection))
					result.add((L2NpcInstance) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2NpcInstance> getNpcsList(final GArray<L2NpcInstance> result, final int exclude, final long reflection, final int x, final int y, final int z, final long sqrad, final int height)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj == null || !obj.isNpc() || obj.getObjectId() == exclude || reflection != -1 && obj.getReflection().getId() != reflection)
					continue;
				if(Math.abs(obj.getZ() - z) > height)
					continue;
				long dx = obj.getX() - x;
				dx *= dx;
				if(dx > sqrad)
					continue;
				long dy = obj.getY() - y;
				dy *= dy;
				if(dx + dy < sqrad)
					result.add((L2NpcInstance) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Player> getPlayersList(final GArray<L2Player> result, final int exclude, final Reflection reflection)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj != null && obj.isPlayer() && obj.getObjectId() != exclude && (reflection.getId() == -1 || obj.getReflection() == reflection))
					result.add((L2Player) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Player> getPlayersList(final GArray<L2Player> result, final int exclude, final Reflection reflection, final int x, final int y, final int z, final long sqrad, final int height)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj == null || !obj.isPlayer() || obj.getObjectId() == exclude || reflection.getId() != -1 && obj.getReflection().getId() != reflection.getId())
					continue;
				if(Math.abs(obj.getZ() - z) > height)
					continue;
				long dx = obj.getX() - x;
				dx *= dx;
				if(dx > sqrad)
					continue;
				long dy = obj.getY() - y;
				dy *= dy;
				if(dx + dy < sqrad)
					result.add((L2Player) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Playable> getPlayablesList(final GArray<L2Playable> result, final int exclude, final Reflection reflection)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj != null && obj.isPlayable() && obj.getObjectId() != exclude && (reflection.getId() == -1 || obj.getReflection() == reflection))
					result.add((L2Playable) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public GArray<L2Playable> getPlayablesList(final GArray<L2Playable> result, final int exclude, final Reflection reflection, final int x, final int y, final int z, final long sqrad, final int height)
	{
		objects_read_lock.lock();
		try
		{
			if(_objects == null || _objectsSize == 0)
				return result;
			L2Object obj;
			for(int i = _objectsSize; i-- > 0;)
			{
				obj = _objects[i];
				if(obj == null || !obj.isPlayable() || obj.getObjectId() == exclude || reflection.getId() != -1 && obj.getReflection().getId() != reflection.getId())
					continue;
				if(Math.abs(obj.getZ() - z) > height)
					continue;
				long dx = obj.getX() - x;
				dx *= dx;
				if(dx > sqrad)
					continue;
				long dy = obj.getY() - y;
				dy *= dy;
				if(dx + dy < sqrad)
					result.add((L2Playable) obj);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}
		return result;
	}

	public void deleteVisibleNpcSpawns()
	{
		final GArray<L2NpcInstance> toRemove = new GArray<L2NpcInstance>(_objectsSize);
		objects_read_lock.lock();
		try
		{
			if(_objects != null)
			{
				L2Object obj;
				for(int i = _objectsSize; i-- > 0;)
				{
					obj = _objects[i];
					if(obj != null && obj.isNpc())
						toRemove.add((L2NpcInstance) obj);
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			objects_read_lock.unlock();
		}

		L2Spawn spawn;
		for(final L2NpcInstance npc : toRemove)
		{
			spawn = npc.getSpawn();
			if(spawn != null)
			{
				npc.deleteMe();
				spawn.stopRespawn();
			}
		}
	}

	public void showObjectsToPlayer(final L2Player player)
	{
		if(player != null && _objects != null)
		{
			final ArrayDeque<L2GameServerPacket> result = new ArrayDeque<L2GameServerPacket>(_objectsSize);
			for(final L2Object obj : getObjectsList(new GArray<L2Object>(_objectsSize), player.getObjectId(), player.getReflection()))
				result.addAll(player.addVisibleObject(obj, null));
			player.sendPackets2(result);
		}
	}

	public void removeObjectsFromPlayer(final L2Player player)
	{
		if(player != null && _objects != null)
		{
			final ArrayDeque<L2GameServerPacket> result = new ArrayDeque<L2GameServerPacket>(_objectsSize);
			final GArray<L2Object> objects = getObjectsList(new GArray<L2Object>(_objectsSize), player.getObjectId(), player.getReflection());
			objects.forEach(new RemoveObjectsFromPlayerProc(player, result, null, true));
			player.sendPackets2(result);
		}
	}

	public void removePlayerFromOtherPlayers(final L2Object object)
	{
		if(object != null && _objects != null)
		{
			final GArray<L2Player> players = getPlayersList(new GArray<L2Player>(_playersSize), object.getObjectId(), object.getReflection());
			players.forEach(new RemovePlayerFromOtherPlayersProc(object, new DeleteObject(object), true));
		}
	}

	public boolean areNeighborsEmpty()
	{
		if(!isEmpty())
			return false;
		for(final L2WorldRegion neighbor : getNeighbors())
			if(neighbor != null && !neighbor.isEmpty())
				return false;
		return true;
	}

	public GArray<L2WorldRegion> getNeighbors()
	{
		return L2World.getNeighbors(tileX, tileY, tileZ, _small);
	}

	public int getObjectsSize()
	{
		return _objectsSize;
	}

	public int getPlayersSize()
	{
		return _playersSize;
	}

	public boolean isEmpty()
	{
		return _playersSize <= 0;
	}

	public boolean isNull()
	{
		return _objectsSize <= 0;
	}

	public void addTerritory(final L2Territory territory)
	{
		territories_write_lock.lock();
		try
		{
			if(territories == null)
				territories = new GArray<L2Territory>(5);
			if(!territories.contains(territory))
				territories.add(territory);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			territories_write_lock.unlock();
		}
	}

	public void removeTerritory(final L2Territory territory)
	{
		territories_write_lock.lock();
		try
		{
			if(territories == null)
				return;
			territories.remove(territory);
			if(territories.isEmpty())
				territories = null;
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			territories_write_lock.unlock();
		}
	}

	public GArray<L2Territory> getTerritories(final int x, final int y, final int z)
	{
		territories_read_lock.lock();
		try
		{
			if(territories == null)
				return null;
			final GArray<L2Territory> result = new GArray<L2Territory>(territories.size());
			for(final L2Territory terr : territories)
				if(terr != null && terr.isInside(x, y, z))
					result.addLastUnsafe(terr);
			return result.isEmpty() ? null : result;
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		finally
		{
			territories_read_lock.unlock();
		}
		return null;
	}

	public String getName()
	{
		return "(" + tileX + ", " + tileY + ", " + tileZ + ")";
	}
}
