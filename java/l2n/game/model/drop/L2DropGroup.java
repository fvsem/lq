package l2n.game.model.drop;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.base.ItemToDrop;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.DynamicRateTable;
import l2n.util.Rnd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class L2DropGroup implements Cloneable
{
	private int _id;
	private double _chance;
	private boolean _isAdena = false; // Шанс фиксирован, растет только количество
	private boolean _fixedQty = false; // Вместо увеличения количества используется увеличение количества роллов группы
	private boolean _notRate = false; // Рейты вообще не применяются
	private List<L2DropData> _items = new ArrayList<L2DropData>();

	public L2DropGroup(int id)
	{
		_id = id;
	}

	public int getId()
	{
		return _id;
	}

	public boolean fixedQty()
	{
		return _fixedQty;
	}

	public boolean notRate()
	{
		return _notRate;
	}

	public void addDropItem(L2DropData item)
	{
		if(item.getItem().isAdena())
			_isAdena = true;
		if(item.getItem().isRaidAccessory() || item.getItem().isArrow() || item.getItem().isHerb())
			_notRate = true;
		if(item.getItem().isEquipment() || item.getItem().isKeyMatherial())
			_fixedQty = true;
		item.setChanceInGroup(_chance);
		_chance += item.getChance();
		_items.add(item);
	}

	/**
	 * Возвращает список вещей или копию списка
	 */
	public List<L2DropData> getDropItems(boolean copy)
	{
		if(!copy)
			return _items;
		List<L2DropData> temp = new ArrayList<L2DropData>();
		temp.addAll(_items);
		return temp;
	}

	/**
	 * Возвращает полностью независимую копию группы
	 */
	@Override
	public L2DropGroup clone()
	{
		L2DropGroup ret = new L2DropGroup(_id);
		for(L2DropData i : _items)
			ret.addDropItem(i.clone());
		return ret;
	}

	/**
	 * Возвращает оригинальный список вещей если рейты не нужны или клон с примененными рейтами
	 */
	public List<L2DropData> getRatedItems(double mod)
	{
		if(mod == 1 || _notRate)
			return _items;

		List<L2DropData> ret = new ArrayList<L2DropData>();

		for(L2DropData d : _items)
			ret.add(d.clone());

		double perItemChance = L2Drop.MAX_CHANCE / ret.size(); // средний шанс для предмета (макс шанс / количество предметов в группе)
		double gChance = 0;
		for(L2DropData i : ret)
		{
			double avgQty = (i.getMinDrop() + i.getMaxDrop()) / 2; // среднее колличество итемов для дропа ((мин колличество + макс колличество) / 2)
			double newChance = mod * i.getChance() * avgQty; // Новый шанс для итема: с учётом старого шанса, учётом модификаторы, среднего колличество итемов для дропа
			long avgCount = (long) Math.ceil(newChance / perItemChance); // Новое количество итемов для дропа

			// count = (RateDropItems * ItemChance * AvgCount) / (MAX_CHANCE / GItemCount)
			// RateDropItems - рейты дропа итемов
			// ItemChance - шанс дропа предмета в БД
			// AvgCount - среднеарифметическое значение максимального и минимального количества для итема
			// MAX_CHANCE - максимально возможный шанс (всегда равен 1000000)
			// GItemCount - количество итемов в группе

			long min = avgCount;
			long max = avgCount;
			long shift = Math.min(Math.round(avgCount * 1 / 3), avgCount - 1);
			if(shift > 0)
			{
				min -= shift;
				max += shift;
			}
			i.setMinDrop(min);
			i.setMaxDrop(max);

			i.setChance(newChance / avgCount);
			i.setChanceInGroup(gChance);
			gChance += i.getChance();
		}

		return ret;
	}

	/**
	 * Эта функция выбирает одну вещь из группы
	 * Используется в основном механизме расчета дропа
	 */
	public List<ItemToDrop> roll(int diff, boolean isSpoil, L2MonsterInstance monster, L2Player player, double mod)
	{
		if(_isAdena)
			return rollAdena(diff, player, mod, monster.isRaid());
		if(isSpoil)
			return rollSpoil(diff, player, mod);
		if(monster.isRaid() || _notRate || _fixedQty)
			return rollFixedQty(diff, monster, player, mod);

		// если множитель дропа большой разбивать дроп на кучки
		// количество итераций не более L2Drop.MAX_DROP_ITERATIONS
		double cmod = mod * monster.getDropRate(player) * (Config.SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB && monster.isRaid() ? 1 : player.getRateItems());
		if(cmod > Config.RATE_BREAKPOINT)
		{
			long iters = Math.min((long) Math.ceil(cmod / Config.RATE_BREAKPOINT), Config.MAX_DROP_ITERATIONS);
			List<ItemToDrop> ret = new ArrayList<ItemToDrop>();
			for(int i = 0; i < iters; i++)
				ret.addAll(rollNormal(diff, monster, player, mod / iters));
			return ret;
		}

		return rollNormal(diff, monster, player, mod);
	}

	public List<ItemToDrop> rollNormal(int diff, L2NpcInstance monster, L2Player player, double mod)
	{
		if(Config.DEEPBLUE_DROP_RULES && diff > 0)
			mod *= Experience.penaltyModifier(diff, 9);
		if(mod <= 0)
			return Collections.emptyList();

		double rate = monster.getDropRate(player) * (Config.SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB && monster.isRaid() ? 1 : player.getRateItems());
		double calcChance = 0;
		double rollChance = 0;

		double mult = 1;

		List<L2DropData> items = getRatedItems(rate * mod);
		for(L2DropData i : items)
			calcChance += i.getChance();
		rollChance = calcChance;

		if(Rnd.get(1, L2Drop.MAX_CHANCE) > calcChance)
			return Collections.emptyList();

		List<ItemToDrop> ret = new ArrayList<ItemToDrop>();
		rollFinal(items, ret, mult, rollChance);
		return ret;
	}

	public List<ItemToDrop> rollFixedQty(int diff, L2MonsterInstance monster, L2Player player, double mod)
	{
		// Поправка на глубоко синих мобов
		if(Config.DEEPBLUE_DROP_RULES && diff > 0)
			mod *= Experience.penaltyModifier(diff, 9);
		if(mod <= 0)
			return Collections.emptyList();
		double rate;
		if(_notRate)
			rate = Math.min(mod, 1);
		else
			rate = monster.getDropRate(player) * mod * (Config.SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB && monster.isRaid() ? 1 : player.getRateItems());

		// Считаем шанс группы
		double calcChance = _chance * rate;
		double[] balanced = balanceChanceAndMult(calcChance);
		calcChance = balanced[0];
		double dropmult = balanced[1];

		List<ItemToDrop> ret = new ArrayList<ItemToDrop>();
		for(int n = 0; n < dropmult; n++)
			if(Rnd.get(1, L2Drop.MAX_CHANCE) < calcChance)
				rollFinal(_items, ret, 1, _chance);
		return ret;
	}

	private List<ItemToDrop> rollSpoil(int diff, L2Player player, double mod)
	{
		double rate = DynamicRateTable.getRateSpoil(player) * player.getRateSpoil();
		// Поправка на глубоко синих мобов
		if(Config.DEEPBLUE_DROP_RULES && diff > 0)
			mod *= Experience.penaltyModifier(diff, 9);
		if(mod <= 0)
			return null;

		// Считаем шанс группы
		double calcChance = _chance * rate * mod;
		double[] balanced = balanceChanceAndMult(calcChance);
		calcChance = balanced[0];
		double dropmult = balanced[1];

		if(Rnd.get(1, L2Drop.MAX_CHANCE) > calcChance)
			return null;

		List<ItemToDrop> ret = new ArrayList<ItemToDrop>(1);
		rollFinal(_items, ret, dropmult, _chance);
		return ret;
	}

	private List<ItemToDrop> rollAdena(int diff, L2Player player, double mod, boolean isRaid)
	{
		double rate = DynamicRateTable.getRateAdena(player) * (Config.SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB && isRaid ? 1 : player.getRateAdena());
		if(Config.DEEPBLUE_DROP_RULES && diff > 0)
			mod *= Experience.penaltyModifier(diff, 9);
		double chance = _chance;
		if(mod > 10)
		{
			mod *= _chance / L2Drop.MAX_CHANCE;
			chance = L2Drop.MAX_CHANCE;
		}
		if(mod <= 0 || Rnd.get(1, L2Drop.MAX_CHANCE) > chance)
			return null;
		double mult = rate * mod;

		List<ItemToDrop> ret = new ArrayList<ItemToDrop>(1);
		rollFinal(_items, ret, mult, _chance);
		for(ItemToDrop i : ret)
			i.isAdena = true;
		return ret;
	}

	public static double[] balanceChanceAndMult(double calcChance)
	{
		double dropmult = 1.;
		if(calcChance > L2Drop.MAX_CHANCE)
		{
			if(calcChance % L2Drop.MAX_CHANCE == 0) // если кратен 100% то тупо умножаем количество
				dropmult = calcChance / L2Drop.MAX_CHANCE;
			else
				dropmult = Math.ceil(calcChance / L2Drop.MAX_CHANCE); // множитель равен шанс / 100% округление вверх
			calcChance = calcChance / dropmult; // шанс равен шанс / множитель
			// в результате получаем увеличение количества и уменьшение шанса, при этом шанс не падает ниже 50%
		}

		return new double[] { calcChance, dropmult };
	}

	private void rollFinal(List<L2DropData> items, List<ItemToDrop> ret, double mult, double chanceSum)
	{
		// перебираем все вещи в группе и проверяем шанс
		int chance = Rnd.get(0, (int) chanceSum);
		for(L2DropData i : items)
		{
			if(i == null || chance < i.getChanceInGroup())
				continue;

			boolean notlast = false;
			for(L2DropData t : items)
				if(t != null && t.getChanceInGroup() > i.getChanceInGroup() && chance > t.getChanceInGroup())
				{
					notlast = true;
					break;
				}
			if(notlast)
				continue;

			ItemToDrop t = new ItemToDrop(i.getItemId());

			if(i.getMinDrop() >= i.getMaxDrop())
				t.count = Math.round(i.getMinDrop() * mult);
			else
				t.count = Rnd.get(Math.round(i.getMinDrop() * mult), Math.round(i.getMaxDrop() * mult));

			ret.add(t);
			break;
		}
	}

	public double getChance()
	{
		return _chance;
	}

	public void setChance(double chance)
	{
		_chance = chance;
	}

	public boolean isAdena()
	{
		return _isAdena;
	}
}
