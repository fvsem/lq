package l2n.game.model;

public class L2MinionData
{

	private int _minionId;

	private byte _minionAmount;

	public void setMinionId(int id)
	{
		_minionId = id;
	}

	public int getMinionId()
	{
		return _minionId;
	}

	public void setAmount(int amount)
	{
		_minionAmount = (byte) amount;
	}

	public byte getAmount()
	{
		return _minionAmount;
	}
}
