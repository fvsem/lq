package l2n.game.model;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Clan.RankPrivs;
import l2n.game.model.L2Clan.SubPledge;
import l2n.game.model.actor.L2Player;

public class L2ClanMember
{
	private L2Clan _clan;
	private String _name;
	private String _title;
	private int _level;
	private int _classId;
	private int _sex;
	private long playerStoreId;
	private int _pledgeType;
	private int _powerGrade;
	private int _apprentice;
	private Boolean _clanLeader;

	/**
	 * Конструктор
	 */
	public L2ClanMember(final L2Clan clan, final String name, final String title, final int level, final int classId, final int objectId, final int pledgeType, final int powerGrade, final int apprentice, final Boolean clanLeader)
	{
		_clan = clan;
		_name = name;
		_title = title;
		_level = level;
		_classId = classId;
		_pledgeType = pledgeType;
		_powerGrade = powerGrade;
		_apprentice = apprentice;
		_clanLeader = clanLeader;
		playerStoreId = L2ObjectsStorage.objIdNoStore(objectId);
		if(powerGrade != 0)
		{
			final RankPrivs r = clan.getRankPrivs(powerGrade);
			r.setParty(clan.countMembersByRank(powerGrade));
		}
	}

	public L2ClanMember(final L2Player player)
	{
		playerStoreId = player.getStoredId();
	}

	public void setPlayerInstance(final L2Player player)
	{
		if(player == null)
		{
			playerStoreId = L2ObjectsStorage.objIdNoStore(getObjectId());
			return;
		}

		// this is here to keep the data when the player logs off
		playerStoreId = player.getStoredId();
		_clan = player.getClan();
		_name = player.getName();
		_title = player.getTitle();
		_level = player.getLevel();
		_classId = player.getClassId().getId();
		_pledgeType = player.getPledgeType();
		_powerGrade = player.getPowerGrade();
		_apprentice = player.getApprentice();
		_clanLeader = player.isClanLeader();
	}

	public L2Player getPlayer()
	{
		return L2ObjectsStorage.getAsPlayer(playerStoreId);
	}

	public boolean isOnline()
	{
		final L2Player player = getPlayer();
		return player != null && !player.isInOfflineMode();
	}

	public L2Clan getClan()
	{
		final L2Player player = getPlayer();
		return player == null ? _clan : player.getClan();
	}

	/**
	 * @return Returns the classId.
	 */
	public int getClassId()
	{
		final L2Player player = getPlayer();
		return player == null ? _classId : player.getClassId().getId();
	}

	public int getSex()
	{
		final L2Player player = getPlayer();
		return player == null ? _sex : player.getSex();
	}

	/**
	 * @return Returns the level.
	 */
	public int getLevel()
	{
		final L2Player player = getPlayer();
		return player == null ? _level : player.getLevel();
	}

	/**
	 * @return Returns the name.
	 */
	public String getName()
	{
		final L2Player player = getPlayer();
		return player == null ? _name : player.getName();
	}

	/**
	 * @return Returns the objectId.
	 */
	public int getObjectId()
	{
		return L2ObjectsStorage.getStoredObjectId(playerStoreId);
	}

	public String getTitle()
	{
		final L2Player player = getPlayer();
		return player == null ? _title : player.getTitle();
	}

	public void setTitle(final String title)
	{
		_title = title;
		if(getPlayer() != null)
		{
			getPlayer().setTitle(title);
			getPlayer().sendChanges();
		}
		else
		{
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("UPDATE characters SET title=? WHERE obj_Id=?");
				statement.setString(1, title);
				statement.setInt(2, getObjectId());
				statement.execute();
			}
			catch(final Exception e)
			{}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
	}

	public int getPledgeType()
	{
		final L2Player player = getPlayer();
		return player == null ? _pledgeType : player.getPledgeType();
	}

	public void setPledgeType(final int pledgeType)
	{
		final L2Player player = getPlayer();
		_pledgeType = pledgeType;
		if(player != null)
			player.setPledgeType(pledgeType);
		else
			updatePledgeType();
	}

	public void updatePledgeType()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET pledge_type=? WHERE obj_Id=?");
			statement.setInt(1, _pledgeType);
			statement.setInt(2, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public int getPowerGrade()
	{
		final L2Player player = getPlayer();
		return player == null ? _powerGrade : player.getPowerGrade();
	}

	public void setPowerGrade(final int newPowerGrade)
	{
		final L2Player player = getPlayer();
		final int oldPowerGrade = getPowerGrade();
		_powerGrade = newPowerGrade;
		if(player != null)
			player.setPowerGrade(newPowerGrade);
		else
			updatePowerGrade();
		updatePowerGradeParty(oldPowerGrade, newPowerGrade);
	}

	private void updatePowerGradeParty(final int oldGrade, final int newGrade)
	{
		L2Clan clan = getClan();
		if(clan == null)
			return;
		if(oldGrade != 0)
		{
			final RankPrivs r1 = clan.getRankPrivs(oldGrade);
			r1.setParty(clan.countMembersByRank(oldGrade));
		}
		if(newGrade != 0)
		{
			final RankPrivs r2 = clan.getRankPrivs(newGrade);
			r2.setParty(clan.countMembersByRank(newGrade));
		}
	}

	public void updatePowerGrade()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET pledge_rank=? WHERE obj_Id=?");
			statement.setInt(1, _powerGrade);
			statement.setInt(2, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private int getApprentice()
	{
		final L2Player player = getPlayer();
		return player == null ? _apprentice : player.getApprentice();
	}

	public void setApprentice(final int apprentice)
	{
		final L2Player player = getPlayer();
		_apprentice = apprentice;
		if(player != null)
			player.setApprentice(apprentice);
		else
			updateApprentice();
	}

	public void updateApprentice()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET apprentice=? WHERE obj_Id=?");
			statement.setInt(1, _apprentice);
			statement.setInt(2, getObjectId());
			statement.execute();
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public String getApprenticeName()
	{
		if(getApprentice() != 0 && getClan().getClanMember(getApprentice()) != null)
			return getClan().getClanMember(getApprentice()).getName();
		return "";
	}

	public boolean hasApprentice()
	{
		return getApprentice() != 0;
	}

	public int getSponsor()
	{
		if(getPledgeType() != L2Clan.SUBUNIT_ACADEMY)
			return 0;
		final int id = getObjectId();
		for(final L2ClanMember element : getClan().getMembers())
			if(element.getApprentice() == id)
				return element.getObjectId();
		return 0;
	}

	public String getSponsorName()
	{
		final int sponsorId = getSponsor();
		if(sponsorId == 0)
			return "";
		else if(getClan().getClanMember(sponsorId) != null)
			return getClan().getClanMember(sponsorId).getName();
		return "";
	}

	public boolean hasSponsor()
	{
		return getSponsor() != 0;
	}

	public String getRelatedName()
	{
		if(getPledgeType() == L2Clan.SUBUNIT_ACADEMY)
			return getSponsorName();
		return getApprenticeName();
	}

	public boolean isClanLeader()
	{
		final L2Player player = getPlayer();
		return player == null ? _clanLeader : player.isClanLeader();
	}

	public int isSubLeader()
	{
		if(getClan() != null)
			for(final SubPledge element : getClan().getAllSubPledges())
				if(element != null && element.getLeaderId() == getObjectId())
					return element.getType();
		return 0;
	}
}
