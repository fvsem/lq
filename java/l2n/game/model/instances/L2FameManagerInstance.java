package l2n.game.model.instances;

import l2n.game.ai.CtrlIntention;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.PledgeShowInfoUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

public class L2FameManagerInstance extends L2NpcInstance
{
	public L2FameManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!isInRange(player, 200))
		{
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
			player.sendActionFailed();
			return;
		}

		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken();
		NpcHtmlMessage html = new NpcHtmlMessage(getNpcId());
		if(actualCommand.equalsIgnoreCase("PK_Count"))
		{
			if(player.getFame() >= 5000)
			{
				if(player.getPkKills() > 0)
				{
					player.setFame(player.getFame() - 5000);
					player.setPkKills(player.getPkKills() - 1);
					html.setFile("data/html/default/" + getNpcId() + "-3.htm");
				}
				else
					html.setFile("data/html/default/" + getNpcId() + "-4.htm");
			}
			else
				html.setFile("data/html/default/" + getNpcId() + "-lowfame.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
		}
		else if(actualCommand.equalsIgnoreCase("CRP"))
		{
			if(player.getFame() >= 1000 && player.getClassId().level() >= 2 && player.getClan() != null && player.getClan().getLevel() >= 5)
			{
				player.setFame(player.getFame() - 1000);
				player.getClan().setReputationScore(player.getClan().getReputationScore() + 50);
				player.getClan().broadcastToOnlineMembers(new PledgeShowInfoUpdate(player.getClan()));
				player.sendPacket(new SystemMessage(2326));
				html.setFile("data/html/default/" + getNpcId() + "-5.htm");
			}
			else
				html.setFile("data/html/default/" + getNpcId() + "-lowfame.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
