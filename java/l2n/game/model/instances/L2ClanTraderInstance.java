package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.PledgeShowInfoUpdate;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 22.02.2010
 * @time 1:40:46
 */
public final class L2ClanTraderInstance extends L2NpcInstance
{
	private static final int BLOOD_OATH = 9910;
	private static final int BLOOD_ALLIANCE = 9911;
	private static final int KNIGHTS_EPAULETTE = 9912;

	public L2ClanTraderInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		NpcHtmlMessage html = new NpcHtmlMessage(player, this);

		if(command.equalsIgnoreCase("crp"))
		{
			if(player.getClan() != null && player.getClan().getLevel() > 4)
				html.setFile("data/html/default/" + getNpcId() + "-2.htm");
			else
				html.setFile("data/html/default/" + getNpcId() + "-1.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
		}
		else if(command.startsWith("exchange"))
		{
			int itemId = Integer.parseInt(command.substring(9).trim());

			int reputation = 0;
			long itemCount = 0;

			L2ItemInstance item = player.getInventory().getItemByItemId(itemId);
			long playerItemCount = item == null ? 0 : item.getCount();

			switch (itemId)
			{
				case BLOOD_ALLIANCE:
					reputation = 500;
					itemCount = 1;
					break;
				case BLOOD_OATH:
					reputation = 200;
					itemCount = 10;
					break;
				case KNIGHTS_EPAULETTE:
					reputation = 20;
					itemCount = 100;
			}

			if(playerItemCount >= itemCount)
			{
				player.getInventory().destroyItemByItemId(itemId, itemCount, true);
				player.getClan().incReputation(reputation, false, "ClanTrader " + itemId + " from " + player.getName());
				player.getClan().broadcastToOnlineMembers(new PledgeShowInfoUpdate(player.getClan()));
				player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ClanTraderInstance", player).addNumber(reputation));

				html.setFile("data/html/default/" + getNpcId() + "-ExchangeSuccess.htm");
			}
			else
				html.setFile("data/html/default/" + getNpcId() + "-ExchangeFailed.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
