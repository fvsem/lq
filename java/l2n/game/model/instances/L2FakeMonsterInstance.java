package l2n.game.model.instances;

import l2n.Config;
import l2n.game.custom.TheGhostEngine;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.GmListTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Log;

/**
 * This instance is used by BotTracker.
 * 
 * @author L2System Project
 * @date 11.04.2011
 * @time 17:27:50
 */
public class L2FakeMonsterInstance extends L2MonsterInstance
{
	private long _ownerStoredId = 0;

	public L2FakeMonsterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onSpawn()
	{
		setFlying(true);
		getAI().stopAITask();
		super.onSpawn();
	}

	/**
	 * This method is executed when targeting a Fake Monster, which is not possible in any way by using normal client.
	 */
	@Override
	public void onAction(L2Player activeChar, boolean shift)
	{
		deleteMe();

		GmListTable.broadcastMessageToGMs("TheGhostEngine - Player " + activeChar.getName() + " has been detected farming with third party software.");

		if(Config.GHOST_ENGINE_LOG_BOTS_DETECTIONS_AND_PUNISHMENTS)
			Log.add(activeChar.toString() + " has been detected using third party software to farm.", "TheGhostEngineDetections");

		// Let's play with this motherfucker a bit, and make him wonder...^1
		if(Config.GHOST_ENGINE_ANTIBOT_ALLOW_BAN)
		{
			if(Config.GHOST_ENGINE_LOG_BOTS_DETECTIONS_AND_PUNISHMENTS)
				Log.add("Character: " + activeChar.getName() + ", Account: " + activeChar.getAccountName() + " has been added to abusers lists.", "TheGhostEngineDetections");

			// помечаем как не забанненого, больше проверка не будет проходить на него
			activeChar.setCaughtByRabbits(true);
			TheGhostEngine.addThirdPartySoftwareAbuser(activeChar, "'This faggot uses third party software to automate farming.'");
		}
	}

	public L2Player getOwner()
	{
		L2Player player;
		if((player = L2ObjectsStorage.getAsPlayer(_ownerStoredId)) != null)
			return player;
		return null;
	}

	public void setOwner(final L2Player o)
	{
		_ownerStoredId = o.getStoredId();
	}
}
