package l2n.game.model.instances;

import l2n.Config;
import l2n.game.model.L2Multisell;
import l2n.game.model.actor.L2Player;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;
import java.util.StringTokenizer;

public class L2PetManagerInstance extends L2MerchantInstance
{
	public L2PetManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		File file = new File(Config.DATAPACK_ROOT, "data/html/petmanager/" + pom + ".htm");
		if(file.exists())
			return "data/html/petmanager/" + pom + ".htm";
		return "data/html/teleporter/" + pom + ".htm";
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken(); // Get actual command

		if(actualCommand.equalsIgnoreCase("Buy"))
		{
			if(st.countTokens() < 1)
				return;
			int val = Integer.parseInt(st.nextToken());
			showShopWindow(player, val, false);
		}
		else if(actualCommand.equalsIgnoreCase("Sell"))
			showShopWindow(player);
		else if(actualCommand.equalsIgnoreCase("Multisell"))
		{
			if(st.countTokens() < 1)
				return;
			int val = Integer.parseInt(st.nextToken());
			L2Multisell.getInstance().separateAndSend(val, player, getCastle() != null ? getCastle().getTaxRate() : 0);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
