package l2n.game.model.instances;

import javolution.text.TextBuilder;
import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Events;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.L2CharacterAI;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.BypassHandler;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.*;
import l2n.game.instancemanager.boss.BelethManager;
import l2n.game.model.L2ObjectTasks.NotifyFaction;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.recorder.NpcStatsChangeRecorder;
import l2n.game.model.base.ClassId;
import l2n.game.model.base.L2PledgeSkillLearn;
import l2n.game.model.base.Race;
import l2n.game.model.entity.Hero;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestEventType;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.*;
import l2n.game.scripts.CharacterAILoader;
import l2n.game.skills.Stats;
import l2n.game.tables.DynamicRateTable;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.game.tables.TeleportTable.TeleportLocation;
import l2n.game.taskmanager.DecayTaskManager;
import l2n.game.taskmanager.RandomAnimationManager;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2NpcTemplate;
import l2n.game.templates.L2Weapon;
import l2n.util.*;

import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;
import static l2n.game.ai.CtrlIntention.AI_INTENTION_ATTACK;

public class L2NpcInstance extends L2Character
{
	protected static final Logger _log = Logger.getLogger(L2NpcInstance.class.getName());

	public static final class AggroInfo
	{
		public L2Playable attacker;
		public int hate;
		public int damage;

		public AggroInfo(final L2Playable attacker)
		{
			this.attacker = attacker;
		}

		@Override
		public String toString()
		{
			return attacker + " [" + hate + "/" + damage + "]";
		}
	}

	private static final int[][] _mageBuff = {
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4322, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4323, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 5637, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4328, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4329, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4330, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4331, 1 },
			{ 16, 34, 4338, 1 } };

	private static final int[][] _warrBuff = {
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4322, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4323, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 5637, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4324, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4325, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4326, 1 },
			{ 6, 39, 4327, 1 },
			{ 40, Config.ALT_BUFF_MAX_LEVEL, 5632, 1 },
			{ 16, 34, 4338, 1 } };

	private static final int[][] _summonBuff = {
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4322, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4323, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 5637, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4324, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4325, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4326, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4328, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4329, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4330, 1 },
			{ 6, Config.ALT_BUFF_MAX_LEVEL, 4331, 1 },
			{ 6, 39, 4327, 1 },
			{ 40, Config.ALT_BUFF_MAX_LEVEL, 5632, 1 } };

	protected static final int COND_ALL_FALSE = 0;
	protected static final int COND_BUSY_BECAUSE_OF_SIEGE = 1;
	protected static final int COND_OWNER = 2;
	protected static final int COND_CLAN_WPRIVS = 3;
	protected static final int COND_OWNER2 = 4;

	protected static final int COND_CASTLE_ATTACKER = 1;
	protected static final int COND_CASTLE_DEFENDER = 3;

	private static final String teleport_list = Files.read("data/html/common/teleport_list.htm");

	private final ClassId[] _classesToTeach;

	/** The delay after which the attached is stopped */
	private long _attack_timeout;
	private Location _spawnedLoc = new Location();

	private final ReentrantLock getAiLock = new ReentrantLock();
	private final ReentrantLock decayLock = new ReentrantLock();

	protected boolean _unAggred = false;
	private L2Spawn _spawn;
	private boolean _isDecayed = false;
	private Future<?> _decayTask;

	private int _displayId = 0;

	public int minFactionNotifyInterval = 10000;
	private long lastFactionNotifyTime = 0;
	private int _personalAggroRange = -1;

	private byte _level = 0;

	private int _currentLHandId;
	private int _currentRHandId;

	private double _currentCollisionRadius;
	private double _currentCollisionHeight;

	private int _fortressId = -1;
	private int _clanHallId = -1;

	private long _lastSocialAction;
	private boolean _isBusy;
	public boolean hasChatWindow = true;
	protected long _dieTime = 0;
	private String _busyMessage = "";

	/**
	 * Нужно для отображения анимации спауна, используется в пакете NpcInfo:
	 * 0=false, 1=true, 2=summoned (only works if model has a summon animation)
	 **/
	private int _showSpawnAnimation = 2;

	private volatile boolean _inAnimationManager = false;

	// Hides name completly
	private boolean _hideName = false;
	// Shows name at half height of the npc
	private boolean _halfHeightName = false;

	private final boolean _isEpicBoss;

	public L2NpcInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);

		if(template == null)
		{
			_log.warning("No template for Npc. Please check your datapack is setup correctly.");
			throw new IllegalArgumentException();
		}

		_classesToTeach = template.getTeachInfo();
		setName(template.name);
		setTitle(template.title);

		setFlying(template.getAIParams().getBool(AiOptionsType.IS_FLYING, false));

		if(!template.ai_type.equalsIgnoreCase("npc") && !CharacterAILoader.getInstance().isAvailable(template.ai_type))
			_log.warning("L2NpcInstance: AI type '" + template.ai_type + "' not found!");

		// инициализация параметров оружия
		_currentLHandId = getTemplate().lhand;
		_currentRHandId = getTemplate().rhand;

		// инициализация коллизий
		_currentCollisionHeight = getTemplate().collisionHeight;
		_currentCollisionRadius = getTemplate().collisionRadius;

		_isEpicBoss = Config.RATE_DROP_EPIC_RAIDBOSS_IDs.contains(template.npcId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IHardReference<L2NpcInstance> getRef()
	{
		return (IHardReference<L2NpcInstance>) super.getRef();
	}

	@Override
	public L2CharacterAI getAI()
	{
		if(_ai != null)
			return _ai;

		getAiLock.lock();
		try
		{
			if(_ai == null)
			{
				_ai = CharacterAILoader.getInstance().getAI(getTemplate().ai_type, this);
				if(_ai == null)
					_ai = new L2CharacterAI(this);
			}
		}
		finally
		{
			getAiLock.unlock();
		}
		return _ai;
	}

	public void setAttackTimeout(final long time)
	{
		_attack_timeout = time;
	}

	public long getAttackTimeout()
	{
		return _attack_timeout;
	}

	/**
	 * Return the position of the spawned point.<BR>
	 * <BR>
	 */
	public Location getSpawnedLoc()
	{
		return _spawnedLoc;
	}

	public void setSpawnedLoc(final Location loc)
	{
		_spawnedLoc = loc;
	}

	public int getRightHandItem()
	{
		return _currentRHandId;
	}

	public int getLeftHandItem()
	{
		return _currentLHandId;
	}

	public void setLHandId(final int newWeaponId)
	{
		_currentLHandId = newWeaponId;
	}

	public void setRHandId(final int newWeaponId)
	{
		_currentRHandId = newWeaponId;
	}

	/**
	 * @return Returns the zOffset.
	 */
	public double getCollisionHeight()
	{
		return _currentCollisionHeight;
	}

	/**
	 * @param offset
	 *            The zOffset to set.
	 */
	public void setCollisionHeight(final double offset)
	{
		_currentCollisionHeight = offset;
	}

	/**
	 * @return Returns the collisionRadius.
	 */
	public double getCollisionRadius()
	{
		return _currentCollisionRadius;
	}

	/**
	 * @param collisionRadius
	 *            The collisionRadius to set.
	 */
	public void setCollisionRadius(final double collisionRadius)
	{
		_currentCollisionRadius = collisionRadius;
	}

	/**
	 * Kill the L2NpcInstance (the corpse disappeared after 7 seconds), distribute rewards (EXP, SP, Drops...) and notify Quest Engine.
	 */
	@Override
	public void doDie(final L2Character killer)
	{
		_dieTime = System.currentTimeMillis();
		setDecayed(false);

		if(isMonster() && (((L2MonsterInstance) this).isSeeded() || ((L2MonsterInstance) this).isSpoiled()))
			startDecay(20000L);
		else if(isBoss() || getNpcId() == BelethManager.BELETH_NPCID)
			startDecay(20000L);
		else if(isFlying())
			startDecay(4500L);
		else
			startDecay(8500L);

		// установка параметров оружия и коллизий по умолчанию
		_currentLHandId = getTemplate().lhand;
		_currentRHandId = getTemplate().rhand;
		_currentCollisionHeight = getTemplate().collisionHeight;
		_currentCollisionRadius = getTemplate().collisionRadius;

		clearAggroList(false);

		super.doDie(killer);
	}

	/** Возвращает сколько времени прошло со смерти НПС/Монстра */
	public long getDeadTime()
	{
		if(_dieTime <= 0)
			return 0;
		return System.currentTimeMillis() - _dieTime;
	}

	public void callFriends(final L2Character attacker)
	{
		callFriends(attacker, 0);
	}

	public void callFriends(final L2Character attacker, final int damage)
	{
		if(System.currentTimeMillis() - lastFactionNotifyTime > minFactionNotifyInterval)
		{
			if(isMonster())
				if(isMinion())
				{
					final L2MonsterInstance master = ((L2MinionInstance) this).getLeader();
					if(master != null)
					{
						// Если РБ ещё не бьётся и не мертв агрим его
						if(!master.isInCombat() && !master.isDead())
							master.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));

						// Агрим всех миньёнов
						final MinionList list = master.getMinionList();
						if(list != null)
							for(final L2MinionInstance m : list.getSpawnedMinions())
								if(m != this)
									m.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));
					}
				}
				else
					callMinionsToAssist(attacker);

			// call friend's
			if(getFactionId() != null && !getFactionId().isEmpty())
				L2GameThreadPools.getInstance().scheduleAi(new NotifyFaction(this, attacker, damage), 100, false);
			lastFactionNotifyTime = System.currentTimeMillis();
		}
	}

	protected int getMinFactionNotifyInterval()
	{
		return minFactionNotifyInterval;
	}

	public GArray<L2NpcInstance> getActiveFriends(final boolean active, final boolean attack)
	{
		final GArray<L2NpcInstance> ActiveFriends = new GArray<L2NpcInstance>();
		for(final L2NpcInstance obj : getActiveFriends(true))
			if(attack && obj.getAI().getIntention() == AI_INTENTION_ATTACK || active && obj.getAI().getIntention() == AI_INTENTION_ACTIVE)
				ActiveFriends.add(obj);
		return ActiveFriends;
	}

	public GArray<L2NpcInstance> getActiveFriends(final boolean check_canSeeTarget)
	{
		final L2WorldRegion region = L2World.getRegion(this);
		if(region != null && region.getObjectsSize() > 0)
		{
			final GArray<L2NpcInstance> activeFriends = new GArray<L2NpcInstance>();
			for(final L2NpcInstance obj : region.getNpcsList(new GArray<L2NpcInstance>(region.getObjectsSize()), getObjectId(), getReflection().getId()))
				if(obj != null && !obj.isDead() && (!check_canSeeTarget || GeoEngine.canSeeTarget(this, obj, false)))
					activeFriends.add(obj);
			return activeFriends;
		}
		return GArray.emptyCollection();
	}

	public HashMap<L2Playable, AggroInfo> getAggroMap()
	{
		final HashMap<L2Playable, AggroInfo> temp = new HashMap<L2Playable, AggroInfo>();
		for(final L2Playable playable : L2World.getAroundPlayables(this))
			if(playable != null)
			{
				final HateInfo hateInfo = playable.getHateList().get(this);
				if(hateInfo != null)
				{
					final AggroInfo aggroInfo = new AggroInfo(playable);
					aggroInfo.hate = hateInfo.hate;
					aggroInfo.damage = hateInfo.damage;
					temp.put(playable, aggroInfo);
				}
			}
		return temp;
	}

	public GArray<AggroInfo> getAggroList()
	{
		final GArray<AggroInfo> temp = new GArray<AggroInfo>();
		for(final L2Playable playable : L2World.getAroundPlayables(this))
			if(playable != null)
			{
				final HateInfo hateInfo = playable.getHateList().get(this);
				if(hateInfo != null)
				{
					final AggroInfo aggroInfo = new AggroInfo(playable);
					aggroInfo.hate = hateInfo.hate;
					aggroInfo.damage = hateInfo.damage;
					temp.add(aggroInfo);
				}
			}
		return temp;
	}

	public int getAggroListSize()
	{
		return getAggroList().size();
	}

	public void clearAggroList(final boolean onlyHate)
	{
		for(final L2Character cha : L2World.getAroundCharacters(this))
			if(cha != null)
				cha.removeFromHatelist(this, onlyHate);
	}

	public L2Character getMostHated()
	{
		final L2Character target = getAI().getAttackTarget();
		if(target != null && target.isNpc() && target.isVisible() && target != this && !target.isDead() && target.isInRange(this, 2000))
			return target;

		GArray<AggroInfo> aggroList = getAggroList();

		final GArray<AggroInfo> activeList = new GArray<AggroInfo>();
		final GArray<AggroInfo> passiveList = new GArray<AggroInfo>();

		L2Playable cha;
		for(final AggroInfo ai : aggroList)
			if(ai.hate > 0)
			{
				cha = ai.attacker;
				if(cha != null)
					if(!cha.isSummon() && (cha.isStunned() || cha.isSleeping() || cha.isParalyzed() || cha.isAfraid() || cha.isBlocked()))
						passiveList.add(ai);
					else
						activeList.add(ai);
			}

		if(!activeList.isEmpty())
			aggroList = activeList;
		else
			aggroList = passiveList;

		AggroInfo mosthated = null;

		for(final AggroInfo ai : aggroList)
			if(mosthated == null)
				mosthated = ai;
			else if(mosthated.hate < ai.hate)
				mosthated = ai;

		return mosthated != null && mosthated.hate > 0 ? mosthated.attacker : null;
	}

	public L2Character getRandomHated()
	{
		GArray<AggroInfo> aggroList = getAggroList();

		final GArray<AggroInfo> activeList = new GArray<AggroInfo>();
		final GArray<AggroInfo> passiveList = new GArray<AggroInfo>();

		L2Playable cha;
		for(final AggroInfo ai : aggroList)
			if(ai.hate > 0)
			{
				cha = ai.attacker;
				if(cha != null)
					if(cha.isStunned() || cha.isSleeping() || cha.isParalyzed() || cha.isAfraid() || cha.isBlocked() || Math.abs(cha.getZ() - getZ()) > 200)
						passiveList.add(ai);
					else
						activeList.add(ai);
			}

		if(!activeList.isEmpty())
			aggroList = activeList;
		else
			aggroList = passiveList;

		if(!aggroList.isEmpty())
			return aggroList.get(Rnd.get(aggroList.size())).attacker;
		return null;
	}

	public boolean isNoTarget()
	{
		return getAggroList().size() == 0;
	}

	public void dropItem(final L2Player lastAttacker, final int itemId, final long itemCount)
	{
		if(itemCount == 0 || lastAttacker == null)
			return;

		if(Config.DROP_COUNTER)
			lastAttacker.incrementDropCounter(itemId, itemCount);

		L2ItemInstance item;
		for(long i = 1; i <= itemCount; i++)
		{
			item = ItemTable.getInstance().createItem(itemId, lastAttacker.getObjectId(), getNpcId(), "L2NpcInstance.dropItem");

			// Set the Item quantity dropped if L2ItemInstance is stackable
			if(item.isStackable())
			{
				i = itemCount; // Set so loop won't happent again
				item.setCount(itemCount); // Set item count
			}

			if(isRaid() || this instanceof L2ReflectionBossInstance)
			{
				SystemMessage sm;
				if(itemId == 57)
				{
					sm = new SystemMessage(SystemMessage.S1_DIED_AND_HAS_DROPPED_S2_ADENA);
					sm.addString(getName());
					sm.addNumber(item.getCount());
				}
				else
				{
					sm = new SystemMessage(SystemMessage.S1_DIED_AND_DROPPED_S3_S2);
					sm.addString(getName());
					sm.addItemName(itemId);
					sm.addNumber(item.getCount());
				}
				broadcastPacket(sm);
			}

			lastAttacker.doAutoLootOrDrop(item, this);
		}
	}

	public void dropItem(final L2Player lastAttacker, final L2ItemInstance item)
	{
		if(item.getCount() == 0)
			return;

		if(isRaid() || this instanceof L2ReflectionBossInstance)
		{
			SystemMessage sm;
			if(item.getItemId() == 57)
			{
				sm = new SystemMessage(SystemMessage.S1_DIED_AND_HAS_DROPPED_S2_ADENA);
				sm.addString(getName());
				sm.addNumber(item.getCount());
			}
			else
			{
				sm = new SystemMessage(SystemMessage.S1_DIED_AND_DROPPED_S3_S2);
				sm.addString(getName());
				sm.addItemName(item.getItemId());
				sm.addNumber(item.getCount());
			}
			broadcastPacket(sm);
		}

		lastAttacker.doAutoLootOrDrop(item, this);
	}

	public L2ItemInstance getActiveWeapon()
	{
		return null;
	}

	@Override
	public boolean isAttackable(final L2Character attacker)
	{
		return true;
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return false;
	}

	@Override
	public void onSpawn()
	{
		setDecayed(false);
		_dieTime = 0;
	}

	@Override
	protected void onDespawn()
	{
		getAggroList().clear();

		getAI().onEvtDeSpawn();
		getAI().stopAITask();
		getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
		// FIXME stopRandomAnimation();

		super.onDespawn();
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();
		getAI().notifyEvent(CtrlEvent.EVT_SPAWN);
	}

	@Override
	public L2NpcTemplate getTemplate()
	{
		return (L2NpcTemplate) _template;
	}

	@Override
	public int getNpcId()
	{
		return getTemplate().npcId;
	}

	public void setUnAggred(final boolean state)
	{
		_unAggred = state;
	}

	/**
	 * Return True if the L2NpcInstance is aggressive (ex : L2MonsterInstance in function of aggroRange).<BR>
	 * <BR>
	 */
	public boolean isAggressive()
	{
		// может ли чемпион быть агресивным
		if(getChampion() > 0 && !Config.CHAMPION_CAN_BE_AGGRO)
			return false;
		return getAggroRange() > 0;
	}

	/**
	 * @return level 1 - слабый чемпиона, 2 - сильный чемпион, 0 - не чемпион вовсе
	 */
	public int getChampion()
	{
		return 0;
	}

	public int getAggroRange()
	{
		if(_unAggred)
			return 0;

		if(_personalAggroRange >= 0)
			return _personalAggroRange;

		return getTemplate().aggroRange;
	}

	/**
	 * Устанавливает данному npc новый aggroRange.
	 * Если установленый aggroRange < 0, то будет братся аггрорейндж с темплейта.
	 * 
	 * @param aggroRange
	 *            новый agrroRange
	 */
	public void setAggroRange(final int aggroRange)
	{
		_personalAggroRange = aggroRange;
	}

	public int getFactionRange()
	{
		return getTemplate().factionRange;
	}

	public String getFactionId()
	{
		return getTemplate().factionId;
	}

	public long getExpReward()
	{
		return (long) calcStat(Stats.EXP, getTemplate().revardExp, null, null);
	}

	public long getSpReward()
	{
		return (long) calcStat(Stats.SP, getTemplate().revardSp, null, null);
	}

	/**
	 * Remove PROPERLY the L2NpcInstance from the world.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Remove the L2NpcInstance from the world and update its spawn object</li> <li>Remove L2Object object from _allObjects of L2World</li><BR>
	 */
	@Override
	public void deleteMe()
	{
		super.deleteMe();
		stopDecay();
		if(_spawn != null)
			_spawn.stopRespawn();
		setSpawn(null);
	}

	public L2Spawn getSpawn()
	{
		return _spawn;
	}

	public void setSpawn(final L2Spawn spawn)
	{
		_spawn = spawn;
	}

	@Override
	public void onDecay()
	{
		decayLock.lock();
		try
		{
			if(isDecayed())
				return;

			setDecayed(true);

			// Remove the L2NpcInstance from the world when the decay task is launched
			super.onDecay();

			// Decrease its spawn counter
			// Проверяем getNativeRespawnDelay для удаления неправильно заспауненых нпс/монстров которых после спауна нужно удалить из хранилища
			if(_spawn != null && _spawn.getNativeRespawnDelay() > 0)
				_spawn.decreaseCount(this);
			else
				deleteMe(); // Если этот моб заспавнен не через стандартный механизм спавна значит посмертие ему не положено и он умирает насовсем
		}
		finally
		{
			decayLock.unlock();
		}
	}

	/**
	 * Set the decayed state of this L2NpcInstance<BR>
	 * <BR>
	 */
	public final void setDecayed(final boolean mode)
	{
		_isDecayed = mode;
	}

	/**
	 * Return the decayed status of this L2NpcInstance<BR>
	 * <BR>
	 */
	public final boolean isDecayed()
	{
		return _isDecayed;
	}

	/**
	 * Запустить задачу "исчезновения" после смерти
	 */
	protected void startDecay(final long delay)
	{
		stopDecay();
		_decayTask = DecayTaskManager.getInstance().addDecayTask(this, delay);
	}

	/**
	 * Отменить задачу "исчезновения" после смерти
	 */
	public void stopDecay()
	{
		if(_decayTask != null)
		{
			_decayTask.cancel(false);
			_decayTask = null;
		}
	}

	/**
	 * Отменить и завершить задачу "исчезновения" после смерти
	 */
	public void endDecayTask()
	{
		if(_decayTask != null)
		{
			_decayTask.cancel(false);
			_decayTask = null;
		}
		onDecay();
	}

	@Override
	public boolean isUndead()
	{
		return getTemplate().isUndead();
	}

	public void setLevel(final byte level)
	{
		_level = level;
	}

	@Override
	public byte getLevel()
	{
		return _level == 0 ? getTemplate().level : _level;
	}

	public void setDisplayId(final int displayId)
	{
		_displayId = displayId;
	}

	public int getDisplayId()
	{
		return _displayId > 0 ? _displayId : getTemplate().displayId;
	}

	/**
	 * Return null (regular NPCs don't have weapons instancies).<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		// regular NPCs dont have weapons instancies
		return null;
	}

	/**
	 * Return the weapon item equipped in the right hand of the L2NpcInstance or null.<BR>
	 * <BR>
	 */
	@Override
	public L2Weapon getActiveWeaponItem()
	{
		// Get the weapon identifier equipped in the right hand of the L2NpcInstance
		final int weaponId = getTemplate().rhand;

		if(weaponId < 1)
			return null;

		// Get the weapon item equipped in the right hand of the L2NpcInstance
		final L2Item item = ItemTable.getInstance().getTemplate(getTemplate().rhand);

		if(!(item instanceof L2Weapon))
			return null;

		return (L2Weapon) item;
	}

	/**
	 * Return null (regular NPCs don't have weapons instances).<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		// regular NPCs dont have weapons instances
		return null;
	}

	/**
	 * Return the weapon item equipped in the left hand of the L2NpcInstance or null.<BR>
	 * <BR>
	 */
	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		// Get the weapon identifier equipped in the right hand of the L2NpcInstance
		final int weaponId = getTemplate().lhand;

		if(weaponId < 1)
			return null;

		// Get the weapon item equipped in the right hand of the L2NpcInstance
		final L2Item item = ItemTable.getInstance().getTemplate(getTemplate().lhand);

		if(!(item instanceof L2Weapon))
			return null;

		return (L2Weapon) item;
	}

	@Override
	public void sendChanges()
	{
		if(isFlying()) // FIXME
			return;
		super.sendChanges();
	}

	private ScheduledFuture<?> _broadcastCharInfoTask;

	public class BroadcastCharInfoTask implements Runnable
	{
		@Override
		public void run()
		{
			broadcastCharInfoImpl();
			_broadcastCharInfoTask = null;
		}
	}

	@Override
	public void broadcastCharInfo()
	{
		if(!isVisible())
			return;

		if(_broadcastCharInfoTask != null)
			return;

		_broadcastCharInfoTask = L2GameThreadPools.getInstance().scheduleAi(new BroadcastCharInfoTask(), Config.BROADCAST_CHAR_INFO_INTERVAL, false);
	}

	public void broadcastCharInfoImpl()
	{
		for(final L2Player player : L2World.getAroundPlayers(this))
			player.sendPacket(new NpcInfo(this, player));
	}

	// У NPC всегда 2
	public void onRandomAnimation()
	{
		setAnimationManagerStatus(false);
		broadcastPacket(new SocialAction(getObjectId(), 2));
		_lastSocialAction = System.currentTimeMillis();
	}

	public void startRandomAnimation()
	{
		if(!hasRandomAnimation() || _inAnimationManager)
			return;

		setAnimationManagerStatus(true);
		RandomAnimationManager.getInstance().addAnimationTask(this);
	}

	public void setAnimationManagerStatus(final boolean value)
	{
		_inAnimationManager = value;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		if(Config.MAX_NPC_ANIMATION <= 0)
			return false;
		if(getTemplate().getAIParams().getBool(AiOptionsType.RANDOM_ANIMATION_DISABLED, false))
			return false;
		return true;
	}

	/**
	 * Возвращает falsa если текущий и примыкающий к нему регионы пусты (в них нет игроков)
	 */
	public boolean isInActiveRegion()
	{
		final L2WorldRegion region = getCurrentRegion();
		return region != null && !region.areNeighborsEmpty();
	}

	@Override
	public boolean isInvul()
	{
		return true;
	}

	public Castle getCastle()
	{
		return TownManager.getInstance().getClosestTown(this).getCastle();
	}

	public Castle getCastle(final L2Player player)
	{
		return getCastle();
	}

	/** Return the Fortress this L2NpcInstance belongs to. */
	public Fortress getFortress()
	{
		if(_fortressId < 0)
			_fortressId = FortressManager.getInstance().findNearestFortressIndex(getX(), getY(), Short.MAX_VALUE);
		return FortressManager.getInstance().getFortressByIndex(_fortressId);
	}

	/** Return the L2ClanHall this L2NpcInstance belongs to. */
	public ClanHall getClanHall()
	{
		if(_clanHallId < 0)
			_clanHallId = ClanHallManager.getInstance().findNearestClanHallIndex(getX(), getY(), Short.MAX_VALUE);
		return ClanHallManager.getInstance().getClanHall(_clanHallId);
	}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		if(getTemplate().getAIParams().getBool(AiOptionsType.ON_ACTION_DISABLED, false))
		{
			player.sendActionFailed();
			return;
		}

		// при любых видах манипуляций с Матрасом телепортирует в Heine
		if(getNpcId() == 32245 && !player.isGM() && !ServerVariables.getBool("HellboundOpen", false))
		{
			player.teleToLocation(TownManager.getInstance().getTown(13).getSpawn());
			player.sendActionFailed();
			return;
		}

		player.setLastNpc(this);
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
			{
				player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
				if(isAutoAttackable(player))
					player.sendPacket(makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
			}
			player.sendPacket(new ValidateLocation(this), Msg.ActionFail);
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(isAutoAttackable(player))
		{
			player.getAI().Attack(this, false, shift);
			return;
		}

		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}

		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0 && !(player.getTarget() instanceof L2ClanHallManagerInstance) && !(player.getTarget() instanceof L2ClanHallDoormenInstance) && !(player.getTarget() instanceof L2CastleWarehouseInstance) && !(player.getTarget() instanceof L2CastleDoormenInstance) && !(player.getTarget() instanceof L2CastleMagicianInstance) && !(player.getTarget() instanceof L2CastleTeleporterInstance) && !(player.getTarget() instanceof L2CastleChamberlainInstance) && !player.isGM())
		{
			player.sendActionFailed();
			return;
		}

		// С NPC нельзя разговаривать мертвым и сидя
		if(!Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || player.isAlikeDead())
		{
			player.sendActionFailed();
			return;
		}

		if(System.currentTimeMillis() - _lastSocialAction > 10000 && !getTemplate().getAIParams().getBool(AiOptionsType.RANDOM_ANIMATION_DISABLED, false))
		{
			broadcastPacket(new SocialAction(getObjectId(), 2));
			_lastSocialAction = System.currentTimeMillis();
		}

		player.sendActionFailed();

		if(_isBusy)
			showBusyWindow(player);
		else if(hasChatWindow)
		{
			boolean flag = false;
			final Quest[] qlst = getTemplate().getEventQuests(QuestEventType.NPC_FIRST_TALK);

			if(qlst != null && qlst.length > 0)
			{
				QuestState qs;
				for(final Quest element : qlst)
				{
					qs = player.getQuestState(element.getName());
					if((qs == null || !qs.isCompleted()) && element.notifyFirstTalk(this, player))
						flag = true;
				}
			}
			if(!flag)
				showChatWindow(player, 0);
		}
	}

	public void showQuestWindow(final L2Player player, final String questId)
	{
		if(!player.isQuestContinuationPossible(true))
			return;

		try
		{
			// Get the state of the selected quest
			QuestState qs = player.getQuestState(questId);
			if(qs != null)
			{
				if(qs.isCompleted())
				{
					Functions.show(new CustomMessage("quests.QuestAlreadyCompleted", player), player);
					return;
				}
				if(qs.getQuest().notifyTalk(this, qs))
					return;
			}
			else
			{
				final Quest q = QuestManager.getQuest(questId);
				if(q != null)
				{
					// check for start point
					final Quest[] qlst = getTemplate().getEventQuests(QuestEventType.QUEST_START);
					if(qlst != null && qlst.length > 0)
						for(final Quest element : qlst)
							if(element == q)
							{
								qs = q.newQuestState(player, Quest.CREATED);
								if(qs.getQuest().notifyTalk(this, qs))
									return;
								break;
							}
				}
			}
			showChatWindow(player, "data/html/no-quest.htm");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "problem with npc text " + e, e);
		}

		player.sendActionFailed();
	}

	public static boolean canBypassCheck(final L2Player player, final L2NpcInstance npc)
	{
		if(npc == null || player.isActionsDisabled() || !Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || !npc.isInRange(player, L2Character.INTERACTION_DISTANCE))
		{
			player.sendActionFailed();
			return false;
		}
		return true;
	}

	/**
	 * Open a quest or chat window on client with the text of the L2NpcInstance in function of the command.<BR>
	 * <BR>
	 * <B><U> Example of use </U> :</B><BR>
	 * <BR>
	 * <li>Client packet : RequestBypassToServer</li><BR>
	 * <BR>
	 * 
	 * @param command
	 *            The command string received from client
	 */
	public void onBypassFeedback(final L2Player player, final String command)
	{
		 if(!canBypassCheck(player,this)) return;
		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
			return;
		}

		final IBypassHandler byp = BypassHandler.getInstance().getBypassHandler(command);
		if(byp != null)
			byp.useBypass(command, player, this);
	}

	public void showTeleportList(final L2Player player, final TeleportLocation[] list)
	{
		final StringBuilder tplist = StringUtil.startAppend(300);
		if(list != null && player.getPlayerAccess().UseTeleport)
		{
			// TODO генерировать страницу при загрузке списка
			for(final TeleportLocation tl : list)
				if(tl._item.getItemId() == 57)
				{
					double pricemod = player.getLevel() <= Config.GATEKEEPER_FREE ? 0 : Config.GATEKEEPER_MODIFIER;
					if(tl._price > 0 && pricemod > 0 && Config.USE_GK_NIGHT_PRICE)
					{
						final int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
						final int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
						if(day != Calendar.SUNDAY && day != Calendar.SATURDAY && (hour <= 12 || hour >= 22))
							pricemod /= 2;
					}
					StringUtil.append(tplist, "<a action=\"bypass -h npc_", getObjectId(), "_teleport ", tl._target, " ", (int) (tl._price * pricemod), "\" msg=\"811;", tl.getName(player), "\">", tl.getName(player));
					if(tl._price > 0 && pricemod > 0)
						StringUtil.append(tplist, " - ", (int) (tl._price * pricemod), player.isLangRus() ? " аден" : " adena");
					StringUtil.append(tplist, "</a><br1>");
				}
				else
					StringUtil.append(tplist, "<a action=\"bypass -h npc_", getObjectId(), "_quest_teleport ", tl._target, " ", tl._price, " ", tl._item.getItemId(), "\" msg=\"811;", tl.getName(player), "\">", tl.getName(player), " - ", tl._price, " ", tl._item.getName(), "</a><br1>");

		}
		else
			tplist.append("No teleports available for you.");

		final String content = teleport_list.replace("%teleport_list%", tplist.toString());
		final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setHtml(content);
		player.sendPacket(html);
	}

	public void showQuestWindow(final L2Player player)
	{
		// collect awaiting quests and start points
		final GArray<Quest> options = new GArray<Quest>();

		final GArray<QuestState> awaits = player.getQuestsForEvent(this, QuestEventType.QUEST_TALK);
		final Quest[] starts = getTemplate().getEventQuests(QuestEventType.QUEST_START);

		if(awaits != null)
			for(final QuestState x : awaits)
				if(!options.contains(x.getQuest()))
					if(x.getQuest().getQuestIntId() > 0)
						options.add(x.getQuest());

		if(starts != null)
			for(final Quest x : starts)
				if(!options.contains(x))
					if(x.getQuestIntId() > 0)
						options.add(x);

		// Display a QuestChooseWindow (if several quests are available) or QuestWindow
		if(options.size() > 1)
			showQuestChooseWindow(player, options.toArray(new Quest[options.size()]));
		else if(options.size() == 1)
			showQuestWindow(player, options.get(0).getName());
		else
			showQuestWindow(player, "");
	}

	public void showQuestChooseWindow(final L2Player player, final Quest[] quests)
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("<html><body><title>Talk about:</title><br>");
		for(final Quest q : quests)
			if(player.getQuestState(q.getName()) == null)
				sb.append("<a action=\"bypass -h npc_").append(getObjectId()).append("_Quest ").append(q.getName()).append("\">[").append(q.getDescr(player)).append("]</a><br>");
			else if(player.getQuestState(q.getName()).isCompleted())
				sb.append("<a action=\"bypass -h npc_").append(getObjectId()).append("_Quest ").append(q.getName()).append("\">[").append(q.getDescr(player)).append(" (completed)").append("]</a><br>");
			else
				sb.append("<a action=\"bypass -h npc_").append(getObjectId()).append("_Quest ").append(q.getName()).append("\">[").append(q.getDescr(player)).append(" (in progress)").append("]</a><br>");

		sb.append("</body></html>");

		final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}

	public void showChatWindow(final L2Player player, final int val)
	{
		// Если отключено отображение окна диалога
		if(getTemplate().getAIParams().getBool(AiOptionsType.CHAT_WINDOW_DISABLED, false))
			return;

		final int npcId = getTemplate().npcId;

		/* For use with Seven Signs implementation */
		String filename = SevenSigns.SEVEN_SIGNS_HTML_PATH;
		switch (npcId)
		{
			case 31111: // Gatekeeper Spirit (Disciples)
				final int sealAvariceOwner = SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_AVARICE);
				final int compWinner = SevenSigns.getInstance().getCabalHighestScore();
				final int playerCabal = SevenSigns.getInstance().getPlayerCabal(player);
				if(playerCabal == sealAvariceOwner && playerCabal == compWinner)
					switch (sealAvariceOwner)
					{
						case SevenSigns.CABAL_DAWN:
							filename += "spirit_dawn.htm";
							break;
						case SevenSigns.CABAL_DUSK:
							filename += "spirit_dusk.htm";
							break;
						case SevenSigns.CABAL_NULL:
							filename += "spirit_null.htm";
							break;
					}
				else
					filename += "spirit_null.htm";
				break;
			case 31112: // Gatekeeper Spirit (Disciples)
				filename += "spirit_exit.htm";
				break;
			case 31688:
				if(player.isNoble())
					filename = Olympiad.OLYMPIAD_HTML_PATH + "noble_main.htm";
				else
					filename = getHtmlPath(npcId, val);
				break;
			case 31690:
			case 31769:
			case 31770: // Monument of Heroes
			case 31771:
			case 31772:
				if(player.isHero() || Hero.getInstance().isInactiveHero(player.getObjectId()))
					filename = Olympiad.OLYMPIAD_HTML_PATH + "hero_main.htm";
				else
					filename = getHtmlPath(npcId, val);
				break;
			case 36402:
				if(player.olyBuff == 5)
					filename = Olympiad.OLYMPIAD_HTML_PATH + "olympiad_buffs.htm";
				else if(player.olyBuff <= 4 && player.olyBuff >= 1)
					filename = Olympiad.OLYMPIAD_HTML_PATH + "olympiad_buffs.htm";
				else
					filename = Olympiad.OLYMPIAD_HTML_PATH + "olympiad_nobuffs.htm";
				break;
			default:
				if(npcId >= 31093 && npcId <= 31094 || npcId >= 31172 && npcId <= 31201 || npcId >= 31239 && npcId <= 31254)
					return;
				// Get the text of the selected HTML file in function of the npcId and of the page number
				filename = getHtmlPath(npcId, val);
				break;
		}

		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	public void showChatWindow(final L2Player player, final String filename)
	{
		player.sendPacket(new NpcHtmlMessage(player, this, filename, 0));
	}

	public String getHtmlPath(final int npcId, final int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		String temp = "data/html/default/" + pom + ".htm";
		File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		temp = "data/html/trainer/" + pom + ".htm";
		mainText = new File(temp);
		if(mainText.exists())
			return temp;

		temp = "data/html/lottery/" + pom + ".htm";
		mainText = new File(temp);
		if(mainText.exists())
			return temp;

		temp = "data/html/instance/kamaloka/" + pom + ".htm";
		mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}

	// 0 - buy ticket window
	// 1-20 - buttons
	// 22 - lottery ticket window
	// 21 - selected ticket with 5 numbers
	public void showLotoWindow(final L2Player player, final int val)
	{
		final int npcId = getTemplate().npcId;
		String filename;
		SystemMessage sm;
		final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		// if loto
		if(val == 0)
		{
			filename = getHtmlPath(npcId, 1);
			html.setFile(filename);
		}
		else if(val != 21)
		{
			filename = getHtmlPath(npcId, 5);
			html.setFile(filename);
			int count = 0;
			int found = 0;
			// counting buttons and unsetting button if found
			for(int i = 0; i < 5; i++)
				if(player.getLoto(i) == val)
				{
					// unsetting button
					player.setLoto(i, 0);
					found = 1;
				}
				else if(player.getLoto(i) > 0)
					count++;
			// if not rearched limit 5 and not unseted value
			if(count < 5 && found == 0 && val < 21)
				for(int i = 0; i < 5; i++)
					if(player.getLoto(i) == 0)
					{
						player.setLoto(i, val);
						break;
					}
			// setting pusshed buttons
			count = 0;
			for(int i = 0; i < 5; i++)
				if(player.getLoto(i) > 0)
				{
					count++;
					String button = String.valueOf(player.getLoto(i));
					if(player.getLoto(i) < 10)
						button = "0" + button;
					final String search = "fore=\"L2UI.lottoNum" + button + "\" back=\"L2UI.lottoNum" + button + "a_check\"";
					final String replace = "fore=\"L2UI.lottoNum" + button + "a_check\" back=\"L2UI.lottoNum" + button + "\"";
					html.replace(search, replace);
				}
			if(count == 5)
			{
				final String search = "0\">Return";
				final String replace = "21\">The winner selected the numbers above.";
				html.replace(search, replace);
			}
		}
		else
		{
			final int price = 2000;
			final int lotonumber = 1;
			int enchant = 0;
			int type2 = 0;
			for(int i = 0; i < 5; i++)
			{
				if(player.getLoto(i) == 0)
					return;
				if(player.getLoto(i) < 17)
					enchant += Math.pow(2, player.getLoto(i) - 1);
				else
					type2 += Math.pow(2, player.getLoto(i) - 17);
			}
			if(player.getAdena() < price)
			{
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
				return;
			}
			player.reduceAdena(price, true);
			sm = new SystemMessage(SystemMessage.ACQUIRED__S1_S2);
			sm.addNumber(lotonumber);
			sm.addItemName(4442);
			player.sendPacket(sm);
			final L2ItemInstance item = ItemTable.getInstance().createItem(4442, player.getObjectId(), npcId, "showLotoWindow");
			item.setCustomType1(lotonumber);
			item.setEnchantLevel(enchant);
			item.setCustomType2(type2);
			player.getInventory().addItem(item);
			Log.LogItem(player, Log.BuyItem, item);
			player.getInventory().findItemByItemId(57);
			html.setHtml("<html><body>Lottery Ticket Seller:<br>Thank you for playing the lottery<br>The winners will be announced at ??? 19<br><center><a action=\"bypass -h npc_%objectId%_Chat 0\">Back</a></center></body></html>");
		}
		player.sendPacket(html);
		player.sendActionFailed();
	}

	public void makeSupportMagic(final L2Player player)
	{
		// Prevent a cursed weapon weilder of being buffed
		if(player.isCursedWeaponEquipped())
			return;

		final int lvl = player.getLevel();

		if(lvl < Config.ALT_BUFF_MIN_LEVEL)
		{
			player.sendPacket(new NpcHtmlMessage(player, this, "data/html/default/newbie_nosupport6.htm", 0).replace("%minlevel%", String.valueOf(Config.ALT_BUFF_MIN_LEVEL)));
			return;
		}
		if(lvl > Config.ALT_BUFF_MAX_LEVEL)
		{
			player.sendPacket(new NpcHtmlMessage(player, this, "data/html/default/newbie_nosupport62.htm", 0).replace("%maxlevel%", String.valueOf(Config.ALT_BUFF_MAX_LEVEL)));
			return;
		}

		if(!player.isMageClass() || player.getTemplate().race == Race.orc)
			for(final int[] buff : _warrBuff)
			{
				if(lvl < buff[0] || lvl > buff[1])
					continue;
				broadcastPacket(new MagicSkillUse(this, player, buff[2], buff[3], 0, 0));
				callSkill(SkillTable.getInstance().getInfo(buff[2], buff[3]), true, player);
			}
		else
			for(final int[] buff : _mageBuff)
			{
				if(lvl < buff[0] || lvl > buff[1])
					continue;
				broadcastPacket(new MagicSkillUse(this, player, buff[2], buff[3], 0, 0));
				callSkill(SkillTable.getInstance().getInfo(buff[2], buff[3]), true, player);
			}

		if(!Config.ALT_BUFF_SUMMON || player.getPet() == null || player.getPet().isDead())
			return;

		for(final int[] buff : _summonBuff)
		{
			if(lvl < buff[0] || lvl > buff[1])
				continue;
			broadcastPacket(new MagicSkillUse(this, player.getPet(), buff[2], buff[3], 0, 0));
			callSkill(SkillTable.getInstance().getInfo(buff[2], buff[3]), true, player.getPet());
		}
	}

	/**
	 * Return the busy status of this L2NpcInstance.<BR>
	 * <BR>
	 */
	public final boolean isBusy()
	{
		return _isBusy;
	}

	/**
	 * Set the busy status of this L2NpcInstance.<BR>
	 * <BR>
	 */
	public void setBusy(final boolean isBusy)
	{
		_isBusy = isBusy;
	}

	/**
	 * Return the busy message of this L2NpcInstance.<BR>
	 * <BR>
	 */
	public final String getBusyMessage()
	{
		return _busyMessage;
	}

	/**
	 * Set the busy message of this L2NpcInstance.<BR>
	 * <BR>
	 */
	public void setBusyMessage(final String message)
	{
		_busyMessage = message;
	}

	public void showBusyWindow(final L2Player player)
	{
		final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile("data/html/npcbusy.htm");
		html.replace("%npcname%", getName());
		html.replace("%playername%", player.getName());
		html.replace("%busymessage%", _busyMessage);
		player.sendPacket(html);
	}

	/**
	 * this displays SkillList to the player.
	 * 
	 * @param player
	 */
	public void showSkillList(final L2Player player)
	{
		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			return;
		}

		final ClassId classId = player.getClassId();

		if(classId == null)
			return;

		final int npcId = getTemplate().npcId;

		// повышение уровня сбора камней (NPC Tolonis)
		if(npcId == 32611)
		{
			final L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableSpecialSkills(player);
			final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.COLLECT);

			int counts = 0;
			for(final L2SkillLearn s : skills)
			{
				final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
				if(sk == null)
					continue;

				asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), 0, 1);
				counts++;
			}

			if(counts == 0)
			{
				player.sendPacket(Msg.THERE_ARE_NO_OTHER_SKILLS_TO_LEARN);
				player.sendPacket(AcquireSkillDone.STATIC);
			}
			else
				player.sendPacket(asl);

			player.sendActionFailed();
			return;
		}

		if(_classesToTeach == null)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("I cannot teach you. My class list is empty.<br> Ask admin to fix it. Need add my npcid and classes to skill_learn.sql.<br>NpcId:" + npcId + ", Your classId:" + player.getClassId().getId() + "<br>");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);

			return;
		}

		if(!(getTemplate().canTeach(classId) || getTemplate().canTeach(classId.getParent(player.getSex()))))
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.WrongTeacherClass", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			TextBuilder.recycle(sb);
			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.USUAL);
		int counts = 0;

		final L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableSkills(player, classId);
		for(final L2SkillLearn s : skills)
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());

			if(sk == null || !sk.getCanLearn(player.getClassId()) || !sk.canTeachBy(npcId))
				continue;

			final int cost = SkillTreeTable.getInstance().getSkillCost(player, sk);

			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), cost, 0);
			counts++;
		}

		if(counts == 0)
		{
			final int minlevel = SkillTreeTable.getInstance().getMinLevelForNewSkill(player, classId);
			if(minlevel > 0)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_ANY_FURTHER_SKILLS_TO_LEARN__COME_BACK_WHEN_YOU_HAVE_REACHED_LEVEL_S1);
				sm.addNumber(minlevel);
				player.sendPacket(sm);
			}
			else
				player.sendPacket(Msg.THERE_ARE_NO_OTHER_SKILLS_TO_LEARN);
			player.sendPacket(AcquireSkillDone.STATIC);
		}
		else
			player.sendPacket(asl);
		player.sendActionFailed();
	}

	/** Отображает доступные скилы для трансфера */
	public void showTransferSkillList(final L2Player player)
	{
		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			return;
		}

		final ClassId classId = player.getClassId();
		if(classId == null)
			return;
		if(player.getLevel() < 76 || classId.getLevel() < 4)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("You must have 3rd class change quest completed.");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			return;
		}

		int packet_id = 5;
		final GArray<L2SkillLearn> skills = new GArray<L2SkillLearn>();
		switch (classId)
		{
			case cardinal:
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.evaSaint));
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.shillienSaint));
				packet_id = 7;
				break;
			case evaSaint:
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.cardinal));
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.shillienSaint));
				packet_id = 8;
				break;
			case shillienSaint:
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.cardinal));
				skills.addAll(SkillTreeTable.getInstance().getAvailableTransferSkills(player, ClassId.evaSaint));
				packet_id = 9;
				break;
		}

		if(skills.isEmpty())
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("There is no skills for your class.");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			player.sendPacket(AcquireSkillDone.STATIC);
			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(packet_id);
		for(final L2SkillLearn s : skills)
		{
			if(s.getItemCount() == -1)
				continue;
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk != null)
				asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), 0, 0);
		}
		player.sendPacket(asl);
	}

	/**
	 * Отображает страницу со списком доступных скилов
	 */
	public void showSquadSkillList(final L2Player activeChar)
	{
		if(activeChar.getClan() == null || !activeChar.isClanLeader())
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(activeChar, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("Only the castle owning clan leader can add a squad skill!");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			activeChar.sendPacket(html);
			return;
		}

		if(activeChar.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(activeChar, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", activeChar));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			activeChar.sendPacket(html);
			return;
		}

		final GArray<L2PledgeSkillLearn> skills = new GArray<L2PledgeSkillLearn>();
		skills.addAll(SkillTreeTable.getInstance().getAvailableSquadSkills(activeChar.getClan()));
		if(skills.isEmpty())
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(activeChar, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("This squad has no available skills to learn.");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			activeChar.sendPacket(html);
			activeChar.sendPacket(AcquireSkillDone.STATIC);
			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.CLAN_ADDITIONAL);
		for(final L2PledgeSkillLearn s : skills)
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk != null)
				asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), s.getRepCost(), s.getItemId());
		}
		activeChar.sendPacket(asl);
	}

	public void showFishingSkillList(final L2Player player)
	{
		if(Config.DEBUG)
			_log.fine("SkillList activated on: " + getObjectId());

		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);

			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.FISHING);
		int counts = 0;

		final L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableFishingSkills(player);
		for(final L2SkillLearn s : skills)
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk == null)
				continue;
			final int cost = s.getItemId();
			counts++;
			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), 0, cost);
		}

		if(counts == 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("You've learned all skills.");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			player.sendPacket(AcquireSkillDone.STATIC);
		}
		else
			player.sendPacket(asl);

		player.sendActionFailed();
	}

	public void showTransformationSkillList(final L2Player player)
	{
		if(Config.DEBUG)
			_log.fine("TransformationSkillList activated on: " + getObjectId());

		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);

			return;
		}

		if(!Config.ALLOW_LEARN_TRANS_SKILLS_WO_QUEST)
		{
			final QuestState q = player.getQuestState("_136_MoreThanMeetsTheEye");
			if(q == null || !(q.getState() == Quest.COMPLETED) && !Config.ALLOW_LEARN_TRANS_SKILLS_WO_QUEST)
			{
				showChatWindow(player, "data/html/trainer/" + getNpcId() + "-noquest.htm");
				return;
			}
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.TRANSFORMATION);
		int counts = 0;

		final L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableTransformationSkills(player);

		for(final L2SkillLearn s : skills)
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk == null)
				continue;

			counts++;
			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), s.getSpCost(), 1);
		}

		if(counts == 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append("You've learned all skills.");
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);
			player.sendPacket(AcquireSkillDone.STATIC);
		}
		else
			player.sendPacket(asl);

		player.sendActionFailed();
	}

	public void showClanSkillList(final L2Player player)
	{
		if(Config.DEBUG)
			_log.fine("SkillList activated on: " + getObjectId());

		if(player.getTransformationId() != 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			final TextBuilder sb = new TextBuilder();
			sb.append("<html><head><body>");
			sb.append(new CustomMessage("l2n.game.model.instances.L2NpcInstance.CantTeachBecauseTransformation", player));
			sb.append("</body></html>");
			html.setHtml(sb.toString());
			player.sendPacket(html);

			return;
		}

		if(player.getClan() == null || !player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED, Msg.ActionFail);
			return;
		}

		final AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.CLAN);
		int counts = 0;

		final L2PledgeSkillLearn[] skills = SkillTreeTable.getInstance().getAvailablePledgeSkills(player.getClan());
		for(final L2PledgeSkillLearn s : skills)
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(s.getId(), s.getLevel());
			if(sk == null)
			{
				_log.warning("L2NpcInstance: skill " + s.getName() + " [" + s.getId() + "-" + s.getLevel() + "] is NULL.");
				continue;
			}

			final int cost = s.getRepCost();
			final int itemCount = s.getItemCount();
			counts++;

			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), cost, itemCount);
		}

		if(counts == 0)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			html.setHtml("<html><head><body>You've learned all skills.</body></html>");
			player.sendPacket(html);
			player.sendPacket(AcquireSkillDone.STATIC);
		}
		else
			player.sendPacket(asl);

		player.sendActionFailed();
	}

	/**
	 * Возвращает режим NPC: свежезаспавненный или нормальное состояние
	 * 
	 * @return true, если NPC свежезаспавненный
	 */
	public int getShowSpawnAnimation()
	{
		return _showSpawnAnimation;
	}

	public void setShowSpawnAnimation(final int value)
	{
		_showSpawnAnimation = value;
	}

	@Override
	public boolean getChargedSoulShot()
	{
		switch (getTemplate().shots)
		{
			case SOUL:
			case SOUL_SPIRIT:
			case SOUL_BSPIRIT:
				return true;
			default:
				return false;
		}
	}

	@Override
	public int getChargedSpiritShot()
	{
		switch (getTemplate().shots)
		{
			case SPIRIT:
			case SOUL_SPIRIT:
				return 1;
			case BSPIRIT:
			case SOUL_BSPIRIT:
				return 2;
			default:
				return 0;
		}
	}

	@Override
	public boolean unChargeShots(final boolean spirit)
	{
		broadcastPacket(new MagicSkillUse(this, spirit ? 2061 : 2039, 1, 0, 0));
		return true;
	}

	@Override
	public float getColRadius()
	{
		return (float) getCollisionRadius();
	}

	@Override
	public float getColHeight()
	{
		return (float) getCollisionHeight();
	}

	public String getTypeName()
	{
		return getClass().getSimpleName().replaceFirst("L2", "").replaceFirst("Instance", "");
	}

	public void refreshID()
	{
		_objectId = IdFactory.getInstance().getNextId();
		_storedId = L2ObjectsStorage.refreshId(this);
		getEffectList().setOwner(this);
		getAI().refreshActor(this);
		_moveTaskRunnable.updateStoreId(this);
	}

	/**
	 * Возврощает топ дамагера.
	 */
	public L2Character getTopDamager(final Collection<AggroInfo> aggroList)
	{
		AggroInfo top = null;
		for(final AggroInfo aggro : aggroList)
			if(aggro.attacker != null && (top == null || aggro.damage > top.damage))
				top = aggro;
		return top != null ? top.attacker : null;
	}

	/**
	 * @param charLevel
	 *            - уровень игрока
	 * @param isSpoil
	 *            - расчитывать для спойла
	 * @return возвращает разницу в уровнях между игроком и НПС
	 */
	public int calculateLevelDiffForDrop(final int charLevel, final boolean isSpoil)
	{
		if(!Config.DEEPBLUE_DROP_RULES)
			return 0;

		final int mobLevel = getLevel();
		final int deepblue_maxdiff = isRaid() ? Config.DEEPBLUE_DROP_RAID_MAXDIFF : isSpoil ? Config.MAX_SPOIL_LEVEL_DEPEND : Config.DEEPBLUE_DROP_MAXDIFF;

		return Math.max(charLevel - mobLevel - deepblue_maxdiff, 0);
	}

	public boolean isSevenSignsMonster()
	{
		final int npcId = getNpcId();
		// первая часть - обычные
		return npcId >= 21168 && npcId <= 21207;
	}

	public void onClanAttacked(final L2NpcInstance attacked_member, final L2Character attacker, final int damage)
	{
		final String my_name = getName();
		final String attacked_name = attacked_member.getName();

		if(my_name.startsWith("Lilim ") && attacked_name.startsWith("Nephilim "))
			return;
		if(my_name.startsWith("Nephilim ") && attacked_name.startsWith("Lilim "))
			return;
		if(my_name.startsWith("Lith ") && attacked_name.startsWith("Gigant "))
			return;
		if(my_name.startsWith("Gigant ") && attacked_name.startsWith("Lith "))
			return;

		getAI().notifyEvent(CtrlEvent.EVT_CLAN_ATTACKED, attacked_member, attacker, damage);
	}

	@Override
	public String toString()
	{
		return getName() + " [Id " + getNpcId() + ", objId " + getObjectId() + "]";
	}

	public void setHiddenName(final boolean val)
	{
		_hideName = val;
	}

	@Override
	public final boolean isHiddenName()
	{
		return _hideName;
	}

	public void setHalfHeightName(final boolean val)
	{
		_halfHeightName = val;
	}

	@Override
	public final boolean isHalfHeightName()
	{
		return _halfHeightName;
	}

	@Override
	public NpcStatsChangeRecorder getStatsRecorder()
	{
		if(_statsRecorder == null)
			synchronized (this)
			{
				if(_statsRecorder == null)
					_statsRecorder = new NpcStatsChangeRecorder(this);
			}

		return (NpcStatsChangeRecorder) _statsRecorder;
	}

	@Override
	public boolean isNpc()
	{
		return true;
	}

	/**
	 * Возвращает рейты на дроп. <br>
	 * если нпс находиться в списке RATE_DROP_EPIC_RAIDBOSS_IDs, то вернёт Config.RATE_DROP_EPIC_RAIDBOSS<br>
	 * если нпс является РБ или РБ для инстансов, то вернёт Config.RATE_DROP_RAIDBOSS<br>
	 * если ни 1ое ни 2ое вернёт Config.RATE_DROP_ITEMS (или динамические рейты)
	 */
	public double getDropRate(final L2Player player)
	{
		if(_isEpicBoss)
			return Config.RATE_DROP_EPIC_RAIDBOSS;
		else if(isRaid() || this instanceof L2ReflectionBossInstance)
			return Config.RATE_DROP_RAIDBOSS;

		return DynamicRateTable.getRateItems(player);
	}
}
