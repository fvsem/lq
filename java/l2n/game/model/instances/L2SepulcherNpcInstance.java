package l2n.game.model.instances;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.boss.FourSepulchersManager;
import l2n.game.instancemanager.boss.FourSepulchersSpawn;
import l2n.game.instancemanager.boss.FourSepulchersSpawn.GateKeeper;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Util;

import java.util.concurrent.Future;

/**
 * @author L2System Project
 * @date 17.05.2010
 * @time 15:11:54
 */
public class L2SepulcherNpcInstance extends L2NpcInstance
{
	protected Future<?> _closeTask = null, _spawnMonsterTask = null;

	private final static String HTML_FILE_PATH = "data/html/SepulcherNpc/";

	private final static int HALLS_KEY = 7260;

	public L2SepulcherNpcInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);

		if(_closeTask != null)
			_closeTask.cancel(true);
		if(_spawnMonsterTask != null)
			_spawnMonsterTask.cancel(true);
		_closeTask = null;
		_spawnMonsterTask = null;
	}

	@Override
	public void deleteMe()
	{
		if(_closeTask != null)
		{
			_closeTask.cancel(true);
			_closeTask = null;
		}
		if(_spawnMonsterTask != null)
		{
			_spawnMonsterTask.cancel(true);
			_spawnMonsterTask = null;
		}
		super.deleteMe();
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		if(isDead())
		{
			player.sendActionFailed();
			return;
		}

		switch (getNpcId())
		{
			case 31468:
			case 31469:
			case 31470:
			case 31471:
			case 31472:
			case 31473:
			case 31474:
			case 31475:
			case 31476:
			case 31477:
			case 31478:
			case 31479:
			case 31480:
			case 31481:
			case 31482:
			case 31483:
			case 31484:
			case 31485:
			case 31486:
			case 31487:
				doDie(player);
				if(_spawnMonsterTask != null)
					_spawnMonsterTask.cancel(true);
				_spawnMonsterTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnMonster(getNpcId()), 3500);
				return;

			case 31455:
			case 31456:
			case 31457:
			case 31458:
			case 31459:
			case 31460:
			case 31461:
			case 31462:
			case 31463:
			case 31464:
			case 31465:
			case 31466:
			case 31467:
				doDie(player);
				if(player.getParty() != null && !player.getParty().isLeader(player))
					player = player.getParty().getPartyLeader();
				player.addItem(HALLS_KEY, 1, 0, "SepulcherNpc");
				return;
		}

		super.showChatWindow(player, val);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom = "";
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		return HTML_FILE_PATH + pom + ".htm";
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(command.startsWith("open_gate"))
		{
			L2ItemInstance hallsKey = player.getInventory().getItemByItemId(HALLS_KEY);
			if(hallsKey == null)
				showHtmlFile(player, "Gatekeeper-no.htm");
			else if(FourSepulchersManager.isAttackTime())
			{
				switch (getNpcId())
				{
					case 31929:
					case 31934:
					case 31939:
					case 31944:
						FourSepulchersSpawn.spawnShadow(getNpcId());
				}

				// Moved here from switch-default
				openNextDoor(getNpcId());
				if(player.getParty() != null)
					for(L2Player mem : player.getParty().getPartyMembers())
					{
						hallsKey = mem.getInventory().getItemByItemId(HALLS_KEY);
						if(hallsKey != null)
							mem.destroyItemByItemId(HALLS_KEY, hallsKey.getCount(), false);
					}
				else
					player.destroyItemByItemId(HALLS_KEY, hallsKey.getCount(), false);
			}
		}
		else
			super.onBypassFeedback(player, command);
	}

	public void openNextDoor(int npcId)
	{
		GateKeeper gk = FourSepulchersManager.getHallGateKeeper(npcId);
		gk.door.openMe();

		if(_closeTask != null)
			_closeTask.cancel(true);
		_closeTask = L2GameThreadPools.getInstance().scheduleGeneral(new CloseNextDoor(gk), 10000);
	}

	private class CloseNextDoor implements Runnable
	{
		private final GateKeeper _gk;
		private int state = 0;

		public CloseNextDoor(GateKeeper gk)
		{
			_gk = gk;
		}

		@Override
		public void run()
		{
			if(state == 0)
			{
				try
				{
					_gk.door.closeMe();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				state++;
				_closeTask = L2GameThreadPools.getInstance().scheduleGeneral(this, 10000);
			}
			else if(state == 1)
				FourSepulchersSpawn.spawnMysteriousBox(_gk.template.npcId);
		}
	}

	private class SpawnMonster implements Runnable
	{
		private final int _NpcId;

		public SpawnMonster(int npcId)
		{
			_NpcId = npcId;
		}

		@Override
		public void run()
		{
			FourSepulchersSpawn.spawnMonster(_NpcId);
		}
	}

	public void sayInShout(String msg)
	{
		if(msg == null || msg.isEmpty())
			return;

		GArray<L2Player> knownPlayers = getAroundPlayers(15000);
		if(knownPlayers == null || knownPlayers.isEmpty())
			return;
		CreatureSay sm = new CreatureSay(getObjectId(), Say2C.SHOUT, getName(), msg);
		for(L2Player player : knownPlayers)
		{
			if(Util.checkIfInRange(15000, player, this, true))
				player.sendPacket(sm);
		}
	}

	public void showHtmlFile(L2Player player, String file)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile("data/html/SepulcherNpc/" + file);
		html.replace("%npcname%", getName());
		player.sendPacket(html);
	}
}
