package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ClanTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

public class L2ClanHallDoormenInstance extends L2NpcInstance
{
	/**
	 * @param objectId
	 * @param template
	 */
	public L2ClanHallDoormenInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendPacket(Msg.ActionFail);
		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE)
			return;
		else if(condition == COND_OWNER)
		{
			StringTokenizer st = new StringTokenizer(command, " ");
			String actualCommand = st.nextToken();
			String val = "";
			if(st.countTokens() >= 1)
				val = st.nextToken();

			if(actualCommand.equalsIgnoreCase("door"))
				if((player.getClanPrivileges() & L2Clan.CP_CH_OPEN_DOOR) == L2Clan.CP_CH_OPEN_DOOR)
				{
					if(val.equalsIgnoreCase("open"))
					{
						getClanHall().openCloseDoors(player, true);
						NpcHtmlMessage html = new NpcHtmlMessage(player, this);
						html.setFile("data/html/doormen/clanhall/AfterDoorOpen.htm");
						sendHtmlMessage(player, html);
					}
					else if(val.equalsIgnoreCase("close"))
					{
						getClanHall().openCloseDoors(player, false);
						NpcHtmlMessage html = new NpcHtmlMessage(player, this);
						html.setFile("data/html/doormen/clanhall/AfterDoorClose.htm");
						sendHtmlMessage(player, html);
					}
					else
					{
						NpcHtmlMessage html = new NpcHtmlMessage(_objectId);
						html.setFile("data/html/doormen/clanhall/doormen.htm");
						sendHtmlMessage(player, html);
					}
				}
				else
					player.sendMessage(new CustomMessage("common.Privilleges", player));
		}
		super.onBypassFeedback(player, command);
	}

	private void sendHtmlMessage(L2Player player, NpcHtmlMessage html)
	{
		html.replace("%npcname%", getName());
		html.replace("%clanname%", player.getClan().getName());
		player.sendPacket(html);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename = "data/html/doormen/clanhall/doormen-no.htm";
		int condition = validateCondition(player);
		if(condition == COND_OWNER)
			switch (getClanHall().getId())
			{
				case 34:
				case 36:
				case 37:
				case 38:
				case 39:
				case 40:
				case 41:
				case 51:
				case 52:
				case 53:
				case 54:
				case 55:
				case 56:
				case 57:
				case 63:
				case 64:
					filename = "data/html/doormen/clanhall/doormen-elite.htm";
					break;
				case 35:
				case 42:
				case 43:
				case 44:
				case 45:
				case 46:
				case 47:
				case 48:
				case 49:
				case 50:
				case 58:
				case 59:
				case 60:
				case 61:
				case 62:
				default:
					filename = "data/html/doormen/clanhall/doormen.htm";
			}

		NpcHtmlMessage html = new NpcHtmlMessage(player, this, filename, val);
		L2Clan clanowner = ClanTable.getInstance().getClan(getClanHall().getOwnerId());
		html.replace("%clanname%", clanowner != null ? clanowner.getName() : "NPC");
		html.replace("%clanlidername%", clanowner != null ? clanowner.getLeaderName() : "NPC");
		player.sendPacket(html);
	}

	protected int validateCondition(L2Player player)
	{
		int cond = COND_ALL_FALSE;
		if(player.isGM())
			cond = COND_OWNER;
		else if(player.getClan() != null)
			if(getClanHall().getOwnerId() == player.getClanId())
				cond = COND_OWNER;
		return cond;
	}
}
