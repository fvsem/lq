package l2n.game.model.instances;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

/**
 * This class manages all Minions.
 * In a group mob, there are one master called RaidBoss and several slaves called Minions.
 * 
 * @version $Revision: 1.20.4.6 $ $Date: 2005/04/06 16:13:39 $
 */
public final class L2MinionInstance extends L2MonsterInstance
{
	/** The master L2Character whose depends this L2MinionInstance on */
	private long _masterStoredId;

	/**
	 * Constructor of L2MinionInstance (use L2Character and L2NpcInstance constructor).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Call the L2Character constructor to set the _template of the L2MinionInstance (copy skills from template to object and link _calculators to NPC_STD_CALCULATOR)</li> <li>Set the name of the L2MinionInstance</li> <li>Create a RandomAnimation
	 * Task that will be launched after the calculated delay if the server allow it</li><BR>
	 * <BR>
	 * 
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @param L2NpcTemplate
	 *            Template to apply to the NPC
	 */
	public L2MinionInstance(int objectId, L2NpcTemplate template, L2MonsterInstance leader)
	{
		super(objectId, template);
		_masterStoredId = leader.getStoredId();
	}

	/**
	 * Return the master of this L2MinionInstance.<BR>
	 * <BR>
	 */
	public L2MonsterInstance getLeader()
	{
		return L2ObjectsStorage.getAsMonster(_masterStoredId);
	}

	public boolean isRaidFighter()
	{
		return getLeader() != null && getLeader().isRaid();
	}

	@Override
	public void onSpawn()
	{
		// Notify Leader that Minion has Spawned
		getLeader().notifyMinionSpawned(this);
	}

	/**
	 * Manages the doDie event for this L2MinionInstance.<BR>
	 * <BR>
	 * 
	 * @param killer
	 *            The L2Character that killed this L2MinionInstance.<BR>
	 * <BR>
	 */
	@Override
	public void doDie(L2Character killer)
	{
		if(getLeader() != null)
			getLeader().notifyMinionDied(this);

		super.doDie(killer);
	}

	@Override
	public boolean isFearImmune()
	{
		return isRaidFighter();
	}

	@Override
	public Location getSpawnedLoc()
	{
		return getLeader() != null ? getLeader().getMinionPosition() : getLoc();
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
