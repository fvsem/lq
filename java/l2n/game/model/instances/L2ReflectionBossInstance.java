package l2n.game.model.instances;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Party;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;

import java.util.concurrent.ScheduledFuture;

public class L2ReflectionBossInstance extends L2RaidBossInstance
{
	private static final int COLLAPSE_AFTER_DEATH_TIME = 5;

	private ScheduledFuture<ManaRegen> _manaRegen;

	public L2ReflectionBossInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
		final String refName = getReflection().getName();
		if(refName.contains("Kamaloka"))
			_manaRegen = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new ManaRegen(), 20000, 20000, false);
	}

	@Override
	public void doDie(final L2Character killer)
	{
		final String refName = getReflection().getName();
		if(refName.contains("Kamaloka"))
			if(Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("Leader"))
			{
				final L2Party p = killer.getPlayer().getParty();
				if(p != null)
					p.getPartyLeader().setVar(refName, String.valueOf(System.currentTimeMillis()));
			}
			else
				for(final L2Player p : getReflection().getPlayers())
					if(p != null)
						p.setVar(refName, String.valueOf(System.currentTimeMillis()));

		if(_manaRegen != null)
		{
			_manaRegen.cancel(true);
			_manaRegen = null;
		}
		unspawnMinions();
		super.doDie(killer);
		clearReflection();
	}

	@Override
	public void unspawnMinions()
	{
		removeMinions();
	}

	/**
	 * Удаляет все спауны из рефлекшена и запускает 5ти минутный коллапс-таймер.
	 */
	protected void clearReflection()
	{
		getReflection().clearReflection(COLLAPSE_AFTER_DEATH_TIME, true);
	}

	private class ManaRegen implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				for(final L2Player p : L2World.getAroundPlayers(L2ReflectionBossInstance.this))
				{
					if(p == null || p.isDead() || p.isBlockAll())
						continue;
					final int addMp = getAddMp();
					if(addMp <= 0)
						return;
					final double newMp = Math.min(Math.max(.0, p.getMaxMp() - p.getCurrentMp()), addMp);
					if(newMp > 0D)
					{
						p.setCurrentMp(newMp + p.getCurrentMp());
						p.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber(Math.round(newMp)));
					}
				}
			}
			catch(final Throwable e)
			{
				e.printStackTrace();
			}
		}

		private int getAddMp()
		{
			switch (getLevel())
			{
				case 23:
				case 26:
					return 6;
				case 33:
				case 36:
					return 10;
				case 43:
				case 46:
					return 13;
				case 53:
				case 56:
					return 16; // С потолка
				case 63:
				case 66:
					return 19; // С потолка
				case 73:
					return 22; // С потолка
				default:
					return 0;
			}
		}
	}
}
