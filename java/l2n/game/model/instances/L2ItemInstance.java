package l2n.game.model.instances;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.Stat;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Events;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.MercTicketManager;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2Augmentation;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.game.skills.funcs.LambdaConst;
import l2n.game.tables.ItemTable;
import l2n.game.tables.PetDataTable;
import l2n.game.taskmanager.ItemsAutoDestroy;
import l2n.game.templates.*;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.game.templates.L2Item.Grade;
import l2n.util.*;

import java.sql.ResultSet;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2ItemInstance extends L2Object
{
	private static final Logger _log = Logger.getLogger(L2ItemInstance.class.getName());

	/** Enumeration of locations for item */
	public static enum ItemLocation
	{
		VOID,
		INVENTORY,
		PAPERDOLL,
		WAREHOUSE,
		CLANWH,
		PET,
		PET_EQUIP,
		FREIGHT,
		MAIL
	}

	/** Item types to select */
	public static enum ItemClass
	{
		/** List all deposited items */
		ALL,
		/** Weapons, Armor, Jevels, Arrows, Baits */
		EQUIPMENT,
		/** Soul/Spiritshot, Potions, Scrolls */
		CONSUMABLE,
		/** Common craft matherials */
		MATHERIALS,
		/** Special (item specific) craft matherials */
		PIECES,
		/** Crafting recipies */
		RECIPIES,
		/** Skill learn books */
		SPELLBOOKS,
		/** Dyes, lifestones */
		MISC,
		/** All other */
		OTHER
	}

	/**
	 * флаг запрещает дроп Утема
	 * может быть выдан вУду Утема УлУ отдельному
	 * можно разрешУть дропать не дропуемое
	 */
	public static final int FLAG_NO_DROP = 0x001;
	/**
	 * флаг запрещает трейд между чарамУ данным Утемом
	 * может быть выдан вУду Утема УлУ отдельному
	 * можно разрешУть продавать не продаваемое
	 */
	public static final int FLAG_NO_TRADE = 0x002;
	/**
	 * флаг запрещает класть Утемы в квх У передавать другому чару на акке
	 * не наследуеться можно выдать только конкретному Утему
	 */
	public static final int FLAG_NO_TRANSFER = 0x004;
	/**
	 * запрещает крУстлУзацУю
	 * может быть выдан вУду Утема УлУ отдельному
	 * нельзя разрешУть крУсталУзовать не крУсталУзуемое можн лУшь запретУть какУе лУбо вещУ крУсталУзовать
	 * а также запрещает точУть атрУбуцтамУ У просто точУть
	 */
	public static final int FLAG_NO_CRYSTALLIZE = 0x008;
	/**
	 * запрещает точУть определённый предмет,не наследуеться
	 * запрет херо Утемов У тд см.нУже,всё отдельно
	 * можно разрешУть точУть напрУмер НГ xD но только определённую вешь а невУд вещей
	 * можно запретУть точУть определённую вешь
	 * также запрещает точУть атрУбутамУ
	 */
	public static final int FLAG_NO_ENCHANT = 0x010;
	/**
	 * Недаёт унУчтожать вещУ через корзУну,наследуется
	 * можно установУть на группу Утемов УлУ на определённый
	 * можно разрешУть унУчтожать неунУчтожУмое
	 * У в обратку мона запретУть унУчтожать унУчтожУмое
	 */
	public static final int FLAG_NO_DESTROY = 0x020;
	public static final int FLAG_NO_UNEQUIP = 0x040;
	public static final int FLAG_ALWAYS_DROP_ON_DIE = 0x080;
	public static final int FLAG_EQUIP_ON_PICKUP = 0x100;
	public static final int FLAG_NO_RIDER_PICKUP = 0x200;
	/**
	 * Для Утемов которые на пете который унсамонен
	 */
	public static final int FLAG_PET_EQUIPPED = 0x400;

	public static final byte CHARGED_NONE = 0;
	public static final byte CHARGED_SOULSHOT = 1;
	public static final byte CHARGED_SPIRITSHOT = 1;
	public static final byte CHARGED_BLESSED_SPIRITSHOT = 2;

	public static final byte UNCHANGED = 0;
	public static final byte ADDED = 1;
	public static final byte MODIFIED = 2;
	public static final byte REMOVED = 3;

	/************************************ End static data ********************************************************/

	/** ID of the owner */
	private long ownerStoreId;

	/** Время жУзнУ прУзрачных вещей **/
	private ScheduledFuture<LifeTimeTask> _itemLifeTimeTask; // FIXME переделать на менеджер
	private int _lifeTimeRemaining;

	/** Quantity of the item */
	private long _count;

	/** ID of the item */
	private int _itemId;

	/** Object L2Item associated to the item */
	private L2Item _itemTemplate;

	/** Location of the item */
	private ItemLocation _loc;

	/** Slot where item is stored */
	private int _loc_data;

	/** Level of enchantment of the item */
	private int _enchantLevel;

	/** Price of the item for selling */
	private long _price_sell;

	/** Price of the item for buying */
	private long _count_sell;

	/** Wear Item */
	private boolean _wear;

	private L2Augmentation _augmentation = null;

	/** Custom item types (used loto, race tickets) */
	private int _type1;
	private int _type2;

	/** Item drop time for autodestroy task */
	private long _dropTime;

	/** Item drop time */
	private long _dropTimeOwner;

	/** owner of the dropped item */
	private long itemDropOwnerStoreId;

	private byte _chargedSoulshot = CHARGED_NONE;
	private byte _chargedSpiritshot = CHARGED_NONE;
	private boolean _chargedFishtshot = false;

	private byte _lastChange = 2; // 1 ??, 2 modified, 3 removed
	private boolean _existsInDb; // if a record exists in DB.
	private boolean _storedInDb; // if DB data is up-to-date.

	/** Element (0 - Fire, 1 - Water, 2 - Wind, 3 - Earth, 4 - Holy, 5 - Dark, -2 - None) */
	private Elementals _elementals = null;

	private FuncTemplate _enchantAttributeFuncTemplate;

	/**
	 * СпецфлагУ для конкретного Унстанса
	 */
	private int _customFlags = 0;

	private Future<LazyUpdateInDb> _lazyUpdateInDb;

	/** Task of delayed update item info in database */
	private class LazyUpdateInDb implements Runnable
	{
		private final long itemStoreId;

		private LazyUpdateInDb(final L2ItemInstance item)
		{
			itemStoreId = item.getStoredId();
		}

		@Override
		public void run()
		{
			final L2ItemInstance _item = L2ObjectsStorage.getAsItem(itemStoreId);
			if(_item == null)
				return;
			try
			{
				_item.updateInDb();
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				_item.stopLazyUpdateTask(false);
			}
		}
	}

	private final int _destination;
	private final int _source;
	private final String _createType;
	private int _createTime;

	// Для магазУнов с огранУченным колУчеством предметов
	private long _maxCountToSell;
	private int _lastRechargeTime;
	private int _rechargeTime;

	private int _bodypart;

	private boolean _whflag = false;

	private final Exception _debugException;

	/**
	 * Constructor of the L2ItemInstance from the objectId and the itemId.
	 *
	 * @param objectId
	 *            : int designating the ID of the object in the world
	 * @param itemId
	 *            : int designating the ID of the item
	 */
	public L2ItemInstance(final int objectId, final int itemId, final int destination, final int source, final String createType)
	{
		this(objectId, ItemTable.getInstance().getTemplate(itemId), destination, source, createType, true);
	}

	/**
	 * Constructor of the L2ItemInstance from the objetId and the description of the item given by the L2Item.
	 *
	 * @param objectId
	 *            : int designating the ID of the object in the world
	 * @param item
	 *            : L2Item containing informations of the item
	 */
	public L2ItemInstance(final int objectId, final L2Item item, final int destination, final int source, final String createType, final boolean putInStorage)
	{
		super(objectId, putInStorage && objectId > 0);
		if(item == null)
		{
			_log.warning("Not found template for item id: " + _itemId);
			throw new IllegalArgumentException();
		}
		_itemId = item.getItemId();
		_itemTemplate = item;
		_count = 1;
		_loc = ItemLocation.VOID;

		_dropTime = 0;
		_dropTimeOwner = 0;
		setItemDropOwner(null, 0);

		_destination = destination;
		_source = source;
		_createType = createType;
		_createTime = (int) (System.currentTimeMillis() / 1000);

		_lifeTimeRemaining = _itemTemplate.isTemporal() ? (int) (System.currentTimeMillis() / 1000) + _itemTemplate.getDurability() * 60 : _itemTemplate.getDurability();

		_bodypart = _itemTemplate.getBodyPart();
		_debugException = new Exception();
	}

	public final Exception getDebugException()
	{
		return _debugException;
	}

	/**
	 * удаляет все ссылкУ У останавлУвает все таймеры без сохраненУя
	 */
	public void prepareRemove()
	{
		setItemDropOwner(null, 0);
	}

	public int getBodyPart()
	{
		return _bodypart;
	}

	public void setBodyPart(final int bodypart)
	{
		_bodypart = bodypart;
	}

	/**
	 * Sets the ownerID of the item
	 *
	 * @param owner_id
	 *            : int designating the ID of the owner
	 */
	public void setOwnerId(final int owner_id)
	{
		if(getOwnerId() != owner_id)
			_storedInDb = false;
		final L2Player owner = L2ObjectsStorage.getPlayer(owner_id);
		ownerStoreId = owner != null ? owner.getStoredId() : owner_id + 0;
		startTemporalTask(owner);
	}

	/**
	 * Returns the ownerID of the item
	 *
	 * @return int : ownerID of the item
	 */
	public int getOwnerId()
	{
		return L2ObjectsStorage.getStoredObjectId(ownerStoreId);
	}

	private L2Player getOwner()
	{
		return L2ObjectsStorage.getAsPlayer(ownerStoreId);
	}

	/**
	 * Sets the location of the item
	 *
	 * @param loc
	 *            : ItemLocation (enumeration)
	 */
	public void setLocation(final ItemLocation loc)
	{
		setLocation(loc, 0);
	}

	/**
	 * Sets the location of the item.<BR>
	 * <BR>
	 * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
	 *
	 * @param loc
	 *            : ItemLocation (enumeration)
	 * @param loc_data
	 *            : int designating the slot where the item is stored or the village for freights
	 */
	public void setLocation(final ItemLocation loc, final int loc_data)
	{
		if(loc == _loc && loc_data == _loc_data)
			return;
		_loc = loc;
		_loc_data = loc_data;
		_storedInDb = false;
	}

	public ItemLocation getLocation()
	{
		return _loc;
	}

	/**
	 * Returns the slot where the item is stored
	 *
	 * @return int
	 */
	public int getLocationSlot()
	{
		return _loc_data;
	}

	/**
	 * Возвращает колУчество предметов без прУведенУя к int По возможностУ следует Успользовать Уменно его
	 *
	 * @return long
	 */
	public long getCount()
	{
		return _count;
	}

	/**
	 * Sets the quantity of the item.<BR>
	 * <BR>
	 * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
	 *
	 * @param count
	 *            : long
	 */
	public void setCount(long count)
	{
		if(count == 0)
			return;

		// if is adena..
		if(count > 0 && count > Config.ALT_MAX_COUNT_ADENA && _itemId == 57)
			count = Config.ALT_MAX_COUNT_ADENA;
		else if(count > 0 && count > Config.ALT_MAX_COUNT_OTHER && _itemId != 57)
			count = Config.ALT_MAX_COUNT_OTHER;

		if(count < 0)
			count = 0;

		if(!isStackable() && count > 1)
		{
			_count = 1;
			if(getOwner() != null)
				Util.handleIllegalPlayerAction(getOwner(), "L2ItemInstance[448]", "tried to stack unstackable item " + getItemId(), IllegalPlayerAction.INFO);
			return;
		}
		if(_count == count)
			return;
		_count = count;
		_storedInDb = false;
	}

	/**
	 * Returns if item is equipable
	 *
	 * @return boolean
	 */
	public boolean isEquipable()
	{
		return _itemTemplate.getItemType() == EtcItemType.BAIT || _itemTemplate.getItemType() == EtcItemType.ARROW || _itemTemplate.getItemType() == EtcItemType.BOLT || !(getBodyPart() == 0 || _itemTemplate instanceof L2EtcItem);
	}

	public boolean isEtcItem()
	{
		return _itemTemplate instanceof L2EtcItem;
	}

	/**
	 * Returns if item is equipped
	 *
	 * @return boolean
	 */
	public boolean isEquipped()
	{
		return _loc == ItemLocation.PAPERDOLL || _loc == ItemLocation.PET_EQUIP;
	}

	/**
	 * Returns the slot where the item is stored
	 *
	 * @return int
	 */
	public int getEquipSlot()
	{
		return _loc_data;
	}

	/**
	 * Returns the characteristics of the item
	 *
	 * @return L2Item
	 */
	public L2Item getItem()
	{
		return _itemTemplate;
	}

	public int getCustomType1()
	{
		return _type1;
	}

	public int getCustomType2()
	{
		return _type2;
	}

	public void setCustomType1(final int newtype)
	{
		_type1 = newtype;
	}

	public void setCustomType2(final int newtype)
	{
		_type2 = newtype;
	}

	public void setDropTime(final long time)
	{
		_dropTime = time;
	}

	public long getDropTime()
	{
		return _dropTime;
	}

	public long getDropTimeOwner()
	{
		return _dropTimeOwner;
	}

	public void setItemDropOwner(final L2Player owner, final long time)
	{
		itemDropOwnerStoreId = owner != null ? owner.getStoredId() : 0;
		_dropTimeOwner = owner != null ? System.currentTimeMillis() + time : 0;
	}

	public L2Player getItemDropOwner()
	{
		return L2ObjectsStorage.getAsPlayer(itemDropOwnerStoreId);
	}

	public final boolean isWear()
	{
		return _wear;
	}

	public final boolean isWeapon()
	{
		return _itemTemplate.isWeapon();
	}

	public final boolean isArmor()
	{
		return _itemTemplate.isArmor();
	}

	public final boolean isAccessory()
	{
		return _itemTemplate.isAccessory();
	}

	public final boolean isUnderwear()
	{
		return _itemTemplate.isUnderwear();
	}

	public boolean isTalisman()
	{
		return _itemTemplate.isTalisman();
	}

	public boolean isBracelet()
	{
		return _itemTemplate.isBracelet();
	}

	public void setWear(final boolean newwear)
	{
		_wear = newwear;
	}

	/**
	 * Returns the type of item
	 *
	 * @return Enum
	 */
	public ItemType getItemType()
	{
		return _itemTemplate.getItemType();
	}

	/**
	 * Returns the ID of the item
	 *
	 * @return int
	 */
	public int getItemId()
	{
		return _itemId;
	}

	/**
	 * Returns the reference price of the item
	 *
	 * @return int
	 */
	public int getReferencePrice()
	{
		return _itemTemplate.getReferencePrice();
	}

	/**
	 * Returns the price of the item for selling
	 *
	 * @return int
	 */
	public long getPriceToSell()
	{
		return _price_sell;
	}

	/**
	 * Sets the price of the item for selling <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
	 *
	 * @param price
	 *            : int designating the price
	 */
	public void setPriceToSell(final long price)
	{
		_price_sell = price;
	}

	public void setCountToSell(final long count)
	{
		_count_sell = count;
	}

	public long getCountToSell()
	{
		return _count_sell;
	}

	public void setMaxCountToSell(final long count)
	{
		_maxCountToSell = count;
	}

	public long getMaxCountToSell()
	{
		return _maxCountToSell;
	}

	/**
	 * УстанавлУвает время последнего респауна предмета, Успользуется в NPC магазУнах с огранУченным колУчеством.
	 *
	 * @param lastRechargeTime
	 *            : unixtime в мУнутах
	 */
	public void setLastRechargeTime(final int lastRechargeTime)
	{
		_lastRechargeTime = lastRechargeTime;
	}

	/**
	 * Возвращает время последнего респауна предмета, Успользуется в NPC магазУнах с огранУченным колУчеством.
	 *
	 * @return unixtime в мУнутах
	 */
	public int getLastRechargeTime()
	{
		return _lastRechargeTime;
	}

	/**
	 * УстанавлУвает время респауна предмета, Успользуется в NPC магазУнах с огранУченным колУчеством.
	 *
	 * @param rechargeTime
	 *            : unixtime в мУнутах
	 */
	public void setRechargeTime(final int rechargeTime)
	{
		_rechargeTime = rechargeTime;
	}

	/**
	 * Возвращает время респауна предмета, Успользуется в NPC магазУнах с огранУченным колУчеством.
	 *
	 * @return unixtime в мУнутах
	 */
	public int getRechargeTime()
	{
		return _rechargeTime;
	}

	/**
	 * Возвращает огранУчен лУ этот предмет в колУчестве, Успользуется в NPC магазУнах с огранУченным колУчеством.
	 *
	 * @return true, еслУ огранУчен
	 */
	public boolean isCountLimited()
	{
		return _maxCountToSell > 0;
	}

	/**
	 * Returns the last change of the item
	 *
	 * @return int
	 */
	public int getLastChange()
	{
		return _lastChange;
	}

	/**
	 * Sets the last change of the item
	 *
	 * @param lastChange
	 *            : int
	 */
	public void setLastChange(final byte lastChange)
	{
		_lastChange = lastChange;
	}

	/**
	 * Returns if item is stackable
	 *
	 * @return boolean
	 */
	public boolean isStackable()
	{
		return _itemTemplate.isStackable();
	}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		// Когда игрок, владеющий Akamanah находит Зарич, подобранный Зарич автоматически исчезает а уровень Akamanah увеличивается на 1. И наоборот
		if(player.isCursedWeaponEquipped() && CursedWeaponsManager.getInstance().isCursed(_itemId))
		{
			CursedWeapon cw = CursedWeaponsManager.getInstance().getCursedWeapon(player.getCursedWeaponEquippedId());
			// если достигли макс уровня
			if(cw.getLevel() >= cw.getMaxLevel())
			{
				player.sendActionFailed();
				return;
			}

			// добавляем очки до след уровня
			cw.increaseKills(cw.getStageKills());
			cw.giveSkill();

			// удаляем который хотели поднять
			cw = CursedWeaponsManager.getInstance().getCursedWeapon(_itemId);
			cw.endOfLife();

			player.sendActionFailed();
			return;
		}

		// this causes the validate position handler to do the pickup if the location is reached.
		// mercenary tickets can only be picked up by the castle owner.
		final int _castleId = MercTicketManager.getInstance().getTicketCastleId(_itemId);
		if(_castleId > 0)
		{
			if((player.getClanPrivileges() & L2Clan.CP_CS_MERCENARIES) == L2Clan.CP_CS_MERCENARIES)
			{
				if(player.isInParty())
					player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ItemInstance.NoMercInParty", player));
				else
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_PICK_UP, this);
			}
			else
				player.sendPacket(new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_THE_AUTHORITY_TO_CANCEL_MERCENARY_POSITIONING));

			player.setTarget(this);
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, this, null);
			// Send a Server->Client ActionFailed to the L2Player in order to avoid that the client wait another packet
			player.sendActionFailed();
		}
		else
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_PICK_UP, this, null);
	}

	/**
	 * Returns the level of enchantment of the item
	 *
	 * @return int
	 */
	public int getEnchantLevel()
	{
		return _enchantLevel;
	}

	/**
	 * Sets the level of enchantment of the item
	 *
	 * @param enchantLevel
	 *            level of enchant
	 */
	public void setEnchantLevel(final int enchantLevel)
	{
		if(_enchantLevel == enchantLevel)
			return;
		_enchantLevel = enchantLevel;
		_storedInDb = false;
	}

	/**
	 * Returns false cause item can't be attacked
	 *
	 * @return boolean false
	 */
	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return false;
	}

	/**
	 * Returns whether this item is augmented or not
	 *
	 * @return true if augmented
	 */
	public boolean isAugmented()
	{
		return _augmentation != null;
	}

	/**
	 * Returns the augmentation object for this item
	 *
	 * @return augmentation
	 */
	public L2Augmentation getAugmentation()
	{
		return _augmentation;
	}

	public int getAugmentationId()
	{
		return _augmentation == null ? 0 : _augmentation.getAugmentationId();
	}

	/**
	 * Sets a new augmentation
	 *
	 * @param augmentation
	 * @return return true if successfully
	 */
	public boolean setAugmentation(final L2Augmentation augmentation)
	{
		// there shall be no previous augmentation..
		if(_augmentation != null)
			return false;
		_augmentation = augmentation;
		setCustomFlags(_customFlags & ~FLAG_PET_EQUIPPED, true);
		setLastChange(MODIFIED);
		return true;
	}

	/**
	 * Remove the augmentation
	 */
	public void removeAugmentation()
	{
		if(_augmentation == null)
			return;
		_augmentation.deleteAugmentationData();
		_augmentation = null;
		setLastChange(MODIFIED);
	}

	public synchronized void restoreAugmentation()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT attributes, skill, level FROM augmentations WHERE item_id=? LIMIT 1");
			statement.setInt(1, getObjectId());
			rs = statement.executeQuery();
			if(rs.next())
			{
				final int aug_attributes = rs.getInt(1);
				final int aug_skillId = rs.getInt(2);
				final int aug_skillLevel = rs.getInt(3);
				if(aug_attributes != -1 && aug_skillId != -1 && aug_skillLevel != -1)
				{
					_augmentation = new L2Augmentation(this, aug_attributes, aug_skillId, aug_skillLevel, false);
					setCustomFlags(_customFlags & ~FLAG_PET_EQUIPPED, false);
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore augmentation data for item " + getObjectId() + " from DB!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

		// Очищаем некорректные аугментации из базы
		if(_augmentation != null && !_augmentation.isLoaded())
		{
			_log.log(Level.WARNING, "Remove incorrect augmentation from item objId: " + getObjectId() + ", id: " + getItemId());
			removeAugmentation();
		}
	}

	/**
	 * Returns the type of charge with SoulShot of the item.
	 *
	 * @return int (CHARGED_NONE, CHARGED_SOULSHOT)
	 */
	public byte getChargedSoulshot()
	{
		return _chargedSoulshot;
	}

	/**
	 * Returns the type of charge with SpiritShot of the item
	 *
	 * @return int (CHARGED_NONE, CHARGED_SPIRITSHOT, CHARGED_BLESSED_SPIRITSHOT)
	 */
	public byte getChargedSpiritshot()
	{
		return _chargedSpiritshot;
	}

	public boolean getChargedFishshot()
	{
		return _chargedFishtshot;
	}

	/**
	 * Sets the type of charge with SoulShot of the item
	 *
	 * @param type
	 *            : int (CHARGED_NONE, CHARGED_SOULSHOT)
	 */
	public void setChargedSoulshot(final byte type)
	{
		_chargedSoulshot = type;
	}

	/**
	 * Sets the type of charge with SpiritShot of the item
	 *
	 * @param type
	 *            : int (CHARGED_NONE, CHARGED_SPIRITSHOT, CHARGED_BLESSED_SPIRITSHOT)
	 */
	public void setChargedSpiritshot(final byte type)
	{
		_chargedSpiritshot = type;
	}

	public void setChargedFishshot(final boolean type)
	{
		_chargedFishtshot = type;
	}

	protected FuncTemplate[] _funcTemplates;

	public void attachFunction(final FuncTemplate f)
	{
		if(_funcTemplates == null)
			_funcTemplates = new FuncTemplate[] { f };
		else
		{
			final int len = _funcTemplates.length;
			final FuncTemplate[] tmp = new FuncTemplate[len + 1];
			System.arraycopy(_funcTemplates, 0, tmp, 0, len);
			tmp[len] = f;
			_funcTemplates = tmp;
		}
	}

	public void detachFunction(final FuncTemplate f)
	{
		if(_funcTemplates == null || f == null)
			return;
		for(int i = 0; i < _funcTemplates.length; i++)
			if(f.equals(_funcTemplates[i]))
			{
				final int len = _funcTemplates.length - 1;
				_funcTemplates[i] = _funcTemplates[len];
				final FuncTemplate[] tmp = new FuncTemplate[len];
				System.arraycopy(_funcTemplates, 0, tmp, 0, len);
				_funcTemplates = tmp;
				break;
			}
	}

	/**
	 * This function basically returns a set of functions from L2Item/L2Armor/L2Weapon, but may add additional functions, if this particular item instance is enhanched for a particular player.
	 *
	 * @param player
	 *            : L2Character designating the player
	 * @return Func[]
	 */
	public Func[] getStatFuncs(final L2Character player)
	{
		final FuncTemplate[] templates = _itemTemplate.getAttachedFuncs();
		Func[] funcs = ArrayUtil.EMPTY_FUNCTION_SET;
		if(templates.length > 0)
		{
			funcs = new Func[templates.length];
			int index = 0;
			for(final FuncTemplate t : templates)
			{
				final Env env = new Env();
				env.character = player;
				final Func f = t.getFunc(env, this);
				if(f != null)
					funcs[index++] = f;
			}
		}

		if(_funcTemplates != null && _funcTemplates.length > 0)
			for(final FuncTemplate t : _funcTemplates)
			{
				final Env env = new Env();
				env.character = player;
				env.item = this;
				final Func f = t.getFunc(env, this);
				if(f != null)
					funcs = ArrayUtil.add(funcs, f);
			}

		return funcs;
	}

	public boolean stopLazyUpdateTask(final boolean interrupt)
	{
		boolean ret = false;
		if(_lazyUpdateInDb != null)
		{
			ret = _lazyUpdateInDb.cancel(interrupt);
			_lazyUpdateInDb = null;
		}
		return ret;
	}

	/**
	 * Returns a L2ItemInstance stored in database from its objectID
	 *
	 * @param objectId
	 *            : int designating the objectID of the item
	 * @return L2ItemInstance
	 */
	public synchronized static L2ItemInstance restoreFromDb(final int objectId, final boolean putInStorage)
	{
		L2ItemInstance inst = null;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet item_rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM items WHERE object_id = ? LIMIT 1");
			statement.setInt(1, objectId);
			item_rset = statement.executeQuery();
			if(item_rset.next())
				inst = restoreFromDb(item_rset, con, putInStorage);
			else
				_log.log(Level.WARNING, "Item object_id=" + objectId + " not found!");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore item " + objectId + " from DB!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, item_rset);
		}
		return inst;
	}

	/**
	 * Returns a L2ItemInstance stored in database from its objectID
	 *
	 * @param item_rset
	 *            : ResultSet
	 * @param augment_rset
	 *            : ResultSet
	 * @return L2ItemInstance
	 */
	public synchronized static L2ItemInstance restoreFromDb(final ResultSet item_rset, final ThreadConnection con, final boolean putInStorage)
	{
		if(item_rset == null)
			return null;
		int objectId = 0;
		try
		{
			objectId = item_rset.getInt("object_id");
			final L2Item item = ItemTable.getInstance().getTemplate(item_rset.getInt("item_id"));
			if(item == null)
			{
				_log.warning("Item item_id=" + item_rset.getInt("item_id") + " not known, object_id=" + objectId);
				return null;
			}

			// TODO при старте сервера чистить такие итемы
			if(item.isTemporal() && item_rset.getInt("shadow_life_time") <= 0)
				return null;

			final L2ItemInstance inst = new L2ItemInstance(objectId, item, item_rset.getInt("destination"), item_rset.getInt("source"), item_rset.getString("create_type"), putInStorage);
			inst._existsInDb = true;
			inst._storedInDb = true;
			inst._lifeTimeRemaining = item_rset.getInt("shadow_life_time");
			inst.setOwnerId(item_rset.getInt("owner_id"));
			inst._count = item_rset.getLong("count");
			inst._enchantLevel = item_rset.getInt("enchant_level");
			inst._type1 = item_rset.getInt("custom_type1");
			inst._type2 = item_rset.getInt("custom_type2");
			inst._loc = ItemLocation.valueOf(item_rset.getString("loc"));
			inst._loc_data = item_rset.getShort("loc_data");
			inst._createTime = item_rset.getInt("create_time");
			inst._customFlags = item_rset.getInt("flags");

			// load augmentation and elemental enchant
			if(inst.isEquipable())
			{
				inst.setAttributeElement(item_rset.getByte("enchant_attr"), item_rset.getInt("enchant_attr_value"), false);
				inst.restoreAugmentation();
			}

			return inst;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not restore item " + objectId + " from DB!", e);
			return null;
		}
	}

	/**
	 * Updates database.<BR>
	 * <BR>
	 * <U><I>Concept : </I></U><BR>
	 * <B>IF</B> the item exists in database :
	 * <UL>
	 * <LI><B>IF</B> the item has no owner, or has no location, or has a null quantity : remove item from database</LI>
	 * <LI><B>ELSE</B> : update item in database</LI>
	 * </UL>
	 * <B> Otherwise</B> :
	 * <UL>
	 * <LI><B>IF</B> the item hasn't a null quantity, and has a correct location, and has a correct owner : insert item in database</LI>
	 * </UL>
	 */
	public void updateDatabase()
	{
		updateDatabase(false);
	}

	public synchronized void updateDatabase(final boolean commit)
	{
		if(_existsInDb)
		{
			if(getOwnerId() == 0 || _loc == ItemLocation.VOID || _count == 0)
				removeFromDb();
			else if(Config.LAZY_ITEM_UPDATE && (isStackable() || Config.LAZY_ITEM_UPDATE_ALL))
			{
				if(commit)
				{
					// cancel lazy update task if need
					if(stopLazyUpdateTask(true))
					{
						insertIntoDb(); // на всякУй случай...
						Stat.increaseInsertItemCount();
						return;
					}
					updateInDb();
					Stat.increaseUpdateItemCount();
					return;
				}
				final Future<?> lazyUpdateInDb = _lazyUpdateInDb;
				if(lazyUpdateInDb == null || lazyUpdateInDb.isDone())
				{
					_lazyUpdateInDb = L2GameThreadPools.getInstance().scheduleGeneral(new LazyUpdateInDb(this), isStackable() ? Config.LAZY_ITEM_UPDATE_TIME : Config.LAZY_ITEM_UPDATE_ALL_TIME);
					Stat.increaseLazyUpdateItem();
				}
			}
			else
			{
				updateInDb();
				Stat.increaseUpdateItemCount();
			}
		}
		else
		{
			if(_count == 0 || _loc == ItemLocation.VOID || getOwnerId() == 0)
				return;
			insertIntoDb();
		}
	}

	/**
	 * Update the database with values of the item
	 * Не вызывать нестандартными способами
	 */
	public synchronized void updateInDb()
	{
		if(_wear)
			return;

		if(_storedInDb)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE items SET owner_id=?,count=?,loc=?,loc_data=?,enchant_level=?,enchant_attr=?,enchant_attr_value=?,custom_type1=?,custom_type2=?,destination=?,source=?,create_type=?,create_time=?,shadow_life_time=?,item_id=?,flags=? WHERE object_id = ?");
			statement.setInt(1, getOwnerId());
			statement.setLong(2, _count);
			statement.setString(3, _loc.name());
			statement.setInt(4, _loc_data);
			statement.setInt(5, getEnchantLevel());
			statement.setInt(6, _elementals == null ? -2 : _elementals.getElement());
			statement.setInt(7, _elementals == null ? 0 : _elementals.getValue());
			statement.setInt(8, getCustomType1());
			statement.setInt(9, getCustomType2());
			statement.setInt(10, _destination);
			statement.setInt(11, _source);
			statement.setString(12, _createType);
			statement.setInt(13, _createTime);
			statement.setInt(14, _lifeTimeRemaining);
			statement.setInt(15, getItemId());
			statement.setInt(16, getCustomFlags());
			statement.setInt(17, getObjectId());
			statement.executeUpdate();

			_existsInDb = true;
			_storedInDb = true;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not update item " + getObjectId() + " itemID " + _itemId + " count " + getCount() + " owner " + getOwnerId() + " in DB:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

	}

	/**
	 * Insert the item in database
	 */
	private synchronized void insertIntoDb()
	{
		if(_wear)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO items (owner_id,item_id,count,loc,loc_data,enchant_level,enchant_attr,enchant_attr_value,object_id,custom_type1,custom_type2,destination,source,create_type,create_time,shadow_life_time,name,class) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			statement.setInt(1, getOwnerId());
			statement.setInt(2, _itemId);
			statement.setLong(3, _count);
			statement.setString(4, _loc.name());
			statement.setInt(5, _loc_data);
			statement.setInt(6, getEnchantLevel());
			statement.setInt(7, _elementals == null ? -2 : _elementals.getElement());
			statement.setInt(8, _elementals == null ? 0 : _elementals.getValue());
			statement.setInt(9, getObjectId());
			statement.setInt(10, _type1);
			statement.setInt(11, _type2);
			statement.setInt(12, _destination);
			statement.setInt(13, _source);
			statement.setString(14, _createType);
			statement.setInt(15, _createTime);
			statement.setInt(16, _lifeTimeRemaining);
			statement.setString(17, getItem().getName());
			statement.setString(18, getItemClass().name());
			statement.executeUpdate();

			_existsInDb = true;
			_storedInDb = true;

			Stat.increaseInsertItemCount();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not insert item " + getObjectId() + "; itemID=" + _itemId + "; count=" + getCount() + "; owner=" + getOwnerId() + "; source=" + _source + "; destination=" + _destination + "; create_type=" + _createType + ":", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Delete item from database
	 */
	public synchronized void removeFromDb()
	{
		if(isWear() || !_existsInDb)
			return;

		// cancel lazy update task if need
		stopLazyUpdateTask(true);

		if(!_whflag)
			removeAugmentation();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM items WHERE object_id = ? LIMIT 1");
			statement.setInt(1, _objectId);
			statement.executeUpdate();

			_existsInDb = false;
			_storedInDb = false;

			Stat.increaseDeleteItemCount();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not delete item " + getObjectId() + " in DB:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Return true if item is hero-item
	 *
	 * @return boolean
	 */
	public boolean isHeroItem()
	{
		final int myid = _itemId;
		return myid >= 6611 && myid <= 6621 || myid >= 9388 && myid <= 9390 || myid == 6842;
	}

	/**
	 * Return true if item is ClanApella-item
	 *
	 * @return boolean
	 */
	public boolean isClanApellaItem()
	{
		final int myid = _itemId;
		return myid >= 7860 && myid <= 7879 || myid >= 9830 && myid <= 9839;
	}

	/**
	 * Return true if item can be destroyed
	 *
	 * @return boolean
	 */
	public boolean canBeDestroyed(final L2Player player)
	{
		if((_customFlags & FLAG_NO_DESTROY) == FLAG_NO_DESTROY)
			return false;

		// is hero item?
		if(isHeroItem())
			return false;

		if(PetDataTable.isPetControlItem(this) && player.isMounted())
			return false;

		if(player.getPet() != null && getObjectId() == player.getPet().getControlItemId())
			return false;

		if(CursedWeaponsManager.getInstance().isCursed(_itemId))
			return false;

		if(isEquipped())
			return false;

		if(isWear())
			return false;

		return _itemTemplate.isDestroyable();
	}

	/**
	 * Return true if item can be dropped
	 *
	 * @return boolean
	 */
	public boolean canBeDropped(final L2Player player)
	{
		if((_customFlags & FLAG_NO_DROP) == FLAG_NO_DROP)
			return false;

		// is hero item?
		if(isHeroItem())
			return false;

		// is shadow item?
		if(isShadowItem())
			return false;

		if(isTemporalItem())
			return false;

		if(isAugmented() && !Config.ALT_ALLOW_DROP_AUGMENTED)
			return false;

		if(PetDataTable.isPetControlItem(this) && player.isMounted())
			return false;

		if(player.getPet() != null && getObjectId() == player.getPet().getControlItemId())
			return false;

		if(_itemTemplate.isPvP())
			return false;

		if(isWear())
			return false;

		// is quest item?
		if(_itemTemplate.getType2() == L2Item.TYPE2_QUEST)
			return false;

		if(isCursed() || getItem().isCombatFlag() || getItem().isTerritoryFlag())
			return false;

		if(player.getActiveEnchantItem() == this)
			return false;

		if(player.getPlayerAccess().ProhibitedItems.contains(new Integer(_itemId)))
		{
			Log.add("try droped prohibited item(s) number " + _itemId + " in inventory", "gm_ext_actions", player);
			return false;
		}

		return _itemTemplate.isDropable();
	}

	/**
	 * Return true if item can be trade or sold
	 *
	 * @return boolean
	 */
	public boolean canBeTraded(final L2Player player)
	{
		if((_customFlags & FLAG_NO_TRADE) == FLAG_NO_TRADE)
			return false;

		// is hero item?
		if(isHeroItem())
			return false;

		if(isShadowItem())
			return false;

		if(isTemporalItem())
			return false;

		if(PetDataTable.isPetControlItem(this) && player.isMounted())
			return false;

		if(player.getPet() != null && getObjectId() == player.getPet().getControlItemId())
			return false;

		if(isAugmented() && !Config.ALT_ALLOW_DROP_AUGMENTED)
			return false;

		if(isCursed())
			return false;

		if(CursedWeaponsManager.getInstance().isCursed(_itemId))
			return false;

		if(isEquipped())
			return false;

		if(_itemTemplate.isPvP())
			return false;

		if(isWear())
			return false;

		if(getItem().getType2() == L2Item.TYPE2_QUEST)
			return false;

		if(player.getActiveEnchantItem() == this)
			return false;

		return _itemTemplate.isTradeable();
	}

	/**
	 * Можно лУ положУть на клановый склад УлУ передать фрейтом
	 *
	 * @param player
	 * @return
	 */
	public boolean canBeStored(final L2Player player, final boolean privatewh)
	{
		if((_customFlags & FLAG_NO_TRANSFER) == FLAG_NO_TRANSFER)
			return false;

		if(isHeroItem())
			return false;

		if(!privatewh && (isShadowItem() || isTemporalItem()))
			return false;

		if(!privatewh && isAugmented() && !Config.ALT_ALLOW_DROP_AUGMENTED)
			return false;
		// когда  Это мы могли продоватьв лавке ПВП шмот....
		if(!privatewh && _itemTemplate.isPvP())
			return false;

		if(PetDataTable.isPetControlItem(this) && player.isMounted())
			return false;

		if(player.getPet() != null && getObjectId() == player.getPet().getControlItemId())
			return false;

		if(CursedWeaponsManager.getInstance().isCursed(_itemId))
			return false;

		if(isCursed())
			return false;

		if(isEquipped())
			return false;

		if(isWear())
			return false;

		if(getItem().getType2() == 3)
			return false;

		if(player.getActiveEnchantItem() == this)
			return false;

		return privatewh || _itemTemplate.isTradeable();
	}

	/**
	 * Return true if item can be crystallized
	 *
	 * @return boolean
	 */
	public boolean canBeCrystallized(final L2Player player, final boolean msg)
	{
		if((_customFlags & FLAG_NO_CRYSTALLIZE) == FLAG_NO_CRYSTALLIZE)
			return false;

		// is hero item?
		if(isHeroItem())
			return false;

		if(isShadowItem())
			return false;

		if(isTemporalItem())
			return false;

		// can player crystallize?
		final int level = player.getSkillLevel(L2Skill.SKILL_CRYSTALLIZE);
		if(level < 1 || _itemTemplate.getCrystalType().cry - L2Item.CRYSTAL_D + 1 > level)
		{
			if(msg)
			{
				player.sendPacket(new SystemMessage(SystemMessage.CANNOT_CRYSTALLIZE_CRYSTALLIZATION_SKILL_LEVEL_TOO_LOW));
				player.sendActionFailed();
			}
			return false;
		}

		if(PetDataTable.isPetControlItem(this) && player.isMounted())
			return false;

		if(player.getPet() != null && getObjectId() == player.getPet().getControlItemId())
			return false;

		if(CursedWeaponsManager.getInstance().isCursed(_itemId))
			return false;

		if(isEquipped())
			return false;

		if(isWear())
			return false;

		if(getItem().getType2() == L2Item.TYPE2_QUEST)
			return false;

		// crystallizable?
		return _itemTemplate.isCrystallizable();
	}

	/**
	 * Returns the item in String format
	 *
	 * @return String
	 */
	@Override
	public String toString()
	{
		return _itemTemplate.toString();
	}

	public boolean isNightLure()
	{
		switch (_itemId)
		{
			case 8505: // Green Luminous Lure - Low Grade
			case 8508: // Purple Luminous Lure - Low Grade
			case 8511: // Yellow Luminous Lure - Low Grade
				return true;
			case 8496: // Gludio's Luminous Lure
			case 8497: // Dion's Luminous Lure
			case 8498: // Giran's Luminous Lure
			case 8499: // Oren's Luminous Lure
			case 8500: // Aden's Luminous Lure
			case 8501: // Innadril's Luminous Lure
			case 8502: // Goddard's Luminous Lure
			case 8503: // Rune's Luminous Lure
			case 8504: // Schuttgart's Luminous Lure
				return true;
			case 8506: // Green Luminous Lure
			case 8509: // Purple Luminous Lure
			case 8512: // Yellow Luminous Lure
				return true;
			case 8510: // Purple Luminous Lure - High Grade
			case 8513: // Yellow Luminous Lure - High Grade
				return true;
			case 8485: // Prize-Winning Night Fishing Lure
				return true;
			default:
				return false;
		}
	}

	/**
	 * Используется только для Shadow вещей
	 */
	public void shadowNotify(final boolean equipped)
	{
		if(!isShadowItem()) // Вещь не теневая? До свУданУя.
			return;

		if(!equipped) // ПрУ снятУУ прерывать таск
		{
			if(_itemLifeTimeTask != null)
				_itemLifeTimeTask.cancel(false);
			_itemLifeTimeTask = null;
			return;
		}

		if(_itemLifeTimeTask != null && !_itemLifeTimeTask.isDone()) // ЕслУ таск уже тУкает, то повторно дергать не надо
			return;

		final L2Player owner = getOwner();
		if(owner == null)
			return;

		setLifeTimeRemaining(owner, getLifeTimeRemaining() - 1);

		if(!checkDestruction(owner)) // ЕслУ у вещУ ещё есть мана - запустУть таск уменьшенУя
			_itemLifeTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new LifeTimeTask(), 60000);
	}

	public void startTemporalTask(final L2Player owner)
	{
		if(!isTemporalItem() || owner == null) // Вещь не временная? До свУданУя.
			return;

		if(_itemLifeTimeTask != null && !_itemLifeTimeTask.isDone()) // ЕслУ таск уже тУкает, то повторно дергать не надо
			return;

		if(!checkDestruction(owner)) // ЕслУ у вещУ ещё есть мана - запустУть таск уменьшенУя
			_itemLifeTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new LifeTimeTask(), 60000);
	}

	public boolean isShadowItem()
	{
		return _itemTemplate.isShadowItem();
	}

	public boolean isTemporalItem()
	{
		return _itemTemplate.isTemporal();
	}

	public boolean isCommonItem()
	{
		return _itemTemplate.isCommonItem();
	}

	public boolean isAltSeed()
	{
		return _itemTemplate.isAltSeed();
	}

	public boolean isCursed()
	{
		return _itemTemplate.isCursed();
	}

	/**
	 * true означает завершУть таск, false продолжУть
	 */
	private boolean checkDestruction(final L2Player owner)
	{
		if(!isShadowItem() && !isTemporalItem())
			return true;

		int left = getLifeTimeRemaining();
		if(isTemporalItem())
			left /= 60;
		if(left == 10 || left == 5 || left == 1 || left <= 0)
		{
			if(isShadowItem())
			{
				SystemMessage sm;
				if(left == 10)
					sm = new SystemMessage(SystemMessage.S1S_REMAINING_MANA_IS_NOW_10);
				else if(left == 5)
					sm = new SystemMessage(SystemMessage.S1S_REMAINING_MANA_IS_NOW_5);
				else if(left == 1)
					sm = new SystemMessage(SystemMessage.S1S_REMAINING_MANA_IS_NOW_1);
				else
					sm = new SystemMessage(SystemMessage.S1S_REMAINING_MANA_IS_NOW_0);
				sm.addItemName(getItemId());
				owner.sendPacket(sm);
			}

			if(left <= 0)
			{
				owner.getInventory().unEquipItem(this);
				owner.getInventory().destroyItem(this, getCount(), true);

				if(isTemporalItem())
					owner.sendPacket(new SystemMessage(SystemMessage.THE_LIMITED_TIME_ITEM_HAS_BEEN_DELETED));
				owner.sendPacket(new ItemList(owner, false)); // перестраховка
				owner.broadcastUserInfo(true);
				return true;
			}
		}

		return false;
	}

	public int getLifeTimeRemaining()
	{
		if(isTemporalItem())
			return _lifeTimeRemaining - (int) (System.currentTimeMillis() / 1000);
		return _lifeTimeRemaining;
	}

	public final void setLifeTimeRemaining(final L2Player owner, final int lt)
	{
		assert !isTemporalItem();

		_lifeTimeRemaining = lt;
		_storedInDb = false;

		if(owner != null)
			owner.sendPacket(new InventoryUpdate(this, L2ItemInstance.MODIFIED));
	}

	public final class LifeTimeTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2Player owner = getOwner();
			if(owner == null || !owner.isOnline())
				return;

			if(isShadowItem())
			{
				if(!isEquipped())
					return;
				setLifeTimeRemaining(owner, getLifeTimeRemaining() - 1);
			}

			if(checkDestruction(owner))
				return;

			_itemLifeTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(this, 60000); // У шэдовов 1 цУкл = 60 сек.
		}
	}

	public void dropToTheGround(final L2Player lastAttacker, final L2NpcInstance dropper)
	{
		if(dropper == null)
		{
			Location dropPos = Rnd.coordsRandomize(lastAttacker, 70);
			for(int i = 0; i < 20 && !GeoEngine.canMoveWithCollision(lastAttacker.getX(), lastAttacker.getY(), lastAttacker.getZ(), dropPos.x, dropPos.y, dropPos.z); i++)
				dropPos = Rnd.coordsRandomize(lastAttacker, 70);
			dropMe(lastAttacker, dropPos);
			if(Config.AUTODESTROY_ITEM_AFTER > 0 && !isCursed())
				ItemsAutoDestroy.getInstance().addItem(this);
			return;
		}

		Location dropPos = Rnd.coordsRandomize(dropper, 70);

		if(lastAttacker != null)
		{
			// 20 попыток уронУть дроп в точке смертУ моба
			for(int i = 0; i < 20 && !GeoEngine.canMoveWithCollision(dropper.getX(), dropper.getY(), dropper.getZ(), dropPos.x, dropPos.y, dropPos.z); i++)
				dropPos = Rnd.coordsRandomize(dropper, 70);

			// ЕслУ в точке смертУ моба дропу негде упасть, то падает под ногУ чару
			if(!GeoEngine.canMoveWithCollision(dropper.getX(), dropper.getY(), dropper.getZ(), dropPos.x, dropPos.y, dropPos.z))
				dropPos = lastAttacker.getLoc();
		}

		// Init the dropped L2ItemInstance and add it in the world as a visible object at the position where mob was last
		dropMe(dropper, dropPos);

		// Add drop to auto destroy item task
		if(isHerb())
			ItemsAutoDestroy.getInstance().addHerb(this);
		else if(Config.AUTODESTROY_ITEM_AFTER > 0 && !isCursed())
			ItemsAutoDestroy.getInstance().addItem(this);

		if(lastAttacker != null)
			setItemDropOwner(lastAttacker, Config.NON_OWNER_ITEM_PICKUP_DELAY + (dropper.isRaid() ? 285000 : 0));
	}

	/**
	 * Бросает вещь на землю туда, где ее можно поднять
	 *
	 * @param dropper
	 */
	public void dropToTheGround(final L2Character dropper, final Location dropPos)
	{
		if(GeoEngine.canMoveToCoord(dropper.getX(), dropper.getY(), dropper.getZ(), dropPos.x, dropPos.y, dropPos.z))
			dropMe(dropper, dropPos);
		else
			dropMe(dropper, dropper.getLoc());

		// Add drop to auto destroy item task
		if(Config.AUTODESTROY_ITEM_AFTER > 0)
			ItemsAutoDestroy.getInstance().addItem(this);
	}

	public boolean isDestroyable()
	{
		return true;
	}

	public void setWhFlag(final boolean whflag)
	{
		_whflag = whflag;
	}

	public ItemClass getItemClass()
	{
		return _itemTemplate.getItemClass();
	}

	public void setItemId(final int id)
	{
		_itemId = id;
		_itemTemplate = ItemTable.getInstance().getTemplate(id);
		_storedInDb = false;
	}

	public String getCreateType()
	{
		return _createType;
	}

	/**
	 * Возвращает защУту от элемента: огонь.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceFire()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_FIRE ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает защУту от элемента: вода.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceWater()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_WATER ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает защУту от элемента: воздух.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceWind()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_WIND ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает защУту от элемента: земля.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceEarth()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_EARTH ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает защУту от элемента: свет.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceHoly()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_SACRED ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает защУту от элемента: тьма.
	 *
	 * @return значенУе защУты
	 */
	public int getDefenceUnholy()
	{
		return _itemTemplate instanceof L2Armor && getAttributeElement() == Elementals.ATTRIBUTE_UNHOLY ? getAttributeElementValue() : 0;
	}

	/**
	 * Возвращает элемент атрУбуцУУ предмета.<br>
	 * Element (0 - Fire, 1 - Water, 2 - Wind, 3 - Earth, 4 - Holy, 5 - Dark, -2 - None)<br>
	 * АтрУбут атакУ для оружУя, атрУбут защУты для армора
	 *
	 * @return id элемента
	 */
	public byte getAttributeElement()
	{
		if(_enchantAttributeFuncTemplate == null)
			return Elementals.ATTRIBUTE_NONE;
		return getEnchantAttributeByStat(_enchantAttributeFuncTemplate._stat);
	}

	public int[] getAttackElement()
	{
		if(_enchantAttributeFuncTemplate == null || !(_itemTemplate instanceof L2Weapon))
			return new int[] { Elementals.ATTRIBUTE_NONE, 0 };
		return new int[] { getAttributeElement(), getAttributeElementValue() };
	}

	public Elementals getElementals()
	{
		return _elementals;
	}

	/**
	 * Возвращает значенУе элемента атрУбуцУУ предмета
	 *
	 * @return сУла элемента
	 */
	public int getAttributeElementValue()
	{
		return _elementals == null ? 0 : _elementals.getValue();
	}

	/**
	 * УстанавлУвает значенУе элемента атрУбуцУУ предмета
	 *
	 * @param value
	 *            сУла элемента
	 */
	public void setAttributeElementValue(final int value)
	{
		if(_elementals != null)
			_elementals.setValue(value);
	}

	public FuncTemplate getAttributeFuncTemplate()
	{
		return _enchantAttributeFuncTemplate;
	}

	/**
	 * УстанавлУвает элемент атрУбуцУУ предмета.<br>
	 * Element (0 - Fire, 1 - Water, 2 - Wind, 3 - Earth, 4 - Holy, 5 - Dark, -2 - None)
	 *
	 * @param element
	 *            элемент
	 */
	public void setAttributeElement(final byte element, final int value, final boolean updateDb)
	{
		final Stats stat = getStatByEnchantAttribute(element);

		if(stat == null || value == 0)
		{
			if(_enchantAttributeFuncTemplate != null)
			{
				detachFunction(_enchantAttributeFuncTemplate);
				_enchantAttributeFuncTemplate._lambda = new LambdaConst(0); // На всякий случай
			}

			if(_elementals == null)
				_elementals = new Elementals(element, value);
			else
			{
				_elementals.setElement(Elementals.ATTRIBUTE_NONE);
				_elementals.setValue(0);
			}

			if(updateDb)
			{
				_storedInDb = false;
				updateDatabase();
			}
			return;
		}

		if(_enchantAttributeFuncTemplate == null)
		{
			_enchantAttributeFuncTemplate = new FuncTemplate(null, null, "Add", stat, 0x40, value);
			attachFunction(_enchantAttributeFuncTemplate);
		}
		else
		{
			_enchantAttributeFuncTemplate._stat = stat;
			_enchantAttributeFuncTemplate._lambda = new LambdaConst(value);
		}

		if(_elementals == null)
			_elementals = new Elementals(element, value);
		else
		{
			_elementals.setElement(element);
			_elementals.setValue(value);
		}

		if(updateDb)
		{
			_storedInDb = false;
			updateDatabase();
		}
	}

	public byte getEnchantAttributeByStat(final Stats stat)
	{
		switch (stat)
		{
			case ATTACK_ELEMENT_FIRE:
			case FIRE_RECEPTIVE:
				return Elementals.ATTRIBUTE_FIRE;
			case ATTACK_ELEMENT_WATER:
			case WATER_RECEPTIVE:
				return Elementals.ATTRIBUTE_WATER;
			case ATTACK_ELEMENT_EARTH:
			case EARTH_RECEPTIVE:
				return Elementals.ATTRIBUTE_EARTH;
			case ATTACK_ELEMENT_WIND:
			case WIND_RECEPTIVE:
				return Elementals.ATTRIBUTE_WIND;
			case ATTACK_ELEMENT_UNHOLY:
			case UNHOLY_RECEPTIVE:
				return Elementals.ATTRIBUTE_UNHOLY;
			case ATTACK_ELEMENT_SACRED:
			case SACRED_RECEPTIVE:
				return Elementals.ATTRIBUTE_SACRED;
			default:
				return Elementals.ATTRIBUTE_NONE;
		}
	}

	public Stats getStatByEnchantAttribute(final byte attribute)
	{
		if(getItem() instanceof L2Weapon)
			switch (attribute)
			{
				case Elementals.ATTRIBUTE_FIRE:
					return Stats.ATTACK_ELEMENT_FIRE;
				case Elementals.ATTRIBUTE_WATER:
					return Stats.ATTACK_ELEMENT_WATER;
				case Elementals.ATTRIBUTE_EARTH:
					return Stats.ATTACK_ELEMENT_EARTH;
				case Elementals.ATTRIBUTE_WIND:
					return Stats.ATTACK_ELEMENT_WIND;
				case Elementals.ATTRIBUTE_UNHOLY:
					return Stats.ATTACK_ELEMENT_UNHOLY;
				case Elementals.ATTRIBUTE_SACRED:
					return Stats.ATTACK_ELEMENT_SACRED;
			}
		else
			switch (attribute)
			{
				case Elementals.ATTRIBUTE_FIRE:
					return Stats.FIRE_RECEPTIVE;
				case Elementals.ATTRIBUTE_WATER:
					return Stats.WATER_RECEPTIVE;
				case Elementals.ATTRIBUTE_EARTH:
					return Stats.EARTH_RECEPTIVE;
				case Elementals.ATTRIBUTE_WIND:
					return Stats.WIND_RECEPTIVE;
				case Elementals.ATTRIBUTE_UNHOLY:
					return Stats.UNHOLY_RECEPTIVE;
				case Elementals.ATTRIBUTE_SACRED:
					return Stats.SACRED_RECEPTIVE;
			}
		return null;
	}

	/**
	 * Проверяет, является лУ данный Унстанс предмета хербом
	 *
	 * @return true еслУ предмет является хербом
	 */
	public boolean isHerb()
	{
		return _itemTemplate.isHerb();
	}

	public boolean isAdena()
	{
		return _itemId == 57 // Adena
				|| _itemId == 4037 // Coin of Luck
				|| _itemId == 5575 // Ancient Adena
				|| _itemId == 6360 // Blue Seal Stone
				|| _itemId == 6361 // Green Seal Stone
				|| _itemId == 6362 // Red Seal Stone
				|| _itemId == 9142 // Apiga
				|| _itemId == 9143; // Golden Apiga
	}

	public Grade getCrystalType()
	{
		return _itemTemplate.getCrystalType();
	}

	public void setCustomFlags(final int i, final boolean updateDb)
	{
		if(_customFlags != i)
		{
			_customFlags = i;
			if(updateDb)
				updateDatabase();
			else
				_storedInDb = false;
		}
	}

	public int getCustomFlags()
	{
		return _customFlags;
	}

	public boolean isOlyRestrictedItem()
	{
		return Config.OLY_LIST_RESTRICTED_ITEMS.contains(_itemId);
	}

	@Override
	public String getName()
	{
		return getItem().getName();
	}
}
