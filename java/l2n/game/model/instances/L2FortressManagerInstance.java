package l2n.game.model.instances;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

public class L2FortressManagerInstance extends L2ResidenceManager
{
	public L2FortressManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename = "data/html/fortress/foreman-no.htm";
		int condition = validateCondition(player);
		if(condition > COND_ALL_FALSE)
			if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
				filename = "data/html/fortress/foreman-busy.htm";
			else if(condition == COND_OWNER)
				filename = "data/html/fortress/foreman.htm";
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	protected int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;
		if(player.getClan() != null)
		{
			if(getResidence().getSiege().isInProgress() || TerritorySiege.isInProgress())
				return COND_BUSY_BECAUSE_OF_SIEGE;
			if(getResidence().getOwnerId() == player.getClanId())
				return COND_OWNER;
		}
		return COND_ALL_FALSE;
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;
		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken();

		if(actualCommand.equalsIgnoreCase("receive_report"))
		{
			SimpleDateFormat format2 = new SimpleDateFormat("HH");
			SimpleDateFormat format3 = new SimpleDateFormat("mm");
			NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());

			int ownedTime = (int) (System.currentTimeMillis() / 1000 - getFortress().getOwnDate());
			if(getFortress().getFortState() < 2)
			{
				html.setFile("data/html/fortress/foreman-report.htm");
				html.replace("%objectId%", String.valueOf(getObjectId()));
				html.replace("%hr%", format2.format(ownedTime));
				html.replace("%min%", format3.format(ownedTime));
				player.sendPacket(html);
				return;
			}

			html.setFile("data/html/fortress/report.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			html.replace("%hr%", format2.format(ownedTime));
			html.replace("%min%", format3.format(ownedTime));
			player.sendPacket(html);
			return;
		}

		super.onBypassFeedback(player, command);
	}

	@Override
	protected Residence getResidence()
	{
		return getFortress();
	}

	@Override
	public void broadcastDecoInfo()
	{}

	@Override
	protected int getPrivFunctions()
	{
		return L2Clan.CP_CS_SET_FUNCTIONS;
	}

	@Override
	protected int getPrivDismiss()
	{
		return L2Clan.CP_CS_DISMISS;
	}

	@Override
	protected int getPrivDoors()
	{
		return L2Clan.CP_CS_OPEN_DOOR;
	}
}
