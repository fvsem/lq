package l2n.game.model.instances;

import l2n.game.L2GameThreadPools;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.tables.AirShipDocksTable;
import l2n.game.tables.AirShipDocksTable.AirShipDock;
import l2n.game.templates.L2NpcTemplate;

import java.util.concurrent.ScheduledFuture;

public class L2AirShipControllerInstance extends L2NpcInstance
{
	private ScheduledFuture<AutoDepartureTask> _autoDepartureTask;
	private static final long AUTO_DEPARTURE_TASK_DELAY = 5 * 60 * 1000L; // 5 min

	public L2AirShipControllerInstance(final int objectID, final L2NpcTemplate template)
	{
		super(objectID, template);
	}

	public void startDepartureTask(final L2AirShip newAirship)
	{
		if(_autoDepartureTask != null)
			_autoDepartureTask.cancel(true);
		_autoDepartureTask = L2GameThreadPools.getInstance().scheduleGeneral(new AutoDepartureTask(newAirship), AUTO_DEPARTURE_TASK_DELAY);
	}

	private class AutoDepartureTask implements Runnable
	{
		private final L2AirShip _airship;

		public AutoDepartureTask(final L2AirShip airship)
		{
			_airship = airship;
		}

		@Override
		public void run()
		{
			if(!_airship.isDocked() || _airship.isMoving)
				return;
			final AirShipDock ad = AirShipDocksTable.getInstance().getAirShipDockByNpcId(getNpcId());
			_airship.SetTrajet1(ad.getDepartureTrajetId(), 0, null, null);
			_airship._cycle = 1;
			_airship.begin();
		}
	}
}
