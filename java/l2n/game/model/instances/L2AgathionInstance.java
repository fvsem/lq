package l2n.game.model.instances;

import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO: Возможно скилы призыва/отзыва агнишена должны дергаться автоматом при снимании/одевании вещи.
 * Если так, то надо заюзать BraceletListener по полной, а не только на снимание.
 */
public class L2AgathionInstance
{
	protected static final Logger _log = Logger.getLogger(L2AgathionInstance.class.getName());

	private static final int BEASTLY_AGATHION_ID = 16026;
	private static final int RAINBOW_AGATHION_ID = 16027;
	private static final int CASTLE_LORD_AGATHION_ID = 16028;
	private static final int FORTRESS_AGATHION_ID = 16029;
	private static final int ANGEL_AGATHION_ID = 16031;
	private static final int DEVIL_AGATHION_ID = 16032;
	private static final int RUDOLPH_AGATHION_ID = 16033;
	private static final int LOVE_AGATHION_ID = 16049;

	public static final int MAJO_AGATHION_ID = 1501;
	public static final int GOLD_MAJO_AGATHION_ID = 1502;
	public static final int BLACK_MAJO_AGATHION_ID = 1503;
	public static final int PLAIPITAK_AGATHION_ID = 1504;
	public static final int BABY_PANDA_AGATHION_ID = 1505;
	public static final int BAMBOO_PANDA_AGATHION_ID = 1506;
	public static final int SEXY_PANDA_AGATHION_ID = 1507;
	public static final int CHARMING_CUPID_AGATHION_ID = 1508;
	public static final int NAUGHTY_CUPID_AGATHION_ID = 1509;
	public static final int WHITE_MANEKI_AGATHION_ID = 1510;
	public static final int BLACK_MANEKI_AGATHION_ID = 1511;
	public static final int BROWN_MANEKI_AGATHION_ID = 1512;
	public static final int BAT_DROVE_AGATHION_ID = 1513;
	public static final int PEGASUS_AGATHION_ID = 1514;
	public static final int YELLOW_ROBED_TOJIGONG_AGATHION_ID = 1515;
	public static final int BLUE_ROBED_TOJIGONG_AGATHION_ID = 1516;
	public static final int GREEN_ROBED_TOJIGONG_AGATHION_ID = 1517;
	public static final int BUGBEAR_AGATHION_ID = 1518;

	private IHardReference<L2Player> _ownerRef = HardReferences.emptyRef();
	private IHardReference<? extends L2Character> _targetRef = HardReferences.emptyRef();

	private int _id;

	/** скилы агнишена */
	private GArray<L2Skill> _skills = new GArray<L2Skill>();
	/** скилы, добавляемые хозяину */
	private GArray<L2Skill> _addSkills = new GArray<L2Skill>();

	private Future<Action> _actionTask;

	public L2AgathionInstance(final L2Player owner, final int id)
	{
		_ownerRef = owner.getRef();
		_id = id;

		switch (id)
		{
			case BEASTLY_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5536, 1)); // Little Devil Agathion Cuteness Attack
				_addSkills.add(SkillTable.getInstance().getInfo(5542, 1)); // Firecrackers
				_addSkills.add(SkillTable.getInstance().getInfo(5543, 1)); // Little Devil Agathion Special Skill - Mysterious Medicinal Liquid
				_addSkills.add(SkillTable.getInstance().getInfo(5544, 1)); // BSOE
				_addSkills.add(SkillTable.getInstance().getInfo(5545, 1)); // BRES
				break;
			case RAINBOW_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5535, 1)); // Little Angel Agathion Cuteness Attack
				_addSkills.add(SkillTable.getInstance().getInfo(5538, 1)); // Firecrackers
				_addSkills.add(SkillTable.getInstance().getInfo(5539, 1)); // Little Angel Agathion Special Skill - Mysterious Medicinal Liquid
				_addSkills.add(SkillTable.getInstance().getInfo(5540, 1)); // BSOE
				_addSkills.add(SkillTable.getInstance().getInfo(5541, 1)); // BRES
				break;
			case CASTLE_LORD_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5537, 1)); // Rudolph Agathion Cuteness Attack
				break;
			case FORTRESS_AGATHION_ID:
				_addSkills.add(SkillTable.getInstance().getInfo(5458, 1)); // Performing Agathion - Fortress
				break;
			case ANGEL_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5535, 1)); // Little Angel Agathion Cuteness Attack
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 10316: // Agathion Seal Bracelet - Little Angel - Firecracker
						_addSkills.add(SkillTable.getInstance().getInfo(5538, 1)); // Little Angel Agathion Special Skill - Firecrackers
						break;
					case 10317: // Agathion Seal Bracelet - Little Angel - Big Head
						_addSkills.add(SkillTable.getInstance().getInfo(5539, 1)); // Little Angel Agathion Special Skill - Mysterious Medicinal Liquid
						break;
					case 10318: // Agathion Seal Bracelet - Little Angel - Escape
						_addSkills.add(SkillTable.getInstance().getInfo(5540, 1)); // Little Angel Agathion Special Skill - Blessed Scroll of Escape
						break;
					case 10319: // Agathion Seal Bracelet - Little Angel - Resurrection
						_addSkills.add(SkillTable.getInstance().getInfo(5541, 1)); // Little Angel Agathion Special Skill - Blessed Scroll of Resurrection
				}
				break;
			case DEVIL_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5536, 1)); // Little Devil Agathion Cuteness Attack
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 10322: // Agathion Seal Bracelet - Little Devil - Firecracker
						_addSkills.add(SkillTable.getInstance().getInfo(5542, 1)); // Little Devil Agathion Special Skill - Firecrackers
						break;
					case 10323: // Agathion Seal Bracelet - Little Devil - Big Head
						_addSkills.add(SkillTable.getInstance().getInfo(5543, 1)); // Little Devil Agathion Special Skill - Mysterious Medicinal Liquid
						break;
					case 10324: // Agathion Seal Bracelet - Little Devil - Escape
						_addSkills.add(SkillTable.getInstance().getInfo(5544, 1)); // Little Devil Agathion Special Skill - Blessed Scroll of Escape
						break;
					case 10325: // Agathion Seal Bracelet - Little Devil - Resurrection
						_addSkills.add(SkillTable.getInstance().getInfo(5545, 1)); // Little Devil Agathion Special Skill - Blessed Scroll of Resurrection
				}
				break;
			case RUDOLPH_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(5537, 1)); // Rudolph Agathion Cuteness Attack
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20094: // Agathion Seal Bracelet: Rudolph (Energy - 30 days limited time)
						_addSkills.add(SkillTable.getInstance().getInfo(23016, 1)); // net takogo skilla O_o
				}
				break;
			case MAJO_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23000, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20009: // Agathion Seal Bracelet - Majo - Big Head 30-day limited period
						_addSkills.add(SkillTable.getInstance().getInfo(23004, 1)); // Majo Agathion Special Skill - Mysterious Power
				}
				break;
			case GOLD_MAJO_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23001, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20010:
						_addSkills.add(SkillTable.getInstance().getInfo(23005, 1));
				}
				break;
			case BLACK_MAJO_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23002, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20011:
						_addSkills.add(SkillTable.getInstance().getInfo(23006, 1));
				}
				break;
			case PLAIPITAK_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23003, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20013:
						_addSkills.add(SkillTable.getInstance().getInfo(23007, 1));
						break;
					case 20014:
						_addSkills.add(SkillTable.getInstance().getInfo(23009, 1));
						break;
					case 20015:
						_addSkills.add(SkillTable.getInstance().getInfo(23008, 1));
				}
				break;
			case BABY_PANDA_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23010, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20066:
						_addSkills.add(SkillTable.getInstance().getInfo(23013, 1));
				}
				break;
			case BAMBOO_PANDA_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23011, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20067:
						_addSkills.add(SkillTable.getInstance().getInfo(23014, 1));
				}
				break;
			case SEXY_PANDA_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23012, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20068:
						_addSkills.add(SkillTable.getInstance().getInfo(23015, 1));
				}
				break;
			case CHARMING_CUPID_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23025, 1));
				break;
			case NAUGHTY_CUPID_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23026, 1));
				break;
			case WHITE_MANEKI_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23027, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20224:
						_addSkills.add(SkillTable.getInstance().getInfo(23030, 1));
				}
				break;
			case BLACK_MANEKI_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23028, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20225:
						_addSkills.add(SkillTable.getInstance().getInfo(23031, 1));
				}
				break;
			case BROWN_MANEKI_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23029, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20226:
						_addSkills.add(SkillTable.getInstance().getInfo(23032, 1));
				}
				break;
			case BAT_DROVE_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23033, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20231:
						_addSkills.add(SkillTable.getInstance().getInfo(23034, 1));
						break;
					case 20232:
						_addSkills.add(SkillTable.getInstance().getInfo(23035, 1));
				}
				break;
			case PEGASUS_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23036, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20237:
						_addSkills.add(SkillTable.getInstance().getInfo(23037, 1));
						break;
					case 20238:
						_addSkills.add(SkillTable.getInstance().getInfo(23038, 1));
				}
				break;
			case YELLOW_ROBED_TOJIGONG_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23039, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20248:
						_addSkills.add(SkillTable.getInstance().getInfo(23042, 1));
				}
				break;
			case BLUE_ROBED_TOJIGONG_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23040, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20249:
						_addSkills.add(SkillTable.getInstance().getInfo(23043, 1));
				}
				break;
			case GREEN_ROBED_TOJIGONG_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23041, 1));
				switch (owner.getInventory().getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET))
				{
					case 20250:
						_addSkills.add(SkillTable.getInstance().getInfo(23044, 1));
				}
				break;
			case BUGBEAR_AGATHION_ID:
				_skills.add(SkillTable.getInstance().getInfo(23045, 1));
				break;
			default:
				return;
		}

		_addSkills.add(SkillTable.getInstance().getInfo(3267, 1)); // Dismiss Agathion

		// Выдаем скилы хозяину
		for(final L2Skill s : _addSkills)
			owner.addSkill(s);

		owner.sendPacket(new SkillList(owner));

	}

	public void doAction(final L2Character target)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
		{
			deleteMe();
			return;
		}
		// если таргет не менялся. то ничего не делаем
		if(getTargeObjId() == target.getObjectId())
			return;

		if(owner == target)
			return;

		stopAction();
		_targetRef = target.getRef();
		switch (_id)
		{
			case BEASTLY_AGATHION_ID:
			case RAINBOW_AGATHION_ID:
			case CASTLE_LORD_AGATHION_ID:
			case ANGEL_AGATHION_ID:
			case DEVIL_AGATHION_ID:
				_actionTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new Action(), 100, 10000, false);
				break;
			case FORTRESS_AGATHION_ID:
				_actionTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new Action(), 100, 8000, false);
				break;
		}
	}

	public int getId()
	{
		return _id;
	}

	public void stopAction()
	{
		_targetRef = HardReferences.emptyRef();
		try
		{
			if(_actionTask != null)
				_actionTask.cancel(false);
		}
		catch(final NullPointerException e)
		{}
		_actionTask = null;
	}

	public void deleteMe()
	{
		stopAction();

		final L2Player owner = getPlayer();
		if(owner != null)
		{
			// Забираем скилы у хозяина
			for(final L2Skill s : _addSkills)
				owner.removeSkill(s);
			owner.sendPacket(new SkillList(owner));
		}

		_ownerRef = HardReferences.emptyRef();
		_skills = null;
		_addSkills = null;
	}

	private class Action implements Runnable
	{
		@Override
		public void run()
		{
			final L2Player owner = getPlayer();
			if(owner == null)
			{
				deleteMe();
				return;
			}

			try
			{
				final L2Character target = getTarget();
				if(target != null && target.isPlayer() && !_skills.isEmpty())
				{
					final L2Skill skill = _skills.get(Rnd.get(_skills.size()));
					if(owner.isDead() || target == null || target.isDead() || !owner.isInRangeZ(target, skill.getCastRange()))
					{
						stopAction();
						if(owner.isDead())
							owner.setAgathion(0);
						return;
					}

					if(Rnd.chance(50) && skill.checkCondition(owner, target, false, false, true))
					{
						owner.altUseSkill(skill, target);
						owner.broadcastPacket(new MagicSkillUse(owner, target, skill.getId(), 1, 0, 0));
					}
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "L2AgathionInstance.Action error: ", e);
			}
		}
	}

	private final int getTargeObjId()
	{
		L2Character target = _targetRef.get();
		return target == null ? 0 : target.getObjectId();
	}

	public final L2Player getPlayer()
	{
		return _ownerRef.get();
	}

	private final L2Character getTarget()
	{
		return _targetRef.get();
	}
}
