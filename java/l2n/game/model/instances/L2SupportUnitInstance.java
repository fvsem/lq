package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

public class L2SupportUnitInstance extends L2NpcInstance
{
	public L2SupportUnitInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendActionFailed();
		if(!isInRange(player, INTERACTION_DISTANCE))
			return;

		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE || condition == COND_BUSY_BECAUSE_OF_SIEGE)
			return;

		if((player.getClanPrivileges() & L2Clan.CP_CS_USE_FUNCTIONS) != L2Clan.CP_CS_USE_FUNCTIONS)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ResidenceManager.NotAuthorizedToDoThis", player));
			return;
		}

		if(condition == COND_OWNER)
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		player.sendActionFailed();
		String filename = "data/html/fortress/SupportUnitCaptain-no.htm";

		int condition = validateCondition(player);
		if(condition > COND_ALL_FALSE)
			if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
				filename = "data/html/fortress/SupportUnitCaptain-busy.htm";
			else if(condition == COND_OWNER)
				if(val == COND_ALL_FALSE)
					filename = "data/html/fortress/SupportUnitCaptain.htm";
				else
					filename = "data/html/fortress/SupportUnitCaptain-" + val + ".htm";

		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile(filename);
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcname%", getName());
		player.sendPacket(html);
	}

	protected int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;
		if(getFortress() != null && getFortress().getId() > 0 && player.getClan() != null)
		{
			if(getFortress().getSiege().isInProgress() || TerritorySiege.isInProgress())
				return COND_BUSY_BECAUSE_OF_SIEGE;
			if(getFortress().getOwnerId() == player.getClanId())
				return COND_OWNER;
		}
		return COND_ALL_FALSE;
	}
}
