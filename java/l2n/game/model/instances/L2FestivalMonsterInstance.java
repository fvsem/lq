package l2n.game.model.instances;

import l2n.commons.list.GArray;
import l2n.game.ai.CtrlEvent;
import l2n.game.model.L2Party;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

/**
 * L2FestivalMonsterInstance
 * This class manages all attackable festival NPCs, spawned during the Festival of Darkness.
 */
public class L2FestivalMonsterInstance extends L2MonsterInstance
{
	protected int _bonusMultiplier = 1;

	/**
	 * Constructor of L2FestivalMonsterInstance (use L2Character and L2NpcInstance constructor).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Call the L2Character constructor to set the _template of the L2FestivalMonsterInstance (copy skills from template to object and link _calculators to NPC_STD_CALCULATOR)</li> <li>Set the name of the L2MonsterInstance</li> <li>Create a
	 * RandomAnimation Task that will be launched after the calculated delay if the server allow it</li><BR>
	 * <BR>
	 * 
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @param L2NpcTemplate
	 *            Template to apply to the NPC
	 */
	public L2FestivalMonsterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public void setOfferingBonus(int bonusMultiplier)
	{
		_bonusMultiplier = bonusMultiplier;
	}

	@Override
	public void onSpawn()
	{
		super.onSpawn();
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();

		GArray<L2Player> pl = L2World.getAroundPlayers(this);
		if(pl.isEmpty())
			return;
		GArray<L2Player> alive = new GArray<L2Player>(9);
		for(L2Player p : pl)
			if(!p.isDead())
				alive.add(p);
		if(alive.isEmpty())
			return;

		L2Player target = alive.get(Rnd.get(alive.size()));
		getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, target, 1);
	}

	/**
	 * Return True if the attacker is not another L2FestivalMonsterInstance.<BR>
	 * <BR>
	 */
	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		return !(attacker instanceof L2FestivalMonsterInstance);

	}

	/**
	 * Actions: <li>Check if the killing object is a player, and then find the party they belong to.</li> <li>Add a blood offering item to the leader of the party.</li> <li>Update the party leader's inventory to show the new item addition.</li>
	 */
	@Override
	public void doItemDrop(L2Character lastAttacker)
	{
		super.doItemDrop(lastAttacker);

		if(!lastAttacker.isPlayable())
			return;

		L2Player killingChar = lastAttacker.getPlayer();
		L2Party associatedParty = killingChar.getParty();
		if(associatedParty == null)
			return;

		L2Player partyLeader = associatedParty.getPartyLeader();
		if(partyLeader == null)
			return;
		L2ItemInstance bloodOfferings = ItemTable.getInstance().createItem(SevenSignsFestival.FESTIVAL_OFFERING_ID, killingChar.getObjectId(), getObjectId(), "L2FestivalMonsterInstance.doItemDrop");
		int mult = 1;
		if(getChampion() == 1)
			mult = 12;
		else if(getChampion() == 2)
			mult = 75;

		bloodOfferings.setCount(_bonusMultiplier * mult);
		partyLeader.getInventory().addItem(bloodOfferings);
		partyLeader.sendPacket(SystemMessage.obtainItems(bloodOfferings));
	}

	/**
	 * All mobs in the festival really don't need random animation.
	 */
	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public boolean isAggressive()
	{
		return true;
	}

	@Override
	public int getAggroRange()
	{
		return 1000;
	}
}
