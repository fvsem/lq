package l2n.game.model.instances;

import l2n.game.instancemanager.KrateisCubeManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 12.04.2010
 * @time 1:50:16
 */
public class L2KrateisCubeManagerInstance extends L2NpcInstance
{
	public L2KrateisCubeManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(command.startsWith("Register"))
		{
			if(player.getInventoryLimit() * 0.8 <= player.getInventory().getSize())
			{
				player.sendPacket(new SystemMessage(SystemMessage.PROGRESS_IN_A_QUEST_IS_POSSIBLE_ONLY_WHEN_YOUR_INVENTORYS_WEIGHT_AND_VOLUME_ARE_LESS_THAN_80_PERCENT_OF_CAPACITY));
				showChatWindow(player, "data/html/krateisCube/32503-9.htm");
				return;
			}
			int cmdChoice = Integer.parseInt(command.substring(9, 10).trim());
			switch (cmdChoice)
			{
				case 1:
					if(player.getLevel() < 70 || player.getLevel() > 75)
					{
						showChatWindow(player, "data/html/krateisCube/32503-7.htm");
						return;
					}
					break;
				case 2:
					if(player.getLevel() < 76 || player.getLevel() > 79)
					{
						showChatWindow(player, "data/html/krateisCube/32503-7.htm");
						return;
					}
					break;
				case 3:
					if(player.getLevel() < 80)
					{
						showChatWindow(player, "data/html/krateisCube/32503-7.htm");
						return;
					}
					break;
			}
			if(KrateisCubeManager.getInstance().isTimeToRegister())
			{
				if(KrateisCubeManager.getInstance().registerPlayer(player))
				{
					showChatWindow(player, "data/html/krateisCube/32503-4.htm");
					return;
				}
				showChatWindow(player, "data/html/krateisCube/32503-5.htm");
				return;
			}
			showChatWindow(player, "data/html/krateisCube/32503-8.htm");
			return;
		}
		if(command.startsWith("Cancel"))
		{
			KrateisCubeManager.getInstance().removePlayer(player);
			showChatWindow(player, "data/html/krateisCube/32503-6.htm");
			return;
		}
		if(command.startsWith("TeleportIn"))
		{
			KrateisCubeManager.getInstance().teleportPlayerIn(player);
			return;
		}
		if(command.startsWith("goto"))
		{
			player.teleToLocation(-59224, -56837, -2032);
			return;
		}

		super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom = "";
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		return "data/html/krateisCube/" + pom + ".htm";
	}
}
