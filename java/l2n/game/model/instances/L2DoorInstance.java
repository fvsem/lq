package l2n.game.model.instances;

import l2n.Config;
import l2n.extensions.scripts.Events;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.L2CharacterAI;
import l2n.game.ai.L2StaticObjectAI;
import l2n.game.geodata.GeoControl;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.residence.*;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.game.network.serverpackets.StaticObject;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.game.templates.L2CharTemplate;
import l2n.game.templates.L2Weapon;

import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class L2DoorInstance extends L2Character implements GeoControl
{
	protected static Logger _log = Logger.getLogger(L2DoorInstance.class.getName());

	protected final int _doorId;
	protected final String _name;
	private boolean _open;
	public boolean geoOpen;
	private boolean _geodata = true;
	private final boolean _unlockable;
	private boolean _isHPVisible;
	private boolean _siegeWeaponOlnyAttackable;
	private Residence _siegeUnit;
	private int upgradeHp;

	private L2Territory geoPos;
	private HashMap<Long, Byte> geoAround;

	public int key;
	public byte level = 1;

	protected int _autoActionDelay = -1;
	private ScheduledFuture<?> _autoActionTask;

	@Override
	public L2Territory getGeoPos()
	{
		return geoPos;
	}

	@Override
	public void setGeoPos(final L2Territory value)
	{
		geoPos = value;
	}

	@Override
	public HashMap<Long, Byte> getGeoAround()
	{
		return geoAround;
	}

	@Override
	public void setGeoAround(final HashMap<Long, Byte> value)
	{
		geoAround = value;
	}

	@Override
	public L2CharacterAI getAI()
	{
		if(_ai == null)
			_ai = new L2StaticObjectAI(this);
		return _ai;
	}

	public L2DoorInstance(final int objectId, final L2CharTemplate template, final int doorId, final String name, final boolean unlockable, final boolean showHp)
	{
		super(objectId, template);
		_doorId = doorId;
		_name = name;
		_unlockable = unlockable;
		_isHPVisible = showHp;
		geoOpen = true;
		setInvul(true);
	}

	public final boolean isUnlockable()
	{
		return _unlockable;
	}

	@Override
	public final byte getLevel()
	{
		return level;
	}

	/**
	 * @return Returns the doorId.
	 */
	public int getDoorId()
	{
		return _doorId;
	}

	/**
	 * @return Returns the open.
	 */
	public boolean isOpen()
	{
		return _open;
	}

	/**
	 * @param open
	 *            The open to set.
	 */
	public synchronized void setOpen(final boolean open)
	{
		_open = open;
	}

	/**
	 * Sets the delay in milliseconds for automatic opening/closing of this door instance. <BR>
	 * <B>Note:</B> A value of -1 cancels the auto open/close task.
	 * 
	 * @param actionDelay
	 *            время задержки между действием
	 */
	public void setAutoActionDelay(final int actionDelay)
	{
		if(_autoActionDelay == actionDelay)
			return;

		if(_autoActionTask != null)
		{
			_autoActionTask.cancel(false);
			_autoActionTask = null;
		}

		if(actionDelay > -1)
			_autoActionTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new AutoOpenClose(), actionDelay, actionDelay);

		_autoActionDelay = actionDelay;
	}

	public int getDamage()
	{
		final int dmg = 6 - (int) Math.ceil(getCurrentHpRatio() * 6);
		if(dmg > 6)
			return 6;
		if(dmg < 0)
			return 0;
		return dmg;
	}

	public boolean isEnemyOf(final L2Character cha)
	{
		return true;
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return isAttackable(attacker);
	}

	@Override
	public boolean isAttackable(final L2Character attacker)
	{
		if(attacker == null)
			return false;

		if(isSiegeWeaponOnlyAttackable() && (!(attacker instanceof L2Summon) || !((L2Summon) attacker).isSiegeWeapon()))
			return false;

		final L2Player player = attacker.getPlayer();
		if(player == null)
			return false;
		final L2Clan clan = player.getClan();
		if(clan != null && SiegeManager.getSiege(this, true) == clan.getSiege() && clan.isDefender())
			return false;
		if(player.getTerritorySiege() > -1 && getSiegeUnit() != null && player.getTerritorySiege() == getSiegeUnit().getId())
			return false;
		if(clan != null && getSiegeUnit() != null && clan.getClanId() == getSiegeUnit().getOwnerId() && player.getTerritorySiege() == -1)
			return false;
		return !isInvul();
	}

	/**
	 * Return null.<BR>
	 * <BR>
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getActiveWeaponItem()
	{
		return null;
	}

	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		return null;
	}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		if(player == null)
			return;

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(this != player.getTarget())
		{
			player.setTarget(this);
			player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel()));

			if(isAutoAttackable(player))
				// player.sendPacket(new DoorStatusUpdate(this));
				player.sendPacket(new StaticObject(this));

			// correct location
			player.sendPacket(new ValidateLocation(this));
		}
		else
		{
			player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel()));
			if(isAutoAttackable(player))
				player.getAI().Attack(this, false, shift);
			else if(!isInRange(player, INTERACTION_DISTANCE))
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
			else if(Config.ALLOW_CH_DOOR_OPEN_ON_CLICK && player.getClan() != null && getSiegeUnit() != null && player.getClanId() == getSiegeUnit().getOwnerId())
			{
				if(getSiegeUnit() instanceof Castle)
				{
					if((player.getClanPrivileges() & L2Clan.CP_CS_OPEN_DOOR) == L2Clan.CP_CS_OPEN_DOOR)
						switchOpenClose();
				}
				else if(getSiegeUnit() instanceof Fortress)
				{
					if((player.getClanPrivileges() & L2Clan.CP_CS_OPEN_DOOR) == L2Clan.CP_CS_OPEN_DOOR)
						switchOpenClose();
				}
				else if(getSiegeUnit() instanceof ClanHall)
					if((player.getClanPrivileges() & L2Clan.CP_CH_OPEN_DOOR) == L2Clan.CP_CH_OPEN_DOOR)
						switchOpenClose();
				player.sendActionFailed();
			}
			// Для квеста Seven Signs - Seal of the Emperor (Семь Печатей, Печать Императора): голубые двери открываются щелчком на них
			else if(_doorId == 17240102 || _doorId == 17240104 || _doorId == 17240106 || _doorId == 17240108 || _doorId == 17240110/* || _doorId == 17240111 */)
				openMe();
			else
				player.sendActionFailed();
		}
	}

	public void switchOpenClose()
	{
		if(!isOpen())
			openMe();
		else
			closeMe();
	}

	@Override
	public void broadcastStatusUpdate()
	{
		final StaticObject su = new StaticObject(this);
		for(final L2Player player : L2World.getAroundPlayers(this))
			if(player != null)
				player.sendPacket(su);
	}

	public void onOpen()
	{
		scheduleCloseMe(60000);
	}

	public void onClose()
	{
		closeMe();
	}

	/**
	 * Вызывает задание на закрытие двери через заданное время.
	 * 
	 * @param delay
	 *            Время в миллисекундах
	 */
	public final void scheduleCloseMe(final long delay)
	{
		L2GameThreadPools.getInstance().scheduleGeneral(new CloseTask(), delay);
	}

	public final void closeMe()
	{
		if(isDead())
			return;
		synchronized (this)
		{
			_open = false;
		}

		if(geoOpen)
			setGeoOpen(false);
		broadcastStatusUpdate();
	}

	public final void openMe()
	{
		if(isDead())
			return;
		synchronized (this)
		{
			_open = true;
		}

		if(!geoOpen)
			setGeoOpen(true);
		broadcastStatusUpdate();
	}

	@Override
	public String toString()
	{
		return "door " + _doorId;
	}

	public String getDoorName()
	{
		return _name;
	}

	public void setSiegeUnit(final Residence residence)
	{
		_siegeUnit = residence;
	}

	public Residence getSiegeUnit()
	{
		return _siegeUnit;
	}

	@Override
	public void doDie(final L2Character killer)
	{
		final Siege s = SiegeManager.getSiege(this, true);
		if(s != null)
		{
			for(final SiegeClan sc : s.getDefenderClans().values())
			{
				final L2Clan clan = sc.getClan();
				if(clan != null)
					for(final L2Player player : clan.getOnlineMembers(""))
						if(player != null)
							player.sendPacket(new SystemMessage(SystemMessage.THE_CASTLE_GATE_HAS_BEEN_BROKEN_DOWN));
			}

			for(final SiegeClan sc : s.getAttackerClans().values())
			{
				final L2Clan clan = sc.getClan();
				if(clan != null)
					for(final L2Player player : clan.getOnlineMembers(""))
						if(player != null)
							player.sendPacket(new SystemMessage(SystemMessage.THE_CASTLE_GATE_HAS_BEEN_BROKEN_DOWN));
			}
		}

		setGeoOpen(true);

		super.doDie(killer);
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();
		closeMe();
	}

	@Override
	protected void onDespawn()
	{
		if(_autoActionTask != null)
		{
			_autoActionTask.cancel(false);
			_autoActionTask = null;
		}

		super.onDespawn();
	}

	public boolean isHPVisible()
	{
		return _isHPVisible;
	}

	public void setHPVisible(final boolean val)
	{
		_isHPVisible = val;
	}

	@Override
	public int getMaxHp()
	{
		return super.getMaxHp() + upgradeHp;
	}

	public void setUpgradeHp(final int hp)
	{
		upgradeHp = hp;
	}

	public int getUpgradeHp()
	{
		return upgradeHp;
	}

	@Override
	public int getPDef(final L2Character target)
	{
		switch (SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE))
		{
			case SevenSigns.CABAL_DAWN:
				return (int) (super.getPDef(target) * 1.2);
			case SevenSigns.CABAL_DUSK:
				return (int) (super.getPDef(target) * 0.3);
			default:
				return super.getPDef(target);
		}
	}

	@Override
	public int getMDef(final L2Character target, final L2Skill skill)
	{
		switch (SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE))
		{
			case SevenSigns.CABAL_DAWN:
				return (int) (super.getMDef(target, skill) * 1.2);
			case SevenSigns.CABAL_DUSK:
				return (int) (super.getMDef(target, skill) * 0.3);
			default:
				return super.getMDef(target, skill);
		}
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	/**
	 * Двери на осадах уязвимы во время осады. Остальные двери не уязвимы вообще.
	 * 
	 * @return инвульная ли дверь.
	 */
	@Override
	public boolean isInvul()
	{
		if(_siegeUnit != null)
		{
			if(_siegeUnit.getSiege() != null && _siegeUnit.getSiege().isInProgress())
				return false;
			if(TerritorySiege.isInProgress() && (_siegeUnit.getType() == ResidenceType.Castle || _siegeUnit.getType() == ResidenceType.Fortress))
				return false;
		}
		return super.isInvul();
	}

	public int getDoorHeight()
	{
		return geoPos.getZmax() - geoPos.getZmin() & 0xFFF0;
	}

	/**
	 * Дверь/стена может быть атаоквана только осадным орудием
	 * 
	 * @return true если дверь/стену можно атаковать только осадным орудием
	 */
	public boolean isSiegeWeaponOnlyAttackable()
	{
		return _siegeWeaponOlnyAttackable;
	}

	/**
	 * Устанавливает двери/стене признак возможности атаковать только осадным оружием
	 * 
	 * @param val
	 *            true - дверь/стену можно атаковать только осадным орудием
	 */
	public void setSiegeWeaponOlnyAttackable(final boolean val)
	{
		_siegeWeaponOlnyAttackable = val;
	}

	/**
	 * Устанавливает значение закрытости\открытости в геодате<br>
	 * 
	 * @param val
	 *            новое значение
	 */
	public void setGeoOpen(final boolean val)
	{
		if(geoOpen == val)
			return;
		geoOpen = val;

		if(!getGeodata())
			return;
		if(val)
			GeoEngine.returnGeoAtControl(this);
		else
			GeoEngine.applyControl(this);
	}

	public void setGeodata(final boolean value)
	{
		_geodata = value;
	}

	public boolean getGeodata()
	{
		return _geodata;
	}

	private class CloseTask implements Runnable
	{
		@Override
		public void run()
		{
			onClose();
		}
	}

	/**
	 * Manages the auto open and closing of a door.
	 */
	private class AutoOpenClose implements Runnable
	{
		@Override
		public void run()
		{
			if(!isOpen())
				openMe();
			else
				closeMe();
		}
	}

	@Override
	public L2DoorInstance clone()
	{
		final L2DoorInstance door = new L2DoorInstance(IdFactory.getInstance().getNextId(), _template, _doorId, _name, _unlockable, _isHPVisible);
		door.setXYZInvisible(getLoc());
		door.setCurrentHpMp(door.getMaxHp(), door.getMaxMp(), true);
		door.setOpen(_open);
		door.setSiegeWeaponOlnyAttackable(_siegeWeaponOlnyAttackable);
		door.setGeodata(_geodata);
		door.geoPos = geoPos;
		door.level = level;
		door.key = key;
		door.setInvul(isInvul());
		return door;
	}

	@Override
	public boolean isGeoCloser()
	{
		return true;
	}
}
