package l2n.game.model.instances;

import l2n.Config;
import l2n.game.model.L2Multisell;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.ExShowDominionRegistry;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

/**
 * @author L2System Project
 * @date 23.02.2010
 * @time 10:31:38
 */
public class L2MercenaryCaptainInstance extends L2NpcInstance
{
	public L2MercenaryCaptainInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken();

		int terrId = getNpcId() - 36480;
		if(terrId > 9 || terrId < 1)
			return;

		int territoryBadgeId = 13756 + terrId;

		if(command.equalsIgnoreCase("Territory"))
		{
			player.sendPacket(new ExShowDominionRegistry(player, terrId));
		}
		else if(command.equalsIgnoreCase("Buy"))
		{
			String listId = getNpcId() + "0001";
			L2Multisell.getInstance().separateAndSend(Integer.parseInt(listId), player, 0);
		}
		else if(command.equalsIgnoreCase("Strider"))
		{
			NpcHtmlMessage html = new NpcHtmlMessage(player, this, getHtmlPath(getNpcId(), 3), 5);
			html.replace("%striderBadge%", String.valueOf(Config.MIN_TWBADGE_FOR_STRIDERS));
			html.replace("%gstriderBadge%", String.valueOf(Config.MIN_TWBADGE_FOR_BIG_STRIDER));
			player.sendPacket(html);
		}
		else if(actualCommand.equalsIgnoreCase("TW_Buy"))
		{
			if(st.countTokens() < 1 && !player.isGM())
				return;

			int count = Integer.parseInt(st.nextToken());
			int type = Integer.parseInt(st.nextToken());

			if(player.isGM())
				player.sendMessage("badgeId: " + territoryBadgeId + ", type: " + type);

			if(player.getInventory().getItemByItemId(territoryBadgeId) != null)
			{
				long playerItemCount = player.getInventory().getCountOf(territoryBadgeId);
				if(count <= playerItemCount)
				{
					int boughtId = 0;
					switch (type)
					{
						case 1: // Дракона Ветра
							boughtId = 4422;
							break;
						case 2: // Звездного Дракона
							boughtId = 4423;
							break;
						case 3: // Сумеречного Дракона
							boughtId = 4424;
							break;
						case 4: // Дракона Охранников
							boughtId = 14819;
							break;
						default:
							return;
					}
					player.destroyItemByItemId(territoryBadgeId, count, true);
					player.addItem(boughtId, 1, getNpcId(), "MercenaryCaptain");
					return;
				}
			}

			showChatWindow(player, 6);
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		if(val == 0)
			return "data/html/MercenaryCaptain/MercenaryCaptain.htm";
		return "data/html/MercenaryCaptain/MercenaryCaptain-" + val + ".htm";
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		if(player.getLevel() < 40 || player.getClassId().getLevel() < 3)
			val = 2;
		else if(TerritorySiege.isInProgress())
			val = 10;

		String filename = "";
		if(val == 0)
			filename = "data/html/MercenaryCaptain/MercenaryCaptain.htm";
		else
			filename = "data/html/MercenaryCaptain/MercenaryCaptain-" + val + ".htm";
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}
}
