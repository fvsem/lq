package l2n.game.model.instances;

import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2Party;
import l2n.game.model.L2PetData;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.base.Experience;
import l2n.game.model.items.PetInventory;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.Stats;
import l2n.game.tables.PetDataTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2PetInstance extends L2Summon
{
	protected static final Logger _log = Logger.getLogger(L2PetInstance.class.getName());

	private static final int DELUXE_FOOD_FOR_STRIDER = 5169;
	private static final int PET_DECAY_DELAY = 86400000; // 24 hours

	protected final Object _taskLock;

	private int _level;
	private int _curFed;
	private final int _controlItemObjId;
	private int lostExp;
	private boolean _respawned;

	private Future<FeedTask> _feedTask;

	protected PetInventory _inventory;
	protected L2PetData _data;

	public void sendItemList()
	{
		final L2Player owner = getPlayer();
		if(owner != null)
			owner.sendPacket(new PetItemList(this));
	}

	private class FeedTask implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final L2Player owner = getPlayer();
				if(owner == null)
				{
					stopFeed();
					unSummon();
					return;
				}

				if(getCurrentFed() > getFeedConsume())
					setCurrentFed(getCurrentFed() - getFeedConsume()); // eat
				else
					setCurrentFed(0);

				// пытаемся накормить пока голодный, если есть чем
				while (isHungry() && tryFeed())
				{}

				if(getCurrentFed() == 0)
				{
					owner.sendPacket(Msg.YOUR_PET_IS_VERY_HUNGRY);
					if(Rnd.chance(30))
					{
						stopFeed();
						owner.sendPacket(Msg.YOUR_PET_HAS_LEFT_DUE_TO_UNBEARABLE_HUNGER);

						if(Config.ALT_DELETE_PET_IF_HUNGRY)
						{
							deleteMe();
							_log.info("Hungry pet deleted for player: " + owner.getName() + " Control Item Id: " + getControlItemId());
						}
						else
							unSummon();

						return;
					}
				}
				else if(getCurrentFed() < 0.11 * getMaxFed())
				{
					owner.sendPacket(Msg.PET_CAN_RUN_AWAY_WHEN_HUNGER_BELOW_10_PERCENT);
					if(Rnd.chance(3))
					{
						stopFeed();
						owner.sendPacket(Msg.YOUR_PET_HAS_LEFT_DUE_TO_UNBEARABLE_HUNGER);

						if(Config.ALT_DELETE_PET_IF_HUNGRY)
						{
							deleteMe();
							_log.info("Hungry pet deleted for player: " + owner.getName() + " Control Item Id: " + getControlItemId());
						}
						else
							unSummon();
						return;
					}
				}

				sendStatusUpdate();
				// if its not already on battleFeed mode
				startFeed(isInCombat());
			}
			catch(final Throwable e)
			{
				_log.log(Level.SEVERE, "L2PetInstance: feedTask error.", e);
			}
		}

		private int getFeedConsume()
		{
			// if pet is attacking
			if(isAttackingNow())
				return getPetData().getFeedBattle();
			else
				return getPetData().getFeedNormal();
		}
	}

	public final L2PetData getPetData()
	{
		if(_data == null)
			_data = PetDataTable.getInstance().getInfo(getTemplate().npcId, _level);
		return _data;
	}

	@Override
	public final boolean isHungry()
	{
		return getCurrentFed() < 0.55 * getMaxFed();
	}

	public static L2PetInstance spawnPet(final L2NpcTemplate template, final L2Player owner, final L2ItemInstance control)
	{
		final L2PetInstance result = restore(control, template, owner);
		if(result != null)
			result.updateControlItem();
		return result;
	}

	/**
	 * СозданУе нового пета
	 */
	public L2PetInstance(final int objectId, final L2NpcTemplate template, final L2Player owner, final L2ItemInstance control)
	{
		super(objectId, template, owner);

		_taskLock = new Object();

		_controlItemObjId = control.getObjectId();

		// не понятно нахрена это тут
		final int itemEnchant = getControlItem().getEnchantLevel();

		// Sin Eater
		if(template.npcId == PetDataTable.SIN_EATER_ID)
		{
			_level = itemEnchant;
			if(_level <= 0)
				_level = owner.getLevel();
		}
		else if(itemEnchant > 0)
			_level = itemEnchant;
		else
			_level = template.level;

		final int minLevel = PetDataTable.getMinLevel(template.npcId);
		if(_level < minLevel)
			_level = minLevel;

		_exp = getExpForThisLevel();

		_data = PetDataTable.getInstance().getInfo(template.npcId, _level);

		_inventory = new PetInventory(this);

		startFeed(false);
	}

	/**
	 * Загрузка уже существующего пета
	 */
	public L2PetInstance(final int objectId, final L2NpcTemplate template, final L2Player owner, final L2ItemInstance control, final byte _currentLevel, final long exp)
	{
		super(objectId, template, owner);

		_taskLock = new Object();

		_controlItemObjId = control.getObjectId();
		_exp = exp;

		// Sin Eater
		if(template.npcId == PetDataTable.SIN_EATER_ID)
		{
			_level = getControlItem().getEnchantLevel();
			if(_level <= 0)
			{
				_level = owner.getLevel();
				_exp = getExpForThisLevel();
			}
		}
		else
			_level = _currentLevel == 0 ? template.level : _currentLevel;

		final int minLevel = PetDataTable.getMinLevel(template.npcId);
		while (_exp >= getExpForNextLevel() && _level < Experience.getMaxLevel())
			_level++;

		while (_exp < getExpForThisLevel() && _level > minLevel)
			_level--;

		if(_level < minLevel)
		{
			_level = minLevel;
			_exp = getExpForThisLevel();
		}

		if(PetDataTable.isVitaminPet(template.npcId))
		{
			_level = owner.getLevel();
			_exp = getExpForNextLevel();
		}

		_data = PetDataTable.getInstance().getInfo(template.npcId, _level);

		_inventory = new PetInventory(this);
		_inventory.restore();

		transferPetItems();
		startFeed(false);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IHardReference<L2PetInstance> getRef()
	{
		return (IHardReference<L2PetInstance>) super.getRef();
	}

	private void transferPetItems()
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		boolean transferred = false;

		for(L2ItemInstance item : owner.getInventory().getItemsList())
			if(!item.isEquipped() && (item.getCustomFlags() & L2ItemInstance.FLAG_PET_EQUIPPED) == L2ItemInstance.FLAG_PET_EQUIPPED)
			{
				if(_inventory.getTotalWeight() + item.getItem().getWeight() * item.getCount() > getMaxLoad())
				{
					owner.sendPacket(Msg.EXCEEDED_PET_INVENTORYS_WEIGHT_LIMIT);
					continue;
				}
				if(!item.canBeDropped(owner))
					continue;
				item = owner.getInventory().dropItem(item, item.getCount(), false);
				item.setCustomFlags(item.getCustomFlags() | L2ItemInstance.FLAG_PET_EQUIPPED, true);
				_inventory.addItem(item);
				tryEquipItem(item, false);
				transferred = true;
			}

		if(transferred)
		{
			sendItemList();
			broadcastCharInfo();
			owner.sendPacket(new ItemList(owner, false));
		}
	}

	public boolean tryEquipItem(final L2ItemInstance item, final boolean broadcast)
	{
		if(!item.isEquipable())
			return false;

		final int petId = ((L2NpcTemplate) _template).npcId;

		if(item.getItem().isPendant() //
				|| PetDataTable.isWolf(petId) && item.getItem().isForWolf() //
				|| PetDataTable.isHatchling(petId) && item.getItem().isForHatchling() //
				|| PetDataTable.isStrider(petId) && item.getItem().isForStrider() //
				|| PetDataTable.isGWolf(petId) && item.getItem().isForGreatWolf() //
				|| PetDataTable.isBabyPet(petId) && item.getItem().isForBabyPet() //
				|| PetDataTable.isImprovedBabyPet(petId) && item.getItem().isForBabyPet() //
		)
		{
			if(item.isEquipped())
			{
				_inventory.unEquipItemInSlot(item.getEquipSlot());
				if(item.getItemType() instanceof WeaponType)
					setWearedWeaponType(WeaponType.NONE);
			}
			else
			{
				_inventory.equipItem(item, true);
				if(item.getItemType() instanceof WeaponType)
					setWearedWeaponType(WeaponType.SWORD);
			}

			if(broadcast)
			{
				sendItemList();
				broadcastCharInfo();
			}
			return true;
		}
		return false;
	}

	@Override
	public L2NpcTemplate getTemplate()
	{
		return (L2NpcTemplate) _template;
	}

	@Override
	public final byte getLevel()
	{
		return (byte) _level;
	}

	@Override
	public int getSummonType()
	{
		return 2;
	}

	/**
	 * @return Returns ObjectId for pet control item
	 */
	@Override
	public int getControlItemId()
	{
		return _controlItemObjId;
	}

	public L2ItemInstance getControlItem()
	{
		final int item_id = getControlItemId();
		if(item_id == 0)
			return null;
		return getPlayer().getInventory().getItemByObjectId(item_id);
	}

	public void updateControlItem()
	{
		final L2ItemInstance controlItem = getControlItem();
		if(controlItem == null)
			return;

		controlItem.setEnchantLevel(_level);
		controlItem.setCustomType2(getName() == null || getName().isEmpty() || getName().equalsIgnoreCase(getTemplate().name) ? 0 : 1);

		final L2Player owner = getPlayer();
		if(owner != null)
			owner.sendPacket(new InventoryUpdate(controlItem, L2ItemInstance.MODIFIED));
	}

	protected void updateData()
	{
		_data = PetDataTable.getInstance().getInfo(getTemplate().npcId, _level);
	}

	@Override
	public int getMaxHp()
	{
		return (int) calcStat(Stats.MAX_HP, _data.getHP(), null, null);
	}

	@Override
	public int getMaxMp()
	{
		return (int) calcStat(Stats.MAX_MP, _data.getMP(), null, null);
	}

	@Override
	public int getMaxFed()
	{
		return _data.getFeedMax();
	}

	@Override
	public int getMAtk(final L2Character target, final L2Skill skill)
	{
		// В базе указаны параметры, уже домноженные на этот модификатор, для удобства. Поэтому вычисляем и убираем его.
		return (int) calcStat(Stats.MAGIC_ATTACK, _data.getMAtk(), target, skill);
	}

	@Override
	public int getMDef(final L2Character target, final L2Skill skill)
	{
		// В базе указаны параметры, уже домноженные на этот модификатор, для удобства. Поэтому вычисляем и убираем его.
		return (int) calcStat(Stats.MAGIC_DEFENCE, _data.getMDef(), target, skill);
	}

	@Override
	public int getPAtk(final L2Character target)
	{
		// В базе указаны параметры, уже домноженные на этот модификатор, для удобства. Поэтому вычисляем и убираем его.
		return (int) calcStat(Stats.POWER_ATTACK, _data.getPAtk(), target, null);
	}

	@Override
	public int getPDef(final L2Character target)
	{
		// В базе указаны параметры, уже домноженные на этот модификатор, для удобства. Поэтому вычисляем и убираем его.
		return (int) calcStat(Stats.POWER_DEFENCE, _data.getPDef(), target, null);
	}

	@Override
	public int getAccuracy()
	{
		return (int) calcStat(Stats.ACCURACY_COMBAT, _data.getAccuracy(), null, null);
	}

	@Override
	public int getEvasionRate(final L2Character target)
	{
		return (int) calcStat(Stats.EVASION_RATE, _data.getEvasion(), target, null);
	}

	@Override
	public long getExpForNextLevel()
	{
		return PetDataTable.getInstance().getInfo(getNpcId(), _level + 1).getExp();
	}

	@Override
	public long getExpForThisLevel()
	{
		return PetDataTable.getInstance().getInfo(getNpcId(), _level).getExp();
	}

	public long getMaxExp()
	{
		return PetDataTable.getInstance().getInfo(getNpcId(), Experience.getMaxLevel() + 1).getExp();
	}

	public int getFoodId()
	{
		return _data.getFoodId();
	}

	public int getAddFed()
	{
		return _data.getAddFed();
	}

	@Override
	public int getCriticalHit(final L2Character target, final L2Skill skill)
	{
		return (int) calcStat(Stats.CRITICAL_BASE, _data.getCritical(), target, skill);
	}

	@Override
	public int getRunSpeed()
	{
		return getSpeed(_data.getSpeed());
	}

	@Override
	public int getPAtkSpd()
	{
		return (int) calcStat(Stats.POWER_ATTACK_SPEED, calcStat(Stats.ATK_BASE, _data.getAtkSpeed(), null, null), null, null);
	}

	@Override
	public int getMAtkSpd()
	{
		return (int) calcStat(Stats.MAGIC_ATTACK_SPEED, _data.getCastSpeed(), null, null);
	}

	@Override
	public int getMaxLoad()
	{
		return (int) calcStat(Stats.MAX_LOAD, _data.getMaxLoad(), null, null);
	}

	@Override
	public int getSoulshotConsumeCount()
	{
		return PetDataTable.getSoulshots(getNpcId());
	}

	@Override
	public int getSpiritshotConsumeCount()
	{
		return PetDataTable.getSpiritshots(getNpcId());
	}

	@Override
	public int getCurrentFed()
	{
		return _curFed;
	}

	public int getMinLevel()
	{
		return _data.getMinLevel();
	}

	public void setCurrentFed(final int num)
	{
		_curFed = Math.min(getMaxFed(), Math.max(0, num));
	}

	@Override
	public void setSp(final int sp)
	{
		_sp = sp;
	}

	public void setLevel(final byte level)
	{
		_level = level;
	}

	public void setRespawned(final boolean respawned)
	{
		_respawned = respawned;
	}

	public boolean tryFeedItem(final L2ItemInstance item)
	{
		if(item == null)
			return false;

		final boolean deluxFood = PetDataTable.isStrider(getNpcId()) && item.getItemId() == DELUXE_FOOD_FOR_STRIDER;
		if(getFoodId() != item.getItemId() && !deluxFood)
			return false;

		final int newFed = Math.min(getMaxFed(), getCurrentFed() + Math.max(getMaxFed() * getAddFed() * (deluxFood ? 2 : 1) / 100, 1));
		if(getCurrentFed() != newFed)
		{
			removeItemFromInventory(item, 1, true);
			setCurrentFed(newFed);
			sendStatusUpdate();
		}
		return true;
	}

	public boolean tryFeed()
	{
		L2ItemInstance food = getInventory().getItemByItemId(getFoodId());
		if(food == null && PetDataTable.isStrider(getNpcId()))
			food = getInventory().getItemByItemId(DELUXE_FOOD_FOR_STRIDER);
		return tryFeedItem(food);
	}

	@Override
	public void addExpAndSp(final long addToExp, final long addToSp)
	{
		addExpAndSp(addToExp, addToSp, true, true);
	}

	@Override
	public void addExpAndSp(final long addToExp, final long addToSp, final boolean applyBonus, final boolean appyToPet)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		if(PetDataTable.isVitaminPet(getNpcId()))
			return;

		if(_exp > getMaxExp())
			_exp = getMaxExp();
		_exp += addToExp;
		_sp += addToSp;

		if(addToExp > 0 || addToSp > 0)
			owner.sendPacket(new SystemMessage(SystemMessage.THE_PET_ACQUIRED_EXPERIENCE_POINTS_OF_S1).addNumber(addToExp));
		final int old_level = _level;

		while (_exp >= getExpForNextLevel() && _level < Experience.getMaxLevel())
			increaseLevel();
		while (_exp < getExpForThisLevel() && _level > getMinLevel())
			decreaseLevel();

		// _log.info("current " + _exp + " exp.");

		if(old_level != _level)
		{
			updateControlItem();
			updateData();
			owner.sendPacket(new PetStatusUpdate(this));
		}

		boolean needStatusUpdate = true;

		if(old_level < _level)
		{
			owner.sendMessage(new CustomMessage("l2n.game.model.instances.L2PetInstance.PetLevelUp", owner).addNumber(_level));
			broadcastPacket(new SocialAction(getObjectId(), SocialAction.LEVEL_UP));
			setCurrentHpMp(getMaxHp(), getMaxMp());
			needStatusUpdate = false;
		}

		if(needStatusUpdate && (addToExp > 0 || addToSp > 0))
			broadcastStatusUpdate();
	}

	public void increaseLevel()
	{
		if(getPlayer() == null)
			return;

		// Increase the level
		_level++;

		if(Config.DEBUG)
			_log.finest("increasing level for pet " + getName());
	}

	public void decreaseLevel()
	{
		if(getPlayer() == null)
			return;
		_level--;
	}

	/**
	 * Return null.<BR>
	 */
	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		// temporary? unavailable
		return null;
	}

	@Override
	public L2Weapon getActiveWeaponItem()
	{
		// temporary? unavailable
		return null;
	}

	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		// temporary? unavailable
		return null;
	}

	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		// temporary? unavailable
		return null;
	}

	@Override
	public PetInventory getInventory()
	{
		return _inventory;
	}

	public void removeItemFromInventory(final L2ItemInstance item, final int count, final boolean toLog)
	{
		synchronized (_inventory)
		{
			_inventory.destroyItem(item.getObjectId(), count, toLog);
		}
	}

	@Override
	public void doPickupItem(final L2Object object)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		stopMove();
		getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);

		if(!object.isItem())
		{
			owner.sendActionFailed();
			return;
		}

		final L2ItemInstance target = (L2ItemInstance) object;

		if(target.isCursed())
		{
			owner.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_PICK_UP_S1).addItemName(target.getItemId()));
			return;
		}

		synchronized (target)
		{
			if(!target.isVisible())
			{
				owner.sendActionFailed();
				return;
			}

			if(getInventory().getTotalWeight() + target.getItem().getWeight() * target.getCount() > getMaxLoad())
				owner.sendPacket(Msg.EXCEEDED_PET_INVENTORYS_WEIGHT_LIMIT);

			if(target.isHerb())
			{
				final L2Skill[] skills = target.getItem().getAttachedSkills();
				if(skills != null && skills.length > 0)
					for(final L2Skill skill : skills)
						altUseSkill(skill, this);
				target.deleteMe();
				return;
			}

			// Нужно обязательно сначало удалить предмет с земли.
			if(!target.pickupMe(this))
			{
				owner.sendActionFailed();
				return;
			}
		}

		if(owner.getParty() == null || owner.getParty().getLootDistribution() == L2Party.ITEM_LOOTER)
		{
			owner.sendPacket(SystemMessage.obtainItemsBy(target, "Your pet"));
			target.setCustomFlags(target.getCustomFlags() | L2ItemInstance.FLAG_PET_EQUIPPED, true);

			synchronized (_inventory)
			{
				_inventory.addItem(target);
			}

			sendItemList();
			sendPetInfo();
		}
		else
			owner.getParty().distributeItem(owner, target);

		broadcastPickUpMsg(target);
		setFollowStatus(isFollow(), true);
	}

	@Override
	public void reduceCurrentHp(final double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp, final boolean canReflect, final boolean isCounteAttack)
	{
		if(attacker.isPlayable() && isInZoneBattle() != attacker.isInZoneBattle())
		{
			attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
		// this is usually only called in combat

		if(!isDead() && attacker != null)
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.THE_PET_RECEIVED_DAMAGE_OF_S2_CAUSED_BY_S1);
			if(attacker.isNpc())
				sm.addNpcName(((L2NpcInstance) attacker).getTemplate().npcId);
			else
				sm.addString(attacker.getName());
			sm.addNumber((int) damage);

			if(getPlayer() != null)
				getPlayer().sendPacket(sm);
		}
	}

	@Override
	public void displayHitMessage(final L2Character target, final int damage, final boolean crit, final boolean miss)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		if(crit)
			owner.sendPacket(Msg.PETS_CRITICAL_HIT);
		if(miss)
			owner.sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_WENT_ASTRAY).addName(this));
		else
			owner.sendPacket(new SystemMessage(SystemMessage.THE_PET_GAVE_DAMAGE_OF_S1).addNumber(damage));
	}

	@Override
	public void doDie(final L2Character killer)
	{
		dieLock.lock();
		try
		{
			if(_killedAlreadyPet)
				return;
			setIsKilledAlreadyPet(true);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2PetInstance.doDie(L2Character): ReentrantLock error", e);
		}
		finally
		{
			dieLock.unlock();
		}

		super.doDie(killer);
		final L2Player owner = getPlayer();
		if(owner == null)
		{
			onDecay();
			return;
		}

		owner.sendPacket(new SystemMessage(SystemMessage.PET_HAS_DIED_IF_HE_IS_NOT_RESURRECTED_IN_24_HOURS_THE_PETS_CORPSE_WILL_DISAPPEAR_AND_HIS_ITEMS_WILL_DISAPPEAR_AS_WELL));
		startDecay(PET_DECAY_DELAY);

		if(PetDataTable.isVitaminPet(getNpcId()))
			return;

		stopFeed();
		deathPenalty();
	}

	@Override
	public synchronized void giveAllToOwner()
	{
		final L2Player owner = getPlayer();
		synchronized (_inventory)
		{
			for(final L2ItemInstance i : _inventory.getItems())
			{
				final L2ItemInstance item = _inventory.dropItem(i, i.getCount(), false);
				if(owner != null)
					owner.getInventory().addItem(item);
				else
					item.dropMe(this, getLoc().changeZ(25));
			}
			_inventory.getItemsList().clear();
		}
	}

	/**
	 * Remove the Pet from DB and its associated item from the player inventory
	 *
	 * @param owner
	 *            The owner from whose inventory we should delete the item
	 */
	public void destroyControlItem()
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;
		if(getControlItemId() == 0)
			return;
		// pet control item no longer exists, delete the pet from the db
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM pets WHERE item_obj_id=?");
			statement.setInt(1, getControlItemId());
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.warning("could not delete pet:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		try
		{
			owner.getInventory().destroyItem(getControlItemId(), 1, true);
			owner.sendChanges();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while destroying control item: ", e);
		}
	}

	/**
	 * @return Returns the mountable.
	 */
	@Override
	public boolean isMountable()
	{
		return _data.isMountable();
	}

	public boolean isRespawned()
	{
		return _respawned;
	}

	private static L2PetInstance restore(final L2ItemInstance control, final L2NpcTemplate template, final L2Player owner)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT objId, name, level, curHp, curMp, exp, sp, fed FROM pets WHERE item_obj_id=?");
			statement.setInt(1, control.getObjectId());
			rset = statement.executeQuery();
			L2PetInstance pet = null;
			if(!rset.next())
			{
				if(PetDataTable.isSupportPet(template.getNpcId()))
					pet = new L2SupportPetInstance(IdFactory.getInstance().getNextId(), template, owner, control);
				else
					pet = new L2PetInstance(IdFactory.getInstance().getNextId(), template, owner, control);
				return pet;
			}

			if(PetDataTable.isSupportPet(template.getNpcId()))
				pet = new L2SupportPetInstance(rset.getInt("objId"), template, owner, control, rset.getByte("level"), rset.getLong("exp"));
			else
				pet = new L2PetInstance(rset.getInt("objId"), template, owner, control, rset.getByte("level"), rset.getLong("exp"));
			pet.setRespawned(true);
			pet.setName(rset.getString("name"));
			pet.setCurrentHpMp(rset.getDouble("curHp"), rset.getInt("curMp"), true);
			pet.setCurrentCp(pet.getMaxCp());
			pet.setSp(rset.getInt("sp"));
			pet.setCurrentFed(rset.getInt("fed"));

			return pet;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2PetInstance.restore(L2ItemInstance, L2NpcTemplate, L2Player): ", e);
			return null;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void store()
	{
		if(getControlItemId() == 0 || _exp == 0)
			return;

		String req;
		if(!isRespawned())
			req = "INSERT INTO pets (name,level,curHp,curMp,exp,sp,fed,objId,item_obj_id) VALUES (?,?,?,?,?,?,?,?,?)";
		else
			req = "UPDATE pets SET name=?,level=?,curHp=?,curMp=?,exp=?,sp=?,fed=?,objId=? WHERE item_obj_id = ?";
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(req);
			statement.setString(1, getName().equalsIgnoreCase(getTemplate().name) || getName().isEmpty() ? null : getName());
			statement.setInt(2, _level);
			statement.setDouble(3, getCurrentHp());
			statement.setDouble(4, getCurrentMp());
			statement.setLong(5, _exp);
			statement.setLong(6, _sp);
			statement.setInt(7, _curFed);
			statement.setInt(8, _objectId);
			statement.setInt(9, _controlItemObjId);
			statement.executeUpdate();
			_respawned = true;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not store pet data: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void stopFeed()
	{
		if(_feedTask != null)
		{
			_feedTask.cancel(false);
			_feedTask = null;
			if(Config.DEBUG)
				_log.fine("Feed task stop");
		}
	}

	public void startFeed(final boolean battleFeed)
	{
		final boolean first = _feedTask == null;
		// stop feeding task if its active
		stopFeed();
		if(!isDead() && getPlayer() != null)
		{
			final int needFeed = battleFeed ? _data.getFeedBattle() : _data.getFeedNormal();
			if(needFeed <= 0)
				return;

			int feedTime = Math.max(first ? 15000 : 5000, 60000 / needFeed);
			if(PetDataTable.isVitaminPet(getNpcId()))
				feedTime = 10000;
			// pet feed time must be different than 0. Changing time to bypass divide by 0
			if(feedTime <= 0)
				feedTime = 1;

			_feedTask = L2GameThreadPools.getInstance().scheduleGeneral(new FeedTask(), feedTime);
		}
	}

	@Override
	public void onDecay()
	{
		deleteMe();
	}

	@Override
	public void deleteMe()
	{
		giveAllToOwner();
		destroyControlItem(); // this should also delete the pet from the db
		stopFeed();
		super.deleteMe();
	}

	@Override
	public void unSummon()
	{
		stopFeed();
		giveAllToOwner();
		super.deleteMe();
		store();
	}

	private void deathPenalty()
	{
		if(isInZoneBattle())
			return;

		final int lvl = getLevel();
		final double percentLost = -0.07 * lvl + 6.5;
		// Calculate the Experience loss
		lostExp = (int) Math.round((getExpForNextLevel() - getExpForThisLevel()) * percentLost / 100);
		addExpAndSp(-lostExp, 0);
	}

	public void restoreExp()
	{
		restoreExp(100);
	}

	public void restoreExp(final double percent)
	{
		if(lostExp != 0)
		{
			addExpAndSp((long) (lostExp * percent / 100.), 0);
			lostExp = 0;
		}
	}

	public void doRevive(final double percent)
	{
		restoreExp(percent);
		doRevive();
	}

	@Override
	public void doRevive()
	{
		stopDecay();
		super.doRevive();
		startFeed(false);
		setRunning();
		setFollowStatus(true, true);
	}

	public final int getPetRegularSkillLevel(final int skillId)
	{
		return _data.getPetRegularSkillLevel(skillId);
	}

	@Override
	public boolean consumeItem(final int itemConsumeId, final int itemCount)
	{
		final L2ItemInstance item = getInventory().findItemByItemId(itemConsumeId);
		return !(item == null || item.getCount() < itemCount) && getInventory().destroyItem(item, itemCount, false) != null;
	}

	@Override
	public float getExpPenalty()
	{
		if(_data.getExpPenalty() > 0)
			return _data.getExpPenalty();
		return PetDataTable.getExpPenalty(getTemplate().npcId);
	}

	@Override
	public boolean isPet()
	{
		return true;
	}
}
