package l2n.game.model.instances;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.model.entity.siege.Siege;
import l2n.game.network.serverpackets.PledgeShowInfoUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 */
public class L2FortressBallistaInstance extends L2SiegeGuardInstance
{
	private static final int BALLISTA_BOMB_SKILL_ID = 2342;
	private int _bombsUseCounter = 0;

	public L2FortressBallistaInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void doDie(L2Character killer)
	{
		final Siege siege = SiegeManager.getSiege(this, true);
		if(siege != null && siege.getSiegeUnit().getType() == ResidenceType.Fortress && killer instanceof L2Playable)
		{
			final L2Player player = (L2Player) killer;
			if(player.getClan() != null && player.getClan().getLevel() >= 5)
			{
				player.getClan().incReputation(Config.BALLISTA_POINTS, false, "BALLISTA_POINTS");
				player.getClan().broadcastToOnlineMembers(new PledgeShowInfoUpdate(player.getClan()));
			}
			player.sendPacket(new SystemMessage(SystemMessage.BALLISTA_DESTROYED_CLAN_REPU_INCREASED));
		}
		super.doDie(killer);
	}

	@Override
	public void reduceCurrentHp(double i, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		if(skill == null)
			return;

		if(skill.getId() != BALLISTA_BOMB_SKILL_ID)
		{
			if(attacker != null && attacker.isPlayer())
				attacker.sendPacket(Msg.ATTACK_WAS_BLOCKED);
			return;
		}
		else
			super.reduceCurrentHp(i, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	public void incBombsUseCounter()
	{
		_bombsUseCounter++;
	}

	public int getBombsUseCounter()
	{
		return _bombsUseCounter;
	}

	@Override
	public int getAggroRange()
	{
		return 1200;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		L2Player player = attacker.getPlayer();
		if(player == null)
			return false;
		L2Clan clan = player.getClan();
		if(clan != null && SiegeManager.getSiege(this, true) == clan.getSiege() && clan.isDefender())
			return false;
		return true;
	}

	@Override
	public boolean isInvul()
	{
		return false;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}
}
