package l2n.game.model.instances;

import l2n.game.instancemanager.CastleManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Residence;
import l2n.game.network.serverpackets.AgitDecoInfo;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;

public class L2ClanHallManagerInstance extends L2ResidenceManager
{
	public L2ClanHallManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		super.onAction(player, shift);
		int condition = validateCondition(player);
		if(condition != COND_OWNER)
			return;

		ClanHall ch = getClanHall();
		if(ch.getOwner() == null)
			return;

		long lease = ch.getLease();
		Castle castle = CastleManager.getInstance().getCastleByIndex(ch.getZone().getTaxById());
		long tax = lease * castle.getTaxPercent() / 100;
		lease += tax;
		if(ch.getOwner().getAdenaCount() >= lease)
			return;
		if(ch.getPaidUntil() <= System.currentTimeMillis() + 86400000 && ch.getPaidUntil() >= System.currentTimeMillis() + 43200000)
			player.sendPacket(new SystemMessage(SystemMessage.PAYMENT_FOR_YOUR_CLAN_HALL_HAS_NOT_BEEN_MADE_PLEASE_MAKE_PAYMENT_TO_YOUR_CLAN_WAREHOUSE_BY_S1_TOMORROW).addNumber(ch.getLease()));
		else if(ch.isInDebt())
			player.sendPacket(new SystemMessage(SystemMessage.THE_CLAN_HALL_FEE_IS_ONE_WEEK_OVERDUE_THEREFORE_THE_CLAN_HALL_OWNERSHIP_HAS_BEEN_REVOKED));
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename = "data/html/residence/chamberlain-no.htm";
		int condition = validateCondition(player);
		if(condition > COND_ALL_FALSE)
			if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
				filename = "data/html/residence/chamberlain-busy.htm";
			else if(condition == COND_OWNER)
				filename = "data/html/residence/chamberlain.htm";
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	protected int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;
		if(player.getClan() != null)
		{
			if(getResidence().getSiege() != null && getResidence().getSiege().isInProgress())
				return COND_BUSY_BECAUSE_OF_SIEGE;
			if(getResidence().getOwnerId() == player.getClanId())
				return COND_OWNER;
		}
		return COND_ALL_FALSE;
	}

	@Override
	protected Residence getResidence()
	{
		return getClanHall();
	}

	public void sendDecoInfo(L2Player player)
	{
		ClanHall clanHall = getClanHall();
		if(clanHall != null)
			player.sendPacket(new AgitDecoInfo(getClanHall()));
	}

	@Override
	public void broadcastDecoInfo()
	{
		for(L2Player player : L2World.getAroundPlayers(this))
			if(player != null)
				sendDecoInfo(player);
	}

	@Override
	protected int getPrivFunctions()
	{
		return L2Clan.CP_CH_SET_FUNCTIONS;
	}

	@Override
	protected int getPrivDismiss()
	{
		return L2Clan.CP_CH_DISMISS;
	}

	@Override
	protected int getPrivDoors()
	{
		return L2Clan.CP_CH_OPEN_DOOR;
	}
}
