package l2n.game.model.instances;

import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2RoundTerritoryWithSkill;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.util.concurrent.ScheduledFuture;

public final class L2TrapInstance extends L2NpcInstance
{
	private final IHardReference<? extends L2Character> _ownerRef;

	private final L2Skill _skill;
	private boolean _detected = false;
	private final L2RoundTerritoryWithSkill _territory;
	private ScheduledFuture<?> _destroyTask;

	private class DestroyTask implements Runnable
	{
		final L2TrapInstance _trap;

		@Override
		public void run()
		{
			if(_trap != null)
				_trap.destroy();
		}

		private DestroyTask(final L2TrapInstance trap)
		{
			_trap = trap;
		}
	}

	// TODO: Узнать прпадают ли ловушки при Релогине хозяина, пропадают ли вообще сами по себе?
	public L2TrapInstance(final int objectId, final L2NpcTemplate template, final L2Character owner, final L2Skill trapSkill, final Location loc)
	{
		super(objectId, template);
		_ownerRef = owner.getRef();
		_skill = trapSkill;

		setReflection(owner.getReflection().getId());
		setLevel(owner.getLevel());
		setTitle(owner.getName());
		spawnMe(loc);

		_territory = new L2RoundTerritoryWithSkill(objectId, loc.x, loc.y, 150, loc.z - 200, loc.z + 200, this, trapSkill);
		L2World.addTerritory(_territory);

		for(final L2Character cha : L2World.getAroundCharacters(this, 300, 200))
			cha.updateTerritories();

		final int _lifeTeme = 120000; // Ловушки живут минуту, инфа с гохи
		_destroyTask = L2GameThreadPools.getInstance().scheduleGeneral(new DestroyTask(this), _lifeTeme);
	}

	public void detonate(final L2Character target)
	{
		final L2Character owner = getOwner();
		if(owner == null || _skill == null)
		{
			destroy();
			return;
		}

		if(target == owner || target == this)
			return;

		if(!target.isMonster() && !target.isPlayable() || target.isRaid())
			return;
		if(_skill.checkTarget(owner, target, target, false, false) == null)
		{
			final GArray<L2Character> targets = new GArray<L2Character>();

			if(_skill.getTargetType() != SkillTargetType.TARGET_AREA)
				targets.add(target);
			else
				for(final L2Character t : getAroundCharacters(_skill.getSkillRadius(), 128))
					if(_skill.checkTarget(owner, t, t, false, false) == null)
						targets.add(t);

			_skill.useSkill(this, targets.toArray(new L2Character[targets.size()]));
			target.sendMessage(new CustomMessage("common.Trap", target));
			destroy();
		}
	}

	@Override
	public void broadcastCharInfo()
	{
		if(!isDetected())
			return;
		super.broadcastCharInfo();
	}

	public void destroy()
	{
		L2World.removeTerritory(_territory);
		final L2Character owner = getOwner();
		if(owner != null)
			owner.removeTrap(this);
		deleteMe();
		if(_destroyTask != null)
			_destroyTask.cancel(false);
		_destroyTask = null;
	}

	public L2Character getOwner()
	{
		return _ownerRef.get();
	}

	@Override
	public L2Player getPlayer()
	{
		final L2Character owner = getOwner();
		if(owner == null || !owner.isPlayer())
			return null;
		return (L2Player) owner;
	}

	public void setDetected(final boolean detected)
	{
		_detected = detected;
	}

	public boolean isDetected()
	{
		return _detected;
	}

	public int getSkillLevel()
	{
		return _skill.getLevel();
	}

	public L2Skill getTrapSkill()
	{
		return _skill;
	}

	@Override
	public int getPAtk(final L2Character target)
	{
		if(getOwner() != null)
			return getOwner().getPAtk(target);
		return 0;
	}

	@Override
	public int getMAtk(final L2Character target, final L2Skill skill)
	{
		if(getOwner() != null)
			return getOwner().getMAtk(target, skill);
		return 0;
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return false;
	}

	@Override
	public boolean isAttackable(final L2Character attacker)
	{
		return false;
	}

	@Override
	public void doDie(final L2Character killer)
	{}

	@Override
	public boolean isInvul()
	{
		return true;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}

	@Override
	public void showChatWindow(final L2Player player, final int val)
	{}

	@Override
	public void showChatWindow(final L2Player player, final String filename)
	{}

	@Override
	public void onBypassFeedback(final L2Player player, final String command)
	{}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		player.sendActionFailed();
	}
}
