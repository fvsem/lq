package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanGate;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

public class L2CastleMagicianInstance extends L2NpcInstance
{
	/**
	 * @param template
	 */
	public L2CastleMagicianInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		player.sendActionFailed();
		player.setLastNpc(this);
		String filename = "data/html/castle/magician/magician-no.htm";

		int condition = validateCondition(player);
		if(player.isGM())
			condition = COND_OWNER;
		if(condition > COND_ALL_FALSE)
			if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
				filename = "data/html/castle/magician/magician-busy.htm"; // Busy because of siege
			else if(condition == COND_OWNER)
				if(val == 0)
					filename = "data/html/castle/magician/magician.htm";
				else
					filename = "data/html/castle/magician/magician-" + val + ".htm";

		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile(filename);
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcId%", String.valueOf(getNpcId()));
		html.replace("%npcname%", String.valueOf(getName() + " " + getTitle()));
		player.sendPacket(html);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendActionFailed();
		if(!isInRange(player, INTERACTION_DISTANCE))
			return;

		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE || condition == COND_BUSY_BECAUSE_OF_SIEGE)
			return;

		if((player.getClanPrivileges() & L2Clan.CP_CS_USE_FUNCTIONS) != L2Clan.CP_CS_USE_FUNCTIONS)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ResidenceManager.NotAuthorizedToDoThis", player));
			return;
		}

		if(command.equalsIgnoreCase("ClanGate"))
			teleportThroughClanGate(player);
		else if(condition == COND_OWNER)
			super.onBypassFeedback(player, command);
	}

	protected int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;
		if(getCastle() != null && getCastle().getId() > 0)
			if(player.getClan() != null)
				if(getCastle().getSiege().isInProgress() || TerritorySiege.isInProgress())
					return COND_BUSY_BECAUSE_OF_SIEGE; // Busy because of siege
				else if(getCastle().getOwnerId() == player.getClanId()) // Clan owns castle
					return COND_OWNER;
		return COND_ALL_FALSE;
	}

	/**
	 * Проверяет, ссоздан ли клангейт для данного клана;
	 * если есть, телепортирует игрока через клангейт
	 * 
	 * @param player
	 *            - игрок, которого телепортить
	 */
	protected void teleportThroughClanGate(L2Player player)
	{
		L2Clan clan = player.getClan();
		if(clan == null)
			return;

		if(getCastle().getOwnerId() != clan.getClanId())
			return;

		L2ClanGate clanGate = clan.getClanGate();
		if(clanGate == null)
		{
			showChatWindow(player, "data/html/castle/magician/magician-noClanGate.htm");
			return;
		}
		clanGate.teleportMemberThroughGate(player);
	}
}
