package l2n.game.model.instances;

import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.castle.CastleSiege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

public final class L2CastleTeleporterInstance extends L2NpcInstance
{
	public L2CastleTeleporterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendActionFailed();

		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE)
			return;

		super.onBypassFeedback(player, command);

		if(command.startsWith("CastleMassGK"))
		{
			command = command.substring(13); // срезаем ненужное
			String args[] = command.split("_");

			Siege activeSiege = SiegeManager.getSiege(this, true);
			long delay;
			if(TerritorySiege.isInProgress())
				delay = TerritorySiege.getDefenderRespawnTotal(getCastle().getId());
			else if(activeSiege != null)
			{
				delay = activeSiege.getDefenderRespawnTotal();
				if(activeSiege.getSiegeUnit().getType() == ResidenceType.Castle && ((CastleSiege) activeSiege).isAllTowersDead())
					delay = 480000;
			}
			else
				delay = Long.parseLong(args[0]); // аргумент 0 = время телепорта

			int x = Integer.parseInt(args[1]); // аргумент 1 = точка телепорта х
			int y = Integer.parseInt(args[2]); // аргумент 2 = точка телепорта y
			int z = Integer.parseInt(args[3]); // аргумент 3 = точка телепорта z
			int offset = Integer.parseInt(args[4]) + 1; // аргумент 4 = дистанция случайного расброса игроков
			int radius = Integer.parseInt(args[5]); // аргумент 5 = радиус для сбора персонажей. Возможно правильнее будет переделать на зоны
			String text = args[6]; // аргумент 6 = то что орет гк при телепорте

			if(_massGkTask == null) // если не существует таск, то создать новый. Если существует - игнорить.
			{
				_massGkTask = new MassGKTask(this, x, y, z, offset, radius, text);
				L2GameThreadPools.getInstance().scheduleGeneral(_massGkTask, delay);
			}
			showChatWindow(player, "data/html/teleporter/massGK-Teleported.htm"); // выдать html-ку с ответом
		}
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;

		return "data/html/teleporter/" + pom + ".htm";
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename;
		int cond = validateCondition(player);

		if(_massGkTask != null)
			filename = "data/html/teleporter/massGK-Teleported.htm";
		else if(cond == COND_OWNER || cond == COND_CASTLE_DEFENDER)
			filename = "data/html/teleporter/" + getNpcId() + ".htm"; // Teleport message window
		else
			filename = "data/html/teleporter/castleteleporter-no.htm"; // "Go out!"

		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	private int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;

		if(player.getClan() != null && getCastle() != null)
			if(getCastle().getOwnerId() == player.getClanId()) // Clan owns castle
				return COND_OWNER; // Owner
			else if(getCastle().getSiege().isInProgress() && getCastle().getSiege().checkIsAttacker(player.getClan()))
				return COND_CASTLE_ATTACKER; // Attacker
			else if(getCastle().getSiege().isInProgress() && getCastle().getSiege().checkIsDefender(player.getClan(), false))
				return COND_CASTLE_DEFENDER; // Defender

		return COND_ALL_FALSE;
	}

	protected MassGKTask _massGkTask;

	public class MassGKTask implements Runnable
	{
		private L2NpcInstance _npc;
		private int _x, _y, _z, _offset, _radius;
		private String _text;

		public MassGKTask(L2NpcInstance npc, int x, int y, int z, int offset, int radius, String text)
		{
			_npc = npc;
			_x = x;
			_y = y;
			_z = z;
			_offset = offset;
			_radius = radius;
			_text = text;
		}

		@Override
		public void run()
		{
			Functions.npcShout(_npc, _text);

			for(L2Player p : L2World.getAroundPlayers(_npc, _radius, 50))
			{
				int cond = validateCondition(p);
				if(cond == L2CastleTeleporterInstance.COND_OWNER || cond == L2CastleTeleporterInstance.COND_CASTLE_DEFENDER)
					p.teleToLocation(GeoEngine.findPointToStay(_x, _y, _z, 10, _offset));
			}
			_massGkTask = null; // освободить для дальнейшего использования масс гк
		}
	}
}
