package l2n.game.model.instances;

import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.KamalokaAbyssLabyrinth;
import l2n.game.templates.L2NpcTemplate;

public class L2SpecialMonsterInstance extends L2MonsterInstance
{
	public L2SpecialMonsterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void doDie(L2Character killer)
	{
		// Обрабатываем тут смерть монстров/РБ из Лабиринта Бездны
		if(getReflection() != null && getReflection().isKamalokaAbyssLabyrinth())
		{
			KamalokaAbyssLabyrinth reflection = (KamalokaAbyssLabyrinth) getReflection();
			if(!reflection.isCollapseStarted())
				switch (getNpcId())
				{
				// Монстры 1-й комнаты
					case 22485:
					case 22488:
					case 22491:
					case 22494:
					case 22497:
					case 22500:
					case 22503:
					{
						reflection.setFirstRoomMonsterIsDead(true);
						for(L2NpcInstance npc : L2World.getAroundNpc(this, 1200, 200))
							if(npc.getNpcId() == getNpcId() + 1 && npc.getSpawn() != null)
								npc.getSpawn().stopRespawn();

						break;
					}
					// Монстры 2-й комнаты
					case 22487:
					case 22490:
					case 22493:
					case 22496:
					case 22499:
					case 22502:
					case 22505:
						reflection.incSecondRoomMonsterDeadCount();
						break;
					// РБ 3-й комнаты
					case 25616:
					case 25617:
					case 25618:
					case 25619:
					case 25620:
					case 25621:
					case 25622:
						reflection.setThirdRoomMonsterIsDead(true);
						break;
				}
		}

		super.doDie(killer);
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
