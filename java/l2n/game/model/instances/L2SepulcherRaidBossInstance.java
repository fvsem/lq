package l2n.game.model.instances;

import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.boss.FourSepulchersManager;
import l2n.game.instancemanager.boss.FourSepulchersSpawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.QuestState;
import l2n.game.templates.L2NpcTemplate;

import java.util.concurrent.Future;

/**
 * @author L2System Project
 * @date 18.05.2010
 * @time 12:11:32
 */
public class L2SepulcherRaidBossInstance extends L2RaidBossInstance
{
	public L2SepulcherRaidBossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public int mysteriousBoxId = 0;
	protected Future<?> _onDeadEventTask = null;

	@Override
	public void doDie(L2Character killer)
	{
		super.doDie(killer);

		L2Player player = killer.getPlayer();
		if(player != null)
			giveCup(player);
		if(_onDeadEventTask != null)
			_onDeadEventTask.cancel(true);
		_onDeadEventTask = L2GameThreadPools.getInstance().scheduleGeneral(new OnDeadEvent(this), 8500);
	}

	@Override
	public void deleteMe()
	{
		if(_onDeadEventTask != null)
		{
			_onDeadEventTask.cancel(true);
			_onDeadEventTask = null;
		}

		super.deleteMe();
	}

	private void giveCup(L2Player player)
	{
		String questId = FourSepulchersManager.QUEST_ID;
		int cupId = 0;
		int oldBrooch = 7262;

		switch (getNpcId())
		{
			case 25339:
				cupId = 7256;
				break;
			case 25342:
				cupId = 7257;
				break;
			case 25346:
				cupId = 7258;
				break;
			case 25349:
				cupId = 7259;
				break;
		}

		if(player.getParty() != null)
			for(L2Player mem : player.getParty().getPartyMembers())
			{
				QuestState qs = mem.getQuestState(questId);
				if(qs != null && (qs.isStarted() || qs.isCompleted()) && mem.getInventory().getItemByItemId(oldBrooch) == null)
					mem.addItem(cupId, 1, 0, "SepulcherRaidBoss");
			}
		else
		{
			QuestState qs = player.getQuestState(questId);
			if(qs != null && (qs.isStarted() || qs.isCompleted()) && player.getInventory().getItemByItemId(oldBrooch) == null)
				player.addItem(cupId, 1, 0, "SepulcherRaidBoss");
		}
	}

	private class OnDeadEvent implements Runnable
	{
		L2SepulcherRaidBossInstance _activeChar;

		public OnDeadEvent(L2SepulcherRaidBossInstance activeChar)
		{
			_activeChar = activeChar;
		}

		@Override
		public void run()
		{
			FourSepulchersSpawn.spawnEmperorsGraveNpc(_activeChar.mysteriousBoxId);
		}
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
