package l2n.game.model.instances;

import l2n.Config;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.HashMap;

public class L2OlympiadBufferInstance extends L2NpcInstance
{
	public L2OlympiadBufferInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!Config.ENABLE_OLYMPIAD)
			return;

		if(command.startsWith("OlyBuff"))
		{
			NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
			String[] params = command.split(" ");
			int skillId = Integer.parseInt(params[1]);
			int skillLvl;

			// oly buff whitelist prevents bypass exploiters -.-
			HashMap<Integer, Integer> buffList = new HashMap<Integer, Integer>();
			buffList.put(4357, 2); // Haste Lv2
			buffList.put(4342, 2); // Wind Walk Lv2
			buffList.put(4356, 3); // Empower Lv3
			buffList.put(4355, 3); // Acumen Lv3
			buffList.put(4351, 6); // Concentration Lv6
			buffList.put(4345, 3); // Might Lv3
			buffList.put(4358, 3); // Guidance Lv3
			buffList.put(4359, 3); // Focus Lv3
			buffList.put(4360, 3); // Death Whisper Lv3
			buffList.put(4352, 2); // Berserk Spirit Lv2

			// lets check on our oly buff whitelist
			if(!buffList.containsKey(skillId))
				return;

			// get skilllevel from the hashmap
			skillLvl = buffList.get(skillId);

			L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLvl);

			if(skill == null || skill.getSkillType() == L2Skill.SkillType.NOTDONE)
				System.out.println("L2OlympiadBuffer: skillId " + skillId + " error!");

			setTarget(player);

			if(player.olyBuff > 0)
			{
				// ThreadPoolManager.getInstance().scheduleGeneral(new doBuff(this, skill, player), 100);
				broadcastPacket(new MagicSkillUse(this, player, skill.getId(), skill.getLevel(), 0, 0));
				skill.getEffects(player, player, false, false);
				player.olyBuff--;
			}
			if(player.olyBuff == 5)
			{
				html.setFile(Olympiad.OLYMPIAD_HTML_PATH + "olympiad_buffs.htm");
				player.sendPacket(html);
			}
			if(player.olyBuff <= 4 && player.olyBuff >= 1)
			{
				html.setFile(Olympiad.OLYMPIAD_HTML_PATH + "olympiad_5buffs.htm");
				player.sendPacket(html);
			}
			else
			{
				html.setFile(Olympiad.OLYMPIAD_HTML_PATH + "olympiad_nobuffs.htm");
				player.sendPacket(html);
				deleteMe();
			}
		}
	}
}
