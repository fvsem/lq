package l2n.game.model.instances;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 07.10.2010
 * @time 14:23:53
 */
public final class L2FenceInstance extends L2Object
{
	private int _type;
	private int _width;
	private int _height;

	public L2FenceInstance(int objectId, int type, int width, int height)
	{
		super(objectId);
		_type = type;
		_width = width;
		_height = height;
	}

	public int getType()
	{
		return _type;
	}

	public int getWidth()
	{
		return _width;
	}

	public int getHeight()
	{
		return _height;
	}

	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		return false;
	}
}
