package l2n.game.model.instances;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.QuestManager;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager.RaidBossStatus;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Log;
import l2n.util.MinionList;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class manages all RaidBoss.
 * In a group mob, there are one master called RaidBoss and several slaves called Minions.
 */
public class L2RaidBossInstance extends L2MonsterInstance
{
	private class maintainKilledMinion implements Runnable
	{
		private int _minion;

		public maintainKilledMinion(int minion)
		{
			_minion = minion;
		}

		@Override
		public void run()
		{
			try
			{
				if(!isDead())
				{
					MinionList list = getMinionList();
					if(list != null)
						list.spawnSingleMinionSync(_minion);
				}
			}
			catch(Throwable e)
			{
				_log.log(Level.SEVERE, "", e);
			}
		}
	}

	protected final static Logger _log = Logger.getLogger(L2RaidBossInstance.class.getName());

	private ScheduledFuture<?> minionMaintainTask;
	private static final int RAIDBOSS_MAINTENANCE_INTERVAL = 5000;
	private static final int MINION_UNSPAWN_INTERVAL = 5000; // time to unspawn minions when boss is dead, msec

	private RaidBossStatus _raidStatus;

	/**
	 * Constructor of L2RaidBossInstance (use L2Character and L2NpcInstance constructor).<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Call the L2Character constructor to set the _template of the L2RaidBossInstance (copy skills from template to object and link _calculators to NPC_STD_CALCULATOR)</li> <li>Set the name of the L2RaidBossInstance</li> <li>Create a
	 * RandomAnimation Task that will be launched after the calculated delay if the server allow it</li><BR>
	 * <BR>
	 * 
	 * @param objectId
	 *            Identifier of the object to initialized
	 * @param L2NpcTemplate
	 *            Template to apply to the NPC
	 */
	public L2RaidBossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected int getMaintenanceInterval()
	{
		return RAIDBOSS_MAINTENANCE_INTERVAL;
	}

	protected int getMinionUnspawnInterval()
	{
		return MINION_UNSPAWN_INTERVAL;
	}

	protected int getKilledInterval(L2MinionInstance minion) // TODO вывести время в конфиги
	{
		return 2 * 60 * 1000; // 4-6 minutes to respawn
	}

	@Override
	public void notifyMinionDied(L2MinionInstance minion)
	{
		minionMaintainTask = L2GameThreadPools.getInstance().scheduleAi(new maintainKilledMinion(minion.getNpcId()), getKilledInterval(minion), false);
		super.notifyMinionDied(minion);
	}

	@Override
	public void reduceCurrentHp(double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		if(attacker == null || attacker.getPlayer() == null || (attacker == this || damage > getMaxHp() / 10) && !attacker.getPlayer().isGM())
			return;
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public void doDie(L2Character killer)
	{
		if(minionMaintainTask != null)
		{
			minionMaintainTask.cancel(false);
			minionMaintainTask = null;
		}

		int points = RaidBossSpawnManager.getInstance().getPoinstForRaid(getNpcId());
		if(points > 0)
			calcRaidPointsReward(points);

		String killer_name = killer.isPlayer() ? ((L2Player) killer).getName() : "unknow";
		String kill = "\t" + getName() + "(" + getNpcId() + ") - Player: " + killer_name + "(" + killer.getObjectId() + ") coord[" + getX() + ", " + getY() + ", " + getZ() + "]";
		Log.add(kill, "bosses_kills");

		if(this instanceof L2ReflectionBossInstance)
		{
			super.doDie(killer);
			return;
		}

		if(killer.isPlayable())
		{
			L2Player player = killer.getPlayer();
			if(player.getParty() != null)
				player.getParty().broadcastToPartyMembers(Msg.CONGRATULATIONS_YOUR_RAID_WAS_SUCCESSFUL);
			else
				player.sendPacket(Msg.CONGRATULATIONS_YOUR_RAID_WAS_SUCCESSFUL);

			Quest q = QuestManager.getQuest(508);
			if(q != null)
			{
				String qn = q.getName();
				if(player.getClan() != null && player.getClan().getLeader().isOnline() && player.getClan().getLeader().getPlayer().getQuestState(qn) != null)
				{
					QuestState st = player.getClan().getLeader().getPlayer().getQuestState(qn);
					st.getQuest().onKill(this, st);
				}
			}
		}

		unspawnMinions();

		int boxId = 0;
		switch (getNpcId())
		{
			case 25035: // Shilens Messenger Cabrio
				boxId = 31027;
				break;
			case 25054: // Demon Kernon
				boxId = 31028;
				break;
			case 25126: // Golkonda, the Longhorn General
				boxId = 31029;
				break;
			case 25220: // Death Lord Hallate
				boxId = 31030;
				break;
		}

		if(boxId != 0)
		{
			L2NpcTemplate boxTemplate = NpcTable.getTemplate(boxId);
			if(boxTemplate != null)
			{
				final L2NpcInstance box = new L2NpcInstance(IdFactory.getInstance().getNextId(), boxTemplate);
				box.spawnMe(getLoc());

				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						box.deleteMe();
					}
				}, 60000);
			}
		}

		super.doDie(killer);
		setRaidStatus(RaidBossStatus.DEAD);
	}

	public void unspawnMinions()
	{
		if(hasMinions())
			L2GameThreadPools.getInstance().scheduleAi(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						removeMinions();
					}
					catch(Throwable e)
					{
						_log.log(Level.SEVERE, "", e);
						e.printStackTrace();
					}
				}
			}, getMinionUnspawnInterval(), false);
	}

	@SuppressWarnings("unchecked")
	private void calcRaidPointsReward(int totalPoints)
	{
		// Object groupkey (L2Party/L2CommandChannel/L2Player) | [GArray<L2Player> group, Long GroupDdamage]
		HashMap<Object, Object[]> participants = new HashMap<Object, Object[]>();
		double totalHp = getMaxHp();

		// Разбиваем игроков по группам. По возможности используем наибольшую из доступных групп: Command Channel → Party → StandAlone (сам плюс пет :)
		for(AggroInfo ai : getAggroList())
		{
			L2Player player = ai.attacker.getPlayer();
			Object key = player.getParty() != null ? player.getParty().getCommandChannel() != null ? player.getParty().getCommandChannel() : player.getParty() : player.getPlayer();
			Object[] info = participants.get(key);
			if(info == null)
			{
				info = new Object[] { new HashSet<L2Player>(), new Long(0) };
				participants.put(key, info);
			}

			// если это пати или командный канал то берем оттуда весь список участвующих, даже тех кто не в аггролисте
			// дубликаты не страшны - это хашсет
			if(key instanceof L2CommandChannel)
			{
				for(L2Player p : ((L2CommandChannel) key).getMembers())
					if(p.isInRangeZ(this, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						((HashSet<L2Player>) info[0]).add(p);
			}
			else if(key instanceof L2Party)
			{
				for(L2Player p : ((L2Party) key).getPartyMembers())
					if(p.isInRangeZ(this, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						((HashSet<L2Player>) info[0]).add(p);
			}
			else
				((HashSet<L2Player>) info[0]).add(player);

			info[1] = ((Long) info[1]).longValue() + ai.damage;
		}

		for(Object[] groupInfo : participants.values())
		{
			HashSet<L2Player> players = (HashSet<L2Player>) groupInfo[0];
			// это та часть, которую игрок заслужил дамагом группы, но на нее может быть наложен штраф от уровня игрока
			int perPlayer = (int) Math.round(totalPoints * ((Long) groupInfo[1]).longValue() / (totalHp * players.size()));
			for(L2Player player : players)
			{
				int playerReward = perPlayer;
				// применяем штраф если нужен
				playerReward = (int) Math.round(playerReward * Experience.penaltyModifier(calculateLevelDiffForDrop(player.getLevel(), false), 9));
				if(playerReward == 0)
					continue;
				player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1_RAID_POINTS).addNumber(playerReward));
				RaidBossSpawnManager.getInstance().addPoints(player.getObjectId(), getNpcId(), playerReward);
			}
		}

		RaidBossSpawnManager.getInstance().updatePointsDb();
		RaidBossSpawnManager.getInstance().calculateRanking();
	}

	@Override
	public void onDecay()
	{
		super.onDecay();
		RaidBossSpawnManager.getInstance().onBossDespawned(this);
	}

	@Override
	public void onSpawn()
	{
		addSkill(SkillTable.getInstance().getInfo(4045, 1)); // Добавляем скил Resist Full Magic Attack всем рб при спавне.
		RaidBossSpawnManager.getInstance().onBossSpawned(this);
		super.onSpawn();
	}

	public void setRaidStatus(RaidBossStatus raidBossStatus)
	{
		_raidStatus = raidBossStatus;
	}

	public RaidBossStatus getRaidStatus()
	{
		return _raidStatus;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}

	@Override
	public boolean hasRandomWalk()
	{
		return false;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public boolean canChampion()
	{
		return Config.CHAMPION_CAN_BE_RAIDBOSS;
	}
}
