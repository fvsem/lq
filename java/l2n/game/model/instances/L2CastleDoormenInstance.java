package l2n.game.model.instances;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

public class L2CastleDoormenInstance extends L2NpcInstance
{
	public L2CastleDoormenInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE)
			return;
		if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
			return;
		if(condition == COND_OWNER)
			if((player.getClanPrivileges() & L2Clan.CP_CS_OPEN_DOOR) == L2Clan.CP_CS_OPEN_DOOR)
			{
				if(command.startsWith("open_doors"))
				{
					StringTokenizer st = new StringTokenizer(command.substring(10), ", ");
					st.nextToken(); // Bypass first value since its castleid/hallid
					Castle castle = getCastle();
					while (st.hasMoreTokens())
					{
						int doorId = Integer.parseInt(st.nextToken());
						if(!castle.getDoor(doorId).isOpen())
							castle.openDoor(player, doorId);
					}
				}
				else if(command.startsWith("close_doors"))
				{
					StringTokenizer st = new StringTokenizer(command.substring(11), ", ");
					st.nextToken(); // Bypass first value since its castleid/hallid
					if(condition == COND_OWNER)
					{
						Castle castle = getCastle();
						while (st.hasMoreTokens())
						{
							int doorId = Integer.parseInt(st.nextToken());
							if(castle.getDoor(doorId).isOpen())
								castle.closeDoor(player, doorId);
						}
					}
				}
			}
			else
				player.sendMessage(new CustomMessage("common.Privilleges", player));
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename = "data/html/doormen/" + getTemplate().npcId + "-no.htm";
		int condition = validateCondition(player);
		if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
			filename = "data/html/doormen/" + getTemplate().npcId + "-busy.htm"; // Busy because of siege
		else if(condition == COND_OWNER) // Clan owns castle
			filename = "data/html/doormen/" + getTemplate().npcId + ".htm"; // Owner message window
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	private int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;

		if(player.getClan() != null)
			if(getCastle() != null && getCastle().getId() >= 0)
				if(getCastle().getOwnerId() == player.getClanId())
				{
					if(getCastle().getSiege().isInProgress() || TerritorySiege.isInProgress())
					{
						if(Config.SIEGE_OPERATE_DOORS)
						{
							if(Config.SIEGE_OPERATE_DOORS_LORD_ONLY && !player.isCastleLord(getCastle().getId()))
								return COND_BUSY_BECAUSE_OF_SIEGE;
							return COND_OWNER;
						}
						return COND_BUSY_BECAUSE_OF_SIEGE;
					}
					return COND_OWNER;
				}
		return COND_ALL_FALSE;
	}
}
