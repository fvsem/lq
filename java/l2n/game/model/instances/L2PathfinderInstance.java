package l2n.game.model.instances;

import l2n.extensions.scripts.Functions;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.boss.ZakenManager;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.KamalokaNightmare;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2NpcTemplate;

public class L2PathfinderInstance extends L2NpcInstance
{
	private int _rank;
	private boolean _rewarded = false;
	public static final int boxes[][] = {
			{ 0, 0, 0, 0, 0, 0 },
			{ 12824, 10836, 12825, 10837, 10838, 10839 },
			{ 10840, 10841, 12826, 12827, 10842, 10843 },
			{ 10844, 10845, 10846, 12828, 12829, 10847 },
			{ 10848, 10849, 10850, 10851, 12830, 12831 },
			{ 10852, 10853, 10854, 10855, 10856, 12832 } };

	public L2PathfinderInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(final L2Player player, final String command)
	{
		if(player.getKarma() > 0)
		{
			player.sendActionFailed();
			return;
		}
		if(command.startsWith("Chat"))
		{
			try
			{
				final int val = Integer.parseInt(command.substring(5));
				showChatWindow(player, val);
			}
			catch(final NumberFormatException nfe)
			{
				final String filename = command.substring(5).trim();
				if(filename.length() == 0)
					showChatWindow(player, "data/html/npcdefault.htm");
				else
					showChatWindow(player, filename);
			}
		}
		else if(command.startsWith("DayZaken"))
		{
			int res = ZakenManager.enterToZaken(player, true);
			if(res > 0)
				showChatWindow(player, res);
		}
		else if(command.startsWith("NightZaken"))
		{
			int res = ZakenManager.enterToZaken(player, false);
			if(res > 0)
				showChatWindow(player, res);
		}
		else if(command.startsWith("ExitSoloKama"))
		{
			final Reflection r = getReflection();
			if(r.getReturnLoc() != null)
				player.teleToLocation(r.getReturnLoc(), 0);
			else
				player.setReflection(0);
			player.unsetVar("backCoords");

			r.startCollapseTimer(1000);
		}
		else if(command.startsWith("ListPossible"))
		{
			final StringBuffer sb = new StringBuffer("<font color=\"LEVEL\">Pathfinder Worker:</font><br>");
			if(ReflectionTable.getInstance().findSoloKamaloka(player.getObjectId()) != null)
				sb.append("Hey, what are you doing? Your work isn't done yet!<br><a action=\"bypass -h scripts_Kamaloka:enterSoloKamaloka ").append(-1).append("\">Return to the Hall of Nightmares").append("</a><br>");
			else if(player.getLevel() < 20 || player.getLevel() > 80)
				sb.append("There are no offerings for your level.");
			else
			{
				InstanceManager ilm = InstanceManager.getInstance();
				if(ilm.getTimeToNextEnterInstance("Kamaloka Hall of the Nightmares", player) > 0)
					sb.append("You can not enter hall of nightmares now, you must get some rest. Or... Maybe you have extra entrace pass?<br>");
				for(int i = 25; i <= 75; i += 5)
					if(player.getLevel() >= i - 5 && player.getLevel() <= i + 5)
						sb.append("<a action=\"bypass -h scripts_Kamaloka:enterSoloKamaloka ").append(i).append("\">Enter Hall of the Nightmares, level ").append(i).append("</a><br>");

			}
			player.sendPacket(new NpcHtmlMessage(player, this).setHtml(sb.toString()));
		}
		else if(command.startsWith("SoloKamaReward"))
		{
			final Reflection r = getReflection();
			if(!_rewarded)
			{
				int base = 0;
				int doppler = 0;
				int v0id = 0;
				int level = 0;
				for(final L2NpcTemplate template : ((KamalokaNightmare) r).getCounter().keySet())
				{
					level = template.level;
					final StringBuffer sb = new StringBuffer("").append(template.name).append(" ").append(template.title).append(" killed: ").append(((KamalokaNightmare) r).getCounter().get(template));
					player.sendMessage(sb.toString());
					if(template.title.equals("Void"))
						v0id = ((KamalokaNightmare) r).getCounter().get(template);
					else if(template.title.equals("Doppler"))
						doppler = ((KamalokaNightmare) r).getCounter().get(template);
					else
						base = ((KamalokaNightmare) r).getCounter().get(template);
				}

				final int count = level / 25 * (doppler + v0id + base / 5) / 50;
				if(count > 0)
					Functions.addItem(player, 13002, count);
				Functions.addItem(player, boxes[getGradeByLevel(player.getLevel())][calcRank()], 1L);
				_rewarded = true;
			}
			showChatWindow(player, 2);
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(final L2Player player, final int val)
	{
		final NpcHtmlMessage res = new NpcHtmlMessage(player, this, getHtmlPath(getTemplate().npcId, val), val);
		if(getReflection().getId() > 0 && val == 1)
			switch (calcRank())
			{
				case 0:
					res.replace("%rank%", "NG");
					break;
				case 1:
					res.replace("%rank%", "D");
					break;
				case 2:
					res.replace("%rank%", "C");
					break;
				case 3:
					res.replace("%rank%", "B");
					break;
				case 4:
					res.replace("%rank%", "A");
					break;
				case 5:
					res.replace("%rank%", "S");
					break;
				default:
					res.replace("%rank%", "NG");
					break;
			}

		player.sendPacket(res);
	}

	private int calcRank()
	{
		final Reflection r = getReflection();
		int base = 0;
		int doppler = 0;
		int v0id = 0;
		for(final L2NpcTemplate template : ((KamalokaNightmare) r).getCounter().keySet())
			if(template.title.equals("Void"))
				v0id = ((KamalokaNightmare) r).getCounter().get(template);
			else if(template.title.equals("Doppler"))
				doppler = ((KamalokaNightmare) r).getCounter().get(template);
			else
				base = ((KamalokaNightmare) r).getCounter().get(template);

		final float total = base / 5 + doppler + v0id * 1.2F;
		if(total >= 200F)
			_rank = 5;
		else if(total >= 166F)
			_rank = 4;
		else if(total >= 133F)
			_rank = 3;
		else if(total >= 100F)
			_rank = 2;
		else if(total >= 66F)
			_rank = 1;
		else
			_rank = 0;
		return _rank;
	}

	private int getGradeByLevel(final int level)
	{
		if(level < 20)
			return 0;
		if(level < 40)
			return 1;
		if(level < 52)
			return 2;
		if(level < 61)
			return 3;
		if(level < 76)
			return 4;
		else
			return 5;
	}
}
