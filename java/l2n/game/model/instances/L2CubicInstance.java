package l2n.game.model.instances;

import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2CubicInstance
{
	protected static final Logger _log = Logger.getLogger(L2CubicInstance.class.getName());

	public static enum CubicType
	{
		STORM_CUBIC(1, 10, 80),
		VAMPIRIC_CUBIC(2, 15, 67),
		LIFE_CUBIC(3, 13, 55),
		VIPER_CUBIC(4, 20, 55),
		PHANTOM_CUBIC(5, 8, 55),
		BINDING_CUBIC(6, 30, 55),
		AQUA_CUBIC(7, 30, 67),
		SPARK_CUBIC(8, 30, 55),
		ATTRACTIVE_CUBIC(9, 8, 85),
		SMART_CUBIC_EVATEMPLAR(10, 10, 30),
		SMART_CUBIC_SHILLIENTEMPLAR(11, 10, 30),
		SMART_CUBIC_ARCANALORD(12, 13, 30),
		SMART_CUBIC_ELEMENTALMASTER(13, 13, 30),
		SMART_CUBIC_SPECTRALMASTER(14, 13, 30);

		public final int id;
		public final int delay;
		public final int chance;

		private CubicType(final int id, final int delay, final int chance)
		{
			this.id = id;
			this.delay = delay;
			this.chance = chance;
		}

		public static CubicType getType(final int id)
		{
			for(final CubicType type : values())
				if(type.id == id)
					return type;
			return null;
		}
	}

	/** Оффсет для корректного сохранения кубиков в базе */
	public static final int CUBIC_STORE_OFFSET = 1000000;

	// Max range of cubic skills
	// TODO: Check/fix the max range
	private static final int MAX_MAGIC_RANGE = 900;

	// Cubic skills
	private static final int SKILL_CUBIC_HEAL = 4051;
	private static final int SKILL_CUBIC_CURE = 5579;

	private IHardReference<L2Player> _ownerRef = HardReferences.emptyRef();
	private IHardReference<? extends L2Character> _targetRef = HardReferences.emptyRef();

	private final CubicType _type;
	private int _level = 1;

	private GArray<L2Skill> _offensiveSkills = new GArray<L2Skill>();
	private GArray<L2Skill> _healSkills = new GArray<L2Skill>();

	private Future<Disappear> _disappearTask = null;
	private Future<ActionScheduler> _actionTask = null;

	private final long _startTime;
	private final long _lifeTime;
	private final int _activationChance;

	private final boolean _givenByOther;

	public L2CubicInstance(final L2Player owner, final int id, final int level, final int lifetime, final int chance, final boolean givenByOther)
	{
		_type = CubicType.getType(id);

		_level = level;
		_givenByOther = givenByOther;
		_ownerRef = owner.getRef();

		// Ноль передаётся при ресторе эффектов
		_activationChance = chance <= 0 ? _type.chance : chance;

		// System.out.println("L2CubicInstance - " + _type + ", lvl:" + _level + ", chance:" + _activationChance);
		switch (_type)
		{
			case STORM_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4049, level));
				break;
			case VAMPIRIC_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4050, level));
				break;
			case LIFE_CUBIC:
				_healSkills.add(SkillTable.getInstance().getInfoSafe(SKILL_CUBIC_HEAL, level));
				break;
			case VIPER_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4052, level));
				break;
			case PHANTOM_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4053, level));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4054, level));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4055, level));
				break;
			case BINDING_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4164, level));
				break;
			case AQUA_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4165, level));
				break;
			case SPARK_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4166, level));
				break;
			case ATTRACTIVE_CUBIC:
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(5115, level));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(5116, level));
				break;
			case SMART_CUBIC_EVATEMPLAR:
				_healSkills.add(SkillTable.getInstance().getInfo(SKILL_CUBIC_CURE, 1));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4053, 8));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4165, 9));
				break;
			case SMART_CUBIC_SHILLIENTEMPLAR:
				_healSkills.add(SkillTable.getInstance().getInfo(SKILL_CUBIC_CURE, 1));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4049, 8));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(5115, 4));
				break;
			case SMART_CUBIC_ARCANALORD:
				_healSkills.add(SkillTable.getInstance().getInfo(SKILL_CUBIC_CURE, 1));
				_healSkills.add(SkillTable.getInstance().getInfoSafe(SKILL_CUBIC_HEAL, 7));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4165, 9));
				break;
			case SMART_CUBIC_ELEMENTALMASTER:
				_healSkills.add(SkillTable.getInstance().getInfo(SKILL_CUBIC_CURE, 1));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4049, 8));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4166, 9));
				break;
			case SMART_CUBIC_SPECTRALMASTER:
				_healSkills.add(SkillTable.getInstance().getInfo(SKILL_CUBIC_CURE, 1));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4049, 8));
				_offensiveSkills.add(SkillTable.getInstance().getInfoSafe(4052, 6));
				break;
		}

		for(final L2Skill skill : _offensiveSkills)
			if(skill.getCastRange() != 1500)
				skill.setCastRange(1500);

		for(final L2Skill skill : _healSkills)
			if(skill.getCastRange() != 1500)
				skill.setCastRange(1500);

		if(_actionTask == null)
			_actionTask = L2GameThreadPools.getInstance().scheduleAi(new ActionScheduler(), 1000, true);

		if(_disappearTask == null)
			_disappearTask = L2GameThreadPools.getInstance().scheduleGeneral(new Disappear(), lifetime); // disappear in 20 mins

		_startTime = System.currentTimeMillis();
		_lifeTime = lifetime;
	}

	public void doAction(final L2Character target)
	{
		final L2Player owner = getOwner();
		if(owner == null)
		{
			deleteMe();
			return;
		}

		// если таргет не менялся. то ничего не делаем
		if(getTargeObjId() == target.getObjectId())
			return;

		if(owner == target)
			return;

		final L2Character old_target = getTarget();
		if(old_target != null && (old_target == target || owner == target || owner.getPet() == target))
			return;

		stopAttackAction();
		_targetRef = target.getRef();
	}

	public CubicType getType()
	{
		return _type;
	}

	public int getId()
	{
		return _type.id;
	}

	public int getLevel()
	{
		return _level;
	}

	public boolean isGivenByOther()
	{
		return _givenByOther;
	}

	public long lifeLeft()
	{
		return _lifeTime - (System.currentTimeMillis() - _startTime);
	}

	public void setLevel(final int level)
	{
		_level = level;
	}

	public void stopAttackAction()
	{
		_targetRef = HardReferences.emptyRef();
	}

	public void stopAllActions()
	{
		_targetRef = HardReferences.emptyRef();
		final Future<?> actionTask = _actionTask;
		if(actionTask != null)
		{
			actionTask.cancel(false);
			_actionTask = null;
		}
	}

	public void cancelDisappear()
	{
		if(_disappearTask != null)
		{
			_disappearTask.cancel(true);
			_disappearTask = null;
		}
	}

	public void deleteMe()
	{
		stopAllActions();
		cancelDisappear();

		final L2Player owner = getOwner();
		if(owner != null)
		{
			owner.delCubic(this);
			owner.broadcastUserInfo(true);
		}

		_ownerRef = HardReferences.emptyRef();

		_offensiveSkills = null;
		_healSkills = null;
	}

	private class ActionScheduler implements Runnable
	{
		@Override
		public void run()
		{
			final L2Player owner = getOwner();
			if(owner == null || owner.isDead())
			{
				deleteMe();
				return;
			}

			try
			{
				boolean use = false;
				for(final L2Skill skill : _healSkills)
					// Начиная с финала, снимает 1 дебаф 1 раз в минуту.
					if(skill.getId() == SKILL_CUBIC_CURE) // снятие дебафов
					{
						for(final L2Effect e : owner.getEffectList().getAllEffects())
							if(e.getSkill().isOffensive() && e.getSkill().isCancelable() && !e._template._applyOnCaster)
							{
								owner.sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getDisplayId(), e.getSkill().getDisplayLevel()));
								e.exit();
								use = true;
							}

						if(use)
							// Smart Cubic debuff cancel is needed, no other skill is used in this activation period
							owner.broadcastPacket(new MagicSkillUse(owner, SKILL_CUBIC_CURE, 1, 0, 8000));
					}
					else if(skill.getSkillType() == SkillType.HEAL) // хил скилы
					{
						L2Character target = owner;
						if(owner.getParty() != null)
						{
							for(final L2Playable member : owner.getParty().getPartyMembersWithPets())
								if(member != null && !member.isDead() && member.getCurrentHpRatio() < target.getCurrentHpRatio() && owner.isInRangeZ(member, skill.getCastRange()))
									target = member;
						}
						else if(owner.getPet() != null && !owner.getPet().isDead() && owner.getPet().getCurrentHpRatio() < owner.getCurrentHpRatio())
							target = owner.getPet();
						final double hpp = target.getCurrentHpPercents();
						if(hpp < 95)
							if(Rnd.chance(hpp > 30 ? 66 : hpp > 60 ? 44 : 100))
							{
								owner.altUseSkill(skill, target);
								return;
							}
					}

				final L2Character target = getTarget();
				if(target != null)
				{

					if(target == null || target.isDead() || !owner.isInRangeZ(target, MAX_MAGIC_RANGE))
					{
						stopAttackAction();
						return;
					}
					final L2Skill skill = _offensiveSkills.get(Rnd.get(_offensiveSkills.size()));
					if(Rnd.chance(_activationChance) && skill.checkCondition(owner, target, false, false, true))
					{
						owner.altUseSkill(skill, target);
						return;
					}
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "", e);
			}
			finally
			{
				_actionTask = L2GameThreadPools.getInstance().scheduleAi(this, _type.delay * 1000, true);
			}
		}
	}

	private final L2Player getOwner()
	{
		return _ownerRef.get();
	}

	private final L2Character getTarget()
	{
		return _targetRef.get();
	}

	private final int getTargeObjId()
	{
		L2Character target = _targetRef.get();
		return target == null ? 0 : target.getObjectId();
	}

	private class Disappear implements Runnable
	{
		@Override
		public void run()
		{
			deleteMe();
		}
	}
}
