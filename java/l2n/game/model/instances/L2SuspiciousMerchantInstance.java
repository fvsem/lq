package l2n.game.model.instances;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ClanTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.logging.Level;

public class L2SuspiciousMerchantInstance extends L2NpcInstance
{
	public L2SuspiciousMerchantInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(command.startsWith("showSiegeInfo"))
			showSiegeInfoWindow(player);
		else if(command.startsWith("Chat"))
			try
			{
				int val = Integer.parseInt(command.substring(5));
				showChatWindow(player, val);
			}
			catch(NumberFormatException nfe)
			{
				String filename = command.substring(5).trim();
				if(filename.length() == 0)
					showChatWindow(player, "data/html/npcdefault.htm");
				else
					showChatWindow(player, filename);
			}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename;

		L2Clan clan = player.getClan();
		Fortress fortress = getFortress();

		if(val == 0)
			filename = "data/html/fortress/merchant.htm";
		else
			filename = "data/html/fortress/merchant-" + val + ".htm";

		if(fortress.getSiege().isInProgress() || TerritorySiege.isInProgress())
			filename = "data/html/fortress/merchant-busy.htm";

		if(clan == null)
			filename = "data/html/fortress/merchant-noclan.htm";

		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile(filename);

		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcId%", String.valueOf(getNpcId()));
		html.replace("%castleid%", Integer.toString(getCastle().getId()));

		if(fortress.getOwnerId() > 0)
		{
			L2Clan owner = ClanTable.getInstance().getClan(fortress.getOwnerId());
			if(owner != null && owner.getName() != null)
				html.replace("%clanname%", owner.getName());
			else
			{
				_log.log(Level.WARNING, "L2SuspiciousMerchant: not found owner for fortress[" + fortress.getName() + " - " + fortress.getId() + "]");
				html.replace("%clanname%", "unknow");
			}
		}
		else
			html.replace("%clanname%", "NPC");

		player.sendPacket(html);
	}

	public void showSiegeInfoWindow(L2Player player)
	{
		if(!getFortress().getSiege().isInProgress() && !TerritorySiege.isInProgress())
			getFortress().getSiege().listRegisterClan(player);
		else
		{
			NpcHtmlMessage html = new NpcHtmlMessage(player, this);
			html.setFile("data/html/fortress/merchant-busy.htm");
			html.replace("%fortname%", getFortress().getName());
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
		}
	}
}
