package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

public class L2FortressDoormenInstance extends L2NpcInstance
{
	/**
	 * @param objectId
	 * @param template
	 */
	public L2FortressDoormenInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendPacket(Msg.ActionFail);
		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE)
			return;
		if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
			return;
		if(condition == COND_OWNER)
			if((player.getClanPrivileges() & L2Clan.CP_CS_OPEN_DOOR) == L2Clan.CP_CS_OPEN_DOOR)
			{
				StringTokenizer st;
				if(command.startsWith("open_doors"))
				{
					st = new StringTokenizer(command.substring(10), ", ");
					st.nextToken();
					while (st.hasMoreTokens())
					{
						int id = Integer.parseInt(st.nextToken());

						if((getFortress().getSiege().isInProgress() || TerritorySiege.isInProgress()) && (FortressSiegeManager.getGuardDoors(getFortress().getId()).containsKey(id) || FortressSiegeManager.getCommandCenterDoors(getFortress().getId()).contains(id)))
						{
							player.sendPacket(new NpcHtmlMessage(player, this, "data/html/doormen/fortress/busy.htm", 0));
							break;
						}
						getFortress().openDoor(player, id);
					}
				}
				else if(command.startsWith("close_doors"))
				{
					st = new StringTokenizer(command.substring(11), ", ");
					st.nextToken();
					while (st.hasMoreTokens())
					{
						int id = Integer.parseInt(st.nextToken());

						if((getFortress().getSiege().isInProgress() || TerritorySiege.isInProgress()) && (FortressSiegeManager.getGuardDoors(getFortress().getId()).containsKey(id) || FortressSiegeManager.getCommandCenterDoors(getFortress().getId()).contains(id)))
						{
							player.sendPacket(new NpcHtmlMessage(player, this, "data/html/doormen/fortress/busy.htm", 0));
							break;
						}
						getFortress().closeDoor(player, id);
					}
				}
			}
			else
				player.sendMessage(new CustomMessage("common.Privilleges", player));
		super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename = "data/html/doormen/fortress/" + getTemplate().npcId + "-no.htm";
		int condition = validateCondition(player);
		if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
			filename = "data/html/doormen/fortress/" + getTemplate().npcId + "-busy.htm";
		else if(condition == COND_OWNER)
			filename = "data/html/doormen/fortress/" + getTemplate().npcId + ".htm";
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	private int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER;

		if(player.getClan() != null && getFortress() != null && getFortress().getId() >= 0 && getFortress().getOwnerId() == player.getClanId())
		{
			if(getFortress().getSiege().isInProgress())
				return COND_BUSY_BECAUSE_OF_SIEGE;
			return COND_OWNER;
		}
		return COND_ALL_FALSE;
	}
}
