package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.util.StringTokenizer;

import static l2n.game.model.L2Zone.ZoneType.Siege;

public final class L2ObservationInstance extends L2NpcInstance
{
	/**
	 * @param template
	 */
	public L2ObservationInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(final L2Player player, final String command)
	{
		// first do the common stuff
		// and handle the commands that all NPC classes know
		super.onBypassFeedback(player, command);
		if(player.isInEvent(L2EventType.NONE))
		{
			player.sendMessage(new CustomMessage("common.notAvailable", player));
			return;
		}

		if(command.startsWith("observeSiege"))
		{
			final String val = command.substring(13);
			final StringTokenizer st = new StringTokenizer(val);
			st.nextToken(); // Bypass cost

			if(ZoneManager.getInstance().checkIfInZone(Siege, Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken())))
				doObserve(player, val);
			else
				player.sendPacket(Msg.OBSERVATION_IS_ONLY_POSSIBLE_DURING_A_SIEGE);
		}
		else if(command.startsWith("observe"))
			doObserve(player, command.substring(8));
	}

	@Override
	public String getHtmlPath(final int npcId, final int val)
	{
		String pom = "";
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;

		return "data/html/observation/" + pom + ".htm";
	}

	private void doObserve(final L2Player player, final String val)
	{
		final StringTokenizer st = new StringTokenizer(val);
		final int cost = Integer.parseInt(st.nextToken());
		final int x = Integer.parseInt(st.nextToken());
		final int y = Integer.parseInt(st.nextToken());
		final int z = Integer.parseInt(st.nextToken());
		if(player.getAdena() < cost)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else if(player.enterObserverMode(new Location(x, y, z)))
		{
			player.reduceAdena(cost, true);
			player.sendPacket(new SystemMessage(SystemMessage.S1_ADENA_DISAPPEARED).addNumber(cost));
		}
		if(player.isInOlympiadMode() || Olympiad.isRegisteredInComp(player) || player.getOlympiadGameId() > -1)
			player.sendActionFailed();
		player.sendActionFailed();
	}
}
