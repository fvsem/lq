package l2n.game.model.instances;

import l2n.Config;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ItemToDrop;
import l2n.game.model.drop.L2DropGroup;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.model.entity.siege.Siege;
import l2n.game.templates.L2NpcTemplate;

import java.util.List;

public class L2SiegeGuardInstance extends L2NpcInstance
{
	private boolean _isInvul = false;

	public L2SiegeGuardInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public int getAggroRange()
	{
		return 1200;
	}

	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		L2Player player = attacker.getPlayer();
		if(player == null)
			return false;
		L2Clan clan = player.getClan();
		if(clan != null && SiegeManager.getSiege(this, true) == clan.getSiege() && clan.isDefender())
			return false;
		Castle castle = getCastle();
		if(player.getTerritorySiege() > -1 && castle != null && player.getTerritorySiege() == castle.getId())
			return false;
		return true;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public boolean isInvul()
	{
		return _isInvul;
	}

	@Override
	public void setInvul(boolean b)
	{
		_isInvul = b;
	}

	@Override
	public void doDie(L2Character killer)
	{
		if(killer != null)
		{
			final L2Player player = killer.getPlayer();
			Siege siege = SiegeManager.getSiege(this, true);
			if(siege != null && player != null && siege.getSiegeUnit().getType() == ResidenceType.Fortress && killer.isPlayable())
			{
				final L2Clan clan = player.getClan();
				if(clan != null && siege == clan.getSiege() && !clan.isDefender())
				{
					L2Character topdam = getTopDamager(getAggroList());
					if(topdam == null)
						topdam = killer;

					final int levelDiff = calculateLevelDiffForDrop(topdam.getLevel(), false);
					if(getTemplate().getDropData() != null)
					{
						final List<L2DropGroup> drops = getTemplate().getDropData().getNormal();
						for(L2DropGroup h : drops)
						{
							List<ItemToDrop> itdl = h.rollNormal(levelDiff, this, player, Config.EPAULETTE_DROP_RATE);
							if(itdl != null)
								for(ItemToDrop itd : itdl)
									dropItem(player, itd.itemId, itd.count);
						}
					}
				}
			}
		}
		super.doDie(killer);
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}
}
