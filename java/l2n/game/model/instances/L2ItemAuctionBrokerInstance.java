package l2n.game.model.instances;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.itemauction.ItemAuction;
import l2n.game.instancemanager.itemauction.ItemAuctionInstance;
import l2n.game.instancemanager.itemauction.ItemAuctionManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExItemAuctionInfoPacket;
import l2n.game.templates.L2NpcTemplate;

import java.text.SimpleDateFormat;
import java.util.logging.Level;

public class L2ItemAuctionBrokerInstance extends L2NpcInstance
{
	private static final SimpleDateFormat fmt = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");

	private ItemAuctionInstance _instance;

	public L2ItemAuctionBrokerInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public final void onBypassFeedback(final L2Player activeChar, final String command)
	{
		if(Config.ALT_ITEM_AUCTION_ENABLED && command.startsWith("auction"))
		{
			if(_instance == null)
			{
				_instance = ItemAuctionManager.getInstance().getManagerInstance(getNpcId());
				if(_instance == null)
				{
					_log.log(Level.SEVERE, "L2ItemAuctionBrokerInstance: Missing instance for: " + getNpcId());
					return;
				}
			}

			final String[] params = command.split(" ");
			if(params.length == 1)
				return;

			if(params[1].equals("cancel"))
			{
				if(params.length == 3)
				{
					int auctionId = 0;
					try
					{
						auctionId = Integer.parseInt(params[2]);
					}
					catch(final NumberFormatException e)
					{
						_log.log(Level.WARNING, "", e);
						return;
					}

					final ItemAuction auction = _instance.getAuction(auctionId);
					if(auction != null)
						auction.cancelBid(activeChar);
				}
				else
				{
					final ItemAuction[] auctions = _instance.getAuctionsByBidder(activeChar.getObjectId());
					for(final ItemAuction auction : auctions)
						auction.cancelBid(activeChar);
				}
			}
			else if(params[1].equals("show"))
			{
				final ItemAuction currentAuction = _instance.getCurrentAuction();
				final ItemAuction nextAuction = _instance.getNextAuction();

				if(currentAuction == null)
				{
					activeChar.sendPacket(Msg.NO_AUCTION_PERIOD);
					if(nextAuction != null)
						activeChar.sendMessage("Следующий аукцион начнется " + fmt.format(nextAuction.getStartingTime()) + ".");
					return;
				}

				if(System.currentTimeMillis() - activeChar.getLastItemAuctionPacket() < 2000)
				{
					activeChar.sendPacket(Msg.THERE_ARE_NO_OFFERINGS_I_OWN_OR_I_MADE_A_BID_FOR);
					return;
				}

				activeChar.setLastItemAuctionPacket();
				activeChar.sendPacket(new ExItemAuctionInfoPacket(false, currentAuction, nextAuction));
			}
		}
		else
			super.onBypassFeedback(activeChar, command);
	}
}
