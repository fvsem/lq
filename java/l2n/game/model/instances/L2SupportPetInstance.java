package l2n.game.model.instances;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.EffectList;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.PetSkillsTable.L2PetSkillLearn;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

import java.util.concurrent.Future;

public final class L2SupportPetInstance extends L2PetInstance implements Runnable
{
	private final GArray<L2Skill> _possibleBuffs;

	private L2PetSkillLearn _majorHeal;
	private L2PetSkillLearn _minorHeal;
	private L2PetSkillLearn _recharge;
	private L2PetSkillLearn[] _buffs;

	private boolean _castTaskEnabled;
	private Future<?> _castTask;

	private boolean _toggleSupportCombatMode;

	public L2SupportPetInstance(final int objectId, final L2NpcTemplate template, final L2Player owner, final L2ItemInstance control, final byte _currentLevel, final long exp)
	{
		super(objectId, template, owner, control, _currentLevel, exp);
		_possibleBuffs = new GArray<L2Skill>();
		_toggleSupportCombatMode = true;
	}

	public L2SupportPetInstance(final int objectId, final L2NpcTemplate template, final L2Player owner, final L2ItemInstance control)
	{
		super(objectId, template, owner, control);
		_possibleBuffs = new GArray<L2Skill>();
		_toggleSupportCombatMode = true;
	}

	public final void toggleSupportCombatMode()
	{
		final L2Player owner = getPlayer();
		if(owner != null)
			synchronized (_taskLock)
			{
				_toggleSupportCombatMode = !_toggleSupportCombatMode;

				if(_toggleSupportCombatMode)
				{
					owner.sendMessage((getName() == null || getName().isEmpty() ? getTemplate().name : getName()) + ": Buff is now on.");
					startCastTask(false);
				}
				else if(_castTaskEnabled && _castTask != null)
				{
					owner.sendMessage((getName() == null || getName().isEmpty() ? getTemplate().name : getName()) + ": Buff is now off.");
					stopCastTask();
					_castTaskEnabled = true;
				}
			}
	}

	public final boolean isInSupportMode()
	{
		return _toggleSupportCombatMode;
	}

	public final boolean isInCombatMode()
	{
		return !_toggleSupportCombatMode;
	}

	public final void updateSupportSkills()
	{
		final L2PetSkillLearn[] supportSkills = _data.getPetSupportSkills();

		GArray<L2PetSkillLearn> buffs = null;
		double healPower = 0;
		L2Skill skill;

		for(final L2PetSkillLearn supportSkill : supportSkills)
		{
			skill = supportSkill.getSkill();
			if(skill != null)
				switch (skill.getSkillType())
				{
					case HEAL:
					case HEAL_PERCENT:
					case HEALCOMBATPOINT_PERCENT:
					{
						if(healPower == 0)
						{
							// set first heal as major heal
							_majorHeal = supportSkill;
							healPower = skill.getPower();
						}
						else // another heal skill found - search for most powerful
						if(skill.getPower() > healPower)
						{
							if(skill.getId() != _majorHeal.getId())
								_minorHeal = _majorHeal;
							_majorHeal = supportSkill;
						}
						else if(skill.getId() != _majorHeal.getId())
							_minorHeal = supportSkill;
						break;
					}

					case BUFF:
					{
						if(buffs == null)
							buffs = new GArray<L2PetSkillLearn>();
						buffs.add(supportSkill);
						break;
					}
					case MANAHEAL:
					case MANAHEAL_PERCENT:
					{
						_recharge = supportSkill;
						break;
					}
				}
		}

		_buffs = buffs != null ? buffs.toArray(new L2PetSkillLearn[buffs.size()]) : null;

		synchronized (_taskLock)
		{
			if(_castTaskEnabled && _castTask == null)
				startCastTask(false);
		}
	}

	@Override
	public final void run()
	{
		final L2PetSkillLearn majorHeal;
		final L2PetSkillLearn minorHeal;
		final L2PetSkillLearn recharge;
		final L2PetSkillLearn[] buffs;

		synchronized (_taskLock)
		{
			if(!_castTaskEnabled)
				return;

			majorHeal = _majorHeal;
			minorHeal = _minorHeal;
			recharge = _recharge;
			buffs = _buffs;

			if(isDead() || !isInSupportMode() || majorHeal == null && minorHeal == null && recharge == null && (buffs == null || buffs.length == 0))
			{
				if(_castTaskEnabled)
				{
					stopCastTask();
					_castTaskEnabled = true;
				}
				else
					stopCastTask();
				return;
			}
		}

		final L2Player owner = getPlayer();

		// if the owner is dead, merely wait for the owner to be resurrected
		// if the pet is still casting from the previous iteration, allow the cast to complete...
		if(owner != null && !owner.isDead() && !owner.isInvul() && !owner.isInvisible() && !isActionsDisabled() && getAI().getIntention() != CtrlIntention.AI_INTENTION_CAST)
		{
			if(getEffectList().getFirstEffect(5753) != null) // Awakening
				return;

			L2Skill skill = null;

			if(majorHeal != null)
			{
				// if the owner's HP is more than 80%, do nothing.
				// if the owner's HP is very low (less than 30%) have a high chance for strong heal
				// otherwise, have a low chance for weak heal
				final double hpPercent = owner.getCurrentHpPercents();
				if(hpPercent < 30)
				{
					if(Rnd.get(100) <= (hpPercent < 15 ? 100 : 80))
						if(!isSkillDisabled(majorHeal.hashCode()))
						{
							skill = majorHeal.getSkill();
							if(skill != null && getCurrentMp() >= skill.getMpConsume())
							{
								castSkill(owner, skill);
								return;
							}
							else if(minorHeal != null && majorHeal != minorHeal && !isSkillDisabled(minorHeal.hashCode()))
							{
								skill = minorHeal.getSkill();
								if(skill != null && getCurrentMp() >= skill.getMpConsume())
								{
									castSkill(owner, skill);
									return;
								}
							}
						}
						else if(minorHeal != null && majorHeal != minorHeal && !isSkillDisabled(minorHeal.hashCode()))
						{
							skill = minorHeal.getSkill();
							if(skill != null && getCurrentMp() >= skill.getMpConsume())
							{
								castSkill(owner, skill);
								return;
							}
						}
				}
				else if(hpPercent < 80 && minorHeal != null)
					if(Rnd.get(100) <= 25)
						if(!isSkillDisabled(minorHeal.hashCode()))
						{
							skill = minorHeal.getSkill();
							if(skill != null && getCurrentMp() >= skill.getMpConsume())
							{
								castSkill(owner, skill);
								return;
							}
						}
			}

			_possibleBuffs.clear();
			// searching for usable buffs
			if(buffs != null)
				outer: for(final L2PetSkillLearn sk : buffs)
				{
					if((skill = sk.getSkill()) == null)
					{
						_log.warning("L2SupportPetInstance: wrong pet buff " + sk.getId() + " lvl " + sk.getLevel());
						continue; // пропустим итерацию
					}

					if(getCurrentMp() < skill.getMpConsume())
						continue;

					for(final L2Effect ef : owner.getEffectList().getAllEffects())
						if(EffectList.checkEffect(ef, skill))
							continue outer;

					_possibleBuffs.add(skill);
				}

			if(!_possibleBuffs.isEmpty())
			{
				skill = _possibleBuffs.getUnsafe(Rnd.get(_possibleBuffs.size()));
				castSkill(owner, skill);
				return;
			}

			// buffs not casted, trying recharge, if exist
			if(recharge != null && !isSkillDisabled(recharge.hashCode()) && owner.getCurrentMpPercents() < 66 && Rnd.get(100) <= 60)
			{
				skill = recharge.getSkill();
				if(skill != null && getCurrentMp() >= skill.getMpConsume())
				{
					castSkill(owner, skill);
					return;
				}
			}
		}
	}

	private synchronized final void startCastTask(final boolean updateSupportSkills)
	{
		synchronized (_taskLock)
		{
			if(updateSupportSkills)
				updateSupportSkills();

			_castTaskEnabled = true;

			if(_castTask == null && !isDead() && isInSupportMode() && (_majorHeal != null || _minorHeal != null || _recharge != null || _buffs != null && _buffs.length > 0))
				_castTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(this, 3000, 2000, false);
		}
	}

	private final void stopCastTask()
	{
		synchronized (_taskLock)
		{
			_castTaskEnabled = false;
			if(_castTask != null)
			{
				_castTask.cancel(true);
				_castTask = null;
			}
		}
	}

	private synchronized void castSkill(final L2Player owner, final L2Skill skill)
	{
		// casting automatically stops any other action (such as autofollow or a move-to).
		// We need to gather the necessary info to restore the previous state.
		final boolean previousFollowStatus = isFollow();

		// pet not following and owner outside cast range
		if(!previousFollowStatus && !isInsideRadius(owner, skill.getCastRange(), true, false))
			return;

		if(skill.checkCondition(this, owner, false, !isFollow(), true))
		{
			setTarget(owner);
			getAI().Cast(skill, owner, false, !isFollow());
		}

		final SystemMessage msg = new SystemMessage(SystemMessage.THE_PET_USES_S1);
		msg.addSkillName(skill.getId(), skill.getLevel());
		owner.sendPacket(msg);

		// calling useMagic changes the follow status, if the babypet actually casts
		// (as opposed to failing due some factors, such as too low MP, etc).
		// if the status has actually been changed, revert it. Else, allow the pet to
		// continue whatever it was trying to do.
		// NOTE: This is important since the pet may have been told to attack a target.
		// reverting the follow status will abort this attack! While aborting the attack
		// in order to heal is natural, it is not acceptable to abort the attack on its own,
		// merely because the timer stroke and without taking any other action...
		if(previousFollowStatus != isFollow())
			setFollowStatus(previousFollowStatus, true);
	}

	@Override
	public void onSpawn()
	{
		super.onSpawn();
		startCastTask(true);
	}

	@Override
	public void doDie(final L2Character killer)
	{
		stopCastTask();
		super.doDie(killer);
	}

	@Override
	public void doRevive()
	{
		super.doRevive();
		startCastTask(true);
	}

	@Override
	public void onDecay()
	{
		stopCastTask();
		super.onDecay();
	}

	@Override
	public void unSummon()
	{
		stopCastTask();
		super.unSummon();
	}

	@Override
	public void updateData()
	{
		super.updateData();
		updateSupportSkills();
	}
}
