package l2n.game.model.instances;

import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.templates.L2NpcTemplate;

public class L2CommanderInstance extends L2SiegeGuardInstance
{
	public L2CommanderInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void doDie(L2Character killer)
	{
		FortressSiege siege = FortressSiegeManager.getSiege(this);

		if(siege != null && siege.isInProgress())
			siege.killedCommander(this);

		super.doDie(killer);
	}
}
