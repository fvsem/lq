package l2n.game.model.instances;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.L2Multisell;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 19.04.2010
 * @time 5:13:59
 */
public class L2TerritoryManagerInstance extends L2NpcInstance
{
	public L2TerritoryManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		int npcId = getNpcId();
		int terr = npcId - 36489;
		if(terr > 9 || terr < 1)
			return;

		String badges = player.getVar("badges" + terr);
		int count = badges != null ? Integer.parseInt(badges) : 0;
		int territoryBadgeId = 13756 + terr;

		if(player.isGM())
			player.sendMessage("badgeId: " + territoryBadgeId);

		if(command.equalsIgnoreCase("buyspecial"))
		{
			if(Functions.getItemCount(player, territoryBadgeId) < 1)
				showChatWindow(player, getHtmlPath(npcId, 1));
			else
				L2Multisell.getInstance().separateAndSend(npcId, player, 0);
			return;
		}
		// покупка нублеса
		else if(command.equalsIgnoreCase("nobless"))
		{
			if(Functions.getItemCount(player, territoryBadgeId) < 1)
			{
				showChatWindow(player, getHtmlPath(npcId, 4));
				return;
			}
			NpcHtmlMessage html = new NpcHtmlMessage(player, this, getHtmlPath(npcId, 2), 5);
			html.replace("%noblessBadge%", String.valueOf(Config.MIN_TWBADGE_FOR_NOBLESS));
			player.sendPacket(html);
		}
		else if(command.equalsIgnoreCase("calculate"))
		{
			if(count < 1)
			{
				showChatWindow(player, getHtmlPath(npcId, 4));
				return;
			}
			NpcHtmlMessage html = new NpcHtmlMessage(player, this, getHtmlPath(npcId, 5), 5);
			html.replace("%territory%", CastleManager.getInstance().getCastleByIndex(terr).getName());
			html.replace("%badges%", "" + count);
			html.replace("%adena%", "" + count * 520);
			player.sendPacket(html);
		}
		else if(command.equalsIgnoreCase("recivelater"))
			showChatWindow(player, getHtmlPath(npcId, 6));
		else if(command.equalsIgnoreCase("recive"))
		{
			if(count < 1)
			{
				showChatWindow(player, getHtmlPath(npcId, 4));
				return;
			}
			player.unsetVar("badges" + terr);
			Functions.addItem(player, territoryBadgeId, count);
			Functions.addItem(player, 57, count * 520);
			showChatWindow(player, getHtmlPath(npcId, 7));
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		if(val == 0)
			return "data/html/TerritoryManager/TerritoryManager.htm";
		return "data/html/TerritoryManager/TerritoryManager-" + val + ".htm";
	}
}
