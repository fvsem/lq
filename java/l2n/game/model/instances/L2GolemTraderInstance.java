package l2n.game.model.instances;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.TradeController;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShopPreviewList;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class L2GolemTraderInstance extends L2MerchantInstance
{
	private static Logger _log = Logger.getLogger(L2GolemTraderInstance.class.getName());
	private ScheduledFuture<DestroyTask> _destroyTask;
	private long _ownerStoredId = 0;

	private class DestroyTask implements Runnable
	{
		@Override
		public void run()
		{
			unSummon();
		}
	}

	public L2GolemTraderInstance(int objectId, L2NpcTemplate template, L2Player owner, int lifeTime)
	{
		super(objectId, template);

		_ownerStoredId = owner.getStoredId();
		setTitle(owner.getName());
		spawnMe(owner.getLoc());

		owner.setMerchant(this);
		_destroyTask = L2GameThreadPools.getInstance().scheduleGeneral(new DestroyTask(), lifeTime);
	}

	public static void spawn(L2Skill skill, L2Player owner, int lifeTime)
	{
		new L2GolemTraderInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(skill.getNpcId()), owner, lifeTime);
	}

	public void unSummon()
	{
		L2Player player = getOwner();
		if(player != null)
			player.setMerchant(null);

		deleteMe();
		if(_destroyTask != null)
			_destroyTask.cancel(false);
		_destroyTask = null;
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		File file = new File(Config.DATAPACK_ROOT, "data/html/merchant/" + pom + ".htm");
		if(file.exists())
			return "data/html/merchant/" + pom + ".htm";
		return "data/html/teleporter/" + pom + ".htm";
	}

	private void showWearWindow(L2Player player, int val)
	{
		if(!player.getPlayerAccess().UseShop)
			return;

		player.tempInventoryDisable();
		if(Config.DEBUG)
			_log.fine("Showing wearlist");
		NpcTradeList list = TradeController.getInstance().getBuyList(val);

		if(list != null)
		{
			ShopPreviewList bl = new ShopPreviewList(list, player.getAdena(), player.expertiseIndex);
			player.sendPacket(bl);
		}
		else
		{
			_log.warning("no buylist with id:" + val);
			player.sendActionFailed();
		}
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken(); // Get actual command

		if(actualCommand.equalsIgnoreCase("Buy"))
		{
			if(st.countTokens() < 1)
				return;
			int val = Integer.parseInt(st.nextToken());
			showShopWindow(player, val, false);
		}
		else if(actualCommand.equalsIgnoreCase("Sell"))
			showShopWindow(player);
		else if(actualCommand.equalsIgnoreCase("Wear") && Config.WEAR_TEST_ENABLED)
		{
			if(st.countTokens() < 1)
				return;
			int val = Integer.parseInt(st.nextToken());
			showWearWindow(player, val);
		}
		else if(actualCommand.equalsIgnoreCase("Multisell"))
		{
			if(st.countTokens() < 1)
				return;
			int val = Integer.parseInt(st.nextToken());
			L2Multisell.getInstance().separateAndSend(val, player, getCastle() != null ? getCastle().getTaxRate() : 0);
		}
		else
			super.onBypassFeedback(player, command);
	}

	public L2Player getOwner()
	{
		return L2ObjectsStorage.getAsPlayer(_ownerStoredId);
	}
}
