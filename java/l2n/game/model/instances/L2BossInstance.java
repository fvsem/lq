package l2n.game.model.instances;

import l2n.game.templates.L2NpcTemplate;

import java.util.logging.Logger;

public class L2BossInstance extends L2RaidBossInstance
{
	protected final static Logger _log = Logger.getLogger(L2BossInstance.class.getName());

	private static final int BOSS_MAINTENANCE_INTERVAL = 10000;

	/**
	 * Constructor for L2BossInstance. This represent all grandbosses:
	 * <ul>
	 * <li>29001 Queen Ant</li>
	 * <li>29014 Orfen</li>
	 * <li>29019 Antharas</li>
	 * <li>29067 Antharas</li>
	 * <li>29068 Antharas</li>
	 * <li>29020 Baium</li>
	 * <li>29022 Zaken</li>
	 * <li>29028 Valakas</li>
	 * <li>29006 Core</li>
	 * <li>29045 Frintezza</li>
	 * <li>29046 Scarlet Van Halisha 1st Morph</li>
	 * <li>29047 Scarlet Van Halisha 3rd Morph</li>
	 * </ul>
	 * <br>
	 * <b>For now it's nothing more than a L2Monster but there'll be a scripting<br>
	 * engine for AI soon and we could add special behaviour for those boss</b><br>
	 * <br>
	 * 
	 * @param objectId
	 *            ID of the instance
	 * @param template
	 *            L2NpcTemplate of the instance
	 */
	public L2BossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected int getMaintenanceInterval()
	{
		return BOSS_MAINTENANCE_INTERVAL;
	}

	@Override
	public final boolean isMovementDisabled()
	{
		// Core should stay anyway
		return getNpcId() == 29006 || super.isMovementDisabled();
	}

	@Override
	public boolean isAggressionImmune()
	{
		if(getTemplate().npcId == 29020) // Baium'a нельзя агрить.
			return true;
		return super.isAggressionImmune();
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}
}
