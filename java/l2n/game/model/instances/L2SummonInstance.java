package l2n.game.model.instances;

import l2n.commons.lang.reference.IHardReference;
import l2n.commons.sql.SqlBatch;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.event.L2EventType;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.serverpackets.SetSummonRemainTime;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.StringUtil;

import java.sql.ResultSet;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2SummonInstance extends L2Summon
{
	private static final Logger _log = Logger.getLogger(L2SummonInstance.class.getName());
	public static final int CYCLE = 5000; // in millis

	private float _expPenalty = 0;
	private final int _itemConsumeIdInTime;
	private final int _itemConsumeCountInTime;
	private int _itemConsumeDelay;

	private Future<SummonLifetime> _disappearTask;

	private int _consumeCountdown;
	private int _lifetimeCountdown;
	private final int _maxLifetime;
	public int lastShowntimeRemaining; // to avoid sending useless packets

	private final int _ownerObjectId;

	public L2SummonInstance(final int objectId, final L2NpcTemplate template, final L2Player owner, final int lifetime, final int consumeid, final int consumecount, final int consumedelay)
	{
		super(objectId, template, owner);

		setName(template.name);
		_lifetimeCountdown = _maxLifetime = lifetime;
		lastShowntimeRemaining = lifetime;
		_itemConsumeIdInTime = consumeid;
		_itemConsumeCountInTime = consumecount;
		_consumeCountdown = _itemConsumeDelay = consumedelay;
		_disappearTask = L2GameThreadPools.getInstance().scheduleGeneral(new SummonLifetime(), CYCLE);

		// сохранУм, а то прУ выходе Угрока не сможем получУть этУ данные
		_ownerObjectId = owner.getObjectId();

		// TODO добавУть проверкУ на Увенты
		// на олУмпе не восстанавлУваем У на Увентах
		if(owner.getOlympiadGameId() == -1 && !owner.isInOlympiadMode() && !Olympiad.isRegisteredInComp(owner) && owner.getTeam() == 0 && !owner.isInEvent(L2EventType.NONE))
			restoreEffects(); // восстанавлУваем эффекты суммона

		updateEffectIcons();
		setWearedWeaponType(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IHardReference<L2SummonInstance> getRef()
	{
		return (IHardReference<L2SummonInstance>) super.getRef();
	}

	@Override
	public final byte getLevel()
	{
		return getTemplate() != null ? getTemplate().level : 0;
	}

	@Override
	public int getSummonType()
	{
		return 1;
	}

	@Override
	public int getCurrentFed()
	{
		return _lifetimeCountdown;
	}

	@Override
	public int getMaxFed()
	{
		return _maxLifetime;
	}

	public void setExpPenalty(final float expPenalty)
	{
		_expPenalty = expPenalty;
	}

	@Override
	public float getExpPenalty()
	{
		return _expPenalty;
	}

	@Override
	public void reduceCurrentHp(final double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp, final boolean canReflect, final boolean isCounteAttack)
	{
		if(attacker.isPlayable() && isInZoneBattle() != attacker.isInZoneBattle())
		{
			attacker.getPlayer().sendPacket(Msg.INCORRECT_TARGET);
			return;
		}
				
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);

		final L2Player owner = getPlayer();
		if(owner == null)
			return;			
		final SystemMessage sm = new SystemMessage(SystemMessage.THE_SUMMONED_MONSTER_RECEIVED_DAMAGE_OF_S2_CAUSED_BY_S1);

		if(attacker.isNpc())
			sm.addNpcName(((L2NpcInstance) attacker).getTemplate().npcId);
		else
			sm.addString(attacker.getName());

		owner.sendPacket(sm.addNumber((int) damage));
	}

	@Override
	public void displayHitMessage(final L2Character target, final int damage, final boolean crit, final boolean miss)
	{
		final L2Player owner = getPlayer();
		if(owner == null)
			return;

		if(crit)
			owner.sendPacket(Msg.SUMMONED_MONSTERS_CRITICAL_HIT);
		if(miss)
			owner.sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_WENT_ASTRAY).addName(this));
		else
			owner.sendPacket(new SystemMessage(SystemMessage.S1_HAS_GIVEN_S2_DAMAGE_OF_S3).addName(this).addName(target).addNumber(damage));
	}

	private class SummonLifetime implements Runnable
	{
		@Override
		public void run()
		{
			final L2Player owner = getPlayer();
			if(owner == null)
			{
				// TODO при выходе игрока не удалять суммона
				unSummon();
				return;
			}

			final int usedtime = isInCombat() ? CYCLE : CYCLE / 4;
			_lifetimeCountdown -= usedtime;

			// check if the summon's lifetime has ran out
			if(_lifetimeCountdown <= 0)
			{
				owner.sendPacket(Msg.YOUR_SERVITOR_HAS_VANISHED_YOU_LL_NEED_TO_SUMMON_A_NEW_ONE);
				unSummon();
				return;
			}

			_consumeCountdown -= usedtime;
			// check if it is time to consume another item
			if(_itemConsumeIdInTime > 0 && _itemConsumeCountInTime > 0 && _consumeCountdown <= 0)
			{
				// check if owner has enought itemConsume, if requested
				final L2ItemInstance item = owner.getInventory().getItemByItemId(getItemConsumeIdInTime());
				if(item != null && item.getCount() >= getItemConsumeCountInTime())
				{
					_consumeCountdown = _itemConsumeDelay;
					final L2ItemInstance dest = owner.getInventory().destroyItemByItemId(getItemConsumeIdInTime(), getItemConsumeCountInTime(), false);
					owner.sendPacket(new SystemMessage(SystemMessage.A_SUMMONED_MONSTER_USES_S1).addItemName(dest.getItemId()));
				}
				else
				{
					owner.sendPacket(Msg.SINCE_YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_MAINTAIN_THE_SERVITOR_S_STAY_THE_SERVITOR_HAS_DISAPPEARED);
					unSummon();
					return;
				}
			}

			// prevent useless packet-sending when the difference isn't visible.
			if(lastShowntimeRemaining - _lifetimeCountdown > _maxLifetime / 352)
			{
				owner.sendPacket(new SetSummonRemainTime(L2SummonInstance.this));
				lastShowntimeRemaining = _lifetimeCountdown;
				updateEffectIcons();
			}

			_disappearTask = L2GameThreadPools.getInstance().scheduleGeneral(new SummonLifetime(), CYCLE);
		}
	}

	@Override
	public void doDie(final L2Character killer)
	{
		super.doDie(killer);

		// сохраняем эффекты суммона
		storeEffects();
		startDecay(8500L);
		if(_disappearTask != null)
		{
			_disappearTask.cancel(false);
			_disappearTask = null;
		}
	}

	public int getItemConsumeIdInTime()
	{
		return _itemConsumeIdInTime;
	}

	public int getItemConsumeCountInTime()
	{
		return _itemConsumeCountInTime;
	}

	public int getItemConsumeDelay()
	{
		return _itemConsumeDelay;
	}

	protected synchronized void stopDisappear()
	{
		if(_disappearTask != null)
		{
			_disappearTask.cancel(true);
			_disappearTask = null;
		}
	}

	@Override
	public void unSummon()
	{
		// сохраняем эффекты суммона
		storeEffects();
		stopDisappear();
		super.unSummon();
	}

	/**
	 * сохраняет эффекты для суммона
	 */
	private void storeEffects()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM summon_effects_save WHERE char_obj_id = " + _ownerObjectId + " AND npc_id=" + getNpcId());

			if(_effectList == null || _effectList.isEmpty())
				return;

			int order = 0;
			final SqlBatch b = new SqlBatch("INSERT IGNORE INTO `summon_effects_save` (`char_obj_id`,`npc_id`,`skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration`,`order`) VALUES");
			synchronized (getEffectList())
			{
				StringBuilder sb;
				for(L2Effect effect : getEffectList().getAllEffects())
					if(effect != null && effect.isInUse() && !effect.getSkill().isToggle())
					{
						switch (effect.getEffectType())
						{
							case HealOverTime:
							case CombatPointHealOverTime:
							case Invisible:
								continue;
						}

						if(effect.isSaveable())
						{
							sb = StringUtil.startAppend(200, "(");
							StringUtil.append(sb, _ownerObjectId, ",", getNpcId(), ",", effect.getSkill().getId(), ",", effect.getSkill().getLevel(), ",", effect.getCount(), ",", effect.getTime(), ",", effect.getPeriod(), ",", order, ")");
							b.write(sb.toString());
						}
						while ((effect = effect.getNext()) != null && effect.isSaveable())
						{
							sb = StringUtil.startAppend(200, "(");
							StringUtil.append(sb, _ownerObjectId, ",", getNpcId(), ",", effect.getSkill().getId(), ",", effect.getSkill().getLevel(), ",", effect.getCount(), ",", effect.getTime(), ",", effect.getPeriod(), ",", order, ")");
							b.write(sb.toString());
						}
						order++;
					}
			}
			if(!b.isEmpty())
				statement.executeUpdate(b.close());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2SummonInstance.storeEffects() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * восстанавлУвает эффекты для суммона
	 */
	public void restoreEffects()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT `skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration` FROM `summon_effects_save` WHERE `char_obj_id`=? AND `npc_id`=? ORDER BY `order` ASC");
			statement.setInt(1, _ownerObjectId);
			statement.setInt(2, getNpcId());
			rset = statement.executeQuery();
			while (rset.next())
			{
				final int skillId = rset.getInt("skill_id");
				final int skillLvl = rset.getInt("skill_level");
				final int effectCount = rset.getInt("effect_count");
				final long effectCurTime = rset.getLong("effect_cur_time");
				final long duration = rset.getLong("duration");

				final L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLvl);
				if(skill == null || skill.getEffectTemplates() == null)
					continue;

				for(final EffectTemplate et : skill.getEffectTemplates())
				{
					if(et == null)
						continue;
					final Env env = new Env(this, this, skill);
					final L2Effect effect = et.getEffect(env);
					if(effect == null || effect.isOneTime())
						continue;

					effect.setCount(effectCount);
					effect.setPeriod(effectCount == 1 ? duration - effectCurTime : duration);

					// Добавляем эффекты
					getEffectList().addEffect(effect);
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "L2SummonInstance.restoreEffects() error: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	@Override
	public boolean isSummon()
	{
		return true;
	}

	@Override
	public String toString()
	{
		return "Summon: name-" + getName() + "[" + getNpcId() + "], ownerObjId-" + _ownerObjectId;
	}
}
