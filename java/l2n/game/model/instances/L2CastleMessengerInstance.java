package l2n.game.model.instances;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

public class L2CastleMessengerInstance extends L2NpcInstance
{
	public L2CastleMessengerInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		if(!getCastle().getSiege().isInProgress() || !TerritorySiege.isInProgress())
			getCastle().getSiege().listRegisterClan(player);
		else
		{
			NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
			html.setHtml("<html><body><font color=\"LEVEL\">Oh! Our castle is being attacked and I can't do anything for you right now.</font></body></html>");
			player.sendPacket(html);
		}
	}
}
