package l2n.game.model.instances;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ActionFail;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

/**
 * Класс инстанса Варден НПЦ в крепостях
 * Отвечает за вход в крепостную пайлаку<br>
 * <br>
 * 
 * @author minlexx
 */
public class L2FortressWardenInstance extends L2NpcInstance
{
	public L2FortressWardenInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		player.sendActionFailed();
		player.sendPacket(ActionFail.STATIC_PACKET);
		player.setLastNpc(this);
		String filename = "data/html/fortress/fwarden-no.htm";

		int condition = validateCondition(player);

		if(condition > COND_ALL_FALSE)
			if(condition == COND_BUSY_BECAUSE_OF_SIEGE)
				filename = "data/html/fortress/fwarden-busy.htm"; // Busy because of siege
			else if(condition == COND_OWNER)
				if(val == 0)
					filename = "data/html/fortress/warden.htm";
				else
					filename = "data/html/fortress/warden-" + val + ".htm";

		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		html.setFile(filename);
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcId%", String.valueOf(getNpcId()));
		html.replace("%npcname%", String.valueOf(getName() + " " + getTitle()));
		player.sendPacket(html);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		player.sendActionFailed();
		if(!isInRange(player, INTERACTION_DISTANCE))
			return;

		int condition = validateCondition(player);
		if(condition <= COND_ALL_FALSE || condition == COND_BUSY_BECAUSE_OF_SIEGE)
			return;

		if((player.getClanPrivileges() & L2Clan.CP_CS_USE_FUNCTIONS) != L2Clan.CP_CS_USE_FUNCTIONS)
		{
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ResidenceManager.NotAuthorizedToDoThis", player));
			return;
		}

		/*
		 * if( command.equalsIgnoreCase("UseCastleDungeon") )
		 * {
		 * if(condition == COND_OWNER) showChatWindow(player, 2);
		 * else showChatWindow(player, "data/html/fortress/warden-no.htm");
		 * return;
		 * }
		 */

		if(condition == COND_OWNER)
			super.onBypassFeedback(player, command);
	}

	protected int validateCondition(L2Player player)
	{
		if(player.isGM())
			return COND_OWNER; // GM is owner of all castles :)
		if(getFortress() != null && getFortress().getId() > 0)
			if(player.getClan() != null)
				if(getFortress().getSiege().isInProgress())
					return COND_BUSY_BECAUSE_OF_SIEGE; // Busy because of siege
				else if(getFortress().getOwnerId() == player.getClanId())
					return COND_OWNER; // Clan owns fort
		return COND_ALL_FALSE;
	}
}
