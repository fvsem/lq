package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.instancemanager.PcCafePointsManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.base.Experience;
import l2n.game.model.entity.DimensionalRift;
import l2n.game.model.entity.SevenSignsFestival.DarknessFestival;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.Stats;
import l2n.game.tables.ItemTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2Item;
import l2n.util.*;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class L2Party
{
	public static final int MAX_PARTY_MEMBERS = 9;

	public static final int ITEM_LOOTER = 0;
	public static final int ITEM_RANDOM = 1;
	public static final int ITEM_RANDOM_SPOIL = 2;
	public static final int ITEM_ORDER = 3;
	public static final int ITEM_ORDER_SPOIL = 4;

	private static final double[] BONUS_EXP_SP =
	{
			1, 1.30, 1.39, 1.50, 1.54, 1.58, 1.63, 1.67, 1.71
	};

	private final Lock _readLock;
	private final Lock _writeLock;
	private final GArray<Long> _members;

	private int _partyLvl = 0;
	private int _itemDistribution = 0;
	private int _itemOrder = 0;
	private long _dr; // Dimensiona lRift
	private long _reflection;
	private L2CommandChannel _commandChannel;

	public float _rateExp;
	public float _rateSp;
	public float _rateDrop;
	public float _rateAdena;
	public float _rateSpoil;

	private final UpdatePositionTask posTask = new UpdatePositionTask(this);
	private ScheduledFuture<UpdatePositionTask> _posTask;

	/**
	 * constructor ensures party has always one member - leader
	 * 
	 * @param leader
	 *            создатель парти
	 * @param itemDistribution
	 *            режим распределения лута
	 */
	public L2Party(final L2Player leader, final int itemDistribution)
	{
		final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
		_readLock = lock.readLock();
		_writeLock = lock.writeLock();
		_members = new GArray<Long>(MAX_PARTY_MEMBERS);
		_members.addLastUnsafe(leader.getStoredId());

		_itemDistribution = itemDistribution;
		_partyLvl = leader.getLevel();

		if(_posTask != null)
		{
			_posTask.cancel(true);
			_posTask = null;
		}
		_posTask = L2GameThreadPools.getInstance().scheduleGeneral(posTask, 11000);
	}

	/**
	 * @return number of party members
	 */
	public final int getMemberCount()
	{
		return _members.size();
	}

	/**
	 * @return all party members
	 */
	public final GArray<L2Player> getPartyMembers()
	{
		final GArray<L2Player> result = new GArray<L2Player>(MAX_PARTY_MEMBERS);
		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				result.addLastUnsafe(member);
		}
		return result;
	}

	public final GArray<Integer> getPartyMembersObjIds()
	{
		final GArray<Integer> result = new GArray<Integer>(MAX_PARTY_MEMBERS);
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null)
				result.addLastUnsafe(L2ObjectsStorage.getStoredObjectId(storedId));
		}
		return result;
	}

	public final GArray<L2Playable> getPartyMembersWithPets()
	{
		final GArray<L2Playable> result = new GArray<L2Playable>(MAX_PARTY_MEMBERS * 2);
		L2Player member;
		L2Summon member_pet;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
			{
				result.addLastUnsafe(member);
				if((member_pet = member.getPet()) != null)
					result.addLastUnsafe(member_pet);
			}
		}
		return result;
	}

	public final L2Player getRandomMember()
	{
		long storedId = 0;
		_readLock.lock();
		try
		{
			storedId = _members.getUnsafe(Rnd.get(_members.size()));
		}
		finally
		{
			_readLock.unlock();
		}
		return L2ObjectsStorage.getAsPlayer(storedId);
	}

	public final int indexOfPartyMember(final L2Player player)
	{
		final int size = _members.size();
		for(int i = size; i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && storedId == player.getStoredId())
				return i;
		}
		return -1;
	}

	/**
	 * @return random member from party
	 */
	private L2Player getRandomMemberInRange(final L2Player player, final L2ItemInstance item, final int range)
	{
		GArray<L2Player> stack = null;

		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				if(member.getReflectionId() == player.getReflectionId() && member.isInRange(player, range) && !member.isDead() && member.getInventory().validateCapacity(item) && member.getInventory().validateWeight(item))
				{
					if(stack == null)
						stack = new GArray<L2Player>(MAX_PARTY_MEMBERS);
					stack.addLastUnsafe(member);
				}
		}

		if(stack != null)
			return stack.getUnsafe(Rnd.get(stack.size()));
		return null;
	}

	/**
	 * @return next item looter
	 */
	private L2Player getNextLooterInRange(final L2Player player, final L2ItemInstance item, final int range)
	{
		final GArray<L2Player> members = getPartyMembers();

		int antiloop = members.size();
		while (--antiloop > 0)
		{
			final int looter = _itemOrder;
			_itemOrder++;
			if(_itemOrder > members.size() - 1)
				_itemOrder = 0;

			final L2Player ret = looter < members.size() ? members.getUnsafe(looter) : player;
			if(ret != null && ret.getReflectionId() == player.getReflectionId() && ret.isInRange(player, range) && !ret.isDead())
				return ret;
		}
		return player;
	}

	/**
	 * true if player is party leader
	 * 
	 * @param player
	 * @return
	 */
	public boolean isLeader(final L2Player player)
	{
		final L2Player leader = getPartyLeader();
		return leader != null && player.equals(leader);
	}

	/**
	 * Returns the Object ID for the party leader to be used as a unique identifier of this party
	 * 
	 * @return int
	 */
	public int getPartyLeaderOID()
	{
		if(_members.size() == 0)
			return 0;
		return L2ObjectsStorage.getStoredObjectId(_members.getUnsafe(0));
	}

	/**
	 * Возвращает лидера партии
	 * 
	 * @return L2Player Лидер партии
	 */
	public L2Player getPartyLeader()
	{
		if(_members.size() == 0)
			return null;
		return L2ObjectsStorage.getAsPlayer(_members.getUnsafe(0));
	}

	/**
	 * Broadcasts packet to every party member
	 * 
	 * @param msg
	 *            packet to broadcast
	 */
	public void broadcastToPartyMembers(final L2GameServerPacket... packets)
	{
		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				member.sendPacket(packets);
		}
	}

	/**
	 * Рассылает пакет всем членам группы исключая указанного персонажа
	 */
	public void broadcastToPartyMembers(final L2Player exclude, final L2GameServerPacket... packets)
	{
		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				if(member != exclude)
					member.sendPacket(packets);
		}
	}

	/**
	 * Рассылает текстовое сообщение всем членам группы
	 * 
	 * @param msg
	 *            сообщение
	 */
	public void broadcastMessageToPartyMembers(final String msg)
	{
		broadcastToPartyMembers(SystemMessage.sendString(msg));
	}

	public void broadcastToPartyMembersInRange(final L2Player player, final L2GameServerPacket packet, final int range)
	{
		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null && member.getReflectionId() == player.getReflectionId() && player.isInRange(member, range))
				member.sendPacket(packet);
		}
	}

	/**
	 * Возвращает true если персонаж уже находиться в этой группе
	 */
	public boolean containsMember(final L2Player player)
	{
		if(player == null)
			return false;

		final GArray<L2Player> members = getPartyMembers();
		for(final L2Player pl : members)
			if(pl.getObjectId() == player.getObjectId())
				return true;
		return false;
	}

	/**
	 * adds new member to party
	 * 
	 * @param player
	 *            L2Player to add
	 */
	public void addPartyMember(final L2Player player)
	{
		final L2Player leader = getPartyLeader();
		if(leader == null)
			return;

		// Проверка на пати баг ^_~
		if(containsMember(player))
		{
			player.setAccessLevel(-100);
			player.setAccountAccesslevel(-66, "Try self add party", -1);
			player.logout(false, false, true, true);
			Util.handleIllegalPlayerAction(player, "L2Party.addPartyMember()", "try self add party", IllegalPlayerAction.WARNING);
			return;
		}

		L2Player member;
		L2Summon player_pet, member_pet;
		Collection<L2GameServerPacket> pmember, pmember_proto = new GArray<L2GameServerPacket>(), pplayer = new GArray<L2GameServerPacket>();
		_writeLock.lock();
		try
		{
			if(_members.size() >= MAX_PARTY_MEMBERS)
				return;

			if(_members.contains(player.getStoredId()))
				return;

			// sends new member party window for all members
			// we do all actions before adding member to a list, this speeds things up a little
			pplayer.add(new PartySmallWindowAll(this, player));
			pplayer.add(new SystemMessage(SystemMessage.YOU_HAVE_JOINED_S1S_PARTY).addString(leader.getName()));

			// sends new member party window for all members
			pmember_proto.add(new SystemMessage(SystemMessage.S1_HAS_JOINED_THE_PARTY).addString(player.getName()));
			pmember_proto.add(new PartySmallWindowAdd(player));
			pmember_proto.add(new PartySpelled(player, true));
			if((player_pet = player.getPet()) != null)
			{
				pmember_proto.add(new ExPartyPetWindowAdd(player_pet));
				pmember_proto.add(new PartySpelled(player_pet, true));
			}

			for(final Long storedId : _members)
				if((member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				{
					pmember = new GArray<L2GameServerPacket>();
					pmember.addAll(pmember_proto);
					pmember.addAll(RelationChanged.update(player, member));
					member.sendPackets(pmember);
					pmember = null;

					pplayer.add(new PartySpelled(member, true));
					if((member_pet = member.getPet()) != null)
						pplayer.add(new PartySpelled(member_pet, true));
					pplayer.addAll(RelationChanged.update(member, player));
				}

			_members.addLastUnsafe(player.getStoredId());

			// Если партия уже в СС, то вновь прибывшем посылаем пакет открытия окна СС
			if(isInCommandChannel())
				pplayer.add(Msg.ExMPCCOpen);

			player.sendPackets(pplayer);
			pplayer = null;
			pmember_proto = null;
		}
		finally
		{
			_writeLock.unlock();
		}

		recalculatePartyData();

		if(player.getPet() != null)
			player.getPet().updateEffectIcons();

		if(isInReflection() && getReflection().isDimensionalRift())
			((DimensionalRift) getReflection()).partyMemberInvited();

		if(player.getPartyRoom() > 0)
		{
			final PartyRoom room = PartyRoomManager.getInstance().getRooms().get(player.getPartyRoom());
			if(room != null)
				room.updateInfo();
		}
	}

	/**
	 * Удаляет все связи
	 */
	public void dissolveParty()
	{
		_writeLock.lock();
		try
		{
			for(final L2Player p : getPartyMembers())
				p.setParty(null);
			_members.clear();
		}
		finally
		{
			_writeLock.unlock();
		}

		setDimensionalRift(null);
		_commandChannel = null;
		if(_posTask != null)
			_posTask.cancel(false);
	}

	/**
	 * removes player from party
	 * 
	 * @param player
	 *            L2Player to remove
	 */
	private final void removePartyMember(final L2Player player)
	{
		if(player == null)
			return;

		_writeLock.lock();
		try
		{
			if(!_members.remove(player.getStoredId()))
				return;
			posTask.remove(player);
		}
		finally
		{
			_writeLock.unlock();
		}

		Collection<L2GameServerPacket> pplayer = new GArray<L2GameServerPacket>();

		// Отсылаемы вышедшему пакет закрытия СС
		if(isInCommandChannel())
			pplayer.add(Msg.ExMPCCClose);

		pplayer.add(Msg.YOU_HAVE_WITHDRAWN_FROM_THE_PARTY);
		pplayer.add(Msg.PartySmallWindowDeleteAll);
		player.setParty(null);

		final Collection<L2GameServerPacket> pmember_proto = new GArray<L2GameServerPacket>();
		L2Summon player_pet;
		if((player_pet = player.getPet()) != null)
			pmember_proto.add(new ExPartyPetWindowDelete(player_pet));
		pmember_proto.add(new PartySmallWindowDelete(player));
		pmember_proto.add(new SystemMessage(SystemMessage.S1_HAS_LEFT_THE_PARTY).addString(player.getName()));

		_writeLock.lock();
		try
		{
			L2Player member;
			Collection<L2GameServerPacket> pmember;
			for(final long storedId : _members)
				if((member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				{
					pmember = new GArray<L2GameServerPacket>();
					pmember.addAll(pmember_proto);
					pmember.addAll(RelationChanged.update(player, member));
					member.sendPackets(pmember);
					pmember = null;
					pplayer.addAll(RelationChanged.update(member, player));
				}
		}
		finally
		{
			_writeLock.unlock();
		}

		player.sendPackets(pplayer);
		pplayer = null;

		recalculatePartyData();

		final Reflection reflection = getReflection();
		if(reflection instanceof DarknessFestival)
			((DarknessFestival) reflection).partyMemberExited();
		else if(isInReflection() && getReflection() instanceof DimensionalRift)
			((DimensionalRift) getReflection()).partyMemberExited(player);

		if(reflection != null && player.getReflection().getId() == reflection.getId() && reflection.getReturnLoc() != null)
		{
			player.teleToLocation(reflection.getReturnLoc(), 0);
			player.sendPacket(new ExSendUIEvent(player, true, true, 0, 10, ""));
		}

		if(player.getDuel() != null)
			player.getDuel().onRemoveFromParty(player);

		final L2Player leader = getPartyLeader();
		if(_members.size() == 1 || leader == null)
		{
			if(leader != null && leader.getDuel() != null)
				leader.getDuel().onRemoveFromParty(leader);

			// Если в партии остался 1 человек, то удаляем ее из СС
			if(isInCommandChannel())
				_commandChannel.removeParty(this);
			else if(reflection != null)
			{
				if(reflection.getParty() == this)
					reflection.startCollapseTimer(60000);
				if(leader != null && leader.getReflection().getId() == reflection.getId())
					leader.broadcastPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(1));
				setReflection(null);
			}

			if(leader != null)
				leader.setParty(null);
			dissolveParty();
		}
		else if(isInCommandChannel() && _commandChannel.getChannelLeader() == player)
			_commandChannel.setChannelLeader(leader);

		if(player.getPartyRoom() > 0)
		{
			final PartyRoom room = PartyRoomManager.getInstance().getRooms().get(player.getPartyRoom());
			if(room != null)
				room.updateInfo();
		}
	}

	/**
	 * Change party leader (used for string arguments)
	 * 
	 * @param name
	 *            имя нового лидера парти
	 */
	public void changePartyLeader(final String name)
	{
		final L2Player new_leader = getPlayerByName(name);
		if(new_leader == null)
			return;

		_writeLock.lock();
		try
		{
			final L2Player current_leader = getPartyLeader();
			if(current_leader == null)
				return;

			if(current_leader.equals(new_leader))
			{
				current_leader.sendPacket(Msg.YOU_CANNOT_TRANSFER_RIGHTS_TO_YOURSELF);
				return;
			}

			if(!_members.contains(new_leader.getStoredId()))
			{
				current_leader.sendPacket(Msg.YOU_CAN_TRANSFER_RIGHTS_ONLY_TO_ANOTHER_PARTY_MEMBER);
				return;
			}

			// меняем местами нового и текущего лидера
			final int idx = _members.indexOf(new_leader.getStoredId());
			_members.set(0, new_leader.getStoredId());
			_members.set(idx, current_leader.getStoredId());

			updateLeaderInfo();

			// при смене пати лидера должен меняться лидер СС
			if(isInCommandChannel() && _commandChannel.getChannelLeader() == current_leader)
				_commandChannel.setChannelLeader(new_leader);
		}
		finally
		{
			_writeLock.unlock();
		}
	}

	private final void updateLeaderInfo()
	{
		final L2Player member = getPartyLeader();
		if(member == null)
			return;

		final SystemMessage msg = new SystemMessage(SystemMessage.S1_HAS_BECOME_A_PARTY_LEADER).addString(member.getName());
		final GArray<L2Player> members = getPartyMembers();
		// индивидуальные пакеты - удаления и инициализация пати
		for(final L2Player pl : members)
			if(pl != null)
				pl.sendPacket(Msg.PartySmallWindowDeleteAll, new PartySmallWindowAll(this, pl), msg); // Удаляем все окошки, показываем окошки, сообщаем о смене лидера

		// броадкасты состояний
		for(final L2Player pl : members)
			if(pl != null)
			{
				broadcastToPartyMembers(pl, new PartySpelled(pl, true)); // Показываем иконки
				if(pl.getPet() != null)
					broadcastToPartyMembers(new ExPartyPetWindowAdd(pl.getPet())); // Показываем окошки петов
			}

		posTask.clear();
	}

	/**
	 * finds a player in the party by name
	 * 
	 * @param name
	 *            имя для поиска
	 * @return найденый L2Player или null если не найдено
	 */
	public L2Player getPlayerByName(final String name)
	{
		L2Player member;
		for(int i = _members.size(); i-- > 0;)
		{
			final Long storedId = _members.getUnsafe(i);
			if(storedId != null && (member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				if(name.equalsIgnoreCase(member.getName()))
					return member;
		}
		return null;
	}

	/**
	 * Oust player from party
	 * 
	 * @param player
	 *            L2Player которого выгоняют
	 */
	public void oustPartyMember(final L2Player player)
	{
		_writeLock.lock();
		try
		{
			if(player == null || !_members.contains(player.getStoredId()))
				return;
			if(_members.size() == 0)
				return;
		}
		finally
		{
			_writeLock.unlock();
		}

		final boolean isLeader = isLeader(player);
		removePartyMember(player);

		if(isLeader && _members.size() > 1)
			updateLeaderInfo();
	}

	/**
	 * Oust player from party Overloaded method that takes player's name as parameter
	 * 
	 * @param name
	 *            имя игрока для изгнания
	 */
	public void oustPartyMember(final String name)
	{
		oustPartyMember(getPlayerByName(name));
	}

	/**
	 * distribute item(s) to party members
	 * 
	 * @param player
	 * @param item
	 */
	public void distributeItem(final L2Player player, final L2ItemInstance item)
	{
		distributeItem(player, item, null);
	}

	public void distributeItem(final L2Player player, final L2ItemInstance item, final L2NpcInstance fromNpc)
	{
		L2Player target = player;

		switch (_itemDistribution)
		{
			case ITEM_RANDOM:
			case ITEM_RANDOM_SPOIL:
				target = getRandomMemberInRange(player, item, Config.ALT_PARTY_DISTRIBUTION_RANGE);
				break;
			case ITEM_ORDER:
			case ITEM_ORDER_SPOIL:
				target = getNextLooterInRange(player, item, Config.ALT_PARTY_DISTRIBUTION_RANGE);
				break;
			case ITEM_LOOTER:
			default:
				target = player;
				break;
		}

		if(target == null)
		{
			item.dropToTheGround(player, fromNpc);
			return;
		}

		if(!target.getInventory().validateWeight(item))
		{
			target.sendActionFailed();
			target.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
			item.dropToTheGround(target, fromNpc);
			return;
		}

		if(!target.getInventory().validateCapacity(item))
		{
			target.sendActionFailed();
			target.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
			item.dropToTheGround(player, fromNpc);
			return;
		}

		// Remove the L2ItemInstance from the world and send server->client GetItem packets
		if(!item.pickupMe(target))
			return;

		target.sendPacket(SystemMessage.obtainItems(item));
		broadcastToPartyMembers(target, SystemMessage.obtainItemsBy(item, target.getName()));

		final L2ItemInstance item2 = target.getInventory().addItem(item);
		Log.LogItem(target, fromNpc, Log.GetItemInPaty, item2);

		target.sendChanges();
	}

	/**
	 * distribute adena to party members
	 * 
	 * @param adena
	 *            инстанс адены для распределения
	 */
	public void distributeAdena(final L2ItemInstance adena, final L2Player player)
	{
		distributeAdena(adena, null, player);
	}

	public void distributeAdena(final L2ItemInstance adena, final L2NpcInstance fromNpc, final L2Player player)
	{
		if(player == null)
			return;

		final int _fromNpc = fromNpc == null ? -1 : fromNpc.getObjectId();
		final GArray<L2Player> members_listInRange = new GArray<L2Player>(MAX_PARTY_MEMBERS);

		_readLock.lock();
		try
		{
			if(adena.getCount() < _members.size())
				members_listInRange.add(player);
			else
			{
				L2Player member;
				for(final long storedId : _members)
				{
					if((member = L2ObjectsStorage.getAsPlayer(storedId)) == null)
						continue;

					if(!member.equals(player) && !player.isInRange(member, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						continue;

					if(member.isDead())
						continue;

					if(member.getReflectionId() != player.getReflectionId())
						continue;

					members_listInRange.addLastUnsafe(member);
				}
			}
		}
		finally
		{
			_readLock.unlock();
		}

		if(members_listInRange.isEmpty())
			members_listInRange.add(player);

		final long totalAdena = adena.getCount();
		final long amount = totalAdena / members_listInRange.size();
		final long ost = totalAdena % members_listInRange.size();

		L2ItemInstance newAdena;
		for(final L2Player member : members_listInRange)
		{
			newAdena = ItemTable.getInstance().createItem(L2Item.ITEM_ID_ADENA, member.getObjectId(), _fromNpc, "L2Party.distributeAdena");
			newAdena.setCount(member.equals(player) ? amount + ost : amount);
			member.sendPacket(SystemMessage.obtainItems(newAdena));
			member.getInventory().addItem(newAdena);
		}

		if(fromNpc == null)
			player.broadcastPacket(new GetItem(adena, player.getObjectId()));

		// удаляем, так как раздаём новые предметы
		adena.deleteMe();
	}

	/**
	 * Distribute Experience and SP rewards to L2Player Party members in the known area of the last attacker.<BR>
	 * <BR>
	 * <B><U> Actions</U> :</B><BR>
	 * <BR>
	 * <li>Get the L2Player owner of the L2SummonInstance (if necessary)</li> <li>Calculate the Experience and SP reward distribution rate</li> <li>Add Experience and SP to the L2Player</li><BR>
	 * <BR>
	 * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method DOESN'T GIVE rewards to L2PetInstance</B></FONT><BR>
	 * <BR>
	 * 
	 * @param xpReward
	 *            The Experience reward to distribute
	 * @param spReward
	 *            The SP reward to distribute
	 * @param rewardedMembers
	 *            The list of L2Player to reward and LSummonInstance whose owner must be reward
	 * @param lastAttacker
	 *            The L2Character that has killed the L2NpcInstance
	 */
	public void distributeXpAndSp(final double xpReward, final double spReward, final GArray<L2Player> rewardedMembers, final L2Character lastAttacker, final L2NpcInstance target, final int partyDmg)
	{
		recalculatePartyData();

		final GArray<L2Player> mtr = new GArray<L2Player>();
		int minPartyLevel = lastAttacker.getLevel();
		int maxPartyLevel = lastAttacker.getLevel();
		double partyLvlSum = 0;

		// создаём список тех кто рядом
		for(final L2Player member : rewardedMembers)
		{
			if(!target.isInRange(member, Config.ALT_PARTY_DISTRIBUTION_RANGE) || target.getReflectionId() != member.getReflectionId())
				continue;

			minPartyLevel = Math.min(minPartyLevel, member.getLevel());
			maxPartyLevel = Math.max(maxPartyLevel, member.getLevel());

			partyLvlSum += member.getLevel();
			mtr.add(member);
		}

		if(maxPartyLevel - minPartyLevel > Config.ALT_PARTY_MAX_LVL_DIFF)
		{
			for(final L2Player member : rewardedMembers)
				if(member.getLevel() < maxPartyLevel - Config.ALT_PARTY_MAX_LVL_DIFF)
					mtr.remove(member);
			partyLvlSum = 0;
			for(final L2Player member : mtr)
				partyLvlSum += member.getLevel();
		}

		if(mtr.size() == 0)
			return;

		// бонус за пати
		final double bonus = BONUS_EXP_SP[mtr.size() - 1];

		// количество эксп и сп для раздачи на всех
		final double XP = xpReward * bonus;
		final double SP = spReward * bonus;

		for(final L2Player member : mtr)
		{
			final double lvlPenalty = Experience.penaltyModifier(target.calculateLevelDiffForDrop(member.getLevel(), false), 9);

			// отдаем его часть с учетом пенальти
			double memberXp = XP * lvlPenalty * member.getLevel() / partyLvlSum;
			double memberSp = SP * lvlPenalty * member.getLevel() / partyLvlSum;

			// больше чем соло не дадут
			memberXp = Math.min(memberXp, xpReward);
			memberSp = Math.min(memberSp, spReward);

			// Начисление душ камаэлянам
			final double neededExp = member.calcStat(Stats.SOULS_CONSUME_EXP, 0, target, null);
			if(neededExp > 0 && memberXp > neededExp)
			{
				target.broadcastPacket(new SpawnEmitter(target, member));
				L2GameThreadPools.getInstance().scheduleGeneral(new L2ObjectTasks.SoulConsumeTask(member), 1000);
			}

			// Выдаём PCBang очки до применения рейтов
			PcCafePointsManager.getInstance().givePcCafePoint(member, (long) memberXp);

			if(Config.ENABLE_VITALITY)
			{
				member.calculateVitalityPoints(target, (long) memberXp);
				// Теперь можно умножить на рейты пати
				memberXp *= _rateExp;
				memberSp *= _rateSp;
				member.addVitExpAndSp((long) memberXp, (long) memberSp, target, false, true);
			}
			else
			{
				// Теперь можно умножить на рейты пати
				memberXp *= _rateExp;
				memberSp *= _rateSp;
				member.addExpAndSp((long) memberXp, (long) memberSp, false, true);
			}
		}

		recalculatePartyData();
	}

	public void recalculatePartyData()
	{
		_partyLvl = 0;
		float rateExp = 0;
		float rateSp = 0;
		float rateDrop = 0;
		float rateAdena = 0;
		float rateSpoil = 0;
		byte count = 0;

		_readLock.lock();
		try
		{
			L2Player member;
			for(final long storedId : _members)
				if((member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				{
					final int level = member.getLevel();
					_partyLvl = Math.max(_partyLvl, level);

					rateExp += member.getBonus().RATE_XP;
					rateSp += member.getBonus().RATE_SP;
					rateDrop += member.getBonus().RATE_DROP_ITEMS;
					rateAdena += member.getBonus().RATE_DROP_ADENA;
					rateSpoil += member.getBonus().RATE_DROP_SPOIL;
					count++;
				}
		}
		finally
		{
			_readLock.unlock();
		}

		_rateExp = rateExp / count * Config.RATE_XP_PARTY;
		_rateSp = rateSp / count * Config.RATE_SP_PARTY;
		_rateDrop = rateDrop / count * Config.RATE_DROP_ITEMS_PARTY;
		_rateAdena = rateAdena / count * Config.RATE_DROP_ADENA_PARTY;
		_rateSpoil = rateSpoil / count;
	}

	public int getLevel()
	{
		return _partyLvl;
	}

	public int getLootDistribution()
	{
		return _itemDistribution;
	}

	public boolean isDistributeSpoilLoot()
	{
		boolean rv = false;

		if(_itemDistribution == ITEM_RANDOM_SPOIL || _itemDistribution == ITEM_ORDER_SPOIL)
			rv = true;

		return rv;
	}

	public boolean isInDimensionalRift()
	{
		return _dr > 0L && getDimensionalRift() != null;
	}

	public void setDimensionalRift(final DimensionalRift dr)
	{
		_dr = dr == null ? 0 : dr.getId();
	}

	public DimensionalRift getDimensionalRift()
	{
		return _dr == 0 ? null : (DimensionalRift) ReflectionTable.getInstance().get(_dr);
	}

	public boolean isInReflection()
	{
		if(_reflection > 0)
			return true;
		if(_commandChannel != null)
			return _commandChannel.isInReflection();
		return false;
	}

	public void setReflection(final Reflection reflection)
	{
		_reflection = reflection == null ? 0 : reflection.getId();
	}

	public Reflection getReflection()
	{
		if(_reflection > 0)
			return ReflectionTable.getInstance().get(_reflection);
		if(_commandChannel != null)
			return _commandChannel.getReflection();
		return null;
	}

	public boolean isInCommandChannel()
	{
		return _commandChannel != null;
	}

	public L2CommandChannel getCommandChannel()
	{
		return _commandChannel;
	}

	public void setCommandChannel(final L2CommandChannel channel)
	{
		_commandChannel = channel;
	}

	/**
	 * Телепорт всей пати в одну точку (x,y,z)
	 */
	public void teleport(final int x, final int y, final int z)
	{
		teleportParty(getPartyMembers(), new Location(x, y, z));
	}

	/**
	 * Телепорт всей пати в одну точку dest
	 */
	public void teleport(final Location dest)
	{
		teleportParty(getPartyMembers(), dest);
	}

	/**
	 * Телепорт всей пати на территорию, игроки расставляются рандомно по территории
	 */
	public void teleport(final L2Territory territory)
	{
		randomTeleportParty(getPartyMembers(), territory);
	}

	/**
	 * Телепорт всей пати на территорию, лидер попадает в точку dest, а все остальные относительно лидера
	 */
	public void teleport(final L2Territory territory, final Location dest)
	{
		teleportParty(getPartyMembers(), territory, dest);
	}

	public static void teleportParty(final GArray<L2Player> members, final Location dest)
	{
		for(final L2Player _member : members)
		{
			if(_member == null)
				continue;
			_member.teleToLocation(dest);
		}
	}

	public static void teleportParty(final GArray<L2Player> members, final L2Territory territory, final Location dest)
	{
		if(!territory.isInside(dest.x, dest.y))
		{
			Log.add("TeleportParty: dest is out of territory", "errors");
			Thread.dumpStack();
			return;
		}
		final int base_x = members.get(0).getX();
		final int base_y = members.get(0).getY();

		for(final L2Player _member : members)
		{
			if(_member == null)
				continue;
			int diff_x = _member.getX() - base_x;
			int diff_y = _member.getY() - base_y;
			final Location loc = new Location(dest.x + diff_x, dest.y + diff_y, dest.z);
			while (!territory.isInside(loc.x, loc.y))
			{
				diff_x = loc.x - dest.x;
				diff_y = loc.y - dest.y;
				if(diff_x != 0)
					loc.x -= diff_x / Math.abs(diff_x);
				if(diff_y != 0)
					loc.y -= diff_y / Math.abs(diff_y);
			}
			_member.teleToLocation(loc);
		}
	}

	public static void randomTeleportParty(final GArray<L2Player> members, final L2Territory territory)
	{
		for(final L2Player _member : members)
		{
			final int[] _loc = territory.getRandomPoint();
			if(_member == null || _loc == null)
				continue;
			_member.teleToLocation(_loc[0], _loc[1], _loc[2]);
		}
	}

	private class UpdatePositionTask implements Runnable
	{
		private final WeakReference<L2Party> party_ref;
		private final HashMap<Integer, int[]> lastpositions = new HashMap<Integer, int[]>();

		public UpdatePositionTask(final L2Party party)
		{
			party_ref = new WeakReference<L2Party>(party);
		}

		public void remove(final L2Player player)
		{
			synchronized (lastpositions)
			{
				lastpositions.remove(player.getObjectId());
			}
		}

		public void clear()
		{
			synchronized (lastpositions)
			{
				lastpositions.clear();
			}
		}

		@Override
		public void run()
		{
			final L2Party party = party_ref.get();
			if(party == null || party.getMemberCount() < 2)
			{
				synchronized (lastpositions)
				{
					lastpositions.clear();
				}
				party_ref.clear();
				dissolveParty();
				return;
			}

			try
			{
				final GArray<L2Player> full_updated = new GArray<L2Player>();
				final GArray<L2Player> members = party.getPartyMembers();
				PartyMemberPosition just_updated = new PartyMemberPosition();

				for(final L2Player member : members)
				{
					if(member == null)
						continue;

					synchronized (lastpositions)
					{
						final int[] lastpos = lastpositions.get(member.getObjectId());
						if(lastpos == null)
						{
							just_updated.add(member);
							full_updated.add(member);
							lastpositions.put(member.getObjectId(), new int[] { member.getX(), member.getY(), member.getZ() });
						}
						else if(member.getDistance(lastpos[0], lastpos[1], lastpos[2]) > 256)
						{
							just_updated.add(member);
							lastpos[0] = member.getX();
							lastpos[1] = member.getY();
							lastpos[2] = member.getZ();
						}
					}
				}

				// посылаем изменения позиций старым членам пати
				if(just_updated.size() > 0)
					for(final L2Player member : members)
						if(!full_updated.contains(member))
							member.sendPacket(just_updated);

				// посылаем полный список позиций новым членам пати
				if(full_updated.size() > 0)
				{
					just_updated = new PartyMemberPosition().add(members);
					for(final L2Player member : full_updated)
						member.sendPacket(just_updated);
					full_updated.clear();
				}
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				if(_posTask != null)
				{
					_posTask.cancel(false);
					_posTask = null;
				}
				_posTask = L2GameThreadPools.getInstance().scheduleGeneral(this, 2000);
			}
		}
	}
}
