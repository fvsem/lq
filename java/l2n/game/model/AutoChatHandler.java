package l2n.game.model;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastList;
import javolution.util.FastMap;
import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcSay;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class AutoChatHandler implements SpawnListener
{
	protected static Logger _log = Logger.getLogger(AutoChatHandler.class.getName());

	private static AutoChatHandler _instance;

	private static final long DEFAULT_CHAT_DELAY = 180000; // 3 mins by default

	private TIntObjectHashMap<AutoChatInstance> _registeredChats;

	protected AutoChatHandler()
	{
		_registeredChats = new TIntObjectHashMap<AutoChatInstance>();
		restoreChatData();
		L2Spawn.addSpawnListener(this);
	}

	private void restoreChatData()
	{
		int numLoaded = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		FiltredPreparedStatement statement2 = null;
		ResultSet rset = null, rset2 = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM auto_chat ORDER BY groupId ASC");
			statement2 = con.prepareStatement("SELECT * FROM auto_chat_text WHERE groupId=?");

			rset = statement.executeQuery();
			while (rset.next())
			{
				numLoaded++;

				statement2.setInt(1, rset.getInt("groupId"));
				rset2 = statement2.executeQuery();

				ArrayList<String> list = new ArrayList<String>();
				while (rset2.next())
					list.add(rset2.getString("chatText"));

				registerGlobalChat(rset.getInt("npcId"), list.toArray(new String[] {}), rset.getLong("chatDelay") * 1000L);
				DbUtils.close(rset2);
			}

			if(Config.DEBUG)
				_log.config("AutoChatHandler: Loaded " + numLoaded + " chat group(s) from the database.");
		}
		catch(Exception e)
		{
			_log.warning("AutoSpawnHandler: Could not restore chat data: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(statement2, rset2);
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static AutoChatHandler getInstance()
	{
		if(_instance == null)
			_instance = new AutoChatHandler();

		return _instance;
	}

	public int size()
	{
		return _registeredChats.size();
	}

	public AutoChatInstance registerGlobalChat(int npcId, String[] chatTexts, long chatDelay)
	{
		return registerChat(npcId, null, chatTexts, chatDelay);
	}

	public AutoChatInstance registerChat(L2NpcInstance npcInst, String[] chatTexts, long chatDelay)
	{
		return registerChat(npcInst.getNpcId(), npcInst, chatTexts, chatDelay);
	}

	private AutoChatInstance registerChat(int npcId, L2NpcInstance npcInst, String[] chatTexts, long chatDelay)
	{
		AutoChatInstance chatInst;

		if(chatDelay < 0)
			chatDelay = DEFAULT_CHAT_DELAY;

		if(_registeredChats.containsKey(npcId))
			chatInst = _registeredChats.get(npcId);
		else
			chatInst = new AutoChatInstance(npcId, chatTexts, chatDelay, npcInst == null);

		if(npcInst != null)
			chatInst.addChatDefinition(npcInst);

		_registeredChats.put(npcId, chatInst);

		return chatInst;
	}

	public boolean removeChat(int npcId)
	{
		AutoChatInstance chatInst = _registeredChats.get(npcId);
		return removeChat(chatInst);
	}

	public boolean removeChat(AutoChatInstance chatInst)
	{
		if(chatInst == null)
			return false;

		_registeredChats.remove(chatInst.getNPCId());
		chatInst.setActive(false);

		if(Config.DEBUG)
			_log.config("AutoChatHandler: Removed auto chat for NPC ID " + chatInst.getNPCId());

		return true;
	}

	public AutoChatInstance getAutoChatInstance(int id, boolean byObjectId)
	{
		if(!byObjectId)
			return _registeredChats.get(id);

		for(AutoChatInstance chatInst : _registeredChats.valueCollection())
			if(chatInst.getChatDefinition(id) != null)
				return chatInst;

		return null;
	}

	public void setAutoChatActive(boolean isActive)
	{
		for(AutoChatInstance chatInst : _registeredChats.valueCollection())
			chatInst.setActive(isActive);
	}

	@Override
	public void npcSpawned(L2NpcInstance npc)
	{
		synchronized (_registeredChats)
		{
			if(npc == null)
				return;

			int npcId = npc.getNpcId();

			if(_registeredChats.containsKey(npcId))
			{
				AutoChatInstance chatInst = _registeredChats.get(npcId);

				if(chatInst != null && chatInst.isGlobal())
					chatInst.addChatDefinition(npc);
			}
		}
	}

	@Override
	public void npcDeSpawned(L2NpcInstance npc)
	{}

	public class AutoChatInstance
	{
		int _npcId;

		private long _defaultDelay = DEFAULT_CHAT_DELAY;
		private String[] _defaultTexts;
		private boolean _defaultRandom = false;
		private boolean _globalChat = false;
		private boolean _isActive;
		private Map<Integer, AutoChatDefinition> _chatDefinitions = new FastMap<Integer, AutoChatDefinition>().shared();
		private ScheduledFuture<AutoChatRunner> _chatTask;

		AutoChatInstance(int npcId, String[] chatTexts, long chatDelay, boolean isGlobal)
		{
			_defaultTexts = chatTexts;
			_npcId = npcId;
			_defaultDelay = chatDelay;
			_globalChat = isGlobal;

			if(Config.DEBUG)
				_log.config("AutoChatHandler: Registered auto chat for NPC ID " + _npcId + " (Global Chat = " + _globalChat + ").");

			setActive(true);
		}

		AutoChatDefinition getChatDefinition(int objectId)
		{
			return _chatDefinitions.get(objectId);
		}

		AutoChatDefinition[] getChatDefinitions()
		{
			return _chatDefinitions.values().toArray(new AutoChatDefinition[_chatDefinitions.values().size()]);
		}

		public int addChatDefinition(L2NpcInstance npcInst)
		{
			return addChatDefinition(npcInst, null, 0);
		}

		public int addChatDefinition(L2NpcInstance npcInst, String[] chatTexts, long chatDelay)
		{
			int objectId = npcInst.getObjectId();
			AutoChatDefinition chatDef = new AutoChatDefinition(this, npcInst, chatTexts, chatDelay);

			_chatDefinitions.put(objectId, chatDef);
			return objectId;
		}

		public boolean removeChatDefinition(int objectId)
		{
			if(!_chatDefinitions.containsKey(objectId))
				return false;

			AutoChatDefinition chatDefinition = _chatDefinitions.get(objectId);
			chatDefinition.setActive(false);

			_chatDefinitions.remove(objectId);

			return true;
		}

		public boolean isActive()
		{
			return _isActive;
		}

		public boolean isGlobal()
		{
			return _globalChat;
		}

		public boolean isDefaultRandom()
		{
			return _defaultRandom;
		}

		public boolean isRandomChat(int objectId)
		{
			if(!_chatDefinitions.containsKey(objectId))
				return false;

			return _chatDefinitions.get(objectId).isRandomChat();
		}

		public int getNPCId()
		{
			return _npcId;
		}

		public int getDefinitionCount()
		{
			return _chatDefinitions.size();
		}

		public L2NpcInstance[] getNPCInstanceList()
		{
			List<L2NpcInstance> npcInsts = new FastList<L2NpcInstance>();

			for(AutoChatDefinition chatDefinition : _chatDefinitions.values())
				npcInsts.add(chatDefinition._npcInstance);

			return npcInsts.toArray(new L2NpcInstance[npcInsts.size()]);
		}

		public long getDefaultDelay()
		{
			return _defaultDelay;
		}

		public String[] getDefaultTexts()
		{
			return _defaultTexts;
		}

		public void setDefaultChatDelay(long delayValue)
		{
			_defaultDelay = delayValue;
		}

		public void setDefaultChatTexts(String[] textsValue)
		{
			_defaultTexts = textsValue;
		}

		public void setDefaultRandom(boolean randValue)
		{
			_defaultRandom = randValue;
		}

		public void setChatDelay(int objectId, long delayValue)
		{
			AutoChatDefinition chatDef = getChatDefinition(objectId);

			if(chatDef != null)
				chatDef.setChatDelay(delayValue);
		}

		public void setChatTexts(int objectId, String[] textsValue)
		{
			AutoChatDefinition chatDef = getChatDefinition(objectId);

			if(chatDef != null)
				chatDef.setChatTexts(textsValue);
		}

		public void setRandomChat(int objectId, boolean randValue)
		{
			AutoChatDefinition chatDef = getChatDefinition(objectId);

			if(chatDef != null)
				chatDef.setRandomChat(randValue);
		}

		public void setActive(boolean activeValue)
		{
			if(_isActive == activeValue)
				return;

			_isActive = activeValue;

			if(!isGlobal())
			{
				for(AutoChatDefinition chatDefinition : _chatDefinitions.values())
					chatDefinition.setActive(activeValue);

				return;
			}

			if(isActive())
			{
				AutoChatRunner acr = new AutoChatRunner(_npcId, -1);
				_chatTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(acr, _defaultDelay, _defaultDelay);
			}
			else
				_chatTask.cancel(false);
		}

		private class AutoChatDefinition
		{
			protected int _chatIndex = 0;
			protected L2NpcInstance _npcInstance;
			protected AutoChatInstance _chatInstance;
			protected ScheduledFuture<AutoChatRunner> _chatTask;
			private long _chatDelay = 0;
			private String[] _chatTexts = null;
			private boolean _isActive;
			private boolean _randomChat;

			protected AutoChatDefinition(AutoChatInstance chatInst, L2NpcInstance npcInst, String[] chatTexts, long chatDelay)
			{
				_npcInstance = npcInst;

				_chatInstance = chatInst;
				_randomChat = chatInst.isDefaultRandom();

				_chatDelay = chatDelay;
				_chatTexts = chatTexts;

				if(Config.DEBUG)
					_log.info("AutoChatHandler: Chat definition added for NPC ID " + _npcInstance.getNpcId() + " (Object ID = " + _npcInstance.getObjectId() + ").");

				if(!chatInst.isGlobal())
					setActive(true);
			}

			String[] getChatTexts()
			{
				if(_chatTexts != null)
					return _chatTexts;
				return _chatInstance.getDefaultTexts();
			}

			private long getChatDelay()
			{
				if(_chatDelay > 0)
					return _chatDelay;
				return _chatInstance.getDefaultDelay();
			}

			private boolean isActive()
			{
				return _isActive;
			}

			boolean isRandomChat()
			{
				return _randomChat;
			}

			void setRandomChat(boolean randValue)
			{
				_randomChat = randValue;
			}

			void setChatDelay(long delayValue)
			{
				_chatDelay = delayValue;
			}

			void setChatTexts(String[] textsValue)
			{
				_chatTexts = textsValue;
			}

			void setActive(boolean activeValue)
			{
				if(isActive() == activeValue)
					return;

				if(activeValue)
				{
					AutoChatRunner acr = new AutoChatRunner(_npcId, _npcInstance.getObjectId());
					_chatTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(acr, getChatDelay(), getChatDelay());
				}
				else
					_chatTask.cancel(false);

				_isActive = activeValue;
			}
		}

		public class AutoChatRunner implements Runnable
		{
			private int _npcId;

			private int _objectId;

			protected AutoChatRunner(int npcId, int objectId)
			{
				_npcId = npcId;
				_objectId = objectId;
			}

			@Override
			public synchronized void run()
			{
				AutoChatInstance chatInst = _registeredChats.get(_npcId);
				AutoChatDefinition[] chatDefinitions;

				if(chatInst.isGlobal())
					chatDefinitions = chatInst.getChatDefinitions();
				else
				{
					AutoChatDefinition chatDef = chatInst.getChatDefinition(_objectId);

					if(chatDef == null)
					{
						_log.warning("AutoChatHandler: Auto chat definition is NULL for NPC ID " + _npcId + ".");
						return;
					}

					chatDefinitions = new AutoChatDefinition[] { chatDef };
				}

				if(Config.DEBUG)
					_log.info("AutoChatHandler: Running auto chat for " + chatDefinitions.length + " instances of NPC ID " + _npcId + "." + " (Global Chat = " + chatInst.isGlobal() + ")");

				for(AutoChatDefinition chatDef : chatDefinitions)
					try
					{
						if(chatDef == null)
							continue;

						L2NpcInstance chatNpc = chatDef._npcInstance;
						List<L2Player> nearbyPlayers = new FastList<L2Player>();

						for(L2Player pl : chatNpc.getAroundPlayers(1500))
							nearbyPlayers.add(pl);

						int maxIndex = chatDef.getChatTexts().length;
						int lastIndex = Rnd.get(maxIndex);

						String text;

						if(!chatDef.isRandomChat())
						{
							lastIndex = chatDef._chatIndex;
							lastIndex++;

							if(lastIndex == maxIndex)
								lastIndex = 0;

							chatDef._chatIndex = lastIndex;
						}

						text = chatDef.getChatTexts()[lastIndex];
						if(text == null)
							return;

						if(!nearbyPlayers.isEmpty())
						{
							L2Player randomPlayer = nearbyPlayers.get(Rnd.get(nearbyPlayers.size()));

							final int winningCabal = SevenSigns.getInstance().getCabalHighestScore();
							int losingCabal = SevenSigns.CABAL_NULL;

							if(winningCabal == SevenSigns.CABAL_DAWN)
								losingCabal = SevenSigns.CABAL_DUSK;
							else if(winningCabal == SevenSigns.CABAL_DUSK)
								losingCabal = SevenSigns.CABAL_DAWN;

							if(text.indexOf("%player_random%") > -1)
								text = text.replaceAll("%player_random%", randomPlayer.getName());

							if(text.indexOf("%player_cabal_winner%") > -1)
							{
								boolean playerFound = false;

								for(L2Player nearbyPlayer : nearbyPlayers)
								{
									if(nearbyPlayer == null)
										continue;
									if(SevenSigns.getInstance().getPlayerCabal(nearbyPlayer) == winningCabal)
									{
										text = text.replaceAll("%player_cabal_winner%", nearbyPlayer.getName());
										playerFound = true;
										break;
									}
								}

								// If a player on the winning side isn't nearby,
								// just use the randomly selected player.
								if(!playerFound)
									text = "";// text =
								// text.replaceAll("%player_cabal_winner%",
								// randomPlayer.getName());
							}

							if(text.indexOf("%player_cabal_loser%") > -1)
							{
								boolean playerFound = false;

								for(L2Player nearbyPlayer : nearbyPlayers)
								{
									if(nearbyPlayer == null)
										continue;
									if(SevenSigns.getInstance().getPlayerCabal(nearbyPlayer) == losingCabal)
									{
										text = text.replaceAll("%player_cabal_loser%", nearbyPlayer.getName());
										playerFound = true;
										break;
									}
								}

								if(!playerFound)
									text = "";// text =

							}
						}

						NpcSay cs = null;
						if(text != null && !text.equals(""))
							cs = new NpcSay(chatNpc, 0, text);

						if(cs != null)
							for(L2Player nearbyPlayer : nearbyPlayers)
							{
								if(nearbyPlayer == null)
									continue;
								nearbyPlayer.sendPacket(cs);
							}

						if(Config.DEBUG)
							_log.fine("AutoChatHandler: Chat propogation for object ID " + chatNpc.getObjectId() + " (" + chatNpc.getName() + ") with text '" + text + "' sent to " + nearbyPlayers.size() + " nearby players.");
					}
					catch(Exception e)
					{
						e.printStackTrace();
						return;
					}
			}
		}
	}
}
