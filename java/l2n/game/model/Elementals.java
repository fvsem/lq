package l2n.game.model;

public final class Elementals
{
	public static final byte ATTRIBUTE_NONE = -2;

	public static final byte ATTRIBUTE_FIRE = 0;

	public static final byte ATTRIBUTE_WATER = 1;

	public static final byte ATTRIBUTE_WIND = 2;

	public static final byte ATTRIBUTE_EARTH = 3;

	public static final byte ATTRIBUTE_SACRED = 4;

	public static final byte ATTRIBUTE_UNHOLY = 5;

	public final static int[] WEAPON_VALUES =
	{
			0, // Level 1
			25, // Level 2
			75, // Level 3
			150, // Level 4
			175, // Level 5
			225, // Level 6
			300, // Level 7
			325,//  Level 8
			375,//  Level 9
			450,//  Level 10
			475,//  Level 11
			525,// Level 12
			600,// Level 13
			Integer.MAX_VALUE // TODO: Higher stones
	};

	public final static int[] ARMOR_VALUES =
	{
			0, // Level 1
			12, // Level 2
			30, // Level 3
			60, // Level 4
			72, // Level 5
			90, // Level 6
			120, // Level 7
			132,// Level 8
			150,//Level 9
			180,// Level 10
			192,// Level 11
			210,// Level 12
			240,// Level 13
			Integer.MAX_VALUE // TODO: Higher stones
	};

	public final static int[] STONES = {
			9546, // Fire Stone
			9547, // Water Stone
			9549, // Wind Stone
			9548, // Earth Stone
			9551, // Divine Stone
			9550 // Dark Stone
	};
	public final static int[] ROUGHORES = {
			10521, // Rough Ore of Fire
			10522, // Rough Ore of Water
			10524, // Rough Ore of Wind
			10523, // Rough Ore of the Earth
			10526, // Rough Ore of Divinity
			10525 // Rough Ore of Darkness
	};

	public final static int[] CRYSTALS = {
			9552, // Fire Crystal
			9553, // Water Crystal
			9555, // Wind Crystal
			9554, // Earth Crystal
			9557, // Divine Crystal
			9556 // Dark Crystal
	};

	public final static int[] JEWELS = {
			9558, // Fire Jewel
			9559, // Water Jewel
			9561, // Wind Jewel
			9560, // Earth Jewel
			9563, // Divine Jewel
			9562 // Dark Jewel
	};

	public final static int[] ENERGIES = {
			9564, // Fire Energy
			9565, // Water Energy
			9567, // Wind Energy
			9566, // Earth Energy
			9569, // Divine Energy
			9568 // Dark Energy
	};

	public static final int[] ALL_MODIFERS = {
			9546, 9547, 9549, 9548, 9551, 9550,// STONES
			9552,
			9553,
			9555,
			9554,
			9557,
			9556,// CRYSTALS
			9558,
			9559,
			9561,
			9560,
			9563,
			9562,// JEWELS
			9564,
			9565,
			9567,
			9566,
			9569,
			9568,// ENERGIES
			10521,
			10522,
			10524,
			10523,
			10525,
			10526 // ROUGHORES
	};

	private byte _element = ATTRIBUTE_NONE;
	private int _value = 0;

	public byte getElement()
	{
		return _element;
	}

	public void setElement(final byte type)
	{
		_element = type;
	}

	public int getValue()
	{
		return _value;
	}

	public void setValue(final int val)
	{
		_value = val;
	}

	public static String getElementName(final byte element)
	{
		switch (element)
		{
			case ATTRIBUTE_FIRE:
				return "Fire";
			case ATTRIBUTE_WATER:
				return "Water";
			case ATTRIBUTE_WIND:
				return "Wind";
			case ATTRIBUTE_EARTH:
				return "Earth";
			case ATTRIBUTE_UNHOLY:
				return "Unholy";
			case ATTRIBUTE_SACRED:
				return "Sacred";
		}
		return "None";
	}

	public static byte getElementId(final String name)
	{
		final String tmp = name.toLowerCase();
		if(tmp.equals("fire"))
			return ATTRIBUTE_FIRE;
		if(tmp.equals("water"))
			return ATTRIBUTE_WATER;
		if(tmp.equals("wind"))
			return ATTRIBUTE_WIND;
		if(tmp.equals("earth"))
			return ATTRIBUTE_EARTH;
		if(tmp.equals("unholy"))
			return ATTRIBUTE_UNHOLY;
		if(tmp.equals("sacred"))
			return ATTRIBUTE_SACRED;
		return ATTRIBUTE_NONE;
	}

	public static byte getOppositeElement(final byte element)
	{
		return (byte) (element % 2 == 0 ? element + 1 : element - 1);
	}

	@Override
	public String toString()
	{
		return getElementName(_element) + " +" + _value;
	}

	public Elementals(final byte type, final int value)
	{
		_element = type;
		_value = value;
	}

	public static byte getEnchantAttributeStoneElement(final int itemId)
	{
		switch (itemId)
		{
			case 9546:
			case 9552:
			case 9558:
			case 9564:
			case 10521:
				return ATTRIBUTE_FIRE;
			case 9547:
			case 9553:
			case 9559:
			case 9565:
			case 10522:
				return ATTRIBUTE_WATER;
			case 9549:
			case 9555:
			case 9561:
			case 9567:
			case 10524:
				return ATTRIBUTE_WIND;
			case 9548:
			case 9554:
			case 9560:
			case 9566:
			case 10523:
				return ATTRIBUTE_EARTH;
			case 9551:
			case 9557:
			case 9563:
			case 9569:
			case 10526:
				return ATTRIBUTE_SACRED;
			case 9550:
			case 9556:
			case 9562:
			case 9568:
			case 10525:
				return ATTRIBUTE_UNHOLY;
			default:
				return ATTRIBUTE_NONE;
		}
	}
}
