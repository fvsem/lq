package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.tables.NpcTable;
import l2n.game.tables.TerritoryTable;
import l2n.game.taskmanager.SpawnTaskManager;
import l2n.game.taskmanager.SpawnTaskManager.SpawnTask;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2Spawn implements Cloneable
{
	private final static Logger _log = Logger.getLogger(L2Spawn.class.getName());

	private static final int MIN_RESPAWN_DELAY = 20;

	private static final IntArrayList locations_error = new IntArrayList();

	private final int _npcId;

	private int _id;

	private int _location;

	private int _maximumCount;

	private int _referenceCount;

	private int _currentCount;

	protected int _scheduledCount;

	private int _locx;

	private int _locy;

	private int _locz;

	private int _heading;

	private int _respawnDelay;
	private int _respawnDelayRandom;
	private int _nativeRespawnDelay = 0;

	private int _respawnTime;

	private boolean _doRespawn;

	private L2NpcInstance _lastSpawn;
	private static final GArray<SpawnListener> _spawnListeners = new GArray<SpawnListener>();

	private final GArray<L2NpcInstance> _spawned;
	private int _siegeId;
	private long _reflection;

	protected Future<SpawnTask> _spawnTask;

	public L2Spawn(final L2NpcTemplate mobTemplate) throws ClassNotFoundException
	{
		this(mobTemplate.getNpcId());
	}

	public L2Spawn(final int npcId) throws ClassNotFoundException
	{
		_npcId = npcId;
		final L2NpcTemplate mobTemplate = NpcTable.getTemplate(npcId);
		if(mobTemplate == null || mobTemplate.getInstanceConstructor() == null)
			throw new ClassNotFoundException("Unable to instantiate npc " + npcId);
		_spawned = new GArray<L2NpcInstance>(1);
	}

	public int getAmount()
	{
		return _maximumCount;
	}

	public int getSpawnedCount()
	{
		return _currentCount;
	}
	
	public int getSheduledCount()
	{
		return _scheduledCount;
	}

	public int getId()
	{
		return _id;
	}

	public int getLocation()
	{
		return _location;
	}

	public Location getLoc()
	{
		return new Location(_locx, _locy, _locz);
	}

	public int getLocx()
	{
		return _locx;
	}

	public int getLocy()
	{
		return _locy;
	}

	public int getLocz()
	{
		return _locz;
	}

	public int getNpcId()
	{
		return _npcId;
	}

	public L2NpcTemplate getTemplate()
	{
		return NpcTable.getTemplate(_npcId);
	}

	public int getHeading()
	{
		return _heading;
	}

	public int getRespawnDelay()
	{
		return _respawnDelay;
	}

	public int getNativeRespawnDelay()
	{
		return _nativeRespawnDelay;
	}

	public int getRespawnDelayRandom()
	{
		return _respawnDelayRandom;
	}

	public int getRespawnDelayWithRnd()
	{
		return _respawnDelayRandom == 0 ? _respawnDelay : Rnd.get(_respawnDelay - _respawnDelayRandom, _respawnDelay + _respawnDelayRandom);
	}

	public int getRespawnTime()
	{
		return _respawnTime;
	}

	public void setAmount(final int amount)
	{
		if(_referenceCount == 0)
			_referenceCount = amount;
		_maximumCount = amount;
	}

	public void restoreAmount()
	{
		_maximumCount = _referenceCount;
	}

	public void setId(final int id)
	{
		_id = id;
	}

	public void setLocation(final int location)
	{
		_location = location;
	}

	public void setLoc(final Location loc)
	{
		_locx = loc.x;
		_locy = loc.y;
		_locz = loc.z;
		_heading = loc.h;
	}

	public void setLocx(final int locx)
	{
		_locx = locx;
	}

	public void setLocy(final int locy)
	{
		_locy = locy;
	}

	public void setLocz(final int locz)
	{
		_locz = locz;
	}

	public void setHeading(final int heading)
	{
		_heading = heading;
	}

	public void decreaseCount(final L2NpcInstance oldNpc)
	{
		_currentCount--;

		if(_currentCount < 0)
			_currentCount = 0;

		notifyNpcDeSpawned(oldNpc);

		if(_doRespawn && _scheduledCount + _currentCount < _maximumCount)
		{
			_scheduledCount++;


			long delay = getRespawnDelayWithRnd() * 1000L;
			delay = Math.max(1000, delay - oldNpc.getDeadTime());

			_respawnTime = (int) ((System.currentTimeMillis() + delay) / 1000L);

			addSpawnTask(oldNpc, delay);
		}
	}

	public int init()
	{
		while (_currentCount + _scheduledCount < _maximumCount)
			doSpawn(false);
		_doRespawn = true;
		return _currentCount;
	}

	public L2NpcInstance spawnOne()
	{
		return doSpawn(false);
	}

	public void despawnAll()
	{
		stopRespawn();
		_spawned.forEach(L2ObjectProcedures.PROC_DELETE_NPC);
		_currentCount = 0;
	}

	public void stopRespawn()
	{
		_doRespawn = false;
	}

	public void startRespawn()
	{
		_doRespawn = true;
	}

	public boolean isDoRespawn()
	{
		return _doRespawn;
	}

	public L2NpcInstance doSpawn(boolean spawn)
	{
		try
		{
			final L2NpcTemplate template = NpcTable.getTemplate(_npcId);
			if(template.isInstanceOf(L2PetInstance.class) || template.isInstanceOf(L2MinionInstance.class))
			{
				_currentCount++;
				return null;
			}

			final L2NpcInstance tmp = template.getNewInstance();
			if(!spawn)
				spawn = _respawnTime <= System.currentTimeMillis() / 1000 + MIN_RESPAWN_DELAY;

			_spawned.add(tmp);

			return intializeNpc(tmp, spawn);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "NPC " + _npcId + " class not found", e);
		}

		return null;
	}

	public GArray<L2NpcInstance> getAllSpawned()
	{
		return _spawned;
	}

	public GArray<L2NpcInstance> getAllLiveSpawned()
	{
		final GArray<L2NpcInstance> live = new GArray<L2NpcInstance>(_spawned.size());
		for(final L2NpcInstance npc : _spawned)
			if(npc != null && !npc.isDead())
				live.addLastUnsafe(npc);
		return live;
	}

	private L2NpcInstance intializeNpc(final L2NpcInstance mob, final boolean spawn)
	{
		Location newLoc;

		if(getLocation() != 0)
		{
			final int p[] = TerritoryTable.getInstance().getRandomPoint(getLocation());
			if(p[0] == 0 && p[1] == 0 && p[2] == 0 && !locations_error.contains(getLocation()))
			{
				locations_error.add(getLocation());
				Log.addDev("territory " + getLocation() + " not found for npc " + mob.getNpcId(), "dev_locations_error", false);
			}

			newLoc = new Location(p[0], p[1], p[2], Rnd.get(0xFFFF));
		}
		else
		{
			newLoc = getLoc();

			newLoc.h = getHeading() == -1 ? Rnd.get(0xFFFF) : getHeading();
		}

		mob.getEffectList().stopAllEffects();

		mob.setCurrentHpMp(mob.getMaxHp(), mob.getMaxMp(), true);

		mob.setSpawn(this);

		mob.setHeading(newLoc.h);

		mob.setSpawnedLoc(newLoc);

		mob.setReflection(getReflection());

		if(spawn)
		{
			mob.onSpawn();

			mob.spawnMe(newLoc);

			notifyNpcSpawned(mob);
			_currentCount++;
		}
		else
		{
			mob.setXYZInvisible(newLoc);

			_scheduledCount++;

			addSpawnTask(mob, _respawnTime * 1000L - System.currentTimeMillis());
		}

		_lastSpawn = mob;
		return mob;
	}

	private void addSpawnTask(final L2NpcInstance actor, final long interval)
	{
		SpawnTaskManager.getInstance().addSpawnTask(actor, interval);
	}

	public static void addSpawnListener(final SpawnListener listener)
	{
		synchronized (_spawnListeners)
		{
			_spawnListeners.add(listener);
		}
	}

	public static void removeSpawnListener(final SpawnListener listener)
	{
		synchronized (_spawnListeners)
		{
			_spawnListeners.remove(listener);
		}
	}

	public static void notifyNpcSpawned(final L2NpcInstance npc)
	{
		synchronized (_spawnListeners)
		{
			for(final SpawnListener listener : _spawnListeners)
				listener.npcSpawned(npc);
		}
	}

	public static void notifyNpcDeSpawned(final L2NpcInstance npc)
	{
		synchronized (_spawnListeners)
		{
			for(final SpawnListener listener : _spawnListeners)
				listener.npcDeSpawned(npc);
		}
	}

	public void setRespawnDelay(final int respawnDelay)
	{
		setRespawnDelay(respawnDelay, 0);
	}

	public void setRespawnDelay(final int respawnDelay, final int respawnDelayRandom)
	{
		if(respawnDelay < 0)
			_log.warning("respawn delay is negative for spawnId:" + _id);

		_nativeRespawnDelay = respawnDelay;
		_respawnDelay = respawnDelay > MIN_RESPAWN_DELAY ? respawnDelay : MIN_RESPAWN_DELAY;
		_respawnDelayRandom = respawnDelayRandom > 0 ? respawnDelayRandom : 0;
	}

	public void setRespawnTime(final int respawnTime)
	{
		_respawnTime = respawnTime;
	}

	public L2NpcInstance getLastSpawn()
	{
		return _lastSpawn;
	}

	public void respawnNpc(final L2NpcInstance oldNpc)
	{
		oldNpc.refreshID();
		intializeNpc(oldNpc, true);
	}

	public void setSiegeId(final int id)
	{
		_siegeId = id;
	}

	public int getSiegeId()
	{
		return _siegeId;
	}

	public void setReflection(final long reflection)
	{
		_reflection = reflection;
	}

	public void decreaseScheduledCount()
	{
		if(_scheduledCount > 0)
			_scheduledCount--;
	}

	public long getReflection()
	{
		return _reflection;
	}

	@Override
	public L2Spawn clone()
	{
		L2Spawn spawnDat = null;
		try
		{
			spawnDat = new L2Spawn(getTemplate());
			spawnDat.setLocation(_location);
			spawnDat.setLocx(_locx);
			spawnDat.setLocy(_locy);
			spawnDat.setLocz(_locz);
			spawnDat.setHeading(_heading);
			spawnDat.setAmount(_maximumCount);
			spawnDat.setRespawnDelay(_respawnDelay, _respawnDelayRandom);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		return spawnDat;
	}
}
