package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.tables.SkillTable;

public final class L2ArmorSet
{
	private final int _chest;
	private final int _legs;
	private final int _head;
	private final int _gloves;
	private final int _feet;
	private final int _mw_legs;
	private final int _mw_head;
	private final int _mw_gloves;
	private final int _mw_feet;

	private final L2Skill[] _skills;
	private final L2Skill _shieldSkill;
	private final L2Skill _enchant6Skill;

	private final int _shield;
	private final int _mw_shield;

	public L2ArmorSet(int chest, int legs, int head, int gloves, int feet, String[] skills, int shield, L2Skill shield_skill, L2Skill enchant6skill, int mw_legs, int mw_head, int mw_gloves, int mw_feet, int mw_shield)
	{
		_chest = chest;
		_legs = legs;
		_head = head;
		_gloves = gloves;
		_feet = feet;
		_mw_legs = mw_legs;
		_mw_head = mw_head;
		_mw_gloves = mw_gloves;
		_mw_feet = mw_feet;
		_mw_shield = mw_shield;

		final GArray<L2Skill> list = new GArray<L2Skill>();

		for(String skillInfo : skills)
		{
			if(skillInfo.isEmpty())
				continue;

			final String[] skill = skillInfo.split("-");
			if(skill.length == 2)
			{
				final int skillId = Integer.parseInt(skill[0]);
				final int skillLvl = Integer.parseInt(skill[1]);
				if(skillId > 0 && skillLvl > 0)
				{
					final L2Skill sk = SkillTable.getInstance().getInfo(skillId, skillLvl);

					if(sk != null)
						list.add(sk);
					else
						throw new IllegalStateException("[59] Invalid 'skill " + skillId + ":" + skillLvl + "' parameters for armorset ID " + _chest);
				}
				else if(skillId != 0 || skillLvl != 0)
					throw new IllegalStateException("[62] Invalid 'skill" + skillId + ":" + skillLvl + "' parameters for armorset ID " + _chest);
			}
			else
				throw new IllegalStateException("[65] Invalid 'skill" + skill.toString() + "' parameters for armorset ID " + _chest);
		}

		_skills = list.toArray(new L2Skill[list.size()]);

		_shield = shield;

		_shieldSkill = shield_skill;
		_enchant6Skill = enchant6skill;
	}

	public boolean containAll(L2Player player)
	{
		Inventory inv = player.getInventory();

		L2ItemInstance legsItem = inv.getPaperdollItem(Inventory.PAPERDOLL_LEGS);
		L2ItemInstance headItem = inv.getPaperdollItem(Inventory.PAPERDOLL_HEAD);
		L2ItemInstance glovesItem = inv.getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
		L2ItemInstance feetItem = inv.getPaperdollItem(Inventory.PAPERDOLL_FEET);

		int legs = 0;
		int head = 0;
		int gloves = 0;
		int feet = 0;

		if(legsItem != null)
			legs = legsItem.getItemId();
		if(headItem != null)
			head = headItem.getItemId();
		if(glovesItem != null)
			gloves = glovesItem.getItemId();
		if(feetItem != null)
			feet = feetItem.getItemId();

		return containAll(_chest, legs, head, gloves, feet);
	}

	public boolean containAll(int chest, int legs, int head, int gloves, int feet)
	{
		if(_chest != 0 && _chest != chest)
			return false;

		if(_legs != 0 && _legs != legs && (_mw_legs == 0 || _mw_legs != legs))
			return false;

		if(_head != 0 && _head != head && (_mw_head == 0 || _mw_head != head))
			return false;

		if(_gloves != 0 && _gloves != gloves && (_mw_gloves == 0 || _mw_gloves != gloves))
			return false;

		if(_feet != 0 && _feet != feet && (_mw_feet == 0 || _mw_feet != feet))
			return false;

		return true;
	}

	public boolean containItem(int slot, int itemId)
	{
		switch (slot)
		{
			case Inventory.PAPERDOLL_CHEST:
				return _chest == itemId;
			case Inventory.PAPERDOLL_LEGS:
				return _legs == itemId || _mw_legs == itemId;
			case Inventory.PAPERDOLL_HEAD:
				return _head == itemId || _mw_head == itemId;
			case Inventory.PAPERDOLL_GLOVES:
				return _gloves == itemId || _mw_gloves == itemId;
			case Inventory.PAPERDOLL_FEET:
				return _feet == itemId || _mw_feet == itemId;
			default:
				return false;
		}
	}

	public L2Skill[] getSkills()
	{
		return _skills;
	}

	public boolean containShield(L2Player player)
	{
		Inventory inv = player.getInventory();

		L2ItemInstance shieldItem = inv.getPaperdollItem(Inventory.PAPERDOLL_LHAND);
		if(shieldItem != null && (shieldItem.getItemId() == _shield || shieldItem.getItemId() == _mw_shield))
			return true;

		return false;
	}

	public boolean containShield(int shield_id)
	{
		if(_shield == 0)
			return false;

		return _shield == shield_id || _mw_shield == shield_id;
	}

	public L2Skill getShieldSkill()
	{
		return _shieldSkill;
	}

	public L2Skill getEnchant6skill()
	{
		return _enchant6Skill;
	}

	public boolean isEnchanted6(L2Player player)
	{
		if(!containAll(player))
			return false;

		Inventory inv = player.getInventory();

		L2ItemInstance chestItem = inv.getPaperdollItem(Inventory.PAPERDOLL_CHEST);
		L2ItemInstance legsItem = inv.getPaperdollItem(Inventory.PAPERDOLL_LEGS);
		L2ItemInstance headItem = inv.getPaperdollItem(Inventory.PAPERDOLL_HEAD);
		L2ItemInstance glovesItem = inv.getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
		L2ItemInstance feetItem = inv.getPaperdollItem(Inventory.PAPERDOLL_FEET);

		if(chestItem == null || chestItem.getEnchantLevel() < 6)
			return false;

		if(_legs != 0 && (legsItem == null || legsItem.getEnchantLevel() < 6))
			return false;

		if(_gloves != 0 && (glovesItem == null || glovesItem.getEnchantLevel() < 6))
			return false;

		if(_head != 0 && (headItem == null || headItem.getEnchantLevel() < 6))
			return false;

		if(_feet != 0 && (feetItem == null || feetItem.getEnchantLevel() < 6))
			return false;

		return true;
	}
}
