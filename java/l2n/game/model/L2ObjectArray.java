package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.game.L2GameThreadPools;

import javax.realtime.MemoryArea;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

public class L2ObjectArray<E extends L2Object> implements Iterable<E>
{
	private static final Logger _log = Logger.getLogger(L2ObjectsStorage.class.getName());

	public final String name;
	public final int resizeStep, initCapacity;
	private E[] elementData;
	private int size = 0;
	private int real_size = 0;

	private final IntArrayList freeIndexes;

	private final ReentrantReadWriteLock _lock;
	private final Lock _read_lock;
	private final Lock _write_lock;

	@SuppressWarnings("unchecked")
	public L2ObjectArray(final String _name, final int initialCapacity, final int _resizeStep)
	{
		name = _name;
		resizeStep = _resizeStep;
		initCapacity = initialCapacity;

		_lock = new ReentrantReadWriteLock();
		_read_lock = _lock.readLock();
		_write_lock = _lock.writeLock();

		if(initialCapacity < 0)
			throw new IllegalArgumentException("Illegal Capacity (" + name + "): " + initialCapacity);
		if(resizeStep < 1)
			throw new IllegalArgumentException("Illegal resize step (" + name + "): " + resizeStep);

		freeIndexes = new IntArrayList(resizeStep, -1);
		elementData = (E[]) new L2Object[initialCapacity];
	}

	public int size()
	{
		return size;
	}

	public int getRealSize()
	{
		return real_size;
	}

	public int capacity()
	{
		return elementData.length;
	}

	@SuppressWarnings("unchecked")
	public int add(final E e)
	{
		_write_lock.lock();
		try
		{
			final int freeIndex = freeIndexes.removeLast();
			if(freeIndex > -1)
			{
				real_size++;
				elementData[freeIndex] = e;
				return freeIndex;
			}

			if(elementData.length <= size)
			{
				MemoryArea.getMemoryArea(this).executeInArea(new Runnable()
				{
					@Override
					public void run()
					{
						final int newCapacity = elementData.length + resizeStep;
						_log.warning("Object array [" + name + "] resized: " + elementData.length + "/" + size + " -> " + newCapacity);

						final E[] values = (E[]) new L2Object[newCapacity];
						System.arraycopy(elementData, 0, values, 0, elementData.length);
						elementData = values;
					}
				});
			}

			elementData[size++] = e;
			real_size++;
			return size - 1;
		}
		finally
		{
			_write_lock.unlock();
		}
	}

	public E remove(final int index, final int expectedObjId)
	{
		_write_lock.lock();
		try
		{
			if(index >= size)
				return null;

			E old;
			if((old = elementData[index]) == null || old.getObjectId() != expectedObjId)
				return null;

			elementData[index] = null;
			real_size--;

			if(index == size - 1)
				size--;
			else
				freeIndexes.add(index);
			return old;
		}
		finally
		{
			_write_lock.unlock();
		}
	}

	public E get(final int index)
	{
		return index >= size ? null : elementData[index];
	}

	public E findByObjectId(final int objId)
	{
		if(objId <= 0)
			return null;
		_read_lock.lock();
		try
		{
			E o;
			for(int i = 0; i < size; i++)
				if((o = elementData[i]) != null && o.getObjectId() == objId)
					return o;
		}
		finally
		{
			_read_lock.unlock();
		}
		return null;
	}

	public E findByName(final String s)
	{
		_read_lock.lock();
		try
		{
			E o;
			for(int i = 0; i < size; i++)
				if((o = elementData[i]) != null && s.equalsIgnoreCase(o.getName()))
					return o;
		}
		finally
		{
			_read_lock.unlock();
		}
		return null;
	}

	public GArray<E> findAllByName(final String s)
	{
		final GArray<E> result = new GArray<E>();
		_read_lock.lock();
		try
		{
			E o;
			for(int i = 0; i < size; i++)
				if((o = elementData[i]) != null && s.equalsIgnoreCase(o.getName()))
					result.add(o);
		}
		finally
		{
			_read_lock.unlock();
		}
		return result;
	}

	public GArray<E> getAll()
	{
		return getAll(new GArray<E>(size));
	}

	public GArray<E> getAll(final GArray<E> list)
	{
		_read_lock.lock();
		try
		{
			E o;
			for(int i = 0; i < size; i++)
				if((o = elementData[i]) != null)
					list.add(o);
		}
		finally
		{
			_read_lock.unlock();
		}
		return list;
	}

	public final void forEach(final INgVoidProcedure<E> procedure)
	{
		L2GameThreadPools.getInstance().executeGeneral(new Runnable()
		{
			@Override
			public final void run()
			{
				forEachInernal(procedure);
			}
		});
	}

	private final void forEachInernal(final INgVoidProcedure<E> procedure)
	{
		_read_lock.lock();
		try
		{
			forEachInernalNoLock(procedure);
		}
		finally
		{
			_read_lock.unlock();
		}
	}

	private final void forEachInernalNoLock(final INgVoidProcedure<E> procedure)
	{
		for(int i = size; i-- > 0;)
			procedure.execute(elementData[i]);
	}

	@Override
	public final Iterator<E> iterator()
	{
		return new Itr();
	}

	private final class Itr implements Iterator<E>
	{
		private int cursor = 0;
		private E _next;

		@Override
		public final boolean hasNext()
		{
			while (cursor < size)
				if((_next = elementData[cursor++]) != null)
					return true;
			return false;
		}

		@Override
		public final E next()
		{
			final E result = _next;
			_next = null;
			if(result == null)
				throw new NoSuchElementException();
			return result;
		}

		@Override
		public final void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}
