package l2n.game.model;

import l2n.game.model.L2Zone.ZoneType;
import l2n.util.ArrayUtil;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

import static l2n.game.model.L2Zone.ZoneType.*;

public class ZoneInfo
{
	public static final int ZONE_SIEGE = Siege.mask | siege_residense.mask;
	public static final int ZONE_DANGERAREA = poison.mask | instant_skill.mask | swamp.mask | damage.mask;
	public static final int ZONE_BATTLE = battle_zone.mask | pvp_zone.mask | OlympiadStadia.mask;

	public static final int ZONE_NO_SAVE_BOOKMARK = Siege.mask | siege_residense.mask | battle_zone.mask | pvp_zone.mask | OlympiadStadia.mask | epic.mask | no_restart.mask | no_summon.mask | no_escape.mask;

	private final ReentrantLock _lock;

	private L2Zone[] _zones;
	private int _size = 0;
	private int _set;

	public ZoneInfo()
	{
		_lock = new ReentrantLock();
		_zones = L2Zone.EMPTY_ARRAY;
	}

	public final boolean isInsideZone(final ZoneType type)
	{
		return isInsideZone(type.mask);
	}

	public final boolean isInsideZone(final int zone)
	{
		return (_set & zone) != 0;
	}

	public final boolean isInZone(final L2Zone zone)
	{
		if(isEmpty())
			return false;
		return indexOf(_zones, zone) != -1;
	}

	public final L2Zone getZone(final ZoneType type)
	{
		if(!isInsideZone(type))
			return null;

		final int index = indexOf(_zones, type);
		if(index != -1)
			return _zones[index];
		return null;
	}

	public final L2Zone[] getAllZones()
	{
		return _zones;
	}

	public final int getCurrentZoneSet()
	{
		return _set;
	}

	public final void addZone(final L2Zone zone)
	{
		if(zone == null)
			throw new NullPointerException();

		_lock.lock();
		try
		{
			if(_zones.length == 0)
			{
				_zones = new L2Zone[3];
				_zones[_size++] = zone;
				_set |= zone.getTypeId();
			}
			else if(!ArrayUtil.arrayContains(_zones, zone))
			{
				ensureCapacity(_size + 1);
				_zones[_size++] = zone;
				_set |= zone.getTypeId();
			}
		}
		finally
		{
			_lock.unlock();
		}
	}

	public final void removeZone(final L2Zone zone)
	{
		if(zone == null)
			throw new NullPointerException();

		_lock.lock();
		try
		{
			if(isEmpty())
				return;

			final int index = indexOf(_zones, zone);
			if(index != -1)
			{
				final int numMoved = _size - index - 1;
				if(numMoved > 0)
					System.arraycopy(_zones, index + 1, _zones, index, numMoved);
				_zones[--_size] = null;

				if(indexOf(_zones, zone.getType()) == -1)
					_set &= ~zone.getTypeId();

				if(_size == 0)
				{
					_size = 0;
					_zones = L2Zone.EMPTY_ARRAY;
					_set = 0;
				}
			}
		}
		finally
		{
			_lock.unlock();
		}
	}

	private final void ensureCapacity(final int minCapacity)
	{
		final int oldCapacity = _zones.length;
		if(minCapacity > oldCapacity)
		{
			int newCapacity = oldCapacity * 3 / 2 + 1;
			if(newCapacity < minCapacity)
				newCapacity = minCapacity;
			_zones = Arrays.copyOf(_zones, newCapacity);
		}
	}

	public final boolean isEmpty()
	{
		return _size == 0;
	}

	private static final int indexOf(final L2Zone[] zones, final L2Zone zone)
	{
		if(zone == null)
			return -1;
		else
			for(int i = 0; i < zones.length; i++)
				if(zone == zones[i] || zone.equals(zones[i]))
					return i;
		return -1;
	}

	private static final int indexOf(final L2Zone[] zones, final ZoneType type)
	{
		for(int i = 0; i < zones.length; i++)
			if(zones[i] != null && zones[i].getType() == type)
				return i;
		return -1;
	}
}
