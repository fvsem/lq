package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone.ZoneTarget;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.util.Location;
import l2n.util.Rnd;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.logging.Logger;

public class L2Territory
{
	protected final static Logger _log = Logger.getLogger(L2Territory.class.getName());

	protected static class Point
	{
		protected int x, y, zmin, zmax;

		protected Point(final int _x, final int _y, final int _zmin, final int _zmax)
		{
			x = _x;
			y = _y;
			zmin = _zmin;
			zmax = _zmax;
		}
	}

	private L2Zone _zone;
	private final Polygon poly;
	private Point[] _points;
	protected int _x_min;
	protected int _x_max;
	protected int _y_min;
	protected int _y_max;
	protected int _z_min;
	protected int _z_max;

	private final int _id;

	public L2Territory(final int id)
	{
		poly = new Polygon();
		_points = new Point[0];
		_x_min = 999999;
		_x_max = -999999;
		_y_min = 999999;
		_y_max = -999999;
		_z_min = 999999;
		_z_max = -999999;
		_id = id;
	}

	public void add(final Location loc)
	{
		add(loc.x, loc.y, loc.z, loc.h);
	}

	public void add(final int x, final int y, int zmin, int zmax)
	{
		if(zmax == -1)
		{
			zmin -= 50;
			zmax = zmin + 100;
		}
		final Point[] newPoints = new Point[_points.length + 1];
		System.arraycopy(_points, 0, newPoints, 0, _points.length);
		newPoints[_points.length] = new Point(x, y, zmin, zmax);
		_points = newPoints;

		poly.addPoint(x, y);

		if(x < _x_min)
			_x_min = x;
		if(y < _y_min)
			_y_min = y;
		if(x > _x_max)
			_x_max = x;
		if(y > _y_max)
			_y_max = y;
		if(zmin < _z_min)
			_z_min = zmin;
		if(zmax > _z_max)
			_z_max = zmax;
	}

	public void validate()
	{
		if(_points.length > 3)
			for(int i = 1; i < _points.length; i++)
			{
				final int ii = i + 1 < _points.length ? i + 1 : 0;
				for(int n = i; n < _points.length; n++)
					if(Math.abs(n - i) > 1)
					{
						final int nn = n + 1 < _points.length ? n + 1 : 0;
						if(Line2D.linesIntersect(_points[i].x, _points[i].y, _points[ii].x, _points[ii].y, _points[n].x, _points[n].y, _points[nn].x, _points[nn].y))
							_log.warning(this + " is self-intersecting in lines " + i + "-" + ii + " and " + n + "-" + nn);
					}
			}
	}

	public void print()
	{
		for(final Point p : _points)
			System.out.println("(" + p.x + "," + p.y + ")");
	}

	public boolean isInside(final int x, final int y)
	{
		return poly.contains(x, y);
	}

	public boolean isInside(final int x, final int y, final int z)
	{
		return z >= _z_min && z <= _z_max && isInside(x, y);
	}

	public boolean isInside(final Location loc)
	{
		return isInside(loc.x, loc.y, loc.z);
	}

	public boolean isInside(final L2Object obj)
	{
		return isInside(obj.getLoc());
	}

	public int[] getRandomPoint()
	{
		int i, x = 0, y = 0, z = 0;
		for(i = 0; i < 100; i++)
		{
			x = Rnd.get(_x_min, _x_max);
			y = Rnd.get(_y_min, _y_max);
			z = _z_min + (_z_max - _z_min) / 2;

			if(i == 40 && Config.DEBUG_HEAVY_TERR)
				_log.warning("Heavy territory: " + this + ", need manual correction");

			if(poly.contains(x, y))
			{
				if(ZoneManager.getInstance().checkIfInZone(ZoneType.no_spawn, x, y))
					continue;

				final int tempz = GeoEngine.getHeight(x, y, z);
				if(_z_min != _z_max)
				{
					if(tempz < _z_min || tempz > _z_max || _z_min > _z_max)
						continue;
				}
				else if(tempz < _z_min - 200 || tempz > _z_min + 200)
					continue;

				z = tempz;

				if(GeoEngine.getNSWE(x, y, z) != GeoEngine.NSWE_ALL)
					continue;

				return new int[] { x, y, z };
			}
		}
		if(Config.DEBUG_HEAVY_TERR)
			_log.warning("Can't make point for " + this);
		return new int[] { x, y, z };
	}

	public void doEnter(final L2Character cha)
	{
		if(_zone != null)
			if(cha.isPlayable())
				_zone.doEnter(cha);
			else if(_zone.getZoneSkill() != null && _zone.getZoneTarget() == ZoneTarget.npc && cha.isNpc())
				_zone.doEnter(cha);
	}

	public void doLeave(final L2Character cha, final boolean notify)
	{
		if(_zone != null)
			if(cha.isPlayable())
				_zone.doLeave(cha, notify);
			else if(_zone.getZoneSkill() != null && _zone.getZoneTarget() == ZoneTarget.npc && cha.isNpc())
				_zone.doLeave(cha, notify);
	}

	public final int getId()
	{
		return _id;
	}

	@Override
	public final String toString()
	{
		return "territory '" + _id + "'";
	}

	public int getZmin()
	{
		return _z_min;
	}

	public int getZmax()
	{
		return _z_max;
	}

	public int getXmax()
	{
		return _x_max;
	}

	public int getXmin()
	{
		return _x_min;
	}

	public int getYmax()
	{
		return _y_max;
	}

	public int getYmin()
	{
		return _y_min;
	}

	public void setZone(final L2Zone zone)
	{
		_zone = zone;
	}

	public L2Zone getZone()
	{
		return _zone;
	}

	public GArray<int[]> getCoords()
	{
		final GArray<int[]> result = new GArray<int[]>();
		for(final Point point : _points)
			result.add(new int[] { point.x, point.y, point.zmin, point.zmax });
		return result;
	}

	public Location getCenter()
	{
		return new Location(_x_min + (_x_max - _x_min) / 2, _y_min + (_y_max - _y_min) / 2, _z_min + (_z_max - _z_min) / 2);
	}

	public boolean isWorldTerritory()
	{
		return getZone() != null && getZone().getLoc() == this;
	}
}
