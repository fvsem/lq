package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BypassManager
{
	private static final Pattern p = Pattern.compile("\"(bypass +-h +)(.+?)\"");

	private static final Pattern PATTERN_SIMPLE = Pattern.compile("^(_mrsl|_clbbs|_mm|_diary|friendlist|friendmail|manor_menu_select|_match).*", Pattern.DOTALL);
	private static final Pattern PATTERN_SIMPLE_BBS = Pattern.compile("^(bbs_|_bbs|_mail|_friend|_block).*", Pattern.DOTALL);

	public static enum BypassType
	{
		ENCODED,
		ENCODED_BBS,
		SIMPLE,
		SIMPLE_BBS,
		SIMPLE_DIRECT
	}

	public static BypassType getBypassType(final String bypass)
	{
		switch (bypass.charAt(0))
		{
			case '0':
				return BypassType.ENCODED;
			case '1':
				return BypassType.ENCODED_BBS;
			default:
				if(Strings.matches(bypass, PATTERN_SIMPLE))
					return BypassType.SIMPLE;
				if(Strings.matches(bypass, PATTERN_SIMPLE_BBS))
					return BypassType.SIMPLE_BBS;
				return BypassType.SIMPLE_DIRECT;
		}
	}

	public static String encode(final String html, final GArray<String> bypassStorage, final boolean bbs)
	{
		final Matcher m = p.matcher(html);
		final StringBuffer sb = new StringBuffer();

		while (m.find())
		{
			final String bypass = m.group(2);
			String code = bypass;
			String params = "";
			final int i = bypass.indexOf(" $");
			final boolean use_params = i >= 0;
			if(use_params)
			{
				code = bypass.substring(0, i);
				params = bypass.substring(i).replace("$", "\\$");
			}

			if(bbs)
				m.appendReplacement(sb, "\"bypass -h 1" + Integer.toHexString(bypassStorage.size()) + params + "\"");
			else
				m.appendReplacement(sb, "\"bypass -h 0" + Integer.toHexString(bypassStorage.size()) + params + "\"");

			bypassStorage.add(code);
		}

		m.appendTail(sb);
		return sb.toString();
	}

	public static DecodedBypass decode(final String bypass, final GArray<String> bypassStorage, final boolean bbs, final L2Player player)
	{
		synchronized (bypassStorage)
		{
			final String[] bypass_parsed = bypass.split(" ");
			int idx;
			try
			{
				idx = Integer.parseInt(bypass_parsed[0].substring(1), 16);
			}
			catch(final NumberFormatException e)
			{
				Log.addDev("Can't decode bypass (bypass not exists): " + (bbs ? "[bbs] " : "") + bypass + " / Player: " + player.getName() + " / Npc: " + (player.getLastNpc() == null ? "null" : player.getLastNpc().getName()), "debug_bypass");
				return null;
			}

			String bp;
			try
			{
				bp = bypassStorage.get(idx);
			}
			catch(final Exception e)
			{
				bp = null;
			}

			if(bp == null)
			{
				Log.addDev("Can't decode bypass (bypass not exists): " + (bbs ? "[bbs] " : "") + bypass + " / Player: " + player.getName() + " / Npc: " + (player.getLastNpc() == null ? "null" : player.getLastNpc().getName()), "debug_bypass");
				return null;
			}

			DecodedBypass result = null;
			result = new DecodedBypass(bp, bbs);
			for(int i = 1; i < bypass_parsed.length; i++)
				result.bypass += " " + bypass_parsed[i];
			result.trim();

			return result;
		}
	}

	public static class DecodedBypass
	{
		public String bypass;
		public boolean bbs;

		public DecodedBypass(final String _bypass, final boolean _bbs)
		{
			bypass = _bypass;
			bbs = _bbs;
		}

		public DecodedBypass trim()
		{
			bypass = bypass.trim();
			return this;
		}
	}
}
