package l2n.game.model;

import l2n.commons.lang.reference.AbstractHardReference;

public class L2Reference<T extends L2Object> extends AbstractHardReference<T>
{
	public L2Reference(final T reference)
	{
		super(reference);
	}
}
