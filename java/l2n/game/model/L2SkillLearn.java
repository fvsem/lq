package l2n.game.model;

public final class L2SkillLearn
{
	public final int id;
	public final int skillLevel;
	public final int _spCost;
	public final int _repCost;
	public final byte minLevel;
	public final int itemId;
	public final int itemCount;
	public final boolean common;
	public final boolean clan;
	public final boolean transformation;

	public final int raceId;

	public final String name;

	public L2SkillLearn(int _id, int lvl, byte minLvl, String _name, int cost, int _itemId, int _itemCount, boolean _common, boolean _clan, boolean _transformation, int race_id)
	{
		id = _id;
		skillLevel = lvl;
		minLevel = minLvl;
		name = _name.intern();
		if(_clan)
		{
			_spCost = 0;
			_repCost = cost;
		}
		else if(_transformation)
		{
			_repCost = 0;
			_spCost = 0;
		}
		else
		{
			_repCost = 0;
			_spCost = cost;
		}
		itemId = _itemId;
		itemCount = _itemCount;
		common = _common;
		clan = _clan;
		transformation = _transformation;
		raceId = race_id;
	}

	public L2SkillLearn(int _id, int lvl, byte minLvl, String _name, int cost, int _itemId, int _itemCount, boolean _common, boolean _clan, boolean _transformation)
	{
		this(_id, lvl, minLvl, _name, cost, _itemId, _itemCount, _common, _clan, _transformation, -1);
	}

	public int getId()
	{
		return id;
	}

	public int getLevel()
	{
		return skillLevel;
	}

	public byte getMinLevel()
	{
		return minLevel;
	}

	public String getName()
	{
		return name;
	}

	public int getSpCost()
	{
		return _spCost;
	}

	public int getItemId()
	{
		return itemId;
	}

	public int getItemCount()
	{
		return itemCount;
	}

	public int getRepCost()
	{
		return _repCost;
	}

	@Override
	public String toString()
	{
		return "SkillLearn for " + name + " id " + id + " level " + skillLevel;
	}
}
