package l2n.game.model;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.MultiSellEntry;
import l2n.game.model.base.MultiSellIngredient;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.MultiSellList;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Item.Grade;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Log;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2Multisell
{
	private static final Logger _log = Logger.getLogger(L2Multisell.class.getName());

	private static L2Multisell _instance;

	private final TIntObjectHashMap<MultiSellListContainer> entries = new TIntObjectHashMap<MultiSellListContainer>();

	public MultiSellListContainer getList(final L2Player player, final int id)
	{
		final MultiSellListContainer list = entries.get(id);
		if(list != null)
			return list;

		_log.warning("[L2Multisell] can't find list with id: " + id + ", maybe cheater " + player);
		return null;
	}

	public L2Multisell()
	{
		parseData();
	}

	public void reload()
	{
		parseData();
	}

	public static L2Multisell getInstance()
	{
		if(_instance == null)
			_instance = new L2Multisell();
		return _instance;
	}

	private synchronized void parseData()
	{
		entries.clear();
		parse();
		loadHardcoded();
		_log.info("L2Multisell: Loaded " + entries.size() + " lists.");
	}

	public class MultiSellListContainer
	{
		private int _listId;
		private boolean _showall = true;
		private boolean keep_enchanted = false;
		private boolean is_dutyfree = false;
		private boolean nokey = false;
		private GArray<MultiSellEntry> entries = new GArray<MultiSellEntry>();

		public void setListId(final int listId)
		{
			_listId = listId;
		}

		public int getListId()
		{
			return _listId;
		}

		public void setShowAll(final boolean bool)
		{
			_showall = bool;
		}

		public void setNoTax(final boolean bool)
		{
			is_dutyfree = bool;
		}

		public void setKeepEnchant(final boolean bool)
		{
			keep_enchanted = bool;
		}

		public void setNoKey(final boolean bool)
		{
			nokey = bool;
		}

		public boolean isNoKey()
		{
			return nokey;
		}

		public boolean isShowAll()
		{
			return _showall;
		}

		public boolean isNoTax()
		{
			return is_dutyfree;
		}

		public boolean isKeepEnchant()
		{
			return keep_enchanted;
		}

		public void addEntry(final MultiSellEntry e)
		{
			entries.add(e);
		}

		public GArray<MultiSellEntry> getEntries()
		{
			return entries;
		}

		public boolean isEmpty()
		{
			return entries.isEmpty();
		}
	}

	private void hashFiles(final String dirname, final GArray<File> hash)
	{
		final File dir = new File(Config.DATAPACK_ROOT, "data/" + dirname);
		if(!dir.exists())
		{
			_log.config("Dir " + dir.getAbsolutePath() + " not exists");
			return;
		}
		final File[] files = dir.listFiles();
		for(final File f : files)
			if(f.getName().endsWith(".xml"))
				hash.add(f);
			else if(f.isDirectory() && !f.getName().equals(".svn"))
				hashFiles("multisell/" + f.getName(), hash);
	}

	private void addMultiSellListContainer(final int id, final MultiSellListContainer list)
	{
		if(entries.containsKey(id))
			_log.warning("MultiSell redefined: " + id);

		list.setListId(id);
		entries.put(id, list);
	}

	public void parseFile(final File f)
	{
		int id = 0;
		try
		{
			id = Integer.parseInt(f.getName().replaceAll(".xml", ""));
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Error loading file " + f, e);
			return;
		}
		Document doc = null;
		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			doc = factory.newDocumentBuilder().parse(f);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Error loading file " + f, e);
			return;
		}
		try
		{
			addMultiSellListContainer(id, parseDocument(doc, id));
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Error in file " + f, e);
		}
	}

	private void parse()
	{
		final GArray<File> files = new GArray<File>();
		hashFiles("multisell", files);

		for(final File f : files)
			parseFile(f);

		files.clear();
	}

	protected MultiSellListContainer parseDocument(final Document doc, final int id)
	{
		final MultiSellListContainer list = new MultiSellListContainer();
		int entId = 1;

		for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if("list".equalsIgnoreCase(n.getNodeName()))
				for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					if("item".equalsIgnoreCase(d.getNodeName()))
					{
						final MultiSellEntry e = parseEntry(d, id);
						if(e != null)
						{
							e.setEntryId(entId++);
							list.addEntry(e);
						}
					}
					else if("config".equalsIgnoreCase(d.getNodeName()))
					{
						list.setShowAll(XMLUtil.getAttributeBooleanValue(d, "showall", true));
						list.setNoTax(XMLUtil.getAttributeBooleanValue(d, "notax", false));
						list.setKeepEnchant(XMLUtil.getAttributeBooleanValue(d, "keepenchanted", false));
						list.setNoKey(XMLUtil.getAttributeBooleanValue(d, "nokey", false));
					}

		return list;
	}

	protected MultiSellEntry parseEntry(final Node node, final int multiSellId)
	{
		final MultiSellEntry entry = new MultiSellEntry();

		for(Node n = node.getFirstChild(); n != null; n = n.getNextSibling())
		{
			int enchant = 0;
			int element = -2;
			int elementValue = 0;

			if("ingredient".equalsIgnoreCase(n.getNodeName()))
			{
				final int id = Integer.parseInt(n.getAttributes().getNamedItem("id").getNodeValue());
				final long count = Long.parseLong(n.getAttributes().getNamedItem("count").getNodeValue());

				if(n.getAttributes().getNamedItem("enchant") != null)
					enchant = Integer.parseInt(n.getAttributes().getNamedItem("enchant").getNodeValue());
				if(n.getAttributes().getNamedItem("element") != null)
					element = Integer.parseInt(n.getAttributes().getNamedItem("element").getNodeValue());
				if(n.getAttributes().getNamedItem("elementValue") != null)
					elementValue = Integer.parseInt(n.getAttributes().getNamedItem("elementValue").getNodeValue());

				entry.addIngredient(new MultiSellIngredient(id, count, enchant, element, elementValue));
			}
			else if("production".equalsIgnoreCase(n.getNodeName()))
			{
				final int id = Integer.parseInt(n.getAttributes().getNamedItem("id").getNodeValue());
				final long count = Long.parseLong(n.getAttributes().getNamedItem("count").getNodeValue());

				if(n.getAttributes().getNamedItem("enchant") != null)
					enchant = Integer.parseInt(n.getAttributes().getNamedItem("enchant").getNodeValue());
				if(n.getAttributes().getNamedItem("element") != null)
					element = Integer.parseInt(n.getAttributes().getNamedItem("element").getNodeValue());
				if(n.getAttributes().getNamedItem("elementValue") != null)
					elementValue = Integer.parseInt(n.getAttributes().getNamedItem("elementValue").getNodeValue());

				entry.addProduct(new MultiSellIngredient(id, count, enchant, element, elementValue));
			}
		}

		if(entry.getIngredients().isEmpty() || entry.getProduction().isEmpty())
			return null;

		if(entry.getIngredients().size() == 1 && entry.getProduction().size() == 1 && entry.getIngredients().get(0).getItemId() == 57)
		{
			final L2Item item = ItemTable.getInstance().getTemplate(entry.getProduction().get(0).getItemId());
			if(item == null)
			{
				_log.warning("WARNING!!! MultiSell [" + multiSellId + "] Production [" + entry.getProduction().get(0).getItemId() + "] is null");
				return null;
			}
			if((multiSellId < 70000 || multiSellId > 70010) && item.getReferencePrice() > entry.getIngredients().get(0).getItemCount())
				Log.addDev("MultiSell [" + multiSellId + "] Production '" + item.getName() + "' [" + entry.getProduction().get(0).getItemId() + "] price is lower than referenced | " + item.getReferencePrice() + " > " + entry.getIngredients().get(0).getItemCount(), "dev_multisellbug", false);
		}

		return entry;
	}

	public void separateAndSend(final int listId, final L2Player player, final double taxRate)
	{
		if(Config.ALT_SHOP_CONDITION_ENABLE && Config.ALT_DISABLED_MULTISELL.contains(listId))
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return;
		}

		final MultiSellListContainer list = generateMultiSell(listId, player, taxRate);
		if(list == null)
		{
			if(player != null)
				_log.warning("L2Multisell: list is null, id:" + listId + " player:" + player.getName() + "[" + player.getObjectId() + "].");
			else
				_log.warning("L2Multisell: list is null, id:" + listId + " player is null.");
			return;
		}

		MultiSellListContainer temp = new MultiSellListContainer();
		int page = 1;

		temp.setListId(list.getListId());

		player.setMultisell(list);

		for(final MultiSellEntry e : list.getEntries())
		{
			if(temp.getEntries().size() == Config.MULTISELL_SIZE)
			{
				player.sendPacket(new MultiSellList(temp, page, 0));
				page++;
				temp = new MultiSellListContainer();
				temp.setListId(list.getListId());
			}
			temp.addEntry(e);
		}
		player.sendPacket(new MultiSellList(temp, page, 1));
	}

	private MultiSellListContainer generateMultiSell(final int listId, final L2Player player, final double taxRate)
	{
		MultiSellListContainer list;
		final GArray<MultiSellEntry> possiblelist = new GArray<MultiSellEntry>();

		GArray<L2ItemInstance> _items;
		if(listId == 9999)
		{
			list = new MultiSellListContainer();
			list._listId = listId;

			list.setShowAll(false);
			list.setKeepEnchant(true);
			list.setNoTax(true);
			final Inventory inv = player.getInventory();
			_items = new GArray<L2ItemInstance>();
			for(final L2ItemInstance itm : inv.getItems())
				if(itm.getItem().getAdditionalName().isEmpty() // Менять можно только обычные предметы
						&& !itm.isStackable()
						&& !itm.getItem().isSa() // SA менять нельзя
						&& !itm.getItem().isRare() // Rare менять нельзя
						&& !itm.getItem().isCommonItem() // Common менять нельзя
						&& !itm.getItem().isPvP() // PvP менять нельзя
						&& itm.canBeTraded(player) // универсальная проверка\
						&& itm.getItem().isWeapon() // менять можно только оружие
						&& itm.getItem().getCrystalType() != Grade.NONE
						&& itm.getItem().getCrystalCount() > 0
						&& itm.getItem().isTradeable() //
						&& itm.getReferencePrice() <= Config.ALT_MAMMON_EXCHANGE
						&& (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE)
					_items.add(itm);

			for(final L2ItemInstance itm : _items)
				for(final L2Weapon i : ItemTable.getInstance().getAllWeapons())
					if(i.getAdditionalName().isEmpty() // Менять можно только обычные предметы
							&& !i.isMonsterWeapon()
							&& !i.isSa() // На SA менять нельзя
							&& !i.isRare() // На Rare менять нельзя
							&& !i.isCommonItem() // На Common менять нельзя
							&& !i.isPvP() // На PvP менять нельзя
							&& !i.isShadowItem() // На Shadow менять нельзя
							&& i.isTradeable() // можно использовать чтобы запретить менять специальные вещи
							&& i.isWeapon()
							&& i.getItemId() != itm.getItemId()
							&& i.getItemType() == WeaponType.DUAL == (itm.getItem().getItemType() == WeaponType.DUAL) // Двурук/однорук
							&& itm.getItem().getCrystalType() != Grade.NONE //
							&& itm.getItem().getCrystalType() == i.getCrystalType()// одного типа
							&& itm.getItem().getCrystalCount() == i.getCrystalCount())
					{
						final int entry = new int[] { itm.getItemId(), i.getItemId(), itm.getEnchantLevel() }.hashCode();
						final MultiSellEntry possibleEntry = new MultiSellEntry(entry, i.getItemId(), 1, itm.getEnchantLevel());
						possibleEntry.addIngredient(new MultiSellIngredient(itm.getItemId(), 1, itm.getEnchantLevel()));
						possiblelist.add(possibleEntry);
					}
			list.entries = possiblelist;
		}
		else if(listId == 9998)
		{
			list = new MultiSellListContainer();
			list._listId = listId;

			list.setShowAll(false);
			list.setKeepEnchant(false);
			list.setNoTax(true);
			final Inventory inv = player.getInventory();
			_items = new GArray<L2ItemInstance>();
			for(final L2ItemInstance itm : inv.getItems())
				if(itm.getItem().getAdditionalName().isEmpty() // Менять можно только обычные предметы
						&& !itm.getItem().isSa() // SA менять нельзя
						&& !itm.getItem().isRare() // Rare менять нельзя
						&& !itm.getItem().isCommonItem() // Common менять нельзя
						&& !itm.getItem().isPvP() // PvP менять нельзя
						&& !itm.getItem().isShadowItem() // Shadow менять нельзя
						&& !itm.getItem().isTemporal() // Temporal менять нельзя
						&& !itm.isEquipped()
						&& !itm.isStackable() //
						&& itm.isWeapon()
						&& itm.getReferencePrice() <= Config.ALT_MAMMON_UPGRADE
						&& itm.getItem().isTradeable() //
						&& itm.getItem().getCrystalType() != Grade.NONE
						&& itm.getItem().getCrystalCount() > 0
						&& (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE)
					_items.add(itm);

			for(final L2ItemInstance itemtosell : _items)
				for(final L2Weapon itemtobuy : ItemTable.getInstance().getAllWeapons())
					if(itemtobuy.getAdditionalName().isEmpty() // Менять можно только обычные предметы
							&& !itemtobuy.isMonsterWeapon()
							&& !itemtobuy.isSa() // На SA менять нельзя
							&& !itemtobuy.isRare() // На Rare менять нельзя
							&& !itemtobuy.isCommonItem() // На Common менять нельзя
							&& !itemtobuy.isPvP() // На PvP менять нельзя
							&& !itemtobuy.isShadowItem() // На Shadow менять нельзя
							&& itemtobuy.isTradeable() //
							&& itemtobuy.isWeapon() //
							&& itemtobuy.getItemType() == WeaponType.DUAL == (itemtosell.getItem().getItemType() == WeaponType.DUAL) // Двурук/однорук
							&& itemtobuy.getCrystalType().ordinal() >= itemtosell.getItem().getCrystalType().ordinal() // одного типа или круче
							&& itemtobuy.getReferencePrice() <= Config.ALT_MAMMON_UPGRADE //
							&& itemtosell.getItem().getReferencePrice() < itemtobuy.getReferencePrice() //
							&& itemtosell.getReferencePrice() * 1.7 > itemtobuy.getReferencePrice() //
					)
					{
						final int entry = new int[] { itemtosell.getItemId(), itemtobuy.getItemId(), itemtosell.getEnchantLevel() }.hashCode();
						final MultiSellEntry possibleEntry = new MultiSellEntry(entry, itemtobuy.getItemId(), 1, 0);
						possibleEntry.addIngredient(new MultiSellIngredient(itemtosell.getItemId(), 1, itemtosell.getEnchantLevel()));
						possibleEntry.addIngredient(new MultiSellIngredient(5575, (int) ((itemtobuy.getReferencePrice() - itemtosell.getReferencePrice()) * 1.2), 0));
						possiblelist.add(possibleEntry);
					}
			list.entries = possiblelist;
		}
		else if(listId == 9997)
		{
			list = new MultiSellListContainer();
			list._listId = listId;

			list.setShowAll(false);
			list.setKeepEnchant(true);
			list.setNoTax(false);
			final Inventory inv = player.getInventory();
			for(final L2ItemInstance itm : inv.getItems())
				if(!itm.isStackable() && itm.getItem().isCrystallizable() && itm.getItem().getCrystalType() != Grade.NONE && itm.getItem().getCrystalCount() > 0 && !itm.isShadowItem() && !itm.isTemporalItem() && !itm.isEquipped() && (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_CRYSTALLIZE) != L2ItemInstance.FLAG_NO_CRYSTALLIZE)
				{
					final L2Item crystal = ItemTable.getInstance().getTemplate(itm.getItem().getCrystalType().cry);
					final int entry = new int[] { itm.getItemId(), itm.getEnchantLevel() }.hashCode();
					final MultiSellEntry possibleEntry = new MultiSellEntry(entry, crystal.getItemId(), itm.getItem().getCrystalCount(), 0);
					possibleEntry.addIngredient(new MultiSellIngredient(itm.getItemId(), 1, itm.getEnchantLevel()));
					possibleEntry.addIngredient(new MultiSellIngredient(57, (int) (itm.getItem().getCrystalCount() * crystal.getReferencePrice() * 0.05), 0));
					possiblelist.add(possibleEntry);
				}
			list.entries = possiblelist;
		}
		else if(listId == 90210)
		{
			list = new MultiSellListContainer();
			list._listId = listId;

			list.setShowAll(false);
			list.setKeepEnchant(true);
			list.setNoTax(true);
			final Inventory inv = player.getInventory();
			_items = new GArray<L2ItemInstance>();
			for(final L2ItemInstance itm : inv.getItems())
				if(itm.getItem().isSa() // только с SA
						&& !itm.isCommonItem() // Common менять нельзя
						&& !itm.isShadowItem() // Shadow менять нельзя
						&& !itm.isTemporalItem() // Temporal менять нельзя
						&& !itm.isStackable() //
						&& !itm.isEquipped() // не одетое
						&& itm.isWeapon() // ПУШКА!
						&& itm.getItem().getItemGradeSPlus() == Grade.S
						&& itm.getItem().getCrystalCount() > 0 //
						&& itm.getItem().isTradeable() //
						&& (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE //
				)
					_items.add(itm);

			for(final L2ItemInstance itemtosell : _items)
				for(final L2Weapon itemtobuy : ItemTable.getInstance().getAllWeapons())
					if(itemtobuy.isSa() // Обмен только на SA
							&& !itemtobuy.isCommonItem() // На Common менять нельзя
							&& !itemtobuy.isShadowItem() // На Shadow менять нельзя
							&& !itemtobuy.isPvP() // На PvP менять нельзя
							&& itemtobuy.isTradeable() //
							&& itemtobuy.getType2() == L2Item.TYPE2_WEAPON // ПУШКА!
							&& itemtobuy.getItemType() == WeaponType.DUAL == (itemtosell.getItem().getItemType() == WeaponType.DUAL) // Двурук/однорук
							&& itemtobuy.getCrystalType().ordinal() >= itemtosell.getItem().getCrystalType().ordinal() // одного типа или круче
					)
					{
						final int entry = new int[] { itemtosell.getItemId(), itemtobuy.getItemId(), itemtosell.getEnchantLevel() }.hashCode();
						final MultiSellEntry possibleEntry = new MultiSellEntry(entry, itemtobuy.getItemId(), 1, 0);
						possibleEntry.addIngredient(new MultiSellIngredient(itemtosell.getItemId(), 1, itemtosell.getEnchantLevel()));
						final int[][] price = {
								{ 2052, 2052, 5 }, // S Grade to S Grade
								{ 2052, 3597, 143 }, // S Grade to S80 Dynasty
								{ 2052, 4965, 216 }, // S Grade to S80 Icarus
								{ 2052, 7050, 390 }, // S Grade to S84 Vesper (хз, будет ли вообще)
								{ 3597, 3597, 25 }, // S80 Dynasty to S80 Dynasty
								{ 3597, 4965, 25 }, // S80 Dynasty to S80 Icarus
								{ 3597, 7050, 117 }, // S80 Dynasty to S84 Vesper
								{ 4965, 3597, 25 }, // S80 Icarus to S80 Dynasty
								{ 4965, 4965, 25 }, // S80 Icarus to S80 Icarus
								{ 4965, 7050, 117 }, // S80 Icarus to S84 Vesper
								{ 7050, 7050, 40 }, // S84 Vesper to S84 Vesper
						};
						int priceFinal = 1;
						for(final int[] i : price)
							if(itemtosell.getItem().getCrystalCount() == i[0] && itemtobuy.getCrystalCount() == i[1])
								if(itemtosell.getItem().isRare() || itemtosell.getItem().isPvP())
									priceFinal = i[2];
								else if(itemtobuy.isRare() || itemtobuy.isPvP())
									priceFinal = Math.round(i[2] / 10) + i[2];
								else
									priceFinal = i[2];
						possibleEntry.addIngredient(new MultiSellIngredient(4357, priceFinal, 0));
						possiblelist.add(possibleEntry);
					}
			list.entries = possiblelist;
		}
		else
		{
			final MultiSellListContainer container = getList(player, listId);
			if(container == null)
			{
				_log.warning("Not found myltisell " + listId);
				return null;
			}
			if(container.isEmpty())
			{
				player.sendMessage(new CustomMessage("common.Disabled", player));
				return null;
			}

			final boolean enchant = container.isKeepEnchant();
			final boolean notax = container.isNoTax();
			final boolean showall = container.isShowAll();
			final boolean nokey = container.isNoKey();

			final Inventory inv = player.getInventory();

			list = new MultiSellListContainer();
			list._listId = listId;

			list.setShowAll(showall);
			list.setKeepEnchant(enchant);
			list.setNoTax(notax);
			list.setNoKey(nokey);

			for(final MultiSellEntry origEntry : container.getEntries())
			{
				final MultiSellEntry ent = origEntry.clone();

				GArray<MultiSellIngredient> ingridients;

				if(!notax && taxRate > 0)
				{
					ingridients = new GArray<MultiSellIngredient>(ent.getIngredients().size() + 1);
					double tax = 0;
					double adena = 0;

					for(final MultiSellIngredient i : ent.getIngredients())
					{
						if(i.getItemId() == 57)
						{
							adena += i.getItemCount(); // количество адены
							tax += i.getItemCount() * taxRate; // налог (количество которое нужно ДОБАВИТЬ к адене)
							continue;
						}

						ingridients.add(i);

						if(i.getItemId() == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE)
							tax += i.getItemCount() / 120 * 1000 * taxRate * 100;

						if(i.getItemId() < 1)
							continue;

						final L2Item item = ItemTable.getInstance().getTemplate(i.getItemId());
						if(item == null)
							_log.warning("L2Multisell: not found template for itemId: " + i.getItemId());
						else if(item.isStackable())
							tax += item.getReferencePrice() * i.getItemCount() * taxRate;
					}

					adena = Math.round(adena + tax);
					if(adena >= 1)
						ingridients.add(new MultiSellIngredient(57, (long) adena));

					tax = Math.round(tax);
					if(tax >= 1)
						ent.setTax((long) tax);

					ent.getIngredients().clear();
					ent.getIngredients().addAll(ingridients);
				}
				else
					ingridients = ent.getIngredients();

				if(showall)
					possiblelist.add(ent);
				else
				{
					final GArray<Integer> _itm = new GArray<Integer>();

					for(final MultiSellIngredient i : ingridients)
					{
						final L2Item template = i.getItemId() <= 0 ? null : ItemTable.getInstance().getTemplate(i.getItemId());
						if(i.getItemId() == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE || i.getItemId() == L2Item.ITEM_ID_PC_BANG_POINTS || i.getItemId() == L2Item.ITEM_ID_FAME || template.getType2() <= L2Item.TYPE2_ACCESSORY || template.getType2() >= (nokey ? L2Item.TYPE2_OTHER : L2Item.TYPE2_PET_WOLF)) // Экипировка
						{
							if(i.getItemId() == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE)
							{
								if(!_itm.contains(i.getItemId()) && player.getClan() != null && player.getClan().getReputationScore() >= i.getItemCount())
									_itm.add(i.getItemId());
								continue;
							}
							else if(i.getItemId() == L2Item.ITEM_ID_PC_BANG_POINTS)
							{
								if(!_itm.contains(i.getItemId()) && player.getPcBangPoints() >= i.getItemCount())
									_itm.add(i.getItemId());
								continue;
							}
							else if(i.getItemId() == L2Item.ITEM_ID_FAME)
							{
								if(!_itm.contains(i.getItemId()) && player.getFame() >= i.getItemCount())
									_itm.add(i.getItemId());
								continue;
							}

							if(i.getItemId() == 12374) // Mammon's Varnish Enhancer
								continue;

							for(final L2ItemInstance item : inv.getItems())
								if(item.getItemId() == i.getItemId() && !item.isEquipped() && (item.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE)
								{
									if(_itm.contains(enchant ? i.getItemId() + i.getItemEnchant() * 100000 : i.getItemId())) // Не проверять одинаковые вещи
										continue;

									if(item.getEnchantLevel() < i.getItemEnchant())
										continue;

									if(item.isStackable() && item.getCount() < i.getItemCount())
										break;

									_itm.add(enchant ? i.getItemId() + i.getItemEnchant() * 100000 : i.getItemId());
									final MultiSellEntry possibleEntry = new MultiSellEntry(enchant ? ent.getEntryId() + item.getEnchantLevel() * 100000 : ent.getEntryId());

									for(final MultiSellIngredient p : ent.getProduction())
									{
										p.setItemEnchant(item.getEnchantLevel());
										possibleEntry.addProduct(p);
									}

									for(final MultiSellIngredient ig : ingridients)
									{
										// Set enchant level only for weapon/armor/jewelry
										if(template != null && template.getType2() <= L2Item.TYPE2_ACCESSORY)
											ig.setItemEnchant(item.getEnchantLevel());
										possibleEntry.addIngredient(ig);
									}

									possiblelist.add(possibleEntry);
									break;
								}
						}
					}
				}
			}

			list.entries = possiblelist;
		}

		return list;
	}

	private void loadHardcoded()
	{
		MultiSellListContainer list = new MultiSellListContainer();
		list.setListId(4000);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSAInsertion(Grade.C, Grade.B, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(4001);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSAInsertion(Grade.A, Grade.A, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(4002);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSARemove(list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(40011);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSAInsertion(Grade.S, Grade.S, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(40010);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSAInsertion(Grade.S80, Grade.S80, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(326150002);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generateSAInsertion(Grade.S80, Grade.S84, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(311262517);
		list.setShowAll(true);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		list.setNoKey(true);
		generateFinishMastetrwork(list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(364790006);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generatePvPAdd(list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(364790007);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(true);
		generatePvPRemove(list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1002);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(Grade.B, Grade.B, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1003);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateReseal(Grade.B, Grade.B, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1005);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(Grade.A, Grade.A, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1007);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateReseal(Grade.A, Grade.A, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1008);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(Grade.S, Grade.S, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1009);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(Grade.S80, Grade.S80, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1010);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(Grade.S84, Grade.S84, list);
		entries.put(list.getListId(), list);

		list = new MultiSellListContainer();
		list.setListId(1011);
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		generateUnseal(L2Item.Grade.S80, L2Item.Grade.S84, list, 57, 5);
		entries.put(list.getListId(), list);
	}

	private void generateSAInsertion(final Grade mingrade, final Grade maxgrade, final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; i++)
		{
			final int[] item = weapons[i];
			if(item == null)
				continue;
			final L2Item itm = ItemTable.getInstance().getTemplate(i);
			if(itm.getItemGrade().ordinal() < mingrade.ordinal() || itm.getItemGrade().ordinal() > maxgrade.ordinal())
				continue;
			final int[] price = getSaInsertPrice(itm);
			if(price == null)
				continue;

			if(item[ItemTable.WEX_SA1] > 0 && item[ItemTable.WEX_SA_CRY1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY1], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_SA2] > 0 && item[ItemTable.WEX_SA_CRY2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY2], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_SA3] > 0 && item[ItemTable.WEX_SA_CRY3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY3], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA1] > 0 && item[ItemTable.WEX_SA_CRY1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY1], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA2] > 0 && item[ItemTable.WEX_SA_CRY2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY2], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA3] > 0 && item[ItemTable.WEX_SA_CRY3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA_CRY3], 1));
				e1.addIngredient(new MultiSellIngredient(price[0], price[1]));
				if(price[2] > 0)
					e1.addIngredient(new MultiSellIngredient(57, price[2]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA3], 1));
				list.addEntry(e1);
			}
		}
	}

	private int[] getSaInsertPrice(final L2Item i)
	{
		switch (i.getCrystalType())
		{
			case C:
				if(i.getCrystalCount() <= 706)
					return new int[] { 2131, 97, 97 * 3000 };
				else if(i.getCrystalCount() <= 884)
					return new int[] { 2131, 238, 238 * 3000 };
				else if(i.getCrystalCount() <= 1325)
					return new int[] { 2131, 306, 306 * 3000 };
				return new int[] { 2131, 555, 555 * 3000 };
			case B:
				if(i.getCrystalCount() <= 892)
					return new int[] { 2132, 222, 222 * 10000 };
				return new int[] { 2132, 339, 339 * 10000 };
			case A:
				if(i.getCrystalCount() <= 1128)
					return new int[] { 2133, 147, 0 };
				return new int[] { 2133, 157, 0 };
			case S:
				return new int[] { 2134, 82, 0 };
			case S80:
				if(i.getCrystalCount() <= 7050)
					return new int[] { 2134, 285, 0 };
				return new int[] { 2134, 399, 0 };
			case S84:
				return new int[] { 2134, 623, 0 };
		}
		return null;
	}

	private void generateSARemove(final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; i++)
		{
			final int[] item = weapons[i];
			if(item == null)
				continue;
			final L2Item itm = ItemTable.getInstance().getTemplate(i);
			final int price = getSARemovePrice(itm);

			if(item[ItemTable.WEX_SA1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA1], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA2], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA3], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA1], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA2], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA3], 1));
				if(price > 0)
					e1.addIngredient(new MultiSellIngredient(5575, price));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				list.addEntry(e1);
			}
		}
	}

	private int getSARemovePrice(final L2Item i)
	{
		switch (i.getCrystalType())
		{
			case C:
				if(i.getCrystalCount() <= 706)
					return 14550;
				else if(i.getCrystalCount() <= 884)
					return 35700;
				else if(i.getCrystalCount() <= 1325)
					return 45900;
				else
					return 83250;
			case B:
				if(i.getCrystalCount() <= 892)
					return 111000;
				return 169500;
			case A:
				return 210000;
			case S:
			case S80:
			case S84:
				return 250000; // FIXME: цифра с потолка
		}
		return 0;
	}

	private void generateFinishMastetrwork(final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; i++)
		{
			final int[] item = weapons[i];
			if(item == null)
				continue;

			if(item[ItemTable.WEX_FOUNDATION] > 0 && item[ItemTable.WEX_RARE] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_FOUNDATION], 1));
				if(item[ItemTable.WEX_VARNISH_COUNT] > 0)
					e1.addIngredient(new MultiSellIngredient(12374, item[ItemTable.WEX_VARNISH_COUNT]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE], 1));
				list.addEntry(e1);
			}
		}

		final int[][] armors = ItemTable.getInstance().getArmorEx();
		for(int i = 0; i < armors.length; i++)
		{
			final int[] item = armors[i];
			if(item == null)
				continue;

			if(item[ItemTable.AEX_FOUNDATION] <= 0)
				continue;

			if(item[ItemTable.AEX_SEALED_RARE_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
				if(item[ItemTable.AEX_VARNISH] > 0)
					e1.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_1], 1));
				list.addEntry(e1);
				if(item[ItemTable.AEX_SEALED_RARE_2] > 0)
				{
					final MultiSellEntry e2 = new MultiSellEntry(entId++);
					e2.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
					if(item[ItemTable.AEX_VARNISH] > 0)
						e2.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
					e2.addProduct(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_2], 1));
					list.addEntry(e2);
				}
				if(item[ItemTable.AEX_SEALED_RARE_3] > 0)
				{
					final MultiSellEntry e3 = new MultiSellEntry(entId++);
					e3.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
					if(item[ItemTable.AEX_VARNISH] > 0)
						e3.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
					e3.addProduct(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_3], 1));
					list.addEntry(e3);
				}
			}
			else if(item[ItemTable.AEX_UNSEALED_RARE_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
				if(item[ItemTable.AEX_VARNISH] > 0)
					e1.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				list.addEntry(e1);
				if(item[ItemTable.AEX_UNSEALED_RARE_2] > 0)
				{
					final MultiSellEntry e2 = new MultiSellEntry(entId++);
					e2.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
					if(item[ItemTable.AEX_VARNISH] > 0)
						e2.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
					e2.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
					list.addEntry(e2);
				}
				if(item[ItemTable.AEX_UNSEALED_RARE_3] > 0)
				{
					final MultiSellEntry e3 = new MultiSellEntry(entId++);
					e3.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_FOUNDATION], 1));
					if(item[ItemTable.AEX_VARNISH] > 0)
						e3.addIngredient(new MultiSellIngredient(12374, item[ItemTable.AEX_VARNISH]));
					e3.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
					list.addEntry(e3);
				}
			}
		}
	}

	private void generatePvPAdd(final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; i++)
		{
			final int[] item = weapons[i];
			if(item == null)
				continue;

			final L2Item itm = ItemTable.getInstance().getTemplate(i);
			final int[] price = getPvPWeaponPrice(itm);
			if(price == null)
				continue;

			if(item[ItemTable.WEX_PVP1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA1] > 0 ? item[ItemTable.WEX_SA1] : i, 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_PVP1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_PVP2] > 0 && item[ItemTable.WEX_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA2], 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_PVP2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_PVP3] > 0 && item[ItemTable.WEX_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_SA3], 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_PVP3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP1] > 0 && (item[ItemTable.WEX_RARE_SA1] > 0 || item[ItemTable.WEX_RARE] > 0))
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA1] > 0 ? item[ItemTable.WEX_RARE_SA1] : item[ItemTable.WEX_RARE], 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP2] > 0 && item[ItemTable.WEX_RARE_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA2], 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP3] > 0 && item[ItemTable.WEX_RARE_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA3], 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP3], 1));
				list.addEntry(e1);
			}
		}

		final int[][] armors = ItemTable.getInstance().getArmorEx();
		for(int i = 0; i < armors.length; i++)
		{
			final int[] item = armors[i];
			if(item == null || item[ItemTable.AEX_PvP] <= 0)
				continue;

			final L2Item itm = ItemTable.getInstance().getTemplate(item[ItemTable.AEX_UNSEALED_1] > 0 ? item[ItemTable.AEX_UNSEALED_1] : i);
			final int[] price = getPvPArmorPrice(itm);
			if(price == null)
				continue;

			if(item[ItemTable.AEX_PvP] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(itm.getItemId(), 1));
				e1.addIngredient(new MultiSellIngredient(L2Item.ITEM_ID_FAME, price[0]));
				e1.addIngredient(new MultiSellIngredient(57, price[1]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_PvP], 1));
				list.addEntry(e1);
			}
		}
	}

	private int[] getPvPWeaponPrice(final L2Item i)
	{
		switch (i.getCrystalType())
		{
			case A:
				if(i.getCrystalCount() <= 1128)
					return null;
				return new int[] { 8091, 809050 };
			case S:
				return new int[] { 11545, 1154520 };
			case S80:
			{
				if(i.getCrystalCount() == 3597) // Dynasty
					return new int[] { 18700, 3747300 };
				else if(i.getCrystalCount() == 4219) // Dynasty Dual Daggers
					return new int[] { 43950, 4395050 };
				else if(i.getCrystalCount() == 5845) // Icarus Dual Daggers
					return new int[] { 30400, 6089000 };

				return new int[] { 25900, 5171950 }; // Icarus
			}
			case S84:
				if(i.getCrystalCount() == 8233) // Vesper Dual Daggers
					return new int[] { 42900, 8576500 };
				return new int[] { 36700, 7343650 }; // Vesper
		}
		return null;
	}

	private int[] getPvPArmorPrice(final L2Item i)
	{
		switch (i.getCrystalType())
		{
			case A:
				if(i.getCrystalCount() < 477)
					return null;
				return new int[] { 1981, 198100 };
			case S:
				return new int[] { 4206, 420570 };
			case S80:
			{
				if(i.getWeight() == 7570 && i.getPrice() == 33673000) // Dynasty Platinum Plate
					return new int[] { 12689, 1268850 };
				else if(i.getWeight() == 7570) // Dynasty Plate
					return new int[] { 8606, 860640 };
				else if(i.getPrice() == 18298000) // Dynasty Light/Tunic
					return new int[] { 6455, 645480 };
				else if(i.getPrice() == 25255000)// Dynasty Light/Tunic (Silve & Jewel)
					return new int[] { 9516, 951630 };

				return new int[] { 8608, 860640 };
			}
			case S84:
			{
				if(i.getPrice() == 55840000) // Vesper Noble Breastplate
					return new int[] { 14000, 2792000 };
				else if(i.getPrice() == 41880000) // Vesper Noble Tunic/Leather
					return new int[] { 10500, 2094000 };
				else if(i.getPrice() == 46668000) // Vesper Breastplate
					return new int[] { 11700, 2333400 };
				else if(i.getPrice() == 35000000) // Vesper Tunic/Leather
					return new int[] { 8800, 1750000 };

				return new int[] { 6455, 645480 };
			}
		}
		return null;
	}

	private void generatePvPRemove(final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; i++)
		{
			final int[] item = weapons[i];
			if(item == null)
				continue;

			if(item[ItemTable.WEX_PVP1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_PVP1], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA1] > 0 ? item[ItemTable.WEX_SA1] : i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_PVP2] > 0 && item[ItemTable.WEX_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_PVP2], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_PVP3] > 0 && item[ItemTable.WEX_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_PVP3], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_SA3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP1], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA1] > 0 ? item[ItemTable.WEX_RARE_SA1] : item[ItemTable.WEX_RARE], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP2] > 0 && item[ItemTable.WEX_RARE_SA2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP2], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.WEX_RARE_PVP3] > 0 && item[ItemTable.WEX_RARE_SA3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.WEX_RARE_PVP3], 1));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.WEX_RARE_SA3], 1));
				list.addEntry(e1);
			}
		}

		final int[][] armors = ItemTable.getInstance().getArmorEx();
		for(int i = 0; i < armors.length; i++)
		{
			final int[] item = armors[i];
			if(item == null)
				continue;

			final L2Item itm = ItemTable.getInstance().getTemplate(i);
			final int[] price = getPvPArmorPrice(itm);
			if(price == null)
				continue;

			if(item[ItemTable.AEX_PvP] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_PvP], 1));
				if(item[ItemTable.AEX_UNSEALED_1] > 0)
					e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_1], 1));
				else
					e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
		}
	}

	private void generateUnseal(final Grade mingrade, final Grade maxgrade, final MultiSellListContainer list)
	{
		generateUnseal(mingrade, maxgrade, list, 5575, 1);
	}

	private void generateUnseal(final Grade mingrade, final Grade maxgrade, final MultiSellListContainer list, final int unsealItem, final int priceMult)
	{
		int entId = 1;
		final int[][] armors = ItemTable.getInstance().getArmorEx();
		for(int i = 0; i < armors.length; i++)
		{
			final int[] item = armors[i];
			if(item == null)
				continue;

			final L2Item itm = ItemTable.getInstance().getTemplate(i);

			if(itm.getItemGrade().ordinal() < mingrade.ordinal() || itm.getItemGrade().ordinal() > maxgrade.ordinal())
				continue;

			if(item[ItemTable.AEX_UNSEALED_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(i, 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_3], 1));
				list.addEntry(e1);
			}

			if(item[ItemTable.AEX_UNSEALED_RARE_1] > 0 && item[ItemTable.AEX_SEALED_RARE_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_1], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_2] > 0 && (item[ItemTable.AEX_SEALED_RARE_2] > 0 || item[ItemTable.AEX_SEALED_RARE_1] > 0))
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				if(item[ItemTable.AEX_SEALED_RARE_2] > 0)
					e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_2], 1));
				else
					e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_1], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_3] > 0 && (item[ItemTable.AEX_SEALED_RARE_3] > 0 || item[ItemTable.AEX_SEALED_RARE_2] > 0 || item[ItemTable.AEX_SEALED_RARE_1] > 0))
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				if(item[ItemTable.AEX_SEALED_RARE_3] > 0)
					e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_3], 1));
				else
					e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_SEALED_RARE_1], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_1] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_2] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_3] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				if(item[ItemTable.AEX_UNSEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(unsealItem, item[ItemTable.AEX_UNSEAL_PRICE] * priceMult / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_3], 1));
				list.addEntry(e1);
			}
		}
	}

	private void generateReseal(final Grade mingrade, final Grade maxgrade, final MultiSellListContainer list)
	{
		int entId = 1;
		final int[][] armors = ItemTable.getInstance().getArmorEx();
		for(int i = 0; i < armors.length; i++)
		{
			final int[] item = armors[i];
			if(item == null || item[ItemTable.AEX_RESEAL_PRICE] < 0)
				continue;

			final L2Item itm = ItemTable.getInstance().getTemplate(i);

			if(itm.getItemGrade().ordinal() < mingrade.ordinal() || itm.getItemGrade().ordinal() > maxgrade.ordinal())
				continue;

			if(item[ItemTable.AEX_UNSEALED_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_1], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_2], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_3], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(i, 1));
				list.addEntry(e1);
			}

			if(item[ItemTable.AEX_UNSEALED_RARE_1] > 0 && item[ItemTable.AEX_UNSEALED_RARE_2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_1] > 0 && item[ItemTable.AEX_UNSEALED_RARE_3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_2] > 0 && item[ItemTable.AEX_UNSEALED_RARE_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_2] > 0 && item[ItemTable.AEX_UNSEALED_RARE_3] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_3] > 0 && item[ItemTable.AEX_UNSEALED_RARE_1] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_1], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_RARE_3] > 0 && item[ItemTable.AEX_UNSEALED_RARE_2] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_3], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE]));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_RARE_2], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_1] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_1], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE] / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_2] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_2], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE] / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				list.addEntry(e1);
			}
			if(item[ItemTable.AEX_UNSEALED_COMMON_3] > 0 && item[ItemTable.AEX_COMMON] > 0)
			{
				final MultiSellEntry e1 = new MultiSellEntry(entId++);
				e1.addIngredient(new MultiSellIngredient(item[ItemTable.AEX_UNSEALED_COMMON_3], 1));
				if(item[ItemTable.AEX_RESEAL_PRICE] > 0)
					e1.addIngredient(new MultiSellIngredient(5575, item[ItemTable.AEX_RESEAL_PRICE] / 20));
				e1.addProduct(new MultiSellIngredient(item[ItemTable.AEX_COMMON], 1));
				list.addEntry(e1);
			}
		}
	}
}
