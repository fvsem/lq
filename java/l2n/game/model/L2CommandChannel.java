package l2n.game.model;

import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;

public class L2CommandChannel
{
	private GArray<L2Party> _partys;
	private L2Player _commandLeader;
	private int _channelLvl;
	private long _reflection;

	public static final int STRATEGY_GUIDE_ID = 8871;
	public static final int CLAN_IMPERIUM_ID = 391;

	public L2CommandChannel(final L2Player leader)
	{
		_commandLeader = leader;
		_partys = new GArray<L2Party>();
		_partys.add(leader.getParty());
		_channelLvl = leader.getParty().getLevel();
		leader.getParty().setCommandChannel(this);
		leader.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.THE_COMMAND_CHANNEL_HAS_BEEN_FORMED));
		leader.getParty().broadcastToPartyMembers(Msg.ExMPCCOpen);
		leader.getParty().broadcastToPartyMembers(new ExMultiPartyCommandChannelInfo(this));
	}

	public void addParty(final L2Party party)
	{
		_partys.add(party);
		refreshLevel();
		party.setCommandChannel(this);

		if(_partys.size() > 2)
			party.broadcastToPartyMembers(new SystemMessage(SystemMessage.YOU_ARE_PARTICIPATING_IN_THE_CHANNEL_WHICH_HAS_BEEN_ALREADY_OPENED));
		else
			party.broadcastToPartyMembers(new SystemMessage(SystemMessage.YOU_HAVE_PARTICIPATED_IN_THE_COMMAND_CHANNEL));

		party.broadcastToPartyMembers(Msg.ExMPCCOpen);

		if(_partys.size() < 5)
		{
			SystemMessage sm = new SystemMessage(SystemMessage.THE_COMMAND_CHANNEL_IS_ACTIVATED_ONLY_IF_AT_LEAST_FIVE_PARTIES_PARTICIPATE_IN);
			broadcastToChannelMembers(sm);
			sm = new SystemMessage(SystemMessage.THE_NUMBER_OF_REMAINING_PARTIES_IS_S1_UNTIL_A_CHANNEL_IS_ACTIVATED);
			sm.addNumber(5 - _partys.size());
		}
		broadcastToChannelMembers(new ExMultiPartyCommandChannelInfo(this));
	}

	public void removeParty(final L2Party party)
	{
		_partys.remove(party);
		refreshLevel();
		party.setCommandChannel(null);
		party.broadcastToPartyMembers(Msg.ExMPCCClose);
		final Reflection reflection = getReflection();
		if(reflection != null)
			for(final L2Player player : party.getPartyMembers())
			{
				player.teleToLocation(reflection.getReturnLoc(), 0);
				player.sendPacket(new ExSendUIEvent(player, true, true, 0, 10, ""));
			}

		if(_partys.size() < 2)
			disbandChannel();
		else
		{
			broadcastToChannelMembers(new ExMultiPartyCommandChannelInfo(this));
			if(party != null)
			{
				L2Player leader = party.getPartyLeader();
				if(leader != null)
					broadcastToChannelMembers(new ExMPCCPartyInfoUpdate(leader.getName(), leader.getObjectId(), party.getMemberCount(), 0));
			}
		}
	}

	public void disbandChannel()
	{
		broadcastToChannelMembers(new SystemMessage(SystemMessage.THE_COMMAND_CHANNEL_HAS_BEEN_DISBANDED));
		for(final L2Party party : _partys)
			if(party != null)
			{
				party.setCommandChannel(null);
				party.broadcastToPartyMembers(Msg.ExMPCCClose);
				if(isInReflection())
					party.broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(1));
			}
		final Reflection reflection = getReflection();
		if(reflection != null)
		{
			reflection.startCollapseTimer(60000L);
			setReflection(null);
		}
		_partys = null;
		_commandLeader = null;
	}

	public int getMemberCount()
	{
		int count = 0;
		for(final L2Party party : _partys)
			if(party != null)
				count += party.getMemberCount();
		return count;
	}

	public void broadcastToChannelMembers(final L2GameServerPacket gsp)
	{
		if(_partys != null && !_partys.isEmpty())
			for(final L2Party party : _partys)
				if(party != null)
					party.broadcastToPartyMembers(gsp);
	}

	public void broadcastToChannelPartyLeaders(final L2GameServerPacket gsp)
	{
		if(_partys != null && !_partys.isEmpty())
			for(final L2Party party : _partys)
				if(party != null)
				{
					final L2Player leader = party.getPartyLeader();
					if(leader != null)
						leader.sendPacket(gsp);
				}
	}

	public GArray<L2Party> getParties()
	{
		return _partys;
	}

	public GArray<L2Player> getMembers()
	{
		final GArray<L2Player> members = new GArray<L2Player>();
		for(final L2Party party : getParties())
			members.addAll(party.getPartyMembers());
		return members;
	}

	public boolean contains(final L2Player target)
	{
		if(target.getParty() == null)
			return false;

		return _partys.contains(target.getParty());
	}

	public int getLevel()
	{
		return _channelLvl;
	}

	public void setChannelLeader(final L2Player newLeader)
	{
		_commandLeader = newLeader;
		broadcastToChannelMembers(new SystemMessage(SystemMessage.COMMAND_CHANNEL_AUTHORITY_HAS_BEEN_TRANSFERRED_TO_S1).addString(newLeader.getName()));
	}

	public L2Player getChannelLeader()
	{
		return _commandLeader;
	}

	public boolean meetRaidWarCondition(final L2Object obj)
	{
		if(!obj.isRaid())
			return false;
		final int npcId = ((L2MonsterInstance) obj).getNpcId();
		switch (npcId)
		{
			case 29001: // Queen Ant
			case 29006: // Core
			case 29014: // Orfen
			case 29022: // Zaken
				return getMemberCount() > 36;
			case 29020: // Baium
				return getMemberCount() > 56;
			case 29019: // Antharas
				return getMemberCount() > 225;
			case 29028: // Valakas
				return getMemberCount() > 99;
			case 29045: // Frintezza
				return getMemberCount() > 35;
			default: // normal Raidboss
				return getMemberCount() > 18;
		}
	}

	private void refreshLevel()
	{
		_channelLvl = 0;
		for(final L2Party pty : _partys)
			if(pty.getLevel() > _channelLvl)
				_channelLvl = pty.getLevel();
	}

	public boolean isInReflection()
	{
		return _reflection > 0;
	}

	public void setReflection(final Reflection reflection)
	{
		_reflection = reflection == null ? 0 : reflection.getId();
	}

	public Reflection getReflection()
	{
		return _reflection > 0 ? ReflectionTable.getInstance().get(_reflection) : null;
	}

	public void teleToLocation(final Location loc, final int rnd_min, final int rnd_max)
	{
		final GArray<L2Player> members = getMembers();
		for(final L2Player player : members)
			if(rnd_min > 0 && rnd_max > 0)
				player.teleToLocation(loc.rnd(rnd_min, rnd_max, false));
			else
				player.teleToLocation(loc);
	}

	public static boolean checkAuthority(final L2Player creator)
	{
		if(creator.getClan() == null || !creator.isInParty() || !creator.getParty().isLeader(creator) || creator.getPledgeClass() < L2Player.RANK_BARON)
		{
			creator.sendPacket(new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL));
			return false;
		}

		final boolean haveSkill = creator.getSkillLevel(CLAN_IMPERIUM_ID) > 0;
		final boolean haveItem = creator.getInventory().getItemByItemId(STRATEGY_GUIDE_ID) != null;
		if(!haveSkill && !haveItem)
		{
			creator.sendPacket(new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL));
			return false;
		}

		return true;
	}
}
