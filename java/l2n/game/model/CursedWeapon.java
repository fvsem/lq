package l2n.game.model;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.TransformationManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Item;
import l2n.util.Location;
import l2n.util.Rnd;

import java.sql.SQLException;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class CursedWeapon
{
	private static final Logger _log = Logger.getLogger(CursedWeapon.class.getName());
	// _name is the name of the cursed weapon associated with its ID.
	private final String _name;
	// _itemId is the Item ID of the cursed weapon.
	private final int _itemId;
	// _skillId is the skills ID.
	private final int _skillId;
	private final int _skillMaxLevel;
	private int _dropRate;
	private int _durationMin;
	private int _durationMax;
	private int _durationLost;
	private int _disapearChance;
	private int _stageKills; // Сколько нужно убить чтоб подняться до след уровня
	private int _transformationId;

	public enum CursedWeaponState
	{
		NONE,
		ACTIVATED,
		DROPPED,
	}

	private ScheduledFuture<?> _removeTask;
	private CursedWeaponState _state = CursedWeaponState.NONE;

	private int _nbKills = 0;
	private long _endTime = 0;

	private int _playerId = 0;
	private L2Player _player = null;
	private L2ItemInstance _item = null;
	private int _playerKarma = 0;
	private int _playerPkKills = 0;
	private Location _loc = null;

	public CursedWeapon(int itemId, Integer skillId, String name)
	{
		_name = name;
		_itemId = itemId;
		_skillId = skillId;
		_skillMaxLevel = SkillTable.getInstance().getMaxLevel(_skillId);
	}

	public void endOfLife()
	{
		if(isActivated())
		{
			if(_player != null && _player.isOnline())
			{
				_log.info("Cursed Weapon: " + _name + " being removed online from " + _player + ".");

				_player.abortAttack();

				_player.setKarma(_playerKarma);
				_player.setPkKills(_playerPkKills);
				_player.setCursedWeaponEquippedId(0);
				_player.stopTransformation();
				_player.removeSkill(SkillTable.getInstance().getInfo(_skillId, _player.getSkillLevel(_skillId)), false);

				_player.getInventory().unEquipItemInBodySlot(L2Item.SLOT_LR_HAND, null);
				_player.store(false);

				if(_player.getInventory().destroyItemByItemId(_itemId, 1, false) == null)
					_log.info("Cursed Weapons[395]: Error! Cursed weapon not found!!!");

				_player.broadcastUserInfo(true);
			}
			else
			{
				_log.info("Cursed Weapon: " + _name + " being removed offline.");

				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();

					statement = con.prepareStatement("DELETE FROM items WHERE owner_id=? AND item_id=?");
					statement.setInt(1, _playerId);
					statement.setInt(2, _itemId);
					statement.execute();
					DbUtils.close(statement);

					statement = con.prepareStatement("DELETE FROM character_skills WHERE char_obj_id=? AND skill_id=?");
					statement.setInt(1, _playerId);
					statement.setInt(2, _skillId);
					statement.execute();
					DbUtils.close(statement);

					statement = con.prepareStatement("UPDATE characters SET karma=?, pkkills=? WHERE obj_Id=?");
					statement.setInt(1, _playerKarma);
					statement.setInt(2, _playerPkKills);
					statement.setInt(3, _playerId);
					statement.execute();
				}
				catch(SQLException e)
				{
					_log.warning("Cursed Weapon: Could not delete : " + e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement);
				}
			}
		}
		else if(_player != null && _player.getInventory().getItemByItemId(_itemId) != null)
		{
			if(_player.getInventory().destroyItemByItemId(_itemId, 1, false) == null)
				_log.info("Cursed Weapon[453]: Error! Cursed weapon not found!!!");

			_player.sendChanges();
			_player.broadcastUserInfo(true);
		}
		else if(_item != null)
		{
			_item.removeFromDb();
			_item.deleteMe();
			_log.info("Cursed Weapon: " + _name + " item has been removed from World.");
		}

		CursedWeaponsManager.removeFromDb(_itemId);
		CursedWeaponsManager.announce(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED_CW).addString(_name));

		cancelTask();

		_state = CursedWeaponState.NONE;
		_endTime = 0;
		_player = null;
		_playerId = 0;
		_playerKarma = 0;
		_playerPkKills = 0;
		_item = null;
		_nbKills = 0;
	}

	private void cancelTask()
	{
		if(_removeTask != null)
		{
			_removeTask.cancel(true);
			_removeTask = null;
		}
	}

	private class RemoveTask implements Runnable
	{
		@Override
		public void run()
		{
			if(System.currentTimeMillis() >= getEndTime())
				endOfLife();
		}
	}

	public boolean dropIt(L2NpcInstance attackable, L2Player killer)
	{
		boolean success = false;

		if(attackable != null)
		{
			if(Rnd.get(100000000) <= _dropRate)
			{
				_item = ItemTable.getInstance().createItem(_itemId, killer.getObjectId(), attackable.getNpcId(), "CursedWeapon.dropIt");
				if(_item != null)
				{
					_player = null;
					_playerId = 0;
					_playerKarma = 0;
					_playerPkKills = 0;
					_state = CursedWeaponState.DROPPED;

					SystemMessage sm = new SystemMessage(SystemMessage.S2_WAS_DROPPED_IN_THE_S1_REGION);
					sm.addZoneName(killer.getX(), killer.getY(), killer.getZ()); // Region Name
					sm.addString(_item.getItem().getName());
					CursedWeaponsManager.announce(sm);

					if(_endTime == 0)
						_endTime = System.currentTimeMillis() + getRndDuration() * 60000;
					_removeTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new RemoveTask(), _durationLost * 12000, _durationLost * 12000L);

					_item.dropToTheGround(killer, attackable);
					_loc = _item.getLoc();
					_item.setDropTime(0);

					L2GameServerPacket redSky = new ExRedSky(10);
					L2GameServerPacket eq = new Earthquake(killer.getLoc(), 30, 12);
					for(L2Player aPlayer : L2ObjectsStorage.getAllPlayersForIterate())
						aPlayer.sendPacket(redSky, eq);
					success = true;
				}
			}
		}
		else if(!Rnd.chance(_disapearChance))
		{
			L2ItemInstance dropedItem = _player.getInventory().findItemByItemId(_itemId);
			if(dropedItem == null)
				return false;

			long oldCount = dropedItem.getCount();
			_player.validateLocation(0);

			if((dropedItem = _player.getInventory().dropItem(dropedItem, oldCount)) == null)
				return false;

			dropedItem.dropToTheGround(_player, (L2NpcInstance) null);
			dropedItem.setDropTime(0);

			_player.setKarma(_playerKarma);
			_player.setPkKills(_playerPkKills);
			_player.setCursedWeaponEquippedId(0);
			_player.stopTransformation();
			_player.removeSkill(SkillTable.getInstance().getInfo(_skillId, _player.getSkillLevel(_skillId)), false);
			_player.abortAttack();


			_playerId = 0;
			_playerKarma = 0;
			_playerPkKills = 0;
			_state = CursedWeaponState.DROPPED;

			_loc = dropedItem.getLoc();
			_item = dropedItem;

			_player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_DROPPED_S1).addItemName(dropedItem.getItemId()));

			_player.refreshExpertisePenalty();
			_player.broadcastUserInfo(true);

			Earthquake eq = new Earthquake(_player.getLoc(), 30, 12);
			ExRedSky redSky = new ExRedSky(10);
			_player.broadcastPacket(eq, redSky);

			success = true;
		}
		return success;
	}

	public void transform()
	{
		if(_transformationId == 0)
			return;

		if(_player.isTransformed())
		{
			_player.stopTransformation();
			TransformationManager.getInstance().transformPlayer(_transformationId, _player);
		}
		else
			TransformationManager.getInstance().transformPlayer(_transformationId, _player);
	}

	public final void giveSkill()
	{
		int level = 1 + _nbKills / _stageKills;
		if(level > getMaxLevel())
			level = getMaxLevel();

		L2Skill skill = SkillTable.getInstance().getInfo(_skillId, level);
		_player.addSkill(skill, false);
	}

	public void removeSkillAndAppearance()
	{
		_player.stopTransformation();

		_player.removeSkill(SkillTable.getInstance().getInfo(_skillId, _player.getSkillLevel(_skillId)), false);

		if(_player.getTransformationId() > 0)
			TransformationManager.getInstance().transformPlayer(_player.getTransformationId(), _player);

		_player.sendPacket(new SkillList(_player));
	}

	public void reActivate()
	{
		_state = CursedWeaponState.ACTIVATED;

		if(_endTime - System.currentTimeMillis() <= 0)
			endOfLife();
		else
			_removeTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new RemoveTask(), _durationLost * 12000L, _durationLost * 12000L);
	}

	public void activate(L2Player player, L2ItemInstance item)
	{
		if(_state != CursedWeaponState.ACTIVATED || _playerId != player.getObjectId())
		{
			_playerKarma = player.getKarma();
			_playerPkKills = player.getPkKills();
		}

		_state = CursedWeaponState.ACTIVATED;
		_player = player;
		_playerId = player.getObjectId();

		if(_endTime == 0)
			_endTime = System.currentTimeMillis() + getRndDuration() * 60000;
		if(_removeTask == null)
			_removeTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new RemoveTask(), _durationLost * 12000, _durationLost * 12000L);

		player.setCursedWeaponEquippedId(_itemId);
		player.setKarma(9999999);
		player.setPkKills(_nbKills);

		if(player.isInParty())
			player.getParty().oustPartyMember(player);

		if(player.isMounted())
			player.setMount(0, 0);

		giveSkill();

		_item = item;
		player.getInventory().setPaperdollItem(Inventory.PAPERDOLL_LHAND, null);
		player.getInventory().setPaperdollItem(Inventory.PAPERDOLL_RHAND, null);
		player.getInventory().setPaperdollItem(Inventory.PAPERDOLL_RHAND, _item);
		player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EQUIPPED_YOUR_S1).addItemName(_item.getItemId()));

		player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
		player.setCurrentCp(player.getMaxCp());
		transform();
		player.broadcastUserInfo(true);
	}

	public void increaseKills()
	{
		increaseKills(1);
	}

	public void increaseKills(final int kills)
	{
		_nbKills += kills;
		_player.setPkKills(_nbKills);
		_player.broadcastUserInfo(true);

		if(_nbKills % _stageKills == 0 && _nbKills <= _stageKills * (_skillMaxLevel - 1))
			giveSkill();

		_endTime -= _durationLost * 60000;
	}

	public void setDisapearChance(int disapearChance)
	{
		_disapearChance = disapearChance;
	}

	public void setDropRate(int dropRate)
	{
		_dropRate = dropRate;
	}

	public void setDurationMin(int duration)
	{
		_durationMin = duration;
	}

	public void setDurationMax(int duration)
	{
		_durationMax = duration;
	}

	public void setDurationLost(int durationLost)
	{
		_durationLost = durationLost;
	}

	public void setStageKills(int stageKills)
	{
		_stageKills = stageKills;
	}

	public void setTransformationId(int transformationId)
	{
		_transformationId = transformationId;
	}

	public int getTransformationId()
	{
		return _transformationId;
	}

	public void setNbKills(int nbKills)
	{
		_nbKills = nbKills;
	}

	public void setPlayerId(int playerId)
	{
		_playerId = playerId;
	}

	public void setPlayerKarma(int playerKarma)
	{
		_playerKarma = playerKarma;
	}

	public void setPlayerPkKills(int playerPkKills)
	{
		_playerPkKills = playerPkKills;
	}

	public void setState(CursedWeaponState state)
	{
		_state = state;
	}

	public void setEndTime(long endTime)
	{
		_endTime = endTime;
	}

	public void setPlayer(L2Player player)
	{
		_player = player;
	}

	public void setItem(L2ItemInstance item)
	{
		_item = item;
	}

	public void setLoc(Location loc)
	{
		_loc = loc;
	}

	public CursedWeaponState getState()
	{
		return _state;
	}

	public boolean isActivated()
	{
		return _state == CursedWeaponState.ACTIVATED;
	}

	public boolean isDropped()
	{
		return _state == CursedWeaponState.DROPPED;
	}

	public long getEndTime()
	{
		return _endTime;
	}

	public String getName()
	{
		return _name;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public L2ItemInstance getItem()
	{
		return _item;
	}

	public Integer getSkillId()
	{
		return _skillId;
	}

	public int getPlayerId()
	{
		return _playerId;
	}

	public L2Player getPlayer()
	{
		return _player;
	}

	public int getPlayerKarma()
	{
		return _playerKarma;
	}

	public int getPlayerPkKills()
	{
		return _playerPkKills;
	}

	public int getNbKills()
	{
		return _nbKills;
	}

	public int getStageKills()
	{
		return _stageKills;
	}

	public Location getLoc()
	{
		return _loc;
	}

	public int getRndDuration()
	{
		if(_durationMin > _durationMax)
			_durationMax = 2 * _durationMin;
		return Rnd.get(_durationMin, _durationMax);
	}

	public boolean isActive()
	{
		return _state == CursedWeaponState.ACTIVATED || _state == CursedWeaponState.DROPPED;
	}

	public int getLevel()
	{
		if(_nbKills > _stageKills * getMaxLevel())
			return getMaxLevel();
		else
			return _nbKills / _stageKills;
	}

	public final int getMaxLevel()
	{
		return _skillMaxLevel;
	}

	public long getTimeLeft()
	{
		return _endTime - System.currentTimeMillis();
	}

	public Location getWorldPosition()
	{
		if(isActivated())
		{
			if(_player != null && _player.isOnline())
				return _player.getLoc();
		}
		else if(isDropped())
			if(_item != null)
				return _item.getLoc();

		return null;
	}
}
