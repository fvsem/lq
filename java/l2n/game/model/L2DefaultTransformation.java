package l2n.game.model;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SkillList;

public abstract class L2DefaultTransformation extends L2Transformation
{
	public L2DefaultTransformation(int id, double collisionRadius, double collisionHeight)
	{
		super(id, collisionRadius, collisionHeight);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != getId())
			return;
		if(player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
		addSkill(player, 619, 1);

		addSkill(player, 5491, 1);

		player.sendPacket(new SkillList(player));
	}

	public abstract void transformedSkills(L2Player player);

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
		removeSkill(player, 619);

		removeSkill(player, 5491);

		player.sendPacket(new SkillList(player));
	}

	public abstract void removeSkills(L2Player player);
}
