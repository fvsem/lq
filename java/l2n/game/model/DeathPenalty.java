package l2n.game.model;

import l2n.Config;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.EtcStatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class DeathPenalty
{
	private long playerStoreId = 0;
	private byte _level;

	private static final int _skillId = 5076;

	private boolean _hasCharmOfLuck;

	public DeathPenalty(final L2Player player, final byte level)
	{
		playerStoreId = player.getStoredId();
		_level = player.isGM() ? 0 : level;
	}

	public L2Player getPlayer()
	{
		return L2ObjectsStorage.getAsPlayer(playerStoreId);
	}

	public int getLevel()
	{
		if(_level > 15)
			_level = 15;

		if(_level < 0)
			_level = 0;

		return Config.ALLOW_DEATH_PENALTY_C5 ? _level : 0;
	}

	public int getLevelOnSaveDB()
	{
		if(_level > 15)
			_level = 15;

		if(_level < 0)
			_level = 0;

		return _level;
	}

	public void notifyDead(final L2Character killer)
	{
		if(!Config.ALLOW_DEATH_PENALTY_C5)
			return;

		if(_hasCharmOfLuck)
		{
			_hasCharmOfLuck = false;
			return;
		}

		final L2Player player = getPlayer();
		if(player == null || player.getLevel() <= 9)
			return;

		int karmaBonus = player.getKarma() / Config.ALT_DEATH_PENALTY_C5_KARMA_PENALTY;
		if(karmaBonus < 0)
			karmaBonus = 0;

		if(Rnd.chance(Config.ALT_DEATH_PENALTY_C5_CHANCE + karmaBonus) && !(killer instanceof L2Playable))
			addLevel();
	}

	public void restore()
	{
		final L2Player player = getPlayer();
		if(player == null)
			return;

		final L2Skill remove = getCurrentSkill();

		if(remove != null)
			player.removeSkill(remove, true);

		if(!Config.ALLOW_DEATH_PENALTY_C5)
			return;

		if(getLevel() > 0)
		{
			player.addSkill(SkillTable.getInstance().getInfo(_skillId, getLevel()), false);
			player.sendPacket(new SystemMessage(SystemMessage.THE_LEVEL_S1_DEATH_PENALTY_WILL_BE_ASSESSED).addNumber(getLevel()));
		}
		player.sendPacket(new EtcStatusUpdate(player));
		player.updateStats();
	}

	public void addLevel()
	{
		final L2Player player = getPlayer();
		if(player == null || getLevel() >= 15 || player.isGM())
			return;

		if(getLevel() != 0)
		{
			final L2Skill remove = getCurrentSkill();

			if(remove != null)
				player.removeSkill(remove, true);
		}

		_level++;

		player.addSkill(SkillTable.getInstance().getInfo(_skillId, getLevel()), false);
		player.sendPacket(new EtcStatusUpdate(player));
		player.updateStats();
		player.sendPacket(new SystemMessage(SystemMessage.THE_LEVEL_S1_DEATH_PENALTY_WILL_BE_ASSESSED).addNumber(getLevel()));
	}

	public void reduceLevel()
	{
		final L2Player player = getPlayer();
		if(player == null || getLevel() <= 0)
			return;

		final L2Skill remove = getCurrentSkill();

		if(remove != null)
			player.removeSkill(remove, true);

		_level--;

		if(getLevel() > 0)
		{
			player.addSkill(SkillTable.getInstance().getInfo(_skillId, getLevel()), false);
			player.sendPacket(new SystemMessage(SystemMessage.THE_LEVEL_S1_DEATH_PENALTY_WILL_BE_ASSESSED).addNumber(getLevel()));
		}
		else
			player.sendPacket(new SystemMessage(SystemMessage.THE_DEATH_PENALTY_HAS_BEEN_LIFTED));

		player.sendPacket(new EtcStatusUpdate(player));
		player.updateStats();
	}

	public L2Skill getCurrentSkill()
	{
		final L2Player player = getPlayer();
		if(player != null)
			for(final L2Skill s : player.getAllSkills())
				if(s.getId() == _skillId)
					return s;
		return null;
	}

	public void checkCharmOfLuck()
	{
		final L2Player player = getPlayer();
		if(player != null)
			for(final L2Effect e : player.getEffectList().getEffects())
			{
				if(e == null)
					continue;
				final L2Skill s = e.getSkill();
				if(s != null && (s.getId() == L2Skill.SKILL_RAID_BLESSING || s.getId() == L2Skill.SKILL_FORTUNE_OF_NOBLESSE))
				{
					_hasCharmOfLuck = true;
					return;
				}
			}
		_hasCharmOfLuck = false;
	}
}
