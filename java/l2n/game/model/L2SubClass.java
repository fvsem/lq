package l2n.game.model;

import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;

import java.util.logging.Logger;

public class L2SubClass
{
	private int _classId = 0;
	private int _numSub = -1;
	private long _exp = Experience.LEVEL[40];
	private int _sp = 0;
	private byte _level = 40;
	private double _Hp = 1;
	private double _Mp = 1;
	private double _Cp = 1;
	private boolean _active = false;
	private boolean _isBase = false;
	private L2Player _player;
	private DeathPenalty _dp;

	static final Logger _log = Logger.getLogger(L2SubClass.class.getName());

	public int getClassId()
	{
		return _classId;
	}

	public long getExp()
	{
		return _exp;
	}

	public void addExp(long val)
	{
		setExp(_exp + val);
	}

	public int getSp()
	{
		return Math.min(_sp, Integer.MAX_VALUE);
	}

	public byte getLevel()
	{
		return _level;
	}

	public int getNumSub()
	{
		return _numSub;
	}

	public void setClassId(final int classId)
	{
		_classId = classId;
	}

	public void setNumSub(final int numSub)
	{
		_numSub = numSub;
	}

	public void setExp(final long expValue)
	{
		if(expValue > Experience.LEVEL[getMaxLevel() + 1])
			_exp = Experience.LEVEL[getMaxLevel() + 1];
		else if(expValue < Experience.LEVEL[40] && !_isBase)
			_exp = Experience.LEVEL[40];
		else
			_exp = expValue;
	}

	public void setSp(long spValue)
	{
		spValue = Math.max(spValue, 0);
		spValue = Math.min(spValue, Integer.MAX_VALUE);
		_sp = (int) spValue;
	}

	public void setHp(final double hpValue)
	{
		_Hp = hpValue;
	}

	public double getHp()
	{
		return _Hp;
	}

	public void setMp(final double mpValue)
	{
		_Mp = mpValue;
	}

	public double getMp()
	{
		return _Mp;
	}

	public void setCp(final double cpValue)
	{
		_Cp = cpValue;
	}

	public double getCp()
	{
		return _Cp;
	}

	public void setLevel(byte levelValue)
	{
		if(levelValue > getMaxLevel())
			levelValue = (byte) getMaxLevel();
		else if(levelValue < 40 && !_isBase)
			levelValue = 40;

		_level = levelValue;
	}

	public void incLevel()
	{
		if(_level > getMaxLevel())
			return;

		_level++;
	}

	public void decLevel()
	{
		if(_level == 40 && !_isBase)
			return;

		_level--;
	}

	public void setActive(final boolean active)
	{
		_active = active;
	}

	public boolean isActive()
	{
		return _active;
	}

	public void setBase(final boolean base)
	{
		_isBase = base;
	}

	public boolean isBase()
	{
		return _isBase;
	}

	public DeathPenalty getDeathPenalty()
	{
		if(_dp == null)
			_dp = new DeathPenalty(_player, (byte) 0);
		return _dp;
	}

	public void setDeathPenalty(DeathPenalty dp)
	{
		_dp = dp;
	}

	public void setPlayer(L2Player player)
	{
		_player = player;
	}

	public final int getMaxLevel()
	{
		if(_player != null)
			return _player.getMaxLevel();
		else
		if(_isBase || _classId == 135 || _classId == 136)
			return Experience.getMaxLevel();
		else
			return Experience.getMaxSubLevel();
	}
}
