package l2n.game.model.entity;

import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.instancemanager.games.HandysBlockCheckerManager.ArenaParticipantsHolder;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.*;
import l2n.game.scripts.npc.L2BlockInstance;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Отвечает за прохождения самого ивента
 * 
 * @author L2System Project
 * @date 20.12.2010
 * @time 16:23:45
 */
public final class BlockCheckerEngine
{
	protected static final Logger _log = Logger.getLogger(BlockCheckerEngine.class.getName());
	// The object which holds all basic members info
	protected ArenaParticipantsHolder _holder;
	// Maps to hold player of each team and his points
	protected FastMap<L2Player, Integer> _redTeamPoints = new FastMap<L2Player, Integer>();
	protected FastMap<L2Player, Integer> _blueTeamPoints = new FastMap<L2Player, Integer>();
	// The initial points of the event
	protected int _redPoints = 15;
	protected int _bluePoints = 15;
	// Current used arena
	protected int _arena = -1;
	// All blocks
	protected GArray<L2Spawn> _spawns = new GArray<L2Spawn>();
	// Sets if the red team won the event at the end of this (used for packets)
	protected boolean _isRedWinner;
	// Time when the event starts. Used on packet sending
	protected long _startedTime;
	// The needed arena coordinates
	// Arena X: team1X, team1Y, team2X, team2Y, ArenaCenterX, ArenaCenterY
	protected static final int[][] _arenaCoordinates = {
			// Arena 0 - Team 1 XY, Team 2 XY - CENTER XY
			{ -58368, -62745, -57751, -62131, -58053, -62417 },
			// Arena 1 - Team 1 XY, Team 2 XY - CENTER XY
			{ -58350, -63853, -57756, -63266, -58053, -63551 },
			// Arena 2 - Team 1 XY, Team 2 XY - CENTER XY
			{ -57194, -63861, -56580, -63249, -56886, -63551 },
			// Arena 3 - Team 1 XY, Team 2 XY - CENTER XY
			{ -57200, -62727, -56584, -62115, -56850, -62391 } };
	// Common z coordinate
	protected static final int _zCoord = -2405;
	// Girl Npc
	protected L2NpcInstance _girlNpc;
	// List of dropped items in event (for later deletion)
	protected GArray<L2ItemInstance> _drops = new GArray<L2ItemInstance>();
	// Default arena
	protected static final byte DEFAULT_ARENA = -1;
	// Event is started
	protected boolean _isStarted = false;
	// Event end
	protected ScheduledFuture<?> _task;
	// Preserve from exploit reward by logging out
	protected boolean _abnormalEnd = false;

	public BlockCheckerEngine(HandysBlockCheckerManager.ArenaParticipantsHolder holder, int arena)
	{
		_holder = holder;
		if(arena > -1 && arena < 4)
			_arena = arena;

		for(L2Player player : holder.getRedPlayers())
			_redTeamPoints.put(player, 0);
		for(L2Player player : holder.getBluePlayers())
			_blueTeamPoints.put(player, 0);
	}

	/**
	 * Updates the player holder before the event starts
	 * to synchronize all info
	 * 
	 * @param holder
	 */
	public void updatePlayersOnStart(ArenaParticipantsHolder holder)
	{
		_holder = holder;
	}

	/**
	 * Returns the current holder object of this
	 * object engine
	 * 
	 * @return HandysBlockCheckerManager.ArenaParticipantsHolder
	 */
	public ArenaParticipantsHolder getHolder()
	{
		return _holder;
	}

	/**
	 * Will return the id of the arena used
	 * by this event
	 * 
	 * @return false;
	 */
	public int getArena()
	{
		return _arena;
	}

	/**
	 * Returns the time when the event
	 * started
	 * 
	 * @return long
	 */
	public long getStarterTime()
	{
		return _startedTime;
	}

	/**
	 * Returns the current red team points
	 * 
	 * @return int
	 */
	public int getRedPoints()
	{
		synchronized (this)
		{
			return _redPoints;
		}
	}

	/**
	 * Returns the current blue team points
	 * 
	 * @return int
	 */
	public int getBluePoints()
	{
		synchronized (this)
		{
			return _bluePoints;
		}
	}

	/**
	 * Returns the player points
	 * 
	 * @param player
	 * @param isRed
	 * @return int
	 */
	public int getPlayerPoints(L2Player player, boolean isRed)
	{
		if(!_redTeamPoints.containsKey(player) && !_blueTeamPoints.containsKey(player))
			return 0;

		if(isRed)
			return _redTeamPoints.get(player);
		else
			return _blueTeamPoints.get(player);
	}

	/**
	 * Increases player points for his teams
	 * 
	 * @param player
	 * @param team
	 */
	public synchronized void increasePlayerPoints(L2Player player, int team)
	{
		if(player == null)
			return;

		if(team == 0)
		{
			int points = _redTeamPoints.get(player) + 1;
			_redTeamPoints.put(player, points);
			_redPoints++;
			_bluePoints--;
		}
		else
		{
			int points = _blueTeamPoints.get(player) + 1;
			_blueTeamPoints.put(player, points);
			_bluePoints++;
			_redPoints--;
		}
	}

	/**
	 * Will add a new drop into the list of
	 * dropped items
	 * 
	 * @param item
	 */
	public void addNewDrop(L2ItemInstance item)
	{
		if(item != null)
			_drops.add(item);
	}

	/**
	 * Will return true if the event is alredy
	 * started
	 * 
	 * @return boolean
	 */
	public boolean isStarted()
	{
		return _isStarted;
	}

	/**
	 * Will send all packets for the event members with
	 * the relation info
	 */
	protected void broadcastRelationChanged(L2Player plr)
	{
		for(L2Player player : _holder.getAllPlayers())
			plr.sendPackets(RelationChanged.update(player, plr));
	}

	/**
	 * Called when a there is an empty team. The event
	 * will end.
	 */
	public void endEventAbnormally()
	{
		try
		{
			synchronized (this)
			{
				_isStarted = false;

				if(_task != null)
					_task.cancel(true);

				_abnormalEnd = true;

				L2GameThreadPools.getInstance().executeGeneral(new EndEvent());
			}
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Couldnt end Block Checker event at " + _arena, e);
		}
	}

	/**
	 * This inner class set ups all player
	 * and arena parameters to start the event
	 */
	public class StartEvent implements Runnable
	{
		// In event used skills
		private L2Skill _freeze, _transformationRed, _transformationBlue;
		// Common and unparametizer packet
		private final ExCubeGameCloseUI _closeUserInterface = new ExCubeGameCloseUI();

		public StartEvent()
		{
			// Initialize all used skills
			_freeze = SkillTable.getInstance().getInfo(6034, 1);
			_transformationRed = SkillTable.getInstance().getInfo(6035, 1);
			_transformationBlue = SkillTable.getInstance().getInfo(6036, 1);
		}

		/**
		 * Will set up all player parameters and
		 * port them to their respective location
		 * based on their teams
		 */
		private void setUpPlayers()
		{
			// Set current arena as being used
			HandysBlockCheckerManager.getInstance().setArenaBeingUsed(_arena);

			// Initialize packets avoiding create a new one per player
			_redPoints = _spawns.size() / 2;
			_bluePoints = _spawns.size() / 2;
			final ExCubeGameChangePoints initialPoints = new ExCubeGameChangePoints(300, _bluePoints, _redPoints);
			ExCubeGameExtendedChangePoints clientSetUp;

			for(L2Player player : _holder.getAllPlayers())
			{
				if(player == null)
					continue;

				// Send the secret client packet set up
				boolean isRed = _holder.getRedPlayers().contains(player);

				clientSetUp = new ExCubeGameExtendedChangePoints(300, _bluePoints, _redPoints, isRed, player, 0);
				player.sendPacket(clientSetUp);

				player.sendActionFailed();

				// Teleport Player - Array access
				// Team 0 * 2 = 0; 0 = 0, 0 1 = 1.
				// Team 1 * 2 = 2; 2 = 2, 2 1 = 3
				int tc = _holder.getPlayerTeam(player) * 2;
				// Get x and y coordinates
				int x = _arenaCoordinates[_arena][tc];
				int y = _arenaCoordinates[_arena][tc + 1];
				player.teleToLocation(x, y, _zCoord);
				// Set the player team
				if(isRed)
				{
					_redTeamPoints.put(player, 0);
					player.setTeam(2, true);
				}
				else
				{
					_blueTeamPoints.put(player, 0);
					player.setTeam(1, true);
				}
				player.stopAllEffects();
				// Give the player start up effects
				// Freeze
				_freeze.getEffects(player, player, false, false);

				// Tranformation
				if(_holder.getPlayerTeam(player) == 0)
					_transformationRed.getEffects(player, player, false, false);
				else
					_transformationBlue.getEffects(player, player, false, false);

				// Set the current player arena
				player.setHandysBlockCheckerEventArena(_arena);

				// Send needed packets
				player.sendPacket(initialPoints);
				player.sendPacket(_closeUserInterface);
				// ExBasicActionList
				player.sendPacket(ExBasicActionList.TRANSFORMED_ACTION_LIST);
				broadcastRelationChanged(player);
			}
		}

		@Override
		public void run()
		{
			// Wrong arena passed, stop event
			if(_arena == -1)
			{
				_log.severe("Couldnt set up the arena Id for the Block Checker event, cancelling event...");
				return;
			}
			_isStarted = true;
			// Spawn the blocks
			L2GameThreadPools.getInstance().executeGeneral(new SpawnRound(16, 1));
			// Start up player parameters
			setUpPlayers();
			// Set the started time
			_startedTime = System.currentTimeMillis() + 300000;
		}
	}

	/**
	 * This class spawns the second round of boxes
	 * and schedules the event end
	 */
	private class SpawnRound implements Runnable
	{
		private int _numOfBoxes;
		private int _round;

		public SpawnRound(int numberOfBoxes, int round)
		{
			_numOfBoxes = numberOfBoxes;
			_round = round;
		}

		@Override
		public void run()
		{
			if(!_isStarted)
				return;

			switch (_round)
			{
				case 1:
					// Schedule second spawn round
					_task = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRound(20, 2), 60000);
					break;
				case 2:
					// Schedule third spawn round
					_task = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRound(14, 3), 60000);
					break;
				case 3:
					// Schedule Event End
					_task = L2GameThreadPools.getInstance().scheduleGeneral(new EndEvent(), 180000);
					break;
			}
			// random % 2, if == 0 will spawn a red block
			// if != 0, will spawn a blue block
			byte random = 2;
			// common template
			final L2NpcTemplate template = NpcTable.getTemplate(18672);
			// Spawn blocks
			try
			{
				// Creates 50 new blocks
				for(int i = 0; i < _numOfBoxes; i++)
				{
					L2Spawn spawn = new L2Spawn(template);
					spawn.setLocx(_arenaCoordinates[_arena][4] + Rnd.get(-400, 400));
					spawn.setLocy(_arenaCoordinates[_arena][5] + Rnd.get(-400, 400));
					spawn.setLocz(_zCoord);
					spawn.setAmount(1);
					spawn.setHeading(1);
					spawn.init();
					spawn.stopRespawn();

					L2BlockInstance block = (L2BlockInstance) spawn.getLastSpawn();
					// switch color
					if(random % 2 == 0)
						block.setRed(true);
					else
						block.setRed(false);

					block.broadcastPacket(new NpcInfo(block, null));
					_spawns.add(spawn);
					random++;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			// Spawn the block carrying girl
			if(_round == 1 || _round == 2)
			{
				L2NpcTemplate girl = NpcTable.getTemplate(18676);
				try
				{
					final L2Spawn girlSpawn = new L2Spawn(girl);
					girlSpawn.setLocx(_arenaCoordinates[_arena][4] + Rnd.get(-400, 400));
					girlSpawn.setLocy(_arenaCoordinates[_arena][5] + Rnd.get(-400, 400));
					girlSpawn.setLocz(_zCoord);
					girlSpawn.setAmount(1);
					girlSpawn.setHeading(1);
					girlSpawn.init();
					girlSpawn.stopRespawn();

					_girlNpc = girlSpawn.getLastSpawn();

					// Schedule his deletion after 9 secs of spawn
					L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
					{
						@Override
						public void run()
						{
							if(_girlNpc == null)
								return;
							_girlNpc.deleteMe();
						}
					}, 9000);
				}
				catch(Exception e)
				{
					_log.warning("Couldnt Spawn Block Checker NPCs! Wrong instance type at npc table?");
				}
			}

			_redPoints = _numOfBoxes / 2;
			_bluePoints = _numOfBoxes / 2;

			int timeLeft = (int) ((getStarterTime() - System.currentTimeMillis()) / 1000);
			ExCubeGameChangePoints changePoints = new ExCubeGameChangePoints(timeLeft, getBluePoints(), getRedPoints());
			getHolder().broadCastPacketToTeam(changePoints);
		}
	}

	/**
	 * This class erase all event parameters on player
	 * and port them back near Handy. Also, unspawn
	 * blocks, runs a garbage collector and set as free
	 * the used arena
	 */
	private class EndEvent implements Runnable
	{
		// Garbage collector and arena free setter
		private void clearMe()
		{
			HandysBlockCheckerManager.getInstance().clearPaticipantQueueByArenaId(_arena);
			_holder.clearPlayers();
			_blueTeamPoints.clear();
			_redTeamPoints.clear();
			HandysBlockCheckerManager.getInstance().setArenaFree(_arena);

			for(L2Spawn spawn : _spawns)
			{
				spawn.despawnAll();
				spawn = null;
			}
			_spawns.clear();

			for(L2ItemInstance item : _drops)
			{
				// npe
				if(item == null)
					continue;

				// a player has it, it will be deleted later
				if(!item.isVisible() || item.getOwnerId() != 0)
					continue;

				item.deleteMe();
			}
			_drops.clear();
		}

		/**
		 * Reward players after event.
		 * Tie - No Reward
		 */
		private void rewardPlayers()
		{
			if(_redPoints == _bluePoints)
				return;

			_isRedWinner = _redPoints > _bluePoints ? true : false;

			if(_isRedWinner)
			{
				rewardAsWinner(true);
				rewardAsLooser(false);
			}
			else
			{
				rewardAsWinner(false);
				rewardAsLooser(true);
			}
		}

		/**
		 * Reward the speicifed team as a winner team
		 * 1) Higher score - 8 extra
		 * 2) Higher score - 5 extra
		 * 
		 * @param isRed
		 */
		private void rewardAsWinner(boolean isRed)
		{
			FastMap<L2Player, Integer> tempPoints = isRed ? _redTeamPoints : _blueTeamPoints;

			// Main give
			for(L2Player pc : tempPoints.keySet())
			{
				if(pc == null)
					continue;

				if(tempPoints.get(pc) >= 10)
					pc.addItem(13067, 2, 0, "Block Checker");
				else
					tempPoints.remove(pc);
			}

			int first = 0, second = 0;
			L2Player winner1 = null, winner2 = null;
			for(L2Player pc : tempPoints.keySet())
			{
				int pcPoints = tempPoints.get(pc);
				if(pcPoints > first)
				{
					// Move old data
					second = first;
					winner2 = winner1;
					// Set new data
					first = pcPoints;
					winner1 = pc;
				}
				else if(pcPoints > second)
				{
					second = pcPoints;
					winner2 = pc;
				}
			}
			if(winner1 != null)
				winner1.addItem(13067, 8, 0, "Block Checker");
			if(winner2 != null)
				winner2.addItem(13067, 5, 0, "Block Checker");
		}

		/**
		 * Will reward the looser team with the
		 * predefined rewards
		 * Player got >= 10 points: 2 coins
		 * Player got < 10 points: 0 coins
		 * 
		 * @param isRed
		 */
		private void rewardAsLooser(boolean isRed)
		{
			FastMap<L2Player, Integer> tempPoints = isRed ? _redTeamPoints : _blueTeamPoints;
			for(L2Player player : tempPoints.keySet())
				if(player != null && tempPoints.get(player) >= 10)
					player.addItem(13067, 2, 0, "Block Checker");
		}

		/**
		 * Telport players back, give status back and
		 * send final packet
		 */
		private void setPlayersBack()
		{
			final ExCubeGameEnd end = new ExCubeGameEnd(_isRedWinner);

			for(L2Player player : _holder.getAllPlayers())
			{
				if(player == null)
					continue;

				player.stopAllEffects();
				// Remove team aura
				player.setTeam(0, false);

				// Set default arena
				player.setHandysBlockCheckerEventArena(DEFAULT_ARENA);

				// Remove the event items
				PcInventory inv = player.getInventory();
				if(inv.getItemByItemId(13787) != null)
				{
					long count = inv.getCountOf(13787);
					inv.destroyItemByItemId(13787, count, true);
				}
				if(inv.getItemByItemId(13788) != null)
				{
					long count = inv.getCountOf(13788);
					inv.destroyItemByItemId(13788, count, true);
				}
				broadcastRelationChanged(player);

				// Teleport Back
				player.teleToLocation(-57478, -60367, -2370);

				// Send end packet
				player.sendPacket(end);
				player.broadcastUserInfo(true);
			}
		}

		@Override
		public void run()
		{
			if(!_abnormalEnd)
				rewardPlayers();
			setPlayersBack();
			clearMe();
			_isStarted = false;
			_abnormalEnd = false;
		}
	}
}
