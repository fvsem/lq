package l2n.game.model.entity.siege.clanhall;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.ClanHallSiegeManager;
import l2n.game.instancemanager.SiegeGuardManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.siege.*;
import l2n.game.model.instances.L2SiegeBossInstance;
import l2n.game.network.serverpackets.SiegeInfo;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;
import l2n.game.tables.MapRegionTable.TeleportWhereType;
import l2n.game.tables.NpcTable;
import l2n.util.Util;

import java.util.Calendar;

public class ClanHallSiege extends Siege
{
	private GArray<L2SiegeBossInstance> _siegeBosses = new GArray<L2SiegeBossInstance>();

	private final int addRepWin = Config.CLANREP_SIEGE_CH_WIN;
	private final int addRepLoss = Config.CLANREP_SIEGE_CH_LOSS;
	private final int addRepBadAttack = Config.CLANREP_SIEGE_CH_BADATTACK;

	public ClanHallSiege(ClanHall siegeUnit)
	{
		super(siegeUnit);
		_database = new ClanHallSiegeDatabase(this);
		_siegeGuardManager = new SiegeGuardManager(getSiegeUnit());
		_database.loadSiegeClan();
	}

	@Override
	public void startSiege()
	{
		if(!_isInProgress)
		{
			_database.loadSiegeClan();

			int oldOwner = getSiegeUnit().getOwnerId();
			if(oldOwner > 0)
			{
				getSiegeUnit().changeOwner(null);
				addSiegeClan(oldOwner, SiegeClanType.ATTACKER);
			}

			if(getAttackerClans().isEmpty())
			{
				if(getSiegeUnit().getOwnerId() <= 0)
					announceToPlayer(new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_BEEN_CANCELED_DUE_TO_LACK_OF_INTEREST).addString(getSiegeUnit().getName()), false, ANNOUNCE_TO_BOTH_SIDES);
				else
					announceToPlayer(new SystemMessage(SystemMessage.S1S_SIEGE_WAS_CANCELED_BECAUSE_THERE_WERE_NO_CLANS_THAT_PARTICIPATED).addString(getSiegeUnit().getName()), false, ANNOUNCE_TO_BOTH_SIDES);
				return;
			}

			getZone().setActive(true);

			_isInProgress = true;
			_isMidVictory = true;

			updateSiegeClans();
			updatePlayerSiegeStateFlags(false);

			teleportPlayer(TeleportWhoType.Attacker, TeleportWhereType.ClosestTown);
			teleportPlayer(TeleportWhoType.Spectator, TeleportWhereType.ClosestTown);

			spawnSiegeBosses();
			getSiegeUnit().spawnDoor();
			getSiegeGuardManager().spawnSiegeGuard();

			_siegeEndDate = Calendar.getInstance();
			_siegeEndDate.add(Calendar.MINUTE, getSiegeLength());
			L2GameThreadPools.getInstance().scheduleGeneral(new SiegeEndTask(this), 1000);

			_fameTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SiegeFameTask(), 300000, 300000);

			announceToPlayer(new SystemMessage(SystemMessage.THE_SIEGE_OF_THE_CLAN_HALL_HAS_BEGUN), false, ANNOUNCE_TO_BOTH_SIDES);
		}
	}

	@Override
	public void midVictory()
	{
		if(!isInProgress() || getSiegeUnit().getOwnerId() <= 0)
			return;

		SiegeClan sc_newowner = getAttackerClan(getSiegeUnit().getOwner());
		removeSiegeClan(sc_newowner, SiegeClanType.ATTACKER);
		addSiegeClan(sc_newowner, SiegeClanType.OWNER);

		endSiege();
	}

	@Override
	public void engrave(L2Clan clan, int objId)
	{
		if(clan != null)
		{
			getSiegeUnit().changeOwner(clan);
			midVictory();
		}
		else
			endSiege();
	}

	@Override
	public void addSiegeClan(SiegeClan sc, SiegeClanType type)
	{
		if(type == SiegeClanType.ATTACKER)
			super.addSiegeClan(sc, type);
	}

	@Override
	public void registerDefender(L2Player player, boolean force)
	{}

	@Override
	public void endSiege()
	{
		getZone().setActive(false);

		if(isInProgress())
		{
			announceToPlayer(new SystemMessage(SystemMessage.THE_SIEGE_OF_THE_CLAN_HALL_IS_FINISHED), false, ANNOUNCE_TO_BOTH_SIDES);

			if(getSiegeUnit().getOwnerId() <= 0)
				announceToPlayer(new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_ENDED_IN_A_DRAW).addString(getSiegeUnit().getName()), false, ANNOUNCE_TO_BOTH_SIDES);
			else
			{
				L2Clan newOwner = ClanTable.getInstance().getClan(getSiegeUnit().getOwnerId());

				if(newOwner.getLevel() >= 5)
					newOwner.broadcastToOnlineMembers(new SystemMessage(1774).addNumber(newOwner.incReputation(addRepWin, true, "ClanHallSiege")));
			}

			unspawnSiegeBosses();

			removeHeadquarters();

			teleportPlayer(TeleportWhoType.Attacker, TeleportWhereType.ClosestTown);
			teleportPlayer(TeleportWhoType.Spectator, TeleportWhereType.ClosestTown);

			removeSiegeSummons();

			_isInProgress = false;
			updatePlayerSiegeStateFlags(true);
			saveSiege();

			_database.clearSiegeClan();
			getSiegeGuardManager().unspawnSiegeGuard();
			getSiegeUnit().spawnDoor();
			clearSiegeClans();

			if(_siegeStartTask != null)
			{
				_siegeStartTask.cancel(false);
				_siegeStartTask = null;
			}
			if(_fameTask != null)
			{
				_fameTask.cancel(true);
				_fameTask = null;
			}

			setRegistrationOver(false);
		}
	}

	@Override
	protected void correctSiegeDateTime()
	{
		boolean corrected = false;
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
		{
			// Since siege has past reschedule it to the next one (14 days)
			// This is usually caused by server being down
			corrected = true;
			setNextSiegeDate();
		}
		if(_siegeDate.get(Calendar.DAY_OF_WEEK) != getSiegeUnit().getSiegeDayOfWeek())
		{
			corrected = true;
			_siegeDate.set(Calendar.DAY_OF_WEEK, getSiegeUnit().getSiegeDayOfWeek());
		}
		if(_siegeDate.get(Calendar.HOUR_OF_DAY) != getSiegeUnit().getSiegeHourOfDay())
		{
			corrected = true;
			_siegeDate.set(Calendar.HOUR_OF_DAY, getSiegeUnit().getSiegeHourOfDay());
		}
		_siegeDate.set(Calendar.MINUTE, 0);
		if(corrected)
			_database.saveSiegeDate();
	}

	@Override
	public void listRegisterClan(L2Player player)
	{
		player.sendPacket(new SiegeInfo(getSiegeUnit()));
	}

	@Override
	protected void setNextSiegeDate()
	{
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
		{
			// Set next siege date if siege has passed
			_siegeDate.add(Calendar.DAY_OF_MONTH, Config.CLANHALL_SIEGE_DAY); // Schedule to happen in 14 days
			if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
				setNextSiegeDate(); // Re-run again if still in the pass
		}
	}

	@Override
	public void startAutoTask(boolean isServerStarted)
	{
		if(_siegeStartTask != null)
			return;
		correctSiegeDateTime();

		_siegeRegistrationEndDate = Calendar.getInstance();
		_siegeRegistrationEndDate.setTimeInMillis(_siegeDate.getTimeInMillis());
		_siegeRegistrationEndDate.add(Calendar.DATE, -1);

		// Если сервер только что стартовал, осада начнется не ранее чем через час
		if(isServerStarted)
		{
			Calendar minDate = Calendar.getInstance();
			minDate.add(Calendar.HOUR_OF_DAY, 1);
			_siegeDate.setTimeInMillis(Math.max(minDate.getTimeInMillis(), _siegeDate.getTimeInMillis()));
			_database.saveSiegeDate();

			// Если был рестарт во время осады, даем зарегистрироваться еще раз
			if(_siegeDate.getTimeInMillis() <= minDate.getTimeInMillis())
			{
				setRegistrationOver(false);
				_siegeRegistrationEndDate.setTimeInMillis(_siegeDate.getTimeInMillis());
				_siegeRegistrationEndDate.add(Calendar.MINUTE, -10);
			}
		}

		_log.info("Siege of " + getSiegeUnit().getName() + " - " + Util.datetimeFormatter.format(_siegeDate.getTime()));

		_siegeStartTask = L2GameThreadPools.getInstance().scheduleGeneral(new SiegeStartTask(this), 1000);
	}

	@Override
	protected void saveSiege()
	{
		setNextSiegeDate();
		_database.saveSiegeDate();
		startAutoTask(false);
	}

	public void killedSiegeBoss(L2SiegeBossInstance boss)
	{
		if(boss.getNpcId() == 35408)
			Functions.npcShout(boss, "Умереть еще раз ? поражения позор .. Но трагедия не закончилась ..");
		else if(boss.getNpcId() == 35409)
			Functions.npcShout(boss, "Это моя граница .. Но разрешение у Гюстава, я могу умереть но вам не пройти!");
		else if(boss.getNpcId() == 35410)
		{
			Functions.npcShout(boss, "День .. Неожиданно побежден? Но я, конечно, могу снова вернуться!");
			engrave(boss.getWinner(), boss.getObjectId());
		}
		else if(boss.getNpcId() == 35368)
			engrave(boss.getWinner(), boss.getObjectId());
		else if(boss.getNpcId() == 35629)
			engrave(boss.getWinner(), boss.getObjectId());
		_siegeBosses.remove(boss);
	}

	private void unspawnSiegeBosses()
	{
		for(L2SiegeBossInstance siegeBoss : _siegeBosses)
			if(siegeBoss != null)
				siegeBoss.deleteMe();
		_siegeBosses.clear();
	}

	private void spawnSiegeBosses()
	{
		for(SiegeSpawn sp : ClanHallSiegeManager.getSiegeBossSpawnList(getSiegeUnit().getId()))
		{
			L2SiegeBossInstance siegeBoss = new L2SiegeBossInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(sp.getNpcId()));
			siegeBoss.setCurrentHpMp(siegeBoss.getMaxHp(), siegeBoss.getMaxMp(), true);
			siegeBoss.setXYZInvisible(sp.getLoc().correctGeoZ());
			siegeBoss.setSpawnedLoc(siegeBoss.getLoc());
			siegeBoss.setHeading(sp.getLoc().h);
			siegeBoss.spawnMe();
			_siegeBosses.add(siegeBoss);
			if(sp.getNpcId() == 35408)
				Functions.npcShout(siegeBoss, "Солдаты Густова, в бой! Довести  захватчика до смерти!");
			if(sp.getNpcId() == 35409)
				Functions.npcShout(siegeBoss, "Королевство Аденского льва, благодарны! Предоставления не умирает. Густав Благодарен!");
			if(sp.getNpcId() == 35410)
				Functions.npcShout(siegeBoss, "Приходиться понимать! Вы эти зарубежные захватчики земель! Этот форт навсегда наш, мой Густав поднимает меч!");
		}
	}
}
