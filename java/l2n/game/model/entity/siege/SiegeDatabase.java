package l2n.game.model.entity.siege;

import javolution.util.FastMap;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Clan;
import l2n.game.model.entity.residence.ClanHall;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class SiegeDatabase
{
	protected final static Logger _log = Logger.getLogger(SiegeDatabase.class.getName());

	protected Siege _siege;

	public SiegeDatabase(Siege siege)
	{
		_siege = siege;
	}

	public abstract void saveSiegeDate();

	public void saveLastSiegeDate()
	{}

	/**
	 * Return true if the clan is registered or owner of a castle<BR>
	 * <BR>
	 * 
	 * @param clan
	 *            The L2Clan of the player
	 */
	public static boolean checkIsRegistered(L2Clan clan, int unitid)
	{
		if(clan == null)
			return false;

		if(unitid > 0 && clan.getHasCastle() == unitid)
			return true;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		boolean register = false;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT clan_id FROM siege_clans where clan_id=?" + (unitid == 0 ? "" : " and unit_id=?"));
			statement.setInt(1, clan.getClanId());
			if(unitid != 0)
				statement.setInt(2, unitid);
			rset = statement.executeQuery();
			if(rset.next())
				register = true;
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: checkIsRegistered(): ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return register;
	}

	public void clearSiegeClan()
	{
		int unit_id = _siege.getSiegeUnit().getId();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM siege_clans WHERE unit_id=?");
			statement.setInt(1, unit_id);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: clearSiegeClan(): " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		_siege.getAttackerClans().clear();
		_siege.getDefenderClans().clear();
		_siege.getDefenderWaitingClans().clear();
		_siege.getDefenderRefusedClans().clear();
	}

	public void clearSiegeClan(SiegeClanType type)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM siege_clans WHERE unit_id=? and type=?");
			statement.setInt(1, _siege.getSiegeUnit().getId());
			statement.setInt(2, type.getId());
			statement.execute();

			_siege.getSiegeClans(type).clear();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: clearSiegeClan(): ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void removeSiegeClan(int clanId)
	{
		if(clanId <= 0)
			return;

		int unit_id = _siege.getSiegeUnit().getId();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM siege_clans WHERE unit_id=? and clan_id=?");
			statement.setInt(1, unit_id);
			statement.setInt(2, clanId);
			statement.execute();
			loadSiegeClan();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: removeSiegeClan(): ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void loadSiegeClan()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			FastMap<SiegeClanType, FastMap<Integer, SiegeClan>> clans = _siege.getSiegeClanList();
			clans.put(SiegeClanType.ATTACKER, new FastMap<Integer, SiegeClan>().shared());
			clans.put(SiegeClanType.DEFENDER, new FastMap<Integer, SiegeClan>().shared());
			clans.put(SiegeClanType.DEFENDER_REFUSED, new FastMap<Integer, SiegeClan>().shared());
			clans.put(SiegeClanType.DEFENDER_WAITING, new FastMap<Integer, SiegeClan>().shared());

			// Add castle owner as defender
			if(_siege.getSiegeUnit().getOwnerId() > 0 && !(_siege.getSiegeUnit() instanceof ClanHall))
				_siege.addSiegeClan(_siege.getSiegeUnit().getOwnerId(), SiegeClanType.OWNER);

			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT clan_id, type FROM siege_clans where unit_id = ?");
			statement.setInt(1, _siege.getSiegeUnit().getId());
			rset = statement.executeQuery();
			while (rset.next())
				_siege.addSiegeClan(rset.getInt("clan_id"), SiegeClanType.getById(rset.getInt("type")));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: loadSiegeClan(): ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void saveSiegeClan(L2Clan clan, int typeId)
	{
		if(clan == null)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO siege_clans (clan_id,unit_id,type) values (?,?,?)");
			statement.setInt(1, clan.getClanId());
			statement.setInt(2, _siege.getSiegeUnit().getId());
			statement.setInt(3, typeId);
			statement.execute();
			_siege.addSiegeClan(clan.getClanId(), SiegeClanType.getById(typeId));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Exception: saveSiegeClan: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
