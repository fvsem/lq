package l2n.game.model.entity.siege.territory;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.configuration.L2Properties;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.sql.ResultSet;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @project L2_NextGen
 * @time 10:22:13
 * @date 16.04.2010
 */
public class TerritorySiegeDatabase
{
	private final static Logger _log = Logger.getLogger(TerritorySiegeDatabase.class.getName());

	private final static TIntObjectHashMap<GArray<SiegeSpawn>> _flagSpawnList = new TIntObjectHashMap<GArray<SiegeSpawn>>();
	/** менеджеры в городах */
	private final static TIntObjectHashMap<GArray<L2Spawn>> _npcsSpawnList = new TIntObjectHashMap<GArray<L2Spawn>>();
	/** гварды */
	private final static TIntObjectHashMap<GArray<L2Spawn>> _guardsSpawnList = new TIntObjectHashMap<GArray<L2Spawn>>();
	/** катапульты */
	private final static TIntObjectHashMap<GArray<L2Spawn>> _catapultsSpawnList = new TIntObjectHashMap<GArray<L2Spawn>>();

	/**
	 * Загружает списки зарегистрированных на ТВ (кланы и игроков)
	 */
	public static void loadSiegeMembers()
	{
		// Сначало чистим списки
		TerritorySiege.getPlayers().clear();
		TerritorySiege.getClans().clear();

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT obj_Id, side, type FROM siege_territory_members");

			while (rset.next())
				if(rset.getInt("type") == 0) // если игрок
					TerritorySiege.getPlayers().put(rset.getInt("obj_Id"), rset.getInt("side"));
				else
					// иначе добавлем в список кланов
					TerritorySiege.getClans().put(new SiegeClan(rset.getInt("obj_Id"), null), rset.getInt("side"));
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TerritorySiegeDatabase loadSiegeMembers: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * 
	 */
	public static void saveSiegeMembers()
	{
		clearSiegeMembers();
		for(final Entry<Integer, Integer> entry : TerritorySiege.getPlayers().entrySet())
			changeRegistration(entry.getKey(), entry.getValue(), 0, false);
		for(final Entry<SiegeClan, Integer> entry : TerritorySiege.getClans().entrySet())
			changeRegistration(entry.getKey().getClanId(), entry.getValue(), 1, false);
	}

	/**
	 * Добавляет в БД регистрацию на ТВ
	 * 
	 * @param obj_Id
	 *            - id игрока или клана
	 * @param side
	 *            - за какую сторону
	 * @param type
	 *            0 - один игрок, 1 - клан
	 * @param delete
	 *            TODO
	 */
	public static void changeRegistration(final int obj_Id, final int side, final int type, final boolean delete)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			if(delete)
			{
				statement = con.prepareStatement("DELETE FROM `siege_territory_members` WHERE `obj_Id`=? AND `type`=?");
				statement.setInt(1, obj_Id);
				statement.setInt(2, type);
			}
			else
			{
				statement = con.prepareStatement("INSERT INTO `siege_territory_members` (obj_Id, side, type) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE side = ?, type = ?");
				statement.setInt(1, obj_Id);
				statement.setInt(2, side);
				statement.setInt(3, type);
				statement.setInt(4, side);
				statement.setInt(5, type);
			}
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TerritorySiegeDatabase saveSiegeMember: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Отчищает в БД всех зарегистрированных на территориальные войны
	 */
	public static void clearSiegeMembers()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM siege_territory_members");
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TerritorySiegeDatabase clearSiegeMembers: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * загружает список спауна флагов для городов
	 */
	public static void loadSiegeFlags()
	{
		try
		{
			final L2Properties siegeSettings = new L2Properties(Config.SIEGE_TERRITORY_CONFIGURATION_FILE);
			for(final Castle castle : CastleManager.getInstance().getCastles().values())
			{
				final int flagItemId = siegeSettings.getInteger(castle.getName() + "FlagItemId", 0);
				final int flagNpcId = siegeSettings.getInteger(castle.getName() + "FlagNpcId", 0);
				final String spawnParams = siegeSettings.getProperty(castle.getName() + "FlagPos", "");

				if(spawnParams.length() > 0)
				{
					final StringTokenizer st = new StringTokenizer(spawnParams.trim(), ",");
					final int xc = Integer.parseInt(st.nextToken());
					final int yc = Integer.parseInt(st.nextToken());
					final int zc = Integer.parseInt(st.nextToken());

					final GArray<SiegeSpawn> flagSpawns = new GArray<SiegeSpawn>();
					for(int x = xc - 150; x <= xc + 150; x += 150)
						for(int y = yc - 150; y <= yc + 150; y += 150)
							flagSpawns.add(new SiegeSpawn(castle.getId(), new Location(x, y, zc), flagNpcId, flagItemId));
					_flagSpawnList.put(castle.getId(), flagSpawns);
				}
				else
					_log.warning("Not found flags for " + castle.getName());
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TerritorySiegeDatabase loadSiegeFlags: ", e);
		}
	}

	/**
	 * загружает список спауна npc для городов
	 */
	public static void loadNpcsSpawnList()
	{
		loadSpawnList(0, _npcsSpawnList);
		_log.info("TerritorySiege: load " + _npcsSpawnList.size() + " town spawns.");
	}

	/**
	 * загружает список спауна охраны (Castle spawns)
	 */
	public static void loadSiegeGuardsSpawnList()
	{
		loadSpawnList(1, _guardsSpawnList);

		int count = 0;
		for(final GArray<L2Spawn> list : _guardsSpawnList.valueCollection())
			count += list.size();
		_log.info("TerritorySiege: load " + count + " castle spawns.");
	}

	/**
	 * загружает список спауна катапульт (fortress spawns)
	 */
	public static void loadSiegeCatapultsSpawnList()
	{
		loadSpawnList(2, _catapultsSpawnList);
		_log.info("TerritorySiege: load " + _catapultsSpawnList.size() + " fortress spawns.");
	}

	/**
	 * загружает список спауна
	 * 
	 * @param type
	 *            - 0 town npcs (spawned when castle have owner), 1-fortress spawns in TW time, 2-Castle spawns in TW time
	 * @param result
	 *            - добавляет сюда спаун
	 */
	private static void loadSpawnList(final int type, final TIntObjectHashMap<GArray<L2Spawn>> result)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rs = null;
		try
		{
			for(final Castle castle : CastleManager.getInstance().getCastles().values())
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.createStatement();
				rs = statement.executeQuery("SELECT * FROM territory_spawnlist WHERE castleId = '" + castle.getId() + "' AND spawnType = '" + type + "'");

				final GArray<L2Spawn> spawns = new GArray<L2Spawn>();
				L2Spawn spawn;
				L2NpcTemplate template;
				while (rs.next())
				{
					final int npcId = rs.getInt("npcId");
					final Location loc = new Location(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"), rs.getInt("heading"));

					template = NpcTable.getTemplate(npcId);
					if(template != null)
					{
						spawn = new L2Spawn(template);
						spawn.setAmount(1);
						spawn.setLoc(loc);
						spawn.setRespawnDelay(0);
						spawn.setLocation(0);

						spawns.add(spawn);
					}
					else
						_log.warning("Error loading siege guard, missing npc data in npc table for id: " + npcId);

				}
				result.put(castle.getId(), spawns);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TerritorySiegeDatabase loadSiegeGuards: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * @return список спауна флагов для городов
	 */
	public static TIntObjectHashMap<GArray<SiegeSpawn>> getSiegeFlags()
	{
		return _flagSpawnList;
	}

	/**
	 * @return список спауна нпс в городах (спауняться если замком города владеют)
	 */
	public static TIntObjectHashMap<GArray<L2Spawn>> getNpcsSpawnList()
	{
		return _npcsSpawnList;
	}

	/**
	 * @return список спауна гвардов
	 */
	public static TIntObjectHashMap<GArray<L2Spawn>> getGuardsSpawnList()
	{
		return _guardsSpawnList;
	}

	/**
	 * @return список спауна катапульт
	 */
	public static TIntObjectHashMap<GArray<L2Spawn>> getCatapultsSpawnList()
	{
		return _catapultsSpawnList;
	}
}
