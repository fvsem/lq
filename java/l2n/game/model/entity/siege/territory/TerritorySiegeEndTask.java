package l2n.game.model.entity.siege.territory;

import l2n.game.L2GameThreadPools;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.Calendar;

/**
 * @author L2System Project
 * @project L2_NextGen
 * @time 10:22:13
 * @date 16.04.2010
 */
public class TerritorySiegeEndTask implements Runnable
{
	@Override
	public void run()
	{
		if(!TerritorySiege.isInProgress())
			return;
		try
		{
			long timeRemaining = TerritorySiege.getSiegeEndDate().getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
			if(timeRemaining > 3600000)
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 3600000);
			else if(timeRemaining <= 3600000 && timeRemaining > 1800000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.THE_TERRITORY_WAR_WILL_END_IN_S1_HOURS).addNumber(1), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 1800000);
			}
			else if(timeRemaining <= 1800000 && timeRemaining > 600000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.THE_TERRITORY_WAR_WILL_END_IN_S1_MINUTES).addNumber(30), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 600000);
			}
			else if(timeRemaining <= 600000 && timeRemaining > 300000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.THE_TERRITORY_WAR_WILL_END_IN_S1_MINUTES).addNumber(10), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 300000);
			}
			else if(timeRemaining <= 300000 && timeRemaining > 60000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.THE_TERRITORY_WAR_WILL_END_IN_S1_MINUTES).addNumber(5), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 60000);
			}
			else if(timeRemaining <= 60000 && timeRemaining > 30000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.THE_TERRITORY_WAR_WILL_END_IN_S1_MINUTES).addNumber(1), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 30000);
			}
			else if(timeRemaining <= 30000 && timeRemaining > 10000)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECONDS_TO_THE_END_OF_TERRITORY_WAR).addNumber(30), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining - 10000);
			}
			else if(timeRemaining <= 10000 && timeRemaining > 0)
			{
				TerritorySiege.announceToPlayer(new SystemMessage(SystemMessage.S1_SECONDS_TO_THE_END_OF_TERRITORY_WAR).addNumber(Math.round(timeRemaining / 1000) + 1), true);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), timeRemaining);
			}
			else
				TerritorySiege.endSiege();
		}
		catch(Throwable t)
		{}
	}
}
