package l2n.game.model.entity.siege.territory;

import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;

/**
 * @author L2System Project
 * @project L2_NextGen
 * @time 10:22:13
 * @date 16.04.2010
 */
public class TerritorySiegeStartTask implements Runnable
{
	@Override
	public void run()
	{
		if(TerritorySiege.isInProgress())
			return;
		try
		{
			long timeRemaining = TerritorySiege.getSiegeDate().getTimeInMillis() - System.currentTimeMillis();
			if(timeRemaining > 7200000)
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining - 7200000);
			else if(timeRemaining <= 7200000 && timeRemaining > 1200000)
			{
				checkRegistrationOver();
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining - 1200000);
			}
			else if(timeRemaining <= 1200000 && timeRemaining > 600000)
			{
				checkRegistrationOver();
				TerritorySiege.announceToPlayer(Msg.THE_TERRITORY_WAR_WILL_BEGIN_IN_20_MINUTES_TERRITORY_RELATED_FUNCTIONS_IE__BATTLEFIELD_CHANNEL, false);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining - 600000);
			}
			else if(timeRemaining <= 600000 && timeRemaining > 300000)
			{
				checkRegistrationOver();
				TerritorySiege.announceToPlayer(Msg.TERRITORY_WAR_BEGINS_IN_10_MINUTES, false);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining - 300000);
			}
			else if(timeRemaining <= 300000 && timeRemaining > 60000)
			{
				checkRegistrationOver();
				TerritorySiege.announceToPlayer(Msg.TERRITORY_WAR_BEGINS_IN_5_MINUTES, false);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining - 60000);
			}
			else if(timeRemaining <= 60000 && timeRemaining > 0)
			{
				checkRegistrationOver();
				TerritorySiege.announceToPlayer(Msg.TERRITORY_WAR_BEGINS_IN_1_MINUTE, false);
				L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), timeRemaining);
			}
			else
				TerritorySiege.startSiege();
		}
		catch(Throwable t)
		{}
	}

	private void checkRegistrationOver()
	{
		if(!TerritorySiege.isRegistrationOver() && TerritorySiege.getSiegeRegistrationEndDate().getTimeInMillis() - System.currentTimeMillis() <= 10000)
		{
			TerritorySiege.announceToPlayer(Msg.THE_TERRITORY_WAR_REQUEST_PERIOD_HAS_ENDED, false);
			TerritorySiege.setRegistrationOver(true);
		}
	}
}
