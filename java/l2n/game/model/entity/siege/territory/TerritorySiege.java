package l2n.game.model.entity.siege.territory;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.*;
import l2n.game.model.*;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2TerritoryFlagInstance;
import l2n.game.model.quest.Quest;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.tables.ClanTable;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Util;

import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

/**
 * http://www.linedia.ru/wiki/Territory_Battles
 * http://www.linedia.ru/wiki/Territory_Battles/Practical_Guide
 * Отвечает за прохождение территориальных войн
 * 
 * @author L2System Project
 * @project L2_NextGen
 * @time 10:22:13
 * @date 16.04.2010
 */
public class TerritorySiege
{
	private final static Logger _log = Logger.getLogger(TerritorySiege.class.getName());

	public static final int[] TERRITORY_SKILLS = { 0, 848, 849, 850, 851, 852, 853, 854, 855, 856 };

	private static final int _controlTowerLosePenalty = 20000;
	private static final int _defenderRespawnDelay = 20000;

	private static boolean _isInProgress = false;
	private static boolean _isRegistrationOver = false;
	private static Calendar _siegeDate;
	private static Calendar _siegeEndDate;
	private static Calendar _siegeRegistrationEndDate;
	private static ScheduledFuture<TerritorySiegeStartTask> _siegeStartTask;
	private static ScheduledFuture<TerritorySiegeFameTask> _fameTask;

	/**
	 * Список игроков, где: <BR>
	 * <b>key</b> - objId игрока<BR>
	 * <b>value</b> - id территории
	 */
	private final static Map<Integer, Integer> _players = new ConcurrentHashMap<Integer, Integer>();
	private final static Map<SiegeClan, Integer> _clans = new ConcurrentHashMap<SiegeClan, Integer>();

	private final static GArray<Castle> _castles = new GArray<Castle>();
	private final static GArray<Fortress> _fortress = new GArray<Fortress>();

	private final static GArray<L2TerritoryFlagInstance> _flags = new GArray<L2TerritoryFlagInstance>();

	private final static Map<Integer, Location> _wardsLoc = new ConcurrentHashMap<Integer, Location>();
	private final static Map<Integer, Integer> _defenderRespawnPenalty = new ConcurrentHashMap<Integer, Integer>();

	private static Quest _tw_quest = null;

	// TODO Все Ward успешно доставленные с чужих территорий остаются в захватившем их замке до следующей Территориальной Битвы. Если замок потерял свою Ward, он должен перезахватить её в следующей Территориальной Битве, если хочет вернуть назад.
	// TODO Территориальная Катапульта: Как только она будет разрушена, все NPC-защитники замка исчезают, все защитные функции замка отключаются и все ворота замка открываются

	public static void load()
	{
		_siegeDate = Calendar.getInstance();
		_siegeDate.setTimeInMillis(ServerVariables.getLong("TerritorySiegeDate", 0));

		_castles.addAll(CastleManager.getInstance().getCastles().values());
		_fortress.addAll(FortressManager.getInstance().getFortresses().values());

		// загружаем списки зарегистрированных
		TerritorySiegeDatabase.loadSiegeMembers();
		// загружаем флаги
		TerritorySiegeDatabase.loadSiegeFlags();

		TerritorySiegeDatabase.loadNpcsSpawnList();
		TerritorySiegeDatabase.loadSiegeGuardsSpawnList();
		TerritorySiegeDatabase.loadSiegeCatapultsSpawnList();

		spawnNpcInTown();

		L2Zone zone;
		for(final Castle unitId : _castles)
		{
			zone = getZone(unitId);
			if(zone != null)
				zone.setActive(false);
		}

		for(final Fortress unitId : _fortress)
		{
			zone = getZone(unitId);
			if(zone != null)
				zone.setActive(false);
		}

		for(final Castle unitId : _castles)
			_defenderRespawnPenalty.put(unitId.getId(), 0);

		startAutoTask();
	}

	public static void registerClan(final int territoryId, final SiegeClan clan)
	{
		if(clan == null)
			return;
		_clans.put(clan, territoryId);
		TerritorySiegeDatabase.changeRegistration(clan.getClanId(), territoryId, 1, false);
	}

	public static void registerPlayer(final int territoryId, final L2Player player)
	{
		if(player == null)
			return;
		_players.put(player.getObjectId(), territoryId);
		TerritorySiegeDatabase.changeRegistration(player.getObjectId(), territoryId, 0, false);
	}

	public static void removeClan(final SiegeClan clan)
	{
		if(clan == null)
			return;
		_clans.remove(clan);
		TerritorySiegeDatabase.changeRegistration(clan.getClanId(), -1, 1, true);
	}

	public static void removePlayer(final L2Player player)
	{
		if(player == null)
			return;
		_players.remove(player.getObjectId());
		TerritorySiegeDatabase.changeRegistration(player.getObjectId(), -1, 0, true);
	}

	/**
	 * Возвращает список игроков участвующих в территориальной битве
	 */
	public static Map<Integer, Integer> getPlayers()
	{
		return _players;
	}

	/**
	 * Возвращает количество игроков зарегестрированных на битву с территорией <b>territoryId</b>
	 * 
	 * @param territoryId
	 *            - id территории для которой нужно найти количество зарегестрированных игроков
	 */
	public static int getPlayersForTerritory(final int territoryId)
	{
		int counter = 0;
		for(final int id : _players.values())
			if(id == territoryId)
				counter++;
		return counter;
	}

	/**
	 * Возвращает id территории для игрока
	 */
	public static int getTerritoryForPlayer(final int playerId)
	{
		final Integer terrId = _players.get(playerId);
		return terrId == null ? -1 : terrId;
	}

	/**
	 * Возвращает список кланов участвующих в территориальной битве
	 */
	public static Map<SiegeClan, Integer> getClans()
	{
		return _clans;
	}

	/**
	 * Возвращает количество кланов зарегестрированных на битву с территорией <b>territoryId</b>
	 * 
	 * @param territoryId
	 *            - id территории для которой нужно найти количество зарегестрированных кланов
	 */
	public static int getClansForTerritory(final int territoryId)
	{
		int counter = 0;
		for(final int terrId : _clans.values())
			if(terrId == territoryId)
				counter++;
		return counter;
	}

	/**
	 * Возвращает id территории для клана с clanId
	 */
	public static int getTerritoryForClan(final int clanId)
	{
		if(clanId == 0)
			return 0;

		final L2Clan clan = ClanTable.getInstance().getClan(clanId);
		if(clan == null)
			return 0;

		if(clan.getHasCastle() > 0)
			return clan.getHasCastle();

		for(final Entry<SiegeClan, Integer> entry : getClans().entrySet())
			if(entry.getKey().getClanId() == clanId)
				return entry.getValue();
		return 0;
	}

	public static L2Zone getZone(final Residence unitId)
	{
		return ZoneManager.getInstance().getZoneByIndex(ZoneType.Siege, unitId.getId(), false);
	}

	public static L2Zone getResidenseZone(final Residence unitId)
	{
		return ZoneManager.getInstance().getZoneByIndex(ZoneType.siege_residense, unitId.getId(), false);
	}

	public static void catapultDestroyed(final int castleId)
	{
		final Castle castle = CastleManager.getInstance().getCastleByIndex(castleId);
		if(castle != null)
		{
			// Как только Территориальная Катапульта будет разрушена все защитные функции замка отключаются и все ворота замка открываются
			for(final L2DoorInstance door : castle.getDoors())
				door.openMe();
			// все NPC-защитники замка исчезают
			changeSpawnGuard(castleId, false);
		}
	}

	public static void giveBadges(final L2Player killer, final int victimSide, final int reward)
	{
		if(victimSide < 1)
			return;

		if(killer.getParty() != null)
			for(final L2Player pl : killer.getParty().getPartyMembers())
			{
				if(pl.getSiegeSide() == victimSide || pl.getSiegeSide() < 1 || !killer.isInRange(pl, Config.ALT_PARTY_DISTRIBUTION_RANGE))
					continue;

				final String var = killer.getVar("badges" + killer.getTerritorySiege());
				int badges = 0;
				if(var != null)
					badges = Integer.parseInt(var);
				killer.setVar("badges" + killer.getTerritorySiege(), "" + (badges + reward));
			}
	}

	/**
	 * Стартует ТВ
	 */
	public static void startSiege()
	{
		if(!_isInProgress)
		{
			_tw_quest = QuestManager.getQuest("TerritoryWarSuperClass");

			L2Zone zone;
			for(final Castle unitId : _castles)
			{
				zone = getZone(unitId);
				if(zone != null)
					zone.setActive(true);
				getResidenseZone(unitId).setActive(true);
			}

			for(final Fortress unitId : _fortress)
			{
				zone = getZone(unitId);
				if(zone != null)
					zone.setActive(true);
			}

			// Кланы, владеющие замком, автоматически регистрируются за свои земли.
			for(final Castle castle : _castles)
				if(castle.getOwner() != null)
					getClans().put(new SiegeClan(castle.getOwner().getClanId(), null), castle.getId());

			_isInProgress = true;

			playersUpdate(false);
			clearSiegeFields();

			for(final Castle castle : _castles)
				castle.spawnDoor();
			for(final Fortress fortress : _fortress)
				fortress.spawnDoor();

			spawnFlags(-1);

			// TODO спавн гвардов, баллисты
			spawnSiegeGuard();

			// Таймер окончания осады
			_siegeEndDate = Calendar.getInstance();
			_siegeEndDate.add(Calendar.MINUTE, getSiegeLength());
			L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeEndTask(), 1000);

			_fameTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new TerritorySiegeFameTask(), 5 * 60 * 1000L, 5 * 60 * 1000L);

			announceToPlayer(Msg.TERRITORY_WAR_HAS_BEGUN, false);
		}
	}

	/**
	 * Заканчивает ТВ
	 */
	public static void endSiege()
	{
		L2Zone zone;
		for(final Castle unitId : _castles)
		{
			zone = getZone(unitId);
			if(zone != null)
				zone.setActive(false);
			getResidenseZone(unitId).setActive(false);
		}

		for(final Fortress unitId : _fortress)
		{
			zone = getZone(unitId);
			if(zone != null)
				zone.setActive(false);
		}

		if(_isInProgress)
		{
			announceToPlayer(Msg.TERRITORY_WAR_HAS_ENDED, false);

			// Награда участников в виде exp/sp
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null && player.getTerritorySiege() > -1)
					player.addExpAndSp(270000, 27000, true, false);

			announceToPlayer(Msg.THE_TERRITORY_WAR_CHANNEL_AND_FUNCTIONS_WILL_NOW_BE_DEACTIVATED, true);

			removeHeadquarters();

			clearSiegeFields();

			removeSiegeSummons();

			playersUpdate(true);

			saveSiege();

			TerritorySiegeDatabase.clearSiegeMembers();
			getPlayers().clear();
			getClans().clear();

			unSpawnFlags();

			// TODO деспавн гвардов, баллисты
			unspawnSiegeGuard();

			for(final Castle castle : _castles)
				castle.saveFlags();

			for(final Castle castle : _castles)
				castle.spawnDoor();

			for(final Fortress fortress : _fortress)
				fortress.spawnDoor();

			if(_siegeStartTask != null)
			{
				_siegeStartTask.cancel(false);
				_siegeStartTask = null;
			}
			if(_fameTask != null)
			{
				_fameTask.cancel(true);
				_fameTask = null;
			}

			setRegistrationOver(false);

			_isInProgress = false;
		}
	}

	private static void startAutoTask()
	{
		if(_siegeStartTask != null)
			return;

		correctSiegeDateTime();

		_log.info("TerritorySiege: " + Util.datetimeFormatter.format(_siegeDate.getTime()));

		_siegeRegistrationEndDate = Calendar.getInstance();
		_siegeRegistrationEndDate.setTimeInMillis(_siegeDate.getTimeInMillis());
		_siegeRegistrationEndDate.add(5, -1);

		_siegeStartTask = L2GameThreadPools.getInstance().scheduleGeneral(new TerritorySiegeStartTask(), 1000L);
	}

	private static void setNextSiegeDate()
	{
		if(_siegeDate.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis())
			return;

		_siegeDate.add(Calendar.DAY_OF_MONTH, Config.TERRITORY_SIEGE_DAY); // Schedule to happen in 14 days
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
			setNextSiegeDate(); // Re-run again if still in the pass
	}

	private static void correctSiegeDateTime()
	{
		boolean corrected = false;
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
		{
			// Since siege has past reschedule it to the next one (14 days)
			// This is usually caused by server being down
			corrected = true;
			setNextSiegeDate();
		}

		if(_siegeDate.get(Calendar.DAY_OF_WEEK) != Config.SIEGE_DAY_OF_WEEK)
		{
			corrected = true;
			_siegeDate.set(Calendar.DAY_OF_WEEK, Config.SIEGE_DAY_OF_WEEK);
		}

		if(_siegeDate.get(Calendar.HOUR_OF_DAY) != Config.SIEGE_HOUR_OF_DAY)
		{
			corrected = true;
			_siegeDate.set(Calendar.HOUR_OF_DAY, Config.SIEGE_HOUR_OF_DAY);
		}

		_siegeDate.set(Calendar.MINUTE, 0);

		if(corrected)
			ServerVariables.set("TerritorySiegeDate", _siegeDate.getTimeInMillis());
	}

	private static void saveSiege()
	{
		setNextSiegeDate(); // Выставляем дату следующей осады
		ServerVariables.set("TerritorySiegeDate", _siegeDate.getTimeInMillis()); // Сохраняем дату следующей осады
		startAutoTask(); // Запускаем таск для следующей осады
	}

	private static void removeSiegeSummons()
	{
		for(final L2Player player : getPlayersInZone())
			for(final int id : Siege.SIEGE_SUMMONS)
				if(player.getPet() != null && id == player.getPet().getNpcId())
					player.getPet().unSummon();
	}

	/**
	 * Рассылка бродкастом сообщений всем или только участникам ТВ.<br>
	 * Нельза рассылать только участникам если территориальная война не стартовала.
	 */
	public static void announceToPlayer(final L2GameServerPacket message, final boolean participantsOnly)
	{
		final GArray<L2Player> players = new GArray<L2Player>();
		if(participantsOnly)
		{
			// Нет смысла перебирать всех игроков, если терриориальная война не началась
			if(!isInProgress())
				return;

			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null && player.getTerritorySiege() > -1)
					players.add(player);
		}
		else
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null)
					players.add(player);

		for(final L2Player player : players)
			player.sendPacket(message);
	}

	public static GArray<L2Player> getPlayersInZone()
	{
		final GArray<L2Player> players = new GArray<L2Player>();
		if(!isInProgress())
			return players;

		for(final Castle unitId : _castles)
			players.addAll(getZone(unitId).getInsidePlayers());

		for(final Fortress unitId : _fortress)
			players.addAll(getZone(unitId).getInsidePlayers());

		return players;
	}

	/**
	 * @param character
	 *            - id объекат для проверки
	 * @return true если находиться в зоне ТВ
	 */
	public static boolean checkIfInZone(final L2Character character)
	{
		if(!isInProgress())
			return false;

		for(final Castle unitId : _castles)
			for(final L2Character cha : getZone(unitId).getObjectsInside())
				if(cha == character)
					return true;
		for(final Fortress unitId : _fortress)
			for(final L2Character cha : getZone(unitId).getObjectsInside())
				if(cha == character)
					return true;
		return false;
	}

	public static SiegeClan getSiegeClan(final L2Clan clan)
	{
		if(clan == null)
			return null;
		for(final SiegeClan siegeClan : _clans.keySet())
			if(siegeClan.getClan() == clan)
				return siegeClan;
		return null;
	}

	private static void clearSiegeFields()
	{
		for(final L2Player player : getPlayersInZone())
			if(player != null && !player.isGM())
				player.teleToClosestTown();
	}

	private static void playersUpdate(final boolean end)
	{
		if(end) // обрабатываем в квестах окончание ТВ
			_tw_quest.onChange(end);

		for(final Entry<SiegeClan, Integer> entry : _clans.entrySet())
		{
			final SiegeClan siegeClan = entry.getKey();
			final L2Clan clan = ClanTable.getInstance().getClan(siegeClan.getClanId());
			if(clan == null)
			{
				_log.warning("TerritorySiege: siege clan is null!!! id: " + siegeClan.getClanId());
				continue;
			}

			if(end)
			{
				clan.setTerritorySiege(-1);
				refreshTerritorySkills();
			}
			else
			{
				clan.setTerritorySiege(entry.getValue());
				// Скиллы территорий не должны работать во время Битв за Земли
				deleteTerritorySkills();
			}

			for(final L2Player member : clan.getOnlineMembers(0))
				member.broadcastCharInfo();
		}

		for(final Entry<Integer, Integer> entry : _players.entrySet())
		{
			final L2Player player = L2ObjectsStorage.getPlayer(entry.getKey());
			if(player != null)
			{
				if(end)
					player.setTerritorySiege(-1);
				else
					player.setTerritorySiege(entry.getValue());
				player.broadcastCharInfo();
			}
		}

		if(!end) // обрабатываем в квестах начало ТВ
			_tw_quest.onChange(end);
	}

	private static void removeHeadquarters()
	{
		for(final SiegeClan sc : getClans().keySet())
			if(sc != null)
				sc.removeHeadquarter();
	}

	public static L2NpcInstance getHeadquarter(final L2Clan clan)
	{
		if(clan != null)
		{
			final SiegeClan sc = getSiegeClan(clan);
			if(sc != null)
				return sc.getHeadquarter();
		}
		return null;
	}

	public static void spawnFlags(final int onlyOne)
	{
		for(final Castle castle : _castles)
		{
			final GArray<SiegeSpawn> points = TerritorySiegeDatabase.getSiegeFlags().get(castle.getId());
			int i = 0;
			for(final int flagCastleId : castle.getFlags())
			{
				if(onlyOne == -1 || flagCastleId == onlyOne)
				{
					final SiegeSpawn info = TerritorySiegeDatabase.getSiegeFlags().get(flagCastleId).get(0);
					final Location loc = points.get(i).getLoc();

					final L2TerritoryFlagInstance flag = new L2TerritoryFlagInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(info.getNpcId()));
					flag.setCurrentHpMp(flag.getMaxHp(), flag.getMaxMp(), true);
					flag.setXYZInvisible(loc.correctGeoZ());
					flag.setSpawnedLoc(flag.getLoc());
					flag.setHeading(loc.h);
					flag.setItemId(info.getValue());
					flag.setBaseTerritoryId(flagCastleId);
					flag.setCurrentTerritoryId(castle.getId());
					flag.spawnMe();
					_flags.add(flag);

					setWardLoc(flagCastleId, flag.getLoc());
				}
				i++;
			}
		}
	}

	private static void unSpawnFlags()
	{
		for(final L2TerritoryFlagInstance flag : _flags)
			if(flag != null)
			{
				L2ItemInstance item = flag.getItem();
				if(item != null)
				{
					if(item.getOwnerId() > 0)
					{
						final L2Player owner = L2ObjectsStorage.getPlayer(item.getOwnerId());
						if(owner != null)
							item = owner.getInventory().dropItem(item, item.getCount(), true);
					}
					item.deleteMe();
				}
				flag.deleteMe();
			}
		_flags.clear();
		_wardsLoc.clear();
	}

	public static void removeFlag(final L2TerritoryFlagInstance flag)
	{
		_flags.remove(flag);
	}

	public static L2TerritoryFlagInstance getNpcFlagByItemId(final int itemId)
	{
		for(final L2TerritoryFlagInstance flag : _flags)
			if(flag.getItemId() == itemId)
				return flag;
		return null;
	}

	/**
	 * Spawn guards.
	 */
	public static void spawnSiegeGuard()
	{
		for(final Castle unit : _castles)
		{
			changeSpawnGuard(unit.getId(), true);
			changeSpawnCatapul(unit.getId(), true);
		}
	}

	/**
	 * Unspawn guards.
	 */
	public static void unspawnSiegeGuard()
	{
		for(final Castle unit : _castles)
		{
			changeSpawnGuard(unit.getId(), false);
			changeSpawnCatapul(unit.getId(), false);
		}
	}

	/**
	 * спаунит/удаляет гвардов в указаном замке
	 * 
	 * @param castleId
	 * @param isSpawn
	 */
	private static void changeSpawnGuard(final int castleId, final boolean isSpawn)
	{
		if(isSpawn)
		{
			for(final L2Spawn spawn : TerritorySiegeDatabase.getGuardsSpawnList().get(castleId))
				if(spawn != null)
				{
					final L2NpcInstance mob = spawn.doSpawn(true);
					mob.getSpawn().stopRespawn();
				}
		}
		else
			for(final L2Spawn spawn : TerritorySiegeDatabase.getGuardsSpawnList().get(castleId))
				if(spawn != null)
				{
					spawn.stopRespawn();
					if(spawn.getLastSpawn() != null)
						spawn.getLastSpawn().deleteMe();
				}
	}

	/**
	 * спаунит/удаляет катапульты в указаном замке
	 * 
	 * @param castleId
	 * @param isSpawn
	 */
	private static void changeSpawnCatapul(final int castleId, final boolean isSpawn)
	{
		if(isSpawn)
		{
			for(final L2Spawn spawn : TerritorySiegeDatabase.getCatapultsSpawnList().get(castleId))
				if(spawn != null)
				{
					final L2NpcInstance mob = spawn.doSpawn(true);
					mob.getSpawn().stopRespawn();
				}
		}
		else
			for(final L2Spawn spawn : TerritorySiegeDatabase.getCatapultsSpawnList().get(castleId))
				if(spawn != null)
				{
					spawn.stopRespawn();
					if(spawn.getLastSpawn() != null)
						spawn.getLastSpawn().deleteMe();
				}
	}

	/**
	 * Спаунит нпс в городах, если у замка есть владелец
	 */
	public static void spawnNpcInTown()
	{
		for(final Castle castle : _castles)
			if(castle.getOwnerId() != 0) // если у замка есть владелец
				for(final L2Spawn spawn : TerritorySiegeDatabase.getNpcsSpawnList().get(castle.getId()))
					if(spawn != null)
						spawn.init();
	}

	/** Возвращает продолжительность осады территории, в минутах */
	public static int getSiegeLength()
	{
		return Config.TW_LENGTH;
	}

	/** Дату осады территории */
	public static Calendar getSiegeDate()
	{
		return _siegeDate;
	}

	/** Возвращает время окончание осады */
	public static Calendar getSiegeEndDate()
	{
		return _siegeEndDate;
	}

	/** возвращает true если идёт осада */
	public static boolean isInProgress()
	{
		return _isInProgress;
	}

	public static void setRegistrationOver(final boolean value)
	{
		_isRegistrationOver = value;
	}

	/** возвращает true если регистрация окончена */
	public static boolean isRegistrationOver()
	{
		return _isRegistrationOver;
	}

	/** возвращает время окончания регистрации на осаду территории */
	public static Calendar getSiegeRegistrationEndDate()
	{
		return _siegeRegistrationEndDate;
	}

	public static int getDefenderRespawnTotal(final int unitId)
	{
		return _defenderRespawnDelay + _defenderRespawnPenalty.get(unitId);
	}

	public static void killedCT(final int unitId)
	{
		_defenderRespawnPenalty.put(unitId, _defenderRespawnPenalty.get(unitId) + _controlTowerLosePenalty);
	}

	/** Канал автоматически деактивируется через 10 минут после окончания Территориальной Битвы. */
	public static boolean isTerritoryChatAccessible()
	{
		return getSiegeDate().getTimeInMillis() - 10 * 60 * 1000 > System.currentTimeMillis() && getSiegeEndDate().getTimeInMillis() + 10 * 60 * 1000 > System.currentTimeMillis();
	}

	/**
	 * Производит обновление скилов территорий для кланов-владельцев замков
	 */
	public static void refreshTerritorySkills()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			final L2Clan owner = c.getOwner();
			if(owner == null)
				continue;

			// Удаляем лишние
			L2Skill[] clanSkills = owner.getAllSkills();
			for(final L2Skill cs : clanSkills)
			{
				if(!isTerritoriSkill(cs))
					continue;

				if(!c.getTerritorySkills().contains(cs))
					owner.removeSkill(cs);
			}

			// Добавляем недостающие
			clanSkills = owner.getAllSkills();
			boolean exist;
			for(final L2Skill cs : c.getTerritorySkills())
			{
				exist = false;
				for(final L2Skill clanSkill : clanSkills)
				{
					if(!isTerritoriSkill(clanSkill))
						continue;
					if(clanSkill.getId() == cs.getId())
					{
						exist = true;
						break;
					}
				}
				if(!exist)
					owner.addNewSkill(cs, false);
			}
		}
	}

	public static void deleteTerritorySkills()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			final L2Clan owner = c.getOwner();
			if(owner == null)
				continue;

			final L2Skill[] clanSkills = owner.getAllSkills();
			for(final L2Skill cs : clanSkills)
			{
				if(!isTerritoriSkill(cs))
					continue;
				owner.removeSkill(cs);
			}
		}
	}

	/**
	 * Проверяет, является ли данный скилл скилом территорий
	 */
	private static boolean isTerritoriSkill(final L2Skill skill)
	{
		for(final int id : TERRITORY_SKILLS)
			if(id == skill.getId())
				return true;
		return false;
	}

	public static Map<Integer, Location> getWardsLoc()
	{
		return _wardsLoc;
	}

	public static void setWardLoc(final Integer id, final Location loc)
	{
		if(_wardsLoc.get(id) != null)
			_wardsLoc.get(id).set(loc);
		else
			_wardsLoc.put(id, loc);
	}

	public static class TerritorySiegeFameTask implements Runnable
	{
		@Override
		public void run()
		{
			if(!isInProgress())
				return;

			int bonus = 0;
			for(final L2Player player : TerritorySiege.getPlayersInZone())
				if(player != null && !player.isDead() && !player.isInOfflineMode() && player.getTerritorySiege() > -1)
				{
					if(player.isInZone(ZoneType.Fortress))
						bonus = Config.TerritoryFortress_ZONE_FAME_AQUIRE_POINTS;
					else if(player.isInZone(ZoneType.Castle))
						bonus = Config.TerritorySiege_ZONE_FAME_AQUIRE_POINTS;
					player.addFame(bonus);

					// если сидит или нет оружия в руках, то не выдаём баджи
					if(player.isSitting() || player.getActiveWeaponInstance() == null)
						continue;

					double badgesCount = 0.5;
					if(player.isInCombat())
						badgesCount += 0.5;
					final L2Object target = player.getTarget();
					if(target != null && target.isPlayable())
					{
						badgesCount += 0.5;
						final L2Player ptarget = target.getPlayer();
						if(ptarget != null && player.getTerritorySiege() != ptarget.getTerritorySiege() && ptarget.getTerritorySiege() > -1)
							badgesCount += 0.5;
					}
					final String var = player.getVar("badges" + player.getTerritorySiege());
					int badges = 0;
					if(var != null)
						badges = Integer.parseInt(var);
					badges += badgesCount;
					player.setVar("badges" + player.getTerritorySiege(), "" + badges);
				}
		}
	}
}
