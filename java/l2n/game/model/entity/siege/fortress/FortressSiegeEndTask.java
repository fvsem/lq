package l2n.game.model.entity.siege.fortress;

/**
 * @author L2System Project
 * @date 08.08.2011
 * @time 4:54:37
 */
public class FortressSiegeEndTask implements Runnable
{
	private final FortressSiege _siege;

	public FortressSiegeEndTask(final FortressSiege siege)
	{
		_siege = siege;
	}

	@Override
	public void run()
	{
		if(!_siege.isInProgress())
			return;

		try
		{
			_siege.endSiege();
		}
		catch(final Throwable t)
		{}
	}
}
