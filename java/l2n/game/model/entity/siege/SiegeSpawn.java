package l2n.game.model.entity.siege;

import l2n.util.Location;

public class SiegeSpawn
{
	private final Location _location;
	private final int _npcId;
	private final int _siegeUnitId;
	private final int _value;

	public SiegeSpawn(final int siegeUnitId, final Location loc, final int npc_id)
	{
		_siegeUnitId = siegeUnitId;
		_location = loc;
		_npcId = npc_id;
		_value = 0;
	}

	public SiegeSpawn(final int siegeUnitId, final Location loc, final int npc_id, final int value)
	{
		_siegeUnitId = siegeUnitId;
		_location = loc;
		_npcId = npc_id;
		_value = value;
	}

	public int getSiegeUnitId()
	{
		return _siegeUnitId;
	}

	public int getNpcId()
	{
		return _npcId;
	}

	public int getValue()
	{
		return _value;
	}

	public Location getLoc()
	{
		return _location;
	}
}
