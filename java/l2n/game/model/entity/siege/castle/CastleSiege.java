package l2n.game.model.entity.siege.castle;

import gnu.trove.map.hash.TIntIntHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.MercTicketManager;
import l2n.game.instancemanager.SiegeGuardManager;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.*;
import l2n.game.model.instances.L2ArtefactInstance;
import l2n.game.model.instances.L2ControlTowerInstance;
import l2n.game.network.serverpackets.SiegeInfo;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;
import l2n.game.tables.MapRegionTable;
import l2n.game.tables.MapRegionTable.TeleportWhereType;
import l2n.util.Util;

import java.util.Calendar;

public class CastleSiege extends Siege
{
	private final GArray<L2ControlTowerInstance> _controlTowers = new GArray<L2ControlTowerInstance>();
	private final GArray<L2ArtefactInstance> _artifacts = new GArray<L2ArtefactInstance>();
	private final TIntIntHashMap _engrave = new TIntIntHashMap();

	protected TrapPacketSender trapListener = new TrapPacketSender();

	private final int addRepNewOwner = Config.CLANREP_SIEGE_CASLE_NEW_OWNER;
	private final int addRepOldOwner = Config.CLANREP_SIEGE_CASLE_OLD_OWNER;
	private final int addRepLoss = Config.CLANREP_SIEGE_CASLE_LOSS;

	public CastleSiege(final Residence castle)
	{
		super(castle);
		_database = new CastleSiegeDatabase(this);
		_siegeGuardManager = new SiegeGuardManager(getSiegeUnit());
		_database.loadSiegeClan();
	}

	@Override
	public void startSiege()
	{
		if(!_isInProgress)
		{
			setRegistrationOver(true);
			_database.loadSiegeClan();

			if(getAttackerClans().isEmpty())
			{
				SystemMessage sm;
				if(getSiegeUnit().getOwnerId() <= 0)
					sm = new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_BEEN_CANCELED_DUE_TO_LACK_OF_INTEREST).addString(getSiegeUnit().getName());
				else
					sm = new SystemMessage(SystemMessage.S1S_SIEGE_WAS_CANCELED_BECAUSE_THERE_WERE_NO_CLANS_THAT_PARTICIPATED).addString(getSiegeUnit().getName());
				Announcements.announceToAll(sm);
				return;
			}

			// Слушатель добавляется перед активацией т.к. зона выполнит вход
			getZone().getListenerEngine().addMethodInvokedListener(trapListener);
			getZone().setActive(true);
			getResidenseZone().setActive(true);

			_isInProgress = true; // Flag so that same siege instance cannot be started again
			_isMidVictory = false;
			_ownerBeforeStart = getSiegeUnit().getOwnerId();

			updateSiegeClans(); // update siege clans
			updatePlayerSiegeStateFlags(false);

			teleportPlayer(TeleportWhoType.Attacker, MapRegionTable.TeleportWhereType.ClosestTown); // Teleport to the closest town
			teleportPlayer(TeleportWhoType.Spectator, MapRegionTable.TeleportWhereType.ClosestTown); // Teleport to the closest town

			respawnControlTowers(); // Respawn control towers

			getSiegeUnit().spawnDoor(); // Spawn door
			getSiegeGuardManager().spawnSiegeGuard(); // Spawn siege guard
			MercTicketManager.getInstance().deleteTickets(getSiegeUnit().getId()); // remove the tickets from the ground
			_defenderRespawnPenalty = 0; // Reset respawn delay

			// Schedule a task to prepare auto siege end
			_siegeEndDate = Calendar.getInstance();
			_siegeEndDate.add(Calendar.MINUTE, getSiegeLength());
			L2GameThreadPools.getInstance().scheduleGeneral(new SiegeEndTask(this), 1000); // Prepare auto end task

			_fameTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SiegeFameTask(), Config.CASTLE_ZONE_FAME_TASK_FREQUENCY, Config.CASTLE_ZONE_FAME_TASK_FREQUENCY);

			announceToPlayer(Msg.TEMPORARY_ALLIANCE, false, ANNOUNCE_TO_ATTACKERS);
			announceToPlayer(new SystemMessage(SystemMessage.YOU_HAVE_PARTICIPATED_IN_THE_SIEGE_OF_S1_THIS_SIEGE_WILL_CONTINUE_FOR_2_HOURS).addString(getSiegeUnit().getName() + " Castle"), false, ANNOUNCE_TO_BOTH_SIDES);
			announceToPlayer(new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_STARTED).addString(getSiegeUnit().getName() + " Castle"), false, ANNOUNCE_TO_ALL_EXCEPT_PARTICIPATING);
		}
	}

	@Override
	public void midVictory()
	{
		// Если осада закончилась
		if(!isInProgress() || getSiegeUnit().getOwnerId() <= 0)
			return;

		// Если атакуется замок, принадлежащий NPC, и только 1 атакующий - закончить осаду
		if(getDefenderClans().isEmpty() && getAttackerClans().size() == 1)
		{
			final SiegeClan sc_newowner = getAttackerClan(getSiegeUnit().getOwner());
			removeSiegeClan(sc_newowner, SiegeClanType.ATTACKER);
			addSiegeClan(sc_newowner, SiegeClanType.OWNER);
			endSiege();
			return;
		}

		final int allyId = ClanTable.getInstance().getClan(getSiegeUnit().getOwnerId()).getAllyId();

		// Если атакуется замок, принадлежащий NPC, и все атакующие в одном альянсе - закончить осаду
		if(allyId != 0 && getDefenderClans().isEmpty())
		{
			boolean allinsamealliance = true;
			for(final SiegeClan sc : getAttackerClans().values())
				if(sc != null)
				{
					final L2Clan clan = sc.getClan();
					if(clan != null && clan.getAllyId() != allyId)
						allinsamealliance = false;
				}
			if(allinsamealliance)
			{
				final SiegeClan sc_newowner = getAttackerClan(getSiegeUnit().getOwner());
				removeSiegeClan(sc_newowner, SiegeClanType.ATTACKER);
				addSiegeClan(sc_newowner, SiegeClanType.OWNER);
				endSiege();
				return;
			}
		}

		// Поменять местами атакующих и защитников
		for(final SiegeClan sc : getDefenderClans().values())
			if(sc != null)
			{
				removeSiegeClan(sc, SiegeClanType.DEFENDER);
				addSiegeClan(sc, SiegeClanType.ATTACKER);
			}

		final SiegeClan sc_newowner = getAttackerClan(getSiegeUnit().getOwner());
		removeSiegeClan(sc_newowner, SiegeClanType.ATTACKER);
		addSiegeClan(sc_newowner, SiegeClanType.OWNER);

		_isMidVictory = true;

		clearSiegeClans();
		updateSiegeClans();
		updatePlayerSiegeStateFlags(false);

		announceToPlayer(new SystemMessage(SystemMessage.THE_TEMPORARY_ALLIANCE_OF_THE_CASTLE_ATTACKER_TEAM_HAS_BEEN_DISSOLVED), false, ANNOUNCE_TO_BOTH_SIDES);

		teleportPlayer(TeleportWhoType.Attacker, TeleportWhereType.ClosestTown);
		teleportPlayer(TeleportWhoType.Spectator, TeleportWhereType.ClosestTown);

		_defenderRespawnPenalty = 0;

		getSiegeGuardManager().unspawnSiegeGuard(); // Remove all spawned siege guard from this castle
		getSiegeUnit().removeUpgrade(); // Remove all castle upgrade
		respawnControlTowers();
		getSiegeUnit().spawnDoor(true); // Respawn door to castle but make them weaker (50% hp)
	}

	@Override
	public void endSiege()
	{
		getZone().setActive(false);
		getZone().getListenerEngine().removeMethodInvokedListener(trapListener); // Слушаетель убирается после деактивации т.к. зона выполнит выход
		getResidenseZone().setActive(false);

		if(isInProgress())
		{
			SystemMessage sm = new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_FINISHED).addString(getSiegeUnit().getName());
			Announcements.announceToAll(sm);

			if(getSiegeUnit().getOwnerId() <= 0)
			{
				sm = new SystemMessage(SystemMessage.THE_SIEGE_OF_S1_HAS_ENDED_IN_A_DRAW).addString(getSiegeUnit().getName());
				Announcements.announceToAll(sm);
			}
			else
			{
				L2Clan oldOwner = null;
				if(_ownerBeforeStart != 0)
					oldOwner = ClanTable.getInstance().getClan(_ownerBeforeStart);
				final L2Clan newOwner = ClanTable.getInstance().getClan(getSiegeUnit().getOwnerId());

				// castle was taken over from scratch
				if(oldOwner == null)
				{
					if(newOwner.getLevel() >= 5)
						newOwner.broadcastToOnlineMembers(new SystemMessage(SystemMessage.SINCE_YOUR_CLAN_EMERGED_VICTORIOUS_FROM_THE_SIEGE_S1_POINTS_HAVE_BEEN_ADDED_TO_YOUR_CLAN_REPUTATION_SCORE).addNumber(newOwner.incReputation(addRepNewOwner, true, "CastleSiege")));
					SiegeManager.clearCastleRegistrations(newOwner);
					SiegeManager.clearFortressRegistrations(newOwner); // TODO убрать
				}
				// castle was defended
				else if(newOwner.equals(oldOwner))
				{
					if(newOwner.getLevel() >= 5)
						newOwner.broadcastToOnlineMembers(new SystemMessage(SystemMessage.SINCE_YOUR_CLAN_EMERGED_VICTORIOUS_FROM_THE_SIEGE_S1_POINTS_HAVE_BEEN_ADDED_TO_YOUR_CLAN_REPUTATION_SCORE).addNumber(newOwner.incReputation(addRepOldOwner, true, "CastleSiege")));
					SiegeManager.clearCastleRegistrations(newOwner);
					SiegeManager.clearFortressRegistrations(newOwner); // TODO убрать
				}
				// castle was taken over by another clan
				else
				{
					sm = new SystemMessage(SystemMessage.CLAN_S1_IS_VICTORIOUS_OVER_S2S_CASTLE_SIEGE).addString(newOwner.getName()).addString(getSiegeUnit().getName());
					Announcements.announceToAll(sm);

					if(newOwner.getLevel() >= 5)
						newOwner.broadcastToOnlineMembers(new SystemMessage(SystemMessage.SINCE_YOUR_CLAN_EMERGED_VICTORIOUS_FROM_THE_SIEGE_S1_POINTS_HAVE_BEEN_ADDED_TO_YOUR_CLAN_REPUTATION_SCORE).addNumber(newOwner.incReputation(addRepNewOwner, true, "CastleSiege")));
					if(oldOwner.getLevel() >= 5)
						oldOwner.broadcastToOnlineMembers(new SystemMessage(SystemMessage.YOUR_CLAN_HAS_FAILED_TO_DEFEND_THE_CASTLE_S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_YOUR_CLAN_REPUTATION_SCORE).addNumber(-oldOwner.incReputation(-addRepLoss, Config.USE_REDUCE_REPSCORE_RATE, "CastleSiege")));

					SiegeManager.clearCastleRegistrations(newOwner);
					SiegeManager.clearFortressRegistrations(newOwner); // TODO убрать
				}
			}

			removeHeadquarters();
			teleportPlayer(TeleportWhoType.Attacker, TeleportWhereType.ClosestTown); // Teleport to the closest town
			teleportPlayer(TeleportWhoType.Spectator, TeleportWhereType.ClosestTown); // Teleport to the closest town
			removeSiegeSummons();
			_isInProgress = false; // Flag so that siege instance can be started
			updatePlayerSiegeStateFlags(true);
			saveSiege(); // Save castle specific data
			_database.clearSiegeClan(); // Clear siege clan from db
			respawnControlTowers(); // Remove all control tower from this castle
			getSiegeGuardManager().unspawnSiegeGuard(); // Remove all spawned siege guard from this castle
			SiegeGuardManager.removeMercsFromDb(getSiegeUnit().getId());
			getSiegeUnit().spawnDoor(); // Respawn door to castle
			if(_ownerBeforeStart != getSiegeUnit().getOwnerId())
				getSiegeUnit().setOwnDate((int) (System.currentTimeMillis() / 1000));
			getSiegeUnit().saveOwnDate();
			clearSiegeClans();

			if(_siegeStartTask != null)
			{
				_siegeStartTask.cancel(false);
				_siegeStartTask = null;
			}
			if(_fameTask != null)
			{
				_fameTask.cancel(true);
				_fameTask = null;
			}

			setRegistrationOver(false);
		}
	}

	@Override
	public void engrave(final L2Clan clan, final int objId)
	{
		_engrave.put(objId, clan.getClanId());
		if(_engrave.size() >= _artifacts.size())
		{
			boolean rst = true;
			for(final int id : _engrave.values())
				if(id != clan.getClanId())
					rst = false;
			if(rst)
			{
				_engrave.clear();
				getSiegeUnit().changeOwner(clan);
			}
		}
	}

	/**
	 * Start the auto tasks<BR>
	 * <BR>
	 */
	@Override
	public void startAutoTask(boolean isServerStarted)
	{
		if(_siegeStartTask != null)
			return;

		correctSiegeDateTime();

		_database.loadSiegeClan();

		// Schedule registration end
		_siegeRegistrationEndDate = Calendar.getInstance();
		_siegeRegistrationEndDate.setTimeInMillis(_siegeDate.getTimeInMillis());
		_siegeRegistrationEndDate.add(Calendar.DAY_OF_MONTH, -1);

		// Если сервер только что стартовал, осада начнется не ранее чем через час
		if(isServerStarted)
		{
			Calendar minDate = Calendar.getInstance();
			minDate.add(Calendar.HOUR_OF_DAY, 1);
			_siegeDate.setTimeInMillis(Math.max(minDate.getTimeInMillis(), _siegeDate.getTimeInMillis()));
			_database.saveSiegeDate();

			// Если был рестарт во время осады, даем зарегистрироваться еще раз
			if(_siegeDate.getTimeInMillis() <= minDate.getTimeInMillis())
			{
				setRegistrationOver(false);
				_siegeRegistrationEndDate.setTimeInMillis(_siegeDate.getTimeInMillis());
				_siegeRegistrationEndDate.add(Calendar.MINUTE, -10);
			}
		}

		_log.info("Siege of " + getSiegeUnit().getName() + " - " + Util.datetimeFormatter.format(_siegeDate.getTime()));

		// Schedule siege auto start
		_siegeStartTask = L2GameThreadPools.getInstance().scheduleGeneral(new SiegeStartTask(this), 1000);
	}

	/** Set the date for the next siege. */
	@Override
	protected void setNextSiegeDate()
	{
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
		{
			// Set next siege date if siege has passed
			_siegeDate.add(Calendar.DAY_OF_MONTH, Config.CASTLE_SIEGE_DAY); // Schedule to happen in 14 days
			_isRegistrationOver = false; // Allow registration for next siege

			if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
				setNextSiegeDate(); // Re-run again if still in the pass
		}
	}

	@Override
	protected void correctSiegeDateTime()
	{
		boolean corrected = false;
		if(_siegeDate.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
		{
			// Since siege has past reschedule it to the next one (14 days)
			// This is usually caused by server being down
			corrected = true;
			setNextSiegeDate();
		}
		if(_siegeDate.get(Calendar.DAY_OF_WEEK) != getSiegeUnit().getSiegeDayOfWeek())
		{
			corrected = true;
			_siegeDate.set(Calendar.DAY_OF_WEEK, getSiegeUnit().getSiegeDayOfWeek());
		}
		if(_siegeDate.get(Calendar.HOUR_OF_DAY) != getSiegeUnit().getSiegeHourOfDay())
		{
			corrected = true;
			_siegeDate.set(Calendar.HOUR_OF_DAY, getSiegeUnit().getSiegeHourOfDay());
		}
		_siegeDate.set(Calendar.MINUTE, 0);
		if(corrected)
			_database.saveSiegeDate();
	}

	/** Display list of registered clans */
	@Override
	public void listRegisterClan(final L2Player player)
	{
		player.sendPacket(new SiegeInfo(getSiegeUnit()));
	}

	@Override
	protected void saveSiege()
	{
		// Выставляем дату следующей осады
		setNextSiegeDate();
		// Сохраняем дату следующей осады
		_database.saveSiegeDate();
		// Запускаем таск для следующей осады
		startAutoTask(false);
	}

	/** Remove all control tower spawned. */
	private void respawnControlTowers()
	{
		// Remove all instance of control tower for this castle
		for(final L2ControlTowerInstance ct : _controlTowers)
			if(ct != null)
			{
				ct.decayMe();
				ct.spawnMe();
			}
	}

	public void addControlTower(final L2ControlTowerInstance tower)
	{
		_controlTowers.add(tower);
	}

	public boolean isAllTowersDead()
	{
		for(final L2ControlTowerInstance ct : _controlTowers)
			if(ct != null && !ct.isFakeTower())
				return false;
		return true;
	}

	public void addArtifact(final L2ArtefactInstance art)
	{
		_artifacts.add(art);
	}

	/**
	 * Обновляет статус ловушек у текущей осады. Если игрок входит(enter == true), то будет отослано состояние трэпов. Если выходит, то трэпы будут простро выключены Если осада не запущена, то трепы
	 * выключатся.
	 * 
	 * @param player
	 *            игрок
	 * @param enter
	 *            вход или выход игрока
	 *            <p>
	 *            TODO: обработка
	 */
	@Override
	public void sendTrapStatus(final L2Player player, final boolean enter)
	{
		if(enter)
		{
			// TODO: player.sendPacket(new EventTrigger(...));
		}
		else
		{
			// TODO: player.sendPacket(new EventTrigger(...));
		}
	}

	/**
	 * Осадной зоне добавляется слушатель для входа/выхода объекта
	 */
	private class TrapPacketSender extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character.isPlayer())
			{
				final L2Player player = (L2Player) character;

				sendTrapStatus(player, true);
			}
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{
			if(character.isPlayer())
			{
				final L2Player player = (L2Player) character;
				sendTrapStatus(player, false);
			}
		}
	}
}
