package l2n.game.model.entity.siege;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.SiegeGuardManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.model.entity.siege.castle.CastleSiege;
import l2n.game.model.entity.siege.clanhall.ClanHallSiege;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;
import l2n.game.tables.MapRegionTable;
import l2n.game.tables.MapRegionTable.TeleportWhereType;

import java.util.Calendar;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public abstract class Siege
{
	protected static final Logger _log = Logger.getLogger(Siege.class.getName());

	// Siege Announcements...
	public static final byte ANNOUNCE_TO_DEFENDERS = 1;
	public static final byte ANNOUNCE_TO_ATTACKERS = 2;
	public static final byte ANNOUNCE_TO_BOTH_SIDES = 3;
	public static final byte ANNOUNCE_TO_ALL_EXCEPT_PARTICIPATING = 4;

	// typeId's
	public static final byte OWNER = -1;
	public static final byte DEFENDER = 0;
	public static final byte ATTACKER = 1;
	public static final byte DEFENDER_PENDING = 2;
	public static final byte DEFENDER_REFUSED = 3;

	private int _defenderRespawnDelay = 20000;
	private int _siegeClanMinLevel = 4;
	private int _siegeLength = 120;
	private int _controlTowerLosePenalty = 20000;

	protected FastMap<SiegeClanType, FastMap<Integer, SiegeClan>> _siegeClans = new FastMap<SiegeClanType, FastMap<Integer, SiegeClan>>().shared();

	protected Residence _siegeUnit;
	protected SiegeDatabase _database;
	protected SiegeGuardManager _siegeGuardManager;

	/* State */
	protected boolean _isInProgress = false;
	protected boolean _isMidVictory = false;
	protected boolean _isRegistrationOver = false;

	/* Differet Vars while siege isInProgress */
	protected int _ownerBeforeStart;
	protected int _defenderRespawnPenalty = 0;

	/* AllTimers must be private acsess */
	protected Calendar _siegeDate;
	protected Calendar _siegeEndDate;
	protected Calendar _siegeRegistrationEndDate;

	protected ScheduledFuture<?> _siegeStartTask;
	protected ScheduledFuture<?> _fameTask;

	public static final short[] SIEGE_SUMMONS = {
			1459,
			14768,
			14769,
			14770,
			14771,
			14772,
			14773,
			14774,
			14775,
			14776,
			14777,
			14778,
			14779,
			14780,
			14781,
			14782,
			14783,
			14784,
			14785,
			14786,
			14787,
			14788,
			14789,
			14790,
			14791,
			14792,
			14793,
			14794,
			14795,
			14796,
			14797,
			14798,
			14839 };

	public Siege(final Residence siegeUnit)
	{
		_siegeUnit = siegeUnit;
		_siegeDate = Calendar.getInstance();
	}

	public L2Zone getZone()
	{
		return ZoneManager.getInstance().getZoneByIndex(ZoneType.Siege, getSiegeUnit().getId(), false);
	}

	public L2Zone getResidenseZone()
	{
		return ZoneManager.getInstance().getZoneByIndex(ZoneType.siege_residense, getSiegeUnit().getId(), false);
	}

	/**
	 * When siege starts
	 */
	public abstract void startSiege();

	/**
	 * When control of castle changed during siege
	 */
	public abstract void midVictory();

	/** Display list of registered clans */
	public abstract void listRegisterClan(L2Player player);

	/**
	 * When siege ends
	 */
	public abstract void endSiege();

	public abstract void engrave(L2Clan clan, int objId);

	/** Start the auto tasks */
	public abstract void startAutoTask(boolean isServerStarted);

	protected abstract void setNextSiegeDate();

	protected abstract void correctSiegeDateTime();

	/** Save siege related to database. */
	protected abstract void saveSiege();

	public Residence getSiegeUnit()
	{
		return _siegeUnit;
	}

	public SiegeGuardManager getSiegeGuardManager()
	{
		return _siegeGuardManager;
	}

	public SiegeDatabase getDatabase()
	{
		return _database;
	}

	/** Return true if object is inside the zone */
	public boolean checkIfInZone(final int x, final int y, final boolean onlyActive)
	{
		if(onlyActive && !isInProgress())
			return false;
		if(getSiegeUnit() != null && getSiegeUnit().checkIfInZone(x, y))
			return true;
		final L2Zone zone = getZone();
		return zone != null && zone.checkIfInZone(x, y);
	}

	/**
	 * Announce to player.<BR>
	 * <BR>
	 * 
	 * @param message
	 *            : The String of the message to send to player
	 * @param inAreaOnly
	 *            : The boolean flag to show message to players in area only.
	 * @param announceType
	 *            : 1 = Defenders, 2 = Attackers, 3 = Defenders & Attackers, 4 = Whole world, except defenders/attackers.
	 */
	public void announceToPlayer(final SystemMessage message, final boolean inAreaOnly, final byte announceType)
	{
		if(announceType == ANNOUNCE_TO_DEFENDERS || announceType == ANNOUNCE_TO_BOTH_SIDES)
		{
			L2Clan clan;
			for(final SiegeClan siegeClans : getDefenderClans().values())
			{
				clan = ClanTable.getInstance().getClan(siegeClans.getClanId());
				if(clan != null)
					for(final L2Player member : clan.getOnlineMembers(0))
						if(member != null && (!inAreaOnly || member.isInZone(ZoneType.Siege)))
							member.sendPacket(message);
			}
		}

		if(announceType == ANNOUNCE_TO_ATTACKERS || announceType == ANNOUNCE_TO_BOTH_SIDES)
		{
			L2Clan clan;
			for(final SiegeClan siegeClans : getAttackerClans().values())
			{
				clan = ClanTable.getInstance().getClan(siegeClans.getClanId());
				if(clan != null)
					for(final L2Player member : clan.getOnlineMembers(0))
						if(member != null && (!inAreaOnly || member.isInZone(ZoneType.Siege)))
							member.sendPacket(message);
			}
		}

		if(announceType == ANNOUNCE_TO_ALL_EXCEPT_PARTICIPATING)
		{
			final Iterable<L2Player> allOnlinePlayers = L2ObjectsStorage.getAllPlayersForIterate();
			for(final L2Player player : allOnlinePlayers)
				if(player.isOnline() && !isParticipant(player) && (!inAreaOnly || player.isInZone(ZoneType.Siege)))
					player.sendPacket(message);
		}
	}

	public void updatePlayerSiegeStateFlags(final boolean clear)
	{
		L2Clan clan;
		for(final SiegeClan siegeClan : getAttackerClans().values())
		{
			clan = ClanTable.getInstance().getClan(siegeClan.getClanId());
			if(clan != null)
				for(final L2Player member : clan.getOnlineMembers(0))
					member.setSiegeState(clear ? 0 : 1);
		}
		for(final SiegeClan siegeclan : getDefenderClans().values())
		{
			clan = ClanTable.getInstance().getClan(siegeclan.getClanId());
			if(clan != null)
				for(final L2Player member : clan.getOnlineMembers(0))
					member.setSiegeState(clear ? 0 : 2);
		}
	}

	/** Return list of L2Player in the zone. */
	public GArray<L2Player> getPlayersInZone()
	{
		final GArray<L2Player> players = new GArray<L2Player>();
		for(final L2Character object : getZone().getObjectsInside())
			if(object.isPlayer())
				players.add((L2Player) object);
		return players;
	}

	/**
	 * Teleport players
	 */
	public void teleportPlayer(final TeleportWhoType teleportWho, final TeleportWhereType teleportWhere)
	{
		GArray<L2Player> players = new GArray<L2Player>();
		final int ownerId = getSiegeUnit().getOwnerId();
		switch (teleportWho)
		{
			case Owner:
				if(ownerId > 0)
					for(final L2Player player : getPlayersInZone())
						if(player.getClan() != null && player.getClan().getClanId() == ownerId)
							players.add(player);
				break;
			case Attacker:
				for(final L2Player player : getPlayersInZone())
					if(player.getClan() != null && getAttackerClan(player.getClan()) != null)
						players.add(player);
				break;
			case Defender:
				for(final L2Player player : getPlayersInZone())
					if(player.getClan() != null && player.getClan().getClanId() != ownerId && getDefenderClan(player.getClan()) != null)
						players.add(player);
				break;
			case Spectator:
				for(final L2Player player : getPlayersInZone())
					if(player.getClan() == null || getAttackerClan(player.getClan()) == null && getDefenderClan(player.getClan()) == null)
						players.add(player);
				break;
			default:
				players = getPlayersInZone();
		}
		for(final L2Player player : players)
			if(player != null && !player.isGM())
			{
				if(player.getCastingSkill() != null && player.getCastingSkill().getId() == 246)
					player.abortCast();
				if(teleportWho == TeleportWhoType.Defender && teleportWhere == MapRegionTable.TeleportWhereType.Castle)
				{
					player.teleToLocation(getSiegeUnit().getZone().getSpawn());
					continue;
				}
				player.teleToLocation(MapRegionTable.getInstance().getTeleToLocation(player, teleportWhere));
			}
	}

	/**
	 * Set siege date time<BR>
	 * <BR>
	 * 
	 * @param siegeDateTime
	 *            The long of date time in millisecond
	 */
	public void setSiegeDateTime(final long siegeDateTime)
	{
		_siegeDate.setTimeInMillis(siegeDateTime); // Set siege date
	}

	/**
	 * Return true if the player can register.<BR>
	 * <BR>
	 * 
	 * @param player
	 *            The L2Player of the player trying to register
	 * @return true if the player can register.
	 */
	protected boolean checkIfCanRegister(final L2Player player)
	{
		if(isRegistrationOver())
		{
			player.sendPacket(new SystemMessage(SystemMessage.THE_DEADLINE_TO_REGISTER_FOR_THE_SIEGE_OF_S1_HAS_PASSED).addString(getSiegeUnit().getName()));
			return false;
		}

		if(isInProgress())
		{
			player.sendPacket(Msg.THIS_IS_NOT_THE_TIME_FOR_SIEGE_REGISTRATION_AND_SO_REGISTRATIONS_CANNOT_BE_ACCEPTED_OR_REJECTED);
			return false;
		}

		if(player.getClan() == null || player.getClan().getLevel() < getSiegeClanMinLevel())
		{
			player.sendPacket(Msg.ONLY_CLANS_WITH_LEVEL_4_AND_HIGHER_MAY_REGISTER_FOR_A_CASTLE_SIEGE);
			return false;
		}

		if(player.getClan().getHasCastle() > 0 && getSiegeUnit().getType() != ResidenceType.Fortress)
		{
			player.sendPacket(Msg.A_CLAN_THAT_OWNS_A_CASTLE_CANNOT_PARTICIPATE_IN_ANOTHER_SIEGE);
			return false;
		}

		if(player.getClan().getClanId() == getSiegeUnit().getOwnerId())
		{
			player.sendPacket(Msg.THE_CLAN_THAT_OWNS_THE_CASTLE_IS_AUTOMATICALLY_REGISTERED_ON_THE_DEFENDING_SIDE);
			return false;
		}

		if(SiegeDatabase.checkIsRegistered(player.getClan(), getSiegeUnit().getId()))
		{
			player.sendPacket(Msg.YOU_HAVE_ALREADY_REQUESTED_A_SIEGE_BATTLE);
			return false;
		}

		if(getSiegeUnit().getType() == ResidenceType.Fortress)
		{
			// Регистрация невозможна, если последняя осада проходила менее чем четыре часа назад
			if(getSiegeUnit().getLastSiegeDate() * 1000 + 4 * 60 * 60 * 1000 > System.currentTimeMillis())
			{
				player.sendPacket(new SystemMessage(SystemMessage.THE_DEADLINE_TO_REGISTER_FOR_THE_SIEGE_OF_S1_HAS_PASSED).addString(getSiegeUnit().getName()));
				return false;
			}

			if(TerritorySiege.getSiegeDate().getTimeInMillis() > System.currentTimeMillis() && TerritorySiege.getSiegeDate().getTimeInMillis() - System.currentTimeMillis() < 2 * 60 * 60 * 1000)
			{
				player.sendPacket(new SystemMessage(SystemMessage.THE_DEADLINE_TO_REGISTER_FOR_THE_SIEGE_OF_S1_HAS_PASSED).addString(getSiegeUnit().getName()));
				return false;
			}

			// Владельцам замка нельзя атаковать присягнувшие крепости.
			if(player.getClan().getHasCastle() > 0 && ((Fortress) getSiegeUnit()).getCastleId() == player.getClan().getHasCastle() && ((Fortress) getSiegeUnit()).getFortState() == 2)
			{
				player.sendPacket(Msg.SIEGE_REGISTRATION_IS_NOT_POSSIBLE_DUE_TO_A_CONTRACT_WITH_A_HIGHER_CASTLE);
				return false;
			}
		}

		return true;
	}

	/**
	 * Register clan as attacker<BR>
	 * <BR>
	 * 
	 * @param player
	 *            The L2Player of the player trying to register
	 */
	public void registerAttacker(final L2Player player)
	{
		registerAttacker(player, false);
	}

	public void registerAttacker(final L2Player player, final boolean force)
	{
		if(player.getClan() == null)
			return;

		int allyId = 0;
		if(getSiegeUnit().getOwnerId() != 0)
		{
			final L2Clan castleClan = ClanTable.getInstance().getClan(getSiegeUnit().getOwnerId());
			if(castleClan != null)
				allyId = castleClan.getAllyId();
		}
		if(allyId != 0 && player.getClan().getAllyId() == allyId && !force)
		{
			player.sendPacket(Msg.YOU_CANNOT_REGISTER_ON_THE_ATTACKING_SIDE_BECAUSE_YOU_ARE_PART_OF_AN_ALLIANCE_WITH_THE_CLAN_THAT_OWNS_THE_CASTLE);
			return;
		}
		if(force || checkIfCanRegister(player))
		{
			if(getSiegeUnit().getType() == ResidenceType.Fortress)
			{
				// Только первый клан платит за регистрацию на атаку форта
				if(getAttackerClans().size() == 0)
				{
					if(player.getAdena() < 250000)
						return;

					player.reduceAdena(250000, true); // Плата за регистрацию на осаду форта.
				}

				player.sendPacket(new SystemMessage(SystemMessage.YOUR_CLAN_HAS_BEEN_REGISTERED_TO_S1_FORTRESS_BATTLE).addString(_siegeUnit.getName()));
			}
			_database.saveSiegeClan(player.getClan(), 1); // Save to database
		}
	}

	/**
	 * Register clan as defender<BR>
	 * <BR>
	 * 
	 * @param player
	 *            The L2Player of the player trying to register
	 */
	public void registerDefender(final L2Player player)
	{
		registerDefender(player, false);
	}

	public void registerDefender(final L2Player player, final boolean force)
	{
		if(getSiegeUnit().getOwnerId() <= 0)
			player.sendMessage(new CustomMessage("l2n.game.model.entity.siege.Siege.OwnedByNPC", player).addString(getSiegeUnit().getName()));
		else if(force || checkIfCanRegister(player))
			_database.saveSiegeClan(player.getClan(), 2); // Save to database
	}

	public void clearSiegeClan(final L2Clan clan, final boolean force)
	{
		if(clan == null || !force && clan.getHasCastle() == getSiegeUnit().getId() || !SiegeDatabase.checkIsRegistered(clan, getSiegeUnit().getId()))
			return;
		_database.removeSiegeClan(clan.getClanId());
		for(final FastMap<Integer, SiegeClan> siegeClans : _siegeClans.values())
			siegeClans.remove(clan.getClanId());
	}

	public FastMap<SiegeClanType, FastMap<Integer, SiegeClan>> getSiegeClanList()
	{
		return _siegeClans;
	}

	public void addSiegeClan(final SiegeClan sc, final SiegeClanType type)
	{
		if(sc == null)
			return;
		sc.setTypeId(type);
		_siegeClans.get(type.simple()).put(sc.getClanId(), sc);
	}

	public void addSiegeClan(final int clanId, final SiegeClanType type)
	{
		addSiegeClan(new SiegeClan(clanId, type), type);
	}

	public void removeSiegeClan(final int clanId, final SiegeClanType type)
	{
		_siegeClans.get(type.simple()).remove(clanId);
	}

	public void removeSiegeClan(final SiegeClan sc, final SiegeClanType type)
	{
		if(sc != null)
			removeSiegeClan(sc.getClanId(), type);
	}

	public SiegeClan getSiegeClan(final int clanId, final SiegeClanType type)
	{
		return _siegeClans.get(type.simple()).get(clanId);
	}

	public SiegeClan getSiegeClan(final L2Clan clan, final SiegeClanType type)
	{
		if(clan == null)
			return null;
		return getSiegeClan(clan.getClanId(), type);
	}

	public FastMap<Integer, SiegeClan> getSiegeClans(final SiegeClanType type)
	{
		return _siegeClans.get(type.simple());
	}

	public boolean isParticipant(final L2Player player)
	{
		final L2Clan clan = player.getClan();
		return clan != null && (checkIsAttacker(clan) || checkIsDefender(clan, false));
	}

	public boolean checkIsAttacker(final L2Clan clan)
	{
		return getSiegeClan(clan, SiegeClanType.ATTACKER) != null;
	}

	/**
	 * Return true if clan is defender
	 * 
	 * @param clan
	 *            The L2Clan of the player
	 * @param reg
	 *            TODO
	 */
	public boolean checkIsDefender(final L2Clan clan, final boolean reg)
	{
		if(getSiegeClan(clan, SiegeClanType.DEFENDER) != null)
		{
			// если объект является Фортом, а клан его владельцем, то разрешаем регистрацию на атаку других фортов
			if(reg && _siegeUnit.getType() == ResidenceType.Fortress && _siegeUnit.getOwnerId() == clan.getClanId())
				return false;
			return true;
		}
		return false;
	}

	/**
	 * Return true if clan is defender waiting approval
	 * 
	 * @param clan
	 *            The L2Clan of the player
	 */
	public boolean checkIsDefenderWaiting(final L2Clan clan)
	{
		return getSiegeClan(clan, SiegeClanType.DEFENDER_WAITING) != null;
	}

	public boolean checkIsDefenderRefused(final L2Clan clan)
	{
		return getSiegeClan(clan, SiegeClanType.DEFENDER_REFUSED) != null;
	}

	/**
	 * Return true if clan is registered to the siege
	 * 
	 * @param clanId
	 *            The clan id of the player
	 */
	public boolean checkIsClanRegistered(final L2Clan clan)
	{
		return checkIsAttacker(clan) || checkIsDefender(clan, true) || checkIsDefenderWaiting(clan) || checkIsDefenderRefused(clan);
	}

	public FastMap<Integer, SiegeClan> getAttackerClans()
	{
		return getSiegeClans(SiegeClanType.ATTACKER);
	}

	public FastMap<Integer, SiegeClan> getDefenderClans()
	{
		return getSiegeClans(SiegeClanType.DEFENDER);
	}

	public FastMap<Integer, SiegeClan> getDefenderRefusedClans()
	{
		return getSiegeClans(SiegeClanType.DEFENDER_REFUSED);
	}

	public FastMap<Integer, SiegeClan> getDefenderWaitingClans()
	{
		return getSiegeClans(SiegeClanType.DEFENDER_WAITING);
	}

	public SiegeClan getAttackerClan(final L2Clan clan)
	{
		return getSiegeClan(clan, SiegeClanType.ATTACKER);
	}

	public SiegeClan getDefenderClan(final L2Clan clan)
	{
		return getSiegeClan(clan, SiegeClanType.DEFENDER);
	}

	/**
	 * Approve clan as defender for siege<BR>
	 * <BR>
	 * 
	 * @param clanId
	 *            The int of player's clan id
	 */
	public void approveSiegeDefenderClan(final int clanId)
	{
		if(clanId <= 0)
			return;
		_database.saveSiegeClan(ClanTable.getInstance().getClan(clanId), 0);
		_database.loadSiegeClan();
	}

	public void refuseSiegeDefenderClan(final int clanId)
	{
		if(clanId <= 0)
			return;
		_database.saveSiegeClan(ClanTable.getInstance().getClan(clanId), 3);
		_database.loadSiegeClan();
	}

	protected void updateSiegeClans()
	{
		L2Clan clan;
		for(final SiegeClan sclan : getDefenderClans().values())
		{
			clan = sclan.getClan();
			if(clan != null)
			{
				clan.setSiege(this);
				clan.setDefender(true);
				clan.setAttacker(false);
			}
		}
		for(final SiegeClan sclan : getAttackerClans().values())
		{
			clan = sclan.getClan();
			if(clan != null)
			{
				clan.setSiege(this);
				clan.setDefender(false);
				clan.setAttacker(true);
			}
		}
	}

	protected void clearSiegeClans()
	{
		for(final L2Clan clan : ClanTable.getInstance().getClans())
		{
			if(clan.getSiege() != this)
				continue;
			clan.setSiege(null);
			clan.setDefender(false);
			clan.setAttacker(false);
		}
	}

	public int getDefenderRespawnTotal()
	{
		return _defenderRespawnDelay + _defenderRespawnPenalty;
	}

	public boolean isInProgress()
	{
		return _isInProgress;
	}

	public boolean isMidVictory()
	{
		return _isMidVictory;
	}

	public boolean isRegistrationOver()
	{
		return _isRegistrationOver;
	}

	public void setRegistrationOver(final boolean value)
	{
		_isRegistrationOver = value;
	}

	public Calendar getSiegeDate()
	{
		return _siegeDate;
	}

	public Calendar getSiegeEndDate()
	{
		return _siegeEndDate;
	}

	public long getTimeRemaining()
	{
		return getSiegeDate().getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
	}

	protected void removeSiegeSummons()
	{
		for(final L2Player player : getPlayersInZone())
			for(final short id : SIEGE_SUMMONS)
				if(player.getPet() != null && id == player.getPet().getNpcId())
					player.getPet().unSummon();
	}

	/** Remove all Headquarters */
	protected void removeHeadquarters()
	{
		for(final SiegeClan sc : getAttackerClans().values())
			if(sc != null)
				sc.removeHeadquarter();
		for(final SiegeClan sc : getDefenderClans().values())
			if(sc != null)
				sc.removeHeadquarter();
	}

	public L2NpcInstance getHeadquarter(final L2Clan clan)
	{
		if(clan != null)
		{
			final SiegeClan sc = getSiegeClan(clan, SiegeClanType.ATTACKER);
			if(sc != null)
				return sc.getHeadquarter();
		}
		return null;
	}

	/**
	 * Control Tower was killed Add respawn penalty to defenders for each control tower lose
	 */
	public void killedCT()
	{
		_defenderRespawnPenalty += getControlTowerLosePenalty();
	}

	public void sendTrapStatus(final L2Player player, final boolean enter)
	{}

	public Calendar getSiegeRegistrationEndDate()
	{
		return _siegeRegistrationEndDate;
	}

	public int getSiegeClanMinLevel()
	{
		return _siegeClanMinLevel;
	}

	public void setSiegeClanMinLevel(final int siegeClanMinLevel)
	{
		_siegeClanMinLevel = siegeClanMinLevel;
	}

	/**
	 * @return продолжительность осады в минутах
	 */
	public int getSiegeLength()
	{
		return _siegeLength;
	}

	public void setSiegeLength(final int siegeLength)
	{
		_siegeLength = siegeLength;
	}

	public int getControlTowerLosePenalty()
	{
		return _controlTowerLosePenalty;
	}

	public void setControlTowerLosePenalty(final int controlTowerLosePenalty)
	{
		_controlTowerLosePenalty = controlTowerLosePenalty;
	}

	public int getDefenderRespawnDelay()
	{
		return _defenderRespawnDelay;
	}

	public void setDefenderRespawnDelay(final int respawnDelay)
	{
		_defenderRespawnDelay = respawnDelay;
	}

	public class SiegeFameTask implements Runnable
	{
		@Override
		public void run()
		{
			if(!isInProgress())
				return;

			int bonus = 0;
			if(Siege.this instanceof CastleSiege)
				bonus = Config.CASTLE_ZONE_FAME_AQUIRE_POINTS;
			else if(Siege.this instanceof ClanHallSiege)
				bonus = 75;
			else if(Siege.this instanceof FortressSiege)
				bonus = Config.FORTRESS_ZONE_FAME_AQUIRE_POINTS;

			for(final L2Player player : getPlayersInZone())
				if(player != null && !player.isDead() && !player.isInOfflineMode() && player.getClan() != null && player.getClan().getSiege() == Siege.this)
					player.addFame(bonus);
		}
	}

	public boolean isForterss()
	{
		return _siegeUnit.getType() == ResidenceType.Fortress;
	}
}
