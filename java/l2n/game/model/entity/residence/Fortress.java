package l2n.game.model.entity.residence;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.instancemanager.SiegeGuardManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.entity.siege.SiegeClanType;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.model.instances.L2StaticObjectInstance;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.tables.SkillTable;
import l2n.game.tables.StaticObjectsTable;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fortress extends Residence
{
	private final static String[] flagPoles = new String[]
	{
			"FlagPole;18220500;-52802;156500;-1156;3;none;0;0", // id: 101 Shanty Fortress
			"FlagPole;19240500;-22671;219813;-2329;3;none;0;0", // id: 102 Southern Fortress
			"FlagPole;20230500;16603;188096;-2027;3;none;0;0", // id: 103 Hive Fortress
			"FlagPole;23210500;126081;123393;-1688;3;none;0;0", // id: 104 Valley Fortress
			"FlagPole;22180500;72831;4306;-2148;3;none;0;0", // id: 105 Ivory Fortress
			"FlagPole;24190500;154872;55325;-2357;3;none;0;0", // id: 106 Narsell Fortress
			"FlagPole;25190500;189930;39776;-2513;3;none;0;0", // id: 107 Bayou Fortress
			"FlagPole;23240500;118443;204941;-2436;3;none;0;0", // id: 108 White Sands Fortress
			"FlagPole;24150500;159111;-70289;-1967;3;none;0;0", // id: 109 Borderland Fortress
			"FlagPole;22160500;69848;-61390;-1889;3;none;0;0", // id: 110 Swamp Fortress
			"FlagPole;23130500;109441;-141212;-2060;3;none;0;0", // id: 111 Archaic Fortress
			"FlagPole;20220500;5566;149751;-1992;3;none;0;0", // id: 112 Floran Fortress
			"FlagPole;18200500;-53230;91264;-1924;3;none;0;0", // id: 113 Cloud Mountain Fortress
			"FlagPole;21220500;60288;139474;-857;3;none;0;0", // id: 114 Tanor Fortress
			"FlagPole;20200500;11546;95030;-2498;3;none;0;0", // id: 115 Dragonspine Fortress
			"FlagPole;22200500;79274;91070;-1987;3;none;0;0", // id: 116 Antharas Fortress
			"FlagPole;23170500;111363;-15111;-98;3;none;0;0", // id: 117 Western Fortress
			"FlagPole;23200500;125245;95175;-1243;3;none;0;0", // id: 118 Hunters Fortress
			"FlagPole;22230500;73087;186022;-1684;3;none;0;0", // id: 119 Aaru Fortress
			"FlagPole;23160500;100674;-55322;251;3;none;0;0", // id: 120 Demon Fortress
			"FlagPole;22150500;72183;-94726;-531;3;none;0;0", // id: 121 Monastic Fortress
	};
	private final static Logger _log = Logger.getLogger(Fortress.class.getName());

	private L2StaticObjectInstance _flagPole = null;

	private FortressSiege _siege = null;
	private int _siegeDate = 0;
	private int _lastSiegeDate = 0;
	private int _contractStatus = 0;
	private int _castleId = 0;
	private int _fortType = 0;

	public Fortress(int fortressId)
	{
		super(fortressId);
	}

	@Override
	public void init()
	{
		super.init();
		if(getOwner() != null)
			setVisibleFlag(true);
	}

	@Override
	public FortressSiege getSiege()
	{
		if(_siege == null)
			_siege = new FortressSiege(this);
		return _siege;
	}


	public int getSiegeDate()
	{
		return _siegeDate;
	}

	public void setSiegeDate(int time)
	{
		_siegeDate = time;
	}

	@Override
	public int getLastSiegeDate()
	{
		return _lastSiegeDate;
	}

	public int getFortType()
	{
		return _fortType;
	}

	public void setFortType(int type)
	{
		_fortType = type;
	}

	public int getFortSize()
	{
		return getFortType() != 0 ? 5 : 3;
	}

	@Override
	public void setLastSiegeDate(int time)
	{
		_lastSiegeDate = time;
	}

	@Override
	public int getSiegeDayOfWeek()
	{
		return -1;
	}

	@Override
	public int getSiegeHourOfDay()
	{
		return -1;
	}

	@Override
	public void changeOwner(L2Clan clan)
	{
		if(clan != null)
		{
			if(clan.getHasFortress() != 0)
			{
				Fortress oldFortress = FortressManager.getInstance().getFortressByIndex(clan.getHasFortress());
				if(oldFortress != null)
					oldFortress.changeOwner(null);
			}
			if(clan.getHasCastle() != 0)
			{
				Castle oldCastle = CastleManager.getInstance().getCastleByIndex(clan.getHasCastle());
				if(oldCastle != null)
					oldCastle.changeOwner(null);
			}
		}

		if(getOwnerId() > 0 && (clan == null || clan.getClanId() != getOwnerId()))
		{
			removeSkills();
			L2Clan oldOwner = getOwner();
			if(oldOwner != null)
				oldOwner.setHasFortress(0);
		}

		if(clan != null)
			clan.setHasFortress(getId());

		if(clan == null)
		{
			getSiege().getDatabase().clearSiegeClan(SiegeClanType.DEFENDER);
			getSiege().getDatabase().clearSiegeClan(SiegeClanType.DEFENDER_WAITING);
			getSiege().getDatabase().clearSiegeClan(SiegeClanType.DEFENDER_REFUSED);
		}

		updateOwnerInDB(clan);

		rewardSkills();

		SiegeGuardManager.removeMercsFromDb(_id);

		removeUpgrade();

		if(clan != null && getSiege().isInProgress())
			getSiege().midVictory();

		setFortState(0, 0);

		if(getOwner() == null)
			setVisibleFlag(false);
	}

	@Override
	protected void loadData()
	{
		_type = ResidenceType.Fortress;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM `forts` WHERE `id` = ?");
			statement.setInt(1, _id);
			rs = statement.executeQuery();

			while (rs.next())
			{
				_name = rs.getString("name");
				_siegeDate = rs.getInt("siegeDate");
				_lastSiegeDate = rs.getInt("lastSiegeDate");
				_contractStatus = rs.getInt("state");
				if(_contractStatus == 0)
					_contractStatus = 1;
				_castleId = rs.getInt("castleId");

				setOwnDate(rs.getInt("ownDate"));

				StringTokenizer st = new StringTokenizer(rs.getString("skills"), ";");
				while (st.hasMoreTokens())
				{
					int skId = Integer.parseInt(st.nextToken());
					int skLvl = Integer.parseInt(st.nextToken());
					L2Skill skill = SkillTable.getInstance().getInfo(skId, skLvl);

					if(skill != null)
						_skills.add(skill);
				}
			}
			DbUtils.closeQuietly(statement, rs);

			statement = con.prepareStatement("SELECT `clan_id` FROM `clan_data` WHERE hasFortress = ?");
			statement.setInt(1, _id);
			rs = statement.executeQuery();

			while (rs.next())
				_ownerId = rs.getInt("clan_id");

			_zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.Fortress, getId(), true);

			_flagPole = StaticObjectsTable.parse(flagPoles[getId() - 101]);
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Fortress: load(): " + e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	private void updateOwnerInDB(L2Clan clan)
	{
		_ownerId = clan == null ? 0 : clan.getClanId(); // Update owner id property

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET hasFortress=0 WHERE hasFortress=? LIMIT 1");
			statement.setInt(1, getId());
			statement.execute();
			DbUtils.close(statement);
			statement = null;

			if(clan != null)
			{
				statement = con.prepareStatement("UPDATE clan_data SET hasFortress=? WHERE clan_id=? LIMIT 1");
				statement.setInt(1, getId());
				statement.setInt(2, getOwnerId());
				statement.execute();

				clan.broadcastClanStatus(false, true, true);
				clan.broadcastToOnlineMembers(new PlaySound("Siege_Victory"));
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Fortress: updateOwnerInDB(L2Clan clan): " + e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	@Override
	public void saveOwnDate()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE forts SET ownDate = ? WHERE id = ?");
			statement.setInt(1, getOwnDate());
			statement.setInt(2, getId());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Fortress: saveOwnDate(): " + e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void setFortState(int state, int castleId)
	{
		_contractStatus = state;
		_castleId = castleId;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE forts SET state = ?, castleId = ? WHERE id = ?");
			statement.setInt(1, getFortState());
			statement.setInt(2, getCastleId());
			statement.setInt(3, getId());
			statement.execute();
			statement.close();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Fortress: setFortState(int state, int castleId): " + e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	@Override
	public L2StaticObjectInstance getFlagPole()
	{
		return _flagPole;
	}

	/**
	 * Show or hide flag inside flagpole<BR>
	 * <BR>
	 */
	@Override
	public void setVisibleFlag(boolean val)
	{
		if(_flagPole != null)
			_flagPole.setMeshIndex(val ? 1 : 0);
	}

	public int getCastleId()
	{
		return _castleId;
	}

	/**
	 * @return Returns state of fortress.<BR>
	 * <BR>
	 *         0 - not decided yet<BR>
	 *         1 - independent<BR>
	 *         2 - contracted with castle<BR>
	 */
	public int getFortState()
	{
		return _contractStatus;
	}
}
