package l2n.game.model.entity.residence;

import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

public class TeleportLocation
{
	public TeleportLocation(String target, int item, int price, String name)
	{
		_target = target;
		_price = price;
		_name = name;
		_item = ItemTable.getInstance().getTemplate(item);
	}

	public int _price;
	public L2Item _item;
	public String _name;
	public String _target;
}
