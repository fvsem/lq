package l2n.game.model.entity;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.database.utils.mysql;
import l2n.game.model.L2Alliance;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hero
{
	private final static Logger _log = Logger.getLogger(Hero.class.getName());

	private static final String DELETE_ITEMS = "DELETE FROM items WHERE item_id IN (6842, 6611, 6612, 6613, 6614, 6615, 6616, 6617, 6618, 6619, 6620, 6621, 9388, 9389, 9390) AND owner_id NOT IN (SELECT obj_Id FROM characters WHERE accesslevel > 0)";
	private static final String DELETE_SKILLS = "DELETE FROM character_skills WHERE skill_id IN (395, 396, 1374, 1375, 1376) AND char_obj_id NOT IN (SELECT obj_Id FROM characters WHERE accesslevel > 0)";

	private final Map<Integer, StatsSet> _heroes;
	private final Map<Integer, StatsSet> _completeHeroes;

	public static final String COUNT = "count";
	public static final String PLAYED = "played";
	public static final String CLAN_NAME = "clan_name";
	public static final String CLAN_CREST = "clan_crest";
	public static final String ALLY_NAME = "ally_name";
	public static final String ALLY_CREST = "ally_crest";
	public static final String ACTIVE = "active";

	public static Hero getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final Hero _instance = new Hero();
	}

	public Hero()
	{
		_heroes = new FastMap<Integer, StatsSet>();
		_completeHeroes = new FastMap<Integer, StatsSet>();
		init();
	}

	private void init()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM heroes WHERE played = 1");
			rset = statement.executeQuery();

			while (rset.next())
			{
				StatsSet hero = new StatsSet();
				int charId = rset.getInt(Olympiad.CHAR_ID);
				hero.set(Olympiad.CHAR_NAME, Olympiad.getNobleName(charId));
				hero.set(Olympiad.CLASS_ID, Olympiad.getNobleClass(charId));
				hero.set(COUNT, rset.getInt(COUNT));
				hero.set(PLAYED, rset.getInt(PLAYED));
				hero.set(ACTIVE, rset.getInt(ACTIVE));

				initRelationBetweenHeroAndClan(charId, hero);

				_heroes.put(charId, hero);
			}
			DbUtils.closeQuietly(statement, rset);

			statement = con.prepareStatement("SELECT * FROM heroes");
			rset = statement.executeQuery();

			while (rset.next())
			{
				StatsSet hero = new StatsSet();
				int charId = rset.getInt(Olympiad.CHAR_ID);
				hero.set(Olympiad.CHAR_NAME, Olympiad.getNobleName(charId));
				hero.set(Olympiad.CLASS_ID, Olympiad.getNobleClass(charId));
				hero.set(COUNT, rset.getInt(COUNT));
				hero.set(PLAYED, rset.getInt(PLAYED));
				hero.set(ACTIVE, rset.getInt(ACTIVE));

				initRelationBetweenHeroAndClan(charId, hero);

				_completeHeroes.put(charId, hero);
			}
		}
		catch(SQLException e)
		{
			_log.log(Level.WARNING, "Hero System: Couldnt load Heroes", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_log.info("Hero System: Loaded " + _heroes.size() + " Heroes.");
		_log.info("Hero System: Loaded " + _completeHeroes.size() + " all time Heroes.");
	}

	private void initRelationBetweenHeroAndClan(int charId, StatsSet hero)
	{
		Entry<L2Clan, L2Alliance> e = ClanTable.getInstance().getClanAndAllianceByCharId(charId);
		hero.set("clan_crest", e.getKey() == null ? 0 : e.getKey().getCrestId());
		hero.set("clan_name", e.getKey() == null ? "" : e.getKey().getName());
		hero.set("ally_crest", e.getValue() == null ? 0 : e.getValue().getAllyCrestId());
		hero.set("ally_name", e.getValue() == null ? "" : e.getValue().getAllyName());
		e = null;
	}

	public Map<Integer, StatsSet> getHeroes()
	{
		return _heroes;
	}

	public synchronized void clearHeroes()
	{
		mysql.set("UPDATE heroes SET played = 0, active = 0");

		if(!_heroes.isEmpty())
			for(StatsSet hero : _heroes.values())
			{
				if(hero.getInteger(ACTIVE) == 0)
					continue;

				String name = hero.getString(Olympiad.CHAR_NAME);

				L2Player player = L2ObjectsStorage.getPlayer(name);

				if(player != null)
				{
					// СнУмаем геройскУе вещУ
					for(L2ItemInstance equippedItem : player.getInventory().getPaperdollItems())
						if(equippedItem != null && equippedItem.isHeroItem())
							player.getInventory().unEquipItem(equippedItem);

					// УдаленУе геройскУх вещей Уз Унвентаря
					for(L2ItemInstance item : player.getInventory().getItems())
						if(item != null && item.isHeroItem())
							player.getInventory().destroyItem(item, 1, true);

					// УдаленУе геройскУх вещей Уз вархауса
					for(L2ItemInstance item : player.getWarehouse().listItems(ItemClass.EQUIPMENT))
						if(item != null && item.isHeroItem())
							player.getWarehouse().destroyItem(item.getObjectId(), 1);

					player.setHero(false, 2);
					player.updatePledgeClass();
					player.broadcastUserInfo(true);
				}
			}

		_heroes.clear();
	}

	public synchronized boolean computeNewHeroes(GArray<StatsSet> newHeroes)
	{
		if(newHeroes.size() == 0)
			return true;

		Map<Integer, StatsSet> heroes = new FastMap<Integer, StatsSet>();
		boolean error = false;

		for(StatsSet hero : newHeroes)
		{
			int charId = hero.getInteger(Olympiad.CHAR_ID);

			if(_completeHeroes != null && _completeHeroes.containsKey(charId))
			{
				StatsSet oldHero = _completeHeroes.get(charId);
				int count = oldHero.getInteger(COUNT);
				oldHero.set(COUNT, count + 1);
				oldHero.set(PLAYED, 1);
				oldHero.set(ACTIVE, 0);

				heroes.put(charId, oldHero);
			}
			else
			{
				StatsSet newHero = new StatsSet();
				newHero.set(Olympiad.CHAR_NAME, hero.getString(Olympiad.CHAR_NAME));
				newHero.set(Olympiad.CLASS_ID, hero.getInteger(Olympiad.CLASS_ID));
				newHero.set(COUNT, 1);
				newHero.set(PLAYED, 1);
				newHero.set(ACTIVE, 0);

				heroes.put(charId, newHero);
			}
		}

		deleteItemsInDb();
		deleteSkillsInDb();

		_heroes.putAll(heroes);
		heroes.clear();

		updateHeroes(0);

		return error;
	}

	public void updateHeroes(int id)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO heroes VALUES (?,?,?,?)");

			for(int heroId : _heroes.keySet())
			{
				if(id > 0 && heroId != id)
					continue;

				StatsSet hero = _heroes.get(heroId);
				statement.setInt(1, heroId);
				statement.setInt(2, hero.getInteger(COUNT));
				statement.setInt(3, hero.getInteger(PLAYED));
				statement.setInt(4, hero.getInteger(ACTIVE));
				statement.execute();

				if(_completeHeroes != null && !_completeHeroes.containsKey(heroId))
				{
					initRelationBetweenHeroAndClan(heroId, hero);
					_completeHeroes.put(heroId, hero);
				}
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Hero System: Couldnt update Heroes", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean isHero(int id)
	{
		if(_heroes == null || _heroes.isEmpty())
			return false;
		return _heroes.containsKey(id) && _heroes.get(id).getInteger(ACTIVE) == 1;
	}

	public boolean isInactiveHero(int id)
	{
		if(_heroes == null || _heroes.isEmpty())
			return false;
		return _heroes.containsKey(id) && _heroes.get(id).getInteger(ACTIVE) == 0;
	}

	public void activateHero(L2Player player)
	{
		StatsSet hero = _heroes.get(player.getObjectId());
		hero.set(ACTIVE, 1);
		_heroes.remove(player.getObjectId());
		_heroes.put(player.getObjectId(), hero);

		player.setHero(true, 1);
		player.updatePledgeClass();
		player.broadcastPacket(new SocialAction(player.getObjectId(), 3));

		if(player.getClan() != null && player.getClan().getLevel() >= 5)
		{
			player.getClan().incReputation(Config.CLANREP_HERO_REP * (int) Config.RATE_CLAN_REP_SCORE, true, "Hero:computeNewHeroes:" + player);

			SystemMessage sm = new SystemMessage(SystemMessage.CLAN_MEMBER_S1_WAS_NAMED_A_HERO_2S_POINTS_HAVE_BEEN_ADDED_TO_YOUR_CLAN_REPUTATION_SCORE);
			sm.addString(player.getName());
			sm.addNumber(Math.round(Config.CLANREP_HERO_REP * Config.RATE_CLAN_REP_SCORE));
			player.getClan().broadcastToOtherOnlineMembers(sm, player);
		}
		else
			player.broadcastUserInfo(true);

		updateHeroes(player.getObjectId());
	}

	private void deleteItemsInDb()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_ITEMS);
			statement.execute();
			statement.close();
		}
		catch(SQLException e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void deleteSkillsInDb()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SKILLS);
			statement.execute();
			statement.close();
		}
		catch(SQLException e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
