package l2n.game.model.entity.Auction;

import l2n.game.L2GameThreadPools;

public class AutoEndTask implements Runnable
{
	private final Auction auction;

	public AutoEndTask(final Auction auction)
	{
		this.auction = auction;
	}

	@Override
	public void run()
	{
		try
		{
			final long timeRemaining = auction.getTimeRemaining();
			if(timeRemaining > 1800000) // 30 mins or more
				L2GameThreadPools.getInstance().scheduleGeneral(new AutoEndTask(auction), timeRemaining - 1800000);
			else if(timeRemaining > 600000) // 30 - 10 mins
				L2GameThreadPools.getInstance().scheduleGeneral(new AutoEndTask(auction), 60000);
			else if(timeRemaining > 0) // 10 - 0 mins
				L2GameThreadPools.getInstance().scheduleGeneral(new AutoEndTask(auction), 5000);
			else
				auction.endAuction();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}
}
