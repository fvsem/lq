package l2n.game.model.entity.l2auction;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Logger;

public class L2Member
{
	protected static Logger logger = Logger.getLogger("l2auction");

	protected int _id = 0;
	protected int _playerId = 0;
	protected String _playerName = null;
	protected L2Lot _lot = null;
	protected AuctionStatusEnum _status = null;

	public L2Member()
	{}

	public L2Member(int id)
	{
		_id = id;
	}

	public int getId()
	{
		return _id;
	}

	public void setId(int id)
	{
		_id = id;
	}

	public L2Player getPlayer()
	{
		return L2ObjectsStorage.getPlayer(_playerId);
	}

	public void setPlayer(L2Player player)
	{
		if(player != null)
		{
			_playerId = player.getObjectId();
			_playerName = player.getName();
		}
	}

	public L2Lot getLot()
	{
		return _lot;
	}

	public void setLot(L2Lot lot)
	{
		_lot = lot;
	}

	public AuctionStatusEnum getStatus()
	{
		return _status;
	}

	public void setStatus(AuctionStatusEnum status)
	{
		_status = status;
	}

	public int getPlayerId()
	{
		return _playerId;
	}

	public void setPlayerId(int playerId)
	{
		_playerId = playerId;
	}

	public String getPlayerName()
	{
		return _playerName;
	}

	public void setPlayerName(String playerName)
	{
		_playerName = playerName;
	}

	/**
	 * Создаем нового участнУка
	 * 
	 * @return объект участнУка
	 */
	public L2Member create()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO l2_lot_member (lot_id, customer_id, status, registered) " + "VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			statement.setLong(1, _lot.getId());
			statement.setLong(2, _playerId);
			statement.setString(3, AuctionStatusEnum.actv.toString());
			statement.setTimestamp(4, new Timestamp(new java.util.Date().getTime()));
			int res = statement.executeUpdate();
			if(res == 1)
			{
				rs = statement.getGeneratedKeys();
				if(rs.next())
				{
					int memberId = rs.getInt(1);
					L2Member result = new L2Member(memberId);
					result.load();
					return result;
				}
			}
		}
		catch(final Exception e)
		{
			logger.fine(toString());
			logger.severe("Error while creating member cause " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return null;
	}

	/**
	 * Меняем статус участнУка
	 * 
	 * @param status
	 *            новый статус
	 * @return true - еслУ все прошло успешно, false - Уначе
	 */
	protected boolean updateMemberStatus(AuctionStatusEnum status)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_lot_member SET status=? WHERE id=?");
			statement.setString(1, status.toString());
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				setStatus(status);
				logger.fine("Member " + getId() + " change status to " + status.toString());
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Error while updating member with id " + _id + " status to '" + status + "' cause: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * Метод вызывается, когда участнУк отпУсывается от участУя в торгах
	 */
	protected void gone()
	{
		logger.info("Member " + getId() + "is unregister from lot " + _lot.getId());

		// ЕслУ торгУ былУ с возвратом денег, то возвращаем ему его ставкУ за вычетом комУссУУ
		if(_lot.isEasy())
		{
			logger.info("Lot level is easy");
			int bets = _lot.getSummaryBet(getId());
			logger.info("Summary bets from member " + bets);
			int commission = bets / 100 * _lot.getProcent();
			logger.info("Commission: " + commission);
			int returningSum = bets - commission;
			logger.info("returning sum: " + returningSum);

			L2AuctionUtils.addAdena(this, returningSum);
			CreatureSay message = new CreatureSay(0, Say2C.TRADE, "Аукцион", "Вам были возвращены ваши ставки на лот " + _lot.getId() + " за вычетом комиссии " + _lot.getProcent() + "%");
			_lot.sendTradeMessage(this, message);
		}

		// Ставкам этого участнУка ставУм прУзнак отмены
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_member_bet SET status=? WHERE member_id=? AND lot_id=? AND status=?");
			statement.setString(1, AuctionStatusEnum.cncl.toString());
			statement.setInt(2, getId());
			statement.setInt(3, getLot().getId());
			statement.setString(4, AuctionStatusEnum.actv.toString());
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			logger.severe("Error while updating status to cancel for bets for member with id " + _id + " on lot '" + getLot().getId() + "' cause: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Загружаем участнУка Уз базы
	 * <p/>
	 * ИнУцУалУзацУя поля lot проходУт в месте, где этот метод вызывается
	 */
	protected void load()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT lot_id, customer_id, status FROM l2_lot_member WHERE id=?");
			statement.setInt(1, getId());
			rs = statement.executeQuery();
			if(rs.next())
			{
				setStatus(AuctionStatusEnum.valueOf(rs.getString("status")));
				_playerId = rs.getInt("customer_id");
				_playerName = L2AuctionUtils.getPlayerName(_playerId);
			}
		}
		catch(final Exception e)
		{
			logger.severe("Error while loading member with id " + _id + " cause: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	@Override
	public String toString()
	{
		return "L2Member{" + "id=" + _id + ", playerId=" + _playerId + ", playerName='" + _playerName + '\'' + ", status=" + _status + '}';
	}
}
