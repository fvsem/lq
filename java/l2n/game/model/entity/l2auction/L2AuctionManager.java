package l2n.game.model.entity.l2auction;

import javolution.util.FastList;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class L2AuctionManager
{
	protected static Logger logger = Logger.getLogger("l2auction");

	private static L2AuctionManager instance = null;
	private GArray<L2Auction> auctions = null;
	private GArray<L2AuctionUser> users = null;
	private boolean pause = true;

	private L2AuctionManager()
	{}

	public GArray<L2AuctionUser> getUsers()
	{
		if(users == null)
			users = new GArray<L2AuctionUser>();
		return users;
	}

	public L2AuctionUser getAuctionUser(L2Player player)
	{
		L2AuctionUser user = findUser(player);
		if(user == null)
		{
			user = new L2AuctionUser(player);
			addUser(user);
		}
		return user;
	}

	public void addUser(L2AuctionUser user)
	{
		getUsers().add(user);
	}

	public void removeUser(L2AuctionUser user)
	{
		getUsers().remove(user);
	}

	public L2AuctionUser findUser(L2Player player)
	{
		GArray<L2AuctionUser> users = getUsers();
		for(L2AuctionUser user : users)
			if(user.getPlayer().getObjectId() == player.getObjectId())
				return user;
		return null;
	}

	public static L2AuctionManager getInstance()
	{
		if(instance == null)
		{
			logger.config("Initializing L2AuctionManager");
			L2AuctionConfig.load();
			instance = new L2AuctionManager();
			instance.loadAuctions();
			instance.startManager();
		}
		return instance;
	}

	public void reload()
	{
		logger.info("Reload auction manager start");
		pause = true;
		getAuctions().clear();
		loadAuctions();
		pause = false;
	}

	protected void loadAuctions()
	{
		logger.config("Start loading auctions");
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT id FROM l2_auction ORDER BY date_start desc");
			rs = statement.executeQuery();
			while (rs.next())
			{
				L2Auction auction = new L2Auction(rs.getInt("id"));
				getAuctions().add(auction);
			}
			logger.config("Loaded: " + getAuctions().size() + " auction(s)");
			GArray<L2Auction> auctions = getAuctions();
			for(L2Auction auction : auctions)
				logger.config(auction.toString());
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	protected void startManager()
	{
		pause = false;
		new AuctionsTimer();
	}

	public final GArray<L2Auction> getAuctions()
	{
		if(auctions == null)
			auctions = new GArray<L2Auction>();
		return auctions;
	}

	public final L2Auction getCurrentAuction()
	{
		for(L2Auction auction : getAuctions())
			if(auction.getStatus() == AuctionStatusEnum.open)
				return auction;
		return null;
	}

	public final List<L2Auction> getArchiveAuctions()
	{
		List<L2Auction> list = new ArrayList<L2Auction>();
		if(auctions == null)
			auctions = new GArray<L2Auction>();
		for(L2Auction auction : auctions)
			if(auction.getStatus() == AuctionStatusEnum.clsd || auction.getStatus() == AuctionStatusEnum.cncl)
				list.add(auction);
		return list;
	}

	public L2Auction findAuction(long id)
	{
		GArray<L2Auction> list = getAuctions();
		for(L2Auction auction : list)
			if(auction.getId() == id)
				return auction;
		return null;
	}

	public L2Lot findLot(long id)
	{
		GArray<L2Auction> list = getAuctions();
		for(L2Auction auction : list)
		{
			FastList<L2Lot> lots = auction.getLots();
			for(L2Lot lot : lots)
				if(lot.getId() == id)
					return lot;
		}
		return null;
	}

	public L2Auction createAuction(L2Auction auction)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO l2_auction (date_start, date_to_stop, step, description, status) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			statement.setTimestamp(1, new Timestamp(auction.getStartDate().getTime()));
			statement.setTimestamp(2, new Timestamp(auction.getAdminStopDate().getTime()));
			statement.setInt(3, auction.getStepTime());
			statement.setString(4, auction.getDescription());
			statement.setString(5, AuctionStatusEnum.init.toString());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				rs = statement.getGeneratedKeys();
				if(rs.next())
				{
					int auctionId = rs.getInt(1);
					L2Auction result = new L2Auction(auctionId);
					result.load();
					getAuctions().add(result);
					return result;
				}
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return null;
	}

	class AuctionsTimer implements Runnable
	{
		/**
		 * Время запуска таймера.
		 */
		private static final int delay = 10;

		AuctionsTimer()
		{
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 0, delay * 1000);
		}

		@Override
		public void run()
		{
			// ЕслУ Удет перезапуск - ждем
			if(pause)
				return;
			// проверяем - не Удет лУ счас грешным делом какой аукцУон
			L2Auction auction = getCurrentAuction();
			if(auction != null)
				return;
			// бежУм по аукцУонам - есть лУ подходящУе для нас
			GArray<L2Auction> auctions = getAuctions();
			for(L2Auction a : auctions)
				// Подготовленные аукцУоны - вот то что мы Ущем
				if(a.getStatus() == AuctionStatusEnum.actv)
				{
					Date current = new Date();
					// Только аукцУоны, промежуток выполненУя которых нам подходУт. Берем самый первый!
					// ЕслУ не запустУлся - Ущем следующУй.
					if(current.after(a.getStartDate()) && current.before(a.getAdminStopDate()))
					{
						// Ура! Мы нашлУ его
						logger.fine("Auction Manager Timer find auction for start: " + a);
						// Запускаем
						boolean result = a.start();
						if(result)
						{
							logger.fine("Auction Manager Timer successfully start finded auction");
							return;
						}
						else
						{
							logger.severe("Auction Manager Timer cannot start finded auction");
							continue;
						}
					}
				}
		}
	}
}
