package l2n.game.model.entity.l2auction;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class L2AuctionConfig
{
	protected static Logger logger = Logger.getLogger("l2auction");

	/** Путь до конфигурационного файла */
	public static final String AUCTION_CONFIG_FILE = "./config/auction.ini";
	/** Ставка на лот по умолчанию */
	public static int LOT_BET = 50000;
	/** Количество секунд, на которые увеличивает ставка время до окончания аукциона */
	public static int TIME_STEP = 30;
	/** Номинал ставки в аденах */
	public static int BET = 1;
	/** Количество стартовых секунд до завершения лота */
	public static int START_LOT_TIME = 10 * 60;
	/** Минимальное количество игроков на сервере, при котором происходит публикация очередного лота */
	public static int CRITICAL_MASS = 250;
	/** Время старта аукциона по умолчанию */
	public static String DEFAULT_START_TIME = "12:00";
	/** Время окончания объявления лотов */
	public static String DEFAULT_STOP_TIME = "24:00";
	/** Время между публикациями лотов */
	public static int AUCTION_STEP_TIME = 30;
	/** Комиссия при возврате ставок, если лот выставлен так, чтобы проигравшие получали свои деньги обратно. */
	public static int LOT_PROCENT = 30;
	/** Указывает - может ли участник регистрироваться на лот после того, как начальное время вышло. */
	public static boolean LOT_REGISTER = true;

	/** Загрузка параметров из файла */
	public static void load()
	{
		try
		{
			Properties auctionSettings = new Properties();
			InputStream is = new FileInputStream(new File(AUCTION_CONFIG_FILE));
			auctionSettings.load(is);
			is.close();

			LOT_BET = Integer.valueOf(auctionSettings.getProperty("lot_bet", String.valueOf(LOT_BET)));
			TIME_STEP = Integer.valueOf(auctionSettings.getProperty("time_step", String.valueOf(TIME_STEP)));
			BET = Integer.valueOf(auctionSettings.getProperty("bet", String.valueOf(BET)));
			START_LOT_TIME = Integer.valueOf(auctionSettings.getProperty("start_lot_time", String.valueOf(START_LOT_TIME)));
			AUCTION_STEP_TIME = Integer.valueOf(auctionSettings.getProperty("auction_step_time", String.valueOf(AUCTION_STEP_TIME)));
			CRITICAL_MASS = Integer.valueOf(auctionSettings.getProperty("critical_mass", String.valueOf(CRITICAL_MASS)));
			DEFAULT_START_TIME = auctionSettings.getProperty("auction_start_time", DEFAULT_START_TIME);
			DEFAULT_STOP_TIME = auctionSettings.getProperty("auction_stop_time", DEFAULT_STOP_TIME);
			LOT_PROCENT = Integer.valueOf(auctionSettings.getProperty("lot_procent", String.valueOf(LOT_PROCENT)));
			LOT_REGISTER = Boolean.valueOf(auctionSettings.getProperty("lot_register", String.valueOf(LOT_REGISTER)));
		}
		catch(Exception e)
		{
			logger.severe(e.getMessage());
		}
	}
}
