package l2n.game.model.entity.l2auction;

public enum AuctionStatusEnum
{

	init,
	cncl,
	open,
	clsd,
	view,
	actv
}
