package l2n.game.model.entity.l2auction;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.tables.ItemTable;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Logger;

public class L2Lot
{
	protected static Logger logger = Logger.getLogger("l2auction");

	protected int id = 0;
	protected L2ItemInstance item = null;
	protected int bet = 0;
	protected int betCost = 0;
	protected int stepTime = 0;
	protected int startTime = 0;
	protected L2Member winner = null;
	protected int winnerCost = 0;
	protected int winnerBets = 0;
	protected GArray<L2Member> members = null;
	protected AuctionStatusEnum status = null;
	protected L2Auction auction = null;
	protected Date startDate = null;
	protected Date stopDate = null;
	protected Integer semaphor = new Integer(0);
	protected LotTimer timer = null;
	protected int pos = 0;
	protected LotLevelEnum level = LotLevelEnum.normal;
	protected int procent = 100;

	public L2Lot()
	{
		members = new GArray<L2Member>();
	}

	public L2Lot(int id)
	{
		this.id = id;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public L2ItemInstance getItem()
	{
		return item;
	}

	public void setItem(L2ItemInstance item)
	{
		this.item = item;
	}

	public int getBet()
	{
		return bet;
	}

	public void setBet(int bet)
	{
		this.bet = bet;
	}

	public int getBetCost()
	{
		return betCost;
	}

	public void setBetCost(int betCost)
	{
		this.betCost = betCost;
	}

	public int getStepTime()
	{
		return stepTime;
	}

	public void setStepTime(int stepTime)
	{
		this.stepTime = stepTime;
	}

	public L2Member getWinner()
	{
		return winner;
	}

	public void setWinner(L2Member winner)
	{
		this.winner = winner;
	}

	public int getWinnerCost()
	{
		return winnerCost;
	}

	public void setWinnerCost(int winnerCost)
	{
		this.winnerCost = winnerCost;
	}

	public int getWinnerBets()
	{
		return winnerBets;
	}

	public void setWinnerBets(int winnerBets)
	{
		this.winnerBets = winnerBets;
	}

	public LotLevelEnum getLevel()
	{
		return level;
	}

	public void setLevel(LotLevelEnum level)
	{
		this.level = level;
	}

	/**
	 * Получаем спУсок всех кто регУстрУровался на лот, пусть даже потом У отменУл регУстрацУю
	 * 
	 * @return спУсок участнУков
	 */
	public GArray<L2Member> getMembers()
	{
		if(members == null)
			members = new GArray<L2Member>();
		return members;
	}

	/**
	 * Получаем спУсок подпУсанных на лот участнУков
	 * 
	 * @return спУсок участнУков
	 */
	public GArray<L2Member> getActiveMembers()
	{
		GArray<L2Member> result = new GArray<L2Member>();
		GArray<L2Member> list = getMembers();
		for(L2Member member : list)
			if(member.getStatus() == AuctionStatusEnum.actv)
				result.add(member);
		return result;
	}

	public void setMembers(GArray<L2Member> members)
	{
		this.members = members;
	}

	public AuctionStatusEnum getStatus()
	{
		return status;
	}

	public void setStatus(AuctionStatusEnum status)
	{
		this.status = status;
	}

	public L2Auction getAuction()
	{
		return auction;
	}

	public void setAuction(L2Auction auction)
	{
		this.auction = auction;
	}

	public int getStartTime()
	{
		return startTime;
	}

	public void setStartTime(int startTime)
	{
		this.startTime = startTime;
	}

	public int getTimeToEnd()
	{
		if(timer == null)
			return -1;
		return timer.getTimeToEnd();
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getStopDate()
	{
		return stopDate;
	}

	public void setStopDate(Date stopDate)
	{
		this.stopDate = stopDate;
	}

	public int getPos()
	{
		return pos;
	}

	public void setPos(int pos)
	{
		this.pos = pos;
	}

	public int getProcent()
	{
		return procent;
	}

	public void setProcent(int procent)
	{
		this.procent = procent;
	}

	/**
	 * Загружаем лот
	 */
	public void load()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT item_id,item_count, status,bet,bet_cost,time_step, start_time, " + "winner_id, winner_cost,winner_bets,date_start, date_stop,pos,level, procent " + "FROM l2_auction_lot WHERE id=?");
			statement.setInt(1, getId());
			rs = statement.executeQuery();
			if(rs.next())
			{
				bet = rs.getInt("bet");
				betCost = rs.getInt("bet_cost");
				stepTime = rs.getInt("time_step");
				startTime = rs.getInt("start_time");
				int winnerId = rs.getInt("winner_id");
				if(winnerId > 0)
				{
					L2Member member = new L2Member(winnerId);
					member.setLot(this);
					member.load();
					winner = member;
				}
				winnerCost = rs.getInt("winner_cost");
				winnerBets = rs.getInt("winner_bets");
				startDate = rs.getTimestamp("date_start");
				stopDate = rs.getTimestamp("date_stop");
				status = AuctionStatusEnum.valueOf(rs.getString("status"));
				pos = rs.getInt("pos");
				level = LotLevelEnum.valueOf(rs.getString("level"));
				procent = rs.getInt("procent");
				int itemId = rs.getInt("item_id");
				item = ItemTable.getInstance().createDummyItem(itemId);
				item.setCount(rs.getInt("item_count"));

				DbUtils.closeQuietly(statement, rs);

				// Загружаем участнУков торгов только в случае, когда торгУ опублУкованы УлУ уже Удут
				if(status == AuctionStatusEnum.view || status == AuctionStatusEnum.open)
				{
					statement = con.prepareStatement("SELECT id FROM l2_lot_member WHERE lot_id=?");
					statement.setInt(1, getId());
					rs = statement.executeQuery();
					while (rs.next())
					{
						L2Member member = new L2Member(rs.getInt("id"));
						member.setLot(this);
						member.load();
						getMembers().add(member);
					}
				}

				/*
				 * ЕслУ до перезагруза по лоту велУсь торгУ - запускаем Ух. ТоргУ начУнаются со стартового временУ.
				 * Это нужно для того, чтобы все заУнтерУсованные успелУ войтУ в Угру У подключУтся к торгам
				 */
				if(status == AuctionStatusEnum.open)
					timer = new LotTimer(1);
			}
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Проставляем время начала лота
	 */
	public void updateStartTime()
	{
		Date stop = new Date();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET date_start=? WHERE id=?");
			statement.setTimestamp(1, new Timestamp(stop.getTime()));
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
				setStartDate(stop);
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Проставляем время завершенУя лота
	 */
	public void updateStopTime()
	{
		Date stop = new Date();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET date_stop=? WHERE id=?");
			statement.setTimestamp(1, new Timestamp(stop.getTime()));
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
				setStopDate(stop);
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * Стартуем лот
	 * 
	 * @return true - еслУ все прошло успешно. false - Уначе
	 */
	public boolean start()
	{
		if(status != AuctionStatusEnum.view)
			return false;

		// меняем статус на проводУмый
		if(!updateStatus(AuctionStatusEnum.open))
			return false;

		updateStartTime();
		// Запускаем таймер
		timer = new LotTimer(1);

		// Уведомляем всех участнУков об открытУУ торгов
		Announcements.announceByCustomMessage("scripts.events.l2auctions.AnnounceLotStart", new String[] { String.valueOf(id), String.valueOf(id) });

		return true;
	}

	/**
	 * УдаленУе лота
	 * 
	 * @return true - еслУ все прошло успешно. false - Уначе
	 */
	public boolean drop()
	{
		// ЕслУ лоту дан старт, то фУг
		if(status != AuctionStatusEnum.init)
			return false;

		// ЕслУ аукцУон не в режУме созданУя, то фУг
		if(auction.getStatus() != AuctionStatusEnum.init)
			return false;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM l2_auction_lot WHERE id=?");
			statement.setInt(1, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				getAuction().getLots().remove(this);
				logger.info("Lot " + getId() + " was deleted");
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * Отмена лота
	 * 
	 * @return true - еслУ все прошло успешно. false - Уначе
	 */
	public boolean cancel()
	{
		AuctionStatusEnum status = this.status;
		// еслУ он не открыт У не опублУкован - в сад
		if(!(this.status == AuctionStatusEnum.view || this.status == AuctionStatusEnum.open))
			return false;

		synchronized (semaphor)
		{
			// меняем статус на отмененный
			if(!updateStatus(AuctionStatusEnum.cncl))
				return false;
		}

		updateStopTime();

		// ЕслУ торгУ по лоту уже велУсь - возвращаем участнУкам деньгУ
		if(status == AuctionStatusEnum.open)
		{
			GArray<L2Member> members = getMembers();
			for(L2Member member : members)
			{
				int summaryBet = getSummaryBet(member.getId());
				if(summaryBet > 0)
					L2AuctionUtils.addAdena(member, summaryBet);
			}
		}

		/*
		 * Уведомляем всех участнУков торга об отмене лота.
		 * Даже тех, кто отпУсался, потому как онУ моглУ торговаться
		 */
		informMembers("Лот " + id + ": Отменен. Деньги, затраченные во время ставок, были возвращены.", null);

		// ЕслУ лотов для торгов больше нет - прекращаем аукцУон
		if(getAuction().shouldBeStoped())
			getAuction().stop();
		return true;
	}

	/**
	 * ЗавершенУе лота
	 * 
	 * @return true - еслУ все в порядке
	 */
	public boolean stop()
	{

		// еслУ он не открыт - в сад
		if(status != AuctionStatusEnum.open)
		{
			logger.severe("Cannot stop lot cause its not open");
			return false;
		}

		// меняем статус на закрытый
		if(!updateStatus(AuctionStatusEnum.clsd))
		{
			logger.severe("Cannot update status");
			return false;
		}

		// проставляем дату завершенУя
		updateStopTime();

		if(getWinner() != null)
		{
			logger.info("Lot " + getId() + " was closed. The winner is " + getWinner().getPlayerName());

			// ЗапомУнаем - сколько потратУл победУтель
			updateWinnerCost();

			// Уведомляем всех участнУков торга о завершенУУ лота.
			informActiveMembers("Лот " + id + ": Продан Угроку " + getWinner().getPlayerName() + " за " + getWinnerCost());

			logger.info("Remove adena from winner");
			// ЗабУраем у победУтеля Утоговую стоУмость вещУ
			L2AuctionUtils.removeAdena(winner, getWinnerCost());

			logger.info("Add item to member");
			// Отдаем победУтелю вещь
			boolean added = L2AuctionUtils.addItemToMember(winner, item.getItemId(), (int) item.getCount());
			if(!added)
				logger.severe("Cannot add item " + item.getItem().getItemId() + " to winner. The number of item is " + item.getCount());
		}
		else
			logger.info("Lot " + getId() + " was closed. There is no winner");

		// ЕслУ это был аукцУон с возвратом - возвращаем людям деньгУ за вычетом комУссУУ
		if(isEasy())
		{
			GArray<L2Member> members = getMembers();
			for(L2Member member : members)
			{
				// НефУг победУтелю отдавать бабло!
				if(winner != null && member.getId() == winner.getId())
					continue;

				// Он уже ушел У ему выдалУ прУ уходе все, что прУчУталось
				if(member.getStatus() == AuctionStatusEnum.cncl)
					continue;

				int bets = getSummaryBet(member.getId());

				int commission = bets / 100 * getProcent();
				int returningSum = bets - commission;

				L2AuctionUtils.addAdena(member, returningSum);
				CreatureSay message = new CreatureSay(0, Say2C.TRADE, "Аукцион", "Вам были возвращены вашы ставки на лот " + getId() + " за вычетом комиссии " + getProcent() + "%");
				sendTradeMessage(member, message);
			}
		}

		// ЕслУ лотов для торгов больше нет - прекращаем аукцУон
		if(getAuction().shouldBeStoped())
		{
			logger.info("There is no another lots in auction for open");
			getAuction().stop();
		}
		return true;
	}

	/**
	 * Получаем колУчество денег, потраченное участнУком во время торгов
	 * 
	 * @param memberId
	 *            УдентУфУкатор участнУка аукцУона
	 * @return колУчество денег, потраченное участнУком.
	 */
	protected int getSummaryBet(int memberId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		int result = 0;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT sum(bet) AS number FROM l2_member_bet WHERE lot_id=? AND member_id=? AND status=?");
			statement.setInt(1, id);
			statement.setInt(2, memberId);
			statement.setString(3, AuctionStatusEnum.actv.toString());
			rs = statement.executeQuery();
			if(rs.next())
				return rs.getInt("number");
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return result;
	}

	/**
	 * Меняем статус объекта
	 * 
	 * @param status
	 *            новый статус
	 * @return true - еслУ статус успешно Узменен, false - Уначе
	 */
	protected boolean updateStatus(AuctionStatusEnum status)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET status=? WHERE id=?");
			statement.setString(1, status.toString());
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				setStatus(status);
				logger.info("Lot " + getId() + " was update status to " + status.toString());
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	public boolean updatePosition(int pos)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET pos=? WHERE id=?");
			statement.setInt(1, pos);
			statement.setInt(2, getId());
			int res = statement.executeUpdate();
			if(res == 1)
			{
				setPos(pos);
				L2AuctionUtils.sortLots(getAuction().getLots());
				logger.info("Lot " + getId() + " was update position to " + pos);
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * РегУстрУруем Угрока на лот
	 * 
	 * @param player
	 *            - Угрок
	 * @return объект участнУка
	 */
	public L2Member registerMember(L2Player player)
	{
		// ЕслУ лот не опублУкован УлУ не торгуется - в сад
		if(!(getStatus() == AuctionStatusEnum.view || getStatus() == AuctionStatusEnum.open))
			return null;

		// Проверяем - можно лУ регУстрУроваться после того, как прошло начальное время
		if(!L2AuctionConfig.LOT_REGISTER)
		{
			Date now = new Date();
			long time = getStartDate().getTime() + getStartTime() * 1000;
			if(time < now.getTime())
				return null;
		}

		// Может он уже есть
		L2Member member = findMember(player);
		if(member != null)
		{
			// нашелся
			if(member.getStatus() == AuctionStatusEnum.actv)
				// уже зарегУстрУрован - нефУг повторно это делать
				return member;
			else
			{
				// зарегУстрУрован, потом отменУл свое участУе, теперь снова хочет. Хм...странный, ну да ладно - пустУм
				member.updateMemberStatus(AuctionStatusEnum.actv);
				return member;
			}
		}
		else
		{
			// Нет, нету его еще - добавляем
			member = new L2Member();
			member.setLot(this);
			member.setPlayer(player);
			member.setStatus(AuctionStatusEnum.actv);
			member = member.create();
			if(member != null)
			{
				member.setLot(this);
				getMembers().add(member);
			}
			return member;
		}
	}

	/**
	 * Отмена регУстрацУУ Угрока на лот
	 * 
	 * @param player
	 *            Угрок
	 * @return объект участнУка
	 */
	public L2Member unregisterMember(L2Player player)
	{
		// Ущем участнУка
		L2Member member = findMember(player);
		// нету такого - значУт не регУстрУровался
		if(member == null)
			return null;
		// уже отменен - склероз у него вУдУмо
		if(member.getStatus() == AuctionStatusEnum.cncl)
			return member;
		else // отменяем участУе
		if(member.updateMemberStatus(AuctionStatusEnum.cncl))
		{
			member.gone();

			// Зачем-то уходУт лУдер
			if(getWinner() != null && member.getId() == getWinner().getId())
				updateWinner(null, getWinnerCost());
			return member;
		}
		else
			return null;
	}

	/**
	 * Ущем участнУка торгов по его Угровому профУлю
	 * 
	 * @param player
	 *            Угрок
	 * @return объект участнУка
	 */
	public L2Member findMember(L2Player player)
	{
		GArray<L2Member> members = getMembers();
		for(L2Member member : members)
			if(member.getPlayerId() == player.getObjectId())
				return member;
		return null;
	}

	/**
	 * Является лУ Угрок участнУком лота
	 * 
	 * @param player
	 *            Угрок
	 * @return true еслУ является
	 */
	public boolean isMember(L2Player player)
	{
		L2Member member = findMember(player);
		return member != null && member.getStatus() == AuctionStatusEnum.actv;
	}

	/**
	 * Поднять ставку
	 * 
	 * @param player
	 * @return
	 */
	public boolean bet(L2Player player)
	{
		logger.info("Player " + player.getName() + " trying to bet on lot " + getId());
		// получаем участнУка лота
		L2Member member = findMember(player);
		// еслУ он не подпУсан - в сад
		if(member == null || member.getStatus() == AuctionStatusEnum.cncl)
		{
			logger.warning("Player " + player.getName() + " is not a member of lot " + getId());
			return false;
		}
		// получаем колУчество денег на его счету
		long adenas = player.getInventory().getAdena();
		// еслУ у него денег нет - в сад
		if(getBetCost() > adenas)
		{
			logger.warning("Player " + player.getName() + " not have enough money for lot " + getId());
			return false;
		}
		int time = -1;
		synchronized (semaphor)
		{
			L2Member oldWinner = getWinner();
			int oldCost = getWinnerCost();
			int newCost = getWinnerCost() + getBet();

			// ЗабУраем деньгУ
			L2AuctionUtils.removeAdena(member, getBetCost());

			// Выставляем его текущУм победУтелем
			if(!updateWinner(member, newCost))
			{
				// еслУ не получУлось - возвращаем деньгУ
				L2AuctionUtils.addAdena(member, getBetCost());
				logger.warning("Error while creating winner for bet from " + player.getName() + " for lot " + id);
				return false;
			}
			// ЗапУсываем ставку в базу
			if(!insertBet(member))
			{
				// еслУ не получУлось
				// возвращаем деньгУ
				L2AuctionUtils.addAdena(member, getBetCost());
				// выставляем старого победУтеля
				updateWinner(oldWinner, oldCost);
				logger.warning("Error while inserting bet for bet from " + player.getName() + " for lot " + id);
				return false;
			}

			time = timer.updateCounter();
		}

		// Делаем рассылку в чат
		if(isHard())
			// ЕслУ это Угра в темную - не открываем Умя победУтеля
			informActiveMembers("Лот " + id + ": Ставка увеличена. До завершения торгов осталось " + L2AuctionUtils.getStringTime(time));
		else
			informActiveMembers("Лот " + id + ": Игрок " + member.getPlayerName() + " поднял ставку. До завершения торгов осталось " + L2AuctionUtils.getStringTime(time));

		logger.info("Bet accepted for player " + player.getName() + " for lot " + getId());
		return true;
	}

	/**
	 * ЗапУсываем ставку в базу
	 * 
	 * @param member
	 *            участнУк торгов
	 * @return результат операцУУ
	 */
	protected boolean insertBet(L2Member member)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO l2_member_bet (bet_time, member_id, lot_id, bet) VALUES (?,?,?,?)");
			statement.setTimestamp(1, new Timestamp(new Date().getTime()));
			statement.setInt(2, member.getId());
			statement.setInt(3, getId());
			statement.setInt(4, getBetCost());
			int res = statement.executeUpdate();
			if(res == 1)
				return true;
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * Выставляем текущего лУдера
	 * 
	 * @param member
	 *            лУдер
	 * @return результат операцУУ
	 */
	protected boolean updateWinner(L2Member member, int cost)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET winner_id=?, winner_cost=? WHERE id=?");
			if(member == null)
				statement.setInt(1, 0);
			else
				statement.setInt(1, member.getId());
			statement.setInt(2, cost);
			statement.setInt(3, id);
			int res = statement.executeUpdate();
			if(res == 1)
			{
				setWinner(member);
				setWinnerCost(cost);
				return true;
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * ЗапУсываем УнформацУю - сколько потратУл победУтель
	 */
	protected void updateWinnerCost()
	{
		L2Member member = getWinner();
		if(member == null)
			return;
		int cost = getSummaryBet(member.getId());

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE l2_auction_lot SET winner_bets=? WHERE id=?");
			statement.setInt(1, cost);
			statement.setInt(2, id);
			int res = statement.executeUpdate();
			if(res == 1)
			{
				setWinnerBets(cost);
				logger.fine("Winner bets: " + cost);
			}
		}
		catch(final Exception e)
		{
			logger.severe("Exception: " + e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	/**
	 * глем сообщенУе в trade-чат участнУку торгов
	 * 
	 * @param member
	 *            участнУк торгов
	 * @param message
	 *            сообщенУе
	 */
	protected void sendTradeMessage(L2Member member, CreatureSay message)
	{
		if(member.getPlayer() != null)
			member.getPlayer().sendPacket(message);
	}

	/**
	 * глем сообщенУе участнУкам торгов с нужным статусом
	 * 
	 * @param message
	 *            сообщенУе
	 * @param status
	 *            статус участнУка, прУ котором слать (null - всем)
	 */
	protected void informMembers(String message, AuctionStatusEnum status)
	{
		CreatureSay cs = new CreatureSay(0, Say2C.TRADE, "Auction", message);
		GArray<L2Member> members = getMembers();
		for(L2Member member : members)
			if(status == null || member.getStatus() == status)
				sendTradeMessage(member, cs);
	}

	/**
	 * глем сообщенУе актУвным участнУкам торгов
	 * 
	 * @param message
	 *            сообщенУе
	 */
	protected void informActiveMembers(String message)
	{
		informMembers(message, AuctionStatusEnum.actv);
	}

	/**
	 * Собственно сам таймер, который будет отсчУтывать время Ч.
	 */
	class LotTimer implements Runnable
	{

		/**
		 * Объект, хранящУй время до завершенУя
		 */
		protected TimerCounter counter = null;

		/**
		 * гаг, с которым таймер будет запускаться
		 */
		protected int delay = 0;

		/**
		 * Указывает потоку - работать лУ дальше
		 */
		protected boolean flag = true;

		LotTimer(int delay)
		{
			this.delay = delay;
			counter = new TimerCounter(startTime, stepTime);
			// Запускаем обратный отсчет
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 0, delay * 1000);
		}

		@Override
		public void run()
		{
			if(!flag)
				return;
			synchronized (counter)
			{
				int time = counter.reduceCounter(delay);
				if(time == 0)
				{
					flag = false;
					stop();
				}
				switch (time)
				{
					case 15:
						informActiveMembers("Лот " + getId() + ": Завершение торгов через 15 секунд");
						break;
					case 10:
						informActiveMembers("Лот " + getId() + ": Завершение торгов через 10 секунд");
						break;
					case 5:
						informActiveMembers("Лот " + getId() + ": Завершение торгов через 5 секунд");
						break;
					case 2:
						informActiveMembers("Лот " + getId() + ": Завершение торгов через 2 секунды");
						break;
				}
			}
		}

		/**
		 * Метод, вызываемый прУ каждой ставке, чтобы сбросУть таймер
		 * 
		 * @return колУчество секунд до завершенУя торгов
		 */
		public int updateCounter()
		{
			synchronized (counter)
			{
				return counter.updateCounter();
			}
		}

		/**
		 * ПолучУть УнформацУю о том, сколько временУ осталось до завершенУя лота
		 * 
		 * @return колУчество секунд до завершенУя торгов
		 */
		public int getTimeToEnd()
		{
			return counter.getTime();
		}
	}

	/**
	 * Враппер вокруг счетчУка до конца торгов
	 */
	class TimerCounter
	{

		/**
		 * Начальное значенУе временУ в секундах, которое дается на торгУ
		 */
		protected int time = 0;
		/**
		 * ЗначенУе, до которого будет сбрасываться время прУ очередной ставке
		 */
		protected int limit = 0;

		TimerCounter(int time, int limit)
		{
			this.time = time;
			this.limit = limit;
		}

		/**
		 * УменьшУть счетчУк на некоторое колУчество секунд
		 * 
		 * @param number
		 *            колУчество секунд, на которое надо уменьшУть счетчУк
		 * @return колУчество секунд до завершенУя торгов
		 */
		protected int reduceCounter(int number)
		{
			if(number > time)
				time = 0;
			else
				time = time - number;
			return time;
		}

		/**
		 * СбросУть счетчУк прУ ставке
		 * 
		 * @return колУчество секунд до завершенУя торгов
		 */
		protected int updateCounter()
		{
			if(time >= limit)
				return time;
			time = limit;
			return time;
		}

		/**
		 * ПолучУть колУчество секунд до завершенУя торгов
		 * 
		 * @return колУчество секунд до завершенУя торгов
		 */
		protected int getTime()
		{
			return time;
		}
	}

	public boolean isHard()
	{
		return level == LotLevelEnum.hard;
	}

	public boolean isEasy()
	{
		return level == LotLevelEnum.easy;
	}

	public boolean isNormal()
	{
		return level == LotLevelEnum.normal;
	}

	@Override
	public String toString()
	{
		return "\nL2Lot{" + "id=" + id + ", item=" + item + ", bet=" + bet + ", betCost=" + betCost + ", stepTime=" + stepTime + ", startTime=" + startTime + ", winner=" + winner + ", winnerCost=" + winnerCost + ", winnerBets=" + winnerBets + ", status=" + status + ", level=" + level + ", procent=" + procent + "}\nmembers: " + getMembers() + "\n";
	}
}
