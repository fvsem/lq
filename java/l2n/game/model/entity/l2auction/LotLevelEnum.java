package l2n.game.model.entity.l2auction;

/**
 * Это энумерация типов лота
 * normal - проведение торгов в обычном порядке
 * hard - игра в слепую
 * easy - возврат денег проигравшим, за исключением некоторого процента
 * <p/>
 * Created 30.10.2009 23:08:19
 * 
 */
public enum LotLevelEnum
{
	easy,
	normal,
	hard
}
