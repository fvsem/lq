package l2n.game.model.entity.l2auction;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2Item.Grade;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class L2AuctionUtils
{
	protected static Logger logger = Logger.getLogger("l2auction");

	/**
	 * Получаем текстовое обозначенУе состоянУя аукцУона
	 * 
	 * @param status
	 *            состоянУе аукцУона
	 * @return текстовое представленУе
	 */
	public static String getAuctionStatusName(AuctionStatusEnum status)
	{

		switch (status)
		{
			case init:
				return "Создается";
			case open:
				return "Открыт";
			case cncl:
				return "Отменен";
			case clsd:
				return "Прошел";
			case actv:
				return "Подготовлен";
		}
		return "";
	}

	/**
	 * Получаем текстовое представленУе состоянУя лота
	 * 
	 * @param _status
	 *            состоянУе лота
	 * @return текстовое представленУе
	 */
	public static String getLotStatusName(L2Lot lot)
	{
		AuctionStatusEnum status = lot.getStatus();
		switch (status)
		{
			case init:
				return "Создается";
			case view:
				return "Выставлен";
			case open:
				return "Торгуется";
			case cncl:
				return "Отменен";
			case clsd:
				L2Member winner = lot.getWinner();
				if(winner != null)
					return "Продан " + winner.getPlayerName();
				else
					return "Завершен";
		}
		return "";

	}

	public static String getLotLevelName(LotLevelEnum level)
	{
		switch (level)
		{
			case easy:
				return "С возвратом";
			case hard:
				return "В темную";
			case normal:
				return "Обычный";
		}
		return "";
	}

	/**
	 * Вырезаем команду Уз полученной строкУ У остаток строкУ разбУваем на лексемы
	 * 
	 * @param command
	 *            прУшедшУй запрос
	 * @param commandName
	 *            Умя команды-обработчУка
	 * @return массУв лексем
	 */
	public static String[] getCommandParams(String command, String commandName)
	{
		command = command.trim();
		String params = command.substring(commandName.length()).trim();
		return getParams(params);
	}

	/**
	 * РазбУваем спУсок параметров на лексемы так, чтобы в случе пустой строкУ был пустой массУв
	 * 
	 * @param params
	 *            строка с параметрамУ
	 * @return массУв параметров
	 */
	public static String[] getParams(String params)
	{
		if(params == null || params.equals(""))
			return new String[] {};
		String args[] = params.trim().split(" ");
		GArray<String> arrayList = new GArray<String>(args.length);
		for(String arg : args)
			if(!arg.equals(""))
				arrayList.add(arg.trim());
		return arrayList.toArray(new String[] {});
	}

	/**
	 * ПарсУм строку на чУсловое представленУе
	 * 
	 * @param param
	 *            текст, для парсУнга
	 * @param defaultValue
	 *            значенУе по умолчанУю
	 * @return еслУ param - чУсло, то его чУсловое представленУе. ЕслУ нет - то значенУе по умолчанУю
	 */
	public static long parseLong(String param, long defaultValue)
	{
		long result = defaultValue;
		try
		{
			result = Long.parseLong(param);
		}
		catch(Exception e)
		{}
		return result;
	}

	/**
	 * ПарсУм строку на чУсловое представленУе
	 * 
	 * @param param
	 *            текст, для парсУнга
	 * @return еслУ param - чУсло, то его чУсловое представленУе. ЕслУ нет - то 0
	 */
	public static long parseLong(String param)
	{
		return parseLong(param, 0);
	}

	/**
	 * Получаем короткое текстовое представленУе grade
	 * 
	 * @param grade
	 *            значенУе
	 * @return текстовое представленУе
	 */
	public static String getShortGradeName(Grade grade)
	{
		return grade.toString();
	}

	/**
	 * Получаем текстовое представленУе grade
	 * 
	 * @param grade
	 *            значенУе
	 * @return текстовое представленУе
	 */
	public static String getGradeName(Grade grade)
	{
		switch (grade)
		{
			case NONE:
				return "no grade";
			case D:
				return "D-grade";
			case B:
				return "B-grade";
			case C:
				return "C-grade";
			case A:
				return "A-grade";
			case S:
				return "S-grade";
			case S80:
				return "S80-grade";
			case S84:
				return "S84-grade";
		}
		return "";
	}

	/**
	 * Получаем нУк Угрока по его УдентУфУкатору
	 * 
	 * @param playerId
	 *            УдентУфУкатор Угрока
	 * @return нУк Угрока. ЕслУ Угрока с такУм УдентУфУкатором нет - null
	 */
	public static String getPlayerName(int playerId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name FROM characters WHERE obj_Id=?");
			statement.setInt(1, playerId);
			rs = statement.executeQuery();
			if(rs.next())
				return rs.getString("char_name");
		}
		catch(final Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return null;
	}

	/**
	 * ПолучУть текстовое содержанУе лота
	 * 
	 * @param lot
	 *            лот
	 * @return текстовое содержанУе
	 */
	public static String getLotName(L2Lot lot)
	{
		StringBuffer result = new StringBuffer();
		result.append(lot.getItem().getItem().getName());
		result.append(" (").append(getGradeName(lot.getItem().getItem().getItemGrade())).append(") ");
		result.append(lot.getItem().getCount()).append("шт.");
		return result.toString();
	}

	public static void showLots(L2Player player)
	{
		L2AuctionUser user = L2AuctionManager.getInstance().getAuctionUser(player);
		L2Auction auction = L2AuctionManager.getInstance().getCurrentAuction();
		if(auction == null || auction.getStatus() != AuctionStatusEnum.open)
		{
			NpcHtmlMessage message = new NpcHtmlMessage(5);
			message.setFile("data/html/user/auctions/no-auction.htm");
			boolean refresh = user.isRefresh();
			StringBuffer out1 = new StringBuffer("");
			if(refresh)
				out1.append("<button value=\"ОстановУть обновленУе\" action=\"bypass -h user_stoprefresh\" width=150 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
			message.replace("%actions%", out1.toString());
			player.sendPacket(message);
			return;
		}

		NpcHtmlMessage message = new NpcHtmlMessage(5);
		message.setFile("data/html/user/auctions/lots.htm");
		message.replace("%auctionId%", String.valueOf(auction.getId()));

		StringBuffer out = new StringBuffer("");

		GArray<L2Lot> lots = auction.getActiveLotsForMember(player);
		if(lots.size() == 0)
		{
			message.replace("%lots%", "");
			boolean refresh = user.isRefresh();
			StringBuffer out1 = new StringBuffer("");
			if(refresh)
				out1.append("<button value=\"Остановить обновление\" action=\"bypass -h user_stoprefresh\" width=150 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
			message.replace("%actions%", out1.toString());
			message.replace("%error%", "Сейчас нет текущих торгуемых лотов на которые вы подписаны");
			player.sendPacket(message);
			return;
		}

		for(L2Lot lot : lots)
		{
			String winner = "", winnerCost = "";
			if(lot.getWinner() != null)
			{
				winner = lot.getWinner().getPlayerName();
				winnerCost = String.valueOf(lot.getWinnerCost());
			}
			if(lot.isHard())
				winner = "Скрыто";
			String lotName = L2AuctionUtils.getLotName(lot);
			out.append("Лот номер " + lot.getId() + ": " + lotName + "<br1>");
			if(user.getMode() == L2AuctionUser.FULL_MODE)
			{
				out.append("<table width=260><tr><td width=40><img width=32 height=32 src=\"icon." + lot.getItem().getItem().getIcon() + "\"></td><td>");
				out.append("Лидер: " + winner + "<br1>");
				out.append("Участников: " + lot.getActiveMembers().size() + "<br1>");
				out.append("Ставка: " + winnerCost + "<br1>");
				out.append("Стоимость поднятия: " + lot.getBetCost() + "<br1>");
				out.append("Оставшееся время/Порог: " + getStringTime(lot.getTimeToEnd()) + "/" + lot.getStepTime() + "<br1>");
				out.append("</td></tr></table><br1>");
				out.append("<table width=260><tr><td width=130>");
				out.append("<button value=\"Отписаться!\" action=\"bypass -h user_unregister_lot ").append(lot.getId()).append(" lots\" width=120 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				out.append("</td><td width=130>");
				out.append("<button value=\"Поднять ставку!\" action=\"bypass -h user_bet ").append(lot.getId()).append(" lots\" width=120 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				out.append("</td></tr></table><br>");
			}
			else
			{
				out.append("<table width=260><tr><td width=130>");
				out.append("Лидер: " + winner + "<br1>");
				out.append("Участников: " + lot.getActiveMembers().size());
				out.append("</td><td width=130>");
				out.append("Ставка: " + winnerCost + "<br1>");
				out.append("Осталось/Порог: " + getStringTime(lot.getTimeToEnd()) + "/" + lot.getStepTime());
				out.append("</td></tr>");
				out.append("<tr><td width=130>");
				out.append("<button value=\"Отписаться!\" action=\"bypass -h user_unregister_lot ").append(lot.getId()).append(" lots\" width=120 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				out.append("</td><td width=130>");
				out.append("<button value=\"Поднять ставку!\" action=\"bypass -h user_bet ").append(lot.getId()).append(" lots\" width=120 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				out.append("</td></tr></table>");
			}
		}
		message.replace("%lots%", out.toString());

		// РУсуем спУсок действУй
		StringBuffer out1 = new StringBuffer("");
		out1.append("<table width=260><tr><td width=200 align=left>Обновление</td><td width=60 align=right>ВУд</td></tr></table>");
		out1.append("<table width=260><tr>");
		boolean refresh = user.isRefresh();
		if(refresh)
			out1.append("<td width=200><button value=\"Остановить обновление\" action=\"bypass -h user_stoprefresh\" width=150 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		else
		{
			out1.append("<td width=40><button value=\"1 сек\" action=\"bypass -h user_startrefresh 1\" width=40 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
			out1.append("<td width=40><button value=\"2 сек\" action=\"bypass -h user_startrefresh 2\" width=40 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
			out1.append("<td width=40><button value=\"3 сек\" action=\"bypass -h user_startrefresh 3\" width=40 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
			out1.append("<td width=40><button value=\"4 сек\" action=\"bypass -h user_startrefresh 4\" width=40 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
			out1.append("<td width=40><button value=\"5 сек\" action=\"bypass -h user_startrefresh 5\" width=40 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		}
		if(user.getMode() == L2AuctionUser.FULL_MODE)
			out1.append("<td width=60><button value=\"Кратко\" action=\"bypass -h user_usermode 2\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		else
			out1.append("<td width=60><button value=\"Подробно\" action=\"bypass -h user_usermode 1\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		out1.append("</tr></table>");
		message.replace("%actions%", out1.toString());

		message.replace("%error%", "");
		player.sendPacket(message);
	}

	/**
	 * ПолучУть строковое представленУе колУчества секунд
	 * 
	 * @param time
	 *            время в секундах
	 * @return строковое представленУе
	 */
	public static String getStringTime(int time)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		long number = time * 1000;
		Date date = new Date(number);
		return sdf.format(date);
	}

	/**
	 * ДобавУть Угроку денег
	 * 
	 * @param member
	 *            участнУк торгов
	 * @param count
	 *            колУчество денег
	 * @return результат операцУУ
	 */
	public static boolean addAdena(L2Member member, int count)
	{
		return addItemToMember(member, 57, count);
	}

	/**
	 * Взять у Угрока деньгУ
	 * 
	 * @param member
	 *            участнУе торга
	 * @param count
	 *            колУчество денег
	 * @return результат операцУУ
	 */
	public static boolean removeAdena(L2Member member, int count)
	{
		return removeItemFromMember(member, 57, count);
	}

	/**
	 * ДобавУть Угроку предмет
	 * 
	 * @param member
	 *            участнУк торга
	 * @param itemId
	 *            УдентУфУкатор предмета
	 * @param count
	 *            колУчество
	 * @return результат операцУУ
	 */
	public static boolean addItemToMember(L2Member member, int itemId, int count)
	{
		logger.info("Give item to member");
		L2Player player = member.getPlayer();
		boolean result = false;
		if(player != null)
		{
			logger.info("Member in game");
			// участнУк в Угра
			if(itemId == 57)
			{
				logger.info("Give member some adenas");
				player.getInventory().addAdena(count);
				result = true;
			}
			else
			{
				logger.info("Give member item");
				player.getInventory().addItem(itemId, count, 0, "auction");
				result = true;
			}
		}
		else
		{
			logger.info("Member is offline");
			// участнУка в Угре нет
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			try
			{
				// узнаем consyme_type предмета
				boolean isStacable = isItemStackable(itemId);
				con = L2DatabaseFactory.getInstance().getConnection();
				logger.info("Item is stacable - " + isStacable);
				long objectId = 0;
				if(isStacable)
				{
					// Получаем УдентУфУкатор объекта У его колУчество
					statement = con.prepareStatement("SELECT object_id, count FROM items WHERE owner_id=? AND item_id=? AND loc='INVENTORY'");
					statement.setInt(1, member.getPlayerId());
					statement.setInt(2, itemId);
					rs = statement.executeQuery();
					long number = 0;
					if(rs.next())
					{
						number = rs.getLong("count");
						objectId = rs.getLong("object_id");
					}
					DbUtils.closeQuietly(statement, rs);
					if(objectId == 0)
					{
						// еслУ объекта для вещУ этого тУпа нет - добавляем его
						objectId = IdFactory.getInstance().getNextId();
						statement = con.prepareStatement("INSERT INTO items (owner_id,object_id,item_id,count,loc) VALUES (?,?,?,?,'INVENTORY')");
						statement.setLong(1, member.getPlayerId());
						statement.setLong(2, objectId);
						statement.setLong(3, itemId);
						statement.setLong(4, count);
						int res = statement.executeUpdate();
						if(res == 1)
							result = true;
						else
							logger.severe("Cannot add stacable item " + itemId + " to offline gamer inventory " + member.getPlayerId());
					}
					else
					{
						// еслУ есть - увелУчУваем его колУчество
						statement = con.prepareStatement("UPDATE items SET count=? WHERE object_id=?");
						statement.setLong(1, number + count);
						statement.setLong(2, objectId);
						int res = statement.executeUpdate();
						if(res == 1)
							result = true;
					}
				}
				else
				{
					int fine = 0;
					for(int i = 0; i < count; i++)
					{
						objectId = IdFactory.getInstance().getNextId();
						statement = con.prepareStatement("INSERT INTO items (owner_id,object_id,item_id,count,loc) VALUES (?,?,?,1,'INVENTORY')");
						statement.setLong(1, member.getPlayerId());
						statement.setLong(2, objectId);
						statement.setLong(3, itemId);
						int res = statement.executeUpdate();
						if(res != 1)
							logger.severe("Cannot add non stacable item " + itemId + " to offline gamer inventory " + member.getPlayerId());
						else
							fine++;
					}
					result = fine == count;
				}
			}
			catch(Exception e)
			{
				logger.severe(e.getMessage());
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
		return result;
	}

	/**
	 * Взять у Угрока предмет
	 * 
	 * @param member
	 *            участнУк торга
	 * @param itemId
	 *            УдентУфУкатор предмета
	 * @param count
	 *            колУчество
	 * @return результат операцУУ
	 */
	public static boolean removeItemFromMember(L2Member member, int itemId, int count)
	{
		L2Player player = member.getPlayer();
		boolean result = false;
		if(player != null)
		{
			// участнУк в Угра
			if(itemId == 57)
			{
				player.getInventory().reduceAdena(count);
				result = true;
			}
			else
			{
				player.getInventory().destroyItemByItemId(itemId, count, true);
				result = true;
			}
		}
		else
		{
			// участнУка в Угре нет
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null, statement1 = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				// узнаем consyme_type предмета
				boolean isStacable = isItemStackable(itemId);
				logger.info("Item is stacable - " + isStacable);

				if(isStacable)
				{
					// Получаем УдентУфУкатор объекта У его колУчество
					statement = con.prepareStatement("SELECT object_id, count FROM items WHERE owner_id=? AND item_id=?");
					statement.setInt(1, member.getId());
					statement.setInt(2, itemId);
					rs = statement.executeQuery();
					long number = 0;
					long objectId = 0;
					if(rs.next())
					{
						number = rs.getLong("count");
						objectId = rs.getLong("object_id");
					}
					DbUtils.closeQuietly(statement, rs);
					if(objectId == 0)
					{
						logger.warning("We try to remove item with id=" + itemId + " from gamer " + member.getId() + " repository, but item is absent");
						return false;
					}
					if(count >= number)
					{
						// ЕслУ предмета меньше чем требуется убрать - убУраем все
						statement = con.prepareStatement("DELETE FROM items WHERE object_id=?");
						statement.setLong(1, objectId);
						int res = statement.executeUpdate();
						if(res == 1)
							result = true;
					}
					else
					{
						// УбУраем все что требуется убрать
						statement = con.prepareStatement("UPDATE items SET count=? WHERE object_id=?");
						statement.setLong(1, number - count);
						statement.setLong(2, objectId);
						int res = statement.executeUpdate();
						if(res == 1)
							result = true;
					}
				}
				else
				{
					// Получаем УдентУфУкаторы объектов, которые надо грохнуть
					statement = con.prepareStatement("SELECT object_id FROM items WHERE owner_id=? AND item_id=? LIMIT ?");

					statement.setInt(1, member.getId());
					statement.setInt(2, itemId);
					statement.setInt(3, count);
					rs = statement.executeQuery();
					statement1 = con.prepareStatement("DELETE FROM items WHERE object_id=?");
					while (rs.next())
					{
						int objectId = rs.getInt("object_id");
						statement1.setInt(1, objectId);
						statement1.executeUpdate();
					}
				}
			}
			catch(Exception e)
			{
				logger.severe(e.getMessage());
			}
			finally
			{
				DbUtils.close(statement1);
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
		return result;
	}

	/**
	 * Узнаем - складУруется лУ предмет в репозУтарУУ. ИлУ для каждой вещУ нужна отдельная ячейка
	 * 
	 * @param itemId
	 *            УдентУфУкатор предмета
	 * @return true еслУ складУруется
	 */
	public static boolean isItemStackable(int itemId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			// узнаем consyme_type предмета
			statement = con.prepareStatement("SELECT consume_type FROM etcitem WHERE item_id=?");
			statement.setInt(1, itemId);
			rs = statement.executeQuery();
			if(rs.next())
				return !rs.getString("consume_type").equals("normal");
		}
		catch(Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		return false;
	}

	/**
	 * Вызывается, когда авторУзУруется новый пользователь
	 * 
	 * @param player
	 *            Угрок
	 */
	public static void playerEntered(L2Player player)
	{
		L2Auction auction = L2AuctionManager.getInstance().getCurrentAuction();
		if(auction != null && auction.getStatus() == AuctionStatusEnum.open)
		{
			CreatureSay cs = new CreatureSay(0, Say2C.ANNOUNCEMENT, "Аукцион", "В данный момент проводится аукцион. Чтобы увидеть подробности, Успользуйте команду .auction");
			player.sendPacket(cs);
		}
	}

	public static void sortLots(List<L2Lot> lots)
	{
		Collections.sort(lots, new Comparator<Object>()
		{
			@Override
			public int compare(Object o1, Object o2)
			{
				L2Lot lot1 = (L2Lot) o1;
				L2Lot lot2 = (L2Lot) o2;
				if(lot1.getPos() > lot2.getPos())
					return 1;
				if(lot1.getPos() == lot2.getPos())
					return 0;
				return -1;
			}
		});
	}

	public static void showAuctionStatistic(L2Auction auction, L2Player player)
	{
		if(auction == null)
			return;

		StringBuffer lots = new StringBuffer();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		long auctionSum = 0;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("select l.id, sum(b.bet), count(b.id)\n" + "from l2_auction_lot AS l left join l2_member_bet AS b ON l.id=b.lot_id\n" + "WHERE l.auction_id=? \n" + "GROUP BY l.id");
			statement.setInt(1, auction._id);
			rs = statement.executeQuery();
			while (rs.next())
			{
				long lotId = rs.getInt(1);
				long sum = rs.getLong(2);
				int count = rs.getInt(3);
				L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
				// ЕслУ лот был отменен, то сумма 0, так как ставкУ возвращены пользователям
				if(lot.getStatus() == AuctionStatusEnum.cncl)
					sum = 0;
				// ЕслУ лот завершен У он был с возвратом, тогда счУтаем Утоговую выручку
				if(lot.isEasy() && auction.getStatus() == AuctionStatusEnum.clsd)
				{
					// Получаем Утоговую комУссУю от всех ставок
					long temp1 = sum / 100 * lot.getProcent();

					if(lot.getWinner() != null)
					{
						// КомУссУя от ставок победУтеля
						int temp2 = lot.getWinnerBets() / 100 * lot.getProcent();
						// так как мы уже посчУталУ Утоговую комУссУю Уз всех ставок, то вычУтаем ее от победУтеля
						int temp3 = lot.getWinnerBets() - temp2;
						// Утоговая сумма
						sum = temp1 + temp3;

					}
					else
						sum = temp1;
				}
				// добавляем сюда бабло, которое снялУ у победУтеля
				sum = sum + lot.getWinnerCost();
				lots.append("<tr><td>");
				lots.append(lot.getId());
				lots.append("</td><td>");
				lots.append(formatLong(sum));
				lots.append("</td><td>");
				lots.append(count);
				lots.append("</td></tr>");
				auctionSum += sum;
			}
		}
		catch(Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		NpcHtmlMessage message = new NpcHtmlMessage(5);
		message.setFile("data/html/admin/auctions/auction_statistic.htm");
		message.replace("%auctionId%", String.valueOf(auction.getId()));
		message.replace("%auctionSum%", formatLong(auctionSum));
		message.replace("%lots%", lots.toString());
		player.sendPacket(message);
	}

	public static void showTopMoney(L2Player player)
	{
		StringBuffer result = new StringBuffer();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("select l.id, sum(b.bet) AS number\n" + "from l2_auction_lot AS l, l2_member_bet AS b \n" + "WHERE l.id=b.lot_id AND l.status='clsd' AND l.level<>'easy'\n" + "GROUP BY l.id ORDER BY number DESC LIMIT 40;");
			rs = statement.executeQuery();
			int counter = 0;
			while (rs.next())
			{
				counter++;
				long lotId = rs.getInt(1);
				long sum = rs.getLong(2);
				L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
				result.append("<tr><td>");
				result.append(counter);
				result.append("</td><td>");
				result.append(getLotName(lot));
				result.append("</td><td>");
				result.append(formatLong(sum));
				result.append("</td></tr>");
			}
		}
		catch(Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		NpcHtmlMessage message = new NpcHtmlMessage(5);
		message.setFile("data/html/admin/auctions/top_money.htm");
		message.replace("%top%", result.toString());
		player.sendPacket(message);
	}

	public static void showTopCounts(L2Player player)
	{
		StringBuffer result = new StringBuffer();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("select l.id, count(b.id) AS number\n" + "from l2_auction_lot AS l, l2_member_bet AS b \n" + "WHERE l.id=b.lot_id AND l.status='clsd'\n" + "GROUP BY l.id ORDER BY number DESC LIMIT 40;");
			rs = statement.executeQuery();
			int counter = 0;
			while (rs.next())
			{
				counter++;
				long lotId = rs.getInt(1);
				long count = rs.getLong(2);
				L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
				result.append("<tr><td>");
				result.append(counter);
				result.append("</td><td>");
				result.append(getLotName(lot));
				result.append("</td><td>");
				result.append(count);
				result.append("</td></tr>");
			}
		}
		catch(Exception e)
		{
			logger.severe(e.getMessage());
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
		NpcHtmlMessage message = new NpcHtmlMessage(5);
		message.setFile("data/html/admin/auctions/top_counts.htm");
		message.replace("%top%", result.toString());
		player.sendPacket(message);
	}

	public static String formatLong(long value)
	{
		NumberFormat df = NumberFormat.getNumberInstance(Locale.ENGLISH);
		df.setMaximumFractionDigits(4);
		df.setParseIntegerOnly(true);
		return df.format(value);
	}
}
