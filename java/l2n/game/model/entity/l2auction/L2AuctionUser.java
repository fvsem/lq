package l2n.game.model.entity.l2auction;

import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

/**
 * Created 30.10.2009 23:21:05
 * 
 * @author Alexey Lahtadir mailto: <a href="mailto:alexey_lahtadir@mail.ru">alexey_lahtadir@mail.ru</a>
 */
public class L2AuctionUser
{
	protected static Logger logger = Logger.getLogger("l2auction");

	public static final short FULL_MODE = 1;
	public static final short SHORT_MODE = 2;

	/** Режим отображения списка лотов */
	protected short _mode = 1;
	/** Таймер на обновлениие лотов */
	protected RefreshTimer _timer = null;
	/** Игрок */
	protected L2Player _player = null;

	public L2AuctionUser(L2Player player)
	{
		_player = player;
	}

	public short getMode()
	{
		return _mode;
	}

	public void setMode(short mode)
	{
		_mode = mode;
	}

	public L2Player getPlayer()
	{
		return _player;
	}

	public boolean isRefresh()
	{
		return _timer != null;
	}

	public void startRefresh(int rate)
	{
		if(_timer != null)
			_timer.stop();
		_timer = new RefreshTimer(rate);
		_timer.start();
	}

	public void stopRefresh()
	{
		if(_timer != null)
		{
			_timer.stop();
			_timer = null;
		}
	}

	class RefreshTimer implements Runnable
	{

		protected int _refreshRate = 0;
		protected boolean flag = true;

		public RefreshTimer(int refreshRate)
		{
			_refreshRate = refreshRate;
		}

		public void start()
		{
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 0, _refreshRate * 1000);
		}

		@Override
		public void run()
		{
			if(flag)
			{
				L2Player player = L2ObjectsStorage.getPlayer(_player.getObjectId());
				// Если выключился - отрубаем выполнение и убираем его из пользователей
				if(player == null)
				{
					stop();
					L2AuctionManager.getInstance().removeUser(L2AuctionUser.this);
				}
				else
					L2AuctionUtils.showLots(player);
			}
		}

		public void stop()
		{
			logger.info("Start refresh lots for player " + _player.getName());
			flag = false;
		}

		public int getRefreshRate()
		{
			return _refreshRate;
		}

		public void setRefreshRate(int refreshRate)
		{
			_refreshRate = refreshRate;
		}
	}
}
