package l2n.game.model.entity;

import l2n.commons.list.GArray;
import l2n.commons.util.Crontab;
import l2n.commons.util.StatsSet;
import l2n.game.instancemanager.InstanceManager.SpawnInfo;
import l2n.game.model.instances.L2DoorInstance;
import l2n.util.Location;

public class Instance
{
	private final String _name; // name of location
	private final Crontab _resetReuse;
	private final int _duration; // в мин
	private final boolean _remove_buff;
	private final boolean _need_channel;
	private final int _min_level;
	private final int _max_level;
	private final int _min_user;
	private final int _max_user;

	private final Location _teleportCoords;
	private final Location _returnCoords;
	private final GArray<L2DoorInstance> _roomDoors;
	private final GArray<SpawnInfo> _spawnsInfo;

	/**
	 * @param name
	 *            Имя инстанса
	 * @param minLevel
	 *            минимальный уровень игрока для входа
	 * @param maxLevel
	 *            мксимальный уровень игрока для входа
	 * @param minParty
	 *            минимальное количество пати для входа
	 * @param maxParty
	 *            мксимальное количество пати для входа
	 * @param teleLoc
	 *            локация для входа
	 * @param retLoc
	 *            локация для выхода
	 * @param resetReuse
	 *            время через которое инстанс будет доступен снова, resetReuse в формате Cron (http://ru.wikipedia.org/wiki/Cron)
	 * @param dispellBuffs
	 *            снимать ли бафы
	 * @param timelimit
	 *            лимит времени нахождения в инстансе, в мин
	 */
	public Instance(final StatsSet set, final Location teleLoc, final Location retLoc, final Crontab resetReuse)
	{
		_name = set.getString("name");
		_duration = set.getInteger("duration");
		_min_level = set.getInteger("min_level");
		_max_level = set.getInteger("max_level");
		_min_user = set.getInteger("min_user");
		_max_user = set.getInteger("max_user");
		_remove_buff = set.getBool("remove_buff");
		_need_channel = set.getBool("channel");

		_resetReuse = resetReuse;
		_teleportCoords = teleLoc;
		_returnCoords = retLoc;
		_spawnsInfo = new GArray<SpawnInfo>();
		_roomDoors = new GArray<L2DoorInstance>();

	}

	/** Имя инстанса */
	public String getName()
	{
		return _name;
	}

	public Crontab getResetReuse()
	{
		return _resetReuse;
	}

	public boolean isRemoveBuffs()
	{
		return _remove_buff;
	}

	/**
	 * @return the _need_channel
	 */
	public boolean isNeedChannel()
	{
		return _need_channel;
	}

	/**
	 * @return instance zone time limit in min.
	 */
	public int getDuration()
	{
		return _duration;
	}

	public int getMinLevel()
	{
		return _min_level;
	}

	public int getMaxLevel()
	{
		return _max_level;
	}

	public int getMinParty()
	{
		return _min_user;
	}

	public int getMaxParty()
	{
		return _max_user;
	}

	public Location getTeleportCoords()
	{
		return _teleportCoords;
	}

	public Location getReturnCoords()
	{
		return _returnCoords;
	}

	public GArray<L2DoorInstance> getDoors()
	{
		return _roomDoors;
	}

	public GArray<SpawnInfo> getSpawnsInfo()
	{
		return _spawnsInfo;
	}
}
