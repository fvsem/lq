package l2n.game.model.entity;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.InstanceManager.SpawnInfo;
import l2n.game.model.L2Spawn;
import l2n.game.model.Reflection;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 20.09.2010
 * @time 21:46:30
 */
public class KamalokaAbyssLabyrinth extends Reflection
{
	private final static Logger _log = Logger.getLogger(KamalokaAbyssLabyrinth.class.getName());

	/** Порядковый номер для выбора из массива */
	private final int id;

	private boolean firstRoomMonsterIsDead = false;
	private int secondRoomMonsterDeadCount = 0;
	private boolean thirdRoomMonsterIsDead = false;

	/**
	 * @param iz
	 */
	public KamalokaAbyssLabyrinth(final Instance iz, final int id)
	{
		super(iz);
		if(id > 69)
			this.id = id == 78 ? 5 : 6;
		else
			this.id = id == 29 ? 0 : (id - 29) / 10;
	}

	private final static int ESCAPE_DEVICE = 32496;

	/** Монстры 1-й комнаты */
	private final static int[][] LOST_WATCHER = {
			{ 22485, 22486 },
			{ 22488, 22489 },
			{ 22491, 22492 },
			{ 22494, 22495 },
			{ 22497, 22498 },
			{ 22500, 22501 },
			{ 22503, 22504 } };

	/** Монстры 2-й комнаты */
	private static final int[] LOST_BEHOLDER = ArrayUtil.getIntArrayRange(22487, 22505, 3);
	/** РБ 3-й комнаты */
	private static final int[] LOST_WARDEN = ArrayUtil.getIntArrayRange(25616, 25622, 1);
	/** РБ 4-й комнаты */
	private static final int[] LOST_CAPTAIN = ArrayUtil.getIntArrayRange(29129, 29147, 3);

	private static final Location ESCAPE_DEVICE_SPAWN = new Location(21848, -174888, -10904, 0);

	private static final Location[] LOST_WATCHER_SPAWN = {
			new Location(20504, -174888, -10912, 0),
			new Location(20632, -174888, -10912, 0),
			new Location(20360, -174888, -10912, 0),
			new Location(20488, -174760, -10912, 0),
			new Location(20488, -175016, -10912, 0),
			new Location(20424, -174968, -10912, 0),
			new Location(20584, -174968, -10912, 0),
			new Location(20568, -174808, -10912, 0),
			new Location(20408, -174808, -10912, 0), };

	private static final Location[] LOST_BEHOLDER_SPAWN = {
			new Location(18168, -174904, -10648, 0),
			new Location(18152, -175016, -10648, 0),
			new Location(18280, -174888, -10648, 0),
			new Location(18152, -174760, -10648, 0),
			new Location(18024, -174888, -10648, 0), };

	private static final Location LOST_WARDEN_SPAWN = new Location(15832, -174888, -10384, 0);
	private static final Location LOST_CAPTAIN_SPAWN = new Location(12056, -174888, -9944, 0);

	@Override
	public void fillSpawns(final GArray<SpawnInfo> si)
	{
		// сначало спауним альтарь для выхода из камалоки
		spawnEscapeDevice();

		// спауним монстров 1й команты
		spawnLostWatcherMonster();
		// спауним монстров 2й команты
		spawnLostBeholderMonster();
		// спауним РБ 3-ей комнаты
		spawnLostWardenRaidBoss();
		// спауним РБ 4-ой комнаты
		spawnLostCaptainRaidBoss();
	}

	/**
	 * Спауним моснтров 1й комнаты (Наблюдатель Лабиринта)
	 */
	private void spawnLostWatcherMonster()
	{
		// Первый элемент настоящий монстр, 2й - фальшивый (всего должно быть 9 штук)
		final int lostWatcher = LOST_WATCHER[id][0];
		final int lostWatcherFake = LOST_WATCHER[id][1];
		final int realId = Rnd.get(LOST_WATCHER_SPAWN.length); // запоминаем точку куда спауним настоящего

		for(int i = 0; i < 9; i++)
		{
			final L2NpcTemplate template = NpcTable.getTemplate(i == realId ? lostWatcher : lostWatcherFake); // первым спауним настоящего
			try
			{
				final L2Spawn spawnDat = new L2Spawn(template);
				spawnDat.setLocation(0);
				spawnDat.setLoc(LOST_WATCHER_SPAWN[i]);

				if(template.getNpcId() == lostWatcherFake)
					spawnDat.setRespawnDelay(30, 10); // фальшивые монстры после убийства появятся вновь
				else
					spawnDat.setRespawnDelay(0, 0);

				spawnDat.setAmount(1);
				spawnDat.setReflection(getId());
				spawnDat.doSpawn(true);
				spawnDat.startRespawn();
				if(spawnDat.getNativeRespawnDelay() == 0)
					spawnDat.stopRespawn();

				addSpawn(spawnDat);
			}
			catch(final Exception e)
			{
				_log.warning("KamalokaAbyssLabyrinth: spawnLostWatcherMonster error!");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Спауним моснтров 2й комнаты (Смотритель Лабиринта)
	 */
	private void spawnLostBeholderMonster()
	{
		final int lostBeholder = LOST_BEHOLDER[id];
		for(int i = 0; i < 5; i++)
		{
			final L2NpcTemplate template = NpcTable.getTemplate(lostBeholder);
			try
			{
				final L2Spawn spawnDat = new L2Spawn(template);
				spawnDat.setLocation(0);
				spawnDat.setLoc(LOST_BEHOLDER_SPAWN[i]);
				spawnDat.setRespawnDelay(0, 0);
				spawnDat.setAmount(1);
				spawnDat.setReflection(getId());
				spawnDat.doSpawn(true);
				spawnDat.stopRespawn();
				addSpawn(spawnDat);
			}
			catch(final Exception e)
			{
				_log.warning("KamalokaAbyssLabyrinth: spawnLostBeholderMonster error!");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Спауним РБ 3-ей комнаты
	 */
	private void spawnLostWardenRaidBoss()
	{
		final L2NpcTemplate template = NpcTable.getTemplate(LOST_WARDEN[id]);
		try
		{
			final L2Spawn spawnDat = new L2Spawn(template);
			spawnDat.setLocation(0);
			spawnDat.setLoc(LOST_WARDEN_SPAWN);
			spawnDat.setRespawnDelay(0, 0);
			spawnDat.setAmount(1);
			spawnDat.setReflection(getId());
			spawnDat.doSpawn(true);
			spawnDat.stopRespawn();
			addSpawn(spawnDat);
		}
		catch(final Exception e)
		{
			_log.warning("KamalokaAbyssLabyrinth: spawnLostWardenRaidBoss error!");
			e.printStackTrace();
		}
	}

	/**
	 * Спауним РБ 4-0й комнаты
	 */
	private void spawnLostCaptainRaidBoss()
	{
		final L2NpcTemplate template = NpcTable.getTemplate(LOST_CAPTAIN[id]);
		try
		{
			final L2Spawn spawnDat = new L2Spawn(template);
			spawnDat.setLocation(0);
			spawnDat.setLoc(LOST_CAPTAIN_SPAWN);
			spawnDat.setRespawnDelay(0, 0);
			spawnDat.setAmount(1);
			spawnDat.setReflection(getId());
			spawnDat.doSpawn(true);
			spawnDat.stopRespawn();
			addSpawn(spawnDat);
		}
		catch(final Exception e)
		{
			_log.warning("KamalokaAbyssLabyrinth: spawnLostCaptainRaidBoss error!");
			e.printStackTrace();
		}
	}

	/**
	 * Спауним альтарь для выхода из камалоки
	 */
	private void spawnEscapeDevice()
	{
		L2Spawn spawnDat = null;
		// сначало спауним альтарь для выхода из камалоки
		final L2NpcTemplate template = NpcTable.getTemplate(ESCAPE_DEVICE);
		try
		{
			spawnDat = new L2Spawn(template);
			spawnDat.setLocation(0);
			spawnDat.setLoc(ESCAPE_DEVICE_SPAWN);
			spawnDat.setRespawnDelay(0, 0);
			spawnDat.setAmount(1);
			spawnDat.setReflection(getId());
			spawnDat.doSpawn(true);
			spawnDat.stopRespawn();
		}
		catch(final Exception e)
		{
			_log.warning("KamalokaAbyssLabyrinth: spawnEscapeDevice error!");
			e.printStackTrace();
		}

		addSpawn(spawnDat);
	}

	/**
	 * @return true если настоящий Шаман из первой комнаты убит
	 */
	public boolean isFirstRoomMonsterIsDead()
	{
		return firstRoomMonsterIsDead;
	}

	/**
	 * @return true если все монстры из 2-ой комнаты убиты
	 */
	public boolean isSecondRoomMonsterIsDead()
	{
		return secondRoomMonsterDeadCount == 5;
	}

	/**
	 * @return true если РБ из 3-ей комнаты убит
	 */
	public boolean isThirdRoomMonsterIsDead()
	{
		return thirdRoomMonsterIsDead;
	}

	/**
	 * @param firstRoomMonsterIsDead
	 *            the firstRoomMonsterIsDead to set
	 */
	public void setFirstRoomMonsterIsDead(final boolean val)
	{
		firstRoomMonsterIsDead = val;
	}

	/**
	 * @param secondRoomMonsterIsDead
	 *            the secondRoomMonsterIsDead to set
	 */
	public void incSecondRoomMonsterDeadCount()
	{
		secondRoomMonsterDeadCount += 1;
	}

	/**
	 * @param thirdRoomMonsterIsDead
	 *            the thirdRoomMonsterIsDead to set
	 */
	public void setThirdRoomMonsterIsDead(final boolean val)
	{
		thirdRoomMonsterIsDead = val;
	}

	/** Если рефлекшн является камалокой - Лабиринт Бездны */
	@Override
	public boolean isKamalokaAbyssLabyrinth()
	{
		return true;
	}

	@Override
	public boolean canChampions()
	{
		return false;
	}
}
