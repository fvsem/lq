package l2n.game.model.entity.SevenSignsFestival;

import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Party;
import l2n.game.model.L2Spawn;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.instances.L2FestivalMonsterInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;

/**
 * Each running festival is represented by an L2DarknessFestival class.
 * It contains all the spawn information and data for the running festival.
 * All festivals are managed by the FestivalManager class, which must be initialized first.
 */
public class DarknessFestival extends Reflection
{
	private final class FestivalSpawnTask implements Runnable
	{
		private final boolean _scheduleNext;
		private final String _msg;
		private final int _spawnType;

		public FestivalSpawnTask(final boolean scheduleNext, final String msg, final int spawnType)
		{
			_scheduleNext = scheduleNext;
			_msg = msg;
			_spawnType = spawnType;
		}

		@Override
		public void run()
		{
			spawnFestivalMonsters(FestivalSpawn.FESTIVAL_DEFAULT_RESPAWN, _spawnType);
			sendMessageToParticipants(_msg);
			if(_scheduleNext)
				scheduleNext();
		}
	}

	public static final int FESTIVAL_LENGTH = 1080000; // 18 mins
	public static final int FESTIVAL_FIRST_SPAWN = 60000; // 1 min
	public static final int FESTIVAL_SECOND_SPAWN = 540000; // 9 mins
	public static final int FESTIVAL_CHEST_SPAWN = 900000; // 15 mins

	private FestivalSpawn _witchSpawn;
	private FestivalSpawn _startLocation;

	private int currentState = 0;
	private boolean _challengeIncreased = false;
	private final int _levelRange;
	private final int _cabal;

	private ScheduledFuture<FestivalSpawnTask> _spawnTimer;

	public DarknessFestival(final L2Party party, final int cabal, final int level)
	{
		super("Darkness Festival");
		setParty(party);
		_levelRange = level;
		_cabal = cabal;
		startCollapseTimer(FESTIVAL_LENGTH + FESTIVAL_FIRST_SPAWN);

		if(cabal == SevenSigns.CABAL_DAWN)
		{
			_witchSpawn = new FestivalSpawn(FestivalSpawn.FESTIVAL_DAWN_WITCH_SPAWNS[_levelRange]);
			_startLocation = new FestivalSpawn(FestivalSpawn.FESTIVAL_DAWN_PLAYER_SPAWNS[_levelRange]);
		}
		else
		{
			_witchSpawn = new FestivalSpawn(FestivalSpawn.FESTIVAL_DUSK_WITCH_SPAWNS[_levelRange]);
			_startLocation = new FestivalSpawn(FestivalSpawn.FESTIVAL_DUSK_PLAYER_SPAWNS[_levelRange]);
		}

		party.setReflection(this);
		setReturnLoc(party.getPartyLeader().getLoc());
		for(final L2Player p : party.getPartyMembers())
		{
			p.setVar("backCoords", p.getLoc().toXYZString());
			p.getEffectList().stopAllEffects();
			p.teleToLocation(Rnd.coordsRandomize(_startLocation.loc, 20, 100), getId());
		}

		scheduleNext();
		final L2NpcTemplate witchTemplate = NpcTable.getTemplate(_witchSpawn.npcId);
		// Spawn the festival witch for this arena
		try
		{
			final L2Spawn npcSpawn = new L2Spawn(witchTemplate);
			npcSpawn.setLoc(_witchSpawn.loc);
			npcSpawn.setReflection(_id);
			addSpawn(npcSpawn);
			npcSpawn.doSpawn(true);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "error create DarknessFestival", e);
		}
		sendMessageToParticipants("The festival will begin in 1 minute.");
	}

	private void scheduleNext()
	{
		switch (currentState)
		{
			case 0:
				currentState = FESTIVAL_FIRST_SPAWN;
				_spawnTimer = L2GameThreadPools.getInstance().scheduleGeneral(new FestivalSpawnTask(true, "Go!", 0), FESTIVAL_FIRST_SPAWN);
				break;
			case FESTIVAL_FIRST_SPAWN:
				currentState = FESTIVAL_SECOND_SPAWN;
				_spawnTimer = L2GameThreadPools.getInstance().scheduleGeneral(new FestivalSpawnTask(true, "Next wave arrived!", 2), FESTIVAL_SECOND_SPAWN - FESTIVAL_FIRST_SPAWN);
				break;
			case FESTIVAL_SECOND_SPAWN:
				currentState = FESTIVAL_CHEST_SPAWN;
				_spawnTimer = L2GameThreadPools.getInstance().scheduleGeneral(new FestivalSpawnTask(false, "The chests have spawned! Be quick, the festival will end soon.", 3), FESTIVAL_CHEST_SPAWN - FESTIVAL_SECOND_SPAWN);
				break;
			default:
				System.out.println("WTF???");
		}
	}

	public void spawnFestivalMonsters(final int respawnDelay, final int spawnType)
	{
		int[][] _npcSpawns = null;
		switch (spawnType)
		{
			case 0:
			case 1:
				_npcSpawns = _cabal == SevenSigns.CABAL_DAWN ? FestivalSpawn.FESTIVAL_DAWN_PRIMARY_SPAWNS[_levelRange] : FestivalSpawn.FESTIVAL_DUSK_PRIMARY_SPAWNS[_levelRange];
				break;
			case 2:
				_npcSpawns = _cabal == SevenSigns.CABAL_DAWN ? FestivalSpawn.FESTIVAL_DAWN_SECONDARY_SPAWNS[_levelRange] : FestivalSpawn.FESTIVAL_DUSK_SECONDARY_SPAWNS[_levelRange];
				break;
			case 3:
				_npcSpawns = _cabal == SevenSigns.CABAL_DAWN ? FestivalSpawn.FESTIVAL_DAWN_CHEST_SPAWNS[_levelRange] : FestivalSpawn.FESTIVAL_DUSK_CHEST_SPAWNS[_levelRange];
				break;
			default:
				return;
		}
		for(final int[] element : _npcSpawns)
		{
			final FestivalSpawn currSpawn = new FestivalSpawn(element);
			final L2NpcTemplate npcTemplate = NpcTable.getTemplate(currSpawn.npcId);
			try
			{
				final L2Spawn npcSpawn = new L2Spawn(npcTemplate);
				npcSpawn.setReflection(_id);
				npcSpawn.setLoc(currSpawn.loc);
				npcSpawn.setHeading(Rnd.get(65536));
				npcSpawn.setAmount(1);
				npcSpawn.setRespawnDelay(respawnDelay);
				npcSpawn.startRespawn();
				final L2FestivalMonsterInstance festivalMob = (L2FestivalMonsterInstance) npcSpawn.doSpawn(true);
				// Set the offering bonus to 2x or 5x the amount per kill, if this spawn is part of an increased challenge or is a festival chest.
				if(spawnType == 1)
					festivalMob.setOfferingBonus(2);
				else if(spawnType == 3)
					festivalMob.setOfferingBonus(5);
				addSpawn(npcSpawn);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "SevenSignsFestival: Error while spawning NPC ID " + currSpawn.npcId, e);
			}
		}
	}

	public boolean increaseChallenge()
	{
		if(_challengeIncreased)
			return false;
		// Set this flag to true to make sure that this can only be done once.
		_challengeIncreased = true;
		// Spawn more festival monsters, but this time with a twist.
		spawnFestivalMonsters(FestivalSpawn.FESTIVAL_DEFAULT_RESPAWN, 1);
		return true;
	}

	protected void stopAllTimers()
	{
		if(_spawnTimer != null)
		{
			_spawnTimer.cancel(false);
			_spawnTimer = null;
		}
	}

	@Override
	public void collapse()
	{
		stopAllTimers();
		if(getParty() != null)
		{
			final L2Player player = getParty().getPartyLeader();
			final L2ItemInstance bloodOfferings = player.getInventory().getItemByItemId(SevenSignsFestival.FESTIVAL_OFFERING_ID);

			// Check if the player collected any blood offerings during the festival.
			if(bloodOfferings != null)
			{
				final long offeringCount = bloodOfferings.getCount();
				final long offeringScore = offeringCount * SevenSignsFestival.FESTIVAL_OFFERING_VALUE;
				final boolean isHighestScore = SevenSignsFestival.getInstance().setFinalScore(getParty(), _cabal, _levelRange, offeringScore);

				player.getInventory().destroyItem(bloodOfferings, offeringCount, true);

				// Send message that the contribution score has increased.
				player.sendPacket(new SystemMessage(SystemMessage.YOUR_CONTRIBUTION_SCORE_IS_INCREASED_BY_S1).addNumber(offeringScore));

				sendCustomMessageToParticipants("l2n.game.model.entity.SevenSignsFestival.Ended");
				if(isHighestScore)
					sendMessageToParticipants("Your score is highest!");
			}
			else
				player.sendMessage(new CustomMessage("l2n.game.model.instances.L2FestivalGuideInstance.BloodOfferings", player));
		}
		super.collapse();
	}

	private void sendMessageToParticipants(final String s)
	{
		final GArray<L2Player> players = getPlayers();
		for(final L2Player p : players)
			if(p != null)
				p.sendMessage(s);
	}

	private void sendCustomMessageToParticipants(final String s)
	{
		final GArray<L2Player> players = getPlayers();
		for(final L2Player p : players)
			if(p != null)
				p.sendMessage(new CustomMessage(s, p));
	}

	public void partyMemberExited()
	{
		if(getParty() == null || getParty().getMemberCount() <= 1)
			collapse();
	}

	@Override
	public boolean canChampions()
	{
		return true;
	}

	@Override
	public boolean isAutoLootForced()
	{
		return true;
	}

	/** Если рефлекшн является фестивалем тьмы */
	@Override
	public boolean isDarknessFestival()
	{
		return true;
	}
}
