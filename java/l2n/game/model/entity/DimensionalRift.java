package l2n.game.model.entity;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.instancemanager.DimensionalRiftManager.DimensionalRiftRoom;
import l2n.game.model.*;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;

public class DimensionalRift extends Reflection
{
	private final class SpawnRiftTask implements Runnable
	{
		private final int _room;

		public SpawnRiftTask(final int room)
		{
			_room = room;
		}

		@Override
		public void run()
		{
			// получаем комнату рифта
			final DimensionalRiftRoom riftRoom = DimensionalRiftManager.getInstance().getRoom(_roomType, _room);
			// спауним монстров
			for(final L2Spawn s : riftRoom.getSpawns())
			{
				final L2Spawn sp = s.clone();
				sp.setReflection(_id);
				addSpawn(sp);
				if(!isBossRoom)
					sp.startRespawn();
				for(int i = 0; i < sp.getAmount(); i++)
					sp.doSpawn(true);
			}
			Quest.addSpawnToInstance(getManagerId(), riftRoom.getTeleportCoords(), false, _id);
		}
	}

	private final class TeleportRiftTask implements Runnable
	{
		@Override
		public void run()
		{
			// Если текущее количество прыжков меньше максимального и игроки в рифте ещё есть, то портуем в следующию комнату.
			if(jumps_current < getMaxJumps() && getPlayersInside(true) > 0)
			{
				jumps_current++; // увеличиваем счётчик прыжков

				teleportToNextRoom(); // телепортируем в след комнату
				createTeleporterTimer(); // запускаем таймер для следующего телепорта в другую команту
			}
			else
				createNewKillRiftTimer(); // создаём таймер для уничтожения рифта
		}
	}

	protected Integer _roomType;
	private GArray<Integer> _completedRooms = new GArray<Integer>();
	private int jumps_current = 0;

	private ScheduledFuture<SpawnRiftTask> _spawnTimer;
	private ScheduledFuture<TeleportRiftTask> _teleportTimer;

	private int _choosenRoom = -1;
	protected boolean _hasJumped = false;
	protected boolean isBossRoom = false;

	// Создаём рифт)
	public DimensionalRift(final L2Party party, final int type, final int room)
	{
		super("Dimensional Rift");
		_roomType = type;
		setParty(party);

		// назначаем пати рифт, если это не зал илюзий
		if(!isDelusionChamber())
		{
			startCollapseTimer(2 * 60 * MILLISECONDS_IN_1_MINUTE); // 120 (2 часа) минут таймер, для защиты от утечек памяти
			party.setDimensionalRift(this);
		}

		party.setReflection(this);
		_choosenRoom = room;
		checkBossRoom(_choosenRoom);

		final Location coords = getRoomCoord(_choosenRoom);

		setReturnLoc(party.getPartyLeader().getLoc());
		setTeleportLoc(coords);
		final GArray<L2Player> members = getParty().getPartyMembers();
		for(final L2Player p : members)
		{
			p.setVar("backCoords", getReturnLoc().toXYZString());
			DimensionalRiftManager.teleToLocation(p, coords.rnd(50, 100, false), this);
			p.setReflection(this);
		}

		createSpawnTimer(_choosenRoom);
		createTeleporterTimer();
	}

	public int getType()
	{
		return _roomType;
	}

	public int getCurrentRoom()
	{
		return _choosenRoom;
	}

	/**
	 * Создаёт таймер для проверки количества прыжков и наличия игроков в рифте, если что удаляем рифт
	 */
	protected void createTeleporterTimer()
	{
		if(_teleportTimer != null)
		{
			_teleportTimer.cancel(true);
			_teleportTimer = null;
		}
		if(isCollapseStarted())
			return;
		// Teleporter task, 8-10 minutes
		_teleportTimer = L2GameThreadPools.getInstance().scheduleGeneral(new TeleportRiftTask(), calcTimeToNextJump());
	}

	/**
	 * Создаёт таймер для спауна монстров в комнате
	 * 
	 * @param room
	 *            - комната в которой спаунить монстров
	 */
	public void createSpawnTimer(final int room)
	{
		if(_spawnTimer != null)
		{
			_spawnTimer.cancel(true);
			_spawnTimer = null;
		}

		if(isCollapseStarted())
			return;
		// запускаем таск на следующий спаун монстров
		_spawnTimer = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRiftTask(room), Config.RIFT_SPAWN_DELAY);
	}

	public void createNewKillRiftTimer()
	{
		if(isCollapseStarted())
			return;

		stopAllTasks();
		startCollapseTimer(100);
	}

	@Override
	protected void processTeleportList(final GArray<L2Player> teleport_list)
	{
		final GArray<L2Player> members = getParty().getPartyMembers();
		// удаляем одинаковые
		teleport_list.removeAll(members);

		// обрабатываем тех кого нету в пати, но есть в отражении
		for(final L2Player player : teleport_list)
		{
			if(player.getParty() != null)
			{
				if(equals(player.getParty().getReflection()))
					player.getParty().setReflection(null);

				if(player.getParty().getCommandChannel() != null && equals(player.getParty().getCommandChannel().getReflection()))
					player.getParty().getCommandChannel().setReflection(null);
			}

			if(equals(player.getReflection()))
				if(getReturnLoc() != null)
					player.teleToLocation(getReturnLoc(), 0);
				else
					player.setReflection(0);
		}

		for(final L2Player p : members)
			if(p != null && p.getReflectionId() == getId())
				DimensionalRiftManager.getInstance().teleportToWaitingRoom(p);
	}

	public void partyMemberInvited()
	{
		createNewKillRiftTimer();
	}

	public void partyMemberExited(final L2Player player)
	{
		if(getParty().getMemberCount() < Config.RIFT_MIN_PARTY_SIZE || getParty().getMemberCount() == 1 || getPlayersInside(true) == 0)
			createNewKillRiftTimer();
	}

	public void manualTeleport(final L2Player player, final L2NpcInstance npc)
	{
		if(!player.isInParty() || !player.getParty().isInReflection() || !(player.getParty().getReflection() instanceof DimensionalRift))
			return;

		if(player.getObjectId() != player.getParty().getPartyLeaderOID())
		{
			DimensionalRiftManager.getInstance().showHtmlFile(player, "data/html/rift/NotPartyLeader.htm", npc);
			return;
		}

		if(!isBossRoom)
		{
			if(_hasJumped)
			{
				DimensionalRiftManager.getInstance().showHtmlFile(player, "data/html/rift/AllreadyTeleported.html", npc);
				return;
			}
			_hasJumped = true;
		}
		else
		{
			manualExitRift(player, npc);
			return;
		}

		teleportToNextRoom();
	}

	public void manualExitRift(final L2Player player, final L2NpcInstance npc)
	{
		if(!player.isInParty() || !player.getParty().isInDimensionalRift())
			return;
		if(player.getObjectId() != player.getParty().getPartyLeaderOID())
		{
			DimensionalRiftManager.getInstance().showHtmlFile(player, "data/html/rift/NotPartyLeader.htm", npc);
			return;
		}

		createNewKillRiftTimer();
	}

	private void teleportToNextRoom()
	{
		_completedRooms.add(_choosenRoom);

		_spawns.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);

		int size = DimensionalRiftManager.getInstance().getRooms(_roomType).size();
		// комната босса может быть только последней
		if(jumps_current < getMaxJumps())
			size--; // убираем чтобы не попасть раньше времени к рб

		// В DC последние 2 печати всегда кончаются рейдом
		if(getType() >= 11 && jumps_current == getMaxJumps())
			_choosenRoom = 9;
		else
		{
			// выбираем комнату, где еще не были
			final GArray<Integer> notCompletedRooms = new GArray<Integer>();
			for(int i = 1; i <= size; i++)
				if(!_completedRooms.contains(i))
					notCompletedRooms.add(i);
			_choosenRoom = notCompletedRooms.get(Rnd.get(notCompletedRooms.size()));
		}

		checkBossRoom(_choosenRoom);
		setTeleportLoc(getRoomCoord(_choosenRoom));

		final GArray<L2Player> members = getParty().getPartyMembers();
		for(final L2Player p : members)
			if(p.getReflection() == this)
				DimensionalRiftManager.teleToLocation(p, getRoomCoord(_choosenRoom).rnd(50, 100, false), this);
		createSpawnTimer(_choosenRoom);
	}

	public void stopAllTasks()
	{
		if(_spawnTimer != null)
			_spawnTimer.cancel(true);
		if(_teleportTimer != null)
			_teleportTimer.cancel(true);

		_teleportTimer = null;
		_spawnTimer = null;
	}

	@Override
	public void collapse()
	{
		stopAllTasks();
		_completedRooms = null;

		final L2Party party = getParty();
		if(party != null)
			party.setDimensionalRift(null);

		super.collapse();
	}

	protected long calcTimeToNextJump()
	{
		if(isBossRoom)
			return (long) ((Config.RIFT_AUTO_JUMPS_TIME * MILLISECONDS_IN_1_MINUTE + Rnd.get(Config.RIFT_AUTO_JUMPS_TIME_RAND)) * Config.RIFT_BOSS_ROOM_TIME_MUTIPLY);
		return Config.RIFT_AUTO_JUMPS_TIME * MILLISECONDS_IN_1_MINUTE + Rnd.get(Config.RIFT_AUTO_JUMPS_TIME_RAND);
	}

	public void memberDead(final L2Player player)
	{
		if(getPlayersInside(true) == 0)
			createNewKillRiftTimer();
	}

	public void usedTeleport(final L2Player player)
	{
		if(getPlayersInside(false) < Config.RIFT_MIN_PARTY_SIZE)
			createNewKillRiftTimer();
	}

	public void checkBossRoom(final int room)
	{
		isBossRoom = DimensionalRiftManager.getInstance().getRoom(_roomType, room).isBossRoom();
	}

	public Location getRoomCoord(final int room)
	{
		return DimensionalRiftManager.getInstance().getRoom(_roomType, room).getTeleportCoords();
	}

	/** По умолчанию 4 */
	public int getMaxJumps()
	{
		return Math.max(Math.min(Config.RIFT_MAX_JUMPS, 8), 1);
	}

	@Override
	public boolean canChampions()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return "Dimensional Rift";
	}

	protected int getManagerId()
	{
		return 31865;
	}

	/** Если рефлекшн является рифтом */
	@Override
	public boolean isDimensionalRift()
	{
		return true;
	}

	protected int getPlayersInside(final boolean alive)
	{
		final GArray<L2Player> players = getPlayers();
		if(players.isEmpty())
			return 0;
		int sum = 0;
		for(final L2Player p : players)
			if(p != null && (!alive || !p.isDead()))
				sum++;
		return sum;
	}

	@Override
	public void removeObject(final L2Object o)
	{
		if(o.isPlayer() && _playerCount <= 1)
			createNewKillRiftTimer();
		super.removeObject(o);
	}
}
