package l2n.game.model.entity.olympiad;

import l2n.Config;

import java.util.Calendar;

/**
 * Выдаёт очки за неделю
 * 
 * @author L2System Project
 * @date 28.04.2013
 * @time 21:19:09
 */
public class WeeklyTask implements Runnable
{
	@Override
	public void run()
	{
		Olympiad.addWeeklyPoints();
		Olympiad._log.info("Olympiad System: Added weekly points to nobles");

		Calendar nextChange = Calendar.getInstance();
		Olympiad._nextWeeklyChange = nextChange.getTimeInMillis() + Config.OLY_WPERIOD;
	}
}
