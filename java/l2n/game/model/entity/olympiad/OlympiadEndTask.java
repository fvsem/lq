package l2n.game.model.entity.olympiad;

import l2n.Config;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.model.entity.Hero;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.Date;

public class OlympiadEndTask implements Runnable
{
	@Override
	public void run()
	{
		if(Olympiad._inCompPeriod)
		{
			L2GameThreadPools.getInstance().scheduleGeneral(new OlympiadEndTask(), 60000);
			return;
		}

		Announcements.announceToAll(new SystemMessage(SystemMessage.OLYMPIAD_PERIOD_S1_HAS_ENDED).addNumber(Olympiad._currentCycle));
		Announcements.announceToAll("Олимпиада: Проверка периода началась");

		Olympiad._isOlympiadEnd = true;
		if(Olympiad._scheduledWeeklyTask != null)
			Olympiad._scheduledWeeklyTask.cancel(true);

		Olympiad._validationEnd = Olympiad._olympiadEnd + Config.OLY_VPERIOD;

		OlympiadDatabase.saveNobleData();
		Olympiad._period = 1;
		Hero.getInstance().clearHeroes();
		try
		{
			OlympiadDatabase.save();
		}
		catch(Exception e)
		{
			Olympiad._log.warning("Olympiad System: Failed to save Olympiad configuration: ");
			e.printStackTrace();
		}

		Date dt = new Date(Olympiad.getMillisToValidationEnd());
		Olympiad._log.warning("Olympiad System: Starting Validation period. Time to end validation: " + Olympiad.dateFormat.format(dt));

		if(Olympiad._scheduledValdationTask != null)
			Olympiad._scheduledValdationTask.cancel(true);
		Olympiad._scheduledValdationTask = L2GameThreadPools.getInstance().scheduleGeneral(new ValidationTask(), Olympiad.getMillisToValidationEnd());
	}
}
