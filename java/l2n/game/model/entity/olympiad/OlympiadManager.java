package l2n.game.model.entity.olympiad;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.util.Rnd;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.logging.Level;

public class OlympiadManager implements Runnable
{
	private final FastMap<Integer, OlympiadGame> _olympiadInstances;

	public OlympiadManager()
	{
		_olympiadInstances = new FastMap<Integer, OlympiadGame>().shared();
	}

	public void wait2(final long time)
	{
		try
		{
			wait(time);
		}
		catch(final InterruptedException ex)
		{}
	}

	@Override
	public synchronized void run()
	{
		if(Olympiad.isOlympiadEnd())
			return;

		while (Olympiad.inCompPeriod())
		{
			if(Olympiad._nobles.isEmpty()) // Если нублесов нету - ждём минуту
			{
				wait2(60000);
				continue;
			}

			while (Olympiad.inCompPeriod())
			{
				// System.out.println("_nonClassBasedRegisters: size = " + Olympiad._nonClassBasedRegisters.size());
				// System.out.println("_classBasedRegisters: size = " + Olympiad._classBasedRegisters.size());
				// System.out.println("_teamRandomBasedRegisters: size = " + Olympiad._teamRandomBasedRegisters.size());
				// System.out.println("_teamBasedRegisters: size = " + Olympiad._teamBasedRegisters.size());

				// Внеклассовые бои
				if(Olympiad.REGISTERS_NON_CLASSED.size() >= Config.NONCLASS_GAME_MIN)
					prepareBattles(CompType.NON_CLASSED, Olympiad.REGISTERS_NON_CLASSED);

				// Классовые бои
				for(final Entry<Integer, GCSArray<Integer>> entry : Olympiad.REGISTERS_CLASSED.entrySet())
					if(entry.getValue().size() >= Config.CLASS_GAME_MIN)
						prepareBattles(CompType.CLASSED, entry.getValue());

				// Командные бои 3х3 случайного типа
				if(Olympiad.REGISTERS_TEAM_RANDOM.size() >= Config.RANDOM_TEAM_GAME_MIN)
					prepareBattles(CompType.TEAM_RANDOM, Olympiad.REGISTERS_TEAM_RANDOM);

				// Внеклассовые командные бои 3х3
				if(Olympiad.REGISTERS_TEAM.size() >= Config.TEAM_GAME_MIN)
					prepareTeamBattles(CompType.TEAM, Olympiad.REGISTERS_TEAM.values());

				wait2(30000);
			}

			wait2(30000);
		}

		Olympiad.REGISTERS_CLASSED.clear();
		Olympiad.REGISTERS_NON_CLASSED.clear();
		Olympiad.REGISTERS_TEAM_RANDOM.clear();
		Olympiad.REGISTERS_TEAM.clear();
		Olympiad.clearHWID();

		// when comp time finish wait for all games terminated before execute the cleanup code
		boolean allGamesTerminated = false;

		// wait for all games terminated
		while (!allGamesTerminated)
		{
			wait2(30000);

			if(_olympiadInstances.isEmpty())
				break;

			allGamesTerminated = true;
			for(final OlympiadGame game : _olympiadInstances.values())
				if(game.getTask() != null && !game.getTask().isTerminated())
					allGamesTerminated = false;
		}
		_olympiadInstances.clear();
	}

	/** Подготавливает игроков/команды к бою */
	private void prepareBattles(final CompType type, final GCSArray<Integer> list)
	{
		// System.out.println("OlympiadManager.prepareBattles(CompType, GCSArray<Integer>): " + type + ", size = " + list.size());
		for(int i = 0; i < Olympiad.STADIUMS.length; i++)
			try
			{
				// Берём свободный стадион
				if(Olympiad.STADIUMS[i].isFreeToUse())
				{
					// Если участниковм меньше минимального количество, то не стартуем
					if(list.size() < type.getMinSize())
						break;

					// Создаём игру
					final OlympiadGame game = new OlympiadGame(i, type, nextOpponents(list, type));
					// Запускаем
					game.sheduleTask(new OlympiadGameTask(game, BattleStatus.Begining, 0, 1));

					_olympiadInstances.put(i, game);

					Olympiad.STADIUMS[i].setStadiaBusy();
				}
			}
			catch(final Exception e)
			{
				Olympiad._log.log(Level.WARNING, "OlympiadManager: prepareBattles error", e);
			}
	}

	/** Подготавливает команды к бою */
	private void prepareTeamBattles(final CompType type, final Collection<GCSArray<Integer>> list)
	{
		GCSArray<Integer> nextOpponents;
		OlympiadGame game;
		for(int i = 0; i < Olympiad.STADIUMS.length; i++)
			try
			{
				if(Olympiad.STADIUMS[i].isFreeToUse())
				{
					if(list.size() < type.getMinSize())
						break;

					nextOpponents = nextTeamOpponents(list, type);
					if(nextOpponents == null)
						break;

					game = new OlympiadGame(i, type, nextOpponents);
					game.sheduleTask(new OlympiadGameTask(game, BattleStatus.Begining, 0, 1));

					_olympiadInstances.put(i, game);

					Olympiad.STADIUMS[i].setStadiaBusy();
				}
			}
			catch(final Exception e)
			{
				Olympiad._log.log(Level.WARNING, "OlympiadManager: prepareTeamBattles error", e);
			}
	}

	public void freeOlympiadInstance(final int index)
	{
		_olympiadInstances.remove(index);
		Olympiad.STADIUMS[index].setStadiaFree();
	}

	public OlympiadGame getOlympiadInstance(final int index)
	{
		return _olympiadInstances.get(index);
	}

	public FastMap<Integer, OlympiadGame> getOlympiadGames()
	{
		return _olympiadInstances;
	}

	private GCSArray<Integer> nextOpponents(final GCSArray<Integer> list, final CompType type)
	{
		final GCSArray<Integer> opponents = new GCSArray<Integer>();
		Integer noble;
		for(int i = 0; i < type.getMinSize(); i++)
		{
			noble = list.remove(Rnd.get(list.size()));
			opponents.add(noble);
			removeOpponent(noble);
		}

		return opponents;
	}

	private GCSArray<Integer> nextTeamOpponents(final Collection<GCSArray<Integer>> list, final CompType type)
	{
		final GCSArray<Integer> opponents = new GCSArray<Integer>();
		final GArray<GCSArray<Integer>> a = new GArray<GCSArray<Integer>>();
		a.addAll(list);

		GCSArray<Integer> team;
		for(int i = 0; i < type.getMinSize(); i++)
		{
			if(a.isEmpty())
			{
				Olympiad._log.log(Level.WARNING, "OlympiadManager[182] error!", new Exception("OlympiadManager[182]"));
				return null;
			}

			team = a.remove(Rnd.get(a.size()));
			if(team.size() == 3)
				for(final Integer noble : team)
				{
					opponents.add(noble);
					removeOpponent(noble);
				}
			else
			// Дисквалифицируем команды с количеством менее 3-х
			{
				for(final Integer noble : team)
					removeOpponent(noble);
				i--;
			}

			list.remove(team);
		}

		return opponents;
	}

	private void removeOpponent(final Integer noble)
	{
		Olympiad.REGISTERS_CLASSED.removeValue(noble);
		Olympiad.REGISTERS_NON_CLASSED.remove(noble);
		Olympiad.REGISTERS_TEAM_RANDOM.remove(noble);
		Olympiad.REGISTERS_TEAM.removeValue(noble);
	}
}
