package l2n.game.model.entity.olympiad;

import l2n.commons.list.GArray;

public class Stadia
{
	private boolean _freeToUse;
	private GArray<Integer> _doors;

	public Stadia()
	{
		_freeToUse = true;
		_doors = new GArray<Integer>();
	}

	public boolean isFreeToUse()
	{
		return _freeToUse;
	}

	public void setStadiaBusy()
	{
		_freeToUse = false;
	}

	public void setStadiaFree()
	{
		_freeToUse = true;
	}

	public void setDoor(int id)
	{
		_doors.add(Integer.valueOf(id));
	}

	public GArray<Integer> getDoors()
	{
		return _doors;
	}
}
