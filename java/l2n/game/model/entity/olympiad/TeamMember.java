package l2n.game.model.entity.olympiad;

import l2n.Config;
import l2n.commons.network.MMOConnection;
import l2n.commons.util.StatsSet;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2CubicInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.SkillTimeStamp;
import l2n.game.tables.HeroSkillTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Log;

import java.util.concurrent.ConcurrentSkipListSet;

@SuppressWarnings("rawtypes")
public class TeamMember
{
	private static final long SKILL_REUSE = 15 * 60000L;

	private OlympiadGame _game;
	private L2Player _player;
	private int _objId;
	private String _name = "";
	private CompType _type;
	private int _side;
	private Location _returnLoc;
	private long _returnRef;
	private boolean _isDead;

	public boolean isDead()
	{
		return _isDead;
	}

	public void doDie()
	{
		_isDead = true;
	}

	public TeamMember(final int obj_id, final String name, final OlympiadGame game, final int side)
	{
		_objId = obj_id;
		_name = name;
		_game = game;
		_type = game.getType();
		_side = side;

		final L2Player player = L2ObjectsStorage.getPlayer(obj_id);
		if(player == null)
			return;

		_player = player;
		try
		{
			if(player.inObserverMode())
				if(player.getOlympiadObserveId() > 0)
					player.leaveOlympiadObserverMode();
				else
					player.leaveObserverMode();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		player.setOlympiadSide(side);
		player.setOlympiadGameId(game.getId());
	}

	public StatsSet getStat()
	{
		return Olympiad._nobles.get(_objId);
	}

	public void takePointsForCrash()
	{
		if(checkPlayer())
			return;
		try
		{
			final StatsSet stat = getStat();
			final int points = stat.getInteger("olympiad_points");
			final int diff = Math.min(10, points / _type.getLooseMult());
			stat.set("olympiad_points", points - diff);
			Log.add("Olympiad Result: " + _name + " lost " + diff + " points for crash", "olympiad");

			// TODO: Снести подробный лог после исправления беспричинного отъёма очков.
			final L2Player player = _player;
			if(player == null)
				Log.add("Olympiad info: " + _name + " crashed coz player == null", "olympiad");
			else
			{
				if(player.isLogoutStarted())
					Log.add("Olympiad info: " + _name + " crashed coz player.isLogoutStarted()", "olympiad");
				if(!player.isOnline())
					Log.add("Olympiad info: " + _name + " crashed coz !player.isOnline()", "olympiad");
				if(player.getOlympiadGameId() == -1)
					Log.add("Olympiad info: " + _name + " crashed coz player.getOlympiadGameId() == -1", "olympiad");
				if(player.getOlympiadObserveId() > 0)
					Log.add("Olympiad info: " + _name + " crashed coz player.getOlympiadObserveId() > 0", "olympiad");
				final L2GameClient client = player.getNetConnection();
				if(client == null)
					Log.add("Olympiad info: " + _name + " crashed: client == null", "olympiad");
				else
				{
					final MMOConnection conn = client.getConnection();
					if(conn == null)
						Log.add("Olympiad info: " + _name + " crashed coz conn == null", "olympiad");
					else if(conn.isClosed())
						Log.add("Olympiad info: " + _name + " crashed coz conn.isClosed()", "olympiad");
				}
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public boolean checkPlayer()
	{
		final L2Player player = _player;
		if(player == null || player.isLogoutStarted() || !player.isOnline() || player.getOlympiadGameId() == -1 || player.getOlympiadObserveId() > 0)
			return false;

		final L2GameClient client = player.getNetConnection();
		if(client == null)
			return false;

		final MMOConnection conn = client.getConnection();
		if(conn == null || conn.isClosed())
			return false;
		return true;
	}

	public void portPlayerToArena()
	{
		final L2Player player = _player;
		if(!checkPlayer())
		{
			_player = null;
			return;
		}

		try
		{
			_returnLoc = player.getLoc();
			_returnRef = player.getReflection().getId();

			if(player.isDead())
				player.setIsPendingRevive(true);

			if(player.isSitting())
				player.standUp();

			player.setTarget(null);
			player.setIsInOlympiadMode(true);

			player.olyBuff = 5;

			if(player.getParty() != null)
			{
				final L2Party party = player.getParty();
				party.oustPartyMember(player);
			}

			final L2Zone zone = ZoneManager.getInstance().getZoneById(ZoneType.OlympiadStadia, 3001 + _game.getId(), false);
			final int[] tele = zone.getSpawns().get(_side - 1);

			player.teleToLocation(tele[0], tele[1], tele[2], 0);

			if(_type == CompType.TEAM_RANDOM || _type == CompType.TEAM)
				player.setTeam(_side, true);
			player.sendPacket(new ExOlympiadMode(_side));
			Olympiad.removeHWID(player);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public void portPlayerBack()
	{
		final L2Player player = _player;
		if(player == null)
			return;
		try
		{
			player.setIsInOlympiadMode(false);
			player.setOlympiadSide(-1);
			player.setOlympiadGameId(-1);

			if(_type == CompType.TEAM_RANDOM || _type == CompType.TEAM)
			{
				final L2Party party = player.getParty();
				if(party != null)
					party.oustPartyMember(player); // Удаляем патУ после боя на олУмпе
				player.setTeam(0, true);
			}

			player.stopAllEffects();

			player.setCurrentCp(player.getMaxCp());
			player.setCurrentMp(player.getMaxMp());

			if(player.isDead())
			{
				player.setCurrentHp(player.getMaxHp(), true);
				player.broadcastPacket(new Revive(player));
			}
			else
				player.setCurrentHp(player.getMaxHp(), true);

			// Add clan skill
			if(player.getClan() != null)
				for(final L2Skill skill : player.getClan().getAllSkills())
					if(skill.getMinPledgeClass() <= player.getPledgeClass())
						player.addSkill(skill, false);

			// Add Hero Skills
			if(player.isHero())
				for(final L2Skill sk : HeroSkillTable.getHeroSkills())
					player.addSkill(sk);

			// Обновляем скУлл лУст, после добавленУя скУлов
			player.sendPacket(new SkillList(player), ExOlympiadMode.RETURN, new ExOlympiadMatchEnd());
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			if(_returnLoc != null)
			{
				player.setReflection(_returnRef);
				player.teleToLocation(_returnLoc);
			}
			else
			{
				player.setReflection(0);
				player.teleToClosestTown();
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public void preparePlayer()
	{
		final L2Player player = _player;
		if(player == null)
			return;

		try
		{
			// Remove Buffs
			player.stopAllEffects();

			// Сброс кулдауна скилов с базовым реюзом 15 мин и менее
			boolean reseted = false;
			for(final SkillTimeStamp sts : player.getSkillReuses())
			{
				if(sts == null)
					continue;
				final L2Skill skill = SkillTable.getInstance().getInfo(sts.getId(), sts.getLevel());
				if(skill == null)
					continue;
				if(sts.getReuseBasic() <= SKILL_REUSE)
				{
					player.enableSkill(skill);
					reseted = true;
				}
			}

			if(reseted)
				player.sendPacket(new SkillCoolTime(player));

			// Remove Clan Skills
			if(player.getClan() != null)
				for(final L2Skill sk : player.getClan().getAllSkills())
					player.removeSkill(sk, false);

			// Force the character to be mortal
			if(player.isInvul())
				player.setInvul(false);

			// Force the character to be visible
			if(player.isInvisible())
				player.setInvisible(false);

			// Remove Hero Skills
			if(player.isHero())
				for(final L2Skill sk : HeroSkillTable.getHeroSkills())
					player.removeSkillById(sk.getId());

			if(player.isCastingNow())
				player.abortCast();

			if(player.getCubics() != null)
				for(final L2CubicInstance cubic : player.getCubics())
					if(cubic.isGivenByOther())
						cubic.deleteMe();

			// Remove Summon's Buffs
			if(player.getPet() != null)
			{
				final L2Summon summon = player.getPet();
				if(summon.isPet())
					summon.unSummon();
				else
					summon.stopAllEffects();
			}

			// unsummon agathion
			if(player.getAgathion() != null)
				player.setAgathion(0);

			player.sendPacket(new SkillList(player));

			// СнУмаем запрещённые вещУ
			if(!Config.OLY_LIST_RESTRICTED_ITEMS.isEmpty())
			{
				for(final L2ItemInstance equippedItem : player.getInventory().getPaperdollItems())
					if(equippedItem != null && equippedItem.isOlyRestrictedItem())
						player.getInventory().unEquipItem(equippedItem);

				player.refreshExpertisePenalty();
				player.broadcastUserInfo(true);
			}

			// Remove Hero weapons
			final L2ItemInstance wpn = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
			if(wpn != null && wpn.isHeroItem())
			{
				player.getInventory().unEquipItem(wpn);
				player.abortAttack();
				player.refreshExpertisePenalty();
				player.broadcastUserInfo(true);
			}

			// remove bsps/sps/ss automation
			final ConcurrentSkipListSet<Integer> activeSoulShots = player.getAutoSoulShot();
			for(final int itemId : activeSoulShots)
			{
				player.removeAutoSoulShot(itemId);
				player.sendPacket(new ExAutoSoulShot(itemId, false));
			}

			final L2ItemInstance item = player.getActiveWeaponInstance();
			if(item != null)
			{
				// Разряжаем заряженные соул У спУрУт шоты
				item.setChargedSpiritshot(L2ItemInstance.CHARGED_NONE);
				item.setChargedSoulshot(L2ItemInstance.CHARGED_NONE);
			}

			player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
			player.setCurrentCp(player.getMaxCp());

			player.broadcastUserInfo(true);
			player.broadcastCharInfo();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	public void preparePlayer1()
	{
		final L2Player player = _player;
		if(player == null)
			return;

		try
		{

			player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
			player.setCurrentCp(player.getMaxCp());

			player.broadcastUserInfo(true);
			player.broadcastCharInfo();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}

	
	public void saveNobleData()
	{
		OlympiadDatabase.saveNobleData(_objId);
	}

	public void logout()
	{
		_player = null;
	}

	public L2Player getPlayer()
	{
		return _player;
	}

	public int getObjId()
	{
		return _objId;
	}

	public String getName()
	{
		return _name;
	}
}
