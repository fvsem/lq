package l2n.game.model.entity.olympiad;

import l2n.game.Announcements;
import l2n.game.model.entity.Hero;

public class ValidationTask implements Runnable
{
	@Override
	public void run()
	{
		Olympiad._period = 1;

		OlympiadDatabase.sortHerosToBe();
		OlympiadDatabase.saveNobleData(); // Сохраняем героев-ноблесов, получивших бонус в виде очков

		if(Hero.getInstance().computeNewHeroes(Olympiad._heroesToBe))
			Olympiad._log.warning("Olympiad: Error while computing new heroes!");

		Announcements.announceToAll("Олимпиада Период закончился");

		Olympiad._period = 0;
		Olympiad._currentCycle++;

		OlympiadDatabase.cleanupNobles();
		OlympiadDatabase.loadNoblesRank();
		OlympiadDatabase.setNewOlympiadEnd();

		Olympiad.init();
		OlympiadDatabase.save();
	}
}
