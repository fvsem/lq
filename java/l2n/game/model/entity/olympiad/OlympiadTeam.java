package l2n.game.model.entity.olympiad;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExOlympiadUserInfo;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Log;

import static l2n.game.model.entity.olympiad.Olympiad.*;

public class OlympiadTeam
{
	private final OlympiadGame _game;
	private final GCSArray<TeamMember> _members;
	private String _name = "";
	private final int _side;
	private double _damage;

	public OlympiadTeam(final OlympiadGame game, final int side)
	{
		_game = game;
		_side = side;
		_members = new GCSArray<TeamMember>();
	}

	public void addMember(final int obj_id)
	{
		String player_name = "";
		final L2Player player = L2ObjectsStorage.getPlayer(obj_id);
		if(player != null)
			player_name = player.getName();
		else
		{
			final StatsSet noble = Olympiad._nobles.get(obj_id);
			if(noble != null)
				player_name = noble.getString(CHAR_NAME, "");
		}

		_members.add(new TeamMember(obj_id, player_name, _game, _side));

		switch (_game.getType())
		{
			case CLASSED:
			case NON_CLASSED:
				_name = player_name;
				break;
			case TEAM_RANDOM:
				_name = "Team " + _side;
				break;
			case TEAM:
				if(_name.isEmpty()) // Берется имя первого игрока в команде
					_name = player_name + " team";
				break;
		}
	}

	public void addDamage(final double damage)
	{
		_damage += damage;
	}

	public double getDamage()
	{
		return _damage;
	}

	public String getName()
	{
		return _name;
	}

	public void portPlayersToArena()
	{
		for(final TeamMember member : _members)
			member.portPlayerToArena();
	}

	public void portPlayersBack()
	{
		for(final TeamMember member : _members)
			member.portPlayerBack();
	}
	public void preparePlayers1()
	{
		for(final TeamMember member : _members)
			member.preparePlayer1();
		if(_members.size() <= 1)
			return;
		final GArray<L2Player> list = new GArray<L2Player>();
		for(final TeamMember member : _members)
		{
			final L2Player player = member.getPlayer();
			if(player != null)
			{
				list.add(player);
				if(player.getParty() != null)
				{
					final L2Party party = player.getParty();
					party.oustPartyMember(player);
				}
			}
		}

		if(list.size() <= 1)
			return;

		final L2Player leader = list.get(0);
		if(leader == null)
			return;

		final L2Party party = new L2Party(leader, 0);
		leader.setParty(party);

		for(final L2Player player : list)
			if(player != leader)
				player.joinParty(party);
	}

	public void preparePlayers()
	{
		for(final TeamMember member : _members)
			member.preparePlayer();
		if(_members.size() <= 1)
			return;
		final GArray<L2Player> list = new GArray<L2Player>();
		for(final TeamMember member : _members)
		{
			final L2Player player = member.getPlayer();
			if(player != null)
			{
				list.add(player);
				if(player.getParty() != null)
				{
					final L2Party party = player.getParty();
					party.oustPartyMember(player);
				}
			}
		}

		if(list.size() <= 1)
			return;

		final L2Player leader = list.get(0);
		if(leader == null)
			return;

		final L2Party party = new L2Party(leader, 0);
		leader.setParty(party);

		for(final L2Player player : list)
			if(player != leader)
				player.joinParty(party);
	}

	public void takePointsForCrash()
	{
		for(final TeamMember member : _members)
			member.takePointsForCrash();
	}

	public boolean checkPlayers()
	{
		for(final TeamMember member : _members)
			if(member.checkPlayer())
				return true;
		return false;
	}

	public boolean isAllDead()
	{
		for(final TeamMember member : _members)
			if(!member.isDead() && member.checkPlayer())
				return false;
		return true;
	}

	public boolean contains(final int objId)
	{
		for(final TeamMember member : _members)
			if(member.getObjId() == objId)
				return true;
		return false;
	}

	public GArray<L2Player> getPlayers()
	{
		final GArray<L2Player> players = new GArray<L2Player>(3);
		for(final TeamMember member : _members)
		{
			final L2Player player = member.getPlayer();
			if(player != null)
				players.add(player);
		}
		return players;
	}

	public GCSArray<TeamMember> getMembers()
	{
		return _members;
	}

	public void broadcast(final L2GameServerPacket p)
	{
		L2Player player;
		for(final TeamMember member : _members)
			if((player = member.getPlayer()) != null)
				player.sendPacket(p);
	}

	public void broadcastInfo()
	{
		L2Player player;
		for(final TeamMember member : _members)
			if((player = member.getPlayer()) != null)
				player.broadcastPacket(new ExOlympiadUserInfo(player, player.getOlympiadSide()));
	}

	public boolean logout(final L2Player player)
	{
		if(player != null)
		{
			L2Player pl;
			for(final TeamMember member : _members)
				if((pl = member.getPlayer()) != null && pl == player)
					member.logout();
		}
		return checkPlayers();
	}

	public boolean doDie(final L2Player player)
	{
		if(player != null)
			for(final TeamMember member : _members)
			{
				final L2Player pl = member.getPlayer();
				if(pl != null && pl == player)
					member.doDie();
			}
		return isAllDead();
	}

	public void winGame(final OlympiadTeam looseTeam)
	{
		int pointDiff = 0;
		for(int i = 0; i < _members.size(); i++)
			try
			{
				pointDiff += transferPoints(looseTeam.getMembers().get(i).getStat(), getMembers().get(i).getStat());
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

		for(final L2Player player : getPlayers())
			try
			{
				final L2ItemInstance item = player.getInventory().addItem(Config.OLY_BATTLE_REWARD_ITEM, _game.getType().getReward(), player.getObjectId(), "OlympiadTeam.winGame");
				player.sendPacket(SystemMessage.obtainItems(item.getItemId(), _game.getType().getReward(), 0));
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
		_game.broadcastPacket(new SystemMessage(SystemMessage.S1_HAS_WON_THE_GAME).addString(getName()), true, true);
		_game.broadcastPacket(new SystemMessage(SystemMessage.S1_HAS_GAINED_S2_OLYMPIAD_POINTS).addString(getName()).addNumber(pointDiff), true, false);
		_game.broadcastPacket(new SystemMessage(SystemMessage.S1_HAS_LOST_S2_OLYMPIAD_POINTS).addString(looseTeam.getName()).addNumber(pointDiff), true, false);

		Log.add("Olympiad Result: " + getName() + " vs " + looseTeam.getName() + " ... (" + (int) _damage + " vs " + (int) looseTeam.getDamage() + ") " + getName() + " win " + pointDiff + " points", "olympiad");
	}

	public void tie(final OlympiadTeam otherTeam)
	{
		if(Config.OLY_REDUCE_POINTS_ON_TIE)
			for(int i = 0; i < _members.size(); ++i)
				try
				{
					final StatsSet stat1 = getMembers().get(i).getStat();
					final StatsSet stat2 = otherTeam.getMembers().get(i).getStat();
					stat1.set(POINTS, stat1.getInteger(POINTS) - 2);
					stat2.set(POINTS, stat2.getInteger(POINTS) - 2);
				}
				catch(final Exception e)
				{
					e.printStackTrace();
				}
		_game.broadcastPacket(Msg.THE_GAME_ENDED_IN_A_TIE, true, true);

		Log.add("Olympiad Result: " + getName() + " vs " + otherTeam.getName() + " ... tie", "olympiad");
	}

	private int transferPoints(final StatsSet from, final StatsSet to)
	{
		final double fromPoints = from.getInteger(POINTS);
		final int fromLoose = from.getInteger(COMP_LOOSE);
		final int fromPlayed = from.getInteger(COMP_DONE);

		final double toPoints = to.getInteger(POINTS);
		final int toWin = to.getInteger(COMP_WIN);
		final int toPlayed = to.getInteger(COMP_DONE);

		double pointDiff = Math.min(fromPoints, toPoints) / _game.getType().getLooseMult();
		// округляем в большую сторону
		pointDiff = Math.ceil(pointDiff);

		pointDiff = Math.max(1, pointDiff);
		pointDiff = pointDiff > OlympiadGame.MAX_POINTS_LOOSE ? OlympiadGame.MAX_POINTS_LOOSE : pointDiff;

		from.set(POINTS, (int) (fromPoints - pointDiff));
		from.set(COMP_LOOSE, fromLoose + 1);
		from.set(COMP_DONE, fromPlayed + 1);

		to.set(POINTS, (int) (toPoints + pointDiff));
		to.set(COMP_WIN, toWin + 1);
		to.set(COMP_DONE, toPlayed + 1);

		return (int) pointDiff;
	}

	public void saveNobleData()
	{
		for(final TeamMember member : _members)
			member.saveNobleData();
	}
}
