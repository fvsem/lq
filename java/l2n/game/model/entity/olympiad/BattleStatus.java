package l2n.game.model.entity.olympiad;

public enum BattleStatus
{
	Begining,
	Begin_Countdown,
	PortPlayers,
	Started,
	CountDown,
	StartComp,
	ValidateWinner,
	Ending;
}
