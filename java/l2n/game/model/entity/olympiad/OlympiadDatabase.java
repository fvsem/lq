package l2n.game.model.entity.olympiad;

import gnu.trove.map.hash.TIntIntHashMap;
import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.network.serverpackets.SystemMessage;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.logging.Level;

import static l2n.game.model.entity.olympiad.Olympiad.*;

public class OlympiadDatabase
{
	public static synchronized void loadNobles()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(OLYMPIAD_LOAD_NOBLES);
			rset = statement.executeQuery();
			while (rset.next())
			{
				int classId = rset.getInt(CLASS_ID);
				if(classId < 88)
					for(ClassId id : ClassId.values())
					{
						if(id.level() != 3 || id.getParent(0).getId() != classId)
							continue;
						classId = id.getId();
						break;
					}
				StatsSet statDat = new StatsSet();
				int charId = rset.getInt(CHAR_ID);
				statDat.set(CLASS_ID, classId);
				statDat.set(CHAR_NAME, rset.getString(CHAR_NAME));
				statDat.set(POINTS, rset.getInt(POINTS));
				statDat.set(POINTS_PAST, rset.getInt(POINTS_PAST));
				statDat.set(POINTS_PAST_STATIC, rset.getInt(POINTS_PAST_STATIC));
				statDat.set(COMP_DONE, rset.getInt(COMP_DONE));
				statDat.set(COMP_WIN, rset.getInt(COMP_WIN));
				statDat.set(COMP_LOOSE, rset.getInt(COMP_LOOSE));

				Olympiad._nobles.put(charId, statDat);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static synchronized void loadNoblesRank()
	{
		Olympiad._noblesRank = new FastMap<Integer, Integer>().shared();
		TIntIntHashMap tmpPlace = new TIntIntHashMap();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(GET_ALL_CLASSIFIED_NOBLESS);
			rset = statement.executeQuery();
			int place = 1;
			while (rset.next())
				tmpPlace.put(rset.getInt(CHAR_ID), place++);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		int rank1 = (int) Math.round(tmpPlace.size() * 0.01);
		int rank2 = (int) Math.round(tmpPlace.size() * 0.10);
		int rank3 = (int) Math.round(tmpPlace.size() * 0.25);
		int rank4 = (int) Math.round(tmpPlace.size() * 0.50);

		if(rank1 == 0)
		{
			rank1 = 1;
			rank2++;
			rank3++;
			rank4++;
		}

		for(int charId : tmpPlace.keys())
			if(tmpPlace.get(charId) <= rank1)
				Olympiad._noblesRank.put(charId, 1);
			else if(tmpPlace.get(charId) <= rank2)
				Olympiad._noblesRank.put(charId, 2);
			else if(tmpPlace.get(charId) <= rank3)
				Olympiad._noblesRank.put(charId, 3);
			else if(tmpPlace.get(charId) <= rank4)
				Olympiad._noblesRank.put(charId, 4);
			else
				Olympiad._noblesRank.put(charId, 5);

		Olympiad._log.info("Olympiad System: " + Olympiad._noblesRank.size() + " load noblesse for Ranking.");
	}

	public static synchronized void cleanupNobles()
	{
		Olympiad._log.info("Olympiad System: Calculating last period...");
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(OLYMPIAD_CALCULATE_LAST_PERIOD);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement(OLYMPIAD_CLEANUP_NOBLES);
			statement.execute();
		}
		catch(Exception e)
		{
			Olympiad._log.log(Level.WARNING, "Olympiad System: Couldn't calculate last period!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		for(int nobleId : Olympiad._nobles.keySet())
		{
			StatsSet nobleInfo = Olympiad._nobles.get(nobleId);
			int points = nobleInfo.getInteger(POINTS);
			int compDone = nobleInfo.getInteger(COMP_DONE);
			nobleInfo.set(POINTS, DEFAULT_POINTS);
			if(compDone >= 9)
			{
				nobleInfo.set(POINTS_PAST, points);
				nobleInfo.set(POINTS_PAST_STATIC, points);
			}
			else
			{
				nobleInfo.set(POINTS_PAST, 0);
				nobleInfo.set(POINTS_PAST_STATIC, 0);
			}
			nobleInfo.set(COMP_DONE, 0);
			nobleInfo.set(COMP_WIN, 0);
			nobleInfo.set(COMP_LOOSE, 0);
		}
	}

	public static GArray<String> getClassLeaderBoard(int classId)
	{
		GArray<String> names = new GArray<String>();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(classId == 132 ? GET_EACH_CLASS_LEADER_SOULHOUND : GET_EACH_CLASS_LEADER);
			statement.setInt(1, classId);
			rset = statement.executeQuery();
			while (rset.next())
				names.add(rset.getString(CHAR_NAME));
		}
		catch(Exception e)
		{
			Olympiad._log.log(Level.WARNING, "Olympiad System: Couldnt get heros from db: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return names;
	}

	public static synchronized void sortHerosToBe()
	{
		if(Olympiad._period != 1)
			return;

		Olympiad._heroesToBe = new GArray<StatsSet>();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			for(ClassId id : ClassId.values())
			{
				if(id.getId() == 133 || id.level() != 3)
					continue;

				statement = con.prepareStatement(id.getId() == 132 ? OLYMPIAD_GET_HEROS_SOULHOUND : OLYMPIAD_GET_HEROS);
				statement.setInt(1, id.getId());
				rset = statement.executeQuery();

				if(rset.next())
				{
					StatsSet hero = new StatsSet();
					hero.set(CLASS_ID, id.getId());
					hero.set(CHAR_ID, rset.getInt(CHAR_ID));
					hero.set(CHAR_NAME, rset.getString(CHAR_NAME));

					Olympiad._heroesToBe.add(hero);
				}
				DbUtils.close(statement, rset);
			}

		}
		catch(Exception e)
		{
			Olympiad._log.log(Level.WARNING, "Olympiad System: Couldnt heros from db: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static synchronized void saveNobleData(int nobleId)
	{
		L2Player player = L2ObjectsStorage.getPlayer(nobleId);

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			StatsSet nobleInfo = Olympiad._nobles.get(nobleId);

			int classId = nobleInfo.getInteger(CLASS_ID);
			String charName = player != null ? player.getName() : nobleInfo.getString(CHAR_NAME);
			int points = nobleInfo.getInteger(POINTS);
			int points_past = nobleInfo.getInteger(POINTS_PAST);
			int points_past_static = nobleInfo.getInteger(POINTS_PAST_STATIC);
			int compDone = nobleInfo.getInteger(COMP_DONE);
			int compWin = nobleInfo.getInteger(COMP_WIN);
			int compLoose = nobleInfo.getInteger(COMP_LOOSE);

			statement = con.prepareStatement(Olympiad.OLYMPIAD_SAVE_NOBLES);
			statement.setInt(1, nobleId);
			statement.setInt(2, classId);
			statement.setString(3, charName);
			statement.setInt(4, points);
			statement.setInt(5, points_past);
			statement.setInt(6, points_past_static);
			statement.setInt(7, compDone);
			statement.setInt(8, compWin);
			statement.setInt(9, compLoose);
			statement.execute();
		}
		catch(Exception e)
		{
			Olympiad._log.log(Level.WARNING, "Olympiad System: Couldnt save noble info in db for player " + player != null ? player.getName() : "null", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void removeNobleData(int nobleId)
	{
		L2Player player = L2ObjectsStorage.getPlayer(nobleId);

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement(Olympiad.OLYMPIAD_REMOVE_NOBLES);
			statement.setInt(1, nobleId);
			statement.executeUpdate();
		}
		catch(Exception e)
		{
			Olympiad._log.log(Level.WARNING, "Olympiad System: Couldnt remove noble from db for player " + player != null ? player.getName() : "null", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static synchronized void saveNobleData()
	{
		if(Olympiad._nobles == null)
			return;

		for(int nobleId : Olympiad._nobles.keySet())
			saveNobleData(nobleId);
	}

	public static synchronized void setNewOlympiadEnd()
	{
		Announcements.announceToAll(new SystemMessage(SystemMessage.OLYMPIAD_PERIOD_S1_HAS_STARTED).addNumber(Olympiad._currentCycle));

		Calendar currentTime = Calendar.getInstance();
		
		if(Config.OLYMP_PERIOD)
		{
		if(currentTime.get(Calendar.DAY_OF_MONTH) < Config.OLYMP_DAYS)
		     currentTime.set(Calendar.DAY_OF_MONTH, Config.OLYMP_DAYS);
		else
		{
		     currentTime.set(Calendar.DAY_OF_MONTH, 1);	
		     currentTime.set(Calendar.MONTH, currentTime.get(Calendar.MONTH)+1);
        }			 
		     currentTime.set(Calendar.HOUR_OF_DAY, 00);
		     currentTime.set(Calendar.MINUTE, 00);		
		     Olympiad._olympiadEnd = currentTime.getTimeInMillis();
        }
        else		
        {
		currentTime.set(Calendar.DAY_OF_MONTH, 1);
		currentTime.add(Calendar.MONTH, 1);
		currentTime.set(Calendar.HOUR_OF_DAY, 00);
		currentTime.set(Calendar.MINUTE, 00);
		currentTime.set(Calendar.SECOND, 00);
		Olympiad._olympiadEnd = currentTime.getTimeInMillis();
		}

		Calendar nextChange = Calendar.getInstance();
		Olympiad._nextWeeklyChange = nextChange.getTimeInMillis() + Config.OLY_WPERIOD;

		Olympiad._isOlympiadEnd = false;
	}

	public static void save()
	{
		saveNobleData();
		ServerVariables.set("Olympiad_CurrentCycle", Olympiad._currentCycle);
		ServerVariables.set("Olympiad_Period", Olympiad._period);
		ServerVariables.set("Olympiad_End", Olympiad._olympiadEnd);
		ServerVariables.set("Olympiad_ValdationEnd", Olympiad._validationEnd);
		ServerVariables.set("Olympiad_NextWeeklyChange", Olympiad._nextWeeklyChange);
	}
}
