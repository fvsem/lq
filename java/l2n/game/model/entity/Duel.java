package l2n.game.model.entity;

import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.serverpackets.*;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Duel
{
	private final static Logger _log = Logger.getLogger(Duel.class.getName());

	public static enum DuelResultEnum
	{
		Continue,
		Team1Win,
		Team2Win,
		Team1Surrender,
		Team2Surrender,
		Canceled,
		Timeout
	}

	public static enum DuelState
	{
		Winner,
		Looser,
		Fighting,
		Dead,
		Interrupted
	}

	// =========================================================
	// Data Field
	private final boolean _isPartyDuel;
	private final Calendar _DuelEndTime;
	private int _surrenderRequest = 0;
	private int _countdown = 4;
	private boolean _finished = false;

	private final GArray<Long> _team1 = new GArray<Long>(), _team2 = new GArray<Long>();
	private FastMap<Long, PlayerCondition> _playerConditions = new FastMap<Long, PlayerCondition>().shared();

	// =========================================================
	// Constructor
	public Duel(final L2Player playerA, final L2Player playerB, final boolean partyDuel)
	{
		_isPartyDuel = partyDuel;

		_team1.add(playerA.getStoredId());
		_team2.add(playerB.getStoredId());

		_DuelEndTime = Calendar.getInstance();
		if(_isPartyDuel)
			_DuelEndTime.add(Calendar.SECOND, 300);
		else
			_DuelEndTime.add(Calendar.SECOND, 120);

		if(_isPartyDuel)
		{
			// Добавить игроков в списки дуэлянтов
			for(final L2Player p : playerA.getParty().getPartyMembers())
				if(p != playerA)
					_team1.add(p.getStoredId());

			for(final L2Player p : playerB.getParty().getPartyMembers())
				if(p != playerB)
					_team1.add(p.getStoredId());

			// increase countdown so that start task can teleport players
			_countdown++;
			// inform players that they will be portet shortly
			broadcastToTeam(Msg.IN_A_MOMENT_YOU_WILL_BE_TRANSPORTED_TO_THE_SITE_WHERE_THE_DUEL_WILL_TAKE_PLACE, _team1);
			broadcastToTeam(Msg.IN_A_MOMENT_YOU_WILL_BE_TRANSPORTED_TO_THE_SITE_WHERE_THE_DUEL_WILL_TAKE_PLACE, _team2);
		}

		// Save player Conditions
		savePlayerConditions();

		// Schedule duel start
		L2GameThreadPools.getInstance().scheduleAi(new ScheduleStartDuelTask(this), 3000, true);
	}

	// ===============================================================
	// Nested Class

	public static class PlayerCondition
	{
		private long _playerStoreId = 0;
		private double _hp, _mp, _cp;
		private boolean _paDuel;
		private int _x, _y, _z;
		private DuelState _duelState;
		private GArray<L2Effect> _debuffs;

		public PlayerCondition(final L2Player player, final boolean partyDuel)
		{
			if(player == null)
				return;
			_playerStoreId = player.getStoredId();
			_hp = player.getCurrentHp();
			_mp = player.getCurrentMp();
			_cp = player.getCurrentCp();
			_paDuel = partyDuel;

			if(_paDuel)
			{
				_x = player.getX();
				_y = player.getY();
				_z = player.getZ();
			}
		}

		public void registerDebuff(final L2Effect debuff)
		{
			if(_debuffs == null)
				_debuffs = new GArray<L2Effect>();

			_debuffs.add(debuff);
		}

		public void RestoreCondition(final boolean abnormalEnd)
		{
			final L2Player player = getPlayer();
			if(player == null)
				return;

			if(_debuffs != null)
				for(final L2Effect e : _debuffs)
					if(e != null)
						e.exit();

			// for(L2Effect e : _player.getAllEffects())
			// if(e.getSkill().isOffensive())
			// e.exit();

			// if it is an abnormal DuelEnd do not restore hp, mp, cp
			if(!abnormalEnd && !player.isDead())
			{
				player.setCurrentHp(_hp, false);
				player.setCurrentMp(_mp);
				player.setCurrentCp(_cp);
			}

			if(_paDuel)
				TeleportBack();
		}

		public void TeleportBack()
		{
			final L2Player player = getPlayer();
			if(player != null && _paDuel)
				player.teleToLocation(_x, _y, _z);
		}

		public L2Player getPlayer()
		{
			return L2ObjectsStorage.getAsPlayer(_playerStoreId);
		}

		public void setDuelState(final DuelState d)
		{
			_duelState = d;
		}

		public DuelState getDuelState()
		{
			return _duelState;
		}
	}

	// ===============================================================
	// Schedule task
	public class ScheduleDuelTask implements Runnable
	{
		private final Duel _duel;

		public ScheduleDuelTask(final Duel duel)
		{
			_duel = duel;
		}

		@Override
		public void run()
		{
			try
			{
				final DuelResultEnum status = _duel.checkEndDuelCondition();
				if(status == DuelResultEnum.Canceled)
				{
					// do not schedule duel end if it was interrupted
					setFinished(true); // На всякий пожарный, если верхнее не выполнилось.

					// Колечка должны сниматся сразу
					L2Player p;
					for(final Long storedId : _team1)
						if((p = L2ObjectsStorage.getAsPlayer(storedId)) != null)
							p.setTeam(0, false);
					for(final Long storedId : _team2)
						if((p = L2ObjectsStorage.getAsPlayer(storedId)) != null)
							p.setTeam(0, false);

					L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleEndDuelTask(_duel, status), 5000);
					stopFighting();
				}
				else if(status != DuelResultEnum.Continue)
				{
					setFinished(true);
					playKneelAnimation();

					// Колечка должны сниматся сразу
					L2Player p;
					for(final Long storedId : _team1)
						if((p = L2ObjectsStorage.getAsPlayer(storedId)) != null)
							p.setTeam(0, false);
					for(final Long storedId : _team2)
						if((p = L2ObjectsStorage.getAsPlayer(storedId)) != null)
							p.setTeam(0, false);

					L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleEndDuelTask(_duel, status), 5000);
				}
				else
					L2GameThreadPools.getInstance().scheduleAi(this, 1000, true);
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "Can't continue duel" + t, t);
			}
		}
	}

	public static class ScheduleStartDuelTask implements Runnable
	{
		private final Duel _duel;

		public ScheduleStartDuelTask(final Duel duel)
		{
			_duel = duel;
		}

		@Override
		public void run()
		{
			try
			{
				// start/continue countdown
				final int count = _duel.Countdown();

				if(count == 4)
				{
					// players need to be teleportet first
					// TODO: stadia manager needs a function to return an unused stadium for duels
					// currently only teleports to the same stadium
					// _duel.teleportPlayers(-102495, -209023, -3326); Что за координаты?
					_duel.teleportPlayers(149485, 46718, -3413);

					// give players 20 seconds to complete teleport and get ready (its ought to be 30 on offical..)
					L2GameThreadPools.getInstance().scheduleAi(this, 20000, true);
				}
				else if(count > 0)
					L2GameThreadPools.getInstance().scheduleAi(this, 1000, true);
				else
					_duel.startDuel();
			}
			catch(final Throwable t)
			{}
		}
	}

	public static class ScheduleEndDuelTask implements Runnable
	{
		private final Duel _duel;
		private final DuelResultEnum _result;

		public ScheduleEndDuelTask(final Duel duel, final DuelResultEnum result)
		{
			_duel = duel;
			_result = result;
		}

		@Override
		public void run()
		{
			try
			{
				_duel.endDuel(_result);
			}
			catch(final Throwable t)
			{
				_log.log(Level.WARNING, "Duel: Can't end duel" + t, t);
			}
		}
	}

	// ========================================================
	// Method - Private

	/**
	 * Stops all players from attacking.
	 * Used for duel timeout.
	 */
	private void stopFighting()
	{
		L2Player temp;
		for(final Long storedId : _team1)
			if((temp = L2ObjectsStorage.getAsPlayer(storedId)) != null)
			{
				temp.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				if(temp.getPet() != null)
					temp.getPet().getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				temp.sendActionFailed();
			}

		for(final Long storedId : _team2)
			if((temp = L2ObjectsStorage.getAsPlayer(storedId)) != null)
			{
				temp.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				if(temp.getPet() != null)
					temp.getPet().getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				temp.sendActionFailed();
			}
	}

	/**
	 * Прекращает атаку определенного игрока
	 * 
	 * @param player
	 *            you wish to stop the attack
	 */
	public void stopFighting(final L2Playable player)
	{
		player.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		player.sendActionFailed();
	}

	// ========================================================
	// Method - Public

	/**
	 * Check if a player engaged in pvp combat (only for 1on1 duels)
	 * 
	 * @param sendMessage
	 *            if we need to send message
	 * @return returns true if a duelist is engaged in Pvp combat
	 */
	public boolean isDuelistInPvp(final boolean sendMessage)
	{
		if(_isPartyDuel)
			return false;

		final L2Player d1 = getPlayerA();
		final L2Player d2 = getPlayerB();
		if(d1 == null || d2 == null || d1.getPvpFlag() != 0 || d2.getPvpFlag() != 0)
		{
			if(sendMessage && d1 != null)
				d1.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
			if(sendMessage && d2 != null)
				d2.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
			return true;
		}

		return false;
	}

	/**
	 * Starts the duel
	 */
	public void startDuel()
	{
		final GArray<L2Player> t1 = getPlayers(_team1);
		final GArray<L2Player> t2 = getPlayers(_team2);

		// Начало проверки на наличие дуэли
		String name = null;
		for(final L2Player temp : t1)
			if(temp.getDuel() != null)
			{
				name = temp.getName();
				break;
			}

		if(name == null)
			for(final L2Player temp : t2)
				if(temp.getDuel() != null)
				{
					name = temp.getName();
					break;
				}

		if(name != null)
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_PARTICIPATING_IN_THE_OLYMPIAD);
			sm.addString(name);

			for(final L2Player temp : t1)
				temp.sendPacket(sm);
			for(final L2Player temp : t2)
				temp.sendPacket(sm);
			return;
		}
		// Конец проверки на наличие дуэли

		// set isInDuel() state
		for(final L2Player temp : t1)
		{
			temp.setDuel(this);
			getPlayerCondition(temp).setDuelState(DuelState.Fighting);
			temp.setTeam(1, false);
			temp.broadcastStatusUpdate();
			broadcastToOppositTeam(temp, new ExDuelUpdateUserInfo(temp));
		}
		for(final L2Player temp : t2)
		{
			temp.setDuel(this);
			temp.setTeam(2, false);
			getPlayerCondition(temp).setDuelState(DuelState.Fighting);
			temp.broadcastStatusUpdate();
			broadcastToOppositTeam(temp, new ExDuelUpdateUserInfo(temp));
		}

		// Send duel Start packets
		// TODO: verify: is this done correctly?
		final ExDuelReady ready = new ExDuelReady(_isPartyDuel ? 1 : 0);
		final ExDuelStart start = new ExDuelStart(_isPartyDuel ? 1 : 0);

		broadcastToTeam(ready, _team1);
		broadcastToTeam(ready, _team2);
		broadcastToTeam(start, _team1);
		broadcastToTeam(start, _team2);

		// play duel music
		final PlaySound ps = new PlaySound("B04_S01");
		broadcastToTeam(ps, _team1);
		broadcastToTeam(ps, _team2);

		// start duelling task
		L2GameThreadPools.getInstance().scheduleAi(new ScheduleDuelTask(this), 1000, true);
	}

	/**
	 * Save the current player condition: hp, mp, cp, location
	 */
	public void savePlayerConditions()
	{
		L2Player player;
		for(final Long storedId : _team1)
			if((player = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				_playerConditions.put(storedId, new PlayerCondition(player, _isPartyDuel));
		for(final Long storedId : _team2)
			if((player = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				_playerConditions.put(storedId, new PlayerCondition(player, _isPartyDuel));
	}

	/**
	 * Restore player conditions
	 * 
	 * @param abnormalDuelEnd
	 *            was the duel canceled?
	 */
	public void restorePlayerConditions(final boolean abnormalDuelEnd)
	{
		// update isInDuel() state for all players
		for(final L2Player temp : getPlayers(_team1))
		{
			temp.setDuel(null);
			temp.setTeam(0, false);
		}
		for(final L2Player temp : getPlayers(_team2))
		{
			temp.setDuel(null);
			temp.setTeam(0, false);
		}

		// restore player conditions
		for(final PlayerCondition pc : _playerConditions.values())
			pc.RestoreCondition(abnormalDuelEnd);
	}

	/**
	 * Returns the remaining time
	 * 
	 * @return remaining time
	 */
	public int getRemainingTime()
	{
		return (int) (_DuelEndTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis());
	}

	/**
	 * Get the player that requestet the duel
	 * 
	 * @return duel requester
	 */
	public L2Player getPlayerA()
	{
		return L2ObjectsStorage.getAsPlayer(_team1.get(0));
	}

	/**
	 * Get the player that was challenged
	 * 
	 * @return challenged player
	 */
	public L2Player getPlayerB()
	{
		return L2ObjectsStorage.getAsPlayer(_team2.get(0));
	}

	/**
	 * Returns whether this is a party duel or not
	 * 
	 * @return is party duel
	 */
	public boolean isPartyDuel()
	{
		return _isPartyDuel;
	}

	public void setFinished(final boolean mode)
	{
		_finished = mode;
	}

	public boolean isFinished()
	{
		return _finished;
	}

	/**
	 * teleport all players to the given coordinates
	 * 
	 * @param x
	 *            coord
	 * @param y
	 *            coord
	 * @param z
	 *            coord
	 */
	public void teleportPlayers(final int x, final int y, final int z)
	{
		// TODO: отрегулировать параметры в случае необходимости ... или реализовать что-то лучше (особенно при использовании более чем 1 арена)
		if(!_isPartyDuel)
			return;
		int offset = 0;

		for(final L2Player temp : getPlayers(_team1))
		{
			temp.teleToLocation(x + offset - 180, y - 150, z);
			offset += 40;
		}
		offset = 0;
		for(final L2Player temp : getPlayers(_team2))
		{
			temp.teleToLocation(x + offset - 180, y + 150, z);
			offset += 40;
		}
	}

	/**
	 * Broadcast a packet to the challanger team
	 * 
	 * @param packet
	 *            what you wish to send
	 */
	public void broadcastToTeam(final L2GameServerPacket packet, final GArray<Long> team)
	{
		L2Player temp;
		for(final Long storedId : team)
			if((temp = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				temp.sendPacket(packet);
	}

	/**
	 * Get the duel winner
	 * 
	 * @return winner
	 */
	public L2Player getWinner()
	{
		if(!isFinished() || _team1.size() == 0 || _team2.size() == 0)
			return null;
		if(_playerConditions.get(_team1.get(0)).getDuelState() == DuelState.Winner)
			return getPlayerA();
		if(_playerConditions.get(_team2.get(0)).getDuelState() == DuelState.Winner)
			return getPlayerB();
		return null;
	}

	/**
	 * Get the duel looser
	 * 
	 * @return looser
	 */
	public GArray<Long> getLoosers()
	{
		if(!isFinished() || getPlayerA() == null || getPlayerB() == null)
			return null;
		if(_playerConditions.get(_team1.get(0)).getDuelState() == DuelState.Winner)
			return _team2;
		if(_playerConditions.get(_team2.get(0)).getDuelState() == DuelState.Winner)
			return _team1;
		return null;
	}

	/**
	 * Playback the bow animation for all loosers
	 */
	public void playKneelAnimation()
	{
		final GArray<Long> loosers = getLoosers();
		if(loosers == null || loosers.size() == 0)
			return;

		if(loosers == null || loosers.size() == 0)
			return;

		L2Player looser;
		for(final Long looserId : loosers)
			if((looser = L2ObjectsStorage.getAsPlayer(looserId)) != null)
				looser.broadcastPacket(new SocialAction(looser.getObjectId(), SocialAction.BOW));
	}

	/**
	 * Do the countdown and send message to players if necessary
	 * 
	 * @return current count
	 */
	public int Countdown()
	{
		_countdown--;

		if(_countdown > 3)
			return _countdown;

		// Broadcast countdown to duelists
		SystemMessage sm;
		if(_countdown > 0)
		{
			sm = new SystemMessage(SystemMessage.THE_DUEL_WILL_BEGIN_IN_S1_SECONDS);
			sm.addNumber(_countdown);
		}
		else
			sm = Msg.LET_THE_DUEL_BEGIN;

		broadcastToTeam(sm, _team1);
		broadcastToTeam(sm, _team2);

		return _countdown;
	}

	/**
	 * The duel has reached a state in which it can no longer continue
	 * 
	 * @param result
	 *            of duel
	 */
	public void endDuel(final DuelResultEnum result)
	{
		// _log.info("Executing duel End task.");
		if(getPlayerA() == null || getPlayerB() == null)
		{
			_log.warning("Duel: Duel end with null players.");
			_playerConditions.clear();
			_playerConditions = null;
			return;
		}

		// inform players of the result
		SystemMessage sm;
		switch (result)
		{
			case Team1Win:
				restorePlayerConditions(false);
				// send SystemMessage
				if(_isPartyDuel)
					sm = new SystemMessage(SystemMessage.S1S_PARTY_HAS_WON_THE_DUEL);
				else
					sm = new SystemMessage(SystemMessage.S1_HAS_WON_THE_DUEL);
				sm.addString(getPlayerA().getName());

				broadcastToTeam(sm, _team1);
				broadcastToTeam(sm, _team2);
				break;
			case Team2Win:
				restorePlayerConditions(false);
				// send SystemMessage
				if(_isPartyDuel)
					sm = new SystemMessage(SystemMessage.S1S_PARTY_HAS_WON_THE_DUEL);
				else
					sm = new SystemMessage(SystemMessage.S1_HAS_WON_THE_DUEL);
				sm.addString(getPlayerB().getName());

				broadcastToTeam(sm, _team1);
				broadcastToTeam(sm, _team2);
				break;
			case Team1Surrender:
				restorePlayerConditions(false);
				// send SystemMessage
				if(_isPartyDuel)
					sm = new SystemMessage(SystemMessage.SINCE_S1S_PARTY_WITHDREW_FROM_THE_DUEL_S1S_PARTY_HAS_WON);
				else
					sm = new SystemMessage(SystemMessage.SINCE_S1_WITHDREW_FROM_THE_DUEL_S2_HAS_WON);
				sm.addString(getPlayerA().getName());
				sm.addString(getPlayerB().getName());

				broadcastToTeam(sm, _team1);
				broadcastToTeam(sm, _team2);
				break;
			case Team2Surrender:
				restorePlayerConditions(false);
				// send SystemMessage
				if(_isPartyDuel)
					sm = new SystemMessage(SystemMessage.SINCE_S1S_PARTY_WITHDREW_FROM_THE_DUEL_S1S_PARTY_HAS_WON);
				else
					sm = new SystemMessage(SystemMessage.SINCE_S1_WITHDREW_FROM_THE_DUEL_S2_HAS_WON);
				sm.addString(getPlayerA().getName());
				sm.addString(getPlayerB().getName());

				broadcastToTeam(sm, _team1);
				broadcastToTeam(sm, _team2);
				break;
			case Canceled:
				restorePlayerConditions(true);
				broadcastToTeam(Msg.THE_DUEL_HAS_ENDED_IN_A_TIE, _team1);
				broadcastToTeam(Msg.THE_DUEL_HAS_ENDED_IN_A_TIE, _team2);
				break;
			case Timeout:
				stopFighting();
				// hp,mp,cp seem to be restored in a timeout too...
				restorePlayerConditions(false);
				broadcastToTeam(Msg.THE_DUEL_HAS_ENDED_IN_A_TIE, _team1);
				broadcastToTeam(Msg.THE_DUEL_HAS_ENDED_IN_A_TIE, _team2);
				break;
		}

		// Send end duel packet
		// TODO: verify: is this done correctly?
		final ExDuelEnd duelEnd = new ExDuelEnd(_isPartyDuel ? 1 : 0);

		broadcastToTeam(duelEnd, _team1);
		broadcastToTeam(duelEnd, _team2);

		// clean up
		_playerConditions.clear();
		_playerConditions = null;
	}

	/**
	 * Did a situation occur in which the duel has to be ended?
	 * 
	 * @return DuelResultEnum duel status
	 */
	public DuelResultEnum checkEndDuelCondition()
	{
		L2Player p1, p2;
		// one of the players might leave during duel
		if((p1 = getPlayerA()) == null || (p2 = getPlayerB()) == null)
			return DuelResultEnum.Canceled;

		// got a duel surrender request?
		if(_surrenderRequest != 0)
		{
			if(_surrenderRequest == 1)
				return DuelResultEnum.Team1Surrender;
			return DuelResultEnum.Team2Surrender;
		}
		// duel timed out
		else if(getRemainingTime() <= 0)
			return DuelResultEnum.Timeout;
		// Has a player been declared winner yet?
		if(_playerConditions.get(_team1.get(0)).getDuelState() == DuelState.Winner)
			return DuelResultEnum.Team1Win;
		if(_playerConditions.get(_team2.get(0)).getDuelState() == DuelState.Winner)
			return DuelResultEnum.Team2Win;

		// More end duel conditions for 1on1 duels
		else if(!_isPartyDuel)
		{
			// Duel was interrupted e.g.: player was attacked by mobs / other players
			if(_playerConditions.get(_team1.get(0)).getDuelState() == DuelState.Interrupted || _playerConditions.get(_team2.get(0)).getDuelState() == DuelState.Interrupted)
				return DuelResultEnum.Canceled;

			// Are the players too far apart?
			if(p1.getDistance3D(p2) > 1600)
				return DuelResultEnum.Canceled;

			// Did one of the players engage in PvP combat?
			if(isDuelistInPvp(true))
				return DuelResultEnum.Canceled;

			// is one of the players in a Siege, Peace or PvP zone?
			if(p1.isInPeaceZone() || p2.isInPeaceZone() || p1.isOnSiegeField() || p2.isOnSiegeField() || p1.isInCombatZone() || p2.isInCombatZone() || p1.isInWater() || p2.isInWater() || p1.isFishing() || p2.isFishing()) // и рыбку ловить тоже
				return DuelResultEnum.Canceled;
		}

		return DuelResultEnum.Continue;
	}

	/**
	 * Register a surrender request
	 * 
	 * @param player
	 *            that had surrender
	 */
	public void doSurrender(final L2Player player)
	{
		// already recived a surrender request
		if(_surrenderRequest != 0)
			return;

		// TODO: Can every party member cancel a party duel? or only the party leaders?
		if(getTeamForPlayer(player) == null)
		{
			_log.warning("Error handling duel surrender request by " + player.getName());
			return;
		}

		if(_team1.contains(player.getStoredId()))
		{
			_surrenderRequest = 1;
			for(final Long temp : _team1)
				setDuelState(temp, DuelState.Dead);

			for(final Long temp : _team2)
				setDuelState(temp, DuelState.Winner);
		}
		else if(_team2.contains(player.getStoredId()))
		{
			_surrenderRequest = 2;
			for(final Long temp : _team2)
				setDuelState(temp, DuelState.Dead);

			for(final Long temp : _team1)
				setDuelState(temp, DuelState.Winner);
		}
	}

	/**
	 * This function is called whenever a player was defeated in a duel
	 * 
	 * @param player
	 *            tat loose the duel
	 */
	public void onPlayerDefeat(final L2Player player)
	{
		// Set player as defeated
		setDuelState(player.getStoredId(), DuelState.Dead);

		if(_isPartyDuel)
		{
			boolean teamdefeated = true;
			GArray<Long> team = getTeamForPlayer(player);
			for(final Long temp : getTeamForPlayer(player))
				if(getDuelState(temp) == DuelState.Fighting)
				{
					teamdefeated = false;
					break;
				}

			if(teamdefeated)
			{
				// Установить поьедителем противоположеную команду
				team = team == _team1 ? _team2 : _team1;
				for(final Long temp : team)
					setDuelState(temp, DuelState.Winner);
			}
		}
		else
		{
			if(player != getPlayerA() && player != getPlayerB())
				_log.warning("Error in onPlayerDefeat(): player is not part of this 1vs1 duel");

			if(getPlayerA() == player)
				setDuelState(_team2.get(0), DuelState.Winner);
			else
				setDuelState(_team1.get(0), DuelState.Winner);
		}
	}

	/**
	 * This function is called whenever a player leaves a party
	 * 
	 * @param player
	 *            leaving player
	 */
	public void onRemoveFromParty(final L2Player player)
	{
		// if it isnt a party duel ignore this
		if(!_isPartyDuel)
			return;

		// this player is leaving his party during party duel
		// if hes either playerA or playerB cancel the duel and port the players back
		if(player.getStoredId() == _team1.get(0) || player.getStoredId() == _team2.get(0))
			for(final PlayerCondition pc : _playerConditions.values())
			{
				pc.TeleportBack();
				pc.getPlayer().setDuel(null);
			}
		else
		// teleport the player back & delete his PlayerCondition record
		{
			final PlayerCondition pc = _playerConditions.get(player.getStoredId());

			if(pc == null)
			{
				_log.warning("Duel: Error, can't get player condition from list.");
				return;
			}

			pc.TeleportBack();
			_playerConditions.remove(player.getStoredId());

			// Удалить игрока со списков учасников
			if(_team1.contains(player.getStoredId()))
				_team1.remove(player.getStoredId());
			else if(_team2.contains(player.getStoredId()))
				_team2.remove(player.getStoredId());

			player.setDuel(null);
		}
	}

	/**
	 * Получаем playerCondition
	 * 
	 * @param player
	 *            у которого мы хотим получить
	 * @return либо playerCondition либо null
	 */
	public PlayerCondition getPlayerCondition(final L2Player player)
	{
		return _playerConditions == null ? null : _playerConditions.get(player.getStoredId());
	}

	/**
	 * Получить состояние дуэли
	 * 
	 * @param player
	 *            игрок состояние кого пытаемся получить.
	 * @return состояние дуели
	 */
	public DuelState getDuelState(final Long playerStoreId)
	{
		if(_playerConditions == null)
			return null;
		final PlayerCondition cond = _playerConditions.get(playerStoreId);
		return cond == null ? null : cond.getDuelState();
	}

	/**
	 * Устанавливает состояние дуели
	 * 
	 * @param player
	 *            кому устанавливать
	 * @param state
	 *            что устанавливать
	 */
	public void setDuelState(final Long playerStoreId, final DuelState state)
	{
		if(_playerConditions == null)
			return;
		final PlayerCondition cond = _playerConditions.get(playerStoreId);
		if(cond != null)
			cond.setDuelState(state);
	}

	/**
	 * Broadcasts a packet to the team opposing the given player.
	 * 
	 * @param player
	 *            to whos opponents you wish to send packet
	 * @param packet
	 *            what you wish to send
	 */
	public void broadcastToOppositTeam(final L2Player player, final L2GameServerPacket packet)
	{

		if(_team1.contains(player.getStoredId()))
			broadcastToTeam(packet, _team2);
		else if(_team2.contains(player.getStoredId()))
			broadcastToTeam(packet, _team1);
		else
			_log.warning("Duel: Broadcast by player who is not in duel");
	}

	public GArray<Long> getTeamForPlayer(final L2Player p)
	{
		if(_team1.contains(p.getStoredId()))
			return _team1;
		if(_team2.contains(p.getStoredId()))
			return _team2;
		_log.warning("Duel: got request for player team who is not duel participant");
		return null;
	}

	/**
	 * Посколько мы снесли к чертям мэнеджер дуэлей (Нафига оно надо???)
	 * мы должны запускать дуэли как-то по другому.
	 * Статический метод вполне устроит, т.к. все нужное находится в инстансе.
	 * 
	 * @param playerA
	 *            бросающий вызов
	 * @param playerB
	 *            кто бросает вызов
	 * @param partyDuel
	 *            партийная или нет
	 */
	public static void createDuel(final L2Player playerA, final L2Player playerB, final int partyDuel)
	{
		if(playerA == null || playerB == null || playerA.getDuel() != null || playerB.getDuel() != null)
			return;

		if(partyDuel == 1)
		{
			boolean playerInPvP = false;
			for(final L2Player temp : playerA.getParty().getPartyMembers())
				if(temp.getPvpFlag() != 0)
				{
					playerInPvP = true;
					break;
				}
			if(!playerInPvP)
				for(final L2Player temp : playerB.getParty().getPartyMembers())
					if(temp.getPvpFlag() != 0)
					{
						playerInPvP = true;
						break;
					}
			// A player has PvP flag
			if(playerInPvP)
			{
				for(final L2Player temp : playerA.getParty().getPartyMembers())
					temp.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
				for(final L2Player temp : playerB.getParty().getPartyMembers())
					temp.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
				return;
			}
		}
		else if(playerA.getPvpFlag() != 0 || playerB.getPvpFlag() != 0)
		{
			playerA.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
			playerB.sendPacket(CustomSystemMessageId.DUEL_CANCELED.getPacket());
			return;
		}

		// запуск дуэли происходит в ее конструкторе
		new Duel(playerA, playerB, partyDuel == 1);
	}

	public static boolean checkIfCanDuel(final L2Player requestor, final L2Player target, final boolean sendMessage)
	{
		int _noDuelReason = 0;
		if(target.isInCombat()) // TODO: jail check?
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_CURRENTLY_ENGAGED_IN_BATTLE;
		else if(target.isDead() || target.isAlikeDead() || target.getCurrentHp() < target.getMaxHp() / 2 || target.getCurrentMp() < target.getMaxMp() / 2 || target.getCurrentCp() < target.getCurrentCp() / 2)
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1S_HP_OR_MP_IS_BELOW_50_PERCENT;
		else if(target.getDuel() != null)
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_ALREADY_ENGAGED_IN_A_DUEL;
		else if(target.isInOlympiadMode())
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_PARTICIPATING_IN_THE_OLYMPIAD;
		else if(target.isCursedWeaponEquipped() || target.getKarma() > 0 || target.getPvpFlag() > 0)
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_IN_A_CHAOTIC_STATE;
		else if(target.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_CURRENTLY_ENGAGED_IN_A_PRIVATE_STORE_OR_MANUFACTURE;
		else if(target.isMounted() || target.isInVehicle())
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_CURRENTLY_RIDING_A_BOAT_WYVERN_OR_STRIDER;
		else if(target.isFishing())
			_noDuelReason = SystemMessage.S1_CANNOT_DUEL_BECAUSE_S1_IS_CURRENTLY_FISHING;
		else if(target.isInCombatZone() || target.isInPeaceZone() || target.isOnSiegeField() || target.isSwimming())
			_noDuelReason = SystemMessage.S1_CANNOT_MAKE_A_CHALLANGE_TO_A_DUEL_BECAUSE_S1_IS_CURRENTLY_IN_A_DUEL_PROHIBITED_AREA;
		else if(requestor.getDistance3D(target) > 1200)
			_noDuelReason = SystemMessage.S1_CANNOT_RECEIVE_A_DUEL_CHALLENGE_BECAUSE_S1_IS_TOO_FAR_AWAY;
		else if(target.getTransformationId() != 0)
			_noDuelReason = SystemMessage.C1_CANNOT_DUEL_BECAUSE_C1_IS_CURRENTLY_POLYMORPHED;

		if(sendMessage && _noDuelReason != 0)
		{
			SystemMessage sm;
			if(requestor != target)
			{
				sm = new SystemMessage(_noDuelReason);
				sm.addString(target.getName());
				requestor.sendPacket(sm);
			}
			else
			{
				sm = new SystemMessage(SystemMessage.YOU_ARE_UNABLE_TO_REQUEST_A_DUEL_AT_THIS_TIME);
				requestor.sendPacket(sm);
			}
		}
		return _noDuelReason == 0;
	}

	public void onBuff(final L2Player player, final L2Effect debuff)
	{
		if(player == null || debuff == null || _playerConditions == null)
			return;
		final PlayerCondition pcon = _playerConditions.get(player.getStoredId());
		if(pcon != null)
			pcon.registerDebuff(debuff);
	}

	private GArray<L2Player> getPlayers(final GArray<Long> team)
	{
		final GArray<L2Player> result = new GArray<L2Player>(team.size());
		L2Player player;
		for(final Long storedId : team)
			if((player = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				result.add(player);
		return result;
	}
}
