package l2n.game.model.entity;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2PathfinderInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;

public class KamalokaNightmare extends Reflection
{
	private final class PathfinderTask implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				for(final L2Spawn s : getSpawns().toArray(new L2Spawn[_spawns.size()]))
					if(s != null)
						s.despawnAll();
				getSpawns().clear();

				final GArray<L2MonsterInstance> delete_list = new GArray<L2MonsterInstance>();
				final GArray<L2Object> all_objects = getObjects();
				for(L2Object o : all_objects)
				{
					if(o != null && o instanceof L2MonsterInstance)
						delete_list.add((L2MonsterInstance) o);
				}

				for(final L2MonsterInstance o : delete_list)
					o.deleteMe();

				final L2Player player = (L2Player) L2ObjectsStorage.findObject(_player);
				if(player != null)
				{
					player.sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(10));
					// spawn '32485','Pathfinder Worker'

					final L2PathfinderInstance npc = new L2PathfinderInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(32485));
					npc.setSpawnedLoc(getTeleportLoc());
					npc.setReflection(getId());
					npc.onSpawn();
					npc.spawnMe(npc.getSpawnedLoc());
				}
				else
					collapse();
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "PathfinderTask: get error ", e);
			}
		}
	}

	public static final int TIME_LIMIT = 1200000;
	public static final int KAMALOKA_ESSENCE = 13002;

	private final int _player;
	private ScheduledFuture<PathfinderTask> _pathfinderTask;

	private final HashMap<L2NpcTemplate, Integer> counter = new HashMap<L2NpcTemplate, Integer>();

	public KamalokaNightmare(final L2Player player)
	{
		super("Kamaloka Nightmare");
		_player = player.getObjectId();
		_pathfinderTask = L2GameThreadPools.getInstance().scheduleGeneral(new PathfinderTask(), TIME_LIMIT);
	}

	@Override
	public void collapse()
	{
		ReflectionTable.getInstance().removeSoloKamaloka(_player);
		stopPathfinderTimer();
		super.collapse();
	}

	public void registerKilled(final L2NpcTemplate template)
	{
		Integer current = counter.get(template);
		if(current == null)
			current = 0;
		current++;
		counter.put(template, current);
	}

	public HashMap<L2NpcTemplate, Integer> getCounter()
	{
		return counter;
	}

	private void stopPathfinderTimer()
	{
		if(_pathfinderTask != null)
			_pathfinderTask.cancel(false);
		_pathfinderTask = null;
	}

	public int getPlayerId()
	{
		return _player;
	}

	/** Если рефлекшн является соло камалокой */
	@Override
	public boolean isKamalokaNightmare()
	{
		return true;
	}

	@Override
	public boolean canChampions()
	{
		return false;
	}
}
