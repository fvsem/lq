package l2n.game.model.entity.vehicle;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.*;
import l2n.util.Location;

public class L2Ship extends L2Vehicle
{
	public L2Ship(final String name, final int id)
	{
		super(name, id);
	}

	@Override
	public void begin()
	{
		final L2VehicleTrajet t = _cycle == 1 ? _t1 : _t2;

		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getVehicle() == this)
			{
				final L2ItemInstance it = player.getInventory().getItemByItemId(t._ticketId);
				if(it != null && it.getCount() >= 1)
				{
					player.getInventory().destroyItem(it.getObjectId(), 1, false);
					player.sendPacket(new VehicleStart(this, 1));
				}
				else if(it == null && t._ticketId == 0 || player.isGM())
					player.sendPacket(new VehicleStart(this, 1));
				else
					exitFromBoat(player);
			}
		super.begin();
	}

	@Override
	public void broadcastVehicleStart(final int state)
	{
		broadcastPacket(new VehicleStart(this, state));
	}

	@Override
	public void broadcastVehicleCheckLocation()
	{
		broadcastPacket(new VehicleCheckLocation(this));
	}

	@Override
	public void broadcastVehicleInfo()
	{
		broadcastPacket(new VehicleInfo(this));
	}

	@Override
	public void broadcastGetOffVehicle(final L2Player player, final Location loc)
	{
		broadcastPacket(new GetOffVehicle(player, this, loc.x, loc.y, loc.z));
	}

	@Override
	public void sendVehicleInfo(final L2Player player)
	{
		player.sendPacket(new VehicleInfo(this));
	}

	@Override
	public L2GameServerPacket stopMovePacket()
	{
		return new StopMove(this);
	}

	@Override
	public void oustPlayers()
	{}
}
