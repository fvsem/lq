package l2n.game.model.entity.vehicle;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.templates.L2CharTemplate;
import l2n.game.templates.L2Weapon;
import l2n.util.Location;

import java.util.concurrent.ScheduledFuture;

public abstract class L2Vehicle extends L2Character
{
	private final int _id;
	public int _cycle = 0;
	public int _speed1;
	public int _speed2;
	protected int _runstate = 0;

	protected L2VehicleTrajet _t1;
	protected L2VehicleTrajet _t2;

	protected ScheduledFuture<L2VehicleCaptain> _vehicleCaptainTask;

	private boolean _isArrived;
	private int _playersCountAtBoard;

	public L2Vehicle(final String name, final int id)
	{
		super(IdFactory.getInstance().getNextId(), new L2CharTemplate(L2CharTemplate.getEmptyStatsSet()));
		_name = name;
		_id = id;
	}

	public int getPlayersCountAtBoard()
	{
		return _playersCountAtBoard;
	}

	public void setIsArrived(final boolean val)
	{
		_isArrived = val;
	}

	public boolean isArrived()
	{
		return _isArrived;
	}

	public boolean isDocked()
	{
		return _runstate == 0;
	}

	public void setIsDocked()
	{
		_runstate = 0;
	}

	public int getId()
	{
		return _id;
	}

	@Override
	public int getMoveSpeed()
	{
		return _speed1;
	}

	@Override
	public int getRunSpeed()
	{
		return _speed1;
	}

	public int getRotationSpeed()
	{
		return _speed2;
	}

	@Override
	public void setXYZ(final int x, final int y, final int z)
	{
		super.setXYZ(x, y, z);
		updatePeopleInTheBoat(new Location(x, y, z));
		if(isClanAirShip() && !isDocked() && isArrived())
			((L2AirShip) this).tryToLand();
	}

	@Override
	public void setXYZ(final int x, final int y, final int z, final boolean MoveTask)
	{
		super.setXYZ(x, y, z, MoveTask);
		updatePeopleInTheBoat(new Location(x, y, z));
		if(isClanAirShip() && !isDocked() && isArrived())
			((L2AirShip) this).tryToLand();
	}

	public void VehicleArrived()
	{
		if(_cycle == 1)
			_t1.moveNext();
		else
			_t2.moveNext();
	}

	public void updatePeopleInTheBoat(final Location loc)
	{
		_playersCountAtBoard = 0;
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getVehicle() == this)
			{
				_playersCountAtBoard++;
				player.setLoc(loc);
			}
	}

	public void begin()
	{
		if(_cycle == 1)
			_t1.moveNext();
		else
			_t2.moveNext();
	}

	protected void exitFromBoat(final L2Player player)
	{
		final L2VehicleTrajet t = _cycle == 1 ? _t1 : _t2;
		player.sendPacket(Msg.YOU_MAY_NOT_GET_ON_BOARD_WITHOUT_A_PASS);
		player.setVehicle(null);
		broadcastGetOffVehicle(player, t._return);
		player.teleToLocation(t._return);
	}

	public void say(final int i)
	{
		if(isClanAirShip())
			return;

		final L2VehicleTrajet t = _cycle == 1 ? _t1 : _t2;
		switch (i)
		{
			case 5:
				SayAndSound(t._msgs[0], getId() == 8 || getId() == 9 ? t._msgs[2] : t._msgs[1], "itemsound.ship_arrival_departure");
				break;
			case 3:
				SayAndSound(t._msgs[0], t._msgs[2], "itemsound.ship_5min"); // 3 минуты (не 5)
				break;
			case 1:
				SayAndSound(t._msgs[0], t._msgs[3], "itemsound.ship_1min");
				break;
			case 0:
				SayAndSound(t._msgs[0], t._msgs[4], null);
				break;
			case -1:
				SayAndSound(t._msgs[0], t._msgs[5], "itemsound.ship_arrival_departure");
				break;
		}
	}

	public void SayAndSound(final String npc, final String text, final String sound)
	{
		if(npc == null || text == null || npc.isEmpty() || text.isEmpty())
			return;

		final CreatureSay s1 = new CreatureSay(0, Say2C.SHOUT, npc, text);
		final PlaySound s2 = sound == null ? null : new PlaySound(0, sound, 1, getObjectId(), getLoc());
		for(final L2Player player : L2World.getAroundPlayers(this, 10000, 1000))
			if(player != null)
			{
				player.sendPacket(s1);
				if(s2 != null)
					player.sendPacket(s2);
			}
	}

	public void spawn()
	{
		if(isClanAirShip())
		{
			final L2AirShip airship = (L2AirShip) this;
			setHeading(airship.getClanAirshipSpawnLoc().h);
			setXYZInvisible(airship.getClanAirshipSpawnLoc());
			airship.startMaintenanceTask();
		}

		spawnMe();
		broadcastVehicleInfo();
		_cycle = 1;
		say(5);
		if(_vehicleCaptainTask != null)
			_vehicleCaptainTask.cancel(true);
		_vehicleCaptainTask = L2GameThreadPools.getInstance().scheduleGeneral(new L2VehicleCaptain(this, 1), isClanAirShip() ? 0 : getNpcId() == 5 ? 180000 : 120000);
	}

	public void startManual()
	{
		if(isClanAirShip())
		{
			final L2AirShip airship = (L2AirShip) this;
			setHeading(airship.getClanAirshipSpawnLoc().h);
			setXYZInvisible(airship.getClanAirshipSpawnLoc());
			airship.startMaintenanceTask();
		}

		spawnMe();
		broadcastVehicleInfo();
		if(_vehicleCaptainTask != null)
			_vehicleCaptainTask.cancel(true);
		say(-1);
		begin();
	}

	public void despawn()
	{
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getVehicle() == this)
				exitFromBoat(player);
		if(isVisible())
			decayMe();
	}

	public void teleportShip(final int x, final int y, final int z, final int heading)
	{
		if(isMoving)
			stopMove(false);

		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getVehicle() == this)
			{
				if(player.isGM())
					player.sendMessage("teleport to: (" + x + ", " + y + ", " + z + ")");
				sendStopMove(player);
				broadcastGetOffVehicle(player, new Location(x, y, z));
				player.teleToLocation(x, y, z);
			}

		setHeading(heading);
		// setXYZ(x, y, z, true);
		teleToLocation(x, y, z);

		L2GameThreadPools.getInstance().scheduleGeneral(new L2VehicleArrived(this), 5000);
	}

	@Override
	public void broadcastPacket(final L2GameServerPacket... packets)
	{
		if(!isVisible())
			return;

		final GArray<L2Player> list = new GArray<L2Player>();

		for(final L2Player player : L2World.getAroundPlayers(this))
			if(player != null)
				list.add(player);

		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getVehicle() == this && !list.contains(player))
				list.add(player);

		for(final L2Player target : list)
			target.sendPacket(packets);
	}

	public abstract void broadcastVehicleStart(int state);

	public abstract void broadcastVehicleCheckLocation();

	public abstract void broadcastVehicleInfo();

	public abstract void broadcastGetOffVehicle(L2Player player, Location loc);

	public abstract void sendVehicleInfo(L2Player player);

	public final void sendStopMove(final L2Player player)
	{
		player.sendPacket(stopMovePacket());
	}

	public final void broadcastStopMove()
	{
		broadcastPacket(stopMovePacket());
	}

	@Override
	public final boolean isAlikeDead()
	{
		return false;
	}

	public abstract void oustPlayers();

	public void SetTrajet1(final int idWaypoint1, final int idWTicket1, final Location ret_loc, final String[] msgs)
	{
		_t1 = new L2VehicleTrajet(this, idWaypoint1, idWTicket1, ret_loc, msgs);
	}

	public L2VehicleTrajet getTrajet1()
	{
		return _t1;
	}

	public void SetTrajet2(final int idWaypoint1, final int idWTicket1, final Location ret_loc, final String[] msgs)
	{
		_t2 = new L2VehicleTrajet(this, idWaypoint1, idWTicket1, ret_loc, msgs);
	}

	@Override
	public L2ItemInstance getActiveWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getActiveWeaponItem()
	{
		return null;
	}

	@Override
	public L2ItemInstance getSecondaryWeaponInstance()
	{
		return null;
	}

	@Override
	public L2Weapon getSecondaryWeaponItem()
	{
		return null;
	}

	@Override
	public byte getLevel()
	{
		return 0;
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return false;
	}

	@Override
	public boolean isAttackable(final L2Character attacker)
	{
		return false;
	}

	public boolean isClanAirShip()
	{
		return false;
	}

	public L2Player getDriver()
	{
		return null;
	}

	public L2VehicleTrajet getTrajet(final int id)
	{
		final L2VehicleTrajet t = id == 1 ? _t1 : _t2;
		return t;
	}
}
