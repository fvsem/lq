package l2n.game.model.entity.vehicle;

public class L2VehiclePoint
{
	public int x;
	public int y;
	public int z;
	public int heading;
	public int speed1;
	public int speed2;

	@Override
	public String toString()
	{
		return "speed1=\"" + speed1 + "\" speed2=\"" + speed2 + "\" loc=\"" + x + " " + y + " " + z + " " + heading + "\"";
	}
}
