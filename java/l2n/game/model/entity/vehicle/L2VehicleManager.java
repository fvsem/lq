package l2n.game.model.entity.vehicle;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.util.Location;
import l2n.util.Log;

import java.io.*;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class L2VehicleManager
{
	private static final Logger _log = Logger.getLogger(L2VehicleManager.class.getName());
	private static L2VehicleManager _instance;
	private ConcurrentHashMap<Integer, L2Vehicle> _staticItems;

	public L2VehicleManager()
	{
		_staticItems = new ConcurrentHashMap<Integer, L2Vehicle>();
	}

	public static L2VehicleManager getInstance()
	{
		if(_instance == null)
		{
			_log.info("Initializing L2VehicleManager");
			_instance = new L2VehicleManager();
			_instance.loadShips();
		}
		return _instance;
	}

	public void loadShips()
	{
		if(!Config.ALLOW_BOAT)
			return;

		if(_staticItems == null)
			_staticItems = new ConcurrentHashMap<Integer, L2Vehicle>();

		LineNumberReader lnr = null;
		try
		{
			File vehicleData = new File(Config.DATAPACK_ROOT, "data/vehicle.csv");
			lnr = new LineNumberReader(new BufferedReader(new FileReader(vehicleData)));

			String line = null;
			while ((line = lnr.readLine()) != null)
			{
				if(line.startsWith("#") || line.trim().length() == 0)
					continue;

				parseLine(line);
			}
			_log.info("L2VehicleManager: Loaded " + _staticItems.size() + " vehicles.");
		}
		catch(FileNotFoundException e)
		{
			_log.warning("boat.csv is missing in data folder");
		}
		catch(Exception e)
		{
			_log.warning("error while creating vehicle table ");
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(lnr != null)
					lnr.close();
			}
			catch(Exception e1)
			{}
		}
	}

	private class Spawn implements Runnable
	{
		L2Vehicle ship;

		public Spawn(L2Vehicle ship)
		{
			this.ship = ship;
		}

		@Override
		public void run()
		{
			ship.spawn();
		}
	}

	private void parseLine(String line)
	{
		StringTokenizer st = new StringTokenizer(line, ";");

		String name = st.nextToken();
		int id = Integer.parseInt(st.nextToken());
		int timer = Integer.parseInt(st.nextToken());

		L2Vehicle ship = name.startsWith("AirShip") ? new L2AirShip(null, name, id) : new L2Ship(name, id);

		int xspawn = Integer.parseInt(st.nextToken());
		int yspawn = Integer.parseInt(st.nextToken());
		int zsawn = Integer.parseInt(st.nextToken());
		int heading = Integer.parseInt(st.nextToken());

		ship.setHeading(heading); // Heading нужно выставлять до локации
		ship.setXYZInvisible(new Location(xspawn, yspawn, zsawn));

		int idWaypoint = Integer.parseInt(st.nextToken());
		int idWTicket = Integer.parseInt(st.nextToken());
		int ntx1 = Integer.parseInt(st.nextToken());
		int nty1 = Integer.parseInt(st.nextToken());
		int ntz1 = Integer.parseInt(st.nextToken());
		String npc1 = st.nextToken();
		String mess10_1 = st.nextToken();
		String mess5_1 = st.nextToken();
		String mess1_1 = st.nextToken();
		String mess0_1 = st.nextToken();
		String messb_1 = st.nextToken();
		Location ret_loc = new Location(ntx1, nty1, ntz1);
		String[] msgs = new String[] { npc1, mess10_1, mess5_1, mess1_1, mess0_1, messb_1 };
		ship.SetTrajet1(idWaypoint, idWTicket, ret_loc, msgs);

		idWaypoint = Integer.parseInt(st.nextToken());
		idWTicket = Integer.parseInt(st.nextToken());
		ntx1 = Integer.parseInt(st.nextToken());
		nty1 = Integer.parseInt(st.nextToken());
		ntz1 = Integer.parseInt(st.nextToken());
		npc1 = st.nextToken();
		mess10_1 = st.nextToken();
		mess5_1 = st.nextToken();
		mess1_1 = st.nextToken();
		mess0_1 = st.nextToken();
		messb_1 = st.nextToken();
		ret_loc = new Location(ntx1, nty1, ntz1);
		msgs = new String[] { npc1, mess10_1, mess5_1, mess1_1, mess0_1, messb_1 };
		ship.SetTrajet2(idWaypoint, idWTicket, ret_loc, msgs);

		_staticItems.put(ship.getObjectId(), ship);

		L2GameThreadPools.getInstance().scheduleGeneral(new Spawn(ship), timer * 1000);
	}

	public ConcurrentHashMap<Integer, L2Vehicle> getBoats()
	{
		if(_staticItems == null)
			_staticItems = new ConcurrentHashMap<Integer, L2Vehicle>();
		return _staticItems;
	}

	public L2Vehicle getBoat(int boatId)
	{
		if(_staticItems == null)
			_staticItems = new ConcurrentHashMap<Integer, L2Vehicle>();
		return _staticItems.get(boatId);
	}

	public void addStaticItem(L2Vehicle boat)
	{
		_staticItems.put(boat.getObjectId(), boat);
	}

	@SuppressWarnings("unused")
	private void dump()
	{
		Log.add("<list>", "vehicle", false);
		for(L2Vehicle boat : getBoats().values())
		{
			Log.add("<vehicle id=\"" + boat.getId() + "\" name=\"" + boat.getName() + "\" loc=\"" + boat.getLoc().Print(true) + "\">", "vehicle", false);
			L2VehicleTrajet t = boat.getTrajet(1);
			Log.add("<trajet id=\"" + 1 + "\">", "vehicle", false);
			Log.add("<waypoint id=\"" + t._IdWaypoint + "\"/>", "vehicle", false);
			Log.add("<ticket id=\"" + t._ticketId + "\"/>", "vehicle", false);
			Log.add("<locnoticket loc=\"" + t._return.Print(false) + "\"/>", "vehicle", false);
			Log.add("<annonceur str=\"" + t._msgs[0] + "\"/>", "vehicle", false);
			Log.add("<message10 str=\"" + t._msgs[1] + "\"/>", "vehicle", false);
			Log.add("<message5 str=\"" + t._msgs[2] + "\"/>", "vehicle", false);
			Log.add("<message1 str=\"" + t._msgs[3] + "\"/>", "vehicle", false);
			Log.add("<message0 str=\"" + t._msgs[4] + "\"/>", "vehicle", false);
			Log.add("<message_begin str=\"" + t._msgs[5] + "\"/>", "vehicle", false);
			Log.add("</trajet>", "vehicle", false);

			t = boat.getTrajet(2);
			Log.add("<trajet id=\"" + 2 + "\">", "vehicle", false);
			Log.add("<waypoint id=\"" + t._IdWaypoint + "\"/>", "vehicle", false);
			Log.add("<ticket id=\"" + t._ticketId + "\"/>", "vehicle", false);
			Log.add("<locnoticket loc=\"" + t._return.Print(false) + "\"/>", "vehicle", false);
			Log.add("<annonceur str=\"" + t._msgs[0] + "\"/>", "vehicle", false);
			Log.add("<message10 str=\"" + t._msgs[1] + "\"/>", "vehicle", false);
			Log.add("<message5 str=\"" + t._msgs[2] + "\"/>", "vehicle", false);
			Log.add("<message1 str=\"" + t._msgs[3] + "\"/>", "vehicle", false);
			Log.add("<message0 str=\"" + t._msgs[4] + "\"/>", "vehicle", false);
			Log.add("<message_begin str=\"" + t._msgs[5] + "\"/>", "vehicle", false);
			Log.add("</trajet>", "vehicle", false);
			Log.add("</vehicle>", "vehicle", false);
			Log.add("", "vehicle", false);
		}
		Log.add("</list>", "vehicle", false);
	}
}
