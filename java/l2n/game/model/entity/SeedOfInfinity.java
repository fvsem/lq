package l2n.game.model.entity;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongObjectHashMap;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExSendUIEvent;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 1. Players can Attack Hall of Suffering and Hall of Erosion
 * 2. Players can Attack the Heart of Infinity.
 * 3. Attack Phase Complete
 * 4. Players can defend Hall of Suffering and Hall of Erosion
 * 5. Players can defend Hall of Suffering and Heart of Infinity
 * 
 * @author L2System Project
 * @date 12.01.2011
 * @time 16:13:41
 */
public class SeedOfInfinity extends Quest
{
	public abstract class World
	{
		public long instanceId;
		public long timer;
		public int status;

		public abstract void createNpcList();

		public abstract Map<L2NpcInstance, Boolean> getNpcList();

		public abstract void add(L2NpcInstance npc, boolean dead);
	}

	public class HallofSufferingWorld extends World
	{
		public int rewardType = -1;

		public L2NpcInstance tumor;
		public L2NpcInstance destroyedTumor;

		private HashMap<L2NpcInstance, Boolean> npclist;

		public ScheduledFuture<SpawnBossGuards> guardsSpawnTask;

		@Override
		public void createNpcList()
		{
			npclist = new HashMap<L2NpcInstance, Boolean>();
		}

		@Override
		public Map<L2NpcInstance, Boolean> getNpcList()
		{
			return npclist;
		}

		@Override
		public void add(final L2NpcInstance npc, final boolean dead)
		{
			npclist.put(npc, dead);
		}

		public void cancelGuardsSpawn()
		{
			if(guardsSpawnTask != null)
				guardsSpawnTask.cancel(false);
			guardsSpawnTask = null;
		}
	}

	public class HallofErosionWorld extends World
	{
		public L2NpcInstance raidboss;
		public int[] mark_cohemenes;
		public boolean raidboss_spawned = false;
		public boolean raidboss_dead = false;

		public ScheduledFuture<TimeRemaining> remainingTimeTask;

		private HashMap<L2NpcInstance, Boolean> npclist;

		@Override
		public void createNpcList()
		{
			npclist = new HashMap<L2NpcInstance, Boolean>();
		}

		@Override
		public Map<L2NpcInstance, Boolean> getNpcList()
		{
			return npclist;
		}

		@Override
		public void add(final L2NpcInstance npc, final boolean dead)
		{
			npclist.put(npc, dead);
		}
	}

	public class HeartOfInfinityWorld extends World
	{
		public L2NpcInstance center_tumor;
		public int raidbossCount = 0;
		public boolean successfully = false;
		public int warning = 0;
		public int boss_respawn_time = TIME_BOSS_DELAY_DEFAULT; // изначально 40 секунд

		public ScheduledFuture<?> bossSpawnTask;
		public ScheduledFuture<TimeLimitCheck> remainingTimeTask;

		@Override
		public void createNpcList()
		{}

		@Override
		public Map<L2NpcInstance, Boolean> getNpcList()
		{
			return Collections.emptyMap();
		}

		@Override
		public void add(final L2NpcInstance npc, final boolean dead)
		{}
	}

	protected class TimeRemaining implements Runnable
	{
		private final HallofErosionWorld world;
		private final L2CommandChannel commandChannel;
		private final String msg;

		public TimeRemaining(final HallofErosionWorld w, final L2CommandChannel channel, final String text)
		{
			world = w;
			commandChannel = channel;
			msg = text;
		}

		@Override
		public void run()
		{
			if(!world.raidboss_spawned)
			{
				world.timer -= 5;

				// если время уже вышло
				if(world.timer == 0)
					commandChannel.broadcastToChannelMembers(getQuestIntId() == 696 ? FAILED_HALL_OF_EROSION_ATTACK : FAILED_HALL_OF_EROSION_DEFEND);
				else if(msg != null)
				{
					commandChannel.broadcastToChannelMembers(new ExShowScreenMessage(msg.replace("%time%", String.valueOf(world.timer)), 5000, ScreenMessageAlign.TOP_CENTER, false));
					world.remainingTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new TimeRemaining(world, commandChannel, msg), time_limit);
				}
			}
		}
	}

	protected class TimeLimitCheck implements Runnable
	{
		private final HeartOfInfinityWorld world;
		private final L2CommandChannel commandChannel;
		private final String msg;

		public TimeLimitCheck(final HeartOfInfinityWorld w, final L2CommandChannel channel, final String text)
		{
			world = w;
			commandChannel = channel;
			msg = text;
		}

		@Override
		public void run()
		{
			if(!world.successfully)
			{
				world.timer -= 5;

				// если время уже вышло
				if(world.timer <= 0)
				{
					// время кончилось и лимит РБ не превышен
					if(world.raidbossCount < BOSS_LIMIT_COUNT)
					{
						commandChannel.broadcastToChannelMembers(SUCCEEDED_HEART_OF_INFINITY_DEFEND);
						QuestState st;
						for(final L2Player player : commandChannel.getMembers())
							if((st = getPlayerQuestState(player, getName())) != null)
								st.setCond(2);

						world.successfully = true;
						if(world.bossSpawnTask != null)
						{
							world.bossSpawnTask.cancel(false);
							world.bossSpawnTask = null;
						}
						// отменяем таск
						if(world.remainingTimeTask != null)
						{
							world.remainingTimeTask.cancel(false);
							world.remainingTimeTask = null;
						}
					}
					else
					// фейл, время вышло.
					{
						QuestState st;
						for(final L2Player member : commandChannel.getMembers())
							if((st = getPlayerQuestState(member, getName())) != null)
								st.exitCurrentQuest(true);

						if(world.bossSpawnTask != null)
						{
							world.bossSpawnTask.cancel(false);
							world.bossSpawnTask = null;
						}
						// отменяем таск
						if(world.remainingTimeTask != null)
						{
							world.remainingTimeTask.cancel(false);
							world.remainingTimeTask = null;
						}
					}
				}
				else
				{
					commandChannel.broadcastToChannelMembers(new ExShowScreenMessage(msg.replace("%time%", String.valueOf(world.timer)), 5000, ScreenMessageAlign.TOP_CENTER, false));
					world.remainingTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new TimeLimitCheck(world, commandChannel, msg), time_limit);
				}
			}
		}
	}

	protected class SpawnBossGuards implements Runnable
	{
		private final HallofSufferingWorld world;

		public SpawnBossGuards(final HallofSufferingWorld w)
		{
			world = w;
		}

		@Override
		public void run()
		{
			if(world == null)
				return;

			L2Character topDamager = null;
			for(final L2NpcInstance monster : world.getNpcList().keySet())
				if(monster != null)
				{
					if(!monster.isInCombat())
					{
						world.guardsSpawnTask = null;
						return;
					}
					if(topDamager == null)
						topDamager = monster.getTopDamager(monster.getAggroList());
				}
				else if(monster == null || monster.isDead())
				{
					world.guardsSpawnTask = null;
					return;
				}

			L2NpcInstance mob = addSpawnToInstance(TWIN_MOBIDS[Rnd.get(TWIN_MOBIDS.length)], KLODEKUS_LOC, 100, world.instanceId);
			if(topDamager != null)
				mob.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, topDamager, Rnd.get(1, 100));
			if(Rnd.chance(33))
			{
				mob = addSpawnToInstance(TWIN_MOBIDS[Rnd.get(TWIN_MOBIDS.length)], KLANIKUS_LOC, 100, world.instanceId);
				if(topDamager != null)
					mob.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, topDamager, Rnd.get(1, 100));
			}
			world.guardsSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnBossGuards(world), BOSS_MINION_SPAWN_TIME);
		}
	}

	protected class BroadcastOnScreenMsgStr implements Runnable
	{
		private final L2CommandChannel commandChannel;
		private final L2GameServerPacket packet;

		public BroadcastOnScreenMsgStr(final L2CommandChannel channel, final L2GameServerPacket gsp)
		{
			commandChannel = channel;
			packet = gsp;
		}

		@Override
		public void run()
		{
			if(commandChannel != null)
				commandChannel.broadcastToChannelMembers(packet);
		}
	}

	protected class InitialDelayTask implements Runnable
	{
		private final long storedId;

		public InitialDelayTask(final L2Player starter)
		{
			storedId = starter.getStoredId();
		}

		@Override
		public void run()
		{
			L2Player player;
			if((player = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				initialInstance(player);
		}
	}

	/** 695 quest | _695_DefendtheHallofSuffering | START */

	protected static final int[][][] waves_spawn = new int[][][] {//
	//
			{ { 22509, 2 }, { 22510, 3 } }, // wave 1
			{ { 22509, 1 }, { 22510, 3 }, { 22511, 3 } }, // wave 2
			{ { 22509, 2 }, { 22510, 2 }, { 22511, 2 }, { 22512, 3 } }, // wave 3
			{ { 22513, 2 }, { 22511, 2 }, { 22512, 2 }, { 22514, 3 } }, // wave 4
			{ { 22513, 2 }, { 22512, 3 }, { 22514, 3 }, { 22515, 2 } } // wave 5
	};

	protected static final int[] waves_time = { 1000, 10000, 15000, 25000, 30000, 35000, };

	// *************************************************************//

	/** 696 quest | Conquer the Hall of Erosion | START */

	protected static final L2GameServerPacket DESTROY_MORE_TRUMOR = new ExShowScreenMessage("The tumor inside Hall of Erosion has been destroyed! \nIn order to draw out the cowardly Cohemenes, you must destroy all the tumors!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket DESTROY_ALL_TRUMOR = new ExShowScreenMessage("All the tumors inside Hall of Erosion have been destroyed! \nDriven into a corner, Cohemenes appears close by!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket REVIVED_ALL_TRUMOR = new ExShowScreenMessage("The tumor inside Hall of Erosion has completely revived. \nThe restrengthened Cohemenes has fled deeper inside the seed...", 8000, ScreenMessageAlign.TOP_CENTER, false);

	protected static final int[] monsters = new int[] { 22516, 22517, 22518, 22519, 22520, 22521, 22522, 22524, 22526, 22528, 22530, 22532, 22534 };

	protected static final Location[] mark_cohemenes_loactions = {//
	//
			new Location(-178418, 211653, -12029, 49151),//
			new Location(-178417, 206558, -12032, 16384), //
			new Location(-180911, 206551, -12028, 16384), //
			new Location(-180911, 211652, -12028, 49151) };

	protected static final Location[] tumor_death_locations = { //
	//
			new Location(-176036, 210002, -11948, 36863), //
			new Location(-176039, 208203, -11949, 28672), //
			new Location(-183288, 208205, -11939, 4096), //
			new Location(-183290, 210004, -11939, 61439) };

	// РБ
	protected static final int COHEMENES = 25634;
	protected static final int ARGEKUNTE = 25635;
	protected static final int SYMBOL_OF_COHEMENES = 18780;

	/** каждые 5 мин стартует таск на проверку */
	protected static final int time_limit = 5 * 60 * 1000;
	/** максимальное количесто монстров для каждой комнаты */
	protected static final int max_npc_count = 10;

	protected static final String COHEMENES_DIE = "Keu... I will leave for now... But don't think this is over... The Seed of Infinity can never die...";
	protected static final String COHEMENES_START = "C'mon, c'mon! Show your face, you little rats! Let me see what the doomed weaklings are scheming!";

	// *************************************************************//

	/** 697 quest | Defend the Hall of Erosion | START */

	// РБ
	protected static final int RAVENOUS_SOUL_DEVOURER = 25636;
	protected static final int LIFE_SEED = 32541;

	// TODO этих рандомно спаунит РБ
	protected static final int spc_corpse_a = 18718;
	protected static final int spc_corpse_b = 18719;

	// *************************************************************//

	/** 698 quest | _698_BlocktheLordsEscape | START */

	/** лимит времени 25 минут */
	protected static final int TIME_LIMIT = 1500;

	// npc
	protected static final int GATEKEEPER = 32539;
	protected static final int SOUL_DEVOURER = 22523;
	protected static final int FERAL_HOUND = 29152;

	protected static final int TUMOR_OF_DEATH2 = 18708; // npc_ai={[spc_tumor_2lv_a] похоже убивать надо этого, потом ресается другой
	protected static final int DESTROYED_TUMOR2 = 32535; // отображает какой то диалог tumor_d_spc001.htm
	protected static final int CENTER_TUMOR = 32547;

	// value
	protected static final int BOSS_LIMIT_COUNT = 20;
	protected static final int TIME_BOSS_DELAY_DEFAULT = 40;
	protected static final int TIME_BOSS_DELAY_MIN = 20;
	protected static final int TIME_BOSS_DELAY_MAX = 60;
	protected static final int TIME_TUMOR_RESPAWN = 180;

	// локации колоний
	protected static final Location[] tumors_locations = {
			new Location(-179779, 212540, -15520, 49151),
			new Location(-177028, 211135, -15520, 36863),
			new Location(-176355, 208043, -15520, 28672),
			new Location(-179284, 205990, -15520, 16384),
			new Location(-182268, 208218, -15520, 4096),
			new Location(-182069, 211140, -15520, 61439) };

	protected static final Location center_tumor_loction = new Location(-179538, 211313, -15488, 16384);
	protected static final Location boss_spawn_location = new Location(-179528, 206728, -15488);

	protected static final L2GameServerPacket FIRST_FERAL = new ExShowScreenMessage("The first Feral Hound of the Netherworld has awakened!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket SECOND_FERAL = new ExShowScreenMessage("The second Feral Hound of the Netherworld has awakened!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket TUMOR_DIE = new ExShowScreenMessage("The tumor inside Heart of Infinity has been destroyed! \nThe speed that Ekimus calls out his prey has slowed down!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket TUMOR_RESPAWN = new ExShowScreenMessage("The tumor inside Heart of Infinity has completely revived. \nEkimus started to regain his energy and is desperately looking for his prey...", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// *************************************************************//

	// Hall of Erosion Attack
	protected static final L2GameServerPacket PREPARE_HALL_OF_EROSION_ATTACK = new ExShowScreenMessage("You will participate in Hall of Erosion Attack shortly. Be prepared for anything.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket START_HALL_OF_EROSION_ATTACK = new ExShowScreenMessage("You can hear the undead of Ekimus rushing toward you. Hall of Erosion Attack, it has now begun!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket FAILED_HALL_OF_EROSION_ATTACK = new ExShowScreenMessage("You have failed at Hall of Erosion Attack... The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket SUCCEEDED_HALL_OF_EROSION_ATTACK = new ExShowScreenMessage("Congratulations! You have succeeded at Hall of Erosion Attack! The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// Hall of Erosion Defend
	protected static final L2GameServerPacket PREPARE_HALL_OF_EROSION_DEFEND = new ExShowScreenMessage("You will participate in Hall of Erosion Defend shortly. Be prepared for anything.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket START_HALL_OF_EROSION_DEFEND = new ExShowScreenMessage("You can hear the undead of Ekimus rushing toward you. Hall of Erosion Defend, it has now begun!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket FAILED_HALL_OF_EROSION_DEFEND = new ExShowScreenMessage("You have failed at Hall of Erosion Defend... The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket SUCCEEDED_HALL_OF_EROSION_DEFEND = new ExShowScreenMessage("Congratulations! You have succeeded at Hall of Erosion Defend! The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// Heart of Infinity Defend
	protected static final L2GameServerPacket PREPARE_HEART_OF_INFINITY_DEFEND = new ExShowScreenMessage("You will participate in Heart of Infinity Defend shortly. Be prepared for anything.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket START_HEART_OF_INFINITY_DEFEND = new ExShowScreenMessage("You can hear the undead of Ekimus rushing toward you. Hall of Erosion Defend, it has now begun!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket FAILED_HEART_OF_INFINITY_DEFEND = new ExShowScreenMessage("You have failed at Heart of Infinity Defend... The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	protected static final L2GameServerPacket SUCCEEDED_HEART_OF_INFINITY_DEFEND = new ExShowScreenMessage("Congratulations! You have succeeded at Heart of Infinity Defend! The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);

	private final static TLongObjectHashMap<World> worlds = new TLongObjectHashMap<World>();
	private final ReentrantLock worldLock = new ReentrantLock();

	// NPC
	protected static final int TEPIOS = 32603;
	protected static final int TEPIOS_REWARD = 32530;
	protected static final int MOUTH_OF_EKIMUS = 32537;

	protected static final Location KLODEKUS_LOC = new Location(-173752, 217880, -9582, 31046);
	protected static final Location KLANIKUS_LOC = new Location(-173640, 218312, -9584, 35323);

	// Monsters
	protected static final int BOSS_MINION_SPAWN_TIME = 60000; // in milisex
	protected static final int[] TWIN_MOBIDS = { 22509, 22510, 22511, 22512, 22513 };
	protected static final int YEHAN_KLODEKUS = 25665;
	protected static final int YEHAN_KLANIKUS = 25666;
	protected static final int TUMOR_OF_DEATH = 18704;
	protected static final int DESTROYED_TUMOR = 18705;

	protected static final int[] MOBS = { 22509, 22510, 22511, 22512, 22513, 22514, 22515 };

	// Item rewards
	protected static final int MARK_OF_KEUCEREUS_STAGE_1 = 13691;
	protected static final int MARK_OF_KEUCEREUS_STAGE_2 = 13692;
	protected static final int SOE = 736; // Scroll of Escape
	protected static final int VESPER_STONE = 14052;

	protected static final int[] SUPPLIES = { 0, // NONE
			13777, // Jewel Ornamented Duel Supplies
			13778, // Mother-of-Pearl Ornamented Duel Supplies
			13779, // Gold-Ornamented Duel Supplies
			13780, // Silver-Ornamented Duel Supplies
			13781, // Bronze-Ornamented Duel Supplies
			13782, // Non-Ornamented Duel Supplies
			13783, // Weak-Looking Duel Supplies
			13784, // Sad-Looking Duel Supplies
			13785, // Poor-Looking Duel Supplies
			13786 // Worthless Duel Supplies
	};

	private final static int COLLAPSE_AFTER_DEATH_TIME = 5; // 5 мин

	public SeedOfInfinity(final int id, final int party)
	{
		super(id, party);
		addStartNpc(TEPIOS);
	}

	/**
	 * общий метод для иниацилизации инстанса
	 * 
	 * @param player
	 */
	public void initialInstance(final L2Player player)
	{}

	protected Reflection enterPartyInstance(final L2Player player, final int instId, final World world)
	{
		final InstanceManager ilm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> ils = ilm.getById(instId);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return null;
		}

		final Instance il = ils.get(0);
		assert il != null;

		final String name = il.getName();
		final int timelimit = il.getDuration();

		final Reflection r = new Reflection(name);
		r.setInstancedZoneId(instId);

		for(final Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());

			r.fillDoors(i.getDoors());
			r.fillSpawns(i.getSpawnsInfo());
		}

		world.instanceId = r.getId();
		world.status = 0;
		world.createNpcList();
		world.timer = System.currentTimeMillis() / 1000;

		worldLock.lock();
		try
		{
			worlds.put(r.getId(), world);
		}
		finally
		{
			worldLock.unlock();
		}

		for(final L2Player member : player.getParty().getPartyMembers())
		{
			member.setReflection(r);
			member.teleToLocation(il.getTeleportCoords());
			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.setVar(name, String.valueOf(System.currentTimeMillis()));
			member.sendPacket(new ExSendUIEvent(member, false, true, 0, timelimit * 60, ""));
		}

		player.getParty().setReflection(r);
		r.setParty(player.getParty());
		r.startCollapseTimer(timelimit * 60000L);

		return r;
	}

	protected Reflection enterCommandChannelInstance(final L2Player player, final int instId, final World world)
	{
		final InstanceManager ilm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> ils = ilm.getById(instId);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return null;
		}

		final Instance il = ils.get(0);
		assert il != null;

		final String name = il.getName();
		final int duration = il.getDuration();

		final L2CommandChannel commandChannel = player.getParty().getCommandChannel();

		final Reflection r = new Reflection(name);
		r.setInstancedZoneId(instId);

		for(final Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());

			r.fillDoors(i.getDoors());
			r.fillSpawns(i.getSpawnsInfo());
		}

		world.instanceId = r.getId();
		world.status = 0;
		world.createNpcList();
		world.timer = System.currentTimeMillis() / 1000;

		worldLock.lock();
		try
		{
			worlds.put(r.getId(), world);
		}
		finally
		{
			worldLock.unlock();
		}

		for(final L2Player member : commandChannel.getMembers())
		{
			member.setReflection(r);
			member.teleToLocation(il.getTeleportCoords());
			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.setVar(name, String.valueOf(System.currentTimeMillis()));
		}

		commandChannel.setReflection(r);
		r.setCommandChannel(commandChannel);
		r.startCollapseTimer(duration * 60000L);

		return r;
	}

	protected boolean checkCondition(final L2Player leader, final int id)
	{
		SystemMessage msg;
		if((msg = InstanceManager.checkCondition(id, leader, true, getName(), "_10268_ToTheSeedOfInfinity")) != null)
		{
			leader.sendPacket(msg);
			return false;
		}
		return true;
	}

	/**
	 * Проверка чтоб не обрабатывались несколько квестов, обрабатывается только тот который идёт
	 */
	protected boolean checkQuest(final QuestState qs)
	{
		final L2Player player = qs.getPlayer();
		return player != null && player.getVar("SeedOfInfinityQuest").equalsIgnoreCase(getName());
	}

	protected static QuestState getPlayerQuestState(final L2Player player, final String qn)
	{
		return player.getQuestState(qn);
	}

	protected static void showMessageToParty(final L2Party party, final String s)
	{
		if(party != null)
			party.broadcastToPartyMembers(new ExShowScreenMessage(s, 5000, ScreenMessageAlign.TOP_CENTER, false));
	}

	protected static void showMessageToCommandChannel(final L2CommandChannel commandChannel, final String s)
	{
		if(commandChannel != null)
			commandChannel.broadcastToChannelMembers(new ExShowScreenMessage(s, 5000, ScreenMessageAlign.TOP_CENTER, false));
	}

	protected static void showMessageToParty(final L2Party party, final L2GameServerPacket packet)
	{
		if(party != null)
			party.broadcastToPartyMembers(packet);
	}

	protected static void showMessageToCommandChannel(final L2CommandChannel commandChannel, final L2GameServerPacket packet)
	{
		if(commandChannel != null)
			commandChannel.broadcastToChannelMembers(packet);
	}

	protected static L2Party getParty(final L2Player player)
	{
		return player.getParty();
	}

	protected static L2Party getParty(final long reflectionId)
	{
		return getParty(ReflectionTable.getInstance().get(reflectionId));
	}

	protected static L2Party getParty(final Reflection reflection)
	{
		return reflection.getParty();
	}

	protected static L2CommandChannel getCommandChannel(final L2Player player)
	{
		return player.getParty() != null ? player.getParty().getCommandChannel() : null;
	}

	protected static L2CommandChannel getCommandChannel(final long reflectionId)
	{
		return getCommandChannel(ReflectionTable.getInstance().get(reflectionId));
	}

	protected static L2CommandChannel getCommandChannel(final Reflection reflection)
	{
		if(reflection == null)
			return null;
		final L2Party party = reflection.getParty();
		return party == null ? reflection.getCommandChannel() : party.getCommandChannel();
	}

	protected static void endInstance(final L2Player player)
	{
		player.getReflection().startCollapseTimer(COLLAPSE_AFTER_DEATH_TIME * 60 * 1000L); // запускаем 5 мин коллапс инстанса
		if(player.isInParty())
			player.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(5));
	}

	private World getWorld(final long reflectionId)
	{
		World world = null;
		worldLock.lock();
		try
		{
			world = worlds.get(reflectionId);
		}
		finally
		{
			worldLock.unlock();
		}
		return world;
	}

	protected HallofSufferingWorld getHallofSufferingWorld(final long reflectionId)
	{
		return (HallofSufferingWorld) getWorld(reflectionId);
	}

	protected HallofErosionWorld getHallofErosionWorld(final long reflectionId)
	{
		return (HallofErosionWorld) getWorld(reflectionId);
	}

	protected HeartOfInfinityWorld getHeartOfInfinityWorld(final long reflectionId)
	{
		return (HeartOfInfinityWorld) getWorld(reflectionId);
	}

	protected boolean checkKillProgress(final L2NpcInstance npc, final World world)
	{
		final Map<L2NpcInstance, Boolean> npclist = world.getNpcList();
		if(!npclist.isEmpty())
		{
			if(npclist.containsKey(npc))
				npclist.put(npc, true);
			for(final boolean dead : npclist.values())
				if(!dead)
					return false;
		}
		return true;
	}
}
