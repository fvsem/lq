package l2n.game.model.entity;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;


public class DelusionChamber extends DimensionalRift
{
	public DelusionChamber(final L2Party party, final int type, final int room)
	{
		super(party, type, room);
		// для Delusion Chamber (Great Seal) и Delusion Chamber (Tower of Seal)
		if(type == 11 || type == 12)
		{
			setEmptyDestroyTime(MILLISECONDS_IN_1_MINUTE); // зайти можно в течении 1 минуты
			startCollapseTimer(60 * MILLISECONDS_IN_1_MINUTE); // 1 час таймер
		}
		// остальных
		else
		{
			setEmptyDestroyTime(20 * MILLISECONDS_IN_1_MINUTE); // зайти можно в течении 20 минут
			startCollapseTimer(72 * MILLISECONDS_IN_1_MINUTE); // 72 минут таймер в нашм положении 9 комнат до РБ
		}
		setName("Delusion Chamber");
	}

	@Override
	protected void processTeleportList(final GArray<L2Player> teleport_list)
	{
		final GArray<L2Player> members = getParty().getPartyMembers();
		// удаляем одинаковые
		teleport_list.removeAll(members);

		// обрабатываем тех кого нету в пати, но есть в отражении
		for(final L2Player player : teleport_list)
		{
			if(player.getParty() != null)
			{
				if(equals(player.getParty().getReflection()))
					player.getParty().setReflection(null);

				if(player.getParty().getCommandChannel() != null && equals(player.getParty().getCommandChannel().getReflection()))
					player.getParty().getCommandChannel().setReflection(null);
			}

			if(equals(player.getReflection()))
				if(getReturnLoc() != null)
					player.teleToLocation(getReturnLoc(), 0);
				else
					player.setReflection(0);
		}

		for(final L2Player p : members)
			if(p.getReflection() == this)
			{
				final String var = p.getVar("backCoords");
				if(var == null)
					continue;
				if(var.equals(""))
					continue;
				p.teleToLocation(new Location(var), 0);
				p.unsetVar("backCoords");
			}
	}

	@Override
	public void partyMemberExited(final L2Player player)
	{
		if(getPlayersInside(false) >= 2 && getPlayersInside(true) != 0)
			return;
		createNewKillRiftTimer();
	}

	@Override
	public void manualExitRift(final L2Player player, final L2NpcInstance npc)
	{
		if(!player.isInParty() || player.getParty().getReflection() != this)
			return;
		if(player.getObjectId() != player.getParty().getPartyLeaderOID())
		{
			DimensionalRiftManager.getInstance().showHtmlFile(player, "data/html/rift/NotPartyLeader.htm", npc);
			return;
		}

		createNewKillRiftTimer();
	}

	public static final String getNameById(final int type)
	{
		switch (type)
		{
			case 7:
				return "Delusion Chamber, Eastern Seal";
			case 8:
				return "Delusion Chamber, Western Seal";
			case 9:
				return "Delusion Chamber, Southern Seal";
			case 10:
				return "Delusion Chamber, Northern Seal";
			case 11:
				return "Delusion Chamber, Great Seal";
			case 12:
				return "Delusion Chamber, Tower Seal";
			default:
				return "";
		}
	}

	@Override
	public String getName()
	{
		return getNameById(_roomType);
	}

	@Override
	protected int getManagerId()
	{
		return 32664;
	}

	@Override
	public boolean canChampions()
	{
		return false;
	}

	/** Если рефлекшн является залом илюзий */
	@Override
	public boolean isDelusionChamber()
	{
		return true;
	}
}
