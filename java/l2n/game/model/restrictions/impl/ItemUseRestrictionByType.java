package l2n.game.model.restrictions.impl;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.restrictions.IPlayerRestriction;
import l2n.game.model.restrictions.PlayerRestrictionType;
import l2n.game.templates.L2EtcItem.EtcItemType;

import java.util.StringTokenizer;

public class ItemUseRestrictionByType implements IPlayerRestriction
{
	private final long mask;

	public ItemUseRestrictionByType(final String itemTypes)
	{
		if(itemTypes == null || itemTypes.length() == 0)
		{
			mask = 0;
			return;
		}

		long mask = 0;
		StringTokenizer st = new StringTokenizer(itemTypes, ",");
		while (st.hasMoreTokens())
		{
			String item = st.nextToken().trim();
			for(EtcItemType at : EtcItemType.values())
				if(at.toString().equals(item))
					mask |= at.mask();
		}

		this.mask = mask;
	}

	@Override
	public boolean test(final L2Character actor, final L2Character target, final L2Skill skill, final L2ItemInstance item)
	{
		if(actor == null || item == null)
			return true;
		return (mask & item.getItem().getItemMask()) == 0;
	}

	@Override
	public PlayerRestrictionType getType()
	{
		return PlayerRestrictionType.ITEM_USE;
	}
}
