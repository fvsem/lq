package l2n.game.model.restrictions.impl;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.restrictions.IPlayerRestriction;
import l2n.game.model.restrictions.PlayerRestrictionType;
import l2n.util.ArrayUtil;

public class ItemUseRestrictionById implements IPlayerRestriction
{
	private final int[] item_ids;

	public ItemUseRestrictionById(final int... ids)
	{
		item_ids = ids;
	}

	@Override
	public boolean test(final L2Character actor, final L2Character target, final L2Skill skill, final L2ItemInstance item)
	{
		if(item == null)
			return true;
		return ArrayUtil.arrayFirstIndexOf(item_ids, item.getItemId()) < 0;
	}

	@Override
	public PlayerRestrictionType getType()
	{
		return PlayerRestrictionType.ITEM_USE;
	}
}
