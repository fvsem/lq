package l2n.game.model.restrictions;

public enum PlayerRestrictionType
{
	SKILL_USE, // использование любого скила
	ITEM_USE, // использование предметов
	ATTACK_TARGET, // физ атака цели
	CAST_TARGET, // атака скилом
	BUFF_TARGET; // баф

	private final int _mask;

	private PlayerRestrictionType()
	{
		_mask = 1 << ordinal();
	}

	public final int mask()
	{
		return _mask;
	}
}
