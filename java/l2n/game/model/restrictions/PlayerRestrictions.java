package l2n.game.model.restrictions;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;

import java.util.concurrent.ConcurrentLinkedQueue;

public class PlayerRestrictions
{
	private ConcurrentLinkedQueue<IPlayerRestriction>[] restrictions;

	public PlayerRestrictions()
	{}

	@SuppressWarnings("unchecked")
	public void addRestriction(final IPlayerRestriction restriction)
	{
		if(restriction == null)
			return;
		if(restrictions == null)
			restrictions = new ConcurrentLinkedQueue[PlayerRestrictionType.values().length];

		final int ordinal = restriction.getType().ordinal();
		ConcurrentLinkedQueue<IPlayerRestriction> restrictionsByType = restrictions[ordinal];
		if(restrictionsByType == null)
		{
			restrictionsByType = new ConcurrentLinkedQueue<IPlayerRestriction>();
			restrictions[ordinal] = restrictionsByType;
		}

		restrictionsByType.add(restriction);
	}

	public void removeRestriction(final IPlayerRestriction restriction)
	{
		if(restrictions == null || restriction == null)
			return;

		final int ordinal = restriction.getType().ordinal();
		final ConcurrentLinkedQueue<IPlayerRestriction> restrictionsByType = restrictions[ordinal];
		if(restrictionsByType == null)
			return;

		restrictionsByType.remove(restriction);
	}

	/**
	 * проверяем все условия определённого типа
	 * 
	 * @param actor
	 * @param target
	 * @param skill
	 * @param item
	 * @return
	 */
	public final boolean test(final PlayerRestrictionType type, final L2Character actor, final L2Character target, final L2Skill skill, final L2ItemInstance item)
	{
		if(restrictions == null)
			return true;

		final int ordinal = type.ordinal();
		final ConcurrentLinkedQueue<IPlayerRestriction> restrictionsByType = restrictions[ordinal];
		if(restrictionsByType == null)
			return true;

		for(final IPlayerRestriction restriction : restrictionsByType)
		{
			if(!restriction.test(actor, target, skill, item))
				return false;
		}
		return true;
	}

	public final boolean isEmpty()
	{
		return restrictions == null;
	}
}
