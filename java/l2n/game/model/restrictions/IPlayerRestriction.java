package l2n.game.model.restrictions;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;

public interface IPlayerRestriction
{
	public boolean test(final L2Character actor, final L2Character target, final L2Skill skill, final L2ItemInstance item);

	public PlayerRestrictionType getType();
}
