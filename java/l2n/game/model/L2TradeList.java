package l2n.game.model;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.math.SafeMath;
import l2n.extensions.Stat;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;
import l2n.util.Util;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class L2TradeList
{
	private final GArray<L2ItemInstance> _items = new GArray<L2ItemInstance>();
	private final int _listId;
	private String _buyStoreName;
	private String _sellStoreName;

	public L2TradeList(final int listId)
	{
		_listId = listId;
	}

	public L2TradeList()
	{
		this(0);
	}

	public void addItem(final L2ItemInstance item)
	{
		synchronized (_items)
		{
			_items.add(item);
		}
	}

	public void removeAll()
	{
		_items.clear();
	}

	public int getListId()
	{
		return _listId;
	}

	public void setSellStoreName(final String name)
	{
		_sellStoreName = name;
	}

	public String getSellStoreName()
	{
		return _sellStoreName;
	}

	public void setBuyStoreName(final String name)
	{
		_buyStoreName = name;
	}

	public String getBuyStoreName()
	{
		return _buyStoreName;
	}

	public GArray<L2ItemInstance> getItems()
	{
		return _items;
	}

	public static boolean validateTrade(final L2Player player, final Collection<TradeItem> items)
	{
		final Inventory playersInv = player.getInventory();
		L2ItemInstance playerItem;
		synchronized (items)
		{
			for(final TradeItem item : items)
			{
				playerItem = playersInv.getItemByObjectId(item.getObjectId());
				if(playerItem == null || playerItem.getCount() < item.getCount() || playerItem.getEnchantLevel() < item.getEnchantLevel() || !playerItem.canBeTraded(player))
					return false;
			}

		}
		return true;
	}

	public void updateSellList(final L2Player player, final ConcurrentLinkedQueue<TradeItem> list)
	{
		final Inventory playersInv = player.getInventory();
		L2ItemInstance item;
		for(final L2ItemInstance temp : _items)
		{
			item = playersInv.getItemByObjectId(temp.getObjectId());
			if(item == null || item.getCount() <= 0)
			{
				for(final TradeItem i : list)
					if(i.getObjectId() == temp.getItemId())
					{
						list.remove(i);
						break;
					}
			}
			else if(item.getCount() < temp.getCount())
				temp.setCount(item.getCount());
		}
	}

	public synchronized boolean buySellItems(final L2Player buyer, final ConcurrentLinkedQueue<TradeItem> listToBuy, final L2Player seller, final ConcurrentLinkedQueue<TradeItem> listToSell)
	{
		final Inventory sellerInv = seller.getInventory();
		final Inventory buyerInv = buyer.getInventory();

		TradeItem sellerTradeItem = null;
		L2ItemInstance sellerInventoryItem = null;
		L2ItemInstance transferItem = null;
		L2ItemInstance temp;
		final ConcurrentLinkedQueue<TradeItem> unsold = new ConcurrentLinkedQueue<TradeItem>();
		unsold.addAll(listToSell);

		long cost = 0, amount = 0;

		for(final TradeItem buyerTradeItem : listToBuy)
		{
			sellerTradeItem = null;

			for(final TradeItem unsoldItem : unsold)
				if(unsoldItem.getItemId() == buyerTradeItem.getItemId() && unsoldItem.getOwnersPrice() == buyerTradeItem.getOwnersPrice())
				{
					sellerTradeItem = unsoldItem;
					break;
				}

			if(sellerTradeItem == null)
				continue;

			sellerInventoryItem = sellerInv.getItemByObjectId(sellerTradeItem.getObjectId());

			unsold.remove(sellerTradeItem);

			if(sellerInventoryItem == null)
				continue;

			long buyerItemCount = buyerTradeItem.getCount();
			long sellerItemCount = sellerTradeItem.getCount();

			if(sellerItemCount > sellerInventoryItem.getCount())
				sellerItemCount = sellerInventoryItem.getCount();

			if(seller.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL || seller.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
			{
				if(buyerItemCount > sellerItemCount)
					buyerTradeItem.setCount(sellerItemCount);
				if(buyerItemCount > sellerInventoryItem.getCount())
					buyerTradeItem.setCount(sellerInventoryItem.getCount());
				buyerItemCount = buyerTradeItem.getCount();
				amount = buyerItemCount;
				cost = amount * sellerTradeItem.getOwnersPrice();
			}

			if(buyer.getPrivateStoreType() == L2Player.STORE_PRIVATE_BUY)
			{
				if(sellerItemCount > buyerItemCount)
					sellerTradeItem.setCount(buyerItemCount);
				if(sellerItemCount > sellerInventoryItem.getCount())
					sellerTradeItem.setCount(sellerInventoryItem.getCount());
				sellerItemCount = sellerTradeItem.getCount();
				amount = sellerItemCount;
				cost = amount * buyerTradeItem.getOwnersPrice();
			}

			if(buyer.getAdena() < cost || cost < 0)
				return false;

			if(amount <= 0)
			{
				Util.handleIllegalPlayerAction(seller, "L2TradeList.buySellItems[204]", "try use trade bug", l2n.util.IllegalPlayerAction.WARNING);
				return false;
			}

			try
			{
				SafeMath.safeMulLong(buyerItemCount, buyerTradeItem.getOwnersPrice());
				SafeMath.safeMulLong(sellerItemCount, sellerTradeItem.getOwnersPrice());
			}
			catch(final ArithmeticException e)
			{
				return false;
			}

			transferItem = sellerInv.dropItem(sellerInventoryItem.getObjectId(), amount);
			Log.LogItem(seller, buyer, Log.PrivateStoreSell, transferItem, amount);
			buyer.reduceAdena(cost, true);
			seller.addAdena(cost);

			int tax = (int) (cost * Config.SERVICES_TRADE_TAX / 100);
			
			if(Config.SERVICES_TRADE_TAX_ONLY_OFFLINE && !seller.isInOfflineMode())
				tax = 0;
			if(tax > 0)
			{
				seller.reduceAdena(tax, true);
				Stat.addTax(tax);
				seller.sendMessage(new CustomMessage("trade.HavePaidTax", seller).addNumber(tax));
			}

			temp = buyerInv.addItem(transferItem);
			Log.LogItem(buyer, seller, Log.PrivateStoreBuy, transferItem, amount);

			if(!temp.isStackable())
			{
				if(temp.getEnchantLevel() > 0)
				{
					seller.sendPacket(new SystemMessage(SystemMessage._S2S3_HAS_BEEN_SOLD_TO_S1_AT_THE_PRICE_OF_S4_ADENA).addString(buyer.getName()).addNumber(temp.getEnchantLevel()).addItemName(sellerInventoryItem.getItemId()).addNumber((int) cost));
					buyer.sendPacket(new SystemMessage(SystemMessage._S2S3_HAS_BEEN_PURCHASED_FROM_S1_AT_THE_PRICE_OF_S4_ADENA).addString(seller.getName()).addNumber(temp.getEnchantLevel()).addItemName(sellerInventoryItem.getItemId()).addNumber((int) cost));
				}
				else
				{
					seller.sendPacket(new SystemMessage(SystemMessage.S2_IS_SOLD_TO_S1_AT_THE_PRICE_OF_S3_ADENA).addString(buyer.getName()).addItemName(sellerInventoryItem.getItemId()).addNumber((int) cost));
					buyer.sendPacket(new SystemMessage(SystemMessage.S2_HAS_BEEN_PURCHASED_FROM_S1_AT_THE_PRICE_OF_S3_ADENA).addString(seller.getName()).addItemName(sellerInventoryItem.getItemId()).addNumber((int) cost));
				}
			}
			else
			{
				seller.sendPacket(new SystemMessage(SystemMessage.S2_S3_HAVE_BEEN_SOLD_TO_S1_FOR_S4_ADENA).addString(buyer.getName()).addNumber((int) amount).addItemName(sellerInventoryItem.getItemId()).addNumber((int) cost));
				buyer.sendPacket(new SystemMessage(SystemMessage.S3_S2_HAS_BEEN_PURCHASED_FROM_S1_FOR_S4_ADENA).addString(seller.getName()).addItemName(sellerInventoryItem.getItemId()).addNumber((int) amount).addNumber((int) cost));
			}

			sellerInventoryItem = null;
		}

		seller.sendChanges();
		buyer.sendChanges();

		updateSellerSellList(listToBuy, seller, listToSell);
		updateBuyerBuyList(buyer, listToBuy, listToSell);
		return true;
	}

	private void updateBuyerBuyList(final L2Player buyer, final ConcurrentLinkedQueue<TradeItem> listToBuy, final ConcurrentLinkedQueue<TradeItem> listToSell)
	{
		GArray<TradeItem> tmp;
		if(buyer.getPrivateStoreType() == L2Player.STORE_PRIVATE_BUY)
		{
			tmp = new GArray<TradeItem>();
			tmp.addAll(buyer.getBuyList());

			outer: for(final TradeItem bl : listToBuy)
				for(final TradeItem sl : listToSell)
					if(sl.getItemId() == bl.getItemId() && sl.getOwnersPrice() == bl.getOwnersPrice())
					{
						if(!ItemTable.getInstance().getTemplate(bl.getItemId()).isStackable())
							tmp.remove(bl);
						else
						{
							bl.setCount(bl.getCount() - sl.getCount());
							if(bl.getCount() <= 0)
								tmp.remove(bl);
						}
						listToSell.remove(sl);
						continue outer;
					}

			final ConcurrentLinkedQueue<TradeItem> newlist = new ConcurrentLinkedQueue<TradeItem>();
			newlist.addAll(tmp);
			buyer.setBuyList(newlist);
		}
	}

	private void updateSellerSellList(final ConcurrentLinkedQueue<TradeItem> listToBuy, final L2Player seller, final ConcurrentLinkedQueue<TradeItem> listToSell)
	{
		if(seller.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
		{
			seller.setSellList(null);
			return;
		}

		GArray<TradeItem> tmp;
		if(seller.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL)
		{
			tmp = new GArray<TradeItem>();
			tmp.addAll(seller.getSellList());

			for(final TradeItem sl : listToSell)
				for(final TradeItem bl : listToBuy)
					if(sl.getItemId() == bl.getItemId() && sl.getOwnersPrice() == bl.getOwnersPrice())
					{
						final L2ItemInstance inst = seller.getInventory().getItemByObjectId(sl.getObjectId());
						if(inst == null || inst.getCount() <= 0)
						{
							tmp.remove(sl);
							continue;
						}

						if(inst.isStackable())
						{
							sl.setCount(sl.getCount() - bl.getCount());
							if(sl.getCount() <= 0)
							{
								tmp.remove(sl);
								continue;
							}
							if(inst.getCount() < sl.getCount())
								sl.setCount(inst.getCount());
						}
					}

			final ConcurrentLinkedQueue<TradeItem> newlist = new ConcurrentLinkedQueue<TradeItem>();
			newlist.addAll(tmp);
			seller.setSellList(newlist);
		}
	}

	public static boolean validateList(final L2Player player)
	{
		if(player.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL || player.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
		{
			final ConcurrentLinkedQueue<TradeItem> selllist = player.getSellList();
			for(final TradeItem tl : selllist)
			{
				final L2ItemInstance inst = player.getInventory().getItemByObjectId(tl.getObjectId());
				if(inst == null || inst.getCount() <= 0)
				{
					if(player.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
					{
						cancelStore(player);
						return false;
					}
					selllist.remove(tl);
					continue;
				}

				if(inst.isStackable() && inst.getCount() < tl.getCount())
				{
					if(player.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
					{
						cancelStore(player);
						return false;
					}
					tl.setCount(inst.getCount());
				}
			}
		}
		else if(player.getPrivateStoreType() == L2Player.STORE_PRIVATE_BUY)
		{
			final ConcurrentLinkedQueue<TradeItem> buylist = player.getBuyList();

			long sum = 0;
			for(final TradeItem tl : buylist)
				sum += tl.getOwnersPrice() * tl.getCount();

			if(player.getAdena() < sum)
			{
				cancelStore(player);
				return false;
			}
		}
		return true;
	}

	public static void cancelStore(final L2Player activeChar)
	{
		activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
		activeChar.getBuyList().clear();
		if(activeChar.isInOfflineMode() && Config.SERVICES_OFFLINE_TRADE_KICK_NOT_TRADING)
		{
			activeChar.setOfflineMode(false);
			activeChar.logout(false, false, false, true);
			if(activeChar.getNetConnection() != null)
				activeChar.getNetConnection().disconnectOffline();
		}
		else
			activeChar.broadcastUserInfo(true);
	}
}
