package l2n.game.loginservercon.send;

public class BanIP extends GameServerBasePacket
{
	public BanIP(String ip, String admin)
	{
		writeC(0x07);
		writeS(ip);
		writeS(admin);
	}
}
