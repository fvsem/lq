package l2n.game.loginservercon.send;

public class LockAccountHWID extends GameServerBasePacket
{
	public LockAccountHWID(String account, String HWID)
	{
		writeC(0x0c);
		writeS(account);
		writeS(HWID);
	}
}
