package l2n.game.loginservercon.send;

public class ChangePassword extends GameServerBasePacket
{
	public ChangePassword(String account, String oldPass, String newPass)
	{
		writeC(0x08);
		writeS(account);
		writeS(oldPass);
		writeS(newPass);
	}
}
