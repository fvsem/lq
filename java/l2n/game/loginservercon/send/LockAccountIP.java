package l2n.game.loginservercon.send;

public class LockAccountIP extends GameServerBasePacket
{
	public LockAccountIP(String account, String IP)
	{
		writeC(0x0b);
		writeS(account);
		writeS(IP);
	}
}
