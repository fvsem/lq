package l2n.game.loginservercon.send;

import l2n.commons.list.GArray;
import l2n.game.loginservercon.Attribute;

public class ServerStatus extends GameServerBasePacket
{
	public ServerStatus(GArray<Attribute> attributes)
	{
		writeC(0x06);
		writeD(attributes.size());
		for(Attribute temp : attributes)
		{
			writeD(temp.id);
			writeD(temp.value);
		}

		attributes.clear();
	}
}
