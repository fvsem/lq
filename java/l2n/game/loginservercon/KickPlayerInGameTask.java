package l2n.game.loginservercon;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.L2GameClient;

public class KickPlayerInGameTask implements Runnable
{
	private final L2GameClient client;

	public KickPlayerInGameTask(L2GameClient client)
	{
		this.client = client;
	}

	@Override
	public void run()
	{
		L2Player activeChar = client.getActiveChar();

		if(activeChar != null)
			activeChar.logout(false, false, true, false);
		else
		{
			client.sendPacket(Msg.ServerClose);
			client.closeNow(false);
		}
	}
}
