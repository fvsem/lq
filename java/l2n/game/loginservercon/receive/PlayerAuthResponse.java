package l2n.game.loginservercon.receive;

import l2n.commons.util.StatsSet;
import l2n.extensions.Stat;
import l2n.game.L2GameThreadPools;
import l2n.game.loginservercon.AttLS;
import l2n.game.loginservercon.KickWaitingClientTask;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.SessionKey;
import l2n.game.loginservercon.send.PlayerInGame;
import l2n.game.loginservercon.send.PlayerLogout;
import l2n.game.network.L2GameClient;
import l2n.game.network.L2GameClient.GameClientState;
import l2n.game.network.serverpackets.CharacterSelectionInfo;
import l2n.game.network.serverpackets.LoginFail;

import java.util.logging.Logger;

public class PlayerAuthResponse extends LoginServerBasePacket
{
	private static final Logger _log = Logger.getLogger(PlayerAuthResponse.class.getName());

	public PlayerAuthResponse(byte[] decrypt, AttLS loginserver)
	{
		super(decrypt, loginserver);
	}

	@Override
	public void read()
	{
		String account = readS();
		boolean authed = readC() == 1;
		int playOkId1 = readD();
		int playOkId2 = readD();
		int loginOkId1 = readD();
		int loginOkId2 = readD();
		String account_fields = readS();

		L2GameClient client = getLoginServer().getCon().removeWaitingClient(account);
		if(client != null)
		{
			if(client.getState() != L2GameClient.GameClientState.CONNECTED)
			{
				_log.severe("Trying to authd allready authed client.");
				client.closeNow(true);
				return;
			}

			if(client.getLoginName() == null || client.getLoginName().isEmpty())
			{
				_log.warning("PlayerAuthResponse: empty accname for " + client);
				client.closeNow(true);
				return;
			}

			SessionKey key = client.getSessionId();

			if(authed)
				if(getLoginServer().isLicenseShown())
					authed = key.playOkID1 == playOkId1 && key.playOkID2 == playOkId2 && key.loginOkID1 == loginOkId1 && key.loginOkID2 == loginOkId2;
				else
					authed = key.playOkID1 == playOkId1 && key.playOkID2 == playOkId2;

			if(authed)
			{
				client.setState(GameClientState.AUTHED);

				StatsSet accountfields = StatsSet.unserialize(account_fields);
				client.setBonus(accountfields.getDouble("bonus", 1.));
				client.setBonusExpire(accountfields.getLong("bonus_expire", 0));
				client.setAllowHWIDs(accountfields.getString("allow_hwid", null));

				getLoginServer().getCon().addAccountInGame(client);
				CharacterSelectionInfo csi = new CharacterSelectionInfo(client.getLoginName(), client.getSessionId().playOkID1);
				client.sendPacket(csi);
				client.setCharSelection(csi.getCharInfo());
				sendPacket(new PlayerInGame(client.getLoginName(), Stat.getOnline(true)));
			}
			else
			{
				// log.severe("Cheater? SessionKey invalid! Login: " + client.getLoginName() + ", IP: " + client.getIpAddr());
				client.sendPacket(new LoginFail(LoginFail.INCORRECT_ACCOUNT_INFO_CONTACT_CUSTOMER_SUPPORT));
				L2GameThreadPools.getInstance().scheduleGeneral(new KickWaitingClientTask(client), 1000);
				LSConnection.getInstance().sendPacket(new PlayerLogout(client.getLoginName()));
				LSConnection.getInstance().removeAccount(client);
			}
		}
	}
}
