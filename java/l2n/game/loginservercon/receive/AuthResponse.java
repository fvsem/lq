package l2n.game.loginservercon.receive;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.Stat;
import l2n.game.loginservercon.AttLS;
import l2n.game.loginservercon.Attribute;
import l2n.game.loginservercon.send.PlayerInGame;
import l2n.game.loginservercon.send.PlayersInGame;
import l2n.game.loginservercon.send.ServerStatus;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

public class AuthResponse extends LoginServerBasePacket
{
	private static final Logger log = Logger.getLogger(AuthResponse.class.getName());

	private int _serverId;
	private String _serverName;

	public AuthResponse(byte[] decrypt, AttLS loginServer)
	{
		super(decrypt, loginServer);
	}

	@Override
	public void read()
	{
		_serverId = readC();
		_serverName = readS();
		getLoginServer().setLicenseShown(readC() == 1);
		try
		{
			getLoginServer().setProtocolVersion(readH());
		}
		catch(Exception e)
		{
			getLoginServer().setProtocolVersion(0);
		}

		log.info("Registered on login as Server " + _serverId + " : " + _serverName);
		Config.REQUEST_ID = _serverId;

		GArray<Attribute> attributes = new GArray<Attribute>();

		attributes.add(new Attribute(Attribute.SERVER_LIST_SQUARE_BRACKET, Config.SERVER_LIST_BRACKET ? Attribute.ON : Attribute.OFF));
		attributes.add(new Attribute(Attribute.SERVER_LIST_CLOCK, Config.SERVER_LIST_CLOCK ? Attribute.ON : Attribute.OFF));
		attributes.add(new Attribute(Attribute.TEST_SERVER, Config.SERVER_LIST_TESTSERVER ? Attribute.ON : Attribute.OFF));
		attributes.add(new Attribute(Attribute.SERVER_LIST_STATUS, Config.SERVER_GMONLY ? Attribute.STATUS_GM_ONLY : Attribute.STATUS_AUTO));

		getLoginServer().setAuthResponsed(true);
		sendPacket(new ServerStatus(attributes));

		if(L2ObjectsStorage.getAllPlayersCount() > 0)
		{
			GArray<String> playerList = new GArray<String>();
			for(L2Player player : L2ObjectsStorage.getAllPlayers())
			{
				if(player.isInOfflineMode())
					continue;
				if(player.getAccountName() == null || player.getAccountName().isEmpty())
				{
					log.warning("AuthResponse: empty accname for " + player);
					continue;
				}
				playerList.add(player.getAccountName());
				getLoginServer().getCon().addAccountInGame(player.getNetConnection());
			}

			int online = Stat.getOnline(true);

			sendPacket(new PlayerInGame(null, online));
			if(getLoginServer().getProtocolVersion() > 1)
				sendPackets(PlayersInGame.makePlayersInGame(online, playerList));
			else
				for(String name : playerList)
					sendPacket(new PlayerInGame(name, online));
		}
	}
}
