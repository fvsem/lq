package l2n.game.loginservercon;

import l2n.game.network.L2GameClient;

public class KickWaitingClientTask implements Runnable
{
	private final L2GameClient client;

	public KickWaitingClientTask(L2GameClient client)
	{
		this.client = client;
	}

	@Override
	public void run()
	{
		client.closeNow(false);
	}
}
