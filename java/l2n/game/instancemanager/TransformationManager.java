package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;
import l2n.game.scripts.transformations.*;
import l2n.game.scripts.transformations.battle.*;
import l2n.game.scripts.transformations.cursed.Akamanah;
import l2n.game.scripts.transformations.cursed.Zariche;
import l2n.game.scripts.transformations.divine.*;
import l2n.game.scripts.transformations.event.*;
import l2n.game.scripts.transformations.flying.AurabirdFalcon;
import l2n.game.scripts.transformations.flying.AurabirdOwl;
import l2n.game.scripts.transformations.flying.FlyingFinalForm;
import l2n.game.scripts.transformations.fusion.DemonRace;
import l2n.game.scripts.transformations.fusion.MyoRace;
import l2n.game.scripts.transformations.fusion.Unicorniun;
import l2n.game.scripts.transformations.mercenary.*;
import l2n.game.scripts.transformations.monster.*;
import l2n.game.scripts.transformations.mountable.JetBike;
import l2n.game.scripts.transformations.mountable.LightPurpleManedHorse;
import l2n.game.scripts.transformations.mountable.SteamBeatle;
import l2n.game.scripts.transformations.mountable.TawnyManedLion;
import l2n.game.scripts.transformations.normal.*;
import l2n.game.scripts.transformations.raidboss.*;
import l2n.game.scripts.transformations.stance.*;
import l2n.game.scripts.transformations.strong.*;
import l2n.game.scripts.transformations.weak.*;

import java.util.logging.Logger;

/**
 * @author L2System Project<BR>
 * <BR>
 *         1 | Onyx Beast<BR>
 *         2 | Doom Wraith<BR>
 *         3 | Heretic<BR>
 *         4 | Vale Master<BR>
 *         5 | Saber Tooth Tiger<BR>
 *         6 | Oel Mahum<BR>
 *         7 | Doll Blader <BR>
 *         8 | Aurabird Falcon<BR>
 *         9 | Aurabird Owl<BR>
 *         10 | Human Mercenary<BR>
 *         11 | Elf Mercenary<BR>
 *         12 | Dark Elf Mercenary<BR>
 *         13 | Orc Mercenary<BR>
 *         14 | Dwarf Mercenary<BR>
 *         15 | Kamael Mercenary<BR>
 *         16 | Royal Guard Captain<BR>
 *         17 | Archer Captain<BR>
 *         18 | Magic Leader<BR>
 *         19 | Kamael Guard Captain<BR>
 *         20 | Knight of Dawn <BR>
 *         21 | Fortress Captain<BR>
 *         101 | Hellbound Native<BR>
 *         102 | Yeti<BR>
 *         103 | Buffalo<BR>
 *         104 | Pig<BR>
 *         105 | Rabbit<BR>
 *         106 | Light Purple Maned Horse (Mountable)<BR>
 *         107 | Teleporter 2<BR>
 *         108 | Pumpkin Ghost<BR>
 *         109 | Tawny Maned Lion (Mountable)<BR>
 *         110 | Steam Beattle (Mountable)<BR>
 *         111 | Frog<BR>
 *         112 | Young Child<BR>
 *         113 | Guards of the Dawn<BR>
 *         114 | Snow Kung<BR>
 *         115 | Scarecrow<BR>
 *         116 | Tin Golem<BR>
 *         117 | Tow<BR>
 *         118 | Lure Tow<BR>
 *         119 | Heavy Tow<BR>
 *         120 | Wing Tow<BR>
 *         121 | Red Elf<BR>
 *         122 | Blue Elf<BR>
 *         123 | Guard Strider (Missing client support)<BR>
 *         124 | Hellbound Native 2<BR>
 *         125 | Aqua Elf<BR>
 *         201 | Grail Apostle Strong<BR>
 *         202 | Grail Apostle Normal<BR>
 *         203 | Grail Apostle Weak<BR>
 *         204 | Unicorn Strong<BR>
 *         205 | Unicorn Normal<BR>
 *         206 | Unicorn Weak<BR>
 *         207 | Lilim Knight Strong<BR>
 *         208 | Lilim Knight Normal<BR>
 *         209 | Lilim Knight Weak<BR>
 *         210 | Golem Guardian Strong<BR>
 *         211 | Golem Guardian Normal<BR>
 *         212 | Golem Guardian Weak<BR>
 *         213 | Inferno Drake Strong<BR>
 *         214 | Inferno Drake Normal<BR>
 *         215 | Inferno Drake Weak<BR>
 *         216 | Dragon Bomber Strong<BR>
 *         217 | Dragon Bomber Normal<BR>
 *         218 | Dragon Bomber Weak<BR>
 *         219 | Cat Fusion<BR>
 *         220 | Unicorn Fusion<BR>
 *         221 | Phantom Fusion<BR>
 *         251 | Kamael<BR>
 *         252 | Divine Knight<BR>
 *         253 | Divine Warrior<BR>
 *         254 | Divine Rogue<BR>
 *         255 | Divine Healer<BR>
 *         256 | Divine Wizard<BR>
 *         257 | Divine Enchanter<BR>
 *         258 | Divine Summoner<BR>
 *         259 | Dwarf Golem<BR>
 *         260 | Final Flying Form<BR>
 *         301 | Zariche<BR>
 *         302 | Akamanah<BR>
 *         303 | Zombie<BR>
 *         304 | Pixy<BR>
 *         305 | Zaken<BR>
 *         306 | Anakim<BR>
 *         307 | Benom<BR>
 *         308 | Gordon<BR>
 *         309 | Ranku<BR>
 *         310 | Kechi<BR>
 *         311 | Demon Prince<BR>
 *         312 | Vanguard Paladin<BR>
 *         313 | Vanguard Dark Avenger<BR>
 *         314 | Vanguard Temple Knight<BR>
 *         315 | Vanguard Shilien Knight<BR>
 *         316 | Inquisitor Bishop<BR>
 *         317 | Inquisitor Elven Elder<BR>
 *         318 | Inquisitor Shilien Elder<BR>
 *         319 | Teleporter<BR>
 *         320 | Grizzly Bear<BR>
 *         321 | Timitran<BR>
 *         322 | Lava Golem<BR>
 *         323 | Yeti2<BR>
 *         20000 | Kadomas<BR>
 *         20001 | Jet Bike (Mountable)<BR>
 */
public class TransformationManager
{
	protected static final Logger _log = Logger.getLogger(TransformationManager.class.getName());

	private static TransformationManager _instance;

	private final TIntObjectHashMap<L2Transformation> _transformations;

	public static TransformationManager getInstance()
	{
		if(_instance == null)
			_instance = new TransformationManager();
		return _instance;
	}

	private TransformationManager()
	{
		_transformations = new TIntObjectHashMap<L2Transformation>();
		registerTransformation(new Akamanah());
		registerTransformation(new Anakim());
		registerTransformation(new AquaElf());
		registerTransformation(new ArcherCaptain());
		registerTransformation(new AurabirdFalcon());
		registerTransformation(new AurabirdOwl());
		registerTransformation(new Benom());
		registerTransformation(new Buffalo());
		registerTransformation(new DarkElfMercenary());
		registerTransformation(new DemonPrince());
		registerTransformation(new DemonRace());
		registerTransformation(new DivineEnchanter());
		registerTransformation(new DivineHealer());
		registerTransformation(new DivineKnight());
		registerTransformation(new DivineRogue());
		registerTransformation(new DivineSummoner());
		registerTransformation(new DivineWarrior());
		registerTransformation(new DivineWizard());
		registerTransformation(new DollBlader());
		registerTransformation(new DoomWraith());
		registerTransformation(new DragonBomberNormal());
		registerTransformation(new DragonBomberStrong());
		registerTransformation(new DragonBomberWeak());
		registerTransformation(new DwarfGolem());
		registerTransformation(new DwarfMercenary());
		registerTransformation(new ElfMercenary());
		registerTransformation(new FlyingFinalForm());
		registerTransformation(new FortressCaptain());
		registerTransformation(new EpicQuestFrog());
		registerTransformation(new GolemGuardianNormal());
		registerTransformation(new GolemGuardianStrong());
		registerTransformation(new GolemGuardianWeak());
		registerTransformation(new Gordon());
		registerTransformation(new GrailApostleNormal());
		registerTransformation(new GrailApostleStrong());
		registerTransformation(new GrailApostleWeak());
		registerTransformation(new GrizzlyBear());
		registerTransformation(new GuardianStrider());
		registerTransformation(new GuardsoftheDawn());
		registerTransformation(new HeavyTow());
		registerTransformation(new Heretic());
		registerTransformation(new HumanMercenary());
		registerTransformation(new InfernoDrakeNormal());
		registerTransformation(new InfernoDrakeStrong());
		registerTransformation(new InfernoDrakeWeak());
		registerTransformation(new InquisitorBishop());
		registerTransformation(new InquisitorElvenElder());
		registerTransformation(new InquisitorShilienElder());
		registerTransformation(new JetBike());
		registerTransformation(new Kadomas());
		registerTransformation(new Kamael());
		registerTransformation(new KamaelGuardCaptain());
		registerTransformation(new KamaelMercenary());
		registerTransformation(new Kiyachi());
		registerTransformation(new KnightofDawn());
		registerTransformation(new LavaGolem());
		registerTransformation(new LightPurpleManedHorse());
		registerTransformation(new LilimKnightNormal());
		registerTransformation(new LilimKnightStrong());
		registerTransformation(new LilimKnightWeak());
		registerTransformation(new LureTow());
		registerTransformation(new MagicLeader());
		registerTransformation(new MyoRace());
		registerTransformation(new Native());
		registerTransformation(new EpicQuestNative());
		registerTransformation(new OlMahum());
		registerTransformation(new OnyxBeast());
		registerTransformation(new OrcMercenary());
		registerTransformation(new Pig());
		registerTransformation(new Pixy());
		registerTransformation(new PumpkinGhost());
		registerTransformation(new Rabbit());
		registerTransformation(new Ranku());
		registerTransformation(new RoyalGuardCaptain());
		registerTransformation(new SaberToothTiger());
		registerTransformation(new Scarecrow());
		registerTransformation(new ScrollBlue());
		registerTransformation(new ScrollRed());
		registerTransformation(new SnowKung());
		registerTransformation(new SteamBeatle());
		registerTransformation(new TawnyManedLion());
		registerTransformation(new Teleporter());
		registerTransformation(new Teleporter2());
		registerTransformation(new Timitran());
		registerTransformation(new TinGolem());
		registerTransformation(new Tow());
		registerTransformation(new Unicorniun());
		registerTransformation(new UnicornNormal());
		registerTransformation(new UnicornStrong());
		registerTransformation(new UnicornWeak());
		registerTransformation(new ValeMaster());
		registerTransformation(new VanguardDarkAvenger());
		registerTransformation(new VanguardPaladin());
		registerTransformation(new VanguardShilienKnight());
		registerTransformation(new VanguardTempleKnight());
		registerTransformation(new WingTow());
		registerTransformation(new Yeti());
		registerTransformation(new Yeti2());
		registerTransformation(new EpicQuestChild());
		registerTransformation(new Zaken());
		registerTransformation(new Zariche());
		registerTransformation(new Zombie());
	}

	public void report()
	{
		_log.info("TransformationManager: loaded " + _transformations.size() + " transformations.");
	}

	public void transformPlayer(final int id, final L2Player player)
	{
		final L2Transformation template = getTransformationById(id);
		if(template != null)
			player.setTransformation(id, template);
	}

	public L2Transformation getTransformationById(final int id)
	{
		return _transformations.get(id);
	}

	public L2Transformation registerTransformation(final L2Transformation transformation)
	{
		return _transformations.put(transformation.getId(), transformation);
	}
}
