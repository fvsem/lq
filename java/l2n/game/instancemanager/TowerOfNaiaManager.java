package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.boss.EpidosManager;
import l2n.game.model.L2ObjectProcedures;
import l2n.game.model.L2Party;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.naia.IngeniousContraption;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 30.07.2011
 * @time 2:38:38
 */
public class TowerOfNaiaManager
{
	private final static Logger _log = Logger.getLogger(TowerOfNaiaManager.class.getName());

	private final class TimeOutTask implements Runnable
	{
		@Override
		public void run()
		{
			// таск должен быть отменён, до того как он сработает, иначе выкинит из Tower of Naia
			exitParty();
		}
	}

	public static final Location ENTRANCE_LOCATION = new Location(-47271, 246098, -9120);
	private static final Location TOPROOF = new Location(16110, 243841, 11616);
	private static final Location AFTER12FLOOR = new Location(-47301, 243971, -13376);

	private static final Location[] TOPROOF_SPAWNLIST = new Location[]
	{
			new Location(16608, 244420, 11620, 31264, 18492), // naiazma_key[18492] контроллер который телепортирует в безопасную зону
			new Location(16409, 244438, 11620, -1048, 18491), // naiazma_lock[18491] замок который нужно атаковать - респаун 1 час
	};

	private static final int[][][] FLOOR_SPAWNLIST = new int[][][]
	{
			// для удобства)
			{},

			// 1 floor
			{ { 22393, 1, 700108 },
			{ 22394, 2, 700108 },
			{ 22393, 2, 700108 },
			{ 22394, 1, 700108 } },

			// 2 floor
			{ { 22439, 1, -48146, 249597, -9124, -16280 },
			{ 22439, 1, -48144, 248711, -9124, 16368 },
			{ 22439, 1, -48704, 249597, -9104, -16380 },
			{ 22439, 1, -49219, 249596, -9104, -16400 },
			{ 22439, 1, -49715, 249601, -9104, -16360 },
			{ 22439, 1, -49714, 248696, -9104, 15932 },
			{ 22439, 1, -49225, 248710, -9104, 16512 },
			{ 22439, 1, -48705, 248708, -9104, 16576 } },

			// 3 floor
			{ { 22441, 2, 700110 },
			{ 22442, 2, 700110 } },

			// 4 floor
			{ { 22440, 1, -49754, 243866, -9968, -16328 },
			{ 22440, 1, -49754, 242940, -9968, 16336 },
			{ 22440, 1, -48733, 243858, -9968, -16208 },
			{ 22440, 1, -48745, 242936, -9968, 16320 },
			{ 22440, 1, -49264, 242946, -9968, 16312 },
			{ 22440, 1, -49268, 243869, -9968, -16448 },
			{ 22440, 1, -48186, 242934, -9968, 16576 },
			{ 22440, 1, -48185, 243855, -9968, -16448 } },

			// 5 floor
			{ { 22411, 2, 700112 },
			{ 22393, 1, 700112 },
			{ 22394, 1, 700112 },
			{ 22393, 1, 700112 },
			{ 22394, 1, 700112 } },

			// 6 floor
			{ { 22395, 2, 700113 } },

			// 7 floor
			{ { 22393, 1, 700114 },
			{ 22394, 2, 700114 },
			{ 22393, 2, 700114 },
			{ 22394, 1, 700114 },
			{ 22412, 1, 700114 } },

			// 8 floor
			{ { 22395, 2, 700115 } },

			// 9 floor
			{ { 22441, 2, 700116 },
			{ 22442, 3, 700116 } },

			// 10 floor
			{ { 22395, 2, 700117 } },

			// 11 floor
			{ { 22413, 6, 700118 } },

			// 12 floor
			{ { 18490, 2, 700119 },
			{ 18490, 2, 700119 },
			{ 18490, 2, 700119 },
			{ 18490, 2, 700119 },
			{ 18490, 2, 700119 },
			{ 18490, 2, 700119 } }
	};

	private static final Location[] TOWER_SWITCHERS = new Location[]
	{
			new Location(-46200, 246120, -9120, 0, 18494), // switch1_tower_c[18494]
			new Location(-48148, 249165, -9120, 32768, 18495), // switch2_tower_c[18495]
			new Location(-51625, 246509, -9984, -16384, 18496), // switch3_tower_c[18496]
			new Location(-49749, 243406, -9984, 0, 18497), // switch4_tower_c[18497]
			new Location(-46185, 245905, -9984, 16380, 18498), // switch5_tower_c[18498]
			new Location(-48682, 248290, -9984, 32768, 18499), // switch6_tower_c[18499]
			new Location(-51633, 246339, -10848, -16384, 18500), // switch7_tower_c[18500]
			new Location(-49150, 243968, -10848, 0, 18501), // switch8_tower_c[18501]
			new Location(-46227, 245908, -11704, 16384, 18502), // switch9_tower_c[18502]
			new Location(-48706, 248287, -11712, 32768, 18503), // switch10_tower_c[18503]
			new Location(-51630, 246340, -12568, -16384, 18504), // switch11_tower_c[18504]
			new Location(-48537, 243648, -13376, 0, 18505) // switch12_tower_c[18505]
	};

	public static final int TIME_LIMIT = 300000; // 5 минут

	private L2Spawn naia_controller = null, naia_lock = null;

	/** Данные для текущего этажа */
	private int currentFloor = 0;
	private final ConcurrentHashMap<L2NpcInstance, Boolean> currentMonster = new ConcurrentHashMap<L2NpcInstance, Boolean>();
	private final GArray<L2Spawn> currentSpawns = new GArray<L2Spawn>();

	/** Переключатели в комнатах (+1 для удобства) */
	private final IngeniousContraption[] room_switcher = new IngeniousContraption[TOWER_SWITCHERS.length + 1];

	/** Инстанс группы которая сейчас проходит Tower of Naia */
	private L2Party _party = null;

	private ScheduledFuture<TimeOutTask> timer = null;

	public static final TowerOfNaiaManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private TowerOfNaiaManager()
	{
		// спауним монстров на крыше
		spawnTopRoof();

		// спауним Ingenious Contraption (обработка комнат)
		for(int room = 0; room < TOWER_SWITCHERS.length; room++)
			room_switcher[room + 1] = (IngeniousContraption) Functions.spawn(TOWER_SWITCHERS[room], TOWER_SWITCHERS[room].id);

		_log.info("TowerOfNaiaManager: loaded.");
	}

	private void spawnTopRoof()
	{
		L2NpcTemplate template;
		try
		{
			template = NpcTable.getTemplate(TOPROOF_SPAWNLIST[0].id);
			if(template == null)
			{
				_log.warning("TowerOfNaiaManager: mob with ID = " + TOPROOF_SPAWNLIST[0].id + " can't be spawned!");
				return;
			}
			naia_controller = new L2Spawn(template);
			naia_controller.setAmount(1);
			naia_controller.setLoc(TOPROOF_SPAWNLIST[0]);
			naia_controller.setRespawnDelay(60, 0);
			naia_controller.setRespawnTime(0);
			naia_controller.init();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TowerOfNaiaManager: error spawn ", e);
		}

		try
		{
			template = NpcTable.getTemplate(TOPROOF_SPAWNLIST[1].id);
			if(template == null)
			{
				_log.warning("TowerOfNaiaManager: mob with ID = " + TOPROOF_SPAWNLIST[1].id + " can't be spawned!");
				return;
			}
			naia_lock = new L2Spawn(template);
			naia_lock.setAmount(1);
			naia_lock.setLoc(TOPROOF_SPAWNLIST[1]);
			naia_lock.setRespawnDelay(3600, 0);
			naia_lock.setRespawnTime(0);
			naia_lock.init();
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TowerOfNaiaManager: error spawn ", e);
		}
	}

	public void startRoom(final int floor, final L2Player player)
	{
		if(floor < currentFloor)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			_log.log(Level.WARNING, "TowerOfNaiaManager: try start floor less that current [current:" + currentFloor + "|floor:" + floor + "|player:" + player.getName() + "]");
			return;
		}

		currentFloor = floor;

		if(timer != null)
			timer.cancel(true);
		// запускаем таймер для проверки лимита по времени
		timer = L2GameThreadPools.getInstance().scheduleGeneral(new TimeOutTask(), TIME_LIMIT);

		if(floor != 1)
			// открываем первую дверь и закрываем вторую дверь предыдущей комнаты
			room_switcher[floor - 1].renewSwitcher();

		spawnNpc(floor, player);
	}

	private void spawnNpc(final int floor, final L2Player player)
	{
		currentMonster.clear();
		int[][] spawnlist = FLOOR_SPAWNLIST[floor];

		L2NpcTemplate template;
		L2Spawn spawnDat;
		for(int[] data : spawnlist)
			try
			{
				template = NpcTable.getTemplate(data[0]);
				spawnDat = new L2Spawn(template);
				spawnDat.setAmount(data[1]);
				// спаун по терретории
				if(data.length == 3)
					spawnDat.setLocation(data[2]);
				else
				{
					spawnDat.setLocx(data[2]);
					spawnDat.setLocy(data[3]);
					spawnDat.setLocz(data[4]);
					spawnDat.setHeading(data[5]);
				}
				spawnDat.setRespawnDelay(0);
				spawnDat.setRespawnTime(0);
				// спауним
				spawnDat.init();

				currentSpawns.add(spawnDat);

				// ложим в список с НПС для этажа
				for(L2NpcInstance npc : spawnDat.getAllSpawned())
					currentMonster.put(npc, false);
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "TowerOfNaiaManager: spawnNpc error!", e);
			}
	}

	public void addMonster(L2NpcInstance monster)
	{
		if(monster != null && !monster.isDead())
			currentMonster.put(monster, false);
	}

	/**
	 * Проверка статуса текущей комнаты
	 * 
	 * @param player
	 * @param monster
	 */
	public void checkCurrentRoom(final L2Player player, final L2NpcInstance monster)
	{
		// если все монстры в комнате убиты
		if(checkNpcStatus(monster))
		{
			clearCurrentSpawn();
			// отключаем таймер для проверки лимита по времени
			if(timer != null)
			{
				timer.cancel(true);
				timer = null;
			}

			// если это последняя комната...
			if(currentFloor == 12)
			{
				_party.teleport(AFTER12FLOOR);
				// открываем первую дверь
				room_switcher[currentFloor].openDoor(true);
				EpidosManager.getInstance().init();
			}
			// открываем дверь для прохода в следующую комнату (gg->Castle_GateOpenClose2(DoorName2,0);)
			else
				room_switcher[currentFloor].openDoor(false);
		}
	}

	/**
	 * Проверяем необходимые условия, для разговора с Ingenious Contraption
	 * 
	 * @param player
	 * @param switcher
	 * @return
	 */
	public boolean checkTalkCondition(final L2Player player, final IngeniousContraption switcher)
	{
		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_CAN_OPERATE_THE_MACHINE_WHEN_YOU_PARTICIPATE_IN_THE_PARTY);
			return false;
		}

		if(_party == null || _party.getMemberCount() == 0)
		{
			exitParty();

			// телепортируем всех на крышу
			for(L2Player pl : switcher.getAroundPlayers(3000))
				pl.teleToLocation(TOPROOF);

			_log.log(Level.WARNING, "TowerOfNaiaManager: checkTalkCondition error [current party is NULL], player: " + player.getName());
			return false;
		}

		// Проверяем чтоб пати была та же, что и зашла
		if(player.getParty() != _party)
		{
			player.getParty().teleport(TOPROOF);
			player.sendPacket(Msg.YOU_CAN_OPERATE_THE_MACHINE_WHEN_YOU_PARTICIPATE_IN_THE_PARTY);
			return false;
		}

		// проверяем текущий статус монстров
		if(!checkNpcStatus(null))
			return false;

		// проверяем расстояние до НПС
		for(L2Player member : _party.getPartyMembers())
			if(!member.isInRange(switcher, 500))
			{
				member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				return false;
			}

		return true;
	}

	public boolean checkNpcStatus(final L2NpcInstance monster)
	{
		if(!currentMonster.isEmpty())
		{
			if(monster != null && currentMonster.containsKey(monster))
				currentMonster.put(monster, true);

			for(boolean dead : currentMonster.values())
				if(!dead)
					return false;

			// вернёт true только если список не пустой и все убиты
			return true;
		}
		else if(monster == null)
			// возвращаем true если список пуст, для первой команаты нужно
			return true;
		return false;
	}

	/**
	 * Вызываем при фейле прохождения инстанса
	 */
	public void exitParty()
	{
		// выгоняем пати наверх
		if(_party != null)
		{
			_party.teleport(TOPROOF);
			_party = null;
		}

		renewTower();
	}

	/**
	 * Отчищает текущий спаун
	 */
	private void clearCurrentSpawn()
	{
		currentSpawns.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);
		currentSpawns.clear();

		for(L2NpcInstance monster : currentMonster.keySet())
		{
			if(monster != null)
				monster.deleteMe();
			monster = null;
		}
		currentMonster.clear();
	}

	/**
	 * Приводит статус башни в начальное состояние со всеми НПС. Необходимо для последующего прохождения других групп.
	 */
	private void renewTower()
	{
		// отключаем таймер для проверки лимита по времени
		if(timer != null)
		{
			timer.cancel(true);
			timer = null;
		}

		currentFloor = 0;
		// приводим двери в начальное состояние
		for(IngeniousContraption switcher : room_switcher)
			if(switcher != null)
				switcher.renewSwitcher();

		clearCurrentSpawn();
	}

	/**
	 * Ворзвращает время через которое будет снова доступе Контроллер
	 * 
	 * @return
	 */
	public boolean checkLockHealPoint()
	{
		L2NpcInstance lock = naia_lock.getLastSpawn();
		if(lock == null || lock.isDead() || lock.getCurrentHpPercents() > 10)
			return false;
		else if(lock.getCurrentHpPercents() <= 10)
			return true;
		return false;
	}

	public void setParty(L2Party party)
	{
		_party = party;
	}

	public boolean ifPartyExist()
	{
		if(_party == null || _party.getMemberCount() == 0)
		{
			_party = null;
			return false;
		}
		return true;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TowerOfNaiaManager _instance = new TowerOfNaiaManager();
	}
}
