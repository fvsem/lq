package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.CursedWeapon;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Location;
import l2n.util.Rnd;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Logger;

public class CursedWeaponsManager
{
	private static final Logger _log = Logger.getLogger(CursedWeaponsManager.class.getName());
	private static CursedWeaponsManager _instance;
	private TIntObjectHashMap<CursedWeapon> _cursedWeapons;

	public static CursedWeaponsManager getInstance()
	{
		if(_instance == null)
			_instance = new CursedWeaponsManager();
		return _instance;
	}

	public CursedWeaponsManager()
	{
		_cursedWeapons = new TIntObjectHashMap<CursedWeapon>();
		_log.info("CursedWeaponsManager: Initializing");
		load();
		restore();
		checkConditions();
		_log.info("CursedWeaponsManager: Loaded " + _cursedWeapons.size() + " cursed weapon(s).");

	}

	public final void reload()
	{
		_instance = new CursedWeaponsManager();
	}

	private void load()
	{
		if(Config.DEBUG)
			System.out.print("CursedWeaponsManager: Parsing ... ");
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			File file = new File(Config.DATAPACK_ROOT + "/data/cursed_weapons.xml");
			if(!file.exists())
			{
				if(Config.DEBUG)
					System.out.println("CursedWeaponsManager: NO FILE");
				return;
			}

			Document doc = factory.newDocumentBuilder().parse(file);

			for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			{
				if("list".equalsIgnoreCase(n.getNodeName()))
				{
					for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					{
						if("item".equalsIgnoreCase(d.getNodeName()))
						{
							NamedNodeMap attrs = d.getAttributes();
							int id = Integer.parseInt(attrs.getNamedItem("id").getNodeValue());
							int skillId = Integer.parseInt(attrs.getNamedItem("skillId").getNodeValue());
							String name = attrs.getNamedItem("name").getNodeValue();

							CursedWeapon cw = new CursedWeapon(id, skillId, name);

							int val;
							for(Node cd = d.getFirstChild(); cd != null; cd = cd.getNextSibling())
							{
								if("dropRate".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setDropRate(val);
								}
								else if("durationmin".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setDurationMin(val);
								}
								else if("durationmax".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setDurationMax(val);
								}
								else if("durationLost".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setDurationLost(val);
								}
								else if("disapearChance".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setDisapearChance(val);
								}
								else if("stageKills".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setStageKills(val);
								}
								else if("transformationId".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									val = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
									cw.setTransformationId(val);
								}
							}

							// Store cursed weapon
							_cursedWeapons.put(id, cw);
						}
					}
				}
			}
			if(Config.DEBUG)
				System.out.println("CursedWeaponsManager: OK");
		}
		catch(Exception e)
		{
			_log.severe("CursedWeaponsManager: Error parsing cursed_weapons file. " + e);

			if(Config.DEBUG)
				System.out.println("CursedWeaponsManager: ERROR");
		}
	}

	private void restore()
	{
		if(Config.DEBUG)
			System.out.print("CursedWeaponsManager: restoring ... ");

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM cursed_weapons");
			rset = statement.executeQuery();

			while (rset.next())
			{
				int itemId = rset.getInt("item_id");
				CursedWeapon cw = _cursedWeapons.get(itemId);
				if(cw != null)
				{
					int player_id = rset.getInt("player_id");
					int player_karma = rset.getInt("player_karma");
					int player_pkkills = rset.getInt("player_pkkills");
					int nb_kills = rset.getInt("nb_kills");
					Location loc = new Location(rset.getInt("x"), rset.getInt("y"), rset.getInt("z"));
					long end_time = rset.getLong("end_time");

					cw.setPlayerId(player_id);
					cw.setPlayerKarma(player_karma);
					cw.setPlayerPkKills(player_pkkills);
					cw.setNbKills(nb_kills);
					cw.setLoc(loc);
					cw.setEndTime(end_time * 1000);
					cw.reActivate();
				}
				else
				{
					removeFromDb(itemId);
					_log.warning("CursedWeaponsManager: Unknown cursed weapon " + itemId + ", deleted");
				}
			}
			if(Config.DEBUG)
				System.out.println("CursedWeaponsManager: OK");
		}
		catch(Exception e)
		{
			_log.warning("CursedWeaponsManager: Could not restore cursed_weapons data: " + e);
			e.printStackTrace();

			if(Config.DEBUG)
				System.out.println("CursedWeaponsManager: ERROR");
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void checkConditions()
	{
		if(Config.DEBUG)
			System.out.print("CursedWeaponsManager: Checking conditions ... ");

		ThreadConnection con = null;
		FiltredPreparedStatement statement1 = null, statement2 = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement1 = con.prepareStatement("DELETE FROM character_skills WHERE skill_id=?");
			statement2 = con.prepareStatement("SELECT owner_id FROM items WHERE item_id=?");

			for(CursedWeapon cw : _cursedWeapons.valueCollection())
			{
				// Do an item check to be sure that the cursed weapon and/or skill isn't hold by someone
				int itemId = cw.getItemId();
				int skillId = cw.getSkillId();
				boolean foundedInItems = false;

				// Delete all cursed weapons skills (we don`t care about same skill on multiply weapons, when player back, skill will appears again)
				statement1.setInt(1, skillId);
				statement1.executeUpdate();

				statement2.setInt(1, itemId);
				rset = statement2.executeQuery();

				while (rset.next())
				{
					// A player has the cursed weapon in his inventory ...
					int playerId = rset.getInt("owner_id");

					if(!foundedInItems)
					{
						if(playerId != cw.getPlayerId() || cw.getPlayerId() == 0)
						{
							emptyPlayerCursedWeapon(playerId, itemId, cw);
							_log.info("CursedWeaponsManager[254]: Player " + playerId + " owns the cursed weapon " + itemId + " but he shouldn't.");
						}
						else
							foundedInItems = true;
					}
					else
					{
						emptyPlayerCursedWeapon(playerId, itemId, cw);
						_log.info("CursedWeaponsManager[262]: Player " + playerId + " owns the cursed weapon " + itemId + " but he shouldn't.");
					}
				}

				if(!foundedInItems && cw.getPlayerId() != 0)
				{
					removeFromDb(cw.getItemId());
					_log.info("CursedWeaponsManager: Unownered weapon, removing from table...");
				}
			}
		}
		catch(Exception e)
		{
			_log.warning("CursedWeaponsManager: Could not check cursed_weapons data: " + e);

			if(Config.DEBUG)
				System.out.println("CursedWeaponsManager: ERROR");
			return;
		}
		finally
		{
			DbUtils.close(statement1);
			DbUtils.closeQuietly(con, statement2, rset);
		}

		if(Config.DEBUG)
			System.out.println("CursedWeaponsManager: DONE");
	}

	private void emptyPlayerCursedWeapon(int playerId, int itemId, CursedWeapon cw)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			// Delete the item
			statement = con.prepareStatement("DELETE FROM items WHERE owner_id=? AND item_id=?");
			statement.setInt(1, playerId);
			statement.setInt(2, itemId);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement("UPDATE characters SET karma=?, pkkills=? WHERE obj_id=?");
			statement.setInt(1, cw.getPlayerKarma());
			statement.setInt(2, cw.getPlayerPkKills());
			statement.setInt(3, playerId);
			if(statement.executeUpdate() != 1)
				_log.warning("Error while updating karma & pkkills for userId " + cw.getPlayerId());
			// clean up the cursedweapons table.
			removeFromDb(itemId);
		}
		catch(SQLException e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void removeFromDb(int itemId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("DELETE FROM cursed_weapons WHERE item_id = ?");
			statement.setInt(1, itemId);
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			_log.severe("CursedWeaponsManager: Failed to remove data: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void saveData(CursedWeapon cw)
	{
		if(cw == null)
		{
			_log.severe("CursedWeapon: Failed to save data - cw is NULL!");
			Thread.dumpStack();
			return;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		synchronized (cw)
		{
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();

				// Delete previous datas
				statement = con.prepareStatement("DELETE FROM cursed_weapons WHERE item_id = ?");
				statement.setInt(1, cw.getItemId());
				statement.executeUpdate();
				DbUtils.close(statement);
				statement = null;
				if(cw.isActive())
				{
					statement = con.prepareStatement("REPLACE INTO cursed_weapons (item_id, player_id, player_karma, player_pkkills, nb_kills, x, y, z, end_time) VALUES (?,?,?,?,?,?,?,?,?)");
					statement.setInt(1, cw.getItemId());
					statement.setInt(2, cw.getPlayerId());
					statement.setInt(3, cw.getPlayerKarma());
					statement.setInt(4, cw.getPlayerPkKills());
					statement.setInt(5, cw.getNbKills());
					statement.setInt(6, cw.getLoc().x);
					statement.setInt(7, cw.getLoc().y);
					statement.setInt(8, cw.getLoc().z);
					statement.setLong(9, cw.getEndTime() / 1000);
					statement.executeUpdate();
				}
			}
			catch(SQLException e)
			{
				_log.severe("CursedWeapon: Failed to save data");
				e.printStackTrace();
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
	}

	public void saveData()
	{
		if(Config.DEBUG)
			System.out.println("CursedWeaponsManager: saving data to disk.");
		for(CursedWeapon cw : _cursedWeapons.valueCollection())
			saveData(cw);
	}

	/**
	 * вызывается при логине игрока
	 */
	public void checkPlayer(L2Player player)
	{
		for(CursedWeapon cw : _cursedWeapons.valueCollection())
			if(player.getInventory().findItemByItemId(cw.getItemId()) != null)
				checkPlayer(player, player.getInventory().findItemByItemId(cw.getItemId()));
	}

	/**
	 * вызывается при выходе игрока
	 */
	public void doLogout(L2Player player)
	{
		for(CursedWeapon cw : _cursedWeapons.valueCollection())
			if(player.getInventory().findItemByItemId(cw.getItemId()) != null)
			{
				cw.setPlayer(null);
				cw.setItem(null);
			}
	}

	/**
	 * вызывается, когда проклятое оружие оказывается в инвентаре игрока
	 */
	public void checkPlayer(L2Player player, L2ItemInstance item)
	{
		if(player == null || item == null || player.isInOlympiadMode())
			return;

		CursedWeapon cw = _cursedWeapons.get(item.getItemId());
		if(cw == null)
			return;

		if(player.getObjectId() == cw.getPlayerId() || cw.getPlayerId() == 0 || cw.isDropped())
		{
			activate(player, item);
			showUsageTime(player, cw);
		}
		else
		{
			// wtf? how you get it?
			_log.warning("CursedWeaponsManager: " + player + " tried to obtain " + item + " in wrong way");
			player.getInventory().destroyItem(item, item.getCount(), true);
		}
	}

	public void activate(L2Player player, L2ItemInstance item)
	{
		if(player == null || player.isInOlympiadMode())
			return;

		CursedWeapon cw = _cursedWeapons.get(item.getItemId());
		if(cw != null)
		{
			if(player.isMounted())
			{
				// TODO: correct this custom message.
				player.sendMessage("You may not pick up this item while riding in this territory");
				L2ItemInstance dropItem = player.getInventory().findItemByItemId(item.getItemId());
				dropItem.dropToTheGround(player, (L2NpcInstance) null);
				dropItem.setDropTime(0);
				return;
			}

			if(player.isCursedWeaponEquipped()) // cannot own 2 cursed swords
			{
				if(player.getCursedWeaponEquippedId() != item.getItemId())
				{
					CursedWeapon cw2 = _cursedWeapons.get(player.getCursedWeaponEquippedId());
					cw2.setNbKills(cw2.getStageKills() - 1);
					cw2.increaseKills();
				}

				cw.setPlayer(player); // NECESSARY in order to find which inventory the weapon is in!
				// erase the newly obtained cursed weapon
				cw.endOfLife();
				player.getInventory().destroyItem(item, 1, true);
			}
			else if(cw.getTimeLeft() > 0)
			{
				cw.activate(player, item);
				saveData(cw);
				announce(new SystemMessage(SystemMessage.THE_OWNER_OF_S2_HAS_APPEARED_IN_THE_S1_REGION).addZoneName(player.getLoc()).addString(cw.getName()));
			}
			else
				cw.endOfLife();
		}
	}

	/**
	 * drop from L2NpcInstance killed by L2Player
	 */
	public void dropAttackable(L2NpcInstance attackable, L2Player player)
	{
		// не дропаем в отражениях
		if(attackable == null || player == null || attackable.getReflectionId() > 0 || player.getReflectionId() > 0)
			return;

		if(player.isCursedWeaponEquipped() || _cursedWeapons.size() == 0)
			return;
		if(!Config.ALLOW_CURSED_WEAPONS)
			return;
		if(Config.CURSED_WEAPONS_MIN_PLAYERS_DROP > 0 && L2ObjectsStorage.getAllPlayersCount() - L2ObjectsStorage.getAllOfflineCount() < Config.CURSED_WEAPONS_MIN_PLAYERS_DROP)
			return;

		synchronized (_cursedWeapons)
		{
			int num = 0;
			short count = 0;
			byte breakFlag = 0;

			while (breakFlag == 0)
			{
				num = _cursedWeapons.keys()[Rnd.get(_cursedWeapons.size())];
				count++;

				if(_cursedWeapons.get(num) != null && !_cursedWeapons.get(num).isActive())
					breakFlag = 1;
				else if(count >= getCursedWeapons().size())
					breakFlag = 2;
			}

			if(breakFlag == 1)
				_cursedWeapons.get(num).dropIt(attackable, player);
		}
	}

	/**
	 * drop from killed L2Player (loosing CursedWeapon)
	 */
	public void dropPlayer(L2Player activeChar)
	{
		if(activeChar == null)
			return;

		CursedWeapon cw = _cursedWeapons.get(activeChar.getCursedWeaponEquippedId());
		if(cw == null)
			return;

		if(cw.dropIt(null, null))
		{
			saveData(cw);
			announce(new SystemMessage(SystemMessage.S2_WAS_DROPPED_IN_THE_S1_REGION).addZoneName(activeChar.getLoc()).addString(cw.getName()));
		}
		else
			cw.endOfLife();
	}

	public void increaseKills(int itemId)
	{
		CursedWeapon cw = _cursedWeapons.get(itemId);
		if(cw != null)
		{
			cw.increaseKills();
			saveData(cw);
		}
	}

	public int getLevel(int itemId)
	{
		CursedWeapon cw = _cursedWeapons.get(itemId);
		return cw != null ? cw.getLevel() : 0;
	}

	public static void announce(SystemMessage sm)
	{
		for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			player.sendPacket(sm);
	}

	public void showUsageTime(L2Player player, short itemId)
	{
		CursedWeapon cw = _cursedWeapons.get(itemId);
		if(cw != null)
			showUsageTime(player, cw);
	}

	public void showUsageTime(L2Player player, CursedWeapon cw)
	{
		SystemMessage sm = new SystemMessage(SystemMessage.S2_MINUTE_OF_USAGE_TIME_ARE_LEFT_FOR_S1);
		sm.addString(cw.getName());
		sm.addNumber(new Long(cw.getTimeLeft() / 60000).intValue());
		player.sendPacket(sm);
	}

	public boolean isCursed(int itemId)
	{
		return _cursedWeapons.containsKey(itemId);
	}

	public Collection<CursedWeapon> getCursedWeapons()
	{
		return _cursedWeapons.valueCollection();
	}

	public int[] getCursedWeaponsIds()
	{
		return _cursedWeapons.keys();
	}

	public CursedWeapon getCursedWeapon(int itemId)
	{
		return _cursedWeapons.get(itemId);
	}
}
