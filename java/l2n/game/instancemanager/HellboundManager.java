package l2n.game.instancemanager;

import java.util.logging.Logger;

/**
 * Менеджер Острова Хеллбаунд
 * 
 * @author L2System Project
 * @date 22.09.2010
 * @time 12:30:24
 */
public class HellboundManager
{
	private final static Logger _log = Logger.getLogger(HellboundManager.class.getName());

	private boolean _isOpen = false;
	private boolean _megalithsCompleted = false;
	private int _level = 0;
	private int _currentTrust = 0;
	private int _tempTrust = 0;
	private int tempLevel = 0;

	// =========================================================
	// Constructor
	private HellboundManager()
	{
		_log.info("Initializing HellboundManager");
		init();
	}

	private void init()
	{
		checkHellboundLevel();
		_log.info("HelboundManager: Loaded ");
	}

	public static final HellboundManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	public boolean checkMegalithsCompleted()
	{
		return _megalithsCompleted;
	}

	public void setMegalithsCompleted(boolean value)
	{
		_megalithsCompleted = value;
	}

	public boolean checkIsOpen()
	{
		return _isOpen;
	}

	public int getLevel()
	{
		return _level;
	}

	public int getTrust()
	{
		return _currentTrust;
	}

	public void increaseTrust(int points)
	{
		// _currentTrust += points * Config.RATE_HELLBOUND_POINTS;
		updateTrust(_currentTrust);
	}

	public void updateTrust(int newTrust)
	{
// _currentTrust = newTrust;
// Connection con = null;
// try
// {
// con = L2DatabaseFactory.getInstance().getConnection();
//
// PreparedStatement statement = con.prepareStatement("UPDATE hellbound SET trustLevel=? WHERE name=8000");
// statement.setInt(1, _currentTrust);
// statement.execute();
// statement.close();
//
// }
// catch(SQLException e)
// {
// _log.warning("HellboundManager: Could not save Hellbound trust points");
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// finally
// {
// try
// {
// con.close();
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// }
	}

	public void changeLevel(int newLevel)
	{
// _level = newLevel;
// Connection con = null;
// try
// {
// con = L2DatabaseFactory.getInstance().getConnection();
//
// PreparedStatement statement = con.prepareStatement("UPDATE hellbound SET zonesLevel=? WHERE name=8000");
// statement.setInt(1, _level);
// statement.execute();
// statement.close();
//
// }
// catch(SQLException e)
// {
// _log.warning("HellboundManager: Could not save Hellbound level");
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// finally
// {
// try
// {
// con.close();
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// }
	}

	public void checkHellboundLevel()
	{
// Connection con = null;
// try
// {
// con = L2DatabaseFactory.getInstance().getConnection();
//
// PreparedStatement statement = con.prepareStatement("SELECT trustLevel,zonesLevel FROM hellbound");
// ResultSet rset = statement.executeQuery();
//
// while (rset.next())
// {
// // Read all info from DB, and store it for AI to read and decide what to do
// // faster than accessing DB in real time
// _tempTrust = rset.getInt("trustLevel");
// tempLevel = rset.getInt("zonesLevel");
// if(_tempTrust > 0 && _tempTrust < 300000)
// _level = 1;
// if(_tempTrust >= 300000 && _tempTrust < 600000)
// _level = 2;
// if(_tempTrust >= 600000 && _tempTrust < 1000000)
// _level = 3;
// if(tempLevel == 4)
// _level = 4;
// if(tempLevel == 5)
// _level = 5;
// if(tempLevel == 6)
// _level = 6;
// if(tempLevel == 7)
// _level = 7;
// if(tempLevel == 8)
// _level = 8;
// if(tempLevel == 9)
// _level = 9;
// if(tempLevel == 10)
// _level = 10;
// if(_tempTrust >= 2000000 && tempLevel >= 10)
// _level = 11;
//
// _currentTrust = _tempTrust;
// }
// rset.close();
// statement.close();
//
// }
// catch(SQLException e)
// {
// _log.warning("HellboundManager: Could not load the hellbound table");
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// finally
// {
// try
// {
// con.close();
// }
// catch(Exception e)
// {
// StackTrace.displayStackTraceInformation(e);
// }
// }
// if(tempLevel != _level && tempLevel != 5)
// changeLevel(_level);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final HellboundManager INSTANCE = new HellboundManager();
	}
}
