package l2n.game.instancemanager;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.GmListTable;
import l2n.util.StringUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 28.12.2010
 * @time 16:19:32
 */
public final class PetitionManager
{
	private static final Logger _log = Logger.getLogger(PetitionManager.class.getName());

	private Map<Integer, Petition> _pendingPetitions;
	private Map<Integer, Petition> _completedPetitions;
	private final AtomicInteger _petitionIds;

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	private static enum PetitionState
	{
		Pending,
		Responder_Cancel,
		Responder_Missing,
		Responder_Reject,
		Responder_Complete,
		Petitioner_Cancel,
		Petitioner_Missing,
		In_Process,
		Completed
	}

	private static enum PetitionType
	{
		Immobility,
		Recovery_Related,
		Bug_Report,
		Quest_Related,
		Bad_User,
		Suggestions,
		Game_Tip,
		Operation_Related,
		Other
	}

	public static PetitionManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	private final class PetitionLogEntry
	{
		private final String _player;
		private final String _message;
		private final boolean _gmMessage;

		public PetitionLogEntry(final String name, final String message, final boolean gmMessage)
		{
			_player = name;
			_message = message;
			_gmMessage = gmMessage;
		}

		public final String getName()
		{
			return _player;
		}

		public final String getMessage()
		{
			return _message;
		}

		public final boolean isGmMessage()
		{
			return _gmMessage;
		}
	}

	private final class Petition
	{
		private final long _petitionCreateTime = System.currentTimeMillis();

		private final int _id;
		private final PetitionType _type;
		private PetitionState _state = PetitionState.Pending;
		private String _content;

		private GArray<PetitionLogEntry> _messageLog = new GArray<PetitionLogEntry>();

		private long _playerStoredId;
		private final int _playerObjId;
		private final String _playerName;
		private long _gmStoredId;

		public Petition(final L2Player petitioner, final String petitionText, int petitionType)
		{
			petitionType--;
			_id = _petitionIds.incrementAndGet();
			if(petitionType >= PetitionType.values().length)
				_log.log(Level.WARNING, "PetitionManager: invalid petition type (received type was +1) : " + petitionType);
			_type = PetitionType.values()[petitionType];
			_content = petitionText;

			_playerStoredId = petitioner.getStoredId();
			_playerObjId = petitioner.getObjectId();
			_playerName = petitioner.getName();
		}

		protected void addLogMessage(final PetitionLogEntry entry)
		{
			synchronized (_messageLog)
			{
				_messageLog.add(entry);
			}
		}

		protected PetitionLogEntry[] getLogMessages()
		{
			return _messageLog.toArray(new PetitionLogEntry[_messageLog.size()]);
		}

		public boolean endPetitionConsultation(PetitionState endState)
		{
			setState(endState);

			L2Player gm = getResponder();
			if(gm != null && gm.isOnline())
				if(endState == PetitionState.Responder_Reject)
					sendMessageToPetitioner("Your petition was rejected. Please try again later.");
				else
				{
					// Ending petition consultation with <Player>.
					SystemMessage sm = new SystemMessage(SystemMessage.ENDING_PETITION_CONSULTATION_WITH_S1);
					sm.addString(getPetitionerName());
					gm.sendPacket(sm);

					if(endState == PetitionState.Petitioner_Cancel)
					{
						// Receipt No. <ID> petition cancelled.
						sm = new SystemMessage(SystemMessage.RECEIPT_NO_S1_PETITION_CANCELLED);
						sm.addNumber(getId());
						gm.sendPacket(sm);
					}
				}

			// End petition consultation and inform them, if they are still online.
			L2Player player = getPetitioner();
			if(player != null && player.isOnline())
				player.sendPacket(Msg.ENDING_PETITION_CONSULTATION);

			_completedPetitions.put(getId(), this);
			return _pendingPetitions.remove(getId()) != null;
		}

		public String getContent()
		{
			return _content;
		}

		public int getId()
		{
			return _id;
		}

		public L2Player getPetitioner()
		{
			return L2ObjectsStorage.getAsPlayer(_playerStoredId);
		}

		public void updatePetitioner(final L2Player petitioner)
		{
			_playerStoredId = petitioner != null ? petitioner.getStoredId() : 0;
		}

		public void sendMessageToPetitioner(String msg)
		{
			L2Player pl = getPetitioner();
			if(pl != null && pl.isOnline() && !pl.isInOfflineMode())
				pl.sendMessage(msg);
		}

		public String getPetitionerName()
		{
			return _playerName;
		}

		public int getPetitionerObjectId()
		{
			return _playerObjId;
		}

		public L2Player getResponder()
		{
			return L2ObjectsStorage.getAsPlayer(_gmStoredId);
		}

		public long getCreateTime()
		{
			return _petitionCreateTime;
		}

		public PetitionState getState()
		{
			return _state;
		}

		public String getTypeAsString()
		{
			return _type.toString().replace("_", " ");
		}

		public void sendPetitionerPacket(final L2GameServerPacket responsePacket)
		{
			L2Player pl = getPetitioner();
			if(pl != null && pl.isOnline() && !pl.isInOfflineMode())
				pl.sendPacket(responsePacket);
		}

		public void sendResponderPacket(final L2GameServerPacket responsePacket)
		{
			L2Player gm = getResponder();
			if(gm == null || !gm.isOnline())
			{
				endPetitionConsultation(PetitionState.Responder_Missing);
				return;
			}
			gm.sendPacket(responsePacket);
		}

		public void setState(PetitionState state)
		{
			_state = state;
		}

		public void setResponder(final L2Player respondingAdmin)
		{
			if(getResponder() != null)
				return;

			_gmStoredId = respondingAdmin.getStoredId();
		}
	}

	private PetitionManager()
	{
		_log.log(Level.INFO, "PetitionManager: Initializing");

		_pendingPetitions = new ConcurrentHashMap<Integer, Petition>();
		_completedPetitions = new ConcurrentHashMap<Integer, Petition>();
		_petitionIds = new AtomicInteger(0);
	}

	public void clearCompletedPetitions()
	{
		int numPetitions = getPendingPetitionCount();

		_completedPetitions.clear();
		_log.log(Level.INFO, "PetitionManager: Completed petition data cleared. " + numPetitions + " petition(s) removed.");
	}

	public void clearPendingPetitions()
	{
		int numPetitions = getPendingPetitionCount();

		_pendingPetitions.clear();
		_log.log(Level.INFO, "PetitionManager: Pending petition queue cleared. " + numPetitions + " petition(s) removed.");
	}

	public boolean acceptPetition(final L2Player respondingAdmin, int petitionId)
	{
		if(!isValidPetition(petitionId))
			return false;

		Petition currPetition = _pendingPetitions.get(petitionId);
		// если ГМ уже назначен петиции
		if(currPetition.getResponder() != null)
			return false;

		currPetition.setResponder(respondingAdmin);
		currPetition.setState(PetitionState.In_Process);

		// Petition application accepted. (Send to Petitioner)
		currPetition.sendPetitionerPacket(Msg.PETITION_APPLICATION_ACCEPTED);

		// Petition application accepted. Reciept No. is <ID>
		SystemMessage sm = new SystemMessage(SystemMessage.PETITION_APPLICATION_ACCEPTED_RECEIPT_NO_IS_S1);
		sm.addNumber(currPetition.getId());
		currPetition.sendResponderPacket(sm);

		// Petition consultation with <Player> underway.
		sm = new SystemMessage(SystemMessage.PETITION_CONSULTATION_WITH_S1_UNDER_WAY);
		sm.addString(currPetition.getPetitionerName());
		currPetition.sendResponderPacket(sm);
		return true;
	}

	/**
	 * Отмена петиции игроком или ГМом
	 */
	public boolean cancelActivePetition(L2Player player)
	{
		for(Petition currPetition : _pendingPetitions.values())
		{
			// Проверяем отмену игроком
			if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == player.getObjectId())
				return currPetition.endPetitionConsultation(PetitionState.Petitioner_Cancel);

			// Проверяем отмену ГМом
			if(currPetition.getResponder() != null && currPetition.getResponder().getObjectId() == player.getObjectId())
				return currPetition.endPetitionConsultation(PetitionState.Responder_Cancel);
		}

		return false;
	}

	/**
	 * Проверка при входе игрока в игре (тут можно обновить storedId для петиции)
	 * 
	 * @param petitioner
	 */
	public void checkPetitionMessages(final L2Player petitioner)
	{
		if(petitioner != null)
			for(Petition currPetition : _pendingPetitions.values())
				if(currPetition != null)
					if(currPetition.getPetitioner() == null && currPetition.getPetitionerObjectId() == petitioner.getObjectId())
					{
						// При входе обновлеяем storedId для игрока
						currPetition.updatePetitioner(petitioner);

						L2GameServerPacket packet;
						PetitionLogEntry entry;

						// отправляем историю переписки
						PetitionLogEntry[] msgs = currPetition.getLogMessages();
						for(int i = 0; i < msgs.length; i++)
						{
							entry = msgs[i];
							packet = new CreatureSay(petitioner.getObjectId(), entry.isGmMessage() ? Say2C.PETITION_GM : Say2C.PETITION_PLAYER, entry.getName(), entry.getMessage());
							petitioner.sendPacket(packet);
						}

						GmListTable.broadcastToGMs(new CreatureSay(0, Say2C.BATTLEFIELD, "Petition System", "Player logged into the game. Petition #" + currPetition.getId() + "."));
						break;
					}
	}

	public void onLogoutPlayer(final L2Player petitioner)
	{
		if(petitioner != null)
			for(Petition currPetition : _pendingPetitions.values())
				if(currPetition != null)
					if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == petitioner.getObjectId())
					{
						// При выходе обновлеяем storedId для игрока
						currPetition.updatePetitioner(null);
						GmListTable.broadcastToGMs(new CreatureSay(0, Say2C.BATTLEFIELD, "Petition System", "Player went offline. Petition #" + currPetition.getId() + "."));
						break;
					}
	}

	public boolean endActivePetition(L2Player player)
	{
		if(!player.isGM())
			return false;

		for(Petition currPetition : _pendingPetitions.values())
			if(currPetition != null)
				if(currPetition.getResponder() != null && currPetition.getResponder().getObjectId() == player.getObjectId())
					return currPetition.endPetitionConsultation(PetitionState.Completed);

		return false;
	}

	public int getPendingPetitionCount()
	{
		return _pendingPetitions.size();
	}

	public int getPlayerTotalPetitionCount(L2Player player)
	{
		if(player == null)
			return 0;

		int petitionCount = 0;

		for(Petition currPetition : _pendingPetitions.values())
			if(currPetition != null)
				if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == player.getObjectId())
					petitionCount++;

		for(Petition currPetition : _completedPetitions.values())
			if(currPetition != null)
				if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == player.getObjectId())
					petitionCount++;

		return petitionCount;
	}

	public boolean isPetitionInProcess()
	{
		for(Petition currPetition : _pendingPetitions.values())
			if(currPetition != null)
				if(currPetition.getState() == PetitionState.In_Process)
					return true;

		return false;
	}

	public boolean isPetitionInProcess(int petitionId)
	{
		if(!isValidPetition(petitionId))
			return false;

		Petition currPetition = _pendingPetitions.get(petitionId);
		return currPetition != null && currPetition.getState() == PetitionState.In_Process;
	}

	public boolean isPlayerInConsultation(L2Player player)
	{
		if(player != null)
			for(Petition currPetition : _pendingPetitions.values())
				if(currPetition != null)
				{
					if(currPetition.getState() != PetitionState.In_Process)
						continue;

					if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == player.getObjectId() || currPetition.getResponder() != null && currPetition.getResponder().getObjectId() == player.getObjectId())
						return true;
				}

		return false;
	}

	public boolean isPetitioningAllowed()
	{
		return Config.PETITIONING_ALLOWED;
	}

	public boolean isPlayerPetitionPending(L2Player petitioner)
	{
		if(petitioner != null)
			for(Petition currPetition : _pendingPetitions.values())
				if(currPetition != null)
					if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == petitioner.getObjectId())
						return true;

		return false;
	}

	private boolean isValidPetition(int petitionId)
	{
		return _pendingPetitions.containsKey(petitionId);
	}

	public boolean rejectPetition(L2Player respondingAdmin, int petitionId)
	{
		if(!isValidPetition(petitionId))
			return false;

		Petition currPetition = _pendingPetitions.get(petitionId);
		if(currPetition == null || currPetition.getResponder() != null)
			return false;

		currPetition.setResponder(respondingAdmin);
		return currPetition.endPetitionConsultation(PetitionState.Responder_Reject);
	}

	public boolean sendActivePetitionMessage(L2Player player, String messageText)
	{
		CreatureSay cs;
		for(Petition currPetition : _pendingPetitions.values())
			if(currPetition != null)
			{
				// Если пишет игрок
				if(currPetition.getPetitioner() != null && currPetition.getPetitionerObjectId() == player.getObjectId())
				{
					cs = new CreatureSay(player.getObjectId(), Say2C.PETITION_PLAYER, player.getName(), messageText);
					currPetition.addLogMessage(new PetitionLogEntry(player.getName(), messageText, false));

					currPetition.sendResponderPacket(cs);
					currPetition.sendPetitionerPacket(cs);
					return true;
				}

				// если пишет ГМ
				if(currPetition.getResponder() != null && currPetition.getResponder().getObjectId() == player.getObjectId())
				{
					cs = new CreatureSay(player.getObjectId(), Say2C.PETITION_GM, player.getName(), messageText);
					currPetition.addLogMessage(new PetitionLogEntry(player.getName(), messageText, true));

					currPetition.sendResponderPacket(cs);
					currPetition.sendPetitionerPacket(cs);
					return true;
				}
			}

		return false;
	}

	public void sendPendingPetitionList(L2Player activeChar)
	{
		final StringBuilder htmlContent = StringUtil.startAppend(600 + getPendingPetitionCount() * 300, "<html><body><center><table width=270><tr>"
				+ "<td width=45><button value=\"Main\" action=\"bypass -h admin_admin\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>"
				+ "<td width=180><center>Petition Menu</center></td>"
				+ "<td width=45><button value=\"Back\" action=\"bypass -h admin admin7\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table><br>"
				+ "<table width=\"270\">"
				+ "<tr><td><table width=\"270\"><tr><td><button value=\"Reset\" action=\"bypass -h admin_reset_petitions\" width=\"80\" height=\"21\" back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>"
				+ "<td align=right><button value=\"Refresh\" action=\"bypass -h admin_view_petitions\" width=\"80\" height=\"21\" back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table><br></td></tr>");

		if(getPendingPetitionCount() == 0)
			htmlContent.append("<tr><td>There are no currently pending petitions.</td></tr>");
		else
			htmlContent.append("<tr><td><font color=\"LEVEL\">Current Petitions:</font><br></td></tr>");

		boolean color = true;
		int petcount = 0;

		for(Petition currPetition : _pendingPetitions.values())
		{
			if(currPetition == null)
				continue;

			StringUtil.append(htmlContent, "<tr><td width=\"270\"><table width=\"270\" cellpadding=\"2\" bgcolor=", color ? "131210" : "444444", "><tr><td width=\"130\">", dateFormat.format(new Date(currPetition.getCreateTime())));
			StringUtil.append(htmlContent, "</td><td width=\"140\" align=right><font color=\"", currPetition.getPetitioner() != null ? "00FF00" : "999999", "\">", currPetition.getPetitionerName(), "</font></td></tr>");
			StringUtil.append(htmlContent, "<tr><td width=\"130\">");
			if(currPetition.getState() != PetitionState.In_Process)
			{
				StringUtil.append(htmlContent, "<table width=\"130\" cellpadding=\"2\"><tr>");
				StringUtil.append(htmlContent, "<td><button value=\"View\" action=\"bypass -h admin_view_petition ", String.valueOf(currPetition.getId()), "\" width=\"50\" height=\"21\" back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				StringUtil.append(htmlContent, "<td><button value=\"Reject\" action=\"bypass -h admin_reject_petition ", String.valueOf(currPetition.getId()), "\" width=\"50\" height=\"21\" back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table>");
			}
			else
				htmlContent.append("<font color=\"999999\">In Process</font> <font color=\"" + (currPetition.getResponder() != null ? "00FF00" : "999999") + "\">" + currPetition.getResponder().getName() + "</font>");

			StringUtil.append(htmlContent, "</td>", currPetition.getTypeAsString(), "<td width=\"140\" align=right>", currPetition.getTypeAsString(), "</td></tr></table></td></tr>");
			color = !color;
			petcount++;
			if(petcount > 10)
			{
				htmlContent.append("<tr><td><font color=\"LEVEL\">There is more pending petition...</font><br></td></tr>");
				break;
			}
		}

		htmlContent.append("</table></center></body></html>");

		NpcHtmlMessage htmlMsg = new NpcHtmlMessage(0);
		htmlMsg.setHtml(htmlContent.toString());
		activeChar.sendPacket(htmlMsg);
	}

	public int submitPetition(L2Player petitioner, String petitionText, int petitionType)
	{
		// Create a new petition instance and add it to the list of pending petitions.
		Petition newPetition = new Petition(petitioner, petitionText, petitionType);
		int newPetitionId = newPetition.getId();
		_pendingPetitions.put(newPetitionId, newPetition);

		// Notify all GMs that a new petition has been submitted.
		String msgContent = petitioner.getName() + " has submitted a new petition."; // (ID: " + newPetitionId + ").";
		GmListTable.broadcastToGMs(new CreatureSay(0, Say2C.BATTLEFIELD, "Petition System", msgContent));

		return newPetitionId;
	}

	public void viewPetition(L2Player activeChar, int petitionId)
	{
		if(!activeChar.isGM())
			return;

		if(!isValidPetition(petitionId))
			return;

		Petition currPetition = _pendingPetitions.get(petitionId);

		NpcHtmlMessage html = new NpcHtmlMessage(0);
		html.setFile("data/html/admin/petition.htm");
		html.replace("%petition%", String.valueOf(currPetition.getId()));
		html.replace("%time%", dateFormat.format(currPetition.getCreateTime()));
		html.replace("%type%", currPetition.getTypeAsString());
		html.replace("%petitioner%", currPetition.getPetitionerName());
		html.replace("%online%", currPetition.getPetitioner() != null ? "00FF00" : "999999");
		html.replace("%text%", currPetition.getContent());

		activeChar.sendPacket(html);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final PetitionManager INSTANCE = new PetitionManager();
	}
}
