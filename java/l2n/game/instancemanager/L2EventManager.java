package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.game.event.L2Event;
import l2n.game.model.actor.L2Player;

/**
 * Менеджер для управления ивентами
 * 
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 07.10.2011
 * @time 20:00:09
 */
public final class L2EventManager
{
	public static L2EventManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final L2EventManager _instance = new L2EventManager();
	}

	private final GArray<L2Event> _events;

	public L2EventManager()
	{
		_events = new GArray<L2Event>();
	}

	public final void registerEvent(final L2Event event)
	{
		_events.add(event);
	}

	/**
	 * @param player
	 * @param answer
	 */
	public void participateAnswer(final L2Player player, final int answer)
	{
		// FIXME передавать ивент с которого идёт запрос в L2Player
		for(final L2Event event : _events)
			event.participateAnswer(player, answer);
	}
}
