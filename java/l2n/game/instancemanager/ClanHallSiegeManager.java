package l2n.game.instancemanager;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.model.entity.siege.clanhall.ClanHallSiege;
import l2n.game.model.instances.L2ClanHallMessengerInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Logger;

public class ClanHallSiegeManager extends SiegeManager
{
	private final static Logger _log = Logger.getLogger(CastleSiegeManager.class.getName());
	private static FastMap<Integer, GArray<SiegeSpawn>> _siegeBossSpawnList;
	private static FastMap<Integer, GArray<L2ClanHallMessengerInstance>> _messengersList;

	public static void load()
	{
		try
		{
			InputStream is = new FileInputStream(new File(Config.SIEGE_CLANHALL_CONFIGURATION_FILE));
			Properties siegeSettings = new Properties();
			siegeSettings.load(is);
			is.close();

			_siegeBossSpawnList = new FastMap<Integer, GArray<SiegeSpawn>>().shared();
			_messengersList = new FastMap<Integer, GArray<L2ClanHallMessengerInstance>>().shared();

			for(ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			{
				if(clanhall.getSiege() == null)
					continue;

				GArray<SiegeSpawn> _siegeBossSpawns = new GArray<SiegeSpawn>();
				GArray<L2ClanHallMessengerInstance> _messengers = new GArray<L2ClanHallMessengerInstance>();

				for(int i = 1; i < 255; i++)
				{
					String _spawnParams = siegeSettings.getProperty("N" + clanhall.getId() + "SiegeBoss" + i, "");

					if(_spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(_spawnParams.trim(), ",");
					try
					{
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
						int npc_id = Integer.parseInt(st.nextToken());

						_siegeBossSpawns.add(new SiegeSpawn(clanhall.getId(), loc, npc_id));
					}
					catch(Exception e)
					{
						_log.warning("Error while loading Siege Boss for " + clanhall.getName() + ", id " + clanhall.getId());
					}
				}

				for(int i = 1; i < 255; i++)
				{
					String _spawnParams = siegeSettings.getProperty("N" + clanhall.getId() + "Messenger" + i, "");

					if(_spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(_spawnParams.trim(), ",");
					try
					{
						int x = Integer.parseInt(st.nextToken());
						int y = Integer.parseInt(st.nextToken());
						int z = Integer.parseInt(st.nextToken());
						int heading = Integer.parseInt(st.nextToken());
						int npc_id = Integer.parseInt(st.nextToken());

						L2ClanHallMessengerInstance messenger = new L2ClanHallMessengerInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(npc_id));
						messenger.setCurrentHpMp(messenger.getMaxHp(), messenger.getMaxMp(), true);
						messenger.setXYZInvisible(x, y, GeoEngine.getHeight(x, y, z));
						messenger.setSpawnedLoc(messenger.getLoc());
						messenger.setHeading(heading);
						messenger.spawnMe();

						_messengers.add(messenger);
					}
					catch(Exception e)
					{
						_log.warning("Error while loading Messenger(s) for " + clanhall.getName() + ", id " + clanhall.getId());
					}
				}

				_siegeBossSpawnList.put(clanhall.getId(), _siegeBossSpawns);
				_messengersList.put(clanhall.getId(), _messengers);

				if(_siegeBossSpawns.isEmpty())
					_log.warning("Not found Siege Boss for " + clanhall.getName() + ", id " + clanhall.getId());
				if(_messengers.isEmpty())
					_log.warning("Not found Messengers for " + clanhall.getName() + ", id " + clanhall.getId());

				clanhall.getSiege().setDefenderRespawnDelay(Config.SIEGE_CLANHALL_DEFENDER_RESPAWN);
				clanhall.getSiege().setSiegeClanMinLevel(Config.SIEGE_CLANHALL_CLAN_MIN_LEVEL);
				clanhall.getSiege().setSiegeLength(Config.SIEGE_CLANHALL_LENGTH);

				if(clanhall.getSiege().getZone() != null)
					clanhall.getSiege().getZone().setActive(false);
				else
					_log.warning("Not found Zone for " + clanhall.getName() + ", id " + clanhall.getId());

				clanhall.getSiege().startAutoTask(true);
			}
		}
		catch(Exception e)
		{
			System.err.println("Error while loading siege data.");
			e.printStackTrace();
		}
	}

	public static GArray<SiegeSpawn> getSiegeBossSpawnList(int siegeUnitId)
	{
		return _siegeBossSpawnList.get(Integer.valueOf(siegeUnitId));
	}

	public static ClanHallSiege getSiege(L2Object activeObject)
	{
		return getSiege(activeObject.getX(), activeObject.getY());
	}

	public static ClanHallSiege getSiege(int x, int y)
	{
		for(ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			if(clanhall.getSiege() != null && clanhall.getSiege().checkIfInZone(x, y, false))
				return clanhall.getSiege();
		return null;
	}

	public static int getSiegeClanMinLevel()
	{
		return Config.SIEGE_CLANHALL_CLAN_MIN_LEVEL;
	}

	public static int getSiegeLength()
	{
		return Config.SIEGE_CLANHALL_LENGTH;
	}
}