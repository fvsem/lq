package l2n.game.instancemanager.itemauction;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:38:50
 */
public final class ItemAuctionBid
{
	private final int _playerObjId;
	private long _lastBid;

	public ItemAuctionBid(final int playerObjId, final long lastBid)
	{
		_playerObjId = playerObjId;
		_lastBid = lastBid;
	}

	public final int getPlayerObjId()
	{
		return _playerObjId;
	}

	public final long getLastBid()
	{
		return _lastBid;
	}

	final void setLastBid(final long lastBid)
	{
		_lastBid = lastBid;
	}

	final void cancelBid()
	{
		_lastBid = -1;
	}

	final boolean isCanceled()
	{
		return _lastBid == -1;
	}

	final L2Player getPlayer()
	{
		return L2ObjectsStorage.getPlayer(_playerObjId);
	}
}
