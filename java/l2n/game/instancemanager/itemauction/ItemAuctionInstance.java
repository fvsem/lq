package l2n.game.instancemanager.itemauction;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.commons.util.DateGenerator;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Rnd;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:36:25
 */
public class ItemAuctionInstance
{
	private static final Logger _log = Logger.getLogger(ItemAuctionInstance.class.getName());
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss dd.MM.yy");

	private static final long START_TIME_SPACE = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
	private static final long FINISH_TIME_SPACE = TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES);

	private final int _instanceId;
	private final AtomicInteger _auctionIds;
	private final TIntObjectHashMap<ItemAuction> _auctions;
	private final GArray<AuctionItem> _items;
	private final DateGenerator _dateGenerator;

	private ItemAuction _currentAuction;
	private ItemAuction _nextAuction;
	private ScheduledFuture<ScheduleAuctionTask> _stateTask;

	public ItemAuctionInstance(final int instanceId, final AtomicInteger auctionIds, final Node node)
	{
		_instanceId = instanceId;
		_auctionIds = auctionIds;
		_auctions = new TIntObjectHashMap<ItemAuction>();
		_items = new GArray<AuctionItem>();

		final NamedNodeMap nanode = node.getAttributes();
		final StatsSet generatorConfig = new StatsSet();
		for(int i = nanode.getLength(); i-- > 0;)
		{
			final Node n = nanode.item(i);
			if(n != null)
				generatorConfig.set(n.getNodeName(), n.getNodeValue());
		}

		_dateGenerator = DateGenerator.newGenerator(generatorConfig);

		for(Node na = node.getFirstChild(); na != null; na = na.getNextSibling())
			try
			{
				if("item".equalsIgnoreCase(na.getNodeName()))
				{
					final NamedNodeMap naa = na.getAttributes();
					final int auctionItemId = Integer.parseInt(naa.getNamedItem("auctionItemId").getNodeValue());
					final int auctionLenght = Integer.parseInt(naa.getNamedItem("auctionLenght").getNodeValue());
					final long auctionInitBid = Integer.parseInt(naa.getNamedItem("auctionInitBid").getNodeValue());

					final int itemId = Integer.parseInt(naa.getNamedItem("itemId").getNodeValue());
					final int itemCount = Integer.parseInt(naa.getNamedItem("itemCount").getNodeValue());

					if(auctionLenght < 1)
						throw new IllegalArgumentException("auctionLenght < 1 for NPC: " + _instanceId + ", itemId " + itemId);

					final StatsSet itemExtra = new StatsSet();
					final AuctionItem item = new AuctionItem(auctionItemId, auctionLenght, auctionInitBid, itemId, itemCount, itemExtra);

					if(!item.checkItemExists())
						throw new IllegalArgumentException("Item with id " + itemId + " not found");

					for(final AuctionItem tmp : _items)
						if(tmp.getAuctionItemId() == auctionItemId)
							throw new IllegalArgumentException("Dublicated auction item id " + auctionItemId);

					_items.add(item);

					for(Node nb = na.getFirstChild(); nb != null; nb = nb.getNextSibling())
						if("extra".equalsIgnoreCase(nb.getNodeName()))
						{
							final NamedNodeMap nab = nb.getAttributes();
							for(int i = nab.getLength(); i-- > 0;)
							{
								final Node n = nab.item(i);
								if(n != null)
									itemExtra.set(n.getNodeName(), n.getNodeValue());
							}
						}
				}
			}
			catch(final IllegalArgumentException e)
			{
				_log.log(Level.WARNING, "ItemAuctionInstance: Failed loading auction item", e);
			}

		if(_items.isEmpty())
			throw new IllegalArgumentException("No items defined");

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT auctionId FROM item_auction WHERE instanceId=?");
			statement.setInt(1, _instanceId);
			rset = statement.executeQuery();
			while (rset.next())
			{
				final int auctionId = rset.getInt(1);
				try
				{
					final ItemAuction auction = loadAuction(auctionId);
					if(auction != null)
						_auctions.put(auctionId, auction);
					else
						ItemAuctionManager.deleteAuction(auctionId);
				}
				catch(final SQLException e)
				{
					_log.log(Level.WARNING, "ItemAuctionInstance: Failed loading auction: " + auctionId, e);
				}
			}
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "L2ItemAuctionInstance: Failed loading auctions.", e);
			return;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_log.log(Level.INFO, "ItemAuction: loaded " + _items.size() + " item(s), registered " + _auctions.size() + " auction(s) for NPC " + _instanceId + ".");
		checkAndSetCurrentAndNextAuction();
	}

	public final ItemAuction getCurrentAuction()
	{
		return _currentAuction;
	}

	public final ItemAuction getNextAuction()
	{
		return _nextAuction;
	}

	public final void shutdown()
	{
		final ScheduledFuture<ScheduleAuctionTask> stateTask = _stateTask;
		if(stateTask != null)
			stateTask.cancel(false);
	}

	private final AuctionItem getAuctionItem(final int auctionItemId)
	{
		for(int i = _items.size(); i-- > 0;)
		{
			final AuctionItem item = _items.getUnsafe(i);
			if(item.getAuctionItemId() == auctionItemId)
				return item;
		}
		return null;
	}

	private final void checkAndSetCurrentAndNextAuction()
	{
		final ItemAuction[] auctions = _auctions.values(new ItemAuction[_auctions.size()]);

		ItemAuction currentAuction = null;
		ItemAuction nextAuction = null;

		switch (auctions.length)
		{
			case 0:
			{
				nextAuction = createAuction(System.currentTimeMillis() + START_TIME_SPACE);
				break;
			}
			case 1:
			{
				switch (auctions[0].getAuctionState())
				{
					case CREATED:
					{
						if(auctions[0].getStartingTime() < System.currentTimeMillis() + START_TIME_SPACE)
						{
							currentAuction = auctions[0];
							nextAuction = createAuction(System.currentTimeMillis() + START_TIME_SPACE);
						}
						else
							nextAuction = auctions[0];
						break;
					}
					case STARTED:
					{
						currentAuction = auctions[0];
						nextAuction = createAuction(Math.max(currentAuction.getEndingTime() + FINISH_TIME_SPACE, System.currentTimeMillis() + START_TIME_SPACE));
						break;
					}
					case FINISHED:
					{
						currentAuction = auctions[0];
						nextAuction = createAuction(System.currentTimeMillis() + START_TIME_SPACE);
						break;
					}
					default:
						throw new IllegalArgumentException();
				}
				break;
			}
			default:
			{
				Arrays.sort(auctions, new Comparator<ItemAuction>()
				{
					@Override
					public final int compare(final ItemAuction o1, final ItemAuction o2)
					{
						return ((Long) o2.getStartingTime()).compareTo(o1.getStartingTime());
					}
				});

				// just to make sure we won`t skip any auction because of little different times
				final long currentTime = System.currentTimeMillis();
				for(int i = 0; i < auctions.length; i++)
				{
					final ItemAuction auction = auctions[i];
					if(auction.getAuctionState() == ItemAuctionState.STARTED)
					{
						currentAuction = auction;
						break;
					}
					else if(auction.getStartingTime() <= currentTime)
						currentAuction = auction;
				}

				for(int i = 0; i < auctions.length; i++)
				{
					final ItemAuction auction = auctions[i];
					if(auction.getStartingTime() > currentTime && currentAuction != auction)
					{
						nextAuction = auction;
						break;
					}
				}

				if(nextAuction == null)
					nextAuction = createAuction(System.currentTimeMillis() + START_TIME_SPACE);
				break;
			}
		}

		_auctions.put(nextAuction.getAuctionId(), nextAuction);

		_currentAuction = currentAuction;
		_nextAuction = nextAuction;

		if(currentAuction != null && currentAuction.getAuctionState() != ItemAuctionState.FINISHED)
		{
			if(currentAuction.getAuctionState() == ItemAuctionState.STARTED)
				setStateTask(L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleAuctionTask(currentAuction), Math.max(currentAuction.getEndingTime() - System.currentTimeMillis(), 0L)));
			else
				setStateTask(L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleAuctionTask(currentAuction), Math.max(currentAuction.getStartingTime() - System.currentTimeMillis(), 0L)));
			_log.log(Level.INFO, "ItemAuction: Schedule current auction " + currentAuction.getAuctionId() + " for NPC " + _instanceId);
		}
		else
		{
			setStateTask(L2GameThreadPools.getInstance().scheduleGeneral(new ScheduleAuctionTask(nextAuction), Math.max(nextAuction.getStartingTime() - System.currentTimeMillis(), 0L)));
			_log.log(Level.INFO, "ItemAuction: Schedule next auction " + nextAuction.getAuctionId() + " on " + DATE_FORMAT.format(nextAuction.getStartingTime()) + " for NPC " + _instanceId);
		}
	}

	public final ItemAuction getAuction(final int auctionId)
	{
		return _auctions.get(auctionId);
	}

	public final ItemAuction[] getAuctionsByBidder(final int bidderObjId)
	{
		final ItemAuction[] auctions = getAuctions();
		final GArray<ItemAuction> stack = new GArray<ItemAuction>(auctions.length);
		for(final ItemAuction auction : getAuctions())
			if(auction.getAuctionState() != ItemAuctionState.CREATED)
			{
				final ItemAuctionBid bid = auction.getBidFor(bidderObjId);
				if(bid != null)
					stack.addLastUnsafe(auction);
			}
		return stack.toArray(new ItemAuction[stack.size()]);
	}

	public final ItemAuction[] getAuctions()
	{
		final ItemAuction[] auctions;

		synchronized (_auctions)
		{
			auctions = _auctions.values(new ItemAuction[_auctions.size()]);
		}

		return auctions;
	}

	private final class ScheduleAuctionTask implements Runnable
	{
		private final ItemAuction _auction;

		public ScheduleAuctionTask(final ItemAuction auction)
		{
			_auction = auction;
		}

		@Override
		public final void run()
		{
			try
			{
				runImpl();
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "L2ItemAuctionInstance: Failed scheduling auction " + _auction.getAuctionId(), e);
			}
		}

		private final void runImpl() throws Exception
		{
			final ItemAuctionState state = _auction.getAuctionState();
			_log.log(Level.SEVERE, "runImpl " + _auction.getAuctionId() + ", " + state.toString());
			switch (state)
			{
				case CREATED:
				{
					if(!_auction.setAuctionState(state, ItemAuctionState.STARTED))
						throw new IllegalStateException("Could not set auction state: " + ItemAuctionState.STARTED.toString() + ", expected: " + state.toString());

					_log.log(Level.INFO, "ItemAuction: auction " + _auction.getAuctionId() + " has started for NPC " + _auction.getInstanceId());

					// говорим что аукцион начался
					Announcements.announceToAll(Msg.THE_AUCTION_HAS_BEGUN);
					checkAndSetCurrentAndNextAuction();
					break;
				}
				case STARTED:
				{
					switch (_auction.getAuctionEndingExtendState())
					{
						case 1:
						{
							if(_auction.getScheduledAuctionEndingExtendState() == 0)
							{
								_auction.setScheduledAuctionEndingExtendState((byte) 1);
								setStateTask(L2GameThreadPools.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
							break;
						}

						case 2:
						{
							if(_auction.getScheduledAuctionEndingExtendState() != 2)
							{
								_auction.setScheduledAuctionEndingExtendState((byte) 2);
								setStateTask(L2GameThreadPools.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
							break;
						}
					}

					if(!_auction.setAuctionState(state, ItemAuctionState.FINISHED))
						throw new IllegalStateException("Could not set auction state: " + ItemAuctionState.FINISHED.toString() + ", expected: " + state.toString());

					onAuctionFinished(_auction);
					checkAndSetCurrentAndNextAuction();
					break;
				}
				default:
					throw new IllegalStateException("Invalid state: " + state);
			}
		}
	}

	private final void onAuctionFinished(final ItemAuction auction)
	{
		final ItemAuctionBid bid = auction.getHighestBid();
		auction.broadcastToAllBiddersInternal(new SystemMessage(SystemMessage.S1_S_AUCTION_HAS_ENDED).addItemName(auction.getItemInfo().getItemId()));
		// выдаём предмет победителю
		if(bid != null)
			auction.giveItemToWinner(bid, auction.getAuctionId(), _instanceId);
		else
			_log.log(Level.INFO, "ItemAuction: auction " + auction.getAuctionId() + " has finished. There have not been any bid for NPC " + _instanceId);
	}

	private final void setStateTask(final ScheduledFuture<ScheduleAuctionTask> future)
	{
		final ScheduledFuture<ScheduleAuctionTask> stateTask = _stateTask;
		if(stateTask != null)
			stateTask.cancel(false);
		_stateTask = future;
	}

	private final ItemAuction createAuction(final long after)
	{
		final AuctionItem auctionItem = _items.get(Rnd.get(_items.size()));
		final long startingTime = _dateGenerator.nextDate(after);
		final long endingTime = startingTime + TimeUnit.MILLISECONDS.convert(auctionItem.getAuctionLength(), TimeUnit.MINUTES);
		final ItemAuction auction = new ItemAuction(_auctionIds.getAndIncrement(), _instanceId, startingTime, endingTime, auctionItem);
		auction.storeMe();
		return auction;
	}

	private final ItemAuction loadAuction(final int auctionId) throws SQLException
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT auctionItemId, startingTime, endingTime, auctionStateId FROM item_auction WHERE auctionId=?");
			statement.setInt(1, auctionId);
			rset = statement.executeQuery();

			if(!rset.next())
			{
				_log.log(Level.WARNING, "ItemAuctionInstance: Auction data not found for auction: " + auctionId);
				return null;
			}

			final int auctionItemId = rset.getInt(1);
			final long startingTime = rset.getLong(2);
			final long endingTime = rset.getLong(3);
			final byte auctionStateId = rset.getByte(4);
			statement.close();

			if(startingTime >= endingTime)
			{
				_log.log(Level.WARNING, "ItemAuctionInstance: Invalid starting/ending paramaters for auction: " + auctionId);
				return null;
			}

			final AuctionItem auctionItem = getAuctionItem(auctionItemId);
			if(auctionItem == null)
			{
				_log.log(Level.WARNING, "ItemAuctionInstance: AuctionItem: " + auctionItemId + ", not found for auction: " + auctionId);
				return null;
			}

			final ItemAuctionState auctionState = ItemAuctionState.stateForStateId(auctionStateId);
			if(auctionState == null)
			{
				_log.log(Level.WARNING, "ItemAuctionInstance: Invalid auctionStateId: " + auctionStateId + ", for auction: " + auctionId);
				return null;
			}

			statement = con.prepareStatement("SELECT playerObjId, playerBid FROM item_auction_bid WHERE auctionId=?");
			statement.setInt(1, auctionId);
			rset = statement.executeQuery();

			final GArray<ItemAuctionBid> auctionBids = new GArray<ItemAuctionBid>();
			ItemAuctionBid bid;
			while (rset.next())
			{
				final int playerObjId = rset.getInt(1);
				final long playerBid = rset.getLong(2);
				bid = new ItemAuctionBid(playerObjId, playerBid);
				auctionBids.add(bid);
			}

			return new ItemAuction(auctionId, _instanceId, startingTime, endingTime, auctionItem, auctionBids, auctionState);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}
}
