package l2n.game.instancemanager.itemauction;

import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.cache.Msg;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.network.model.ItemInfo;
import l2n.game.tables.ItemTable;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:39:29
 */
public final class AuctionItem
{
	private static final Logger _log = Logger.getLogger(AuctionItem.class.getName());

	private final int _auctionItemId;
	private final int _auctionLength;
	private final long _auctionInitBid;

	private final int _itemId;
	private final long _itemCount;
	private final StatsSet _itemExtra;

	public AuctionItem(final int auctionItemId, final int auctionLength, final long auctionInitBid, final int itemId, final long itemCount, final StatsSet itemExtra)
	{
		_auctionItemId = auctionItemId;
		_auctionLength = auctionLength;
		_auctionInitBid = auctionInitBid;

		_itemId = itemId;
		_itemCount = itemCount;
		_itemExtra = itemExtra;
	}

	public final boolean checkItemExists()
	{
		return ItemTable.getInstance().isExists(_itemId);
	}

	public final int getAuctionItemId()
	{
		return _auctionItemId;
	}

	public final int getAuctionLength()
	{
		return _auctionLength;
	}

	public final long getAuctionInitBid()
	{
		return _auctionInitBid;
	}

	public final int getItemId()
	{
		return _itemId;
	}

	public final long getItemCount()
	{
		return _itemCount;
	}

	public final ItemInfo getItemInfo()
	{
		L2ItemInstance item = new L2ItemInstance(IdFactory.getInstance().getNextId(), ItemTable.getInstance().getTemplate(_itemId), 0, _auctionItemId, "AuctionItem", false);
		item.setCount(_itemCount);
		final int enchantLevel = _itemExtra.getInteger("enchant_level", 0);
		item.setEnchantLevel(enchantLevel);
		final ItemInfo info = new ItemInfo(item);
		item = null;
		return info;
	}

	/**
	 * выдаёт вещь победителю на склад. Игрок может быть оффлайн.
	 */
	public final void giveItemToWinner(final ItemAuctionBid bid, final int auctionId, final int instanceId)
	{
		final L2Player winner = L2ObjectsStorage.getPlayer(bid.getPlayerObjId());
		// победитель в игре? отлично, используем стандартный механизм
		if(winner != null)
		{
			final L2ItemInstance item = ItemTable.getInstance().createItem(_itemId, 0, _auctionItemId, "AuctionItem");
			item.setCount(_itemCount);
			final int enchantLevel = _itemExtra.getInteger("enchant_level", 0);
			item.setEnchantLevel(enchantLevel);

			winner.getWarehouse().addItem(item, "ItemAuction");
			winner.sendPacket(Msg.WON_BID_ITEM_CAN_BE_FOUND_IN_WAREHOUSE);

			_log.log(Level.INFO, "AuctionItem: Auction " + auctionId + " has finished. Highest bid by (name) " + winner.getName() + " for instance " + instanceId);
		}
		else if(!ItemTable.getInstance().getTemplate(_itemId).isStackable()) // нестекуемые вещи можно возвращать без проблем
		{
			final L2ItemInstance item = new L2ItemInstance(IdFactory.getInstance().getNextId(), ItemTable.getInstance().getTemplate(_itemId), 0, _auctionItemId, "AuctionItem", false);
			item.setCount(_itemCount);
			final int enchantLevel = _itemExtra.getInteger("enchant_level", 0);
			item.setEnchantLevel(enchantLevel);

			item.setLocation(ItemLocation.WAREHOUSE);
			item.updateDatabase(true);

			_log.log(Level.INFO, "AuctionItem: Auction " + auctionId + " has finished. Highest bid by (id) " + bid.getPlayerObjId() + " for instance " + instanceId);
		}
		else
		{
			// стекуемые проверяем на коллизии, хотя обрабатывает он корректно и несколько одинаковых, но это некрасиво
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			ThreadConnection con = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT object_id FROM items WHERE owner_id = ? AND item_id = ? AND loc = 'WAREHOUSE' LIMIT 1"); // сперва пробуем найти в базе его вещь на складе
				statement.setInt(1, bid.getPlayerObjId());
				statement.setInt(2, _itemId);
				rs = statement.executeQuery();
				if(rs.next())
				{
					final int id = rs.getInt("object_id");
					DbUtils.close(statement);
					statement = con.prepareStatement("UPDATE items SET count=count+? WHERE object_id = ? LIMIT 1"); // если нашли увеличиваем ее количество
					statement.setLong(1, _itemCount);
					statement.setInt(2, id);
					statement.executeUpdate();
				}
				// если не нашли такие же
				else
				{
					final L2ItemInstance item = new L2ItemInstance(IdFactory.getInstance().getNextId(), ItemTable.getInstance().getTemplate(_itemId), 0, _auctionItemId, "AuctionItem", false);
					item.setCount(_itemCount);
					final int enchantLevel = _itemExtra.getInteger("enchant_level", 0);
					item.setEnchantLevel(enchantLevel);

					item.setLocation(ItemLocation.WAREHOUSE);
					item.updateDatabase(true);
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}

			_log.log(Level.INFO, "AuctionItem: Auction " + auctionId + " has finished. Highest bid by (id) " + bid.getPlayerObjId() + " for instance " + instanceId);
		}
	}
}
