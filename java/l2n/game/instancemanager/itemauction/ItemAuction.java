package l2n.game.instancemanager.itemauction;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.model.ItemInfo;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Item;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:39:11
 */
public final class ItemAuction
{
	private static final Logger _log = Logger.getLogger(ItemAuctionManager.class.getName());

	private static final long ENDING_TIME_EXTEND_5 = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);
	private static final long ENDING_TIME_EXTEND_8 = TimeUnit.MILLISECONDS.convert(8, TimeUnit.MINUTES);

	private final int _auctionId;
	private final int _instanceId;
	private final long _startingTime;
	private final long _endingTime;
	private final AuctionItem _auctionItem;
	private final GArray<ItemAuctionBid> _auctionBids;
	private final Object _auctionStateLock;

	private ItemAuctionState _auctionState;
	private byte _scheduledAuctionEndingExtendState;
	private byte _auctionEndingExtendState;

	private final ItemInfo _itemInfo;

	private ItemAuctionBid _highestBid;
	private int _lastBidPlayerObjId;

	public ItemAuction(final int auctionId, final int instanceId, final long startingTime, final long endingTime, final AuctionItem auctionItem)
	{
		this(auctionId, instanceId, startingTime, endingTime, auctionItem, new GArray<ItemAuctionBid>(), ItemAuctionState.CREATED);
	}

	public ItemAuction(final int auctionId, final int instanceId, final long startingTime, final long endingTime, final AuctionItem auctionItem, final GArray<ItemAuctionBid> auctionBids, final ItemAuctionState auctionState)
	{
		_auctionId = auctionId;
		_instanceId = instanceId;
		_startingTime = startingTime;
		_endingTime = endingTime;
		_auctionItem = auctionItem;
		_auctionBids = auctionBids;
		_auctionState = auctionState;
		_auctionStateLock = new Object();

		_itemInfo = _auctionItem.getItemInfo();

		for(final ItemAuctionBid bid : _auctionBids)
			if(_highestBid == null || _highestBid.getLastBid() < bid.getLastBid())
				_highestBid = bid;
		_log.log(Level.INFO, "Restoring/Creating auction " + auctionId + ", highest bid: " + (_highestBid != null ? "(" + _highestBid.getPlayerObjId() + ", " + _highestBid.getLastBid() + ")" : "none made"));
	}

	public final ItemAuctionState getAuctionState()
	{
		final ItemAuctionState auctionState;

		synchronized (_auctionStateLock)
		{
			auctionState = _auctionState;
		}

		return auctionState;
	}

	public final boolean setAuctionState(final ItemAuctionState expected, final ItemAuctionState wanted)
	{
		synchronized (_auctionStateLock)
		{
			if(_auctionState != expected)
				return false;

			_auctionState = wanted;
			storeMe();

			return true;
		}
	}

	public final int getAuctionId()
	{
		return _auctionId;
	}

	public final int getInstanceId()
	{
		return _instanceId;
	}

	public final ItemInfo getItemInfo()
	{
		return _itemInfo;
	}

	public final void giveItemToWinner(final ItemAuctionBid bid, final int auctionId, final int instanceId)
	{
		_auctionItem.giveItemToWinner(bid, auctionId, instanceId);
	}

	public final long getAuctionInitBid()
	{
		return _auctionItem.getAuctionInitBid();
	}

	public final ItemAuctionBid getHighestBid()
	{
		return _highestBid;
	}

	public final byte getAuctionEndingExtendState()
	{
		return _auctionEndingExtendState;
	}

	public final byte getScheduledAuctionEndingExtendState()
	{
		return _scheduledAuctionEndingExtendState;
	}

	public final void setScheduledAuctionEndingExtendState(final byte state)
	{
		_scheduledAuctionEndingExtendState = state;
	}

	public final long getStartingTime()
	{
		return _startingTime;
	}

	public final long getEndingTime()
	{
		if(_auctionEndingExtendState == 0)
			return _endingTime;
		else if(_auctionEndingExtendState == 1)
			return _endingTime + ENDING_TIME_EXTEND_5;
		else
			return _endingTime + ENDING_TIME_EXTEND_8;
	}

	public final long getStartingTimeRemaining()
	{
		return Math.max(getEndingTime() - System.currentTimeMillis(), 0L);
	}

	public final long getFinishingTimeRemaining()
	{
		return Math.max(getEndingTime() - System.currentTimeMillis(), 0L);
	}

	public final void storeMe()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO item_auction (auctionId,instanceId,auctionItemId,startingTime,endingTime,auctionStateId) VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE auctionStateId=?");
			statement.setInt(1, _auctionId);
			statement.setInt(2, _instanceId);
			statement.setInt(3, _auctionItem.getAuctionItemId());
			statement.setLong(4, _startingTime);
			statement.setLong(5, _endingTime);
			statement.setByte(6, _auctionState.getStateId());
			statement.setByte(7, _auctionState.getStateId());
			statement.execute();
		}
		catch(final SQLException e)
		{
			_log.log(Level.WARNING, "ItemAuction.storeMe() ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public final int getAndSetLastBidPlayerObjectId(final int playerObjId)
	{
		final int lastBid = _lastBidPlayerObjId;
		_lastBidPlayerObjId = playerObjId;
		return lastBid;
	}

	private final void updatePlayerBid(final ItemAuctionBid bid, final boolean delete)
	{
		updatePlayerBidInternal(bid, delete);
	}

	private final void updatePlayerBidInternal(final ItemAuctionBid bid, final boolean delete)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			if(delete)
			{
				statement = con.prepareStatement("DELETE FROM item_auction_bid WHERE auctionId=? AND playerObjId=?");
				statement.setInt(1, _auctionId);
				statement.setInt(2, bid.getPlayerObjId());
			}
			else
			{
				_log.log(Level.INFO, "ItemAuction: storing bid, auction: " + _auctionId + ", player: " + bid.getPlayerObjId() + ", bid: " + bid.getLastBid());
				statement = con.prepareStatement("INSERT INTO item_auction_bid (auctionId,playerObjId,playerBid) VALUES (?,?,?) ON DUPLICATE KEY UPDATE playerBid=?");
				statement.setInt(1, _auctionId);
				statement.setInt(2, bid.getPlayerObjId());
				statement.setLong(3, bid.getLastBid());
				statement.setLong(4, bid.getLastBid());
			}

			statement.execute();
		}
		catch(final SQLException e)
		{
			_log.log(Level.WARNING, "ItemAuction.updatePlayerBidInternal(ItemAuctionBid, boolean) ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public final void registerBid(final L2Player player, final long newBid)
	{
		if(player == null)
			throw new NullPointerException();

		if(newBid < getAuctionInitBid())
		{
			player.sendPacket(Msg.BID_PRICE_MUST_BE_HIGHER);
			return;
		}

		if(newBid > Config.ALT_ITEM_AUCTION_MAX_BID)
		{
			if(Config.ALT_ITEM_AUCTION_MAX_BID == 2100000000)
				player.sendPacket(Msg.BID_CANT_EXCEED_2_1_BILLION);
			else
				player.sendMessage("Your bid cannot exceed " + Config.ALT_ITEM_AUCTION_MAX_BID);
			return;
		}

		if(getAuctionState() != ItemAuctionState.STARTED)
			return;

		final int playerObjId = player.getObjectId();
		synchronized (_auctionBids)
		{
			if(_highestBid != null && newBid < _highestBid.getLastBid())
			{
				player.sendPacket(Msg.BID_MUST_BE_HIGHER_THAN_CURRENT_BID);
				return;
			}

			final int bidIndex = getBidIndexFor(playerObjId);
			if(bidIndex == -1)
			{
				if(!reduceItemCount(player, newBid))
					return;

				final ItemAuctionBid bid = new ItemAuctionBid(playerObjId, newBid);
				_auctionBids.add(bid);
				onPlayerBid(player, bid);
				updatePlayerBid(bid, false);

				player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_SUBMITTED_A_BID_IN_THE_AUCTION_OF_S1).addLong(newBid));
				return;
			}

			if(!Config.ALT_ITEM_AUCTION_CAN_REBID)
			{
				player.sendPacket(Msg.ALREADY_SUBMITTED_BID);
				return;
			}

			final ItemAuctionBid oldBid = _auctionBids.getUnsafe(bidIndex);
			if(oldBid.getLastBid() >= newBid)
			{
				player.sendPacket(Msg.BID_AMOUNT_HIGHER_THAN_PREVIOUS_BID);
				return;
			}

			if(!reduceItemCount(player, newBid - oldBid.getLastBid()))
				return;

			oldBid.setLastBid(newBid);
			onPlayerBid(player, oldBid);
			updatePlayerBid(oldBid, false);
			player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_SUBMITTED_A_BID_IN_THE_AUCTION_OF_S1).addLong(newBid));
		}
	}

	private final void onPlayerBid(final L2Player player, final ItemAuctionBid bid)
	{
		if(_highestBid == null)
			_highestBid = bid;
		else if(_highestBid.getLastBid() < bid.getLastBid())
		{
			final L2Player old = _highestBid.getPlayer();
			if(old != null)
				old.sendPacket(Msg.YOU_HAVE_BEEN_OUTBID);
			_highestBid = bid;
		}

		if(getEndingTime() - System.currentTimeMillis() <= 1000 * 60 * 10)
			if(_auctionEndingExtendState == 0)
			{
				_auctionEndingExtendState = 1;
				broadcastToAllBidders(Msg.BIDDER_EXISTS__THE_AUCTION_TIME_HAS_BEEN_EXTENDED_BY_5_MINUTES);
			}
			else if(_auctionEndingExtendState == 1 && getAndSetLastBidPlayerObjectId(player.getObjectId()) != player.getObjectId())
			{
				_auctionEndingExtendState = 2;
				broadcastToAllBidders(Msg.BIDDER_EXISTS__AUCTION_TIME_HAS_BEEN_EXTENDED_BY_3_MINUTES);
			}
	}

	public final void broadcastToAllBidders(final L2GameServerPacket packet)
	{
		L2GameThreadPools.getInstance().executeGeneral(new Runnable()
		{
			@Override
			public final void run()
			{
				broadcastToAllBiddersInternal(packet);
			}
		});
	}

	public final void broadcastToAllBiddersInternal(final L2GameServerPacket packet)
	{
		L2Player player;
		ItemAuctionBid bid;
		for(int i = _auctionBids.size(); i-- > 0;)
		{
			bid = _auctionBids.getUnsafe(i);
			if(bid != null)
			{
				player = bid.getPlayer();
				if(player != null)
					player.sendPacket(packet);
			}
		}
	}

	public final void cancelBid(final L2Player player)
	{
		if(player == null)
			throw new NullPointerException();

		switch (getAuctionState())
		{
			case CREATED:
				return;
			case FINISHED:
				if(_startingTime < System.currentTimeMillis() - Config.ALT_ITEM_AUCTION_MAX_CANCEL_TIME_IN_MILLIS)
					return;
				else
					break;
		}

		final int playerObjId = player.getObjectId();

		synchronized (_auctionBids)
		{
			// _highestBid == null logical consequence is that no one bet yet
			if(_highestBid == null)
			{
				player.sendMessage("_highestBid == null logical consequence is that no one bet yet");
				return;
			}

			final int bidIndex = getBidIndexFor(playerObjId);
			if(bidIndex == -1)
			{
				player.sendMessage("You cannot cancel ur bid: U did not made one yet.");
				return;
			}

			final ItemAuctionBid bid = _auctionBids.getUnsafe(bidIndex);
			if(bid.getPlayerObjId() == _highestBid.getPlayerObjId())
			{
				player.sendMessage("You cannot cancel ur bid: U have the highest bid.");
				return;
			}

			if(bid.isCanceled())
			{
				player.sendMessage("Bid already canceled.");
				return;
			}

			increaseItemCount(player, bid.getLastBid());
			player.sendPacket(Msg.YOU_HAVE_CANCELED_YOUR_BID);

			if(Config.ALT_ITEM_AUCTION_CAN_REBID)
			{
				_auctionBids.removeUnsafeVoid(bidIndex);
				updatePlayerBid(bid, true);
			}
			else
			{
				bid.cancelBid();
				updatePlayerBid(bid, false);
			}
		}
	}

	private final boolean reduceItemCount(final L2Player player, final long count)
	{
		if(Config.ALT_ITEM_AUCTION_BID_ITEM_ID == 57)
		{
			if(player.getAdena() < count)
			{
				player.sendPacket(Msg.NOT_ENOUGH_ADENA_FOR_THIS_BID);
				return false;
			}
			player.reduceAdena(count, true);
			return true;
		}

		if(player.getInventory().getCountOf(Config.ALT_ITEM_AUCTION_BID_ITEM_ID) < count)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
			return false;
		}
		player.destroyItemByItemId(Config.ALT_ITEM_AUCTION_BID_ITEM_ID, count, true);
		return true;
	}

	private final void increaseItemCount(final L2Player player, final long count)
	{
		if(Config.ALT_ITEM_AUCTION_BID_ITEM_ID == 57)
			Functions.addItem(player, L2Item.ITEM_ID_ADENA, count);
		else
			Functions.addItem(player, Config.ALT_ITEM_AUCTION_BID_ITEM_ID, count);
	}

	/**
	 * Returns the last bid for the given player or -1 if he did not made one yet.
	 * 
	 * @param player
	 *            The player that made the bid
	 * @return The last bid the player made or -1
	 */
	public final long getLastBid(final L2Player player)
	{
		final ItemAuctionBid bid = getBidFor(player.getObjectId());
		return bid != null ? bid.getLastBid() : -1L;
	}

	public final ItemAuctionBid getBidFor(final int playerObjId)
	{
		final int index = getBidIndexFor(playerObjId);
		return index != -1 ? _auctionBids.getUnsafe(index) : null;
	}

	private final int getBidIndexFor(final int playerObjId)
	{
		for(int i = _auctionBids.size(); i-- > 0;)
		{
			final ItemAuctionBid bid = _auctionBids.getUnsafe(i);
			if(bid != null && bid.getPlayerObjId() == playerObjId)
				return i;
		}
		return -1;
	}
}
