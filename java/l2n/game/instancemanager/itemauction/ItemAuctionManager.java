package l2n.game.instancemanager.itemauction;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 6:36:03
 */
public class ItemAuctionManager
{
	private static final Logger _log = Logger.getLogger(ItemAuctionManager.class.getName());

	public static final ItemAuctionManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	private final TIntObjectHashMap<ItemAuctionInstance> _managerInstances;
	private final AtomicInteger _auctionIds;

	private ItemAuctionManager()
	{
		_managerInstances = new TIntObjectHashMap<ItemAuctionInstance>();
		_auctionIds = new AtomicInteger(1);

		if(!Config.ALT_ITEM_AUCTION_ENABLED)
		{
			_log.log(Level.INFO, "ItemAuctionManager: Disabled by config.");
			return;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT auctionId FROM item_auction ORDER BY auctionId DESC LIMIT 0, 1");
			rset = statement.executeQuery();
			if(rset.next())
				_auctionIds.set(rset.getInt(1) + 1);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "ItemAuctionManager: Failed loading auctions.", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		final File file = new File(Config.DATAPACK_ROOT, "data/item_auctions.xml");
		if(!file.exists())
		{
			_log.log(Level.WARNING, "ItemAuctionManager: Missing item_auctions.xml!");
			return;
		}

		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			final Document doc = factory.newDocumentBuilder().parse(file);
			for(Node na = doc.getFirstChild(); na != null; na = na.getNextSibling())
				if("list".equalsIgnoreCase(na.getNodeName()))
					for(Node nb = na.getFirstChild(); nb != null; nb = nb.getNextSibling())
						if("instance".equalsIgnoreCase(nb.getNodeName()))
						{
							final NamedNodeMap nab = nb.getAttributes();
							final int instanceId = Integer.parseInt(nab.getNamedItem("id").getNodeValue());

							if(_managerInstances.containsKey(instanceId))
								throw new Exception("Dublicated instanceId " + instanceId);

							final ItemAuctionInstance instance = new ItemAuctionInstance(instanceId, _auctionIds, nb);
							_managerInstances.put(instanceId, instance);
						}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error on loading file - " + file.getName() + ": " + e.getMessage(), e);
		}

		_log.log(Level.INFO, "ItemAuctionManager: Loaded " + _managerInstances.size() + " instance(s).");
	}

	public final void shutdown()
	{
		final ItemAuctionInstance[] instances = _managerInstances.values(new ItemAuctionInstance[_managerInstances.size()]);
		for(final ItemAuctionInstance instance : instances)
			instance.shutdown();
	}

	public final ItemAuctionInstance getManagerInstance(final int instanceId)
	{
		return _managerInstances.get(instanceId);
	}

	public final int getNextAuctionId()
	{
		return _auctionIds.getAndIncrement();
	}

	public final static void deleteAuction(final int auctionId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM item_auction WHERE auctionId=?");
			statement.setInt(1, auctionId);
			statement.execute();
			statement.close();

			statement = con.prepareStatement("DELETE FROM item_auction_bid WHERE auctionId=?");
			statement.setInt(1, auctionId);
			statement.execute();
		}
		catch(final SQLException e)
		{
			_log.log(Level.SEVERE, "L2ItemAuctionManagerInstance: Failed deleting auction: " + auctionId, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ItemAuctionManager INSTANCE = new ItemAuctionManager();
	}
}
