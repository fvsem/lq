package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.sql.ResultSet;
import java.util.logging.Logger;

public class SiegeGuardManager
{
	private final static Logger _log = Logger.getLogger(SiegeGuardManager.class.getName());

	private Residence _castle;
	private GArray<L2Spawn> _siegeGuardSpawn = new GArray<L2Spawn>();

	public SiegeGuardManager(Residence castle)
	{
		_castle = castle;
	}

	/**
	 * Add guard.<BR>
	 * <BR>
	 */
	public void addSiegeGuard(L2Player activeChar, int npcId)
	{
		if(activeChar == null)
			return;
		addSiegeGuard(activeChar.getLoc(), npcId);
	}

	/**
	 * Add guard.<BR>
	 * <BR>
	 */
	public void addSiegeGuard(Location loc, int npcId)
	{
		saveSiegeGuard(loc, npcId, 0);
	}

	/**
	 * Hire merc.<BR>
	 * <BR>
	 */
	public void hireMerc(L2Player activeChar, int npcId)
	{
		if(activeChar == null)
			return;
		hireMerc(activeChar.getLoc(), npcId);
	}

	/**
	 * Hire merc.<BR>
	 * <BR>
	 */
	public void hireMerc(Location loc, int npcId)
	{
		saveSiegeGuard(loc, npcId, 1);
	}

	/**
	 * Remove a single mercenary, identified by the npcId and location. Presumably, this is used when a castle lord picks up a previously dropped ticket
	 */
	public void removeMerc(int npcId, Location loc)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("Delete From siege_guards Where npcId = ? And x = ? AND y = ? AND z = ? AND isHired = 1");
			statement.setInt(1, npcId);
			statement.setInt(2, loc.x);
			statement.setInt(3, loc.y);
			statement.setInt(4, loc.z);
			statement.execute();
		}
		catch(Exception e1)
		{
			_log.warning("Error deleting hired siege guard at " + loc.toString() + ":" + e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Remove mercs.<BR>
	 * <BR>
	 */
	public static void removeMercsFromDb(int castleId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("Delete From siege_guards Where unitId = ? And isHired = 1");
			statement.setInt(1, castleId);
			statement.execute();
		}
		catch(Exception e1)
		{
			_log.warning("Error deleting hired siege guard for castle " + castleId + ":" + e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	/**
	 * Spawn guards.<BR>
	 * <BR>
	 */
	public void spawnSiegeGuard()
	{
		unspawnSiegeGuard();
		loadSiegeGuard();
		for(L2Spawn spawn : _siegeGuardSpawn)
			if(spawn != null)
			{
				spawn.init();
				if(spawn.getNativeRespawnDelay() == 0)
					spawn.stopRespawn();
			}
	}

	/**
	 * Unspawn guards.<BR>
	 * <BR>
	 */
	public void unspawnSiegeGuard()
	{
		for(L2Spawn spawn : _siegeGuardSpawn)
		{
			if(spawn == null)
				continue;

			spawn.stopRespawn();
			if(spawn.getLastSpawn() != null)
				spawn.getLastSpawn().deleteMe();
		}

		getSiegeGuardSpawn().clear();
	}

	/**
	 * Load guards.<BR>
	 * <BR>
	 */
	private void loadSiegeGuard()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM siege_guards Where unitId = ? And isHired = ?");
			statement.setInt(1, getSiegeUnit().getId());
			if(getSiegeUnit().getOwnerId() > 0 && getSiegeUnit().getType() != ResidenceType.Fortress)// If castle is owned by a clan, then don't spawn default guards
				statement.setInt(2, 1);
			else
				statement.setInt(2, 0);
			rset = statement.executeQuery();

			L2Spawn spawn;
			L2NpcTemplate template;

			while (rset.next())
			{
				template = NpcTable.getTemplate(rset.getInt("npcId"));
				if(template != null)
				{
					spawn = new L2Spawn(template);
					spawn.setId(rset.getInt("id"));
					spawn.setAmount(1);
					spawn.setLoc(new Location(rset.getInt("x"), rset.getInt("y"), rset.getInt("z"), rset.getInt("heading")));;
					spawn.setRespawnDelay(rset.getInt("isHired") == 1 ? 0 : rset.getInt("respawnDelay"));
					spawn.setLocation(0);

					_siegeGuardSpawn.add(spawn);
				}
				else
					_log.warning("Error loading siege guard, missing npc data in npc table for id: " + rset.getInt("npcId"));
			}
		}
		catch(Exception e1)
		{
			_log.warning("Error loading siege guard for castle " + getSiegeUnit().getName() + ":" + e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * Save guards.<BR>
	 * <BR>
	 */
	private void saveSiegeGuard(Location loc, int npcId, int isHire)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("Insert Into siege_guards (unitId, npcId, x, y, z, heading, respawnDelay, isHired) Values (?, ?, ?, ?, ?, ?, ?, ?)");
			statement.setInt(1, getSiegeUnit().getId());
			statement.setInt(2, npcId);
			statement.setInt(3, loc.x);
			statement.setInt(4, loc.y);
			statement.setInt(5, loc.z);
			statement.setInt(6, loc.h);
			if(isHire == 1)
				statement.setInt(7, 0);
			else
				statement.setInt(7, 600);
			statement.setInt(8, isHire);
			statement.execute();
		}
		catch(Exception e1)
		{
			_log.warning("Error adding siege guard for castle " + getSiegeUnit().getName() + ":" + e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public final Residence getSiegeUnit()
	{
		return _castle;
	}

	public final GArray<L2Spawn> getSiegeGuardSpawn()
	{
		return _siegeGuardSpawn;
	}
}
