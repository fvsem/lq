package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.model.instances.L2ArtefactInstance;
import l2n.game.model.instances.L2ControlTowerInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CastleSiegeManager extends SiegeManager
{
	private static final Logger _log = Logger.getLogger(CastleSiegeManager.class.getName());

	// Siege spawns settings
	private static final TIntObjectHashMap<GArray<SiegeSpawn>> _controlTowerSpawnList = new TIntObjectHashMap<GArray<SiegeSpawn>>();
	private static final TIntObjectHashMap<GArray<SiegeSpawn>> _artefactSpawnList = new TIntObjectHashMap<GArray<SiegeSpawn>>();

	public static void load()
	{
		try
		{
			InputStream is = new FileInputStream(new File(Config.SIEGE_CASTLE_CONFIGURATION_FILE));
			Properties siegeSettings = new Properties();
			siegeSettings.load(is);
			is.close();

			// Siege setting
			for(Castle castle : CastleManager.getInstance().getCastles().values())
			{
				GArray<SiegeSpawn> controlTowersSpawns = new GArray<SiegeSpawn>();

				for(int i = 1; i < 255; i++)
				{
					String _spawnParams = siegeSettings.getProperty(castle.getName() + "ControlTower" + Integer.toString(i), "");

					if(_spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(_spawnParams.trim(), ",");

					try
					{
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
						int npc_id = Integer.parseInt(st.nextToken());
						int hp = Integer.parseInt(st.nextToken());

						controlTowersSpawns.add(new SiegeSpawn(castle.getId(), loc, npc_id, hp));
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "CastleSiegeManager: Error while loading control tower(s) for " + castle.getName() + " castle.", e);
					}
				}

				GArray<SiegeSpawn> artefactSpawns = new GArray<SiegeSpawn>();

				for(int i = 1; i < 255; i++)
				{
					String _spawnParams = siegeSettings.getProperty(castle.getName() + "Artefact" + Integer.toString(i), "");

					if(_spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(_spawnParams.trim(), ",");

					try
					{
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
						int npc_id = Integer.parseInt(st.nextToken());

						artefactSpawns.add(new SiegeSpawn(castle.getId(), loc, npc_id));
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "CastleSiegeManager: Error while loading artefact(s) for " + castle.getName() + " castle.", e);
					}
				}

				_controlTowerSpawnList.put(castle.getId(), controlTowersSpawns);
				_artefactSpawnList.put(castle.getId(), artefactSpawns);

				castle.getSiege().setControlTowerLosePenalty(Config.SIEGE_CASTLE_CONTROL_TOWER_LOSS_PENALTY);
				castle.getSiege().setDefenderRespawnDelay(Config.SIEGE_CASTLE_DEFENDER_RESPAWN);
				castle.getSiege().setSiegeClanMinLevel(Config.SIEGE_CASTLE_CLAN_MIN_LEVEL);
				castle.getSiege().setSiegeLength(Config.SIEGE_CASTLE_LENGTH);

				castle.getSiege().startAutoTask(true);
				spawnArtifacts(castle);
				spawnControlTowers(castle);

				castle.getSiege().getZone().setActive(false);
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "CastleSiegeManager: Error while loading siege data.", e);
		}
	}

	public static GArray<SiegeSpawn> getControlTowerSpawnList(int _castleId)
	{
		if(_controlTowerSpawnList.containsKey(_castleId))
		return _controlTowerSpawnList.get(_castleId);
		return null;
    }

	public static void spawnArtifacts(Castle castle)
	{
		for(SiegeSpawn _sp : _artefactSpawnList.get(castle.getId()))
		{
			L2ArtefactInstance art = new L2ArtefactInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(_sp.getNpcId()));
			art.setCurrentHpMp(art.getMaxHp(), art.getMaxMp(), true);
			art.setHeading(_sp.getLoc().h);
			art.spawnMe(_sp.getLoc().changeZ(50));
			castle.getSiege().addArtifact(art);
		}
	}

	/**
	 * Spawn control tower.
	 * 
	 * @param Сastle
	 */
	public static void spawnControlTowers(Castle castle)
	{
		for(SiegeSpawn sp : getControlTowerSpawnList(castle.getId()))
		{
			L2ControlTowerInstance tower = new L2ControlTowerInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(sp.getNpcId()), castle.getSiege(), sp.getValue());
			tower.setCurrentHpMp(tower.getMaxHp(), tower.getMaxMp(), true);
			tower.setHeading(sp.getLoc().h);
			tower.spawnMe(sp.getLoc());
			castle.getSiege().addControlTower(tower);
		}
	}

	public static int getControlTowerLosePenalty()
	{
		return Config.SIEGE_CASTLE_CONTROL_TOWER_LOSS_PENALTY;
	}

	public static int getDefenderRespawnDelay()
	{
		return Config.SIEGE_CASTLE_DEFENDER_RESPAWN;
	}

	public static int getSiegeClanMinLevel()
	{
		return Config.SIEGE_CASTLE_CLAN_MIN_LEVEL;
	}

	public static int getSiegeLength()
	{
		return Config.SIEGE_CASTLE_LENGTH;
	}
}