package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.model.quest.Quest;

public class QuestManager
{
	private final static TIntObjectHashMap<Quest> _questsByName = new TIntObjectHashMap<Quest>();
	private final static TIntObjectHashMap<Quest> _questsById = new TIntObjectHashMap<Quest>();

	public static Quest getQuest(final String name)
	{
		return _questsByName.get(name.hashCode());
	}

	public static Quest getQuest(final int questId)
	{
		return _questsById.get(questId);
	}

	public static Quest getQuest2(final String nameOrId)
	{
		if(_questsByName.containsKey(nameOrId.hashCode()))
			return _questsByName.get(nameOrId.hashCode());
		try
		{
			final int questId = Integer.valueOf(nameOrId);
			return _questsById.get(questId);
		}
		catch(final Exception e)
		{
			return null;
		}
	}

	public static Quest addQuest(final Quest newQuest)
	{
		if(newQuest == null)
			throw new IllegalArgumentException("Quest argument cannot be null");

		_questsByName.put(newQuest.getName().hashCode(), newQuest);
		_questsById.put(newQuest.getQuestIntId(), newQuest);
		return newQuest;
	}

	public static Iterable<Quest> getQuests()
	{
		return _questsByName.valueCollection();
	}
}
