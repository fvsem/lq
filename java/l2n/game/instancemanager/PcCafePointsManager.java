package l2n.game.instancemanager;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExPCCafePointInfo;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Rnd;

import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 17.11.2009
 * @time 21:00:14
 */
public class PcCafePointsManager
{
	protected static final Logger _log = Logger.getLogger(PcCafePointsManager.class.getName());

	private static PcCafePointsManager _instance;

	public static PcCafePointsManager getInstance()
	{
		if(_instance == null)
			_instance = new PcCafePointsManager();
		return _instance;
	}

	public PcCafePointsManager()
	{
		if(Config.PCCAFE_ENABLED)
			_log.info("PcCafePoints: system enable.");
	}

	public void givePcCafePoint(final L2Player player, final long givedexp)
	{
		if(!checkCondition(player))
			return;

		double _points = givedexp * 0.0001 * Config.PCCAFE_ACQUISITION_POINTS_RATE;

		if(Config.DEBUG)
			_log.info("PcCafePointsManager: give " + _points + " points to player " + player.getName());

		if(Config.PCCAFE_ACQUISITION_POINTS_RANDOM)
			_points = Rnd.get(_points / 2, _points);

		if(Rnd.chance(Config.PCCAFE_DOUBLE_ACQUISITION_POINTS_CHANCE) && Config.PCCAFE_ENABLE_DOUBLE_ACQUISITION_POINTS)
		{
			_points *= 2;
			player.setPcBangPoints(player.getPcBangPoints() + (int) _points);
			player.sendPacket(new SystemMessage(SystemMessage.DOUBLE_POINTS_YOU_AQUIRED_S1_PC_BANG_POINT).addNumber((int) _points));
			player.sendPacket(new ExPCCafePointInfo(player.getPcBangPoints(), (int) _points, true, true, 1));
		}
		else
		{
			player.setPcBangPoints(player.getPcBangPoints() + (int) _points);
			player.sendPacket(new ExPCCafePointInfo(player.getPcBangPoints(), (int) _points, true, false, 1));
		}
	}

	private boolean checkCondition(L2Player player)
	{
		if(!Config.PCCAFE_ENABLED)
			return false;

		if(player.isInZonePeace() || player.isInZoneBattle() || player.isInZoneSiege() || !player.isOnline() || player.isInJail())
			return false;

		if(Config.PCCAFE_IPCHECK_ADDRESSES && !isAllowedIp(player))
			return false;

		if(Config.PCCAFE_CHECK_NICKNAMES && !isAllowedNickName(player))
			return false;

		return !Config.PCCAFE_IPCHECK_ADDRESSES && !Config.PCCAFE_CHECK_NICKNAMES;
	}

	private boolean isAllowedNickName(L2Player player)
	{
		String playername = player.getName();
		for(String name : Config.PCCAFE_NICKNAMES)
			if(name.equals(playername))
				return true;
		return false;
	}

	private boolean isAllowedIp(L2Player player)
	{
		String ip = "N/A";
		try
		{
			ip = player.getNetConnection().getIpAddr();
		}
		catch(final Exception e)
		{
			return false;
		}

		for(String ipcafe : Config.PCCAFE_IPS)
			if(ip.equals(ipcafe))
				return true;
		return false;
	}
}
