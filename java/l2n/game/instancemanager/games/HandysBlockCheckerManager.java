package l2n.game.instancemanager.games;

import gnu.trove.map.hash.TIntIntHashMap;
import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.BlockCheckerEngine;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.ExCubeGameAddPlayer;
import l2n.game.network.serverpackets.ExCubeGameChangeTeam;
import l2n.game.network.serverpackets.ExCubeGameRemovePlayer;
import l2n.game.network.serverpackets.L2GameServerPacket;

/**
 * Общий менеджер, отвечает за регистрацию
 * 
 * @author L2System Project
 * @date 20.12.2010
 * @time 13:49:37
 */
public class HandysBlockCheckerManager
{
	// All the participants and their team classifed by arena
	private static ArenaParticipantsHolder[] _arenaPlayers;

	// Arena votes to start the game
	private static TIntIntHashMap _arenaVotes = new TIntIntHashMap();

	// Arena Status, True = is being used, otherwise, False
	private static FastMap<Integer, Boolean> _arenaStatus;

	public static HandysBlockCheckerManager _instance = null;

	public static final HandysBlockCheckerManager getInstance()
	{
		if(_instance == null)
			_instance = new HandysBlockCheckerManager();
		return _instance;
	}

	private HandysBlockCheckerManager()
	{
		// Initialize arena status
		if(_arenaStatus == null)
		{
			_arenaStatus = new FastMap<Integer, Boolean>();
			_arenaStatus.put(0, false);
			_arenaStatus.put(1, false);
			_arenaStatus.put(2, false);
			_arenaStatus.put(3, false);
		}
	}

	/**
	 * Return the number of event-start votes for the spcified
	 * arena id
	 * 
	 * @param arenaId
	 * @return int (number of votes)
	 */
	public synchronized int getArenaVotes(int arenaId)
	{
		return _arenaVotes.get(arenaId);
	}

	/**
	 * Add a new vote to start the event for the specified
	 * arena id
	 * 
	 * @param arena
	 */
	public synchronized void increaseArenaVotes(int arena)
	{
		int newVotes = _arenaVotes.get(arena) + 1;
		ArenaParticipantsHolder holder = _arenaPlayers[arena];

		if(newVotes > holder.getAllPlayers().size() / 2 && !holder.getEvent().isStarted())
		{
			clearArenaVotes(arena);

			if(holder.getBlueTeamSize() == 0 || holder.getRedTeamSize() == 0)
				return;

			if(Config.BLOCK_CHECKER_EVENT_FAIR_PLAY)
				holder.checkAndShuffle();

			L2GameThreadPools.getInstance().executeGeneral(holder.getEvent().new StartEvent());
		}
		else
			_arenaVotes.put(arena, newVotes);
	}

	/**
	 * Will clear the votes queue (of event start) for the
	 * specified arena id
	 * 
	 * @param arena
	 */
	public synchronized void clearArenaVotes(int arena)
	{
		_arenaVotes.put(arena, 0);
	}

	/**
	 * Returns the players holder
	 * 
	 * @param arena
	 * @return ArenaParticipantsHolder
	 */
	public ArenaParticipantsHolder getHolder(int arena)
	{
		return _arenaPlayers[arena];
	}

	/**
	 * Initializes the participants holder
	 */
	public void startUpParticipantsQueue()
	{
		_arenaPlayers = new ArenaParticipantsHolder[4];
		for(int i = 0; i < 4; ++i)
			_arenaPlayers[i] = new ArenaParticipantsHolder(i);
	}

	/**
	 * Add the player to the specified arena (throught the specified
	 * arena manager) and send the needed server -> client packets
	 * 
	 * @param player
	 * @param arenaId
	 */
	public boolean addPlayerToArena(L2Player player, int arenaId)
	{
		ArenaParticipantsHolder holder = _arenaPlayers[arenaId];

		synchronized (holder)
		{
			boolean isRed;

			for(int i = 0; i < 4; i++)
				if(_arenaPlayers[i].getAllPlayers().contains(player))
				{
					player.sendMessage("You are alredy registered for a Block Checker Match!");
					return false;
				}

			// Обладатели проклятых мечей не могут участвовать.
			if(player.isCursedWeaponEquipped())
			{
				player.sendMessage("You may not participe holding a Cursed Weapon");
				return false;
			}

			// TODO Игроки не могут участвовать, если они уже зарегистрировались на другие PvP-игры, например на Олимпиаду, в Подземный Колизей или в Kratei's Cube.

			if(player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player) || player.getTeam() != 0)
			{
				player.sendMessage("Couldnt register you due other event participation");
				return false;
			}

			if(holder.getBlueTeamSize() < holder.getRedTeamSize())
			{
				holder.addPlayer(player, 1);
				isRed = false;
			}
			else
			{
				holder.addPlayer(player, 0);
				isRed = true;
			}

			holder.broadCastPacketToTeam(new ExCubeGameAddPlayer(player, isRed));
			return true;
		}
	}

	/**
	 * Will remove the specified player from the specified
	 * team and arena and will send the needed packet to all
	 * his team mates / enemy team mates
	 * 
	 * @param player
	 * @param arenaId
	 */
	public void removePlayer(L2Player player, int arenaId, int team)
	{
		ArenaParticipantsHolder holder = _arenaPlayers[arenaId];
		synchronized (holder)
		{
			boolean isRed = team == 0 ? true : false;

			holder.removePlayer(player, team);
			holder.broadCastPacketToTeam(new ExCubeGameRemovePlayer(player, isRed));

			// End event if theres an empty team
			int teamSize = isRed ? holder.getRedTeamSize() : holder.getBlueTeamSize();
			if(teamSize == 0)
				holder.getEvent().endEventAbnormally();
		}
	}

	/**
	 * Will change the player from one team to other (if possible)
	 * and will send the needed packets
	 * 
	 * @param player
	 * @param arena
	 * @param team
	 */
	public void changePlayerToTeam(L2Player player, int arena, int team)
	{
		ArenaParticipantsHolder holder = _arenaPlayers[arena];

		synchronized (holder)
		{
			boolean isFromRed = holder._redPlayers.contains(player);

			if(isFromRed && holder.getBlueTeamSize() == 2)
			{
				player.sendMessage("The team is full");
				return;
			}
			else if(!isFromRed && holder.getRedTeamSize() == 2)
			{
				player.sendMessage("The team is full");
				return;
			}

			int futureTeam = isFromRed ? 1 : 0;
			holder.addPlayer(player, futureTeam);

			if(isFromRed)
				holder.removePlayer(player, 0);
			else
				holder.removePlayer(player, 1);
			holder.broadCastPacketToTeam(new ExCubeGameChangeTeam(player, isFromRed));
		}
	}

	/**
	 * Will erase all participants from the specified holder
	 * 
	 * @param arenaId
	 */
	public synchronized void clearPaticipantQueueByArenaId(int arenaId)
	{
		_arenaPlayers[arenaId].clearPlayers();
	}

	/**
	 * Returns true if arena is holding an event at this momment
	 * 
	 * @param arenaId
	 * @return Boolean
	 */
	public boolean arenaIsBeingUsed(int arenaId)
	{
		if(arenaId < 0 || arenaId > 3)
			return false;
		return _arenaStatus.get(arenaId);
	}

	/**
	 * Set the specified arena as being used
	 * 
	 * @param arenaId
	 */
	public void setArenaBeingUsed(int arenaId)
	{
		_arenaStatus.put(arenaId, true);
	}

	/**
	 * Set as free the specified arena for future
	 * events
	 * 
	 * @param arenaId
	 */
	public void setArenaFree(int arenaId)
	{
		_arenaStatus.put(arenaId, false);
	}

	/**
	 * Called when played logs out while participating
	 * in Block Checker Event
	 * 
	 * @param L2Player
	 *            player
	 */
	public void onDisconnect(L2Player player)
	{
		int arena = player.getHandysBlockCheckerEventArena();
		int team = getHolder(arena).getPlayerTeam(player);
		HandysBlockCheckerManager.getInstance().removePlayer(player, arena, team);
		if(player.getTeam() > 0)
		{
			player.stopAllEffects();
			// Remove team aura
			player.setTeam(0, false);

			// Remove the event items
			PcInventory inv = player.getInventory();

			if(inv.getItemByItemId(13787) != null)
			{
				long count = inv.getCountOf(13787);
				inv.destroyItemByItemId(13787, count, true);
			}
			if(inv.getItemByItemId(13788) != null)
			{
				long count = inv.getCountOf(13788);
				inv.destroyItemByItemId(13788, count, true);
			}

			// Teleport Back
			player.teleToLocation(-57478, -60367, -2370);
		}
	}

	public class ArenaParticipantsHolder
	{
		private int _arena;
		private GArray<L2Player> _redPlayers;
		private GArray<L2Player> _bluePlayers;
		private BlockCheckerEngine _engine;

		public ArenaParticipantsHolder(int arena)
		{
			_arena = arena;
			_redPlayers = new GArray<L2Player>(2);
			_bluePlayers = new GArray<L2Player>(2);
			_engine = new BlockCheckerEngine(this, _arena);
		}

		public GArray<L2Player> getRedPlayers()
		{
			return _redPlayers;
		}

		public GArray<L2Player> getBluePlayers()
		{
			return _bluePlayers;
		}

		public GArray<L2Player> getAllPlayers()
		{
			GArray<L2Player> all = new GArray<L2Player>(12);
			all.addAll(_redPlayers);
			all.addAll(_bluePlayers);
			return all;
		}

		public void addPlayer(L2Player player, int team)
		{
			if(team == 0)
				_redPlayers.add(player);
			else
				_bluePlayers.add(player);
		}

		public void removePlayer(L2Player player, int team)
		{
			if(team == 0)
				_redPlayers.remove(player);
			else
				_bluePlayers.remove(player);
		}

		public int getPlayerTeam(L2Player player)
		{
			if(_redPlayers.contains(player))
				return 0;
			else if(_bluePlayers.contains(player))
				return 1;
			else
				return -1;
		}

		public int getRedTeamSize()
		{
			return _redPlayers.size();
		}

		public int getBlueTeamSize()
		{
			return _bluePlayers.size();
		}

		public void broadCastPacketToTeam(L2GameServerPacket packet)
		{
			GArray<L2Player> team = new GArray<L2Player>(2);
			team.addAll(_redPlayers);
			team.addAll(_bluePlayers);

			for(L2Player p : team)
				p.sendPacket(packet);
		}

		public void clearPlayers()
		{
			_redPlayers.clear();
			_bluePlayers.clear();
		}

		public BlockCheckerEngine getEvent()
		{
			return _engine;
		}

		public void updateEvent()
		{
			_engine.updatePlayersOnStart(this);
		}

		public void checkAndShuffle()
		{
			int redSize = _redPlayers.size();
			int blueSize = _bluePlayers.size();
			if(redSize > blueSize + 1)
			{
				int needed = redSize - (blueSize + 1);
				for(int i = 0; i < needed + 1; i++)
				{
					L2Player plr = _redPlayers.get(i);
					if(plr == null)
						continue;
					changePlayerToTeam(plr, _arena, 1);
				}
			}
			else if(blueSize > redSize + 1)
			{
				int needed = blueSize - (redSize + 1);
				for(int i = 0; i < needed + 1; i++)
				{
					L2Player plr = _bluePlayers.get(i);
					if(plr == null)
						continue;
					changePlayerToTeam(plr, _arena, 0);
				}
			}
		}
	}
}
