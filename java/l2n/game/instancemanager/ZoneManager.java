package l2n.game.instancemanager;

import gnu.trove.iterator.TLongLongIterator;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TLongLongHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2RoundTerritory;
import l2n.game.model.L2Territory;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.zone.L2MonsterTrapZone;
import l2n.game.model.zone.L2NoLandingZone;
import l2n.game.tables.TerritoryTable;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ZoneManager
{
	private final static Logger _log = Logger.getLogger(ZoneManager.class.getName());

	private final static HashMap<ZoneType, GArray<L2Zone>> _zonesByType = new HashMap<ZoneType, GArray<L2Zone>>();

	private final L2NoLandingZone _noLandingZoneListener = new L2NoLandingZone();
	private final L2MonsterTrapZone _monsterTrapZoneListener = new L2MonsterTrapZone();

	// zoneId, reflectionObjectId, reuseTime
	// FIXME Тут будет утечка памяти
	private TIntObjectHashMap<TLongLongHashMap> _reuseMonsterTrapZones = null;

	private ZoneManager()
	{
		parse();
	}

	public static ZoneManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ZoneManager _instance = new ZoneManager();
	}

	public boolean checkIfInZone(final ZoneType zoneType, final L2Object object)
	{
		return checkIfInZone(zoneType, object.getX(), object.getY(), object.getZ());
	}

	public boolean checkIfInZone(final ZoneType zoneType, final int x, final int y)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return false;
		for(final L2Zone zone : list)
			if(zone.isActive() && zone.getLoc() != null && zone.getLoc().isInside(x, y))
				return true;
		return false;
	}

	public boolean checkIfInZone(final ZoneType zoneType, final int x, final int y, final int z)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return false;
		for(final L2Zone zone : list)
			if(zone.isActive() && zone.getLoc() != null && zone.getLoc().isInside(x, y) && z >= zone.getLoc().getZmin() && z <= zone.getLoc().getZmax())
				return true;
		return false;
	}

	public L2Zone getZoneByType(final ZoneType zoneType, final int x, final int y, final boolean onlyActive)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return null;
		for(final L2Zone zone : list)
			if((!onlyActive || zone.isActive()) && zone.getLoc() != null && zone.getLoc().isInside(x, y))
				return zone;
		return null;
	}

	public GArray<L2Zone> getZoneByType(final ZoneType zoneType)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		final GArray<L2Zone> result = new GArray<L2Zone>();
		if(list == null)
			return result;
		for(final L2Zone zone : list)
			if(zone.isActive())
				result.add(zone);
		return result;
	}

	public L2Zone getZoneByIndex(final ZoneType zoneType, final int index, final boolean onlyActive)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return null;
		for(final L2Zone zone : list)
			if((!onlyActive || zone.isActive()) && zone.getIndex() == index)
				return zone;
		return null;
	}

	public L2Zone getZoneById(final ZoneType zoneType, final int id, final boolean onlyActive)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return null;
		for(final L2Zone zone : list)
			if((!onlyActive || zone.isActive()) && zone.getId() == id)
				return zone;
		return null;
	}

	public L2Zone getZoneByTypeAndObject(final ZoneType zoneType, final L2Object object)
	{
		return getZoneByTypeAndCoords(zoneType, object.getX(), object.getY(), object.getZ());
	}

	public L2Zone getZoneByTypeAndCoords(final ZoneType zoneType, final int x, final int y, final int z)
	{
		final GArray<L2Zone> list = _zonesByType.get(zoneType);
		if(list == null)
			return null;
		for(final L2Zone zone : list)
			if(zone.isActive() && zone.getLoc() != null && zone.getLoc().isInside(x, y, z))
				return zone;
		return null;
	}

	public L2Zone getZoneByCoords(final int x, final int y, final int z)
	{
		for(final Entry<ZoneType, GArray<L2Zone>> entr : _zonesByType.entrySet())
		{
			final GArray<L2Zone> list = entr.getValue();
			if(list == null || list.isEmpty())
				continue;
			for(final L2Zone zone : list)
				if(zone.isActive() && zone.getLoc() != null && zone.getLoc().isInside(x, y, z))
					return zone;
		}
		return null;
	}

	private void parse()
	{
		final GArray<File> files = new GArray<File>();
		hashFiles("zone", files);
		int count = 0;
		for(final File f : files)
			count += parseFile(f);
		_log.config("ZoneManager: Loaded " + count + " zones");

		TerritoryTable.getInstance().registerZones();
	}

	private void hashFiles(final String dirname, final GArray<File> hash)
	{
		final File dir = new File(Config.DATAPACK_ROOT, "data/" + dirname);
		if(!dir.exists())
		{
			_log.config("Dir " + dir.getAbsolutePath() + " not exists");
			return;
		}
		final File[] files = dir.listFiles();
		for(final File f : files)
			if(f.getName().endsWith(".xml"))
				hash.add(f);
			else if(f.isDirectory() && !f.getName().equals(".svn"))
				hashFiles(dirname + "/" + f.getName(), hash);
	}

	private int parseFile(final File f)
	{
		Document doc = null;
		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			doc = factory.newDocumentBuilder().parse(f);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "zones file couldnt be initialized: " + f, e);
			return 0;
		}
		try
		{
			return parseDocument(doc);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "zones file couldnt be initialized: " + f, e);
		}
		return 0;
	}

	private int parseDocument(final Document doc)
	{
		int count = 0;
		for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if("list".equalsIgnoreCase(n.getNodeName()))
				for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					if("zone".equalsIgnoreCase(d.getNodeName()))
					{
						final int id = XMLUtil.getAttributeIntValue(d, "id", 0);
						final ZoneType type = ZoneType.valueOf(XMLUtil.getAttributeValue(d, "type"));
						final String zname = XMLUtil.getAttributeValue(d, "name");

						final L2Zone z = new L2Zone(id, type, zname);

						// Чтоб не повторяться можно задавать координаты по оси Z в основном описании зон
						final int minZ = XMLUtil.getAttributeIntValue(d, "minZ", Integer.MIN_VALUE);
						final int maxZ = XMLUtil.getAttributeIntValue(d, "maxZ", Integer.MAX_VALUE);

						boolean enabled = true;

						for(Node e = d.getFirstChild(); e != null; e = e.getNextSibling())
							if("set".equalsIgnoreCase(e.getNodeName()))
							{
								final String name = XMLUtil.getAttributeValue(e, "name");
								if("index".equalsIgnoreCase(name))
									z.setIndex(XMLUtil.getAttributeIntValue(e, "val", 0));
								else if("taxById".equalsIgnoreCase(name))
									z.setTaxById(XMLUtil.getAttributeIntValue(e, "val", 0));
								else if("entering_message_no".equalsIgnoreCase(name))
									z.setEnteringMessageId(XMLUtil.getAttributeIntValue(e, "val", 0));
								else if("leaving_message_no".equalsIgnoreCase(name))
									z.setLeavingMessageId(XMLUtil.getAttributeIntValue(e, "val", 0));
								else if("target".equalsIgnoreCase(name))
									z.setTarget(XMLUtil.getAttributeValue(e, "val"));
								else if("skill_name".equalsIgnoreCase(name))
									z.setSkill(XMLUtil.getAttributeValue(e, "val"));
								else if("skill_prob".equalsIgnoreCase(name))
									z.setSkillProb(XMLUtil.getAttributeValue(e, "val"));
								else if("unit_tick".equalsIgnoreCase(name))
									z.setUnitTick(XMLUtil.getAttributeValue(e, "val"));
								else if("initial_delay".equalsIgnoreCase(name))
									z.setInitialDelay(XMLUtil.getAttributeValue(e, "val"));
								else if("restart_time".equalsIgnoreCase(name))
									z.setRestartTime(XMLUtil.getAttributeLongValue(e, "val", 0L));
								else if("blocked_actions".equalsIgnoreCase(name))
									z.setBlockedActions(XMLUtil.getAttributeValue(e, "val"));
								else if("damage_on_hp".equalsIgnoreCase(name))
									z.setDamageOnHP(XMLUtil.getAttributeValue(e, "val"));
								else if("damage_on_mp".equalsIgnoreCase(name))
									z.setDamageOnМP(XMLUtil.getAttributeValue(e, "val"));
								else if("message_no".equalsIgnoreCase(name))
									z.setMessageNumber(XMLUtil.getAttributeValue(e, "val"));
								else if("move_bonus".equalsIgnoreCase(name))
									z.setMoveBonus(XMLUtil.getAttributeValue(e, "val"));
								else if("hp_regen_bonus".equalsIgnoreCase(name))
									z.setRegenBonusHP(XMLUtil.getAttributeValue(e, "val"));
								else if("mp_regen_bonus".equalsIgnoreCase(name))
									z.setRegenBonusMP(XMLUtil.getAttributeValue(e, "val"));
								else if("affect_race".equalsIgnoreCase(name))
									z.setAffectRace(XMLUtil.getAttributeValue(e, "val"));
								else if("event".equalsIgnoreCase(name))
									z.setEvent(XMLUtil.getAttributeValue(e, "val"));
								else if("reflectionId".equalsIgnoreCase(name))
									z.setReflectionId(XMLUtil.getAttributeLongValue(e, "val", 0));
								else if("enabled".equalsIgnoreCase(name))
									enabled = XMLUtil.getAttributeBooleanValue(e, "val", true) || z.getType() == ZoneType.water;
								else
									z.setParam(name, XMLUtil.getAttributeValue(e, "val"));
							}
							else
							{
								if(!"shape".equalsIgnoreCase(e.getNodeName()) && !"restart_point".equalsIgnoreCase(e.getNodeName()) && !"PKrestart_point".equalsIgnoreCase(e.getNodeName()))
									continue;
								final int locId = IdFactory.getInstance().getNextId();
								final boolean isRound = !XMLUtil.getAttributeValue(e, "loc").isEmpty();
								L2Territory territory;
								if(isRound)
								{
									final String[] coord = XMLUtil.getAttributeValue(e, "loc").replaceAll(",", " ").replaceAll(";", " ").replaceAll("  ", " ").trim().split(" ");
									if(coord.length < 5)
										territory = new L2RoundTerritory(locId, Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), Integer.parseInt(coord[2]), minZ, maxZ);
									else
										territory = new L2RoundTerritory(locId, Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), Integer.parseInt(coord[2]), Integer.parseInt(coord[3]), Integer.parseInt(coord[4]));
								}
								else
								{
									territory = new L2Territory(locId);
									for(Node location = e.getFirstChild(); location != null; location = location.getNextSibling())
										if("coords".equalsIgnoreCase(location.getNodeName()))
										{
											final String[] coord = XMLUtil.getAttributeValue(location, "loc").replaceAll(",", " ").replaceAll(";", " ").replaceAll("  ", " ").trim().split(" ");
											if(coord.length < 4)
												territory.add(Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), minZ, maxZ);
											else
												territory.add(Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), Integer.parseInt(coord[2]), Integer.parseInt(coord[3]));
										}
								}
								if("shape".equalsIgnoreCase(e.getNodeName()))
								{
									z.setLoc(territory);
									territory.setZone(z);
									territory.validate();
								}
								else if("restart_point".equalsIgnoreCase(e.getNodeName()))
									z.setRestartPoints(territory);
								else if("PKrestart_point".equalsIgnoreCase(e.getNodeName()))
									z.setPKRestartPoints(territory);
								z.setActive(enabled);
								TerritoryTable.getInstance().getLocations().put(locId, territory);
							}

						if(z.getType() == ZoneType.no_landing || z.getType() == ZoneType.Siege || z.getType() == ZoneType.Castle || z.getType() == ZoneType.Fortress || z.getType() == ZoneType.OlympiadStadia)
							z.getListenerEngine().addMethodInvokedListener(_noLandingZoneListener);
						if(z.getType() == ZoneType.monster_trap)
							z.getListenerEngine().addMethodInvokedListener(_monsterTrapZoneListener);

						// Проверяем нет ли уже зоны с таким же ID
						if(getZoneById(z.getType(), z.getId(), false) == null)
						{
							if(_zonesByType.get(z.getType()) == null)
								_zonesByType.put(z.getType(), new GArray<L2Zone>());
							_zonesByType.get(z.getType()).add(z);
							count++;
						}
						else
							_log.warning("ZoneManager: zone[" + z.getId() + "] replace!");

					}
		return count;
	}

	public void reload()
	{
		_zonesByType.clear();
		parse();
	}

	public boolean checkIfInZoneFishing(final int x, final int y, final int z)
	{
		return !Config.ENABLE_FISHING_CHECK_WATER || checkIfInZone(ZoneType.water, x, y, z);
	}

	public void setReuseMonsterTrapZone(final int zoneId, final long refId, final long reuseTime)
	{
		if(_reuseMonsterTrapZones == null)
			_reuseMonsterTrapZones = new TIntObjectHashMap<TLongLongHashMap>();

		final TLongLongHashMap zoneReuse = new TLongLongHashMap();
		zoneReuse.put(refId, reuseTime);
		_reuseMonsterTrapZones.put(zoneId, zoneReuse);
	}

	public long getReuseMonsterTrapZone(final int zoneId, final long refId)
	{
		if(_reuseMonsterTrapZones == null)
			return 0;

		final TLongLongHashMap reuses = _reuseMonsterTrapZones.get(zoneId);
		if(reuses == null || reuses.isEmpty())
			return 0;

		for(final TLongLongIterator iter = reuses.iterator(); iter.hasNext();)
		{
			iter.advance();
			if(refId == iter.key())
				return iter.value();
		}

		return 0;
	}
}
