package l2n.game.instancemanager;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.DimensionalRift;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.TeleportToLocation;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.TerritoryTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class DimensionalRiftManager
{
	private final static Logger _log = Logger.getLogger(DimensionalRiftManager.class.getName());

	private final static int DIMENSIONAL_FRAGMENT_ITEM_ID = 7079;

	private final FastMap<Integer, FastMap<Integer, DimensionalRiftRoom>> _rooms = new FastMap<Integer, FastMap<Integer, DimensionalRiftRoom>>().shared();

	public static DimensionalRiftManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DimensionalRiftManager _instance = new DimensionalRiftManager();
	}

	private DimensionalRiftManager()
	{
		load();
	}

	public DimensionalRiftRoom getRoom(final int type, final int room)
	{
		return _rooms.get(type) == null ? null : _rooms.get(type).get(room);
	}

	public FastMap<Integer, DimensionalRiftRoom> getRooms(final int type)
	{
		return _rooms.get(type);
	}

	public void load()
	{
		int countGood = 0, countBad = 0;
		try
		{
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			final File file = new File(Config.DATAPACK_ROOT + "/data/dimensional_rift.xml");
			if(!file.exists())
				throw new IOException();

			final Document doc = factory.newDocumentBuilder().parse(file);
			NamedNodeMap attrs;
			int type;
			int roomId;
			int mobId, delay, count;
			L2Spawn spawnDat;
			L2NpcTemplate template;
			Location tele = new Location();
			int xMin = 0, xMax = 0, yMin = 0, yMax = 0, zMin = 0, zMax = 0;
			boolean isBossRoom;

			for(Node rift = doc.getFirstChild(); rift != null; rift = rift.getNextSibling())
				if("rift".equalsIgnoreCase(rift.getNodeName()))
					for(Node area = rift.getFirstChild(); area != null; area = area.getNextSibling())
						if("area".equalsIgnoreCase(area.getNodeName()))
						{
							attrs = area.getAttributes();
							type = Integer.parseInt(attrs.getNamedItem("type").getNodeValue());

							for(Node room = area.getFirstChild(); room != null; room = room.getNextSibling())
								if("room".equalsIgnoreCase(room.getNodeName()))
								{
									attrs = room.getAttributes();
									roomId = Integer.parseInt(attrs.getNamedItem("id").getNodeValue());
									final Node boss = attrs.getNamedItem("isBossRoom");
									isBossRoom = boss != null ? Boolean.parseBoolean(boss.getNodeValue()) : false;

									for(Node coord = room.getFirstChild(); coord != null; coord = coord.getNextSibling())
										if("teleport".equalsIgnoreCase(coord.getNodeName()))
										{
											attrs = coord.getAttributes();
											tele = new Location(attrs.getNamedItem("loc").getNodeValue());
										}
										else if("zone".equalsIgnoreCase(coord.getNodeName()))
										{
											attrs = coord.getAttributes();
											xMin = Integer.parseInt(attrs.getNamedItem("xMin").getNodeValue());
											xMax = Integer.parseInt(attrs.getNamedItem("xMax").getNodeValue());
											yMin = Integer.parseInt(attrs.getNamedItem("yMin").getNodeValue());
											yMax = Integer.parseInt(attrs.getNamedItem("yMax").getNodeValue());
											zMin = Integer.parseInt(attrs.getNamedItem("zMin").getNodeValue());
											zMax = Integer.parseInt(attrs.getNamedItem("zMax").getNodeValue());
										}

									final int loc_id = IdFactory.getInstance().getNextId();
									final L2Territory territory = new L2Territory(loc_id);
									territory.add(xMin, yMin, zMin, zMax);
									territory.add(xMax, yMin, zMin, zMax);
									territory.add(xMax, yMax, zMin, zMax);
									territory.add(xMin, yMax, zMin, zMax);
									territory.validate();
									TerritoryTable.getInstance().getLocations().put(loc_id, territory);
									L2World.addTerritory(territory);

									if(!_rooms.containsKey(type))
										_rooms.put(type, new FastMap<Integer, DimensionalRiftRoom>().shared());

									_rooms.get(type).put(roomId, new DimensionalRiftRoom(territory, tele, isBossRoom));

									for(Node spawn = room.getFirstChild(); spawn != null; spawn = spawn.getNextSibling())
										if("spawn".equalsIgnoreCase(spawn.getNodeName()))
										{
											attrs = spawn.getAttributes();
											mobId = Integer.parseInt(attrs.getNamedItem("mobId").getNodeValue());
											delay = Integer.parseInt(attrs.getNamedItem("delay").getNodeValue());
											count = Integer.parseInt(attrs.getNamedItem("count").getNodeValue());

											template = NpcTable.getTemplate(mobId);
											if(template == null)
												_log.warning("Template " + mobId + " not found!");
											if(!_rooms.containsKey(type))
												_log.warning("Type " + type + " not found!");
											else if(!_rooms.get(type).containsKey(roomId))
												_log.warning("Room " + roomId + " in Type " + type + " not found!");

											if(template != null && _rooms.containsKey(type) && _rooms.get(type).containsKey(roomId))
											{
												spawnDat = new L2Spawn(template);
												spawnDat.setLocation(loc_id);
												spawnDat.setHeading(-1);
												spawnDat.setRespawnDelay(delay);
												spawnDat.setAmount(count);
												_rooms.get(type).get(roomId).getSpawns().add(spawnDat);
												countGood += count;
											}
											else
												countBad++;
										}
								}
						}
		}
		catch(final Exception e)
		{
			_log.warning("Error on loading dimensional rift spawns: " + e);
		}
		final int typeSize = _rooms.keySet().size();
		int roomSize = 0;

		for(final int b : _rooms.keySet())
			roomSize += _rooms.get(b).keySet().size();

		_log.info("DimensionalRiftManager: Loaded " + typeSize + " room types with " + roomSize + " rooms.");
		_log.info("DimensionalRiftManager: Loaded " + countGood + " dimensional rift spawns, " + countBad + " errors.");
	}

	public void reload()
	{
		for(final int b : _rooms.keySet())
		{
			for(final int i : _rooms.get(b).keySet())
				_rooms.get(b).get(i).getSpawns().clear();
			_rooms.get(b).clear();
		}
		_rooms.clear();
		load();
	}

	public boolean checkIfInRiftZone(final Location loc, final boolean ignorePeaceZone)
	{
		if(ignorePeaceZone)
			return _rooms.get(0).get(1).checkIfInZone(loc);
		return _rooms.get(0).get(1).checkIfInZone(loc) && !_rooms.get(0).get(0).checkIfInZone(loc);
	}

	public boolean checkIfInPeaceZone(final Location loc)
	{
		return _rooms.get(0).get(0).checkIfInZone(loc);
	}

	public void teleportToWaitingRoom(final L2Player player)
	{
		teleToLocation(player, getRoom(0, 0).getTeleportCoords().rnd(0, 250, false), null);
	}

	public void start(final L2Player player, final int type, final L2NpcInstance npc)
	{
		if(!player.isInParty())
		{
			showHtmlFile(player, "data/html/rift/NoParty.htm", npc);
			return;
		}

		if(!player.isGM())
		{
			if(player.getParty().getPartyLeaderOID() != player.getObjectId())
			{
				showHtmlFile(player, "data/html/rift/NotPartyLeader.htm", npc);
				return;
			}

			if(player.getParty().isInDimensionalRift())
			{
				showHtmlFile(player, "data/html/rift/Cheater.htm", npc);

				if(!player.isGM())
					_log.warning("Player " + player.getName() + "(" + player.getObjectId() + ") was cheating in dimension rift area!");
				return;
			}

			if(player.getParty().getMemberCount() < Config.RIFT_MIN_PARTY_SIZE)
			{
				showHtmlFile(player, "data/html/rift/SmallParty.htm", npc);
				return;
			}

			for(final L2Player p : player.getParty().getPartyMembers())
				if(!checkIfInPeaceZone(p.getLoc()))
				{
					showHtmlFile(player, "data/html/rift/NotInWaitingRoom.htm", npc);
					return;
				}

			L2ItemInstance i;
			for(final L2Player p : player.getParty().getPartyMembers())
			{
				i = p.getInventory().getItemByItemId(DIMENSIONAL_FRAGMENT_ITEM_ID);
				if(i == null || i.getCount() < getNeededItems(type))
				{
					showHtmlFile(player, "data/html/rift/NoFragments.htm", npc);
					return;
				}
			}

			for(final L2Player p : player.getParty().getPartyMembers())
				p.getInventory().destroyItemByItemId(DIMENSIONAL_FRAGMENT_ITEM_ID, getNeededItems(type), true);
		}

		final int roomId = Rnd.get(1, _rooms.get(type).size() - 1);
		// System.out.printf( "DimensionalRiftManager: checks passed, entering rift type %d, room %d\n", type, roomId );
		new DimensionalRift(player.getParty(), type, roomId);
	}

	private long getNeededItems(final int type)
	{
		switch (type)
		{
			case 1:
				return Config.RIFT_ENTER_COST_RECRUIT;
			case 2:
				return Config.RIFT_ENTER_COST_SOLDIER;
			case 3:
				return Config.RIFT_ENTER_COST_OFFICER;
			case 4:
				return Config.RIFT_ENTER_COST_CAPTAIN;
			case 5:
				return Config.RIFT_ENTER_COST_COMMANDER;
			case 6:
				return Config.RIFT_ENTER_COST_HERO;
			default:
				return Long.MAX_VALUE;
		}
	}

	public void showHtmlFile(final L2Player player, final String file, final L2NpcInstance npc)
	{
		final NpcHtmlMessage html = new NpcHtmlMessage(player, npc);
		html.setFile(file);
		html.replace("%t_name%", npc.getName());
		player.sendPacket(html);
	}

	public static void teleToLocation(final L2Player player, final Location loc, Reflection ref)
	{
		if(player.isTeleporting() || player.isLogoutStarted())
			return;

		player.setTarget(null);

		if(player.isInVehicle())
			player.setVehicle(null);
		player.breakFakeDeath();

		player.decayMe();

		player.setXYZInvisible(loc);
		player.setIsTeleporting(true);
		if(ref == null)
			ref = ReflectionTable.getInstance().getDefault();
		player.setReflection(ref);

		// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
		player.setLastClientPosition(null);
		player.setLastServerPosition(null);

		player.sendPacket(new TeleportToLocation(player, loc));
	}

	public class DimensionalRiftRoom
	{
		private final L2Territory _territory;
		private final Location _teleportCoords;
		private final boolean _isBossRoom;
		private final GArray<L2Spawn> _roomSpawns;

		public DimensionalRiftRoom(final L2Territory territory, final Location tele, final boolean isBossRoom)
		{
			_territory = territory;
			_teleportCoords = tele;
			_isBossRoom = isBossRoom;
			_roomSpawns = new GArray<L2Spawn>();
		}

		public Location getTeleportCoords()
		{
			return _teleportCoords;
		}

		public boolean checkIfInZone(final Location loc)
		{
			return checkIfInZone(loc.x, loc.y, loc.z);
		}

		public boolean checkIfInZone(final int x, final int y, final int z)
		{
			return _territory.isInside(x, y, z);
		}

		public boolean isBossRoom()
		{
			return _isBossRoom;
		}

		public GArray<L2Spawn> getSpawns()
		{
			return _roomSpawns;
		}
	}
}
