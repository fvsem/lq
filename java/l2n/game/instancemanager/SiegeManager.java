package l2n.game.instancemanager;

import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.tables.SkillTable;

public abstract class SiegeManager
{
	public static void addSiegeSkills(final L2Player character)
	{
		character.addSkill(SkillTable.getInstance().getInfo(246, 1), false);
		character.addSkill(SkillTable.getInstance().getInfo(247, 1), false);
		if(character.isNoble())
			character.addSkill(SkillTable.getInstance().getInfo(326, 1), false);

		if(character.getClan() != null && character.getClan().getHasCastle() > 0)
		{
			character.addSkill(SkillTable.getInstance().getInfo(844, 1), false);
			character.addSkill(SkillTable.getInstance().getInfo(845, 1), false);
		}
	}

	public static void removeSiegeSkills(final L2Player character)
	{
		character.removeSkill(SkillTable.getInstance().getInfo(246, 1), false);
		character.removeSkill(SkillTable.getInstance().getInfo(247, 1), false);
		character.removeSkill(SkillTable.getInstance().getInfo(326, 1), false);

		if(character.getClan() != null && character.getClan().getHasCastle() > 0)
		{
			character.removeSkill(SkillTable.getInstance().getInfo(844, 1), false);
			character.removeSkill(SkillTable.getInstance().getInfo(845, 1), false);
		}
	}

	public static boolean getCanRide()
	{
		if(TerritorySiege.isInProgress())
			return false;

		for(final Castle castle : CastleManager.getInstance().getCastles().values())
			if(castle != null && castle.getSiege().isInProgress())
				return false;
		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress != null && fortress.getSiege().isInProgress())
				return false;
		for(final ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			if(clanhall != null && clanhall.getSiege() != null && clanhall.getSiege().isInProgress())
				return false;
		return true;
	}

	public static Residence getSiegeUnitByObject(final L2Object activeObject)
	{
		return getSiegeUnitByCoord(activeObject.getX(), activeObject.getY());
	}

	public static Residence getSiegeUnitByCoord(final int x, final int y)
	{
		for(final Castle castle : CastleManager.getInstance().getCastles().values())
			if(castle.checkIfInZone(x, y))
				return castle;
		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress.checkIfInZone(x, y))
				return fortress;
		return null;
	}

	public static Siege getSiege(final L2Object activeObject, final boolean onlyActive)
	{
		return getSiege(activeObject.getX(), activeObject.getY(), onlyActive);
	}

	public static Siege getSiege(final int x, final int y, final boolean onlyActive)
	{
		for(final Castle castle : CastleManager.getInstance().getCastles().values())
			if(castle.getSiege().checkIfInZone(x, y, onlyActive))
				return castle.getSiege();
		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress.getSiege().checkIfInZone(x, y, onlyActive))
				return fortress.getSiege();
		for(final ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			if(clanhall.getSiege() != null && clanhall.getSiege().checkIfInZone(x, y, onlyActive))
				return clanhall.getSiege();
		return null;
	}

	public static void clearCastleRegistrations(final L2Clan clan)
	{
		for(final Castle castle : CastleManager.getInstance().getCastles().values())
			if(castle != null)
				castle.getSiege().clearSiegeClan(clan, true);
	}

	public static void clearFortressRegistrations(final L2Clan clan)
	{
		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress != null)
				fortress.getSiege().clearSiegeClan(clan, true);
	}
}
