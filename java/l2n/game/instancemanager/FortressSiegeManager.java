package l2n.game.instancemanager;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.model.instances.L2FortEnvoyInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2SupportUnitInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FortressSiegeManager extends SiegeManager
{
	private final static Logger _log = Logger.getLogger(FortressSiegeManager.class.getName());

	// Siege spawns settings
	private final static TIntObjectHashMap<GArray<L2NpcInstance>> _envoyNpcsList = new TIntObjectHashMap<GArray<L2NpcInstance>>();
	private final static TIntObjectHashMap<GArray<L2NpcInstance>> _commanderNpcsList = new TIntObjectHashMap<GArray<L2NpcInstance>>();
	private final static TIntObjectHashMap<GArray<SiegeSpawn>> _commanderSpawnList = new TIntObjectHashMap<GArray<SiegeSpawn>>();
	private final static TIntObjectHashMap<GArray<SiegeSpawn>> _flagList = new TIntObjectHashMap<GArray<SiegeSpawn>>();
	private final static TIntObjectHashMap<TIntIntHashMap> _guardDoorList = new TIntObjectHashMap<TIntIntHashMap>();
	private final static TIntObjectHashMap<IntArrayList> _commandCenterDoorList = new TIntObjectHashMap<IntArrayList>();

	public static void load()
	{
		try
		{
			InputStream is = new FileInputStream(new File(Config.SIEGE_FORTRESS_CONFIGURATION_FILE));
			Properties siegeSettings = new Properties();
			siegeSettings.load(is);
			is.close();

			for(Fortress fortress : FortressManager.getInstance().getFortresses().values())
			{
				GArray<L2NpcInstance> envoyNpcs = new GArray<L2NpcInstance>();
				GArray<L2NpcInstance> commanderNpcs = new GArray<L2NpcInstance>();
				GArray<SiegeSpawn> commanderSpawns = new GArray<SiegeSpawn>();
				GArray<SiegeSpawn> flagSpawns = new GArray<SiegeSpawn>();
				TIntIntHashMap guardDoors = new TIntIntHashMap();
				IntArrayList commandCenterDoors = new IntArrayList();

				for(int i = 1; i < 255; i++)
				{
					String spawnParams = siegeSettings.getProperty("N" + fortress.getId() + "Envoy" + i, "");

					if(spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(spawnParams.trim(), ";");
					try
					{
						int castle_id = Integer.parseInt(st.nextToken());
						int npc_id = Integer.parseInt(st.nextToken());
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));

						L2NpcInstance envoyNpc = new L2FortEnvoyInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(npc_id), castle_id);
						envoyNpc.setCurrentHpMp(envoyNpc.getMaxHp(), envoyNpc.getMaxMp(), true);
						envoyNpc.setXYZInvisible(loc.correctGeoZ());
						envoyNpc.setSpawnedLoc(envoyNpc.getLoc());
						envoyNpc.setHeading(loc.h);
						envoyNpcs.add(envoyNpc);
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "Error while loading envoy(s) for " + fortress.getName(), e);
					}
				}

				for(int i = 1; i < 255; i++)
				{
					String guardDoor = siegeSettings.getProperty("N" + fortress.getId() + "GuardDoor" + i, "");
					if(guardDoor.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(guardDoor.trim(), ";");
					guardDoors.put(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
				}

				for(int i = 1; i < 255; i++)
				{
					String commandCenter = siegeSettings.getProperty("N" + fortress.getId() + "CommandCenterDoor" + i, "");

					if(commandCenter.length() == 0)
						break;

					commandCenterDoors.add(Integer.parseInt(commandCenter));
				}

				for(int i = 1; i < 255; ++i)
				{
					String spawnParams = siegeSettings.getProperty("N" + fortress.getId() + "Commander" + i, "");

					if(spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(spawnParams.trim(), ",");
					try
					{
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
						int npc1_id = Integer.parseInt(st.nextToken());
						int npc2_id = Integer.parseInt(st.nextToken());

						L2NpcInstance commanderNpc = new L2SupportUnitInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(npc1_id));
						commanderNpc.setCurrentHpMp(commanderNpc.getMaxHp(), commanderNpc.getMaxMp(), true);
						commanderNpc.setXYZInvisible(loc.correctGeoZ());
						commanderNpc.setSpawnedLoc(commanderNpc.getLoc());
						commanderNpc.setHeading(loc.h);
						commanderNpc.spawnMe();

						commanderNpcs.add(commanderNpc);
						commanderSpawns.add(new SiegeSpawn(fortress.getId(), loc, npc2_id));
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "Error while loading commander(s) for " + fortress.getName(), e);
					}
				}

				for(int i = 1; i < 255; i++)
				{
					String _spawnParams = siegeSettings.getProperty("N" + fortress.getId() + "Flag" + i, "");

					if(_spawnParams.length() == 0)
						break;

					StringTokenizer st = new StringTokenizer(_spawnParams.trim(), ",");

					try
					{
						Location loc = new Location(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
						int flag_id = Integer.parseInt(st.nextToken());

						flagSpawns.add(new SiegeSpawn(fortress.getId(), loc, flag_id));
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "Error while loading control flag(s) for " + fortress.getName(), e);
					}
				}

				_envoyNpcsList.put(fortress.getId(), envoyNpcs);
				_commanderNpcsList.put(fortress.getId(), commanderNpcs);
				_commanderSpawnList.put(fortress.getId(), commanderSpawns);
				_flagList.put(fortress.getId(), flagSpawns);
				_guardDoorList.put(fortress.getId(), guardDoors);
				_commandCenterDoorList.put(fortress.getId(), commandCenterDoors);

				if(commanderSpawns.size() > 3)
					fortress.setFortType(1);

				if(envoyNpcs.isEmpty())
					_log.warning("Not found envoy Npc`s for " + fortress.getName());
				if(commanderNpcs.isEmpty())
					_log.warning("Not found commander Npc`s for " + fortress.getName());
				if(commanderSpawns.isEmpty())
					_log.warning("Not found commanders for " + fortress.getName());
				if(flagSpawns.isEmpty())
					_log.warning("Not found control flags for " + fortress.getName());
				if(guardDoors.isEmpty())
					_log.warning("Not found guard doors for " + fortress.getName());
				if(commandCenterDoors.isEmpty())
					_log.warning("Not found command center doors for " + fortress.getName());

				fortress.getSiege().setDefenderRespawnDelay(Config.SIEGE_FORT_DEFENDER_RESPAWN);
				fortress.getSiege().setSiegeClanMinLevel(Config.SIEGE_FORT_CLAN_MIN_LEVEL);
				fortress.getSiege().setSiegeLength(Config.SIEGE_FORT_LENGTH);

				fortress.getSiege().getZone().setActive(false);
				fortress.getSiege().startAutoTask(true);
			}
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "FortressSiegeManager: Error while loading siege data ", e);
		}
	}

	public static GArray<L2NpcInstance> getEnvoyNpcsList(int siegeUnitId)
	{
		return _envoyNpcsList.get(siegeUnitId);
	}

	public static GArray<L2NpcInstance> getCommanderNpcsList(int siegeUnitId)
	{
		return _commanderNpcsList.get(siegeUnitId);
	}

	public static GArray<SiegeSpawn> getCommanderSpawnList(int siegeUnitId)
	{
		return _commanderSpawnList.get(siegeUnitId);
	}

	public static GArray<SiegeSpawn> getFlagsList(int siegeUnitId)
	{
		return _flagList.get(siegeUnitId);
	}

	public static TIntIntHashMap getGuardDoors(int siegeUnitId)
	{
		return _guardDoorList.get(siegeUnitId);
	}

	public static IntArrayList getCommandCenterDoors(int siegeUnitId)
	{
		return _commandCenterDoorList.get(siegeUnitId);
	}

	public static boolean isCombatFlag(int itemId)
	{
		return itemId == 9819;
	}

	public static boolean checkIfCanPickup(L2Player player)
	{
		// if the player is mounted, attempt to unmount first. Only allow picking up the comabt flag if unmounting is successful.
		if(player.isMounted())
		{
			player.sendMessage("Вы, возможно, не поднимите это изделие, на в этой территории");
			return false;
		}

		// Cannot own 2 combat flag
		if(player.isCombatFlagEquipped())
		{
			player.sendMessage("У вас уже есть боевой флаг");
			return false;
		}

		L2Clan clan = player.getClan();
		Siege siege = getSiege(player);
		if(siege == null || clan == null || clan.getSiege() != siege || !clan.isAttacker())
		{
			player.sendMessage("Вы должны быть в атакующеv клане, чтобы поднять боевой флаг");
			return false;
		}

		return true;
	}

	public static FortressSiege getSiege(L2Object activeObject)
	{
		return getSiege(activeObject.getX(), activeObject.getY());
	}

	public static FortressSiege getSiege(int x, int y)
	{
		for(Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress.getSiege().checkIfInZone(x, y, true))
				return fortress.getSiege();
		return null;
	}

	public static int getDefenderRespawnDelay()
	{
		return Config.SIEGE_FORT_DEFENDER_RESPAWN;
	}

	public static int getSiegeClanMinLevel()
	{
		return Config.SIEGE_FORT_CLAN_MIN_LEVEL;
	}

	public static int getSiegeLength()
	{
		return Config.SIEGE_FORT_LENGTH;
	}
}
