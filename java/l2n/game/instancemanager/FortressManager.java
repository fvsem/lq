package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2SuspiciousMerchantInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;

import java.util.HashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FortressManager
{
	private final static Logger _log = Logger.getLogger(FortressManager.class.getName());

	private HashMap<Integer, Fortress> _fortresses;
	private ScheduledFuture<?> _fortCheckTask;
    private GArray<L2SuspiciousMerchantInstance> _l2SuspiciousMerchantInstance = new GArray<L2SuspiciousMerchantInstance>();

	public static FortressManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final FortressManager _instance = new FortressManager();
	}

	private FortressManager()
	{
		_log.info("Initializing FortressManager");
		load();
	}

	private void load()
	{
		final GArray<L2Zone> zones = ZoneManager.getInstance().getZoneByType(ZoneType.Fortress);
		if(zones.size() == 0)
			_log.info("Not found zones for Fortresses!!!");
		else
			for(final L2Zone zone : zones)
			{
				final Fortress fortress = new Fortress(zone.getIndex());
				final int index = zone.getIndex();
				fortress.init();
				getFortresses().put(index, fortress);
			}

		if(_fortCheckTask != null)
			_fortCheckTask.cancel(false);
        _fortCheckTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new FortCheckTask(), 3600000, 3600000);

		_log.info("Loaded: " + getFortresses().size() + " fortresses.");
	}

    public void setL2SuspiciousMerchantInstance(L2SuspiciousMerchantInstance l2SuspiciousMerchantInstances)
    {
        _l2SuspiciousMerchantInstance.add(l2SuspiciousMerchantInstances);
    }

    public void deleteL2SuspiciousMerchantInstance(L2SuspiciousMerchantInstance l2SuspiciousMerchantInstances)
    {
        _l2SuspiciousMerchantInstance.remove(l2SuspiciousMerchantInstances);
    }

    public GArray<L2SuspiciousMerchantInstance> getL2SuspiciousMerchantInstances()
    {
        return _l2SuspiciousMerchantInstance;
    }

	/**
	 * Возвращает крепость, соответствующую индексу.
	 */
	public Fortress getFortressByIndex(final int index)
	{
		for(final Fortress fortress : getFortresses().values())
			if(fortress != null && fortress.getId() == index)
				return fortress;
		return null;
	}

	/**
	 * Находит крепость по имени. Если такой крепости нет - возвращает null.
	 */
	public Fortress getFortressByName(final String name)
	{
		final int index = getFortressIndexByName(name);
		if(index > 0)
			return getFortresses().get(index);
		return null;
	}

	/**
	 * Находит крепость по владельцу (ID клана). Если такогой крепости нет - возвращает null.
	 */
	public final Fortress getFortressByOwner(final L2Clan clan)
	{
		if(clan == null)
			return null;

		for(final Fortress fortress : getFortresses().values())
			if(fortress.getOwnerId() == clan.getClanId())
				return fortress;
		return null;
	}

	/**
	 * Если координаты принадлежат зоне какой-либо крепости, возвращает эту крепость. Иначе возвращает null.
	 */
	public Fortress getFortressByObject(final L2Object activeObject)
	{
		return getFortressByCoord(activeObject.getX(), activeObject.getY());
	}

	/**
	 * Если обьект находится в зоне какой-либо крепости, возвращает эту крепость. Иначе возвращает null.
	 */
	public Fortress getFortressByCoord(final int x, final int y)
	{
		final int index = getFortressIndexByCoord(x, y);
		if(index > 0)
			return getFortresses().get(index);
		return null;
	}

	/**
	 * Если обьект находится в зоне какой-либо крепости, возвращает эту крепость. Иначе возвращает -1.
	 */
	public int getFortressIndex(final L2Object activeObject)
	{
		return getFortressIndexByCoord(activeObject.getX(), activeObject.getY());
	}

	/**
	 * Если координаты принадлежат зоне какой-либо крепости, возвращает индекс этой крепости. Иначе возвращает -1.
	 */
	public int getFortressIndexByCoord(final int x, final int y)
	{
		for(final Fortress fortress : getFortresses().values())
			if(fortress != null && fortress.checkIfInZone(x, y))
				return fortress.getId();
		return -1;
	}

	/**
	 * Находит крепость по имени, без учета регистра. Если не найден - возвращает -1.
	 */
	public int getFortressIndexByName(final String name)
	{
		for(final Fortress fortress : getFortresses().values())
			if(fortress != null && fortress.getName().equalsIgnoreCase(name.trim()))
				return fortress.getId();
		return -1;
	}

	/**
	 * Возвращает список, содержащий все крепости.
	 */
	public HashMap<Integer, Fortress> getFortresses()
	{
		if(_fortresses == null)
			_fortresses = new HashMap<Integer, Fortress>();
		return _fortresses;
	}

	public int findNearestFortressIndex(final int x, final int y, final int offset)
	{
		int index = getFortressIndexByCoord(x, y);
		if(index < 0)
		{
			double closestDistance = offset;
			for(final Fortress fortress : getFortresses().values())
			{
				if(fortress == null)
					continue;
				final double distance = fortress.getZone().findDistanceToZone(x, y, 0, false);
				if(closestDistance > distance)
				{
					closestDistance = distance;
					index = fortress.getId();
				}
			}
		}
		return index;
	}

	public final class FortCheckTask implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				for(final Fortress fort : getFortresses().values())
					if(fort.getOwner() != null && !fort.getSiege().isInProgress() && System.currentTimeMillis() - fort.getOwnDate() * 1000L > 168 * 60 * 60 * 1000L)
					{
						for(final L2Player player : fort.getOwner().getOnlineMembers(0))
							if(player != null)
							{
								player.sendPacket(new SystemMessage(SystemMessage.THE_REBEL_ARMY_RECAPTURED_THE_FORTRESS));
								if(fort.checkIfInZone(player))
									player.teleToClosestTown();
							}
						for(final L2DoorInstance door : fort.getDoors())
							door.closeMe();
						_log.warning("Fortress " + fort.getName() + " recaptured by NPC from clan " + ClanTable.getInstance().getClan(fort.getOwnerId()) + " (ownDate: " + fort.getOwnDate() + ")");
						fort.changeOwner(null);
					}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
		}
	}
}
