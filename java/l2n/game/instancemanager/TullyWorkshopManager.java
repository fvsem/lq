package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.scripts.ai.raidboss.Tully;
import l2n.game.scripts.npc.hellbound.DwarvenGhost;
import l2n.game.scripts.npc.hellbound.MysteriousAgent;
import l2n.game.scripts.npc.hellbound.TeleportationCubic;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TullyWorkshopManager
{
	private final class ActivateDestructionZone implements Runnable
	{
		private boolean value;
		private int time;
		private L2NpcInstance actor;

		public ActivateDestructionZone(boolean value, int time, L2NpcInstance actor)
		{
			this.value = value;
			this.time = time;
			this.actor = actor;
		}

		@Override
		public void run()
		{
			if(time / 60 - 1 > 0)
			{
				time -= 60;
				int min = time / 60;
				Functions.npcSayInRange(actor, Say2C.SHOUT, min + " минут  осталось.", 3000);
				alarmTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivateDestructionZone(value, time, actor), min == 1 ? 10 * 1000 : 60 * 1000);
			}
			else if(time == 20 || time == 30 || time == 40 || time == 50 || time == 60)
			{
				time -= 10;
				Functions.npcSayInRange(actor, Say2C.SHOUT, time + " секунд осталось.", 3000);
				alarmTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivateDestructionZone(value, time, actor), 10 * 1000);
			}
			else if(time <= 10)
			{
				destruction_zone.setActive(value);
				// чтоб не запускалась после выключения
				if(value)
					// через 5 минут после включения зоны - выключаем её
					alarmTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivateDestructionZone(false, 0, null), 5 * 60 * 1000);
			}
		}
	}

	private final class RespawnChallangers implements Runnable
	{
		@Override
		public void run()
		{
			// спауним Darion's Challenger
			spawnChallanges(true);
		}
	}

	private final static Logger _log = Logger.getLogger(TullyWorkshopManager.class.getName());

	/** Убиваем The Original Sin Warden, есть шанс что после его убийства в одной из комнат появится Darion's Faithful Servant. */
	private final static int CHANCE_SERVANT_SPAWN = 5;
	/** Убиваем Darion's Faithful Servant, есть шанс, что появится Мистики, которые могут телепортировать на 7-ой, секретный этаж. */
	private final static int CHANCE_MYSTERIOUS_SPAWN = 10;

	private static final int[][] FAITHFUL_SERVANT = new int[][] { { 22405, 22406, 22407 }, { 22408, 22409, 22410 } };
	private static final int MYSTERIOUS_AGENT = 32372;
	private static final Location[] RNDSPAWN_6FLOOR = new Location[]
	{
			new Location(-13312, 279172, -13599, -20300),
			new Location(-11696, 280208, -13599, 13244),
			new Location(-13008, 280496, -13599, 27480),
			new Location(-11984, 278880, -13599, -4472)
	};

	private static final Location[] RNDSPAWN_8FLOOR = new Location[]
	{
			new Location(-13312, 279172, -10492, -20300),
			new Location(-11696, 280208, -10492, 13244),
			new Location(-13008, 280496, -10492, 27480),
			new Location(-11984, 278880, -10492, -4472)
	};

	private final static int[][] SPAWN_MOBS =
	{
			/* Этаж 7 */
			// npcId x y z heading respawn_time
			{ 25599, -12281, 281497, -11935, 49151, 60 }, // igraz[25599]
			{ 25599, -11903, 281488, -11934, 49151, 60 }, // igraz[25599]
			{ 25597, -11599, 281323, -11933, -23808, 60 }, // hurrel[25597]
			{ 25597, -11381, 281114, -11934, -23808, 60 }, // hurrel[25597]
			{ 25597, -11089, 280819, -11934, -23808, 60 }, // hurrel[25597]
			{ 25597, -10818, 280556, -11934, -23808, 60 }, // hurrel[25597]
			{ 25598, -10697, 280244, -11936, 32768, 60 }, // gege[25598]
			{ 25598, -10702, 279926, -11936, 32768, 60 }, // gege[25598]
			{ 25598, -10722, 279470, -11936, 32768, 60 }, // gege[25598]
			{ 25598, -10731, 279126, -11936, 32768, 60 }, // gege[25598]
			{ 25597, -10903, 278798, -11934, 25680, 60 }, // hurrel[25597]
			{ 25597, -11134, 278558, -11934, 25680, 60 }, // hurrel[25597]
			{ 25597, -11413, 278265, -11934, 25680, 60 }, // hurrel[25597]
			{ 25597, -11588, 278072, -11935, 25680, 60 }, // hurrel[25597]
			{ 25599, -11966, 277935, -11936, 16384, 60 }, // igraz[25599]
			{ 25599, -12334, 277935, -11936, 16384, 60 }, // igraz[25599]
			{ 25599, -12739, 277935, -11936, 16384, 60 }, // igraz[25599]
			{ 25599, -13063, 277934, -11936, 16384, 60 }, // igraz[25599]
			{ 25597, -13357, 278058, -11935, 9068, 60 }, // hurrel[25597]
			{ 25597, -13617, 278289, -11935, 9068, 60 }, // hurrel[25597]
			{ 25597, -13920, 278567, -11935, 9068, 60 }, // hurrel[25597]
			{ 25597, -14131, 278778, -11936, 9068, 60 }, // hurrel[25597]
			{ 25598, -14284, 279140, -11936, 0, 60 }, // gege[25598]
			{ 25598, -14286, 279464, -11936, 0, 60 }, // gege[25598]
			{ 25598, -14290, 279909, -11935, 0, 60 }, // gege[25598]
			{ 25598, -14281, 280229, -11936, 0, 60 }, // gege[25598]
			{ 25597, -14184, 280545, -11936, -7548, 60 }, // hurrel[25597]
			{ 25597, -13946, 280792, -11936, -7548, 60 }, // hurrel[25597]
			{ 25597, -13626, 281105, -11936, -7548, 60 }, // hurrel[25597]
			{ 25597, -13386, 281360, -11935, -7548, 60 }, // hurrel[25597]
			{ 25599, -13077, 281506, -11935, 49151, 60 }, // igraz[25599]
			{ 25599, -12738, 281503, -11935, 49151, 60 }, // igraz[25599]
			{ MYSTERIOUS_AGENT, -12527, 279714, -11622, 16384, 60 } // unknown_agent[32372]
	};

	private final static int CHALLENGERS_RESPAWN = 10 * 60 * 1000; // время респауна Challengers'ов в секундах.
	private final static Location[] CHALLANGERS_SPAWN =
	{
			new Location(-12528, 279488, -11622, 16384, 25602), // challanger_kearetsea[25602]
			new Location(-12736, 279681, -11622, 0, 25600), // challanger_temenerr[25600]
			new Location(-12324, 279681, -11622, 32768, 25601) // challanger_draxious[25601]
	};

	/** с 1 этажа на 2, зона около РБ Zalios */
	public static final Location TullyFloor2LocationPoint = new Location(-14180, 273060, -13600);
	/** с 3 этажа на 4, зона около РБ Festina */
	public static final Location TullyFloor4LocationPoint = new Location(-14238, 273002, -10496);
	/** с 4 этажа на 5 */
	public static final Location TullyFloor5LocationPoint = new Location(-10440, 273320, -9062);
	/** с 5 этажа на 6 */
	public static final Location TullyFloor6LocationPoint = new Location(-12176, 279696, -13596);
	/** на 7 этаж */
	public static final Location TullyFloor7LocationPoint = new Location(-12501, 281397, -11936);
	/** на 8 этаж */
	public static final Location TullyFloor8LocationPoint = new Location(-12176, 279696, -10492);
	/** на крышу */
	public static final Location TullyRoofLocationPoint = new Location(21935, 243923, 11088);

	// Teleportation Cubic для перехода между 6 и 8 этажами
	private static final int CUBE_FOR_TULLY = 32467;
	private static final Location[] TELEPORT_CUBE_LOCATIONS = new Location[]
	{
			// x y z heading mode
			new Location(-12752, 279696, -13596, 32768, 1), // 6 этаж
			new Location(-12752, 279696, -10492, 29664, 2), // 8 этаж
			new Location(-12527, 279714, -11622, 16384, 3) // 7 этаж
	};

	// на 2-ом, для перехода на новый уровень, спавнятся при инцилизации рандомно.
	private static final int WORKSHOP_TELEPORTER = 32637;
	private final static Location[] DOOR_STAGE_RND_COORDS =
	{
			new Location(-11704, 274360, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-11752, 275784, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-13208, 275816, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-12984, 274728, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-13352, 275656, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-14360, 274712, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-14392, 273240, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-13304, 273464, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-14312, 273096, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-13272, 272088, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-11816, 272024, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-10600, 274664, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-10648, 273160, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-11640, 272120, -13626, 0, WORKSHOP_TELEPORTER),
			new Location(-12056, 273128, -13626, 0, WORKSHOP_TELEPORTER)
	};

	private static final Location GHOST_TOLLES_LOCATION = new Location(-45531, 245872, -14192, 49151);

	public static final L2Zone destruction_zone = ZoneManager.getInstance().getZoneById(ZoneType.damage, 183, false);

	private ScheduledFuture<ActivateDestructionZone> alarmTask = null;
	private final ConcurrentHashMap<L2NpcInstance, Boolean> darion_challangers = new ConcurrentHashMap<L2NpcInstance, Boolean>();
	private TeleportationCubic cubic7Floor = null;
	private MysteriousAgent agent = null;

	public static final TullyWorkshopManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private TullyWorkshopManager()
	{
		Location tp = DOOR_STAGE_RND_COORDS[Rnd.get(DOOR_STAGE_RND_COORDS.length)];
		spawnNpc(tp.id, tp.x, tp.y, tp.z, 0, 30, 0, false);

		// спауним Old Dwarven Ghost
		try
		{
			DwarvenGhost npc = new DwarvenGhost(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(Tully.GHOST_TOLLES));
			npc.sedMode(3);
			npc.setSpawnedLoc(GHOST_TOLLES_LOCATION);
			npc.onSpawn();
			npc.spawnMe(npc.getSpawnedLoc());
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TullyWorkshopManager: error spawn Old Dwarven Ghost: ", e);
		}

		// спауним Teleportation Сubic
		try
		{
			TeleportationCubic npc;
			for(Location loc : TELEPORT_CUBE_LOCATIONS)
			{
				npc = new TeleportationCubic(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(CUBE_FOR_TULLY), loc.id);
				npc.setSpawnedLoc(loc);
				npc.onSpawn();
				npc.spawnMe(npc.getSpawnedLoc());

				// кубик на 7 этаже показываем только когда все Darion's Challenger мертвы
				if(loc.id == 3)
				{
					cubic7Floor = npc;
					cubic7Floor.decayMe();
				}
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TullyWorkshopManager: error spawn Teleportation Сubic: ", e);
		}

		try
		{
			for(int[] mob_loc : SPAWN_MOBS)
				spawnNpc(mob_loc[0], mob_loc[1], mob_loc[2], mob_loc[3], mob_loc[4], mob_loc[5], mob_loc.length > 6 ? mob_loc[6] : 0, false);
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TullyWorkshopManager: error spawn other mobs: ", e);
		}

		// спауним Darion's Challenger
		spawnChallanges(false);

		_log.info("TullyWorkshopManager: loaded.");
	}

	private void spawnNpc(int id, int x, int y, int z, int h, int respawn, int respawn_rnd, boolean ghost)
	{
		L2NpcInstance npc;
		try
		{
			L2NpcTemplate template = NpcTable.getTemplate(id);
			if(template == null)
			{
				_log.warning("TullyWorkshopManager: mob with ID = " + id + " can't be spawned!");
				return;
			}
			L2Spawn spawnData = new L2Spawn(template);
			spawnData.setAmount(1);
			spawnData.setLocx(x);
			spawnData.setLocy(y);
			spawnData.setLocz(z);
			spawnData.setHeading(h);
			spawnData.setRespawnDelay(respawn, respawn_rnd);
			spawnData.setRespawnTime(0);
			npc = spawnData.spawnOne();
			if(id == MYSTERIOUS_AGENT)
			{
				agent = (MysteriousAgent) npc;
				agent.setMode(1);
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "TullyWorkshopManager: error spawn ", e);
		}
	}

	public void trySpawnFaithfulServant(int floor)
	{
		switch (floor)
		{
			case 6:
			{
				if(Rnd.chance(CHANCE_SERVANT_SPAWN))
					Functions.spawn(RNDSPAWN_6FLOOR[Rnd.get(RNDSPAWN_6FLOOR.length)], FAITHFUL_SERVANT[0][Rnd.get(3)]);
				break;
			}
			case 8:
			{
				if(Rnd.chance(CHANCE_SERVANT_SPAWN))
					Functions.spawn(RNDSPAWN_8FLOOR[Rnd.get(RNDSPAWN_8FLOOR.length)], FAITHFUL_SERVANT[1][Rnd.get(3)]);
				break;
			}
		}
	}

	public void trySpawnMysteriousAgent(int floor)
	{
		switch (floor)
		{
			case 6:
			{
				if(Rnd.chance(CHANCE_MYSTERIOUS_SPAWN))
					Functions.spawn(RNDSPAWN_6FLOOR[Rnd.get(RNDSPAWN_6FLOOR.length)], MYSTERIOUS_AGENT);
				break;
			}
			case 8:
			{
				if(Rnd.chance(CHANCE_MYSTERIOUS_SPAWN))
					Functions.spawn(RNDSPAWN_8FLOOR[Rnd.get(RNDSPAWN_8FLOOR.length)], MYSTERIOUS_AGENT);
				break;
			}
		}
	}

	public void increaseAlarmTime()
	{

	}

	public void runAlarmTask(boolean val, int time, L2NpcInstance actor)
	{
		if(val)
		{
			Functions.npcSayInRange(actor, Say2C.SHOUT, "Детонатор активирован у вас всего 10 минут", 3000);
			if(alarmTask != null)
			{
				alarmTask.cancel(false);
				alarmTask = null;
			}
			alarmTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivateDestructionZone(val, time, actor), 60 * 1000);
		}
	}

	public void killChallanger(final L2NpcInstance challanger, final L2Player killer)
	{
		// если все Darion's Challenger убиты, телепортируем на крышу
		if(checkDeadChallanger(challanger))
		{
			// Телепортирует на крашу, при убийстве всех Darion's Challenger
			if(killer.isInParty())
				killer.getParty().teleport(TullyWorkshopManager.TullyRoofLocationPoint);
			else
				killer.teleToLocation(TullyWorkshopManager.TullyRoofLocationPoint);

			// скрываем агенты, пока Darion's Challenger мертвы
			agent.decayMe();
			// отображаем кубик
			cubic7Floor.spawnMe();

			darion_challangers.clear();
			// запускаем респаун Darion's Challengers
			L2GameThreadPools.getInstance().scheduleGeneral(new RespawnChallangers(), CHALLENGERS_RESPAWN);
		}
	}

	private boolean checkDeadChallanger(final L2NpcInstance challanger)
	{
		if(!darion_challangers.isEmpty())
		{
			if(darion_challangers.containsKey(challanger))
				darion_challangers.put(challanger, true);

			for(boolean dead : darion_challangers.values())
				if(!dead)
					return false;

			// вернёт true только если список не пустой и все убиты
			return true;
		}
		return false;
	}

	public void rejectKillChallanges(final L2NpcInstance agent, final L2Player player)
	{
		L2Player target;
		GArray<L2Player> list = agent.getAroundPlayers(500);
		if(list.isEmpty())
			target = player;
		else
			target = list.get(Rnd.get(list.size()));

		for(L2NpcInstance challanger : darion_challangers.keySet())
			if(challanger != null)
				challanger.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, target, Rnd.get(1, 100));
	}

	// FIXME по идее их нужно спаунить когда Darion жив, вместе с Mysterious Agent'ом
	// если дарион мертв, то спаунить кубик, Mysterious Agentа спаунить НЕ НУЖНО.
	public void spawnChallanges(boolean respawn)
	{
		if(respawn)
		{
			agent.spawnMe();
			cubic7Floor.decayMe();
		}

		darion_challangers.clear();
		for(Location loc : CHALLANGERS_SPAWN)
			darion_challangers.put(Functions.spawn(loc, loc.id), false);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TullyWorkshopManager _instance = new TullyWorkshopManager();
	}
}
