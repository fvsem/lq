package l2n.game.instancemanager.boss;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EpicBossState
{
	public static enum State
	{
		NOTSPAWN, // 0
		ALIVE, // 1
		DEAD, // 2
		INTERVAL // 3
	}

	public static enum EpicBoss
	{
		ANTHARAS(29019, "Antharas"),
		BAIUM(29020, "Baium"),
		VALAKAS(29028, "Valakas"),
		FRINTEZZA(29045, "Frintezza"),
		VAN_HALTER(29062, "Andreas Van Halter"),
		SAILREN(29065, "Sailren"),
		BAYLOR(29099, "Baylor"),
		EPIDOS(25609, "Epidos"),
		BELETH(29118, "Beleth");

		public final int id;
		private final String name;

		private EpicBoss(final int id, final String name)
		{
			this.id = id;
			this.name = name;
		}

		public static EpicBoss getById(final int id)
		{
			for(final EpicBoss eb : values())
				if(eb.id == id)
					return eb;
			throw new NoSuchElementException("EpicBoss not found for id: '" + id + "'.\n");
		}

		@Override
		public String toString()
		{
			return name;
		}
	}

	private int _bossId;
	private long _respawnDate;
	private State _state;

	private static final Logger _log = Logger.getLogger(EpicBossState.class.getName());

	public int getBossId()
	{
		return _bossId;
	}

	public void setBossId(final int newId)
	{
		_bossId = newId;
	}

	public State getState()
	{
		return _state;
	}

	public boolean isNotSpawn()
	{
		return _state == State.NOTSPAWN;
	}

	public boolean isAlive()
	{
		return _state == State.ALIVE;
	}

	public boolean isInterval()
	{
		return _state == State.INTERVAL;
	}

	public void setState(final State newState)
	{
		_state = newState;
	}

	public long getRespawnDate()
	{
		return _respawnDate;
	}

	public void setRespawnDate(final long interval)
	{
		_respawnDate = interval + System.currentTimeMillis();
	}

	public EpicBossState(final int bossId)
	{
		this(bossId, true);
	}

	public EpicBossState(final int bossId, final boolean isDoLoad)
	{
		_bossId = bossId;
		if(isDoLoad)
			load();
	}

	public void load()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM epic_boss_spawn WHERE bossId = " + _bossId + " LIMIT 1");
			rset = statement.executeQuery();

			if(rset.next())
			{
				_respawnDate = rset.getLong("respawnDate") * 1000;

				if(_respawnDate - System.currentTimeMillis() <= 0)
					_state = State.NOTSPAWN;
				else
				{
					final int tempState = rset.getInt("state");
					switch (tempState)
					{
						case 0:
							_state = State.NOTSPAWN;
							break;
						case 1:
							_state = State.ALIVE;
							break;
						case 2:
							_state = State.DEAD;
							break;
						case 3:
							_state = State.INTERVAL;
							break;
						default:
							_state = State.NOTSPAWN;
					}
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void save()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO epic_boss_spawn (bossId,respawnDate,state) VALUES(?,?,?)");
			statement.setInt(1, _bossId);
			statement.setInt(2, (int) (_respawnDate / 1000));
			statement.setInt(3, _state.ordinal());
			statement.execute();
			statement.close();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void update()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE epic_boss_spawn SET respawnDate = ?, state = ? WHERE bossId = ?");
			statement.setInt(1, (int) (_respawnDate / 1000));
			statement.setInt(2, _state.ordinal());
			statement.setInt(3, _bossId);
			statement.executeUpdate();
			statement.close();

			final Date dt = new Date(_respawnDate);
			_log.info("EpicBossState: " + EpicBoss.getById(_bossId) + " - respawn[" + Util.datetimeFormatter.format(dt) + "] state[" + _state + "]");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Exeption on update EpicBossState: ID " + _bossId + ", RespawnDate:" + _respawnDate / 1000 + ", State:" + _state, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void setNextRespawnDate(final long newRespawnDate)
	{
		_respawnDate = newRespawnDate;
	}

	public long getInterval()
	{
		final long interval = _respawnDate - System.currentTimeMillis();
		return interval > 0 ? interval : 0;
	}
}
