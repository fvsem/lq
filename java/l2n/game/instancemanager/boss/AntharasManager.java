package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.EpicBossState.State;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2BossInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;

public class AntharasManager
{
	private static final Logger _log = Logger.getLogger(AntharasManager.class.getName());

	private static class AntharasSpawn implements Runnable
	{
		private int _distance = 2550;
		private int _taskId = 0;
		private GArray<L2Player> _players = getPlayersInside();

		private AntharasSpawn(int taskId)
		{
			_taskId = taskId;
		}

		@Override
		public void run()
		{
			int npcId;
			SocialAction sa = null;

			if(_socialTask != null)
			{
				_socialTask.cancel(false);
				_socialTask = null;
			}

			switch (_taskId)
			{
				case 1: // spawn.
					if(_antharas != null)
						return;

					// Strength of Antharas is decided by the number of players that invaded the lair.
					if(Config.FWA_OLDANTHARAS)
						npcId = ANTHARAS_OLD; // old
					else if(_players.size() <= Config.FWA_LIMITOFWEAK)
						npcId = ANTHARAS_WEAK; // weak
					else if(_players.size() >= Config.FWA_LIMITOFNORMAL)
						npcId = ANTHARAS_STRONG; // strong
					else
						npcId = ANTHARAS_NORMAL; // normal

					Dying = false;

					// do spawn.
					_antharas = new L2BossInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(npcId));
					_antharas.setSpawnedLoc(_antharasLocation);
					_antharas.onSpawn();
					_antharas.spawnMe(_antharas.getSpawnedLoc());
					_antharas.setImmobilized(true);

					_state.setRespawnDate(getRespawnInterval());
					_state.setState(State.ALIVE);
					_state.update();

					// setting 1st time of minions spawn task.
					if(!Config.FWA_OLDANTHARAS)
					{
						int intervalOfBehemoth;
						int intervalOfBomber;

						// Interval of minions is decided by the number of players
						// that invaded the lair.
						if(_players.size() <= Config.FWA_LIMITOFWEAK) // weak
						{
							intervalOfBehemoth = Config.FWA_INTERVALOFBEHEMOTHONWEAK;
							intervalOfBomber = Config.FWA_INTERVALOFBOMBERONWEAK;
						}
						else if(_players.size() >= Config.FWA_LIMITOFNORMAL) // strong
						{
							intervalOfBehemoth = Config.FWA_INTERVALOFBEHEMOTHONSTRONG;
							intervalOfBomber = Config.FWA_INTERVALOFBOMBERONSTRONG;
						}
						else
						// normal
						{
							intervalOfBehemoth = Config.FWA_INTERVALOFBEHEMOTHONNORMAL;
							intervalOfBomber = Config.FWA_INTERVALOFBOMBERONNORMAL;
						}

						// spawn Behemoth
						_behemothSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new BehemothSpawn(intervalOfBehemoth), 30000);

						// spawn Bomber.
						_bomberSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new BomberSpawn(intervalOfBomber), 30000);
					}

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(2), 16);
					break;
				case 2:
					// set camera.
					for(L2Player pc : _players)
						if(pc.getDistance(_antharas) <= _distance)
						{
							pc.enterMovieMode();
							pc.specialCamera(_antharas, 700, 13, -19, 0, 10000);
						}
						else
							pc.leaveMovieMode();

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(3), 3000);
					break;
				case 3:
					// do social.
					sa = new SocialAction(_antharas.getObjectId(), 1);
					_antharas.broadcastPacket(sa);

					// set camera.
					for(L2Player pc : _players)
						if(pc.getDistance(_antharas) <= _distance)
						{
							pc.enterMovieMode();
							pc.specialCamera(_antharas, 700, 13, 0, 6000, 10000);
						}
						else
							pc.leaveMovieMode();

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(4), 10000);
					break;
				case 4:
					// set camera.
					for(L2Player pc : _players)
						if(pc.getDistance(_antharas) <= _distance)
						{
							pc.enterMovieMode();
							pc.specialCamera(_antharas, 3800, 0, -3, 0, 10000);
						}
						else
							pc.leaveMovieMode();

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(5), 200);
					break;
				case 5:
					// do social.
					sa = new SocialAction(_antharas.getObjectId(), 2);
					_antharas.broadcastPacket(sa);

					// set camera.
					for(L2Player pc : _players)
						if(pc.getDistance(_antharas) <= _distance)
						{
							pc.enterMovieMode();
							pc.specialCamera(_antharas, 1200, 0, -3, 22000, 11000);
						}
						else
							pc.leaveMovieMode();

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(6), 10800);
					break;
				case 6:
					// set camera.
					for(L2Player pc : _players)
						if(pc.getDistance(_antharas) <= _distance)
						{
							pc.enterMovieMode();
							pc.specialCamera(_antharas, 1200, 0, -3, 300, 2000);
						}
						else
							pc.leaveMovieMode();

					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(7), 1900);
					break;
				case 7:
					_antharas.abortCast();
					// reset camera.
					for(L2Player pc : _players)
						pc.leaveMovieMode();

					_mobiliseTask = L2GameThreadPools.getInstance().scheduleGeneral(new SetMobilised(_antharas), 16);

					_antharas.setRunning();

					// move at random.
					if(FWA_MOVEATRANDOM)
					{
						Location pos = new Location(Rnd.get(175000, 178500), Rnd.get(112400, 116000), -7707, 0);
						_moveAtRandomTask = L2GameThreadPools.getInstance().scheduleGeneral(new MoveAtRandom(_antharas, pos), 32);
					}

					_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 600000);

					_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWA_ACTIVITYTIMEOFANTHARAS);
					break;
			}
		}
	}

	// do spawn Behemoth.
	private static class BehemothSpawn implements Runnable
	{
		private int _interval;

		public BehemothSpawn(int interval)
		{
			_interval = interval;
		}

		@Override
		public void run()
		{
			L2MonsterInstance behemoth = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(29069));
			behemoth.onSpawn();
			behemoth.setSpawnedLoc(new Location(Rnd.get(175000, 179900), Rnd.get(112400, 116000), -7709));
			behemoth.spawnMe(behemoth.getSpawnedLoc());
			_monsters.add(behemoth);

			if(_behemothSpawnTask != null)
			{
				_behemothSpawnTask.cancel(false);
				_behemothSpawnTask = null;
			}

			// repeat.
			_behemothSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new BehemothSpawn(_interval), _interval);
		}
	}

	// do spawn Bomber.
	private static class BomberSpawn implements Runnable
	{
		private int _interval;

		public BomberSpawn(int interval)
		{
			_interval = interval;
		}

		@Override
		public void run()
		{
			int npcId = Rnd.get(29070, 29076);

			L2MonsterInstance bomber = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(npcId));
			bomber.onSpawn();
			bomber.setSpawnedLoc(new Location(Rnd.get(175000, 179900), Rnd.get(112400, 116000), -7709));
			bomber.spawnMe(bomber.getSpawnedLoc());
			_monsters.add(bomber);
			// set self destruction.
			_selfDestructionTask = L2GameThreadPools.getInstance().scheduleGeneral(new SelfDestructionOfBomber(bomber), 3000);

			if(_bomberSpawnTask != null)
			{
				_bomberSpawnTask.cancel(false);
				_bomberSpawnTask = null;
			}

			// repeat.
			_bomberSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new BomberSpawn(_interval), _interval);
		}
	}

	/** Class<?> ends the activity of the Bosses after a interval of time Exits the battle field in any way... */
	private static class ActivityTimeEnd implements Runnable
	{
		@Override
		public void run()
		{
			sleep();
		}
	}

	private static class CheckLastAttack implements Runnable
	{
		@Override
		public void run()
		{
			if(_state.getState().equals(EpicBossState.State.ALIVE))
				if(_lastAttackTime + FWA_LIMITUNTILSLEEP < System.currentTimeMillis())
					sleep();
				else
					_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 60000);
		}
	}

	// do spawn teleport cube.
	private static class CubeSpawn implements Runnable
	{
		@Override
		public void run()
		{
			if(_behemothSpawnTask != null)
			{
				_behemothSpawnTask.cancel(false);
				_behemothSpawnTask = null;
			}
			if(_bomberSpawnTask != null)
			{
				_bomberSpawnTask.cancel(false);
				_bomberSpawnTask = null;
			}
			if(_selfDestructionTask != null)
			{
				_selfDestructionTask.cancel(false);
				_selfDestructionTask = null;
			}

			_teleportCube = new L2NpcInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(_teleportCubeId));
			_teleportCube.setCurrentHpMp(_teleportCube.getMaxHp(), _teleportCube.getMaxMp(), true);
			_teleportCube.setSpawnedLoc(_teleportCubeLocation);
			_teleportCube.spawnMe(_teleportCube.getSpawnedLoc());
		}
	}

	// at end of interval.
	private static class IntervalEnd implements Runnable
	{
		@Override
		public void run()
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();
		}
	}

	// Move at random on after Antharas appears.
	private static class MoveAtRandom implements Runnable
	{
		private L2NpcInstance _npc;
		private Location _pos;

		public MoveAtRandom(L2NpcInstance npc, Location pos)
		{
			_npc = npc;
			_pos = pos;
		}

		@Override
		public void run()
		{
			if(_npc.getAI().getIntention() == AI_INTENTION_ACTIVE)
				_npc.moveToLocation(_pos, 0, false);
		}
	}

	private static class onAnnihilated implements Runnable
	{
		@Override
		public void run()
		{
			sleep();
		}
	}

	// do self destruction.
	private static class SelfDestructionOfBomber implements Runnable
	{
		private L2NpcInstance _bomber;

		public SelfDestructionOfBomber(L2NpcInstance bomber)
		{
			_bomber = bomber;
		}

		@Override
		public void run()
		{
			L2Skill skill = null;
			switch (_bomber.getNpcId())
			{
				case 29070:
				case 29071:
				case 29072:
				case 29073:
				case 29074:
				case 29075:
					skill = SkillTable.getInstance().getInfo(5097, 1);
					break;
				case 29076:
					skill = SkillTable.getInstance().getInfo(5094, 1);
					break;
			}

			_bomber.doCast(skill, null, false);
		}
	}

	// action is enabled the boss.
	private static class SetMobilised implements Runnable
	{
		private L2BossInstance _boss;

		public SetMobilised(L2BossInstance boss)
		{
			_boss = boss;
		}

		@Override
		public void run()
		{
			_boss.setImmobilized(false);
		}
	}

	// location of teleport cube.
	private static final int _teleportCubeId = 31859;
	private static final int _antharasId = 29019;

	private static final Location _teleportCubeLocation = new Location(177615, 114941, -7709, 0);
	private static final Location _antharasLocation = new Location(181323, 114850, -7623, 32542);

	private static L2BossInstance _antharas = null;
	private static L2NpcInstance _teleportCube = null;

	// instance of monsters.
	private static List<L2NpcInstance> _monsters = new ArrayList<L2NpcInstance>();

	// tasks.
	private static ScheduledFuture<?> _cubeSpawnTask = null;
	private static ScheduledFuture<?> _monsterSpawnTask = null;
	private static ScheduledFuture<?> _intervalEndTask = null;
	private static ScheduledFuture<?> _socialTask = null;
	private static ScheduledFuture<?> _mobiliseTask = null;
	private static ScheduledFuture<?> _behemothSpawnTask = null;
	private static ScheduledFuture<?> _bomberSpawnTask = null;
	private static ScheduledFuture<?> _selfDestructionTask = null;
	private static ScheduledFuture<?> _moveAtRandomTask = null;
	private static ScheduledFuture<?> _sleepCheckTask = null;
	private static ScheduledFuture<?> _onAnnihilatedTask = null;
	private static ScheduledFuture<ActivityTimeEnd> _activityTimeEndTask = null;

	private static final int ANTHARAS_CIRCLET = 8568;

	private static EpicBossState _state;
	private static L2Zone _zone;
	private static long _lastAttackTime = 0;

	private static final boolean FWA_MOVEATRANDOM = true;

	private static final int FWA_LIMITUNTILSLEEP = 30 * 60 * 1000; // 30 min

	private static final int ANTHARAS_OLD = 29019;
	private static final int ANTHARAS_WEAK = 29066;
	private static final int ANTHARAS_NORMAL = 29067;
	private static final int ANTHARAS_STRONG = 29068;

	private static boolean Dying = false;

	private static void banishForeigners()
	{
		for(L2Player player : getPlayersInside())
			player.teleToClosestTown();
	}

	private synchronized static void checkAnnihilated()
	{
		if(_onAnnihilatedTask == null && isPlayersAnnihilated())
			_onAnnihilatedTask = L2GameThreadPools.getInstance().scheduleGeneral(new onAnnihilated(), 5000);
	}

	private static GArray<L2Player> getPlayersInside()
	{
		return getZone().getInsidePlayers();
	}

	private static int getRespawnInterval()
	{
		return Rnd.get(Config.FWA_FIXINTERVALOFANTHARAS, Config.FWA_FIXINTERVALOFANTHARAS + Config.FWA_RANDOMINTERVALOFANTHARAS);
	}

	public static L2Zone getZone()
	{
		return _zone;
	}

	private static boolean isPlayersAnnihilated()
	{
		for(L2Player pc : getPlayersInside())
			if(!pc.isDead())
				return false;
		return true;
	}

	private static void onAntharasDie(L2Character killer)
	{
		if(Dying)
			return;

		Dying = true;
		_state.setRespawnDate(getRespawnInterval());
		_state.setState(EpicBossState.State.INTERVAL);
		_state.update();

		Log.add("Antharas died", "bosses");

		_cubeSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new CubeSpawn(), 10000);

		if(killer != null && killer.isPlayable())
		{
			L2Player pc = killer.getPlayer();
			if(pc == null)
				return;
			L2Party party = pc.getParty();
			if(party != null)
			{
				for(L2Player partyMember : party.getPartyMembers())
					if(partyMember != null && pc.isInRange(partyMember, 5000) && partyMember.getInventory().getItemByItemId(ANTHARAS_CIRCLET) == null)
						partyMember.getInventory().addItem(ANTHARAS_CIRCLET, 1, 0, null);
			}
			else if(pc.getInventory().getItemByItemId(ANTHARAS_CIRCLET) == null)
				pc.getInventory().addItem(ANTHARAS_CIRCLET, 1, 0, null);
		}
	}

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(self == null)
			return;
		if(self.isPlayer() && _state != null && _state.getState() == State.ALIVE && _zone != null && _zone.checkIfInZone(self.getX(), self.getY()))
			checkAnnihilated();
		else if(self.isNpc() && (self.getNpcId() == ANTHARAS_OLD || self.getNpcId() == ANTHARAS_WEAK || self.getNpcId() == ANTHARAS_NORMAL || self.getNpcId() == ANTHARAS_STRONG))
			onAntharasDie(killer);
	}

	private static void setIntervalEndTask()
	{
		setUnspawn();

		if(_state.getState().equals(EpicBossState.State.ALIVE))
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();
			return;
		}

		if(!_state.getState().equals(EpicBossState.State.INTERVAL))
		{
			_state.setRespawnDate(getRespawnInterval());
			_state.setState(EpicBossState.State.INTERVAL);
			_state.update();
		}

		_intervalEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new IntervalEnd(), _state.getInterval());
	}

	// clean Antharas's lair.
	private static void setUnspawn()
	{
		// eliminate players.
		banishForeigners();

		if(_antharas != null)
			_antharas.deleteMe();
		_antharas = null;

		if(_teleportCube != null)
			_teleportCube.deleteMe();
		_teleportCube = null;

		for(L2NpcInstance mob : _monsters)
			mob.deleteMe();
		_monsters.clear();

		// not executed tasks is canceled.
		if(_cubeSpawnTask != null)
		{
			_cubeSpawnTask.cancel(false);
			_cubeSpawnTask = null;
		}
		if(_monsterSpawnTask != null)
		{
			_monsterSpawnTask.cancel(false);
			_monsterSpawnTask = null;
		}
		if(_intervalEndTask != null)
		{
			_intervalEndTask.cancel(false);
			_intervalEndTask = null;
		}
		if(_socialTask != null)
		{
			_socialTask.cancel(false);
			_socialTask = null;
		}
		if(_mobiliseTask != null)
		{
			_mobiliseTask.cancel(false);
			_mobiliseTask = null;
		}
		if(_behemothSpawnTask != null)
		{
			_behemothSpawnTask.cancel(false);
			_behemothSpawnTask = null;
		}
		if(_bomberSpawnTask != null)
		{
			_bomberSpawnTask.cancel(false);
			_bomberSpawnTask = null;
		}
		if(_selfDestructionTask != null)
		{
			_selfDestructionTask.cancel(false);
			_selfDestructionTask = null;
		}
		if(_moveAtRandomTask != null)
		{
			_moveAtRandomTask.cancel(false);
			_moveAtRandomTask = null;
		}
		if(_sleepCheckTask != null)
		{
			_sleepCheckTask.cancel(false);
			_sleepCheckTask = null;
		}
		if(_activityTimeEndTask != null)
		{
			_activityTimeEndTask.cancel(false);
			_activityTimeEndTask = null;
		}
		if(_onAnnihilatedTask != null)
		{
			_onAnnihilatedTask.cancel(false);
			_onAnnihilatedTask = null;
		}
	}

	public static void init()
	{
		_state = new EpicBossState(_antharasId);
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.epic, 702002, false);

		_log.info("EpicBossManager: Antharas state - " + _state.getState() + ".");
		if(!_state.getState().equals(EpicBossState.State.NOTSPAWN))
			setIntervalEndTask();

		// Выводим только если есть дата следующего спауна
		if(_state.isInterval())
			_log.info("EpicBossManager: Antharas next spawn - " + Util.datetimeFormatter.format(_state.getRespawnDate()) + ".");
	}

	private static void sleep()
	{
		setUnspawn();
		if(_state.getState().equals(EpicBossState.State.ALIVE))
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();
		}
	}

	public static void setLastAttackTime()
	{
		_lastAttackTime = System.currentTimeMillis();
	}

	// setting Antharas spawn task.
	public synchronized static void setAntharasSpawnTask()
	{
		if(_monsterSpawnTask == null)
			_monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new AntharasSpawn(1), Config.FWA_APPTIMEOFANTHARAS);
	}

	public static boolean isEnableEnterToLair()
	{
		return _state.getState() == EpicBossState.State.NOTSPAWN;
	}

	public static final EpicBossState getState()
	{
		return _state;
	}
}
