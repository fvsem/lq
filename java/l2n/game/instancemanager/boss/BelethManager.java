package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.procedure.INgVoidProcedure;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.EpicBossState.State;
import l2n.game.model.L2ObjectProcedures;
import l2n.game.model.L2ObjectProcedures.SendPacketProc;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.DoorTable;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 03.08.2011
 * @time 4:54:08
 */
public class BelethManager
{
	private final static Logger _log = Logger.getLogger(BelethManager.class.getName());

	private final class SpawnMinions implements Runnable
	{
		@Override
		public void run()
		{
			// прячим пока Beleth'а
			beleth.teleToLocation(new Location(-105200, -253104, -15264, -16384));
			// запустить такс через минуту для появления белета, в рандомной точке
			L2GameThreadPools.getInstance().scheduleGeneral(new ShowBeleth(), 60 * 1000);

			for(int index = MINION_POSITIONS.length; index-- > 0;)
			{
				final Location[] pos = MINION_POSITIONS[index];
				for(final Location loc : pos)
					minions.addLastUnsafe(Functions.spawn(loc, 29119));
			}

			// стартуем AI у всех миньёнов
			minions.forEachThread(L2ObjectProcedures.PROC_START_AI);

			// запускаем таск для проверки последней атаки
			_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 10 * 60 * 1000);
		}
	}

	private final class ShowBeleth implements Runnable
	{
		@Override
		public void run()
		{
			// Показываем белета и спауним в рандомную точку
			final int pos = Rnd.get(MINION_POSITIONS.length);
			beleth.teleToLocation(MINION_POSITIONS[pos][Rnd.get(MINION_POSITIONS[pos].length)].rnd(50, 100, false));

			// стартуем AI Белефа
			L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(-1, null), 300);
		}
	}

	private final class DoActionBeleth implements Runnable
	{
		private final int _socialAction;
		private final L2Skill _skill;

		public DoActionBeleth(final int socialAction, final L2Skill skill)
		{
			_socialAction = socialAction;
			_skill = skill;
		}

		@Override
		public void run()
		{
			if(_socialAction == -1)
				beleth.getAI().startAITask();
			if(_socialAction > 0)
				beleth.broadcastPacket(new SocialAction(beleth.getObjectId(), _socialAction));
			if(_skill != null)
				beleth.broadcastPacket(new MagicSkillUse(beleth, beleth, _skill.getId(), 1, _skill.getHitTime(), 1));
		}
	}

	private final class ShowSpawnMovie implements Runnable
	{
		private final int _taskId;

		public ShowSpawnMovie(final int taskId)
		{
			_taskId = taskId;
		}

		@Override
		public void run()
		{
			try
			{
				switch (_taskId)
				{
					case 1000:
					{
						// Спауним его в какой-то тайной комнате)
						beleth = Functions.spawn(new Location(-105200, -253104, -15264, -16384), BELETH_NPCID);

						camera1 = cameras[0].spawnOne();
						// myself::SpecialCamera3(myself.sm,1500,0,90,2500,10000,30000,0,180,1,0,1);
						// myself::AddTimerEx(1001,2500);

						showSocialActionMovie(camera1, 1500, 0, 90, 2500, 10000, 0, 180, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 2500);
						break;
					}
					case 1001:
					{
						// myself::SpecialCamera3(myself.sm,900,180,0,15000,10000,30000,50,20,1,0,0);
						// myself::AddTimerEx(1002,15000);
						showSocialActionMovie(camera1, 900, 180, 0, 15000, 30000, 50, 20, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 15000);
						break;
					}
					case 1002:
					{
						// myself::SpecialCamera3(myself.sm,20,180,0,1500,10000,15000,80,15,1,0,0);
						// myself::AddTimerEx(1003,1500);
						showSocialActionMovie(camera1, 20, 180, 0, 1500, 15000, 80, 15, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 1500);
						break;
					}
					case 1003:
					{
						// gg::Castle_GateOpenClose2(interdoor,1);
						DoorTable.getInstance().getDoor(INTERDOOR).closeMe();
						// myself::SpecialCamera3(myself.sm,20,180,0,0,10000,4000,80,15,1,0,0);
						// myself::AddTimerEx(1004,1500);
						showSocialActionMovie(camera1, 20, 180, 0, 0, 4000, 80, 15, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 1500);
						break;
					}
					case 1004:
					{
						// myself::CreateOnePrivateEx(1029121,"ai_boss09_beres_camera02",0,0,16321,214835,-9352,0,0,0,0);
						// myself::AddTimerEx(1005,10000);
						camera2 = cameras[1].spawnOne();
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 10000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(2000), 1);
						break;
					}
					case 1005:
					{
						// myself::Despawn();
						cameras[0].despawnAll();
						break;
					}
					case 2000:
					{
						// myself::SpecialCamera3(myself.sm,600,190,0,0,10000,3000,0,15,1,0,1);
						// myself::AddTimerEx(1001,1);
						showSocialActionMovie(camera2, 600, 190, 0, 0, 3000, 0, 15, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 1);
						break;
					}
					case 2001:
					{
						// myself::SpecialCamera3(myself.sm,0,255,0,5000,10000,30000,0,15,1,0,0);
						// myself::AddTimerEx(1002,5000);
						showSocialActionMovie(camera2, 0, 255, 0, 5000, 30000, 0, 15, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 5000);
						break;
					}
					case 2002:
					{
						// myself::SpecialCamera3(myself.sm,0,255,0,0,10000,30000,0,15,1,0,0);
						showSocialActionMovie(camera2, 0, 255, 0, 0, 30000, 0, 15, 1, 0);

						// нужна эта камера для отображение красного луча)
						// myself::CreateOnePrivateEx(1029125,"ai_boss09_beres_camera06",0,0,16321,215338,-9352,0,0,0,0);
						cameras[4].spawnOne(); // спауним 6 камеру
						broadcastSpawnCamera6();
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(6000), 30000);

						// myself::AddTimerEx(1003,2000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 2000);
						break;
					}
					case 2003:
					{
						onScriptEvent(1109229, 100);

						// myself::SpecialCamera3(myself.sm,0,255,0,7000,10000,20000,0,45,1,0,0);
						// myself::AddTimerEx(1004,9000);
						showSocialActionMovie(camera2, 0, 255, 0, 7000, 20000, 0, 45, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 9000);
						break;
					}
					case 2004:
					{
						onScriptEvent(1109230, 100);
						broadcastSpawnBeleth();

						// myself::SpecialCamera3(myself.sm,0,255,0,0,10000,20000,0,45,1,0,0);
						// myself::AddTimerEx(1005,7500);
						showSocialActionMovie(camera2, 0, 255, 0, 0, 20000, 0, 45, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 7500);
						break;
					}
					case 2005:
					{
						// myself::SpecialCamera3(myself.sm,350,270,45,0,10000,4000,0,45,1,0,0);
						// myself::AddTimerEx(1006,100);
						showSocialActionMovie(camera2, 350, 270, 45, 0, 4000, 0, 45, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 100);
						break;
					}
					case 2006:
					{
						// myself::SpecialCamera3(myself.sm,0,270,0,2000,10000,8000,0,12,1,0,0);
						// myself::AddTimerEx(1007,2000);
						showSocialActionMovie(camera2, 0, 270, 0, 2000, 8000, 0, 12, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 2000);
						break;
					}
					case 2007:
					{
						// ::SpecialCamera3(myself.sm,-50,270,0,1500,10000,7000,0,18,1,0,0);
						showSocialActionMovie(camera2, -50, 270, 0, 1500, 7000, 0, 18, 1, 0);

						onScriptEvent(1109231, 10);

						// myself::AddTimerEx(1008,5500);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 5500);
						break;
					}
					case 2008:
					{
						// myself::SpecialCamera3(myself.sm,500,270,5,3000,10000,40000,0,12,1,0,0);
						// myself::AddTimerEx(1009,5000);
						showSocialActionMovie(camera2, 500, 270, 5, 3000, 40000, 0, 12, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 5000);
						break;
					}
					case 2009:
					{
						// myself::CreateOnePrivateEx(1029122,"ai_boss09_beres_camera03",0,0,16323,213139,-9352,0,0,0,0);
						camera3 = cameras[2].spawnOne();

						// myself::AddTimerEx(1010,10000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 10000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(3000), 10);
						break;
					}
					case 2010:
					{
						// myself::Despawn();
						cameras[1].despawnAll();
						break;
					}
					case 3000:
					{
						// myself::SpecialCamera3(myself.sm,50,270,15,3000,10000,7000,0,5,1,0,1);
						showSocialActionMovie(camera3, 50, 270, 15, 3000, 7000, 0, 5, 1, 0);

						onScriptEvent(1109232, 10);

						// myself::AddTimerEx(1001,4000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 4000);
						break;
					}
					case 3001:
					{
						onScriptEvent(1109233, 10);

						// myself::SpecialCamera3(myself.sm,400,270,20,2000,10000,6000,0,-5,1,0,0);
						// myself::AddTimerEx(1002,5000);
						showSocialActionMovie(camera3, 400, 270, 20, 2000, 6000, 0, -5, 1, 0);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 5000);
						break;
					}
					case 3002:
					{
						// myself::SpecialCamera3(myself.sm,20,263,0,0,10000,4000,-10,15,1,0,0);
						showSocialActionMovie(camera3, 20, 263, 0, 0, 4000, -10, 15, 1, 0);

						// myself::CreateOnePrivateEx(1029126,"ai_boss09_beres_minion_npc",0,0,16404,213053,-9352,-16384,0,0,0);
						belethPresentation1 = Functions.spawn(new Location(16404, 213053, -9352, -16384), 29126);

						// myself::AddTimerEx(1003,3000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 3000);
						break;
					}
					case 3003:
					{
						// myself::SpecialCamera3(myself.sm,20,274,0,0,10000,6000,9,15,1,0,0);
						showSocialActionMovie(camera3, 20, 274, 0, 0, 6000, 9, 15, 1, 0);
						// myself::CreateOnePrivateEx(1029127,"ai_boss09_beres_minion_npc",0,0,16247,213035,-9352,-16384,0,0,0);
						belethPresentation2 = Functions.spawn(new Location(16247, 213035, -9352, -16384), 29127);

						// myself::AddTimerEx(1004,4000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 4000);
						break;
					}
					case 3004:
					{
						// myself::SpecialCamera3(myself.sm,0,260,0,0,10000,5000,20,35,1,0,0);
						showSocialActionMovie(camera3, 0, 260, 0, 0, 5000, 20, 35, 1, 0);

						onScriptEvent(1109234, 10);

						// myself::AddTimerEx(1005,4000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 4000);
						break;
					}
					case 3005:
					{
						// myself::SpecialCamera3(myself.sm,0,260,0,0,10000,10000,20,35,1,0,0);
						showSocialActionMovie(camera3, 0, 260, 0, 0, 10000, 20, 35, 1, 0);

						onScriptEvent(1109235, 10);

						// myself::AddTimerEx(1006,10000);
						L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(_taskId + 1), 10000);
						break;
					}
					case 3006:
					{
						// myself::Despawn();
						cameras[2].despawnAll();
						belethPresentation1.deleteMe();
						belethPresentation2.deleteMe();
						break;
					}
					case 6000:
					{
						broadcastDeleteCamera6();
						cameras[4].despawnAll();
						break;
					}
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
		}
	}

	private final class ShowDieMovie implements Runnable
	{
		private final int _taskId;

		public ShowDieMovie(final int taskId)
		{
			_taskId = taskId;
		}

		@Override
		public void run()
		{
			switch (_taskId)
			{
				case 1000:
				{
					// myself::SpecialCamera3(myself.sm,0,275,30,0,10000,3000,0,5,1,0,1);
					showSocialActionMovie(camera4, 0, 275, 30, 0, 3000, 0, 5, 1, 0);

					onScriptEvent(1109236, 10);

					// myself::AddTimerEx(1001,1000);
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 1000);
					break;
				}
				case 1001:
				{
					// myself::SpecialCamera3(myself.sm,0,100,30,2500,10000,4000,0,5,1,0,0);
					showSocialActionMovie(camera4, 0, 100, 30, 2500, 4000, 0, 5, 1, 0);
					// myself::AddTimerEx(1002,2500);
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 2500);
					break;
				}
				case 1002:
				{
					// myself::SpecialCamera3(myself.sm,0,281,30,2500,10000,4000,0,5,1,0,0);
					showSocialActionMovie(camera3, 0, 281, 30, 2500, 4000, 0, 5, 1, 0);
					// myself::AddTimerEx(1003,2500);
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 2500);
					break;
				}
				case 1003:
				{
					// myself::SpecialCamera3(myself.sm,150,281,0,3000,10000,12000,0,35,1,0,0);
					showSocialActionMovie(camera4, 150, 281, 0, 3000, 12000, 0, 35, 1, 0);
					// myself::AddTimerEx(1004,9000);
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 9000);
					break;
				}
				case 1004:
				{
					// myself::SpecialCamera3(myself.sm,-45,281,30,0,10000,4000,0,10,1,0,0);
					showSocialActionMovie(camera4, -45, 281, 30, 0, 4000, 0, 10, 1, 0);
					onScriptEvent(1109237, 10);

					// myself::AddTimerEx(1005,3500);
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 3500);
					break;
				}
				case 1005:
				{
					// myself::SpecialCamera3(myself.sm,700,0,0,0,10000,3000,180,15,1,0,0);
					showSocialActionMovie(camera4, 700, 0, 0, 0, 3000, 180, 15, 1, 0);

					// gg::Castle_GateOpenClose2(secret_door,0);
					DoorTable.getInstance().getDoor(SECRETDOOR).openMe();
					DoorTable.getInstance().getDoor(SECRETDOOR2).openMe();
					L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(_taskId + 1), 2000);
					break;
				}
				case 1006:
				{
					beleth_elf.deleteMe();
					cameras[3].despawnAll();
					break;
				}
			}
		}
	}

	private class CheckLastAttack implements Runnable
	{
		@Override
		public void run()
		{
			if(_state.getState().equals(State.ALIVE))
				if(_lastAttackTime + Config.BELETH_LIMITUNTILSLEEP < System.currentTimeMillis())
					sleep();
				else
					_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 60 * 1000);
		}
	}

	// at end of interval.
	private final class IntervalEnd implements Runnable
	{
		@Override
		public void run()
		{
			_state.setState(State.NOTSPAWN);
			_state.update();

			renewBeleth();
		}
	}

	private static final Location[][] MINION_POSITIONS = new Location[][]
	{
			// position 1
			{
					new Location(16064, 212528, -9312, 11980),
					new Location(16592, 212528, -9312, 21128),
					new Location(16936, 212896, -9312, 29020),
					new Location(16940, 213392, -9312, -29192),
					new Location(16580, 213760, -9312, -19360),
					new Location(16064, 213760, -9312, -12172),
					new Location(15700, 213388, -9312, -3876),
					new Location(15704, 212880, -9312, 3944),
					new Location(16320, 212512, -9344, 16468),
					new Location(16768, 212704, -9344, 25504),
					new Location(16944, 213136, -9344, 32240),
					new Location(16768, 213584, -9344, -24380),
					new Location(16320, 213776, -9344, -15924),
					new Location(15888, 213576, -9348, -8436),
					new Location(15696, 213136, -9344, 0),
					new Location(15888, 212704, -9344, 8028) },
			// position 2
			{
					new Location(17628, 214428, -9360, 0),
					new Location(16319, 214437, -9360, 16768),
					new Location(15027, 214447, -9360, 32408),
					new Location(15012, 213132, -9360, -32580),
					new Location(15012, 211820, -9360, -31544),
					new Location(16320, 211836, -9360, -16144),
					new Location(17628, 211836, -9360, 0),
					new Location(17632, 213138, -9360, 0),
					new Location(17020, 214429, -9344, 16296),
					new Location(15619, 214438, -9344, 16528),
					new Location(15016, 213759, -9344, -32288),
					new Location(15019, 212480, -9344, -32352),
					new Location(15605, 211833, -9344, -16428),
					new Location(17022, 211832, -9344, -16084),
					new Location(17622, 212480, -9344, 24),
					new Location(17623, 213763, -9344, 112) },
			// position 3
			{
					new Location(16317, 212140, -9360, -16112),
					new Location(17196, 212632, -9360, -3536),
					new Location(17189, 213639, -9360, 2992),
					new Location(16321, 214140, -9360, 16236),
					new Location(15449, 213641, -9360, 27728),
					new Location(15456, 212644, -9360, -27816),
					new Location(15955, 212509, -9360, -21824),
					new Location(16681, 212508, -9360, -10872),
					new Location(17006, 213137, -9360, 0),
					new Location(16678, 213759, -9360, 10148),
					new Location(15963, 213756, -9360, 21964),
					new Location(15640, 213125, -9360, -32452) },

			// position 4
			{
					new Location(16059, 212205, -9312, -16132),
					new Location(16583, 212200, -9312, -16284),
					new Location(17253, 212883, -9312, 0),
					new Location(17254, 213404, -9312, 0),
					new Location(16577, 214069, -9312, 15732),
					new Location(16059, 214066, -9312, 16568),
					new Location(15397, 213408, -9312, 31916),
					new Location(15393, 212886, -9312, -30624),
					new Location(16052, 211915, -9312, -16392),
					new Location(16586, 211907, -9312, -15756),
					new Location(17559, 212890, -9312, 0),
					new Location(17549, 213408, -9312, 0),
					new Location(16579, 214372, -9312, 16640),
					new Location(16057, 214367, -9312, 16284),
					new Location(15091, 213403, -9312, -31532),
					new Location(15095, 212892, -9312, -32384) },
	};

	private static final int display_magic1 = 5531; // 3
	private static final int display_magic2 = 5532; // 2
	private static final int display_magic3 = 5533; // 2

	private static final L2Zone beleth_zone = ZoneManager.getInstance().getZoneById(ZoneType.epic, 702122, false);

	private static final int INTERDOOR = 20240001;
	private static final int SECRETDOOR = 20240002;
	private static final int SECRETDOOR2 = 20240003;

	public static final Location ENTRANCE_LOCATION = new Location(16342, 209557, -9352, 3000);
	// локация  куда  бросаем игроков всего КК после  открытия  крышки гроба у Белефа и получения кольца
	public static final Location ENTRANCE_LOCATIONOFF_B = new Location(16632, 244424, 11595);

	private static final int[] CAMERA_IDS = new int[] { 29120, 29121, 29122, 29123, 29125 };
	private static final Location[] CAMERAS_LOCATION = new Location[]
	{
			new Location(16326, 212940, -9352), // camera01
			new Location(16321, 214835, -9352), // camera02
			new Location(16323, 213139, -9352), // camera03
			new Location(16325, 213136, -9352), // camera04
			new Location(16321, 215338, -9352) // camera06
	};

	public static final int BELETH_NPCID = 29118;

	private long _lastAttackTime = 0;
	private final EpicBossState _state;
	private ScheduledFuture<IntervalEnd> _intervalEndTask = null;
	private ScheduledFuture<CheckLastAttack> _sleepCheckTask = null;

	private final L2Spawn[] cameras;

	private L2NpcInstance beleth;
	private L2NpcInstance beleth_elf;
	private L2NpcInstance camera1, camera2, camera3, camera4;
	/** для красоты */
	private L2NpcInstance belethPresentation1, belethPresentation2;

	private final SendEventMinions call_minions = new SendEventMinions();

	/**
	 * список заспауненых миньёнов по квадратам
	 */
	private final GArray<L2NpcInstance> minions;

	private boolean _activated = false;

	public static final BelethManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public BelethManager()
	{
		_state = new EpicBossState(BELETH_NPCID);
		if(!_state.isNotSpawn())
			startIntervalEndTask();

		_log.info("EpicBossManager: Beleth state - " + _state.getState() + ".");
		// Выводим только если есть дата следующего спауна
		if(_state.isInterval())
			_log.info("EpicBossManager: Beleth next spawn - " + Util.datetimeFormatter.format(_state.getRespawnDate()) + ".");

		// если Beleth не заспаунен, то открываем дверь для входа к нему
		if(_state.isNotSpawn())
		{
			DoorTable.getInstance().getDoor(INTERDOOR).openMe();
			DoorTable.getInstance().getDoor(SECRETDOOR).closeMe();
			DoorTable.getInstance().getDoor(SECRETDOOR2).closeMe();
		}

		cameras = new L2Spawn[CAMERA_IDS.length];
		minions = new GArray<L2NpcInstance>(60);

		L2NpcTemplate template;
		L2Spawn spawn;
		for(byte index = 0; index < CAMERA_IDS.length; index++)
			try
			{
				template = NpcTable.getTemplate(CAMERA_IDS[index]);
				if(template == null)
				{
					_log.warning("BelethManager: mob with ID = " + CAMERA_IDS[index] + " can't be spawned!");
					return;
				}
				spawn = new L2Spawn(template);
				spawn.setAmount(1);
				spawn.setLoc(CAMERAS_LOCATION[index]);
				spawn.setRespawnDelay(0);
				spawn.stopRespawn();

				cameras[index] = spawn;
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "BelethManager: error spawn ", e);
			}
	}

	/**
	 * Запускаем таск для показа видео и начала фарма Белета
	 */
	public synchronized void showMustGoOn()
	{
		if(_activated || !isEnableEnter())
			return;

		_activated = true;

		_state.setRespawnDate(getRespawnInterval());
		_state.setState(State.ALIVE);
		_state.update();

		// запускаем
		L2GameThreadPools.getInstance().scheduleGeneral(new ShowSpawnMovie(1000), Config.BELETH_APPTIME);
	}

	/**
	 * Обработка смерти Beleth'а
	 */
	public synchronized void onBelethDie()
	{
		if(!_activated)
			return;

		_activated = false;

		_state.setRespawnDate(getRespawnInterval());
		_state.setState(State.INTERVAL);
		_state.update();

		// удаляем всех миньёнов
		minions.forEach(L2ObjectProcedures.PROC_DELETE_NPC);

		camera4 = cameras[3].spawnOne();

		beleth.teleToLocation(new Location(16325, 213136, -9352));
		L2GameThreadPools.getInstance().scheduleGeneral(new ShowDieMovie(1000), 100);
		startIntervalEndTask();
	}

	private void onScriptEvent(final int script_event_arg1, final int script_event_arg2)
	{
		switch (script_event_arg1)
		{
			case 1109229:
			{
				// myself::AddTimerEx(1010,script_event_arg2);
				beleth.setHeading(48384);
				beleth.teleToLocation(new Location(13500, 213140, -9352, 48384));
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(0, null), script_event_arg2);
				break;
			}
			case 1109230:
			{
				// myself::AddTimerEx(1020,script_event_arg2);
				beleth.setHeading(-16384);
				beleth.teleToLocation(new Location(16321, 214818, -9352, -16384));
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(2, null), script_event_arg2);
				break;
			}
			case 1109231:
			{
				// myself::AddTimerEx(1030,script_event_arg2);
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(0, SkillTable.getInstance().getInfo(display_magic1, 1)), script_event_arg2);
				break;
			}
			case 1109232:
			{
				// myself::AddTimerEx(1040,script_event_arg2);
				beleth.setHeading(48384);
				beleth.teleToLocation(new Location(16326, 212994, -9352, 48384));
				break;
			}
			case 1109233:
			{
				// myself::AddTimerEx(1050,script_event_arg2);
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(0, SkillTable.getInstance().getInfo(display_magic2, 1)), script_event_arg2);
				break;
			}
			case 1109234:
			{
				// myself::AddTimerEx(1060,script_event_arg2);
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(3, null), script_event_arg2);
				break;
			}
			case 1109235:
			{
				// myself::AddTimerEx(1070,script_event_arg2);
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(0, SkillTable.getInstance().getInfo(display_magic3, 1)), script_event_arg2);

				// через 5 секунд делать Белета невидимым и спануить миньёнов
				L2GameThreadPools.getInstance().scheduleGeneral(new SpawnMinions(), 5000);
				break;
			}
			case 1109236:
			{
				// myself::AddTimerEx(9900,script_event_arg2);
				// myself::AddEffectActionDesire2(myself.sm,5,10000000,6000);
				L2GameThreadPools.getInstance().scheduleGeneral(new DoActionBeleth(5, null), 3000);
				break;
			}
			// смерть Белефа
			case 1109237:
			{
				// myself::AddTimerEx(9910,script_event_arg2);
				// удаляем для нормльной анимации
				if(beleth != null)
					beleth.deleteMe();
				// myself::CreateOnePrivateEx(1029128,"ai_boss09_beres_elf_npc",0,0,16325,213136,( -9352 + 100 ),-16384,0,0,0);
				beleth_elf = Functions.spawn(new Location(16325, 213136, -9352, -16384), 29128);
				// myself::SetVisible(0);
				break;
			}
		}
	}

	public final void sendEventIdMinions()
	{
		minions.forEach(call_minions);
	}

	/**
	 * Обработка засыпания Epidos, если его долго не атаковали
	 */
	private void sleep()
	{
		renewBeleth();

		if(_state.isAlive())
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
		}
	}

	/**
	 * Сбрасывает всю информацию
	 */
	private synchronized final void renewBeleth()
	{
		_activated = false;
		_lastAttackTime = 0;

		if(beleth != null)
			beleth.deleteMe();

		DoorTable.getInstance().getDoor(INTERDOOR).closeMe();
		DoorTable.getInstance().getDoor(SECRETDOOR).openMe();
		DoorTable.getInstance().getDoor(SECRETDOOR2).openMe();

		// удаляем всех миньёнов
		minions.forEach(L2ObjectProcedures.PROC_DELETE_NPC);

		if(_intervalEndTask != null)
		{
			_intervalEndTask.cancel(false);
			_intervalEndTask = null;
		}

		if(_sleepCheckTask != null)
		{
			_sleepCheckTask.cancel(false);
			_sleepCheckTask = null;
		}
		// Выгоняем всех из зоны
		final GArray<L2Player> players = beleth_zone.getInsidePlayers();
		for(final L2Player player : players)
			player.teleToClosestTown(); // Teleport to the closest town
	}

	private void startIntervalEndTask()
	{
		if(_state.isAlive())
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
			return;
		}

		if(!_state.isInterval())
		{
			_state.setRespawnDate(getRespawnInterval());
			_state.setState(State.INTERVAL);
			_state.update();
		}

		_intervalEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new IntervalEnd(), _state.getInterval());
	}

	public void setLastAttackTime()
	{
		_lastAttackTime = System.currentTimeMillis();
	}

	public boolean isEnableEnter()
	{
		return _state.isNotSpawn();
	}

	public final boolean isActivated()
	{
		return _activated;
	}

	private static int getRespawnInterval()
	{
		return Config.BELETH_FIXINTERVAL + Rnd.get(Config.BELETH_RANDOMINTERVAL);
	}

	/**
	 * Shows a movie to the players in the lair.
	 * 
	 * @param target
	 *            - L2NpcInstance target is the center of this movie
	 * @param dist
	 *            - расстояние до цели
	 * @param yaw
	 *            - угол (north = 90, south = 270, east = 0 , west = 180)
	 * @param pitch
	 *            - наклон: если > 0 вид сверху / если < 0 вид снизу
	 * @param time
	 *            - fast ++ or slow -- depends on the value
	 * @param duration
	 *            - How long to watch the movie
	 */
	private static void showSocialActionMovie(final L2Character target, final int dist, final int yaw, final int pitch, final int time, final int duration, final int turn, final int rise, final int widescreen, final int unk)
	{
		if(target == null)
			return;

		final SpecialCamera movie = new SpecialCamera(target.getObjectId(), dist, yaw, pitch, time, duration, turn, rise, widescreen, unk);
		beleth_zone.broadcastPacket(movie);
	}

	/**
	 * Так как игроки там в разных регионах, если стоять далеко, то не будет показываться
	 */
	private void broadcastSpawnBeleth()
	{
		beleth.setShowSpawnAnimation(1);
		final GArray<L2Player> players = beleth_zone.getInsidePlayers();
		for(final L2Player player : players)
			player.sendPacket(new NpcInfo(beleth, player));
		beleth.setShowSpawnAnimation(0);
	}

	/**
	 * Так как игроки там в разных регионах, если стоять далеко, то не будет показываться
	 */
	private void broadcastSpawnCamera6()
	{
		final GArray<L2Player> players = beleth_zone.getInsidePlayers();
		for(final L2Player player : players)
			player.sendPacket(new NpcInfo(cameras[4].getLastSpawn(), player));
	}

	/**
	 * Так как игроки там в разных регионах, если стоять далеко, то не будет показываться
	 */
	private void broadcastDeleteCamera6()
	{
		final DeleteObject d = new DeleteObject(cameras[4].getLastSpawn());
		final GArray<L2Player> players = beleth_zone.getInsidePlayers();
		players.forEach(new SendPacketProc(d));
	}

	public final class SendEventMinions implements INgVoidProcedure<L2NpcInstance>
	{
		@Override
		public void execute(final L2NpcInstance npc)
		{
			if(npc != null && npc.isInRange(beleth, 1000))
				npc.getAI().onScriptEvent(1109239);
		}
	}

	public final EpicBossState getState()
	{
		return _state;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final BelethManager _instance = new BelethManager();
	}
}
