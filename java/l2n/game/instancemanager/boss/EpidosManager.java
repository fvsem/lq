package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.EpicBossState.State;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.tables.DoorTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 01.08.2011
 * @time 9:23:04
 */
public class EpidosManager
{
	private final static Logger _log = Logger.getLogger(EpidosManager.class.getName());

	private final class ShowMessage implements Runnable
	{
		private final int npcId;
		private final String msg;

		private ShowMessage(final int id, final String message)
		{
			npcId = id;
			msg = message;
		}

		@Override
		public void run()
		{
			final GArray<L2NpcInstance> live = sporesSpawn[getReverseSpawnIndex(npcId)].getAllLiveSpawned();
			// если есть живые, то показываем сообщение
			if(live != null && !live.isEmpty())
				Functions.npcSayInRange(live.get(0), Say2C.LOCAL, msg, 3000);
		}
	}

	private final class SpawnEpidos implements Runnable
	{
		@Override
		public void run()
		{
			// спауним)
			epidos = Functions.spawn(SPAWN_LOCATION, epidosId);

			_state.setRespawnDate(getRespawnInterval());
			_state.setState(State.ALIVE);
			_state.update();

			// запускаем таск для проверки последней атаки
			_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 10 * 60 * 1000);
		}
	}

	private class CheckLastAttack implements Runnable
	{
		@Override
		public void run()
		{
			if(_state.getState().equals(State.ALIVE))
				if(_lastAttackTime + Config.FWE_LIMITUNTILSLEEP < System.currentTimeMillis())
					sleep();
				else
					_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 60 * 1000);
		}
	}

	// at end of interval.
	private final class IntervalEnd implements Runnable
	{
		@Override
		public void run()
		{
			_state.setState(State.NOTSPAWN);
			_state.update();

			renewCoreNaia();
		}
	}

	private static final Location SPAWN_LOCATION = new Location(-45474, 247450, -13994, 49152);

	private static final int death_count_positive = Config.FWE_INDEX_NEED_COUNT;
	private static final int death_count_negative = -Config.FWE_INDEX_NEED_COUNT;

	private static final int SPORE_BASIC = 25613; // 4 index
	public static final int SPORE_FIRE = 25605; // 0 index
	public static final int SPORE_WATER = 25606; // 1 index
	public static final int SPORE_WIND = 25607; // 2 index
	public static final int SPORE_EARTH = 25608; // 3 index

	private static final int[] SPORES_IDS = new int[] { SPORE_FIRE, SPORE_WATER, SPORE_WIND, SPORE_EARTH, SPORE_BASIC };

	private static final int NAIA_CUBIC = 32376;

	private static final Location[] MUTATED_ELPY_LOCATIONS = new Location[]
	{
			new Location(-46080, 246368, -14183, 49152),
			new Location(-44816, 246368, -14183, 49152),
			new Location(-44224, 247440, -14184, 49152),
			new Location(-44896, 248464, -14183, 49152),
			new Location(-46064, 248544, -14183, 49152),
			new Location(-46720, 247424, -14183, 49152)
	};

	private static final int NAIA_DOOR = 18250025;

	private static final L2Zone core_naia_zone = ZoneManager.getInstance().getZoneById(ZoneType.poison, 4637, false);

	private static final String[][] MESSAGES = new String[][]
	{
			{ "...Это Огонь ...", "...Огонь Сильный...", "...Всегда Огонь...", "... Огоня будет делать ..." },
			{ "...Это Вода...", "...Вода Сильная...", "...Всегда Вода...", "... Воды будет делать ..." },
			{ "...Это Ветер...", "...Ветер Сильный...", "...Всегда Ветер...", "... Ветра будет делать ..." },
			{ "...Это Земля...", "...Земля Сильная...", "...Всегда земля...", "... Земли будет делать ..." }
	};

	private boolean init = false;

	private int fire_water = 0; // счётчик
	private int wind_earth = 0; // счётчик

	private int epidosId = 0;
	private boolean spawnEpidos = false;

	private long _lastAttackTime = 0;
	private final EpicBossState _state;
	private ScheduledFuture<IntervalEnd> _intervalEndTask = null;
	private ScheduledFuture<CheckLastAttack> _sleepCheckTask = null;

	private final L2Spawn[] sporesSpawn;
	private L2Spawn mutated_elpy;

	private L2NpcInstance epidos;
	private L2NpcInstance teleportCube;

	public static final EpidosManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public EpidosManager()
	{
		sporesSpawn = new L2Spawn[SPORES_IDS.length];

		_state = new EpicBossState(25609);
		if(!_state.isNotSpawn())
			startIntervalEndTask();

		_log.info("EpicBossManager: Epidos state - " + _state.getState() + ".");
		// Выводим только если есть дата следующего спауна
		if(_state.isInterval())
			_log.info("EpicBossManager: Epidos next spawn - " + Util.datetimeFormatter.format(_state.getRespawnDate()) + ".");

		L2NpcTemplate template;
		L2Spawn spawn;
		for(byte index = 0; index < SPORES_IDS.length; index++)
			try
			{
				template = NpcTable.getTemplate(SPORES_IDS[index]);
				if(template == null)
				{
					_log.warning("EpidosManager: mob with ID = " + SPORES_IDS[index] + " can't be spawned!");
					return;
				}
				spawn = new L2Spawn(template);
				spawn.setAmount(1);
				spawn.setLoc(SPAWN_LOCATION);
				spawn.setRespawnDelay(0);
				spawn.stopRespawn();

				sporesSpawn[index] = spawn;
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "EpidosManager: error spawn ", e);
			}

		template = NpcTable.getTemplate(25604);
		try
		{
			mutated_elpy = new L2Spawn(template);
			mutated_elpy.setAmount(1);
			mutated_elpy.setLoc(MUTATED_ELPY_LOCATIONS[Rnd.get(MUTATED_ELPY_LOCATIONS.length)]);
			mutated_elpy.setRespawnDelay(120, 60);
			mutated_elpy.setRespawnTime(0);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "EpidosManager: fail spawn Mutated Elpy: ", e);
		}
	}

	/**
	 * Считаем количество убитых спор
	 * 
	 * @param spore
	 * @param killer
	 */
	public synchronized void calculateCounter(final L2NpcInstance spore, final L2Player killer)
	{
		// если айди для спауна Epidos'a определён, то дальше не идём
		if(epidosId > 0 || spawnEpidos)
			return;

		switch (spore.getNpcId())
		{
			case SPORE_FIRE:
			{
				fire_water += Config.FWE_INDEX_KILL_SPORE;
				if(wind_earth > 0)
					wind_earth -= Config.FWE_INDEX_KILL_OTHER_SPORE;
				else if(wind_earth < 0)
					wind_earth += Config.FWE_INDEX_KILL_OTHER_SPORE;

				if(fire_water == 20 || fire_water == 40 || fire_water == 60 || fire_water == 80 || fire_water == 100)
					sendMessage(spore.getNpcId(), MESSAGES[0][Rnd.get(MESSAGES.length)]);

				// проверяем условия для спауна Epidos'а
				if(fire_water >= death_count_positive)
					epidosId = 25610; // water

				break;
			}
			case SPORE_WATER:
			{
				fire_water -= Config.FWE_INDEX_KILL_SPORE;
				if(wind_earth > 0)
					wind_earth -= Config.FWE_INDEX_KILL_OTHER_SPORE;
				else if(wind_earth < 0)
					wind_earth += Config.FWE_INDEX_KILL_OTHER_SPORE;

				if(fire_water == -20 || fire_water == -40 || fire_water == -60 || fire_water == -80 || fire_water == -100)
					sendMessage(spore.getNpcId(), MESSAGES[1][Rnd.get(MESSAGES.length)]);

				// проверяем условия для спауна Epidos'а
				if(fire_water <= death_count_negative)
					epidosId = 25609; // fire

				break;
			}
			case SPORE_WIND:
			{
				wind_earth += Config.FWE_INDEX_KILL_SPORE;
				if(fire_water > 0)
					fire_water -= Config.FWE_INDEX_KILL_OTHER_SPORE;
				else if(fire_water < 0)
					fire_water += Config.FWE_INDEX_KILL_OTHER_SPORE;

				if(wind_earth == 20 || wind_earth == 40 || wind_earth == 60 || wind_earth == 80 || wind_earth == 100)
					sendMessage(spore.getNpcId(), MESSAGES[2][Rnd.get(MESSAGES.length)]);

				// проверяем условия для спауна Epidos'а
				if(wind_earth >= death_count_positive)
					epidosId = 25612; // earth

				break;
			}
			case SPORE_EARTH:
			{
				wind_earth -= Config.FWE_INDEX_KILL_SPORE;
				if(fire_water > 0)
					fire_water -= Config.FWE_INDEX_KILL_OTHER_SPORE;
				else if(fire_water < 0)
					fire_water += Config.FWE_INDEX_KILL_OTHER_SPORE;

				if(wind_earth == -20 || wind_earth == -40 || wind_earth == -60 || wind_earth == -80 || wind_earth == -100)
					sendMessage(spore.getNpcId(), MESSAGES[3][Rnd.get(MESSAGES.length)]);

				// проверяем условия для спауна Epidos'а
				if(wind_earth <= death_count_negative)
					epidosId = 25611; // wind

				break;
			}
		}

		if(killer != null && killer.isGM())
			killer.sendMessage("Счетчик огненная-воды: " + fire_water + "/ ветер-земля счетчик: " + wind_earth);

		// спауним эпидоса)
		if(epidosId > 0)
			spawnEpidos();
	}

	private void sendMessage(final int npcId, final String msg)
	{
		if(Rnd.chance(50))
			L2GameThreadPools.getInstance().scheduleAi(new ShowMessage(npcId, msg), 300, false);
	}

	/**
	 * Срабатывает при прохождении группы последней комнаты
	 */
	public void init()
	{
		// если можно входить и первой инициализации не было, то...
		if(_state.isNotSpawn() && !init)
		{
			// открываем главную дверь
			DoorTable.getInstance().getDoor(NAIA_DOOR).openMe();
			init = true;

			// спауним Mutated Elpy
			mutated_elpy.setRespawnTime(0);
			mutated_elpy.startRespawn();
			mutated_elpy.init();

			// обрабатываем зоны
			core_naia_zone.setActive(false);
		}
		// если Эпидос уже был убит и первой инициализации не было, то...
		else if(_state.isInterval() && !init && teleportCube == null)
		{
			// открываем главную дверь
			DoorTable.getInstance().getDoor(NAIA_DOOR).openMe();
			init = true;

			// TODO спаунить кубик
			teleportCube = Functions.spawn(new Location(-45482, 246277, -14184), NAIA_CUBIC);
		}
	}

	/**
	 * Финишируем убийство Epidos'а
	 */
	public synchronized void onEpidosDie()
	{
		// открываем главную дверь
		DoorTable.getInstance().getDoor(NAIA_DOOR).openMe();

		_state.setRespawnDate(getRespawnInterval());
		_state.setState(State.INTERVAL);
		_state.update();

		renewCoreNaia();

		// спаунить кубик
		teleportCube = Functions.spawn(new Location(-45482, 246277, -14184), NAIA_CUBIC);

		startIntervalEndTask();
	}

	/**
	 * Обработка засыпания Epidos, если его долго не атаковали
	 */
	private void sleep()
	{
		renewCoreNaia();

		if(_state.isAlive())
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
		}
	}

	private void renewCoreNaia()
	{
		init = false;
		spawnEpidos = false;

		epidosId = 0;
		// сбрасываем счётчики
		fire_water = 0;
		wind_earth = 0;

		// удалить все споры
		for(final L2Spawn spawn : sporesSpawn)
			spawn.despawnAll();

		// выключить зоны
		core_naia_zone.setActive(false);

		// удаляем заспауненные споры и самого Epidos'а
		if(epidos != null)
		{
			epidos.getAI().onScriptEvent(78010035);
			epidos.deleteMe();
			epidos = null;
		}

		// удаляем Кубик телепортации
		if(teleportCube != null)
		{
			teleportCube.deleteMe();
			teleportCube = null;
		}

		if(_intervalEndTask != null)
		{
			_intervalEndTask.cancel(false);
			_intervalEndTask = null;
		}

		if(_sleepCheckTask != null)
		{
			_sleepCheckTask.cancel(false);
			_sleepCheckTask = null;
		}
	}

	/**
	 * @param npc
	 */
	public void tryStartEpidos(final L2NpcInstance elpy)
	{
		final GArray<L2Player> players = core_naia_zone.getInsidePlayers();
		if(players.size() < Config.FWE_MIN_PLAYER_COUNT)
			return;

		DoorTable.getInstance().getDoor(NAIA_DOOR).closeMe();
		core_naia_zone.setActive(true);

		if(mutated_elpy != null)
		{
			mutated_elpy.despawnAll();
			mutated_elpy.stopRespawn();
		}

		// спауним споры (только базовые)
		L2NpcInstance npc;
		for(int i = 0; i < 10; i++)
		{
			npc = sporesSpawn[4].spawnOne();
			npc.getAI().startAITask();
		}
	}

	private void spawnEpidos()
	{
		spawnEpidos = true;

		// сбрасываем счётчики при спауне эпидоса
		fire_water = 0;
		wind_earth = 0;

		// те которые заспаунились надо отправить в центр
		GArray<L2NpcInstance> alive_spores;
		for(final L2Spawn spawn : sporesSpawn)
		{
			alive_spores = spawn.getAllLiveSpawned();
			for(final L2NpcInstance spore : alive_spores)
				// которые заспаунились надо отправить в центр
				spore.getAI().onScriptEvent(78010012);
		}

		// удалить все споры
		for(final L2Spawn spawn : sporesSpawn)
			spawn.despawnAll();

		L2GameThreadPools.getInstance().scheduleAi(new SpawnEpidos(), 10 * 1000, false);
	}

	/**
	 * Обработка спауна новых спор, после смерти споры)
	 * 
	 * @param npcId
	 */
	public void spawnSpore(final int npcId)
	{
		// если Epidos уже заспаунен, то дальше ничего не делаем
		if(spawnEpidos)
			return;

		L2GameThreadPools.getInstance().executeAi(new Runnable()
		{
			@Override
			public void run()
			{
				L2NpcInstance npc;
				switch (npcId)
				{
					case SPORE_BASIC: // при убийстве обычно споры, спауним 2 другие, тип выбирается рандомно
					{
						for(byte i = 0; i < 2; i++)
						{
							npc = sporesSpawn[Rnd.get(4)].spawnOne();
							npc.getAI().startAITask();
						}
						break;
					}
					case SPORE_FIRE:
					case SPORE_WATER:
					case SPORE_WIND:
					case SPORE_EARTH:
					{
						// 20% шанс что заспауниться противоположная спора
						if(Rnd.chance(20))
						{
							npc = sporesSpawn[getReverseSpawnIndex(npcId)].spawnOne();
							npc.getAI().startAITask();
						}
						// иначе рандомный тип
						else
						{
							npc = sporesSpawn[Rnd.get(4)].spawnOne();
							npc.getAI().startAITask();
						}
					}
				}
			}
		}, false);
	}

	private void startIntervalEndTask()
	{
		if(_state.isAlive())
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
			return;
		}

		if(!_state.isInterval())
		{
			_state.setRespawnDate(getRespawnInterval());
			_state.setState(State.INTERVAL);
			_state.update();
		}

		_intervalEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new IntervalEnd(), _state.getInterval());
	}

	public void setLastAttackTime()
	{
		_lastAttackTime = System.currentTimeMillis();
	}

	public boolean isEnableEnter()
	{
		return _state.isNotSpawn();
	}

	/**
	 * @return позицию спауна в массиве
	 */
	private static int getSpawnIndex(final int npcId)
	{
		if(npcId == SPORE_BASIC)
			return 4;
		return npcId - 25605;
	}

	/**
	 * @return позицию спауна в массиве (противоположный элемент)
	 */
	private static int getReverseSpawnIndex(final int npcId)
	{
		switch (npcId)
		{
			case SPORE_FIRE:
				return getSpawnIndex(SPORE_WATER);
			case SPORE_WATER:
				return getSpawnIndex(SPORE_FIRE);
			case SPORE_WIND:
				return getSpawnIndex(SPORE_EARTH);
			case SPORE_EARTH:
				return getSpawnIndex(SPORE_WIND);
		}
		return 4;
	}

	private static int getRespawnInterval()
	{
		return Config.FWE_FIXINTERVALOFEPIDOS + Rnd.get(Config.FWE_RANDOMINTERVALOFEPIDOS);
	}

	public final EpicBossState getState()
	{
		return _state;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final EpidosManager _instance = new EpidosManager();
	}
}
