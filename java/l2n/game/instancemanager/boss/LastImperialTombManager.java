package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptEventType;
import l2n.game.L2GameThreadPools;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2Party;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.QuestState;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorTable;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

/**
 * @author L2System Project (L2JFree)
 */
public class LastImperialTombManager extends EventScript
{
	private static final Logger _log = Logger.getLogger(LastImperialTombManager.class.getName());

	private static LastImperialTombManager _instance;

	private static boolean _isInvaded = false;

	// Instance list of monsters.
	protected static GArray<L2NpcInstance> _hallAlarmDevices = new GArray<L2NpcInstance>();
	protected static GArray<L2NpcInstance> _darkChoirPlayers = new GArray<L2NpcInstance>();
	protected static GArray<L2NpcInstance> _darkChoirCaptains = new GArray<L2NpcInstance>();
	protected static GArray<L2NpcInstance> _room1Monsters = new GArray<L2NpcInstance>();
	protected static GArray<L2NpcInstance> _room2InsideMonsters = new GArray<L2NpcInstance>();
	protected static GArray<L2NpcInstance> _room2OutsideMonsters = new GArray<L2NpcInstance>();

	// Instance list of doors.
	protected static GArray<L2DoorInstance> _room1Doors = new GArray<L2DoorInstance>();
	protected static GArray<L2DoorInstance> _room2InsideDoors = new GArray<L2DoorInstance>();
	protected static GArray<L2DoorInstance> _room2OutsideDoors = new GArray<L2DoorInstance>();
	protected static L2DoorInstance _room3Door = null;

	// Instance list of players.
	protected static GArray<L2Player> _partyLeaders = new GArray<L2Player>();
	protected static GArray<L2Player> _registedPlayers = new GArray<L2Player>();
	protected static L2Player _commander = null;

	// Frintezza's Magic Force Field Removal Scroll.
	private static final int SCROLL = 8073;

	private static final int ALARM_DEVICE = 18328;
	private static final int CHOIR_PRAYER = 18339;
	private static final int CHOIR_CAPTAIN = 18334;

	// player does reach to HallofFrintezza
	private static boolean _isReachToHall = false;

	private static final int[][] _invadeLoc = {
			{ 173235, -76884, -5107 },
			{ 175003, -76933, -5107 },
			{ 174196, -76190, -5107 },
			{ 174013, -76120, -5107 },
			{ 173263, -75161, -5107 } };

	private static ScheduledFuture<?> _InvadeTask = null;
	private static ScheduledFuture<?> _RegistrationTimeInfoTask = null;
	private static ScheduledFuture<?> _Room1SpawnTask = null;
	private static ScheduledFuture<?> _Room2InsideDoorOpenTask = null;
	private static ScheduledFuture<?> _Room2OutsideSpawnTask = null;
	private static ScheduledFuture<?> _CheckTimeUpTask = null;

	private final static String _name = "LastImperialTomb";
	private static L2Zone _zone;

	// Constructor
	// Load monsters and close doors.
	public LastImperialTombManager()
	{
		_instance = this;
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.epic, 702121, false);

		LastImperialTombSpawnlist.clear();
		LastImperialTombSpawnlist.fill();
		initDoors();

		addEventId(ScriptEventType.ON_DIE);
		_log.info("LastImperialTombManager: Init The Last Imperial Tomb.");
	}

	// Instance.
	public static LastImperialTombManager getInstance()
	{
		if(_instance == null)
			_instance = new LastImperialTombManager();
		return _instance;
	}

	public int getRandomRespawnDate()
	{
		return 0;
	}

	// Setting list of doors and close doors.
	private void initDoors()
	{
		_room1Doors.clear();
		_room1Doors.add(DoorTable.getInstance().getDoor(25150042));
		for(int i = 25150051; i <= 25150058; i++)
			_room1Doors.add(DoorTable.getInstance().getDoor(i));

		_room2InsideDoors.clear();
		for(int i = 25150061; i <= 25150070; i++)
			_room2InsideDoors.add(DoorTable.getInstance().getDoor(i));
		_room2OutsideDoors.clear();
		_room2OutsideDoors.add(DoorTable.getInstance().getDoor(25150043));
		_room2OutsideDoors.add(DoorTable.getInstance().getDoor(25150045));
		_room3Door = DoorTable.getInstance().getDoor(25150046);
		for(final L2DoorInstance door : _room1Doors)
			door.closeMe();
		for(final L2DoorInstance door : _room2InsideDoors)
			door.closeMe();
		for(final L2DoorInstance door : _room2OutsideDoors)
			door.closeMe();
		_room3Door.closeMe();
	}

	// Return true,tomb was already invaded by players.
	public boolean isInvaded()
	{
		return _isInvaded;
	}

	public boolean isReachToHall()
	{
		return _isReachToHall;
	}

	// RegistrationMode = command channel.
	public boolean tryRegistrationCc(final L2Player pc)
	{
		if(!FrintezzaManager.isEnableEnterToLair())
		{
			pc.sendMessage("Currently no entry possible.");
			return false;
		}

		if(isInvaded())
		{
			pc.sendMessage("Another group is already fighting inside the imperial tomb.");
			return false;
		}

		if(_commander == null)
		{
			final L2Party party = pc.getParty();
			if(party != null)
			{
				final L2CommandChannel channel = party.getCommandChannel();
				if(channel != null)
				{
					if(channel.getChannelLeader().getObjectId() != pc.getObjectId())
						return false;
					if(channel.getParties().size() < Config.LIT_MIN_PARTY_CNT || channel.getParties().size() > Config.LIT_MAX_PARTY_CNT)
						return false;
					if(pc.getInventory().getCountOf(SCROLL) < 1)
						return false;

					final GArray<L2Player> members = channel.getMembers();
					for(final L2Player member : members)
					{
						if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
						{
							pc.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
							return false;
						}

						// проверяем расстояние
						if(!pc.isInRange(member, 500))
						{
							final SystemMessage sm = new SystemMessage(SystemMessage.C1_IS_IN_A_LOCATION_WHICH_CANNOT_BE_ENTERED_THEREFORE_IT_CANNOT_BE_PROCESSED);
							sm.addName(member);
							pc.sendPacket(sm);
							return false;
						}
					}

					return true;
				}
			}

			pc.sendMessage("You must be the commander of a party channel and possess a \"Frintezza's Magic Force Field Removal Scroll\"."); // Retail messages?
			return false;
		}
		else
		{
			pc.sendMessage("There can only one command channel register at the same time."); // Retail messages?
			return false;
		}
	}

	// RegistrationMode = party.
	public boolean tryRegistrationPt(final L2Player pc)
	{
		if(!FrintezzaManager.isEnableEnterToLair())
		{
			pc.sendMessage("Currently no entry possible."); // Retail messages?
			return false;
		}

		if(isInvaded())
		{
			pc.sendMessage("Another group is already fighting inside the imperial tomb."); // Retail messages?
			return false;
		}

		if(_partyLeaders.size() < Config.LIT_MAX_PARTY_CNT)
			if(pc.getParty() != null)
			{
				final L2Party party = pc.getParty();
				if(party.getPartyLeader() != pc || pc.getInventory().getCountOf(SCROLL) < 1)
					return false;

				final GArray<L2Player> members = party.getPartyMembers();
				for(final L2Player member : members)
				{
					if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
					{
						pc.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
						return false;
					}

					// проверяем расстояние
					if(!pc.isInRange(member, 500))
					{
						final SystemMessage sm = new SystemMessage(SystemMessage.C1_IS_IN_A_LOCATION_WHICH_CANNOT_BE_ENTERED_THEREFORE_IT_CANNOT_BE_PROCESSED);
						sm.addName(member);
						pc.sendPacket(sm);
						return false;
					}
				}

				return true;
			}

		pc.sendMessage("You must be the leader of a party and possess a \"Frintezza's Magic Force Field Removal Scroll\"."); // Retail messages?
		return false;
	}

	public void unregisterPt(final L2Party pt)
	{
		if(_partyLeaders.contains(pt.getPartyLeader()))
		{
			_partyLeaders.remove(pt.getPartyLeader());
			pt.getPartyLeader().sendMessage("Warning: Unregistered from last imperial tomb invasion."); // Retail messages?
		}
	}

	public void unregisterPc(final L2Player pc)
	{
		if(_registedPlayers.contains(pc))
		{
			_registedPlayers.remove(pc);
			pc.sendMessage("Warning: Unregistered from last imperial tomb invasion."); // Retail messages?
		}
	}

	// RegistrationMode = single.
	public boolean tryRegistrationPc(final L2Player pc)
	{
		if(!FrintezzaManager.isEnableEnterToLair())
		{
			pc.sendMessage("Currently no entry possible."); // Retail messages?
			return false;
		}

		if(_registedPlayers.contains(pc))
		{
			pc.sendMessage("You are already registered."); // Retail messages?
			return false;
		}

		if(isInvaded())
		{
			pc.sendMessage("Another group is already fighting inside the imperial tomb."); // Retail messages?
			return false;
		}

		if(_registedPlayers.size() < Config.LIT_MAX_PLAYER_CNT)
			if(pc.getInventory().getCountOf(SCROLL) >= 1)
				return true;

		pc.sendMessage("You have to possess a \"Frintezza's Magic Force Field Removal Scroll\"."); // Retail messages?
		return false;
	}

	// Registration to enter to tomb.
	public synchronized void registration(final L2Player pc, final L2NpcInstance npc)
	{
		switch (Config.LIT_REGISTRATION_MODE)
		{
			case 0:
			{
				if(_commander != null)
					return;
				_commander = pc;
				if(_InvadeTask != null)
					_InvadeTask.cancel(true);
				_InvadeTask = L2GameThreadPools.getInstance().scheduleGeneral(new Invade(), 10000);
				break;
			}
			case 1:
			{
				if(_partyLeaders.contains(pc))
					return;
				_partyLeaders.add(pc);

				if(_partyLeaders.size() == 1)
					_RegistrationTimeInfoTask = L2GameThreadPools.getInstance().scheduleGeneral(new AnnouncementRegstrationInfo(npc, Config.LIT_REGISTRATION_TIME * 60000), 1000);
				break;
			}
			case 2:
			{
				if(_registedPlayers.contains(pc))
					return;
				_registedPlayers.add(pc);
				if(_registedPlayers.size() == 1)
					_RegistrationTimeInfoTask = L2GameThreadPools.getInstance().scheduleGeneral(new AnnouncementRegstrationInfo(npc, Config.LIT_REGISTRATION_TIME * 60000), 1000);
				break;
			}
			default:
				_log.warning("LastImperialTombManager: Invalid Registration Mode!");
		}
	}

	// Announcement of remaining time of registration to players.
	protected void doAnnouncementRegstrationInfo(final L2NpcInstance npc, int remaining)
	{
		CreatureSay cs = null;

		if(remaining == Config.LIT_REGISTRATION_TIME * 60000)
		{
			cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "Entrance is now possible.");
			npc.broadcastPacket(cs);
		}

		if(remaining >= 60000)
		{
			cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), remaining / 60000 + " minute(s) left for entrance.");
			npc.broadcastPacket(cs);
			remaining = remaining - 60000;

			switch (Config.LIT_REGISTRATION_MODE)
			{
				case 1:
					Functions.npcSayInRange(npc, "", 100);
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "For entrance, at least " + Config.LIT_MIN_PARTY_CNT + " parties are needed.");
					npc.broadcastPacket(cs);
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), Config.LIT_MAX_PARTY_CNT + " is the maximum party count.");
					npc.broadcastPacket(cs);
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "The current number of registered parties is " + _partyLeaders.size() + ".");
					npc.broadcastPacket(cs);
					break;
				case 2:
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "For entrance, at least " + Config.LIT_MIN_PLAYER_CNT + " people are needed.");
					npc.broadcastPacket(cs);
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), Config.LIT_MAX_PLAYER_CNT + " is the capacity.");
					npc.broadcastPacket(cs);
					cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), _registedPlayers.size() + " people are currently registered.");
					npc.broadcastPacket(cs);
					break;
			}

			if(_RegistrationTimeInfoTask != null)
				_RegistrationTimeInfoTask.cancel(true);
			_RegistrationTimeInfoTask = L2GameThreadPools.getInstance().scheduleGeneral(new AnnouncementRegstrationInfo(npc, remaining), 60000);
		}
		else
		{
			cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "Entrance period ended.");
			npc.broadcastPacket(cs);

			switch (Config.LIT_REGISTRATION_MODE)
			{
				case 1:
					if(_partyLeaders.size() < Config.LIT_MIN_PARTY_CNT || _partyLeaders.size() > Config.LIT_MAX_PARTY_CNT)
					{
						cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "Since the conditions were not met, the entrance was refused.");
						npc.broadcastPacket(cs);
						return;
					}
					break;
				case 2:
					if(_registedPlayers.size() < Config.LIT_MIN_PLAYER_CNT || _registedPlayers.size() > Config.LIT_MAX_PLAYER_CNT)
					{
						cs = new CreatureSay(npc.getObjectId(), Say2C.SHOUT, npc.getName(), "Since the conditions were not met, the entrance was refused.");
						npc.broadcastPacket(cs);
						return;
					}
					break;
			}

			if(_RegistrationTimeInfoTask != null)
				_RegistrationTimeInfoTask.cancel(true);

			if(_InvadeTask != null)
				_InvadeTask.cancel(true);
			_InvadeTask = L2GameThreadPools.getInstance().scheduleGeneral(new Invade(), 10000);
		}
	}

	// Invade to tomb.
	public void doInvade()
	{
		initDoors();

		switch (Config.LIT_REGISTRATION_MODE)
		{
			case 0:
				doInvadeCc();
				break;
			case 1:
				doInvadePt();
				break;
			case 2:
				doInvadePc();
				break;
			default:
				_log.warning("LastImperialTombManager: Invalid Registration Mode!");
				return;
		}

		_isInvaded = true;

		if(_Room1SpawnTask != null)
			_Room1SpawnTask.cancel(true);
		_Room1SpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRoom1Mobs1st(), 15000);

		if(_CheckTimeUpTask != null)
			_CheckTimeUpTask.cancel(true);
		_CheckTimeUpTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckTimeUp(Config.LIT_TIME_LIMIT * 60 * 1000), 15 * 1000);
	}

	// Invade to tomb. when registration mode is command channel.
	private void doInvadeCc()
	{
		if(_commander.getInventory().getCountOf(SCROLL) < 1)
		{
			_commander.sendPacket(new SystemMessage(SystemMessage.INCORRECT_ITEM_COUNT));
			_commander.sendMessage("Since the conditions were not met, the entrance was refused.");
			return;
		}
		_commander.getInventory().destroyItemByItemId(SCROLL, 1, true);

		int locId = 0;
		for(final L2Player pc : _commander.getParty().getCommandChannel().getMembers())
		{
			if(locId >= 5)
				locId = 0;
			teleport(pc, _invadeLoc[locId][0] + Rnd.get(50), _invadeLoc[locId][1] + Rnd.get(50), _invadeLoc[locId][2]);
			locId++;
		}
	}

	// Invade to tomb. when registration mode is party.
	private void doInvadePt()
	{
		boolean isReadyToInvade = true;

		final SystemMessage sm = new SystemMessage(SystemMessage.S1);
		sm.addString("Since the conditions were not met, the entrance was refused.");
		for(final L2Player ptl : _partyLeaders)
			if(ptl.getInventory().getCountOf(SCROLL) < 1)
			{
				ptl.sendPacket(new SystemMessage(SystemMessage.INCORRECT_ITEM_COUNT));
				ptl.sendPacket(sm);

				isReadyToInvade = false;
			}

		if(!isReadyToInvade)
		{
			for(final L2Player ptl : _partyLeaders)
				ptl.sendPacket(sm);

			return;
		}

		for(final L2Player ptl : _partyLeaders)
			ptl.getInventory().destroyItemByItemId(SCROLL, 1, true);

		int locId = 0;
		for(final L2Player ptl : _partyLeaders)
		{
			if(locId >= 5)
				locId = 0;
			for(final L2Player pc : ptl.getParty().getPartyMembers())
				teleport(pc, _invadeLoc[locId][0] + Rnd.get(50), _invadeLoc[locId][1] + Rnd.get(50), _invadeLoc[locId][2]);
			locId++;
		}
	}

	// Invade to tomb. when registration mode is single.
	private void doInvadePc()
	{
		boolean isReadyToInvade = true;

		for(final L2Player pc : _registedPlayers)
			if(pc.getInventory().getCountOf(SCROLL) < 1)
			{
				SystemMessage sm = new SystemMessage(SystemMessage.INCORRECT_ITEM_COUNT);
				pc.sendPacket(sm);

				sm = new SystemMessage(SystemMessage.S1);
				sm.addString("Since the conditions were not met, the entrance was refused.");
				pc.sendPacket(sm);

				isReadyToInvade = false;
			}

		if(!isReadyToInvade)
		{
			for(final L2Player pc : _registedPlayers)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.S1);
				sm.addString("Since the conditions were not met, the entrance was refused.");
				pc.sendPacket(sm);
			}

			return;
		}

		for(final L2Player pc : _registedPlayers)
			pc.getInventory().destroyItemByItemId(SCROLL, 1, true);

		int locId = 0;
		for(final L2Player pc : _registedPlayers)
		{
			if(locId >= 5)
				locId = 0;
			teleport(pc, _invadeLoc[locId][0] + Rnd.get(50), _invadeLoc[locId][1] + Rnd.get(50), _invadeLoc[locId][2]);
			locId++;
		}
	}

	// Is the door of room1 in confirmation to open.
	public void onKillHallAlarmDevice()
	{
		int killCnt = 0;
		for(final L2NpcInstance HallAlarmDevice : _hallAlarmDevices)
			if(HallAlarmDevice.isDead())
				killCnt++;

		switch (killCnt)
		{
			case 1:
				if(Rnd.chance(0))
				{
					openRoom1Doors();
					openRoom2OutsideDoors();
					spawnRoom2InsideMob();
				}
				else
				{
					if(_Room1SpawnTask != null)
						_Room1SpawnTask.cancel(true);
					_Room1SpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRoom1Mobs2nd(), 3000);
				}
				break;
			case 2:
				if(Rnd.chance(0))
				{
					openRoom1Doors();
					openRoom2OutsideDoors();
					spawnRoom2InsideMob();
				}
				else
				{
					if(_Room1SpawnTask != null)
						_Room1SpawnTask.cancel(true);
					_Room1SpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRoom1Mobs3rd(), 3000);
				}
				break;
			case 3:
				if(Rnd.chance(0))
				{
					openRoom1Doors();
					openRoom2OutsideDoors();
					spawnRoom2InsideMob();
				}
				else
				{
					if(_Room1SpawnTask != null)
						_Room1SpawnTask.cancel(true);
					_Room1SpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRoom1Mobs4th(), 3000);
				}
				break;
			case 4:
				openRoom1Doors();
				openRoom2OutsideDoors();
				spawnRoom2InsideMob();
				break;
			default:
				break;
		}
	}

	// Is the door of inside of room2 in confirmation to open.
	public void onKillDarkChoirPlayer()
	{
		int killCnt = 0;

		for(final L2NpcInstance DarkChoirPlayer : _room2InsideMonsters)
			if(DarkChoirPlayer.isDead())
				killCnt++;

		if(_room2InsideMonsters.size() <= killCnt)
		{
			if(_Room2InsideDoorOpenTask != null)
				_Room2InsideDoorOpenTask.cancel(true);
			if(_Room2OutsideSpawnTask != null)
				_Room2OutsideSpawnTask.cancel(true);

			_Room2InsideDoorOpenTask = L2GameThreadPools.getInstance().scheduleGeneral(new OpenRoom2InsideDoors(), 3000);
			_Room2OutsideSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnRoom2OutsideMobs(), 4000);
		}
	}

	// Is the door of outside of room2 in confirmation to open.
	public void onKillDarkChoirCaptain()
	{
		int killCnt = 0;

		for(final L2NpcInstance DarkChoirCaptain : _darkChoirCaptains)
			if(DarkChoirCaptain.isDead())
				killCnt++;

		if(_darkChoirCaptains.size() <= killCnt)
		{
			openRoom2OutsideDoors();

			for(final L2NpcInstance mob : _room2OutsideMonsters)
				mob.deleteMe();

			for(final L2NpcInstance DarkChoirCaptain : _darkChoirCaptains)
				DarkChoirCaptain.deleteMe();
		}
	}

	private void openRoom1Doors()
	{
		for(final L2NpcInstance npc : _hallAlarmDevices)
			if(npc != null)
				npc.deleteMe();

		for(final L2NpcInstance npc : _room1Monsters)
			if(npc != null)
				npc.deleteMe();

		for(final L2DoorInstance door : _room1Doors)
			door.openMe();
	}

	protected void openRoom2InsideDoors()
	{
		for(final L2DoorInstance door : _room2InsideDoors)
			door.openMe();
	}

	protected void openRoom2OutsideDoors()
	{
		for(final L2DoorInstance door : _room2OutsideDoors)
			door.openMe();
		_room3Door.openMe();
	}

	protected void closeRoom2OutsideDoors()
	{
		for(final L2DoorInstance door : _room2OutsideDoors)
			door.closeMe();
		_room3Door.closeMe();
	}

	private void spawnRoom2InsideMob()
	{
		L2NpcInstance mob;
		for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom2InsideSpawnList())
		{
			mob = spawn.doSpawn(true);
			spawn.stopRespawn();
			_room2InsideMonsters.add(mob);
		}
	}

	public void setReachToHall()
	{
		_isReachToHall = true;
	}

	protected void doCheckTimeUp(int remaining)
	{
		if(_isReachToHall)
			return;

		CreatureSay cs = null;
		int timeLeft;
		int interval;

		if(remaining > 300000)
		{
			timeLeft = remaining / 60000;
			interval = 300000;
			cs = new CreatureSay(0, Say2C.ALLIANCE, "Notice", timeLeft + " minutes left.");
			remaining = remaining - 300000;
		}
		else if(remaining > 60000)
		{
			timeLeft = remaining / 60000;
			interval = 60000;
			cs = new CreatureSay(0, Say2C.ALLIANCE, "Notice", timeLeft + " minutes left.");
			remaining = remaining - 60000;
		}
		else if(remaining > 30000)
		{
			timeLeft = remaining / 1000;
			interval = 30000;
			cs = new CreatureSay(0, Say2C.ALLIANCE, "Notice", timeLeft + " seconds left.");
			remaining = remaining - 30000;
		}
		else
		{
			timeLeft = remaining / 1000;
			interval = 10000;
			cs = new CreatureSay(0, Say2C.ALLIANCE, "Notice", timeLeft + " seconds left.");
			remaining = remaining - 10000;
		}

		for(final L2Player pc : getPlayersInside())
			pc.sendPacket(cs);

		if(_CheckTimeUpTask != null)
			_CheckTimeUpTask.cancel(true);
		if(remaining >= 10000)
			_CheckTimeUpTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckTimeUp(remaining), interval);
		else
			_CheckTimeUpTask = L2GameThreadPools.getInstance().scheduleGeneral(new TimeUp(), interval);
	}

	protected void cleanUpTomb(final boolean banish)
	{
		initDoors();
		cleanUpMobs();
		if(banish)
			banishForeigners();
		cleanUpRegister();
		_isInvaded = false;
		_isReachToHall = false;

		if(_InvadeTask != null)
		{
			_InvadeTask.cancel(true);
			_InvadeTask = null;
		}
		if(_RegistrationTimeInfoTask != null)
		{
			_RegistrationTimeInfoTask.cancel(true);
			_RegistrationTimeInfoTask = null;
		}
		if(_Room1SpawnTask != null)
		{
			_Room1SpawnTask.cancel(true);
			_Room1SpawnTask = null;
		}
		if(_Room2InsideDoorOpenTask != null)
		{
			_Room2InsideDoorOpenTask.cancel(true);
			_Room2InsideDoorOpenTask = null;
		}
		if(_Room2OutsideSpawnTask != null)
		{
			_Room2OutsideSpawnTask.cancel(true);
			_Room2OutsideSpawnTask = null;
		}
		if(_CheckTimeUpTask != null)
		{
			_CheckTimeUpTask.cancel(true);
			_CheckTimeUpTask = null;
		}
	}

	// Delete all mobs from tomb.
	private void cleanUpMobs()
	{
		for(final L2NpcInstance mob : _hallAlarmDevices)
			if(mob != null)
				mob.deleteMe();
		for(final L2NpcInstance mob : _darkChoirPlayers)
			if(mob != null)
				mob.deleteMe();
		for(final L2NpcInstance mob : _darkChoirCaptains)
			if(mob != null)
				mob.deleteMe();
		for(final L2NpcInstance mob : _room1Monsters)
			if(mob != null)
				mob.deleteMe();
		for(final L2NpcInstance mob : _room2InsideMonsters)
			if(mob != null)
				mob.deleteMe();
		for(final L2NpcInstance mob : _room2OutsideMonsters)
			if(mob != null)
				mob.deleteMe();

		_hallAlarmDevices.clear();
		_darkChoirCaptains.clear();
		_room1Monsters.clear();
		_room2InsideMonsters.clear();
		_room2OutsideMonsters.clear();
	}

	private void cleanUpRegister()
	{
		_commander = null;
		_partyLeaders.clear();
		_registedPlayers.clear();
	}

	private class SpawnRoom1Mobs1st implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance mob;
			for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom1SpawnList1st())
				if(spawn.getNpcId() == 18328)
				{
					mob = spawn.doSpawn(true);
					spawn.stopRespawn();
					_hallAlarmDevices.add(mob);
				}
				else
				{
					mob = spawn.doSpawn(true);
					spawn.stopRespawn();
					_room1Monsters.add(mob);
				}
		}
	}

	private class SpawnRoom1Mobs2nd implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance mob;
			for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom1SpawnList2nd())
			{
				mob = spawn.doSpawn(true);
				spawn.stopRespawn();
				_room1Monsters.add(mob);
			}
		}
	}

	private class SpawnRoom1Mobs3rd implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance mob;
			for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom1SpawnList3rd())
			{
				mob = spawn.doSpawn(true);
				spawn.stopRespawn();
				_room1Monsters.add(mob);
			}
		}
	}

	private class SpawnRoom1Mobs4th implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance mob;
			for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom1SpawnList4th())
			{
				mob = spawn.doSpawn(true);
				spawn.stopRespawn();
				_room1Monsters.add(mob);
			}
		}
	}

	private class OpenRoom2InsideDoors implements Runnable
	{
		@Override
		public void run()
		{
			closeRoom2OutsideDoors();
			openRoom2InsideDoors();
		}
	}

	private class SpawnRoom2OutsideMobs implements Runnable
	{
		@Override
		public void run()
		{
			for(final L2Spawn spawn : LastImperialTombSpawnlist.getRoom2OutsideSpawnList())
				if(spawn.getNpcId() == 18334)
				{
					final L2NpcInstance mob = spawn.doSpawn(true);
					spawn.stopRespawn();
					_darkChoirCaptains.add(mob);
				}
				else
				{
					final L2NpcInstance mob = spawn.doSpawn(true);
					spawn.startRespawn();
					_room2OutsideMonsters.add(mob);
				}
		}
	}

	private class AnnouncementRegstrationInfo implements Runnable
	{
		private L2NpcInstance _npc = null;
		private final int _remaining;

		public AnnouncementRegstrationInfo(final L2NpcInstance npc, final int remaining)
		{
			_npc = npc;
			_remaining = remaining;
		}

		@Override
		public void run()
		{
			doAnnouncementRegstrationInfo(_npc, _remaining);
		}
	}

	private class Invade implements Runnable
	{
		@Override
		public void run()
		{
			doInvade();
		}
	}

	private class CheckTimeUp implements Runnable
	{
		private final int _remaining;

		public CheckTimeUp(final int remaining)
		{
			_remaining = remaining;
		}

		@Override
		public void run()
		{
			doCheckTimeUp(_remaining);
		}
	}

	private class TimeUp implements Runnable
	{
		@Override
		public void run()
		{
			if(_isReachToHall)
				return;

			cleanUpTomb(true);
		}
	}

	private static void banishForeigners()
	{
		for(final L2Player player : getPlayersInside())
		{
			if(_name != null)
			{
				final QuestState qs = player.getQuestState(_name);
				if(qs != null && qs.getInt("ok") == 1)
					qs.exitCurrentQuest(true);
			}
			player.teleToClosestTown();
		}
	}

	private static GArray<L2Player> getPlayersInside()
	{
		return getZone().getInsidePlayers();
	}

	public synchronized boolean isPlayersAnnihilated()
	{
		for(final L2Player pc : getPlayersInside())
			if(!pc.isDead())
				return false;
		return true;
	}

	@Override
	protected void onDie(final L2Character self, final L2Character killer)
	{
		if(self == null)
			return;

		switch (self.getNpcId())
		{
			case ALARM_DEVICE:
				onKillHallAlarmDevice();
				break;
			case CHOIR_PRAYER:
				onKillDarkChoirPlayer();
				break;
			case CHOIR_CAPTAIN:
				onKillDarkChoirCaptain();
				break;
		}
	}

	private static void teleport(final L2Player player, final int x, final int y, final int z)
	{
		player.teleToLocation(GeoEngine.findPointToStay(x, y, z, 0, 0));
	}

	public static L2Zone getZone()
	{
		return _zone;
	}
}
