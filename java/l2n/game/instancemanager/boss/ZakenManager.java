package l2n.game.instancemanager.boss;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.GameTimeController;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.InstanceWorld;
import l2n.game.model.EffectList;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2Party;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExSendUIEvent;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncMul;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.Collection;

public class ZakenManager
{
	public static final class ZakenInstanceInfo extends InstanceWorld
	{
		private final int zakenNpcId;
		private final Location zakenLoc;
		private final IntArrayList blueKandles;
		private final IntArrayList redKandles;

		private final GArray<L2Player> players;

		public ZakenInstanceInfo(final boolean isDay, final Location loc)
		{
			zakenNpcId = isDay ? 29176 : 29022;
			zakenLoc = loc;
			blueKandles = new IntArrayList();
			redKandles = new IntArrayList();
			players = new GArray<L2Player>();
		}

		public Location getZakenLoc()
		{
			return zakenLoc;
		}

		public IntArrayList getBlueKandles()
		{
			return blueKandles;
		}

		public IntArrayList getRedKandles()
		{
			return redKandles;
		}

		public int getZakenId()
		{
			return zakenNpcId;
		}

		public void addPlayers(final Collection<L2Player> members)
		{
			players.addAll(members);
		}

		public GArray<L2Player> getPlayers()
		{
			return players;
		}
	}

	public static final int[][] ROOM_CENTER_COORDS = {
			// комнаты 1-го этажа
			{ 54248, 220120, -3522, 0 },
			{ 56280, 220120, -3522, 0 },
			{ 55272, 219096, -3522, 0 },
			{ 54232, 218072, -3522, 0 },
			{ 56296, 218072, -3522, 0 },
			// комнаты 2-го этажа
			{ 56280, 218072, -3250, 0 },
			{ 54232, 218072, -3250, 0 },
			{ 55256, 219112, -3250, 0 },
			{ 56296, 220120, -3250, 0 },
			{ 54232, 220136, -3250, 0 },
			// комнаты 3-го этажа
			{ 56296, 218072, -2978, 0 },
			{ 54232, 218072, -2978, 0 },
			{ 55272, 219112, -2978, 0 },
			{ 56280, 220120, -2978, 0 },
			{ 54232, 220120, -2978, 0 }
	};

	private static boolean checkTimeCondition(final L2Player player, final boolean isDay)
	{
		if(player.isGM())
		{
			player.sendMessage("< now " + (GameTimeController.getInstance().isNowNight() ? "night " : "day"));
			player.sendMessage("< you try enter in " + (isDay ? "day" : "night") + " zaken");
		}

		// Доступ к дневному закену только днём
		if(isDay && GameTimeController.getInstance().isNowNight())
		{
			player.sendMessage(new CustomMessage("l2n.game.instancemanager.boss.ZakenManager.ZakenDay", player));
			return false;
		}

		// Доступ к ночному закену только ночью
		if(!isDay && !GameTimeController.getInstance().isNowNight())
		{
			player.sendMessage(new CustomMessage("l2n.game.instancemanager.boss.ZakenManager.ZakenNight", player));
			return false;
		}

		return true;
	}

	/** Загружаем данные */
	public static void init()
	{

	}

	public static int enterToZaken(final L2Player player, final boolean isDay)
	{
		if(!checkTimeCondition(player, isDay))
			return 0;

		// Вход для ГМов
		if(player.isGM() && player.isPartyLeader())
		{
			createNewZakenEvent(player.getParty().getPartyMembers(), isDay);
			return 0;
		}

		final int instanceId = isDay ? ReflectionTable.ZAKEN_DAY : ReflectionTable.ZAKEN_NIGH;

		final InstanceManager izm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> izs = InstanceManager.getInstance().getById(instanceId);
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return 4;
		}

		final Instance iz = izs.get(0);
		assert iz != null;

		final String name = iz.getName();
		final int minMembers = iz.getMinParty();
		final int maxMembers = iz.getMaxParty();
		final int minLevel = iz.getMinLevel();
		final int maxLevel = iz.getMaxLevel();
		final Location coords = iz.getTeleportCoords();

		final L2Party party = player.getParty();
		if(party == null)
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return 4;
		}

		// находиться ли пати уже в инстансе с закеном
		final boolean partyInInstance = party.isInReflection() ? party.getReflection().getInstancedZoneId() == instanceId : false;
		if(partyInInstance)
		{
			if(player.getLevel() < minLevel || player.getLevel() > maxLevel)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player);
				player.sendPacket(sm);
				return 6;
			}
			if(player.isCursedWeaponEquipped() || player.isInFlyingTransform() || player.isDead())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return 6;
			}

			// портуем игрока в инстанс к своим)
			final Reflection r = party.getReflection();
			player.setVar("backCoords", r.getReturnLoc().toXYZString());
			player.teleToLocation(coords, r.getId());
			return 0;
		}

		if(isDay)
		{
			// Является ли игрок лидером пати
			if(!party.isLeader(player))
			{
				player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
				return 4;
			}

			// проверяем требуемое количество игроков для входа
			if(party.getMemberCount() < minMembers)
			{
				player.sendMessage("The party must contains at least " + minMembers + " members.");
				return 4;
			}

			// проверяем требуемое количество игроков для входа
			if(party.getMemberCount() > maxMembers)
			{
				player.sendMessage("The party must contains not more than " + maxMembers + " members.");
				return 4;
			}

			// Проверяем каждого члена пати)
			for(final L2Player member : party.getPartyMembers())
			{
				if(member == null)
					continue;

				if(member.getLevel() < minLevel || member.getLevel() > maxLevel)
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
					member.sendPacket(sm);
					player.sendPacket(sm);
					return 6;
				}

				if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
					return 6;
				}

				if(!player.isInRange(member, 500))
				{
					member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					return 6;
				}

				if(izm.getTimeToNextEnterInstance(name, member) > 0)
				{
					party.broadcastToPartyMembers(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return 5;
				}
			}

			final Reflection r = createNewZakenEvent(party.getPartyMembers(), isDay);
			if(r != null)
			{
				party.setReflection(r);
				r.setParty(party);
			}
			return 0;
		}
		else
		{
			// Если не в командном чате
			final L2CommandChannel cchannel = party.getCommandChannel();
			if(cchannel == null)
			{
				player.sendPacket(Msg.YOU_CANNOT_ENTER_BECAUSE_YOU_ARE_NOT_ASSOCIATED_WITH_THE_CURRENT_COMMAND_CHANNEL);
				return 4;
			}

			if(cchannel.getChannelLeader() != player)
			{
				player.sendMessage("You must be leader of the command channel.");
				return 4;
			}

			final int playerCount = cchannel.getMemberCount();
			// проверяем требуемое количество игроков для входа
			if(playerCount < minMembers)
			{
				player.sendMessage("The command channel must contains at least " + minMembers + " members.");
				return 4;
			}

			if(playerCount > maxMembers)
			{
				player.sendMessage("The command channel must contains not more than " + maxMembers + " members.");
				return 4;
			}

			for(final L2Player member : cchannel.getMembers())
			{
				if(member == null)
					continue;

				if(member.getLevel() < minLevel || member.getLevel() > maxLevel)
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
					member.sendPacket(sm);
					player.sendPacket(sm);
					return 6;
				}

				if(member.isCursedWeaponEquipped() || member.isInFlyingTransform() || member.isDead())
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
					return 6;
				}

				if(!player.isInRange(member, 500))
				{
					member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					return 6;
				}

				if(izm.getTimeToNextEnterInstance(name, member) > 0)
				{
					cchannel.broadcastToChannelMembers(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return 5;
				}
			}

			final Reflection r = createNewZakenEvent(cchannel.getMembers(), isDay);
			if(r != null)
			{
				cchannel.setReflection(r);
				r.setCommandChannel(cchannel);
			}
			return 0;
		}
	}

	private static Reflection createNewZakenEvent(final GArray<L2Player> partyMembers, final boolean isDay)
	{
		final int instanceId = isDay ? ReflectionTable.ZAKEN_DAY : ReflectionTable.ZAKEN_NIGH;
		final TIntObjectHashMap<Instance> izs = InstanceManager.getInstance().getById(instanceId);

		final Instance iz = izs.get(0);
		assert iz != null;

		final String name = iz.getName();
		final Location coords = iz.getTeleportCoords();
		final int timelimit = iz.getDuration();
		final boolean dispellBuffs = iz.isRemoveBuffs();

		final Reflection r = new Reflection(name);
		r.setInstancedZoneId(instanceId);

		if(r.getReturnLoc() == null)
			r.setReturnLoc(iz.getReturnCoords());
		if(r.getTeleportLoc() == null)
			r.setTeleportLoc(coords);
		r.fillSpawns(iz.getSpawnsInfo());
		r.fillDoors(iz.getDoors());

		for(final L2Player member : partyMembers)
		{
			if(dispellBuffs)
			{
				member.stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
				if(member.getPet() != null)
					member.getPet().stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
			}

			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.teleToLocation(coords, r.getId());
			member.sendPacket(new ExSendUIEvent(member, false, true, 0, timelimit * 60 * 1000, ""));

			if(timelimit > 0)
				member.sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));
		}

		r.startCollapseTimer(timelimit * 60 * 1000);

		final int[] zaken_loc = ROOM_CENTER_COORDS[Rnd.get(ROOM_CENTER_COORDS.length)];
		final ZakenInstanceInfo world = new ZakenInstanceInfo(isDay, new Location(zaken_loc));
		// добавляем всех в список
		world.addPlayers(partyMembers);
		InstanceManager.getInstance().addWorld(r.getId(), world);

		return r;
	}

	public static void calcZakenStat(final L2NpcInstance zaken, final ZakenInstanceInfo instanceInfo)
	{
		final double count = instanceInfo.getRedKandles().size();
		final double stat = Math.max(0, 1 + instanceInfo.getRedKandles().size() / 40);
		if(count > 0)
		{
			zaken.addStatFunc(new FuncMul(Stats.POWER_DEFENCE, 0x30, zaken, stat));
			zaken.addStatFunc(new FuncMul(Stats.MAGIC_DEFENCE, 0x30, zaken, stat));
			zaken.addStatFunc(new FuncMul(Stats.POWER_ATTACK, 0x30, zaken, stat));
			zaken.addStatFunc(new FuncMul(Stats.MAGIC_ATTACK, 0x30, zaken, stat));
		}
	}

	public static void OnDie(final L2Character self, final L2Character killer)
	{
		// _log.info("ZakenManager: Zaken is die!");
	}
}
