package l2n.game.instancemanager.boss;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.InstanceWorld;
import l2n.game.instancemanager.SeedOfInfinityManager;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.EffectType;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author L2System Project
 * @date 12.01.2011
 * @time 16:31:50
 */
public class EkimusManager
{
	private static class TimeRemaining implements Runnable
	{
		private Reflection reflection;

		public TimeRemaining(Reflection ref)
		{
			reflection = ref;
		}

		@Override
		public void run()
		{
			EkimusWorld world;
			if(reflection != null && (world = InstanceManager.getInstance().getWorld(reflection.getId(), EkimusWorld.class)) != null)
				// если Экимуса ещё не убили
				if(!world.rb_died)
				{
					world.timer -= 5;
					// если время уже вышло
					if(world.timer == 0)
					{
						world.showMovieFailure();
						L2GameThreadPools.getInstance().scheduleGeneral(new BroadcastOnScreenMsgStr(reflection, atk_failure), time_scene_atk_failure);

						// удаляем всех нпс в инстансе
						for(L2NpcInstance npc : reflection.getNpcs())
							npc.deleteMe();
					}
					else
					{
						GArray<L2Player> players = reflection.getPlayers();
						for(L2Player player : players)
							player.sendPacket(new ExShowScreenMessage("Heart of Infinity Attack " + world.timer + " minute(s) are remaining.", 5000, ScreenMessageAlign.TOP_CENTER, false));

						world.remainingTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new TimeRemaining(reflection), 5 * 60 * 1000);
					}
				}
		}
	}

	private static class RespawnTumor implements Runnable
	{
		private L2NpcInstance destroyed_tumor;

		private RespawnTumor(L2NpcInstance npc)
		{
			destroyed_tumor = npc;
		}

		@Override
		public void run()
		{
			EkimusWorld w;
			if((w = InstanceManager.getInstance().getWorld(destroyed_tumor.getReflectionId(), EkimusWorld.class)) != null)
				// если Экимуса ещё не убили
				if(!w.rb_died)
				{
					// уменьшаем счётчик мертвый Опухолей
					w.status--;

					// респаун должен влиять на характериситки Экимуса
					if(w.ekimus != null && !w.ekimus.isDead())
					{
						w.ekimus.getEffectList().stopEffects(EffectType.SoulRetain);

						if(w.status > 0)
						{
							L2Skill debuff = soul_retain[w.status];
							// накладываем дебаф
							debuff.getEffects(w.ekimus, w.ekimus, false, false);
						}
					}

					Reflection ref = destroyed_tumor.getReflection();
					// востановились все колонии
					if(w.status == 0)
					{
						// восстановить связь с сабаками
						for(L2Player player : ref.getPlayers())
							player.sendPacket(connection_restored);

						w.feral_hound1.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, start_Attack, null);
						w.feral_hound2.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, start_Attack, null);
					}
					else
						for(L2Player player : ref.getPlayers())
							player.sendPacket(tumor_respawn);

					destroyed_tumor.deleteMe();
					addSpawnToInstance(TUMOR_OF_DEATH_NPCID, destroyed_tumor.getSpawnedLoc(), 0, destroyed_tumor.getReflectionId());
				}
		}
	}

	private static class SpawnEkimus implements Runnable
	{
		private EkimusWorld world;

		private SpawnEkimus(EkimusWorld w)
		{
			world = w;
		}

		@Override
		public void run()
		{
			// показываем сообщение
			for(L2Player pl : world.reflection.getPlayers())
				pl.sendPacket(atk_opening);

			// спауним Экимуса
			world.spawnEkimus();
		}
	}

	private static class BroadcastOnScreenMsgStr implements Runnable
	{
		private Reflection reflection;
		private L2GameServerPacket packet;

		public BroadcastOnScreenMsgStr(Reflection ref, L2GameServerPacket gsp)
		{
			reflection = ref;
			packet = gsp;
		}

		@Override
		public void run()
		{
			if(reflection != null && reflection.getInstancedZoneId() == ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK)
				for(L2Player pl : reflection.getPlayers())
					pl.sendPacket(packet);
		}
	}

	private static class InitialDelayTask implements Runnable
	{
		private long playerStoredId;

		public InitialDelayTask(L2Player starter)
		{
			playerStoredId = starter.getStoredId();
		}

		@Override
		public void run()
		{
			L2Player player;
			if((player = L2ObjectsStorage.getAsPlayer(playerStoredId)) != null)
			{
				EkimusWorld w = InstanceManager.getInstance().getWorld(player.getReflectionId(), EkimusWorld.class);
				if(w != null)
					w.initialInstance(player);
			}
		}
	}

	// скилы для дебафа Экимуса при убийстве Колоний
	private static final L2Skill[] soul_retain = new L2Skill[] {
			null,
			SkillTable.getInstance().getInfo(5923, 1),
			SkillTable.getInstance().getInfo(6020, 1),
			SkillTable.getInstance().getInfo(6021, 1),
			SkillTable.getInstance().getInfo(6022, 1),
			SkillTable.getInstance().getInfo(6023, 1) };

	private static final L2Skill stop_Attack = SkillTable.getInstance().getInfo(5909, 1);
	private static final L2Skill start_Attack = SkillTable.getInstance().getInfo(5910, 1);

	private static final L2GameServerPacket tumor_die = new ExShowScreenMessage("The tumor inside Heart of Infinity that has provided energy \n to Ekimus is destroyed!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	private static final L2GameServerPacket tumor_respawn = new ExShowScreenMessage("The tumor inside Heart of Infinity has been completely resurrected \n and started to energize Ekimus again...", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// [%s's party has entered the Chamber of Ekimus through the crack in the tumor!
	private static final String party_entered = "%name%'s party has entered the Chamber of Ekimus through the crack in the tumor!";

	private static final L2GameServerPacket connection_lost = new ExShowScreenMessage("With all connections to the tumor severed, Ekimus has lost its power to control the Feral Hound!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	private static final L2GameServerPacket connection_restored = new ExShowScreenMessage("With the connection to the tumor restored, Ekimus has regained control over the Feral Hound...", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// показываем сразу после ТП в зону
	private static final L2GameServerPacket atk_prepare = new ExShowScreenMessage("You will participate in Heart of Infinity Attack shortly. Be prepared for anything.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	private static final L2GameServerPacket atk_opening = new ExShowScreenMessage("You can hear the undead of Ekimus rushing toward you. Heart of Infinity Attack, it has now begun!", 8000, ScreenMessageAlign.TOP_CENTER, false);
	private static final L2GameServerPacket atk_success = new ExShowScreenMessage("Congratulations! You have succeeded at Heart of Infinity Attack! The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);
	private static final L2GameServerPacket atk_failure = new ExShowScreenMessage("You have failed at Heart of Infinity Attack... The instance will shortly expire.", 8000, ScreenMessageAlign.TOP_CENTER, false);

	// локации колоний
	private static final Location[] tumors_locations = {
			new Location(-179779, 212540, -15520, 49151, 301),
			new Location(-177028, 211135, -15520, 36863, 302),
			new Location(-176355, 208043, -15520, 28672, 303),
			new Location(-179284, 205990, -15520, 16384, 304), // тут спауним Опухоль с телепортом к Экимусу
			new Location(-182268, 208218, -15520, 4096, 305),
			new Location(-182069, 211140, -15520, 61439, 306) };

	private static final Location ekimus_loc = new Location(-179537, 209551, -15504, 16384);
	private static final Location feral_hound1_loc = new Location(-179686, 208854, -15496, 16384);
	private static final Location feral_hound2_loc = new Location(-179387, 208854, -15496, 16384);

	private static final int[] MONSTERS_NPCIDS = new int[] { 22516, 22517, 22518, 22519, 22520, 22521, 22522, 22524, 22526, 22528, 22530, 22532, 22534 };

	private static final int TUMOR_OF_DEATH_NPCID = 18708; // npc_ai={[spc_tumor_2lv_a] похоже убивать надо этого, потом ресается другой
	private static final int DESTROYED_TUMOR_NPCID = 32535; // отображает какой то диалог tumor_d_spc001.htm
	private static final int DESTROYED_TUMOR_ENTRANCE_NPCID = 32536; // отображает какой то диалог tumor_d_spc001.htm

	private static final int EKIMUS_NPCID = 29150;
	private static final int FERAL_HOUND_NPCID = 29151;

	// times
	private static final int time_scene_atk_opening = 62000;
	private static final int time_scene_atk_success = 20000;
	private static final int time_scene_atk_failure = 19000;

	private final static ReentrantLock worldLock = new ReentrantLock();

	public static class EkimusWorld extends InstanceWorld
	{
		public Reflection reflection;
		public boolean partyInHeart;

		public int status;
		public long timer;
		public ScheduledFuture<TimeRemaining> remainingTimeTask;

		public L2NpcInstance ekimus;
		public L2MonsterInstance feral_hound1;
		public L2MonsterInstance feral_hound2;

		public boolean rb_died = false;

		public EkimusWorld(Reflection ref)
		{
			reflection = ref;
		}

		/**
		 * метод для иниацилизации инстанса<br>
		 * Спауним только телепортер
		 * 
		 * @param player
		 *            - тот кто эту кашу заварил
		 */
		public void initialInstance(L2Player player)
		{
			// Колония через которую ТП к Экимусу
			addSpawnToInstance(DESTROYED_TUMOR_ENTRANCE_NPCID, tumors_locations[3], 0, reflection.getId());
		}

		/**
		 * Метод для входа к Экимусу: телепортирует группу в центр, показывает ролик, запускает спаун экимуса
		 * 
		 * @param party
		 *            - группа которая входит к Экимусу
		 */
		public void enterInHeart(L2Party party, L2NpcInstance teleporter)
		{
			partyInHeart = true;
			teleporter.deleteMe();

			// отправляем сообщение о том что вошли к Экимусу
			final L2GameServerPacket msg = new ExShowScreenMessage(party_entered.replace("%name%", party.getPartyLeader().getName()), 8000, ScreenMessageAlign.TOP_CENTER, false);
			for(L2Player player : reflection.getPlayers())
				player.sendPacket(msg);

			// телепортируем группу к Экимусу
			for(L2Player m : party.getPartyMembers())
				m.teleToLocation(ekimus_loc.rnd(100, 100, false));

			// октрываем дверь 14240102
			reflection.openDoor(14240102);

			// стартуем таск на проверку лимита по времени
			timer = 25; // 25 мин даётся
			remainingTimeTask = L2GameThreadPools.getInstance().scheduleGeneral(new TimeRemaining(reflection), 5 * 60 * 1000);

			// показываем видео
			showMovieOpening();

			// запускам спаун Экимуса
			L2GameThreadPools.getInstance().scheduleGeneral(new SpawnEkimus(this), time_scene_atk_opening + 2000);
		}

		/**
		 * Спаун Экимуса, Колоний, Сабачек, запуск таймера
		 */
		public void spawnEkimus()
		{
			// спауним колонии
			for(Location loc : tumors_locations)
			{
				addSpawnToInstance(TUMOR_OF_DEATH_NPCID, loc, 0, reflection.getId());
				// всех мобов
				for(int i = 0; i < 16; i++)
					addSpawnToInstance(MONSTERS_NPCIDS[Rnd.get(MONSTERS_NPCIDS.length)], loc.rnd(200, 300, false), 0, reflection.getId());
			}

			ekimus = addSpawnToInstance(EKIMUS_NPCID, ekimus_loc, 0, reflection.getId());
			ekimus.getAI().startAITask();

			feral_hound1 = (L2MonsterInstance) addSpawnToInstance(FERAL_HOUND_NPCID, feral_hound1_loc, 0, reflection.getId());
			feral_hound2 = (L2MonsterInstance) addSpawnToInstance(FERAL_HOUND_NPCID, feral_hound2_loc, 0, reflection.getId());
			feral_hound1.getAI().startAITask();
			feral_hound2.getAI().startAITask();
		}

		public void showMovieOpening()
		{
			for(L2Player player : reflection.getPlayers())
				player.showQuestMovie(ExStartScenePlayer.SCENE_ECHMUS_OPENING);
		}

		public void showMovieSuccess()
		{
			for(L2Player player : reflection.getPlayers())
				player.showQuestMovie(ExStartScenePlayer.SCENE_ECHMUS_SUCCESS);
		}

		public void showMovieFailure()
		{
			for(L2Player player : reflection.getPlayers())
				player.showQuestMovie(ExStartScenePlayer.SCENE_ECHMUS_FAIL);
		}
	}

	public static void enterInstance(L2Player player)
	{
		SystemMessage msg = InstanceManager.checkCondition(ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK, player, true, null, null);
		if(msg != null)
		{
			player.sendPacket(msg);
			return;
		}

		InstanceManager ilm = InstanceManager.getInstance();
		TIntObjectHashMap<Instance> ils = ilm.getById(ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);
		assert il != null;

		String name = il.getName();
		int duration = il.getDuration();
		boolean needCC = il.isNeedChannel();

		Reflection r = new Reflection(name);
		r.setInstancedZoneId(ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK);

		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());

			r.fillDoors(i.getDoors());
			r.fillSpawns(i.getSpawnsInfo());
		}

		EkimusWorld world = new EkimusWorld(r);
		world.status = 0;

		worldLock.lock();
		try
		{
			InstanceManager.getInstance().addWorld(r.getId(), world);
		}
		finally
		{
			worldLock.unlock();
		}

		if(needCC)
		{
			L2CommandChannel commandChannel = player.getParty().getCommandChannel();
			for(L2Player member : commandChannel.getMembers())
			{
				member.setReflection(r);
				member.teleToLocation(il.getTeleportCoords());
				member.setVar("backCoords", r.getReturnLoc().toXYZString());
				member.setVar(name, String.valueOf(System.currentTimeMillis()));
			}

			commandChannel.setReflection(r);
			r.setCommandChannel(commandChannel);
			r.startCollapseTimer(duration * 60000L);
			commandChannel.broadcastToChannelMembers(atk_prepare);
		}
		else
		{
			L2Party party = player.getParty();
			for(L2Player member : party.getPartyMembers())
			{
				member.setReflection(r);
				member.teleToLocation(il.getTeleportCoords());
				member.setVar("backCoords", r.getReturnLoc().toXYZString());
				member.setVar(name, String.valueOf(System.currentTimeMillis()));
			}

			party.setReflection(r);
			r.setParty(party);
			r.startCollapseTimer(duration * 60000L);
			party.broadcastToPartyMembers(atk_prepare);
		}

		L2GameThreadPools.getInstance().scheduleGeneral(new InitialDelayTask(player), 30 * 1000);
	}

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(self != null && self.getReflectionId() > 0 && self.getReflection().getInstancedZoneId() == ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK)
		{
			EkimusWorld world = InstanceManager.getInstance().getWorld(self.getReflectionId(), EkimusWorld.class);
			if(world == null)
				return;

			int npcId = self.getNpcId();
			switch (npcId)
			{
			// обработка смерти Опухоли
				case TUMOR_OF_DEATH_NPCID:
				{
					// увеличиваем количество убитых Опухолей
					world.status++;

					// смерть должна влиять на характериситки Экимуса
					if(world.ekimus != null && !world.ekimus.isDead() && world.status < 6)
					{
						world.ekimus.getEffectList().stopEffects(EffectType.SoulRetain);
						L2Skill debuff = soul_retain[world.status];
						// накладываем дебаф
						debuff.getEffects(world.ekimus, world.ekimus, false, false);
					}

					// уничтёжены все колонии
					if(world.status == 6)
					{
						for(L2Player player : world.reflection.getPlayers())
							player.sendPacket(connection_lost);

						// отправляем сабачек по местам
						// разоврать связь с сабаками, увезти их в угол и пусть не реагируют
						world.feral_hound1.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, stop_Attack, null);
						world.feral_hound2.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, stop_Attack, null);
					}
					else
						for(L2Player player : world.reflection.getPlayers())
							player.sendPacket(tumor_die);

					L2NpcInstance destroyed_tumor = addSpawnToInstance(DESTROYED_TUMOR_NPCID, self.getLoc(), 0, self.getReflectionId());
					L2GameThreadPools.getInstance().scheduleGeneral(new RespawnTumor(destroyed_tumor), 2 * 60 * 1000);
					break;
				}
				case EKIMUS_NPCID:
				{
					world.rb_died = true;
					world.ekimus.deleteMe();
					world.feral_hound1.deleteMe();
					world.feral_hound2.deleteMe();

					world.reflection.startCollapseTimer(5 * 60 * 1000L);

					SeedOfInfinityManager.addEkimusKill();

					world.showMovieSuccess();
					L2GameThreadPools.getInstance().scheduleGeneral(new BroadcastOnScreenMsgStr(world.reflection, atk_success), time_scene_atk_success);
				}
			}
		}
	}

	public static boolean teleToHeart(L2Player player, L2NpcInstance teleporter)
	{
		EkimusWorld world = InstanceManager.getInstance().getWorld(player.getReflectionId(), EkimusWorld.class);
		if(world != null && !world.partyInHeart)
		{
			world.enterInHeart(player.getParty(), teleporter);
			return true;
		}
		return false;
	}

	private static L2NpcInstance addSpawnToInstance(int npcId, Location loc, int randomOffset, long refId)
	{
		try
		{
			L2NpcTemplate template = NpcTable.getTemplate(npcId);
			if(template != null)
			{
				L2NpcInstance npc = NpcTable.getTemplate(npcId).getNewInstance();
				npc.setReflection(refId);
				npc.setSpawnedLoc(randomOffset > 50 ? loc.rnd(50, randomOffset, false) : loc);
				npc.onSpawn();
				npc.spawnMe(npc.getSpawnedLoc());
				return npc;
			}
		}
		catch(Exception e)
		{}
		return null;
	}
}
