package l2n.game.instancemanager.boss;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.EpicBossState.State;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SocialAction;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 18.02.2010
 * @time 15:28:58
 */
public class SailrenManager
{
	private static final Logger _log = Logger.getLogger(SailrenManager.class.getName());

	private static class ActivityTimeEnd implements Runnable
	{
		@Override
		public void run()
		{
			sleep();
		}
	}

	private static class CubeSpawn implements Runnable
	{
		@Override
		public void run()
		{
			_teleportCube = Functions.spawn(new Location(27734, -6838, -1982, 0), TeleportCubeId);
		}
	}

	private static class IntervalEnd implements Runnable
	{
		@Override
		public void run()
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();
		}
	}

	private static class Social implements Runnable
	{
		private int _action;
		private L2NpcInstance _npc;

		public Social(L2NpcInstance npc, int actionId)
		{
			_npc = npc;
			_action = actionId;
		}

		@Override
		public void run()
		{
			_npc.broadcastPacket(new SocialAction(_npc.getObjectId(), _action));
		}
	}

	private static class onAnnihilated implements Runnable
	{
		@Override
		public void run()
		{
			sleep();
		}
	}

	private static class SailrenSpawn implements Runnable
	{
		private int _npcId;
		private final Location _pos = new Location(27628, -6109, -1982, 44732);

		SailrenSpawn(int npcId)
		{
			_npcId = npcId;
		}

		@Override
		public void run()
		{
			if(_socialTask != null)
			{
				_socialTask.cancel(false);
				_socialTask = null;
			}

			switch (_npcId)
			{
				case Velociraptor:
					_velociraptor = Functions.spawn(new Location(27852, -5536, -1983, 44732), Velociraptor);
					_velociraptor.getAI().addTaskMove(_pos);

					if(_socialTask != null)
					{
						_socialTask.cancel(true);
						_socialTask = null;
					}
					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new Social(_velociraptor, 2), 6000);
					if(_activityTimeEndTask != null)
					{
						_activityTimeEndTask.cancel(true);
						_activityTimeEndTask = null;
					}
					_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWS_ACTIVITYTIMEOFMOBS);
					break;
				case Pterosaur:
					_pterosaur = Functions.spawn(new Location(27852, -5536, -1983, 44732), Pterosaur);
					_pterosaur.getAI().addTaskMove(_pos);
					if(_socialTask != null)
					{
						_socialTask.cancel(true);
						_socialTask = null;
					}
					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new Social(_pterosaur, 2), 6000);
					if(_activityTimeEndTask != null)
					{
						_activityTimeEndTask.cancel(true);
						_activityTimeEndTask = null;
					}
					_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWS_ACTIVITYTIMEOFMOBS);
					break;
				case Tyrannosaurus:
					_tyranno = Functions.spawn(new Location(27852, -5536, -1983, 44732), Tyrannosaurus);
					_tyranno.getAI().addTaskMove(_pos);
					if(_socialTask != null)
					{
						_socialTask.cancel(true);
						_socialTask = null;
					}
					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new Social(_tyranno, 2), 6000);
					if(_activityTimeEndTask != null)
					{
						_activityTimeEndTask.cancel(true);
						_activityTimeEndTask = null;
					}
					_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWS_ACTIVITYTIMEOFMOBS);
					break;
				case Sailren:
					_sailren = Functions.spawn(new Location(27810, -5655, -1983, 44732), Sailren);

					_state.setRespawnDate(getRespawnInterval() + Config.FWS_ACTIVITYTIMEOFMOBS);
					_state.setState(EpicBossState.State.ALIVE);
					_state.update();

					_sailren.setRunning();
					_sailren.getAI().addTaskMove(_pos);
					if(_socialTask != null)
					{
						_socialTask.cancel(true);
						_socialTask = null;
					}
					_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new Social(_sailren, 2), 6000);
					if(_activityTimeEndTask != null)
					{
						_activityTimeEndTask.cancel(true);
						_activityTimeEndTask = null;
					}
					_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWS_ACTIVITYTIMEOFMOBS);
					break;
			}
		}
	}

	private static L2NpcInstance _velociraptor;
	private static L2NpcInstance _pterosaur;
	private static L2NpcInstance _tyranno;
	private static L2NpcInstance _sailren;
	private static L2NpcInstance _teleportCube;

	// Tasks.
	private static ScheduledFuture<CubeSpawn> _cubeSpawnTask = null;
	private static ScheduledFuture<SailrenSpawn> _monsterSpawnTask = null;
	private static ScheduledFuture<IntervalEnd> _intervalEndTask = null;
	private static ScheduledFuture<Social> _socialTask = null;
	private static ScheduledFuture<ActivityTimeEnd> _activityTimeEndTask = null;

	private static final int Sailren = 29065;
	private static final int Velociraptor = 22198;
	private static final int Pterosaur = 22199;
	private static final int Tyrannosaurus = 22217;
	private static final int TeleportCubeId = 32107;

	private static EpicBossState _state;
	private static L2Zone _zone;

	private static boolean _isAlreadyEnteredOtherParty = false;

	private static boolean Dying = false;

	private static void banishForeigners()
	{
		for(L2Player player : getPlayersInside())
			player.teleToClosestTown();
	}

	private static void checkAnnihilated()
	{
		if(isPlayersAnnihilated())
			L2GameThreadPools.getInstance().scheduleGeneral(new onAnnihilated(), 5000);
	}

	private static GArray<L2Player> getPlayersInside()
	{
		return getZone().getInsidePlayers();
	}

	private static int getRespawnInterval()
	{
		return Config.FWS_FIXINTERVALOFSAILRENSPAWN + Rnd.get(Config.FWS_RANDOMINTERVALOFSAILRENSPAWN);
	}

	public static L2Zone getZone()
	{
		return _zone;
	}

	public static void init()
	{
		_state = new EpicBossState(Sailren);
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.epic, 702004, false);

		_log.info("EpicBossManager: SailrenManager state - " + _state.getState() + ".");
		if(!_state.isNotSpawn())
			setIntervalEndTask();

		// Выводим только если есть дата следующего спауна
		if(_state.isInterval())
			_log.info("EpicBossManager: SailrenManager next spawn - " + Util.datetimeFormatter.format(_state.getRespawnDate()) + ".");
	}

	private static synchronized boolean isPlayersAnnihilated()
	{
		for(L2Player pc : getPlayersInside())
			if(!pc.isDead())
				return false;
		return true;
	}

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(self == null)
			return;
		if(self.isPlayer() && _state != null && _state.getState() == State.ALIVE && _zone != null && _zone.checkIfInZone(self.getX(), self.getY()))
			checkAnnihilated();
		else if(self != null)
			if(self == _velociraptor)
			{
				if(_monsterSpawnTask != null)
					_monsterSpawnTask.cancel(false);
				_monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SailrenSpawn(Pterosaur), Config.FWS_INTERVALOFNEXTMONSTER);
			}
			else if(self == _pterosaur)
			{
				if(_monsterSpawnTask != null)
					_monsterSpawnTask.cancel(false);
				_monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SailrenSpawn(Tyrannosaurus), Config.FWS_INTERVALOFNEXTMONSTER);
			}
			else if(self == _tyranno)
			{
				if(_monsterSpawnTask != null)
					_monsterSpawnTask.cancel(false);
				_monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SailrenSpawn(Sailren), Config.FWS_INTERVALOFNEXTMONSTER);
			}
			else if(self == _sailren)
				onSailrenDie(killer);
	}

	private static void onSailrenDie(L2Character killer)
	{
		if(Dying)
			return;

		Dying = true;
		_state.setRespawnDate(getRespawnInterval());
		_state.setState(EpicBossState.State.INTERVAL);
		_state.update();

		Log.add("Sailren died", "bosses");

		_cubeSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new CubeSpawn(), 10000);
	}

	// Start interval.
	private static void setIntervalEndTask()
	{
		setUnspawn();

		// init state of Sailren lair.
		if(_state.isAlive())
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
			return;
		}

		if(!_state.isInterval())
		{
			_state.setRespawnDate(getRespawnInterval());
			_state.setState(State.INTERVAL);
			_state.update();
		}

		_intervalEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new IntervalEnd(), _state.getInterval());
	}

	private static void setUnspawn()
	{
		banishForeigners();

		if(_velociraptor != null)
		{
			_velociraptor.deleteMe();
			_velociraptor = null;
		}
		if(_pterosaur != null)
		{
			_pterosaur.deleteMe();
			_pterosaur = null;
		}
		if(_tyranno != null)
		{
			_tyranno.deleteMe();
			_tyranno = null;
		}
		if(_sailren != null)
		{
			_sailren.deleteMe();
			_sailren = null;
		}
		if(_teleportCube != null)
		{
			_teleportCube.deleteMe();
			_teleportCube = null;
		}
		if(_cubeSpawnTask != null)
		{
			_cubeSpawnTask.cancel(false);
			_cubeSpawnTask = null;
		}
		if(_monsterSpawnTask != null)
		{
			_monsterSpawnTask.cancel(false);
			_monsterSpawnTask = null;
		}
		if(_intervalEndTask != null)
		{
			_intervalEndTask.cancel(false);
			_intervalEndTask = null;
		}
		if(_socialTask != null)
		{
			_socialTask.cancel(false);
			_socialTask = null;
		}
		if(_activityTimeEndTask != null)
		{
			_activityTimeEndTask.cancel(true);
			_activityTimeEndTask = null;
		}
	}

	private static void sleep()
	{
		setUnspawn();
		if(_state.getState().equals(EpicBossState.State.ALIVE))
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();
		}
	}

	public synchronized static void setSailrenSpawnTask()
	{
		if(_monsterSpawnTask == null)
			_monsterSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SailrenSpawn(Velociraptor), Config.FWS_INTERVALOFNEXTMONSTER);
	}

	public static boolean isEnableEnterToLair()
	{
		return _state.getState() == EpicBossState.State.NOTSPAWN;
	}

	public static int canIntoSailrenLair(L2Player pc)
	{
		if(pc.isGM())
			return 0;
		if(!Config.FWS_ENABLESINGLEPLAYER && !pc.isInParty())
			return 4;
		else if(_isAlreadyEnteredOtherParty)
			return 2;
		else if(_state.getState().equals(EpicBossState.State.NOTSPAWN))
			return 0;
		else if(_state.getState().equals(EpicBossState.State.ALIVE) || _state.getState().equals(EpicBossState.State.DEAD))
			return 1;
		else if(_state.getState().equals(EpicBossState.State.INTERVAL))
			return 3;
		else
			return 0;
	}

	public static void entryToSailrenLair(L2Player pc)
	{
		if(pc.getParty() == null)
			pc.teleToLocation(new Location(27734, -6938, -1982).rnd(0, 80, false));
		else
		{
			GArray<L2Player> members = new GArray<L2Player>();
			for(L2Player mem : pc.getParty().getPartyMembers())
				if(mem != null && !mem.isDead() && mem.isInRange(pc, 1000))
					members.add(mem);
			for(L2Player mem : members)
				mem.teleToLocation(new Location(27734, -6938, -1982).rnd(0, 80, false));
		}
		_isAlreadyEnteredOtherParty = true;
	}

	public static final EpicBossState getState()
	{
		return _state;
	}
}
