package l2n.game.instancemanager.boss;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.EpicBossState.State;
import l2n.game.model.L2ObjectProcedures;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2BossInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;

public class BaiumManager
{

	private static class CallArchAngel implements Runnable
	{
		@Override
		public void run()
		{
			for(L2Spawn spawn : _angelSpawns)
				_angels.add(spawn.doSpawn(true));
		}
	}

	private static class CheckLastAttack implements Runnable
	{
		@Override
		public void run()
		{
			if(_state.getState() == State.ALIVE)
				if(_lastAttackTime + Config.FWB_LIMITUNTILSLEEP < System.currentTimeMillis())
					sleepBaium();
				else
					_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 60000);
		}
	}

	private static class CubeSpawn implements Runnable
	{
		@Override
		public void run()
		{
			_teleportCube = _teleportCubeSpawn.doSpawn(true);
		}
	}

	private static class EarthquakeTask implements Runnable
	{
		private final L2BossInstance baium;

		public EarthquakeTask(L2BossInstance _baium)
		{
			baium = _baium;
		}

		@Override
		public void run()
		{
			Earthquake eq = new Earthquake(baium.getLoc(), 40, 5);
			baium.broadcastPacket(eq);
		}
	}

	private static class IntervalEnd implements Runnable
	{
		@Override
		public void run()
		{
			_state.setState(EpicBossState.State.NOTSPAWN);
			_state.update();

			_statueSpawn.doSpawn(true);
		}
	}

	private static class KillPc implements Runnable
	{
		private L2BossInstance _boss;
		private L2Player _target;

		public KillPc(L2Player target, L2BossInstance boss)
		{
			_target = target;
			_boss = boss;
		}

		@Override
		public void run()
		{
			if(_target != null && !_target.isDead())
			{
				_boss.broadcastPacket(new MagicSkillUse(_boss, _target, 4136, 1, 2000, 0));
				_target.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RECEIVED_DAMAGE_OF_S3_FROM_S2).addName(_target).addName(_boss).addNumber((int) _target.getCurrentHp()));
				_target.doDie(_boss);
			}
		}
	}
	
	private static class MoveAtRandom implements Runnable
	{
		private L2NpcInstance _npc;
		private Location _pos;

		public MoveAtRandom(L2NpcInstance npc, Location pos)
		{
			_npc = npc;
			_pos = pos;
		}

		@Override
		public void run()
		{
			if(_npc.getAI().getIntention() == AI_INTENTION_ACTIVE)
				_npc.moveToLocation(_pos, 0, false);
		}
	}

	private static class onAnnihilated implements Runnable
	{
		@Override
		public void run()
		{
			sleepBaium();
		}
	}

	private static class SetMobilised implements Runnable
	{
		private L2BossInstance _boss;

		public SetMobilised(L2BossInstance boss)
		{
			_boss = boss;
		}

		@Override
		public void run()
		{
			_boss.setImmobilized(false);
		}
	}

	private static class Social implements Runnable
	{
		private int _action;
		private L2NpcInstance _npc;

		public Social(L2NpcInstance npc, int actionId)
		{
			_npc = npc;
			_action = actionId;
		}

		@Override
		public void run()
		{
			SocialAction sa = new SocialAction(_npc.getObjectId(), _action);
			_npc.broadcastPacket(sa);
		}
	}

	private static GArray<L2NpcInstance> _angels = new GArray<L2NpcInstance>();
	private static GArray<L2Spawn> _angelSpawns = new GArray<L2Spawn>();
	private static ScheduledFuture<?> _callAngelTask = null;
	private static ScheduledFuture<?> _cubeSpawnTask = null;
	private static ScheduledFuture<?> _intervalEndTask = null;
	private static ScheduledFuture<?> _killPcTask = null;
	private static ScheduledFuture<?> _mobiliseTask = null;
	private static ScheduledFuture<?> _moveAtRandomTask = null;
	private static ScheduledFuture<?> _onAnnihilatedTask = null;
	private static ScheduledFuture<?> _sleepCheckTask = null;
	private static ScheduledFuture<?> _socialTask = null;
	private static ScheduledFuture<?> _socialTask2 = null;
	private static ScheduledFuture<ActivityTimeEnd> _activityTimeEndTask = null;

	private static long _lastAttackTime = 0;
	private static final Logger _log = Logger.getLogger(BaiumManager.class.getName());

	private static GArray<L2NpcInstance> _monsters = new GArray<L2NpcInstance>();

	private static FastMap<Integer, L2Spawn> _monsterSpawn = new FastMap<Integer, L2Spawn>();

	private static L2NpcInstance _npcBaium;

	private static EpicBossState _state;

	private static L2Spawn _statueSpawn = null;
	private static L2NpcInstance _teleportCube = null;
	private static L2Spawn _teleportCubeSpawn = null;
	private static L2Zone _zone;

	private final static Location[] ANGEL_LOCATION = new Location[] { new Location(113004, 16209, 10076, 60242), new Location(114053, 16642, 10076, 4411),
			new Location(114563, 17184, 10076, 49241), new Location(116356, 16402, 10076, 31109), new Location(115015, 16393, 10076, 32760), new Location(115481, 15335, 10076, 16241),
			new Location(114680, 15407, 10051, 32485), new Location(114886, 14437, 10076, 16868), new Location(115391, 17593, 10076, 55346), new Location(115245, 17558, 10076, 35536) };

	private final static int ARCHANGEL = 29021;
	private final static int BAIUM = 29020;
	private final static int BAIUM_NPC = 29025;

	private final static Location CUBE_LOCATION = new Location(115203, 16620, 10078, 0);
	private final static Location STATUE_LOCATION = new Location(115996, 17417, 10106, 41740);

	private static boolean Dying = false;

	private final static int TELEPORT_CUBE = 31759;

	private static void banishForeigners()
	{
		for(L2Player player : getPlayersInside())
			player.teleToClosestTown();
	}

	private static void checkAnnihilated()
	{
		if(_onAnnihilatedTask == null && isPlayersAnnihilated())
			_onAnnihilatedTask = L2GameThreadPools.getInstance().scheduleGeneral(new onAnnihilated(), 5000);
	}

	private static void deleteArchangels()
	{
		_angels.forEach(L2ObjectProcedures.PROC_DELETE_NPC);
		_angels.clear();
	}

	private static GArray<L2Player> getPlayersInside()
	{
		return getZone().getInsidePlayersIncludeZ();
	}

	private static int getRespawnInterval()
	{
		return Rnd.get(Config.FWB_FIXINTERVALOFBAIUM, Config.FWB_FIXINTERVALOFBAIUM + Config.FWB_RANDOMINTERVALOFBAIUM);
	}

	public static L2Zone getZone()
	{
		return _zone;
	}

	public static void init()
	{
		_state = new EpicBossState(BAIUM);
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.epic, 702001, false);

		try
		{
			L2Spawn tempSpawn;

			_statueSpawn = new L2Spawn(NpcTable.getTemplate(BAIUM_NPC));
			_statueSpawn.setAmount(1);
			_statueSpawn.setLoc(STATUE_LOCATION);
			_statueSpawn.stopRespawn();

			tempSpawn = new L2Spawn(NpcTable.getTemplate(BAIUM));
			tempSpawn.setAmount(1);
			_monsterSpawn.put(BAIUM, tempSpawn);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			L2NpcTemplate Cube = NpcTable.getTemplate(TELEPORT_CUBE);
			_teleportCubeSpawn = new L2Spawn(Cube);
			_teleportCubeSpawn.setAmount(1);
			_teleportCubeSpawn.setLoc(CUBE_LOCATION);
			_teleportCubeSpawn.setRespawnDelay(60);
			_teleportCubeSpawn.setLocation(0);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		try
		{
			L2NpcTemplate angel = NpcTable.getTemplate(ARCHANGEL);
			L2Spawn spawnDat;
			_angelSpawns.clear();

			GArray<Integer> random = new GArray<Integer>();
			for(int i = 0; i < 5; i++)
			{
				int r = -1;
				while (r == -1 || random.contains(r))
					r = Rnd.get(10);
				random.add(r);
			}

			for(int i : random)
			{
				spawnDat = new L2Spawn(angel);
				spawnDat.setAmount(1);
				spawnDat.setLoc(ANGEL_LOCATION[i]);
				spawnDat.setRespawnDelay(300000);
				spawnDat.setLocation(0);
				_angelSpawns.add(spawnDat);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		_log.info("EpicBossManager: Baium state - " + _state.getState() + ".");

		if(_state.getState().equals(State.NOTSPAWN))
			_statueSpawn.doSpawn(true);
		else if(_state.getState().equals(State.ALIVE))
		{
			_state.setState(State.NOTSPAWN);
			_state.update();
			_statueSpawn.doSpawn(true);
		}
		else if(_state.getState().equals(State.INTERVAL) || _state.getState().equals(State.DEAD))
			setIntervalEndTask();

		if(_state.isInterval())
			_log.info("EpicBossManager: Baium next spawn date - " + Util.datetimeFormatter.format(_state.getRespawnDate()));
	}

	private static synchronized boolean isPlayersAnnihilated()
	{
		for(L2Player pc : getPlayersInside())
			if(!pc.isDead() || pc.isGM())
				return false;
		return true;
	}

	public static void onBaiumDie(L2Character self)
	{
		if(Dying)
			return;

		Dying = true;
		self.broadcastPacket(new PlaySound(1, "BS02_D", 1, 0, self.getLoc()));
		_state.setRespawnDate(getRespawnInterval());
		_state.setState(State.INTERVAL);
		_state.update();

		Log.add("Baium died", "bosses");

		deleteArchangels();

		_cubeSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new CubeSpawn(), 10000);
	}

	public static void OnDie(L2Character self, L2Character killer)
	{
		if(self == null)
			return;
		if(self.isPlayer() && _state != null && _state.getState() == State.ALIVE && _zone != null && _zone.checkIfInZone(self))
			checkAnnihilated();
		else if(self.isNpc() && self.getNpcId() == BAIUM)
			onBaiumDie(self);
	}

	private static void setIntervalEndTask()
	{
		setUnspawn();

		if(!_state.getState().equals(State.INTERVAL))
		{
			_state.setRespawnDate(getRespawnInterval());
			_state.setState(State.INTERVAL);
			_state.update();
		}

		_intervalEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new IntervalEnd(), _state.getInterval());
	}

	public static void setLastAttackTime()
	{
		_lastAttackTime = System.currentTimeMillis();
	}

	private static class ActivityTimeEnd implements Runnable
	{
		@Override
		public void run()
		{
			setUnspawn();
		}
	}

	public static void setUnspawn()
	{
		banishForeigners();

		deleteArchangels();
		_monsters.forEach(L2ObjectProcedures.PROC_DELETE_NPC);
		_monsters.clear();

		if(_teleportCube != null)
		{
			_teleportCube.getSpawn().stopRespawn();
			_teleportCube.deleteMe();
			_teleportCube = null;
		}

		if(_cubeSpawnTask != null)
		{
			_cubeSpawnTask.cancel(true);
			_cubeSpawnTask = null;
		}
		if(_intervalEndTask != null)
		{
			_intervalEndTask.cancel(true);
			_intervalEndTask = null;
		}
		if(_socialTask != null)
		{
			_socialTask.cancel(true);
			_socialTask = null;
		}
		if(_mobiliseTask != null)
		{
			_mobiliseTask.cancel(true);
			_mobiliseTask = null;
		}
		if(_moveAtRandomTask != null)
		{
			_moveAtRandomTask.cancel(true);
			_moveAtRandomTask = null;
		}
		if(_socialTask2 != null)
		{
			_socialTask2.cancel(true);
			_socialTask2 = null;
		}
		if(_killPcTask != null)
		{
			_killPcTask.cancel(true);
			_killPcTask = null;
		}
		if(_callAngelTask != null)
		{
			_callAngelTask.cancel(true);
			_callAngelTask = null;
		}
		if(_sleepCheckTask != null)
		{
			_sleepCheckTask.cancel(true);
			_sleepCheckTask = null;
		}
		if(_onAnnihilatedTask != null)
		{
			_onAnnihilatedTask.cancel(false);
			_onAnnihilatedTask = null;
		}
		if(_activityTimeEndTask != null)
		{
			_activityTimeEndTask.cancel(true);
			_activityTimeEndTask = null;
		}
	}

	private static void sleepBaium()
	{
		setUnspawn();
		Log.add("Baium going to sleep, spawning statue", "bosses");
		_state.setState(State.NOTSPAWN);
		_state.update();

		_statueSpawn.doSpawn(true);
	}

	public static boolean isEnableEnter()
	{
		return _state.isNotSpawn();
	}

	public static void spawnBaium(L2NpcInstance NpcBaium, L2Player awake_by)
	{
		Dying = false;
		_npcBaium = NpcBaium;

		L2Spawn baiumSpawn = _monsterSpawn.get(BAIUM);
		baiumSpawn.setLoc(_npcBaium.getLoc());

		_npcBaium.getSpawn().stopRespawn();
		_npcBaium.deleteMe();

		final L2BossInstance baium = (L2BossInstance) baiumSpawn.doSpawn(true);
		_monsters.add(baium);

		_state.setRespawnDate(getRespawnInterval());
		_state.setState(State.ALIVE);
		_state.update();

		Log.add("Spawned Baium, awake by: " + awake_by, "bosses");

		setLastAttackTime();

		baium.setImmobilized(true);
		baium.broadcastPacket(new PlaySound(1, "BS02_A", 1, 0, baium.getLoc()));
		L2GameThreadPools.getInstance().scheduleGeneral(new Social(baium, 2), 100);

		_socialTask = L2GameThreadPools.getInstance().scheduleGeneral(new Social(baium, 3), 15000);

		L2GameThreadPools.getInstance().scheduleGeneral(new EarthquakeTask(baium), 25000);

		_socialTask2 = L2GameThreadPools.getInstance().scheduleGeneral(new Social(baium, 1), 25000);
		_killPcTask = L2GameThreadPools.getInstance().scheduleGeneral(new KillPc(awake_by, baium), 26000);
		_callAngelTask = L2GameThreadPools.getInstance().scheduleGeneral(new CallArchAngel(), 35000);
		_mobiliseTask = L2GameThreadPools.getInstance().scheduleGeneral(new SetMobilised(baium), 35500);

		Location pos = new Location(Rnd.get(112826, 116241), Rnd.get(15575, 16375), 10078, 0);
		_moveAtRandomTask = L2GameThreadPools.getInstance().scheduleGeneral(new MoveAtRandom(baium, pos), 36000);

		_sleepCheckTask = L2GameThreadPools.getInstance().scheduleGeneral(new CheckLastAttack(), 600000);

		_activityTimeEndTask = L2GameThreadPools.getInstance().scheduleGeneral(new ActivityTimeEnd(), Config.FWB_ACTIVITYTIMEOFBAIUM);
	}

	public static final EpicBossState getState()
	{
		return _state;
	}
}
