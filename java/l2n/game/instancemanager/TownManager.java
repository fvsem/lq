package l2n.game.instancemanager;

import l2n.commons.list.GArray;
import l2n.game.model.L2Object;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.Town;
import l2n.game.tables.MapRegionTable;

import java.util.logging.Logger;

public final class TownManager
{
	private static TownManager _instance;
	private GArray<Town> _towns = new GArray<Town>();

	private final static Logger _log = Logger.getLogger(TownManager.class.getName());

	private TownManager()
	{
		GArray<L2Zone> zones = ZoneManager.getInstance().getZoneByType(ZoneType.Town);
		if(zones.size() == 0)
			_log.warning("Not found zones for Towns!!!");
		else
			for(L2Zone zone : zones)
				_towns.add(new Town(zone.getIndex()));
	}

	public static TownManager getInstance()
	{
		if(_instance == null)
			_instance = new TownManager();
		return _instance;
	}

	public Town getClosestTown(int x, int y)
	{
		return getTown(MapRegionTable.getInstance().getMapRegion(x, y));
	}

	public Town getClosestTown(L2Object activeObject)
	{
		return getTown(MapRegionTable.getInstance().getMapRegion(activeObject.getX(), activeObject.getY()));
	}

	public int getClosestTownNumber(L2Character activeChar)
	{
		return MapRegionTable.getInstance().getMapRegion(activeChar.getX(), activeChar.getY());
	}

	public String getClosestTownName(L2Character activeChar)
	{
		return getClosestTown(activeChar).getName();
	}

	public Town getTown(int townId)
	{
		for(Town town : _towns)
			if(town.getTownId() == townId)
				return town;
		return null;
	}
}
