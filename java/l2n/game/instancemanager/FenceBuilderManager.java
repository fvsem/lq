package l2n.game.instancemanager;

import javolution.util.FastMap;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2FenceInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;

/**
 * Создание заборов
 * 
 * @author L2System Project
 * @date 07.10.2010
 * @time 14:04:06
 */
public class FenceBuilderManager
{
	private static FastMap<Integer, L2FenceInstance> _fences = new FastMap<Integer, L2FenceInstance>();
	private static FastMap<Integer, L2FenceInstance> _fence = new FastMap<Integer, L2FenceInstance>();

	public static void main_fence(L2Player pc)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(5);
		StringBuilder sb = new StringBuilder();
		sb.append("<html><title>Fence Builder</title><body>");
		sb.append("<table width=270>");
		sb.append("<tr></tr>");
		sb.append("<tr><td>New Fence:</td></tr>");
		sb.append("<tr><td>Type: </td></tr>");
		sb.append("<tr><td><combobox width=75 var=ts list=0;1;2></td></tr>");
		sb.append("<tr><td>Width: (min 150)</td></tr>");
		sb.append("<tr><td><td><edit var=\"tdist\"></td></tr>");
		sb.append("<tr><td>Length: (min 150)</td></tr>");
		sb.append("<tr><td><td><edit var=\"tyaw\"></td></tr>");
		sb.append("<tr><td>Height: </td></tr>");
		sb.append("<tr><td><td><combobox width=75 var=tscreen list=1;2;3></td></tr>");
		sb.append("<tr>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("<BR>");
		sb.append("<BR>");
		sb.append("<table width=270>");
		sb.append("<tr>");
		sb.append("<td><button value=\"Spawn Fence\" width=100 action=\"bypass -h admin_fence $ts $tdist $tyaw $tscreen\" height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("<td><button value=\"Delete Last Created\" width=150 action=\"bypass -h admin_dellastspawned\" height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		sb.append("<td><button value=\"Delete All Fences\" width=150 action=\"bypass -h admin_delallspawned\" height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		sb.append("</tr>");
		sb.append("<tr>");
		sb.append("</tr>");
		sb.append("</table></body></html>");
		html.setHtml(sb.toString());
		pc.sendPacket(html);
	}

	public static void spawn_fence(L2Player pc, int type, int width, int length, int height)
	{
		_fence.clear();
		for(int i = 0; i < height; i++)
		{
			int id = IdFactory.getInstance().getNextId();
			L2FenceInstance inst = new L2FenceInstance(id, type, width, length);
			inst.spawnMe(pc.getLoc());
			_fences.put(id, inst);
			_fence.put(id, inst);
		}
		main_fence(pc);
	}

	public static void del_last(L2Player pc)
	{
		if(!_fence.isEmpty())
			for(L2FenceInstance f : _fence.values())
				if(f != null)
				{
					f.deleteMe();
					_fences.remove(f);
					_fence.remove(f);
				}
		main_fence(pc);
	}

	public static void del_all(L2Player pc)
	{
		if(!_fences.isEmpty())
			for(L2FenceInstance f : _fences.values())
				if(f != null)
				{
					f.deleteMe();
					_fences.remove(f);
					_fence.clear();
				}
		main_fence(pc);
	}
}
