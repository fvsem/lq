package l2n.game.instancemanager;

import l2n.Config;
import l2n.commons.sql.SqlBatch;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.Inventory;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.StringUtil;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <span> Этапы цикла Семени Бессмертия </span></h3>
 * <p>
 * Всего в цикле Семени Бессмертия 5 этапов. Во время каждого из них доступны разные временные зоны и квесты. Переход с одного этапа на другой зависит от того сколько раз игроки проходили временные зоны Семени.
 * </p>
 * <table border="1">
 * <tr>
 * <th>Этап цикла</th>
 * <th>Открытые зоны</th>
 * <th>Факторы перехода на следующий этап</th>
 * <th>Факторы перехода на предыдущий этап</th>
 * </tr>
 * <tr>
 * <td>1 этап</td>
 * <td>Зал Страданий (захват)<br />
 * Зал Гибели (захват)</td>
 * <td>Удачный захват Зала Гибели</td>
 * <td></td>
 * </tr>
 * <tr>
 * <td>2 этап</td>
 * <td>Зал Страданий (захват)<br />
 * Источник Бессмертия (захват)</td>
 * <td>Удачный захват Источника Бессмертия</td>
 * <td></td>
 * </tr>
 * <tr>
 * <td>3 этап</td>
 * <td>Доступны все зоны (стабильное состояние)</td>
 * <td>
 * <ul>
 * <li>Сбор Кристаллов Аттрибута</li>
 * <li>Этап закончится через 48 часов</li>
 * </ul>
 * </td>
 * <td></td>
 * </tr>
 * <tr>
 * <td>4 этап</td>
 * <td>Зал Страданий (защита)<br />
 * Зал Гибели (защита)<br />
 * Источник Бессмертия (стабильное состояние)</td>
 * <td>
 * <ul>
 * <li>Сбор Кристаллов Аттрибута</li>
 * <li>Этап закончится через 48 часов</li>
 * </ul>
 * </td>
 * <td>Удачная защита Зала Гибели</td>
 * </tr>
 * <tr>
 * <td>5 этап</td>
 * <td>Зал Страданий (защита)<br />
 * Источник Бессмертия (защита)</td>
 * <td>
 * <ul>
 * <li>Сбор Кристаллов Аттрибута</li>
 * <li>Этап закончится через 48 часов</li>
 * </ul>
 * </td>
 * <td>Удачная защита Источника Бессмертия</td>
 * </tr>
 * </table>
 *
 */
public class SeedOfInfinityManager
{
    private static final Logger _log = Logger.getLogger(SeedOfInfinityManager.class.getName());

    // vars
    private static final String NEXT_CYCLE_TIME = "SeedOfInfinity_nextCycleTime";
    private static final String ATTACK_POINTS = "SeedOfInfinity_attackPoints";
    private static final String CYCLE = "SeedOfInfinity_cycle";

    private static ScheduledFuture<TaskSwitchCycle> taskSwitchCycle = null;

    private static final int ONE_DAY = 24 * 60 * 60 * 1000;
    private static final int ONE_HOUR = 60 * 60 * 1000;
    private static final int ONE_MINUTE = 60 * 1000;

    private static final int KECERUS_MARK_1 = 13691;
    private static final int KECERUS_MARK_2 = 13692;

    private static final ArrayList<L2NpcInstance> NPCS = new ArrayList<L2NpcInstance>();

    private static final Location MOUTH_LOCS[] = {
            new Location(-183002, 205944, -12896),
            new Location(-183586, 205947, -12896),
            new Location(-183589, 206510, -12896),
            new Location(-182996, 206510, -12896) };

    /** [0]count, [1]npc_id, [2]respawn, [3]respawn_rnd, [4]loc_id */
    private static final int[][] seeds_spawnlist = {
            // count, npc_id, respawn, respawn_rnd, loc_id
            { 1, 18682, 1200, 600, 14003 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14003 }, // seed_energe_unholy
            { 1, 18682, 1800, 600, 14004 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14004 }, // seed_energe_unholy
            { 1, 18682, 1800, 600, 14005 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14005 }, // seed_energe_unholy
            { 1, 18682, 1800, 600, 14006 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14006 }, // seed_energe_unholy
            { 1, 18682, 1800, 600, 14007 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14007 }, // seed_energe_unholy
            { 1, 18682, 1800, 600, 14008 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14008 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14009 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14009 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14009 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14009 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14010 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14010 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14010 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14010 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14011 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14011 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14011 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14011 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14012 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14012 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14012 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14012 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14013 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14013 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14013 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14013 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14014 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14014 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14014 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14014 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14015 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14015 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14015 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14015 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14016 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14016 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14016 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14016 }, // seed_energe_earth
            { 1, 18678, 1800, 600, 14017 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14017 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14017 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14017 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14018 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14018 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14018 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14018 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14018 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14018 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14019 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14019 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14019 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14019 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14019 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14019 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14020 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14020 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14020 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14020 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14020 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14020 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14021 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14021 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14021 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14021 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14021 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14021 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14022 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14022 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14022 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14022 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14022 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14022 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14023 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14023 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14023 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14023 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14023 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14023 }, // seed_energe_earth
            { 1, 18682, 1800, 600, 14024 }, // seed_energe_holy
            { 1, 18683, 1800, 600, 14024 }, // seed_energe_unholy
            { 1, 18678, 1800, 600, 14024 }, // seed_energe_water
            { 1, 18679, 1800, 600, 14024 }, // seed_energe_fire
            { 1, 18680, 1800, 600, 14024 }, // seed_energe_wind
            { 1, 18681, 1800, 600, 14024 } // seed_energe_earth
    };

    private static final ArrayList<L2Spawn> seed_energies = new ArrayList<L2Spawn>();

    private final static Location[] gather_locations = {
            new Location(-179537, 209551, -15504),
            new Location(-179779, 212540, -15520),
            new Location(-177028, 211135, -15520),
            new Location(-176355, 208043, -15520),
            new Location(-179284, 205990, -15520),
            new Location(-182268, 208218, -15520),
            new Location(-182069, 211140, -15520),
            new Location(-176036, 210002, -11948),
            new Location(-176039, 208203, -11949),
            new Location(-183288, 208205, -11939),
            new Location(-183290, 210004, -11939),
            new Location(-187776, 205696, -9536),
            new Location(-186327, 208286, -9536),
            new Location(-184429, 211155, -9536),
            new Location(-182811, 213871, -9504),
            new Location(-180921, 216789, -9536),
            new Location(-177264, 217760, -9536),
            new Location(-173727, 218169, -9536) };

    public static void init()
    {
        long nextCycleTime = ServerVariables.getLong(NEXT_CYCLE_TIME, 0);
        if(getCurrentCycle() > 1)
        {
            if(nextCycleTime < System.currentTimeMillis())
            {
                if(getCurrentCycle() >= 5)
                    taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(1), 500);
                else
                    taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(getCurrentCycle() + 1), 500);
            }
            else if(getCurrentCycle() >= 5)
                taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(1), nextCycleTime - System.currentTimeMillis());
            else
            {
                switchCycle(getCurrentCycle());
                taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(getCurrentCycle() + 1), nextCycleTime - System.currentTimeMillis());
            }

            Date dt = new Date(ServerVariables.getLong(NEXT_CYCLE_TIME, 0));
            _log.info("SeedOfInfinityManager: cycle: " + getCurrentCycle() + " / next switch: " + Util.datetimeFormatter.format(dt));
        }
        spawnNpcs(getCurrentCycle());
    }

    public static void addEkimusKill()
    {
        if(getCurrentCycle() == 2)
        {
            addAttackPoints(1);
            if(getAttackPoints() >= 5)
                checkCurrentCycle();
        }
    }

    public static void addAttackSuffering()
    {
        if(getCurrentCycle() == 1)
        {
            addAttackPoints(1);
            checkCurrentCycle();
        }
    }

    public static void addAttackErrosion()
    {
        if(getCurrentCycle() == 1)
        {
            addAttackPoints(2);
            checkCurrentCycle();
        }
    }

    private static int getAttackPoints()
    {
        return ServerVariables.getInt(ATTACK_POINTS, 0);
    }

    private static void addAttackPoints(int n)
    {
        setAttackPoints(n + getAttackPoints());
    }

    private static void setAttackPoints(int n)
    {
        ServerVariables.set(ATTACK_POINTS, n);
    }

    public static int getCurrentCycle()
    {
        return Math.min(5, ServerVariables.getInt(CYCLE, 1));
    }

    private static void setCurrentCycle(int cycle)
    {
        ServerVariables.set(CYCLE, cycle);
        switchCycle(cycle);
    }

    private static void switchCycle(int cycle)
    {
        switch (cycle)
        {
            case 1:
                clearItems();
                deleteEnergies();
                break;
            case 3:
                spawnEnergies();
                break;
            case 4:
                spawnEnergies();
                break;
            case 5:
                deleteEnergies();
                break;
        }
        spawnNpcs(cycle);
    }

    private static void checkCurrentCycle()
    {
        switch (getCurrentCycle())
        {
            case 1:
            {
                // 1.Идет 1-ая осада семени бессмертия.
                // На этом этапе можно убивать братьев и кохименеса. Для того, чтобы перейти на следующий этап - нужно убить ~10 кохименесов за промежуток времени (вероятнее всего неделя).

                if(getAttackPoints() >= Config.POINTS_OPEN_2_CYCLE)
                {
                    setCurrentCycle(2);
                    setAttackPoints(0);
                }
                break;
            }
            case 2:
            {

                // 2.Идет 2-ая осада семени бессмертия
                // Здесь можно убивать только экимуса. Сид перейдет на следующию стадию автоматически через 12 часов после открытия

                int wait_time = Rnd.get(Config.SOF_STAGE_2_FROM * ONE_MINUTE, Config.SOF_STAGE_2_TO * ONE_MINUTE);
                ServerVariables.set(NEXT_CYCLE_TIME, System.currentTimeMillis() + wait_time);
                if(getAttackPoints() >= 5)
                {
                    setAttackPoints(0);
                    if(taskSwitchCycle != null)
                    {
                        taskSwitchCycle.cancel(true);
                        taskSwitchCycle = null;
                    }
                    taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(3), wait_time);
                }
                break;
            }
            case 3:
            {
                // 3.Семя бессмертия захвачено
                // Фарм камней 150+.

                int wait_time = Rnd.get(Config.SOF_STAGE_3_FROM * ONE_MINUTE, Config.SOF_STAGE_3_TO * ONE_MINUTE);
                ServerVariables.set(NEXT_CYCLE_TIME, System.currentTimeMillis() + wait_time);
                if(taskSwitchCycle != null)
                {
                    taskSwitchCycle.cancel(true);
                    taskSwitchCycle = null;
                }
                taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(4), wait_time);
                break;
            }
            case 4:
            {
                // 4.Идет 1-ая оборона семени бессмертия
                // Фарм камней для распечатки веспера в форме дефа столбов от мобов. Переход на следующию стадию через 2-3 суток

                int wait_time = Rnd.get(Config.SOF_STAGE_4_FROM * ONE_MINUTE, Config.SOF_STAGE_4_TO * ONE_MINUTE);
                ServerVariables.set(NEXT_CYCLE_TIME, System.currentTimeMillis() + wait_time);
                if(taskSwitchCycle != null)
                {
                    taskSwitchCycle.cancel(true);
                    taskSwitchCycle = null;
                }
                taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(5), wait_time);
                break;
            }

            // TODO 5.Идет 2-ая оборона семени бессмертия
            // Аналогично, фарм веспер камней, только в виде хиро дефенса (аля WC3), прут жирные мобы, как только 20 из них доходят до центра - происходит фэйл. Переход на следующию стадию через 2-3 суток

            // TODO 6.Идет 1-ая осада семени бессмертия
            // Конец цикла, снова фарм кохименеса

            default:
            {
                int wait_time = Rnd.get(Config.SOF_STAGE_OTHER_FROM * ONE_MINUTE, Config.SOF_STAGE_OTHER_TO * ONE_MINUTE);
                setAttackPoints(0);
                ServerVariables.set(NEXT_CYCLE_TIME, System.currentTimeMillis() + wait_time);
                if(taskSwitchCycle != null)
                {
                    taskSwitchCycle.cancel(true);
                    taskSwitchCycle = null;
                }
                taskSwitchCycle = L2GameThreadPools.getInstance().scheduleGeneral(new TaskSwitchCycle(1), wait_time);
                break;
            }
        }
    }

    private static class TaskSwitchCycle implements Runnable
    {
        private int new_cycle;

        private TaskSwitchCycle(int cycle)
        {
            new_cycle = cycle;
        }

        @Override
        public void run()
        {
            setCurrentCycle(new_cycle);
            checkCurrentCycle();
        }
    }

    private static void clearItems()
    {
        for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
        {
            deleteItem(player.getInventory(), KECERUS_MARK_1);
            deleteItem(player.getInventory(), KECERUS_MARK_2);
        }

        ThreadConnection con = null;
        FiltredStatement statement = null;
        ResultSet rset = null;
        try
        {
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.createStatement();
            rset = statement.executeQuery("SELECT DISTINCT owner_id FROM items WHERE item_id IN(13691,13692)");

            SqlBatch b = new SqlBatch("REPLACE INTO character_variables (`obj_id`,`type`,`name`,`index`,`value`,`expire_time`) VALUES");
            StringBuilder sb;
            while (rset.next())
            {
                sb = StringUtil.startAppend(200, "(");
                StringUtil.append(sb, rset.getInt("owner_id"), ",'user-var', 'NeedToDeleteSOIItems', 0, 'true', -1)");
                b.write(sb.toString());
            }

            if(!b.isEmpty())
            {
                statement = con.createStatement();
                statement.executeUpdate(b.close());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            DbUtils.closeQuietly(con, statement, rset);
        }
    }

    private static void deleteItem(Inventory inv, int id)
    {
        long c = inv.getCountOf(id);
        if(c > 0)
            inv.destroyItemByItemId(id, c, false);
    }

    private static void spawnNpcs(int stage)
    {
        for(L2NpcInstance n : NPCS)
            n.deleteMe();
        NPCS.clear();

        int mouthNpcId = 32537; // Opened
        if(stage == 3)
            mouthNpcId = 32538; // Фарм камней 150+.
        for(Location loc : MOUTH_LOCS)
            NPCS.add(Functions.spawn(loc, mouthNpcId));
    }

    public static void enterToGathering(L2Player player)
    {
        player.teleToLocation(gather_locations[Rnd.get(gather_locations.length)]);
    }

    public static void spawnEnergies()
    {
        deleteEnergies();
        int count = 0;
        L2NpcTemplate template;
        L2Spawn spawnDat;
        for(int[] data : seeds_spawnlist)
        {
            try
            {
                template = NpcTable.getTemplate(data[1]);
                spawnDat = new L2Spawn(template);
                spawnDat.setAmount(data[0]);
                spawnDat.setRespawnDelay(data[2], data[3]);
                spawnDat.setLocation(data[4]);
                spawnDat.setReflection(0);
                spawnDat.setRespawnTime(0);
                seed_energies.add(spawnDat);
                // спауним
                count += spawnDat.init();
            }
            catch(Exception e)
            {
                _log.log(Level.WARNING, "SeedOfInfinityManager: spawnEnergies error!", e);
            }
        }

        _log.info("SeedOfInfinityManager: spawned " + count + " seed energies.");
    }

    public static void deleteEnergies()
    {
        for(L2Spawn spawn : seed_energies)
            if(spawn != null)
                spawn.despawnAll();
        seed_energies.clear();
    }
}