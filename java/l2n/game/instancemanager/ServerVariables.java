package l2n.game.instancemanager;

import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;

import java.sql.ResultSet;

public class ServerVariables
{
	private final StatsSet _variablesMap;

	public static final ServerVariables getInstance()
	{
		return SingletonHolder._instance;
	}

	private ServerVariables()
	{
		_variablesMap = new StatsSet();
		loadFromDB();
	}

	private StatsSet getVars()
	{
		return _variablesMap;
	}

	private void loadFromDB()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM server_variables");
			rs = statement.executeQuery();
			while (rs.next())
				_variablesMap.set(rs.getString(1), rs.getString(2));
		}
		catch(Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	private void saveToDB(String name)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			String value = _variablesMap.getString(name, "");
			if(value.isEmpty())
			{
				statement = con.prepareStatement("DELETE FROM server_variables WHERE name = ?");
				statement.setString(1, name);
				statement.execute();
			}
			else
			{
				statement = con.prepareStatement("REPLACE INTO server_variables (name, value) VALUES (?,?)");
				statement.setString(1, name);
				statement.setString(2, value);
				statement.execute();
			}
		}
		catch(Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static boolean getBool(String name)
	{
		return getInstance().getVars().getBool(name);
	}

	public static boolean getBool(String name, boolean _defult)
	{
		return getInstance().getVars().getBool(name, _defult);
	}

	public static int getInt(String name)
	{
		return getInstance().getVars().getInteger(name);
	}

	public static int getInt(String name, int _defult)
	{
		return getInstance().getVars().getInteger(name, _defult);
	}

	public static long getLong(String name)
	{
		return getInstance().getVars().getLong(name);
	}

	public static long getLong(String name, long _defult)
	{
		return getInstance().getVars().getLong(name, _defult);
	}

	public static float getFloat(String name)
	{
		return getInstance().getVars().getFloat(name);
	}

	public static float getFloat(String name, float _defult)
	{
		return getInstance().getVars().getFloat(name, _defult);
	}

	public static String getString(String name)
	{
		return getInstance().getVars().getString(name);
	}

	public static String getString(String name, String _defult)
	{
		return getInstance().getVars().getString(name, _defult);
	}

	public static void set(String name, boolean value)
	{
		getInstance().getVars().set(name, value);
		getInstance().saveToDB(name);
	}

	public static void set(String name, int value)
	{
		getInstance().getVars().set(name, value);
		getInstance().saveToDB(name);
	}

	public static void set(String name, long value)
	{
		getInstance().getVars().set(name, value);
		getInstance().saveToDB(name);
	}

	public static void set(String name, double value)
	{
		getInstance().getVars().set(name, value);
		getInstance().saveToDB(name);
	}

	public static void set(String name, String value)
	{
		getInstance().getVars().set(name, value);
		getInstance().saveToDB(name);
	}

	public static void unset(String name)
	{
		getInstance().getVars().unset(name);
		getInstance().saveToDB(name);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final ServerVariables _instance = new ServerVariables();
	}
}
