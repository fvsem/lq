package l2n.game.instancemanager;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.Reflection;
import l2n.game.tables.ReflectionTable;

import java.sql.ResultSet;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 28.04.2013
 * @time 14:10:12
 */
public class GraciaSeedManager
{
	private static final Logger _log = Logger.getLogger(GraciaSeedManager.class.getName());

	private static final String LOAD_VAR = "SELECT var,value FROM gracia_seeds";
	private static final String SAVE_VAR = "INSERT INTO gracia_seeds (var,value) VALUES (?,?) ON DUPLICATE KEY UPDATE value=?";

	private int _tiatKilled = 0;
	/** Состояние SoD: 1 - Тиат жив; 2 - Тиат мёртв, можно собирать энергию стихий; 3 - Защита TODO */
	private int _state = 1;
	/** хранит время последнего изменения состояния SoD */
	private long _lastStateChangeDate;

	private GraciaSeedManager()
	{
		_log.info("GraciaSeedManager: Initializing");
		_lastStateChangeDate = System.currentTimeMillis();
		loadData();
		handleSodStages();
		_log.info("GraciaSeedManager: state " + _state + ", tiat killed " + _tiatKilled);
	}

	public void saveData()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SAVE_VAR);

			statement.setString(1, "tiatKilled");
			statement.setInt(2, _tiatKilled);
			statement.setInt(3, _tiatKilled);
			statement.execute();

			statement.setString(1, "state");
			statement.setInt(2, _state);
			statement.setInt(3, _state);
			statement.execute();

			statement.setString(1, "lastStateChangeDate");
			statement.setLong(2, _lastStateChangeDate);
			statement.setLong(3, _lastStateChangeDate);
			statement.execute();

			_log.info("GraciaSeedManager: Database updated.");
		}
		catch(Exception e)
		{
			_log.warning("GraciaSeedManager: problem while saving variables: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void loadData()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		String var, value;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(LOAD_VAR);

			rset = statement.executeQuery();
			while (rset.next())
			{
				var = rset.getString(1);
				value = rset.getString(2);

				// SoD variables
				if("tiatKilled".equalsIgnoreCase(var))
					_tiatKilled = Integer.parseInt(value);
				else if("state".equalsIgnoreCase(var))
					_state = Integer.parseInt(value);
				else if("lastStateChangeDate".equalsIgnoreCase(var))
					_lastStateChangeDate = Long.parseLong(value);
			}
		}
		catch(Exception e)
		{
			_log.warning("GraciaSeedManager: problem while loading variables: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void handleSodStages()
	{
		switch (_state)
		{
			case 1:
				if(_tiatKilled >= 10)
				{
					setSoDState(2, true); // Переключаем SoD в режУм сбора атрУбут камней
					handleSodStages();
				}
				// do nothing, players should kill Tiat a few times
				break;
			case 2:
				// Conquest Complete state, if too much time is passed than change to defense state
				long timePast = System.currentTimeMillis() - _lastStateChangeDate;
				if(timePast >= Config.SOD_STAGE_2_LENGTH)
					// change to Attack state because Defend statet is not implemented
					setSoDState(1, true);
				else
					L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
					{
						@Override
						public void run()
						{
							setSoDState(1, true);
						}
					}, Config.SOD_STAGE_2_LENGTH - timePast);
				break;
			case 3:
				// TODO not implemented
				setSoDState(1, true);
				break;
			default:
				_log.warning("GraciaSeedManager: Unknown Seed of Destruction state(" + _state + ")! ");
		}
	}

	public void increaseSoDTiatKilled()
	{
		if(_state == 1)
		{
			_tiatKilled++;
			if(_tiatKilled >= Config.SOD_TIAT_KILL_COUNT)
				setSoDState(2, false); // Переключаем SoD в режУм сбора атрУбут камней
			saveData();
		}
	}

	public int getSoDTiatKilled()
	{
		return _tiatKilled;
	}

	public void setSoDState(int value, boolean doSave)
	{
		_log.info("GraciaSeedManager: New Seed of Destruction state -> " + value + ".");
		_lastStateChangeDate = System.currentTimeMillis();
		_state = value;

		// reset number of Tiat kills
		if(_state == 1)
		{
			Reflection r = ReflectionTable.SOD_REFLECTION_ID == 0 ? null : ReflectionTable.getInstance().get(ReflectionTable.SOD_REFLECTION_ID);
			if(ReflectionTable.SOD_REFLECTION_ID > 0 && r != null)
				r.startCollapseTimer(5 * 60 * 1000);

			_tiatKilled = 0;
		}
		else if(_state == 2)
		{
			L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public void run()
				{
					setSoDState(1, true);
				}
			}, Config.SOD_STAGE_2_LENGTH);
		}
		if(doSave)
			saveData();
	}

	public long getSoDTimeForNextStateChange()
	{
		switch (_state)
		{
			case 1:
				return -1;
			case 2:
				return _lastStateChangeDate + Config.SOD_STAGE_2_LENGTH - System.currentTimeMillis();
			case 3:
				// TODO not implemented yet
				return -1;
			default:
				// this should not happen!
				return -1;
		}
	}

	public long getSoDLastStateChangeDate()
	{
		return _lastStateChangeDate;
	}

	public int getSoDState()
	{
		return _state;
	}

	public static final GraciaSeedManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private static class SingletonHolder
	{
		protected static final GraciaSeedManager _instance = new GraciaSeedManager();
	}
}
