package l2n.game.cache;

import gnu.trove.map.hash.TIntIntHashMap;
import javolution.util.FastMap;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Alliance;
import l2n.game.model.L2Clan;

import java.sql.ResultSet;
import java.util.logging.Logger;

public final class CrestCache
{
	private static final Logger _log = Logger.getLogger(CrestCache.class.getName());

	public static final int CREST_CLAN_NORMAL_SIZE = 256;
	public static final int CREST_CLAN_LARGE_SIZE = 2176;
	public static final int CREST_ALLY_SIZE = 192;

	// Требуется для полученУя ID значка по ID клана
	private final static TIntIntHashMap _cachePledge = new TIntIntHashMap();
	private final static TIntIntHashMap _cachePledgeLarge = new TIntIntHashMap();
	private final static TIntIntHashMap _cacheAlly = new TIntIntHashMap();

	// Integer - ID значка, byte[] - сам значек
	private final static FastMap<Integer, byte[]> _cachePledgeHashed = new FastMap<Integer, byte[]>();
	private final static FastMap<Integer, byte[]> _cachePledgeLargeHashed = new FastMap<Integer, byte[]>();
	private final static FastMap<Integer, byte[]> _cacheAllyHashed = new FastMap<Integer, byte[]>();

	public static void load()
	{
		_cachePledge.clear();
		_cachePledgeLarge.clear();
		_cacheAlly.clear();
		_cachePledgeHashed.clear();
		_cachePledgeLargeHashed.clear();
		_cacheAllyHashed.clear();

		int counter = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet list = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT `clan_id`,`crest` FROM `clan_data` WHERE `crest` IS NOT NULL");
			list = statement.executeQuery();
			while (list.next())
			{
				counter++;
				final int hash = mhash(list.getBytes("crest"));
				_cachePledge.put(list.getInt("clan_id"), hash);
				_cachePledgeHashed.put(hash, list.getBytes("crest"));
			}
			DbUtils.closeQuietly(statement, list);

			statement = con.prepareStatement("SELECT `clan_id`,`largecrest` FROM `clan_data` WHERE `largecrest` IS NOT NULL");
			list = statement.executeQuery();
			while (list.next())
			{
				counter++;
				final int hash = mhash(list.getBytes("largecrest"));
				_cachePledgeLarge.put(list.getInt("clan_id"), hash);
				_cachePledgeLargeHashed.put(hash, list.getBytes("largecrest"));
			}
			DbUtils.closeQuietly(statement, list);

			statement = con.prepareStatement("SELECT `ally_id`,`crest` FROM `ally_data` WHERE `crest` IS NOT NULL");
			list = statement.executeQuery();
			while (list.next())
			{
				counter++;
				final int hash = mhash(list.getBytes("crest"));
				_cacheAlly.put(list.getInt("ally_id"), hash);
				_cacheAllyHashed.put(hash, list.getBytes("crest"));
			}
			DbUtils.closeQuietly(statement, list);
			statement = null;
			list = null;
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, list);
		}
		_log.config("CrestCache: Loaded " + counter + " crests");
	}

	public static byte[] getPledgeCrest(final int id)
	{
		final byte[] crest = _cachePledgeHashed.get(id);
		return crest != null ? crest : new byte[0];
	}

	public static byte[] getPledgeCrestLarge(final int id)
	{
		final byte[] crest = _cachePledgeLargeHashed.get(id);
		return crest != null ? crest : new byte[0];
	}

	public static byte[] getAllyCrest(final int id)
	{
		final byte[] crest = _cacheAllyHashed.get(id);
		return crest != null ? crest : new byte[0];
	}

	public static int getPledgeCrestId(final int clan_id)
	{
		final Integer crest = _cachePledge.get(clan_id);
		return crest != null ? crest : 0;
	}

	public static int getPledgeCrestLargeId(final int clan_id)
	{
		final Integer crest = _cachePledgeLarge.get(clan_id);
		return crest != null ? crest : 0;
	}

	public static int getAllyCrestId(final int ally_id)
	{
		final Integer crest = _cacheAlly.get(ally_id);
		return crest != null ? crest : 0;
	}

	public static void removePledgeCrest(final L2Clan clan)
	{
		clan.setCrestId(0);
		_cachePledge.remove(clan.getClanId());
		_cachePledgeHashed.remove(clan.getCrestId());
		clan.broadcastClanStatus(false, true, false);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET crest=? WHERE clan_id=?");
			statement.setNull(1, -3);
			statement.setInt(2, clan.getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void removePledgeCrestLarge(final L2Clan clan)
	{
		clan.setCrestLargeId(0);
		_cachePledgeLarge.remove(clan.getClanId());
		_cachePledgeLargeHashed.remove(clan.getCrestLargeId());
		clan.broadcastClanStatus(false, true, false);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET largecrest=? WHERE clan_id=?");
			statement.setNull(1, -3);
			statement.setInt(2, clan.getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void removeAllyCrest(final L2Alliance ally)
	{
		ally.setAllyCrestId(0);
		_cacheAlly.remove(ally.getAllyId());
		_cacheAllyHashed.remove(ally.getAllyCrestId());
		ally.broadcastAllyStatus(true);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE ally_data SET crest=? WHERE ally_id=?");
			statement.setNull(1, -3);
			statement.setInt(2, ally.getAllyId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void savePledgeCrest(final L2Clan clan, final byte[] data)
	{
		final int hash = mhash(data);
		clan.setCrestId(hash);
		_cachePledgeHashed.put(hash, data);
		_cachePledge.put(clan.getClanId(), hash);
		clan.broadcastClanStatus(false, true, false);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET crest=? WHERE clan_id=?");
			statement.setBytes(1, data);
			statement.setInt(2, clan.getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void savePledgeCrestLarge(final L2Clan clan, final byte[] data)
	{
		final int hash = mhash(data);
		clan.setCrestLargeId(hash);
		_cachePledgeLargeHashed.put(hash, data);
		_cachePledgeLarge.put(clan.getClanId(), hash);
		clan.broadcastClanStatus(false, true, false);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE clan_data SET largecrest=? WHERE clan_id=?");
			statement.setBytes(1, data);
			statement.setInt(2, clan.getClanId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static void saveAllyCrest(final L2Alliance ally, final byte[] data)
	{
		final int hash = mhash(data);
		ally.setAllyCrestId(hash);
		_cacheAllyHashed.put(hash, data);
		_cacheAlly.put(ally.getAllyId(), hash);
		ally.broadcastAllyStatus(false);
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE ally_data SET crest=? WHERE ally_id=?");
			statement.setBytes(1, data);
			statement.setInt(2, ally.getAllyId());
			statement.execute();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public static int mhash(final byte[] data)
	{
		int ret = 0;
		if(data != null)
			for(final byte element : data)
				ret = 7 * ret + element;
		return Math.abs(ret);
	}
}
