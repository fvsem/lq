package l2n.game;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.extensions.Stat;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2ManufactureItem;
import l2n.game.model.L2Recipe;
import l2n.game.model.L2RecipeList;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.game.templates.L2Item;
import l2n.util.Rnd;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class RecipeController
{
	private final static Logger _log = Logger.getLogger(RecipeController.class.getName());
	private static RecipeController _instance;

	private TIntObjectHashMap<L2RecipeList> _lists;

	private static final String RECIPES_FILE = "recipes.xml";

	/***************************************************************************/

	public static RecipeController getInstance()
	{
		if(_instance == null)
			_instance = new RecipeController();
		return _instance;
	}

	/***************************************************************************/

	public RecipeController()
	{
		_lists = new TIntObjectHashMap<L2RecipeList>();

		try
		{
			loadRecipes();
			_log.config("RecipeController: Loaded " + _lists.size() + " Recipes.");
		}
		catch(Exception e)
		{
			_log.warning("Failed loading recipe list!");
			e.printStackTrace();
		}
	}

	private void loadRecipes() throws SAXException, IOException, ParserConfigurationException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setIgnoringComments(true);
		File file = new File(Config.DATAPACK_ROOT, "data/" + RECIPES_FILE);
		if(file.exists())
		{
			Document doc = factory.newDocumentBuilder().parse(file);
			GArray<L2Recipe> recipePartList = new GArray<L2Recipe>();

			for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
				if("list".equalsIgnoreCase(n.getNodeName()))
					for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						if("item".equalsIgnoreCase(d.getNodeName()))
						{
							recipePartList.clear();
							NamedNodeMap attrs = d.getAttributes();
							Node att;
							int id = -1;
							boolean haveRare = false;
							StatsSet set = new StatsSet();

							att = attrs.getNamedItem("id");
							if(att == null)
							{
								_log.warning("Missing id for recipe item, skipping");
								continue;
							}
							id = Integer.parseInt(att.getNodeValue());
							set.set("id", id);

							att = attrs.getNamedItem("recipeId");
							if(att == null)
							{
								_log.warning("Missing recipeId for recipe item id: " + id + ", skipping");
								continue;
							}
							set.set("recipeId", Integer.parseInt(att.getNodeValue()));

							att = attrs.getNamedItem("name");
							if(att == null)
							{
								_log.warning("Missing name for recipe item id: " + id + ", skipping");
								continue;
							}
							set.set("recipeName", att.getNodeValue());

							att = attrs.getNamedItem("craftLevel");
							if(att == null)
							{
								_log.warning("Missing level for recipe item id: " + id + ", skipping");
								continue;
							}
							set.set("craftLevel", Integer.parseInt(att.getNodeValue()));

							att = attrs.getNamedItem("type");
							if(att == null)
							{
								_log.warning("Missing type for recipe item id: " + id + ", skipping");
								continue;
							}
							set.set("isDwarvenRecipe", att.getNodeValue().equalsIgnoreCase("dwarven"));

							att = attrs.getNamedItem("successRate");
							if(att == null)
							{
								_log.warning("Missing successRate for recipe item id: " + id + ", skipping");
								continue;
							}
							set.set("successRate", Integer.parseInt(att.getNodeValue()));

							for(Node c = d.getFirstChild(); c != null; c = c.getNextSibling())
							{
								if("mpCost".equalsIgnoreCase(c.getNodeName()))
								{
									int mpCost = Integer.parseInt(c.getAttributes().getNamedItem("val").getNodeValue());
									set.set("mpCost", mpCost);
								}
								if("ingredient".equalsIgnoreCase(c.getNodeName()))
								{
									int ingId = Integer.parseInt(c.getAttributes().getNamedItem("id").getNodeValue());
									int ingCount = Integer.parseInt(c.getAttributes().getNamedItem("count").getNodeValue());
									recipePartList.add(new L2Recipe(ingId, ingCount));
								}
								else if("production".equalsIgnoreCase(c.getNodeName()))
								{
									set.set("itemId", Integer.parseInt(c.getAttributes().getNamedItem("id").getNodeValue()));
									set.set("count", Integer.parseInt(c.getAttributes().getNamedItem("count").getNodeValue()));
								}
								else if("productionRare".equalsIgnoreCase(c.getNodeName()))
								{
									set.set("rareItemId", Integer.parseInt(c.getAttributes().getNamedItem("id").getNodeValue()));
									set.set("rareCount", Integer.parseInt(c.getAttributes().getNamedItem("count").getNodeValue()));
									set.set("rarity", Integer.parseInt(c.getAttributes().getNamedItem("rarity").getNodeValue()));
									haveRare = true;
								}
							}

							L2RecipeList recipeList = new L2RecipeList(set, haveRare);
							for(L2Recipe recipePart : recipePartList)
								recipeList.addRecipe(recipePart);

							_lists.put(recipeList.getId(), recipeList);
						}
		}
		else
			_log.warning("Recipes file (" + file.getAbsolutePath() + ") doesnt exists.");
	}

	/***************************************************************************/

	public int getRecipesCount()
	{
		return _lists.size();
	}

	/***************************************************************************/

	public L2RecipeList getRecipeList(int listId)
	{
		return _lists.get(listId);
	}

	private L2RecipeList getValidRecipeList(L2Player player, int id)
	{
		L2RecipeList recipeList = getRecipeList(id);

		if(recipeList == null || recipeList.getRecipes().length == 0)
		{
			player.sendMessage("No recipe for: " + id);
			player.sendActionFailed();
			return null;
		}
		return recipeList;
	}

	/***************************************************************************/

	public L2RecipeList getRecipeByItemId(int itemId)
	{
		for(L2RecipeList find : _lists.valueCollection())
			if(find.getRecipeId() == itemId)
				return find;
		return null;
	}

	public int[] getAllItemIds()
	{
		int[] idList = new int[_lists.size()];
		int i = 0;
		for(L2RecipeList rec : _lists.valueCollection())
			idList[i++] = rec.getRecipeId();
		return idList;
	}

	/***************************************************************************/

	public void requestBookOpen(L2Player player, boolean isDwarvenCraft)
	{
		RecipeBookItemList response = new RecipeBookItemList(isDwarvenCraft, (int) player.getCurrentMp());
		if(isDwarvenCraft)
			response.setRecipes(player.getDwarvenRecipeBook());
		else
			response.setRecipes(player.getCommonRecipeBook());
		player.sendPacket(response);
	}

	public void requestMakeItem(L2Player player, int recipeListId)
	{
		L2RecipeList recipeList = getValidRecipeList(player, recipeListId);
		player.resetWaitSitTime();

		if(recipeList == null)
			return;

		int skillId = recipeList.isDwarvenRecipe() ? L2Skill.SKILL_CRAFTING : L2Skill.SKILL_CREATE_COMMON;
		int skillLevel = player.getSkillLevel(skillId);

		synchronized (player)
		{
			if(player.getCurrentMp() < recipeList.getMpCost())
			{
				player.sendPacket(Msg.NOT_ENOUGH_MP, new RecipeItemMakeInfo(recipeList.getId(), player, false));
				return;
			}

			if(!player.findRecipe(recipeListId))
			{
				player.sendPacket(Msg.PLEASE_REGISTER_A_RECIPE, Msg.ActionFail);
				return;
			}

			if(player.isAlikeDead())
			{
				player.sendMessage("Мертвым нельзя крафтить.");
				player.sendActionFailed();
				return;
			}

			if(player.isInTransaction())
			{
				player.sendMessage("Вы заняты.");
				player.sendActionFailed();
				return;
			}

			if(skillLevel < recipeList.getLevel())
			{
				player.sendMessage("Нужен уровень умения " + recipeList.getLevel());
				player.sendActionFailed();
				return;
			}
		}

		synchronized (player.getInventory())
		{
			L2Recipe[] recipes = recipeList.getRecipes();
			Inventory inventory = player.getInventory();
			for(L2Recipe recipe : recipes)
			{
				if(recipe.getQuantity() == 0)
					continue;

				if(Config.ALT_GAME_UNREGISTER_RECIPE && ItemTable.getInstance().getTemplate(recipe.getItemId()).getItemType() == EtcItemType.RECIPE)
				{
					L2RecipeList rp = RecipeController.getInstance().getRecipeByItemId(recipe.getItemId());
					if(player.findRecipe(rp))
						continue;
					player.sendPacket(Msg.NOT_ENOUGH_MATERIALS, new RecipeItemMakeInfo(recipeList.getId(), player, false));
					return;
				}

				L2ItemInstance invItem = inventory.getItemByItemId(recipe.getItemId());
				if(invItem == null || recipe.getQuantity() > invItem.getCount())
				{
					player.sendPacket(Msg.NOT_ENOUGH_MATERIALS, new RecipeItemMakeInfo(recipeList.getId(), player, false));
					return;
				}
			}

			player.reduceCurrentMp(recipeList.getMpCost(), null);

			for(L2Recipe recipe : recipes)
				if(recipe.getQuantity() != 0)
				{
					L2ItemInstance invItem = inventory.getItemByItemId(recipe.getItemId());
					if(Config.ALT_GAME_UNREGISTER_RECIPE && ItemTable.getInstance().getTemplate(recipe.getItemId()).getItemType() == EtcItemType.RECIPE)
						player.unregisterRecipe(RecipeController.getInstance().getRecipeByItemId(recipe.getItemId()).getId());
					else
						inventory.destroyItem(invItem, recipe.getQuantity(), false);
				}
		}

		int itemId = recipeList.getItemId();
		int itemCount = recipeList.getCount();
		int rareProdId = recipeList.getRareItemId();

		boolean success = false;
		if(!Rnd.chance(recipeList.getSuccessRate()))
			player.sendPacket(Msg.YOU_FAILED_AT_ITEM_MIXING);
		else
		{
			if(Config.ALT_MASTERWORK_CONFIG)
			{
				if(Config.ALLOW_MASTERWORK)
					if(rareProdId > 0 && Rnd.chance(Config.RATE_MASTERWORK))
						itemId = rareProdId;

				if(Config.ALLOW_CRITICAL_CRAFT && recipeList.isCriticalAffected())
					if(Rnd.chance(Config.RATE_CRITICAL_CRAFT_CHANCE))
						itemCount = itemCount * Config.RATE_CRITICAL_CRAFT_MULTIPLIER;
			}
			else if(rareProdId > 0 && Config.ALLOW_MASTERWORK)
				if(Rnd.chance(recipeList.getRarity()))
				{
					itemId = rareProdId;
					if(Config.ALLOW_CRITICAL_CRAFT)
						itemCount = recipeList.getRareCount();
				}

			L2ItemInstance createdItem = ItemTable.getInstance().createItem(itemId, player.getObjectId(), player.getObjectId(), "RecipeController");
			createdItem.setCount(itemCount);
			success = true;

			if(Config.CRAFT_COUNTER)
				player.incrementCraftCounter(createdItem.getItemId(), new Integer(itemCount));

			player.getInventory().addItem(createdItem);
			player.sendPacket(SystemMessage.obtainItems(createdItem));
		}

		if(Config.ALT_GAME_EXP_FOR_CRAFT)
		{
			L2Item template = ItemTable.getInstance().getTemplate(itemId);
			long exp = -1;
			long sp = -1;

			int recipeLevel = recipeList.getLevel();
			if(exp < 0)
			{
				exp = template.getReferencePrice() * itemCount;
				exp /= recipeLevel;
			}
			if(sp < 0)
				sp = (int) (exp / 10);
			if(itemId == rareProdId)
			{
				exp *= Config.ALT_GAME_CREATION_RARE_XPSP_RATE;
				sp *= Config.ALT_GAME_CREATION_RARE_XPSP_RATE;
			}

			if(exp < 0)
				exp = 0;
			if(sp < 0)
				sp = 0;

			for(int i = skillLevel; i > recipeLevel; i--)
			{
				exp /= 4;
				sp /= 4;
			}

			player.addExpAndSp((long) (exp * Config.CRAFT_RATE_XP), (long) (sp * Config.CRAFT_RATE_SP), true, false);
		}

		player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD, StatusUpdate.CUR_MP);
		player.sendPacket(new RecipeItemMakeInfo(recipeList.getId(), player, success));
	}

	public void requestManufactureItem(L2Player master, L2Player employer, int recipeListId)
	{
		L2RecipeList recipeList = getValidRecipeList(master, recipeListId);
		if(recipeList == null)
			return;

		master.resetWaitSitTime();
		int success = 0;

		int skillId = recipeList.isDwarvenRecipe() ? L2Skill.SKILL_CRAFTING : L2Skill.SKILL_CREATE_COMMON;
		int skillLevel = master.getSkillLevel(skillId);

		master.sendMessage(new CustomMessage("l2n.game.RecipeController.GotOrder", master).addString(recipeList.getRecipeName()));

		if(recipeList.getRecipes().length == 0)
		{
			master.sendMessage(new CustomMessage("l2n.game.RecipeController.NoRecipe", master).addString(recipeList.getRecipeName()));
			employer.sendMessage(new CustomMessage("l2n.game.RecipeController.NoRecipe", master).addString(recipeList.getRecipeName()));
			return;
		}

		long price = 0;
		for(L2ManufactureItem temp : master.getCreateList().getList())
			if(temp.getRecipeId() == recipeList.getId())
			{
				price = temp.getCost();
				break;
			}

		synchronized (master)
		{
			if(master.getCurrentMp() < recipeList.getMpCost())
			{
				master.sendPacket(Msg.NOT_ENOUGH_MP);
				employer.sendPacket(Msg.NOT_ENOUGH_MP, new RecipeShopItemInfo(master.getObjectId(), recipeListId, price, success, employer));
				return;
			}

			if(!master.findRecipe(recipeListId))
			{
				master.sendPacket(Msg.PLEASE_REGISTER_A_RECIPE, Msg.ActionFail);
				return;
			}

			if(master.isAlikeDead() || employer.isAlikeDead())
			{
				employer.sendMessage("Мертвым нельзя крафтить.");
				employer.sendActionFailed();
				return;
			}

			if(master.isInTransaction() || employer.isInTransaction())
			{
				employer.sendMessage("Нужен уровень умения.");
				employer.sendActionFailed();
				return;
			}

			if(skillLevel < recipeList.getLevel())
			{
				employer.sendActionFailed();
				return;
			}
		}

		if(employer.getAdena() < price)
		{
			employer.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			employer.sendPacket(new RecipeShopItemInfo(master.getObjectId(), recipeListId, price, success, employer));
			return;
		}

		synchronized (employer.getInventory())
		{
			L2Recipe[] recipes = recipeList.getRecipes();
			Inventory inventory = employer.getInventory();
			for(L2Recipe recipe : recipes)
			{
				if(recipe.getQuantity() == 0)
					continue;

				L2ItemInstance invItem = inventory.getItemByItemId(recipe.getItemId());

				if(invItem == null || recipe.getQuantity() > invItem.getCount())
				{
					employer.sendPacket(Msg.NOT_ENOUGH_MATERIALS);
					employer.sendPacket(new RecipeShopItemInfo(master.getObjectId(), recipeListId, price, success, employer));
					return;
				}
			}

			master.reduceCurrentMp(recipeList.getMpCost(), null);

			for(L2Recipe recipe : recipes)
				if(recipe.getQuantity() != 0)
				{
					L2ItemInstance invItem = inventory.getItemByItemId(recipe.getItemId());
					inventory.destroyItem(invItem, recipe.getQuantity(), false);
				}
		}

		if(price > 0)
		{
			employer.reduceAdena(price, true);
			master.addAdena(price);

			int tax = (int) (price * Config.SERVICES_TRADE_TAX / 100);
                        
			if(Config.SERVICES_TRADE_TAX_ONLY_OFFLINE && !master.isInOfflineMode())
				tax = 0;
			if(tax > 0)
			{
				master.reduceAdena(tax, true);
				Stat.addTax(tax);
				master.sendMessage(new CustomMessage("trade.HavePaidTax", master).addNumber(tax));
			}
		}

		int itemId = recipeList.getItemId();
		int itemCount = recipeList.getCount();
		int rareProdId = recipeList.getRareItemId();

		SystemMessage msgtoemployer;
		SystemMessage msgtomaster;
		if(!Rnd.chance(recipeList.getSuccessRate()))
		{
			msgtoemployer = new SystemMessage(SystemMessage.S1_HAS_FAILED_TO_CREATE_S2_AT_THE_PRICE_OF_S3_ADENA).addString(master.getName()).addItemName(itemId).addNumber(price);
			msgtomaster = new SystemMessage(SystemMessage.THE_ATTEMPT_TO_CREATE_S2_FOR_S1_AT_THE_PRICE_OF_S3_ADENA_HAS_FAILED).addString(employer.getName()).addItemName(itemId).addNumber(price);
		}
		else
		{
			if(Config.ALT_MASTERWORK_CONFIG)
			{
				if(Config.ALLOW_MASTERWORK)
					if(rareProdId > 0 && Rnd.chance(Config.RATE_MASTERWORK))
						itemId = rareProdId;

				// Critical Craft
				if(Config.ALLOW_CRITICAL_CRAFT && recipeList.isCriticalAffected())
					if(Rnd.chance(Config.RATE_CRITICAL_CRAFT_CHANCE))
						itemCount = itemCount * Config.RATE_CRITICAL_CRAFT_MULTIPLIER;
			}
			else if(rareProdId > 0 && Config.ALLOW_MASTERWORK)
				if(Rnd.chance(recipeList.getRarity()))
				{
					itemId = rareProdId;
					if(Config.ALLOW_CRITICAL_CRAFT)
						itemCount = recipeList.getRareCount();
				}

			L2ItemInstance createdItem = ItemTable.getInstance().createItem(itemId, employer.getObjectId(), master.getObjectId(), "RecipeController");
			createdItem.setCount(itemCount);
			if(Config.CRAFT_COUNTER)
				master.incrementCraftCounter(createdItem.getItemId(), itemCount);

			employer.getInventory().addItem(createdItem);
			if(itemCount > 1)
			{
				msgtoemployer = new SystemMessage(SystemMessage.S1_CREATED_S2_S3_AT_THE_PRICE_OF_S4_ADENA);
				msgtoemployer.addString(master.getName());
				msgtoemployer.addNumber(itemCount);
				msgtoemployer.addItemName(itemId);
				msgtoemployer.addNumber(price);
				msgtomaster = new SystemMessage(SystemMessage.S2_S3_HAVE_BEEN_SOLD_TO_S1_FOR_S4_ADENA);
				msgtomaster.addString(employer.getName());
				msgtomaster.addNumber(itemCount);
				msgtomaster.addItemName(itemId);
				msgtomaster.addNumber(price);
			}
			else
			{
				msgtoemployer = new SystemMessage(SystemMessage.S1_CREATED_S2_AFTER_RECEIVING_S3_ADENA);
				msgtoemployer.addString(master.getName());
				msgtoemployer.addItemName(itemId);
				msgtoemployer.addNumber(price);
				msgtomaster = new SystemMessage(SystemMessage.S2_IS_SOLD_TO_S1_AT_THE_PRICE_OF_S3_ADENA);
				msgtomaster.addString(employer.getName());
				msgtomaster.addItemName(itemId);
				msgtomaster.addNumber(price);
			}
			success++;
		}

		if(Config.ALT_GAME_EXP_FOR_CRAFT)
		{
			long exp = -1;
			long sp = -1;

			L2Item template = ItemTable.getInstance().getTemplate(itemId);
			int recipeLevel = recipeList.getLevel();
			if(exp < 0)
			{
				exp = template.getReferencePrice() * itemCount;
				exp /= recipeLevel;
			}
			if(sp < 0)
				sp = (int) (exp / 10);
			if(itemId == rareProdId)
			{
				exp *= Config.ALT_GAME_CREATION_RARE_XPSP_RATE;
				sp *= Config.ALT_GAME_CREATION_RARE_XPSP_RATE;
			}

			if(exp < 0)
				exp = 0;
			if(sp < 0)
				sp = 0;

			for(int i = skillLevel; i > recipeLevel; i--)
			{
				exp /= 4;
				sp /= 4;
			}

			master.addExpAndSp((long) (exp * Config.CRAFT_RATE_XP), (long) (sp * Config.CRAFT_RATE_SP), true, false);
		}

		master.sendStatusUpdate(false, StatusUpdate.CUR_LOAD, StatusUpdate.CUR_MP);

		master.sendPacket(msgtomaster);
		employer.sendPacket(msgtoemployer);
		employer.sendChanges();

		employer.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
		employer.sendPacket(new RecipeShopItemInfo(master.getObjectId(), recipeListId, price, success, employer));
	}
}
