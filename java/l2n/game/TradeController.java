package l2n.game;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.TradeItem;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Logger;

public class TradeController
{
	private static final Logger _log = Logger.getLogger(TradeController.class.getName());
	private static TradeController _instance;

	private TIntObjectHashMap<NpcTradeList> _lists;

	public static TradeController getInstance()
	{
		if(_instance == null)
			_instance = new TradeController();
		return _instance;
	}

	public static void reload()
	{
		_instance = new TradeController();
	}

	private TradeController()
	{
		_lists = new TIntObjectHashMap<NpcTradeList>();

		try
		{
			File filelists = new File(Config.DATAPACK_ROOT + "/data/merchant_filelists.xml");
			DocumentBuilderFactory factory1 = DocumentBuilderFactory.newInstance();
			factory1.setValidating(false);
			factory1.setIgnoringComments(true);
			Document doc1 = factory1.newDocumentBuilder().parse(filelists);

			int counterFiles = 0;
			int counterItems = 0;
			for(Node n1 = doc1.getFirstChild(); n1 != null; n1 = n1.getNextSibling())
				if("list".equalsIgnoreCase(n1.getNodeName()))
					for(Node d1 = n1.getFirstChild(); d1 != null; d1 = d1.getNextSibling())
						if("file".equalsIgnoreCase(d1.getNodeName()))
						{
							final String filename = d1.getAttributes().getNamedItem("name").getNodeValue();

							File file = new File(Config.DATAPACK_ROOT + "/data/" + filename);
							DocumentBuilderFactory factory2 = DocumentBuilderFactory.newInstance();
							factory2.setValidating(false);
							factory2.setIgnoringComments(true);
							Document doc2 = factory2.newDocumentBuilder().parse(file);
							counterFiles++;

							for(Node n2 = doc2.getFirstChild(); n2 != null; n2 = n2.getNextSibling())
								if("list".equalsIgnoreCase(n2.getNodeName()))
									for(Node d2 = n2.getFirstChild(); d2 != null; d2 = d2.getNextSibling())
										if("tradelist".equalsIgnoreCase(d2.getNodeName()))
										{
											final int shop_id = Integer.parseInt(d2.getAttributes().getNamedItem("shop").getNodeValue());
											final int npc_id = Integer.parseInt(d2.getAttributes().getNamedItem("npc").getNodeValue());
											final float markup = npc_id > 0 ? 1 + Float.parseFloat(d2.getAttributes().getNamedItem("markup").getNodeValue()) / 100f : 0f;
											NpcTradeList tl = new NpcTradeList(shop_id);
											tl.setNpcId(npc_id);
											for(Node i = d2.getFirstChild(); i != null; i = i.getNextSibling())
												if("item".equalsIgnoreCase(i.getNodeName()))
												{
													final int itemId = Integer.parseInt(i.getAttributes().getNamedItem("id").getNodeValue());
													L2Item template = ItemTable.getInstance().getTemplate(itemId);
													if(template == null)
														_log.warning("TradeController: UNKNOWN ITEM '" + itemId + "' IN SHOP '" + shop_id + "' AT NPC '" + npc_id + "' IN file '" + filename + "'");
													else
													{
														if(Config.ALT_SHOP_CONDITION_ENABLE && !checkItem(template))
															continue;

														counterItems++;

														long price = i.getAttributes().getNamedItem("price") != null ? Long.parseLong(i.getAttributes().getNamedItem("price").getNodeValue()) : Math.round(ItemTable.getInstance().getTemplate(itemId).getReferencePrice() * markup);
														TradeItem item = new TradeItem();
														item.setItemId(itemId);
														int itemCount = i.getAttributes().getNamedItem("count") != null ? Integer.parseInt(i.getAttributes().getNamedItem("count").getNodeValue()) : 0;
														// Время респауна задается минутах
														int itemRechargeTime = i.getAttributes().getNamedItem("time") != null ? Integer.parseInt(i.getAttributes().getNamedItem("time").getNodeValue()) : 0;
														if(item != null)
														{
															item.setOwnersPrice(price);
															item.setCount(itemCount);
															item.setCurrentValue(itemCount);
															item.setLastRechargeTime((int) (System.currentTimeMillis() / 60000));
															item.setRechargeTime(itemRechargeTime);
															tl.addItem(item);
														}
													}
												}
											_lists.put(shop_id, tl);
										}
						}

			_log.config("TradeController: Loaded " + counterFiles + " file(s).");
			_log.config("TradeController: Loaded " + counterItems + " Items.");
			_log.config("TradeController: Loaded " + _lists.size() + " Buylists.");
		}
		catch(Exception e)
		{
			_log.warning("TradeController: Buylists could not be initialized.");
			e.printStackTrace();
		}
	}

	public boolean checkItem(L2Item template)
	{
		if(template.isCommonItem() && !Config.ALT_ALLOW_SELL_COMMON)
			return false;
		if(template.isEquipment() && !template.isForPet() && Config.ALT_SHOP_PRICE_LIMITS.length > 0)
			for(int i = 0; i < Config.ALT_SHOP_PRICE_LIMITS.length; i += 2)
			{
				if(template.getBodyPart() != Config.ALT_SHOP_PRICE_LIMITS[i])
					continue;
				if(template.getReferencePrice() <= Config.ALT_SHOP_PRICE_LIMITS[i + 1])
					break;
				return false;
			}
		if(Config.ALT_SHOP_UNALLOWED_ITEMS.contains(template.getItemId()))
			return false;
		return true;
	}

	public NpcTradeList getBuyList(int listId)
	{
		return _lists.get(listId);
	}

	public void addToBuyList(int listId, NpcTradeList list)
	{
		_lists.put(listId, list);
	}

	public static class NpcTradeList
	{
		private static final GArray<TradeItem> emptyList = new GArray<TradeItem>(0);
		private GArray<TradeItem> tradeList;
		private int _id;
		private int _npcId;

		public NpcTradeList(int id)
		{
			_id = id;
		}

		public int getListId()
		{
			return _id;
		}

		public void setNpcId(int id)
		{
			_npcId = id;
		}

		public int getNpcId()
		{
			return _npcId;
		}

		public void addItem(TradeItem ti)
		{
			if(tradeList == null)
				tradeList = new GArray<TradeItem>();
			tradeList.add(ti);
		}

		public GArray<TradeItem> getItems()
		{
			return tradeList == null ? emptyList : tradeList;
		}
	}
}
