package l2n.game;

import l2n.Config;
import l2n.Server;
import l2n.commons.misc.L2ThreadPool;
import l2n.commons.threading.FIFOExecutableQueue;
import l2n.commons.threading.ThreadPoolFactory;
import l2n.game.network.L2GameClient;
import l2n.game.network.clientpackets.L2GameClientPacket;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.util.ArrayUtil;
import l2n.util.ThreadUtil;

import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2GameThreadPools implements L2ThreadPool<L2GameClient, L2GameClientPacket, L2GameServerPacket>
{
	public static final Logger _log = Logger.getLogger(L2GameThreadPools.class.getName());

	private static final long MAX_DELAY = Long.MAX_VALUE / 1000000 / 2;

	public static long validateDelay(long delay)
	{
		if(delay < 0)
			delay = 0;
		else if(delay > MAX_DELAY)
		{
			_log.log(Level.WARNING, "ThreadPoolManager: big delay for task!", new Exception("validateDelay"));
			delay = MAX_DELAY;
		}
		return delay;
	}

	public static long validatePeriode(long periode)
	{
		if(periode < 1)
			periode = 1;
		else if(periode > MAX_DELAY)
		{
			_log.log(Level.WARNING, "ThreadPoolManager: big periode for task!", new Exception("validatePeriode"));
			periode = MAX_DELAY;
		}
		return periode;
	}

	private final ScheduledThreadPoolExecutor _generalScheduledExecutor;
	private ScheduledThreadPoolExecutor _moveScheduledThreadPool;
	private ScheduledThreadPoolExecutor _aiScheduledThreadPool;

	private ThreadPoolExecutor _generalExecutor;
	private ThreadPoolExecutor _gameClientPacketsThreadExecutor;
	private final ThreadPoolExecutor _loginServerPacketsThreadExecutor;

	private boolean _shutdown;

	private static final class SingletonHolder
	{
		private static final L2GameThreadPools INSTANCE = new L2GameThreadPools();
	}

	public static L2GameThreadPools getInstance()
	{
		return SingletonHolder.INSTANCE;
	}

	private L2GameThreadPools()
	{
		_generalScheduledExecutor = ThreadPoolFactory.createScheduledThreadPoolExecutor(Config.THREAD_SP_GENERAL, "Gerenal-STP", Thread.NORM_PRIORITY + 1);

		if(Server.SERVER_MODE == Server.MODE_GAMESERVER || Server.SERVER_MODE == Server.MODE_COMBOSERVER)
		{
			_moveScheduledThreadPool = ThreadPoolFactory.createScheduledThreadPoolExecutor(Config.THREAD_P_MOVE, "Move-STP", Thread.NORM_PRIORITY);
			_aiScheduledThreadPool = ThreadPoolFactory.createScheduledThreadPoolExecutor(Config.AI_MAX_THREAD, "AI-STP", Thread.NORM_PRIORITY);
			_generalExecutor = ThreadPoolFactory.createThreadPoolExecutor(Config.THREAD_EP_GENERAL, Integer.MAX_VALUE, 5L, TimeUnit.SECONDS, "Gerenal-ETP", Thread.NORM_PRIORITY + 1);

			if(Config.IG_PACKET_THREAD_CORE_SIZE == -1)
				_gameClientPacketsThreadExecutor = ThreadPoolFactory.createThreadPoolExecutor(0, Integer.MAX_VALUE, 15L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), "GamePacket-TE", Thread.NORM_PRIORITY + 3);
			else
				_gameClientPacketsThreadExecutor = ThreadPoolFactory.createThreadPoolExecutor(Config.IG_PACKET_THREAD_CORE_SIZE, Config.IG_PACKET_THREAD_CORE_SIZE * 2, 5L, TimeUnit.SECONDS, "GamePacket-TE", Thread.NORM_PRIORITY + 3);

			scheduleGeneralAtFixedRate(new Runnable()
			{
				@Override
				public void run()
				{
					purge();
				}
			}, 300000L, 300000L);
		}
		_loginServerPacketsThreadExecutor = ThreadPoolFactory.createThreadPoolExecutor(1, 5, 5L, TimeUnit.MILLISECONDS, "LsPacket-TP", Thread.NORM_PRIORITY + 3);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneral(final T r, final long delay)
	{
		return (ScheduledFuture<T>) _generalScheduledExecutor.schedule(r, validateDelay(delay), TimeUnit.MILLISECONDS);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleGeneralAtFixedRate(final T r, final long initial, final long delay)
	{
		return (ScheduledFuture<T>) _generalScheduledExecutor.scheduleAtFixedRate(r, validateDelay(initial), validatePeriode(delay), TimeUnit.MILLISECONDS);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleMove(final T r, final long delay)
	{
		return (ScheduledFuture<T>) _moveScheduledThreadPool.schedule(r, validateDelay(delay), TimeUnit.MILLISECONDS);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleAi(final T r, final long delay, final boolean isPlayer)
	{
		return (ScheduledFuture<T>) _aiScheduledThreadPool.schedule(r, validateDelay(delay), TimeUnit.MILLISECONDS);
	}

	@SuppressWarnings("unchecked")
	public <T extends Runnable> ScheduledFuture<T> scheduleAiAtFixedRate(final T r, final long initial, final long delay, final boolean isPlayer)
	{
		return (ScheduledFuture<T>) _aiScheduledThreadPool.scheduleAtFixedRate(r, validateDelay(initial), validatePeriode(delay), TimeUnit.MILLISECONDS);
	}


	@Override
	public void executeQueue(final FIFOExecutableQueue queue)
	{
		_gameClientPacketsThreadExecutor.execute(queue);
	}

	public void executeLoginServerPacket(final Runnable r)
	{
		_loginServerPacketsThreadExecutor.execute(r);
	}

	public final void executeGeneral(final Runnable r)
	{
		_generalExecutor.execute(r);
	}

	public final void executeAi(final Runnable r, final boolean isPlayer)
	{
		_generalExecutor.execute(r);
	}

	public final void executeMove(final Runnable r)
	{
		_generalExecutor.execute(r);
	}


	public void shutdown()
	{
		System.out.println("ThreadPoolManager: Shutting down all thread pools...");
		_shutdown = true;
		
		shutdown(_loginServerPacketsThreadExecutor);
		shutdown(_aiScheduledThreadPool);
		shutdown(_generalExecutor);
		shutdown(_gameClientPacketsThreadExecutor);
		shutdown(_moveScheduledThreadPool);
		shutdown(_generalScheduledExecutor);

		System.out.println("ThreadPoolManager: Shutdown complete.");
	}

	private static final void shutdown(final ThreadPoolExecutor tpe)
	{
		try
		{
			tpe.awaitTermination(1, TimeUnit.SECONDS);
		}
		catch(final InterruptedException e)
		{}
		finally
		{
			tpe.shutdown();
		}
	}

	public final void purge()
	{
		_generalScheduledExecutor.purge();
		_aiScheduledThreadPool.purge();
		_generalExecutor.purge();
		_gameClientPacketsThreadExecutor.purge();
		_moveScheduledThreadPool.purge();
		_loginServerPacketsThreadExecutor.purge();
	}

	public String[] getGPacketStats()
	{
		return ThreadUtil.getStats(_gameClientPacketsThreadExecutor, true);
	}

	public String[] getGeneralPoolStats()
	{
		return ThreadUtil.getStats(_generalScheduledExecutor, true);
	}

	public String[] getMovePoolStats()
	{
		return ThreadUtil.getStats(_moveScheduledThreadPool, true);
	}

	public String[] getAIPoolStats()
	{
		return ThreadUtil.getStats(_aiScheduledThreadPool, true);
	}

	public String[] getLoginServerPacketsThreadPoolStats()
	{
		return ThreadUtil.getStats(_loginServerPacketsThreadExecutor, true);
	}

	public String[] getStats()
	{
		String[] stack = null;
		stack = ArrayUtil.arrayAdd(stack, "ScheduledThreadPool: ", String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_generalScheduledExecutor, false), String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_moveScheduledThreadPool, false), String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_aiScheduledThreadPool, false), String.class);
		stack = ArrayUtil.arrayAdd(stack, "ExecotorThreadPool: ", String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_generalExecutor, false), String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_gameClientPacketsThreadExecutor, false), String.class);
		stack = ArrayUtil.arrayAddToArray(stack, ThreadUtil.getStats(_loginServerPacketsThreadExecutor, false), String.class);
		return stack;
	}

	public long[] getQueue()
	{
		return new long[]
		{
				_generalScheduledExecutor.getTaskCount() - _generalScheduledExecutor.getCompletedTaskCount(),
				_generalExecutor.getTaskCount() - _generalExecutor.getCompletedTaskCount(),
				_moveScheduledThreadPool.getTaskCount() - _moveScheduledThreadPool.getCompletedTaskCount(),
				_aiScheduledThreadPool.getTaskCount() - _aiScheduledThreadPool.getCompletedTaskCount(),
				_gameClientPacketsThreadExecutor.getTaskCount() - _gameClientPacketsThreadExecutor.getCompletedTaskCount()
		};
	}

	public boolean isShutdown()
	{
		return _shutdown;
	}

	public ScheduledThreadPoolExecutor getGeneralScheduledThreadPool()
	{
		return _generalScheduledExecutor;
	}

	public ScheduledThreadPoolExecutor getMoveScheduledThreadPool()
	{
		return _moveScheduledThreadPool;
	}

	public ScheduledThreadPoolExecutor getAiScheduledThreadPool()
	{
		return _aiScheduledThreadPool;
	}

	public ThreadPoolExecutor getInGamePacketsThreadPool()
	{
		return _gameClientPacketsThreadExecutor;
	}
}
