package l2n.game;

import l2n.Config;
import l2n.Server;
import l2n.commons.Universe;
import l2n.commons.threading.RunnableStatsManager;
import l2n.commons.threading.RunnableStatsManager.SortBy;
import l2n.commons.util.StatsUtil;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Scripts;
import l2n.game.geodata.PathFindBuffers;
import l2n.game.instancemanager.CoupleManager;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.itemauction.ItemAuctionManager;
import l2n.game.loginservercon.LSConnection;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2n.game.model.entity.olympiad.OlympiadDatabase;
import l2n.game.network.serverpackets.ExSendUIEvent;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.tables.NpcTable;
import l2n.status.gshandlers.HandlerStats;
import l2n.util.Log;
import l2n.util.StringUtil;
import l2n.util.Util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

public class Shutdown extends Thread
{
	private static final Logger _log = Logger.getLogger(Shutdown.class.getName());

	private static Shutdown _instance;
	private static Shutdown _counterInstance = null;

	private int secondsShut;
	private int shutdownMode;

	public static final int SIGTERM = 0;
	public static final int GM_SHUTDOWN = 1;
	public static final int GM_RESTART = 2;
	public static final int ABORT = 3;

	private final static String[] _modeText = { "Отменены", "Выключен", "Перезагружен", "Отменены" };

	private static boolean show = true;

	public int getSeconds()
	{
		if(_counterInstance != null)
			return _counterInstance.secondsShut;
		return -1;
	}

	public int getMode()
	{
		if(_counterInstance != null)
			return _counterInstance.shutdownMode;
		return -1;
	}

	private void announce(final String text, final int time, final ExShowScreenMessage.ScreenMessageAlign align)
	{
		final ExShowScreenMessage sm = new ExShowScreenMessage(text, time, align, false);
		switch (Config.SHUTDOWN_MSG_TYPE)
		{
			case 1:
				Announcements.announceToAll(text);
				break;
			case 2:
				for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
					player.sendPacket(sm);
				break;
			case 3:
				Announcements.announceToAll(text);
				for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
					player.sendPacket(sm);
		}
	}


	public void startTelnetShutdown(final String IP, final int seconds, final boolean restart)
	{
		_log.info("IP: " + IP + " issued shutdown command in " + seconds + " seconds!");
		announce("Сервер будет " + _modeText[shutdownMode] + " через  " + seconds + " секунд !", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);

		if(_counterInstance != null)
			_counterInstance._abort();
		_counterInstance = new Shutdown(seconds, restart);
		_counterInstance.start();
	}

	public void setAutoRestart(final int seconds)
	{
		_log.info("AutoRestart for  " + Util.formatTime(seconds));

		if(_counterInstance != null)
			_counterInstance._abort();

		_counterInstance = new Shutdown(seconds, true);
		_counterInstance.start();
	}


	public void Telnetabort(final String IP)
	{
		_log.info("IP: " + IP + " issued shutdown ABORT. " + _modeText[shutdownMode] + " has been stopped!");

		if(_counterInstance != null)
		{
			_counterInstance._abort();
			announce("Все действия Администрации " + _modeText[shutdownMode] + " и сервер продолжает работу в нормальном Режиме!", 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER);
		}
	}


	public Shutdown()
	{
		secondsShut = -1;
		shutdownMode = SIGTERM;
	}


	public Shutdown(int seconds, final boolean restart)
	{
		if(seconds < 0)
			seconds = 0;
		secondsShut = seconds;
		if(restart)
			shutdownMode = GM_RESTART;
		else
			shutdownMode = GM_SHUTDOWN;
	}


	public static Shutdown getInstance()
	{
		if(_instance == null)
			_instance = new Shutdown();
		return _instance;
	}


	@Override
	public void run()
	{
		if(this == _instance)
		{
			LSConnection.getInstance().shutdown();
			System.out.println("Shutting down scripts.");
			Scripts.getInstance().shutdown();

			saveData();

			if(Config.ACTIVATE_POSITION_RECORDER)
				try
				{
					Universe.getInstance().implode(true);
					System.err.println("All character postions recored.");
				}
				catch(final Throwable t)
				{
					t.printStackTrace();
				}

			try
			{
				L2GameThreadPools.getInstance().shutdown();
			}
			catch(final Throwable t)
			{}
			LSConnection.getInstance().shutdown();

			try
			{
				System.out.println("Shutting down selector.");
				GameServer.gameServer.getSelectorThread().shutdown();
				GameServer.gameServer.getSelectorThread().setDaemon(true);
			}
			catch(final Throwable t)
			{}

			try
			{
				System.out.println("Shutting down database communication.");
				L2DatabaseFactory.getInstance().shutdown();
			}
			catch(final Throwable t)
			{}

			System.out.println("Shutdown finished.");
			Server.halt(_instance.shutdownMode == GM_RESTART ? 2 : 0, "GS Shutdown");
		}
		else
		{
			countdown();

			switch (shutdownMode)
			{
				case GM_SHUTDOWN:
					_instance.setMode(GM_SHUTDOWN);
					Server.exit(0, "GM_SHUTDOWN");
					break;
				case GM_RESTART:
					_instance.setMode(GM_RESTART);
					Server.exit(2, "GM_RESTART");
					break;
			}
		}
	}

	public void startShutdown(final L2Player activeChar, final int seconds, final boolean restart)
	{
		_log.info("GMAcces: " + activeChar.getName() +  " issued shutdown command in " + seconds + " seconds!");
		
		if(shutdownMode > 0)
			announce("Сервер будет " + _modeText[shutdownMode] + " через " + seconds + " секунд", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
		if(_counterInstance != null)
			_counterInstance._abort();

		_counterInstance = new Shutdown(seconds, restart);

		_counterInstance.start();
	}

	public void abort(final L2Player activeChar)
	{
		_log.info("GMAcces: " + activeChar.getName() + " issued shutdown ABORT has been stopped!");
		
		
		announce("Все действия Администрации " + _modeText[shutdownMode] + " и сервер продолжает работу в нормальном Режиме!", 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER);

		if(_counterInstance != null)
			_counterInstance._abort();
	}

	private void setMode(final int mode)
	{
		shutdownMode = mode;
	}

	private void _abort()
	{
		shutdownMode = ABORT;
	}

	private void countdown()
	{
		while (secondsShut > 0)
			try
			{
				switch (secondsShut)
				{
					case 1800:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 30 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 1500:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 25 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 1200:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 20 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 900:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 15 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 600:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 10 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 300:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 5 минут.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 240:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 4 минуты.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 180:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 3 минуты.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 120:
						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 2 минуты.", 10000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT);
						break;
					case 60:
						final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
						final String file = "server_stats_" + datetimeFormatter.format(System.currentTimeMillis());

						Log.addDev(L2ObjectsStorage.getStats().toString(), file, false);
						Log.addDev(StringUtil.EMPTY_STRING, file, false);
						Log.addDev(PathFindBuffers.getStats().toString(), file, false);
						Log.addDev(StringUtil.EMPTY_STRING, file, false);
						Log.addDev(L2DatabaseFactory.getInstance().getStats(), file, false);
						Log.addDev(StringUtil.EMPTY_STRING, file, false);
						Log.addDev(HandlerStats.getGCStats().toString(), file, false);
						Log.addDev(StringUtil.EMPTY_STRING, file, false);
						Log.addDev(StatsUtil.getThreadStats().toString(), file, false);

						System.out.println("Server stats successfully dumped.");
						System.out.println("See file {./game/log/developer/" + file + ".txt}");

						announce("Внимание Игроки! Сервер Будет " + _modeText[shutdownMode] + " через 1 минуту.", 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER);
						if(!Config.DONTLOADSPAWN)
							try
							{
								L2World.deleteVisibleNpcSpawns();
							}
							catch(final Throwable t)
							{
								System.out.println("Error while unspawn Npcs!");
								t.printStackTrace();
							}
						break;
					case 50:
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							player.sendPacket(new ExSendUIEvent(player, false, false, secondsShut - 1, 0, "Внимание Сервер Будет " + _modeText[shutdownMode] + " Через:"));
						break;
					case 40:
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							player.sendPacket(new ExSendUIEvent(player, false, false, secondsShut - 1, 0, "Внимание Сервер Будет " + _modeText[shutdownMode] + " Через:"));
						break;
					case 30:
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							player.sendPacket(new ExSendUIEvent(player, false, false, secondsShut - 1, 0, "Внимание Сервер Будет " + _modeText[shutdownMode] + " Через:"));
						break;
					case 20:
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							player.sendPacket(new ExSendUIEvent(player, false, false, secondsShut - 1, 0, "Внимание Сервер Будет " + _modeText[shutdownMode] + " Через:"));
						break;
					case 10:
						disconnectAllCharacters();
						break;
				}

				secondsShut--;

				final int delay = 1000;
				Thread.sleep(delay);

				if(shutdownMode == ABORT)
					break;
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
	}

	private void saveData()
	{
		switch (shutdownMode)
		{
			case SIGTERM:
				System.err.println("SIGTERM received. Shutting down NOW!");
				break;
			case GM_SHUTDOWN:
				System.err.println("GM shutdown received. Shutting down NOW!");
				break;
			case GM_RESTART:
				System.err.println("GM restart received. Restarting NOW!");
				break;
		}

		disconnectAllCharacters();
		if(!SevenSigns.getInstance().isSealValidationPeriod())
		{
			SevenSignsFestival.getInstance().saveFestivalData(false);
			System.out.println("SevenSignsFestival: data saved.");
		}

		SevenSigns.getInstance().saveSevenSignsData(0, true);
		System.out.println("SevenSigns: data saved.");

		RunnableStatsManager.dumpClassStats(SortBy.MAX);
		System.out.println("RunnableStatsManager: Method stats successfully dumped.");

		try
		{
			if(Config.ENABLE_OLYMPIAD)
			{
				OlympiadDatabase.save();
				System.out.println("Olympiad System: Data saved!");
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		if(Config.ALLOW_WEDDING)
		{
			CoupleManager.getInstance().store();
			System.out.println("Couples: Data saved!");
		}

		if(Config.ALLOW_CURSED_WEAPONS)
		{
			CursedWeaponsManager.getInstance().saveData();
			System.out.println("CursedWeaponsManager: Data saved!");
		}

		if(Config.KILL_COUNTER)
		{
			NpcTable.storeKillsCount();
			System.out.println("Kill counter saved!");
		}

		if(Config.ALT_ITEM_AUCTION_ENABLED)
			try
			{
				ItemAuctionManager.getInstance().shutdown();
				System.out.println("ItemAuctionManager: shutdown.");
			}
			catch(final Throwable e)
			{
				e.printStackTrace();
			}

		setAllCharacterOffline();
		System.out.println("Clear characters online status and accesslevel.");

		System.out.println("All Data saved. All players disconnected, shutting down.");
		try
		{
			final int delay = 5000;
			Thread.sleep(delay);
		}
		catch(final InterruptedException e)
		{
		}
	}


	private void disconnectAllCharacters()
	{
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
		
			try
			{
				player.logout(true, false, false, true);
			}
			catch(final Throwable t)
			{}
		try
		{
			Thread.sleep(1000);
		}
		catch(final Throwable t)
		{
			System.out.println(t);
		}
	}

	private void setAllCharacterOffline()
	{
		ThreadConnection conn = null;
		FiltredStatement stmt = null;
		try
		{
			conn = L2DatabaseFactory.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE characters SET online = 0");
			stmt.executeUpdate("UPDATE characters SET accesslevel = 0 WHERE accesslevel = -1");
		}
		catch(final SQLException e)
		{}
		finally
		{
			DbUtils.closeQuietly(conn, stmt);
		}
	}
}
