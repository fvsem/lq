package l2n.game;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2ObjectProcedures.SendCustomMessageProc;
import l2n.game.model.L2ObjectProcedures.SendPacketProc;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.MapRegionTable;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Announcements
{
	private final static Logger _log = Logger.getLogger(Announcements.class.getName());

	private final GArray<String> _announcements = new GArray<String>();

	public Announcements()
	{
		loadAnnouncements();
	}

	public static Announcements getInstance()
	{
		return SingletonHolder._instance;
	}

	public void loadAnnouncements()
	{
		_announcements.clear();
		final File file = new File("./", "config/announcements.txt");
		if(file.exists())
			readFromDisk(file);
		else
			_log.config("config/announcements.txt doesn't exist");
	}

	public void showAnnouncements(final L2Player activeChar)
	{
		for(final String _announcement : _announcements)
		{
			final CreatureSay cs = new CreatureSay(0, Say2C.ANNOUNCEMENT, activeChar.getName(), _announcement);
			activeChar.sendPacket(cs);
		}
	}

	public void listAnnouncements(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center>Меню Анонсов</center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<center>Add or announce a new announcement:</center>");
		replyMSG.append("<center><multiedit var=\"new_announcement\" width=240 height=30></center><br>");
		replyMSG.append("<center><table><tr><td>");
		replyMSG.append("<button value=\"Add\" action=\"bypass -h admin_add_announcement $new_announcement\" width=60 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td><td>");
		replyMSG.append("<button value=\"Announce\" action=\"bypass -h admin_announce_menu $new_announcement\" width=60 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td><td>");
		replyMSG.append("<button value=\"Reload\" action=\"bypass -h admin_announce_announcements\" width=60 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
		replyMSG.append("</td></tr></table></center>");
		replyMSG.append("<br>");
		for(int i = 0; i < _announcements.size(); i++)
		{
			replyMSG.append("<table width=260><tr><td width=220>" + _announcements.get(i) + "</td><td width=40>");
			replyMSG.append("<button value=\"Delete\" action=\"bypass -h admin_del_announcement " + i + "\" width=60 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr></table>");
		}
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	public void addAnnouncement(final String text)
	{
		_announcements.add(text);
		saveToDisk();
	}

	public void delAnnouncement(final int line)
	{
		_announcements.remove(line);
		saveToDisk();
	}

	private void readFromDisk(final File file)
	{
		LineNumberReader lnr = null;
		try
		{
			String line;
			lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));

			while ((line = lnr.readLine()) != null)
			{
				final StringTokenizer st = new StringTokenizer(line, "\n\r");
				if(st.hasMoreTokens())
				{
					final String announcement = st.nextToken();
					_announcements.add(announcement);

				}
			}
		}
		catch(final IOException e1)
		{
			_log.log(Level.SEVERE, "Error reading news", e1);
		}
		finally
		{
			try
			{
				if(lnr != null)
					lnr.close();
			}
			catch(final Exception e2)
			{}
		}
	}

	private void saveToDisk()
	{
		final File file = new File("config/announcements.txt");
		FileWriter save = null;

		try
		{
			save = new FileWriter(file);
			for(final String _announcement : _announcements)
			{
				save.write(_announcement);
				save.write("\r\n");
			}
		}
		catch(final IOException e)
		{
			_log.warning("saving the news file has failed: " + e);
		}
		finally
		{
			try
			{
				if(save != null)
					save.close();
			}
			catch(final Exception e1)
			{}
		}
	}

	public static void shout(final L2Character activeChar, final String text, final int type)
	{
		final CreatureSay cs = new CreatureSay(activeChar.getObjectId(), type, activeChar.getName(), text);
		final int mapregion = MapRegionTable.getInstance().getMapRegion(activeChar.getX(), activeChar.getY());
		if(Config.SHOUT_CHAT_MODE == 1)
			for(final L2Player player : L2World.getAroundPlayers(activeChar))
				player.sendPacket(cs);
		else
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(MapRegionTable.getInstance().getMapRegion(player.getX(), player.getY()) == mapregion && player != activeChar)
					player.sendPacket(cs);
		activeChar.sendPacket(cs);
	}

	public static void announceByCustomMessage(final String address, final String... replacements)
	{
		announceByCustomMessage(address, replacements, Say2C.ANNOUNCEMENT);
	}

	public static void announceByCustomMessage(final String address, final String[] replacements, final int type)
	{
		L2ObjectsStorage.forEachPlayer(new SendCustomMessageProc(address, replacements, type));
	}

	public static void announceToPlayerByCustomMessage(final L2Player player, final String address, final String[] replacements)
	{
		announceToPlayerByCustomMessage(player, address, replacements, Say2C.ANNOUNCEMENT);
	}

	public static void announceToPlayerByCustomMessage(final L2Player player, final String address, final String[] replacements, final int type)
	{
		final CustomMessage cm = new CustomMessage(address, player);
		if(replacements != null)
			for(final String s : replacements)
				cm.addString(s);
		player.sendPacket(new CreatureSay(0, type, "", cm.toString()));
	}

	public static final void announceToAll(final String text)
	{
		announceToAll(text, Say2C.ANNOUNCEMENT);
	}
	public static final void announceToPlayer(final L2Player player, final String text)
	{
		player.sendPacket(new CreatureSay(0, Say2C.ANNOUNCEMENT, StringUtils.EMPTY, text));
	}
	public static final void announceToAll(final String text, final int type)
	{
		announceToAll(new CreatureSay(0, type, StringUtils.EMPTY, text));
	}

	public static final void announceToAll(final SystemMessage sm)
	{
		L2ObjectsStorage.forEachPlayer(new SendPacketProc(sm));
	}

	public static final void announceToAll(final CreatureSay cs)
	{
		L2ObjectsStorage.forEachPlayer(new SendPacketProc(cs));
	}

	public void handleAnnounce(final String command, final int lengthToTrim, final String gmName)
	{
		handleAnnounce(command, lengthToTrim, Say2C.ANNOUNCEMENT, gmName);
	}

	public void handleAnnounce(final String command, final int lengthToTrim, final int type, final String gmName)
	{
		try
		{
			String text = command.substring(lengthToTrim);
			if(gmName != null)
				text += " [" + gmName + "]";
			announceToAll(text, type);
		}
		catch(final StringIndexOutOfBoundsException e)
		{
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final Announcements _instance = new Announcements();
	}
}
