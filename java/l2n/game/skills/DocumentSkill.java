package l2n.game.skills;

import gnu.trove.set.hash.TIntHashSet;
import javolution.text.TypeFormat;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.skills.conditions.Condition;
import l2n.game.tables.SkillTreeTable;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

public final class DocumentSkill extends DocumentBase
{
	private static class Skill
	{
		public int id;
		public String name;
		public StatsSet[][] sets = new StatsSet[9][];
		public int currentLevel;
		public GArray<L2Skill> skills = new GArray<L2Skill>();
		public GArray<L2Skill> currentSkills = new GArray<L2Skill>();
	}

	private Skill _currentSkill;
	private final List<L2Skill> _skillsInFile = new LinkedList<L2Skill>();
	private final TIntHashSet usedTables = new TIntHashSet();

	public DocumentSkill(final File file)
	{
		super(file);
	}

	@Override
	protected void resetTable()
	{
		if(!usedTables.isEmpty())
			for(final int table : tables.keys())
				if(!usedTables.contains(table))
					System.out.println("WARNING: Unused table " + table + " for skill " + _currentSkill.id);
		usedTables.clear();
		super.resetTable();
	}

	private void setCurrentSkill(final Skill skill)
	{
		_currentSkill = skill;
	}

	protected List<L2Skill> getSkills()
	{
		return _skillsInFile;
	}

	@Override
	protected Number getTableValue(final String name)
	{
		try
		{
			usedTables.add(name.hashCode());
			final Number[] a = tables.get(name.hashCode());
			if(a.length - 1 >= _currentSkill.currentLevel)
				return a[_currentSkill.currentLevel];
			return a[a.length - 1];
		}
		catch(final RuntimeException e)
		{
			_log.log(Level.SEVERE, "error in table of skill Id " + _currentSkill.id, e);
			return 0;
		}
	}

	@Override
	protected Number getTableValue(final String name, final int idx)
	{
		try
		{
			usedTables.add(name.hashCode());
			final Number[] a = tables.get(name.hashCode());
			if(idx < a.length)
				return a[idx - 1];
			return a[a.length - 1];
		}
		catch(final RuntimeException e)
		{
			_log.log(Level.SEVERE, "wrong level count in skill Id " + _currentSkill.id + " table " + name + " level " + idx, e);
			return 0;
		}
	}

	@Override
	protected void parseDocument(final Document doc)
	{
		for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
			if("list".equalsIgnoreCase(n.getNodeName()))
			{
				for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					if("skill".equalsIgnoreCase(d.getNodeName()))
					{
						setCurrentSkill(new Skill());
						parseSkill(d);
						_skillsInFile.addAll(_currentSkill.skills);
						resetTable();
					}
			}
			else if("skill".equalsIgnoreCase(n.getNodeName()))
			{
				setCurrentSkill(new Skill());
				parseSkill(n);
				_skillsInFile.addAll(_currentSkill.skills);
			}
	}

	protected void parseSkill(Node n)
	{
		final NamedNodeMap attrs = n.getAttributes();
		int enchantLevels1 = 0;
		int enchantLevels2 = 0;
		int enchantLevels3 = 0;
		int enchantLevels4 = 0;
		int enchantLevels5 = 0;
		int enchantLevels6 = 0;
		int enchantLevels7 = 0;
		int enchantLevels8 = 0;
		final int skillId = TypeFormat.parseInt(attrs.getNamedItem("id").getNodeValue());
		final String skillName = attrs.getNamedItem("name").getNodeValue();
		final int lastLvl = TypeFormat.parseInt(attrs.getNamedItem("levels").getNodeValue());

		if(attrs.getNamedItem("enchantLevels1") != null)
		{
			enchantLevels1 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels1").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels1, 100);
		}
		if(attrs.getNamedItem("enchantLevels2") != null)
		{
			enchantLevels2 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels2").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels2, 200);
		}
		if(attrs.getNamedItem("enchantLevels3") != null)
		{
			enchantLevels3 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels3").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels3, 300);
		}
		if(attrs.getNamedItem("enchantLevels4") != null)
		{
			enchantLevels4 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels4").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels4, 400);
		}
		if(attrs.getNamedItem("enchantLevels5") != null)
		{
			enchantLevels5 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels5").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels5, 500);
		}
		if(attrs.getNamedItem("enchantLevels6") != null)
		{
			enchantLevels6 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels6").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels6, 600);
		}
		if(attrs.getNamedItem("enchantLevels7") != null)
		{
			enchantLevels7 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels7").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels7, 700);
		}
		if(attrs.getNamedItem("enchantLevels8") != null)
		{
			enchantLevels8 = TypeFormat.parseInt(attrs.getNamedItem("enchantLevels8").getNodeValue());
			addEnchantSkills(skillId, skillName, lastLvl, enchantLevels8, 800);
		}

		_currentSkill.id = skillId;
		_currentSkill.name = skillName;
		_currentSkill.sets[0] = new StatsSet[lastLvl];
		_currentSkill.sets[1] = new StatsSet[enchantLevels1];
		_currentSkill.sets[2] = new StatsSet[enchantLevels2];
		_currentSkill.sets[3] = new StatsSet[enchantLevels3];
		_currentSkill.sets[4] = new StatsSet[enchantLevels4];
		_currentSkill.sets[5] = new StatsSet[enchantLevels5];
		_currentSkill.sets[6] = new StatsSet[enchantLevels6];
		_currentSkill.sets[7] = new StatsSet[enchantLevels7];
		_currentSkill.sets[8] = new StatsSet[enchantLevels8];

		for(int i = 0; i < lastLvl; i++)
		{
			_currentSkill.sets[0][i] = new StatsSet();
			_currentSkill.sets[0][i].set("skill_id", _currentSkill.id);
			_currentSkill.sets[0][i].set("level", i + 1);
			_currentSkill.sets[0][i].set("name", _currentSkill.name);
		}

		if(_currentSkill.sets[0].length != lastLvl)
			throw new RuntimeException("Skill id=" + skillId + " number of levels missmatch, " + lastLvl + " levels expected");

		final Node first = n.getFirstChild();
		for(n = first; n != null; n = n.getNextSibling())
			if("table".equalsIgnoreCase(n.getNodeName()))
				parseTable(n);

		for(int i = 1; i <= lastLvl; i++)
			for(n = first; n != null; n = n.getNextSibling())
				if("set".equalsIgnoreCase(n.getNodeName()))
					parseBeanSet(n, _currentSkill.sets[0][i - 1], i);

		parseSkill(n, first, skillId, enchantLevels1, 1);
		parseSkill(n, first, skillId, enchantLevels2, 2);
		parseSkill(n, first, skillId, enchantLevels3, 3);
		parseSkill(n, first, skillId, enchantLevels4, 4);
		parseSkill(n, first, skillId, enchantLevels5, 5);
		parseSkill(n, first, skillId, enchantLevels6, 6);
		parseSkill(n, first, skillId, enchantLevels7, 7);
		parseSkill(n, first, skillId, enchantLevels8, 8);

		// FIXME
		makeSkills();

		for(int i = 0; i < lastLvl; i++)
		{
			_currentSkill.currentLevel = i;
			for(n = first; n != null; n = n.getNextSibling())
			{
				if("cond".equalsIgnoreCase(n.getNodeName()))
				{
					final Condition condition = parseCondition(n.getFirstChild());
					final Node msg = n.getAttributes().getNamedItem("msg");
					if(condition != null && msg != null)
						condition.setMessage(msg.getNodeValue());
					final Node msgId = n.getAttributes().getNamedItem("msgId");
					if(condition != null && msg != null)
						condition.setMessage(msg.getNodeValue());
					else if(condition != null && msgId != null)
					{
						condition.setMessageId(Integer.decode(msgId.getNodeValue()));
						final Node addName = n.getAttributes().getNamedItem("addName");
						if(addName != null && Integer.decode(msgId.getNodeValue()) > 0)
							condition.addName();
					}
					_currentSkill.currentSkills.get(i).attach(condition);
				}
				if("for".equalsIgnoreCase(n.getNodeName()))
					parseTemplate(n, _currentSkill.currentSkills.get(i));
			}
		}

		parseEnchSkill(1, n, first, lastLvl, lastLvl + enchantLevels1, enchantLevels1, lastLvl);
		parseEnchSkill(2, n, first, lastLvl + enchantLevels1, lastLvl + enchantLevels1 + enchantLevels2, enchantLevels2, lastLvl);
		parseEnchSkill(3, n, first, lastLvl + enchantLevels1 + enchantLevels2, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3, enchantLevels3, lastLvl);
		parseEnchSkill(4, n, first, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4, enchantLevels4, lastLvl);
		parseEnchSkill(5, n, first, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5, enchantLevels5, lastLvl);
		parseEnchSkill(6, n, first, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5 + enchantLevels6, enchantLevels6, lastLvl);
		parseEnchSkill(7, n, first, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5 + enchantLevels6, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5 + enchantLevels6 + enchantLevels7, enchantLevels7, lastLvl);
		parseEnchSkill(8, n, first, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5 + enchantLevels6 + enchantLevels7, lastLvl + enchantLevels1 + enchantLevels2 + enchantLevels3 + enchantLevels4 + enchantLevels5 + enchantLevels6 + enchantLevels7 + enchantLevels8, enchantLevels8, lastLvl);

		_currentSkill.skills.addAll(_currentSkill.currentSkills);
	}

	private void makeSkills()
	{
		int size = 0;
		for(final StatsSet[] arr : _currentSkill.sets)
			size += arr.length;
		_currentSkill.currentSkills = new GArray<L2Skill>(size);

		int _count = makeSkill(0, 0);
		_count = makeSkill(1, _count);
		_count = makeSkill(2, _count);
		_count = makeSkill(3, _count);
		_count = makeSkill(4, _count);
		_count = makeSkill(5, _count);
		_count = makeSkill(6, _count);
		_count = makeSkill(7, _count);
		_count = makeSkill(8, _count);
	}

	private void addEnchantSkills(final int id, final String name, final int baseLvl, final int maxLvl, final int num)
	{
		for(int i = 1; i <= maxLvl; i++)
		{
			final int level = num + i;
			final int minSkillLvl = i == 1 ? baseLvl : level - 1;
			final L2EnchantSkillLearn e = new L2EnchantSkillLearn(id, level, name, minSkillLvl, baseLvl, maxLvl);

			GArray<L2EnchantSkillLearn> skills = SkillTreeTable._enchantSkillTrees.get(id);
			if(skills == null)
				skills = new GArray<L2EnchantSkillLearn>();
			skills.add(e);

			SkillTreeTable._enchantSkillTrees.put(id, skills);
		}
	}

	private int makeSkill(final int enchGroup, final int n)
	{
		int count = n;
		for(int i = 0; i < _currentSkill.sets[enchGroup].length; i++)
			try
			{
				_currentSkill.currentSkills.add(n + i, _currentSkill.sets[enchGroup][i].getEnum("skillType", SkillType.class).makeSkill(_currentSkill.sets[enchGroup][i]));
				count++;
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "Skill id=" + _currentSkill.sets[enchGroup][i].getEnum("skillType", SkillType.class).makeSkill(_currentSkill.sets[enchGroup][i]).getDisplayId() + " level=" + _currentSkill.sets[enchGroup][i].getEnum("skillType", SkillType.class).makeSkill(_currentSkill.sets[enchGroup][i]).getLevel(), e);
			}

		return count;
	}

	private void parseSkill(Node n, final Node first, final int skillId, final int enchantLevels, final int enchGroup)
	{
		final String enchant = new StringBuilder("enchant").append(enchGroup).toString();
		for(int i = 0; i < enchantLevels; i++)
		{
			_currentSkill.sets[enchGroup][i] = new StatsSet();
			_currentSkill.sets[enchGroup][i].set("skill_id", _currentSkill.id);
			_currentSkill.sets[enchGroup][i].set("level", i + enchGroup * 100 + 1);
			_currentSkill.sets[enchGroup][i].set("name", _currentSkill.name);

			for(n = first; n != null; n = n.getNextSibling())
				if("set".equalsIgnoreCase(n.getNodeName()))
					parseBeanSet(n, _currentSkill.sets[enchGroup][i], _currentSkill.sets[0].length);

			for(n = first; n != null; n = n.getNextSibling())
				if(enchant.equalsIgnoreCase(n.getNodeName()))
					parseBeanSet(n, _currentSkill.sets[enchGroup][i], i + 1);
		}

		if(_currentSkill.sets[enchGroup].length != enchantLevels)
			throw new RuntimeException("Skill id=" + skillId + " number of levels missmatch, " + enchantLevels + " levels expected");
	}

	private void parseEnchSkill(final int enchGroup, Node n, final Node first, final int enchMin, final int enchMax, final int enchantLevels, final int maxNormal)
	{
		final String enchantCond = new StringBuilder("enchant").append(enchGroup).append("cond").toString();
		final String enchantFor = new StringBuilder("enchant").append(enchGroup).append("for").toString();
		for(int i = enchMin; i < enchMax; i++)
		{
			boolean foundCond = false, foundFor = false;
			_currentSkill.currentLevel = i - enchMin;
			for(n = first; n != null; n = n.getNextSibling())
			{
				if(enchantCond.equalsIgnoreCase(n.getNodeName()))
				{
					foundCond = true;
					final Condition condition = parseCondition(n.getFirstChild());
					final Node msg = n.getAttributes().getNamedItem("msg");
					if(condition != null && msg != null)
						condition.setMessage(msg.getNodeValue());
					_currentSkill.currentSkills.get(i).attach(condition);
				}
				if(enchantFor.equalsIgnoreCase(n.getNodeName()))
				{
					foundFor = true;
					parseTemplate(n, _currentSkill.currentSkills.get(i));
				}
			}
			if(!foundCond || !foundFor)
			{
				_currentSkill.currentLevel = maxNormal - 1;
				for(n = first; n != null; n = n.getNextSibling())
					// Если не найдены условия для заточенного скила, то берём условия из обычного скила максимального уровня
					if(!foundCond && "cond".equalsIgnoreCase(n.getNodeName()))
					{
						final Condition condition = parseCondition(n.getFirstChild());
						final Node msg = n.getAttributes().getNamedItem("msg");
						if(condition != null && msg != null)
							condition.setMessage(msg.getNodeValue());
						_currentSkill.currentSkills.get(i).attach(condition);
					}
					// Если эффект для заточенного скила не найден, то берём эффект из обычного скила максимального уровня
					else if(!foundFor && "for".equalsIgnoreCase(n.getNodeName()))
						parseTemplate(n, _currentSkill.currentSkills.get(i));
			}

			// Устанавливаем количесьво уровней заточки для скила
			_currentSkill.currentSkills.get(i).setEnchantLevelCount(enchantLevels);
		}
	}
}
