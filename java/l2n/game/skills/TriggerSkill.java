package l2n.game.skills;

import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.TriggerActionType;
import l2n.game.model.actor.L2Character;

public class TriggerSkill
{
	public final L2Skill skill;
	public boolean isReuse = false;

	public TriggerSkill(final L2Skill sk, final boolean setReuse)
	{
		skill = sk;
		isReuse = setReuse;
	}

	public final int getId()
	{
		return skill.getId();
	}

	public final int getReuseDelay()
	{
		return skill.getReuseDelay();
	}

	public L2Character[] getTargets(final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		return skill.getTargets(activeChar, aimingTarget, forceUse);
	}

	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		return skill.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public double getChanceForAction(final TriggerActionType action)
	{
		return skill.getChanceForAction(action);
	}

	public double getMinDamageForAction(final TriggerActionType action)
	{
		return skill.getMinDamageForAction(action);
	}

	public final L2Character getAimingTarget(final L2Character activeChar, final L2Object obj)
	{
		return skill.getAimingTarget(activeChar, obj);
	}
}
