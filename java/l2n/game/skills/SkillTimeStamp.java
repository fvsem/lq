package l2n.game.skills;

import l2n.game.model.L2Skill;

/**
 * Simple class containing all neccessary information to maintain
 * valid timestamps and reuse for skills upon relog. Filter this
 * carefully as it becomes redundant to store reuse for small delays.
 */
public final class SkillTimeStamp
{
	private final int _id;
	private final int _level;
	private final long _reuse;
	private final long _endTime;

	public SkillTimeStamp(final L2Skill skill, final long reuse)
	{
		this(skill, System.currentTimeMillis() + reuse, reuse);
	}

	public SkillTimeStamp(final L2Skill skill, final long endTime, final long reuse)
	{
		this(skill.getId(), skill.getLevel(), endTime, reuse);
	}

	public SkillTimeStamp(final int id, final int level, final long reuse)
	{
		this(id, level, System.currentTimeMillis() + reuse, reuse);
	}

	public SkillTimeStamp(final int id, final int level, final long endTime, final long reuse)
	{
		_id = id;
		_level = level;
		_reuse = reuse;
		_endTime = endTime;
	}

	public long getReuseBasic()
	{
		if(_reuse == 0)
			return getReuseCurrent();
		return _reuse;
	}

	/**
	 * Возвращает оставшееся время реюза в миллисекундах.
	 */
	public long getReuseCurrent()
	{
		return Math.max(_endTime - System.currentTimeMillis(), 0);
	}

	public long getEndTime()
	{
		return _endTime;
	}

	public int getId()
	{
		return _id;
	}

	public int getLevel()
	{
		return _level;
	}

	/**
	 * Check if the reuse delay has passed and
	 * if it has not then update the stored reuse time
	 * according to what is currently remaining on
	 * the delay.
	 */
	public boolean hasNotPassed()
	{
		return System.currentTimeMillis() < _endTime;
	}
}
