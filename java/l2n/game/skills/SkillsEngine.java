package l2n.game.skills;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.set.hash.TIntHashSet;
import l2n.commons.list.GArray;
import l2n.game.model.L2Skill;
import l2n.game.tables.SkillTable;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SkillsEngine
{
	private final static Logger _log = Logger.getLogger(SkillsEngine.class.getName());

	private final static SkillsEngine _instance = new SkillsEngine();

	private final List<File> _skillFiles;

	public static SkillsEngine getInstance()
	{
		return _instance;
	}

	private SkillsEngine()
	{
		_skillFiles = new LinkedList<File>();
		hashFiles("data/stats/skills", _skillFiles);
	}
	private void hashFiles(final String dirname, final List<File> hash)
	{
		final File dir = new File(dirname);
		if(!dir.exists())
			_log.warning("SkillsEngine: dir " + dir.getAbsolutePath() + " not exists");
		else
		{
			final File[] files = dir.listFiles();
			for(final File f : files)
				if(f.getName().endsWith(".xml"))
					hash.add(f);
		}
	}

	private List<L2Skill> loadSkills(final File file)
	{
		if(file == null)
		{
			_log.warning("SkillsEngine: file not found!");
			return null;
		}
		final DocumentSkill doc = new DocumentSkill(file);
		doc.parse();
		return doc.getSkills();
	}

	public L2Skill[][][] loadAllSkills()
	{
		final TIntObjectHashMap<GArray<GArray<L2Skill>>> allSkills = new TIntObjectHashMap<GArray<GArray<L2Skill>>>();
		int maxSkillId = Integer.MIN_VALUE;

		int count = 0;
		int ccount = 0;
		int rcount = 0;
		final TIntHashSet skillIds = new TIntHashSet();
		
		// загружаем скилы
		for(final File file : _skillFiles)
		{
			final List<L2Skill> skills = loadSkills(file);
			if(skills == null)
				continue;

			for(final L2Skill skill : skills)
			{
				if(skillIds.contains(skill.getId()))
				{
					rcount++;
					continue;
				}

				if(maxSkillId < skill.getId())
					maxSkillId = skill.getId();

				treeInsert(skill, allSkills);
				count++;
			}
		}

		final L2Skill[][][] converted = new L2Skill[maxSkillId][][];
		for(int i = converted.length; i-- > 0;)
			converted[i] = SkillTable.EMPTY_ARRAY_2D;

		allSkills.forEachEntry(new TIntObjectProcedure<GArray<GArray<L2Skill>>>()
		{
			@Override
			public final boolean execute(int skillId, final GArray<GArray<L2Skill>> trees)
			{
				if(trees.isEmpty())
					converted[skillId - 1] = SkillTable.EMPTY_ARRAY_2D;
				else
				{
					GArray<L2Skill> levels;
					converted[--skillId] = new L2Skill[trees.size()][];
					for(int tree = trees.size(); tree-- > 0;)
					{
						levels = trees.getUnsafe(tree);
						if(levels.isEmpty())
							converted[skillId][tree] = SkillTable.EMPTY_ARRAY;
						else
						{
							converted[skillId][tree] = new L2Skill[levels.size()];
							for(int level = levels.size(); level-- > 0;)
								converted[skillId][tree][level] = levels.getUnsafe(level);
						}
					}
				}
				return true;
			}
		});

		_log.log(Level.INFO, "SkillsEngine: Loaded " + count + " templates from main XML files.");
		if(skillIds.size() > 0)
		{
			_log.log(Level.INFO, "SkillsEngine: Loaded " + ccount + " custom templates, " + rcount + " overriden.");
			_log.log(Level.INFO, "SkillsEngine: Loaded " + skillIds.size() + " custom skills from XML files.");
		}
		_log.log(Level.INFO, "SkillsEngine: Max id " + maxSkillId);

		return converted;
	}

	private final void treeInsert(final L2Skill skill, final TIntObjectHashMap<GArray<GArray<L2Skill>>> allSkills)
	{
		GArray<GArray<L2Skill>> trees = allSkills.get(skill.getId());
		if(trees == null)
		{
			trees = new GArray<GArray<L2Skill>>(2);
			allSkills.put(skill.getId(), trees);
		}

		final int tree = skill.getLevel() / 100;
		final int level = skill.getLevel() % 100;

		trees.ensureCapacity(trees.size() + tree - trees.size() + 1);
		for(int i = tree - trees.size() + 1; i-- > 0;)
			trees.addLastUnsafe(new GArray<L2Skill>(2));

		final GArray<L2Skill> levels = trees.getUnsafe(tree);
		levels.ensureCapacity(levels.size() + level - levels.size());
		for(int i = level - levels.size(); i-- > 0;)
			levels.addLastUnsafe(null);

		levels.set(level - 1, skill);
	}
}
