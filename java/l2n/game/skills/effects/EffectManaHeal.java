package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class EffectManaHeal extends L2Effect
{
	public EffectManaHeal(final Env env, final EffectTemplate template)
	{
		super(env, template);
		if(_effected.isDead() || _effected.isHealMPBlocked(true))
			return;
		final double mp = calc();
		final double newMp = Math.min(mp * 1.7, _effected.calcStat(Stats.MANAHEAL_EFFECTIVNESS, mp, _effector, getSkill()));
		final double addToMp = Math.max(0, Math.min(newMp, _effected.calcStat(Stats.MP_LIMIT, 100, null, null) * _effected.getMaxMp() / 100. - _effected.getCurrentMp()));
		_effected.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToMp)));
		if(addToMp > 0)
			_effected.setCurrentMp(addToMp + _effected.getCurrentMp());
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
