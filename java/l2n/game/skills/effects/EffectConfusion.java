package l2n.game.skills.effects;

import l2n.commons.list.GArray;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Env;
import l2n.util.Rnd;

public final class EffectConfusion extends L2Effect
{
	public EffectConfusion(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		if(!getEffected().isPlayer())
		{
			getEffected().startConfused();
			onActionTime();
		}
	}

	@Override
	public void onExit()
	{
		super.onExit();
		if(!getEffected().isPlayer())
			getEffected().stopConfused();
	}

	@Override
	public boolean onActionTime()
	{
		final GArray<L2Character> targetList = new GArray<L2Character>();
		// Getting the possible targets
		for(final L2Character obj : _effected.getAroundCharacters(300, 200))
			if(obj.isMonster() && obj != _effected && obj != _effector)
				targetList.add(obj);

		// if there is no target, exit function
		if(targetList.size() == 0)
			return true;

		// Choosing randomly a new target
		final L2Character target = targetList.get(Rnd.get(targetList.size()));

		// Attacking the target
		getEffected().setRunning();
		getEffected().getAI().Attack(target, true, false);

		return true;
	}
}
