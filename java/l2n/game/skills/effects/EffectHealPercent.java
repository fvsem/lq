package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class EffectHealPercent extends L2Effect
{
	public EffectHealPercent(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effector.isPlayable() && _effected.isMonster())
			return false;

		return super.checkCondition();
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead())
			return false;

		final double base = calc() * _effected.getMaxHp() / 100;
		final double newHp = base * _effected.calcStat(Stats.HEAL_EFFECTIVNESS, 100, null, null) / 100;
		final double addToHp = Math.max(0, Math.min(newHp, _effected.calcStat(Stats.HP_LIMIT, 100, null, null) * _effected.getMaxHp() / 100. - _effected.getCurrentHp()));

		if(_effected.isPlayer())
			if(_effected != _effector)
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_HP_HAS_BEEN_RESTORED_BY_XS2S).addString(_effector.getName()).addNumber(Math.round(addToHp)));
			else
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_HPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToHp)));
		if(addToHp > 0)
			_effected.setCurrentHp(addToHp + _effected.getCurrentHp(), false);
		return false;
	}
}
