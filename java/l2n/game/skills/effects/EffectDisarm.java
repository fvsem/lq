package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.skills.Env;

public final class EffectDisarm extends L2Effect
{
	public EffectDisarm(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(!_effected.isPlayer())
			return false;
		final L2Player player = _effected.getPlayer();
		if(player == null)
			return false;
		return super.checkCondition();
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		final L2Player player = _effected.getPlayer();
		if(player != null)
		{
			if(!player.isCursedWeaponEquipped() && !player.isCombatFlagEquipped() && !player.isTerritoryFlagEquipped())
			{
				final L2ItemInstance weapon = player.getActiveWeaponInstance();
				if(weapon != null)
					player.getInventory().unEquipItemInBodySlotAndNotify(weapon.getBodyPart(), weapon);
			}
		}
	}

	@Override
	public boolean onActionTime()
	{
		// just stop this effect
		return false;
	}
}
