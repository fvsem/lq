package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.BeginRotation;
import l2n.game.network.serverpackets.StopRotation;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;

public final class EffectTurner extends L2Effect
{
	public EffectTurner(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		getEffected().broadcastPacket(new BeginRotation(getEffected(), getEffected().getHeading(), 1, 65535));
		getEffected().broadcastPacket(new StopRotation(getEffected(), getEffector().getHeading(), 65535));
		getEffected().setHeading(getEffector().getHeading());
		getEffected().sendPacket(new SystemMessage(SystemMessage.YOU_CAN_FEEL_S1S_EFFECT).addSkillName(_displayId, _displayLevel));
		getEffected().startStunning();
	}

	@Override
	public void onExit()
	{
		super.onExit();
		getEffected().stopStunning();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
