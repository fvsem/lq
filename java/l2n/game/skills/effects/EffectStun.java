package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectStun extends L2Effect
{
	public EffectStun(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		_effected.startStunning();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopStunning();
	}

	@Override
	public boolean onActionTime()
	{
		// just stop this effect
		return false;
	}
}
