package l2n.game.skills.effects;

import l2n.game.instancemanager.TransformationManager;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

public final class EffectTransformation extends L2Effect
{
	public EffectTransformation(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(!_effected.isPlayer())
			return false;

		if(_effected.isAlikeDead() || _effected.isCursedWeaponEquipped())
			return false;

		if(((L2Player) _effected).isTransformed())
			return false;

		return super.checkCondition();
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		if(getEffected().isAlikeDead())
			return;

		super.onStart();
		if(_effected.isPlayer())
		{
			final L2Player player = (L2Player) _effected;
			final int transformId = getSkill().getTransformId();
			TransformationManager.getInstance().transformPlayer(transformId, player);
		}
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		if(_effected.isPlayer())
			((L2Player) _effected).stopTransformation();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
