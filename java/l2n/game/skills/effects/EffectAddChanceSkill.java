package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill.AddedSkill;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 04.06.2011
 * @time 9:43:54
 */
public class EffectAddChanceSkill extends L2Effect
{
	public EffectAddChanceSkill(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		for(final AddedSkill as : getSkill().getAddedSkills())
			_effected.addTriggerableSkill(as.getSkill(), calc() > 0);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		for(final AddedSkill as : getSkill().getAddedSkills())
			_effected.removeTriggerableSkill(as.getSkill().getId());
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
