package l2n.game.skills.effects;

import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.*;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.skills.Env;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.util.logging.Logger;

public final class EffectSymbol extends L2Effect
{
	private static final Logger log = Logger.getLogger(EffectSymbol.class.getName());
	private L2RoundTerritoryWithSkill _territory;
	private L2NpcInstance _symbol;
	private L2Skill _symbolSkill;
	private int count = 0;

	public EffectSymbol(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		if(getSkill().getTargetType() != SkillTargetType.TARGET_SELF)
		{
			log.warning("Symbol skill with target != self, id = " + getSkill().getId());
			exit();
			return;
		}

		_symbolSkill = getSkill().getFirstAddedSkill();
		if(_symbolSkill == null || _symbolSkill != null && _symbolSkill.getSkillType() == SkillType.NOTDONE)
		{
			log.warning("Not implemented symbol skill, id = " + getSkill().getId());
			exit();
			return;
		}

		_symbolSkill.setIsMagic(getSkill().isMagic());

		Location loc = _effected.getLoc();
		if(_effected.isPlayer() && ((L2Player) _effected).getGroundSkillLoc() != null)
		{
			loc = ((L2Player) _effected).getGroundSkillLoc();
			((L2Player) _effected).setGroundSkillLoc(null);
		}

		_territory = new L2RoundTerritoryWithSkill(_effected.getObjectId(), loc.x, loc.y, _skill.getSkillRadius(), loc.z - 200, loc.z + 200, _effector, _symbolSkill);
		L2World.addTerritory(_territory);

		final L2NpcTemplate template = NpcTable.getTemplate(_skill.getSymbolId());
		try
		{
			final L2Spawn spawn = new L2Spawn(template);
			spawn.setLoc(loc);
			spawn.setReflection(_effected.getReflection().getId());
			spawn.setAmount(1);
			spawn.init();
			spawn.stopRespawn();
			_symbol = spawn.getLastSpawn();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		_symbol.broadcastPacket(new MagicSkillUse(_symbol, _symbolSkill.getDisplayId(), _symbolSkill.getDisplayLevel(), 0, 0));

		for(final L2Character cha : _symbol.getAroundCharacters(_skill.getSkillRadius() + 200, 400))
			cha.updateTerritories();
	}

	@Override
	public void onExit()
	{
		super.onExit();
		L2World.removeTerritory(_territory);
		if(_symbol == null)
			return;

		for(final L2Character cha : _symbol.getAroundCharacters(_skill.getSkillRadius() + 200, 400))
			cha.updateTerritories();
		_symbol.deleteMe();
		_symbol = null;
	}

	@Override
	public boolean onActionTime()
	{
		if(getSkill().isOffensive() && count++ < 2)
			return true; // do nothing first 2 times

		final L2Character effector = getEffector();
		final L2NpcInstance symbol = _symbol;

		final GArray<L2Character> targets = new GArray<L2Character>();
		for(final L2Character cha : L2World.getAroundCharacters(symbol, getSkill().getSkillRadius(), 200))
			if(!cha.isDoor() && _symbolSkill.checkTarget(effector, cha, cha, false, false) == null)
			{
				if(_symbolSkill.isOffensive() && !GeoEngine.canSeeTarget(symbol, cha, false))
					continue;

				if(_territory != null && !_territory.isInside(cha)) // Проверяем чтоб объект находился в созданной нами терретории
					continue;

				targets.add(cha);
			}

		if(effector == null || _symbolSkill == null || symbol == null)
			return false;

		final L2Character[] victims = targets.toArray(new L2Character[targets.size()]);
		if(victims != null && victims.length != 0)
		{
			// Symbol скилы которые дают положительный эффект, не должны потреблять мп во время действия
			if(_symbolSkill.isOffensive())
			{
				// mp_consume_tick = 80
				if(80 > effector.getCurrentMp())
				{
					effector.sendPacket(Msg.SKILL_WAS_REMOVED_DUE_TO_LACK_OF_MP);
					effector.abortCast();
					return false;
				}
				effector.reduceCurrentMp(80, effector); // забераем МП только если есть цели
			}

			effector.callSkill(_symbolSkill, false, victims);
			symbol.broadcastPacket(new MagicSkillLaunched(symbol.getObjectId(), _symbolSkill.getDisplayId(), _symbolSkill.getDisplayLevel(), _symbolSkill.isOffensive(), victims));
			symbol.broadcastPacket(new MagicSkillLaunched(symbol.getObjectId(), getSkill().getDisplayId(), getSkill().getDisplayLevel(), getSkill().isOffensive(), victims));

			// Используем соски
			effector.unChargeShots(false);
		}

		return true;
	}
}
