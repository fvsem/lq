package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.08.2010
 * @time 15:31:16
 */
public class EffectNegateEffects extends L2Effect
{
	public EffectNegateEffects(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onExit()
	{
		super.onExit();
	}

	@Override
	public boolean onActionTime()
	{
		for(final L2Effect e : _effected.getEffectList().getAllEffects())
			if(!e.getStackType().equals(EffectTemplate.NO_STACK) && (e.getStackType().equals(getStackType()) || e.getStackType().equals(getStackType2())) || !e.getStackType2().equals(EffectTemplate.NO_STACK) && (e.getStackType2().equals(getStackType()) || e.getStackType2().equals(getStackType2())))
				if(e.getStackOrder() <= getStackOrder())
					e.exit();
		return false;
	}
}
