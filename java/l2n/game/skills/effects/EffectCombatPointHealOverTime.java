package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class EffectCombatPointHealOverTime extends L2Effect
{
	public EffectCombatPointHealOverTime(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead() || _effected.isHealHPBlocked(true))
			return false;

		final double addToCp = Math.max(0, Math.min(calc(), _effected.calcStat(Stats.CP_LIMIT, 100, null, null) * _effected.getMaxCp() / 100. - _effected.getCurrentCp()));
		if(addToCp > 0)
			_effected.setCurrentCp(_effected.getCurrentCp() + addToCp);
		return true;
	}
}
