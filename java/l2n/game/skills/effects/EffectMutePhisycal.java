package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public class EffectMutePhisycal extends L2Effect
{
	public EffectMutePhisycal(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		_effected.startPMuted();
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopPMuted();

	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
