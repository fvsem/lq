package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 02.06.2011
 * @time 15:19:47
 */
public class EffectProtectionBlessing extends L2Effect
{
	public EffectProtectionBlessing(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		if(getEffected().isPlayable())
			getEffected().startProtectionBlessing();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		getEffected().stopProtectionBlessing();

	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
