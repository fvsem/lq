package l2n.game.skills.effects;

import l2n.commons.util.StatsSet;
import l2n.game.model.EffectList;
import l2n.game.model.L2Effect;
import l2n.game.skills.AbnormalEffect;
import l2n.game.skills.EffectType;
import l2n.game.skills.Env;
import l2n.game.skills.conditions.Condition;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.util.ArrayUtil;
import l2n.util.Log;

import java.util.Queue;

public final class EffectTemplate
{
	public static final String HP_RECOVER_CAST = "HpRecoverCast".intern();
	public static final String NO_STACK = "none".intern();

	public Condition _attachCond;
	public final double _value;
	public final int _ticks;
	public final long _period; // in milliseconds
	public AbnormalEffect _abnormalEffect;
	public AbnormalEffect _specialEffect;
	public FuncTemplate[] _funcTemplates = ArrayUtil.EMPTY_FUNCTION_TEMPLATE;
	public final EffectType _effectType;

	public final String _stackType;
	public final String _stackType2;
	public final int _stackOrder;

	public final boolean _hidden;
	public final String _options;
	public final int _activateRate;

	public final int _displayId;
	public final int _displayLevel;

	public final boolean _applyOnCaster;
	public final boolean _cancelOnAction;
	public final boolean _isReflectable;

	public final StatsSet _paramSet;

	public EffectTemplate(final StatsSet set)
	{
		final int skillId = set.getInteger("skillId", 0);

		_value = set.getDouble("value");
		_ticks = set.getInteger("count", 1) < 0 ? Integer.MAX_VALUE : set.getInteger("count", 1);
		_period = Math.min(Integer.MAX_VALUE, 1000 * (set.getInteger("time", 1) < 0 ? Integer.MAX_VALUE : set.getInteger("time", 1)));

		_abnormalEffect = set.getEnum("abnormal", AbnormalEffect.class);
		_specialEffect = set.getEnum("special", AbnormalEffect.class);

		_stackType = set.getString("stackType", NO_STACK);
		_stackType2 = set.getString("stackType2", NO_STACK);

		_stackOrder = set.getInteger("stackOrder", _stackType == NO_STACK && _stackType2 == NO_STACK ? 1 : 0);
		_applyOnCaster = set.getBool("applyOnCaster", false);
		_cancelOnAction = set.getBool("cancelOnAction", false);
		_isReflectable = set.getBool("isReflectable", true);
		_displayId = set.getInteger("displayId", 0);
		_displayLevel = set.getInteger("displayLevel", 0);
		_hidden = set.getBool("hidden", false);
		_options = set.getString("options", null);
		_paramSet = set;

		final String func = set.getString("name");
		if(func.equals("Pdam") || func.equals("Mdam"))
			_activateRate = -1;
		else
			_activateRate = set.getInteger("activateRate", 0);

		_effectType = set.getEnum("name");
		if(_effectType == EffectType.ERROR)
		{
			System.out.println("EffectTemplate: effect '" + func + "' not found for skill id = " + skillId);
			Log.addDev("EffectTemplate: effect '" + func + "' not found for skill id = " + skillId, "skills_load_errors", false);
		}
	}

	public L2Effect getEffect(final Env env)
	{
		if(_attachCond != null && !_attachCond.test(env))
			return null;
		return _effectType.makeEffect(env, this);
	}

	public void attachCond(final Condition c)
	{
		_attachCond = c;
	}

	public void attach(final FuncTemplate f)
	{
		_funcTemplates = ArrayUtil.arrayAdd(_funcTemplates, f);
	}

	public boolean isHidden()
	{
		return _hidden;
	}

	public int getCount()
	{
		return _ticks;
	}

	public long getPeriod()
	{
		return _period;
	}

	public EffectType getEffectType()
	{
		return _effectType;
	}

	/**
	 * @param effects
	 * @return
	 */
	public L2Effect getSameByStackType(final Queue<L2Effect> queue)
	{
		for(final L2Effect ef : queue)
			if(ef != null && EffectList.checkStackType(ef._template, this))
				return ef;
		return null;
	}

	public StatsSet getParam()
	{
		return _paramSet;
	}

	/**
	 * возвращает имя эффекта
	 */
	public String getName()
	{
		return _effectType.name();
	}
}
