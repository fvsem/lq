package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectParalyze extends L2Effect
{
	public EffectParalyze(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effected.isParalyzeImmune())
			return false;
		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		_effected.setParalyzed(true);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.setParalyzed(false);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
