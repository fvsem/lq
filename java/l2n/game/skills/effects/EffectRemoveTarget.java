package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectRemoveTarget extends L2Effect
{
	public EffectRemoveTarget(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		getEffected().setTarget(null);
		getEffected().abortAttack();
		getEffected().abortCast();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
