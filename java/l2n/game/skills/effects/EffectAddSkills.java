package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill.AddedSkill;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 09.08.2010
 * @time 21:36:47
 */
public class EffectAddSkills extends L2Effect
{
	public EffectAddSkills(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		for(final AddedSkill as : getSkill().getAddedSkills())
			getEffected().addSkill(as.getSkill());
	}

	@Override
	public void onExit()
	{
		super.onExit();
		for(final AddedSkill as : getSkill().getAddedSkills())
			getEffected().removeSkill(as.getSkill());
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
