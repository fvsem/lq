package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * Заглушка
 * 
 * @author L2System Project
 * @date 04.08.2010
 * @time 0:28:02
 */
public final class EffectError extends L2Effect
{
	public EffectError(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
