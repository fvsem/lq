package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectImmobileBuff extends L2Effect
{
	public EffectImmobileBuff(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		_effected.setImmobilized(true);
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.setImmobilized(false);
	}

	@Override
	public boolean onActionTime()
	{
		// just stop this effect
		return false;
	}
}
