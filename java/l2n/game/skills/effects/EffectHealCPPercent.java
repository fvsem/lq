package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

/**
 * @author L2System Project
 * @date 23.06.2010
 * @time 14:55:51
 */
public class EffectHealCPPercent extends L2Effect
{
	public EffectHealCPPercent(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(!_effected.isPlayer())
			return false;

		if(_effected.isDead() || _effected.isHealHPBlocked(true))
			return false;
		return super.checkCondition();
	}

	@Override
	public boolean onActionTime()
	{
		final double newCp = calc() * _effected.getMaxCp() / 100;
		final double addToCp = Math.max(0, Math.min(newCp, _effected.calcStat(Stats.CP_LIMIT, 100, null, null) * _effected.getMaxCp() / 100. - _effected.getCurrentCp()));

		if(_effected.isPlayer())
			if(_effected != _effector)
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_WILL_RESTORE_S2S_CP).addString(_effector.getName()).addNumber(Math.round(addToCp)));
			else
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_CPS_WILL_BE_RESTORED).addNumber(Math.round(addToCp)));
		if(addToCp > 0)
			_effected.setCurrentCp(addToCp + _effected.getCurrentCp());
		return false;
	}
}
