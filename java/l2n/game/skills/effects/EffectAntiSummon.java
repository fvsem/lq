package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Summon;
import l2n.game.skills.Env;

public final class EffectAntiSummon extends L2Effect
{
	public EffectAntiSummon(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		if(getEffected().isPlayer())
		{
			if(getEffected().getPet() != null)
				getEffected().getPet().unSummon();
		}
		else if(getEffected().isSummon())
			((L2Summon) getEffected()).unSummon();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
