package l2n.game.skills.effects;

import l2n.commons.list.GArray;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Env;
import l2n.util.Rnd;

public class EffectDiscord extends L2Effect
{
	public EffectDiscord(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		int skilldiff = _effected.getLevel() - _skill.getMagicLevel();
		int lvldiff = _effected.getLevel() - _effector.getLevel();
		if(skilldiff > 10 || skilldiff > 5 && Rnd.chance(30) || Rnd.chance(Math.abs(lvldiff) * 2))
			return false;

		boolean multitargets = _skill.isAoE();

		if(!_effected.isMonster())
		{
			if(!multitargets)
				getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		if(_effected.isFearImmune() || _effected.isRaid())
		{
			if(!multitargets)
				getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		// Discord нельзя наложить на осадных саммонов
		if(_effected instanceof L2Summon && ((L2Summon) _effected).isSiegeWeapon())
		{
			if(!multitargets)
				getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		if(_effected.isInZonePeace())
		{
			if(!multitargets)
				getEffector().sendPacket(Msg.YOU_CANNOT_ATTACK_IN_THE_PEACE_ZONE);
			return false;
		}

		return super.checkCondition();
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		_effected.startConfused();
		onActionTime();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopConfused();
		_effected.setWalking();
		_effected.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
	}

	@Override
	public boolean onActionTime()
	{
		final GArray<L2Character> targetList = new GArray<L2Character>();

		for(final L2Character character : getEffected().getAroundCharacters(900))
			if(character instanceof L2NpcInstance && character != getEffected())
				targetList.add(character);
		// if there is no target, exit function
		if(targetList.size() == 0)
			return true;

		// Choosing randomly a new target
		final L2Character target = targetList.get(Rnd.get(targetList.size()));

		// Attacking the target
		getEffected().setTarget(target);
		getEffected().setRunning();
		getEffected().getAI().Attack(target, true, false);

		return true;
	}
}
