package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.skills.Env;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 19.09.2011
 * @time 7:21:42
 */
public class EffectCancelTarget extends L2Effect
{
	public EffectCancelTarget(final Env env, final EffectTemplate template)
	{
		super(env, template);

		if(_effected.isDead())
			return;

		if(_effected.isRaid())
			return;

		final L2Skill castignSkill = _effected.getCastingSkill();
		if(castignSkill != null && (castignSkill.getSkillType() == SkillType.TAKECASTLE || castignSkill.getSkillType() == SkillType.TAKEFORT || castignSkill.getSkillType() == SkillType.TAKEFLAG))
			return;

		_effected.stopMove(true);
		_effected.abortAttack();
		_effected.breakCast(true);
		_effected.setTarget(null);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
