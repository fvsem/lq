package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;
import l2n.game.skills.skillclasses.NegateStats;

public class EffectBlockStat extends L2Effect
{
	public EffectBlockStat(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		_effected.addBlockStats(((NegateStats) _skill).getNegateStats());
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.removeBlockStats(((NegateStats) _skill).getNegateStats());
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
