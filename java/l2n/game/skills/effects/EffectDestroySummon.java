package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Summon;
import l2n.game.skills.Env;

public final class EffectDestroySummon extends L2Effect
{
	public EffectDestroySummon(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		if(_effected.isSummon())
		{
			((L2Summon) _effected).unSummon();
			exit();
		}
	}

	@Override
	public boolean onActionTime()
	{
		// just stop this effect
		return false;
	}
}
