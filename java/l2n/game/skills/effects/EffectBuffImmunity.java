package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectBuffImmunity extends L2Effect
{

	public EffectBuffImmunity(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		getEffected().setBuffImmunity(true);
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		getEffected().setBuffImmunity(false);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
