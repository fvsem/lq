package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.08.2010
 * @time 14:29:36
 */
public class EffectConsumeSoulsOverTime extends L2Effect
{
	public EffectConsumeSoulsOverTime(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead())
			return false;
		if(_effected.getConsumedSouls() < 0)
			return false;
		final int damage = (int) calc();

		if(_effected.getConsumedSouls() < damage)
			_effected.setConsumedSouls(0, null);
		else
			_effected.setConsumedSouls(_effected.getConsumedSouls() - damage, null);
		return true;
	}
}
