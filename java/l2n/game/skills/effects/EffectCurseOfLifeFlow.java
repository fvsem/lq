package l2n.game.skills.effects;

import javolution.util.FastMap;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.reduceHp.ReduceCurrentHpListener;
import l2n.game.model.L2Effect;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;

import java.util.Map.Entry;

/**
 * @author L2System Project
 * @date 27.10.2010
 * @time 3:49:25
 */
public final class EffectCurseOfLifeFlow extends L2Effect
{
	private class CurseOfLifeFlowListener extends ReduceCurrentHpListener
	{
		@Override
		public void onReduceCurrentHp(final L2Character actor, final double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp)
		{
			if(attacker == actor || attacker == _effected)
				return;
			final Integer old_damage = _damagers.get(attacker.getStoredId());
			_damagers.put(attacker.getStoredId(), old_damage == null ? (int) damage : old_damage + (int) damage);
		}
	}

	private CurseOfLifeFlowListener _listener;
	private final FastMap<Long, Integer> _damagers = new FastMap<Long, Integer>();

	/**
	 * @param env
	 * @param template
	 */
	public EffectCurseOfLifeFlow(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		_listener = new CurseOfLifeFlowListener();
		_effected.addMethodInvokeListener(MethodCollection.ReduceCurrentHp, _listener);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.removeMethodInvokeListener(MethodCollection.ReduceCurrentHp, _listener);
		_listener = null;
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead())
			return false;

		for(final Entry<Long, Integer> entry : _damagers.entrySet())
		{
			final L2Character damager = L2ObjectsStorage.getAsCharacter(entry.getKey());
			if(damager == null || damager.isDead() || damager.isCurrentHpFull())
				continue;

			final Integer damage = entry.getValue();
			if(damage == null || damage <= 0)
				continue;

			final double max_heal = calc();
			final double heal = Math.min(damage, max_heal);
			final double newHp = Math.min(damager.getCurrentHp() + heal, damager.getMaxHp());

			damager.sendPacket(new SystemMessage(SystemMessage.S1_HPS_HAVE_BEEN_RESTORED).addNumber((long) (newHp - damager.getCurrentHp())));
			damager.setCurrentHp(newHp, false);
		}

		_damagers.clear();
		return true;
	}
}
