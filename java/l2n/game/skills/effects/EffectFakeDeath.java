package l2n.game.skills.effects;

import l2n.game.ai.CtrlEvent;
import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ChangeWaitType;
import l2n.game.network.serverpackets.Revive;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;

public final class EffectFakeDeath extends L2Effect
{
	public EffectFakeDeath(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		L2Player player = (L2Player) getEffected();
		player.setFakeDeath(true);
		player.clearHateList(true);
		player.getAI().notifyEvent(CtrlEvent.EVT_FAKE_DEATH, null, null);
		player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_START_FAKEDEATH));
		player.broadcastCharInfo();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		L2Player player = (L2Player) getEffected();
		// 5 секунд после FakeDeath на персонажа не агрятся мобы
		player.setNonAggroTime(System.currentTimeMillis() + 5000L);
		player.setFakeDeath(false);
		player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
		player.broadcastPacket(new Revive(player));
		player.broadcastCharInfo();
	}

	@Override
	public boolean onActionTime()
	{
		if(getEffected().isDead())
			return false;

		final double manaDam = calc();

		if(manaDam > getEffected().getCurrentMp())
			if(getSkill().isToggle())
			{
				getEffected().sendPacket(Msg.NOT_ENOUGH_MP);
				getEffected().sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(getSkill().getId(), getSkill().getLevel()));
				return false;
			}

		getEffected().reduceCurrentMp(manaDam, null);
		return true;
	}
}
