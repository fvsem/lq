package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class EffectHeal extends L2Effect
{
	public EffectHeal(final Env env, final EffectTemplate template)
	{
		super(env, template);
		if(_effected.isDead())
			return;

		final double newHp = calc() * _effected.calcStat(Stats.HEAL_EFFECTIVNESS, 100, _effector, getSkill()) / 100;
		final double addToHp = Math.max(0, Math.min(newHp, _effected.calcStat(Stats.HP_LIMIT, 100, null, null) * _effected.getMaxHp() / 100. - _effected.getCurrentHp()));
		if(_effected.isPlayer())
			if(_effected != _effector)
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_HP_HAS_BEEN_RESTORED_BY_XS2S).addString(_effector.getName()).addNumber(Math.round(addToHp)));
			else
				_effected.sendPacket(new SystemMessage(SystemMessage.S1_HPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToHp)));
		if(addToHp > 0)
			_effected.setCurrentHp(addToHp + _effected.getCurrentHp(), false);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
