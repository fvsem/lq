package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.08.2010
 * @time 15:31:22
 */
public class EffectNegateMusic extends L2Effect
{
	public EffectNegateMusic(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onExit()
	{
		super.onExit();
	}

	@Override
	public boolean onActionTime()
	{
		for(final L2Effect e : _effected.getEffectList().getAllEffects())
			if(e.getSkill().isDanceSong())
				e.exit();
		return false;
	}
}
