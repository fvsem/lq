package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.AbnormalEffect;
import l2n.game.skills.Env;

public final class EffectGrow extends L2Effect
{
	public EffectGrow(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		if(getEffected() instanceof L2NpcInstance)
		{
			final L2NpcInstance npc = (L2NpcInstance) getEffected();
			npc.setCollisionHeight(npc.getCollisionHeight() * 1.24);
			npc.setCollisionRadius(npc.getCollisionRadius() * 1.19);

			npc.startAbnormalEffect(AbnormalEffect.GROW);
		}
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		if(getEffected() instanceof L2NpcInstance)
		{
			final L2NpcInstance npc = (L2NpcInstance) getEffected();
			npc.setCollisionHeight(npc.getTemplate().collisionHeight);
			npc.setCollisionRadius(npc.getTemplate().collisionRadius);

			npc.stopAbnormalEffect(AbnormalEffect.GROW);
		}
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
