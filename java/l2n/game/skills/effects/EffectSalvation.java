package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

public final class EffectSalvation extends L2Effect
{
	public EffectSalvation(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();
		final int power = (int) calc();
		getEffected().setIsSalvation(true, power);
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		getEffected().setIsSalvation(false, 0);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
