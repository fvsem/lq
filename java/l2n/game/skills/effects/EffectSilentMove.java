package l2n.game.skills.effects;

import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;

public final class EffectSilentMove extends L2Effect
{
	public EffectSilentMove(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		if(_effected.isPlayer())
			((L2Player) _effected).setSilentMoving(true);
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		if(_effected.isPlayer())
			((L2Player) _effected).setSilentMoving(false);
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead())
			return false;

		if(!getSkill().isToggle())
			return false;

		final double manaDam = calc();

		if(manaDam > _effected.getCurrentMp())
		{
			_effected.sendPacket(Msg.NOT_ENOUGH_MP);
			_effected.sendPacket(new SystemMessage(SystemMessage.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(getSkill().getId(), getSkill().getLevel()));
			return false;
		}

		_effected.reduceCurrentMp(manaDam, null);
		return true;
	}
}
