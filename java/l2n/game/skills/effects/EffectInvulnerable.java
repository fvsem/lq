package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.skills.Env;

public final class EffectInvulnerable extends L2Effect
{
	public EffectInvulnerable(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effected.isInvul(false))
			return false;
		final L2Skill skill = _effected.getCastingSkill();
		if(skill != null && (skill.getSkillType() == SkillType.TAKECASTLE || skill.getSkillType() == SkillType.TAKEFORT))
			return false;
		return super.checkCondition();
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
