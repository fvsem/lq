package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 21.02.2011
 * @time 21:20:13
 */
public class EffectHealBlock extends L2Effect
{
	public EffectHealBlock(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effected.isHealHPBlocked(false))
			return false;
		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		final double newHp = calc() * _effected.getMaxHp() / 100;
		if(newHp > 0)
			getEffected().setCurrentHp(Math.max(1, newHp), false);
		_effected.setHealHPBlocked(true);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.setHealHPBlocked(false);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
