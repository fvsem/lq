package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Summon;
import l2n.game.skills.Env;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;

public class EffectBetray extends L2Effect
{
	public EffectBetray(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		if(_effected instanceof L2Summon)
		{
			final L2Summon summon = (L2Summon) _effected;
			summon.setPossessed(true);
			summon.getAI().setAttackTarget(summon.getPlayer());
			summon.getAI().Attack(summon.getPlayer(), true, false);
		}
	}

	@Override
	public void onExit()
	{
		super.onExit();
		if(_effected instanceof L2Summon)
		{
			final L2Summon summon = (L2Summon) _effected;
			summon.setPossessed(false);
			summon.getAI().setAttackTarget(null);
			summon.getAI().setIntention(AI_INTENTION_ACTIVE);
		}
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
