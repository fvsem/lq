package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * Иммобилизует и парализует на время действия.
 */
public final class EffectMeditation extends L2Effect
{
	public EffectMeditation(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		_effected.block();
		_effected.setMeditated(true);
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.unblock();
		_effected.setMeditated(false);
	}

	@Override
	public boolean onActionTime()
	{
		// just stop this effect
		return false;
	}
}
