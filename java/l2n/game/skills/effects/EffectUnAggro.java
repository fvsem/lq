package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Env;

public class EffectUnAggro extends L2Effect
{
	public EffectUnAggro(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();

		if(_effected instanceof L2NpcInstance)
			((L2NpcInstance) _effected).setUnAggred(true);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		if(_effected instanceof L2NpcInstance)
			((L2NpcInstance) _effected).setUnAggred(false);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
