package l2n.game.skills.effects;

import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Summon;
import l2n.game.skills.Env;
import l2n.util.Location;
import l2n.util.Rnd;

public final class EffectFear extends L2Effect
{
	public static final int FEAR_RANGE = 500;

	public EffectFear(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	/** Notify started */
	@Override
	public void onStart()
	{
		super.onStart();

		if(_effected.isFearImmune())
		{
			getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			exit();
			return;
		}

		// Fear нельзя наложить на осадных саммонов
		if(_effected.getNpcId() == L2Summon.SIEGE_GOLEM_ID || _effected.getNpcId() == L2Summon.SIEGE_CANNON_ID || _effected.getNpcId() == L2Summon.SWOOP_CANNON_ID)
		{
			getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			exit();
			return;
		}

		if(_effected.isInZonePeace())
		{
			getEffector().sendPacket(Msg.YOU_CANNOT_ATTACK_IN_THE_PEACE_ZONE);
			exit();
			return;
		}

		if((_effected.isSummon() || _effected.isPet()) && _effected.getPlayer() == getEffector())
		{
			exit();
			return;
		}

		_effected.startFear();

		onActionTime();
	}

	/** Notify exited */
	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopFear();
		_effected.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
	}

	@Override
	public boolean onActionTime()
	{
		final Location pos = Rnd.coordsRandomize(_effected.getLoc(), FEAR_RANGE, FEAR_RANGE);
		final Location loc = GeoEngine.moveCheck(_effected.getX(), _effected.getY(), _effected.getZ(), pos.x, pos.y);

		_effected.setRunning();
		_effected.moveToLocation(loc, 0, false);
		_effected.sendMessage("You can feel Fears's effect");

		return true;
	}
}
