package l2n.game.skills.effects;

import l2n.game.ai.L2PlayableAI;
import l2n.game.cache.Msg;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Summon;
import l2n.game.skills.Env;

public class EffectAggression extends L2Effect
{
	public EffectAggression(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		// нельзя наложить на осадных саммонов
		if(_effected instanceof L2Summon && ((L2Summon) _effected).isSiegeWeapon())
		{
			getEffector().sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		if(_effected.isInZonePeace())
		{
			getEffector().sendPacket(Msg.YOU_CANNOT_ATTACK_IN_THE_PEACE_ZONE);
			return false;
		}

		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();
		if(_effected.isPlayable())
			((L2PlayableAI) _effected.getAI()).lockTarget(_effector);
	}

	@Override
	public void onExit()
	{
		super.onExit();
		if(_effected.isPlayable())
			((L2PlayableAI) _effected.getAI()).lockTarget(null);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
