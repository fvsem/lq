package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public final class EffectManaHealOverTime extends L2Effect
{
	public EffectManaHealOverTime(final Env env, final EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead() || _effected.isHealMPBlocked(true))
			return false;
		final double addToMp = Math.max(0, Math.min(calc(), _effected.calcStat(Stats.MP_LIMIT, 100, null, null) * _effected.getMaxMp() / 100. - _effected.getCurrentMp()));
		if(addToMp > 0)
			_effected.setCurrentMp(_effected.getCurrentMp() + addToMp);
		return true;
	}
}
