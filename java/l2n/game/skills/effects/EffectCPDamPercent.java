package l2n.game.skills.effects;

import l2n.game.model.L2Effect;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.08.2010
 * @time 14:28:10
 */
public class EffectCPDamPercent extends L2Effect
{
	public EffectCPDamPercent(final Env env, final EffectTemplate template)
	{
		super(env, template);
		if(_effected.isDead())
			return;
		double newCp = (100.0D - calc()) * _effected.getMaxCp() / 100.0D;
		newCp = Math.min(_effected.getCurrentCp(), Math.max(0.0D, newCp));
		_effected.setCurrentCp(newCp);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}
