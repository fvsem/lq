package l2n.game.skills;

import l2n.game.model.L2Effect;
import l2n.game.skills.effects.*;

import java.lang.reflect.Constructor;

import static l2n.game.skills.Stats.*;

public enum EffectType
{
	// Основные эффекты
	AddSkills(EffectAddSkills.class, false),
	AddChanceSkill(EffectAddChanceSkill.class, false),

	Aggression(EffectAggression.class, true),
	Betray(EffectBetray.class, MENTAL_RECEPTIVE, MENTAL_POWER, true),
	BlessNoblesse(EffectBlessNoblesse.class, true),
	BlockStat(EffectBlockStat.class, true),
	Buff(EffectBuff.class, false),
	BuffImmunity(EffectBuffImmunity.class, true),
	CancelTarget(EffectCancelTarget.class, MENTAL_RECEPTIVE, MENTAL_POWER, true),
	CombatPointHealOverTime(EffectCombatPointHealOverTime.class, true),
	Confusion(EffectConfusion.class, MENTAL_RECEPTIVE, MENTAL_POWER, true),
	ConsumeSoulsOverTime(EffectConsumeSoulsOverTime.class, true),
	CharmOfCourage(EffectCharmOfCourage.class, true),
	CPDamPercent(EffectCPDamPercent.class, true),
	DamOverTime(EffectDamOverTime.class, false),
	DamOverTimeLethal(EffectDamOverTimeLethal.class, false),
	DebuffImmunity(EffectDebuffImmunity.class, null, true),
	DestroySummon(EffectDestroySummon.class, MENTAL_RECEPTIVE, MENTAL_POWER, true),
	Disarm(EffectDisarm.class, true),
	Discord(EffectDiscord.class, AbnormalEffect.CONFUSED, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	DispelEffects(EffectDispelEffects.class, null, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER, true),
	// Enervation(EffectEnervation.class, false), пока не надо)
	FakeDeath(EffectFakeDeath.class, true),
	Fear(EffectFear.class, AbnormalEffect.AFFRAID, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	Grow(EffectGrow.class, false),
	Heal(EffectHeal.class, false),
	HealBlock(EffectHealBlock.class, true),
	HealCPPercent(EffectHealCPPercent.class, true),
	HealOverTime(EffectHealOverTime.class, false),
	HealPercent(EffectHealPercent.class, false),
	ImmobileBuff(EffectImmobileBuff.class, true),
	Interrupt(EffectInterrupt.class, true),
	Invulnerable(EffectInvulnerable.class, false),
	Invisible(EffectInvisible.class, false),
	CurseOfLifeFlow(EffectCurseOfLifeFlow.class, true),
	LDManaDamOverTime(EffectLDManaDamOverTime.class, true),
	ManaDamOverTime(EffectManaDamOverTime.class, true),
	ManaHeal(EffectManaHeal.class, false),
	ManaHealOverTime(EffectManaHealOverTime.class, false),
	ManaHealPercent(EffectManaHealPercent.class, false),
	Meditation(EffectMeditation.class, false),
	Mute(EffectMute.class, AbnormalEffect.MUTED, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	MuteAll(EffectMuteAll.class, AbnormalEffect.MUTED, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	MuteAttack(EffectMuteAttack.class, AbnormalEffect.MUTED, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	MutePhisycal(EffectMutePhisycal.class, AbnormalEffect.MUTED, Stats.MENTAL_RECEPTIVE, Stats.MENTAL_POWER, true),
	NegateEffects(EffectNegateEffects.class, false),
	NegateMusic(EffectNegateMusic.class, false),
	Paralyze(EffectParalyze.class, AbnormalEffect.HOLD_1, Stats.PARALYZE_RECEPTIVE, Stats.PARALYZE_POWER, true),
	Petrification(EffectPetrification.class, AbnormalEffect.HOLD_2, Stats.PARALYZE_RECEPTIVE, Stats.PARALYZE_POWER, true),
	ProtectionBlessing(EffectProtectionBlessing.class, false),
	Relax(EffectRelax.class, true),
	RemoveTarget(EffectRemoveTarget.class, true),
	Root(EffectRoot.class, AbnormalEffect.ROOT, Stats.ROOT_RECEPTIVE, Stats.ROOT_POWER, true),
	Salvation(EffectSalvation.class, true),
	SilentMove(EffectSilentMove.class, AbnormalEffect.STEALTH, true),
	Sleep(EffectSleep.class, AbnormalEffect.SLEEP, Stats.SLEEP_RECEPTIVE, Stats.SLEEP_POWER, true),
	Stun(EffectStun.class, AbnormalEffect.STUN, Stats.STUN_RECEPTIVE, Stats.STUN_POWER, true),
	Symbol(EffectSymbol.class, false),
	Transformation(EffectTransformation.class, true),
	Turner(EffectTurner.class, AbnormalEffect.STUN, MENTAL_RECEPTIVE, MENTAL_POWER, true),
	UnAggro(EffectUnAggro.class, true),
	Vitality(EffectBuff.class, AbnormalEffect.VITALITY, true),

	// Производные от основных эффектов
	Poison(EffectDamOverTime.class, POISON_RECEPTIVE, POISON_POWER, false),
	PoisonLethal(EffectDamOverTimeLethal.class, POISON_RECEPTIVE, POISON_POWER, false),
	Bleed(EffectDamOverTime.class, BLEED_RECEPTIVE, BLEED_POWER, false),
	Debuff(EffectBuff.class, DEBUFF_RECEPTIVE, DEBUFF_POWER, false),
	WatcherGaze(EffectBuff.class, false),
	TransferDam(EffectBuff.class, null, false),
	FishPotion(EffectBuff.class, null, false),

	// для дебафа Экимуса, для удобства
	SoulRetain(EffectBuff.class, false),

	// Для отладки
	ERROR(EffectError.class, false);

	private final Constructor<? extends L2Effect> _constructor;
	private final AbnormalEffect _abnormal;
	private final Stats _resistType;
	private final Stats _attackType;
	private final boolean _isRaidImmune;

	private EffectType(Class<? extends L2Effect> clazz, boolean isRaidImmune)
	{
		this(clazz, null, null, isRaidImmune);
	}

	private EffectType(Class<? extends L2Effect> clazz, AbnormalEffect abnormal, boolean isRaidImmune)
	{
		this(clazz, abnormal, null, null, isRaidImmune);
	}

	private EffectType(Class<? extends L2Effect> clazz, Stats resistType, Stats attibuteType, boolean isRaidImmune)
	{
		this(clazz, null, resistType, attibuteType, isRaidImmune);
	}

	private EffectType(Class<? extends L2Effect> clazz, AbnormalEffect abnormal, Stats resistType, Stats attibuteType, boolean isRaidImmune)
	{
		try
		{
			_constructor = clazz.getConstructor(Env.class, EffectTemplate.class);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		_abnormal = abnormal;
		_resistType = resistType;
		_attackType = attibuteType;
		_isRaidImmune = isRaidImmune;
	}

	public AbnormalEffect getAbnormal()
	{
		return _abnormal;
	}

	/** @return тип статов который увеличивает шанс прохождения эффекта */
	public Stats getAttackType()
	{
		return _attackType;
	}

	/** @return тип статов который уменьшает шанс прохождения (разные сопротивляемости) */
	public Stats getResistType()
	{
		return _resistType;
	}

	public boolean isRaidImmune()
	{
		return _isRaidImmune;
	}

	public L2Effect makeEffect(Env env, EffectTemplate template)
	{
		try
		{
			return _constructor.newInstance(env, template);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
