package l2n.game.skills.funcs;

import l2n.game.skills.Stats;

import java.lang.reflect.Constructor;

public class FuncFactory
{
	private final static FuncFactory _instance = new FuncFactory();

	private FuncFactory()
	{}

	public static FuncFactory getInstance()
	{
		return _instance;
	}

	public static Func createFunc(final String func, final Stats stat, final int order, final double lambdaValue) throws Exception
	{
		return createFunc(func, stat, order, lambdaValue, _instance);
	}

	@SuppressWarnings("unchecked")
	public static Func createFunc(final String func, final Stats stat, final int order, final double lambdaValue, final Object owner) throws Exception
	{
		final Class funcClass = Class.forName("l2n.game.skills.funcs.Func" + func);
		final Constructor funcConstructor = funcClass.getConstructor(new Class[] { Stats.class, Integer.TYPE, Object.class, Lambda.class });
		final Lambda lambda = new LambdaConst(lambdaValue);
		return (Func) funcConstructor.newInstance(stat, order, owner, lambda);
	}
}
