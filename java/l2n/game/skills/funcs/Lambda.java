package l2n.game.skills.funcs;

import l2n.game.skills.Env;

public abstract class Lambda
{
	public double _value = 0;

	public Lambda(final double value)
	{
		_value = value;
	}

	public abstract double calc(Env env);
}
