package l2n.game.skills.funcs;

import l2n.game.skills.Env;

public final class LambdaCalc extends Lambda
{
	public Func[] _funcs;

	public LambdaCalc()
	{
		super(0);
		_funcs = Func.EMPTY_FUNCTION_SET;
	}

	@Override
	public double calc(final Env env)
	{
		final double saveValue = env.value;
		try
		{
			env.value = 0;
			for(final Func f : _funcs)
				f.calc(env);
			return env.value;
		}
		finally
		{
			env.value = saveValue;
		}
	}

	public void addFunc(final Func f)
	{
		final int len = _funcs.length;
		final Func[] tmp = new Func[len + 1];
		System.arraycopy(_funcs, 0, tmp, 0, len);
		tmp[len] = f;
		_funcs = tmp;
	}
}
