package l2n.game.skills.funcs;

import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class FuncAdd extends Func
{
	public FuncAdd(final Stats stat, final int order, final Object owner, final Lambda lambda)
	{
		super(stat, order, owner, lambda);
	}

	public FuncAdd(final Stats stat, final int order, final Object owner, final float val)
	{
		super(stat, order, owner, new LambdaConst(val));
	}

	@Override
	public void calc(final Env env)
	{
		env.value += _lambda.calc(env);
	}
}
