package l2n.game.skills.funcs;

import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class FuncMul extends Func
{
	public FuncMul(final Stats stat, final int order, final Object owner, final Lambda lambda)
	{
		super(stat, order, owner, lambda);
	}

	public FuncMul(final Stats stat, final int order, final Object owner, final double value)
	{
		super(stat, order, owner, new LambdaConst(value));
	}

	@Override
	public void calc(final Env env)
	{
		env.value *= _lambda.calc(env);
	}
}
