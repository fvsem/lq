package l2n.game.skills.funcs;

import l2n.game.skills.Env;
import l2n.game.skills.Stats;
import l2n.game.skills.conditions.Condition;

import java.lang.reflect.Constructor;

public final class FuncTemplate
{
	public Condition _attachCond;
	public Condition _applyCond;
	public final Class<?> _func;
	public final Constructor<?> _constructor;
	public Stats _stat;
	public final int _order;
	public Lambda _lambda;

	public FuncTemplate(final Condition attachCond, final Condition applyCond, final String func, final Stats stat, final int order, final Lambda lambda)
	{
		_attachCond = attachCond;
		_applyCond = applyCond;
		_stat = stat;
		_order = order;
		_lambda = lambda;
		try
		{
			_func = Class.forName("l2n.game.skills.funcs.Func" + func);
		}
		catch(final ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
		try
		{
			_constructor = _func.getConstructor(new Class[] { Stats.class, // stats to update
					Integer.TYPE, // order of execution
					Object.class, // owner
					Lambda.class // value for function
			});
		}
		catch(final NoSuchMethodException e)
		{
			throw new RuntimeException(e);
		}
	}

	/** Заглушка, для удобства */
	public FuncTemplate(final Condition attachCond, final Condition applyCond, final String func, final Stats stat, final int order, final double value)
	{
		this(attachCond, applyCond, func, stat, order, new LambdaConst(value));
	}

	public Func getFunc(final Env env, final Object owner)
	{
		if(_attachCond != null && !_attachCond.test(env))
			return null;
		try
		{
			final Func f = (Func) _constructor.newInstance(_stat, _order, owner, _lambda);
			if(_applyCond != null)
				f.setCondition(_applyCond);
			return f;
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
