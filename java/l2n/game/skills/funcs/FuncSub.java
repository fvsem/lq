package l2n.game.skills.funcs;

import l2n.game.skills.Env;
import l2n.game.skills.Stats;

public class FuncSub extends Func
{
	public FuncSub(final Stats stat, final int order, final Object owner, final Lambda lambda)
	{
		super(stat, order, owner, lambda);
	}

	@Override
	public void calc(final Env env)
	{
		env.value -= _lambda.calc(env);
	}
}
