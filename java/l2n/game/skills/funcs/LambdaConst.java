package l2n.game.skills.funcs;

import l2n.game.skills.Env;

public final class LambdaConst extends Lambda
{
	public LambdaConst(final double value)
	{
		super(value);
	}

	@Override
	public double calc(final Env env)
	{
		return _value;
	}
}
