package l2n.game.skills.funcs;

import l2n.game.skills.Env;

public class LambdaConstPercent extends Lambda
{
	public LambdaConstPercent(final double value)
	{
		super(value);
	}

	@Override
	public double calc(final Env env)
	{
		return env.value + env.value * _value / 100;
	}

	public void join(final Func func)
	{
		if(func instanceof FuncAdd)
			_value += func._lambda._value;
		else if(func instanceof FuncSub)
			_value -= func._lambda._value;
	}

	public void set(final double value)
	{
		_value = value;
	}
}
