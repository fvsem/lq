package l2n.game.skills;

import javolution.lang.MathLib;
import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.Element;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.base.Race;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.residence.*;
import l2n.game.model.instances.*;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.conditions.ConditionPlayerState;
import l2n.game.skills.conditions.ConditionPlayerState.PlayerState;
import l2n.game.skills.funcs.Func;
import l2n.game.templates.L2PlayerTemplate;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Rnd;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 * <b>STR</b> - Strength - Влияет на физический урон <br>
 * <br>
 * DEX - Dexterity - Влияет на физический урон при обычной атаке, на скорость атаки, на меткость, на уклонение, на шанс критического урона, на шанс блокировки удара щитом, на скорость движения <br>
 * <br>
 * СON – Constitution - Влияет на общее количество HP, скорость восстановления HP, на максимально допустимый вес, на Запас дыхания, на сопротивление Shock/Stun/Bleed <br>
 * <br>
 * INT – Intelligence - Влияет на магический урон, шанс успеха Curse/дебафов <br>
 * <br>
 * WIT – Wit - Влияет на шанс критического урона магией, на скорость каста, на сопротивление Curse/дебафам <br>
 * <br>
 * MEN – Mentality - Влияет на сопротивление магии, общее количество MP, скорость восстановления MP, на шанс успешного прерывания каста при атаке (если атакуют вас, то на шанс не прерывания каста), на сопротивление Poison
 */
public class Formulas
{
	private static final Logger _log = Logger.getLogger(Formulas.class.getName());

	public static final int MAX_STAT_VALUE = 100;

	public static final double DEATHLINK_PDAM_MULTIPLIER = 1.8;

	public final static NumberFormat numFormatter = new DecimalFormat("###.###");

	public static final double[] WITbonus = new double[MAX_STAT_VALUE];
	public static final double[] MENbonus = new double[MAX_STAT_VALUE];
	public static final double[] INTbonus = new double[MAX_STAT_VALUE];
	public static final double[] STRbonus = new double[MAX_STAT_VALUE];
	public static final double[] DEXbonus = new double[MAX_STAT_VALUE];
	public static final double[] CONbonus = new double[MAX_STAT_VALUE];

	// These values are 100% matching retail tables, no need to change and no need add calculation into the stat bonus when accessing (not efficient),
	// better to have everything precalculated and use values directly (saves CPU)
	static
	{
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setIgnoringComments(true);
		final File file = new File("data/attribute_bonus.xml");
		Document doc = null;

		try
		{
			doc = factory.newDocumentBuilder().parse(file);
		}
		catch(final SAXException e)
		{
			e.printStackTrace();
		}
		catch(final IOException e)
		{
			e.printStackTrace();
		}
		catch(final ParserConfigurationException e)
		{
			e.printStackTrace();
		}

		int i;
		double val;

		if(doc != null)
			for(Node z = doc.getFirstChild(); z != null; z = z.getNextSibling())
				for(Node n = z.getFirstChild(); n != null; n = n.getNextSibling())
				{
					if(n.getNodeName().equalsIgnoreCase("str_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								STRbonus[i] = (100 + val) / 100;
							}
						}
					if(n.getNodeName().equalsIgnoreCase("int_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								INTbonus[i] = (100 + val) / 100;
							}
						}
					if(n.getNodeName().equalsIgnoreCase("con_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								CONbonus[i] = (100 + val) / 100;
							}
						}
					if(n.getNodeName().equalsIgnoreCase("men_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								MENbonus[i] = (100 + val) / 100;
							}
						}
					if(n.getNodeName().equalsIgnoreCase("dex_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								DEXbonus[i] = (100 + val) / 100;
							}
						}
					if(n.getNodeName().equalsIgnoreCase("wit_bonus"))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							final String node = d.getNodeName();
							if(node.equalsIgnoreCase("set"))
							{
								i = Integer.valueOf(d.getAttributes().getNamedItem("attribute").getNodeValue());
								val = Integer.valueOf(d.getAttributes().getNamedItem("val").getNodeValue());
								WITbonus[i] = (100 + val) / 100;
							}
						}
				}
	}

	public static class AttackInfo
	{
		public double damage;
		public double defence;
		public double crit_rcpt;
		public double crit_static;
		public double death_rcpt;
		public double lethal1;
		public double lethal2;
		public double lethal_dmg = 0;
		public boolean crit;
		public boolean shld;
		public boolean lethal;
		public boolean miss;
		public boolean isPvP;
	}

	private static class FuncMultRegenResting extends Func
	{
		private static final FuncMultRegenResting[] _instance = new FuncMultRegenResting[Stats.NUM_STATS];

		private static Func getInstance(final Stats stat)
		{
			final int pos = stat.ordinal();
			if(_instance[pos] == null)
				_instance[pos] = new FuncMultRegenResting(stat);
			return _instance[pos];
		}

		private FuncMultRegenResting(final Stats stat)
		{
			super(stat, 0x20, null);
			setCondition(new ConditionPlayerState(PlayerState.RESTING, true));
		}

		@Override
		public void calc(final Env env)
		{
			if(env.character.isPlayer() && env.character.getLevel() <= 40 && ((L2Player) env.character).getClassId().getLevel() < 3)
				env.value *= 6;
			else
				env.value *= 1.5;
		}
	}

	private static class FuncMultRegenStanding extends Func
	{
		private static final FuncMultRegenStanding[] _instance = new FuncMultRegenStanding[Stats.NUM_STATS];

		private static Func getInstance(final Stats stat)
		{
			final int pos = stat.ordinal();
			if(_instance[pos] == null)
				_instance[pos] = new FuncMultRegenStanding(stat);
			return _instance[pos];
		}

		private FuncMultRegenStanding(final Stats stat)
		{
			super(stat, 0x20, null);
			setCondition(new ConditionPlayerState(PlayerState.STANDING, true));
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= 1.1;
		}
	}

	private static class FuncMultRegenRunning extends Func
	{
		private static final FuncMultRegenRunning[] _instance = new FuncMultRegenRunning[Stats.NUM_STATS];

		private static Func getInstance(final Stats stat)
		{
			final int pos = stat.ordinal();
			if(_instance[pos] == null)
				_instance[pos] = new FuncMultRegenRunning(stat);
			return _instance[pos];
		}

		private FuncMultRegenRunning(final Stats stat)
		{
			super(stat, 0x20, null);
			setCondition(new ConditionPlayerState(PlayerState.RUNNING, true));
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= 0.7;
		}
	}

	private static class FuncPAtkMod extends Func
	{
		private static final FuncPAtkMod func = new FuncPAtkMod();

		private FuncPAtkMod()
		{
			super(Stats.POWER_ATTACK, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= STRbonus[env.character.getSTR()] * env.character.getLevelMod();
		}
	}

	private static class FuncMAtkMod extends Func
	{
		private static final FuncMAtkMod func = new FuncMAtkMod();

		private FuncMAtkMod()
		{
			super(Stats.MAGIC_ATTACK, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			// {Wpn*(lvlbn^2)*[(1+INTbn)^2]+Msty}
			final double intb = INTbonus[env.character.getINT()];
			final double lvlb = env.character.getLevelMod();
			env.value *= lvlb * lvlb * intb * intb;
		}
	}

	private static class FuncPDefMod extends Func
	{
		private static final FuncPDefMod func = new FuncPDefMod();

		private FuncPDefMod()
		{
			super(Stats.POWER_DEFENCE, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= env.character.getLevelMod();
		}
	}

	private static class FuncMDefMod extends Func
	{
		private static final FuncMDefMod func = new FuncMDefMod();

		private FuncMDefMod()
		{
			super(Stats.MAGIC_DEFENCE, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= MENbonus[env.character.getMEN()] * env.character.getLevelMod();
		}
	}

	private static class FuncAttackRange extends Func
	{
		private static final FuncAttackRange func = new FuncAttackRange();

		private FuncAttackRange()
		{
			super(Stats.POWER_ATTACK_RANGE, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Weapon weapon = env.character.getActiveWeaponItem();
			if(weapon != null)
				env.value += weapon.getAttackRange();
		}
	}

	private static class FuncAtkAccuracy extends Func
	{
		private static final FuncAtkAccuracy func = new FuncAtkAccuracy();

		private FuncAtkAccuracy()
		{
			super(Stats.ACCURACY_COMBAT, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			// [Square(DEX)]*6 + lvl + weapon hitbonus;
			env.value += Math.sqrt(env.character.getDEX()) * 6 + env.character.getLevel();

			if(env.character.isPlayer())
			{
				if(env.character.getLevel() > 77)
					env.value += env.character.getLevel() - 77;
				if(env.character.getLevel() > 69)
					env.value += env.character.getLevel() - 69;
			}
			if(env.character.isSummon())
				env.value += env.character.getLevel() < 60 ? 4 : 5;
		}
	}

	private static class FuncAtkEvasion extends Func
	{
		private static final FuncAtkEvasion func = new FuncAtkEvasion();

		private FuncAtkEvasion()
		{
			super(Stats.EVASION_RATE, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			// [Square(DEX)]*6 + lvl;
			env.value += Math.sqrt(env.character.getDEX()) * 6;
			env.value += env.character.getLevel();
			if(env.character.isPlayer())
			{
				if(env.character.getLevel() > 77)
					env.value += env.character.getLevel() - 77;
				if(env.character.getLevel() > 69)
					env.value += env.character.getLevel() - 69;
			}
		}
	}

	private static class FuncMCriticalRateMul extends Func
	{
		private static final FuncMCriticalRateMul func = new FuncMCriticalRateMul();

		private FuncMCriticalRateMul()
		{
			super(Stats.MCRITICAL_RATE, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= WITbonus[env.character.getWIT()];
		}
	}

	private static class FuncPCriticalRateMul extends Func
	{
		private static final FuncPCriticalRateMul func = new FuncPCriticalRateMul();

		private FuncPCriticalRateMul()
		{
			super(Stats.CRITICAL_BASE, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			if(!(env.character instanceof L2Summon))
				env.value *= DEXbonus[env.character.getDEX()];
			env.value *= 0.01 * env.character.calcStat(Stats.CRITICAL_RATE, 100, env.target, env.skill);
		}
	}

	private static class FuncMoveSpeedMul extends Func
	{
		private static final FuncMoveSpeedMul func = new FuncMoveSpeedMul();

		private FuncMoveSpeedMul()
		{
			super(Stats.RUN_SPEED, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= DEXbonus[env.character.getDEX()];
		}
	}

	private static class FuncPAtkSpeedMul extends Func
	{
		private static final FuncPAtkSpeedMul func = new FuncPAtkSpeedMul();

		private FuncPAtkSpeedMul()
		{
			super(Stats.POWER_ATTACK_SPEED, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= DEXbonus[env.character.getDEX()];
		}
	}

	private static class FuncMAtkSpeedMul extends Func
	{
		private static final FuncMAtkSpeedMul func = new FuncMAtkSpeedMul();

		private FuncMAtkSpeedMul()
		{
			super(Stats.MAGIC_ATTACK_SPEED, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= WITbonus[env.character.getWIT()];
		}
	}

	private static class FuncHennaSTR extends Func
	{
		private static final FuncHennaSTR func = new FuncHennaSTR();

		private FuncHennaSTR()
		{
			super(Stats.STAT_STR, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatSTR());
		}
	}

	private static class FuncHennaDEX extends Func
	{
		private static final FuncHennaDEX func = new FuncHennaDEX();

		private FuncHennaDEX()
		{
			super(Stats.STAT_DEX, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatDEX());
		}
	}

	private static class FuncHennaINT extends Func
	{
		private static final FuncHennaINT func = new FuncHennaINT();

		private FuncHennaINT()
		{
			super(Stats.STAT_INT, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatINT());
		}
	}

	private static class FuncHennaMEN extends Func
	{
		private static final FuncHennaMEN func = new FuncHennaMEN();

		private FuncHennaMEN()
		{
			super(Stats.STAT_MEN, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatMEN());
		}
	}

	private static class FuncHennaCON extends Func
	{
		private static final FuncHennaCON func = new FuncHennaCON();

		private FuncHennaCON()
		{
			super(Stats.STAT_CON, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatCON());
		}
	}

	private static class FuncHennaWIT extends Func
	{
		private static final FuncHennaWIT func = new FuncHennaWIT();

		private FuncHennaWIT()
		{
			super(Stats.STAT_WIT, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player pc = (L2Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatWIT());
		}
	}

	private static class FuncMaxHpAdd extends Func
	{
		private static final FuncMaxHpAdd func = new FuncMaxHpAdd();

		private FuncMaxHpAdd()
		{
			super(Stats.MAX_HP, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2PlayerTemplate t = (L2PlayerTemplate) env.character.getTemplate();
			final int lvl = env.character.getLevel() - t.classBaseLevel;
			final double hpmod = t.lvlHpMod * lvl;
			final double hpmax = (t.lvlHpAdd + hpmod) * lvl;
			final double hpmin = t.lvlHpAdd * lvl + hpmod;
			env.value += (hpmax + hpmin) / 2;
		}
	}

	private static class FuncMaxHpMul extends Func
	{
		private static final FuncMaxHpMul func = new FuncMaxHpMul();

		private FuncMaxHpMul()
		{
			super(Stats.MAX_HP, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= CONbonus[env.character.getCON()];
		}
	}

	private static class FuncMaxCpAdd extends Func
	{
		private static final FuncMaxCpAdd func = new FuncMaxCpAdd();

		private FuncMaxCpAdd()
		{
			super(Stats.MAX_CP, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2PlayerTemplate t = (L2PlayerTemplate) env.character.getTemplate();
			final int lvl = env.character.getLevel() - t.classBaseLevel;
			final double cpmod = t.lvlCpMod * lvl;
			final double cpmax = (t.lvlCpAdd + cpmod) * lvl;
			final double cpmin = t.lvlCpAdd * lvl + cpmod;
			env.value += (cpmax + cpmin) / 2;
		}
	}

	private static class FuncMaxCpMul extends Func
	{
		private static final FuncMaxCpMul func = new FuncMaxCpMul();

		private FuncMaxCpMul()
		{
			super(Stats.MAX_CP, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			double cpSSmod = 1;
			final int sealOwnedBy = SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE);
			final int playerCabal = SevenSigns.getInstance().getPlayerCabal((L2Player) env.character);

			if(sealOwnedBy != SevenSigns.CABAL_NULL)
				if(playerCabal == sealOwnedBy)
					cpSSmod = 1.1;
				else
					cpSSmod = 0.9;

			env.value *= CONbonus[env.character.getCON()] * cpSSmod;
		}
	}

	private static class FuncMaxMpAdd extends Func
	{
		private static final FuncMaxMpAdd func = new FuncMaxMpAdd();

		private FuncMaxMpAdd()
		{
			super(Stats.MAX_MP, 0x10, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2PlayerTemplate t = (L2PlayerTemplate) env.character.getTemplate();
			final int lvl = env.character.getLevel() - t.classBaseLevel;
			final double mpmod = t.lvlMpMod * lvl;
			final double mpmax = (t.lvlMpAdd + mpmod) * lvl;
			final double mpmin = t.lvlMpAdd * lvl + mpmod;
			env.value += (mpmax + mpmin) / 2;
		}
	}

	private static class FuncMaxMpMul extends Func
	{
		private static final FuncMaxMpMul func = new FuncMaxMpMul();

		private FuncMaxMpMul()
		{
			super(Stats.MAX_MP, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			env.value *= MENbonus[env.character.getMEN()];
		}
	}

	private static class FuncPhysicalDamageResists extends Func
	{
		private static final FuncPhysicalDamageResists func = new FuncPhysicalDamageResists();

		private FuncPhysicalDamageResists()
		{
			super(Stats.PHYSICAL_DAMAGE, 0x30, null);
		}

		@Override
		public void calc(final Env env)
		{
			if(env.target instanceof L2RaidBossInstance && Math.abs(env.character.getLevel() - env.target.getLevel()) > Config.RAID_MAX_LEVEL_DIFF)
			{
				env.value = 1;
				return;
			}

			final L2Weapon weapon = env.character.getActiveWeaponItem();
			if(weapon == null)
				env.value *= 0.01 * env.target.calcStat(Stats.FIST_WPN_RECEPTIVE, 100, env.character, env.skill);
			else if(weapon.getItemType().getDefence() != null)
				env.value *= 0.01 * env.target.calcStat(weapon.getItemType().getDefence(), 100, env.character, env.skill);

			env.value = calcDamageResists(env.skill, env.character, env.target, env.value);
		}
	}

	private static class FuncMagicDamageResists extends Func
	{
		private static final FuncMagicDamageResists func = new FuncMagicDamageResists();

		private FuncMagicDamageResists()
		{
			super(Stats.MAGIC_DAMAGE, 0x30, null);
		}

		@Override
		public void calc(final Env env)
		{
			if(env.target.isRaid() && Math.abs(env.character.getLevel() - env.target.getLevel()) > Config.RAID_MAX_LEVEL_DIFF)
			{
				env.value = 1;
				return;
			}

			env.value = calcDamageResists(env.skill, env.character, env.target, env.value);
		}
	}

	private static class FuncInventory extends Func
	{
		private static final FuncInventory func = new FuncInventory();

		private FuncInventory()
		{
			super(Stats.INVENTORY_LIMIT, 0x01, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player _cha = (L2Player) env.character;
			if(_cha.isGM())
				env.value = Config.INVENTORY_MAXIMUM_GM;
			else if(_cha.getTemplate().race == Race.dwarf)
				env.value = Config.INVENTORY_MAXIMUM_DWARF;
			else
				env.value = Config.INVENTORY_MAXIMUM_NO_DWARF;
			env.value += _cha.getExpandInventory();
		}
	}

	private static class FuncWarehouse extends Func
	{
		private static final FuncWarehouse func = new FuncWarehouse();

		private FuncWarehouse()
		{
			super(Stats.STORAGE_LIMIT, 0x01, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player _cha = (L2Player) env.character;
			if(_cha.getTemplate().race == Race.dwarf)
				env.value = Config.WAREHOUSE_SLOTS_DWARF;
			else
				env.value = Config.WAREHOUSE_SLOTS_NO_DWARF;
			env.value += _cha.getExpandWarehouse();
		}
	}

	private static class FuncTradeLimit extends Func
	{
		private static final FuncTradeLimit func = new FuncTradeLimit();

		private FuncTradeLimit()
		{
			super(Stats.TRADE_LIMIT, 0x01, null);
		}

		@Override
		public void calc(final Env env)
		{
			final L2Player _cha = (L2Player) env.character;
			if(_cha.getRace() == Race.dwarf)
				env.value = Config.MAX_PVTSTORE_SLOTS_DWARF;
			else
				env.value = Config.MAX_PVTSTORE_SLOTS_OTHER;
		}
	}

	private static class FuncSDefAll extends Func
	{
		private static final FuncSDefAll func = new FuncSDefAll();

		private FuncSDefAll()
		{
			super(Stats.SHIELD_RATE, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			if(env.value == 0)
				return;

			final L2Character target = env.target;
			if(target != null)
			{
				final L2Weapon weapon = target.getActiveWeaponItem();
				if(weapon != null)
					switch (weapon.getItemType())
					{
					// if attacker use bow and target wear shield, shield block rate is multiplied by 1.3 (30%)
						case BOW:
						case CROSSBOW:
							env.value += 30.;
							break;
						case DAGGER:
						case DUALDAGGER:
							env.value += 12.;
							break;
					}
			}
		}
	}

	private static class FuncSDefPlayers extends Func
	{
		private static final FuncSDefPlayers func = new FuncSDefPlayers();

		private FuncSDefPlayers()
		{
			super(Stats.SHIELD_RATE, 0x20, null);
		}

		@Override
		public void calc(final Env env)
		{
			if(env.value == 0)
				return;

			final L2Character cha = env.character;
			final L2ItemInstance shld = ((L2Player) cha).getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			if(shld == null || shld.getItemType() != WeaponType.NONE)
				return;
			env.value *= DEXbonus[cha.getDEX()];
		}
	}

	/**
	 * Add basics Func objects to L2Player and L2Summon.<BR>
	 * <BR>
	 * <B><U> Concept</U> :</B><BR>
	 * <BR>
	 * A calculator is created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...). In fact, each calculator is a table of Func object in which
	 * each Func represents a mathematic function : <BR>
	 * <BR>
	 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<BR>
	 * <BR>
	 *
	 * @param cha
	 *            L2Player or L2Summon that must obtain basic Func objects
	 */
	public static void addFuncsToNewCharacter(final L2Character cha)
	{
		if(cha.isPlayer())
		{
			cha.addStatFunc(FuncMultRegenResting.getInstance(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getInstance(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getInstance(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenResting.getInstance(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getInstance(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getInstance(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenResting.getInstance(Stats.REGENERATE_MP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getInstance(Stats.REGENERATE_MP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getInstance(Stats.REGENERATE_MP_RATE));

			cha.addStatFunc(FuncMaxCpAdd.func);
			cha.addStatFunc(FuncMaxHpAdd.func);
			cha.addStatFunc(FuncMaxMpAdd.func);

			cha.addStatFunc(FuncMaxCpMul.func);
			cha.addStatFunc(FuncMaxHpMul.func);
			cha.addStatFunc(FuncMaxMpMul.func);

			cha.addStatFunc(FuncAttackRange.func);

			cha.addStatFunc(FuncMoveSpeedMul.func);

			cha.addStatFunc(FuncHennaSTR.func);
			cha.addStatFunc(FuncHennaDEX.func);
			cha.addStatFunc(FuncHennaINT.func);
			cha.addStatFunc(FuncHennaMEN.func);
			cha.addStatFunc(FuncHennaCON.func);
			cha.addStatFunc(FuncHennaWIT.func);

			cha.addStatFunc(FuncInventory.func);
			cha.addStatFunc(FuncWarehouse.func);
			cha.addStatFunc(FuncTradeLimit.func);

			cha.addStatFunc(FuncSDefPlayers.func);

			cha.addStatFunc(FuncPAtkMod.func);
			cha.addStatFunc(FuncMAtkMod.func);
			cha.addStatFunc(FuncPDefMod.func);
			cha.addStatFunc(FuncMDefMod.func);
		}

		if(!cha.isPet())
		{
			cha.addStatFunc(FuncAtkAccuracy.func);
			cha.addStatFunc(FuncAtkEvasion.func);
		}

		if(!cha.isPet() && !cha.isSummon())
		{
			cha.addStatFunc(FuncPAtkSpeedMul.func);
			cha.addStatFunc(FuncMAtkSpeedMul.func);
			cha.addStatFunc(FuncSDefAll.func);
		}

		cha.addStatFunc(FuncMCriticalRateMul.func);
		cha.addStatFunc(FuncPCriticalRateMul.func);

		cha.addStatFunc(FuncPhysicalDamageResists.func);
		cha.addStatFunc(FuncMagicDamageResists.func);
	}

	/**
	 * Calculate the HP regen rate (base + modifiers).<BR>
	 * <BR>
	 */
	public static double calcHpRegen(final L2Character cha)
	{
		double init;
		if(cha.isPlayer())
			init = (cha.getLevel() <= 10 ? 1.95 + cha.getLevel() / 20. : 1.4 + cha.getLevel() / 10.) * cha.getLevelMod() * CONbonus[cha.getCON()];
		else if(cha.isPet())
			init = ((L2PetInstance) cha).getPetData().getHpRegen() * CONbonus[cha.getCON()];
		else
			init = cha.getTemplate().baseHpReg * CONbonus[cha.getCON()];

		double hpRegenMultiplier = cha.isRaid() ? Config.RATE_RAID_REGEN : 1;
		if((Config.CHAMPION_WEAK_CHANCE > 0 || Config.CHAMPION_STRONG_CHANCE > 0) && cha.isMonster() && ((L2MonsterInstance) cha).getChampion() > 0)
			hpRegenMultiplier *= Config.CHAMPION_HP_REGEN;

		// Функция на реген HP не работает в замках/фортах/КХ во время осады
		if(cha instanceof L2Playable)
		{
			final L2Player player = cha.getPlayer();
			if(player != null && player.getClan() != null && player.getInResidence() != ResidenceType.None)
				switch (player.getInResidence())
				{
					case Clanhall:
						final int clanHallIndex = player.getClan().getHasHideout();
						if(clanHallIndex > 0)
						{
							final ClanHall clansHall = ClanHallManager.getInstance().getClanHall(clanHallIndex);
							if(clansHall != null && (clansHall.getSiege() == null || clansHall.getSiege() != null && !clansHall.getSiege().isInProgress()))
								if(clansHall.isFunctionActive(ResidenceFunction.RESTORE_HP))
									init *= 1. + clansHall.getFunction(ResidenceFunction.RESTORE_HP).getLevel() / 100.;
						}
						break;
					case Castle:
						final int caslteIndex = player.getClan().getHasCastle();
						if(caslteIndex > 0)
						{
							final Castle castle = CastleManager.getInstance().getCastleByIndex(caslteIndex);
							if(castle != null && !castle.getSiege().isInProgress())
								if(castle.isFunctionActive(ResidenceFunction.RESTORE_HP))
									init *= 1. + castle.getFunction(ResidenceFunction.RESTORE_HP).getLevel() / 100.;
						}
						break;
					case Fortress:
						final int fortIndex = player.getClan().getHasCastle();
						if(fortIndex > 0)
						{
							final Fortress fort = FortressManager.getInstance().getFortressByIndex(fortIndex);
							if(fort != null && !fort.getSiege().isInProgress())
								if(fort.isFunctionActive(ResidenceFunction.RESTORE_HP))
									init *= 1. + fort.getFunction(ResidenceFunction.RESTORE_HP).getLevel() / 100.;
						}
						break;
				}
		}

		return cha.calcStat(Stats.REGENERATE_HP_RATE, init, null, null) * hpRegenMultiplier;
	}

	/**
	 * Calculate the MP regen rate (base + modifiers).<BR>
	 * <BR>
	 */
	public static double calcMpRegen(final L2Character cha)
	{
		double init;
		if(cha.isPlayer())
			init = (.87 + cha.getLevel() * .03) * cha.getLevelMod() * MENbonus[cha.getMEN()];
		else if(cha.isPet())
			init = ((L2PetInstance) cha).getPetData().getMpRegen() * MENbonus[cha.getMEN()];
		else
			init = cha.getTemplate().baseMpReg * MENbonus[cha.getMEN()];

		final double mpRegenMultiplier = cha.isRaid() ? Config.RATE_RAID_REGEN : 1;

		// Функция на реген MP не работает в замках/фортах/КХ во время осады?
		if(cha instanceof L2Playable)
		{
			final L2Player player = cha.getPlayer();
			if(player != null && player.getClan() != null && player.getInResidence() != ResidenceType.None)
				switch (player.getInResidence())
				{
					case Clanhall:
						final int clanHallIndex = player.getClan().getHasHideout();
						if(clanHallIndex > 0)
						{
							final ClanHall clansHall = ClanHallManager.getInstance().getClanHall(clanHallIndex);
							if(clansHall != null/* && (clansHall.getSiege() == null || clansHall.getSiege() != null && !clansHall.getSiege().isInProgress()) */)
								if(clansHall.isFunctionActive(ResidenceFunction.RESTORE_MP))
									init *= 1. + clansHall.getFunction(ResidenceFunction.RESTORE_MP).getLevel() / 100.;
						}
						break;
					case Castle:
						final int caslteIndex = player.getClan().getHasCastle();
						if(caslteIndex > 0)
						{
							final Castle castle = CastleManager.getInstance().getCastleByIndex(caslteIndex);
							if(castle != null/* && !castle.getSiege().isInProgress() */)
								if(castle.isFunctionActive(ResidenceFunction.RESTORE_MP))
									init *= 1. + castle.getFunction(ResidenceFunction.RESTORE_MP).getLevel() / 100.;
						}
						break;
					case Fortress:
						final int fortIndex = player.getClan().getHasCastle();
						if(fortIndex > 0)
						{
							final Fortress fort = FortressManager.getInstance().getFortressByIndex(fortIndex);
							if(fort != null/* && !fort.getSiege().isInProgress() */)
								if(fort.isFunctionActive(ResidenceFunction.RESTORE_MP))
									init *= 1. + fort.getFunction(ResidenceFunction.RESTORE_MP).getLevel() / 100.;
						}
						break;
				}
		}

		return cha.calcStat(Stats.REGENERATE_MP_RATE, init, null, null) * mpRegenMultiplier;
	}

	/**
	 * Calculate the CP regen rate (base + modifiers).
	 */
	public static double calcCpRegen(final L2Character cha)
	{
		final double init = (1.5 + cha.getLevel() / 10) * cha.getLevelMod() * CONbonus[cha.getCON()];
		return cha.calcStat(Stats.REGENERATE_CP_RATE, init, null, null);
	}

	/**
	 * Для простых ударов
	 * patk = patk
	 * При крите простым ударом:
	 * patk = patk * (1 + crit_damage_rcpt) * crit_damage_mod + crit_damage_static
	 * Для blow скиллов
	 * TODO
	 * Для скилловых критов, повреждения просто удваиваются, бафы не влияют (кроме blow, для них выше)
	 * patk = (1 + crit_damage_rcpt) * (patk + skill_power)
	 * Для обычных атак
	 * damage = patk * ss_bonus * 70 / pdef
	 */
	public static AttackInfo calcPhysDam(final L2Character attacker, final L2Character target, final L2Skill skill, final boolean dual, final boolean blow, final boolean ss, final boolean onCrit)
	{
		final AttackInfo info = new AttackInfo();

		info.isPvP = attacker.isPlayable() && target.isPlayable();
		info.damage = attacker.getPAtk(target);
		info.defence = target.getPDef(attacker);
		info.crit_static = attacker.calcStat(Stats.CRITICAL_DAMAGE_STATIC, 0, target, skill);

		info.death_rcpt = 0.01 * target.calcStat(Stats.DEATH_RECEPTIVE, 100, target, skill);
		info.lethal1 = skill == null ? 0 : skill.getLethal1() * info.death_rcpt;
		info.lethal2 = skill == null ? 0 : skill.getLethal2() * info.death_rcpt;

		// Calculate if hit is critical
		info.crit = Rnd.chance(calcCrit(attacker, target, skill, blow));

		// Calculate if shield defense is efficient
		info.shld = (skill == null || !skill.isShieldIgnore()) && calcShldUse(attacker, target);

		info.lethal = false;
		info.miss = false;

		if(info.shld)
			info.defence += target.getShldDef();
		info.defence = Math.max(info.defence, 1);

		if(skill != null)
		{
			// считаем леталы для не blow скиллов
			// Считаем шанс летала, если скил не имеет блоу эффекта и не имеет имунитет к леталу
			if(!blow && !target.isLethalImmune())
				if(Rnd.chance(info.lethal1))
				{
					if(target.isPlayer())
					{
						info.lethal = true;
						info.lethal_dmg = target.getCurrentCp();
						target.sendPacket(Msg.CP_DISAPPEARS_WHEN_HIT_WITH_A_HALF_KILL_SKILL);
					}
					else
						info.lethal_dmg = target.getCurrentHp() / 2.;

					attacker.sendPacket(Msg.HALF_KILL);
				}
				else if(Rnd.chance(info.lethal2))
				{
					if(target.isPlayer())
					{
						info.lethal = true;
						info.lethal_dmg = target.getCurrentHp() + target.getCurrentCp() - 1.1; // Oly\Duel хак установки не точно 1 ХП, а чуть больше для предотвращения псевдосмерти
						target.sendPacket(Msg.INSTANT_KILL);
					}
					else
						info.lethal_dmg = target.getCurrentHp() - 1.;

					attacker.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}

			// Для скила Deadly Roulette
			// если скилл не имеет своей силы дальше идти бесполезно, можно сразу вернуть дамаг от летала
			if(skill.getPower(target) == 0)
			{
				info.damage = 0; // обычного дамага в этом случае не наносится
				return info;
			}

			if(blow && !skill.isBehind() && ss) // Для обычных blow не влияет на power
				info.damage *= 2.04; // 104% bonus with SS

			info.damage += Math.max(0., skill.getPower(target) * attacker.calcStat(Stats.SKILL_POWER, 1, null, null));
			
			if(blow && skill.isBehind() && ss) // Для backstab влияет на power, но меньше множитель
				info.damage *= 1.5;

			// Заряжаемые скилы имеют постоянный урон
			if(!skill.isChargeBoost())
				info.damage *= 1. + (Rnd.get() * attacker.getRandomDamage() * 2. - attacker.getRandomDamage()) / 100.;

			if(blow)
			{
				info.damage *= 0.01 * attacker.calcStat(Stats.CRITICAL_DAMAGE, 100, target, skill); // Штрафы на крит распространяются только на критическую часть вреда
				info.damage = target.calcStat(Stats.CRIT_DAMAGE_RECEPTIVE, info.damage, attacker, skill);
				info.damage += 6.1 * info.crit_static;
			}

			// thanks Diego Vargas of L2Guru: 70*((0.8+0.201*No.Charges) * (PATK+POWER)) / PDEF
			if(skill.isChargeBoost())
				info.damage *= 0.8 + 0.201 * attacker.getIncreasedForce();

			if(skill.isSoulBoost())
				info.damage *= 1.0 + 0.06 * Math.min(attacker.getConsumedSouls(), 5);

			// Gracia Physical Skill Damage Bonus
			info.damage *= 1.10113;

			if(info.crit)
				// Заряжаемые скилы игнорируют снижающие силу крита статы
				if(skill.isChargeBoost() || skill.getSkillType() == SkillType.CHARGE)
					info.damage *= 2.;
				else
					info.damage = 2 * target.calcStat(Stats.CRIT_DAMAGE_RECEPTIVE, info.damage, attacker, skill);
		}
		else
		{
			info.damage *= 1.0 + (Rnd.get() * attacker.getRandomDamage() * 2.0 - attacker.getRandomDamage()) / 100.0;

			if(dual)
				info.damage /= 2.;
			if(info.crit)
			{
				info.damage *= 0.01 * attacker.calcStat(Stats.CRITICAL_DAMAGE, 100, target, skill); // Штрафы на крит распространяются только на критическую часть вреда
				info.damage = 2 * target.calcStat(Stats.CRIT_DAMAGE_RECEPTIVE, info.damage, attacker, skill);
				info.damage += info.crit_static;
			}
		}

		if(info.crit)
		{
			// шанс абсорбации души (без анимации) при крите, если Soul Mastery 4го уровня или более
			int chance = attacker.getSkillLevel(L2Skill.SKILL_SOUL_MASTERY);
			if(chance > 0)
			{
				if(chance >= 21)
					chance = 30;
				else if(chance >= 15)
					chance = 25;
				else if(chance >= 9)
					chance = 20;
				else if(chance >= 4)
					chance = 15;
				if(Rnd.chance(chance))
					attacker.setConsumedSouls(attacker.getConsumedSouls() + 1, null);
			}
		}

		// в грации дамаг со спины и с боков повышен
		switch (attacker.getDirectionTo(target, true))
		{
			case BEHIND:
				info.damage *= 1.2; // +20% crit dmg when back stabbed
				break;
			case SIDE:
				info.damage *= 1.1; // +10% crit dmg when side stabbed
				break;
		}

		if(ss)
			info.damage *= blow ? 1.0 : 2.0;

		info.damage *= 70. / info.defence;
		info.damage = attacker.calcStat(Stats.PHYSICAL_DAMAGE, info.damage, target, skill);

		if(info.shld && Rnd.chance(5))
			info.damage = 1;

		// Dmg bonusses in PvP fight
		if(info.isPvP)
		{
			if(skill == null)
			{
				info.damage *= attacker.calcStat(Stats.PVP_PHYSICAL_DMG, 1, null, null);
				info.damage /= target.calcStat(Stats.PVP_PHYSICAL_DEF, 1, null, null);
			}
			else
			{
				info.damage *= attacker.calcStat(Stats.PVP_PHYS_SKILL_DMG, 1, null, null);
				info.damage /= target.calcStat(Stats.PVP_PHYS_SKILL_DEF, 1, null, null);
			}
		}

		// Тут проверяем только если skill != null, т.к. L2Character.onHitTimer не обсчитывает дамаг.
		if(skill != null)
		{
			if(info.shld)
				if(info.damage == 1)
					target.sendPacket(Msg.YOUR_EXCELLENT_SHIELD_DEFENSE_WAS_A_SUCCESS);
				else
					target.sendPacket(Msg.SHIELD_DEFENSE_HAS_SUCCEEDED);

			// Уворот от физ скилов уводит атаку в 0
			if(info.damage > 1 && !skill.hasEffects() && Rnd.chance(target.calcStat(Stats.EVADE_PHYSIC_SKILL, 0, null, skill)))
			{
				attacker.sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_WENT_ASTRAY).addName(attacker));
				target.sendPacket(new SystemMessage(SystemMessage.S1_HAS_EVADED_S2S_ATTACK).addName(target).addName(attacker));
				info.damage = 1;
			}

			if(info.damage > 1. && skill.isDeathlink())
				info.damage *= DEATHLINK_PDAM_MULTIPLIER * (1.0 - attacker.getCurrentHpRatio());

			if(onCrit && !calcBlow(attacker, target, skill))
			{
				info.miss = true;
				info.damage = 0.;
				attacker.sendPacket(new SystemMessage(SystemMessage.S1S_ATTACK_WENT_ASTRAY).addName(attacker));
			}

			if(blow)
			{
				if(calcLethal(attacker, target, info.lethal1))
				{
					if(target.isPlayer())
					{
						info.lethal = true;
						info.lethal_dmg = target.getCurrentCp();
						target.sendPacket(Msg.CP_DISAPPEARS_WHEN_HIT_WITH_A_HALF_KILL_SKILL);
					}
					else if(target.isLethalImmune())
						info.damage *= 2;
					else
						info.lethal_dmg = target.getCurrentHp() / 2;
					attacker.sendPacket(Msg.HALF_KILL);
				}
				if(calcLethal(attacker, target, info.lethal2))
				{
					if(target.isPlayer())
					{
						info.lethal = true;
						info.lethal_dmg = target.getCurrentHp() + target.getCurrentCp() - 1.1;
						target.sendPacket(Msg.INSTANT_KILL);
					}
					else if(target.isLethalImmune())
						info.damage *= 3;
					else
						info.lethal_dmg = target.getCurrentHp() - 1;
					attacker.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}
			}

			if(info.damage > 0)
				// Отправляем сообщения о дамаге)
				if(attacker.isSummon0())
					((L2Summon) attacker).displayHitMessage(target, (int) info.damage, info.crit || blow, false);
				else if(attacker.isPlayer())
				{
					if(info.crit || blow)
						attacker.sendPacket(new SystemMessage(SystemMessage.S1_HAD_A_CRITICAL_HIT).addName(attacker));
					attacker.sendPacket(new SystemMessage(SystemMessage.S1_HAS_GIVEN_S2_DAMAGE_OF_S3).addName(attacker).addName(target).addNumber((long) info.damage));
				}

			if(target.isStunned() && calcStunBreak(target, info.damage, info.crit))
				target.getEffectList().stopEffects(EffectType.Stun, EffectType.Turner);

			if(calcCastBreak(target, info.crit))
				target.breakCast(false);
		}

		info.damage = Math.max(0, info.damage);
		return info;
	}

	/** Calculate the chance of stun break when hit */
	public static boolean calcStunBreak(final L2Character target, final double dmg, final boolean crit)
	{
		/** Info by Ewan @ Franz */
		if(target.isStunned() && dmg > 100 && Rnd.get(100) < 25 * Math.sqrt(CONbonus[target.getCON()]) * (crit ? 2 : 1))
			return true;
		return false;
	}

	public static void doCounterAttack(final L2Skill skill, final L2Character attacker, final L2Character attacked, boolean blow)
	{
		if(attacked.isInvul() || attacker.isInvul()) // Не отражаем, если есть неуязвимость, иначе она может отмениться
			return;

		if(attacked.isDead() || attacker.isDead())
			return;

		if(skill == null || skill.isMagic() || !skill.isOffensive() || skill.getCastRange() > 200)
			return;

		if(Rnd.chance(attacked.calcStat(Stats.COUNTER_ATTACK, 0, attacker, skill)))
		{
			final double damage = 1189 * attacked.getPAtk(null) / attacker.getPDef(null);
			if(damage > 0)
			{
				attacker.sendPacket(new SystemMessage(SystemMessage.S1_PERFORMING_COUNTERATTACK).addName(attacked));
				attacked.sendPacket(new SystemMessage(SystemMessage.COUNTERED_S1_ATTACK).addName(attacker));
				if(blow) // урон х2 для отражения blow скиллов
				{
					skill.displayHitMessage(attacked, attacker, (long) damage, false);
					attacker.reduceCurrentHp(damage, attacked, skill, true, true, false, false, true);
				}
				skill.displayHitMessage(attacked, attacker, (long) damage, false);
				attacker.reduceCurrentHp(damage, attacked, skill, true, true, false, false, true);
			}
		}
	}

	public static double calcMagicDam(final L2Character attacker, final L2Character target, final L2Skill skill, final int sps)
	{
		// Параметр ShieldIgnore для магических скиллов инвертирован
		final boolean shield = skill.getShieldIgnore() && Formulas.calcShldUse(attacker, target);

		final boolean isPvP = attacker.isPlayable() && target.isPlayable();
		double mAtk = attacker.getMAtk(target, skill);
		double mdef = target.getMDef(null, skill);

		if(sps == 2)
			mAtk *= 4;
		else if(sps == 1)
			mAtk *= 2;

		// Pvp bonuses for def
		if(isPvP)
			if(skill.isMagic())
				mdef = target.calcStat(Stats.PVP_MAGICAL_DEF, mdef, null, null);
			else
				mdef = target.calcStat(Stats.PVP_PHYS_SKILL_DEF, mdef, null, null);

		if(shield)
			mdef += target.getShldDef();
		if(mdef == 0)
			mdef = 1;

		double power = skill.getPower(target);
		double lethalDamage = 0;

		if(skill.getLethal1() > 0 && Rnd.chance(skill.getLethal1()))
		{
			if(target.isPlayer())
			{
				lethalDamage = target.getCurrentCp();
				target.sendPacket(Msg.CP_DISAPPEARS_WHEN_HIT_WITH_A_HALF_KILL_SKILL);
			}
			else if(!target.isLethalImmune())
				lethalDamage = target.getCurrentHp() / 2;
			else
				power *= 2;
			attacker.sendPacket(Msg.HALF_KILL);
		}
		else if(skill.getLethal2() > 0 && Rnd.chance(skill.getLethal2()))
		{
			if(target.isPlayer())
			{
				lethalDamage = target.getCurrentHp() + target.getCurrentCp() - 1;
				target.sendPacket(Msg.INSTANT_KILL);
			}
			else if(!target.isLethalImmune())
				lethalDamage = target.getCurrentHp() - 1;
			else
				power *= 3;
			attacker.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
		}

		if(power == 0)
		{
			if(lethalDamage > 0)
				skill.displayHitMessage(attacker, target, (long) lethalDamage, false);
			return lethalDamage;
		}

		if(skill.isSoulBoost())
			power *= 1. + 0.06 * Math.min(attacker.getConsumedSouls(), 5);

		double damage = 91. * power * Math.sqrt(mAtk) / mdef;

		damage *= 1f + (Rnd.get() * attacker.getRandomDamage() * 2f - attacker.getRandomDamage()) / 100f;

		final boolean crit = calcMCrit(attacker.getCriticalMagic(target, skill));

		if(crit)
			damage *= attacker.calcStat(Stats.MCRITICAL_DAMAGE, attacker.isPlayable() && target.isPlayable() ? 2.5 : 3, target, skill);
		damage = attacker.calcStat(Stats.MAGIC_DAMAGE, damage, target, skill);

		// Pvp bonuses for dmg
		if(isPvP)
			if(skill.isMagic())
				damage = attacker.calcStat(Stats.PVP_MAGICAL_DMG, damage, null, null);
			else
				damage = attacker.calcStat(Stats.PVP_PHYS_SKILL_DMG, damage, null, null);

		if(shield && Config.ALT_MDAM_SHILEDS && Rnd.chance(Config.ALT_MDAM_PERFECT_SHILEDS_RATE))
		{
			if(attacker.isPlayer())
				attacker.sendPacket(Msg.ATTACK_WAS_BLOCKED);
			if(target.isPlayer())
				target.sendPacket(Msg.YOUR_EXCELLENT_SHIELD_DEFENSE_WAS_A_SUCCESS);
			damage = 1;
		}

		/** маджик фейлы */
		if(Config.ALT_GAME_MAGICFAILURES)
		{
			final int mLevel = skill.getMagicLevel() == 0 || !attacker.isPlayer() ? attacker.getLevel() : skill.getMagicLevel();
			final int levelDiff = target.getLevel() - mLevel;

			// add min checking cap
			if(levelDiff > Config.ALT_MIN_LVLDEPEND_MAGIC_SUCCES)
			{
				final double magic_rcpt = target.calcStat(Stats.MAGIC_RECEPTIVE, 0, attacker, skill) - attacker.calcStat(Stats.MAGIC_POWER, 0, target, skill);
				final double failChance = 5. * Math.max(1, levelDiff) * (1. + magic_rcpt / 100.);

				if(attacker.isPlayer() && attacker.getPlayer().isGM())
				{
					attacker.sendMessage("[F] skill: " + skill);
					attacker.sendMessage("[F] mLevel: " + mLevel);
					attacker.sendMessage("[F] tLevel: " + target.getLevel());
					attacker.sendMessage("[F] level diff: " + levelDiff);
					attacker.sendMessage("[F] magic fail: " + (int) (0.1 * failChance));
				}

				if(Rnd.chance(0.1 * failChance))
				{
					damage = 1;
					final SystemMessage msg = new SystemMessage(SystemMessage.S1_RESISTED_S2S_MAGIC).addName(target).addName(attacker);
					attacker.sendPacket(msg);
					target.sendPacket(msg);
				}
				else if(Rnd.chance(failChance))
				{
					damage /= 2;
					final SystemMessage msg = new SystemMessage(SystemMessage.DAMAGE_IS_DECREASED_BECAUSE_S1_RESISTED_AGAINST_S2S_MAGIC).addName(target).addName(attacker);
					attacker.sendPacket(msg);
					target.sendPacket(msg);
				}
			}
		}

		// TODO
		// if(Rnd.chance(target.calcStat(Stats.MSKILL_EVASION, 0.0D, attacker, skill)))
		// {
		// attacker.sendPacket(new L2GameServerPacket[] { new SystemMessage(2265).addName(attacker) });
		// target.sendPacket(new L2GameServerPacket[] { new SystemMessage(2264).addName(target).addName(attacker) });
		// damage = 1.0D;
		// }

		if(damage > 1. && skill.isDeathlink())
			damage *= DEATHLINK_PDAM_MULTIPLIER * (1.0 - attacker.getCurrentHpRatio());

		if(damage > 1 && skill.getDependOnTargetBuff() != 0)
			damage *= 1 + skill.getDependOnTargetBuff() * target.getEffectList().getAllEffects().length;

		damage += lethalDamage;

		if(skill.getSkillType() == SkillType.MANADAM)
			damage = Math.max(1, damage / 2.);

		if(attacker instanceof L2Summon)
			((L2Summon) attacker).displayHitMessage(target, (int) damage, crit, false);
		else
			skill.displayHitMessage(attacker, target, (long) damage, crit);

		if(calcCastBreak(target, crit))
			target.breakCast(false);

		return damage;
	}

	/**
	 * Returns true in case of fatal blow success. <BR>
	 * Calculate chance based on DEX, Position and on self BUFF
	 */
	public static boolean calcBlow(final L2Character activeChar, final L2Character target, final L2Skill skill)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		final double base_weapon_crit = weapon == null ? 4 : weapon.getCritical();
		final double dex_bonus = DEXbonus[activeChar.getDEX()];

		// разница высот, на которых стоят атакующий персонаж и его жертва (из координаты z атакующего вычитаем координату z жертвы)
		final double clamped_dz = Math.min(25, Math.max(-25, target.getZ() - activeChar.getZ()));
		final double crit_height_bonus = 0.008 * clamped_dz + 1.1;

		final double buffs_mult = activeChar.calcStat(Stats.BLOW_RATE, 1, target, skill);
		final double skill_mod = skill.isBehind() ? 4 : 3;

		double chance = dex_bonus * base_weapon_crit * buffs_mult * crit_height_bonus * skill_mod;

		if(!target.isInCombat())
			chance *= 1.1;

		final int head = activeChar.getHeadingTo(target, true);
		if(head <= 10430 || head >= 55105)
			chance *= 1.3;
		else if(skill.isBehind())
			chance = 3;
		else if(head <= 21000 || head >= 44500)
			chance *= 1.1;

		chance = Math.min(skill.isBehind() ? 100 : 80, chance);

		final boolean success = Rnd.chance(chance);
		if(Config.SKILLS_SHOW_CHANCE && activeChar.isPlayer() && !((L2Player) activeChar).getVarB("SkillsHideChance"))
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.skills.Formulas.Chance", activeChar).addString(skill.getName()).addNumber((int) chance));
			if(activeChar.getPlayer().isGM())
			{
				activeChar.sendMessage("[F] skill: " + skill + " is " + success);
				activeChar.sendMessage("[F] chance: " + (int) chance);
				activeChar.sendMessage("[F] direction: " + activeChar.getDirectionTo(target, true));
			}
		}
		return success;
	}

	/** Returns true in case of critical hit */
	public static double calcCrit(final L2Character attacker, final L2Character target, final L2Skill skill, final boolean blow)
	{
		if(attacker.isPlayer() && attacker.getActiveWeaponItem() == null)
			return 0;

		if(skill != null)
			return skill.getCriticalRate() * (blow ? DEXbonus[attacker.getDEX()] : STRbonus[attacker.getSTR()]);

		double rate = attacker.getCriticalHit(target, null) * 0.01 * target.calcStat(Stats.CRIT_CHANCE_RECEPTIVE, 100, target, skill);
		switch (attacker.getDirectionTo(target, true))
		{
			case BEHIND:
				rate *= 1.4;
				break;
			case SIDE:
				rate *= 1.2;
		}

		return rate / 10;
	}

	/** Calculate value of lethal chance */
	public static final boolean calcLethal(final L2Character activeChar, final L2Character target, final double baseChance)
	{
		double chance = baseChance;
		if(chance <= 0)
			return false;

		// уменьшение шанса летала
		if(target.isNpc() && target.getLevel() >= 80)
			chance = chance * 2 / 3;

		// Отображение шанса прохождения скила
		if(Config.SKILLS_SHOW_CHANCE && activeChar.isPlayer() && activeChar.getPlayer().isGM())
		{
			final L2Player player = activeChar.getPlayer();
			if(player != null && !player.getVarB("SkillsHideChance"))
				activeChar.sendMessage("Lethal chance: " + numFormatter.format(chance));
		}

		return Rnd.chance(chance);
	}

	/** Returns true in case of magic critical hit */
	public static boolean calcMCrit(final double mRate)
	{
		// floating point random gives more accuracy calculation, because argument also floating point
		return Rnd.get() * 100.0D <= Math.min(Config.LIM_MCRIT, mRate);
	}

	/** Returns true in case when ATTACK is canceled due to hit */
	public static boolean calcCastBreak(final L2Character target, final boolean crit)
	{
		if(target == null)
			return false;

		final L2Skill skill = target.getCastingSkill();
		if(skill != null && (skill.getSkillType() == SkillType.TAKECASTLE || skill.getSkillType() == SkillType.TAKEFORT || skill.getSkillType() == SkillType.TAKEFLAG))
			return false;

		return !(!target.isCastingNow() || target.isInvul() || target.isRaid()) && Rnd.chance((int) target.calcStat(Stats.CAST_INTERRUPT, crit ? 75 : 10, null, null));
	}

	/** Calculate delay (in milliseconds) before next ATTACK */
	public static int calcPAtkSpd(final double rate)
	{
		if(rate < 2)
			return 2700;
		else
			return (int) (500000 / rate);
	}

	/** Calculate delay (in milliseconds) for skills cast */
	public static int calcMAtkSpd(final L2Character attacker, final L2Skill skill, final double skillTime)
	{
		if(skill.isMagic())
			return (int) (skillTime * 333 / Math.max(attacker.getMAtkSpd(), 1));
		return (int) (skillTime * 333 / Math.max(attacker.getPAtkSpd(), 1));
	}

	/** Calculate reuse delay (in milliseconds) for skills */
	public static int calcSkillReuseDelay(final L2Character actor, final L2Skill skill)
	{
		int reuseDelay = skill.getReuseDelay();
		if(actor.isMonster())
			reuseDelay = skill.getReuseForMonsters();
		if(skill.isSkillReusePermanent() || skill.isHandler() || skill.isItemSkill())
			return reuseDelay;
		if(actor.getSkillMastery(skill.getId()) == 1)
		{
			actor.removeSkillMastery(skill.getId());
			return 0;
		}

		if(skill.isMagic())
			return (int) actor.calcStat(Stats.MAGIC_REUSE_RATE, reuseDelay, null, skill);
		return (int) actor.calcStat(Stats.PHYSIC_REUSE_RATE, reuseDelay, null, skill);
	}

	/**
	 * Returns true if hit missed (target evaded)<br>
	 * Formula based on http://l2p.l2wh.com/nonskillattacks.html
	 **/
	public static boolean calcHitMiss(final L2Character attacker, final L2Character target)
	{
		final int delta = attacker.getAccuracy() - target.getEvasionRate(attacker);
		int chance;
		if(delta >= 10)
			chance = 980;
		else
		{
			switch (delta)
			{
				case 9:
					chance = 975;
					break;
				case 8:
					chance = 970;
					break;
				case 7:
					chance = 965;
					break;
				case 6:
					chance = 960;
					break;
				case 5:
					chance = 955;
					break;
				case 4:
					chance = 945;
					break;
				case 3:
					chance = 935;
					break;
				case 2:
					chance = 925;
					break;
				case 1:
					chance = 915;
					break;
				case 0:
					chance = 905;
					break;
				case -1:
					chance = 890;
					break;
				case -2:
					chance = 875;
					break;
				case -3:
					chance = 860;
					break;
				case -4:
					chance = 845;
					break;
				case -5:
					chance = 830;
					break;
				case -6:
					chance = 815;
					break;
				case -7:
					chance = 800;
					break;
				case -8:
					chance = 785;
					break;
				case -9:
					chance = 770;
					break;
				case -10:
					chance = 755;
					break;
				case -11:
					chance = 735;
					break;
				case -12:
					chance = 715;
					break;
				case -13:
					chance = 695;
					break;
				case -14:
					chance = 675;
					break;
				case -15:
					chance = 655;
					break;
				case -16:
					chance = 625;
					break;
				case -17:
					chance = 595;
					break;
				case -18:
					chance = 565;
					break;
				case -19:
					chance = 535;
					break;
				case -20:
					chance = 505;
					break;
				case -21:
					chance = 455;
					break;
				case -22:
					chance = 405;
					break;
				case -23:
					chance = 355;
					break;
				case -24:
					chance = 305;
					break;
				default:
					chance = 275;
			}
			if(!attacker.isInFrontOfTarget())
			{
				if(attacker.isBehindTarget())
					chance *= 1.2;
				else
					// side
					chance *= 1.1;
				if(chance > 980)
					chance = 980;
			}
		}

		return chance < Rnd.get(1000);
	}

	/** Returns true if shield defence successfull */
	public static boolean calcShldUse(final L2Character attacker, final L2Character target)
	{
		final int angle = (int) target.calcStat(Stats.SHIELD_ANGLE, 60, null, null);
		if(!target.isInFront(attacker, angle))
			return false;

		double shldRate = 0.;
		if((shldRate = target.calcStat(Stats.SHIELD_RATE, target.getTemplate().baseShldRate, attacker, null)) == 0)
			return false;

		return Rnd.chance(shldRate);
	}

	public static double calcSavevsDependence(final int save, final L2Character cha)
	{
		try
		{
			switch (save)
			{
				case L2Skill.SAVEVS_INT:
					return cha.getINT();
				case L2Skill.SAVEVS_WIT:
					return cha.getWIT();
				case L2Skill.SAVEVS_MEN:
					return cha.getMEN();
				case L2Skill.SAVEVS_CON:
					return cha.getCON();
				case L2Skill.SAVEVS_DEX:
					return cha.getDEX();
				case L2Skill.SAVEVS_STR:
					return cha.getSTR();
			}
		}
		catch(final ArrayIndexOutOfBoundsException e)
		{
			_log.warning("Failed calc savevs on char " + cha + " with save-stat " + save);
		}
		return 1.;
	}

	public static boolean calcSkillSuccess(final Env env, final Stats resistType, final Stats attackType, final int spiritshot)
	{
		if(env.value == -1)
			return true;

		final L2Skill skill = env.skill;
		// если скилл не для атаки, остально считать не нужно
		if(!skill.isOffensive())
			return Rnd.chance(env.value);

		final L2Character character = env.character;
		final L2Character target = env.target;
		// не учитываем резисты когда бьются два НПС
		final boolean monsterVSmonster = character.isMonster() && target.isMonster();

		// На всякий случай
		env.value = Math.max(Math.min(env.value, 100), 1);

		// Запоминаем базовый шанс (нужен позже)
		final double base = env.value;
		final boolean isGM = character.isPlayer() && ((L2Player) character).isGM();

		/** отображаем начальные данные */
		if(isGM)
		{
			character.sendMessage("[F] skill: " + skill);
			character.sendMessage("[F] base chance: " + env.value);
		}

		/** Бонус от MEN/CON/etc */
		if(skill.getSavevs() > 0)
		{
			// Бонус от MEN/CON/etc
			env.value += 30 - calcSavevsDependence(skill.getSavevs(), target); // Бонус от MEN/CON/etc
			if(isGM)
				character.sendMessage("[F] save type: " + skill.getSavevs() + " / chance: " + numFormatter.format(env.value));
		}

		env.value = Math.max(env.value, 1);

		/** Бонус к шансу от разницы в уровнях */
		env.value *= getLevelModifier(skill, character, target);

		if(isGM)
			character.sendMessage("[F] magic level: " + numFormatter.format(getLevelModifier(skill, character, target)) + " / chance: " + numFormatter.format(env.value));

		/** Расчёт Matk/Mdef бонуса */
		if(skill.isMagic()) // Этот блок только для магических скиллов
		{
			// Вычисляем mDef цели
			final int mdef = Math.max(1, target.getMDef(target, skill));
			double mAtk = character.getMAtk(target, skill);

			// Считаем бонус от шотов
			if(skill.isSSPossible() && spiritshot > 0)
				mAtk *= spiritshot * 2;

			// формула зависимости от мАтк, мДеф и бонуса спирит шотов трепует проверки и возможно корректировки
			final double mAtkMod = Math.sqrt(13.5 * Math.pow(mAtk, 0.5) / mdef);

			// if(mAtkMod < 0.7)
			// mAtkMod = 0.7;
			// else if(mAtkMod > 1.4)
			// mAtkMod = 1.4;

			if(isGM)
			{
				character.sendMessage("[F] mAtk mod: " + numFormatter.format(mAtkMod) + " / chance: " + numFormatter.format(env.value * mAtkMod));
				// character.sendMessage("[F] mAtk mod: 2 = " + formatter.format(getMAtkBonusPhoenix(skill, character, target, spiritshot)) + " / chance: " + formatter.format(env.value * getMAtkBonusPhoenix(skill, character, target, spiritshot)));
				// character.sendMessage("[F] mAtk mod: 3 = " + formatter.format(getMAtkBonusDuoTM(skill, character, target, spiritshot)) + " / chance: " + formatter.format(env.value * getMAtkBonusDuoTM(skill, character, target, spiritshot)));
			}
			env.value *= Config.SKILLS_CHANCE_MOD * mAtkMod;
		}

		/** Применяем различные сопротивляемости/восприимчивости */
		if(!skill.isIgnoreResists() && !monsterVSmonster)
		{
			double res = 0;
			// Различные сопротивляемости/восприимчивости
			if(resistType != null)
				res += target.calcStat(resistType, 0, target, skill); // в описании эффектов сила вычитается

			// Различные аттрибуты (не стихийные)
			if(attackType != null)
				res -= character.calcStat(attackType, 0, target, skill); // в описании эффектов резист добавляется

			res += target.calcStat(Stats.DEBUFF_RECEPTIVE, 0, target, skill);

			if(res != 0)
			{
				// применяем резисты
				final double mod = Math.abs(0.02 * res) + 1;
				// если резисты больше то делим шанс и получаем меньшее значение,
				env.value = res > 0 ? env.value / mod : env.value * mod;

				if(isGM)
				{
					if(resistType != null)
					{
						character.sendMessage("[F] resist: " + resistType);
						character.sendMessage("[F] defense: " + (int) target.calcStat(resistType, 0, target, skill));
					}
					if(attackType != null)
						character.sendMessage("[F] attack: " + (int) character.calcStat(attackType, 0, target, skill));
					character.sendMessage("[F] chance w/resist: " + numFormatter.format(env.value));
				}
			}
		}

		// Учитываем общий бонус к шансам, если есть
		env.value = character.calcStat(Stats.ACTIVATE_RATE, env.value, target, skill);

		// Если базовый шанс более Config.SKILLS_CHANCE_MIN, то при небольшой разнице в уровнях, делаем кап снизу.
		env.value = Math.max(env.value, Math.min(base, Config.SKILLS_CHANCE_MIN));

		// Применяем кап
		env.value = Math.max(Math.min(env.value, Config.SKILLS_CHANCE_CAP), 1);

		// Отображение шанса прохождения скилов у Монстров
		if(target.isPlayer())
		{
			final L2Player player = (L2Player) target;
			if(Config.SKILLS_SHOW_CHANCE_MONSTER && character.isMonster() && (player.isGM() || player.getVarB("SkillsMobChance")))
				target.sendMessage(character.getName() + ": " + new CustomMessage("l2n.game.skills.Formulas.Chance", target).addString(skill.getName()).addNumber(Math.round(env.value)).toString());
		}

		// Отображение шанса прохождения скила
		if(Config.SKILLS_SHOW_CHANCE || isGM)
		{
			final L2Player player = character.getPlayer();
			if(player != null && !player.getVarB("SkillsHideChance"))
				player.sendMessage(new CustomMessage("l2n.game.skills.Formulas.Chance", player).addString(skill.getName()).addNumber(Math.round(env.value)));
		}
		return Rnd.chance(env.value);
	}

	public static boolean calcSkillSuccess(final L2Character player, final L2Character target, final L2Skill skill)
	{
		return calcSkillSuccess(player, target, skill, skill.getActivateRate());
	}

	public static boolean calcSkillSuccess(final L2Character player, final L2Character target, final L2Skill skill, final int activateRate)
	{
		final Env env = new Env();
		env.character = player;
		env.target = target;
		env.skill = skill;
		env.value = activateRate;
		return calcSkillSuccess(env, skill.getResistType(), skill.getAttackType(), player.getChargedSpiritShot());
	}

	private static double getLevelModifier(final L2Skill skill, final L2Character attacker, final L2Character target)
	{
		// fix for Traps
		int attackerlevel = attacker.getLevel();
		if(attacker instanceof L2TrapInstance)
			if(((L2TrapInstance) attacker).getOwner() != null)
				attackerlevel = ((L2TrapInstance) attacker).getOwner().getLevel();

		int learnLevel = skill.getMagicLevel() > 0 ? skill.getMagicLevel() : attackerlevel;

		// force all summons not use magic levels
		if(attacker instanceof L2Summon)
			learnLevel = attacker.getLevel();

		double lvlmodifier = (double) learnLevel / target.getLevel();
		if(skill.getLevelModifier() > 1)
		{
			if(lvlmodifier > 1.0)
				// Ex: 1.03 - 1 = 0.3 * 2 + 1 = 1.6
				lvlmodifier = (lvlmodifier - 1) * skill.getLevelModifier() + 1;
			else if(lvlmodifier < 1.0)
				// Ex: 1 - 0.90 = 0.1, 0.1 * 2 = 0.2, 1 - 0.2 = 0.8
				lvlmodifier = 1 - (1 - lvlmodifier) * skill.getLevelModifier();
			lvlmodifier = MathLib.max(lvlmodifier, 0.001);
		}
		return lvlmodifier;
	}

	private static double getMAtkBonusPhoenix(final L2Skill skill, final L2Character character, final L2Character target, final int spiritshot)
	{
		final int mdef = Math.max(1, target.getMDef(target, skill)); // Вычисляем mDef цели
		double matk = character.getMAtk(target, skill);
		if(skill.isSSPossible() && spiritshot > 0) // Считаем бонус от шотов
			matk *= spiritshot * 2;
		return 11 * Math.pow(matk, 0.5) / mdef;
	}

	private static double getMAtkBonusDuoTM(final L2Skill skill, final L2Character character, final L2Character target, final int spiritshot)
	{
		// Вычисляем mDef цели
		final int mdef = Math.max(1, target.getMDef(target, skill));
		double mAtk = character.getMAtk(target, skill);

		// Считаем бонус от шотов
		if(skill.isSSPossible() && spiritshot > 0)
			switch (spiritshot)
			{
				case 1:
					mAtk *= 1.41;
					break;
				case 2:
					mAtk *= 2;
					break;
			}

		// формула зависимости от мАтк, мДеф и бонуса спирит шотов трепует проверки и возможно корректировки
		final double mAtkMod = Math.pow(mAtk, .35) * Math.pow(Math.log1p(mAtk), 2.) / mdef;
		return Config.SKILLS_CHANCE_MOD * mAtkMod;
	}

	public static boolean calculateUnlockChance(final L2Skill skill)
	{
		final int level = skill.getLevel();
		int chance = 0;
		switch (level)
		{
			case 1:
				chance = 30;
				break;

			case 2:
				chance = 50;
				break;

			case 3:
				chance = 75;
				break;

			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
				chance = 100;
				break;
		}

		return Rnd.get(100) <= chance;
	}

	public static void calcSkillMastery(final L2Skill skill, final L2Character activeChar)
	{
		if(activeChar == null || skill == null || !activeChar.isPlayable() || skill.isHandler() || skill.getSkillType() == SkillType.FISHING)
			return;

		// Skill id 330 for fighters, 331 for mages
		// Actually only GM can have 2 skill masteries, so let's make them more lucky ^^
		double val = activeChar.calcStat(Stats.SKILL_MASTERY, 0, null, null);
		if(activeChar.getSkillLevel(331) > 0 || activeChar.getSkillLevel(330) > 0)
		{
			if(activeChar.isMageClass())
				val *= INTbonus[activeChar.getSTR()];
			else
				val *= STRbonus[activeChar.getSTR()];

			if(Rnd.chance(val))
			{
				// byte mastery level, 0 = no skill mastery, 1 = no reuseTime, 2 = buff duration*2, 3 = power*3
				byte masteryLevel;
				final SkillType type = skill.getSkillType();
				if(skill.isDanceSong() || type == SkillType.BUFF || type == SkillType.HOT || type == SkillType.HEAL_PERCENT) // Hope i didn't forget skills to multiply their time
					masteryLevel = 2; // 2 = buff duration * 2
				else if(type == SkillType.HEAL || skill.getId() == L2Skill.SKILL_STEAL_DIVINITY)
					masteryLevel = 3; // 3 = power * 3
				else
					masteryLevel = 1; // 1 = no reuseTime

				if(masteryLevel > 0)
					activeChar.setSkillMastery(skill.getId(), masteryLevel);
			}
		}
	}

	public static double calcDamageResists(final L2Skill skill, L2Character attacker, L2Character defender, double value)
	{
		if(attacker == defender) // это дамаг от местности вроде ожога в лаве, наносится от своего имени
			return value; // TODO: по хорошему надо учитывать защиту, но поскольку эти скиллы немагические то надо делать отдельный механизм

		if(attacker.isBoss())
			value *= Config.RATE_EPIC_ATTACK;
		else if(attacker.isRaid() || attacker instanceof L2ReflectionBossInstance)
			value *= Config.RATE_RAID_ATTACK;

		if(defender.isBoss())
			value /= Config.RATE_EPIC_DEFENSE;
		else if(defender.isRaid() || defender instanceof L2ReflectionBossInstance)
			value /= Config.RATE_RAID_DEFENSE;

		// модификатор для расчёта атрибутов для суммонов
		double attackMod = 1.0;

		// Передача атрибутов от мастера к самону работает тока у саммонеров (фантом суммонер, элементал суммонер, варлок и классов, производных от этих трех)
		if(Config.SUMMON_ATTRIBUTE_TRANSFER)
		{
			// Случай 1: атакует суммонер с призванным суммоном
			if(attacker.isPlayer() && attacker.getPlayer().getClassId().isAllowAttributeTransfer() && attacker.getPet() != null && attacker.getPet().isSummon())
				attackMod = 0.2; // 20% остаток у мастера
			// Случай 2: атакует суммон
			else if(attacker.isSummon() && attacker.getPlayer() != null && attacker.getPlayer().getClassId().isAllowAttributeTransfer())
			{
				attackMod = 0.8; // атак атрибуты 80% суммону, 20 остаток у мастера
				attacker = attacker.getPlayer(); // меняем, чтоб тупо всё считалось с хозяина
			}

			// Случай 3: атакуют суммона
			if(defender.isSummon() && defender.getPlayer() != null && defender.getPlayer().getClassId().isAllowAttributeTransfer())
				defender = defender.getPlayer(); // защитн. атрибуты 100% суммону, 100% мастеру
		}

		final L2Player pAttacker = attacker.getPlayer();

		// если уровень игрока ниже чем на 2 и более уровней моба 78+, то его урон по мобу снижается
		final int diff = defender.getLevel() - (pAttacker != null ? pAttacker.getLevel() : attacker.getLevel());
		if(attacker.isPlayable() && defender.isMonster() && defender.getLevel() >= 78 && diff > 2)
			value *= 0.7 / Math.pow(diff - 2, 0.25);

		if(skill != null)
		{
			if(skill.getElement() == Element.NONE)
				return value;

			final int elementId = skill.getElement().getId();
			final double attack = attacker.calcStat(skill.getElement().getAttack(), skill.getElementPower() + attacker.getAttackElementValue(elementId), null, null);
			final double defense = defender.calcStat(skill.getElement().getDefence(), defender.getDefenseElementValue(elementId), null, null);

			return applyDefense(attacker, defender, defense, attack * attackMod, value, skill);
		}

		final TreeMap<Double, Element> sort_attibutes = new TreeMap<Double, Element>();
		for(final Element e : Element.values())
			if(e != Element.NONE)
				sort_attibutes.put(attacker.calcStat(e.getAttack(), attacker.getAttackElementValue(e.getId()), null, null), e);

		final Entry<Double, Element> lastEntry = sort_attibutes.lastEntry();
		final int attack = lastEntry.getKey().intValue();
		if(attack <= 0)
			return value;

		final double defense = defender.calcStat(lastEntry.getValue().getDefence(), defender.getDefenseElementValue(lastEntry.getValue().getId()), null, null);
		return applyDefense(attacker, defender, defense, attack * attackMod, value, null);
	}

	// TODO уточнить если ли разница в PvE
	public static double applyDefense(final L2Character attacker, final L2Character defender, final double defense, final double attack, final double value, final L2Skill skill)
	{
		// для физ атак и не магических скилов
		if(skill == null || !skill.isMagic())
		{
			if(Config.ATTIBUTE_SHOW_CALC)
				if(attacker.isPlayer() && ((L2Player) attacker).isGM())
				{
					final double mod = getElementMod(defense, attack, skill != null);
					attacker.sendMessage("--- element calc (phys) ---");
					attacker.sendMessage("[F] mod: " + mod);
					attacker.sendMessage("[F] defense: " + (int) defense);
					attacker.sendMessage("[F] attack: " + (int) attack);
					attacker.sendMessage("[F] skill: " + (skill == null ? "null" : skill));
					attacker.sendMessage("[F] old value: " + (int) value);
					attacker.sendMessage("[F] new value: " + (int) (value * mod));
					attacker.sendMessage("--------------------");
				}
				else if(attacker.isSummon() && attacker.getPlayer() != null && attacker.getPlayer().isGM())
				{
					final double mod = getElementMod(defense, attack, skill != null);
					attacker.getPlayer().sendMessage("--- element calc (phys) ---");
					attacker.getPlayer().sendMessage("[F] mod: " + mod);
					attacker.getPlayer().sendMessage("[F] defense: " + (int) defense);
					attacker.getPlayer().sendMessage("[F] attack: " + (int) attack);
					attacker.getPlayer().sendMessage("[F] skill: " + (skill == null ? "null" : skill));
					attacker.getPlayer().sendMessage("[F] old value: " + (int) value);
					attacker.getPlayer().sendMessage("[F] new value: " + (int) (value * mod));
					attacker.getPlayer().sendMessage("--------------------");
				}

			return value * getElementMod(defense, attack, skill != null);
		}

		if(Config.ATTIBUTE_SHOW_CALC)
			if(skill != null && skill.isMagic() && attacker.isPlayer() && ((L2Player) attacker).isGM())
			{
				final double mod = getElementMod(defense, attack, skill != null);

				attacker.sendMessage("--- element calc (magic) ---");
				attacker.sendMessage("[F] mod: " + mod);
				attacker.sendMessage("[F] defense: " + (int) defense);
				attacker.sendMessage("[F] attack: " + (int) attack);
				attacker.sendMessage("[F] skill: " + skill);
				attacker.sendMessage("[F] skill element: " + skill.getElement());
				attacker.sendMessage("[F] skill power: " + skill.getElementPower());
				attacker.sendMessage("[F] old value: " + (int) value);
				attacker.sendMessage("[F] new value: " + (int) (value * mod));
				attacker.sendMessage("--------------------");
			}

		return value * getElementMod(defense, attack, skill != null);
	}

	/**
	 * Возвращает множитель для атаки из значений атакующего и защитного элемента. Только для простых атак и немагических скиллов. <br />
	 * Для простых атак диапазон от 1.0 до 1.7 <br />
	 * Для скиллов от 1.0 до 2.0 <br />
	 *
	 * @param defense
	 *            значение защиты
	 * @param attack
	 *            значение атаки
	 * @param skill
	 *            флаг использования скилла
	 * @return множитель
	 */
	private static double getElementMod(final double defense, final double attack, final boolean skill)
	{
		double diff = attack - defense;
		if(diff <= 0)
			return 1.0;
		else if(!skill) // для обычных физ атак
		{
			double result = 1.0;
			diff = Math.max(diff, -20);
			if(diff > 250)
			{
				if(diff >= 300)
					result += 1.0;
				else
					result += 0.85;
			}
			else
			{
				diff = Math.min(diff, 150);
				result += diff * 0.0047;
			}
			return result;
		}
		else if(diff < 75)
			return 1.0 + diff * 0.0052;
		else if(diff < 150)
			return 1.4;
		else if(diff < 290)
			return 1.7;
		else if(diff < 300)
			return 1.8;
		else
			return 2.0;
	}

	/**
	 * Используется только для отображения в окне информации <br>
	 * Возвращает тип атакующего элемента и его силу.
	 *
	 * @return массив, в котором: <li>[0]: тип элемента, <li>[1]: его сила
	 */
	public static int[] calcAttackElement(final L2Character attacker)
	{
		// [0]: тип элемента, [1]: его сила
		final TreeMap<Double, Integer> sort_attibutes = new TreeMap<Double, Integer>();
		for(final Element e : Element.values())
			if(e != Element.NONE)
				sort_attibutes.put(attacker.calcStat(e.getAttack(), 0, null, null), e.getId());

		final Entry<Double, Integer> element = sort_attibutes.lastEntry();
		if(element.getKey() <= 0)
			return null;
		return new int[] { element.getValue(), element.getKey().intValue() };
	}
}
