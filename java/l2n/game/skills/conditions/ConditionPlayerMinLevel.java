package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionPlayerMinLevel extends Condition
{
	private final int _level;

	public ConditionPlayerMinLevel(final int level)
	{
		_level = level;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.getLevel() >= _level;
	}
}
