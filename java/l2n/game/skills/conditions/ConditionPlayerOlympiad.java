package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 18.11.2011
 * @time 14:29:15
 */
public class ConditionPlayerOlympiad extends Condition
{
	private final boolean _value;

	public ConditionPlayerOlympiad(boolean v)
	{
		_value = v;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		return env.character.isInOlympiadMode() == _value;
	}
}
