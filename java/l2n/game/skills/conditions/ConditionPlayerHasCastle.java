package l2n.game.skills.conditions;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 *         19.10.2009 3:26:05
 */
public class ConditionPlayerHasCastle extends Condition
{
	private final int _castle;

	public ConditionPlayerHasCastle(final int castle)
	{
		_castle = castle;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;

		final L2Clan clan = ((L2Player) env.character).getClan();
		if(clan == null)
			return _castle == 0;

		// Any castle
		if(_castle == -1)
			return clan.getHasCastle() > 0;

		return clan.getHasCastle() == _castle;
	}
}
