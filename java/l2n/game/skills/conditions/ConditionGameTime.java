package l2n.game.skills.conditions;

import l2n.game.GameTimeController;
import l2n.game.skills.Env;

public class ConditionGameTime extends Condition
{
	public enum CheckGameTime
	{
		NIGHT
	}

	private final CheckGameTime _check;

	private final boolean _required;

	public ConditionGameTime(final CheckGameTime check, final boolean required)
	{
		_check = check;
		_required = required;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		switch (_check)
		{
			case NIGHT:
				return GameTimeController.getInstance().isNowNight() == _required;
		}
		return !_required;
	}
}
