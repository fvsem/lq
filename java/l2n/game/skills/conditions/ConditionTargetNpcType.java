package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @author L2System Project
 */
public class ConditionTargetNpcType extends Condition
{
	private final String[] _npcType;

	public ConditionTargetNpcType(final String[] type)
	{
		_npcType = type;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.target == null)
			return false;

		boolean mt;
		for(int i = 0; i < _npcType.length; i++)
		{
			mt = env.target.getClass().getSimpleName().replaceFirst("L2", "").replaceFirst("Instance", "").equalsIgnoreCase(_npcType[i]);
			if(mt)
				return true;
		}

		return false;
	}
}
