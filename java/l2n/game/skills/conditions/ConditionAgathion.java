package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 01.06.2010
 * @time 11:48:51
 */
public class ConditionAgathion extends Condition
{
	private final int _agathionId;

	public ConditionAgathion(final int agathionId)
	{
		_agathionId = agathionId;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer() || ((L2Player) env.character).getAgathion() == null)
			return false;
		return ((L2Player) env.character).getAgathion().getId() == _agathionId;
	}
}
