package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionPlayerRiding extends Condition
{
	public enum CheckPlayerRiding
	{
		NONE,
		STRIDER,
		WYVERN
	}

	private final CheckPlayerRiding _riding;

	public ConditionPlayerRiding(final CheckPlayerRiding riding)
	{
		_riding = riding;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(_riding == CheckPlayerRiding.STRIDER && env.character.isRiding())
			return true;
		if(_riding == CheckPlayerRiding.WYVERN && env.character.isFlying())
			return true;
		if(_riding == CheckPlayerRiding.NONE && !env.character.isRiding() && !env.character.isFlying())
			return true;
		return false;
	}
}
