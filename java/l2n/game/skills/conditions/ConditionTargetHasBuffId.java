package l2n.game.skills.conditions;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Env;

public final class ConditionTargetHasBuffId extends Condition
{
	private final int _id;
	private final int _level;

	public ConditionTargetHasBuffId(final int id, final int lvl)
	{
		_id = id;
		_level = lvl;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		final L2Character target = env.target;
		if(target == null)
			return false;

		if(_level == -1)
			return target.getEffectList().getFirstEffect(_id) != null;

		final L2Effect effect = target.getEffectList().getFirstEffect(_id);
		if(effect != null)
			return effect.getSkill().getLevel() >= _level;

		return false;
	}
}
