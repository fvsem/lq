package l2n.game.skills.conditions;

import l2n.game.model.Reflection;
import l2n.game.skills.Env;
import l2n.util.ArrayUtil;

/**
 * @<a href="L2System Project">L2s</a>
 * @date 03.09.2011
 * @time 7:18:09
 */
public class ConditionInstancedZoneId extends Condition
{
	private final int[] _ids;

	public ConditionInstancedZoneId(final int[] ids)
	{
		_ids = ids;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		Reflection ref = env.character.getReflection();
		return ArrayUtil.arrayContains(_ids, ref.getInstancedZoneId());
	}

}
