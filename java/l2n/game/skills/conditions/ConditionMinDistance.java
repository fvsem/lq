package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Character;
import l2n.game.skills.Env;

public class ConditionMinDistance extends Condition
{
	private final int _validDistance;

	public ConditionMinDistance(final int distance)
	{
		_validDistance = distance;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.target == null)
			return false;
		final L2Character target = env.target;
		final L2Character character = env.character;
		return character.getRealDistance(target) >= _validDistance;
	}
}
