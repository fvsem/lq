package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public final class ConditionItemId extends Condition
{
	private final short _itemId;

	public ConditionItemId(final short itemId)
	{
		_itemId = itemId;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.item == null)
			return false;
		return env.item.getItemId() == _itemId;
	}
}
