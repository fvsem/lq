package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionPlayerMinHp extends Condition
{
	private final float _hp;

	public ConditionPlayerMinHp(final int hp)
	{
		_hp = hp;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.getCurrentHp() > _hp;
	}
}
