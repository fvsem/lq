package l2n.game.skills.conditions;

import l2n.game.skills.Env;
import l2n.util.Rnd;

public class ConditionGameChance extends Condition
{
	private final int _chance;

	ConditionGameChance(final int chance)
	{
		_chance = chance;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return Rnd.chance(_chance);
	}
}
