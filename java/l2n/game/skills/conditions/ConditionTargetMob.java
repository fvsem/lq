package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionTargetMob extends Condition
{
	private final boolean _isMob;

	public ConditionTargetMob(final boolean isMob)
	{
		_isMob = isMob;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.target.isMonster() == _isMob;
	}
}
