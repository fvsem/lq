package l2n.game.skills.conditions;

import l2n.game.skills.Env;
import l2n.util.ArrayUtil;

public class ConditionLogicAnd extends Condition
{
	public Condition[] _conditions = ArrayUtil.EMPTY_CONDITION_ARRAY;

	public ConditionLogicAnd()
	{
		super();
	}

	public void add(final Condition condition)
	{
		if(condition == null)
			return;

		final int len = _conditions.length;
		final Condition[] tmp = new Condition[len + 1];
		System.arraycopy(_conditions, 0, tmp, 0, len);
		tmp[len] = condition;
		_conditions = tmp;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		for(final Condition c : _conditions)
			if(!c.test(env))
				return false;
		return true;
	}
}
