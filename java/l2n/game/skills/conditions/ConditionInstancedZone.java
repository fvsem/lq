package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 01.06.2010
 * @time 10:19:56
 */
public class ConditionInstancedZone extends Condition
{
	private final String _name;

	public ConditionInstancedZone(final String name)
	{
		_name = name;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;
		return env.character.getReflection().getName().equals(_name);
	}
}
