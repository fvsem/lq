package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionTargetLevel extends Condition
{
	private final int _level;

	public ConditionTargetLevel(final int level)
	{
		_level = level;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.target == null)
			return false;
		return env.target.getLevel() >= _level;
	}
}
