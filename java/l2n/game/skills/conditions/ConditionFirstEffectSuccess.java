package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 09.02.2011
 * @time 7:30:37
 */
public class ConditionFirstEffectSuccess extends Condition
{
	boolean _param;

	public ConditionFirstEffectSuccess(final boolean param)
	{
		_param = param;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		return _param == (env.value == Integer.MAX_VALUE);
	}
}
