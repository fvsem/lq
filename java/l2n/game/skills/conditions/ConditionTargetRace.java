package l2n.game.skills.conditions;

import l2n.commons.list.GArray;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Env;

public class ConditionTargetRace extends Condition
{
	private final GArray<Integer> _raceIds;

	public ConditionTargetRace(final GArray<Integer> raceId)
	{
		_raceIds = raceId;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!(env.target instanceof L2NpcInstance) || _raceIds == null || _raceIds.isEmpty())
			return false;
		return _raceIds.contains(((L2NpcInstance) env.target).getTemplate().getRace().ordinal());
	}
}
