package l2n.game.skills.conditions;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Env;

public final class ConditionPlayerHasBuffId extends Condition
{
	private final int _id;
	private final int _level;

	public ConditionPlayerHasBuffId(final int id, final int level)
	{
		_id = id;
		_level = level;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		final L2Character character = env.character;
		if(character == null)
			return false;

		if(_level == -1)
			return character.getEffectList().getFirstEffect(_id) != null;

		final L2Effect effect = character.getEffectList().getFirstEffect(_id);
		if(effect != null)
			return effect.getSkill().getLevel() >= _level;

		return false;
	}
}
