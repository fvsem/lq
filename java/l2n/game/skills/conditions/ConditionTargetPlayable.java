package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Playable;
import l2n.game.skills.Env;

public class ConditionTargetPlayable extends Condition
{
	private final boolean _flag;

	public ConditionTargetPlayable(final boolean flag)
	{
		_flag = flag;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.target instanceof L2Playable == _flag;
	}
}
