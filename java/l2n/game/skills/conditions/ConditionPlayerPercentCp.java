package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.03.2010
 * @time 1:25:29
 */
public class ConditionPlayerPercentCp extends Condition
{
	private final float _cp;

	public ConditionPlayerPercentCp(final int cp)
	{
		_cp = cp / 100f;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.getCurrentCpRatio() <= _cp;
	}
}
