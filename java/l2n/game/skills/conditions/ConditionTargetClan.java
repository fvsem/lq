package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 28.04.2013
 * @time 13:11:02
 */
public class ConditionTargetClan extends Condition
{
	private final boolean _test;

	public ConditionTargetClan(final boolean param)
	{
		_test = param;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		final L2Player character = env.character.getPlayer();
		final L2Player target = env.target.getPlayer();
		return character != null && target != null && (character.getClanId() != 0 && character.getClanId() == target.getClanId() == _test || character.getParty() != null && target.getParty() != null && character.getParty() == target.getParty());
	}
}
