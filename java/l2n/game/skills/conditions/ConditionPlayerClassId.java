package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 31.12.2010
 * @time 19:59:45
 */
public class ConditionPlayerClassId extends Condition
{
	private final int _class;

	public ConditionPlayerClassId(final int id)
	{
		_class = id;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;
		return ((L2Player) env.character).getActiveClassId() == _class;
	}
}
