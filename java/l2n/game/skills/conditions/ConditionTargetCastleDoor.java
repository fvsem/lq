package l2n.game.skills.conditions;

import l2n.game.model.instances.L2DoorInstance;
import l2n.game.skills.Env;

public class ConditionTargetCastleDoor extends Condition
{
	private final boolean _isCastleDoor;

	public ConditionTargetCastleDoor(final boolean isCastleDoor)
	{
		_isCastleDoor = isCastleDoor;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.target instanceof L2DoorInstance == _isCastleDoor;
	}
}
