package l2n.game.skills.conditions;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.EffectType;
import l2n.game.skills.Env;

public final class ConditionTargetHasBuff extends Condition
{
	private final EffectType _effectType;
	private final int _level;

	public ConditionTargetHasBuff(final EffectType effectType, final int level)
	{
		_effectType = effectType;
		_level = level;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		final L2Character target = env.target;
		if(target == null)
			return false;

		final L2Effect effect = target.getEffectList().getEffectByType(_effectType);
		if(effect == null)
			return false;

		return _level == -1 || effect.getSkill().getLevel() >= _level;
	}
}
