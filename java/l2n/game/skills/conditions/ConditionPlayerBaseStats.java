package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

public class ConditionPlayerBaseStats extends Condition
{
	private final BaseStat _stat;

	private final byte _value;

	public ConditionPlayerBaseStats(final L2Character player, final BaseStat stat, final byte value)
	{
		super();
		_stat = stat;
		_value = value;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;
		final L2Player player = (L2Player) env.character;
		switch (_stat)
		{
			case Int:
				return player.getINT() >= _value;
			case Str:
				return player.getSTR() >= _value;
			case Con:
				return player.getCON() >= _value;
			case Dex:
				return player.getDEX() >= _value;
			case Men:
				return player.getMEN() >= _value;
			case Wit:
				return player.getWIT() >= _value;
		}
		return false;
	}
}
