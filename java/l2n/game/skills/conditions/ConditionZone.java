package l2n.game.skills.conditions;

import l2n.game.model.L2Zone.ZoneType;
import l2n.game.skills.Env;

public class ConditionZone extends Condition
{
	private final ZoneType _zoneType;

	public ConditionZone(final String zoneType)
	{
		_zoneType = ZoneType.valueOf(zoneType);
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;
		return env.character.isInZone(_zoneType);
	}
}
