package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.items.Inventory;
import l2n.game.skills.Env;

public final class ConditionUsingItemType extends Condition
{
	private final long _mask;

	public ConditionUsingItemType(final long mask)
	{
		_mask = mask;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.character instanceof L2Summon)
			return (_mask & ((L2Summon) env.character).getWearedMask()) != 0;

		if(!env.character.isPlayer())
			return false;

		final Inventory inv = ((L2Player) env.character).getInventory();
		return (_mask & inv.getWearedMask()) != 0;
	}
}
