package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.skills.Env;

public final class ConditionSlotItemType extends ConditionInventory
{
	private final int _mask;

	public ConditionSlotItemType(final short slot, final int mask)
	{
		super(slot);
		_mask = mask;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;
		final Inventory inv = ((L2Player) env.character).getInventory();
		final L2ItemInstance item = inv.getPaperdollItem(_slot);
		if(item == null)
			return false;
		return (item.getItem().getItemMask() & _mask) != 0;
	}
}
