package l2n.game.skills.conditions;

import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.EffectType;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 * @date 03.08.2010
 * @time 12:05:42
 */
public class ConditionPlayerHasBuff extends Condition
{
	private final EffectType _effectType;
	private final int _level;

	public ConditionPlayerHasBuff(final EffectType effectType, final int level)
	{
		_effectType = effectType;
		_level = level;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		final L2Character character = env.character;
		if(character == null)
			return false;

		final L2Effect effect = character.getEffectList().getEffectByType(_effectType);
		if(effect == null)
			return false;

		return _level == -1 || effect.getSkill().getLevel() >= _level;
	}
}
