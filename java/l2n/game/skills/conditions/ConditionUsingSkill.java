package l2n.game.skills.conditions;

import l2n.game.model.L2Skill.SkillType;
import l2n.game.skills.Env;

public final class ConditionUsingSkill extends Condition
{
	private final int _skillId;
	private final SkillType _skillType;

	public ConditionUsingSkill(final int skillId)
	{
		_skillId = skillId;
		_skillType = null;
	}

	public ConditionUsingSkill(final String skilltype)
	{
		_skillId = -1;
		_skillType = SkillType.valueOf(skilltype);
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.skill == null)
			return false;
		return env.skill.getId() == _skillId || env.skill.getSkillType() == _skillType;
	}
}
