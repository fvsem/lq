package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionPlayerPercentMp extends Condition
{
	private final float _mp;

	public ConditionPlayerPercentMp(final int mp)
	{
		_mp = mp / 100f;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.getCurrentMpRatio() <= _mp;
	}
}
