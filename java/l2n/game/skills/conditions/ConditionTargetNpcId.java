package l2n.game.skills.conditions;

import l2n.game.skills.Env;

/**
 * @author L2System Project
 */
public class ConditionTargetNpcId extends Condition
{
	private final String[] _npcIds;

	public ConditionTargetNpcId(final String[] ids)
	{
		_npcIds = ids;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.target == null)
			return false;

		for(int i = 0; i < _npcIds.length; i++)
			if(env.target.getNpcId() == Integer.valueOf(_npcIds[i]))
				return true;
		return false;
	}
}
