package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public final class ConditionHasSkill extends Condition
{
	private final Integer _id;
	private final short _level;

	public ConditionHasSkill(final Integer id, final short level)
	{
		_id = id;
		_level = level;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.skill == null)
			return false;
		return env.character.getSkillLevel(_id) >= _level;
	}
}
