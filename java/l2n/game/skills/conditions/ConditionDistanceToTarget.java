package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Character;
import l2n.game.skills.Env;

public class ConditionDistanceToTarget extends Condition
{
	private final int _validDistance;

	public ConditionDistanceToTarget(final int distance)
	{
		_validDistance = distance;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		final L2Character target = env.target;
		final L2Character character = env.character;
		final double range = character.getRealDistance(target);
		return range > _validDistance - 50 && range < _validDistance + 50;
	}
}
