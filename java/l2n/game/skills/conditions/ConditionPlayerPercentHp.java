package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionPlayerPercentHp extends Condition
{
	private final double _hp;

	public ConditionPlayerPercentHp(final int hp)
	{
		_hp = hp;
	}

	@Override
	protected boolean testImpl(final Env env)
	{
		return env.character.getCurrentHpPercents() <= _hp;
	}
}
