package l2n.game.skills.conditions;

public enum BaseStat
{
	Int,
	Str,
	Con,
	Dex,
	Men,
	Wit;
}
