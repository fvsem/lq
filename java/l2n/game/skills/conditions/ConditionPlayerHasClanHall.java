package l2n.game.skills.conditions;

import l2n.commons.list.GArray;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

import java.util.Arrays;

/**
 * @author L2System Project
 *         19.10.2009 3:26:12
 */
public class ConditionPlayerHasClanHall extends Condition
{
	private final Integer[] _clanHall;

	public ConditionPlayerHasClanHall(final GArray<Integer> clanHall)
	{
		_clanHall = clanHall.toArray(new Integer[clanHall.size()]);
		Arrays.sort(_clanHall);
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!env.character.isPlayer())
			return false;

		final L2Clan clan = ((L2Player) env.character).getClan();
		if(clan == null)
			return _clanHall.length == 1 && _clanHall[0] == 0;

		// All Clan Hall
		if(_clanHall.length == 1 && _clanHall[0] == -1)
			return clan.getHasHideout() > 0;

		return Arrays.binarySearch(_clanHall, clan.getHasHideout()) >= 0;
	}
}
