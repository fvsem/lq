package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionLogicNot extends Condition
{
	private final Condition _condition;

	public ConditionLogicNot(final Condition condition)
	{
		_condition = condition;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return !_condition.test(env);
	}
}
