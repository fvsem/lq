package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

public class ConditionPlayerMaxPK extends Condition
{
	private final int _pk;

	public ConditionPlayerMaxPK(final int pk)
	{
		_pk = pk;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(env.character.isPlayer())
			return ((L2Player) env.character).getPkKills() <= _pk;
		return false;
	}
}
