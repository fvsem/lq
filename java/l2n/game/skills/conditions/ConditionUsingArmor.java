package l2n.game.skills.conditions;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;
import l2n.game.templates.L2Armor.ArmorType;

public class ConditionUsingArmor extends Condition
{
	private final ArmorType _armor;

	public ConditionUsingArmor(final ArmorType armor)
	{
		_armor = armor;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		return env.character.isPlayer() && ((L2Player) env.character).isWearingArmor(_armor);
	}
}
