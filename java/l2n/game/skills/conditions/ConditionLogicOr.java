package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public class ConditionLogicOr extends Condition
{
	private final static Condition[] emptyConditions = new Condition[0];

	public Condition[] _conditions = emptyConditions;

	public void add(final Condition condition)
	{
		if(condition == null)
			return;

		final int len = _conditions.length;
		final Condition[] tmp = new Condition[len + 1];
		System.arraycopy(_conditions, 0, tmp, 0, len);
		tmp[len] = condition;
		_conditions = tmp;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		for(final Condition c : _conditions)
			if(c.test(env))
				return true;
		return false;
	}
}
