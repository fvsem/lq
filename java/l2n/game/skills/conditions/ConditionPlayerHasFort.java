package l2n.game.skills.conditions;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.skills.Env;

/**
 * @author L2System Project
 *         19.10.2009 3:25:51
 */
public class ConditionPlayerHasFort extends Condition
{
	private final int _fort;

	public ConditionPlayerHasFort(final int fort)
	{
		_fort = fort;
	}

	@Override
	public boolean testImpl(final Env env)
	{
		if(!(env.character instanceof L2Player))
			return false;

		final L2Clan clan = ((L2Player) env.character).getClan();
		if(clan == null)
			return _fort == 0;

		// Any fortress
		if(_fort == -1)
			return clan.getHasFortress() > 0;

		return clan.getHasFortress() == _fort;
	}
}
