package l2n.game.skills.conditions;

import l2n.game.skills.Env;

public abstract class Condition
{
	private String _msg;
	private int _msgId;
	private boolean _addName = false;

	public final void setMessage(final String msg)
	{
		_msg = msg;
	}

	public final String getMessage()
	{
		return _msg;
	}

	public final void setMessageId(final int msgId)
	{
		_msgId = msgId;
	}

	public final int getMessageId()
	{
		return _msgId != 0 ? _msgId : 0;
	}

	public final void addName()
	{
		_addName = true;
	}

	public final boolean isAddName()
	{
		return _addName;
	}

	public final boolean test(final Env env)
	{
		return testImpl(env);
	}

	protected abstract boolean testImpl(Env env);
}
