package l2n.game.skills;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TypeFormat;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.skills.conditions.*;
import l2n.game.skills.conditions.ConditionGameTime.CheckGameTime;
import l2n.game.skills.conditions.ConditionPlayerRiding.CheckPlayerRiding;
import l2n.game.skills.conditions.ConditionPlayerState.PlayerState;
import l2n.game.skills.conditions.ConditionTargetDirection.TargetDirection;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.funcs.*;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Armor.ArmorType;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.ArrayUtil;
import l2n.util.Util;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class DocumentBase
{
	protected final static Logger _log = Logger.getLogger(DocumentBase.class.getName());

	private File file;
	protected TIntObjectHashMap<Number[]> tables;

	protected DocumentBase(File file)
	{
		this.file = file;
		tables = new TIntObjectHashMap<Number[]>();
	}

	public Document parse()
	{
		Document doc;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			doc = factory.newDocumentBuilder().parse(file);
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Error loading file " + file, e);
			return null;
		}
		try
		{
			parseDocument(doc);
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Error in file " + file, e);
			return null;
		}
		return doc;
	}

	protected abstract void parseDocument(Document doc);

	protected abstract Number getTableValue(String name);

	protected abstract Number getTableValue(String name, int idx);

	protected void resetTable()
	{
		tables = new TIntObjectHashMap<Number[]>();
	}

	protected void setTable(String name, Number[] table)
	{
		tables.put(name.hashCode(), table);
	}

	protected void parseTemplate(Node n, Object template)
	{
		Condition condition = null;
		n = n.getFirstChild();
		if(n == null)
			return;
		if("cond".equalsIgnoreCase(n.getNodeName()))
		{
			condition = parseCondition(n.getFirstChild());
			Node msg = n.getAttributes().getNamedItem("msg");
			if(condition != null && msg != null)
				condition.setMessage(msg.getNodeValue());

			Node msgId = n.getAttributes().getNamedItem("msgId");
			if(msgId != null)
				condition.setMessageId(Integer.decode(msgId.getNodeValue()));

			n = n.getNextSibling();
		}

		for(; n != null; n = n.getNextSibling())
		{
			String nodeName = n.getNodeName();
			if("add".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Add", condition);
			else if("sub".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Sub", condition);
			else if("mul".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Mul", condition);
			else if("basemul".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "BaseMul", condition);
			else if("div".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Div", condition);
			else if("set".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Set", condition);
			else if("enchant".equalsIgnoreCase(nodeName))
				attachFunc(n, template, "Enchant", condition);
			else if("effect".equalsIgnoreCase(nodeName))
			{
				if(template instanceof EffectTemplate)
					throw new RuntimeException("Nested effects");
				attachEffect(n, template);
			}
			else if(template instanceof EffectTemplate)
			{
				if("def".equalsIgnoreCase(nodeName))
					parseBeanSet(n, ((EffectTemplate) template).getParam(), new Integer(((L2Skill) ((EffectTemplate) template).getParam().getObject("object")).getLevel()));
				else
				{
					Condition cond = parseCondition(n);
					if(cond != null)
						((EffectTemplate) template).attachCond(cond);
				}
			}
			else if("skill".equalsIgnoreCase(nodeName))
			{
				if(!(template instanceof L2Item))
					throw new RuntimeException("Nested skills");
				attachSkill(n, (L2Item) template, condition);
			}
		}
	}

	protected void attachSkill(Node n, L2Item template, Condition condition)
	{
		NamedNodeMap attrs = n.getAttributes();
		int skillId = Short.valueOf(attrs.getNamedItem("id").getNodeValue());
		int skillLevel = Byte.valueOf(attrs.getNamedItem("level").getNodeValue());
		byte chance = getNumber(attrs.getNamedItem("chance").getNodeValue(), template).byteValue();
		String action = attrs.getNamedItem("action").getNodeValue();

		L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLevel);

		if("crit".equalsIgnoreCase(action))
			template.attachSkillOnAction(skill, chance, true);
		else if("magic".equalsIgnoreCase(action))
			template.attachSkillOnAction(skill, chance, false);
		else if("add".equalsIgnoreCase(action))
			template.attachSkill(skill);
		else
			throw new NoSuchElementException("Unsupported action type for weapon-attached skill");
	}

	protected void attachFunc(Node n, Object template, String name, Condition attachCond)
	{
		Stats stat = Stats.valueOfXml(n.getAttributes().getNamedItem("stat").getNodeValue());
		String order = n.getAttributes().getNamedItem("order").getNodeValue();
		Lambda lambda = getLambda(n, template);
		int ord = getNumber(order, template).intValue();
		Condition applyCond = parseCondition(n.getFirstChild());
		FuncTemplate ft = new FuncTemplate(attachCond, applyCond, name, stat, ord, lambda);
		if(template instanceof L2Item)
			((L2Item) template).attachFunction(ft);
		else if(template instanceof L2Skill)
			((L2Skill) template).attach(ft);
		else if(template instanceof EffectTemplate)
			((EffectTemplate) template).attach(ft);
	}

	protected void attachLambdaFunc(Node n, Object template, LambdaCalc calc)
	{
		String name = n.getNodeName();
		StringBuffer sb = new StringBuffer(name);
		sb.setCharAt(0, Character.toUpperCase(name.charAt(0)));
		name = sb.toString();
		Lambda lambda = getLambda(n, template);
		FuncTemplate ft = new FuncTemplate(null, null, name, null, calc._funcs.length, lambda);
		calc.addFunc(ft.getFunc(new Env(), calc));
	}

	protected void attachEffect(Node n, Object template)
	{
		NamedNodeMap attrs = n.getAttributes();
		StatsSet set = new StatsSet();

		int skillId = 0;
		int time = 0;
		if(template instanceof L2Skill)
		{
			skillId = ((L2Skill) template).getId();
			set.set("skillId", skillId);
		}

		set.set("name", attrs.getNamedItem("name").getNodeValue());
		set.set("object", template);

		/**
		 * Keep this values as default ones, DP needs it
		 */
		if(attrs.getNamedItem("count") != null)
			set.set("count", getNumber(attrs.getNamedItem("count").getNodeValue(), template).intValue());
		if(attrs.getNamedItem("time") != null)
		{
			time = getNumber(attrs.getNamedItem("time").getNodeValue(), template).intValue();
			if(skillId > 0 && Config.ENABLE_MODIFY_SKILL_DURATION && time > 0)
				if(Config.SKILL_DURATION_LIST.containsKey(skillId))
				{
					L2Skill skill = (L2Skill) template;
					if(skill.getLevel() < 100)
						time = Config.SKILL_DURATION_LIST.get(skillId);
					else if(skill.getLevel() >= 100 && skill.getLevel() < 140)
						time += Config.SKILL_DURATION_LIST.get(skillId);
					else if(skill.getLevel() > 140)
						time = Config.SKILL_DURATION_LIST.get(skillId);
				}

			set.set("time", time);
		}

		Lambda lambda = getLambda(n, template);
		set.set("value", lambda._value);

		set.set("abnormal", AbnormalEffect.NULL);
		set.set("special", AbnormalEffect.NULL);
		if(attrs.getNamedItem("abnormal") != null)
		{
			AbnormalEffect ae = AbnormalEffect.getByName(attrs.getNamedItem("abnormal").getNodeValue());
			if(ae.isSpecial())
				set.set("special", ae);
			else
				set.set("abnormal", ae);
		}

		if(attrs.getNamedItem("stackType") != null)
		{
			String stackType = attrs.getNamedItem("stackType").getNodeValue();
			StringTokenizer st = new StringTokenizer(stackType, ",");

			set.set("stackType", st.nextToken());
			if(st.hasMoreTokens())
				set.set("stackType2", st.nextToken());
		}

		if(attrs.getNamedItem("stackOrder") != null)
			set.set("stackOrder", getNumber(attrs.getNamedItem("stackOrder").getNodeValue(), template).intValue());
		if(attrs.getNamedItem("applyOnCaster") != null)
			set.set("applyOnCaster", TypeFormat.parseBoolean(attrs.getNamedItem("applyOnCaster").getNodeValue()));

		if(attrs.getNamedItem("hidden") != null)
			set.set("hidden", TypeFormat.parseBoolean(attrs.getNamedItem("hidden").getNodeValue()));
		// noicon(1) == hidden(true)
		// noicon(0) == hidden(false)

		if(attrs.getNamedItem("options") != null)
			set.set("options", attrs.getNamedItem("options").getNodeValue());
		if(attrs.getNamedItem("activateRate") != null)
			set.set("activateRate", getNumber(attrs.getNamedItem("activateRate").getNodeValue(), template).intValue());

		if(attrs.getNamedItem("displayId") != null)
			set.set("displayId", getNumber(attrs.getNamedItem("displayId").getNodeValue(), template).intValue());
		if(attrs.getNamedItem("displayLevel") != null)
			set.set("displayLevel", getNumber(attrs.getNamedItem("displayLevel").getNodeValue(), template).intValue());
		if(attrs.getNamedItem("cancelOnAction") != null)
			set.set("cancelOnAction", TypeFormat.parseBoolean(attrs.getNamedItem("cancelOnAction").getNodeValue()));

		EffectTemplate lt = new EffectTemplate(set);

		parseTemplate(n, lt);

		if(template instanceof L2Skill)
			((L2Skill) template).attach(lt);
	}

	protected Condition parseCondition(Node n)
	{
		while (n != null && n.getNodeType() != Node.ELEMENT_NODE)
			n = n.getNextSibling();
		if(n == null)
			return null;
		if("and".equalsIgnoreCase(n.getNodeName()))
			return parseLogicAnd(n);
		if("or".equalsIgnoreCase(n.getNodeName()))
			return parseLogicOr(n);
		if("not".equalsIgnoreCase(n.getNodeName()))
			return parseLogicNot(n);
		if("player".equalsIgnoreCase(n.getNodeName()))
			return parsePlayerCondition(n);
		if("target".equalsIgnoreCase(n.getNodeName()))
			return parseTargetCondition(n);
		if("has".equalsIgnoreCase(n.getNodeName()))
			return parseHasCondition(n);
		if("using".equalsIgnoreCase(n.getNodeName()))
			return parseUsingCondition(n);
		if("game".equalsIgnoreCase(n.getNodeName()))
			return parseGameCondition(n);
		if("zone".equalsIgnoreCase(n.getNodeName()))
			return parseZoneCondition(n);
		if("InstancedZone".equalsIgnoreCase(n.getNodeName()))
			return parseInstancedZoneCondition(n);
		if("InstancedZoneId".equalsIgnoreCase(n.getNodeName()))
			return parseInstancedZoneIdCondition(n);
		return null;
	}

	protected Condition parseLogicAnd(Node n)
	{
		ConditionLogicAnd cond = new ConditionLogicAnd();
		for(n = n.getFirstChild(); n != null; n = n.getNextSibling())
			if(n.getNodeType() == Node.ELEMENT_NODE)
				cond.add(parseCondition(n));
		if(cond._conditions == null || cond._conditions.length == 0)
			_log.severe("Empty <and> condition in " + file);
		return cond;
	}

	protected Condition parseLogicOr(Node n)
	{
		ConditionLogicOr cond = new ConditionLogicOr();
		for(n = n.getFirstChild(); n != null; n = n.getNextSibling())
			if(n.getNodeType() == Node.ELEMENT_NODE)
				cond.add(parseCondition(n));
		if(cond._conditions == null || cond._conditions.length == 0)
			_log.severe("Empty <or> condition in " + file);
		return cond;
	}

	protected Condition parseLogicNot(Node n)
	{
		for(n = n.getFirstChild(); n != null; n = n.getNextSibling())
			if(n.getNodeType() == Node.ELEMENT_NODE)
				return new ConditionLogicNot(parseCondition(n));
		_log.severe("Empty <not> condition in " + file);
		return null;
	}

	protected Condition parsePlayerCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			String nodeName = a.getNodeName();
			if("race".equalsIgnoreCase(nodeName))
				cond = joinAnd(cond, new ConditionPlayerRace(a.getNodeValue()));
			else if("minLevel".equalsIgnoreCase(nodeName))
			{
				int lvl = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerMinLevel(lvl));
			}
			else if("maxLevel".equalsIgnoreCase(nodeName))
			{
				int lvl = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerMaxLevel(lvl));
			}
			else if("maxPK".equalsIgnoreCase(nodeName))
			{
				int pk = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerMaxPK(pk));
			}
			else if("resting".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.RESTING, val));
			}
			else if("moving".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.MOVING, val));
			}
			else if("running".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.RUNNING, val));
			}
			else if("standing".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.STANDING, val));
			}
			else if("flying".equalsIgnoreCase(a.getNodeName()))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.FLYING, val));
			}
			else if("flyingTransform".equalsIgnoreCase(a.getNodeName()))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.FLYING_TRANSFORM, val));
			}
			else if("behind".equalsIgnoreCase(a.getNodeName()))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.BEHIND, val));
			}
			else if("front".equalsIgnoreCase(a.getNodeName()))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionPlayerState(PlayerState.FRONT, val));
			}
			else if("percentHP".equalsIgnoreCase(nodeName))
			{
				int percentHP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerPercentHp(percentHP));
			}
			else if("percentCP".equalsIgnoreCase(nodeName))
			{
				int percentCP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerPercentCp(percentCP));
			}
			else if("minpercentCP".equalsIgnoreCase(nodeName))
			{
				int minpercentCP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionMinPlayerPercentCp(minpercentCP));
			}
			else if("maxpercentCP".equalsIgnoreCase(nodeName))
			{
				int maxpercentCP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionMaxPlayerPercentCp(maxpercentCP));
			}
			else if("minHP".equalsIgnoreCase(nodeName))
			{
				int minHP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerMinHp(minHP));
			}
			else if("percentMP".equalsIgnoreCase(nodeName))
			{
				int percentMP = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionPlayerPercentMp(percentMP));
			}
			else if("classId".equalsIgnoreCase(nodeName))
				cond = joinAnd(cond, new ConditionPlayerClassId(getNumber(a.getNodeValue(), null).intValue()));
			else if("hasBuff".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ";");
				EffectType et = Enum.valueOf(EffectType.class, st.nextToken().trim());
				int level = -1;
				if(st.hasMoreTokens())
					level = TypeFormat.parseInt(st.nextToken().trim());
				cond = joinAnd(cond, new ConditionTargetHasBuff(et, level));
			}
			else if("hasBuffId".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ";");
				int id = TypeFormat.parseInt(st.nextToken().trim());
				int level = -1;
				if(st.hasMoreTokens())
					level = TypeFormat.parseInt(st.nextToken().trim());
				cond = joinAnd(cond, new ConditionPlayerHasBuffId(id, level));
			}
			else if("agathion".equalsIgnoreCase(nodeName))
			{
				int agathionId = getNumber(a.getNodeValue(), null).intValue();
				cond = joinAnd(cond, new ConditionAgathion(agathionId));
			}
			else if("riding".equalsIgnoreCase(nodeName))
			{
				String riding = a.getNodeValue();
				if("strider".equalsIgnoreCase(riding))
					cond = joinAnd(cond, new ConditionPlayerRiding(CheckPlayerRiding.STRIDER));
				else if("wyvern".equalsIgnoreCase(riding))
					cond = joinAnd(cond, new ConditionPlayerRiding(CheckPlayerRiding.WYVERN));
				else if("none".equalsIgnoreCase(riding))
					cond = joinAnd(cond, new ConditionPlayerRiding(CheckPlayerRiding.NONE));
			}
			else if("clanHall".equalsIgnoreCase(nodeName))
			{
				GArray<Integer> array = new GArray<Integer>();
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ",");
				while (st.hasMoreTokens())
					array.add(Integer.decode(st.nextToken().trim()));
				return new ConditionPlayerHasClanHall(array);
			}
			else if("fort".equalsIgnoreCase(nodeName))
			{
				int fort = getNumber(a.getNodeValue(), null).intValue();
				return new ConditionPlayerHasFort(fort);
			}
			else if("castle".equalsIgnoreCase(nodeName))
			{
				int castle = getNumber(a.getNodeValue(), null).intValue();
				return new ConditionPlayerHasCastle(castle);
			}
		}

		if(cond == null)
			_log.severe("Unrecognized <player> condition in " + file);
		return cond;
	}

	protected Condition parseTargetCondition(Node n)
	{
		Condition cond = null;
		String TnodeName = "";
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			String nodeName = TnodeName = a.getNodeName();

			if("aggro".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionTargetAggro(val));
			}
			else if("pvp".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionTargetPlayable(val));
			}
			else if("mob".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionTargetMob(val));
			}
			else if("race".equalsIgnoreCase(a.getNodeName()))
			{
				GArray<Integer> array = new GArray<Integer>();
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ",");
				while (st.hasMoreTokens())
				{
					String item = st.nextToken().trim();
					// -1 because we want to take effect for exactly race that is by -1 lower in FastList
					array.add(Integer.decode(getNumber(item, null).toString()) - 1);
				}
				cond = joinAnd(cond, new ConditionTargetRace(array));
			}
			else if("playerRace".equalsIgnoreCase(nodeName))
				cond = joinAnd(cond, new ConditionTargetPlayerRace(a.getNodeValue()));
			else if("castledoor".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionTargetCastleDoor(val));
			}
			else if("playerSameClan".equalsIgnoreCase(nodeName))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionTargetClan(val));
			}
			else if("direction".equalsIgnoreCase(nodeName))
			{
				String val = a.getNodeValue();
				if("behind".equalsIgnoreCase(val))
					cond = joinAnd(cond, new ConditionTargetDirection(TargetDirection.BEHIND));
				if("front".equalsIgnoreCase(val))
					cond = joinAnd(cond, new ConditionTargetDirection(TargetDirection.FRONT));
				if("side".equalsIgnoreCase(val))
					cond = joinAnd(cond, new ConditionTargetDirection(TargetDirection.FRONT));
			}
			else if("hasBuff".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ";");
				EffectType et = Enum.valueOf(EffectType.class, st.nextToken().trim());
				int level = -1;
				if(st.hasMoreTokens())
					level = TypeFormat.parseInt(st.nextToken().trim());
				cond = joinAnd(cond, new ConditionTargetHasBuff(et, level));
			}
			else if("hasBuffId".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(a.getNodeValue(), ";");
				int id = TypeFormat.parseInt(st.nextToken().trim());
				int level = -1;
				if(st.hasMoreTokens())
					level = TypeFormat.parseInt(st.nextToken().trim());
				cond = joinAnd(cond, new ConditionTargetHasBuffId(id, level));
			}
			else if("distanceToTarget".equalsIgnoreCase(nodeName))
			{
				int distance = getNumber(a.getNodeValue().trim(), null).intValue();
				cond = joinAnd(cond, new ConditionDistanceToTarget(distance));
			}
			else if("MinDistanceToTarget".equalsIgnoreCase(nodeName))
			{
				int distance = getNumber(a.getNodeValue().trim(), null).intValue();
				cond = joinAnd(cond, new ConditionMinDistance(distance));
			}
			else if("npcId".equalsIgnoreCase(nodeName))
			{
				String[] valuesSplit = a.getNodeValue().trim().split(",");
				return new ConditionTargetNpcId(valuesSplit);
			}
			else if("npcType".equalsIgnoreCase(nodeName))
			{
				String[] valuesSplit = a.getNodeValue().trim().split(",");
				return new ConditionTargetNpcType(valuesSplit);
			}
		}
		if(cond == null)
			_log.severe("Unrecognized <target> condition '" + TnodeName + "' in " + file);
		return cond;
	}

	protected Condition parseUsingCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			String nodeName = a.getNodeName();
			String nodeValue = a.getNodeValue();
			// Внимание вернёт труе если одет хоть 1 итем этого типа
			// например по запросу none вернёт труе если есть арм с типов none щиты так не выцепишь
			if("kind".equalsIgnoreCase(nodeName))
			{
				long mask = 0;
				StringTokenizer st = new StringTokenizer(nodeValue, ",");
				while (st.hasMoreTokens())
				{
					String item = st.nextToken().trim();
					for(WeaponType wt : WeaponType.values())
						if(wt.toString().equals(item))
						{
							mask |= wt.mask();
							break;
						}
					for(ArmorType at : ArmorType.values())
						if(at.toString().equals(item))
						{
							mask |= at.mask();
							break;
						}
				}
				if(mask > 0)
					cond = joinAnd(cond, new ConditionUsingItemType(mask));
			}
			else if("armor".equalsIgnoreCase(nodeName))
			{
				ArmorType armor = ArmorType.valueOf(nodeValue.toUpperCase());
				cond = joinAnd(cond, new ConditionUsingArmor(armor));
			}
			else if("skill".equalsIgnoreCase(nodeName))
			{
				if(Util.isNumber(nodeValue))
					cond = joinAnd(cond, new ConditionUsingSkill(TypeFormat.parseInt(nodeValue)));
				else
					cond = joinAnd(cond, new ConditionUsingSkill(nodeValue));
			}
			else if("slotitem".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(nodeValue, ";");
				int id = TypeFormat.parseInt(st.nextToken().trim());
				short slot = Short.parseShort(st.nextToken().trim());
				int enchant = 0;
				if(st.hasMoreTokens())
					enchant = TypeFormat.parseInt(st.nextToken().trim());
				cond = joinAnd(cond, new ConditionSlotItemId(slot, id, enchant));
			}
			else if("direction".equalsIgnoreCase(nodeName))
			{
				TargetDirection Direction = TargetDirection.valueOf(nodeValue.toUpperCase());
				cond = joinAnd(cond, new ConditionTargetDirection(Direction));
			}
		}
		if(cond == null)
			_log.severe("Unrecognized <using> condition in " + file);
		return cond;
	}

	protected Condition parseHasCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			String nodeName = a.getNodeName();
			String nodeValue = a.getNodeValue();
			if("skill".equalsIgnoreCase(nodeName))
			{
				StringTokenizer st = new StringTokenizer(nodeValue, ";");
				Integer id = getNumber(st.nextToken().trim(), null).intValue();
				short level = getNumber(st.nextToken().trim(), null).shortValue();
				cond = joinAnd(cond, new ConditionHasSkill(id, level));
			}
			else if("success".equalsIgnoreCase(nodeName))
				cond = joinAnd(cond, new ConditionFirstEffectSuccess(Boolean.valueOf(nodeValue)));
		}
		if(cond == null)
			_log.severe("Unrecognized <has> condition in " + file);
		return cond;
	}

	protected Condition parseGameCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			if("night".equalsIgnoreCase(a.getNodeName()))
			{
				boolean val = TypeFormat.parseBoolean(a.getNodeValue());
				cond = joinAnd(cond, new ConditionGameTime(CheckGameTime.NIGHT, val));
			}
		}
		if(cond == null)
			_log.severe("Unrecognized <game> condition in " + file);
		return cond;
	}

	protected Condition parseZoneCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); i++)
		{
			Node a = attrs.item(i);
			if("type".equalsIgnoreCase(a.getNodeName()))
				cond = joinAnd(cond, new ConditionZone(a.getNodeValue()));
		}
		if(cond == null)
			_log.severe("Unrecognized <zone> condition in " + file);
		return cond;
	}

	protected Condition parseInstancedZoneCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); ++i)
		{
			Node a = attrs.item(i);
			if("name".equalsIgnoreCase(a.getNodeName()))
				cond = joinAnd(cond, new ConditionInstancedZone(a.getNodeValue()));
		}
		if(cond == null)
			_log.severe("Unrecognized <InstancedZone> condition in " + file);
		return cond;
	}

	protected Condition parseInstancedZoneIdCondition(Node n)
	{
		Condition cond = null;
		NamedNodeMap attrs = n.getAttributes();
		for(int i = 0; i < attrs.getLength(); ++i)
		{
			Node a = attrs.item(i);
			if("ids".equalsIgnoreCase(a.getNodeName()))
				cond = joinAnd(cond, new ConditionInstancedZoneId(ArrayUtil.toIntArray(a.getNodeValue(), ",")));
		}
		if(cond == null)
			_log.severe("Unrecognized <InstancedZoneId> condition in " + file);
		return cond;
	}

	protected void parseTable(Node n)
	{
		NamedNodeMap attrs = n.getAttributes();
		String name = attrs.getNamedItem("name").getNodeValue();
		if(name.charAt(0) != '#')
			throw new IllegalArgumentException("Table name must start with #");
		StringTokenizer data = new StringTokenizer(n.getFirstChild().getNodeValue());
		GArray<String> array = new GArray<String>();
		while (data.hasMoreTokens())
			array.add(data.nextToken());
		Number[] res = new Number[array.size()];
		for(int i = 0; i < array.size(); i++)
			res[i] = getNumber(array.get(i), null);
		setTable(name, res);
	}

	protected void parseBeanSet(Node n, StatsSet set, Integer level)
	{
		String name = n.getAttributes().getNamedItem("name").getNodeValue().trim();
		String value = n.getAttributes().getNamedItem("val").getNodeValue().trim();
		char ch = value.length() == 0 ? ' ' : value.charAt(0);
		if(value.contains("#") && ch != '#') // кошмарная затычка на таблицы в сложных строках вроде triggerActions
			for(String str : value.split("[;: ]+"))
				if(str.charAt(0) == '#')
					value = value.replace(str, String.valueOf(getTableValue(str, level)));
		if(ch == '#')
		{
			Object tableVal = getTableValue(value, level);
			Number parsedVal = getNumber(tableVal.toString(), level);
			set.set(name, parsedVal == null ? tableVal : String.valueOf(parsedVal));
		}
		else if((Character.isDigit(ch) || ch == '-') && !value.contains(" ") && !value.contains(";"))
			set.set(name, String.valueOf(getNumber(value, level)));
		else
			set.set(name, value);
	}

	protected Lambda getLambda(Node n, Object template)
	{
		Node nval = n.getAttributes().getNamedItem("val");
		if(nval != null)
		{
			String val = nval.getNodeValue();
			if(val.charAt(0) == '#') // table by level
			{
				if(val.charAt(val.length() - 1) == '%')
				{
					String newVal = val.substring(0, val.length() - 1).trim();
					return new LambdaConstPercent(getTableValue(newVal).doubleValue());
				}
				return new LambdaConst(getTableValue(val.trim()).doubleValue());
			}

			if(val.charAt(val.length() - 1) == '%')
			{
				String newVal = val.substring(0, val.length() - 1);
				return new LambdaConstPercent(Double.parseDouble(newVal));
			}

			return new LambdaConst(Double.parseDouble(val));
		}

		LambdaCalc calc = new LambdaCalc();
		n = n.getFirstChild();
		while (n != null && n.getNodeType() != Node.ELEMENT_NODE)
			n = n.getNextSibling();
		if(n == null || !"val".equals(n.getNodeName()))
			throw new IllegalArgumentException("Value not specified");

		for(n = n.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if(n.getNodeType() != Node.ELEMENT_NODE)
				continue;
			attachLambdaFunc(n, template, calc);
		}
		return calc;
	}

	protected Number getNumber(String value, Object template)
	{
		if(value.charAt(0) == '#')
			if(template == null || template instanceof L2Skill)
				return getTableValue(value);
			else if(template instanceof Integer)
				return getTableValue(value, (Integer) template);
			else
				throw new IllegalStateException();
		if(value.indexOf('.') == -1)
		{
			int radix = 10;
			if(value.length() > 2 && value.substring(0, 2).equalsIgnoreCase("0x"))
			{
				value = value.substring(2);
				radix = 16;
			}
			return Integer.valueOf(value, radix);
		}
		return Double.valueOf(value);
	}

	protected Condition joinAnd(Condition cond, Condition c)
	{
		if(cond == null)
			return c;
		if(cond instanceof ConditionLogicAnd)
		{
			((ConditionLogicAnd) cond).add(c);
			return cond;
		}
		ConditionLogicAnd and = new ConditionLogicAnd();
		and.add(cond);
		and.add(c);
		return and;
	}
}
