package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.tables.NpcTable;

/**
 * @author L2System Project
 * @date 01.06.2010
 * @time 22:31:55
 */
public class Outpost extends L2Skill
{
	private final boolean _build;

	public Outpost(final StatsSet set)
	{
		super(set);
		_build = set.getBool("build", true);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;
		if(!super.checkCondition(activeChar, target, forceUse, dontMove, first))
			return false;
		final L2Player player = (L2Player) activeChar;
		final L2Clan clan = player.getClan();
		if(clan == null || !player.isClanLeader())
		{
			_log.warning(player.toFullString() + " has " + toString() + ", but he isn't in a clan leader.");
			return false;
		}

		if(player.isInZone(ZoneType.siege_residense) || !TerritorySiege.checkIfInZone(player))
		{
			activeChar.sendMessage("Outpost can't be placed here.");
			return false;
		}

		final SiegeClan siegeClan = TerritorySiege.getSiegeClan(clan);
		if(player.getTerritorySiege() == -1 || siegeClan == null)
		{
			activeChar.sendMessage("You must be registered to place a Outpost.");
			return false;
		}

		if(_build && siegeClan.getHeadquarter() != null)
		{
			activeChar.sendMessage("You already has a Outpost.");
			return false;
		}

		return _build || siegeClan.getHeadquarter() != null;
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Player player = (L2Player) activeChar;

		final L2Clan clan = player.getClan();
		if(clan == null || !player.isClanLeader())
			return;

		if(!TerritorySiege.checkIfInZone(player) || clan.getTerritorySiege() == -1 || player.isInZone(ZoneType.siege_residense))
			return;

		final SiegeClan siegeClan = TerritorySiege.getSiegeClan(clan);
		if(siegeClan == null)
			return;

		if(_build)
		{
			final L2SiegeHeadquarterInstance outpost = new L2SiegeHeadquarterInstance(player, IdFactory.getInstance().getNextId(), NpcTable.getTemplate(36590));

			outpost.setCurrentHpMp(outpost.getMaxHp(), outpost.getMaxMp(), true);
			outpost.setHeading(player.getHeading());
			outpost.setInvul(true);

			final int x = (int) (player.getX() + 100.0D * Math.cos(player.headingToRadians(player.getHeading() - 32768)));
			final int y = (int) (player.getY() + 100.0D * Math.sin(player.headingToRadians(player.getHeading() - 32768)));
			outpost.spawnMe(GeoEngine.moveCheck(player.getX(), player.getY(), player.getZ(), x, y));

			siegeClan.setHeadquarter(outpost);
		}
		else
		{
			final L2SiegeHeadquarterInstance outpost = siegeClan.getHeadquarter();
			if(outpost == null)
				return;
			outpost.deleteMe();
			siegeClan.setHeadquarter(null);
		}
	}
}
