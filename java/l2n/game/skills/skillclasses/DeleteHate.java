package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Formulas;

public class DeleteHate extends L2Skill
{
	public DeleteHate(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
		{
			if(!target.isRaid() && target.isNpc() && Formulas.calcSkillSuccess(activeChar, target, this, getActivateRate()))
			{
				final L2NpcInstance npc = (L2NpcInstance) target;
				npc.clearAggroList(true);
				npc.setAttackTimeout(Integer.MAX_VALUE);
				npc.getAI().setAttackTarget(null);
				npc.getAI().setGlobalAggro(-10);
				npc.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
			}

			getEffects(activeChar, target, false, false);
		}
	}
}
