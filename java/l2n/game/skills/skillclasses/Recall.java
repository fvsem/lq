package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TownManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.StopMove;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Location;

public class Recall extends L2Skill
{
	private final int _townId;
	private final boolean _clanhall;
	private final boolean _castle;
	private final boolean _fortress;

	public Recall(final StatsSet set)
	{
		super(set);
		_townId = set.getInteger("townId", 0);
		_clanhall = set.getBool("clanhall", false);
		_castle = set.getBool("castle", false);
		_fortress = set.getBool("fortress", false);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		final L2Player player = (L2Player) activeChar;
		if(player == null)
			return false;

		// BSOE в кланхолл/замок/форт работает только при наличии оного
		if(getHitTime() == 0 || getHitTime() == 200)
		{
			if(_clanhall && (player.getClan() == null || player.getClan().getHasHideout() == 0))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(getItemConsumeIDs()[0]));
				return false;
			}
			if(_castle && (player.getClan() == null || player.getClan().getHasCastle() == 0))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(getItemConsumeIDs()[0]));
				return false;
			}
			if(_fortress && (player.getClan() == null || player.getClan().getHasFortress() == 0))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(getItemConsumeIDs()[0]));
				return false;
			}
		}

		if(activeChar.isPlayer())
		{
			final L2Player p = (L2Player) activeChar;
			// Нельзя телепортироваться с флагом
			if(p.isCombatFlagEquipped() || p.isTerritoryFlagEquipped())
			{
				activeChar.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
				return false;
			}

			if(p.getDuel() != null || p.getTeam() != 0)
			{
				activeChar.sendMessage(new CustomMessage("common.RecallInDuel", activeChar));
				return false;
			}

			if(p.isInOlympiadMode() || p.isInJail())
			{
				activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Recall.Here", activeChar));
				return false;
			}
		}

		if(activeChar.isInZone(ZoneType.no_escape) || _townId > 0 && activeChar.getReflection().getCoreLoc() != null)
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Recall.Here", activeChar));
			return false;
		}
		// Нельзя юзать рекал на олимпе.
		if(activeChar.isInZone(ZoneType.OlympiadStadia))
		{
			activeChar.sendPacket(Msg.THIS_SKILL_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT);
			return false;
		}
		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(true);

		for(final L2Character target : targets)
		{
			final L2Player pcTarget = target.getPlayer();
			if(pcTarget == null)
				continue;

			if(pcTarget.isCombatFlagEquipped() || pcTarget.isTerritoryFlagEquipped())
			{
				activeChar.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
				continue;
			}

			if(pcTarget.isFestivalParticipant())
			{
				activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Recall.Festival", activeChar));
				continue;
			}
			if(pcTarget.isInOlympiadMode())
			{
				activeChar.sendPacket(Msg.THIS_SKILL_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT);
				return;
			}
			if(pcTarget.getDuel() != null)
			{
				activeChar.sendMessage(new CustomMessage("common.RecallInDuel", activeChar));
				return;
			}

			if(Config.COMMAND_ALLOW_SAVE_LOCATION && target.isPlayer())
			{
				final L2Player player = (L2Player) target;
				if(player.getVar("saveloc") != null)
				{
					final String[] coords = player.getVar("saveloc").split(";");
					final Location loc = new Location(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]));
					player.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Recall.savecoords", player));
					player.teleToLocation(loc);
					return;
				}
			}

			L2GameThreadPools.getInstance().scheduleAi(new Runnable()
			{
				@Override
				public void run()
				{
					target.abortAttack();
					target.abortCast();
					target.sendActionFailed();
					target.broadcastPacket(new StopMove(target));

					if(_isItemHandler)
					{
						// Scroll of Escape: Floran
						if(_itemConsumeIDs[0] == 7125)
						{
							pcTarget.teleToLocation(17144, 170156, -3502, 0);
							return;
						}
						// Scroll of Escape: Hardin's Private Academy
						if(_itemConsumeIDs[0] == 7127)
						{
							pcTarget.teleToLocation(105918, 109759, -3207, 0);
							return;
						}
						// Scroll of Escape: Ivory Tower
						if(_itemConsumeIDs[0] == 7130)
						{
							pcTarget.teleToLocation(85475, 16087, -3672, 0);
							return;
						}
						// Scroll of Escape: Kamael Village
						if(_itemConsumeIDs[0] == 9716)
						{
							pcTarget.teleToLocation(-120000, 44500, 352, 0);
							return;
						}
						// Scroll of Escape: Ketra Orc Village
						if(_itemConsumeIDs[0] == 7618)
						{
							pcTarget.teleToLocation(149864, -81062, -5618, 0);
							return;
						}
						// Scroll of Escape: Varka Silenos Village
						if(_itemConsumeIDs[0] == 7619)
						{
							pcTarget.teleToLocation(108275, -53785, -2524, 0);
							return;
						}
						if(_townId > 0)
						{
							pcTarget.teleToLocation(TownManager.getInstance().getTown(_townId).getSpawn());
							return;
						}
						if(_castle)
						{
							pcTarget.teleToCastle();
							return;
						}
						if(_clanhall)
						{
							pcTarget.teleToClanhall();
							return;
						}
						if(_fortress)
						{
							pcTarget.teleToFortress();
							return;
						}
					}

					if(target.isInZone(ZoneType.battle_zone) && target.getZone(ZoneType.battle_zone).getRestartPoints() != null)
					{
						target.teleToLocation(target.getZone(ZoneType.battle_zone).getSpawn());
						return;
					}

					if(target.isInZone(ZoneType.peace_zone) && target.getZone(ZoneType.peace_zone).getRestartPoints() != null)
					{
						target.teleToLocation(target.getZone(ZoneType.peace_zone).getSpawn());
						return;
					}

					// check for Hellbound
					boolean isInHellbound = false;
					final GArray<L2Territory> territories = L2World.getTerritories(target.getX(), target.getY(), target.getZ());
					if(territories != null)
						for(final L2Territory terr : territories)
							// все территории Хеллбаунда лежат в диапазоне от 700001 до 700152 (см. таблицу locations)
							if(terr.getId() >= 700000 && terr.getId() <= 700200)
							{
								isInHellbound = true;
								break;
							}
					// Рекалл из ХБ должен возвращать в Wastelands неподалеку от Warpgate
					if(isInHellbound)
					{
						target.teleToLocation(-15926, 209909, -3690);
						return;
					}

					target.teleToClosestTown();

				}
			}, 100, true);
		}
	}
}
