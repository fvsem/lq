package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ChestInstance;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.tables.DoorTable;
import l2n.util.Rnd;

public class Unlock extends L2Skill
{
	final int _unlockPower;

	public Unlock(final StatsSet set)
	{
		super(set);
		_unlockPower = set.getInteger("unlockPower", 0) + 100;
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null || target instanceof L2ChestInstance && target.isDead())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		if(target instanceof L2ChestInstance && activeChar.isPlayer())
			return super.checkCondition(activeChar, target, forceUse, dontMove, first);

		if(!(target instanceof L2DoorInstance) || _unlockPower == 0)
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		final L2DoorInstance door = (L2DoorInstance) target;
		if(door.isOpen())
		{
			activeChar.sendPacket(Msg.IT_IS_NOT_LOCKED);
			return false;
		}

		if(!door.isUnlockable())
		{
			activeChar.sendPacket(Msg.YOU_ARE_UNABLE_TO_UNLOCK_THE_DOOR);
			return false;
		}

		if(door.key == 0 && _unlockPower - door.getLevel() * 100 < 0)
		{
			activeChar.sendPacket(Msg.YOU_ARE_UNABLE_TO_UNLOCK_THE_DOOR);
			return false;
		}

		if(door.key > 0 && (getItemConsumeIDs().length == 0 || getItemConsumeIDs()[0] != door.key))
		{
			activeChar.sendPacket(Msg.YOU_ARE_UNABLE_TO_UNLOCK_THE_DOOR);
			return false;
		}
		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character targ : targets)
			if(targ instanceof L2DoorInstance)
			{
				final L2DoorInstance target = (L2DoorInstance) targ;
				if(Formulas.calculateUnlockChance(this) && !target.isOpen())
				{
					if(target.getDoorId() == 20210001 || target.getDoorId() == 20210002 || target.getDoorId() == 20210003)
					{
						for(final L2DoorInstance doorInst : DoorTable.getInstance().getDoors())
						{
							if(doorInst.getDoorId() == 20210001 || doorInst.getDoorId() == 20210002 || doorInst.getDoorId() == 20210003)
							{
								doorInst.openMe();
								doorInst.onOpen();
							}
						}	
					}
					else
					{	
						target.openMe();
						target.onOpen();
						return;
					}	
				}
				else
					activeChar.sendPacket(Msg.YOU_HAVE_FAILED_TO_UNLOCK_THE_DOOR);
				return;
			}
			else if(targ instanceof L2ChestInstance)
			{
				final L2ChestInstance chest = (L2ChestInstance) targ;
				if(chest.isDead())
					return;

				if(chest.isFake())
				{
					chest.onOpen((L2Player) activeChar);
					return;
				}

				double chance = getActivateRate();
				final double levelmod = (double) getMagicLevel() - chest.getLevel();
				chance += levelmod * getLevelModifier();

				if(chance < 0)
					chance = 1;

				if(chance < 100)
					activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Unlock.Chance", activeChar).addString(getName()).addNumber((int) chance));

				if(Rnd.chance(chance))
				{
					activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Unlock.Success", activeChar));
					chest.onOpen((L2Player) activeChar);
				}
				else
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_FAILED).addString(getName()));
					chest.doDie(activeChar);
				}
			}
	}
}
