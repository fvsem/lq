package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Formulas;
import l2n.game.templates.L2Weapon;

public class Deathlink extends L2Skill
{
	private final float _absorbPart;
	private final int _absorbAbs;

	public Deathlink(final StatsSet set)
	{
		super(set);

		_absorbPart = set.getFloat("absorbPart", 0.f);
		_absorbAbs = set.getInteger("absorbAbs", 0);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final int sps = isSSPossible() ? activeChar.getChargedSpiritShot() : 0;
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
		{
			if(target.isDead() && _targetType != SkillTargetType.TARGET_ONE)
				continue;

			double hp = 0.;

			if(_targetType != SkillTargetType.TARGET_ONE)
			{
				double damage = Formulas.calcMagicDam(activeChar, target, this, sps);
				final double targetCP = target.getCurrentCp();

				if(damage > targetCP || !target.isPlayer())
					hp = (damage - targetCP) * _absorbPart;

				if(damage < 1.0D - activeChar.getCurrentHp() / activeChar.getMaxHp() * 2.5D)
					damage = 1;

				target.reduceCurrentHp(damage, activeChar, this, true, true, false, true, false);
			}

			hp += _absorbAbs + activeChar.getCurrentHp();

			activeChar.setCurrentHp(hp, false);

			if(target.isDead() && _targetType == SkillTargetType.TARGET_ONE && target instanceof L2NpcInstance)
				((L2NpcInstance) target).endDecayTask();

			getEffects(activeChar, target, getActivateRate() > 0, false);

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
		}

		if(sps != 0)
			activeChar.unChargeShots(true);
	}

}
