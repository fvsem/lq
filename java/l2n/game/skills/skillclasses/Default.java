package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public class Default extends L2Skill
{
	public Default(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Default.NotImplemented", activeChar).addNumber(getId()).addString("" + getSkillType()));
		activeChar.sendActionFailed();
	}
}
