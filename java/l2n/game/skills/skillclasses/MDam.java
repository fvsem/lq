package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Formulas;
import l2n.game.templates.L2Weapon;

public class MDam extends L2Skill
{
	public MDam(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final int sps = isSSPossible() ? activeChar.getChargedSpiritShot() : 0;
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(L2Character target : targets)
		{
			if(target == null || target.isDead())
				continue;

			if(getPower() > 0) // Если == 0 значит скилл "отключен"
				if(target.checkReflectSkill(activeChar, this))
					target = activeChar;

			double damage = Formulas.calcMagicDam(activeChar, target, this, sps);
			if(damage >= 1)
				target.reduceCurrentHp(damage, activeChar, this, true, true, false, true, false);

			getEffects(activeChar, target, getActivateRate() > 0, false);

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
		}
		if(sps != 0)
			activeChar.unChargeShots(true);

		if(isSuicideAttack())
			activeChar.doDie(null);
	}
}
