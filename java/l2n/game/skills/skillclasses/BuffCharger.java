package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.tables.SkillTable;

/**
 * @author L2System Project
 * @date 09.08.2010
 * @time 22:23:36
 */
public class BuffCharger extends L2Skill
{
	private final int _target;

	public BuffCharger(final StatsSet set)
	{
		super(set);
		_target = set.getInteger("targetBuff", 0);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
		{
			int level = 0;
			final L2Effect e = target.getEffectList().getFirstEffect(_target);
			if(e != null)
				level = e.getSkill().getLevel();

			final L2Skill next = SkillTable.getInstance().getInfo(_target, level + 1);
			if(next != null)
				next.getEffects(activeChar, target, false, false);
		}
	}
}
