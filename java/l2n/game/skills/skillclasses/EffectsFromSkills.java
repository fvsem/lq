package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 09.08.2010
 * @time 22:08:43
 */
public class EffectsFromSkills extends L2Skill
{
	public EffectsFromSkills(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null)
				for(final AddedSkill as : getAddedSkills())
					as.getSkill().getEffects(activeChar, target, false, false);
	}
}
