package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.network.serverpackets.SystemMessage;

public class TakeCastle extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!super.checkCondition(activeChar, target, forceUse, dontMove, first))
			return false;

		if(activeChar == null || !activeChar.isPlayer())
			return false;

		final L2Player player = (L2Player) activeChar;
		if(player.getClan() == null || !player.isClanLeader())
			return false;

		final Siege siege = SiegeManager.getSiege(activeChar, true);
		if(siege == null || !(siege.getSiegeUnit() instanceof Castle))
			return false;
		if(!siege.isInProgress())
			return false;
		if(siege.getAttackerClan(player.getClan()) == null)
			return false;

		if(first)
			for(final SiegeClan sc : siege.getDefenderClans().values())
			{
				final L2Clan clan = sc.getClan();
				if(clan != null)
					for(final L2Player pl : clan.getOnlineMembers(0))
						if(pl != null)
							pl.sendPacket(new SystemMessage(SystemMessage.THE_OPPONENT_CLAN_HAS_BEGUN_TO_ENGRAVE_THE_RULER));
			}

		return true;
	}

	public TakeCastle(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target.isArtefact())
			{
				final L2Player player = (L2Player) activeChar;
				final Siege siege = SiegeManager.getSiege(activeChar, true);
				if(siege != null && siege.isInProgress())
				{
					siege.announceToPlayer(new SystemMessage(SystemMessage.CLAN_S1_HAS_SUCCEEDED_IN_ENGRAVING_THE_RULER).addString(player.getClan().getName()), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
					siege.engrave(player.getClan(), target.getObjectId());
				}
			}
	}
}
