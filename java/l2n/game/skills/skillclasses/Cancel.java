package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Formulas;
import l2n.game.templates.L2Weapon;
import l2n.util.Rnd;

public class Cancel extends L2Skill
{
	private final String _dispelType;
	private final int _cancelRate;
	private final int _negateCount;

	public Cancel(final StatsSet set)
	{
		super(set);
		_dispelType = set.getString("dispelType", "");
		_cancelRate = set.getInteger("cancelRate", 0);
		_negateCount = set.getInteger("negateCount", 5);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(L2Character target : targets)
		{
			if(target == null)
				continue;

			if(target.checkReflectSkill(activeChar, this))
				target = activeChar;

			if(!target.getEffectList().isEmpty())
				if(_cancelRate <= 0 || Formulas.calcSkillSuccess(activeChar, target, this, _cancelRate))
				{
					int counter = 0;
					if(_dispelType.equals(""))
					{
						int antiloop = Config.ALT_BUFF_LIMIT;
						final L2Effect[] effects = target.getEffectList().getAllEffects();
						if(effects.length > 0)
							while (counter < _negateCount && antiloop > 0)
							{
								final L2Effect e = effects[Rnd.get(effects.length)];
								if(e != null && e.getSkill().isCancelable())
									if(caclChance(counter, _negateCount))
									{
										e.exit();
										counter++;
									}
								antiloop--;
							}
					}
					else
					{
						final L2Effect[] effects = target.getEffectList().getAllEffects();
						if(_dispelType.contains("negative"))
						{
							for(final L2Effect e : effects)
								if(e != null && counter < _negateCount && e.getSkill().isOffensive() && e.getSkill().isCancelable())
									if(caclChance(counter, _negateCount))
									{
										e.exit();
										counter++;
									}
						}
						else if(_dispelType.contains("positive"))
							for(final L2Effect e : effects)
								if(e != null && counter < _negateCount && !e.getSkill().isOffensive() && e.getSkill().isCancelable())
									if(caclChance(counter, _negateCount))
									{
										e.exit();
										counter++;
									}
					}

					if(weapon != null)
						weapon.getEffect(false, activeChar, target, isOffensive());
				}

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}

	private static boolean caclChance(final double count, final double max)
	{
		double rate = 1 - count / max;
		if(rate < 0.33)
			rate = 0.33;
		else if(rate > 0.95)
			rate = 0.95;
		return Rnd.get(1000) < rate * 1000;
	}
}
