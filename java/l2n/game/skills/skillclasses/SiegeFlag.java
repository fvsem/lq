package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncFactory;
import l2n.game.tables.NpcTable;
import l2n.util.Location;

import java.util.logging.Logger;

public class SiegeFlag extends L2Skill
{
	protected static Logger _log = Logger.getLogger(SiegeFlag.class.getName());
	private final boolean _advanced;
	private final double _advancedMult;

	public SiegeFlag(final StatsSet set)
	{
		super(set);
		_advanced = set.getBool("advancedFlag", false);
		_advancedMult = set.getDouble("advancedMultiplier", 1.);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;
		if(!super.checkCondition(activeChar, target, forceUse, dontMove, first))
			return false;

		final L2Player player = (L2Player) activeChar;
		if(player.getClan() == null || !player.isClanLeader())
		{
			_log.warning(player.toFullString() + " has " + toString() + ", but he isn't in a clan leader.");
			return false;
		}

		if(player.isInZone(ZoneType.siege_residense))
		{
			activeChar.sendMessage("Flag can't be placed at castle.");
			return false;
		}

		final Siege siege = SiegeManager.getSiege(activeChar, true);
		if(siege == null || !checkIfOkToPlaceFlag(player, siege))
			return false;

		return true;
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Player player = (L2Player) activeChar;

		final Siege siege = SiegeManager.getSiege(activeChar, true);

		if(siege == null || !checkIfOkToPlaceFlag(player, siege))
			return;
		//Проверяем точки спауна флага
		final int x = (int) (player.getX() + 100 * Math.cos(player.headingToRadians(player.getHeading() - 32768)));
		final int y = (int) (player.getY() + 100 * Math.sin(player.headingToRadians(player.getHeading() - 32768)));
		
		if(!GeoEngine.canMoveToCoord(player.getX(), player.getY(), player.getZ(), x, y, player.getZ()))
		{
			player.sendMessage("Нельзя поставить флаг, когда есть препятствие.");
			return;
		}

		final L2SiegeHeadquarterInstance flag = new L2SiegeHeadquarterInstance(player, IdFactory.getInstance().getNextId(), NpcTable.getTemplate(35062));

		if(_advanced)
			try
			{
				flag.addStatFunc(FuncFactory.createFunc("Mul", Stats.MAX_HP, 0x50, _advancedMult, flag));
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

		flag.setCurrentHpMp(flag.getMaxHp(), flag.getMaxMp(), true);
		flag.setHeading(player.getHeading());

		// Ставим флаг перед чаром
		flag.spawnMe(new Location(x, y, GeoEngine.getHeight(x, y, player.getZ())));

		final SiegeClan clan = siege.getAttackerClan(player.getClan());
		if(clan != null)
			clan.setHeadquarter(flag);
	}

	public boolean checkIfOkToPlaceFlag(final L2Player activeChar, final Siege siege)
	{
		if(siege == null || !activeChar.isInZone(ZoneType.Siege))
			activeChar.sendMessage("You must be on siege field to place a flag.");
		else if(activeChar.isInZone(ZoneType.siege_residense))
			activeChar.sendMessage("Flag can't be placed ay castle.");
		else if(!siege.isInProgress())
			activeChar.sendMessage("You can only place a flag during a siege.");
		else if(siege.getAttackerClan(activeChar.getClan()) == null)
			activeChar.sendMessage("You must be an attacker to place a flag.");
		else if(activeChar.getClan() == null || !activeChar.isClanLeader())
			activeChar.sendMessage("You must be a clan leader to place a flag.");
		else
			return true;
		return false;
	}
}
