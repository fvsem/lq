package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2TrapInstance;

/**
 * @author L2System Project
 * @date 13.02.2011
 * @time 5:45:24
 */
public class DefuseTrap extends L2Skill
{
	public DefuseTrap(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null || !target.isTrap())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null && target.isTrap())
			{

				final L2TrapInstance trap = (L2TrapInstance) target;
				if(trap.getLevel() <= getPower())
					trap.destroy();
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
