package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Summon;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;

public class DestroySummon extends L2Skill
{
	public DestroySummon(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null)
			{
				if(getActivateRate() > 0 && !Formulas.calcSkillSuccess(activeChar, target, this, getActivateRate()))
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addString(target.getName()).addSkillName(getId(), getLevel()));
					continue;
				}

				if(target.isSummon())
				{
					((L2Summon) target).unSummon();
					getEffects(activeChar, target, getActivateRate() > 0, false);
				}
			}
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
