package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.BeginRotation;
import l2n.game.network.serverpackets.StopRotation;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Env;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;
import l2n.game.skills.Stats;
import l2n.util.Rnd;

public class PDam extends L2Skill
{
	private final boolean _onCrit;
	private final boolean _directHp;
	private final boolean _blow;
	private final int _turner;
	private final String _dispelType;
	private final int _cancelRate;
	private final int _negateCount;

	public PDam(final StatsSet set)
	{
		super(set);
		_onCrit = set.getBool("onCrit", false);
		_directHp = set.getBool("directHp", false);
		_blow = set.getBool("blow", false);
		_turner = set.getInteger("turner", 0);
		_dispelType = set.getString("dispelType", "");
		_cancelRate = set.getInteger("cancelRate", 0);
		_negateCount = set.getInteger("negateCount", 5);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		// Thread.dumpStack();
		final boolean ss = activeChar.getChargedSoulShot() && isSSPossible();
		if(ss)
			activeChar.unChargeShots(false);

		L2Character realTarget;
		boolean reflected;

		Env env;
		for(L2Character target : targets)
			if(target != null && !target.isDead())
			{
				if(_cancelRate > 0)
				{
					env = new Env();
					env.character = activeChar;
					env.target = target;
					env.skill = this;
					env.value = _cancelRate;
					if(Formulas.calcSkillSuccess(env, Stats.CANCEL_RECEPTIVE, Stats.CANCEL_POWER, activeChar.getChargedSpiritShot()))
					{
						int counter = 0;
						if(_dispelType.contains("negative"))
							for(final L2Effect e : target.getEffectList().getAllEffects())
								if(counter < _negateCount && e.getSkill().isOffensive() && e.getSkill().isCancelable())
								{
									e.exit();
									counter++;
								}

						if(_dispelType.contains("positive"))
							for(final L2Effect e : target.getEffectList().getAllEffects())
							{
								final L2Skill skill = e.getSkill();
								if(counter < _negateCount && !skill.isOffensive() && skill.isCancelable())
								{
									e.exit();
									counter++;
								}
							}
					}
				}

				if(_turner > 0 && Rnd.chance(_turner) && !target.isInvul())
				{
					target.broadcastPacket(new BeginRotation(target, target.getHeading(), 1, 65535));
					target.broadcastPacket(new StopRotation(target, activeChar.getHeading(), 65535));
					target.setHeading(activeChar.getHeading());
					target.sendPacket(new SystemMessage(SystemMessage.YOU_CAN_FEEL_S1S_EFFECT).addSkillName(_displayId, _displayLevel));
				}

				reflected = target.checkReflectSkill(activeChar, this);
				realTarget = reflected ? activeChar : target;

				AttackInfo info = Formulas.calcPhysDam(activeChar, realTarget, this, false, _blow, ss, _onCrit);
				if(info.lethal_dmg > 0)
					realTarget.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, false, false);

				if(!info.miss || info.damage >= 1)
					realTarget.reduceCurrentHp(info.damage, activeChar, this, true, true, info.lethal ? false : _directHp, true, false);

				if(!reflected)
					Formulas.doCounterAttack(this, activeChar, realTarget, _blow);

				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

		if(isSuicideAttack())
		{
			activeChar.doDie(null);
			activeChar.onDecay();
		}

		else if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
