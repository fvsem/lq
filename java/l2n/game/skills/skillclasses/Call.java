package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class Call extends L2Skill
{
	public static final int SUMMONING_CRYSTAL = 8615;

	private final boolean _party;

	public Call(final StatsSet set)
	{
		super(set);
		_party = set.getBool("party", false);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(activeChar.isPlayer())
		{
			if(_party && ((L2Player) activeChar).getParty() == null)
				return false;
			SystemMessage msg = canSummonHere(activeChar);
			if(msg != null)
			{
				activeChar.sendPacket(msg);
				return false;
			}

			if(!_party)
			{
				if(activeChar == target)
					return false;
				msg = canBeSummoned(target);
				if(msg != null)
				{
					activeChar.sendPacket(msg);
					return false;
				}
			}
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final SystemMessage msg = canSummonHere(activeChar);
		if(msg != null)
		{
			activeChar.sendPacket(msg);
			return;
		}

		if(_party)
		{
			if(((L2Player) activeChar).getParty() != null)
				for(final L2Player target : ((L2Player) activeChar).getParty().getPartyMembers())
					if(!target.equals(activeChar) && canBeSummoned(target) == null)
					{
						target.stopMove();
						target.teleToLocation(GeoEngine.findPointToStay(activeChar.getX(), activeChar.getY(), activeChar.getZ(), 100, 150));
						getEffects(activeChar, target, getActivateRate() > 0, false);
					}

			if(isSSPossible())
				activeChar.unChargeShots(isMagic());
			return;
		}

		for(final L2Character target : targets)
			if(target != null)
			{
				if(canBeSummoned(target) != null)
					continue;

				// При использовании скилов Summon Friend и Word of Invitation у призываемых персонажей
				// надо удалить 1 Summoning Crystall из инвентаря
				((L2Player) target).summonCharacterRequest(activeChar.getName(), GeoEngine.findPointToStay(activeChar.getX(), activeChar.getY(), activeChar.getZ(), 100, 150), getId() == 1403 || getId() == 1404 ? 1 : 0);
				getEffects(activeChar, target, getActivateRate() > 0, false);
			}
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}

	public static SystemMessage canSummonHere(final L2Character activeChar)
	{
		// "Нельзя вызывать персонажей в/из зоны свободного PvP"
		// "в зоны осад"
		// "на Олимпийский стадион"
		// "в зоны определенных рейд-боссов и эпик-боссов"
		if(activeChar.isInZoneBattle() || activeChar.isInZone(ZoneType.Siege) || activeChar.isInZone(ZoneType.epic) || activeChar.isInZone(ZoneType.no_restart) || activeChar.isInZone(ZoneType.no_summon) || activeChar.isInZone(ZoneType.OlympiadStadia) || activeChar.isFlying() || activeChar.isInVehicle() || activeChar.getReflection().getId() != 0)
			return Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION;
		if(activeChar.isInCombat())
			return Msg.YOU_CANNOT_SUMMON_DURING_COMBAT;

		if(((L2Player) activeChar).getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || ((L2Player) activeChar).isInTransaction())
			return Msg.YOU_CANNOT_SUMMON_DURING_A_TRADE_OR_WHILE_USING_THE_PRIVATE_SHOPS;

		// нельзя сумонить в тюрьму)
		if(((L2Player) activeChar).isInJail())
			return Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION;

		// Нельзя саммонить в зону рифта
		if(DimensionalRiftManager.getInstance().checkIfInRiftZone(activeChar.getLoc(), false))
			return Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION;

		return null;
	}

	public static SystemMessage canBeSummoned(final L2Character target)
	{
		if(target == null || !target.isPlayer() || target.getPlayer().isTerritoryFlagEquipped())
			return Msg.INCORRECT_TARGET;

		if(target.isInZoneOlympiad())
			return Msg.YOU_CANNOT_SUMMON_PLAYERS_WHO_ARE_CURRENTLY_PARTICIPATING_IN_THE_GRAND_OLYMPIAD;

		if(target.isInZoneBattle() || target.isInZone(ZoneType.Siege) || target.isInZone(ZoneType.epic) || target.isInZone(ZoneType.no_restart) || target.isInZone(ZoneType.no_summon) || target.getReflection().getId() != 0 || target.isFlying() || target.isInVehicle() || target.inObserverMode())
			return Msg.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING;

		// Нельзя призывать мертвых персонажей
		if(target.isDead() || target.isAlikeDead())
			return new SystemMessage(SystemMessage.S1_IS_DEAD_AT_THE_MOMENT_AND_CANNOT_BE_SUMMONED).addString(target.getName());

		if(target.getPvpFlag() != 0 || target.isInCombat())
			return new SystemMessage(SystemMessage.S1_IS_ENGAGED_IN_COMBAT_AND_CANNOT_BE_SUMMONED).addString(target.getName());
		final L2Player pTarget = (L2Player) target;

		// Нельзя призывать торгующих персонажей
		if(pTarget.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || pTarget.isInTransaction())
			return new SystemMessage(SystemMessage.S1_IS_CURRENTLY_TRADING_OR_OPERATING_A_PRIVATE_STORE_AND_CANNOT_BE_SUMMONED).addString(target.getName());

		// нельзя суммонить цель из тюрьмы
		if(target.isPlayer() && target.getPlayer().isInJail())
			return Msg.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING;

		return null;
	}
}
