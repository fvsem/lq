package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2TrapInstance;
import l2n.game.network.serverpackets.NpcInfo;

/**
 * @author L2System Project
 * @date 13.02.2011
 * @time 5:43:32
 */
public class DetectTrap extends L2Skill
{
	public DetectTrap(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null && target.isTrap())
			{
				final L2TrapInstance trap = (L2TrapInstance) target;
				if(trap.getLevel() <= getPower())
				{
					trap.setDetected(true);
					for(final L2Player player : L2World.getAroundPlayers(trap))
						if(player != null)
							player.sendPacket(new NpcInfo(trap, player));
				}
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
