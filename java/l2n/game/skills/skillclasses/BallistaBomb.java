package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2FortressBallistaInstance;
import l2n.util.Rnd;

/**
 * @author L2System Project
 */
public class BallistaBomb extends L2Skill
{
	public BallistaBomb(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character cha, final L2Character... targets)
	{
		if(!cha.isPlayer() || targets.length == 0)
			return;

		for(final L2Character target : targets)
			if(target instanceof L2FortressBallistaInstance)
			{
				final L2FortressBallistaInstance ballista = (L2FortressBallistaInstance) target;
				ballista.incBombsUseCounter();
				if(Rnd.chance(20) || ballista.getBombsUseCounter() > 4) // TODO вывести в конфиг
					ballista.reduceCurrentHp(target.getMaxHp(), cha, this, true, true, false, false, false);
			}
			else
				cha.sendPacket(Msg.INCORRECT_TARGET);

	}
}
