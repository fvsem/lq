package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.instances.*;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncAdd;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

public class Summon extends L2Skill
{
	private final SummonType _summonType;

	private final float _expPenalty;
	private final int _itemConsumeIdInTime;
	private final int _itemConsumeCountInTime;
	private final int _itemConsumeDelay;
	private final int _lifeTime;

	/** activation chance for a cubic. */
	private final int _activationChance;

	/** level of enchanted cubic skill */
	private final int _cubicSkillLevelEnchant;

	private static enum SummonType
	{
		PET,
		CUBIC,
		AGATHION,
		TRAP,
		DECOY,
		MERCHANT
	}

	public Summon(final StatsSet set)
	{
		super(set);

		_summonType = Enum.valueOf(SummonType.class, set.getString("summonType", "PET").toUpperCase());
		_expPenalty = set.getFloat("expPenalty", 0);
		_itemConsumeIdInTime = set.getInteger("itemConsumeIdInTime", 0);
		_itemConsumeCountInTime = set.getInteger("itemConsumeCountInTime", 0);
		_itemConsumeDelay = set.getInteger("itemConsumeDelay", 240) * 1000;

		_lifeTime = set.getInteger("lifeTime", 1200) * 1000; // 20 minutes default
		_activationChance = set.getInteger("activationChance", 0);

		// Для заточенных кубиков)
		_cubicSkillLevelEnchant = set.getInteger("cubicSkillLevels", 0);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(_summonType != SummonType.CUBIC && !activeChar.isPlayer())
			return false;

		final L2Player player = activeChar.getPlayer();

		switch (_summonType)
		{
			case CUBIC:
				if(_targetType == SkillTargetType.TARGET_SELF)
				{
					int mastery = player.getSkillLevel(L2Skill.SKILL_CUBIC_MASTERY);
					if(mastery < 0)
						mastery = 0;
					final int count = player.getCubics().size();
					if(count > mastery && player.getCubic(getNpcId()) == null)
						return false;
				}
				break;
			case AGATHION:
				if(player.getAgathion() != null && getNpcId() != 0)
				{
					player.sendMessage("You may not use multiple agathions at the same time.");
					return false;
				}
				break;
			case TRAP:
				if(player.isInZonePeace() && !player.isGM())
				{
					player.sendPacket(Msg.A_MALICIOUS_SKILL_CANNOT_BE_USED_IN_A_PEACE_ZONE);
					return false;
				}
				break;
			case PET:
			case DECOY:
				if(player.getPet() != null || player.isMounted())
				{
					player.sendPacket(Msg.YOU_ALREADY_HAVE_A_PET);
					return false;
				}
				break;
			case MERCHANT:
				if(player.getPet() != null || player.isMounted() || player.getMerchant() != null)
				{
					player.sendPacket(Msg.YOU_ALREADY_HAVE_A_PET);
					return false;
				}
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character caster, final L2Character... targets)
	{
		if(_summonType != SummonType.CUBIC && !caster.isPlayer())
			return;

		final L2Player activeChar = caster.getPlayer();

		if(isSSPossible())
			caster.unChargeShots(isMagic());

		if(getNpcId() == 0)
			if(activeChar.getAgathion() != null)
				activeChar.setAgathion(0);
			else
			{
				caster.sendMessage("Summon skill " + getId() + " not described yet");
				return;
			}

		switch (_summonType)
		{
			case AGATHION:
				activeChar.setAgathion(getNpcId());
				break;
			case CUBIC:
				int level = _level;
				if(_cubicSkillLevelEnchant > 0)
					level = _cubicSkillLevelEnchant;
				for(final L2Character targ : targets)
				{
					if(!targ.isPlayer())
						continue;
					final L2Player target = (L2Player) targ;

					int mastery = target.getSkillLevel(L2Skill.SKILL_CUBIC_MASTERY);
					if(mastery < 0)
						mastery = 0;

					if(target.getCubics().size() > mastery && target.getCubic(getNpcId()) == null)
					{
						target.sendPacket(Msg.CUBIC_SUMMONING_FAILED);
						continue;
					}

					if(getNpcId() == 3 && _lifeTime == 3600000) // novice life cubic
						target.addCubic(3, 8, 3600000, _activationChance, false);
					else
						target.addCubic(getNpcId(), level, _lifeTime, _activationChance, caster != target);

					target.broadcastUserInfo(true);
					getEffects(caster, target, getActivateRate() > 0, false);
				}
				break;
			case TRAP:
				final L2Skill trapSkill = getFirstAddedSkill();
				if(trapSkill == null)
				{
					_log.warning("Not implemented trap skill, id = " + getId());
					return;
				}
				if(activeChar.getTrapsCount() >= 5)
				{
					activeChar.destroyFirstTrap();
					if(activeChar.getTrapsCount() >= 5)
					{
						System.out.println("Error while deleting trap!");
						return;
					}
				}
				activeChar.addTrap(new L2TrapInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(getNpcId()), activeChar, trapSkill, activeChar.getLoc()));
				break;
			case PET:
				// Удаление трупа, если идет суммон из трупа.
				Location loc = null;
				if(_targetType == SkillTargetType.TARGET_CORPSE)
					for(final L2Character target : targets)
						if(target != null && target.isDead() && target instanceof L2NpcInstance)
						{
							((L2NpcInstance) target).endDecayTask();
							loc = target.getLoc();
						}

				if(activeChar.getPet() != null || activeChar.isMounted())
					return;

				final L2NpcTemplate summonTemplate = NpcTable.getTemplate(getNpcId());
				final L2SummonInstance summon = new L2SummonInstance(IdFactory.getInstance().getNextId(), summonTemplate, activeChar, _lifeTime, _itemConsumeIdInTime, _itemConsumeCountInTime, _itemConsumeDelay);

				summon.setTitle(activeChar.getName());
				summon.setExpPenalty(_expPenalty);
				summon.setExp(Experience.LEVEL[summon.getLevel()]);
				summon.setCurrentHp(summon.getMaxHp(), false);
				summon.setCurrentMp(summon.getMaxMp());
				summon.setHeading(activeChar.getHeading());
				summon.setReflection(activeChar.getReflection());
				summon.setRunning();

				activeChar.setPet(summon);

				summon.spawnMe(loc == null ? GeoEngine.findPointToStay(activeChar.getX(), activeChar.getY(), activeChar.getZ(), 100, 150).rnd(50, 80, false) : loc);

				if(summon.getSkillLevel(4140) > 0)
					summon.altUseSkill(SkillTable.getInstance().getInfo(4140, summon.getSkillLevel(4140)), activeChar);

				if(summon.getName().equalsIgnoreCase("Shadow"))
					summon.addStatFunc(new FuncAdd(Stats.ABSORB_DAMAGE_PERCENT, 0x40, this, 15));

				summon.setFollowStatus(true, true);
				summon.broadcastCharInfo();
				
				// addVisibleObject created the info packets with summon animation
				// if someone comes into range now, the animation shouldnt show any more
				summon.setShowSpawnAnimation(0);
				break;
			case DECOY:
				if(activeChar.getPet() != null || activeChar.isMounted())
					return;

				final L2NpcTemplate DecoyTemplate = NpcTable.getTemplate(getNpcId());
				final L2DecoyInstance decoy = new L2DecoyInstance(IdFactory.getInstance().getNextId(), DecoyTemplate, activeChar, _lifeTime);

				decoy.setCurrentHp(decoy.getMaxHp(), false);
				decoy.setCurrentMp(decoy.getMaxMp());
				decoy.setHeading(activeChar.getHeading());
				decoy.setReflection(activeChar.getReflection());

				activeChar.setDecoy(decoy);

				decoy.spawnMe(activeChar.getLoc());
				break;

			case MERCHANT:
				if(activeChar.getPet() != null || activeChar.isMounted() || activeChar.getMerchant() != null)
					return;

				L2GolemTraderInstance.spawn(this, activeChar, _lifeTime);
				break;

		}
		if(isSSPossible())
			caster.unChargeShots(isMagic());
	}

	@Override
	public boolean isOffensive()
	{
		return _targetType == SkillTargetType.TARGET_CORPSE;
	}
}
