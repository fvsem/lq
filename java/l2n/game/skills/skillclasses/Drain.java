package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;
import l2n.game.skills.Stats;

public class Drain extends L2Skill
{
	private final double _absorbAbs;

	public Drain(final StatsSet set)
	{
		super(set);
		_absorbAbs = set.getFloat("absorbAbs", 0.f);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final int sps = isSSPossible() ? activeChar.getChargedSpiritShot() : 0;
		final boolean ss = isSSPossible() && activeChar.getChargedSoulShot();

		L2Character realTarget;
		boolean reflected;
		final boolean corpseSkill = _targetType == SkillTargetType.TARGET_CORPSE;
		for(L2Character target : targets)
			if(target != null)
			{
				reflected = !corpseSkill && target.checkReflectSkill(activeChar, this);
				realTarget = reflected ? activeChar : target;

				if(getPower() > 0 || _absorbAbs > 0) // Если == 0 значит скилл "отключен"
				{
					if(target.isDead() && !corpseSkill)
						continue;

					double hp = 0.;
					final double targetHp = target.getCurrentHp();

					if(!corpseSkill)
					{
						double damage;
						if(isMagic())
							damage = Formulas.calcMagicDam(activeChar, realTarget, this, sps);
						else
						{
							AttackInfo info = Formulas.calcPhysDam(activeChar, realTarget, this, false, false, ss, false);
							damage = info.damage;

							if(info.lethal_dmg > 0)
								realTarget.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, false, false);
						}

						final double targetCP = target.getCurrentCp();

						// Нельзя восстанавлУвать HP из CP
						if(damage > targetCP || !target.isPlayer())
							hp = (damage - targetCP) * _absorbPart;

						target.reduceCurrentHp(damage, activeChar, this, true, true, false, true, false);
						if(!reflected)
							Formulas.doCounterAttack(this, activeChar, realTarget, false);
					}

					if(_absorbAbs == 0 && _absorbPart == 0)
						continue;

					hp += _absorbAbs;

					// Нельзя восстановить больше hp, чем есть у цели.
					if(hp > targetHp && !corpseSkill)
						hp = targetHp;

					double addToHp = Math.max(0, Math.min(hp, activeChar.calcStat(Stats.HP_LIMIT, 100, null, null) * activeChar.getMaxHp() / 100. - activeChar.getCurrentHp()));
					
					if(addToHp > targetHp && !corpseSkill)
						addToHp = targetHp;
					
					if(addToHp > 0 && !target.isDoor() && !activeChar.isHealHPBlocked(false))
						activeChar.setCurrentHp(activeChar.getCurrentHp() + addToHp, false);

					if(target.isDead() && corpseSkill && target.isNpc())
					{
						activeChar.getAI().setAttackTarget(null);
						((L2NpcInstance) target).endDecayTask();
					}
				}

				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

		if(isMagic() ? sps != 0 : ss)
			activeChar.unChargeShots(isMagic());
	}

}
