package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.fortress.FortressSiege;
import l2n.game.model.instances.L2StaticObjectInstance;
import l2n.game.network.serverpackets.SystemMessage;

public class TakeFortress extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!super.checkCondition(activeChar, target, forceUse, dontMove, first))
			return false;

		if(activeChar == null || !activeChar.isPlayer())
			return false;

		final L2Player player = (L2Player) activeChar;
		if(player.getClan() == null)
			return false;

		final FortressSiege siege = FortressSiegeManager.getSiege(activeChar);
		if(siege == null || !siege.getCommanders().isEmpty() || !siege.isInProgress())
			return false;
		if(siege.getAttackerClan(player.getClan()) == null)
			return false;
		
		//vitalay fortress bug fix
		L2Object flagPole = activeChar.getTarget();
		if(!(flagPole instanceof L2StaticObjectInstance) || ((L2StaticObjectInstance) flagPole).getType() != 3)
		{
			return false;
		}

		if(!activeChar.isInRangeZ(flagPole, getCastRange()))
		{
			activeChar.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
			return false;
		}

		if(first)
			for(final SiegeClan sc : siege.getDefenderClans().values())
			{
				final L2Clan clan = sc.getClan();
				if(clan != null)
					clan.broadcastToOnlineMembers(new SystemMessage(SystemMessage.THE_OPPONENT_CLAN_HAS_BEGUN_TO_ENGRAVE_THE_RULER));
			}

		return true;
	}

	public TakeFortress(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Object flagPole = activeChar.getTarget();
		if(flagPole != null && flagPole.isStaticObject() && ((L2StaticObjectInstance) flagPole).getType() == 3)
		{
			final L2Player player = (L2Player) activeChar;
			final FortressSiege siege = FortressSiegeManager.getSiege(activeChar);
			if(siege != null && siege.isInProgress() && siege.getCommanders().isEmpty())
			{
				siege.announceToPlayer(new SystemMessage(SystemMessage.CLAN_S1_HAS_SUCCEEDED_IN_ENGRAVING_THE_RULER).addString(player.getClan().getName()), false, Siege.ANNOUNCE_TO_BOTH_SIDES);
				siege.engrave(player.getClan(), flagPole.getObjectId());
			}
		}
	}
}
