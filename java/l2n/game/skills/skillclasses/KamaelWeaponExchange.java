package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExAutoSoulShot;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.ShortCutInit;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;

import java.util.HashMap;

public class KamaelWeaponExchange extends L2Skill
{
	public KamaelWeaponExchange(final StatsSet set)
	{
		super(set);
	}

	private static HashMap<Integer, Integer> exchangemap;

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer() || activeChar.isOutOfControl() || activeChar.getDuel() != null || activeChar.getActiveWeaponInstance() == null)
			return false;

		final L2Player p = (L2Player) activeChar;
		if(p.getPrivateStoreType() != 0 || p.isInTransaction())
			return false;

		final L2ItemInstance item = activeChar.getActiveWeaponInstance();
		if(item != null && convertWeaponId(item.getItemId()) == 0)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_CONVERT_THIS_ITEM));
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Player player = (L2Player) activeChar;
		final L2ItemInstance item = activeChar.getActiveWeaponInstance();
		if(item == null)
			return;

		final int itemtoexchange = convertWeaponId(item.getItemId());
		if(itemtoexchange == 0)
			return;

		player.getInventory().unEquipItemInBodySlotAndNotify(item.getBodyPart(), item);
		player.sendPacket(new InventoryUpdate(item, L2ItemInstance.REMOVED));
		item.setItemId(itemtoexchange);

		player.sendPacket(new ShortCutInit(player));

		for(final int shotId : player.getAutoSoulShot())
			player.sendPacket(new ExAutoSoulShot(shotId, true));

		player.sendPacket(new InventoryUpdate(item, L2ItemInstance.ADDED), new SystemMessage(SystemMessage.YOU_HAVE_EQUIPPED_YOUR_S1).addItemName(itemtoexchange));

		L2GameThreadPools.getInstance().scheduleAi(new Runnable()
		{
			@Override
			public void run()
			{
				player.getInventory().equipItem(item, true);
				player.broadcastUserInfo(true);
			}
		}, 150, true);
	}

	private int convertWeaponId(final int source)
	{
		if(exchangemap == null)
			fillExchangeMap();

		final Integer ret = exchangemap.get(source);
		return ret != null ? ret : 0;
	}

	private synchronized void fillExchangeMap()
	{
		if(exchangemap != null)
			return;

		exchangemap = new HashMap<Integer, Integer>();

		final int[][] weapons = ItemTable.getInstance().getWeaponEx();
		for(int i = 0; i < weapons.length; ++i)
		{
			final int[] item = weapons[i];
			if(item != null && item[ItemTable.WEX_KAMAEL_EX] > 0)
			{
				final int analog = item[ItemTable.WEX_KAMAEL_EX];
				exchangemap.put(i, analog);
				exchangemap.put(item[ItemTable.WEX_SA1], weapons[analog][ItemTable.WEX_SA1]);
				exchangemap.put(item[ItemTable.WEX_SA2], weapons[analog][ItemTable.WEX_SA2]);
				exchangemap.put(item[ItemTable.WEX_SA3], weapons[analog][ItemTable.WEX_SA3]);
				exchangemap.put(item[ItemTable.WEX_COMMON], weapons[analog][ItemTable.WEX_COMMON]);
				exchangemap.put(item[ItemTable.WEX_RARE], weapons[analog][ItemTable.WEX_RARE]);
				exchangemap.put(item[ItemTable.WEX_RARE_SA1], weapons[analog][ItemTable.WEX_RARE_SA1]);
				exchangemap.put(item[ItemTable.WEX_RARE_SA2], weapons[analog][ItemTable.WEX_RARE_SA2]);
				exchangemap.put(item[ItemTable.WEX_RARE_SA3], weapons[analog][ItemTable.WEX_RARE_SA3]);
			}
		}
	}

}
