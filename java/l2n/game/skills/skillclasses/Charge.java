package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;

public class Charge extends L2Skill
{
	private final int _charges;
	private final boolean _fullCharge;

	public Charge(final StatsSet set)
	{
		super(set);
		_charges = set.getInteger("charges", getLevel());
		_fullCharge = set.getBool("fullCharge", false);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;

		final L2Player player = (L2Player) activeChar;

		// Камушки можно юзать даже если заряд > 7, остальное только если заряд < уровень скила
		if(getId() != 2165 && player.getIncreasedForce() >= _charges && getId() != 345 && getId() != 346 && getId() != 570)
		{
			activeChar.sendPacket(Msg.YOUR_FORCE_HAS_REACHED_MAXIMUM_CAPACITY);
			return false;
		}
		else if(getId() == 2165)
			player.sendPacket(new MagicSkillUse(player, player, 2165, 1, 0, 0));

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		final boolean ss = activeChar.getChargedSoulShot() && isSSPossible();
		if(ss && getTargetType() != SkillTargetType.TARGET_SELF)
			activeChar.unChargeShots(false);

		L2Character realTarget;
		boolean reflected;
		for(L2Character target : targets)
		{
			if(target.isDead() || target == activeChar)
				continue;

			reflected = target.checkReflectSkill(activeChar, this);
			realTarget = reflected ? activeChar : target;

			if(getPower() > 0)
			{
				AttackInfo info = Formulas.calcPhysDam(activeChar, realTarget, this, false, false, ss, false);
				if(info.lethal_dmg > 0)
					realTarget.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, true, false);

				realTarget.reduceCurrentHp(info.damage, activeChar, this, true, true, false, true, false);
				if(!reflected)
					Formulas.doCounterAttack(this, activeChar, realTarget, false);
			}

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}

		chargePlayer((L2Player) activeChar, getId());
	}

	public void chargePlayer(final L2Player player, final int skillId)
	{
		if(player.getIncreasedForce() >= _charges)
		{
			player.sendPacket(Msg.YOUR_FORCE_HAS_REACHED_MAXIMUM_CAPACITY);
			return;
		}
		if(_fullCharge)
			player.setIncreasedForce(_charges);
		else
			player.setIncreasedForce(player.getIncreasedForce() + 1);
	}
}
