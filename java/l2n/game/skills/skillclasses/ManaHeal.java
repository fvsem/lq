package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.tables.PetDataTable;
import l2n.game.templates.L2Weapon;

public class ManaHeal extends L2Skill
{
	public ManaHeal(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!isHandler() && oneTarget() && !checkTarget(target, activeChar))
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		double mp = _power;

		final int sps = isSSPossible() ? activeChar.getChargedSpiritShot() : 0;
		if(sps > 0)
		{
			if(Config.MANAHEAL_SPS_BONUS)
				mp *= sps == 2 ? 1.2 : 1.1;
			activeChar.unChargeShots(true);
		}
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
			if(checkTarget(target, activeChar))
			{
				if(target.isDead() || target.isHealMPBlocked(true))
					continue;

				double maxNewMp = activeChar == target ? mp : Math.min(mp * 1.7, target.calcStat(Stats.MANAHEAL_EFFECTIVNESS, mp, activeChar, this));

				// Обработка разницы в левелах при речардже. Учитывыется разница уровня скилла и уровня цели.
				// 1013 = id скилла recharge. Для сервиторов не проверено убавление маны, пока оставлено так как есть.
				if(getMagicLevel() > 0 && activeChar != target)
				{
					final int diff = target.getLevel() - getMagicLevel();
					if(diff > 5)
						if(diff < 20)
							maxNewMp = maxNewMp / 100. * (100 - diff * 5);
						else
							maxNewMp = 0;
				}

				if(maxNewMp == 0)
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_FAILED).addSkillName(_id, getLevel()));
					if(weapon != null)
						weapon.getEffect(false, activeChar, target, isOffensive());
					getEffects(activeChar, target, getActivateRate() > 0, false);
					continue;
				}

				final double addToMp = Math.max(0, Math.min(maxNewMp, target.calcStat(Stats.MP_LIMIT, 100, null, null) * target.getMaxMp() / 100. - target.getCurrentMp()));

				if(addToMp > 0)
					target.setCurrentMp(addToMp + target.getCurrentMp());

				if(target.isPlayer())
					if(activeChar != target)
						target.sendPacket(new SystemMessage(SystemMessage.S1_MP_HAS_BEEN_RESTORED_BY_XS2S).addString(activeChar.getName()).addNumber(Math.round(addToMp)));
					else
						activeChar.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToMp)));

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());
				getEffects(activeChar, target, getActivateRate() > 0, false);
			}
	}

	private final boolean checkTarget(final L2Character target, final L2Character activeChar)
	{
		if(target == null || target.isDead() || target.isHealMPBlocked(true))
			return false;

		// бутылки, хербы, и массовый речардж действует на всех
		if(getTargetType() == SkillTargetType.TARGET_SELF || !oneTarget())
			return true;

		// петы и саммоны могут речарджить всех, ограничения только для игроков
		if(target.isPlayer() && activeChar.isPlayer() && target.getPlayer().getSkillLevel(1013) > 0)
			return false;

		// речардж кукабурр - большая халява
		if(!Config.ALT_ALLOW_RECHARG_KOOKABURRA && target.getNpcId() == PetDataTable.IMPROVED_BABY_KOOKABURRA_ID)
			return false;

		return true;
	}
}
