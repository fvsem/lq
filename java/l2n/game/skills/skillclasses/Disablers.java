package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.templates.L2Weapon;

public class Disablers extends L2Skill
{
	public Disablers(final StatsSet set)
	{
		super(set);
		_skillInterrupt = set.getBool("skillInterrupt", false);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(L2Character target : targets)
			if(target != null)
			{
				if(target.checkReflectSkill(activeChar, this))
					target = activeChar;
				if(_skillInterrupt)
				{
					if(target.getCastingSkill() != null && !target.getCastingSkill().isMagic() && !target.isRaid())
						target.breakCast(false);

					if(!target.isRaid())
						target.abortAttack();
				}
				getEffects(activeChar, target, getActivateRate() > 0, false);

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());
			}
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
