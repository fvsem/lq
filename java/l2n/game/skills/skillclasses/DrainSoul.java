package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public class DrainSoul extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!target.isMonster())
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}
		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public DrainSoul(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		// This is just a dummy skill for the soul crystal skill condition,
		// since the Soul Crystal item handler already does everything.
	}
}
