package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.EffectType;
import l2n.game.skills.Formulas;
import l2n.game.templates.L2Weapon;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

public class NegateEffects extends L2Skill
{
	private Object[][] _negateEffects = null; // <EffectType, Integer>
	private Object[][] _negateStackType = null; // <String, Integer>
	private final boolean _onlyPhysical;
	private final boolean _negateDebuffs;
	private final boolean _isProbability;

	public NegateEffects(final StatsSet set)
	{
		super(set);

		final String[] negateEffectsString = set.getString("negateEffects", "").split(";");
		for(int i = 0; i < negateEffectsString.length; i++)
			if(!negateEffectsString[i].isEmpty())
			{
				final String[] entry = negateEffectsString[i].split(":");
				_negateEffects = ArrayUtil.arrayAdd(_negateEffects, new Object[] { Enum.valueOf(EffectType.class, entry[0]), entry.length > 1 ? Integer.decode(entry[1]) : Integer.MAX_VALUE }, Object[].class);
			}

		final String[] negateStackTypeString = set.getString("negateStackType", "").split(";");
		for(int i = 0; i < negateStackTypeString.length; i++)
			if(!negateStackTypeString[i].isEmpty())
			{
				final String[] entry = negateStackTypeString[i].split(":");
				_negateStackType = ArrayUtil.arrayAdd(_negateStackType, new Object[] { entry[0], entry.length > 1 ? Integer.decode(entry[1]) : Integer.MAX_VALUE }, Object[].class);
			}

		_onlyPhysical = set.getBool("onlyPhysical", false);
		_negateDebuffs = set.getBool("negateDebuffs", true);
		_isProbability = set.getBool("isProbability", false);

		if(_negateEffects == null && _negateStackType == null)
			_log.info("Invalid skill " + getId() + "." + getLevel() + ": NegateEffects must have negateEffects or negateStackType.");
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(final L2Character target : targets)
			if(target != null)
			{
				if(!_negateDebuffs && !Formulas.calcSkillSuccess(activeChar, target, this, getActivateRate()))
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_RESISTED_YOUR_S2).addString(target.getName()).addSkillName(getId(), getLevel()));
					continue;
				}

				if(_negateEffects != null)
					for(final Object[] stat : _negateEffects)
						negateEffectAtPower(target, (EffectType) stat[0], (Integer) stat[1]);

				if(_negateStackType != null)
					for(final Object[] stat : _negateStackType)
						negateEffectAtPower(target, (String) stat[0], (Integer) stat[1]);

				getEffects(activeChar, target, getActivateRate() > 0, false);

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());

			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}

	private void negateEffectAtPower(final L2Character target, final EffectType type, final Integer power)
	{
		for(final L2Effect e : target.getEffectList().getAllEffects())
		{
			if(e == null)
				continue;
			final L2Skill skill = e.getSkill();
			if(_onlyPhysical && skill.isMagic() || !skill.isCancelable() || skill.isOffensive() && !_negateDebuffs)
				continue;
			// Если у бафа выше уровень чем у скилла Cancel, то есть шанс, что этот баф не снимется
			if(!skill.isOffensive() && skill.getMagicLevel() > getMagicLevel() && Rnd.chance(skill.getMagicLevel() - getMagicLevel()))
				continue;
			if(e.getEffectType() == type && (_isProbability && Rnd.chance(power) || !_isProbability && e.getStackOrder() <= power))
				e.exit();
		}
	}

	private void negateEffectAtPower(final L2Character target, final String stackType, final Integer power)
	{
		for(final L2Effect e : target.getEffectList().getAllEffects())
		{
			if(e == null)
				continue;
			final L2Skill skill = e.getSkill();
			if(_onlyPhysical && skill.isMagic() || !skill.isCancelable() || skill.isOffensive() && !_negateDebuffs)
				continue;
			// Если у бафа выше уровень чем у скилла Cancel, то есть шанс, что этот баф не снимется
			if(!skill.isOffensive() && skill.getMagicLevel() > getMagicLevel() && Rnd.chance(skill.getMagicLevel() - getMagicLevel()))
				continue;
			if(e.checkStackType(stackType) && (_isProbability && Rnd.chance(power) || !_isProbability && e.getStackOrder() <= power))
				e.exit();
		}
	}
}
