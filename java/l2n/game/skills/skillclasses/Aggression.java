package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2DecoyInstance;
import l2n.game.model.instances.L2NpcInstance;

public class Aggression extends L2Skill
{
	private final boolean _unaggring;
	private final boolean _lure;

	public Aggression(final StatsSet set)
	{
		super(set);
		_unaggring = set.getBool("unaggroing", false);
		_lure = set.getBool("lure", false);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		int effect = _aggroPoints;

		if(_aggroPoints <= 0)
			effect = (int) _power;

		if(isSSPossible() && activeChar.unChargeShots(isMagic()))
			effect *= 2;

		for(final L2Character target : targets)
		{
			if(target == null || !(target.isNpc() || target.isPlayable()))
				continue;

			if(target.isAggressionImmune())
				continue;

			if(_unaggring)
			{
				if(target.isNpc() && activeChar.isPlayable())
					((L2Playable) activeChar).addDamageHate((L2NpcInstance) target, 0, -effect);
			}
			else if(activeChar instanceof L2DecoyInstance)
			{
				if(((L2DecoyInstance) activeChar).getPlayer() == target)
					return;
				target.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, activeChar);
			}
			else
			{
				target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, activeChar, effect);
				if(!_lure && target.isNpc())
					((L2NpcInstance) target).callFriends(activeChar);
			}

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
