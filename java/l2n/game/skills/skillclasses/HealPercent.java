package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;

public class HealPercent extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target instanceof L2DoorInstance || target instanceof L2SiegeHeadquarterInstance)
			return false;

		// Restore Life skill has been changed to not be used on an enemy target (Restore Life больше нельзя применить на вражескую цель)
		if(_id == SKILL_RESTORE_LIFE)
		{
			if(!target.isPlayable())
				return false;

			if(activeChar != target)
			{
				final L2Player player = activeChar.getPlayer();
				final L2Player pcTarget = target.getPlayer();
				if(player != null && pcTarget != null)
				{
					if(pcTarget.getKarma() > 0)
						return false;
					if(pcTarget.getPvpFlag() != 0)
						return false;
					if(player.atMutualWarWith(pcTarget))
						return false;
					if(player.isInOlympiadMode() && player.getOlympiadSide() != pcTarget.getOlympiadSide()) // Чужой команде помогать нельзя
						return false;
				}
			}
		}

		if(activeChar.isPlayable() && target.isMonster())
			return false;

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	private final boolean _healMp;
	private final boolean _healCp;

	public HealPercent(final StatsSet set)
	{
		super(set);
		_healMp = set.getBool("healMp", false);
		_healCp = set.getBool("healCp", false);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
		{
			if(target.isDead())
				continue;

			// target.stopEffect(_id);
			getEffects(activeChar, target, getActivateRate() > 0, false);

			final double addToHp = Math.max(0, Math.min(target.getMaxHp() * _power / 100, target.calcStat(Stats.HP_LIMIT, 100, null, null) * target.getMaxHp() / 100. - target.getCurrentHp()));
			if(addToHp > 0)
				target.setCurrentHp(addToHp + target.getCurrentHp(), false);

			if(target.isPlayer() && activeChar == target)
				target.sendPacket(new SystemMessage(SystemMessage.S1_HPS_HAVE_BEEN_RESTORED).addNumber((int) addToHp));
			if(target.isPlayer() && activeChar != target)
				target.sendPacket(new SystemMessage(SystemMessage.S1_HP_HAS_BEEN_RESTORED_BY_XS2S).addString(activeChar.getName()).addNumber((int) addToHp));

			if(_healMp)
			{
				final double addToMp = Math.max(0, Math.min(target.getMaxMp() * _power / 100, target.calcStat(Stats.MP_LIMIT, 100, null, null) * target.getMaxMp() / 100. - target.getCurrentMp()));
				if(addToMp > 0)
					target.setCurrentMp(addToMp + target.getCurrentMp());

				if(target.isPlayer() && activeChar == target)
					target.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber((int) addToMp));
				if(target.isPlayer() && activeChar != target)
					target.sendPacket(new SystemMessage(SystemMessage.S1_MP_HAS_BEEN_RESTORED_BY_XS2S).addString(activeChar.getName()).addNumber((int) addToMp));
			}
			if(_healCp)
			{
				final double addToCp = Math.max(0, Math.min(target.getMaxCp() * _power / 100, target.calcStat(Stats.CP_LIMIT, 100, null, null) * target.getMaxCp() / 100. - target.getCurrentCp()));
				if(addToCp > 0)
					target.setCurrentCp(addToCp + target.getCurrentCp());

				if(target.isPlayer() && activeChar == target)
					target.sendPacket(new SystemMessage(SystemMessage.S1_CPS_WILL_BE_RESTORED).addNumber((int) addToCp));
				if(target.isPlayer() && activeChar != target)
					target.sendPacket(new SystemMessage(SystemMessage.S1_WILL_RESTORE_S2S_CP).addString(activeChar.getName()).addNumber((int) addToCp));
			}

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
			if(isSuicideAttack())
				activeChar.doDie(activeChar);
		}
	}
}
