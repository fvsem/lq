package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;

public class ChargeSoul extends L2Skill
{
	private final int _numSouls;

	public ChargeSoul(final StatsSet set)
	{
		super(set);
		_numSouls = set.getInteger("numSouls", getLevel());
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		final boolean ss = activeChar.getChargedSoulShot() && isSSPossible();

		L2Character realTarget;
		boolean reflected;
		for(L2Character target : targets)
		{
			if(target == null || target.isDead())
				continue;

			reflected = target != activeChar && target.checkReflectSkill(activeChar, this);
			realTarget = reflected ? activeChar : target;

			if(getPower() > 0)
			{
				AttackInfo info = Formulas.calcPhysDam(activeChar, realTarget, this, false, false, ss, false);
				if(info.lethal_dmg > 0)
					realTarget.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, false, false);

				realTarget.reduceCurrentHp(info.damage, activeChar, this, true, true, false, true, false);
				if(!reflected)
					Formulas.doCounterAttack(this, activeChar, realTarget, false);
			}

			if(target.isPlayable() || target.isMonster())
				activeChar.setConsumedSouls(activeChar.getConsumedSouls() + _numSouls, null);
			getEffects(activeChar, target, getActivateRate() > 0, false);
		}

		if(ss && getTargetType() != L2Skill.SkillTargetType.TARGET_SELF)
			activeChar.unChargeShots(isMagic());
	}
}
