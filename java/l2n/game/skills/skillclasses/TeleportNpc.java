package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public class TeleportNpc extends L2Skill
{
	public TeleportNpc(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
		{

			if(target == null || target.isDead())
				continue;

			getEffects(activeChar, target, getActivateRate() > 0, false);
			target.abortAttack();
			target.abortCast();
			target.sendActionFailed();
			target.stopMove();
			int x = activeChar.getX();
			int y = activeChar.getY();
			final int z = activeChar.getZ();
			final int h = activeChar.getHeading();
			final int range = (int) (activeChar.getColRadius() + target.getColRadius());
			final int hyp = (int) Math.sqrt(range * range / 2);
			if(h < 16384)
			{
				x += hyp;
				y += hyp;
			}
			else if(h > 16384 && h <= 32768)
			{
				x -= hyp;
				y += hyp;
			}
			else if(h < 32768 && h <= 49152)
			{
				x -= hyp;
				y -= hyp;
			}
			else if(h > 49152)
			{
				x += hyp;
				y -= hyp;
			}
			target.setXYZ(x, y, z);
			target.validateLocation(1);
		}
	}
}
