package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Manor;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Rnd;

public class Sowing extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		// ПроверкУ в хэндлере
		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public Sowing(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		final L2Player player = (L2Player) activeChar;
		final int seed_id = player.getUseSeed();
		// remove seed from inventory
		final L2ItemInstance seedItem = player.getInventory().getItemByItemId(seed_id);
		if(seedItem != null)
		{
			player.sendPacket(SystemMessage.removeItems(seed_id, 1));
			player.getInventory().destroyItem(seedItem, 1, true);
		}
		else
		{
			activeChar.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		for(final L2Character target : targets)
			if(target != null)
			{
				final L2MonsterInstance monster = (L2MonsterInstance) target;
				if(monster.isSeeded())
					continue;

				// обработка
				double successRate = Config.MANOR_SOWING_BASIC_SUCCESS;

				final double diffPlayerTarget = Math.abs(activeChar.getLevel() - target.getLevel());
				final double diffSeedTarget = Math.abs(L2Manor.getInstance().getSeedLevel(seed_id) - target.getLevel());

				// гтраф, на разнУцу уровней между мобом У Угроком
				// 5% на каждый уровень прУ разнУце >5 - по умолчанУю
				if(diffPlayerTarget > Config.MANOR_DIFF_PLAYER_TARGET)
					successRate -= (diffPlayerTarget - Config.MANOR_DIFF_PLAYER_TARGET) * Config.MANOR_DIFF_PLAYER_TARGET_PENALTY;

				// гтраф, на разнУцу уровней между семечкой У мобом
				// 5% на каждый уровень прУ разнУце >5 - по умолчанУю
				if(diffSeedTarget > Config.MANOR_DIFF_SEED_TARGET)
					successRate -= (diffSeedTarget - Config.MANOR_DIFF_SEED_TARGET) * Config.MANOR_DIFF_SEED_TARGET_PENALTY;

				if(ItemTable.getInstance().getTemplate(seed_id).isAltSeed())
				{
					successRate *= Config.MANOR_SOWING_ALT_BASIC_SUCCESS / Config.MANOR_SOWING_BASIC_SUCCESS;
					successRate = 100 - 1. * (100 - successRate);
				}

				// МУнУмальный шанс успеха всегда 1%
				if(successRate < 1)
					successRate = 1;

				if(Config.SKILLS_SHOW_CHANCE && !player.getVarB("SkillsHideChance"))
					activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Sowing.Chance", activeChar).addNumber((long) successRate));

				if(Rnd.chance(successRate))
				{
					monster.setSeeded(seedItem.getItem(), player);
					activeChar.sendPacket(Msg.THE_SEED_WAS_SUCCESSFULLY_SOWN);
				}
				else
					activeChar.sendPacket(Msg.THE_SEED_WAS_NOT_SOWN);
			}
	}
}
