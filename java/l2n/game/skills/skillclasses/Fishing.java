package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.GameTimeController;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.FishData;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.ExFishingStart;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.FishTable;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

public class Fishing extends L2Skill
{
	public Fishing(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		final L2Player player = (L2Player) activeChar;

		if(player.getSkillLevel(SKILL_FISHING_MASTERY) == -1)
			return false;

		if(player.isFishing())
		{
			if(player.getFishCombat() != null)
				player.getFishCombat().doDie(false);
			else
				player.endFishing(false);
			player.sendPacket(new SystemMessage(SystemMessage.CANCELS_FISHING));
			return false;
		}

		if(player.isInVehicle())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANT_FISH_WHILE_YOU_ARE_ON_BOARD));
			return false;
		}

		if(player.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_FISH_WHILE_USING_A_RECIPE_BOOK_PRIVATE_MANUFACTURE_OR_PRIVATE_STORE));
			return false;
		}

		if(Config.ENABLE_FISHING_CHECK_WATER)
		{
			final int rnd = Rnd.get(50) + 150;
			final double angle = Util.convertHeadingToDegree(player.getHeading());
			final double radian = Math.toRadians(angle - 90);
			final double sin = Math.sin(radian);
			final double cos = Math.cos(radian);
			final int x1 = -(int) (sin * rnd);
			final int y1 = (int) (cos * rnd);
			final int x = player.getX() + x1;
			final int y = player.getY() + y1;
			final int z = GeoEngine.getHeight(x, y, player.getZ());
			player.setFishLoc(new Location(x, y, z));

			if(!ZoneManager.getInstance().checkIfInZoneFishing(x, y, z))
			{
				player.sendPacket(new SystemMessage(SystemMessage.YOU_CANT_FISH_HERE));
				return false;
			}
		}

		final L2Weapon weaponItem = player.getActiveWeaponItem();
		if(weaponItem == null || weaponItem.getItemType() != WeaponType.ROD)
		{
			// Fishing poles are not installed
			player.sendPacket(new SystemMessage(SystemMessage.FISHING_POLES_ARE_NOT_INSTALLED));
			return false;
		}

		final L2ItemInstance lure = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
		if(lure == null || lure.getCount() < 1)
		{
			player.sendPacket(new SystemMessage(SystemMessage.BAITS_ARE_NOT_PUT_ON_A_HOOK));
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character caster, final L2Character... targets)
	{
		if(caster == null || !caster.isPlayer())
			return;

		final L2Player player = (L2Player) caster;

		final L2ItemInstance lure = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
		if(lure == null || lure.getCount() < 1)
		{
			player.sendPacket(new SystemMessage(SystemMessage.BAITS_ARE_NOT_PUT_ON_A_HOOK));
			return;
		}

		final L2ItemInstance lure2 = player.getInventory().destroyItem(player.getInventory().getPaperdollObjectId(Inventory.PAPERDOLL_LHAND), 1, false);
		if(lure2 == null || lure2.getCount() == 0)
			player.sendPacket(new SystemMessage(SystemMessage.NOT_ENOUGH_BAIT));

		final int lvl = FishTable.getInstance().getRandomFishLvl(player);
		final int group = FishTable.getInstance().getFishGroup(lure.getItemId());
		final int type = FishTable.getInstance().getRandomFishType(lure.getItemId());

		final GArray<FishData> fishs = FishTable.getInstance().getfish(lvl, type, group);
		if(fishs == null || fishs.size() == 0)
		{
			player.sendMessage("Error: Fishes are not definied, report admin please.");
			player.endFishing(false);
			return;
		}

		final int check = Rnd.get(fishs.size());
		final FishData fish = fishs.get(check);

		if(!GameTimeController.getInstance().isNowNight() && lure.isNightLure())
			fish.setType(-1);

		player.stopMove();
		player.setImmobilized(true);
		player.setFishing(true);
		player.setFish(fish);
		player.setLure(lure);
		player.broadcastUserInfo(true);
		player.broadcastPacket(new ExFishingStart(player, fish.getType(), player.getFishLoc(), lure.isNightLure()));
		player.sendPacket(new SystemMessage(SystemMessage.STARTS_FISHING));
		player.startLookingForFishTask();
	}
}
