package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.templates.L2Weapon;

public class ManaDam extends L2Skill
{
	public ManaDam(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final int sps = isSSPossible() ? activeChar.getChargedSpiritShot() : 0;
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(L2Character target : targets)
			if(target != null)
			{
				if(target.isDead())
					return;

				if(target.checkReflectSkill(activeChar, this))
					target = activeChar;

				final double damage = Formulas.calcMagicDam(activeChar, target, this, sps);
				final double mp = damage > target.getCurrentMp() ? target.getCurrentMp() : damage;
				if(damage > 0)
				{
					target.reduceCurrentMp(mp, activeChar);
					if(activeChar.isPlayer())
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOUR_OPPONENTS_MP_WAS_REDUCED_BY_S1).addNumber((long) mp));

					if(target.isPlayer())
					{
						final SystemMessage sm = new SystemMessage(SystemMessage.S2S_MP_HAS_BEEN_DRAINED_BY_S1);
						sm.addName(activeChar);
						sm.addNumber((int) mp);
						target.sendPacket(sm);
					}
				}

				getEffects(activeChar, target, getActivateRate() > 0, false);

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}

	@Override
	public void displayHitMessage(final L2Character attacker, final L2Character target, final long damage, final boolean crit)
	{
		if(attacker.isPlayer() && crit)
			attacker.sendPacket(Msg.MAGIC_CRITICAL_HIT);
	}
}
