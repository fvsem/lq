package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

import static l2n.game.model.L2Zone.ZoneType.Siege;
import static l2n.game.model.L2Zone.ZoneType.no_restart;

public class ClanGate extends L2Skill
{
	public ClanGate(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(activeChar.isPlayer())
		{
			// "Нельзя вызывать персонажей в/из зоны свободного PvP"
			// "в зоны осад"
			// "на Олимпийский стадион"
			// "в зоны определенных рейд-боссов и эпик-боссов"
			if(activeChar.isInZoneBattle() || activeChar.isInZone(Siege) || activeChar.isInZone(no_restart))
			{
				activeChar.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
				return false;
			}

			if(activeChar.getReflectionId() != 0)
			{
				activeChar.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
				return false;
			}

			// Нельзя саммонить в зону рифта
			if(DimensionalRiftManager.getInstance().checkIfInRiftZone(activeChar.getLoc(), false))
			{
				activeChar.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
				return false;
			}

			final L2Player activePlayer = (L2Player) activeChar;

			// нельзя сумонить в тюрьму)
			if(activePlayer.isInJail())
			{
				activePlayer.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
				return false;
			}

			// нельзя суммонить в режиме боя
			if(activeChar.isInCombat())
			{
				activeChar.sendPacket(Msg.YOU_CANNOT_SUMMON_DURING_COMBAT);
				return false;
			}

			// нельзя суммонить, находясь в режиме торговли или в процессе торговли/обмена
			if(activePlayer.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || activePlayer.isInTransaction())
			{
				activeChar.sendPacket(Msg.YOU_CANNOT_SUMMON_DURING_A_TRADE_OR_WHILE_USING_THE_PRIVATE_SHOPS);
				return false;
			}

			// проверить наличие боссов в радиусе 5000
			for(final L2Object object : L2World.getAroundObjects(activePlayer, 5000, 500))
				if(object.isBoss())
				{
					activePlayer.sendPacket(Msg.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION);
					return false;
				}

			// проверить мб клангейт уже создан для клана?
			final L2Clan clan = activePlayer.getClan();
			if(clan.getClanGate() != null)
			{
				activePlayer.sendMessage("Clan gate already exists!");
				return false;
			}

			// таргет незачем проверять
		}
		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());

		if(!activeChar.isPlayer())
			return;

		final L2Player activePlayer = (L2Player) activeChar;
		final L2Clan clan = activePlayer.getClan();
		if(clan.getClanGate() != null)
		{
			activePlayer.sendMessage("Clan gate already exists!");
			return;
		}

		new L2ClanGate(activePlayer);

		// apply effects (immobileBuff)
		for(final L2Character target : targets)
			getEffects(activeChar, target, getActivateRate() > 0, false);
	}
}
