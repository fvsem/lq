package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

public class Ride extends L2Skill
{
	public Ride(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;

		final L2Player player = (L2Player) activeChar;
		if(getNpcId() != 0 && !player.checksForMount(false, false))
			return false;
		else if(getNpcId() == 0 && !player.isMounted())
			return false;

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character caster, final L2Character... targets)
	{
		if(!caster.isPlayer())
			return;

		final L2Player activeChar = (L2Player) caster;
		activeChar.setMount(getNpcId(), 0);
	}
}
