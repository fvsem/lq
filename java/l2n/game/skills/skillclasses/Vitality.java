package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.UserInfo;

public class Vitality extends L2Skill
{
	public Vitality(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null && target.isPlayer())
			{
				final L2Player player = target.getPlayer();
				player.addVitalityPoints((float) getPower(), false);
				player.sendPacket(new UserInfo(player));

				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

	}
}
