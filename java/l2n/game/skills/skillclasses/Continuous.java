package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;
import l2n.util.Rnd;

public class Continuous extends L2Skill
{
	private final String _aborts;

	public Continuous(final StatsSet set)
	{
		super(set);
		_aborts = set.getString("abortType", "");
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(L2Character target : targets)
		{
			if(target == null)
				continue;

			// Player holding a cursed weapon can't be buffed and can't buff
			if(getSkillType() == L2Skill.SkillType.BUFF && target != activeChar)
				if(target.isCursedWeaponEquipped() || activeChar.isCursedWeaponEquipped())
					continue;

			if(target.checkReflectSkill(activeChar, this))
				target = activeChar;

			final double mult = 0.01 * target.calcStat(Stats.DEATH_RECEPTIVE, 100, null, null);
			final double lethal1 = getLethal1() * mult;
			final double lethal2 = getLethal2() * mult;

			if(lethal1 > 0 && Rnd.chance(lethal1))
			{
				if(target.isPlayer() && !target.isInvul() && !target.isDead())
				{
					target.reduceCurrentHp(target.getCurrentCp(), activeChar, this, true, true, false, true, false);
					target.sendPacket(Msg.INSTANT_KILL);
					activeChar.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}
				else if(target.isNpc() && !target.isLethalImmune())
				{
					target.reduceCurrentHp(target.getCurrentHp() / 2, activeChar, this, true, true, false, true, false);
					activeChar.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}
			}
			else if(lethal2 > 0 && Rnd.chance(lethal2))
				if(target.isPlayer() && !target.isInvul() && !target.isDead())
				{
					target.reduceCurrentHp(target.getCurrentHp() + target.getCurrentCp() - 1, activeChar, this, true, true, false, true, false);
					target.sendPacket(Msg.INSTANT_KILL);
					activeChar.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}
				else if(target.isNpc() && !target.isLethalImmune())
				{
					target.reduceCurrentHp(target.getCurrentHp() - 1, activeChar, this, true, true, false, true, false);
					activeChar.sendPacket(Msg.YOU_HAVE_BEEN_STRUCK_BY_THE_INSTANT_KILL_SKILL);
				}

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());

			if(_aborts.contains("attack"))
				activeChar.abortAttack();

			if(_aborts.contains("cast"))
				activeChar.abortCast();

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}

		if(isSSPossible())
			if(!(Config.SAVING_SPS && _skillType == SkillType.BUFF))
				activeChar.unChargeShots(isMagic());
	}
}
