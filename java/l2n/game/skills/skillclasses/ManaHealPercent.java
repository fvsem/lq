package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;

public class ManaHealPercent extends L2Skill
{
	public ManaHealPercent(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(final L2Character target : targets)
		{
			if(target != null)
			{
				if(target.isDead() || target.isHealMPBlocked(true))
					continue;

				getEffects(activeChar, target, getActivateRate() > 0, false);

				final double addToMp = Math.max(0, Math.min(target.getMaxMp() * _power / 100, target.calcStat(Stats.MP_LIMIT, 100, null, null) * target.getMaxMp() / 100. - target.getCurrentMp()));

				if(addToMp > 0)
					target.setCurrentMp(target.getCurrentMp() + addToMp);
				if(target.isPlayer())
					if(activeChar != target)
						target.sendPacket(new SystemMessage(SystemMessage.S1_MP_HAS_BEEN_RESTORED_BY_XS2S).addString(activeChar.getName()).addNumber(Math.round(addToMp)));
					else
						activeChar.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToMp)));
			}

			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
		}
	}
}
