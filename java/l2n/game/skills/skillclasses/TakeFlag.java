package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.model.instances.L2TerritoryFlagInstance;

/**
 * Для захвата флагов
 * 
 * @author L2System Project
 * @date 02.06.2010
 * @time 9:36:01
 */
public class TakeFlag extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!super.checkCondition(activeChar, target, forceUse, dontMove, first))
			return false;

		if(activeChar == null || !activeChar.isPlayer())
			return false;

		final L2Player player = (L2Player) activeChar;

		if(player.getClan() == null)
			return false;

		if(player.getTerritorySiege() < 0)
			return false;

		if(player.isMounted())
			return false;

		if(!(target instanceof L2SiegeHeadquarterInstance) || target.getNpcId() != 36590 || ((L2SiegeHeadquarterInstance) target).getClan() != player.getClan())
			return false;

		if(!player.isTerritoryFlagEquipped())
			return false;

		return true;
	}

	public TakeFlag(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null)
			{
				final L2Player player = (L2Player) activeChar;
				if(!(target instanceof L2SiegeHeadquarterInstance) || target.getNpcId() != 36590 || ((L2SiegeHeadquarterInstance) target).getClan() != player.getClan())
					continue;
				if(player.getTerritorySiege() > -1 && player.isTerritoryFlagEquipped())
				{
					final L2ItemInstance flag = player.getActiveWeaponInstance();
					if(flag != null && flag.getCustomType1() != 77) // 77 это эвентовый флаг
					{
						final L2TerritoryFlagInstance flagNpc = TerritorySiege.getNpcFlagByItemId(flag.getItemId());
						flagNpc.engrave(player);
					}
				}
			}
	}
}
