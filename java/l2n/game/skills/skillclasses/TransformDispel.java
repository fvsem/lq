package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

public class TransformDispel extends L2Skill
{
	public TransformDispel(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(activeChar == null || !activeChar.isPlayer())
			return;

		final L2Player player = (L2Player) activeChar;
		if(player.isDead() || player.isCursedWeaponEquipped() || player.getTransformationId() == 0)
		{
			player.sendActionFailed();
			return;
		}

		if(player.isInFlyingTransform() && !player.isInZone(ZoneType.landing))
		{
			player.sendPacket(Msg.BOARDING_OR_CANCELLATION_OF_BOARDING_ON_AIRSHIPS_IS_NOT_ALLOWED_IN_THE_CURRENT_AREA);
			player.sendActionFailed();
			return;
		}

		if(player.getTransformationId() != 0)
			player.stopTransformation();
	}
}
