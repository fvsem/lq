package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * @author L2System Project
 * @date 06.05.2011
 * @time 11:15:16
 */
public class Refill extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null || !target.isPlayer() || !target.isInVehicle() || !target.getPlayer().getVehicle().isAirShip())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addSkillName(_id, _level));
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public Refill(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
			if(target != null)
			{
				if(target.isDead() || !target.isPlayer() || !target.isInVehicle() || !target.getPlayer().getVehicle().isAirShip())
					continue;
				final L2AirShip airship = (L2AirShip) target.getPlayer().getVehicle();
				airship.setFuel(airship.getFuel() + (int) _power);
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
