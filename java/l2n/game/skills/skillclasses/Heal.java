package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2SiegeHeadquarterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;

public class Heal extends L2Skill
{
	private boolean isMastery = false;

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target instanceof L2DoorInstance || target instanceof L2SiegeHeadquarterInstance)
			return false;

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public Heal(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		// Надо уточнить формулу.
		double hp = _power;
		if(!isHandler() && !isAltUse())
		{
			hp += 0.1 * _power * Math.sqrt(activeChar.getMAtk(null, this) / 333);

			final int sps = isSSPossible() && getHpConsume() == 0 ? activeChar.getChargedSpiritShot() : 0;
			if(sps == 2)
				hp *= 1.5;
			else if(sps == 1)
				hp *= 1.3;

			if(activeChar.getSkillMastery(getId()) == 3)
			{
				activeChar.removeSkillMastery(getId());
				hp *= 3;
				isMastery = true;
			}
		}

		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(final L2Character target : targets)
			if(target != null)
			{
				if(target.isDead() || target.isHealHPBlocked(true))
					continue;

				// Player holding a cursed weapon can't be healed and can't heal
				if(target != activeChar)
					if(target.isPlayer() && target.isCursedWeaponEquipped())
						continue;
					else if(activeChar.isPlayer() && activeChar.isCursedWeaponEquipped())
						continue;

				double maxNewHp = hp;
				// на действие банок не должны влиять скилы, которые увеличивают и уменьшают силу хила
				if(!isHandler() && !isAltUse())
				{
					maxNewHp *= target.calcStat(Stats.HEAL_EFFECTIVNESS, 100, activeChar, this) / 100;
					maxNewHp = activeChar.calcStat(Stats.HEAL_POWER, maxNewHp, target, this);
				}
				double addToHp = Math.max(0, Math.min(maxNewHp, target.calcStat(Stats.HP_LIMIT, 100, null, null) * target.getMaxHp() / 100. - target.getCurrentHp()));

				// Heal critic, since CT2.3 Gracia Final (When superior healing occurs from normal heal skills, it now heals the target 3 times more than usual)
				if(!isHandler() && !isAltUse()) // скилы от банок не должны критовать
					if(!isMastery && Formulas.calcMCrit(activeChar.getCriticalMagic(target, this)))
					{
						activeChar.sendPacket(Msg.MAGIC_CRITICAL_HIT);
						addToHp *= 3;
					}

				if(addToHp > 0)
					target.setCurrentHp(addToHp + target.getCurrentHp(), false);
				if(getId() == 4051)
					target.sendPacket(Msg.REJUVENATING_HP);
				else if(target.isPlayer())
					if(activeChar == target)
						activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HPS_HAVE_BEEN_RESTORED).addNumber(Math.round(addToHp)));
					else
						// FIXME показывать ли лекарю, сколько он восстановил HP цели?
						target.sendPacket(new SystemMessage(SystemMessage.S1_HP_HAS_BEEN_RESTORED_BY_XS2S).addString(activeChar.getName()).addNumber(Math.round(addToHp)));
				else if(target.isSummon() || target.isPet())
				{
					final L2Player owner = target.getPlayer();
					if(owner != null)
						if(activeChar == target) // Пет лечит сам себя
							owner.sendMessage(new CustomMessage("YOU_HAVE_RESTORED_S1_HP_OF_YOUR_PET", owner).addNumber(Math.round(addToHp)));
						else if(owner == activeChar) // Хозяин лечит пета
							owner.sendMessage(new CustomMessage("YOU_HAVE_RESTORED_S1_HP_OF_YOUR_PET", owner).addNumber(Math.round(addToHp)));
						else
							// Пета лечит кто-то другой
							owner.sendMessage(new CustomMessage("S1_HAS_BEEN_RESTORED_S2_HP_OF_YOUR_PET", owner).addString(activeChar.getName()).addNumber(Math.round(addToHp)));
				}

				getEffects(activeChar, target, getActivateRate() > 0, false);

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
