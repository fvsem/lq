package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;

public class LethalShot extends L2Skill
{
	public LethalShot(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final boolean ss = activeChar.getChargedSoulShot() && isSSPossible();

		if(ss)
			activeChar.unChargeShots(false);

		L2Character realTarget;
		boolean reflected;

		for(L2Character target : targets)
		{
			if(target.isDead())
				continue;

			reflected = target.checkReflectSkill(activeChar, this);
			realTarget = reflected ? activeChar : target;

			if(getPower() > 0) // Если == 0 значит скилл "отключен"
			{
				AttackInfo info = Formulas.calcPhysDam(activeChar, realTarget, this, false, false, ss, false);

				if(info.lethal_dmg > 0)
					realTarget.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, false, false);

				realTarget.reduceCurrentHp(info.damage, activeChar, this, true, true, false, true, false);
				if(!reflected)
					Formulas.doCounterAttack(this, activeChar, realTarget, false);
			}

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
