package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Log;

public class Sweep extends L2Skill
{
	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null)
			return false;

		if(oneTarget())
		{
			if(!target.isMonster() || !((L2MonsterInstance) target).isDead() || ((L2MonsterInstance) target).isDying())
			{
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
				return false;
			}

			if(!((L2MonsterInstance) target).isSpoiled())
			{
				activeChar.sendPacket(Msg.SWEEPER_FAILED_TARGET_NOT_SPOILED);
				return false;
			}

			if(!((L2MonsterInstance) target).isSpoiled((L2Player) activeChar))
			{
				activeChar.sendPacket(Msg.THERE_ARE_NO_PRIORITY_RIGHTS_ON_A_SWEEPER);
				return false;
			}
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	public Sweep(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		final L2Player player = (L2Player) activeChar;

		for(final L2Character targ : targets)
		{
			if(!checkTarget(activeChar, targ))
				continue;

			final L2MonsterInstance target = (L2MonsterInstance) targ;

			final L2ItemInstance[] items = target.takeSweep();
			if(items == null)
			{
				activeChar.getAI().setAttackTarget(null);
				target.endDecayTask();
				continue;
			}

			target.setSpoiled(false, null);

			for(L2ItemInstance item : items)
			{
				if(player.isInParty() && player.getParty().isDistributeSpoilLoot())
				{
					player.getParty().distributeItem(player, item);
					continue;
				}

				final long itemCount = item.getCount();
				if(player.getInventoryLimit() <= player.getInventory().getSize() && (!item.isStackable() || player.getInventory().getItemByItemId(item.getItemId()) == null))
				{
					item.dropToTheGround(player, target);
					continue;
				}

				item = player.getInventory().addItem(item);
				Log.LogItem(player, target, Log.SweepItem, item);

				SystemMessage smsg;
				if(itemCount == 1)
				{
					smsg = new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S1);
					smsg.addItemName(item.getItemId());
					player.sendPacket(smsg);
				}
				else
				{
					smsg = new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S2_S1);
					smsg.addItemName(item.getItemId());
					smsg.addNumber(itemCount);
					player.sendPacket(smsg);
				}
				if(player.isInParty())
					if(itemCount == 1)
					{
						smsg = new SystemMessage(SystemMessage.S1_HAS_OBTAINED_S2_BY_USING_SWEEPER);
						smsg.addString(player.getName());
						smsg.addItemName(item.getItemId());
						player.getParty().broadcastToPartyMembers(player, smsg);
					}
					else
					{
						smsg = new SystemMessage(SystemMessage.S1_HAS_OBTAINED_3_S2_S_BY_USING_SWEEPER);
						smsg.addString(player.getName());
						smsg.addItemName(item.getItemId());
						smsg.addNumber(itemCount);
						player.getParty().broadcastToPartyMembers(player, smsg);
					}
			}
			activeChar.getAI().setAttackTarget(null);
			target.endDecayTask();
		}
	}

	private boolean checkTarget(final L2Character activeChar, final L2Character target)
	{
		if(target == null)
			return false;

		if(!target.isMonster())
			return false;

		if(!target.isDead() || ((L2MonsterInstance) target).isDying())
			return false;

		if(!((L2MonsterInstance) target).isSpoiled())
			return false;

		if(!((L2MonsterInstance) target).isSpoiled((L2Player) activeChar))
			return false;

		return true;
	}
}
