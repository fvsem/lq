package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.ai.CtrlIntention;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.FlyToLocation;
import l2n.game.network.serverpackets.FlyToLocation.FlyType;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

public class Rush extends L2Skill
{
	public Rush(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		for(final L2Character target : targets)
		{
			if(target == null || target.isDead())
				continue;

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}

		int x = 0;
		int y = 0;
		int z = 0;

		if(oneTarget())
			for(final L2Character target : targets)
			{

				if(target == null || target.isDead())
					continue;

				x = target.getX() + Rnd.get(5);
				y = target.getY() + Rnd.get(5);
				z = target.getZ();

				if(!GeoEngine.canMoveToCoord(activeChar.getX(), activeChar.getY(), activeChar.getZ(), x, y, z))
					return;

				break;
			}
		else
		{
			final int radius = getFlyRadius();
			final double angle = Util.convertHeadingToDegree(activeChar.getHeading());
			final double radian = Math.toRadians(angle - 90);
			final double sin = Math.sin(radian);
			final double cos = Math.cos(radian);
			final int x1 = -(int) (sin * radius);
			final int y1 = (int) (cos * radius);
			x = activeChar.getX() + x1;
			y = activeChar.getY() + y1;
			z = GeoEngine.getHeight(x, y, activeChar.getZ());
		}

		if(x == 0 || y == 0 || z == 0)
			return;

		final Location targetLoc = GeoEngine.moveCheck(activeChar.getX(), activeChar.getY(), activeChar.getZ(), x, y);

		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);

		activeChar.broadcastPacket(new FlyToLocation(activeChar, targetLoc, FlyType.CHARGE));

		activeChar.abortAttack();
		activeChar.abortCast();

		activeChar.setXYZ(targetLoc.x, targetLoc.y, targetLoc.z);
		activeChar.broadcastPacket(new ValidateLocation(activeChar));
	}
}
