package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;

public class SPHeal extends L2Skill
{
	public SPHeal(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());

		for(final L2Character target : targets)
		{
			target.getPlayer().addSp((long) _power);
			target.getPlayer().sendChanges();

			getEffects(activeChar, target, getActivateRate() > 0, false);

			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED__S1S2).addNumber((long) _power).addString("SP"));
		}
	}
}
