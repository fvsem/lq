package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Log;
import l2n.util.Rnd;

public class Harvesting extends L2Skill
{
	public Harvesting(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;

		final L2Player player = (L2Player) activeChar;

		for(final L2Character target : targets)
		{
			if(target == null || !target.isMonster())
				continue;

			final L2MonsterInstance monster = (L2MonsterInstance) target;

			// Не посеяно
			if(!monster.isSeeded())
			{
				activeChar.sendPacket(Msg.THE_HARVEST_FAILED_BECAUSE_THE_SEED_WAS_NOT_SOWN);
				continue;
			}

			if(!monster.isSeeded(player))
			{
				activeChar.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_HARVEST);
				continue;
			}

			double SuccessRate = Config.MANOR_HARVESTING_BASIC_SUCCESS;
			final int diffPlayerTarget = Math.abs(activeChar.getLevel() - monster.getLevel());

			// Штраф, на разницу уровней между мобом и игроком
			// 5% на каждый уровень при разнице >5 - по умолчанию
			if(diffPlayerTarget > Config.MANOR_DIFF_PLAYER_TARGET)
				SuccessRate -= (diffPlayerTarget - Config.MANOR_DIFF_PLAYER_TARGET) * Config.MANOR_DIFF_PLAYER_TARGET_PENALTY;

			// Минимальный шанс успеха всегда 1%
			if(SuccessRate < 1)
				SuccessRate = 1;

			if(Config.SKILLS_SHOW_CHANCE && !player.getVarB("SkillsHideChance"))
				player.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Harvesting.Chance", player).addNumber((int) SuccessRate));

			if(!Rnd.chance(SuccessRate))
			{
				activeChar.sendPacket(Msg.THE_HARVEST_HAS_FAILED);
				monster.takeHarvest();
				continue;
			}

			L2ItemInstance item = monster.takeHarvest();
			if(item == null)
			{
				_log.warning("Harvesting :: monster.takeHarvest() == null :: monster == " + monster);
				continue;
			}

			final long itemCount = item.getCount();
			item = player.getInventory().addItem(item);
			Log.LogItem(player, target, Log.HarvesterItem, item);
			player.sendPacket(new SystemMessage(SystemMessage.S1_HARVESTED_S3_S2_S).addString("You").addNumber(itemCount).addItemName(item.getItemId()));
			if(player.isInParty())
			{
				final SystemMessage smsg = new SystemMessage(SystemMessage.S1_HARVESTED_S3_S2_S).addString(player.getName()).addNumber(itemCount).addItemName(item.getItemId());
				player.getParty().broadcastToPartyMembers(player, smsg);
			}
		}
	}
}
