package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Character.HateInfo;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;

public class ShiftAggression extends L2Skill
{
	public ShiftAggression(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(activeChar.getPlayer() == null)
			return;

		final L2Playable playable = (L2Playable) activeChar;

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());

		for(final L2Character target : targets)
		{
			if(target == null || !target.isPlayer())
				continue;

			final L2Player player_target = (L2Player) target;

			for(final L2NpcInstance npc : L2World.getAroundNpc(activeChar, getSkillRadius(), 900))
			{
				final HateInfo hateInfo = playable.getHateList().get(npc);
				if(hateInfo == null || hateInfo.hate <= 0)
					continue;
				player_target.addDamageHate(npc, 0, hateInfo.hate + 100);
				hateInfo.hate = 0;
			}
		}
	}
}
