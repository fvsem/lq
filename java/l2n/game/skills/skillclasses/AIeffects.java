package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.templates.L2Weapon;

public class AIeffects extends L2Skill
{
	public AIeffects(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
		final L2Weapon weapon = activeChar.getActiveWeaponItem();
		for(final L2Character target : targets)
		{
			if(weapon != null)
				weapon.getEffect(false, activeChar, target, isOffensive());
			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
