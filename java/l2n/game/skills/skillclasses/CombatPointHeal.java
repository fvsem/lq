package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;

public class CombatPointHeal extends L2Skill
{
	public CombatPointHeal(final StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		final L2Weapon weapon = activeChar.getActiveWeaponItem();

		for(final L2Character target : targets)
			if(target != null)
			{
				if(target.isDead() || target.isHealHPBlocked(true))
					continue;

				final double maxNewCp = _power * target.calcStat(Stats.CPHEAL_EFFECTIVNESS, 100, activeChar, this) / 100;
				final double addToCp = Math.max(0, Math.min(maxNewCp, target.calcStat(Stats.CP_LIMIT, 100, null, null) * target.getMaxCp() / 100. - target.getCurrentCp()));
				if(addToCp > 0)
					target.setCurrentCp(addToCp + target.getCurrentCp());
				target.sendPacket(new SystemMessage(SystemMessage.S1_CPS_WILL_BE_RESTORED).addNumber((long) addToCp));

				if(weapon != null)
					weapon.getEffect(false, activeChar, target, isOffensive());

				getEffects(activeChar, target, getActivateRate() > 0, false);
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}
