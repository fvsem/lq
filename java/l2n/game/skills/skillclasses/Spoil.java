package l2n.game.skills.skillclasses;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.ai.CtrlEvent;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.skills.Formulas.AttackInfo;
import l2n.util.Rnd;

public class Spoil extends L2Skill
{
	public Spoil(final StatsSet set)
	{
		super(set);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(target == null)
			return false;

		if(!target.isMonster() && !target.isPlayable())
		{
			activeChar.sendPacket(Msg.IT_IS_A_CHARACTER_THAT_CANNOT_BE_SPOILED);
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		if(!activeChar.isPlayer())
			return;
		final int ss = isSSPossible() ? 0 : activeChar.getChargedSoulShot() ? 2 : isMagic() ? activeChar.getChargedSpiritShot() : 0;
		if(ss > 0 && getPower() > 0)
			activeChar.unChargeShots(false);

		for(final L2Character target : targets)
			if(target != null && !target.isDead())
			{
				if(target.isMonster())
					if(((L2MonsterInstance) target).isSpoiled())
						activeChar.sendPacket(Msg.ALREADY_SPOILED);
					else
					{
						final L2MonsterInstance monster = (L2MonsterInstance) target;

						boolean success;
						if(!Config.ALT_SPOIL_FORMULA)
						{
							final int monsterLevel = monster.getLevel();
							final int modifier = Math.abs(monsterLevel - activeChar.getLevel());
							double rateOfSpoil = Config.BASE_SPOIL_RATE;

							if(modifier > 8)
								rateOfSpoil -= rateOfSpoil * (modifier - 8) * 9 / 100;

							rateOfSpoil = rateOfSpoil * getMagicLevel() / monsterLevel;

							if(rateOfSpoil < Config.MINIMUM_SPOIL_RATE)
								rateOfSpoil = Config.MINIMUM_SPOIL_RATE;
							else if(rateOfSpoil > 99.)
								rateOfSpoil = 99.;

							activeChar.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Spoil.Chance", activeChar).addNumber((int) rateOfSpoil));
							success = Rnd.chance((int) rateOfSpoil);
						}
						else
							success = Formulas.calcSkillSuccess(activeChar, target, this, getActivateRate());

						if(success)
						{
							monster.setSpoiled(true, (L2Player) activeChar);
							activeChar.sendPacket(Msg.THE_SPOIL_CONDITION_HAS_BEEN_ACTIVATED);
						}
						else
							activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_FAILED).addSkillName(_id, getLevel()));
					}

				if(getPower() > 0)
				{
					double damage;
					if(isMagic())
						damage = Formulas.calcMagicDam(activeChar, target, this, ss);
					else
					{
						final AttackInfo info = Formulas.calcPhysDam(activeChar, target, this, false, false, ss > 0, false);
						damage = info.damage;

						if(info.lethal_dmg > 0)
							target.reduceCurrentHp(info.lethal_dmg, activeChar, this, true, true, false, false, false);
					}

					target.reduceCurrentHp(damage, activeChar, this, true, true, false, true, false);
					Formulas.doCounterAttack(this, activeChar, target, false);
				}

				getEffects(activeChar, target, getActivateRate() > 0, false);

				target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, activeChar, Math.max(_aggroPoints, 1));
			}
	}
}
