package l2n.game.skills.skillclasses;

import l2n.commons.util.StatsSet;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.ResidenceType;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.castle.CastleSiege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Formulas;
import l2n.game.tables.ReflectionTable;

public class Resurrect extends L2Skill
{
	private final boolean _canPet;

	public Resurrect(final StatsSet set)
	{
		super(set);
		_canPet = set.getBool("canPet", false);
	}

	@Override
	public boolean checkCondition(final L2Character activeChar, final L2Character target, final boolean forceUse, final boolean dontMove, final boolean first)
	{
		if(!activeChar.isPlayer())
			return false;

		if(target == null || target != activeChar && !target.isDead())
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		final L2Player player = (L2Player) activeChar;
		final L2Player pcTarget = target.getPlayer();

		if(pcTarget == null)
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		if(player.isInOlympiadMode() || pcTarget.isInOlympiadMode())
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return false;
		}

		if(TerritorySiege.checkIfInZone(target))
		{
			if(pcTarget.getTerritorySiege() == -1) // Не зарегистрирован на осаду
			{
				activeChar.sendPacket(Msg.IT_IS_IMPOSSIBLE_TO_BE_RESSURECTED_IN_BATTLEFIELDS_WHERE_SIEGE_WARS_ARE_IN_PROCESS);
				return false;
			}

			final L2Clan clan = pcTarget.getClan();
			final SiegeClan siegeClan = TerritorySiege.getSiegeClan(clan);
			if(siegeClan == null || siegeClan.getHeadquarter() == null) // Возможно, стоит разрешить воскрешаться одиночкам
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.IF_A_BASE_CAMP_DOES_NOT_EXIST_RESURRECTION_IS_NOT_POSSIBLE));
				return false;
			}
		}

		final Siege siege = SiegeManager.getSiege(target, true);
		if(siege != null)
		{
			final L2Clan clan = pcTarget.getClan();
			if(clan == null || clan.getSiege() == null || clan.getSiege() != siege)
			{
				activeChar.sendPacket(Msg.IT_IS_IMPOSSIBLE_TO_BE_RESSURECTED_IN_BATTLEFIELDS_WHERE_SIEGE_WARS_ARE_IN_PROCESS);
				return false;
			}

			final SiegeClan attackClan = siege.getAttackerClan(clan);
			if(attackClan != null && attackClan.getHeadquarter() == null)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.IF_A_BASE_CAMP_DOES_NOT_EXIST_RESURRECTION_IS_NOT_POSSIBLE));
				return false;
			}

			if(siege.checkIsDefender(clan, false) && siege.getSiegeUnit().getType() == ResidenceType.Castle && ((CastleSiege) siege).isAllTowersDead())
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.THE_GUARDIAN_TOWER_HAS_BEEN_DESTROYED_AND_RESURRECTION_IS_NOT_POSSIBLE));
				return false;
			}
		}

		if(oneTarget())
		{
			if(target == null || !target.isDead())
				return false;

			if(!target.isPlayable())
			{
				player.sendPacket(Msg.TARGET_IS_INCORRECT);
				return false;
			}

			if(target.isPet())
			{
				if(target.getPlayer() == null)
				{
					player.sendPacket(Msg.TARGET_IS_INCORRECT);
					return false;
				}
				if(target.getPlayer().isReviveRequested())
				{
					if(target.getPlayer().isRevivingPet())
						activeChar.sendPacket(Msg.BETTER_RESURRECTION_HAS_BEEN_ALREADY_PROPOSED);
					else
						activeChar.sendPacket(Msg.SINCE_THE_MASTER_WAS_IN_THE_PROCESS_OF_BEING_RESURRECTED_THE_ATTEMPT_TO_RESURRECT_THE_PET_HAS_BEEN_CANCELLED);
					return false;
				}
				if(!(_canPet || _targetType == SkillTargetType.TARGET_PET))
				{
					player.sendPacket(Msg.TARGET_IS_INCORRECT);
					return false;
				}
			}
			else if(target.isPlayer())
			{
				if(((L2Player) target).isReviveRequested())
				{
					if(((L2Player) target).isRevivingPet())
						activeChar.sendPacket(Msg.WHILE_A_PET_IS_ATTEMPTING_TO_RESURRECT_IT_CANNOT_HELP_IN_RESURRECTING_ITS_MASTER); // While a pet is attempting to resurrect, it cannot help in resurrecting its master.
					else
						activeChar.sendPacket(Msg.BETTER_RESURRECTION_HAS_BEEN_ALREADY_PROPOSED); // Resurrection is already been proposed.
					return false;
				}
				if(_targetType == SkillTargetType.TARGET_PET)
				{
					player.sendPacket(Msg.TARGET_IS_INCORRECT);
					return false;
				}
				// Check to see if the player is in a festival.
				if(((L2Player) target).isFestivalParticipant())
				{
					player.sendMessage(new CustomMessage("l2n.game.skills.skillclasses.Resurrect", player));
					return false;
				}
			}
		}

		// В Источнике Бессмертия (Heart of Infinity) невозможно воскрешаться обычным способом
		final int instanceId = pcTarget.getReflection().getInstancedZoneId();
		if(instanceId == ReflectionTable.SOI_HEART_OF_INFINITY_ATTACK || instanceId == ReflectionTable.SOI_HEART_OF_INFINITY_DEFENCE)
		{
			player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addSkillName(getId()));
			return false;
		}

		return super.checkCondition(activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(final L2Character activeChar, final L2Character... targets)
	{
		double percent = _power;
		if(percent < 100 && !isHandler())
		{
			final double wit_bonus = _power * (Formulas.WITbonus[activeChar.getWIT()] - 1.);
			percent += wit_bonus > 20 ? 20 : wit_bonus;
			if(percent > 90)
				percent = 90;
		}

		if(isSSPossible())
			activeChar.unChargeShots(true);

		for(final L2Character target : targets)
		{
			if(target.getPlayer() == null)
				continue;

			if(target.isPet() && _canPet)
			{
				if(target.getPlayer() == activeChar)
					((L2PetInstance) target).doRevive(percent);
				else
					target.getPlayer().reviveRequest((L2Player) activeChar, percent, true);
			}
			else if(target.isPlayer())
			{
				if(_targetType == SkillTargetType.TARGET_PET)
					continue;

				final L2Player targetPlayer = (L2Player) target;
				if(targetPlayer.isReviveRequested())
					continue;

				if(targetPlayer.isFestivalParticipant())
					continue;

				targetPlayer.reviveRequest((L2Player) activeChar, percent, false);
			}
			else
				continue;

			getEffects(activeChar, target, getActivateRate() > 0, false);
		}
	}
}
