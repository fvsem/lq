package l2n.game.skills;

import l2n.game.model.actor.L2Character;
import l2n.game.skills.funcs.Func;
import l2n.util.ArrayUtil;

import java.util.Arrays;

/**
 * A calculator is created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...).
 * In fact, each calculator is a table of Func object in which each Func represents a mathematic function : <BR>
 * <BR>
 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<BR>
 * <BR>
 * When the calc method of a calculator is launched, each mathematic function is called according to its priority <B>_order</B>.
 * Indeed, Func with lowest priority order is executed firsta and Funcs with the same order are executed in unspecified order.
 * The result of the calculation is stored in the value property of an Env class instance.<BR>
 * <BR>
 * Method addFunc and removeFunc permit to add and remove a Func object from a Calculator.<BR>
 * <BR>
 */
public final class Calculator
{
	/** Table of Func object */
	private Func[] _functions;

	private double _base;
	private double _last;

	public final Stats _stat;
	public final L2Character _character;

	/**
	 * Constructor of Calculator (Init value : emptyFuncs).
	 */
	public Calculator(final Stats stat, final L2Character character)
	{
		_stat = stat;
		_character = character;
		_functions = ArrayUtil.EMPTY_FUNCTION_SET;
	}

	/**
	 * Return the number of Funcs in the Calculator.<BR>
	 * <BR>
	 */
	public int size()
	{
		return _functions.length;
	}

	/**
	 * Add a Func to the Calculator.<BR>
	 * <BR>
	 */
	public void addFunc(final Func f)
	{
		// _functions = ArrayUtil.add(_functions, f);

		final Func[] tmp = new Func[_functions.length + 1];
		System.arraycopy(_functions, 0, tmp, 0, _functions.length);
		tmp[_functions.length] = f;
		_functions = tmp;

		Arrays.sort(_functions);
	}

	/**
	 * Remove a Func from the Calculator.<BR>
	 * <BR>
	 */
	public void removeFunc(final Func f)
	{
		_functions = ArrayUtil.remove(_functions, f);
		if(_functions.length == 0)
			_functions = ArrayUtil.EMPTY_FUNCTION_SET;
		else
			Arrays.sort(_functions);
	}

	/**
	 * Remove each Func with the specified owner of the Calculator.<BR>
	 * <BR>
	 */
	public void removeOwner(final Object owner)
	{
		final Func[] funcs = _functions;
		for(final Func element : funcs)
			if(element._funcOwner == owner)
				removeFunc(element);
	}

	/**
	 * Run each Func of the Calculator.<BR>
	 * <BR>
	 */
	public void calc(final Env env)
	{
		final Func[] funcs = _functions;
		_base = env.value;
		for(final Func func : funcs)
			if(func.getCondition() == null || func.getCondition().test(env))
				func.calc(env);

		if(env.value != _last)
		{
			final double last = _last; // TODO найти приминение в StatsChangeRecorder
			_last = env.value;
		}
	}

	/**
	 * Для отладки
	 */
	public Func[] getFunctions()
	{
		return _functions;
	}

	public double getBase()
	{
		return _base;
	}

	public double getLast()
	{
		return _last;
	}
}
