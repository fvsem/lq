package l2n.game.handler.usercommandhandlers;

import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.Calendar;

/**
 * Support for /mybirthday command
 * 
 * @author L2System Project
 * @date 28.04.2013
 * @time 17:35:12
 */
public class MyBirthday implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 126 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(COMMAND_IDS[0] != id)
			return false;

		if(activeChar.getCreateTime() == 0)
			return false;

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(activeChar.getCreateTime());

		SystemMessage sm = new SystemMessage(SystemMessage.C1_BIRTHDAY_IS_S3_S4_S2);
		sm.addString(activeChar.getName());
		sm.addString(Integer.toString(c.get(Calendar.YEAR)));
		sm.addString(Integer.toString(c.get(Calendar.MONTH) + 1));
		sm.addString(Integer.toString(c.get(Calendar.DATE)));

		activeChar.sendPacket(sm);
		return true;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
