package l2n.game.handler.usercommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;

/**
 * @author L2System Project
 * @date 23.06.2010
 * @time 9:49:33
 */
public class Mount implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 61 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(COMMAND_IDS[0] != id || activeChar == null)
			return false;

		L2Summon pet = activeChar.getPet();
		if(pet == null)
			return false;

		if(activeChar.getTransformationId() != 0)
			return false;
		if(pet.isMountable() && !activeChar.isMounted())
		{
			if(pet.isDead())
				activeChar.sendPacket(Msg.A_DEAD_STRIDER_CANNOT_BE_RIDDEN);
			else if(pet.isInCombat())
				activeChar.sendPacket(Msg.A_STRIDER_IN_BATTLE_CANNOT_BE_RIDDEN);
			else if(!activeChar.checksForMount(true, false))
				return false;
			else if(!pet.isDead() && !activeChar.isMounted())
			{
				activeChar.setMount(pet.getTemplate().npcId, pet.getObjectId());
				pet.unSummon();
				return true;
			}
		}
		else if(activeChar.isMounted())
		{
			if(activeChar.isFlying() && !activeChar.checkLandingState()) // Виверна
			{
				activeChar.sendActionFailed();
				activeChar.sendPacket(Msg.YOU_ARE_NOT_ALLOWED_TO_DISMOUNT_AT_THIS_LOCATION);
				return false;
			}
			activeChar.setMount(0, 0);
		}
		return false;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
