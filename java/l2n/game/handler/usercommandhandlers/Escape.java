package l2n.game.handler.usercommandhandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.StopMove;
import l2n.game.tables.SkillTable;

/**
 * Support for /unstuck command
 */
public class Escape implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 52 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(id != COMMAND_IDS[0])
			return false;

		if(activeChar.isMovementDisabled() || activeChar.isInOlympiadMode())
			return false;

		if(activeChar.getTeleMode() != 0 || activeChar.getUnstuck() != 0)
		{
			activeChar.sendMessage(new CustomMessage("common.TryLater", activeChar));
			return false;
		}

		if(activeChar.getDuel() != null)
		{
			activeChar.sendMessage(new CustomMessage("common.RecallInDuel", activeChar));
			return false;
		}

		activeChar.abortCast();
		activeChar.abortAttack();
		activeChar.sendActionFailed();
		activeChar.broadcastPacket(new StopMove(activeChar));

		L2Skill skill;
		if(activeChar.getPlayerAccess().FastUnstuck)
			skill = SkillTable.getInstance().getInfo(1050, 2);
		else
			skill = SkillTable.getInstance().getInfo(2099, 1);

		if(skill != null && skill.checkCondition(activeChar, activeChar, false, false, true))
			activeChar.getAI().Cast(skill, activeChar, false, true);

		return true;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
