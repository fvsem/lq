package l2n.game.handler.usercommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 23.06.2010
 * @time 9:49:38
 */
public class DisMount implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 62 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(COMMAND_IDS[0] != id)
			return false;

		if(activeChar == null)
			return false;

		if(activeChar.isMounted())
		{
			if(activeChar.isFlying() && !activeChar.checkLandingState()) // Виверна
			{
				activeChar.sendActionFailed();
				activeChar.sendPacket(Msg.YOU_ARE_NOT_ALLOWED_TO_DISMOUNT_AT_THIS_LOCATION);
				return false;
			}
			activeChar.setMount(0, 0);
			return true;
		}

		return true;

	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
