package l2n.game.handler.usercommandhandlers;

import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;

/**
 * Support for /resetname command
 * 
 * @author L2System Project
 * @date 02.12.2010
 * @time 17:29:44
 */
public class ResetName implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 117 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(id != COMMAND_IDS[0])
			return false;

		if(activeChar != null)
		{
			activeChar.setTitleColor(0xFFFF77);
			activeChar.sendChanges();
			return true;
		}
		return false;
	}

	@Override
	public int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
