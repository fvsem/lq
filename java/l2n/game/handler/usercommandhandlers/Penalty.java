package l2n.game.handler.usercommandhandlers;

import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

import java.text.SimpleDateFormat;

public class Penalty extends Functions implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 100 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(COMMAND_IDS[0] != id)
			return false;

		long _leaveclan = 0;
		if(activeChar.getLeaveClanTime() != 0)
			_leaveclan = activeChar.getLeaveClanTime() + 1 * 24 * 60 * 60 * 1000;
		long _deleteclan = 0;
		if(activeChar.getDeleteClanTime() != 0)
			_deleteclan = activeChar.getDeleteClanTime() + 10 * 24 * 60 * 60 * 1000;
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		String html = Files.read("data/html/command/penalty.htm", activeChar);


		if(activeChar.getClanId() == 0)
		{
			if(_leaveclan == 0 && _deleteclan == 0)
			{
				html = html.replaceFirst("%reason%", "Штрафа нет.");
				html = html.replaceFirst("%expiration%", " ");
			}
			else if(_leaveclan > 0 && _deleteclan == 0)
			{
				html = html.replaceFirst("%reason%", "Штраф за выход с клана.");
				html = html.replaceFirst("%expiration%", format.format(_leaveclan));
			}
			else if(_deleteclan > 0)
			{
				html = html.replaceFirst("%reason%", "Штраф после роспуска клана.");
				html = html.replaceFirst("%expiration%", format.format(_deleteclan));
			}
		}
		else if(activeChar.getClan().canInvite())
		{
			html = html.replaceFirst("%reason%", "Штрафа нет.");
			html = html.replaceFirst("%expiration%", " ");
		}
		else
		{
			html = html.replaceFirst("%reason%", "Штраф за изгнание члена клана.");
			html = html.replaceFirst("%expiration%", format.format(activeChar.getClan().getExpelledMemberTime()));
		}
		show(html, activeChar);
		return false;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
