package l2n.game.handler.usercommandhandlers;

import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;

/**
 * Support for /instancezone command
 * 
 * @author minlexx
 */
public class InstanceZone implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS = { 114 };

	@Override
	public boolean useUserCommand(int id, L2Player activeChar)
	{
		if(COMMAND_IDS[0] != id)
			return false;

		// current instance zone (if any)
		long refId = activeChar.getReflection().getId();
		if(refId > 0)
		{
			Reflection r = ReflectionTable.getInstance().get(refId);
			if(r != null)
				activeChar.sendPacket(new SystemMessage(SystemMessage.INSTANT_ZONE_CURRENTLY_INUSE).addString(r.getName()));
			else
				System.out.printf("Reflection for id [%d] is null!\n", refId);
		}

		// таймауты повторного использования всех инстанс зон для чара (как у сф в InstanceManager)
		activeChar.sendPacket(new SystemMessage(SystemMessage.INSTANCE_ZONE_TIME_LIMIT));
		int limit;
		boolean noLimit = true;
		InstanceManager ilm = InstanceManager.getInstance();
		for(String name : ilm.getNames())
		{
			limit = ilm.getTimeToNextEnterInstance(name, activeChar);
			if(limit > 0)
			{
				noLimit = false;
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_WILL_BE_AVAILABLE_FOR_REUSE_AFTER_S2_HOURS_S3_MINUTES).addString(name).addNumber(limit / 60).addNumber(limit % 60));
			}
		}
		if(noLimit)
			activeChar.sendPacket(new SystemMessage(SystemMessage.THERE_IS_NO_INSTANCE_ZONE_UNDER_A_TIME_LIMIT));

		return true;
	}

	@Override
	public final int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
