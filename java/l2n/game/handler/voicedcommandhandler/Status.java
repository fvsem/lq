package l2n.game.handler.voicedcommandhandler;

import javolution.text.TextBuilder;
import l2n.extensions.Stat;
import l2n.extensions.scripts.Functions;
import l2n.game.GameServer;
import l2n.game.Shutdown;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.taskmanager.MemoryWatchDog;
import l2n.util.Util;

public class Status extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "status" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	public String uptime()
	{
		int uptime = GameServer.uptime();

		int numDays = uptime / 86400;
		uptime -= numDays * 86400;
		int numHours = uptime / 3600;
		uptime -= numHours * 3600;
		int numMins = uptime / 60;
		uptime -= numMins * 60;
		int numSeconds = uptime;

		return new String(numDays + " дней " + numHours + " часов " + numMins + " минут " + numSeconds + " секунд");
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.equals("status"))
		{
			TextBuilder ret = TextBuilder.newInstance();
			boolean en = activeChar.getVar("lang@").equalsIgnoreCase("en");
			if(en)
			{
				ret.append("<center><font color=\"LEVEL\">Server status:</font></center>");
				ret.append("<br>Uptime: ").append(uptime());
				ret.append("<br1>Total online: ").append(Stat.getOnline(true));
			}
			else
			{
				ret.append("<center><font color=\"LEVEL\">Статус сервера:</font></center>");
				ret.append("<br>Работает: ").append(uptime());
				ret.append("<br1>Онлайн сервера: ").append(Stat.getOnline(true));

			}
			if(activeChar.getPlayerAccess().CanRestart)
			{
				if(en)
				{
					ret.append("<br1>Offline Trade: ").append(L2ObjectsStorage.getAllOfflineCount());
				}
				else
				{
					ret.append("<br1>Оффлайн Торг: ").append(L2ObjectsStorage.getAllOfflineCount());
				}

				ret.append("<br>Оперативная память: <font color=\"LEVEL\">").append(MemoryWatchDog.getMemUsedPerc()).append("</font>");
				ret.append("<br1>Свободная оперативная память: <font color=\"LEVEL\">").append(MemoryWatchDog.getMemFreeMb()).append("</font>");
				ret.append("<br1>Ипользуемая оперативная память: <font color=\"LEVEL\">").append(MemoryWatchDog.getMemUsedMb()).append("</font>");
				ret.append("<br1>Максимум оперативной памяти: <font color=\"LEVEL\">").append(MemoryWatchDog.getMemMaxMb()).append("</font>");
				}
				
			int mtc = Shutdown.getInstance().getSeconds();
			if(mtc > 0)
			{
				if(en)
					ret.append("<br1>Time to restart: ");
				else
					ret.append("<br1>До рестарта осталось: ");
				ret.append(Util.formatTime(mtc));
			}
			else
				ret.append("<br1>Перезагрузка сервера не запущена.");

			ret.append("<br><center><button value=\"");
			if(en)
				ret.append("Refresh");
			else
				ret.append("Обновить");
			ret.append("\" action=\"bypass -h user_status\" width=100 height=25 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\" /></center>");

			show(ret.toString(), activeChar);
			TextBuilder.recycle(ret);
			return true;
		}
		
		return false;
	}
}