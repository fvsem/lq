package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;

public class AutoLoot extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "autolooton", "autolootoff" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String args)
	{
		if(!Config.COMMAND_ALLOW_AUTOLOOT_COMMAND)
		{
			activeChar.sendMessage("Эта команда выключена");
			return false;
		}
		command = command.intern();
		if(command.equalsIgnoreCase("autolooton"))
		{
			activeChar.setAutoLoot(true);
			activeChar.sendMessage("Автолут Включен.");
			return true;
		}
		if(command.equalsIgnoreCase("autolootoff"))
		{
			activeChar.setAutoLoot(false);
			activeChar.sendMessage("Автолут Выключен.");
			return true;
		}
		return false;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
