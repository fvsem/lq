package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.commons.text.PrintfFormat;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

import java.util.Date;

public class Lang extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "lang", "cfg" };

	public static final PrintfFormat cfg_row = new PrintfFormat("<table><tr><td width=5></td><td width=120>%s:</td><td width=100>%s</td></tr></table>");
	public static final PrintfFormat cfg_button = new PrintfFormat("<button width=%d height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h user_cfg %s\" value=\"%s\">");

	public static final String OFF = "<font color=\"ff0000\">Выкл</font>";
	public static final String ON = "<font color=\"00ff00\">Вкл</font>";

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String args)
	{
		if(command.equals("lang"))
		{
			if(args != null)
				if(args.equalsIgnoreCase("en"))
					activeChar.setVar("lang@", "en");
				else if(args.equalsIgnoreCase("ru"))
					activeChar.setVar("lang@", "ru");
		}
		else if(command.equals("cfg"))
			if(args != null)
			{
				String[] param = args.split(" ");
				if(param.length == 2)
				{
					if(param[0].equalsIgnoreCase("dli"))
						if(param[1].equalsIgnoreCase("on"))
							activeChar.setVar("DroplistIcons", "1");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("DroplistIcons");

					if(param[0].equalsIgnoreCase("ssc"))
						if(param[1].equalsIgnoreCase("of") && Config.SKILLS_SHOW_CHANCE)
							activeChar.setVar("SkillsHideChance", "1");
						else if(param[1].equalsIgnoreCase("on"))
							activeChar.unsetVar("SkillsHideChance");

					if(param[0].equalsIgnoreCase("SkillsMobChance"))
						if(param[1].equalsIgnoreCase("on") && Config.SKILLS_SHOW_CHANCE)
							activeChar.setVar("SkillsMobChance", "1");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("SkillsMobChance");

					if(param[0].equalsIgnoreCase("noe"))
						if(param[1].equalsIgnoreCase("on"))
							activeChar.setVar("NoExp", "1");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("NoExp");

					if(param[0].equalsIgnoreCase("notraders"))
						if(param[1].equalsIgnoreCase("on"))
							activeChar.setVar("notraders", "1");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("notraders");

					if(param[0].equalsIgnoreCase("notShowBuffAnim"))
						if(param[1].equalsIgnoreCase("on"))
						{
							activeChar.setNotShowBuffAnim(true);
							activeChar.setVar("notShowBuffAnim", "1");
						}
						else if(param[1].equalsIgnoreCase("of"))
						{
							activeChar.setNotShowBuffAnim(false);
							activeChar.unsetVar("notShowBuffAnim");
						}

					if(param[0].equalsIgnoreCase("noShift"))
						if(param[1].equalsIgnoreCase("on"))
							activeChar.setVar("noShift", "1");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("noShift");

					if(param[0].equalsIgnoreCase("translit"))
						if(param[1].equalsIgnoreCase("on"))
							activeChar.setVar("translit", "tl");
						else if(param[1].equalsIgnoreCase("la"))
							activeChar.setVar("translit", "tc");
						else if(param[1].equalsIgnoreCase("of"))
							activeChar.unsetVar("translit");

				}
			}

		String dialog = Files.read("data/html/command/lang.htm", activeChar);

		dialog = dialog.replaceFirst("%lang%", activeChar.getVar("lang@").toUpperCase());
		dialog = dialog.replaceFirst("%autoloot%", activeChar.getVarB("@autoloot") ? ON : OFF);
		dialog = dialog.replaceFirst("%dli%", activeChar.getVarB("DroplistIcons") ? ON : OFF);
		dialog = dialog.replaceFirst("%noe%", activeChar.getVarB("NoExp") ? ON : OFF);
		dialog = dialog.replaceFirst("%notraders%", activeChar.getVarB("notraders") ? ON : OFF);
		dialog = dialog.replaceFirst("%notShowBuffAnim%", activeChar.getVarB("notShowBuffAnim") ? ON : OFF);
		dialog = dialog.replaceFirst("%noShift%", activeChar.getVarB("noShift") ? ON : OFF);
		dialog = dialog.replaceFirst("%repair%", "<font color=\"LEVEL\">Вкл</font>");
		dialog = dialog.replaceFirst("%whoami%", "<font color=\"LEVEL\">Вкл</font>");
		dialog = dialog.replaceFirst("%lock%", "<font color=\"LEVEL\">Вкл</font>");
		dialog = dialog.replaceFirst("%pass%", "<font color=\"LEVEL\">Вкл</font>");
	    dialog = dialog.replaceFirst("%cwh%", activeChar.getVarB("cwh") ? ON : OFF);
		


		if(!Config.SKILLS_SHOW_CHANCE)
		{
			dialog = dialog.replaceFirst("%ssc%", "<font color=\"LEVEL\">N/A</font>");
			dialog = dialog.replaceFirst("%SkillsMobChance%", "<font color=\"LEVEL\">N/A</font>");
		}
		else
		{
			if(!activeChar.getVarB("SkillsHideChance"))
				dialog = dialog.replaceFirst("%ssc%", ON);
			else
				dialog = dialog.replaceFirst("%ssc%", OFF);

			if(activeChar.getVarB("SkillsMobChance"))
				dialog = dialog.replaceFirst("%SkillsMobChance%", ON);
			else
				dialog = dialog.replaceFirst("%SkillsMobChance%", OFF);
		}

		String tl = activeChar.getVar("translit");
		if(tl == null)
			dialog = dialog.replaceFirst("%translit%", OFF);
		else if(tl.equals("tl"))
			dialog = dialog.replaceFirst("%translit%", ON);
		else
			dialog = dialog.replaceFirst("%translit%", "Lt");
        if(activeChar.getNetConnection().getBonus() > 1)
        {
            long endtime = activeChar.getNetConnection().getBonusExpire();
            if(endtime > 0)
                dialog = dialog.replaceFirst("%endtime%", new Date(endtime * 1000).toString());
        }
        else
            dialog = dialog.replaceFirst("%endtime%", "<a action=\"bypass -h scripts_services.RateBonus.RateBonus:list\">Купить ПА</a>");
		show(dialog, activeChar);

		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
