package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;

public class Buffer extends Functions implements IVoicedCommandHandler
{
	/** Список баффов для магов */
	private static final int[][] _mbuffs = {
			// id, lvl
			{ 7059, 1 }, // Wild Magic
			{ 4356, 3 }, // Empower
			{ 4355, 3 }, // Acumen
	};

	/** Список баффов для войнов */
	private static final int[][] _fbuffs = {
			// id, lvl
			{ 7057, 1 }, // Greater Might
			{ 4345, 3 }, // Might
			{ 4344, 3 }, // Shield
	};

	private static final String[] _commandList = new String[] { "mbuff", "fbuff", "sbuff" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String args)
	{
		if(!Config.COMMAND_ALLOW_BUFF_COMMAND)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.TurnOff", activeChar));
			return false;
		}

		if(command.equalsIgnoreCase("mbuff"))
		{
			if(!checkCondition(activeChar, Config.COMMAND_BUFF_PRICE_MBUFF_ITEM_ID, Config.COMMAND_BUFF_PRICE_MBUFF))
				return false;
			else
			{
				removeItem(activeChar, Config.COMMAND_BUFF_PRICE_MBUFF_ITEM_ID, Config.COMMAND_BUFF_PRICE_MBUFF);
				buff(_mbuffs, activeChar);
			}
			return true;
		}
		if(command.equalsIgnoreCase("fbuff"))
		{
			if(!checkCondition(activeChar, Config.COMMAND_BUFF_PRICE_FBUFF_ITEM_ID, Config.COMMAND_BUFF_PRICE_FBUFF))
				return false;
			else
			{
				removeItem(activeChar, Config.COMMAND_BUFF_PRICE_FBUFF_ITEM_ID, Config.COMMAND_BUFF_PRICE_FBUFF);
				buff(_fbuffs, activeChar);
			}
			return true;
		}
		if(command.equalsIgnoreCase("sbuff"))
		{
			if(activeChar == null)
				return false;
			activeChar.sendMessage("The command sbuff is not implemented yet.");
			return true;
		}
		return false;
	}

	private void buff(int[][] buffs, L2Player player)
	{
		L2Skill skill;

		for(int buff[] : buffs)
		{
			if(player == null)
				return;

			skill = SkillTable.getInstance().getInfo(buff[0], buff[1]);
			if(skill != null)
			{
				player.broadcastPacket(new MagicSkillUse(player, player, skill.getId(), skill.getLevel(), 0, 0));
				player.callSkill(skill, false, player);
			}
		}
	}

	/**
	 * Проверяет возможность бафа персонажа.<BR>
	 * 
	 * @param player
	 *            персонаж
	 * @return true, если можно бафать персонажа
	 */
	public static boolean checkCondition(L2Player player, int itemId, int price)
	{
		if(player == null)
			return false;

		if(!Config.COMMAND_ALLOW_BUFF_COMMAND_OLY && (player.getOlympiadGameId() > -1 || player.isInOlympiadMode() || Olympiad.isRegisteredInComp(player)))
			return false;

		if(!Config.COMMAND_ALLOW_BUFF_COMMAND_SIEGE && player.isInZoneSiege())
			return false;

		// Получаем итем из инвенторя
		L2ItemInstance item = player.getInventory().findItemByItemId(itemId);

		// Если такого итема вообще нет...
		if(item == null)
		{
			player.sendPacket(new SystemMessage(SystemMessage.NOT_ENOUGH_MATERIALS));
			return false;
		}

		if(player.isActionsDisabled() || player.isSitting())
			return false;

		// Если количество итемов в инвентаре меньше цены...
		if(price != 0 && item.getCount() < price)
		{
			player.sendPacket(new SystemMessage(SystemMessage.NOT_ENOUGH_MATERIALS));
			return false;
		}

		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
