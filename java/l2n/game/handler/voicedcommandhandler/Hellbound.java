package l2n.game.handler.voicedcommandhandler;

import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.actor.L2Player;

public class Hellbound extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "hellbound" };

	@Override
	public boolean useVoicedCommand(final String command, final L2Player activeChar, final String target)
	{
		if(command.startsWith("hellbound") || command.equalsIgnoreCase("hellbound"))
		{
			final boolean _isOpen = ServerVariables.getBool("HellboundOpen", false);
			final int hellboundTrust = ServerVariables.getInt("HellboundConfidence", 0);
			activeChar.sendMessage("- Информация о Хелбаунд -");
			activeChar.sendMessage("Количество очей: " + hellboundTrust + "\nStatus: " + (_isOpen ? "Открыт" : "Закрыт"));
		}
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

}
