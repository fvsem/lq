package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.LockAccountHWID;
import l2n.game.loginservercon.send.LockAccountIP;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

public class Lock implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "lock" };

	private static final String defaultPage = "data/html/command/lock.html";

	public static final String[] NOT_ALLOW = new String[] { "<font color=\"ff0000\">Запрещено</font>", "<font color=\"ff0000\">Forbidden</font>" };
	public static final String[] ALLOW = new String[] { "<font color=\"00ff00\">Разрешено</font>", "<font color=\"00ff00\">Allow</font>" };

	private void showDefaultPage(L2Player activeChar)
	{
		if(!activeChar.getPassCheck())
        {
            return;
        }
		String html = Files.read(defaultPage, activeChar);
		html = html.replaceFirst("%ip_block%", activeChar.isLangRus() ? getIPLockStatus()[0] : getIPLockStatus()[1]);
		html = html.replaceFirst("%hwid_block%", activeChar.isLangRus() ? getHWIDLockStatus(activeChar)[0] : getHWIDLockStatus(activeChar)[1]);
		Functions.show(html, activeChar);
	}

	private String[] getIPLockStatus()
	{
		return Config.SERVICES_LOCK_ACCOUNT_IP ? ALLOW : NOT_ALLOW;
	}

	private String[] getHWIDLockStatus(L2Player activeChar)
	{
		return Config.SERVICES_LOCK_ACCOUNT_HWID && activeChar.hasHWID() ? ALLOW : NOT_ALLOW;
	}

	/** блокировка по IP */
	private void lock_ip(L2Player activeChar)
	{
		LSConnection.getInstance().sendPacket(new LockAccountIP(activeChar.getAccountName(), activeChar.getIP()));
		String html = Files.read("data/html/command/lock_ip.htm", activeChar);
		html = html.replaceFirst("%curIP%", activeChar.getIP());
		Functions.show(html, activeChar);
	}

	/** блокировка по HWID */
	private void lock_hwid(L2Player activeChar)
	{
		LSConnection.getInstance().sendPacket(new LockAccountHWID(activeChar.getAccountName(), activeChar.getHWID().Full));
		String html = Files.read("data/html/command/lock_hwid.htm", activeChar);
		Functions.show(html, activeChar);
	}

	/** снятие всех блокировок */
	private void unlock(L2Player activeChar)
	{
		LSConnection.getInstance().sendPacket(new LockAccountIP(activeChar.getAccountName(), "*"));
		LSConnection.getInstance().sendPacket(new LockAccountHWID(activeChar.getAccountName(), "*"));
		String html = Files.read("data/html/command/lock_unlock.htm", activeChar);
		Functions.show(html, activeChar);
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(!Config.SERVICES_LOCK_ACCOUNT)
		{
			Functions.show(new CustomMessage("scripts.services.TurnOff", activeChar), activeChar);
			return true;
		}

		// а то могут снять блокировку
		if(activeChar.isOutOfControl())
		{
			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}

		if(command.equals("lock") && (target == null || target.equals("")))
		{
			showDefaultPage(activeChar);
			return true;
		}
		else if(target.startsWith("lock_type"))
		{
			String lock_type = null;
			if(target.contains("="))
				lock_type = target.substring(target.indexOf("=") + 1, target.length());

			if(lock_type == null)
			{
				activeChar.sendMessage(new CustomMessage("common.Error", activeChar));
				return true;
			}
			lock_type = lock_type.trim();
			if(lock_type.equalsIgnoreCase("IP Address") && Config.SERVICES_LOCK_ACCOUNT_IP)
			{
				lock_ip(activeChar);
				return true;
			}
			else if(lock_type.equalsIgnoreCase("Hardware ID") && Config.SERVICES_LOCK_ACCOUNT_HWID && activeChar.hasHWID())
			{
				lock_hwid(activeChar);
				return true;
			}

			activeChar.sendMessage(new CustomMessage("common.notAvailable", activeChar));
			return false;
		}
		else if(target.equalsIgnoreCase("unlock"))
		{
			unlock(activeChar);
			return true;
		}

		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
