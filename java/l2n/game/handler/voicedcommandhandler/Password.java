package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangePassword;
import l2n.game.model.actor.L2Player;
import l2n.util.Util;

public class Password extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "password" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String args)
	{
		if(!activeChar.getPassCheck())
        {
            return false;
        }
		if(command.equals("password") && (args == null || args.equals("")))
		{
			if(Config.SERVICES_CHANGE_PASSWORD)
				show("data/html/command/password.html", activeChar);
			else
				show("data/html/command/nopassword.html", activeChar);
			return true;
		}

		String[] parts = args.split(" ");

		if(parts.length != 3)
		{
			show(new CustomMessage("scripts.commands.user.password.IncorrectValues", activeChar), activeChar);
			return false;
		}

		if(!parts[1].equals(parts[2]))
		{
			show(new CustomMessage("scripts.commands.user.password.IncorrectConfirmation", activeChar), activeChar);
			return false;
		}

		if(parts[1].equals(parts[0]))
		{
			show(new CustomMessage("scripts.commands.user.password.NewPassIsOldPass", activeChar), activeChar);
			return false;
		}

		if(parts[1].length() < 5 || parts[1].length() > 20)
		{
			show(new CustomMessage("scripts.commands.user.password.IncorrectSize", activeChar), activeChar);
			return false;
		}

		if(!Util.isMatchingRegexp(parts[1], "[A-Za-z0-9]{4,16}"))
		{
			show(new CustomMessage("scripts.commands.user.password.IncorrectInput", activeChar), activeChar);
			return false;
		}

		LSConnection.getInstance().sendPacket(new ChangePassword(activeChar.getAccountName(), parts[0], parts[1]));
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
