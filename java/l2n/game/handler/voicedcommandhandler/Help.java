package l2n.game.handler.voicedcommandhandler;

import l2n.extensions.scripts.Functions;
import l2n.game.handler.admincommandhandlers.AdminEditChar;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.quest.Quest;
import l2n.game.network.serverpackets.RadarControl;
import l2n.game.skills.Formulas;
import l2n.game.skills.Stats;
import l2n.game.tables.DynamicRateTable;
import l2n.util.Files;

import java.text.NumberFormat;
import java.util.Locale;

public class Help extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[]
	{
			"help",
			"whoami",
			"whoiam",
			"heading",
			"whofake",
			"sweep",
			"pflag",
			"cflag",
			"exp"
	};

	private static final NumberFormat df = NumberFormat.getNumberInstance(Locale.ENGLISH);
	static
	{
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(1);
	}

	@Override
	public boolean useVoicedCommand(String command, final L2Player activeChar, final String args)
	{
		command = command.intern();
		if(command.equalsIgnoreCase("help"))
		{
			final String dialog = Files.read("data/html/command/help.htm", activeChar);
			
			show(dialog, activeChar);
			return true;
		}
		if(command.equalsIgnoreCase("whoami") || command.equalsIgnoreCase("whoiam"))
		{
			showInfo(activeChar);
			return true;
		}
		if(command.equalsIgnoreCase("heading"))
		{
			activeChar.sendMessage(String.valueOf(activeChar.getHeading()));
			return true;
		}
		if(command.equalsIgnoreCase("sweep"))
			return sweep(command, activeChar, args);
		if(command.equalsIgnoreCase("pflag"))
			return pflag(command, activeChar, args);
		if(command.equalsIgnoreCase("cflag"))
			return cflag(command, activeChar, args);
		if(command.equalsIgnoreCase("exp"))
			return exp(command, activeChar, args);

		return false;
	}

	private boolean exp(final String command, final L2Player activeChar, final String args)
	{
		if(activeChar.getLevel() >= (activeChar.isSubClassActive() ? Experience.getMaxSubLevel() : Experience.getMaxLevel()))
			show("Maximum level!", activeChar);
		else
		{
			final long exp = Experience.LEVEL[activeChar.getLevel() + 1] - activeChar.getExp();
			double count = 0;
			if(args != null && !args.isEmpty())
			{
				final String[] param = args.split(" ");
				if(Quest.isdigit(param[0]))
					count = exp / Integer.parseInt(param[0]);
			}
			String ret = "Exp left: " + exp;
			if(count > 0)
				ret += "<br>Monsters left: " + df.format(count);
			show(ret, activeChar);
		}
		return true;
	}

	private boolean pflag(final String command, final L2Player activeChar, final String args)
	{
		if(!activeChar.isInParty())
			return false;
		final RadarControl rc = new RadarControl(0, 1, activeChar.getLoc());
		for(final L2Player p : activeChar.getParty().getPartyMembers())
			if(p != activeChar)
				p.sendPacket(rc);
		return true;
	}

	private boolean cflag(final String command, final L2Player activeChar, final String args)
	{
		if(activeChar.getClan() == null)
			return false;
		final RadarControl rc = new RadarControl(0, 1, activeChar.getLoc());
		for(final L2Player p : activeChar.getClan().getOnlineMembers(activeChar.getName()))
			p.sendPacket(rc);
		return true;
	}


	private boolean sweep(final String command, final L2Player activeChar, final String args)
	{
		if(activeChar.getSkillLevel(42) > 0)
			for(final L2Character target : activeChar.getAroundCharacters(300, 200))
				if(target.isMonster() && target.isDead() && ((L2MonsterInstance) target).isSweepActive())
				{
					activeChar.getAI().Cast(activeChar.getKnownSkill(42), target);
					return true;
				}
		return false;
	}

	public static void showInfo(final L2Player cha)
	{
		if(cha == null)
			return;

		String content = Files.read("data/html/command/whoiam.htm");

		content = content.replaceFirst("%regen_hp%", df.format(Formulas.calcHpRegen(cha)));
		content = content.replaceFirst("%regen_cp%", df.format(Formulas.calcCpRegen(cha)));
		content = content.replaceFirst("%regen_mp%", df.format(Formulas.calcMpRegen(cha)));

		content = content.replaceFirst("%drain_hp%", df.format(cha.calcStat(Stats.ABSORB_DAMAGE_PERCENT, 0, null, null)));
		content = content.replaceFirst("%gain_hp%", df.format(cha.calcStat(Stats.HEAL_EFFECTIVNESS, 100, null, null)));
		content = content.replaceFirst("%gain_mp%", df.format(cha.calcStat(Stats.MANAHEAL_EFFECTIVNESS, 100, null, null)));

		content = content.replaceFirst("%crit_dmg%", df.format(2 * cha.calcStat(Stats.CRITICAL_DAMAGE, 100, null, null)));
		content = content.replaceFirst("%crit_dmg_static%", Integer.toString((int) cha.calcStat(Stats.CRITICAL_DAMAGE_STATIC, 0, null, null)));

		content = content.replaceFirst("%magic_crit%", df.format(cha.getCriticalMagic(null, null)));

		content = content.replaceFirst("%limit_buff%", Integer.toString(cha.getBuffLimit()));
		content = content.replaceFirst("%limit_music%", Integer.toString(cha.getDanceSongLimit()));
		content = content.replaceFirst("%blow_rate%", df.format(cha.calcStat(Stats.BLOW_RATE, 1, null, null)));
		content = content.replaceFirst("%pole_count%", Integer.toString((int) Math.round(cha.calcStat(Stats.POLE_TARGERT_COUNT, 3, null, null))));
		content = content.replaceFirst("%range_magic%", Integer.toString(cha.getMagicalAttackRange(null)));
		content = content.replaceFirst("%range_phys%", Integer.toString(cha.getPhysicalAttackRange()));
		content = content.replaceFirst("%max_load%", Integer.toString(cha.getMaxLoad()));
		content = content.replaceFirst("%expertise%", Integer.toString((int) cha.calcStat(Stats.GRADE_EXPERTISE_LEVEL, cha.getLevel(), null, null)));
		content = content.replaceFirst("%dps%", Integer.toString((int) calcDPS(cha)));

		// рейты
		content = content.replaceFirst("%rate_spoil%", Double.toString(cha.getRateSpoil() * DynamicRateTable.getRateSpoil(cha)));
		content = content.replaceFirst("%rate_items%", Double.toString(cha.getRateItems() * DynamicRateTable.getRateItems(cha)));
		content = content.replaceFirst("%rate_adena%", Double.toString(cha.getRateAdena() * DynamicRateTable.getRateAdena(cha)));
		content = content.replaceFirst("%rate_exp%", Double.toString(cha.getRateExp()));
		content = content.replaceFirst("%rate_sp%", Double.toString(cha.getRateSp()));

		// Различные резисты
		content = content.replaceFirst("%RESISTS%", AdminEditChar.getResist(cha));
		// Бонусы элементальной атаки
		content = content.replaceFirst("%ELEMENT_ATTACK%", AdminEditChar.getAttackElement(cha));
		// Бонусы атаки
		content = content.replaceFirst("%ATTACK_BONUS%", AdminEditChar.getAttackBonus(cha));
		show(content, cha);
	}

	private static double calcDPS(final L2Character self)
	{
		final L2Character target = null;
		final double def = 500;
		final double critRate = self.getCriticalHit(target, null) / 1000.;
		final double critPower = 2. * self.calcStat(Stats.CRITICAL_DAMAGE, 100, target, null) / 100;
		final double critStatic = self.calcStat(Stats.CRITICAL_DAMAGE_STATIC, 0, target, null);
		final double aSpd = self.getPAtkSpd();
		final double reuseDelay = self.getActiveWeaponItem() != null && self.getActiveWeaponItem().getAttackReuseDelay() > 0 ? 1. / (self.getActiveWeaponItem().getAttackReuseDelay() * self.getReuseModifier(target) * self.getActiveWeaponItem().getAttackReuseDelay() * 1000 / 1.5 / aSpd) : 0;		final double attackDelay = aSpd / 500;
		final double pAtk = self.getPAtk(target);

		final double damageNorm = pAtk * 70. / def;
		final double damageCrit = (pAtk * critPower + critStatic) * 70. / def;
		final double avgDam = critRate * damageCrit + (1. - critRate) * damageNorm;
		return (int) (Math.max(attackDelay, reuseDelay) * avgDam);
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
