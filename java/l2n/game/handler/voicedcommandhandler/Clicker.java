package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.commons.captcha.CaptchaValidator;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
/**
 * autor Расторгуев
 * 
 */
public class Clicker implements IVoicedCommandHandler
{
	private static final String[] _voicedCommands = { "clicker" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.startsWith("clicker"))
			return clicker(activeChar);
		return false;
	}

	public boolean clicker(L2Player activeChar)
	{
		// check target
		if(activeChar.getTarget() == null)
		{
			activeChar.sendMessage("Нет цели.");
			return false;
		}
		// check if target is a L2Player
		if(!activeChar.getTarget().isPlayer())
		{
			activeChar.sendMessage("Цель должна быть игроком.");
			return false;
		}
		
		L2Player ptarget = (L2Player) activeChar.getTarget();

		// check if player target himself
		if(ptarget.getObjectId() == activeChar.getObjectId())
		{
			activeChar.sendMessage("Вы не можете использовать команду на себя.");
			return false;
		}
		
		if(activeChar.getPvpFlag() > 0 || activeChar.getKarma() > 0)
		{
			activeChar.sendMessage("Вы не можете использовать команду пока вы флагнутый или ПК.");
			return false;
		}

		if(System.currentTimeMillis() - ptarget.getCharCaptchaTime() < Config.REUSE_COMMAND_TIME * 60000)
		{
			activeChar.sendMessage("Данный игрок недавно вводил каптчу.");
			return false;
		}
		
		final L2Zone siegeZone = ZoneManager.getInstance().getZoneByType(ZoneType.Siege, ptarget.getX(), ptarget.getY(), true);

		if(!ptarget.isInCombat() || ptarget.getPvpFlag() > 0 || ptarget.isInOlympiadMode() || ptarget.getDuel() != null || ptarget.getTeam() > 0 || ptarget.isInPeaceZone()
			|| siegeZone != null)
		{
			activeChar.sendMessage("Вы не можете использовать команду на этого чара.");
			return false;
		}
		
		CaptchaValidator.getInstance().sendCaptcha(ptarget);
		ptarget.setCharCaptchaTime();		
		activeChar.sendMessage("Каптча успешно отправлена цели.");		
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _voicedCommands;
	}
}
