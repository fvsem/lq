package l2n.game.handler.voicedcommandhandler;

import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.instances.L2DoorInstance;

public class PlayerCastleDoors implements IVoicedCommandHandler
{
	private static final String[] _voicedCommands = { "open", "close" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.startsWith("open") && target.equals("doors") && activeChar.isClanLeader())
		{
			if(activeChar.getTarget() instanceof L2DoorInstance)
			{
				L2DoorInstance door = (L2DoorInstance) activeChar.getTarget();
				Residence castle = CastleManager.getInstance().getCastleByIndex(activeChar.getClan().getHasCastle());
				if(door == null || castle == null)
					return false;
				if(castle.checkIfInZone(door.getX(), door.getY()))
					door.openMe();
			}
			else
				return false;
		}
		else if(command.startsWith("close") && target.equals("doors") && activeChar.isClanLeader())
			if(activeChar.getTarget() instanceof L2DoorInstance)
			{
				L2DoorInstance door = (L2DoorInstance) activeChar.getTarget();
				Residence castle = CastleManager.getInstance().getCastleByIndex(activeChar.getClan().getHasCastle());
				if(door == null || castle == null)
					return false;
				if(castle.checkIfInZone(door.getX(), door.getY()))
					door.closeMe();
			}
			else
				return false;
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _voicedCommands;
	}
}
