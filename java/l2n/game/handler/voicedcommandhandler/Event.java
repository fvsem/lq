package l2n.game.handler.voicedcommandhandler;


import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;

public class Event extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "event" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(!activeChar.getPassCheck())
		{
			return false;
		}
			show("data/html/command/events.htm", activeChar);
		return true;
	}

}
