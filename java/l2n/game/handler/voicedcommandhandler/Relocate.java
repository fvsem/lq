package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.skillclasses.Call;
import l2n.util.Location;


public class Relocate extends Functions implements IVoicedCommandHandler
{
	private final static int CRYSTALL_SUMMON_PRICE = 5;

	private static final String[] _commandList = new String[] { "Clan-escape" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(command.equalsIgnoreCase("Clan-escape"))
		{
			if(!activeChar.isClanLeader())
			{
				activeChar.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
				return false;
			}

			if(activeChar.isAlikeDead())
			{
				activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.Relocate.Dead", activeChar));
				return false;
			}
			if(!activeChar.getPassCheck())
			{
				return false;
			}
			// Проверяем можно ли суммонить к клан лидеру
			SystemMessage msg = Call.canSummonHere(activeChar);
			if(msg != null)
			{
				activeChar.sendPacket(msg);
				return false;
			}

				if(activeChar.getInventory().getCountOf(Config.ITEM_SUMMON) < Config.ITEM_SUMMON_PRICE)
				{
					activeChar.sendMessage("У вас нехватает предметов для призыва клана");
					
					return false;
				}

			Location pos;
			final L2Player[] clan_members = activeChar.getClan().getOnlineMembers(activeChar.getName());
			// снимаем цену за команду
			if(clan_members.length > 0)
				removeItem(activeChar, Config.ITEM_SUMMON, Config.ITEM_SUMMON_PRICE );

			for(L2Player member : clan_members)
				if(Call.canBeSummoned(member) == null)
				{
					pos = GeoEngine.findPointToStay(activeChar.getX(), activeChar.getY(), activeChar.getZ(), 100, 150);
					// Спрашиваем, согласие на призыв
					member.summonCharacterRequest(activeChar.getName(), pos, CRYSTALL_SUMMON_PRICE);
				}

			return true;
		}
		return false;
	}
}
