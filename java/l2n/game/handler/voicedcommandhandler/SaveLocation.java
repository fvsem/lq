package l2n.game.handler.voicedcommandhandler;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;

public class SaveLocation extends Functions implements IVoicedCommandHandler
{

	private static final String[] _commandList = new String[] { "saveloc", "unsaveloc" };

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
		if(!Config.COMMAND_ALLOW_SAVE_LOCATION)
		{
			activeChar.sendMessage(new CustomMessage("scripts.commands.TurnOff", activeChar));
			return false;
		}

		if(command.equalsIgnoreCase("saveloc"))
		{
			if(!checkCondition(activeChar, Config.COMMAND_ALLOW_SAVE_LOCATION_ITEM, Config.COMMAND_ALLOW_SAVE_LOCATION_PRICE))
				return false;
			else
			{
				if(activeChar.getVar("saveloc") != null)
					activeChar.unsetVar("saveloc");
				int x, y, z;
				x = activeChar.getX();
				y = activeChar.getY();
				z = activeChar.getZ();
				removeItem(activeChar, Config.COMMAND_ALLOW_SAVE_LOCATION_ITEM, Config.COMMAND_ALLOW_SAVE_LOCATION_PRICE);
				activeChar.setVar("saveloc", String.valueOf(x) + ";" + String.valueOf(y) + ";" + String.valueOf(z));
				activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.SaveCoords.saveloc", activeChar).addNumber(x).addNumber(y).addNumber(z));
			}
			return true;
		}
		if(command.equalsIgnoreCase("unsaveloc"))
		{
			if(activeChar.getVar("saveloc") != null)
			{
				activeChar.unsetVar("saveloc");
				activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.SaveCoords.unsaveloc", activeChar));
			}
			else
				activeChar.sendMessage(new CustomMessage("scripts.commands.voiced.SaveCoords.unsaveloc2", activeChar));

			return true;
		}
		return true;
	}

	public static boolean checkCondition(L2Player player, int itemId, int price)
	{
		// Получаем итем из инвенторя
		L2ItemInstance item = player.getInventory().findItemByItemId(itemId);

		// Если такого итема вообще нет...
		if(item == null)
		{
			player.sendPacket(new SystemMessage(SystemMessage.NOT_ENOUGH_MATERIALS));
			return false;
		}

		if(player.isActionsDisabled() || player.isSitting())
			return false;

		// Если количество итемов в инвентаре меньше цены...
		if(price != 0 && item.getCount() < price)
		{
			player.sendPacket(new SystemMessage(SystemMessage.NOT_ENOUGH_MATERIALS));
			return false;
		}

		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
