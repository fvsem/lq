package l2n.game.handler.voicedcommandhandler;


import l2n.extensions.scripts.Functions;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.actor.L2Player;

public class Pass extends Functions implements IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] { "pass" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, L2Player activeChar, String target)
	{
        	activeChar.setPassParalyzedTrue();
			show("data/html/passkey/login.htm", activeChar);
		return true;
	}
}
