package l2n.game.handler.bypasshandlers;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.entity.DimensionalRift;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.clientpackets.RequestExRemoveItemAttribute;
import l2n.game.network.serverpackets.ExShowBaseAttributeCancelWindow;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.RadarControl;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;
import l2n.game.tables.DoorTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.TeleportTable;

import java.util.StringTokenizer;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 11.10.2011
 * @time 4:33:37
 */
public class DefaultNpc implements IBypassHandler
{
	private static final String[] commands = new String[]
	{
			"TerritoryStatus",
			"Quest",
			"Chat",
			"Loto",
			"AttributeCancel",
			"CPRecovery", // FIXME
			"NpcLocationInfo",
			"SupportMagic",
			"ProtectionBlessing",
			"Multisell",
			"multisell",
			"EnterRift",
			"ChangeRiftRoom",
			"ExitRift",
			"SkillList",
			"ClanSkillList",
			"FishingSkillList",
			"TransformationSkillList",
			"TransferSkillList",
			"RemoveTransferSkill",
			"SquadSkillList",
			"Gate",
			"Augment",
			"Link",
			"Teleport"
	};

	@Override
	public boolean useBypass(final String command, final L2Player player, final L2Character target)
	{
		if(target == null || !target.isNpc())
			return false;

		final L2NpcInstance npc = (L2NpcInstance) target;
		try
		{
			if(command.equalsIgnoreCase("TerritoryStatus"))
			{
				final NpcHtmlMessage html = new NpcHtmlMessage(player, npc);
				html.setFile("data/html/merchant/territorystatus.htm");
				html.replace("%npcname%", target.getName());

				final Castle castle = npc.getCastle();
				if(castle != null && castle.getId() > 0)
				{
					html.replace("%castlename%", castle.getName());
					html.replace("%taxpercent%", String.valueOf(castle.getTaxPercent()));

					if(castle.getOwnerId() > 0)
					{
						final L2Clan clan = ClanTable.getInstance().getClan(castle.getOwnerId());
						if(clan != null)
						{
							html.replace("%clanname%", clan.getName());
							html.replace("%clanleadername%", clan.getLeaderName());
						}
						else
						{
							html.replace("%clanname%", "unexistant clan");
							html.replace("%clanleadername%", "None");
						}
					}
					else
					{
						html.replace("%clanname%", "NPC");
						html.replace("%clanleadername%", "None");
					}
				}
				else
				{
					html.replace("%castlename%", "Open");
					html.replace("%taxpercent%", "0");

					html.replace("%clanname%", "No");
					html.replace("%clanleadername%", npc.getName());
				}

				player.sendPacket(html);
			}
			else if(command.startsWith("Quest"))
			{
				String quest = "";
				try
				{
					quest = command.substring(5).trim();
				}
				catch(final IndexOutOfBoundsException ioobe)
				{}

				if(quest.length() == 0)
					npc.showQuestWindow(player);
				else
					npc.showQuestWindow(player, quest);
			}
			else if(command.startsWith("Chat"))
				try
				{
					final int val = Integer.parseInt(command.substring(5));
					npc.showChatWindow(player, val);
				}
				catch(final IndexOutOfBoundsException ioobe)
				{
					npc.showChatWindow(player, 0);
				}
				catch(final NumberFormatException nfe)
				{
					final String filename = command.substring(5).trim();
					if(filename.length() == 0)
						npc.showChatWindow(player, "data/html/npcdefault.htm");
					else
						npc.showChatWindow(player, filename);
				}
			else if(command.startsWith("Loto"))
			{
				final int val = Integer.parseInt(command.substring(5));
				if(val == 0)
					// new loto ticket
					for(int i = 0; i < 5; i++)
						player.setLoto(i, 0);
				npc.showLotoWindow(player, val);
			}
			else if(command.equalsIgnoreCase("AttributeCancel"))
				player.sendPacket(new ExShowBaseAttributeCancelWindow(player, RequestExRemoveItemAttribute.UNENCHANT_PRICE));
			else if(command.equalsIgnoreCase("CPRecovery"))
				makeCPRecovery(player, npc);
			else if(command.startsWith("NpcLocationInfo"))
				try
				{
					final int val = Integer.parseInt(command.substring(16));
					final L2NpcInstance radar_npc = L2ObjectsStorage.getByNpcId(val);
					if(radar_npc != null)
					{
						// Убираем флажок на карте и стрелку на компасе
						player.sendPacket(new RadarControl(2, 2, radar_npc.getLoc()));
						// Ставим флажок на карте и стрелку на компасе
						player.sendPacket(new RadarControl(0, 1, radar_npc.getLoc()));
					}
				}
				catch(final IndexOutOfBoundsException ioobe)
				{}
				catch(final NumberFormatException nfe)
				{
					player.sendPacket(CustomSystemMessageId.L2NPCINSTANCE_WRONG_PARAMETER.getPacket());
				}
			else if(command.equalsIgnoreCase("SupportMagic"))
				npc.makeSupportMagic(player);
			else if(command.equalsIgnoreCase("ProtectionBlessing"))
			{
				// Не выдаём блессиг протекшена ПКшникам.
				if(player.getKarma() > 0)
					return false;
				if(player.getLevel() > 39 || player.getClassId().getLevel() >= 3)// думаю так
				{
					final String content = "<html><body>Благословение защиты не доступны для персонажей уровня более 39 .</body></html>";
					final NpcHtmlMessage html = new NpcHtmlMessage(player, npc);
					html.setHtml(content);
					player.sendPacket(html);
					return false;
				}
				npc.doCast(SkillTable.getInstance().getInfo(5182, 1), player, true);
			}
			else if(command.startsWith("Multisell") || command.startsWith("multisell"))
			{
				final String listId = command.substring(9).trim();
				L2Multisell.getInstance().separateAndSend(Integer.parseInt(listId), player, npc.getCastle() != null ? npc.getCastle().getTaxRate() : 0);
			}
			else if(command.startsWith("EnterRift"))
			{
				final StringTokenizer st = new StringTokenizer(command);
				st.nextToken(); // no need for "enterRift"

				final Byte b1 = Byte.parseByte(st.nextToken()); // rift type 1 - Recruit, ..., 6 - Hero rift
				DimensionalRiftManager.getInstance().start(player, b1, npc);
			}
			else if(command.equalsIgnoreCase("ChangeRiftRoom"))
			{
				if(player.isInParty() && player.getParty().isInReflection() && player.getParty().getReflection().isDimensionalRift())
					((DimensionalRift) player.getParty().getReflection()).manualTeleport(player, npc);
				else
					DimensionalRiftManager.getInstance().teleportToWaitingRoom(player);
			}
			else if(command.equalsIgnoreCase("ExitRift"))
			{
				if(player.isInParty() && player.getParty().isInReflection() && player.getParty().getReflection().isDimensionalRift())
					((DimensionalRift) player.getParty().getReflection()).manualExitRift(player, npc);
				else
					DimensionalRiftManager.getInstance().teleportToWaitingRoom(player);
			}
			else if(command.equalsIgnoreCase("SkillList"))
				npc.showSkillList(player);
			else if(command.equalsIgnoreCase("ClanSkillList"))
				npc.showClanSkillList(player);
			else if(command.equalsIgnoreCase("FishingSkillList"))
				npc.showFishingSkillList(player);
			else if(command.equalsIgnoreCase("TransformationSkillList"))
				npc.showTransformationSkillList(player);
			else if(command.equalsIgnoreCase("TransferSkillList"))
				npc.showTransferSkillList(player);
			else if(command.startsWith("RemoveTransferSkill"))
			{
				final StringTokenizer st = new StringTokenizer(command);
				st.nextToken();

				final ClassId classId = player.getClassId();
				if(classId != null)
				{
					int item_id = 0;
					switch (classId)
					{
						case cardinal:
							item_id = 15307; // Cardinal (97)
							break;
						case evaSaint:
							item_id = 15308; // Eva's Saint (105)
							break;
						case shillienSaint:
							item_id = 15309; // Shillen Saint (112)
					}

					final String var = player.getVar("TransferSkills" + item_id);
					if(var == null || var.isEmpty())
						return true;

					if(player.getAdena() < Config.TRANSFER_SKILL_RESET_ADENA_COST)
					{
						player.sendPacket(Msg.YOU_CANNOT_RESET_THE_SKILL_LINK_BECAUSE_THERE_IS_NOT_ENOUGH_ADENA);
						return true;
					}

					player.unsetVar("TransferSkills" + item_id);

					final String[] skills = var.split(";");
					for(final String skill : skills)
						player.removeSkill(Integer.parseInt(skill), true);

					player.reduceAdena(Config.TRANSFER_SKILL_RESET_ADENA_COST, true);
					Functions.addItem(player, item_id, skills.length);
				}
			}
			else if(command.equalsIgnoreCase("SquadSkillList"))
				npc.showSquadSkillList(player);
			else if(command.startsWith("Gate"))
			{
				final int id = Integer.parseInt(command.substring(4).trim());
				final L2DoorInstance door = DoorTable.getInstance().getDoor(id);
				if(door != null)
					if(door.isOpen())
						door.closeMe();
					else
						door.openMe();
			}
			else if(command.startsWith("Augment"))
			{
				final int cmdChoice = Integer.parseInt(command.substring(8, 9).trim());
				if(cmdChoice == 1)
					player.sendPacket(Msg.SELECT_THE_ITEM_TO_BE_AUGMENTED, Msg.ExShowVariationMakeWindow);
				else if(cmdChoice == 2)
					player.sendPacket(Msg.SELECT_THE_ITEM_FROM_WHICH_YOU_WISH_TO_REMOVE_AUGMENTATION, Msg.ExShowVariationCancelWindow);
			}
			else if(command.startsWith("Link"))
				npc.showChatWindow(player, "data/html/" + command.substring(5));
			else if(command.startsWith("Teleport"))
			{
				final int cmdChoice = Integer.parseInt(command.substring(9, 10).trim());
				final TeleportTable.TeleportLocation[] list = TeleportTable.getInstance().getTeleportLocationList(npc.getNpcId(), cmdChoice);
				if(list != null)
					npc.showTeleportList(player, list);
				else
					player.sendMessage("Ссылка неисправна, сообщите администратору.");
			}
		}
		catch(final StringIndexOutOfBoundsException sioobe)
		{
			_log.info("DefaultNpcHandler: incorrect htm bypass! npcId=" + npc.getNpcId() + " command=[" + command + "]");
		}
		catch(final NumberFormatException nfe)
		{
			_log.info("DefaultNpcHandler: invalid bypass to Server command parameter! npcId=" + npc.getNpcId() + " command=[" + command + "]");
		}
		return false;
	}

	private void makeCPRecovery(final L2Player player, final L2NpcInstance npc)
	{
		if(npc.getNpcId() != 31225 && npc.getNpcId() != 31226)
			return;

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(CustomSystemMessageId.L2NPCINSTANCE_GO_AWAY.getPacket(), Msg.ActionFail);
			return;
		}

		final long neededmoney = 100;
		final long currentmoney = player.getAdena();
		if(neededmoney > currentmoney)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}
		player.reduceAdena(neededmoney, true);
		player.setCurrentCp(npc.getCurrentCp() + 5000);
		player.sendPacket(new SystemMessage(SystemMessage.S1_CPS_WILL_BE_RESTORED).addString(player.getName()));
	}

	@Override
	public String[] getBypassList()
	{
		return commands;
	}
}
