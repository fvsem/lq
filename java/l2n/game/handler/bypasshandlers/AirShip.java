package l2n.game.handler.bypasshandlers;

import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.model.L2Clan;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.model.instances.L2AirShipControllerInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExGetOnAirShip;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.AirShipDocksTable;
import l2n.game.tables.AirShipDocksTable.AirShipDock;
import l2n.util.Location;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 11.10.2011
 * @time 5:52:55
 */
public class AirShip implements IBypassHandler
{
	private static final int ENERGY_STAR_STONE = 13277;
	private static final int AIRSHIP_SUMMON_LICENSE = 13559;

	private static final String[] commands = new String[]
	{
			"board",
			"summon",
			"register"
	};

	@Override
	public boolean useBypass(final String command, final L2Player player, final L2Character target)
	{
		if(target == null || !(target instanceof L2AirShipControllerInstance))
			return false;

		final L2AirShipControllerInstance npc = (L2AirShipControllerInstance) target;
		if(command.equalsIgnoreCase("board"))
		{
			final SystemMessage msg = canBoard(player);
			if(msg != null)
			{
				player.sendPacket(msg);
				return true;
			}

			final L2AirShip airship = searchDockedAirShip(npc);
			if(airship == null)
			{
				player.sendActionFailed();
				return true;
			}

			if(player.getVehicle() != null && player.getVehicle().getId() == airship.getId())
			{
				player.sendPacket(Msg.YOU_HAVE_ALREADY_BOARDED_ANOTHER_AIRSHIP);
				return true;
			}

			player._stablePoint = player.getLoc().setH(0);
			player.setVehicle(airship);
			player.setInVehiclePosition(new Location());
			player.setLoc(airship.getLoc());
			player.broadcastPacket(new ExGetOnAirShip(player, airship, player.getInVehiclePosition()));
		}
		else if(command.equalsIgnoreCase("summon"))
		{
			if(player.getClan() == null || player.getClan().getLevel() < 5)
			{
				player.sendPacket(Msg.IN_ORDER_TO_ACQUIRE_AN_AIRSHIP_THE_CLAN_S_LEVEL_MUST_BE_LEVEL_5_OR_HIGHER);
				return true;
			}

			if((player.getClanPrivileges() & L2Clan.CP_CL_SUMMON_AIRSHIP) != L2Clan.CP_CL_SUMMON_AIRSHIP)
			{
				player.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
				return true;
			}

			if(!player.getClan().isHaveAirshipLicense())
			{
				player.sendPacket(Msg.AN_AIRSHIP_CANNOT_BE_SUMMONED_BECAUSE_EITHER_YOU_HAVE_NOT_REGISTERED_YOUR_AIRSHIP_LICENSE_OR_THE);
				return true;
			}

			final L2AirShip dockedAirship = searchDockedAirShip(npc);
			final L2AirShip clanAirship = player.getClan().getAirship();
			if(clanAirship != null)
			{
				if(clanAirship == dockedAirship)
					player.sendPacket(Msg.THE_CLAN_OWNED_AIRSHIP_ALREADY_EXISTS);
				else
					player.sendPacket(Msg.THE_AIRSHIP_OWNED_BY_THE_CLAN_IS_ALREADY_BEING_USED_BY_ANOTHER_CLAN_MEMBER);
				return true;
			}

			if(dockedAirship != null)
			{
				player.sendPacket(Msg.ANOTHER_AIRSHIP_HAS_ALREADY_BEEN_SUMMONED_AT_THE_WHARF_PLEASE_TRY_AGAIN_LATER);
				return true;
			}

			if(Functions.removeItem(player, ENERGY_STAR_STONE, 5) != 5)
			{
				player.sendPacket(new SystemMessage(SystemMessage.THE_AIRSHIP_CANNOT_BE_SUMMONED_BECAUSE_YOU_DON_T_HAVE_ENOUGH_S1).addItemName(ENERGY_STAR_STONE));
				return true;
			}

			final L2AirShip newAirship = new L2AirShip(player.getClan(), "AirShip", 0);
			final AirShipDock ad = AirShipDocksTable.getInstance().getAirShipDockByNpcId(npc.getNpcId());

			L2VehicleManager.getInstance().addStaticItem(newAirship);
			newAirship.SetTrajet1(ad.getArrivalTrajetId(), 0, null, null);
			newAirship.spawn();

			Functions.npcShoutCustomMessage(npc, "l2n.game.model.instances.L2AirShipControllerInstance.AirshipSummoned");
			npc.startDepartureTask(newAirship);
		}
		else if(command.equalsIgnoreCase("register"))
		{
			if(player.getClan() == null || !player.isClanLeader() || player.getClan().getLevel() < 5)
			{
				player.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
				return true;
			}

			if(player.getClan().isHaveAirshipLicense())
			{
				player.sendPacket(Msg.THE_AIRSHIP_SUMMON_LICENSE_HAS_ALREADY_BEEN_ACQUIRED);
				return true;
			}

			if(Functions.getItemCount(player, AIRSHIP_SUMMON_LICENSE) == 0)
			{
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
				return true;
			}

			Functions.removeItem(player, AIRSHIP_SUMMON_LICENSE, 1);
			player.getClan().setAirshipLicense(true);
			player.getClan().setAirshipFuel(L2AirShip.MAX_FUEL);
			player.getClan().updateClanInDB();
			player.sendPacket(Msg.THE_AIRSHIP_SUMMON_LICENSE_HAS_BEEN_ENTERED_YOUR_CLAN_CAN_NOW_SUMMON_THE_AIRSHIP);
		}
		return false;
	}

	private static SystemMessage canBoard(final L2Player player)
	{
		if(player.getTransformationId() != 0)
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_TRANSFORMED;
		if(player.isParalyzed())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_PETRIFIED;
		if(player.isDead() || player.isFakeDeath())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_DEAD;
		if(player.isFishing())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_FISHING;
		if(player.isInCombat())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_IN_BATTLE;
		if(player.getDuel() != null)
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_IN_A_DUEL;
		if(player.isSitting())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_SITTING;
		if(player.isCastingNow())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_SKILL_CASTING;
		if(player.isCursedWeaponEquipped())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_A_CURSED_WEAPON_IS_EQUPPED;
		if(player.isCombatFlagEquipped() || player.isTerritoryFlagEquipped())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_HOLDING_A_FLAG;
		if(player.getPet() != null || player.isMounted())
			return Msg.YOU_CANNOT_BOARD_AN_AIRSHIP_WHILE_A_PET_OR_A_SERVITOR_IS_SUMMONED;

		return null;
	}

	private static L2AirShip searchDockedAirShip(final L2NpcInstance npc)
	{
		L2AirShip airship = null;
		for(final L2Character cha : L2World.getAroundCharacters(npc, 1000, 500))
			if(cha != null && !cha.isMoving && cha.isAirShip() && L2VehicleManager.getInstance().getBoat(cha.getObjectId()) != null)
			{
				airship = (L2AirShip) cha;
				if(!airship.isDocked())
				{
					airship = null;
					continue;
				}
			}
		return airship;
	}

	@Override
	public String[] getBypassList()
	{
		return commands;
	}
}
