package l2n.game.handler.bypasshandlers;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.instancemanager.TownManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.util.Location;


public class Gatekeeper extends Functions implements IBypassHandler
{
	private static final String[] commands = new String[]
	{
			"teleport",
			"quest_teleport"
	};

	@Override
	public boolean useBypass(final String command, final L2Player player, final L2Character target)
	{
		if(target == null || !target.isNpc())
			return false;

		final L2NpcInstance npc = (L2NpcInstance) target;
		if(command.startsWith("teleport"))
			try
			{
				final String teleport_data[] = command.substring(8).trim().split(" ");
				if(teleport_data.length < 4)
					throw new IllegalArgumentException();

				if(player.isActionsDisabled() || player.isSitting())
					return false;

				final int price = Integer.parseInt(teleport_data[3]);
				if(price > 0 && player.getAdena() < price)
				{
					player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
					return false;
				}

				if(player.isTerritoryFlagEquipped() || player.isCombatFlagEquipped())
				{
					player.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
					return false;
				}

				if(player.getMountType() == 2)
				{
					player.sendMessage("Телепортация верхом на виверне невозможна.");
					return false;
				}

				/*
				 * Затычка, npc Mozella (30483) не ТПшит чаров уровень которых превышает заданный в конфиге
				 * Off Like >= 56 lvl, данные по ограничению lvl'a устанавливаются в alternative.ini.
				 */
				if(npc.getNpcId() == 30483 && player.getLevel() >= Config.CRUMA_GATEKEEPER_LVL)
				{
					final NpcHtmlMessage msg = new NpcHtmlMessage(player, npc);
					msg.setFile("data/html/teleporter/30483-no.htm");
					player.sendPacket(msg);
					return false;
				}

				final int x = Integer.parseInt(teleport_data[0]);
				final int y = Integer.parseInt(teleport_data[1]);
				final int z = Integer.parseInt(teleport_data[2]);

				// Нельзя телепортироваться в города, где идет осада
				// Узнаем, идет ли осада в ближайшем замке к точке телепортации
				final Castle castle = TownManager.getInstance().getClosestTown(x, y).getCastle();
				if(castle != null && castle.getSiege().isInProgress())
				{
					// Определяем, в город ли телепортируется чар
					boolean teleToTown = false;
					int townId = 0;
					for(final L2Zone town : ZoneManager.getInstance().getZoneByType(ZoneType.Town))
						if(town.checkIfInZone(x, y))
						{
							teleToTown = true;
							townId = town.getIndex();
							break;
						}

					if(teleToTown && townId == castle.getTown())
					{
						player.sendPacket(Msg.YOU_CANNOT_TELEPORT_TO_A_VILLAGE_THAT_IS_IN_A_SIEGE);
						return false;
					}
				}

				final Location pos = GeoEngine.findPointToStay(x, y, z, 50, 100);
				if(price > 0)
					player.reduceAdena(price, true);
				player.teleToLocation(pos);
			}
			catch(final IndexOutOfBoundsException ioobe)
			{}
		else if(command.startsWith("quest_teleport"))
			try
			{
				final String teleport_data[] = command.substring(14).trim().split(" ");
				if(teleport_data.length < 5)
					throw new IllegalArgumentException();

				if(player.isActionsDisabled() || player.isSitting())
					return false;

				if(player.isTerritoryFlagEquipped() || player.isCombatFlagEquipped())
				{
					player.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
					return false;
				}

				if(player.getMountType() == 2)
				{
					player.sendMessage("Телепортация верхом на виверне невозможна.");
					return false;
				}

				final int count = Integer.parseInt(teleport_data[3]);
				final int item_id = Integer.parseInt(teleport_data[4]);
				if(count > 0 && item_id > 0)
				{
					final long itemCount = getItemCount(player, item_id);
					if(itemCount == 0 || itemCount < count)
					{
						player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
						return false;
					}

					removeItem(player, item_id, count);
				}

				final int x = Integer.parseInt(teleport_data[0]);
				final int y = Integer.parseInt(teleport_data[1]);
				final int z = Integer.parseInt(teleport_data[2]);

				final Location pos = GeoEngine.findPointToStay(x, y, z, 20, 70);
				player.teleToLocation(pos);

			}
			catch(final IndexOutOfBoundsException ioobe)
			{}

		return false;
	}

	@Override
	public String[] getBypassList()
	{
		return commands;
	}
}
