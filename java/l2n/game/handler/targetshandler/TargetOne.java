package l2n.game.handler.targetshandler;

import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 05.08.2011
 * @time 9:44:40
 */
public class TargetOne extends AbstractTargetHandler implements ISkillTargetTypeHandler
{
	@Override
	public L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		if(aimingTarget == null)
			return L2Skill.EMPTY_TARGETS;
		return new L2Character[] { aimingTarget };
	}

	@Override
	public SkillTargetType[] getTargetType()
	{
		return new SkillTargetType[] {
				SkillTargetType.TARGET_CORPSE,
				SkillTargetType.TARGET_CORPSE_PLAYER,
				SkillTargetType.TARGET_HOLY,
				SkillTargetType.TARGET_ITEM,
				SkillTargetType.TARGET_NONE,
				SkillTargetType.TARGET_ONE,
				SkillTargetType.TARGET_PARTY_ONE,
				SkillTargetType.TARGET_PET,
				SkillTargetType.TARGET_TYRANNOSAURUS,
				SkillTargetType.TARGET_OWNER,
				SkillTargetType.TARGET_ENEMY_PET,
				SkillTargetType.TARGET_ENEMY_SUMMON,
				SkillTargetType.TARGET_ENEMY_SERVITOR,
				SkillTargetType.TARGET_SELF,
				SkillTargetType.TARGET_UNLOCKABLE,
				SkillTargetType.TARGET_CHEST,
				SkillTargetType.TARGET_SIEGE,
				SkillTargetType.TARGET_FLAGPOLE };
	}
}
