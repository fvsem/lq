package l2n.game.handler.targetshandler;

import l2n.commons.list.GArray;
import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 05.08.2011
 * @time 10:54:37
 */
public class TargetArea extends AbstractTargetHandler implements ISkillTargetTypeHandler
{
	@Override
	public L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		if(aimingTarget == null || activeChar == null)
			return L2Skill.EMPTY_TARGETS;

		final GArray<L2Character> targets = new GArray<L2Character>();
		if(aimingTarget.isDead() == skill.isCorpse() && (!skill.isUndeadOnly() || aimingTarget.isUndead()))
			targets.add(aimingTarget);
		addTargetsToList(skill, targets, aimingTarget, activeChar, forceUse);

		return targets.toArray(new L2Character[targets.size()], 0);
	}

	@Override
	public SkillTargetType[] getTargetType()
	{
		return new SkillTargetType[] { SkillTargetType.TARGET_AREA_AIM_CORPSE, SkillTargetType.TARGET_AREA, SkillTargetType.TARGET_MULTIFACE, SkillTargetType.TARGET_TUNNEL };
	}
}
