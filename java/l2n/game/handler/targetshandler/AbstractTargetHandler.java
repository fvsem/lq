package l2n.game.handler.targetshandler;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2DecoyInstance;
import l2n.game.skills.EffectType;
import l2n.util.Util;

/**
 * @author L2System Project
 * @date 31.08.2011
 * @time 12:57:47
 */
public abstract class AbstractTargetHandler
{
	protected final void addTargetAndPetToList(final L2Skill skill, final GArray<L2Character> targets, final L2Player actor, final L2Player target)
	{
		if((actor == target || actor.isInRange(target, skill.getSkillRadius())) && target.isDead() == skill.isCorpse() && !targets.contains(target))
			targets.add(target);

		final L2Summon pet = target.getPet();
		if(pet != null && actor.isInRange(pet, skill.getSkillRadius()) && pet.isDead() == skill.isCorpse() && !targets.contains(pet))
			targets.add(pet);
	}

	protected final void addOlympiadTargetsToList(final L2Skill skill, final GArray<L2Character> targets, final L2Player player)
	{
		if(!skill.isCorpse())
			targets.add(player);

		final L2Summon pet = player.getPet();
		if(pet != null && pet.isDead() == skill.isCorpse())
			targets.add(pet);

		for(final L2Player target : L2World.getAroundPlayers(player, skill.getSkillRadius(), L2Skill.HEIGHT))
		{
			if(player.getOlympiadSide() != target.getOlympiadSide()) // Чужой команде помогать нельзя
				continue;
			if(player.getOlympiadGameId() != target.getOlympiadGameId()) // Команде на чужой арене помогать нельзя
				continue;
			addTargetAndPetToList(skill, targets, player, target);
		}
	}

	protected final void addTargetsToList(final L2Skill skill, final GArray<L2Character> targets, final L2Character aimingTarget, final L2Character activeChar, final boolean forceUse)
	{
		int count = 0;
		L2Territory terr = null;
		if(skill.getTargetType() == SkillTargetType.TARGET_TUNNEL)
		{
			final int radius = 100;
			final int zmin1 = activeChar.getZ() - 200;
			final int zmax1 = activeChar.getZ() + 200;
			final int zmin2 = aimingTarget.getZ() - 200;
			final int zmax2 = aimingTarget.getZ() + 200;

			final double angle = Util.convertHeadingToDegree(activeChar.getHeading());
			final double radian1 = Math.toRadians(angle - 90);
			final double radian2 = Math.toRadians(angle + 90);

			terr = new L2Territory(0);
			terr.add(activeChar.getX() + (int) (Math.cos(radian1) * radius), activeChar.getY() + (int) (Math.sin(radian1) * radius), zmin1, zmax1);
			terr.add(activeChar.getX() + (int) (Math.cos(radian2) * radius), activeChar.getY() + (int) (Math.sin(radian2) * radius), zmin1, zmax1);
			terr.add(aimingTarget.getX() + (int) (Math.cos(radian2) * radius), aimingTarget.getY() + (int) (Math.sin(radian2) * radius), zmin2, zmax2);
			terr.add(aimingTarget.getX() + (int) (Math.cos(radian1) * radius), aimingTarget.getY() + (int) (Math.sin(radian1) * radius), zmin2, zmax2);

			if(activeChar.isPlayer() && ((L2Player) activeChar).isGM())
			{
				activeChar.sendPacket(Functions.Points2Trace(terr.getCoords(), 50, true, false));
				activeChar.sendPacket(Functions.Points2Trace(terr.getCoords(), 50, true, true));
			}
		}

		for(final L2Character target : aimingTarget.getAroundCharacters(skill.getSkillRadius(), L2Skill.HEIGHT))
		{
			if(terr != null && !terr.isInside(target) || target == null)
				continue;
			if(activeChar == target)
				continue;
			if(activeChar.getPlayer() != null && activeChar.getPlayer() == target.getPlayer())
				continue;
			if(skill.getId() == L2Skill.SKILL_DETECTION && target.isInvisible() && target.getEffectList().getEffectByType(EffectType.Invisible) != null)
				target.getEffectList().stopEffects(EffectType.Invisible);
			if(skill.checkTarget(activeChar, target, aimingTarget, forceUse, false) != null)
				continue;
			if(!(activeChar instanceof L2DecoyInstance) && activeChar.isNpc() && target.isNpc())
				continue;
			targets.add(target);
			count++;

			// Больше 20 целей не добавляем
			if(count >= 20 && !activeChar.isRaid())
				break;
		}
	}
}
