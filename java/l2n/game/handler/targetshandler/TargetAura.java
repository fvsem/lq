package l2n.game.handler.targetshandler;

import l2n.commons.list.GArray;
import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;

/**
 * @author L2System Project
 * @date 05.08.2011
 * @time 11:06:50
 */
public class TargetAura extends AbstractTargetHandler implements ISkillTargetTypeHandler
{
	@Override
	public L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		final GArray<L2Character> targets = new GArray<L2Character>();
		addTargetsToList(skill, targets, activeChar, activeChar, forceUse);
		return targets.toArray(new L2Character[targets.size()], 0);
	}

	@Override
	public SkillTargetType[] getTargetType()
	{
		return new SkillTargetType[] { SkillTargetType.TARGET_AURA, SkillTargetType.TARGET_MULTIFACE_AURA, SkillTargetType.TARGET_AURA_CORPSE };
	}
}
