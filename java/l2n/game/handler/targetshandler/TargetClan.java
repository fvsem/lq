package l2n.game.handler.targetshandler;

import l2n.commons.list.GArray;
import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 05.08.2011
 * @time 11:36:19
 */
public class TargetClan extends AbstractTargetHandler implements ISkillTargetTypeHandler
{
	private final static Logger _log = Logger.getLogger(TargetClan.class.getName());

	@Override
	public L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse)
	{
		final GArray<L2Character> targets = new GArray<L2Character>();
		// for buff purposes, returns one unbuffed friendly mob nearby or mob itself?
		if(activeChar.isMonster() || activeChar.isSiegeGuard())
		{
			targets.add(activeChar);
			for(L2Character c : L2World.getAroundCharacters(activeChar, skill.getSkillRadius(), L2Skill.HEIGHT))
				if(!c.isDead() && (c.isMonster() || c.isSiegeGuard()))
					targets.add(c);
			return targets.toArray(new L2Character[targets.size()], 0);
		}

		L2Player player = activeChar.getPlayer();
		if(player == null)
		{
			if(activeChar.isPet() || activeChar.isSummon())
				return L2Skill.EMPTY_TARGETS;

			_log.log(Level.WARNING, "L2Skill.getTargets: " + skill.getTargetType() + " | player = null | activeChar = " + activeChar + "[" + activeChar.getNpcId() + "] | skill: " + skill);
			return L2Skill.EMPTY_TARGETS;
		}

		// если на олимпе
		if(player.isInOlympiadMode())
		{
			addOlympiadTargetsToList(skill, targets, player);
			addTargetAndPetToList(skill, targets, player, player);
			return targets.toArray(new L2Character[targets.size()], 0);
		}

		for(L2Player target : L2World.getAroundPlayers(player, skill.getSkillRadius(), L2Skill.HEIGHT))
		{
			if(target == null)
				continue;
			if(!player.isInSameClan(target))
				continue;
			if(skill.checkTarget(player, target, aimingTarget, forceUse, false) != null)
				continue;
			addTargetAndPetToList(skill, targets, player, target);
		}
		addTargetAndPetToList(skill, targets, player, player);

		return targets.toArray(new L2Character[targets.size()], 0);
	}

	@Override
	public SkillTargetType[] getTargetType()
	{
		return new SkillTargetType[] { SkillTargetType.TARGET_CLAN };
	}
}
