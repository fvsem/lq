package l2n.game.handler;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.handler.bypasshandlers.AirShip;
import l2n.game.handler.bypasshandlers.DefaultNpc;
import l2n.game.handler.bypasshandlers.Gatekeeper;
import l2n.game.handler.interfaces.IBypassHandler;
import l2n.game.handler.interfaces.ICommandHandler;

public class BypassHandler implements ICommandHandler
{
	private final TIntObjectHashMap<IBypassHandler> _datatable;

	public static BypassHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	private BypassHandler()
	{
		_datatable = new TIntObjectHashMap<IBypassHandler>();
		registerBypass(new AirShip());
		registerBypass(new DefaultNpc());
		registerBypass(new Gatekeeper());
	}

	public void registerBypass(final IBypassHandler handler)
	{
		final String[] list = handler.getBypassList();
		for(final String element : list)
			_datatable.put(element.hashCode(), handler);
	}

	public IBypassHandler getBypassHandler(final String bypass)
	{
		String command = bypass;
		int index;
		if((index = bypass.indexOf(" ")) != -1)
			command = bypass.substring(0, index);
		return _datatable.get(command.hashCode());
	}

	/**
	 * @return
	 */
	@Override
	public int size()
	{
		return _datatable.size();
	}

	public void clear()
	{
		_datatable.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final BypassHandler _instance = new BypassHandler();
	}
}
