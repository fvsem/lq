package l2n.game.handler.itemhandlers;

import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SetupGauge;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;

public class SoulCrystals implements IItemHandler
{
	// First line is for Red Soul Crystals, second is Green and third is Blue Soul
	// Crystals, ordered by ascending level, from 0 to 14
	private static final int[] _itemIds = {
			4629,
			4640,
			4651,
			4630,
			4641,
			4652,
			4631,
			4642,
			4653,
			4632,
			4643,
			4654,
			4633,
			4644,
			4655,
			4634,
			4645,
			4656,
			4635,
			4646,
			4657,
			4636,
			4647,
			4658,
			4637,
			4648,
			4659,
			4638,
			4649,
			4660,
			4639,
			4650,
			4661,
			5577,
			5578,
			5579,
			5580,
			5581,
			5582,
			5908,
			5911,
			5914,
			9570,
			9571,
			9572 };

	// Our main method, where everything goes on
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		if(player.getTarget() == null || !player.getTarget().isMonster())
		{
			player.sendPacket(Msg.INCORRECT_TARGET);
			player.sendActionFailed();
			return;
		}

		if(player.isImobilised() || player.isCastingNow())
		{
			player.sendActionFailed();
			return;
		}

		L2MonsterInstance target = (L2MonsterInstance) player.getTarget();

		// u can use soul crystal only when target hp goest to <50%
		if(target.getCurrentHpPercents() >= 50)
		{
			player.sendPacket(new SystemMessage(SystemMessage.THE_SOUL_CRYSTAL_WAS_NOT_ABLE_TO_ABSORB_A_SOUL));
			player.sendActionFailed();
			return;
		}

		// Soul Crystal Casting section
		L2Skill skill = SkillTable.getInstance().getInfo(2096, 1);
		player.broadcastPacket(new MagicSkillUse(player, 2096, 1, skill.getHitTime(), 0));
		player.sendPacket(new SetupGauge(0, skill.getHitTime()));
		// End Soul Crystal Casting section

		// Continue execution later
		player._skillTask = L2GameThreadPools.getInstance().scheduleAi(new CrystalFinalizer(player, target), skill.getHitTime(), true);
	}

	private static class CrystalFinalizer implements Runnable
	{
		private L2Player _activeChar;
		private L2MonsterInstance _target;

		CrystalFinalizer(L2Player activeChar, L2MonsterInstance target)
		{
			_activeChar = activeChar;
			_target = target;
		}

		@Override
		public void run()
		{
			_activeChar.setTarget(_target);
			_activeChar.sendActionFailed();
			_activeChar.onCastEndTime();
			if(_activeChar.isDead() || _target.isDead())
				return;
			_target.addAbsorber(_activeChar);
		}
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
