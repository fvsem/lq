package l2n.game.handler.itemhandlers;

import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ShowMiniMap;

public class WorldMap implements IItemHandler
{
	// all the items ids that this handler knowns
	private static final int[] _itemIds = { 1665, 1863 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		player.sendPacket(new ShowMiniMap(player, item.getItemId()));
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
