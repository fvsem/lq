package l2n.game.handler.itemhandlers;

import l2n.game.ai.CtrlEvent;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;

/**
 * Item Spirit of The Lake can be used on blue dragon Fafurion Kindred to heal him.
 */
public class SpiritLake implements IItemHandler
{
	private static final int FAFURION = 18482; // Fafurion Kindred
	private static final int SPIRIT_OF_THE_LAKE = 9689; // Spirit of The Lake
	private static final int SPIRIT_OF_THE_LAKE_SKILL = 2368; // Skill to heal
	private static final int[] ITEM_IDS = { SPIRIT_OF_THE_LAKE };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player activeChar = (L2Player) playable;
		if(activeChar.getTarget() == null || !(activeChar.getTarget() instanceof L2NpcInstance))
			return;

		L2NpcInstance target = (L2NpcInstance) activeChar.getTarget();

		if(target.getNpcId() != FAFURION)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.TARGET_IS_INCORRECT));
			return;
		}

		if(item.getItemId() == SPIRIT_OF_THE_LAKE)
		{
			L2Skill sl_skill = SkillTable.getInstance().getInfo(SPIRIT_OF_THE_LAKE_SKILL, 1);
			if(sl_skill != null)
			{
				activeChar.getInventory().destroyItemByItemId(SPIRIT_OF_THE_LAKE, 1, false);
				// display Heal animation in client
				activeChar.broadcastPacket(new MagicSkillUse(activeChar, 1011, 1, 1000, 500)); // Heal...
				activeChar.broadcastPacket(new MagicSkillLaunched(activeChar.getObjectId(), 1011, 1, false, target)); // Heal...
				sl_skill.useSkill(activeChar, target);
				if(target.hasAI())
					target.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, sl_skill, activeChar);
			}
		}
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
