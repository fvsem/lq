package l2n.game.handler.itemhandlers;

import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.clientpackets.RequestChangeNicknameColor;
import l2n.game.network.serverpackets.ExChangeNicknameNColor;

/**
 * @author L2System Project
 * @date 02.12.2010
 * @time 12:32:21
 */
public class ColorName implements IItemHandler
{
	private static final int[] ITEM_IDS = { RequestChangeNicknameColor.COLOR_NAME_1, RequestChangeNicknameColor.COLOR_NAME_2 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable != null && playable.isPlayer() && item != null)
			playable.sendPacket(new ExChangeNicknameNColor(item.getObjectId()));
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
