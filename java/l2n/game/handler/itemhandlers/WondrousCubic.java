package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.L2Drop;
import l2n.game.model.drop.L2DropData;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Rnd;

/**
 * @author HellSinger
 *         Date: 3/11/2008
 *         Time: 21:11:24
 */
public class WondrousCubic implements IItemHandler
{
	// Wondrous Cubic ID
	private static final int[] _itemIds = { 10632 };

	// Дроп для квестового предмета Wondrous Cubic
	protected static final L2DropData[] _dropPieces = new L2DropData[] {
			// Wondrous Cubic Pieces
			new L2DropData(10633, 1, 1, 333300, 1), // Rough Blue Cubic Piece 33,33%
			new L2DropData(10634, 1, 1, 199100, 1), // Rough Yellow Cubic Piece 19,91%
			new L2DropData(10635, 1, 1, 41600, 1), // Rough Green Cubic Piece 4,16%
			new L2DropData(10636, 1, 1, 15600, 1), // Rough Red Cubic Piece 1,56%
			new L2DropData(10637, 1, 1, 5200, 1), // Rough White Cubic Piece 0,52%
			// new L2DropData(10638, 1, 1, 0, 1), //Rough Black Cubic Piece *шанс неизвестен*
			new L2DropData(10642, 1, 1, 264100, 1), // Fine Blue Cubic Piece 26,41%
			new L2DropData(10643, 1, 1, 94400, 1), // Fine Yellow Cubic Piece 9,44%
			new L2DropData(10644, 1, 1, 32000, 1), // Fine Green Cubic Piece 3,20%
			new L2DropData(10645, 1, 1, 11300, 1), // Fine Red Cubic Piece 1,13%
			new L2DropData(10646, 1, 1, 3500, 1), // Fine White Cubic Piece 0,35%
													// new L2DropData(10647, 1, 1, 0, 1) //Fine Black Cubic Piece *шанс неизвестен*
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;
		L2Player activeChar = playable.getPlayer();
		L2Item itemTemplate = ItemTable.getInstance().getTemplate(item.getItemId());

		int itemId = item.getItemId();

		// Проверка готовности персонажа к действию
		if(!checkCondition(player))
			return;

		if(itemId == 10632)
		{
			long millis = ReuseTime(player);
			if(millis == 0 || player.isGM())
			{
				// устанавливаем и записываем минимально допустимое время следующего использования в переменные персонажа
				activeChar.setVar("WondrousCubic", String.valueOf(System.currentTimeMillis() + 86400000));
				// тупая проверка произошла ли запись в базу
				String time = player.getVar("WondrousCubic");
				if(time != null && Long.parseLong(time) > System.currentTimeMillis())
				{
					// сообщаем персонажу, что кубик сработал
					SystemMessage sm = new SystemMessage(SystemMessage.USE_S1);
					sm.addString(itemTemplate.getName());
					player.sendPacket(sm);
					// выдаем персонажу случайный кусок
					getGroupItem(activeChar, _dropPieces);
				}
				else
					activeChar.sendMessage("Please contact your Administration of a server in occasion of this problem.");
			}
			else
			{
				// представляем время в читаемом формате
				int secs = (int) millis / 1000;
				int mins = secs / 60;
				secs -= mins * 60;
				int hours = mins / 60;
				mins -= hours * 60;
				// отсылаем персонажу сообщение, содержащее время, оставшееся до следующего использования
				SystemMessage sm = new SystemMessage(SystemMessage.THERE_ARE_S2_HOURS_S3_MINUTES_AND_S4_SECONDS_REMAINING_IN_S1S_REUSE_TIME);
				sm.addString(itemTemplate.getName());
				sm.addNumber(hours);
				sm.addNumber(mins);
				sm.addNumber(secs);
				player.sendPacket(sm);
			}
		}
	}

	// Выбирает и выдаёт один предмет из группы
	public void getGroupItem(L2Player activeChar, L2DropData[] dropData)
	{
		L2ItemInstance item;
		long count = 0;
		while (count < 1)
			for(L2DropData d : dropData)
				if(Rnd.get(1, L2Drop.MAX_CHANCE) < d.getChance())
				{
					count = Rnd.get(d.getMinDrop(), (long) (d.getMaxDrop() * Config.RATE_QUESTS_REWARD));
					item = ItemTable.getInstance().createItem(d.getItemId(), activeChar.getObjectId(), 0, "WondrousCubicItem");
					item.setCount(count);
					activeChar.getInventory().addItem(item);
					activeChar.sendPacket(SystemMessage.obtainItems(item));
					return;
				}
	}

	private long ReuseTime(L2Player player)
	{
		String nextUseTime = player.getVar("WondrousCubic");
		if(nextUseTime == null || System.currentTimeMillis() > Long.parseLong(nextUseTime))
			return 0;
		return Long.parseLong(nextUseTime) - System.currentTimeMillis();
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}

	public boolean checkCondition(L2Player player)
	{
		QuestState ASpecialOrder = player.getPlayer().getQuestState("_040_ASpecialOrder");
		boolean q_040_complete = true;
		if(ASpecialOrder == null || !ASpecialOrder.isCompleted())
		{
			q_040_complete = false;
			if(player.getVar("lang@").equalsIgnoreCase("en"))
				player.sendMessage("The quest 'A Special Order' should be finished.");
			else
				player.sendMessage("Квест 'A Special Order' должен быть завершен.");
		}
		if(player.isSitting())
			player.sendPacket(Msg.YOU_CANNOT_MOVE_WHILE_SITTING);
		return !(player.isActionsDisabled() || player.isSitting() || !q_040_complete);
	}
}
