package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorsKeyTable;
import l2n.game.tables.DoorsKeyTable.L2KeyAction;
import l2n.util.Rnd;

/**
 * @author L2System Project
 */
public class DoorKey implements IItemHandler
{
	private static final int INTERACTION_DISTANCE = 100;
	private static final PlaySound SOUND_CLOSE = new PlaySound("interfacesound.system_close_01");

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player activeChar = playable.getPlayer();

		if(item == null || item.getCount() < 1)
		{
			activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		int itemId = item.getItemId();
		// Key of Enigma (Pavel Research Quest)
		if(itemId == 8060)
		{
			activeChar.broadcastPacket(new MagicSkillUse(activeChar, activeChar, 2260, 1, 0, 0));
			return;
		}

		L2Object target = activeChar.getTarget();
		// если это вообще не дверь)
		if(target == null || !target.isDoor())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			activeChar.sendActionFailed();
			return;
		}

		L2DoorInstance door = (L2DoorInstance) target;
		if(!activeChar.isInRange(door, INTERACTION_DISTANCE))
		{
			activeChar.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
			activeChar.sendActionFailed();
			return;
		}

		if(door.isOpen())
		{
			activeChar.sendPacket(Msg.IT_IS_NOT_LOCKED);
			activeChar.sendActionFailed();
			return;
		}

		// Get key actions
		L2KeyAction[] keyActions = DoorsKeyTable.get(itemId);
		if(keyActions == null || keyActions.length == 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		// Found Door id of action
		L2KeyAction actionDoor = null;
		for(L2KeyAction _doorAction : keyActions)
			// если дверь нашли
			if(_doorAction.doorId == door.getDoorId())
			{
				actionDoor = _doorAction;
				break;
			}
		if(actionDoor == null)
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			activeChar.sendActionFailed();
			return;
		}

		if(actionDoor.disappear)
		{
			L2ItemInstance ri = activeChar.getInventory().destroyItem(item, 1, true);
			activeChar.sendPacket(SystemMessage.removeItems(ri.getItemId(), 1));
		}

		int openChance = actionDoor.chance;
		if(Rnd.chance(openChance))
		{
			if(actionDoor.openmsg != null && !actionDoor.openmsg.isEmpty())
				activeChar.sendMessage(actionDoor.openmsg);
			door.openMe();
			door.onOpen(); // Closes the door after 60sec
			return;
		}

		// fail open ;(
		if(actionDoor.failed_msg != null && !actionDoor.failed_msg.isEmpty())
			activeChar.sendMessage(actionDoor.failed_msg);
		activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), 13));
		activeChar.sendPacket(SOUND_CLOSE);
	}

	@Override
	public int[] getItemIds()
	{
		return DoorsKeyTable.keys();
	}
}
