package l2n.game.handler.itemhandlers;

import l2n.game.ai.CtrlEvent;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.ArrayUtil;

/**
 * Magic Bottle используется на химер в Battered Lands или их лидера с целью получения с них
 * Life Force / Contained Life Force для квеста с караваном.
 */
public class MagicBottle implements IItemHandler
{
	private static final int MAGIC_BOTTLE_ITEM = 9672;
	private static final int[] ITEM_IDS = { MAGIC_BOTTLE_ITEM };

	private static final int[] CHIMERAS = { 22349, 22350, 22351, 22352, 22353 };

	private static final int MAGIC_BOTTLE_SKILL = 2359;

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player activeChar = playable.getPlayer();
		if(activeChar.getTarget() == null || !activeChar.getTarget().isNpc())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.TARGET_IS_INCORRECT));
			return;
		}

		L2NpcInstance target = (L2NpcInstance) activeChar.getTarget();
		int npcId = target.getNpcId();

		if(!ArrayUtil.arrayContains(CHIMERAS, npcId) || target.getCurrentHpPercents() > 10 || target.isDead())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.TARGET_IS_INCORRECT));
			return;
		}

		if(activeChar.isInRange(target, 150))
		{
			L2Skill skill = SkillTable.getInstance().getInfo(MAGIC_BOTTLE_SKILL, 1);
			if(skill != null)
			{
				// activeChar.getInventory().destroyItem(item, 1, false);
				activeChar.getPlayer().doCast(skill, target, true);
				activeChar.broadcastPacket(new MagicSkillUse(activeChar, activeChar, 2359, 1, 500, 0));
				target.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, skill, activeChar);
			}
		}
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
