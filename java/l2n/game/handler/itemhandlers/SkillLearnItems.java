package l2n.game.handler.itemhandlers;

import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.L2SkillLearnItem;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SendTradeDone;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillItemsData;
import l2n.game.tables.SkillTable;

/**
 * Хандлер для итемов дабл-кликом по которым учаться скилы.
 * 
 * @author L2System Project
 * @date 03.06.2010
 * @time 10:00:14
 */
public class SkillLearnItems implements IItemHandler
{
	@Override
	public void useItem(final L2Playable playable, final L2ItemInstance item)
	{
		if(!playable.isPlayer())
			return;

		final L2Player activeChar = (L2Player) playable;

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendMessage("Cannot use this item in transform!");
			return;
		}

		if(activeChar.inObserverMode())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isInOlympiadMode())
		{
			activeChar.sendPacket(Msg.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT);
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isAttackingNow())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isCursedWeaponEquipped())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isActionsDisabled() || activeChar.isCastingNow() || activeChar.isTransformed())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		// cancel active trade if item used
		if(activeChar.getTradeList() != null)
		{
			activeChar.getTradeList().removeAll();
			activeChar.sendPacket(SendTradeDone.Fail);
			activeChar.setTradeList(null);
			if(activeChar.isInTransaction())
				activeChar.getTransaction().cancel();
		}

		GArray<L2SkillLearnItem> skillItems = SkillItemsData.getInstance().getSkillGiveItem(item.getItemId());
		if(skillItems == null || skillItems.isEmpty())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		boolean _checked = false;
		L2SkillLearnItem skill_item = null;
		for(L2SkillLearnItem sl : skillItems)
			if(sl.checkConditions(activeChar, false))
			{
				skill_item = sl;
				_checked = true;
				break;
			}

		// check conditions
		if(!_checked)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.destroyItemByItemId(skill_item.getItemId(), 1, true) == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		// "Learn" skill
		_checked = false;
		L2Skill skill;
		for(final int[] sk : skill_item.getGiveSkills())
		{
			if(activeChar.getSkillLevel(sk[0]) > 0)
				continue;

			if(activeChar.getSp() >= skill_item.getPlayerSpReq())
			{
				if(skill_item.getPlayerSpReq() > 0)
					activeChar.setSp(activeChar.getSp() - skill_item.getPlayerSpReq());
			}
			else
			{
				activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_SP_TO_LEARN_SKILLS);
				return;
			}

			_checked = true;
			skill = SkillTable.getInstance().getInfo(sk[0], sk[1]);
			activeChar.addSkill(skill, true); // true - save player skils in DB
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addSkillName(skill.getId(), skill.getLevel()));
		}

		if(_checked)
		{
			// send SkillList, it is not sent automatically
			activeChar.sendPacket(new SkillList(activeChar));

			// send message
			activeChar.sendPacket(SystemMessage.removeItems(skill_item.getItemId(), 1));
			activeChar.sendPacket(new SystemMessage(SystemMessage.USE_S1).addString(item.getItem().getName()));
			// Посылаем анимацию
			activeChar.sendPacket(new MagicSkillUse(activeChar, activeChar, 2241, 1, 1500, 0));
		}
	}

	@Override
	public final int[] getItemIds()
	{
		return SkillItemsData.getInstance().itemIDs();
	}
}
