package l2n.game.handler.itemhandlers;

import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SSQStatus;

/**
 * Item Handler for Seven Signs Record
 */
public class SevenSignsRecord implements IItemHandler
{
	private static final int[] _itemIds = { 5707 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		player.sendPacket(new SSQStatus(player, 1));
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
