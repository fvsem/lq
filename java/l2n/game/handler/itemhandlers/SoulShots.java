package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExAutoSoulShot;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

public class SoulShots implements IItemHandler
{
	private static final int[] ITEM_IDS = { 1835, 5789, 1463, 22082, 1464, 22083, 1465, 22084, 1466, 22085, 1467, 22086 };
	private static final int[] SKILL_IDS = { 2039, 2150, 2151, 2152, 2153, 2154 };

	private static final int[][] SOUL_SHOTS = {
			{ 1835, 5789 }, // NONE grade
			{ 1463, 22082 }, // D grade
			{ 1464, 22083 }, // C grade
			{ 1465, 22084 }, // B grade
			{ 1466, 22085 }, // A grade
			{ 1467, 22086 }, // S grade
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = (L2Player) playable;

		L2Weapon weaponItem = player.getActiveWeaponItem();

		L2ItemInstance weaponInst = player.getActiveWeaponInstance();
		int soulshot_id = item.getItemId();
		boolean auto = false;

		if(player.getAutoSoulShot().contains(soulshot_id))
			auto = true;

		// Check if Soul shot can be used
		if(weaponInst == null || weaponItem.getSoulShotCount() == 0)
		{
			if(!auto)
				player.sendPacket(Msg.CANNOT_USE_SOULSHOTS);
			return;
		}

		// soulshot is already active
		if(weaponInst.getChargedSoulshot() != L2ItemInstance.CHARGED_NONE)
			return;

		// Check for correct grade
		int grade = weaponItem.getCrystalType().externalOrdinal;
		if(!ArrayUtil.arrayContains(SOUL_SHOTS[grade], soulshot_id))
		{
			// wrong grade for weapon
			if(!auto)
				player.sendPacket(Msg.SOULSHOT_DOES_NOT_MATCH_WEAPON_GRADE);
			return;
		}

		int consumption = weaponItem.getSoulShotCount();
		if(weaponItem.getItemType() == WeaponType.BOW || weaponItem.getItemType() == WeaponType.CROSSBOW)
		{
			int newSS = (int) player.calcStat(Stats.SS_USE_BOW, consumption, null, null);
			if(newSS < consumption && .30 > Rnd.get())
				consumption = newSS;
		}

		long count = item.getCount();
		if(count < consumption)
		{
			if(auto)
			{
				player.removeAutoSoulShot(soulshot_id);
				player.sendPacket(new ExAutoSoulShot(soulshot_id, false));
				player.sendPacket(new SystemMessage(SystemMessage.THE_AUTOMATIC_USE_OF_S1_WILL_NOW_BE_CANCELLED).addItemName(soulshot_id));
			}
			else
				player.sendPacket(Msg.NOT_ENOUGH_SOULSHOTS);
			return;
		}

		weaponInst.setChargedSoulshot(L2ItemInstance.CHARGED_SOULSHOT);
		if(!Config.INFINITE_SOULSHOT)
			player.getInventory().destroyItem(item, consumption, false);
		player.sendPacket(Msg.POWER_OF_THE_SPIRITS_ENABLED);
		player.broadcastPacket(new MagicSkillUse(player, player, SKILL_IDS[grade], 1, 0, 0));
	}

	@Override
	public final int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
