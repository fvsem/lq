package l2n.game.handler.itemhandlers;

import l2n.game.RecipeController;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2RecipeList;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.RecipeBookItemList;
import l2n.game.network.serverpackets.SystemMessage;

public class Recipes implements IItemHandler
{
	private static int[] _itemIds = null;

	public Recipes()
	{
		_itemIds = RecipeController.getInstance().getAllItemIds();
	}

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		if(item == null || item.getCount() < 1)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}
		L2RecipeList rp = RecipeController.getInstance().getRecipeByItemId(item.getItemId());
		if(rp.isDwarvenRecipe())
		{
			if(player.getDwarvenRecipeLimit() > 0)
			{
				if(player.getDwarvenRecipeBook().size() >= player.getDwarvenRecipeLimit())
				{
					player.sendPacket(Msg.NO_FURTHER_RECIPES_MAY_BE_REGISTERED);
					return;
				}

				if(rp.getLevel() > player.getSkillLevel(L2Skill.SKILL_CRAFTING))
				{
					player.sendPacket(Msg.CREATE_ITEM_LEVEL_IS_TOO_LOW_TO_REGISTER_THIS_RECIPE);
					return;
				}
				if(player.findRecipe(rp))
				{
					player.sendPacket(Msg.THAT_RECIPE_IS_ALREADY_REGISTERED);
					return;
				}
				// add recipe to recipebook
				player.registerRecipe(rp, true);
				player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_ADDED).addString(item.getItem().getName()));
				player.getInventory().destroyItem(item, 1, true);
				RecipeBookItemList response = new RecipeBookItemList(rp.isDwarvenRecipe(), (int) player.getCurrentMp());
				response.setRecipes(player.getDwarvenRecipeBook());
				player.sendPacket(response);
			}
			else
				player.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_REGISTER_A_RECIPE);
			return;
		}

		if(player.getCommonRecipeLimit() > 0)
		{

			if(player.getCommonRecipeBook().size() >= player.getCommonRecipeLimit())
			{
				player.sendPacket(Msg.NO_FURTHER_RECIPES_MAY_BE_REGISTERED);
				return;
			}
			if(player.findRecipe(rp))
			{
				player.sendPacket(Msg.THAT_RECIPE_IS_ALREADY_REGISTERED);
				return;
			}

			player.registerRecipe(rp, true);
			player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_ADDED).addString(item.getItem().getName()));
			player.getInventory().destroyItem(item, 1, true);
			RecipeBookItemList response = new RecipeBookItemList(rp.isDwarvenRecipe(), (int) player.getCurrentMp());
			response.setRecipes(player.getDwarvenRecipeBook());
			player.sendPacket(response);
		}
		else
			player.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_REGISTER_A_RECIPE);
	}

	@Override
	public int[] getItemIds()
	{
		return _itemIds;
	}
}
