package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.scripts.npc.hellbound.HellboundRemnantInstance;

/**
 * Needed to Kill Remnant Diabolist and Remnant Diviner at the Ancient Temple.<BR>
 * When their HP reaches 0, use the holy water on them.<BR>
 * Unlimited use.<BR>
 * 
 * @author L2System Project
 * @date 14.10.2010
 * @time 13:36:13
 */
public class HolyWater implements IItemHandler
{
	private static final int[] _itemIds = { 9673 };
	L2Player player;
	L2MonsterInstance target;

	@Override
	public void useItem(L2Playable playable, L2ItemInstance _item)
	{
		if(playable == null)
			return;

		// Цель не выделена, цель не недоупокоеный
		if(playable.getTarget() == null || !(playable.getTarget() instanceof HellboundRemnantInstance))
		{
			playable.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		HellboundRemnantInstance target = (HellboundRemnantInstance) playable.getTarget();

		// Моб уже мертвый
		if(target.isDead() || target.isDying())
		{
			playable.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		playable.broadcastPacket(new MagicSkillUse(playable, target, 2358, 1, 0, 0));
		target.onUseHolyWater(playable);
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
