package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;

public class BeastShot implements IItemHandler
{
	private final static int[] _itemIds = { 6645, 6646, 6647, 20332, 20333, 20334 };

	static final SystemMessage PETS_AND_SERVITORS_ARE_NOT_AVAILABLE_AT_THIS_TIME = new SystemMessage(SystemMessage.PETS_AND_SERVITORS_ARE_NOT_AVAILABLE_AT_THIS_TIME);
	static final SystemMessage WHEN_PET_OR_SERVITOR_IS_DEAD_SOULSHOTS_OR_SPIRITSHOTS_FOR_PET_OR_SERVITOR_ARE_NOT_AVAILABLE = new SystemMessage(SystemMessage.WHEN_PET_OR_SERVITOR_IS_DEAD_SOULSHOTS_OR_SPIRITSHOTS_FOR_PET_OR_SERVITOR_ARE_NOT_AVAILABLE);
	static final SystemMessage YOU_DONT_HAVE_ENOUGH_SOULSHOTS_NEEDED_FOR_A_PET_SERVITOR = new SystemMessage(SystemMessage.YOU_DONT_HAVE_ENOUGH_SOULSHOTS_NEEDED_FOR_A_PET_SERVITOR);
	static final SystemMessage YOU_DONT_HAVE_ENOUGH_SPIRITSHOTS_NEEDED_FOR_A_PET_SERVITOR = new SystemMessage(SystemMessage.YOU_DONT_HAVE_ENOUGH_SPIRITSHOTS_NEEDED_FOR_A_PET_SERVITOR);

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = playable.getPlayer();
		L2Summon pet = player.getPet();
		if(pet == null)
		{
			player.sendPacket(PETS_AND_SERVITORS_ARE_NOT_AVAILABLE_AT_THIS_TIME);
			return;
		}

		if(pet.isDead())
		{
			player.sendPacket(WHEN_PET_OR_SERVITOR_IS_DEAD_SOULSHOTS_OR_SPIRITSHOTS_FOR_PET_OR_SERVITOR_ARE_NOT_AVAILABLE);
			return;
		}

		int consumption = 0;
		int skillid = 0;

		switch (item.getItemId())
		{
			case 6645:
			case 20332:
				if(pet.getChargedSoulShot())
					return;
				consumption = pet.getSoulshotConsumeCount();
				if(item.getCount() < consumption)
				{
					player.sendPacket(YOU_DONT_HAVE_ENOUGH_SOULSHOTS_NEEDED_FOR_A_PET_SERVITOR);
					return;
				}
				pet.chargeSoulShot();
				skillid = 2033;
				break;
			case 6646:
			case 20333:
				if(pet.getChargedSpiritShot() > 0)
					return;
				consumption = pet.getSpiritshotConsumeCount();
				if(item.getCount() < consumption)
				{
					player.sendPacket(YOU_DONT_HAVE_ENOUGH_SPIRITSHOTS_NEEDED_FOR_A_PET_SERVITOR);
					return;
				}
				pet.chargeSpiritShot(L2ItemInstance.CHARGED_SPIRITSHOT);
				skillid = 2008;
				break;
			case 6647:
			case 20334:
				if(pet.getChargedSpiritShot() > 1)
					return;
				consumption = pet.getSpiritshotConsumeCount();
				if(item.getCount() < consumption)
				{
					player.sendPacket(YOU_DONT_HAVE_ENOUGH_SPIRITSHOTS_NEEDED_FOR_A_PET_SERVITOR);
					return;
				}
				pet.chargeSpiritShot(L2ItemInstance.CHARGED_BLESSED_SPIRITSHOT);
				skillid = 2009;
				break;
		}

		pet.broadcastPacket(new MagicSkillUse(pet, pet, skillid, 1, 0, 0));
				if(!Config.INFINITE_BEASTSHOT)
		player.getInventory().destroyItem(item, consumption, false);
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
