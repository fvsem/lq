package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExAutoSoulShot;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Weapon;
import l2n.util.ArrayUtil;

public class SpiritShot implements IItemHandler
{
	// all the items ids that this handler knowns
	private static final int[] ITEM_IDS = { 2509, 5790, 2510, 22077, 2511, 22078, 2512, 22079, 2513, 22080, 2514, 22081 };
	private static final short[] SKILL_IDS = { 2061, 2155, 2156, 2157, 2158, 2159 };

	private static final int[][] SPIRIT_SHOTS = {
			{ 2509, 5790 }, // NONE grade
			{ 2510, 22077 }, // D grade
			{ 2511, 22078 }, // C grade
			{ 2512, 22079 }, // B grade
			{ 2513, 22080 }, // A grade
			{ 2514, 22081 }, // S grade
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer() || item == null)
			return;

		L2Player player = playable.getPlayer();

		L2ItemInstance weaponInst = player.getActiveWeaponInstance();
		L2Weapon weaponItem = player.getActiveWeaponItem();
		int spirit_shot = item.getItemId();
		boolean auto = false;

		if(player.getAutoSoulShot().contains(spirit_shot))
			auto = true;

		// Check if Soul shot can be used
		if(weaponInst == null || weaponItem.getSpiritShotCount() == 0)
		{
			if(!auto)
				player.sendPacket(Msg.CANNOT_USE_SPIRITSHOTS);
			return;
		}

		// spiritshot is already active
		if(weaponInst.getChargedSpiritshot() != L2ItemInstance.CHARGED_NONE)
			return;

		// Check for correct grade
		int grade = weaponItem.getCrystalType().externalOrdinal;
		if(!ArrayUtil.arrayContains(SPIRIT_SHOTS[grade], spirit_shot))
		{
			// wrong grade for weapon
			if(!auto)
				player.sendPacket(Msg.SPIRITSHOT_DOES_NOT_MATCH_WEAPON_GRADE);
			return;
		}

		// Consume Spirit shot if player has enough of them
		long count = item.getCount();
		if(count < weaponItem.getSpiritShotCount())
		{
			if(auto)
			{
				player.removeAutoSoulShot(spirit_shot);
				player.sendPacket(new ExAutoSoulShot(spirit_shot, false));
				player.sendPacket(new SystemMessage(SystemMessage.THE_AUTOMATIC_USE_OF_S1_WILL_NOW_BE_CANCELLED).addItemName(item.getItemId()));
			}
			else
				player.sendPacket(Msg.NOT_ENOUGH_SPIRITSHOTS);
			return;
		}

		weaponInst.setChargedSpiritshot(L2ItemInstance.CHARGED_SPIRITSHOT);
		if(!Config.INFINITE_SPIRITSHOT)
			player.getInventory().destroyItem(item, weaponItem.getSpiritShotCount(), false);
		player.sendPacket(Msg.POWER_OF_MANA_ENABLED);
		player.broadcastPacket(new MagicSkillUse(player, player, SKILL_IDS[grade], 1, 0, 0));
	}

	@Override
	public final int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
