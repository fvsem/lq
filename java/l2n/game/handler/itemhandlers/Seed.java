package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.TownManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.NextAction;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2RaidBossInstance;
import l2n.game.tables.SkillTable;

public class Seed implements IItemHandler
{
	private static final int[] _itemIds = {
			5650,
			5016,
			5651,
			5017,
			5652,
			5018,
			5653,
			5019,
			5654,
			5020,
			5655,
			5021,
			5656,
			5022,
			7044,
			7051,
			7033,
			7019,
			5657,
			5023,
			7035,
			7021,
			7031,
			7017,
			7050,
			7057,
			6742,
			7028,
			7037,
			7023,
			7042,
			6768,
			5658,
			5024,
			5659,
			5025,
			5660,
			5026,
			5661,
			5027,
			5662,
			5028,
			5663,
			5029,
			7030,
			7016,
			5664,
			5030,
			5665,
			5031,
			5666,
			5032,
			7045,
			7052,
			5667,
			5033,
			5668,
			5034,
			5669,
			5035,
			5670,
			5036,
			5671,
			5037,
			5672,
			5038,
			5673,
			5039,
			7048,
			7055,
			7036,
			7022,
			7032,
			7018,
			5674,
			7043,
			7029,
			5675,
			5041,
			6743,
			6769,
			6731,
			6757,
			6747,
			6773,
			5676,
			5042,
			5677,
			5043,
			7034,
			7020,
			5678,
			5044,
			7040,
			7026,
			5679,
			5045,
			5680,
			5046,
			5681,
			5047,
			5682,
			5048,
			7039,
			7025,
			7041,
			7027,
			7046,
			7053,
			5683,
			5049,
			5684,
			5050,
			5685,
			5051,
			5686,
			5052,
			6748,
			6774,
			6736,
			6762,
			6728,
			6754,
			5687,
			5053,
			5688,
			5054,
			5689,
			5055,
			5690,
			5056,
			5691,
			7056,
			5691,
			5057,
			5692,
			5058,
			5693,
			5059,
			5694,
			5060,
			5695,
			5061,
			6744,
			6770,
			6732,
			6758,
			6749,
			6775,
			6737,
			6763,
			6729,
			6755,
			5696,
			5221,
			5697,
			5222,
			5698,
			5223,
			5699,
			5224,
			5700,
			5225,
			5701,
			5226,
			5227,
			6733,
			6759,
			6750,
			6776,
			6738,
			6764,
			7038,
			7024,
			6745,
			6771,
			6734,
			6760,
			6751,
			6777,
			6739,
			6765,
			6730,
			6756,
			6741,
			6767,
			6727,
			6746,
			6772,
			6735,
			6761,
			6752,
			6778,
			6740,
			6766,
			6753,
			8255,
			8237,
			8256,
			8238,
			8257,
			8239,
			8258,
			8240,
			8259,
			8241,
			8260,
			8242,
			8261,
			8243,
			8262,
			8244,
			8263,
			8245,
			8264,
			8246,
			8265,
			8247,
			8266,
			8248,
			8267,
			8249,
			8268,
			8250,
			8269,
			8251,
			8270,
			8252,
			8271,
			8253,
			8272,
			8254,
			8223,
			8224,
			8225,
			8226,
			8227,
			8228,
			8229,
			8230,
			8231,
			8232,
			8233,
			8234,
			8235,
			8236,
			8521,
			8522,
			8523,
			8524,
			8525,
			8526,
			5040,
			5702,
			7047,
			7049,
			7054,
			10171,
			10172,
			10173,
			10174,
			10197,
			10198,
			10199,
			10200,
			10201,
			10202 };

	private int _seedId;

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		// Цель не выбрана
		if(playable.getTarget() == null)
		{
			player.sendActionFailed();
			return;
		}

		// Цель не моб, РБ или миньон
		if(!player.getTarget().isMonster() || player.getTarget() instanceof L2RaidBossInstance || player.getTarget() instanceof L2MinionInstance)
		{
			player.sendPacket(Msg.THE_TARGET_IS_UNAVAILABLE_FOR_SEEDING);
			return;
		}

		L2MonsterInstance target = (L2MonsterInstance) playable.getTarget();

		if(target == null)
		{
			player.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		// Моб мертв
		if(target.isDead())
		{
			player.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		// Уже посеяно
		if(target.isSeeded())
		{
			player.sendPacket(Msg.THE_SEED_HAS_BEEN_SOWN);
			return;
		}

		_seedId = item.getItemId();
		if(_seedId == 0 || player.getInventory().findItemByItemId(item.getItemId()) == null)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		int castleId = TownManager.getInstance().getClosestTown(player).getCastleIndex();
		if(castleId < 0)
			castleId = 1; // gludio manor dy default
		else
		{
			Residence castle = CastleManager.getInstance().getCastleByIndex(castleId);
			if(castle != null)
				castleId = castle.getId();
		}

		/**
		 * System.out.println(CastleManager.getInstance().findNearestCastleIndex(activeChar) + " " +castleId + " " +
		 * L2Manor.getInstance().getSeedManorId(_seedId));
		 * // Несовпадение зоны
		 * if(L2Manor.getInstance().getCastleIdForSeed(_seedId) != castleId)
		 * {
		 * //System.out.println("seed (" + _seedId + ") zone " + L2Manor.getInstance().getSeedManorId(_seedId) + " != castle_zone " +
		 * castleId);
		 * player.sendPacket(Msg.THIS_SEED_MAY_NOT_BE_SOWN_HERE);
		 * return;
		 * }
		 * // use Sowing skill, id 2097
		 */
		L2Skill skill = SkillTable.getInstance().getInfo(2097, 1);
		if(skill == null)
		{
			player.sendActionFailed();
			return;
		}

		if(skill.checkCondition(player, target, false, false, true))
		{
			player.setUseSeed(_seedId);
			player.getAI().Cast(skill, target);
		}
		else if(skill.getNextAction() == NextAction.ATTACK && !player.equals(target) && target.isAutoAttackable(player))
			player.getAI().Attack(target, false, false);
		else
			player.sendActionFailed();
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
