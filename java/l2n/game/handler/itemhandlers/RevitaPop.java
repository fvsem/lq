package l2n.game.handler.itemhandlers;

import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

public class RevitaPop implements IItemHandler
{
	private static final int[] _itemIds = { 20034 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = (L2Player) playable;

		player.setVitalityPoints(20000, true);
		player.sendPacket(Msg.GAINED_VITALITY_POINTS);
		Functions.removeItem(player, 20034, 1);
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
