package l2n.game.handler.itemhandlers;

import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;

import java.util.concurrent.ScheduledFuture;

/**
 * @author L2System Project
 * @date 12.12.2010
 * @time 18:50:17
 */
public class SoulBreakingArrow implements IItemHandler
{
	private static final int[] ITEM_IDS = { 8192 };

	private ScheduledFuture<UnBlockTask> _blockTask = null;

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = playable.getPlayer();
		if(player == null)
			return;

		// System.out.println("_blockTask is " + (_blockTask == null ? "NULL" : "NOT NULL"));

		// использовать можно только с луком
		if(player.getActiveWeaponInstance() != null && player.getActiveWeaponItem() != null && (player.getActiveWeaponItem().getItemType().mask() & 32) != 0)
		{
			// проверяем реюз итема
			final L2Skill skill = SkillTable.getInstance().getInfo(2234, 1);
			if(player.isSkillDisabled(skill))
				return;

			L2Object target = player.getTarget();
			if(target == null || !target.isNpc())
			{
				player.sendPacket(Msg.INCORRECT_TARGET);
				return;
			}

			L2NpcInstance frintezza = (L2NpcInstance) target;
			if(!player.isInRange(frintezza, 900))
			{
				player.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
				return;
			}

			// если это действительно фринтеза и не заблочена
			if(frintezza.getNpcId() == 29045 && !frintezza.isBlocked())
			{
				// frintezza.broadcastPacket(new SocialAction(frintezza.getObjectId(), SocialAction.GREETING));
				player.broadcastPacket(new MagicSkillUse(player, frintezza, 2234, 1, 2000, 0));
				player.getInventory().destroyItem(8192, 1, false);

				// ставим откат
				player.disableSkill(skill, 3000);
				// Блокируем фринтезу
				frintezza.block();

				if(_blockTask != null)
				{
					_blockTask.cancel(false);
					_blockTask = null;
				}
				_blockTask = L2GameThreadPools.getInstance().scheduleAi(new UnBlockTask(frintezza), 15000, false);
			}
		}
		else
			player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
	}

	public static class UnBlockTask implements Runnable
	{
		private final long npcStoreId;

		public UnBlockTask(L2Character npc)
		{
			npcStoreId = npc.getStoredId();
		}

		@Override
		public void run()
		{
			L2NpcInstance npc = L2ObjectsStorage.getAsNpc(npcStoreId);
			if(npc == null)
				return;

			npc.unblock();
		}
	}

	@Override
	public final int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
