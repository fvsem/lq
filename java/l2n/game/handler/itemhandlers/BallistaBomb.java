package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2FortressBallistaInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;

public class BallistaBomb implements IItemHandler
{
	// All the item IDs that this handler knows.
	private static final int[] ITEM_IDS = new int[] { 9688 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable != null)
		{
			L2Player player = null;

			if(playable.isSummon())
			{
				player = ((L2Summon) playable).getPlayer();
				player.sendPacket(new SystemMessage(SystemMessage.PETS_CANNOT_USE_THIS_ITEM));
			}
			else if(playable.isPlayer())
			{
				player = (L2Player) playable;
				if(player.getSiegeState() != 1) // 1 = attacker, состояние осады
				{
					player.sendActionFailed();
					return;
				}

				L2Character target = (L2Character) player.getTarget();

				if(target instanceof L2FortressBallistaInstance)
				{
					L2Skill skill = SkillTable.getInstance().getInfo(2342, 1);
					player.doCast(skill, target, false);
				}
				else
					player.sendPacket(Msg.INCORRECT_TARGET);
			}
		}
	}

	@Override
	public final int[] getItemIds()
	{
		return ITEM_IDS;
	}
}
