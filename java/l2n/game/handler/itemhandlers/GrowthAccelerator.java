package l2n.game.handler.itemhandlers;

import l2n.game.ai.CtrlEvent;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.NpcTable;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Rnd;

/**
 * Используется для вызова РБ Cannibalistic Stakato Chief[25667] из Bizarre Cocoon
 * 
 * @author L2System Project
 * @date 07.12.2010
 * @time 10:34:07
 */
public class GrowthAccelerator implements IItemHandler
{
	// TODO #1 уточнить респаун коконов после вызова РБ, по моему они как то связаны.
	// TODO #3

	private static final int GROWTH_ACCELERATOR = 14832;
	private static final int[] COCOONS = new int[] { 18793, 18794, 18795, 18796, 18797, 18798 };

	@Override
	public int[] getItemIds()
	{
		return new int[] { GROWTH_ACCELERATOR };
	}

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer() || item == null)
			return;

		L2Player player = playable.getPlayer();
		if(player.getTarget() == null || player.getTarget() != null && !player.getTarget().isNpc())
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		L2NpcInstance npc = (L2NpcInstance) player.getTarget();
		if(npc == null || npc.isDead()) // наврятли конечно, но всё же)
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		int npcId = npc.getNpcId();
		if(ArrayUtil.arrayContains(COCOONS, npcId)) // проверяем список коконов
		{
			// castRange у скила 120 - проверяем растояние
			if(!player.isInRange(npc, 120))
			{
				player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
				return;
			}

			// Посылаем анимацию
			player.sendPacket(new MagicSkillUse(player, npc, 2905, 1, 4000, 0));
			// Удаляем предмет
			player.getInventory().destroyItem(item, 1, false);
			npc.broadcastPacket(new SocialAction(npc.getObjectId(), 1));
			npc.doDie(null);

			if(Rnd.chance(50))
			{
				try
				{
					L2Spawn sp = new L2Spawn(NpcTable.getTemplate(25667));
					Location pos = GeoEngine.findPointToStay(npc.getX(), npc.getY(), npc.getZ(), 0, 0);
					sp.setLoc(pos);
					L2NpcInstance raidboss = sp.doSpawn(true);
					raidboss.setCurrentHpMp(raidboss.getMaxHp(), raidboss.getMaxMp(), true);
					raidboss.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, player, Rnd.get(1, 100));
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		else
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
	}
}
