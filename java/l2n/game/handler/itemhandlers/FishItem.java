package l2n.game.handler.itemhandlers;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.FishDropData;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.FishTable;
import l2n.game.tables.ItemTable;
import l2n.util.Rnd;

public class FishItem implements IItemHandler
{
	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		GArray<FishDropData> rewards = FishTable.getInstance().getFishReward(item.getItemId());
		int count = 0;
		player.getInventory().destroyItem(item, 1, true);
		for(FishDropData d : rewards)
			if(Rnd.chance(d.getChance()))
			{
				giveItems(player, d.getRewardItemId(), Rnd.get((int) (d.getMinCount() * Config.RATE_FISH_DROP_COUNT), (int) (d.getMaxCount() * Config.RATE_FISH_DROP_COUNT)));
				count++;
				continue;
			}
		if(count == 0)
			player.sendMessage(new CustomMessage("scripts.items.FishItem.Nothing", player));
	}

	public void giveItems(L2Player activeChar, short itemId, long count)
	{
		L2ItemInstance item = ItemTable.getInstance().createItem(itemId, activeChar.getObjectId(), 0, "FishItem_add");
		item.setCount(count);
		activeChar.getInventory().addItem(item);
		activeChar.sendPacket(SystemMessage.obtainItems(item));
	}

	@Override
	public int[] getItemIds()
	{
		return FishTable.getInstance().getFishIds();
	}
}
