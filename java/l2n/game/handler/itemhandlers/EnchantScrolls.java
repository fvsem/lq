package l2n.game.handler.itemhandlers;

import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.clientpackets.AbstractEnchantPacket;
import l2n.game.network.serverpackets.ChooseInventoryItem;
import l2n.game.network.serverpackets.SystemMessage;

public class EnchantScrolls implements IItemHandler
{

	static final SystemMessage SELECT_ITEM_TO_ENCHANT = new SystemMessage(SystemMessage.SELECT_ITEM_TO_ENCHANT);

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;

		L2Player player = (L2Player) playable;
		if(player.isCastingNow())
			return;

		if(player.isEnchanting())
		{
			player.sendPacket(new SystemMessage(SystemMessage.ENCHANTMENT_ALREADY_IN_PROGRESS));
			return;
		}

		player.setActiveEnchantItem(item);
		player.sendPacket(SELECT_ITEM_TO_ENCHANT);
		player.sendPacket(new ChooseInventoryItem(item.getItemId()));
	}

	@Override
	public final int[] getItemIds()
	{
		return AbstractEnchantPacket.getItemIds();
	}
}
