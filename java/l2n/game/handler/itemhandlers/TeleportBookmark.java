package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * @author L2System Project
 * @date 23.01.2011
 * @time 2:16:53
 */
public class TeleportBookmark implements IItemHandler
{
	private static final int[] _itemIds = { 13015 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || item == null || !playable.isPlayer())
			return;

		L2Player player = playable.getPlayer();
		if(player.getBookMarkSlot() >= 30)
		{
			player.sendPacket(Msg.YOUR_NUMBER_OF_MY_TELEPORTS_SLOTS_HAS_REACHED_ITS_MAXIMUM_LIMIT);
			return;
		}

		player.getInventory().destroyItem(item, 1, false);
		player.setBookMarkSlot(player.getBookMarkSlot() + 3);
		SystemMessage sm = new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(item.getItemId());
		player.sendPacket(sm, Msg.THE_NUMBER_OF_MY_TELEPORTS_SLOTS_HAS_BEEN_INCREASED);
	}

	@Override
	public int[] getItemIds()
	{
		return _itemIds;
	}
}
