package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExChooseInventoryAttributeItem;
import l2n.game.network.serverpackets.SystemMessage;

public class AttributeStones implements IItemHandler
{
	private static final int[] _itemIds = { 9546, // Fire Stone
			9547, // Water Stone
			9548, // Earth Stone
			9549, // Wind Stone
			9550, // Dark Stone
			9551, // Divine Stone

			9552, // Fire Crystal
			9553, // Water Crystal
			9554, // Earth Crystal
			9555, // Wind Crystal
			9556, // Dark Crystal
			9557, // Divine Crystal

			9558, // Fire Jewel
			9559, // Water Jewel
			9560, // Earth Jewel
			9561, // Wind Jewel
			9562, // Dark Jewel
			9563, // Divine Jewel

			9564, // Fire Energy
			9565, // Water Energy
			9566, // Earth Energy
			9567, // Wind Energy
			9568, // Dark Energy
			9569, // Divine Energy

			10521, // Rough Ore of Fire
			10522, // Rough Ore of Water
			10523, // Rough Ore of the Earth
			10524, // Rough Ore of Wind
			10525, // Rough Ore of Darkness
			10526 // Rough Ore of Divinity
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		if(player.isCastingNow())
			return;

		if(player.isEnchanting())
		{
			player.sendPacket(new SystemMessage(SystemMessage.ENCHANTMENT_ALREADY_IN_PROGRESS));
			return;
		}

		if(player.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			player.sendPacket(Msg.YOU_CANNOT_ADD_ELEMENTAL_POWER_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
			return;
		}

		player.setActiveEnchantAttrItem(item);
		player.sendPacket(Msg.PLEASE_SELECT_ITEM_TO_ADD_ELEMENTAL_POWER);
		player.sendPacket(new ExChooseInventoryAttributeItem(item.getItemId()));
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
