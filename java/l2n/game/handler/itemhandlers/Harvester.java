package l2n.game.handler.itemhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.SkillTable;

public class Harvester implements IItemHandler
{
	private static final int[] _itemIds = { 5125 };

	@Override
	public void useItem(L2Playable playable, L2ItemInstance _item)
	{
		if(playable == null || !playable.isPlayer())
			return;
		L2Player player = (L2Player) playable;

		// Цель не выделена, цель не моб
		if(player.getTarget() == null || !player.getTarget().isMonster())
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		L2MonsterInstance target = (L2MonsterInstance) player.getTarget();

		// Моб не мертвый
		if(!target.isDead())
		{
			player.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		L2Skill skill = SkillTable.getInstance().getInfo(2098, 1);
		if(skill != null && skill.checkCondition(player, target, false, false, true))
			player.getAI().Cast(skill, target);
		else
			return;
	}

	@Override
	public final int[] getItemIds()
	{
		return _itemIds;
	}
}
