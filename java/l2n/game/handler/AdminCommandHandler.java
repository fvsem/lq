package l2n.game.handler;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.admincommandhandlers.*;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminCommandHandler implements ICommandHandler
{
	private static final Logger _log = Logger.getLogger(AdminCommandHandler.class.getName());

	private final HashMap<String, IAdminCommandHandler> _datatable;

	public static final String GROUP_NONE = "none";
	public static final String GROUP_HELP = "help";
	public static final String GROUP_TELEPORT = "teleport";
	public static final String GROUP_CW = "cursed weapons";
	public static final String GROUP_BANS = "bans";
	public static final String GROUP_EDITCHAR = "edit char";
	public static final String GROUP_SIEGE = "siege";
	public static final String GROUP_AUCTIONS = "auctions";
	public static final String GROUP_OLYMPIAD = "olympiad";
	public static final String GROUP_MOVIE_MAKER = "movie maker";
	public static final String GROUP_THREAD_POOL = "thread pool";
	public static final String GROUP_PETITION = "petition";
	public static final String GROUP_RELOAD = "reload";
	public static final String GROUP_DEBUG = "debug";
	public static final String GROUP_GEODATA = "geodata";
	public static final String GROUP_MANAGESERVER = "ManageServer_0";
	public static final String GROUP_MANAGESERVER1 = "ManageServer_1";
	public static final String GROUP_MANAGESERVER2 = "ManageServer_2";
	public static final String GROUP_MANAGESERVER3 = "ManageServer_3";
	public static final String GROUP_MANAGESERVER4 = "ManageServer_4";
	public static final String GROUP_MANAGESERVER5 = "ManageServer_5";
	public static final String GROUP_MANAGESERVER6 = "ManageServer_6";

	public static AdminCommandHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	private AdminCommandHandler()
	{
		_datatable = new HashMap<String, IAdminCommandHandler>();

		registerAdminCommandHandler(new AdminAdmin());
		registerAdminCommandHandler(new AdminAnnouncements());
		registerAdminCommandHandler(new AdminAttribute());
		registerAdminCommandHandler(new AdminAuctions());
		registerAdminCommandHandler(new AdminBan());
		registerAdminCommandHandler(new AdminCamera());
		registerAdminCommandHandler(new AdminCancel());
		registerAdminCommandHandler(new AdminClanHall());
		registerAdminCommandHandler(new AdminCreateItem());
		registerAdminCommandHandler(new AdminCursedWeapons());
		registerAdminCommandHandler(new AdminDebug());
		registerAdminCommandHandler(new AdminDelete());
		registerAdminCommandHandler(new AdminDisconnect());
		registerAdminCommandHandler(new AdminDoorControl());
		registerAdminCommandHandler(new AdminEditChar());
		registerAdminCommandHandler(new AdminEffects());
		registerAdminCommandHandler(new AdminEnchant());
		registerAdminCommandHandler(new AdminEvents());
		registerAdminCommandHandler(new AdminFame());
		registerAdminCommandHandler(new AdminFence());
		registerAdminCommandHandler(new AdminGeodata());
		registerAdminCommandHandler(new AdminGm());
		registerAdminCommandHandler(new AdminGmChat());
		registerAdminCommandHandler(new AdminHeal());
		registerAdminCommandHandler(new AdminHelpPage());
		registerAdminCommandHandler(new AdminIP());
		registerAdminCommandHandler(new AdminKill());
		registerAdminCommandHandler(new AdminLevel());
		registerAdminCommandHandler(new AdminMammon());
		registerAdminCommandHandler(new AdminManor());
		registerAdminCommandHandler(new AdminMenu());
		registerAdminCommandHandler(new AdminMonsterRace());
		registerAdminCommandHandler(new AdminMovieMaker());
		registerAdminCommandHandler(new AdminNochannel());
		registerAdminCommandHandler(new AdminOlympiad());
		registerAdminCommandHandler(new AdminOther());
		registerAdminCommandHandler(new AdminPetition());
		registerAdminCommandHandler(new AdminPledge());
		registerAdminCommandHandler(new AdminPolymorph());
		registerAdminCommandHandler(new AdminThreadPool());
		registerAdminCommandHandler(new AdminQuests());
		registerAdminCommandHandler(new AdminReload());
		registerAdminCommandHandler(new AdminRepairChar());
		registerAdminCommandHandler(new AdminRes());
		registerAdminCommandHandler(new AdminRide());
		registerAdminCommandHandler(new AdminRideWyvern());
		registerAdminCommandHandler(new AdminServer());
		registerAdminCommandHandler(new AdminScripts());
		registerAdminCommandHandler(new AdminShop());
		registerAdminCommandHandler(new AdminShutdown());
		registerAdminCommandHandler(new AdminSiege());
		registerAdminCommandHandler(new AdminSkill());
		registerAdminCommandHandler(new AdminSpawn());
		registerAdminCommandHandler(new AdminSS());
		registerAdminCommandHandler(new AdminTarget());
		registerAdminCommandHandler(new AdminTeleport());
		registerAdminCommandHandler(new AdminVitLevel());
		registerAdminCommandHandler(new AdminZone());
	}

	public void registerAdminCommandHandler(final IAdminCommandHandler handler)
	{
		try
		{
			if(handler.getAdminCommandList().length != handler.getCommandGroup().length || handler.getAdminCommandList().length != handler.getDescription().length)
			{
				_log.info("Incorrect help imlemented in class: " + handler.getClass().getName());
				return;
			}
		}
		catch(final Exception e)
		{
			_log.info("Null help in class: " + handler.getClass().getName());
			return;
		}

		final String[] ids = handler.getAdminCommandList();
		for(final String element : ids)
			_datatable.put(element.toLowerCase(), handler);
	}

	public IAdminCommandHandler getAdminCommandHandler(final String adminCommand)
	{
		String command = adminCommand;

		int index;
		if((index = adminCommand.indexOf(" ")) != -1)
			command = adminCommand.substring(0, index);

		return _datatable.get(command);
	}

	public void useAdminCommandHandler(final L2Player activeChar, final String adminCommand)
	{
		if(!(activeChar.isGM() || activeChar.getPlayerAccess().UseGMComand))
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.SendBypassBuildCmd.NoCommandOrAccess", activeChar).addString(adminCommand));
			return;
		}

		final String[] wordList = adminCommand.split(" ");
		final IAdminCommandHandler handler = _datatable.get(wordList[0]);
		if(handler != null)
		{
			int command_success = 0;
			try
			{
				for(final String e : handler.getAdminCommandList())
					if(e.toString().equalsIgnoreCase(wordList[0]))
					{
						command_success = handler.useAdminCommand(adminCommand, activeChar) ? 1 : 0;
						break;
					}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
			Log.LogCommand(activeChar, Log.CMD_ADMH, adminCommand, command_success);
		}
	}

	/**
	 * Алгоритм не оптимальный, но затраты на построение списка команд мизерны
	 * по сравнению со всем другим
	 * 
	 * @param command
	 *            команда для поиска хелпа
	 * @return хелп к команде
	 */
	public String getDescriptionForCommand(final String command)
	{
		final String hlp = "Описание отсутсвует";

		final IAdminCommandHandler handler = _datatable.get(command);
		for(int i = 0; i < handler.getAdminCommandList().length; i++)
			if(handler.getAdminCommandList()[i].equals(command))
			{
				final String[] descList = handler.getDescription();
				return descList != null ? descList[i] : hlp;
			}
		return hlp;
	}

	/**
	 * Ищет группу к команде
	 * 
	 * @param command
	 *            команда для поиска группы
	 * @return группа команды
	 */
	public String getGroupForCommand(final String command)
	{
		final String group = GROUP_NONE;

		final IAdminCommandHandler handler = _datatable.get(command);
		for(int i = 0; i < handler.getAdminCommandList().length; i++)
			if(handler.getAdminCommandList()[i].equals(command))
			{
				final String[] groupList = handler.getCommandGroup();
				return groupList != null ? groupList[i] : group;
			}
		return group;
	}

	/**
	 * @return размер комманд
	 */
	@Override
	public int size()
	{
		return _datatable.size();
	}

	public void clear()
	{
		_datatable.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final AdminCommandHandler _instance = new AdminCommandHandler();
	}

	/**
	 * Получение списка зарегистрированных админ команд
	 * 
	 * @return список команд
	 */
	public Set<String> getAllCommands()
	{
		return _datatable.keySet();
	}
}
