package l2n.game.handler;

import l2n.game.handler.interfaces.ISkillTargetTypeHandler;
import l2n.game.handler.targetshandler.*;
import l2n.game.model.L2Skill.SkillTargetType;

import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 05.08.2011
 * @time 10:23:58
 */
public class SkillTargetTypeHandler
{
	private final static Logger _log = Logger.getLogger(SkillTargetTypeHandler.class.getName());

	private final ISkillTargetTypeHandler[] _datatable;

	public static SkillTargetTypeHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	private SkillTargetTypeHandler()
	{
		_datatable = new ISkillTargetTypeHandler[SkillTargetType.values().length];
		registerSkillTargetType(new TargetOne());
		registerSkillTargetType(new TargetArea());
		registerSkillTargetType(new TargetAura());
		registerSkillTargetType(new TargetParty());
		registerSkillTargetType(new TargetClan());
		registerSkillTargetType(new TargetPartyClan());
		registerSkillTargetType(new TargetAlly());

		_log.config("SkillTargetTypeHandler: Loaded " + size() + " handlers.");

		for(final SkillTargetType type : SkillTargetType.values())
			type.setHandler(_datatable[type.ordinal()]);
	}

	public void registerSkillTargetType(final ISkillTargetTypeHandler handler)
	{
		for(final SkillTargetType type : handler.getTargetType())
		{
			if(_datatable[type.ordinal()] != null)
				_log.info("SkillTargetTypeHandler: replace handler " + _datatable[type.ordinal()].getClass().getSimpleName() + " -> " + handler.getClass().getSimpleName() + " for targetType [" + type + "]");
			_datatable[type.ordinal()] = handler;
		}
	}

	/**
	 * @return
	 */
	public int size()
	{
		return _datatable.length;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final SkillTargetTypeHandler _instance = new SkillTargetTypeHandler();
	}
}
