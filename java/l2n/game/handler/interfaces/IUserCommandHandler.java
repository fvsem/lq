package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Player;

public interface IUserCommandHandler
{
	public boolean useUserCommand(int id, L2Player activeChar);

	public int[] getUserCommandList();
}
