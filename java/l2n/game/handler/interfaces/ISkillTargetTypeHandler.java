package l2n.game.handler.interfaces;

import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;

public interface ISkillTargetTypeHandler
{
	public L2Character[] getTargetList(final L2Skill skill, final L2Character activeChar, final L2Character aimingTarget, final boolean forceUse);

	public SkillTargetType[] getTargetType();
}
