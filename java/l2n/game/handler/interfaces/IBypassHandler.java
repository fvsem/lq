package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

public interface IBypassHandler
{
	public static Logger _log = Logger.getLogger(IBypassHandler.class.getName());

	public boolean useBypass(final String command, final L2Player activeChar, final L2Character target);

	public String[] getBypassList();
}
