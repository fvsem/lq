package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;

public interface IItemHandler
{
	public void useItem(L2Playable playable, L2ItemInstance item);

	public int[] getItemIds();
}
