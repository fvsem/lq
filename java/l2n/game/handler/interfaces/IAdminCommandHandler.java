package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Player;

public interface IAdminCommandHandler
{
	public boolean useAdminCommand(String command, L2Player activeChar);

	public String[] getAdminCommandList();

	public String[] getDescription();

	public String[] getCommandGroup();
}
