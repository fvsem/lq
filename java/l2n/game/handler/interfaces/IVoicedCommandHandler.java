package l2n.game.handler.interfaces;

import l2n.game.model.actor.L2Player;

public interface IVoicedCommandHandler
{
	public boolean useVoicedCommand(String command, L2Player activeChar, String target);

	public String[] getVoicedCommandList();
}
