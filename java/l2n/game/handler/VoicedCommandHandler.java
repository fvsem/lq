package l2n.game.handler;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.handler.voicedcommandhandler.*;

/**
 * This class ...
 * 
 * 13.05.2013
 */
public class VoicedCommandHandler implements ICommandHandler
{
	private final TIntObjectHashMap<IVoicedCommandHandler> _datatable;

	public static VoicedCommandHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	private VoicedCommandHandler()
	{
		_datatable = new TIntObjectHashMap<IVoicedCommandHandler>();

		registerVoicedCommandHandler(new AutoLoot());
		registerVoicedCommandHandler(new Buffer());
		registerVoicedCommandHandler(new Help());
		registerVoicedCommandHandler(new Lang());
		registerVoicedCommandHandler(new Lock());
		registerVoicedCommandHandler(new Offline());
		registerVoicedCommandHandler(new Password());
		registerVoicedCommandHandler(new PlayerCastleDoors());
		registerVoicedCommandHandler(new Relocate());
		registerVoicedCommandHandler(new Repair());
		registerVoicedCommandHandler(new SaveLocation());
		registerVoicedCommandHandler(new Status());
		registerVoicedCommandHandler(new Event());
		registerVoicedCommandHandler(new Pass());
		registerVoicedCommandHandler(new Wedding());
		registerVoicedCommandHandler(new Hellbound());
		registerVoicedCommandHandler(new Clicker());
	}

	public void registerVoicedCommandHandler(final IVoicedCommandHandler handler)
	{
		final String[] ids = handler.getVoicedCommandList();
		for(final String element : ids)
			if(!Config.COMMAND_FORBIDDEN_COMMAND_LIST.contains(element))
				_datatable.put(element.hashCode(), handler);
	}

	public IVoicedCommandHandler getVoicedCommandHandler(final String voicedCommand)
	{
		String command = voicedCommand;
		int index;
		if((index = voicedCommand.indexOf(" ")) != -1)
			command = voicedCommand.substring(0, index);
		return _datatable.get(command.hashCode());
	}

	/**
	 * @return
	 */
	@Override
	public int size()
	{
		return _datatable.size();
	}

	public void clear()
	{
		_datatable.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final VoicedCommandHandler _instance = new VoicedCommandHandler();
	}
}
