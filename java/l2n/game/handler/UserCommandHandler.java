package l2n.game.handler;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.handler.usercommandhandlers.*;

import java.util.logging.Logger;

public class UserCommandHandler implements ICommandHandler
{
	protected static final Logger _log = Logger.getLogger(UserCommandHandler.class.getName());

	private final TIntObjectHashMap<IUserCommandHandler> _datatable;

	public static UserCommandHandler getInstance()
	{
		return SingletonHolder._instance;
	}

	private UserCommandHandler()
	{
		_datatable = new TIntObjectHashMap<IUserCommandHandler>();

		registerUserCommandHandler(new ClanWarsList());
		registerUserCommandHandler(new CommandChannel());
		registerUserCommandHandler(new Escape());
		registerUserCommandHandler(new InstanceZone());
		registerUserCommandHandler(new Loc());
		registerUserCommandHandler(new Mount());
		registerUserCommandHandler(new MyBirthday());
		registerUserCommandHandler(new DisMount());
		registerUserCommandHandler(new OlympiadStat());
		registerUserCommandHandler(new PartyInfo());
		registerUserCommandHandler(new Penalty());
		registerUserCommandHandler(new ResetName());
		registerUserCommandHandler(new Time());
	}

	public void registerUserCommandHandler(final IUserCommandHandler handler)
	{
		final int[] ids = handler.getUserCommandList();
		for(final int element : ids)
			_datatable.put(element, handler);
	}

	public IUserCommandHandler getUserCommandHandler(final int userCommand)
	{
		return _datatable.get(userCommand);
	}

	/**
	 * @return
	 */
	@Override
	public int size()
	{
		return _datatable.size();
	}

	public void clear()
	{
		_datatable.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final UserCommandHandler _instance = new UserCommandHandler();
	}
}
