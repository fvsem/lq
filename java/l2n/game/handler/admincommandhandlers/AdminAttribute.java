package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.NpcHtmlMessage;

public class AdminAttribute implements IAdminCommandHandler
{
	private static String[] adminCommands = {
			"admin_setatreh",
			"admin_setatrec",
			"admin_setatreg",
			"admin_setatrel",
			"admin_setatreb",
			"admin_setatrew",
			"admin_setatres",
			"admin_setatrle",
			"admin_setatrre",
			"admin_setatrlf",
			"admin_setatrrf",
			"admin_setatren",
			"admin_setatrun",
			"admin_setatrbl",
			"admin_attribute" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanEnchant)
			return false;

		if(command.equals("admin_attribute"))
		{
			showMainPage(activeChar);
			return true;
		}
		else
		{
			int armorType = -1;

			if(command.startsWith("admin_setatreh"))
				armorType = Inventory.PAPERDOLL_HEAD;
			else if(command.startsWith("admin_setatrec"))
				armorType = Inventory.PAPERDOLL_CHEST;
			else if(command.startsWith("admin_setatreg"))
				armorType = Inventory.PAPERDOLL_GLOVES;
			else if(command.startsWith("admin_setatreb"))
				armorType = Inventory.PAPERDOLL_FEET;
			else if(command.startsWith("admin_setatrel"))
				armorType = Inventory.PAPERDOLL_LEGS;
			else if(command.startsWith("admin_setatrew"))
				armorType = Inventory.PAPERDOLL_RHAND;
			else if(command.startsWith("admin_setatres"))
				armorType = Inventory.PAPERDOLL_LHAND;
			else if(command.startsWith("admin_setatrle"))
				armorType = Inventory.PAPERDOLL_LEAR;
			else if(command.startsWith("admin_setatrre"))
				armorType = Inventory.PAPERDOLL_REAR;
			else if(command.startsWith("admin_setatrlf"))
				armorType = Inventory.PAPERDOLL_LFINGER;
			else if(command.startsWith("admin_setatrrf"))
				armorType = Inventory.PAPERDOLL_RFINGER;
			else if(command.startsWith("admin_setatren"))
				armorType = Inventory.PAPERDOLL_NECK;
			else if(command.startsWith("admin_setatrun"))
				armorType = Inventory.PAPERDOLL_UNDER;

			final String[] wordList = command.split(" ");
			if(armorType == -1 || wordList.length < 2 || activeChar.getInventory().getPaperdollItem(armorType) == null)
			{
				showMainPage(activeChar);
				return true;
			}

			try
			{
				final int ench = Integer.parseInt(wordList[1]);
				byte element = -2;

				if(wordList[2].equals("Fire"))
					element = 0;
				if(wordList[2].equals("Water"))
					element = 1;
				if(wordList[2].equals("Wind"))
					element = 2;
				if(wordList[2].equals("Earth"))
					element = 3;
				if(wordList[2].equals("Holy"))
					element = 4;
				if(wordList[2].equals("Dark"))
					element = 5;

				// check value
				if(ench < 0 || ench > 450)
					activeChar.sendMessage("You must set the enchant level for ARMOR to be between 0-300.");
				else
					setEnchant(activeChar, ench, element, armorType);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Please specify a new enchant value.");
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Please specify a valid new enchant value.");
			}

			// show the enchant menu after an action
			showMainPage(activeChar);
			return true;
		}
	}

	private void setEnchant(final L2Player activeChar, final int value, final byte element, final int armorType)
	{
		L2Object target = activeChar.getTarget();
		if(target == null)
			target = activeChar;
		if(!target.isPlayer())
		{
			activeChar.sendMessage("Wrong target type.");
			return;
		}

		final L2Player player = (L2Player) target;

		final L2ItemInstance item = player.getInventory().getPaperdollItem(armorType);
		if(item != null)
		{
			item.setAttributeElement(element, value, true);
			player.getInventory().refreshListeners();
			player.sendPacket(new InventoryUpdate().addModifiedItem(item));
			player.sendChanges();
		}
	}

	private void showMainPage(final L2Player activeChar)
	{
		activeChar.sendPacket(new NpcHtmlMessage(5).setFile("data/html/admin/attribute.htm"));
	}

	private static String[] commandDescriptions = {
			"описание admin_setatreh",
			"описание admin_setatrec",
			"описание admin_setatreg",
			"описание admin_setatrel",
			"описание admin_setatreb",
			"описание admin_setatrew",
			"описание admin_setatres",
			"описание admin_setatrle",
			"описание admin_setatrre",
			"описание admin_setatrlf",
			"описание admin_setatrrf",
			"описание admin_setatren",
			"описание admin_setatrun",
			"описание admin_setatrbl",
			"описание admin_attribute" };

	private static String[] commandGroups = {
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR,
			AdminCommandHandler.GROUP_EDITCHAR };

	@Override
	public String[] getAdminCommandList()
	{
		return adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return commandDescriptions;
	}

	@Override
	public String[] getCommandGroup()
	{
		return commandGroups;
	}
}
