package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.Announcements;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.AutoBan;
import l2n.util.Log;
import l2n.util.Util;

import java.util.StringTokenizer;

public class AdminNochannel implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_n",
			"admin_nochannel",
			"admin_penalty" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanBanChat)
			return false;

		int banChatCount = 0;
		int penaltyCount = 0;
		final int banChatCountPerDay = activeChar.getPlayerAccess().BanChatCountPerDay;
		if(banChatCountPerDay > -1)
		{
			final String count = activeChar.getVar("banChatCount");
			if(count != null)
				banChatCount = Integer.parseInt(count);

			final String penalty = activeChar.getVar("penaltyChatCount");
			if(penalty != null)
				penaltyCount = Integer.parseInt(penalty);

			long LastBanChatDayTime = 0;
			final String time = activeChar.getVar("LastBanChatDayTime");
			if(time != null)
				LastBanChatDayTime = Long.parseLong(time);

			if(LastBanChatDayTime != 0)
			{
				if(System.currentTimeMillis() - LastBanChatDayTime < 1000 * 60 * 60 * 24)
				{
					if(banChatCount >= banChatCountPerDay)
					{
						activeChar.sendMessage("В сутки, вы можете выдать не более " + banChatCount + " банов чата.");
						return false;
					}
				}
				else
				{
					int bonus_mod = banChatCount / 10;
					bonus_mod = Math.max(1, bonus_mod);
					bonus_mod = 1; // Убрать, если потребуется сделать зависимость бонуса от количества банов
					if(activeChar.getPlayerAccess().BanChatBonusId > 0 && activeChar.getPlayerAccess().BanChatBonusCount > 0)
					{
						int add_count = activeChar.getPlayerAccess().BanChatBonusCount * bonus_mod;

						final L2Item item = ItemTable.getInstance().getTemplate(activeChar.getPlayerAccess().BanChatBonusId);
						activeChar.sendMessage("Бонус за модерирование: " + add_count + " " + item.getName());

						if(penaltyCount > 0) // У игрока был штраф за нарушения
						{
							activeChar.sendMessage("Штраф за нарушения: " + penaltyCount + " " + item.getName());
							activeChar.setVar("penaltyChatCount", "" + Math.max(0, penaltyCount - add_count)); // Уменьшаем штраф
							add_count -= penaltyCount; // Вычитаем штраф из бонуса
						}

						if(add_count > 0)
							Functions.addItem(activeChar, activeChar.getPlayerAccess().BanChatBonusId, add_count);
					}
					activeChar.setVar("LastBanChatDayTime", "" + System.currentTimeMillis());
					activeChar.setVar("banChatCount", "0");
					banChatCount = 0;
				}
			}
			else
				activeChar.setVar("LastBanChatDayTime", "" + System.currentTimeMillis());
		}

		if(!activeChar.getPlayerAccess().CanBanChat)
			return false;

		if(command.startsWith("admin_n") || command.startsWith("admin_nochannel"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			if(st.countTokens() > 1)
			{
				st.nextToken();
				final String player = st.nextToken();

				int timeval = 30; // if no args, then 30 min default.
				String reason = "не указана"; // if no args, then "не указана" default.

				if(st.countTokens() >= 1)
				{
					final String time = st.nextToken();
					timeval = Integer.parseInt(time);
				}

				if(st.countTokens() >= 1)
				{
					reason = st.nextToken();
					while (st.hasMoreTokens())
						reason += " " + st.nextToken();
				}

				final String msg = Nochannel(activeChar, activeChar.getName(), player, timeval, reason);
				activeChar.sendMessage(msg);

				if(banChatCountPerDay > -1 && msg.startsWith("Вы забанили чат"))
				{
					banChatCount++;
					activeChar.setVar("banChatCount", "" + banChatCount);
					activeChar.sendMessage("У вас осталось " + (banChatCountPerDay - banChatCount) + " банов чата.");
				}
			}
			else
			{
				activeChar.sendMessage("USAGE: //nochannel charName [period] [reason]");
				return false;
			}
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	public static String Nochannel(final L2Player adminChar, String adminName, String charName, final int val, String reason)
	{
		final L2Player player = L2ObjectsStorage.getPlayer(charName);

		if(player != null)
			charName = player.getName();
		else if(Util.GetCharIDbyName(charName) == 0)
			return "Игрок " + charName + " не найден.";
		if((adminName == null || adminName.isEmpty()) && adminChar != null)
			adminName = adminChar.getName();
		if(reason == null || reason.isEmpty())
			reason = "не указана";
		String announce = null;
		String result;
		if(val == 0)
		{
			if(adminChar != null && !adminChar.getPlayerAccess().CanUnBanChat)
				return "Вы не имеете прав на снятие бана чата.";
			if(Config.MAT_ANNOUNCE && !Config.MAT_ANNOUNCE_NICK)
				announce = "С игрока " + charName + " снят бан чата.";
			else if(Config.MAT_ANNOUNCE && Config.MAT_ANNOUNCE_NICK)
				announce = adminName + " снял с игрока " + charName + " бан чата.";
			Log.add(adminName + " снял бан чата с игрока " + charName + ".", "banchat", adminChar);
			result = "Вы сняли бан чата с игрока " + charName + ".";
		}
		else if(val < 0)
		{
			if(adminChar != null && adminChar.getPlayerAccess().BanChatMaxValue > 0)
				return "Вы можете банить не более чем на " + adminChar.getPlayerAccess().BanChatMaxValue + " минут.";
			if(Config.MAT_ANNOUNCE && !Config.MAT_ANNOUNCE_NICK)
				announce = "Забанен чат игроку " + charName + " на бессрочный период, причина: " + reason + ".";
			else if(Config.MAT_ANNOUNCE && Config.MAT_ANNOUNCE_NICK)
				announce = adminName + " забанил чат игроку " + charName + " на бессрочный период, причина: " + reason + ".";
			Log.add(adminName + " забанил чат игроку " + charName + " на бессрочный период, причина: " + reason + ".", "banchat", adminChar);
			result = "Вы забанили чат игроку " + charName + " на бессрочный период.";
		}
		else
		{
			if(adminChar != null && !adminChar.getPlayerAccess().CanUnBanChat && (player == null || player.getNoChannel() != 0))
				return "Вы не имеете права изменять время бана.";
			if(adminChar != null && adminChar.getPlayerAccess().BanChatMaxValue != -1 && val > adminChar.getPlayerAccess().BanChatMaxValue)
				return "Вы можете банить не более чем на " + adminChar.getPlayerAccess().BanChatMaxValue + " минут.";
			if(Config.MAT_ANNOUNCE && !Config.MAT_ANNOUNCE_NICK)
				announce = "Забанен чат игроку " + charName + " на " + val + " минут, причина: " + reason + ".";
			else if(Config.MAT_ANNOUNCE && Config.MAT_ANNOUNCE_NICK)
				announce = adminName + " забанил чат игроку " + charName + " на " + val + " минут, причина: " + reason + ".";
			Log.add(adminName + " забанил чат игроку " + charName + " на " + val + " минут, причина: " + reason + ".", "banchat", adminChar);
			result = "Вы забанили чат игроку " + charName + " на " + val + " минут.";
		}
		if(player != null)
			updateNoChannel(player, val);
		else
			AutoBan.ChatBan(charName, val, reason, adminName);
		if(announce != null)
			if(Config.MAT_ANNOUNCE_FOR_ALL_WORLD)
				Announcements.announceToAll(announce);
			else
				Announcements.shout(adminChar, announce, Say2C.CRITICAL_ANNOUNCEMENT);
		return result;
	}

	private static void updateNoChannel(final L2Player player, final int time)
	{
		player.updateNoChannel(time * 60000);
		if(time == 0)
			player.sendMessage(new CustomMessage("common.ChatUnBanned", player));
		else if(time > 0)
			player.sendMessage(new CustomMessage("common.ChatBanned", player).addNumber(time));
		else
			player.sendMessage(new CustomMessage("common.ChatBannedPermanently", player));
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды n",
				"Описание команды nochannel",
				"Описание команды penalty" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
