package l2n.game.handler.admincommandhandlers;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.StringTokenizer;

public class AdminTeleport implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_show_moves",
			"admin_show_moves_other",
			"admin_show_teleport",
			"admin_teleport_to_character",
			"admin_teleportto",
			"admin_tt",
			"admin_move_to",
			"admin_teleport_character",
			"admin_recall",
			"admin_walk",
			"admin_recall_npc",
			"admin_gonorth",
			"admin_gosouth",
			"admin_goeast",
			"admin_gowest",
			"admin_goup",
			"admin_godown",
			"admin_tele",
			"admin_teleto",
			"admin_failed",
			"admin_tonpc",
			"admin_correct_merchants",
			"admin_sendhome",
			"admin_instant_move" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanTeleport)
			return false;

		if(command.equals("admin_correct_merchants"))
		{
			final Location[] list = new Location[10];
			list[0] = new Location(82545, 148604, -3495);
			list[1] = new Location(81930, 149193, -3495);
			list[2] = new Location(81375, 149103, -3495);
			list[3] = new Location(81290, 148618, -3495);
			list[4] = new Location(81413, 148125, -3495);
			list[5] = new Location(81923, 148013, -3495);
			list[6] = new Location(82471, 148124, -3495);
			list[7] = new Location(82477, 149107, -3495);
			list[8] = new Location(83496, 148624, -3431);
			list[9] = new Location(84300, 147409, -3431);

			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE && !activeChar.isInOfflineMode())
				{
					Location loc = list[Rnd.get(list.length)];
					loc = Rnd.coordsRandomize(loc.x, loc.y, loc.z, player.getHeading(), 100, 400);
					player.teleToLocation(loc);
				}

			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player.isInOfflineMode())
				{
					Location loc = list[Rnd.get(list.length)];
					loc = Rnd.coordsRandomize(loc.x, loc.y, loc.z, player.getHeading(), 100, 400);
					player.decayMe();
					player.setXYZ(loc.x, loc.y, loc.z);
					player.spawnMe();
					System.out.println(player.getName());
				}
		}
		else if(command.startsWith("admin_sendhome"))
		{
			final String charName = command.substring(11);
			if(!charName.isEmpty())
			{
				L2Player player = L2ObjectsStorage.getPlayer(charName);
				if(player != null && player.isOnline() && !player.isInOfflineMode())
					player.teleToClosestTown();
			}
		}
		if(command.equals("admin_teleto"))
			activeChar.setTeleMode(1);
		if(command.equals("admin_teleto r"))
			activeChar.setTeleMode(2);
		if(command.equalsIgnoreCase("admin_instant_move"))
		{
			if(activeChar.getTeleMode() > 0)
				activeChar.setTeleMode(0);
			else
				activeChar.setTeleMode(2);
		}
		if(command.equals("admin_teleto end"))
			activeChar.setTeleMode(0);
		if(command.equals("admin_show_moves"))
			AdminHelpPage.showHelpPage(activeChar, "teleports.htm");
		if(command.equals("admin_show_moves_other"))
			AdminHelpPage.showHelpPage(activeChar, "tele/other.htm");
		else if(command.equals("admin_show_teleport"))
			showTeleportCharWindow(activeChar);
		else if(command.equals("admin_recall_npc"))
			recallNPC(activeChar);
		else if(command.equals("admin_teleport_to_character"))
			teleportToCharacter(activeChar, activeChar.getTarget());
		else if(command.startsWith("admin_walk"))
			try
			{
				final String val = command.substring(11);
				final StringTokenizer st = new StringTokenizer(val);
				final String x1 = st.nextToken();
				final int x = Integer.parseInt(x1);
				final String y1 = st.nextToken();
				final int y = Integer.parseInt(y1);
				final String z1 = st.nextToken();
				final int z = Integer.parseInt(z1);
				final Location pos = new Location(x, y, z);
				activeChar.moveToLocation(pos, 0, true);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
		else if(command.startsWith("admin_move_to"))
			try
			{
				final String val = command.substring(14);
				teleportTo(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty coordinates
				activeChar.sendMessage("Wrong or no Coordinates given.");
			}
		else if(command.startsWith("admin_teleport_character"))
			try
			{
				final String val = command.substring(25);

				teleportCharacter(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				// Case of empty coordinates
				activeChar.sendMessage("Wrong or no Coordinates given.");
				showTeleportCharWindow(activeChar); // back to character teleport
			}
		else if(command.startsWith("admin_teleportto ") || command.startsWith("admin_tt "))
			try
			{
				final String targetName = command.substring(17);
				final L2Player player = L2ObjectsStorage.getPlayer(targetName);
				teleportToCharacter(activeChar, player);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_recall "))
			try
			{
				final String targetName = command.substring(13);
				final L2Player recall_player = L2ObjectsStorage.getPlayer(targetName);
				if(recall_player != null)
				{
					teleportTo(activeChar, recall_player, activeChar.getLoc());
					return true;
				}
				final int obj_id = Util.GetCharIDbyName(targetName);
				if(obj_id > 0)
				{
					teleportCharacter_offline(obj_id, activeChar.getLoc());
					activeChar.sendMessage(targetName + " is offline. Offline teleport used...");
					Log.add("teleport player " + targetName + " to " + activeChar.getLoc(), "gm_ext_actions", activeChar);
				}
				else
					activeChar.sendMessage("->" + targetName + "<- is incorrect.");
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_failed"))
		{
			activeChar.sendMessage("Trying ActionFailed...");
			activeChar.sendActionFailed();
		}
		else if(command.equals("admin_tele"))
			showTeleportWindow(activeChar);
		else if(command.equals("admin_goup"))
		{
			activeChar.teleToLocation(activeChar.getLoc().changeZ(150));
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_goup"))
			try
			{
				final String val = command.substring(11);
				activeChar.teleToLocation(activeChar.getLoc().changeZ(Integer.parseInt(val)));
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_godown"))
		{
			activeChar.teleToLocation(activeChar.getLoc().changeZ(-150));
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_godown"))
			try
			{
				final String val = command.substring(13);
				activeChar.teleToLocation(activeChar.getLoc().changeZ(-Integer.parseInt(val)));
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_goeast"))
		{
			final int x = activeChar.getX();
			final int y = activeChar.getY();
			final int z = activeChar.getZ();
			activeChar.teleToLocation(x + 150, y, z);
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_goeast"))
			try
			{
				final String val = command.substring(13);
				final int intVal = Integer.parseInt(val);
				final int x = activeChar.getX() + intVal;
				final int y = activeChar.getY();
				final int z = activeChar.getZ();
				activeChar.teleToLocation(x, y, z);
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_gowest"))
		{
			final int x = activeChar.getX();
			final int y = activeChar.getY();
			final int z = activeChar.getZ();
			activeChar.teleToLocation(x - 150, y, z);
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_gowest"))
			try
			{
				final String val = command.substring(13);
				final int intVal = Integer.parseInt(val);
				final int x = activeChar.getX() - intVal;
				final int y = activeChar.getY();
				final int z = activeChar.getZ();
				activeChar.teleToLocation(x, y, z);
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_gosouth"))
		{
			final int x = activeChar.getX();
			final int y = activeChar.getY() + 150;
			final int z = activeChar.getZ();
			activeChar.teleToLocation(x, y, z);
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_gosouth"))
			try
			{
				final String val = command.substring(14);
				final int intVal = Integer.parseInt(val);
				final int x = activeChar.getX();
				final int y = activeChar.getY() + intVal;
				final int z = activeChar.getZ();
				activeChar.teleToLocation(x, y, z);
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_gonorth"))
		{
			final int x = activeChar.getX();
			final int y = activeChar.getY();
			final int z = activeChar.getZ();
			activeChar.teleToLocation(x, y - 150, z);
			showTeleportWindow(activeChar);
		}
		else if(command.startsWith("admin_gonorth"))
			try
			{
				final String val = command.substring(14);
				final int intVal = Integer.parseInt(val);
				final int x = activeChar.getX();
				final int y = activeChar.getY() - intVal;
				final int z = activeChar.getZ();
				activeChar.teleToLocation(x, y, z);
				showTeleportWindow(activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_tonpc"))
			try
			{
				final Integer npc_id = Integer.parseInt(command.substring(12));
				final L2NpcInstance npc = L2ObjectsStorage.getByNpcId(npc_id);
				teleportToCharacter(activeChar, npc);
			}
			catch(final Exception e)
			{
				try
				{
					final String npc_name = command.substring(12);
					final L2NpcInstance npc = L2ObjectsStorage.getNpc(npc_name);
					teleportToCharacter(activeChar, npc);
				}
				catch(final Exception e2)
				{
					activeChar.sendMessage("НПС не найден");
				}
			}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void teleportTo(final L2Player activeChar, final String Cords)
	{
		final StringTokenizer st = new StringTokenizer(Cords);
		if(st.countTokens() < 3)
		{
			activeChar.sendMessage("3 координаты, необходимые для телепортации");
			return;
		}

		final String x1 = st.nextToken();
		final int x = Integer.parseInt(x1);
		final String y1 = st.nextToken();
		final int y = Integer.parseInt(y1);
		final String z1 = st.nextToken();
		final int z = Integer.parseInt(z1);

		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);
		activeChar.teleToLocation(x, y, z);

		activeChar.sendMessage("Вы были телепортированы  " + Cords);

		Log.add("teleported to " + Cords, "gm_ext_actions", activeChar);
	}

	private void teleportTo(final L2Player activeChar, final L2Player target, final Location loc)
	{
		if(!target.equals(activeChar))
			target.sendMessage("Администратор телепортировал вас.");

		target.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		target.teleToLocation(loc, activeChar.getReflection().getId());

		if(target.equals(activeChar))
		{
			activeChar.sendMessage("Вы были телепортированы " + loc);
			Log.add("teleported to " + loc, "gm_ext_actions", activeChar);
		}
		else
			Log.add("teleported player " + target + " to " + loc, "gm_ext_actions", activeChar);
	}

	private void teleportCharacter_offline(final int obj_id, final Location loc)
	{
		if(obj_id == 0)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement("UPDATE characters SET x=?,y=?,z=? WHERE obj_Id=? LIMIT 1");
			st.setInt(1, loc.x);
			st.setInt(2, loc.y);
			st.setInt(3, loc.z);
			st.setInt(4, obj_id);
			st.executeUpdate();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}
	}

	private void showTeleportWindow(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><title>Teleport Menu</title>");
		replyMSG.append("<body>");

		replyMSG.append("<br>");
		replyMSG.append("<center><table>");

		replyMSG.append("<tr><td><button value=\"  \" action=\"bypass -h admin_tele\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"North\" action=\"bypass -h admin_gonorth\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Up\" action=\"bypass -h admin_goup\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"West\" action=\"bypass -h admin_gowest\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"  \" action=\"bypass -h admin_tele\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"East\" action=\"bypass -h admin_goeast\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"  \" action=\"bypass -h admin_tele\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"South\" action=\"bypass -h admin_gosouth\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Down\" action=\"bypass -h admin_godown\" width=70 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");

		replyMSG.append("</table></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void showTeleportCharWindow(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target.isPlayer())
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><title>Teleport Character</title>");
		replyMSG.append("<body>");
		replyMSG.append("The character you will teleport is " + player.getName() + ".");
		replyMSG.append("<br>");

		replyMSG.append("Co-ordinate x");
		replyMSG.append("<edit var=\"char_cord_x\" width=110>");
		replyMSG.append("Co-ordinate y");
		replyMSG.append("<edit var=\"char_cord_y\" width=110>");
		replyMSG.append("Co-ordinate z");
		replyMSG.append("<edit var=\"char_cord_z\" width=110>");
		replyMSG.append("<button value=\"Teleport\" action=\"bypass -h admin_teleport_character $char_cord_x $char_cord_y $char_cord_z\" width=60 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
		replyMSG.append("<button value=\"Teleport near you\" action=\"bypass -h admin_teleport_character " + activeChar.getX() + " " + activeChar.getY() + " " + activeChar.getZ() + "\" width=115 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
		replyMSG.append("<center><button value=\"Back\" action=\"bypass -h admin_current_player\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void teleportCharacter(final L2Player activeChar, final String Cords)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target.isPlayer())
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		if(player.getObjectId() == activeChar.getObjectId())
			player.sendMessage("Вы не можете телепортироваться.");
		else
		{
			final StringTokenizer st = new StringTokenizer(Cords);
			final String x1 = st.nextToken();
			final int x = Integer.parseInt(x1);
			final String y1 = st.nextToken();
			final int y = Integer.parseInt(y1);
			final String z1 = st.nextToken();
			final int z = Integer.parseInt(z1);
			teleportCharacter(player, new Location(x, y, z));
		}
	}

	/**
	 * @param player
	 * @param loc
	 *            Location
	 */
	private void teleportCharacter(final L2Player player, final Location loc)
	{
		if(player != null)
		{
			// Common character information
			player.sendMessage("Администратор телепортировал вас.");
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);
			player.teleToLocation(loc);
		}
	}

	private void teleportToCharacter(final L2Player activeChar, final L2Object target)
	{
		if(target == null)
			return;

		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);
		activeChar.teleToLocation(target.getLoc().changeZ(25), target.getReflection().getId());

		activeChar.sendMessage("Вы телепортированы к " + target);
	}

	private void recallNPC(final L2Player activeChar)
	{
		final L2Object obj = activeChar.getTarget();
		if(obj != null && obj instanceof L2NpcInstance)
		{
			final L2NpcInstance target = (L2NpcInstance) obj;
			L2Spawn spawn = target.getSpawn();

			final int monsterTemplate = target.getTemplate().npcId;

			final L2NpcTemplate template1 = NpcTable.getTemplate(monsterTemplate);

			if(template1 == null)
			{
				activeChar.sendMessage("Incorrect monster template.");
				return;
			}

			final int respawnTime = spawn.getRespawnDelay();

			target.deleteMe();
			spawn.stopRespawn();

			try
			{
				// L2MonsterInstance mob = new L2MonsterInstance(monsterTemplate,
				// template1);

				spawn = new L2Spawn(template1);
				spawn.setLoc(activeChar.getLoc());
				spawn.setAmount(1);
				spawn.setReflection(activeChar.getReflection().getId());
				spawn.setRespawnDelay(respawnTime);
				spawn.init();

				activeChar.sendMessage("Created " + template1.name + " on " + target.getObjectId() + ".");

				Log.add("GM: " + activeChar.getName() + "(" + activeChar.getObjectId() + ") moved NPC" + target.getObjectId(), "gm_ext_actions", activeChar);
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Target is not in game.");
			}

		}
		else
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды show_moves",
				"Описание команды show_moves_other",
				"Описание команды show_teleport",
				"Описание команды teleport_to_character",
				"Описание команды teleportto",
				"Описание команды tt",
				"Описание команды move_to",
				"Описание команды teleport_character",
				"Описание команды recall",
				"Описание команды walk",
				"Описание команды recall_npc",
				"Описание команды gonorth",
				"Описание команды gosouth",
				"Описание команды goeast",
				"Описание команды gowest",
				"Описание команды goup",
				"Описание команды godown",
				"Описание команды tele",
				"Описание команды teleto",
				"Описание команды failed",
				"Описание команды tonpc",
				"Описание команды correct_merchants",
				"sendhome <char name> - отправить в ближайший город",
				"Описание команды instant_move" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT,
				AdminCommandHandler.GROUP_TELEPORT };
	}
}
