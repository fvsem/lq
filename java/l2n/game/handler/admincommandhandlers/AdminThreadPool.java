package l2n.game.handler.admincommandhandlers;

import l2n.game.L2GameThreadPools;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdminThreadPool implements IAdminCommandHandler
{
	private static final Logger _log = Logger.getLogger(AdminThreadPool.class.getName());

	private static final String[] _adminCommands = {
			"admin_setcore",
			"admin_setpool",
			"admin_alive_time",
			"admin_thread_timeout" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanRestart)
			return false;

		final String[] wordList = command.split(" ");
		try
		{
			if(command.startsWith("admin_setcore"))
			{
				if(wordList.length != 3)
				{
					activeChar.sendMessage("use //setcore <g|n|m|gp|cp> <NUMBER>");
					return false;
				}

				if(wordList[1].equals("g"))
				{
					L2GameThreadPools.getInstance().getGeneralScheduledThreadPool().setCorePoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New General theadpool core size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("n"))
				{
					L2GameThreadPools.getInstance().getAiScheduledThreadPool().setCorePoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New NpcAI theadpool core size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("m"))
				{
					L2GameThreadPools.getInstance().getMoveScheduledThreadPool().setCorePoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New Move theadpool core size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("gp"))
				{
					L2GameThreadPools.getInstance().getInGamePacketsThreadPool().setCorePoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New InGamePackets theadpool core size: " + Integer.parseInt(wordList[2]));
				}
			}
			else if(command.startsWith("admin_setpool"))
			{
				if(wordList.length != 3)
				{
					activeChar.sendMessage("use //setpool <g|n|m|gp|cp> <NUMBER>");
					return false;
				}

				if(wordList[1].equals("g"))
				{
					L2GameThreadPools.getInstance().getGeneralScheduledThreadPool().setMaximumPoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New General theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("n"))
				{
					L2GameThreadPools.getInstance().getAiScheduledThreadPool().setMaximumPoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New NpcAI theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("m"))
				{
					L2GameThreadPools.getInstance().getMoveScheduledThreadPool().setMaximumPoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New Move theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("gp"))
				{
					L2GameThreadPools.getInstance().getInGamePacketsThreadPool().setMaximumPoolSize(Integer.parseInt(wordList[2]));
					activeChar.sendMessage("New InGamePackets theadpool query size: " + Integer.parseInt(wordList[2]));
				}
			}
			else if(command.startsWith("admin_alive_time"))
			{
				if(wordList.length != 3)
				{
					activeChar.sendMessage("use //alive_time <g|n|m|gp|cp> <NUMBER>");
					return false;
				}

				if(wordList[1].equals("g"))
				{
					L2GameThreadPools.getInstance().getGeneralScheduledThreadPool().setKeepAliveTime(Integer.parseInt(wordList[2]), TimeUnit.SECONDS);
					activeChar.sendMessage("New General theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("n"))
				{
					L2GameThreadPools.getInstance().getAiScheduledThreadPool().setKeepAliveTime(Integer.parseInt(wordList[2]), TimeUnit.SECONDS);
					activeChar.sendMessage("New NpcAI theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("m"))
				{
					L2GameThreadPools.getInstance().getMoveScheduledThreadPool().setKeepAliveTime(Integer.parseInt(wordList[2]), TimeUnit.SECONDS);
					activeChar.sendMessage("New Move theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("gp"))
				{
					L2GameThreadPools.getInstance().getInGamePacketsThreadPool().setKeepAliveTime(Integer.parseInt(wordList[2]), TimeUnit.SECONDS);
					activeChar.sendMessage("New InGamePackets theadpool query size: " + Integer.parseInt(wordList[2]));
				}
			}
			else if(command.startsWith("admin_thread_timeout"))
			{
				if(wordList.length != 3)
				{
					activeChar.sendMessage("use //thread_timeout <g|n|m|gp|cp> <1 OR 0>");
					return false;
				}

				if(wordList[1].equals("g"))
				{
					L2GameThreadPools.getInstance().getGeneralScheduledThreadPool().allowCoreThreadTimeOut(Integer.parseInt(wordList[2]) == 1);
					activeChar.sendMessage("New General theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("n"))
				{
					L2GameThreadPools.getInstance().getAiScheduledThreadPool().allowCoreThreadTimeOut(Integer.parseInt(wordList[2]) == 1);
					activeChar.sendMessage("New NpcAI theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("m"))
				{
					L2GameThreadPools.getInstance().getMoveScheduledThreadPool().allowCoreThreadTimeOut(Integer.parseInt(wordList[2]) == 1);
					activeChar.sendMessage("New Move theadpool query size: " + Integer.parseInt(wordList[2]));
				}
				else if(wordList[1].equals("gp"))
				{
					L2GameThreadPools.getInstance().getInGamePacketsThreadPool().allowCoreThreadTimeOut(Integer.parseInt(wordList[2]) == 1);
					activeChar.sendMessage("New InGamePackets theadpool query size: " + Integer.parseInt(wordList[2]));
				}
			}
		}
		catch(final Exception e)
		{
			activeChar.sendMessage("Error!");
			_log.log(Level.WARNING, "", e);
			return false;
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды setcore",
				"Описание команды setpool",
				"Описание команды alive_time",
				"Описание команды thread_timeout" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_THREAD_POOL,
				AdminCommandHandler.GROUP_THREAD_POOL,
				AdminCommandHandler.GROUP_THREAD_POOL,
				AdminCommandHandler.GROUP_THREAD_POOL };
	}
}
