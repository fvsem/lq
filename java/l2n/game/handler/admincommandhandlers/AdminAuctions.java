package l2n.game.handler.admincommandhandlers;

import javolution.util.FastList;
import l2n.commons.list.GArray;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.l2auction.*;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ItemTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

public class AdminAuctions implements IAdminCommandHandler
{

	protected static Logger logger = Logger.getLogger("l2auction");

	private static String[] adminCommands = {
			"admin_auctions",
			"admin_auctioncreate",
			"admin_auctiondrop",
			"admin_auctionview",
			"admin_auction_start",
			"admin_auctioncancel",
			"admin_lotcreate",
			"admin_lotview",
			"admin_lotdrop",
			"admin_lotcancel",
			"admin_lotstart",
			"admin_lotdown",
			"admin_lotup",
			"admin_auction_ready",
			"admin_auctioncopy",
			"admin_auction_stat",
			"admin_auction_top" };

	private static String[] commandDescriptions = {
			"Просмотр аукционов",
			"Создание аукциона",
			"Удаление аукциона",
			"Просмотр аукциона",
			"Начало аукциона",
			"Отмена аукциона",
			"Создание лота",
			"Просмотр лота",
			"Удаление лота",
			"Отмена лота",
			"Старт лота",
			"Опустить лот ниже",
			"Поднять лот выше",
			"Выложить аукцион на запуск",
			"Копирование аукциона с уже существующего",
			"Статистика по аукциону",
			"Страница по аукционам" };

	private static String[] commandGroups = {
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS,
			AdminCommandHandler.GROUP_AUCTIONS };

	@Override
	public String[] getAdminCommandList()
	{
		return adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return commandDescriptions;
	}

	@Override
	public String[] getCommandGroup()
	{
		return commandGroups;
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player player)
	{
		String[] args = null;
		for(final String cmd : adminCommands)
			if(command.startsWith(cmd))
			{
				args = L2AuctionUtils.getCommandParams(command, cmd);
				break;
			}

		if(command.startsWith(adminCommands[0]))
			processAuctions(args, player);
		else if(command.startsWith(adminCommands[1]))
			processAuctionCreate(args, player);
		else if(command.startsWith(adminCommands[2]))
			processAuctionDrop(args, player);
		else if(command.startsWith(adminCommands[3]))
			processAuctionView(args, player);
		else if(command.startsWith(adminCommands[4]))
			processAuctionStart(args, player);
		else if(command.startsWith(adminCommands[5]))
			processAuctionCancel(args, player);
		else if(command.startsWith(adminCommands[6]))
			processAuctionLotCreate(args, player);
		else if(command.startsWith(adminCommands[7]))
			processAuctionLotView(args, player);
		else if(command.startsWith(adminCommands[8]))
			processAuctionLotDrop(args, player);
		else if(command.startsWith(adminCommands[9]))
			processAuctionLotCancel(args, player);
		else if(command.startsWith(adminCommands[10]))
			processAuctionLotStart(args, player);
		else if(command.startsWith(adminCommands[11]))
			processAuctionLotDown(args, player);
		else if(command.startsWith(adminCommands[12]))
			processAuctionLotUp(args, player);
		else if(command.startsWith(adminCommands[13]))
			processAuctionReady(args, player);
		else if(command.startsWith(adminCommands[14]))
			processAuctionCopy(args, player);
		else if(command.startsWith(adminCommands[15]))
			processAuctionStatistic(args, player);
		else if(command.startsWith(adminCommands[16]))
			processTop(args, player);

		return false;
	}

	private void processTop(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/top.htm");
			player.sendPacket(adminReply);
			return;
		}
		if(args[0].equalsIgnoreCase("money"))
			L2AuctionUtils.showTopMoney(player);
		else if(args[0].equalsIgnoreCase("count"))
			L2AuctionUtils.showTopCounts(player);
	}

	private void processAuctionStatistic(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
		{
			showError(player, "Аукциона с указанным идентификатором найти не удалось");
			return;
		}
		L2AuctionUtils.showAuctionStatistic(auction, player);
	}

	private void processAuctionCopy(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
		{
			showError(player, "Аукциона с указанным идентификатором найти не удалось");
			return;
		}
		if(args.length == 1)
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/auction_copy.htm");
			adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
			player.sendPacket(adminReply);
			return;
		}
		else
		{
			final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date = new Date();
			try
			{
				date = sdf.parse(args[1]);
			}
			catch(final Exception e)
			{
				logger.severe("Cannot parse dato for auction copy: " + args[1]);
			}
			final L2Auction newAuction = auction.copy(date);
			if(newAuction == null)
			{
				showError(player, "Не удалось скопировать аукцион.");
				return;
			}
			else
			{
				showAuction(newAuction.getId(), player, null);
				return;
			}
		}
	}

	/**
	 * Выставляем аукциону признак того, что он готов к запуску
	 * 
	 * @param args
	 * @param player
	 */
	private void processAuctionReady(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
		{
			showError(player, "Аукциона с указанным идентификатором найти не удалось");
			return;
		}

		if(auction.getStatus() != AuctionStatusEnum.init)
		{
			showError(player, "Оборзели чтоли? Этот аукцион уже давно не на этапе создания");
			return;
		}

		if(auction.getLots().size() == 0)
		{
			showAuctionError(auction, player, "Нет ни одного лота");
			return;
		}
/*
 * SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
 * if (auction.getStartDate()==null)
 * logger.info("auction start date is null");
 * else
 * logger.info("auction start date is "+sdf.format(auction.getStartDate()));
 * if (auction.getStopDate()==null)
 * logger.info("auction stop date is null");
 * else
 * logger.info("auction stop date is "+sdf.format(auction.getStopDate()));
 */

		if(auction.getStartDate().after(auction.getAdminStopDate()))
		{
			showAuctionError(auction, player, "А почему дата начала позже даты завершения?");
			return;
		}

		if(new Date().after(auction.getAdminStopDate()))
		{
			showAuctionError(auction, player, "А смысл? Время окончания прошло");
			return;
		}

		if(args.length > 1)
		{
			if(args[1].equalsIgnoreCase("yes"))
				auction.ready();
			showAuction(auctionId, player, null);
		}
		else
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/auction_ready.htm");
			adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
			player.sendPacket(adminReply);
		}

	}

	private void processAuctionLotDown(final String[] args, final L2Player player)
	{
		logger.info(Arrays.deepToString(args));
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long lotId = L2AuctionUtils.parseLong(args[0], 0);
		if(lotId == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		if(lot.getAuction().getStatus() != AuctionStatusEnum.init)
		{
			showAuctionError(lot.getAuction(), player, "Эта операция недопустима в данный момент");
			return;
		}
		L2Lot next = null;
		final FastList<L2Lot> lots = lot.getAuction().getLots();
		for(int i = 0; i < lots.size(); i++)
		{
			final L2Lot l = lots.get(i);
			if(l.getId() == lot.getId() && i < lots.size())
			{
				next = lots.get(i + 1);
				break;
			}
		}
		final int pos = lot.getPos();
		if(next != null)
		{
			lot.updatePosition(next.getPos());
			next.updatePosition(pos);
		}
		showAuction(lot.getAuction().getId(), player, null);
	}

	private void processAuctionLotUp(final String[] args, final L2Player player)
	{
		logger.info(Arrays.deepToString(args));
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long lotId = L2AuctionUtils.parseLong(args[0], 0);
		if(lotId == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		if(lot.getAuction().getStatus() != AuctionStatusEnum.init)
		{
			showAuctionError(lot.getAuction(), player, "Эта операция недопустима в данный момент");
			return;
		}
		L2Lot previous = null;
		final FastList<L2Lot> lots = lot.getAuction().getLots();
		for(int i = 0; i < lots.size(); i++)
		{
			final L2Lot l = lots.get(i);
			if(l.getId() == lot.getId() && i > 0)
			{
				previous = lots.get(i - 1);
				break;
			}
		}
		final int pos = lot.getPos();
		if(previous != null)
		{
			lot.updatePosition(previous.getPos());
			previous.updatePosition(pos);
		}
		showAuction(lot.getAuction().getId(), player, null);
	}

	/**
	 * Старт торгов лота
	 * 
	 * @param args
	 *            список параметров, переданных в команду
	 * @param player
	 *            персонаж администратора, выполняющего команду
	 */
	private void processAuctionLotStart(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long lotId = L2AuctionUtils.parseLong(args[0], 0);
		if(lotId == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		if(lot.getStatus() != AuctionStatusEnum.view)
		{
			showLot(lotId, player, "Нельзя запускать лоты, которые находятся не в состоянии \"Выставлен\"");
			return;
		}
		lot.start();
		showLot(lotId, player, null);
	}

	/**
	 * Отмена лота
	 * 
	 * @param args
	 *            список параметров, переданных в команду
	 * @param player
	 *            персонаж администратора, выполняющего команду
	 */
	private void processAuctionLotCancel(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long lotId = L2AuctionUtils.parseLong(args[0], 0);
		if(lotId == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final L2Lot lot = L2AuctionManager.getInstance().findLot(lotId);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		if(args.length > 1)
		{
			if(args[1].equalsIgnoreCase("yes"))
				lot.cancel();
			showLot(lotId, player, null);
		}
		else
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/lot_cancel.htm");
			adminReply.replace("%lotId%", String.valueOf(lot.getId()));
			player.sendPacket(adminReply);
		}
	}

	/**
	 * Удаление лота из аукциона
	 * 
	 * @param args
	 *            список параметров, переданных в команду
	 * @param player
	 *            персонаж администратора, выполняющего команду
	 */
	private void processAuctionLotDrop(final String[] args, final L2Player player)
	{
		// Если список команд пуст
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long id = L2AuctionUtils.parseLong(args[0], 0);
		if(id == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		// ищем нужный лот по переданному идентификатору
		final L2Lot lot = L2AuctionManager.getInstance().findLot(id);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		// нельзя удалить лот, если аукцион уже начался
		if(lot.getStatus() != AuctionStatusEnum.init)
		{
			showLot(id, player, "Нельзя удалять, лот в объявленном аукционе");
			return;
		}

		if(lot.getAuction().getStatus() != AuctionStatusEnum.init)
		{
			showLot(id, player, "Нельзя удалять, аукцион уже ждет публикации");
			return;
		}

		// удаляем лот
		final boolean result = lot.drop();
		if(result)
			// показываем аукцион
			showAuction(lot.getAuction().getId(), player, null);
		else
			// показываем лот с ошибкой
			showLot(id, player, "Не удалось удалить лот");

	}

	private void processAuctionLotView(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		final long lotId = L2AuctionUtils.parseLong(args[0], 0);
		if(lotId == 0)
		{
			showError(player, "Отсутствует идентификатор лота");
			return;
		}
		showLot(lotId, player, null);
	}

	protected void showLot(final long id, final L2Player player, final String error)
	{
		final L2Lot lot = L2AuctionManager.getInstance().findLot(id);
		if(lot == null)
		{
			showError(player, "Лота с указанным идентификатором найти не удалось");
			return;
		}
		final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setFile("data/html/admin/auctions/lot_view.htm");
		adminReply.replace("%lotId%", String.valueOf(lot.getId()));
		adminReply.replace("%auctionId%", String.valueOf(lot.getAuction().getId()));
		adminReply.replace("%status%", L2AuctionUtils.getLotStatusName(lot));
		adminReply.replace("%cost%", String.valueOf(lot.getBetCost()));
		adminReply.replace("%bet%", String.valueOf(lot.getBet()));
		adminReply.replace("%start%", String.valueOf(lot.getStartTime()));
		adminReply.replace("%step%", String.valueOf(lot.getStepTime()));
		final String err = error != null ? error : "";
		adminReply.replace("%error%", err);
		adminReply.replace("%level%", L2AuctionUtils.getLotLevelName(lot.getLevel()));
		if(lot.isEasy())
			adminReply.replace("%procent%", "Комиссии при возврате составляет: " + lot.getProcent() + "%<br1>");
		else
			adminReply.replace("%procent%", "");
		adminReply.replace("%name%", lot.getItem().getItem().getName());
		adminReply.replace("%count%", String.valueOf(lot.getItem().getCount()));
		adminReply.replace("%grade%", L2AuctionUtils.getGradeName(lot.getItem().getItem().getItemGrade()));
		adminReply.replace("%icon%", lot.getItem().getItem().getIcon());
		if(lot.getStartDate() != null)
			adminReply.replace("%start_date%", sdf.format(lot.getStartDate()));
		else
			adminReply.replace("%start_date%", "");
		if(lot.getStopDate() != null)
			adminReply.replace("%stop_date%", sdf.format(lot.getStopDate()));
		else
			adminReply.replace("%stop_date%", "");
		final StringBuffer actions = new StringBuffer("<table><tr>");
		switch (lot.getStatus())
		{
			case init:
			{
				if(lot.getAuction().getStatus() == AuctionStatusEnum.init)
					actions.append("<td><button value=\"Удалить\" action=\"bypass -h admin_lotdrop ").append(lot.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case open:
			{
				actions.append("<td><button value=\"Отменить\" action=\"bypass -h admin_lotcancel ").append(lot.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case view:
			{
				actions.append("<td><button value=\"Отменить\" action=\"bypass -h admin_lotcancel ").append(lot.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Старт!\" action=\"bypass -h admin_lotstart ").append(lot.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case cncl:
			case clsd:
				break;
		}
		actions.append("</tr></table>");
		adminReply.replace("%actions%", actions.toString());
		player.sendPacket(adminReply);
	}

	/**
	 * Стартуем аукцион
	 * 
	 * @param args
	 *            список параметров, переданных в команду
	 * @param player
	 *            персонаж администратора, выполняющего команду
	 */
	private void processAuctionStart(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
		{
			showError(player, "Аукциона с указанным идентификатором найти не удалось");
			return;
		}
		if(auction.getStatus() != AuctionStatusEnum.actv)
		{
			showError(player, "Он ведь еще даже не готов. ");
			return;
		}

		if(args.length > 1)
		{
			if(args[1].equalsIgnoreCase("yes"))
				auction.start();
			showAuction(auctionId, player, null);
		}
		else
		{
			if(auction.getLots().size() == 0)
			{
				showAuctionError(auction, player, "Нет ни одного лота");
				return;
			}
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/auction_start.htm");
			adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
			player.sendPacket(adminReply);
		}
	}

	private void processAuctionCancel(final String[] args, final L2Player player)
	{
		if(args.length == 0)
			return;
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
			return;
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
			return;
		if(args.length > 1)
		{
			if(args[1].equalsIgnoreCase("yes"))
				auction.cancel();

			showAuction(auctionId, player, null);
		}
		else
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/auction_cancel.htm");
			adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
			player.sendPacket(adminReply);
		}
	}

	private void processAuctionLotCreate(final String[] args, final L2Player player)
	{
		if(args.length == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final long auctionId = L2AuctionUtils.parseLong(args[0], 0);
		if(auctionId == 0)
		{
			showError(player, "Отсутствует идентификатор аукциона");
			return;
		}
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(auctionId);
		if(auction == null)
		{
			showError(player, "Аукциона с указанным идентификатором найти не удалось");
			return;
		}
		if(auction.getStatus() != AuctionStatusEnum.init)
		{
			showError(player, "Аукцион уже не в стадии создания. Поздно!");
			return;
		}

		if(auction.getLots().size() >= auction.getMaxLotsNumber())
		{
			showAuction(auction.getId(), player, "Это максимальное количество лотов для данного аукциона");
			return;
		}

		if(args.length > 1)
		{

			final LotLevelEnum level = LotLevelEnum.valueOf(args[1]);

			final long itemId = L2AuctionUtils.parseLong(args[2], 0);
			if(itemId == 0)
			{
				showError(player, "Не указан идентификатор предмета");
				return;
			}

			int nextPos = 4;
			int itemCount = 1;
			if(!args[nextPos].equals("c"))
			{
				itemCount = (int) L2AuctionUtils.parseLong(args[nextPos], 1);
				nextPos += 2;
			}
			else
				nextPos += 1;

			int cost = L2AuctionConfig.LOT_BET;
			if(!args[nextPos].equals("b"))
			{
				cost = (int) L2AuctionUtils.parseLong(args[nextPos], 1);
				nextPos += 2;
			}
			else
				nextPos += 1;
			int bet = L2AuctionConfig.BET;
			if(!args[nextPos].equals("s"))
			{
				bet = (int) L2AuctionUtils.parseLong(args[nextPos], bet);
				nextPos += 2;
			}
			else
				nextPos += 1;
			int start = L2AuctionConfig.START_LOT_TIME;
			if(!args[nextPos].equals("p"))
			{
				start = (int) L2AuctionUtils.parseLong(args[nextPos], 1);
				nextPos += 2;
			}
			else
				nextPos += 1;
			int procent = L2AuctionConfig.LOT_PROCENT;
			if(!args[nextPos].equals("t"))
			{
				procent = (int) L2AuctionUtils.parseLong(args[nextPos], procent);
				nextPos += 2;
			}
			else
				nextPos += 1;
			int step = L2AuctionConfig.TIME_STEP;
			if(args.length > nextPos)
				step = (int) L2AuctionUtils.parseLong(args[nextPos], 1);
			L2Lot lot = new L2Lot();
			lot.setLevel(level);
			final L2ItemInstance item = ItemTable.getInstance().createDummyItem((int) itemId);
			item.setCount(itemCount);
			lot.setItem(item);
			lot.setAuction(auction);
			lot.setBet(bet);
			lot.setBetCost(cost);
			lot.setStepTime(step);
			lot.setStartTime(start);
			lot.setProcent(procent);
			lot = auction.createLot(lot);

			String error = null;
			if(lot == null)
				error = "Не удалось  создать лот";
			showAuction(auctionId, player, error);
			return;
		}
		else
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/lot_create.htm");
			adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
			adminReply.replace("%default_bet_cost%", String.valueOf(L2AuctionConfig.LOT_BET));
			adminReply.replace("%default_bet%", String.valueOf(L2AuctionConfig.BET));
			adminReply.replace("%default_start_time%", String.valueOf(L2AuctionConfig.START_LOT_TIME));
			adminReply.replace("%default_bet_time%", String.valueOf(L2AuctionConfig.TIME_STEP));
			adminReply.replace("%default_procent%", String.valueOf(L2AuctionConfig.LOT_PROCENT));
			player.sendPacket(adminReply);
		}
	}

	private void processAuctionDrop(final String[] args, final L2Player player)
	{
		if(args.length == 0)
			return;
		final long id = L2AuctionUtils.parseLong(args[0], 0);
		if(id == 0)
		{
			processAuctions(args, player);
			return;
		}

		final L2Auction auction = L2AuctionManager.getInstance().findAuction(id);
		if(auction == null)
		{
			processAuctions(args, player);
			return;
		}
		if(auction.getStatus() != AuctionStatusEnum.init)
		{
			showAuction(id, player, "Нельзя удалять, начатый аукцион");
			return;
		}
		if(auction.getLots().size() > 0)
		{
			showAuction(id, player, "Нельзя удалять, пока есть лоты");
			return;
		}

		final boolean result = auction.drop();
		if(result)
			processAuctions(args, player);
		else
			showAuction(id, player, null);
	}

	protected void processAuctions(final String[] args, final L2Player player)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		final int itemsPerPage = 10;
		int page = 0;
		if(args.length > 0)
			page = (int) L2AuctionUtils.parseLong(args[0], page);
		int start = page * itemsPerPage;
		int stop = (page + 1) * itemsPerPage;
		final GArray<L2Auction> list = L2AuctionManager.getInstance().getAuctions();
		final int number = list.size();
		if(stop > list.size())
			stop = list.size();
		if(start > list.size())
			start = 0;
		final int pages = (int) Math.ceil((double) number / (double) itemsPerPage);

		List<L2Auction> ls = new ArrayList<L2Auction>(list);
		ls = ls.subList(start, stop);

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setFile("data/html/admin/auctions/auctions.htm");
		final StringBuilder text = new StringBuilder();
		for(final L2Auction auction : list)
		{
			text.append("<tr><td width=60><a action=\"bypass -h admin_auctionview ").append(auction.getId()).append("\">");
			text.append(auction.getId());
			text.append("</a></td><td width=100><a action=\"bypass -h admin_auctionview ").append(auction.getId()).append("\">");
			text.append(sdf.format(auction.getStartDate()));
			text.append("</a></td><td width=100>");
			text.append(L2AuctionUtils.getAuctionStatusName(auction.getStatus()));
			text.append("</td></tr>");
		}
		adminReply.replace("%auctions%", text.toString());

		final StringBuffer pager = new StringBuffer("");
		if(pages > 1)
			for(int i = 0, counter = 0; i < pages; i++, counter++)
			{
				pager.append("<td width=20><a action=\"bypass -h admin_auctions " + i + "\">" + i + "</a></td>");
				if(counter > 0 && counter % 13 == 0)
					pager.append("</tr><tr>");
			}
		if(pager.length() == 0)
			pager.append("<td></td>");

		adminReply.replace("%pager%", pager.toString());

		player.sendPacket(adminReply);
	}

	protected void processAuctionCreate(final String[] args, final L2Player player)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		final SimpleDateFormat stf = new SimpleDateFormat("HH:mm");
		if(args.length == 0)
		{
			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			adminReply.setFile("data/html/admin/auctions/auction_create.htm");
			adminReply.replace("%current_date%", sdf.format(new Date()));
			adminReply.replace("%default_time%", L2AuctionConfig.DEFAULT_START_TIME);
			adminReply.replace("%default_stop_time%", L2AuctionConfig.DEFAULT_STOP_TIME);
			adminReply.replace("%step_time%", String.valueOf(L2AuctionConfig.AUCTION_STEP_TIME));
			player.sendPacket(adminReply);
		}
		else
		{
			final L2Auction auction = new L2Auction();
			int nextPos = 1;
			Date date = new Date();
			if(!args[nextPos].equals("time"))
			{
				try
				{
					date = sdf.parse(args[nextPos]);
				}
				catch(final ParseException e)
				{
					logger.severe("Error while parsed start date while creation");
				}
				nextPos += 2;
			}
			else
				nextPos += 1;

			Date timeStart = new Date();
			try
			{
				timeStart = stf.parse(L2AuctionConfig.DEFAULT_START_TIME);
			}
			catch(final ParseException e)
			{
				logger.severe("Error while parsed default start time while creation");
			}
			if(!args[nextPos].equals("time1"))
			{
				try
				{
					timeStart = stf.parse(args[nextPos]);
				}
				catch(final Exception e)
				{
					logger.severe("Error while parsed start time while creation");
				}
				nextPos += 2;
			}
			else
				nextPos += 1;
			final Calendar calendarStart = Calendar.getInstance();
			calendarStart.setTime(date);
			Calendar calendarTemp = Calendar.getInstance();
			calendarTemp.setTime(timeStart);
			calendarStart.set(Calendar.HOUR_OF_DAY, calendarTemp.get(Calendar.HOUR_OF_DAY));
			calendarStart.set(Calendar.MINUTE, calendarTemp.get(Calendar.MINUTE));
			final Date plannedDate = calendarStart.getTime();

			Date timeStop = new Date();
			try
			{
				timeStop = stf.parse(L2AuctionConfig.DEFAULT_STOP_TIME);
			}
			catch(final ParseException e)
			{
				logger.severe("Error while parsed default stop time while creation");
			}
			if(!args[nextPos].equals("step"))
			{
				try
				{
					timeStop = stf.parse(args[nextPos]);
				}
				catch(final Exception e)
				{
					logger.severe("Error while parsed stop time while creation");
				}
				nextPos += 2;
			}
			else
				nextPos += 1;
			final Calendar calendarStop = Calendar.getInstance();
			calendarStop.setTime(date);
			calendarTemp = Calendar.getInstance();
			calendarTemp.setTime(timeStop);
			calendarStop.set(Calendar.HOUR_OF_DAY, calendarTemp.get(Calendar.HOUR_OF_DAY));
			calendarStop.set(Calendar.MINUTE, calendarTemp.get(Calendar.MINUTE));

			int stepTime = L2AuctionConfig.AUCTION_STEP_TIME;
			if(args.length > nextPos)
				stepTime = (int) L2AuctionUtils.parseLong(args[nextPos], stepTime);

			auction.setStepTime(stepTime);
			auction.setStartDate(plannedDate);
			auction.setAdminStopDate(calendarStop.getTime());
			auction.setDescription("");

			final L2Auction result = L2AuctionManager.getInstance().createAuction(auction);
			showAuction(result.getId(), player, null);
		}
	}

	protected void processAuctionView(final String args[], final L2Player player)
	{
		if(args.length > 0)
		{
			long id = 0;
			try
			{
				id = Long.parseLong(args[0]);
			}
			catch(final Exception e)
			{}
			if(id != 0)
				showAuction(id, player, null);
		}
	}

	protected void showAuction(final long id, final L2Player player, final String error)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		final L2Auction auction = L2AuctionManager.getInstance().findAuction(id);
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setFile("data/html/admin/auctions/auction_view.htm");
		adminReply.replace("%number%", String.valueOf(auction.getId()));
		adminReply.replace("%Id%", String.valueOf(auction.getId()));
		adminReply.replace("%status%", L2AuctionUtils.getAuctionStatusName(auction.getStatus()));
		adminReply.replace("%step%", String.valueOf(auction.getStepTime()));
		adminReply.replace("%date_start%", sdf.format(auction.getStartDate()));
		if(auction.getStopDate() != null)
			adminReply.replace("%date_stop%", sdf.format(auction.getStopDate()));
		else
			adminReply.replace("%date_stop%", "");
		adminReply.replace("%admin_date_stop%", sdf.format(auction.getAdminStopDate()));
		final String err = error != null ? error : "";
		adminReply.replace("%error%", err);
		final StringBuffer actions = new StringBuffer("<table><tr>");

		switch (auction.getStatus())
		{
			case init:
			{
				actions.append("<td><button value=\"Удалить\" action=\"bypass -h admin_auctiondrop ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Добавить лот\" action=\"bypass -h admin_lotcreate ").append(auction.getId()).append("\" width=100 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Готово\" action=\"bypass -h admin_auction_ready ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("</tr><tr>");
				actions.append("<td><button value=\"Копировать\" action=\"bypass -h admin_auctioncopy ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case actv:
			{
				actions.append("<td><button value=\"Старт!\" action=\"bypass -h admin_auction_start ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Копировать\" action=\"bypass -h admin_auctioncopy ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case open:
			{
				actions.append("<td><button value=\"Отменить\" action=\"bypass -h admin_auctioncancel ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Копировать\" action=\"bypass -h admin_auctioncopy ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Статистика\" action=\"bypass -h admin_auction_stat ").append(auction.getId()).append("\" width=100 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
			}
			case cncl:
			case clsd:
				actions.append("<td><button value=\"Копировать\" action=\"bypass -h admin_auctioncopy ").append(auction.getId()).append("\" width=80 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				actions.append("<td><button value=\"Статистика\" action=\"bypass -h admin_auction_stat ").append(auction.getId()).append("\" width=100 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				break;
		}

		actions.append("</tr></table>");
		adminReply.replace("%actions%", actions.toString());
		final StringBuffer text = new StringBuffer("");
		final FastList<L2Lot> lots = auction.getLots();
		for(final L2Lot l : lots)
		{
			text.append("<tr><td ><a action=\"bypass -h admin_lotview ").append(l.getId()).append("\">");
			text.append(l.getId());
			text.append("</a></td><td><a action=\"bypass -h admin_lotview ").append(l.getId()).append("\">");
			text.append(l.getItem().getItem().getName());
			text.append("</a></td><td>");
			text.append(L2AuctionUtils.getLotStatusName(l));
			text.append("</td><td><table width=50><tr>");
			final int max = auction.getMaxPos();
			final int min = auction.getMinPos();
			if(l.getAuction().getStatus() == AuctionStatusEnum.init)
			{
				if(l.getPos() == min && auction.getLots().size() > 1)
					text.append("<td><button value=\"d\" action=\"bypass -h admin_lotdown ").append(l.getId()).append("\" width=25 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				if(l.getPos() == max && auction.getLots().size() > 1)
					text.append("<td><button value=\"u\" action=\"bypass -h admin_lotup ").append(l.getId()).append("\" width=25 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				if(l.getPos() > min && l.getPos() < max && auction.getLots().size() > 1)
				{
					text.append("<td><button value=\"d\" action=\"bypass -h admin_lotdown ").append(l.getId()).append("\" width=25 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
					text.append("<td><button value=\"u\" action=\"bypass -h admin_lotup ").append(l.getId()).append("\" width=25 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
				}
			}

			text.append("</tr></table></td></tr>");
		}
		adminReply.replace("%lots%", text.toString());
		final String descr = auction.getDescription() != null && !auction.getDescription().equals("") ? auction.getDescription() : "Отсутствует";
		adminReply.replace("%description%", descr);
		player.sendPacket(adminReply);
	}

	protected void showAuctionError(final L2Auction auction, final L2Player player, final String error)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setFile("data/html/admin/auctions/auction_error.htm");
		adminReply.replace("%auctionId%", String.valueOf(auction.getId()));
		adminReply.replace("%error%", error);
		player.sendPacket(adminReply);
	}

	protected void showError(final L2Player player, final String error)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setFile("data/html/admin/auctions/error.htm");
		adminReply.replace("%error%", error);
		player.sendPacket(adminReply);
	}
}
