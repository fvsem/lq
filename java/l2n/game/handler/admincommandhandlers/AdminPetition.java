package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.PetitionManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.StringTokenizer;

public class AdminPetition implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_view_petitions",
			"admin_view_petition",
			"admin_accept_petition",
			"admin_reject_petition",
			"admin_reset_petitions",
			"admin_force_peti" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		int petitionId = -1;
		try
		{
			petitionId = Integer.parseInt(command.split(" ")[1]);
		}
		catch(final Exception e)
		{}

		if(command.equals("admin_view_petitions"))
			PetitionManager.getInstance().sendPendingPetitionList(activeChar);
		else if(command.startsWith("admin_view_petition"))
			PetitionManager.getInstance().viewPetition(activeChar, petitionId);
		else if(command.startsWith("admin_accept_petition"))
		{
			if(PetitionManager.getInstance().isPlayerInConsultation(activeChar))
			{
				activeChar.sendPacket(Msg.ALREADY_APPLIED_FOR_PETITION);
				return true;
			}

			if(PetitionManager.getInstance().isPetitionInProcess(petitionId))
			{
				activeChar.sendPacket(Msg.PETITION_UNDER_PROCESS);
				return true;
			}

			if(!PetitionManager.getInstance().acceptPetition(activeChar, petitionId))
				activeChar.sendPacket(Msg.NOT_UNDER_PETITION_CONSULTATION);
		}
		else if(command.startsWith("admin_reject_petition"))
		{
			if(!PetitionManager.getInstance().rejectPetition(activeChar, petitionId))
				activeChar.sendPacket(Msg.FAILED_TO_CANCEL_PETITION_PLEASE_TRY_AGAIN_LATER);
			PetitionManager.getInstance().sendPendingPetitionList(activeChar);
		}
		else if(command.equals("admin_reset_petitions"))
		{
			if(PetitionManager.getInstance().isPetitionInProcess())
			{
				activeChar.sendPacket(Msg.PETITION_UNDER_PROCESS);
				return false;
			}
			PetitionManager.getInstance().clearPendingPetitions();
			PetitionManager.getInstance().sendPendingPetitionList(activeChar);
		}
		else if(command.startsWith("admin_force_peti"))
		{
			if(PetitionManager.getInstance().isPlayerInConsultation(activeChar))
			{
				activeChar.sendPacket(Msg.ALREADY_APPLIED_FOR_PETITION);
				return true;
			}

			final StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken(); // skip command

			if(st.hasMoreTokens())
			{
				final String firstParam = st.nextToken();
				final L2Player plyr = L2ObjectsStorage.getPlayer(firstParam);
				if(plyr != null)
				{
					petitionId = PetitionManager.getInstance().submitPetition(plyr, "", 1);
					if(PetitionManager.getInstance().isPetitionInProcess(petitionId))
					{
						activeChar.sendPacket(Msg.PETITION_UNDER_PROCESS);
						return true;
					}

					if(!PetitionManager.getInstance().acceptPetition(activeChar, petitionId))
						activeChar.sendPacket(Msg.NOT_UNDER_PETITION_CONSULTATION);
				}
			}
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды",
				"Описание команды",
				"Описание команды",
				"Описание команды",
				"Описание команды",
				"Описание команды" };

	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_PETITION,
				AdminCommandHandler.GROUP_PETITION,
				AdminCommandHandler.GROUP_PETITION,
				AdminCommandHandler.GROUP_PETITION,
				AdminCommandHandler.GROUP_PETITION,
				AdminCommandHandler.GROUP_PETITION };
	}
}
