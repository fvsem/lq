package l2n.game.handler.admincommandhandlers;

import l2n.commons.list.GArray;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.*;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExShowTrace;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.SpawnTable;
import l2n.game.tables.TerritoryTable;

import java.util.StringTokenizer;

public class AdminZone implements IAdminCommandHandler
{
	public static final String[] ADMIN_ZONE_COMMANDS = {
			"admin_zone_check",
			"admin_region",
			"admin_loc",
			"admin_showloc",
			"admin_location",
			"admin_loc_begin",
			"admin_loc_add",
			"admin_loc_reset",
			"admin_loc_end",
			"admin_setref" };

	private static GArray<int[]> create_loc;
	private static int create_loc_id;

	private static void locationMenu(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body><title>Location Create</title>");

		replyMSG.append("<center><table width=260><tr>");
		replyMSG.append("<td width=70>Location:</td>");
		replyMSG.append("<td width=50><edit var=\"loc\" width=50 height=12></td>");
		replyMSG.append("<td width=50><button value=\"Show\" action=\"bypass -h admin_showloc $loc\" width=50 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=90><button value=\"New Location\" action=\"bypass -h admin_loc_begin $loc\" width=90 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table><br><br></center>");

		if(create_loc != null)
		{
			replyMSG.append("<center><table width=260><tr>");
			replyMSG.append("<td width=80><button value=\"Add Point\" action=\"bypass -h admin_loc_add menu\" width=80 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
			replyMSG.append("<td width=90><button value=\"Reset Points\" action=\"bypass -h admin_loc_reset menu\" width=90 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
			replyMSG.append("<td width=90><button value=\"End Location\" action=\"bypass -h admin_loc_end menu\" width=90 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
			replyMSG.append("</tr></table></center>");
		}

		replyMSG.append("</body></html>");
		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private static ExShowTrace Points2Trace(final GArray<int[]> _points, final int _step, final boolean auto_compleate)
	{
		final ExShowTrace result = new ExShowTrace();

		int[] _prev = null;
		int[] _first = null;
		for(final int[] p : _points)
		{
			if(_first == null)
				_first = p;

			if(_prev != null)
				result.addLine(_prev[0], _prev[1], _prev[2], p[0], p[1], p[2], _step, 60000);

			_prev = p;
		}

		if(_prev == null || _first == null)
			return result;

		if(auto_compleate)
			result.addLine(_prev[0], _prev[1], _prev[2], _first[0], _first[1], _first[2], _step, 60000);

		return result;
	}

	public static ExShowTrace Points2Trace(final GArray<int[]> points, final int step, final boolean auto_compleate, final boolean maxz)
	{
		final ExShowTrace result = new ExShowTrace();

		int[] prev = null;
		int[] first = null;
		for(final int[] p : points)
		{
			if(first == null)
				first = p;
			if(prev != null)
				result.addLine(prev[0], prev[1], maxz ? prev[3] : prev[2], p[0], p[1], maxz ? p[3] : p[2], step, 60000);
			prev = p;
		}

		if(prev == null || first == null)
			return result;
		if(auto_compleate)
			result.addLine(prev[0], prev[1], maxz ? prev[3] : prev[2], first[0], first[1], maxz ? first[3] : first[2], step, 60000);
		return result;
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar == null || !activeChar.getPlayerAccess().CanEditZone)
			return false;

		final StringTokenizer st = new StringTokenizer(command, " ");
		final String actualCommand = st.nextToken();

		if(actualCommand.equalsIgnoreCase("admin_zone_check"))
		{
			activeChar.sendMessage("===== Active Territories =====");
			final GArray<L2Territory> territories = L2World.getTerritories(activeChar.getX(), activeChar.getY(), activeChar.getZ());
			if(territories != null)
				for(final L2Territory terr : territories)
					activeChar.sendMessage("Territory " + terr.getId() + ", Zmin: " + terr.getZmin() + "/ Zmax: " + terr.getZmax());

			activeChar.sendMessage("======= Mob Spawns =======");
			for(final L2Spawn spawn : SpawnTable.getInstance().getSpawnTable())
			{
				final int location = spawn.getLocation();
				if(location == 0)
					continue;
				final L2Territory terr = TerritoryTable.getInstance().getLocation(location);

				if(terr == null)
					continue;

				if(terr.isInside(activeChar.getX(), activeChar.getY()))
					activeChar.sendMessage("Territory " + terr.getId());
			}

			activeChar.sendMessage("======= Zones =======");
			if(activeChar.getZones() != null)
				for(final L2Zone zone : activeChar.getZones())
				{
					if(zone == null)
						continue;
					activeChar.sendMessage("Zone: " + zone.getName() + "/" + zone.getType().toString() + ", id: " + zone.getId() + ", state: " + (zone.isActive() ? "active" : "not active"));
				}
		}
		else if(actualCommand.equalsIgnoreCase("admin_region"))
		{
			activeChar.sendMessage("Current region: " + activeChar.getCurrentRegion().getName());
			activeChar.sendMessage("Objects list:");
			for(final L2Object o : activeChar.getCurrentRegion().getObjectsList(new GArray<L2Object>(), 0, activeChar.getReflection()))
				if(o != null)
					activeChar.sendMessage(o.toString());
		}
		/**
		 * Пишет в консоль текущую точку для локации, оформляем в виде SQL запроса
		 * пример: (8699,'loc_8699',111104,-112528,-1400,-1200),
		 * Удобно для рисования локаций под спавн, разброс z +100/-10
		 * необязательные параметры: id локации и название локации
		 * Бросает бутылку, чтобы не запутаццо :)
		 */
		else if(actualCommand.equalsIgnoreCase("admin_loc"))
		{
			String loc_id = "0";
			String loc_name;
			if(st.hasMoreTokens())
				loc_id = st.nextToken();
			if(st.hasMoreTokens())
				loc_name = st.nextToken();
			else
				loc_name = "loc_" + loc_id;
			System.out.println("(" + loc_id + ",'" + loc_name + "'," + activeChar.getX() + "," + activeChar.getY() + "," + activeChar.getZ() + "," + (activeChar.getZ() + 100) + "),");
			activeChar.sendMessage("Point saved.");
			final L2ItemInstance temp = ItemTable.getInstance().createItem(1060, 1, 1, "");
			temp.dropMe(activeChar, activeChar.getLoc());
		}
		else if(actualCommand.equalsIgnoreCase("admin_location"))
			locationMenu(activeChar);
		else if(actualCommand.equalsIgnoreCase("admin_loc_begin"))
		{
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Usage: //loc_begin <location_id>");
				locationMenu(activeChar);
				return false;
			}
			try
			{
				create_loc_id = Integer.valueOf(st.nextToken());
			}
			catch(final Exception E)
			{
				activeChar.sendMessage("location_id should be integer");
				create_loc = null;
				locationMenu(activeChar);
				return false;
			}

			create_loc = new GArray<int[]>();
			create_loc.add(new int[] { activeChar.getX(), activeChar.getY(), activeChar.getZ(), activeChar.getZ() + 100 });
			activeChar.sendMessage("Now you can add points...");
			activeChar.sendPacket(new ExShowTrace());
			locationMenu(activeChar);
		}
		else if(actualCommand.equalsIgnoreCase("admin_loc_add"))
		{
			if(create_loc == null)
			{
				activeChar.sendMessage("Location not started");
				locationMenu(activeChar);
				return false;
			}

			create_loc.add(new int[] { activeChar.getX(), activeChar.getY(), activeChar.getZ(), activeChar.getZ() + 100 });

			if(create_loc.size() > 1)
				activeChar.sendPacket(Points2Trace(create_loc, 50, false));
			if(st.hasMoreTokens() && st.nextToken().equals("menu"))
				locationMenu(activeChar);
		}
		else if(actualCommand.equalsIgnoreCase("admin_loc_reset"))
		{
			if(create_loc == null)
			{
				activeChar.sendMessage("Location not started");
				locationMenu(activeChar);
				return false;
			}

			create_loc.clear();
			activeChar.sendPacket(new ExShowTrace());
			locationMenu(activeChar);
		}
		else if(actualCommand.equalsIgnoreCase("admin_loc_end"))
		{
			if(create_loc == null)
			{
				activeChar.sendMessage("Location not started");
				locationMenu(activeChar);
				return false;
			}
			if(create_loc.size() < 3)
			{
				activeChar.sendMessage("Minimum location size 3 points");
				locationMenu(activeChar);
				return false;
			}

			final String prefix = "(" + create_loc_id + ",'loc_" + create_loc_id + "',";
			for(final int[] _p : create_loc)
				System.out.println(prefix + _p[0] + "," + _p[1] + "," + _p[2] + "," + _p[3] + "),");
			System.out.println("");

			activeChar.sendPacket(Points2Trace(create_loc, 50, true));
			create_loc = null;
			create_loc_id = 0;
			activeChar.sendMessage("Location Created, check stdout");
			if(st.hasMoreTokens() && st.nextToken().equals("menu"))
				locationMenu(activeChar);
		}
		else if(actualCommand.equalsIgnoreCase("admin_showloc"))
		{
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Usage: //showloc <location>");
				return false;
			}

			final int loc_id = Integer.parseInt(st.nextToken());
			final L2Territory terr = TerritoryTable.getInstance().getLocation(loc_id);
			if(terr == null)
			{
				activeChar.sendMessage("Location <" + loc_id + "> undefined.");
				return false;
			}
			if(!terr.isInside(activeChar.getX(), activeChar.getY()))
			{
				final int[] _loc = terr.getRandomPoint();
				activeChar.teleToLocation(_loc[0], _loc[1], _loc[2]);
			}
			activeChar.sendPacket(Points2Trace(terr.getCoords(), 50, true, true));
		}
		else if(actualCommand.equalsIgnoreCase("admin_setref"))
		{
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Usage: //setref <reflection>");
				return false;
			}

			final long ref_id = Long.parseLong(st.nextToken());
			if(ref_id != 0 && ReflectionTable.getInstance().get(ref_id) == null)
			{
				activeChar.sendMessage("Reflection <" + ref_id + "> not found.");
				return false;
			}

			L2Object target = activeChar;
			final L2Object obj = activeChar.getTarget();
			if(obj != null)
				target = obj;

			target.setReflection(ref_id);
			target.decayMe();
			target.spawnMe();
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_ZONE_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"admin_zone_check",
				"admin_region",
				"admin_loc",
				"admin_showloc",
				"admin_location",
				"admin_loc_begin",
				"admin_loc_add",
				"admin_loc_reset",
				"admin_loc_end",
				"admin_setref" };
	}

	@Override
	public String[] getCommandGroup()
	{
		final String[] result = new String[ADMIN_ZONE_COMMANDS.length];
		for(int i = 0; i < result.length; i++)
			result[i] = AdminCommandHandler.GROUP_MANAGESERVER6;
		return result;
	}
}
