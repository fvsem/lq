package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.TransformationManager;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SetupGauge;

import java.util.StringTokenizer;

public class AdminPolymorph implements IAdminCommandHandler
{
	private static String _adminCommands[] = {
			"admin_polymorph",
			"admin_unpolymorph",
			"admin_poly",
			"admin_unpoly",
			"admin_transform",
			"admin_transform_menu" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanPolymorph)
			return false;

		if(command.startsWith("admin_polymorph") || command.startsWith("admin_poly"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			try
			{
				st.nextToken();
				final String id = st.nextToken();
				String type;

				if(st.hasMoreTokens())
					type = st.nextToken();
				else
					type = "npc";

				final L2Object target = activeChar.getTarget();

				if(target != null)
				{
					target.setPolyInfo(type, id);
					if(target.isCharacter())
					{
						final L2Character Char = (L2Character) target;
						Char.broadcastPacket(new MagicSkillUse(Char, Char, 1008, 1, 1000, 0));
						Char.sendPacket(new SetupGauge(0, 1000));
					}
					target.decayMe();
					target.spawnMe(target.getLoc());
				}
				else
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
			}
			catch(final Exception e)
			{}
		}
		else if(command.equals("admin_unpolymorph") || command.equals("admin_unpoly"))
		{
			final L2Object target = activeChar.getTarget();

			if(target != null)
			{
				target.setPolyInfo(null, "0");
				if(target.isCharacter())
				{
					final L2Character Char = (L2Character) target;
					Char.broadcastPacket(new MagicSkillUse(Char, Char, 1008, 1, 1000, 0));
					Char.sendPacket(new SetupGauge(0, 1000));
				}
				target.decayMe();
				target.spawnMe(target.getLoc());
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
		}
		else if(command.startsWith("admin_transform"))
		{
			final L2Object obj = activeChar.getTarget();

			final StringTokenizer st = new StringTokenizer(command);

			if(obj instanceof L2Player)
			{
				final L2Player cha = (L2Player) obj;
				try
				{
					st.nextToken();
					final int id = Integer.parseInt(st.nextToken());
					if(id == 0)
						cha.stopTransformation();
					else
						TransformationManager.getInstance().transformPlayer(id, cha);
				}
				catch(final NumberFormatException e)
				{
					activeChar.sendMessage("Usage: //transform <id>");
				}
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
		}
		else if(command.equals("admin_transform_menu"))
			AdminHelpPage.showHelpPage(activeChar, "transform.htm");
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды polymorph",
				"Описание команды unpolymorph",
				"Описание команды poly",
				"Описание команды unpoly",
				"Описание команды transform",
				"Описание команды transform_menu" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4 };
	}
}
