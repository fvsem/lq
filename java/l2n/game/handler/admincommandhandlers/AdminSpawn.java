package l2n.game.handler.admincommandhandlers;

import l2n.database.utils.mysql;
import l2n.extensions.scripts.Scripts;
import l2n.game.ai.L2CharacterAI;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.GmListTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Log;

import java.lang.reflect.Constructor;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminSpawn implements IAdminCommandHandler
{
	private final static String[] _adminCommands = {
			"admin_show_spawns",
			"admin_spawn",
			"admin_spawn_monster",
			"admin_spawn_index",
			"admin_unspawnall",
			"admin_spawn1",
			"admin_setheading",
			"admin_setai",
			"admin_pspawn",
			"admin_startship" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Spawn)
			return false;

		if(command.equals("admin_show_spawns"))
			AdminHelpPage.showHelpPage(activeChar, "spawns.htm");
		else if(command.startsWith("admin_spawn_index"))
			try
			{
				final String val = command.substring(18);
				AdminHelpPage.showHelpPage(activeChar, "spawns/" + val + ".htm");
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_spawn1"))
		{
			final StringTokenizer st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				final String id = st.nextToken();
				int mobCount = 1;
				if(st.hasMoreTokens())
					mobCount = Integer.parseInt(st.nextToken());
				spawnMonster(activeChar, id, 0, mobCount, false);
			}
			catch(final Exception e)
			{
				// Case of wrong monster data
			}
		}
		else if(command.startsWith("admin_spawn") || command.startsWith("admin_spawn_monster"))
		{
			final StringTokenizer st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				final String id = st.nextToken();
				int respawnTime = 30;
				int mobCount = 1;
				if(st.hasMoreTokens())
					mobCount = Integer.parseInt(st.nextToken());
				if(st.hasMoreTokens())
					respawnTime = Integer.parseInt(st.nextToken());
				spawnMonster(activeChar, id, respawnTime, mobCount, false);
			}
			catch(final Exception e)
			{
				// Case of wrong monster data
			}
		}
		else if(command.startsWith("admin_pspawn"))
		{
			final StringTokenizer st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				final String id = st.nextToken();
				int respawnTime = 30;
				int mobCount = 1;
				if(st.hasMoreTokens())
					mobCount = Integer.parseInt(st.nextToken());
				if(st.hasMoreTokens())
					respawnTime = Integer.parseInt(st.nextToken());
				spawnMonster(activeChar, id, respawnTime, mobCount, true); // spawn in activeChar's current reflection
			}
			catch(final Exception e)
			{
				// Case of wrong monster data
			}
		}
		else if(command.startsWith("admin_unspawnall"))
		{
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				player.sendPacket(new SystemMessage(SystemMessage.THE_NPC_SERVER_IS_NOT_OPERATING));
			L2World.deleteVisibleNpcSpawns();
			GmListTable.broadcastMessageToGMs("NPC Unspawn completed!");
		}
		else if(command.startsWith("admin_setai"))
		{
			if(activeChar.getTarget() == null || !activeChar.getTarget().isNpc())
			{
				activeChar.sendMessage("Please select target NPC or mob.");
				return false;
			}

			final StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Please specify AI name.");
				return false;
			}
			final String aiName = st.nextToken();
			final L2NpcInstance target = (L2NpcInstance) activeChar.getTarget();

			Constructor<?> aiConstructor = null;
			try
			{
				if(!aiName.equalsIgnoreCase("npc"))
					aiConstructor = Class.forName("l2n.game.ai." + aiName).getConstructors()[0];
			}
			catch(final Exception e)
			{
				try
				{
					aiConstructor = Scripts.getInstance().getClasses().get("ai." + aiName).getRawClass().getConstructors()[0];
				}
				catch(final Exception e1)
				{
					activeChar.sendMessage("This type AI not found.");
					return false;
				}
			}

			target.detachAI();

			if(aiConstructor != null)
			{
				try
				{
					target.setAI((L2CharacterAI) aiConstructor.newInstance(new Object[] { target }));
				}
				catch(final Exception e)
				{
					e.printStackTrace();
				}
				target.getAI().startAITask();
			}
		}
		else if(command.startsWith("admin_setheading"))
		{
			final L2Object obj = activeChar.getTarget();
			if(!(obj instanceof L2NpcInstance))
			{
				activeChar.sendMessage("Target is incorrect!");
				return false;
			}

			final L2NpcInstance npc = (L2NpcInstance) obj;

			final L2Spawn spawn = npc.getSpawn();
			if(spawn == null)
			{
				activeChar.sendMessage("Spawn for this npc == null!");
				return false;
			}

			if(!mysql.set("update spawnlist set heading = " + activeChar.getHeading() //
					+ " where npc_templateid = " + npc.getNpcId() //
					+ " and locx = " + spawn.getLocx() //
					+ " and locy = " + spawn.getLocy() //
					+ " and locz = " + spawn.getLocz() //
					+ " and loc_id = " + spawn.getLocation()))
			{
				activeChar.sendMessage("Error in mysql query!");
				return false;
			}

			npc.setHeading(activeChar.getHeading());
			npc.decayMe();
			npc.spawnMe();
			activeChar.sendMessage("New heading : " + activeChar.getHeading());
		}
		else if(command.equalsIgnoreCase("admin_startship"))
		{
			final L2Vehicle boat = activeChar.getVehicle();
			if(boat != null)
				boat.startManual();
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void spawnMonster(final L2Player activeChar, String monsterId, final int respawnTime, final int mobCount, final boolean inDataBase)
	{
		L2Object target = activeChar.getTarget();
		if(target == null)
			target = activeChar;

		final Pattern pattern = Pattern.compile("[0-9]*");
		final Matcher regexp = pattern.matcher(monsterId);
		L2NpcTemplate template;
		if(regexp.matches())
		{
			// First parameter was an ID number
			final int monsterTemplate = Integer.parseInt(monsterId);
			template = NpcTable.getTemplate(monsterTemplate);
		}
		else
		{
			// First parameter wasn't just numbers so go by name not ID
			monsterId = monsterId.replace('_', ' ');
			template = NpcTable.getTemplateByName(monsterId);
		}

		if(template == null)
		{
			activeChar.sendMessage("Incorrect monster template.");
			return;
		}

		try
		{
			final L2Spawn spawn = new L2Spawn(template);
			spawn.setLoc(target.getLoc());

			if(inDataBase)
				if(!mysql.set("INSERT INTO `spawnlist` VALUES ('admin_spawn_" + activeChar.getName() + "'," + mobCount + "," + template.getNpcId() + "," + spawn.getLocx() + "," + spawn.getLocy() + "," + spawn.getLocz() + ", " + activeChar.getHeading() + ", " + respawnTime + ", 0,0,0,0)"))
				{
					activeChar.sendMessage("Error in mysql query!");
					return;
				}

			spawn.setLocation(0);
			spawn.setAmount(mobCount);
			spawn.setHeading(activeChar.getHeading());
			spawn.setRespawnDelay(respawnTime);
			spawn.setReflection(activeChar.getReflection().getId());

			if(RaidBossSpawnManager.getInstance().isDefined(spawn.getNpcId()))
				activeChar.sendMessage("Raid Boss " + template.name + " already spawned.");
			else
			{
				spawn.init();
				if(respawnTime == 0)
					spawn.stopRespawn();
				activeChar.sendMessage("Created " + template.name + " on " + target.getObjectId() + ".");
				Log.add("Created " + template.name + " on " + target.getObjectId(), "gm_ext_actions", activeChar);
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			activeChar.sendMessage("Target is not ingame.");
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"admin_show_spawns",
				"admin_spawn",
				"admin_spawn_monster",
				"admin_spawn_index",
				"admin_unspawnall",
				"admin_spawn1",
				"admin_setheading",
				"admin_setai - установка указанного АИ выделенному мобу/NPC",
				"admin_pspawn - заспавнить с добавлением в БД",
				"admin_startship - запускает корабль" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6 };
	}
}
