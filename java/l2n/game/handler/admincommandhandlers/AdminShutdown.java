package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.game.GameTimeController;
import l2n.game.Shutdown;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.taskmanager.MemoryWatchDog;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class AdminShutdown implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_server_shutdown",
			"admin_server_restart",
			"admin_server_abort" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanRestart)
			return false;

		if(command.startsWith("admin_server_shutdown"))
			try
			{
				final int val = Integer.parseInt(command.substring(22));
				serverShutdown(activeChar, val, false);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				sendHtmlForm(activeChar);
			}
		else if(command.startsWith("admin_server_restart"))
			try
			{
				final int val = Integer.parseInt(command.substring(21));
				serverShutdown(activeChar, val, true);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				sendHtmlForm(activeChar);
			}
		else if(command.startsWith("admin_server_abort"))
			serverAbort(activeChar);

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void sendHtmlForm(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final int t = GameTimeController.getInstance().getGameTime();
		final int h = t / 60;
		final int m = t % 60;
		final SimpleDateFormat format = new SimpleDateFormat("h:mm a");
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, h);
		cal.set(Calendar.MINUTE, m);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center>Управления Сервером</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td>Игроков Онлайн: " + L2ObjectsStorage.getAllPlayersCount() + "</td></tr>");
		replyMSG.append("<tr><td>Игроков Торгует: " + L2ObjectsStorage.getAllOfflineCount() + "</td></tr>");
		replyMSG.append("<tr><td>Используемая память: " + MemoryWatchDog.getMemUsedMb() + " </td></tr>");
		replyMSG.append("<tr><td>Свободная память: " + MemoryWatchDog.getMemFreeMb() + " </td></tr>");
		replyMSG.append("<tr><td>Максимальная память: " + MemoryWatchDog.getMemMaxMb() + " </td></tr>");
		replyMSG.append("<tr><td>Рейты Сервера: " + Config.RATE_XP + "x, " + Config.RATE_SP + "x, " + Config.RATE_DROP_ADENA + "x, " + Config.RATE_DROP_ITEMS + "x</td></tr>");
		replyMSG.append("<tr><td>Игровое Время: " + format.format(cal.getTime()) + "</td></tr>");
		replyMSG.append("</table><br>");
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td>Введите в секундах, время до отключения сервера ниже:</td></tr>");
		replyMSG.append("<br>");
		replyMSG.append("<tr><td><center>Секунды до: <edit var=\"shutdown_time\" width=60></center></td></tr>");
		replyMSG.append("</table><br>");
		replyMSG.append("<center><table><tr><td>");
		replyMSG.append("<button value=\"Выключение\" action=\"bypass -h admin_server_shutdown $shutdown_time\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td><td>");
		replyMSG.append("<button value=\"Рестарт\" action=\"bypass -h admin_server_restart $shutdown_time\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td><td>");
		replyMSG.append("<button value=\"Отмена\" action=\"bypass -h admin_server_abort\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
		replyMSG.append("</td></tr></table></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void serverShutdown(final L2Player activeChar, final int seconds, final boolean restart)
	{
		Shutdown.getInstance().startShutdown(activeChar, seconds, restart);
	}

	private void serverAbort(final L2Player activeChar)
	{
		Shutdown.getInstance().abort(activeChar);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды server_shutdown",
				"Описание команды server_restart",
				"Описание команды server_abort" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5 };
	}
}
