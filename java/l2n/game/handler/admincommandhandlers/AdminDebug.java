package l2n.game.handler.admincommandhandlers;

import l2n.commons.list.GArray;
import l2n.commons.list.primitive.IntArrayList;
import l2n.commons.text.Strings;
import l2n.game.ai.DefaultAI;
import l2n.game.ai.L2CharacterAI;
import l2n.game.ai.model.AdditionalAIParams;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character.HateInfo;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.skills.Calculator;
import l2n.game.skills.Env;
import l2n.game.skills.Formulas;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.Func;
import l2n.game.tables.CharTemplateTable;
import l2n.game.tables.SkillTable;
import l2n.login.L2LoginThreadPools;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ScheduledFuture;

public class AdminDebug implements IAdminCommandHandler
{
	private static final GArray<ScheduledFuture<AddEffect>> tasks = new GArray<ScheduledFuture<AddEffect>>();
	private static ScheduledFuture<EffectsRunner> runner = null;

	private static long count = 0;

	private static final L2Skill[] skills = new L2Skill[]
	{
			SkillTable.getInstance().getInfoSafe(1040, 10),
			SkillTable.getInstance().getInfoSafe(1043, 10),
			SkillTable.getInstance().getInfoSafe(1044, 10),
			SkillTable.getInstance().getInfoSafe(1045, 10),
			SkillTable.getInstance().getInfoSafe(1047, 10),
			SkillTable.getInstance().getInfoSafe(1048, 10),
			SkillTable.getInstance().getInfoSafe(768, 10)
	};

	private static boolean active = false;

	private final class AddEffect implements Runnable
	{
		private final L2Player target;

		public AddEffect(final L2Player target)
		{
			this.target = target;
		}

		@Override
		public void run()
		{
			if(Rnd.chance(50))
				skills[Rnd.get(skills.length)].getEffects(target, target, false, false);
			else
				target.getEffectList().stopEffect(Rnd.get(skills.length));
		}
	}

	private final class EffectsRunner extends Thread
	{
		private final L2Player target;

		public EffectsRunner(final L2Player target)
		{
			this.target = target;
		}

		@Override
		public void run()
		{
			for(;;)
			{
				for(int i = 100; i-- > 0;)
				{
					tasks.add(L2LoginThreadPools.getInstance().scheduleGeneral(new AddEffect(target), 100));
					count++;
				}

				if(count % 500 == 0 && count != 0)
				{
					System.out.println("EffectsRunner: " + count + " already runned");
				}

				if(!active || target == null)
				{
					System.out.println("EffectsRunner: stoped");
					break;
				}

				try
				{
					Thread.sleep(100);
				}
				catch(final InterruptedException e)
				{
					active = false;
					e.printStackTrace();
				}
			}
		}
	}

	private final static String[] _adminCommands = {
			"admin_debugai",
			"admin_debug_player",
			"admin_debug_ai_params",
			"admin_debug_aggro",
			"admin_debug" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.equalsIgnoreCase("admin_debug_player"))
		{
			final L2Object target_obj = activeChar.getTarget();
			if(target_obj == null || !target_obj.isPlayable())
			{
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
				return false;
			}

			final L2Player target = (L2Player) target_obj;
			debug_stats(target, activeChar);
		}
		else if(command.equalsIgnoreCase("admin_debug_aggro"))
			showAggro(activeChar);
		else if(command.equalsIgnoreCase("admin_debug"))
		{
			final String charName = command.substring(12);
			if(!charName.isEmpty())
			{
				L2Player player = L2ObjectsStorage.getPlayer(charName);
				if(player != null)
					debug_stats(player, activeChar);
			}
		}
		else if(command.equalsIgnoreCase("admin_debugai"))
		{
			final L2Object obj = activeChar.getTarget();
			if(obj == null || !obj.isNpc())
			{
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
				return false;
			}

			final L2CharacterAI ai = obj.getAI();
			final L2NpcInstance npc = (L2NpcInstance) obj;
			if(ai instanceof DefaultAI)
			{
				final DefaultAI dai = (DefaultAI) ai;
				final NpcHtmlMessage html = new NpcHtmlMessage(obj.getObjectId());
				final StringBuilder html1 = new StringBuilder("<html><body><title>AI Debug - " + obj.getName() + "</title>");
				html1.append("<br><center><button value=\"Refresh\" action=\"bypass -h admin_debugai\" width=70 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /><button value=\"Aggro\" action=\"bypass -h admin_debug_aggro\" width=70 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center><br>");
				html1.append("<table border=0>");
				html1.append("<tr><td>Name: <font color=\"LEVEL\">" + obj.getName() + "</font>, id: " + npc.getNpcId() + "</td></tr>");
				html1.append("<tr><td>Npc O/S ID: " + npc.getObjectId() + "/" + npc.getStoredId() + "</td></tr>");
				html1.append("<tr><td>Act O/S ID: " + dai.getActor().getObjectId() + "/" + dai.getActor().getStoredId() + "</td></tr>");
				html1.append("<tr><td>AI Class: <font color=\"LEVEL\">" + dai.getClass().getSimpleName() + "</font></td></tr>");
				html1.append("<tr><td>Intention: <font color=\"LEVEL\">" + dai.getIntention() + "</font></td></tr>");
				html1.append("<tr><td>Task delay: " + dai.getTaskDelay() / 1000 + "s, active: " + dai.getTaskActiveDelay() / 1000 + "s</td></tr>");
				html1.append("<tr><td>PurgeRange: " + dai.getMaxPursueRange() + "</td></tr>");
				html1.append("<tr><td>AgroRange: " + npc.getAggroRange() + ", dis: " + (int) activeChar.getDistance(npc) + " (Z: " + (Math.abs(activeChar.getZ() - npc.getZ()) > 200) + ")</td></tr>");
				html1.append("<tr><td>Attack TimeOut: " + (npc.getAttackTimeout() - System.currentTimeMillis()) / 1000 + " sec</td></tr>");
				html1.append("<tr><td>GlobalAgro: " + dai.isGlobalAggro() + " (" + dai.getGlobalAggro() / 1000 + ")</td></tr>");
				html1.append("<tr><td><br></td></tr>");

				html1.append("<tr><td>Is Casting: " + (!npc.isCastingNow() ? "<font color=\"ff0000\">" + npc.isCastingNow() + "</font>" : "<font color=\"00ff00\">" + npc.isCastingNow() + "</font>") + " (" + npc.getCastingSkill() + ")</td></tr>");
				html1.append("<tr><td>Is Attacking: " + (!npc.isAttackingNow() ? "<font color=\"ff0000\">" + npc.isAttackingNow() + "</font>" : "<font color=\"00ff00\">" + npc.isAttackingNow() + "</font>") + "</td></tr>");
				html1.append("<tr><td>Is Moving: " + (!npc.isMoving ? "<font color=\"ff0000\">" + npc.isMoving + "</font>" : "<font color=\"00ff00\">" + npc.isMoving + "</font>") + "</td></tr>");

				html1.append("<tr><td>Target: " + dai.getAttackTarget() + "</td></tr>");
				html1.append("<tr><td><br></td></tr>");
				html1.append("<tr><td>IsActive: " + (!dai.isActive() ? "<font color=\"ff0000\">" + dai.isActive() + "</font>" : "<font color=\"00ff00\">" + dai.isActive() + "</font>") + "</td></tr>");
				html1.append("<tr><td>Tasks: " + dai.getTasksSize() + "</td></tr>");
				if(dai.getTasksSize() > 0)
				{
					dai.debugTasks();
				}

				html1.append("<tr><td>Random Tasks: " + dai.getRandomTasksSize() + "</td></tr>");

				html1.append("<tr><td>Thinking: " + (!dai.isThinking() ? "<font color=\"ff0000\">" + dai.isThinking() + "</font>" : "<font color=\"00ff00\">" + dai.isThinking() + "</font>") + "</td></tr>");
				html1.append("<tr><td>Def think: " + (!dai.isDefThink() ? "<font color=\"ff0000\">" + dai.isDefThink() + "</font>" : "<font color=\"00ff00\">" + dai.isDefThink() + "</font>") + "</td></tr>");
				html1.append("<tr><td>Is mobile: " + (!dai.isMobile() ? "<font color=\"ff0000\">" + dai.isMobile() + "</font>" : "<font color=\"00ff00\">" + dai.isMobile() + "</font>") + "</td></tr>");
				html1.append("<tr><td>Is invul [stat " + (int) npc.calcStat(Stats.INVULNERABLE, 0, null, null) + "]: " + (!npc.isInvul() ? "<font color=\"ff0000\">" + npc.isInvul() + "</font>" : "<font color=\"00ff00\">" + npc.isInvul() + "</font>") + "</td></tr>");

				html1.append("<tr><td><br></td></tr>");
				html1.append("</table></body></html>");

				html.setHtml(html1.toString());
				activeChar.sendPacket(html);
				return true;
			}
			else
				return false;
		}
		else if(command.equalsIgnoreCase("admin_debug_ai_params"))
		{
			final L2Object obj = activeChar.getTarget();
			if(obj == null || !obj.isNpc())
			{
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
				return false;
			}

			final L2CharacterAI ai = obj.getAI();
			if(ai instanceof DefaultAI)
			{
				final DefaultAI dai = (DefaultAI) ai;
				final AdditionalAIParams params = dai.getAIParams();
				final NpcHtmlMessage html = new NpcHtmlMessage(obj.getObjectId());
				final StringBuilder html1 = new StringBuilder("<html><body><title>AI Params - " + obj.getName() + "</title>");
				html1.append("<table width=260>");

				if(params.isEmpty())
				{
					html1.append("<tr><td></td></tr>");
					html1.append("<tr><td><font color=\"LEVEL\">EMPTY</font></td></tr>");
					html1.append("<tr><td></td></tr>");
				}
				else
					for(final AiOptionsType type : AiOptionsType.values())
						html1.append("<tr><td><font color=\"LEVEL\">" + type + "</font>:</td><td>" + params.getString(type, "null") + "</td></tr>");

				html1.append("</table></body></html>");
				html.setHtml(html1.toString());
				activeChar.sendPacket(html);
				return true;
			}
		}
		return false;
	}

	public void startCheck(final L2Player activeChar)
	{
		if(!active)
		{
			System.out.println("Start EffectsRunner tasks");
			active = true;
			if(runner != null)
				runner.cancel(false);
			runner = L2LoginThreadPools.getInstance().scheduleGeneral(new EffectsRunner(activeChar), 1000);
		}
		else
		{
			count = 0;
			System.out.println("Stop EffectsRunner tasks");
			active = false;
			if(runner != null)
				runner.cancel(false);

			for(ScheduledFuture task : tasks)
			{
				if(task != null)
					task.cancel(true);
			}
			System.out.println("Runned " + tasks.size() + " tasks");
			tasks.clear();
		}
	}

	private void debug_stats(final L2Player target, final L2Player activeChar)
	{
		final Calculator[] calculators = target.getCalculators();

		String log_str = "---[ " + Util.datetimeFormatter.format(new Date()) + " debug for " + target.getName() + " ]---\r\n";
		log_str += "| Account.............. " + target.getAccountName() + "\r\n";
		log_str += "| IP................... " + target.getIP() + "\r\n";
		log_str += "| Name................. " + target.getName() + "\r\n";
		log_str += "| Level................ " + target.getLevel() + "\r\n";
		log_str += "| Class................ " + target.getClassId().getId() + "/" + CharTemplateTable.getClassNameById(target.getClassId().getId()) + "\r\n";
		log_str += "| Base................. " + !target.isSubClassActive() + "\r\n";
		log_str += "| --------------------\r\n";
		log_str += "| Dead................. " + target.isDead() + "\r\n";
		log_str += "| Invul................ " + target.isInvul(false) + "\r\n";
		log_str += "| Invul (Stats)........ " + (int) target.calcStat(Stats.INVULNERABLE, 0, null, null) + "\r\n";
		log_str += "| Invisible............ " + target.isInvisible() + "\r\n";
		log_str += "| Like dead............ " + target.isAlikeDead() + "\r\n";
		log_str += "| Noblesse Blessed..... " + target.isBlessedByNoblesse() + "\r\n";
		log_str += "| Teleporting.......... " + target.isTeleporting() + "\r\n";
		log_str += "| Deleting............. " + target.isDeleting() + "\r\n";
		log_str += "| Logout Started....... " + target.isLogoutStarted() + "\r\n";
		log_str += "| Fake Death........... " + target.isFakeDeath() + "\r\n";
		log_str += "| --------------------\r\n";
		log_str += "| CP................... " + (int) target.getCurrentCp() + "/" + target.getMaxCp() + "\r\n";
		log_str += "| HP................... " + (int) target.getCurrentHp() + "/" + target.getMaxHp() + "\r\n";
		log_str += "| MP................... " + (int) target.getCurrentMp() + "/" + target.getMaxMp() + "\r\n";
		log_str += "+ Stats----------------\r\n";
		for(final Calculator calculator : calculators)
		{
			if(calculator == null || calculator.getLast() == 0)
				continue;
			final Env env = new Env(target, activeChar, null);
			env.value = calculator.getBase();
			log_str += "| Stat: " + calculator._stat.getValue() + ", lastValue: " + Formulas.numFormatter.format(calculator.getLast()) + "\r\n";
			final Func[] funcs = calculator.getFunctions();
			for(int i = 0; i < funcs.length; i++)
			{
				String order = Integer.toHexString(funcs[i]._order).toUpperCase();
				if(order.length() == 1)
					order = "0" + order;

				String class_name = null;
				try
				{
					class_name = funcs[i].getClass().getSimpleName();
				}
				catch(final InternalError e)
				{
					class_name = "unknow";
				}

				log_str += "| \tFunc #" + i + "@ [0x" + order + "]" + class_name + "\t" + Formulas.numFormatter.format(env.value);
				if(funcs[i].getCondition() == null || funcs[i].getCondition().test(env))
					funcs[i].calc(env);
				log_str += " -> " + Formulas.numFormatter.format(env.value) + (funcs[i]._funcOwner != null ? "; owner: " + funcs[i]._funcOwner.toString() : "; no owner") + "\r\n";
			}
		}
		log_str += "+ Effects-------------\r\n";
		final L2Effect[] list = target.getEffectList().getAllEffects();
		final IntArrayList eskills = new IntArrayList(list.length);
		int x = 0;
		if(list != null && list.length > 0)
			for(final L2Effect e : list)
			{
				++x;
				if(!eskills.contains(e.getSkill().getId()))
					eskills.add(e.getSkill().getId());
				else
					log_str += "DUPE";
				log_str += "| #" + x + " " + e.effectToString() + ", " + Util.formatTime(e.getTimeLeft() / 1000) + "\r\n";
			}

		log_str += "+ Skills--------------\r\n";
		final TreeMap<Integer, L2Skill> skills = target.getAllSkillsSMap();
		for(final Entry<Integer, L2Skill> entry : skills.entrySet())
			log_str += "| " + entry.getValue() + "\r\n";

		log_str += "+ End------------------\r\n";
		final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
		Log.addDev(log_str, "debug_players" + "_" + datetimeFormatter.format(System.currentTimeMillis()), false);

		activeChar.sendMessage("Information about player saved successfully.");
	}

	public void showAggro(final L2Player activeChar)
	{
		final L2Object obj = activeChar.getTarget();
		if(obj == null || !obj.isNpc())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final L2NpcInstance npc = (L2NpcInstance) obj;
		StringBuilder dialog = new StringBuilder("<html><body><center><font color=\"LEVEL\">");
		dialog.append(npc.getName()).append("<br></font></center>");
		dialog.append("<table width=\"80%\"><tr><td>Attacker</td><td>Damage</td><td>Hate</td></tr>");

		// Сортировка аггролиста по хейту
		TreeSet<HateInfo> aggroList = new TreeSet<HateInfo>(new Comparator<HateInfo>()
		{
			@Override
			public int compare(HateInfo o1, HateInfo o2)
			{
				int hateDiff = o1.hate - o2.hate;
				if(hateDiff != 0)
					return hateDiff;
				return o1.damage - o2.damage;
			}
		});
		aggroList.addAll(getAggroList(npc));

		// Вывод результата
		for(HateInfo aggroInfo : aggroList.descendingSet())
			if(aggroInfo.npc != null)
				dialog.append("<tr><td>" + aggroInfo.npc.getName() + "</td><td>" + aggroInfo.damage + "</td><td>" + aggroInfo.hate + "</td></tr>");

		dialog.append("</table><br><center><button value=\"");
		dialog.append(activeChar.isLangRus() ? "Обновить" : "Refresh");
		dialog.append("\" action=\"bypass -h admin_debug_aggro\" width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center></body></html>");

		NpcHtmlMessage msg = new NpcHtmlMessage(5);
		msg.setHtml(Strings.bbParse(dialog.toString()));
		activeChar.sendPacket(msg);
	}

	public static GArray<HateInfo> getAggroList(final L2NpcInstance npc)
	{
		final GArray<HateInfo> temp = new GArray<HateInfo>();
		for(final L2NpcInstance cha : L2World.getAroundNpc(npc))
			if(cha != null)
			{
				final HateInfo hateInfo = cha.getHateList().get(npc);
				if(hateInfo != null)
				{
					HateInfo ai = new HateInfo(cha);
					ai.damage = hateInfo.damage;
					ai.hate = hateInfo.hate;
					temp.add(ai);
				}
			}
		return temp;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"выводит в консоль задания цели",
				"сбрасывает информацию об игроке в файл",
				"показывает AI параметры цели",
				"полное отображение HateList'a",
				"описание для команды admin_debug" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_DEBUG, AdminCommandHandler.GROUP_DEBUG, AdminCommandHandler.GROUP_DEBUG, AdminCommandHandler.GROUP_DEBUG, AdminCommandHandler.GROUP_DEBUG };
	}
}
