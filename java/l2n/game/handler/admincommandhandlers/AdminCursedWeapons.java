package l2n.game.handler.admincommandhandlers;

import javolution.text.TextBuilder;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.model.CursedWeapon;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ItemTable;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminCursedWeapons implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_cw_info",
			"admin_cw_remove",
			"admin_cw_goto",
			"admin_cw_reload",
			"admin_cw_add",
			"admin_cw_info_menu" };

	private int itemId;

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CursedWeapons)
			return false;

		final CursedWeaponsManager cwm = CursedWeaponsManager.getInstance();
		int id = 0;

		final StringTokenizer st = new StringTokenizer(command);
		st.nextToken();

		if(command.startsWith("admin_cw_info"))
		{
			if(!command.contains("menu"))
			{
				activeChar.sendMessage("======= Cursed Weapons: =======");
				for(final CursedWeapon cw : cwm.getCursedWeapons())
				{
					activeChar.sendMessage("> " + cw.getName() + " (" + cw.getItemId() + ")");
					if(cw.isActivated())
					{
						final L2Player pl = cw.getPlayer();
						activeChar.sendMessage("  Player holding: " + pl.getName());
						activeChar.sendMessage("  Player karma: " + cw.getPlayerKarma());
						activeChar.sendMessage("  Time Remaining: " + cw.getTimeLeft() / 60000 + " min.");
						activeChar.sendMessage("  Kills : " + cw.getNbKills());
					}
					else if(cw.isDropped())
					{
						activeChar.sendMessage("  Lying on the ground.");
						activeChar.sendMessage("  Time Remaining: " + cw.getTimeLeft() / 60000 + " min.");
						activeChar.sendMessage("  Kills : " + cw.getNbKills());
					}
					else
						activeChar.sendMessage("  Don't exist in the world.");
				}
			}
			else
			{
				final TextBuilder replyMSG = new TextBuilder();
				final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
				adminReply.setFile("data/html/admin/cwinfo.htm");
				for(final CursedWeapon cw : cwm.getCursedWeapons())
				{
					itemId = cw.getItemId();
					replyMSG.append("<table width=270><tr><td>Name:</td><td>" + cw.getName() + "</td></tr>");
					if(cw.isActivated())
					{
						final L2Player pl = cw.getPlayer();
						replyMSG.append("<tr><td>Weilder:</td><td>" + (pl == null ? "null" : pl.getName()) + "</td></tr>");
						replyMSG.append("<tr><td>Karma:</td><td>" + String.valueOf(cw.getPlayerKarma()) + "</td></tr>");
						replyMSG.append("<tr><td>Kills:</td><td>" + String.valueOf(cw.getPlayerPkKills()) + "/" + String.valueOf(cw.getNbKills()) + "</td></tr>");
						replyMSG.append("<tr><td>Time remaining:</td><td>" + String.valueOf(cw.getTimeLeft() / 60000) + " min.</td></tr>");
						replyMSG.append("<tr><td><button value=\"Remove\" action=\"bypass -h admin_cw_remove " + String.valueOf(itemId) + "\" width=73 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
						replyMSG.append("<td><button value=\"Go\" action=\"bypass -h admin_cw_goto " + String.valueOf(itemId) + "\" width=73 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
					}
					else if(cw.isDropped())
					{
						replyMSG.append("<tr><td>Position:</td><td>Lying on the ground</td></tr>");
						replyMSG.append("<tr><td>Time remaining:</td><td>" + String.valueOf(cw.getTimeLeft() / 60000) + " min.</td></tr>");
						replyMSG.append("<tr><td>Kills:</td><td>" + String.valueOf(cw.getNbKills()) + "</td></tr>");
						replyMSG.append("<tr><td><button value=\"Remove\" action=\"bypass -h admin_cw_remove " + String.valueOf(itemId) + "\" width=73 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
						replyMSG.append("<td><button value=\"Go\" action=\"bypass -h admin_cw_goto " + String.valueOf(itemId) + "\" width=73 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
					}
					else
					{
						replyMSG.append("<tr><td>Position:</td><td>Doesn't exist.</td></tr>");
						replyMSG.append("<tr><td><button value=\"Give to Target\" action=\"bypass -h admin_cw_add " + String.valueOf(itemId) + "\" width=99 height=15 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td><td></td></tr>");
					}
					replyMSG.append("</table>");
					replyMSG.append("<br>");
				}
				adminReply.replace("%cwinfo%", replyMSG.toString());
				activeChar.sendPacket(adminReply);
			}
		}
		else if(command.startsWith("admin_cw_reload"))
		{
			cwm.reload();
			activeChar.sendMessage("Cursed weapons reloaded.");
		}
		else
		{
			CursedWeapon cw = null;
			try
			{
				String parameter = st.nextToken();
				final Pattern pattern = Pattern.compile("[0-9]*");
				final Matcher regexp = pattern.matcher(parameter);
				if(regexp.matches())
					id = Integer.parseInt(parameter);
				else
				{
					parameter = parameter.replace('_', ' ');
					for(final CursedWeapon cwp : cwm.getCursedWeapons())
						if(cwp.getName().toLowerCase().contains(parameter.toLowerCase()))
						{
							id = cwp.getItemId();
							break;
						}
				}
				cw = cwm.getCursedWeapon(id);
				if(cw == null)
				{
					activeChar.sendMessage("Unknown cursed weapon ID.");
					return false;
				}
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Usage: //cw_remove|//cw_goto|//cw_add <itemid|name>");
			}

			if(cw == null)
				return false;

			if(command.startsWith("admin_cw_remove "))
				cw.endOfLife();
			else if(command.startsWith("admin_cw_goto "))
				activeChar.teleToLocation(cw.getLoc());
			else if(command.startsWith("admin_cw_add"))
			{
				if(cw.isActive())
					activeChar.sendMessage("This cursed weapon is already active.");
				else
				{
					final L2Object target = activeChar.getTarget();
					if(target != null && target.isPlayer())
					{
						final L2Player player = (L2Player) target;

						final L2ItemInstance item = ItemTable.getInstance().createItem(id, activeChar.getObjectId(), 0, "AdminCursedWeapons");
						player.getInventory().addItem(item);
						cwm.activate(player, item);
						cwm.showUsageTime(player, cw);
					}
				}
			}
			else
				activeChar.sendMessage("Unknown command.");
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_cw_info.htm",
				"file://com_cw_remove.htm",
				"file://com_cw_goto.htm",
				"file://com_cw_reload.htm",
				"file://com_cw_add.htm",
				"file://com_cw_info_menu.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_CW,
				AdminCommandHandler.GROUP_CW,
				AdminCommandHandler.GROUP_CW,
				AdminCommandHandler.GROUP_CW,
				AdminCommandHandler.GROUP_CW,
				AdminCommandHandler.GROUP_CW };
	}
}
