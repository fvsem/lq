package l2n.game.handler.admincommandhandlers;

import l2n.game.L2GameThreadPools;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.MonsterRace;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.DeleteObject;
import l2n.game.network.serverpackets.MonRaceInfo;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Location;

public class AdminMonsterRace implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_mons" };

	protected static int state = -1;

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.equalsIgnoreCase("admin_mons"))
		{
			if(!activeChar.getPlayerAccess().MonsterRace)
				return false;
			handleSendPacket(activeChar);
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleSendPacket(final L2Player activeChar)
	{
		/**
		 * -1 0 to initial the race 0 15322 to start race 13765 -1 in middle of race
		 * -1 0 to end the race
		 * 8003 to 8027
		 */

		final int[][] codes = { { -1, 0 }, { 0, 15322 }, { 13765, -1 }, { -1, 0 } };
		final MonsterRace race = MonsterRace.getInstance();

		if(state == -1)
		{
			state++;
			race.newRace();
			race.newSpeeds();
			activeChar.broadcastPacket(new MonRaceInfo(codes[state][0], codes[state][1], race.getMonsters(), race.getSpeeds()));
		}
		else if(state == 0)
		{
			state++;
			activeChar.sendPacket(new SystemMessage(SystemMessage.THEYRE_OFF));
			activeChar.broadcastPacket(new PlaySound("S_Race"));
			activeChar.broadcastPacket(new PlaySound(0, "ItemSound2.race_start", 1, 121209259, new Location(12125, 182487, -3559)));
			activeChar.broadcastPacket(new MonRaceInfo(codes[state][0], codes[state][1], race.getMonsters(), race.getSpeeds()));

			L2GameThreadPools.getInstance().scheduleGeneral(new RunRace(codes, activeChar), 5000);
		}

	}

	class RunRace implements Runnable
	{

		private final int[][] codes;
		private final L2Player activeChar;

		public RunRace(final int[][] codes, final L2Player activeChar)
		{
			this.codes = codes;
			this.activeChar = activeChar;
		}

		@Override
		public void run()
		{
			final int[][] speeds1 = MonsterRace.getInstance().getSpeeds();
			MonsterRace.getInstance().newSpeeds();
			final int[][] speeds2 = MonsterRace.getInstance().getSpeeds();

			final int[] speed = new int[8];
			for(int i = 0; i < 8; i++)
			{
				for(int j = 0; j < 20; j++)
				{
					System.out.println("Adding " + speeds1[i][j] + " and " + speeds2[i][j]);
					speed[i] += speeds1[i][j] * 1;
					++speeds2[i][j];
				}
				System.out.println("Total speed for " + (i + 1) + " = " + speed[i]);
			}

			activeChar.broadcastPacket(new MonRaceInfo(codes[2][0], codes[2][1], MonsterRace.getInstance().getMonsters(), MonsterRace.getInstance().getSpeeds()));
			L2GameThreadPools.getInstance().scheduleGeneral(new RunEnd(activeChar), 30000);
		}
	}

	class RunEnd implements Runnable
	{
		private final L2Player activeChar;

		public RunEnd(final L2Player activeChar)
		{
			this.activeChar = activeChar;
		}

		@Override
		public void run()
		{
			L2NpcInstance obj = null;

			for(int i = 0; i < 8; i++)
			{
				obj = MonsterRace.getInstance().getMonsters()[i];
				// FIXME i don't know, if it's needed (Styx)
				// L2World.removeObject(obj);
				activeChar.broadcastPacket(new DeleteObject(obj));

			}
			state = -1;
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_mons" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER4 };
	}
}
