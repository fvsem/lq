package l2n.game.handler.admincommandhandlers;

import javolution.text.TextBuilder;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;
import l2n.game.instancemanager.CastleManorManager.SeedProduction;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.network.serverpackets.NpcHtmlMessage;

import java.util.StringTokenizer;

public class AdminManor implements IAdminCommandHandler
{
	private static final String[] _adminCommands = {
			"admin_manor",
			"admin_manor_reset",
			"admin_manor_save",
			"admin_manor_disable" };

	@Override
	public boolean useAdminCommand(String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		final StringTokenizer st = new StringTokenizer(command);
		command = st.nextToken();

		if(command.equals("admin_manor"))
			showMainPage(activeChar);
		else if(command.equals("admin_manor_reset"))
		{
			int castleId = 0;
			try
			{
				castleId = Integer.parseInt(st.nextToken());
			}
			catch(final Exception e)
			{}

			if(castleId > 0)
			{
				final Castle castle = CastleManager.getInstance().getCastleByIndex(castleId);
				castle.setCropProcure(new GArray<CropProcure>(), CastleManorManager.PERIOD_CURRENT);
				castle.setCropProcure(new GArray<CropProcure>(), CastleManorManager.PERIOD_NEXT);
				castle.setSeedProduction(new GArray<SeedProduction>(), CastleManorManager.PERIOD_CURRENT);
				castle.setSeedProduction(new GArray<SeedProduction>(), CastleManorManager.PERIOD_NEXT);
				if(Config.MANOR_SAVE_ALL_ACTIONS)
				{
					castle.saveCropData();
					castle.saveSeedData();
				}
				activeChar.sendMessage("Manor data for " + castle.getName() + " was nulled");
			}
			else
			{
				for(final Castle castle : CastleManager.getInstance().getCastles().values())
				{
					castle.setCropProcure(new GArray<CropProcure>(), CastleManorManager.PERIOD_CURRENT);
					castle.setCropProcure(new GArray<CropProcure>(), CastleManorManager.PERIOD_NEXT);
					castle.setSeedProduction(new GArray<SeedProduction>(), CastleManorManager.PERIOD_CURRENT);
					castle.setSeedProduction(new GArray<SeedProduction>(), CastleManorManager.PERIOD_NEXT);
					if(Config.MANOR_SAVE_ALL_ACTIONS)
					{
						castle.saveCropData();
						castle.saveSeedData();
					}
				}
				activeChar.sendMessage("Manor data was nulled");
			}
			showMainPage(activeChar);
		}
		else if(command.equals("admin_manor_save"))
		{
			CastleManorManager.getInstance().save();
			activeChar.sendMessage("Manor System: all data saved");
			showMainPage(activeChar);
		}
		else if(command.equals("admin_manor_disable"))
		{
			final boolean mode = CastleManorManager.getInstance().isDisabled();
			CastleManorManager.getInstance().setDisabled(!mode);
			if(mode)
				activeChar.sendMessage("Manor System: enabled");
			else
				activeChar.sendMessage("Manor System: disabled");
			showMainPage(activeChar);
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	/*
	 * private String formatTime(long millis) //TODO разобраться зачем оно тут ?
	 * {
	 * String s = "";
	 * int secs = (int) millis / 1000;
	 * int mins = secs / 60;
	 * secs -= mins * 60;
	 * int hours = mins / 60;
	 * mins -= hours * 60;
	 * if(hours > 0)
	 * s += hours + ":";
	 * s += mins + ":";
	 * s += secs;
	 * return s;
	 * }
	 */
	private void showMainPage(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final TextBuilder replyMSG = new TextBuilder("<html><body>");

		replyMSG.append("<center><font color=\"LEVEL\"> [Manor System] </font></center><br>");
		replyMSG.append("<table width=\"100%\">");
		replyMSG.append("<tr><td>Disabled: " + (CastleManorManager.getInstance().isDisabled() ? "yes" : "no") + "</td>");
		replyMSG.append("<td>Under Maintenance: " + (CastleManorManager.getInstance().isUnderMaintenance() ? "yes" : "no") + "</td></tr>");
		replyMSG.append("<tr><td>Approved: " + (ServerVariables.getBool("ManorApproved") ? "yes" : "no") + "</td></tr>");
		replyMSG.append("</table>");

		replyMSG.append("<center><table>");
		replyMSG.append("<tr><td><button value=\"" + (CastleManorManager.getInstance().isDisabled() ? "Enable" : "Disable") + "\" action=\"bypass -h admin_manor_disable\" width=110 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Reset\" action=\"bypass -h admin_manor_reset\" width=110 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"Refresh\" action=\"bypass -h admin_manor\" width=110 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Back\" action=\"bypass -h admin_admin\" width=110 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table></center>");

		replyMSG.append("<br><center>Castle Information:<table width=\"100%\">");
		replyMSG.append("<tr><td></td><td>Current Period</td><td>Next Period</td></tr>");

		for(final Castle c : CastleManager.getInstance().getCastles().values())
			replyMSG.append("<tr><td>" + c.getName() + "</td>" + "<td>" + c.getManorCost(CastleManorManager.PERIOD_CURRENT) + "a</td>" + "<td>" + c.getManorCost(CastleManorManager.PERIOD_NEXT) + "a</td>" + "</tr>");

		replyMSG.append("</table><br>");

		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"admin_manor",
				"admin_manor_reset",
				"admin_manor_save",
				"admin_manor_disable" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER4 };
	}
}
