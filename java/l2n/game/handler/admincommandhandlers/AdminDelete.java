package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Log;


public class AdminDelete implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_delete" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Delete)
			return false;
		if(command.equals("admin_delete"))
			handleDelete(activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleDelete(final L2Player activeChar)
	{
		final L2Object obj = activeChar.getTarget();
		if(obj != null && obj instanceof L2NpcInstance)
		{
			final L2NpcInstance target = (L2NpcInstance) obj;
			target.deleteMe();

			final L2Spawn spawn = target.getSpawn();
			if(spawn != null)
				spawn.stopRespawn();

			Log.add("deleted NPC" + target.getObjectId(), "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "file://com_delete.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER1 };
	}
}
