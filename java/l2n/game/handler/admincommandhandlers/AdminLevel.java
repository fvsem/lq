package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;

import java.util.StringTokenizer;

public class AdminLevel implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_sethp",
			"admin_setmp",
			"admin_addLevel",
			"admin_setLevel" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar == null)
			return false;

		if(!activeChar.getPlayerAccess().EditChar)
			return false;

		if(activeChar.getTarget() == null)
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		final StringTokenizer st = new StringTokenizer(command, " ");
		final String actualCommand = st.nextToken();

		String val = "";
		if(st.countTokens() >= 1)
			val = st.nextToken();

		if(actualCommand.equalsIgnoreCase("admin_addLevel"))
			try
			{
				if(!activeChar.getTarget().isPlayer())
				{
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
					return false;
				}
				final L2Player target = (L2Player) activeChar.getTarget();

				final byte level = (byte) (target.getLevel() + Byte.parseByte(val));
				if(level >= 1 && level <= Experience.getMaxLevel())
				{
					final long exp_add = Experience.LEVEL[level] - target.getExp();
					target.addExpAndSp(exp_add, 0, false, false);
				}
				else
				{
					activeChar.sendMessage("You must specify level between 1 and " + Experience.getMaxLevel() + ".");
					return false;
				}
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Wrong Number Format");
			}
		else if(actualCommand.equalsIgnoreCase("admin_setLevel"))
			try
			{
				if(!activeChar.getTarget().isPlayer())
				{
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
					return false;
				}
				final L2Player target = (L2Player) activeChar.getTarget();

				final byte level = Byte.parseByte(val);
				if(level >= 1 && level <= Experience.getMaxLevel())
				{
					final long exp_add = Experience.LEVEL[level] - target.getExp();
					target.addExpAndSp(exp_add, 0, false, false);
				}
				else
				{
					activeChar.sendMessage("You must specify level between 1 and " + Experience.getMaxLevel() + ".");
					return false;
				}
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("You must specify level between 1 and " + Experience.getMaxLevel() + ".");
				return false;
			}
		else if(actualCommand.equalsIgnoreCase("admin_sethp"))
			try
			{
				if(!activeChar.getTarget().isCharacter())
				{
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
					return false;
				}
				final L2Character target = (L2Character) activeChar.getTarget();

				final int hp = Math.min(target.getMaxHp(), Math.max(Integer.parseInt(val), 0));
				target.setCurrentHp(hp, false);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Вводить необходимо только числа.");
				return false;
			}
		else if(actualCommand.equalsIgnoreCase("admin_setmp"))
			try
			{
				if(!activeChar.getTarget().isCharacter())
				{
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
					return false;
				}
				final L2Character target = (L2Character) activeChar.getTarget();

				final int mp = Math.min(target.getMaxMp(), Math.max(Integer.parseInt(val), 0));
				target.setCurrentMp(mp);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Вводить необходимо только числа.");
				return false;
			}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"устанавливает HP (Heal Point)",
				"устанавливает MP (Mana Point)",
				"добавляет персонажу уровни",
				"устанавливает персонажу введённый уровень" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
