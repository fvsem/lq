package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.network.serverpackets.SystemMessage;

import java.lang.reflect.Field;
import java.util.StringTokenizer;

public class AdminAdmin implements IAdminCommandHandler
{
	private static final String[] _adminCommands = {
			"admin_admin",
			"admin_play_sounds",
			"admin_play_sound",
			"admin_silence",
			"admin_atmosphere",
			"admin_diet",
			"admin_tradeoff",
			"admin_config",
			"admin_show_html",
			"admin_npctable" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;
		if(command.equalsIgnoreCase("admin_admin"))
			AdminHelpPage.showHelpPage(activeChar, "admin.htm");
		if(command.equalsIgnoreCase("admin_play_sounds"))
			AdminHelpPage.showHelpPage(activeChar, "songs/songs.htm");
		else if(command.startsWith("admin_play_sounds"))
			try
			{
				AdminHelpPage.showHelpPage(activeChar, "songs/songs" + command.substring(17) + ".htm");
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_play_sound"))
			try
			{
				playAdminSound(activeChar, command.substring(17));
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_silence"))
			if(activeChar.getMessageRefusal()) // already in message refusal
			// mode
			{
				activeChar.unsetVar("gm_silence");
				activeChar.setMessageRefusal(false);
				activeChar.sendPacket(new SystemMessage(SystemMessage.MESSAGE_ACCEPTANCE_MODE));
			}
			else
			{
				if(Config.SAVE_GM_EFFECTS)
					activeChar.setVar("gm_silence", "true");
				activeChar.setMessageRefusal(true);
				activeChar.sendPacket(new SystemMessage(SystemMessage.MESSAGE_REFUSAL_MODE));
			}
		else if(command.startsWith("admin_tradeoff"))
			try
			{
				final StringTokenizer st = new StringTokenizer(command);
				st.nextToken();
				if(st.nextToken().equalsIgnoreCase("on"))
				{
					activeChar.setTradeRefusal(true);
					activeChar.sendMessage("tradeoff enabled");
				}
				else if(st.nextToken().equalsIgnoreCase("off"))
				{
					activeChar.setTradeRefusal(false);
					activeChar.sendMessage("tradeoff disabled");
				}
			}
			catch(final Exception ex)
			{
				if(activeChar.getTradeRefusal())
					activeChar.sendMessage("tradeoff currently enabled");
				else
					activeChar.sendMessage("tradeoff currently disabled");
			}
		else if(command.startsWith("admin_config"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			st.nextToken();
			String pName = "";
			try
			{
				final String[] parameter = st.nextToken().split("=");
				pName = parameter[0].trim();
				final String pValue = parameter[1].trim();
				final Field field = Config.class.getField(pName);
				if(setField(activeChar, field, pValue))
					activeChar.sendMessage("Config set succesfully");
			}
			catch(final NoSuchFieldException e)
			{
				activeChar.sendMessage("Параметр " + pName + " не существует");
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Использование:  config parameter = value");
			}
		}
		else if(command.startsWith("admin_show_html"))
		{
			final String html = command.substring(16);
			try
			{
				if(html != null)
					AdminHelpPage.showHelpPage(activeChar, html);
				else
					activeChar.sendMessage("HTML - Страница не существует.");
			}
			catch(final Exception npe)
			{
				activeChar.sendMessage("HTML - Страница не существует.");
			}
		}
		else if(command.startsWith("admin_npctable"))
		{}

		return true;
	}

	private boolean setField(final L2Player activeChar, final Field field, final String param)
	{
		try
		{
			field.setBoolean(null, Boolean.parseBoolean(param));
		}
		catch(final Exception e)
		{
			try
			{
				field.setInt(null, Integer.parseInt(param));
			}
			catch(final Exception e1)
			{
				try
				{
					field.setLong(null, Long.parseLong(param));
				}
				catch(final Exception e2)
				{
					try
					{
						field.set(null, param);
					}
					catch(final Exception e3)
					{
						activeChar.sendMessage("Ошибка в параметре: " + param + " " + e.getMessage());
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	public void playAdminSound(final L2Player activeChar, final String sound)
	{
		activeChar.broadcastPacket(new PlaySound(sound));
		AdminHelpPage.showHelpPage(activeChar, "admin.htm");
		activeChar.sendMessage("Playing " + sound + ".");
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_admin.htm",
				"Описание команды play_sounds",
				"Описание команды play_sound",
				"Описание команды silence",
				"Описание команды atmosphere",
				"Описание команды diet",
				"Описание команды tradeoff",
				"Описание команды config",
				"Описание команды show_html",
				"Описание команды npctable" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER };
	}
}
