package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

public class AdminKill implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_kill" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().HealKillRes)
			return false;
		if(command.startsWith("admin_kill "))
			handleKill(activeChar, command.split(" ")[1]);
		if(command.equals("admin_kill"))
			handleKill(activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleKill(final L2Player activeChar)
	{
		handleKill(activeChar, null);
	}

	private void handleKill(final L2Player activeChar, final String player)
	{
		L2Object obj = activeChar.getTarget();
		if(player != null)
		{
			final L2Player plyr = L2ObjectsStorage.getPlayer(player);
			if(plyr != null)
				obj = plyr;
			else
			{
				final int radius = Math.max(Integer.parseInt(player), 100);
				for(final L2Character character : activeChar.getAroundCharacters(radius))
					character.reduceCurrentHp(character.getMaxHp() + character.getMaxCp() + 1, character, null, true, true, false, false, false);
				activeChar.sendMessage("Killed within " + radius + " unit radius.");
				return;
			}
		}

		if(obj != null && obj.isCharacter())
		{
			final L2Character target = (L2Character) obj;
			target.reduceCurrentHp(target.getMaxHp() + target.getMaxCp() + 1, activeChar, null, true, true, false, false, false);
			Log.add("kill character " + target.getObjectId() + " " + target.getName(), "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "Описание команды kill" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_EDITCHAR };
	}
}
