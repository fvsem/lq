package l2n.game.handler.admincommandhandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.geodata.GeoEngine;
import l2n.game.geodata.PathFindBuffers;
import l2n.game.geodata.geoeditorcon.GeoEditorConnector;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

public class AdminGeodata implements IAdminCommandHandler
{
	private static String[] _adminCommands =
	{
			"admin_geo_z",
			"admin_geo_type",
			"admin_geo_nswe",
			"admin_geo_los",
			"admin_geo_load",
			"admin_geo_info",
			"admin_geo_dump",
			"admin_pf_stat",
			"admin_geoeditor_connect",
			"admin_geoeditor_join",
			"admin_geoeditor_leave"
	};

	private enum CommandEnum
	{
		admin_geo_z, // 0
		admin_geo_type, // 1
		admin_geo_nswe, // 2
		admin_geo_los, // 3
		admin_geo_load, // 4
		admin_geo_info, // 5
		admin_geo_dump, // 6
		admin_pf_stat, // 7
		admin_geoeditor_connect, // 10
		admin_geoeditor_join, // 11
		admin_geoeditor_leave
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanReload)
			return false;

		final String[] wordList = command.split(" ");
		CommandEnum comm;

		try
		{
			comm = CommandEnum.valueOf(wordList[0]);
		}
		catch(final Exception e)
		{
			return false;
		}

		final CommandEnum commandEnum = comm;
		switch (commandEnum)
		{
			case admin_geo_z:
				activeChar.sendMessage("GeoEngine: Geo_Z = " + GeoEngine.getHeight(activeChar.getLoc()) + " Loc_Z = " + activeChar.getZ());
				break;
			case admin_geo_type:
				final int type = GeoEngine.getType(activeChar.getX(), activeChar.getY());
				activeChar.sendMessage("GeoEngine: Geo_Type = " + type);
				break;
			case admin_geo_nswe:
				String result = "";
				final int nswe = GeoEngine.getNSWE(activeChar.getX(), activeChar.getY(), activeChar.getZ());
				if((nswe & 8) == 0)
					result += " N";
				if((nswe & 4) == 0)
					result += " S";
				if((nswe & 2) == 0)
					result += " W";
				if((nswe & 1) == 0)
					result += " E";
				activeChar.sendMessage("GeoEngine: Geo_NSWE -> " + nswe + "->" + result);
				break;
			case admin_geo_los:
				if(activeChar.getTarget() != null)
					if(GeoEngine.canSeeTarget(activeChar, activeChar.getTarget(), false))
						activeChar.sendMessage("GeoEngine: Can See Target");
					else
						activeChar.sendMessage("GeoEngine: Can't See Target");
				else
					activeChar.sendMessage("None Target!");
				break;
			case admin_geo_load:
				if(wordList.length != 3)
				{
					activeChar.sendMessage("Usage: //geo_load <regionX> <regionY>");
					break;
				}
				try
				{
					final byte rx = Byte.parseByte(wordList[1]);
					final byte ry = Byte.parseByte(wordList[2]);
					if(GeoEngine.LoadGeodataFile(rx, ry))
						activeChar.sendMessage("GeoEngine: Регион [" + rx + "," + ry + "] успешно загружен");
					else
						activeChar.sendMessage("GeoEngine: Регион [" + rx + "," + ry + "] не загрузился");
				}
				catch(final Exception e)
				{
					activeChar.sendMessage(new CustomMessage("common.Error", activeChar));
				}
				break;
			case admin_geo_dump:
				if(wordList.length > 2)
				{
					GeoEngine.DumpGeodataFile(Byte.parseByte(wordList[1]), Byte.parseByte(wordList[2]));
					activeChar.sendMessage("Квадрат геодаты сохранен " + wordList[1] + "_" + wordList[2]);
				}
				GeoEngine.DumpGeodataFile(activeChar.getX(), activeChar.getY());
				activeChar.sendMessage("Текущий квадрат геодаты сохранен");
				break;
			case admin_pf_stat:
				AdminHelpPage.showHelpHtml(activeChar, PathFindBuffers.getStats().toL2Html());
				break;
			case admin_geoeditor_connect:
				try
				{
					final int ticks = Integer.parseInt(command.substring(24));
					GeoEditorConnector.getInstance().connect(activeChar, ticks);
				}
				catch(final Exception e)
				{
					activeChar.sendMessage("Usage: //geoeditor_connect <number>");
				}
				break;
			case admin_geoeditor_join:
				GeoEditorConnector.getInstance().join(activeChar);
				break;
			case admin_geoeditor_leave:
				GeoEditorConnector.getInstance().leave(activeChar);
				break;
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[]
		{
				"Описание команды geo_z",
				"Описание команды geo_type",
				"Описание команды geo_nswe",
				"Описание команды geo_los",
				"Описание команды geo_load",
				"Описание команды geo_info",
				"Описание команды geo_dump",
				"Описание команды pf_stat",
				"Описание команды geoeditor_connect",
				"Описание команды geoeditor_join",
				"Описание команды geoeditor_leave"
		};
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[]
		{
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA,
				AdminCommandHandler.GROUP_GEODATA
		};
	}
}
