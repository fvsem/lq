package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

public class AdminGm implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_gm" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(true)
			return false;
		if(!activeChar.getPlayerAccess().IsGM)
			return false;
		if(command.equals("admin_gm"))
			handleGm(activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleGm(final L2Player activeChar)
	{
		if(activeChar.getPlayerAccess().IsGM)
		{
			activeChar.getPlayerAccess().IsGM = false;
			activeChar.sendMessage("You no longer have GM status.");
			Log.add("GM: " + activeChar.getName() + "(" + activeChar.getObjectId() + ") turned his GM status off", "gm_ext_actions", activeChar);
		}
		else
		{
			activeChar.getPlayerAccess().IsGM = true;
			activeChar.sendMessage("You have GM status now.");
			Log.add("GM: " + activeChar.getName() + "(" + activeChar.getObjectId() + ") turned his GM status on", "gm_ext_actions", activeChar);
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "Описание команды gm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER3 };
	}
}
