package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.util.Location;

import java.util.StringTokenizer;

public class AdminMenu implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_char_manage",
			"admin_teleport_character_to_menu",
			"admin_recall_char_menu",
			"admin_goto_char_menu",
			"admin_kick_menu",
			"admin_kill_menu",
			"admin_ban_menu",
			"admin_unban_menu" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		if(command.equals("admin_char_manage"))
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		else if(command.startsWith("admin_teleport_character_to_menu"))
		{
			final String[] data = command.split(" ");
			if(data.length == 5)
			{
				final String playerName = data[1];
				final L2Player player = L2ObjectsStorage.getPlayer(playerName);
				if(player != null)
					teleportCharacter(player, new Location(Integer.parseInt(data[2]), Integer.parseInt(data[3]), Integer.parseInt(data[4])), activeChar);
			}
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		}
		else if(command.startsWith("admin_recall_char_menu"))
			try
			{
				final String targetName = command.substring(23);
				final L2Player player = L2ObjectsStorage.getPlayer(targetName);
				teleportCharacter(player, activeChar.getLoc(), activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_goto_char_menu"))
			try
			{
				final String targetName = command.substring(21);
				final L2Player player = L2ObjectsStorage.getPlayer(targetName);
				teleportToCharacter(activeChar, player);
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equals("admin_kill_menu"))
		{
			L2Object obj = activeChar.getTarget();
			final StringTokenizer st = new StringTokenizer(command);
			if(st.countTokens() > 1)
			{
				st.nextToken();
				final String player = st.nextToken();
				final L2Player plyr = L2ObjectsStorage.getPlayer(player);
				if(plyr != null)
					activeChar.sendMessage("You kicked " + plyr.getName() + " from the game.");
				else
					activeChar.sendMessage("Player " + player + " not found in game.");
				obj = plyr;
			}
			if(obj != null && obj.isCharacter())
			{
				final L2Character target = (L2Character) obj;
				target.reduceCurrentHp(target.getMaxHp() + 1, activeChar, null, true, true, true, false, false);
			}
			else
				activeChar.sendPacket(Msg.INCORRECT_TARGET);
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		}
		else if(command.startsWith("admin_kick_menu"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			if(st.countTokens() > 1)
			{
				st.nextToken();
				final String player = st.nextToken();
				final L2Player plyr = L2ObjectsStorage.getPlayer(player);
				if(plyr != null)
					plyr.logout(false, false, true, true);
				if(plyr != null)
					activeChar.sendMessage("You kicked " + plyr.getName() + " from the game.");
				else
					activeChar.sendMessage("Player " + player + " not found in game.");
			}
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		}
		else if(command.startsWith("admin_ban_menu"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			if(st.countTokens() > 1)
			{
				st.nextToken();
				final String player = st.nextToken();
				final L2Player plyr = L2ObjectsStorage.getPlayer(player);
				if(plyr != null)
				{
					plyr.setAccountAccesslevel(-100, "admin_ban_menu", -1);
					plyr.logout(false, false, true, true);
				}
			}
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		}
		else if(command.startsWith("admin_unban_menu"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			if(st.countTokens() > 1)
			{
				st.nextToken();
				final String player = st.nextToken();
				final L2Player plyr = L2ObjectsStorage.getPlayer(player);
				if(plyr != null)
					plyr.setAccountAccesslevel(0, "admin_unban_menu", 0);
			}
			AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void teleportCharacter(final L2Player player, final Location loc, final L2Player activeChar)
	{
		if(player != null)
		{
			player.sendMessage("Admin is teleporting you.");
			player.teleToLocation(loc);
		}
		AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
	}

	private void teleportToCharacter(final L2Player activeChar, final L2Object target)
	{
		L2Player player;
		if(target != null && target.isPlayer())
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		if(player.getObjectId() == activeChar.getObjectId())
			activeChar.sendMessage("You cannot self teleport.");
		else
		{
			activeChar.teleToLocation(player.getLoc());
			activeChar.sendMessage("You have teleported to character " + player.getName() + ".");
		}
		AdminHelpPage.showHelpPage(activeChar, "charmanage.htm");
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды char_manage",
				"Описание команды teleport_character_to_menu",
				"Описание команды recall_char_menu",
				"Описание команды goto_char_menu",
				"Описание команды kick_menu",
				"Описание команды kill_menu",
				"Описание команды ban_menu",
				"Описание команды unban_menu" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE,
				AdminCommandHandler.GROUP_NONE };
	}
}
