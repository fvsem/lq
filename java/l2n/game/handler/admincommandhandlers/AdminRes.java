package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.Revive;
import l2n.game.network.serverpackets.SocialAction;
import l2n.util.Log;

public class AdminRes implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_res" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().HealKillRes)
			return false;
		if(command.startsWith("admin_res "))
			handleRes(activeChar, command.split(" ")[1]);
		if(command.equals("admin_res"))
			handleRes(activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleRes(final L2Player activeChar)
	{
		handleRes(activeChar, null);
	}

	private void handleRes(final L2Player activeChar, final String player)
	{
		L2Object obj = activeChar.getTarget();
		if(player != null)
		{
			final L2Player plyr = L2ObjectsStorage.getPlayer(player);
			if(plyr != null)
				obj = plyr;
			else
				try
				{
					final int radius = Math.max(Integer.parseInt(player), 100);
					for(final L2Character character : activeChar.getAroundCharacters(radius))
					{
						character.setCurrentHpMp(character.getMaxHp(), character.getMaxMp(), true);
						character.setCurrentCp(character.getMaxCp());
						if(character.isPlayer())
							((L2Player) character).restoreExp();
						// If the target is an NPC, then abort it's auto decay and respawn.
						else if(character.isNpc())
							((L2NpcInstance) character).stopDecay();

						character.broadcastPacket(new SocialAction(character.getObjectId(), 15));
						character.broadcastPacket(new Revive(character));
						character.doRevive();
					}
					activeChar.sendMessage("Resurrected within " + radius + " unit radius.");
					return;
				}
				catch(final NumberFormatException e)
				{
					activeChar.sendMessage("Enter valid player name or radius");
					return;
				}
		}

		if(obj == null)
			obj = activeChar;
		if(obj.isCharacter())
		{
			final L2Character target = (L2Character) obj;
			if(!target.isDead())
				return;

			target.setCurrentHpMp(target.getMaxHp(), target.getMaxMp());
			target.setCurrentCp(target.getMaxCp());
			// GM Resurrection will restore any lost exp
			if(target.isPlayer())
			{
				final L2Player deadplayer = (L2Player) target;
				deadplayer.restoreExp();
			}
			target.broadcastPacket(new SocialAction(target.getObjectId(), 15));
			target.broadcastPacket(new Revive(target));
			target.doRevive();
			Log.add("resurrected character " + target.getObjectId() + " " + target.getName(), "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_res" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_EDITCHAR };
	}
}
