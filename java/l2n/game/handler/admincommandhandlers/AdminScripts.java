package l2n.game.handler.admincommandhandlers;

import l2n.extensions.scripts.Scripts;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

import java.util.StringTokenizer;

public class AdminScripts implements IAdminCommandHandler
{
	private static final String[] _adminCommands = {
			"admin_scripts_reload",
			"admin_sreload",
			"admin_sqreload" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player player)
	{
		if(!player.getPlayerAccess().IsGM)
			return false;
		if(command.startsWith("admin_scripts_reload") || command.startsWith("admin_sreload"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			if(!st.hasMoreTokens())
				return false;
			st.nextToken();
			final String param = st.nextToken();
			if(param.equalsIgnoreCase("all"))
			{
				player.sendMessage("Scripts reload starting...");
				if(Scripts.getInstance().reload())
					player.sendMessage("Scripts reloaded with errors. Loaded " + Scripts.getInstance().getClasses().size() + " classes.");
				else
					player.sendMessage("Scripts successfully reloaded. Loaded " + Scripts.getInstance().getClasses().size() + " classes.");
			}
			else if(Scripts.getInstance().reloadClass(param))
				player.sendMessage("Scripts reloaded with errors. Loaded " + Scripts.getInstance().getClasses().size() + " classes.");
			else
				player.sendMessage("Scripts successfully reloaded. Loaded " + Scripts.getInstance().getClasses().size() + " classes.");
		}
		else if(command.startsWith("admin_sqreload"))
		{
			final StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			final String quest = st.nextToken();
			if(Scripts.getInstance().reloadQuest(quest))
				player.sendMessage("Quest '" + quest + "' reloaded with errors.");
			else
				player.sendMessage("Quest '" + quest + "' successfully reloaded.");
			reloadQuestStates(player);
		}
		return true;
	}

	private void reloadQuestStates(final L2Player p)
	{
		for(final QuestState qs : p.getAllQuestsStates())
			p.delQuestState(qs.getQuest().getName());

		Quest.playerEnter(p);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Перезагрузка скриптов сервера на лету",
				"Перезагрузка скриптов сервера на лету",
				"Перезагрузка квестов на лету" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER };
	}
}
