package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.commons.text.Strings;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.Announcements;
import l2n.game.TradeController;
import l2n.game.communitybbs.handlers.StatisticHandler;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.OlympiadDatabase;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestRewardTable;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.*;
import l2n.util.Files;
import l2n.util.HWID;

public class AdminReload implements IAdminCommandHandler
{
	private final static String[] ADMIN_COMMANDS = {
			"admin_reload_multisell",
			"admin_reload_announcements",
			"admin_reload_gmaccess",
			"admin_reload_htm",
			"admin_reload_qs",
			"admin_reload_qs_help",
			"admin_reload_loc",
			"admin_reload_skills",
			"admin_reload_npc",
			"admin_reload_spawn",
			"admin_reload_raidspawns",
			"admin_reload_fish",
			"admin_reload_abuse",
			"admin_reload_translit",
			"admin_reload_shops",
			"admin_reload_instances",
			"admin_reload_conf",
			"admin_reload_dynamic_rate",
			"admin_reload_locale",
			"admin_reload_hwid_bonus",
			"admin_reload_nobles",
			"admin_reload_qrewards" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CanReload)
			return false;
		if(command.equalsIgnoreCase("admin_reload_multisell"))
		{
			activeChar.sendMessage("Reloading multisell...");
			try
			{
				L2Multisell.getInstance().reload();
			}
			catch(final Exception e)
			{
				return false;
			}
			activeChar.sendMessage("Multisell list reloaded!");
		}
		else if(command.equalsIgnoreCase("admin_reload_gmaccess"))
		{
			activeChar.sendMessage("Reloading GMAccess...");
			try
			{
				Config.loadGMAccess();
				for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
					if(!Config.EVERYBODY_HAS_ADMIN_RIGHTS)
						player.setPlayerAccess(Config.gmlist.get(player.getObjectId()));
					else
						player.setPlayerAccess(Config.gmlist.get(new Integer(0)));
			}
			catch(final Exception e)
			{
				return false;
			}
			activeChar.sendMessage("GMAccess reloaded!");
		}
		else if(command.equalsIgnoreCase("admin_reload_htm"))
		{
			activeChar.sendMessage("Reloading HTML cache...");
			Files.cacheClean();
			StatisticHandler.clearCache();
			activeChar.sendMessage("HTML cache clearned.");
		}
		else if(command.equalsIgnoreCase("admin_reload_dynamic_rate"))
		{
			activeChar.sendMessage("Reloading Dynamic Rate Table...");
			DynamicRateTable.getInstance().reload();
			activeChar.sendMessage("Dynamic Rate Table reloaded!");
		}
		else if(command.equalsIgnoreCase("admin_reload_announcements"))
		{
			activeChar.sendMessage("Reloading announcements...");
			Announcements.getInstance().loadAnnouncements();
			Announcements.getInstance().listAnnouncements(activeChar);
		}
		if(command.startsWith("admin_reload_qs"))
		{
			if(command.endsWith("all"))
			{
				activeChar.sendMessage("Reloading all QuestStates...");
				for(final L2Player p : L2ObjectsStorage.getAllPlayersForIterate())
					reloadQuestStates(p);
			}
			else
			{
				final L2Object t = activeChar.getTarget();

				if(t != null && t.isPlayer())
				{
					final L2Player p = (L2Player) t;
					reloadQuestStates(p);
				}
				else
					reloadQuestStates(activeChar);
			}
			return true;
		}
		else if(command.startsWith("admin_reload_qs_help"))
		{
			activeChar.sendMessage("");
			activeChar.sendMessage("Quest Help:");
			activeChar.sendMessage("reload_qs_help - This Message.");
			activeChar.sendMessage("reload_qs <selected target> - reload all quest states for target.");
			activeChar.sendMessage("reload_qs <no target or target is not player> - reload quests for self.");
			activeChar.sendMessage("reload_qs all - reload quests for all players in world.");
			activeChar.sendMessage("");

			return true;
		}
		else if(command.equalsIgnoreCase("admin_reload_loc"))
		{
			activeChar.sendMessage("Reloading locations...");
			TerritoryTable.getInstance().reloadData();
			ZoneManager.getInstance().reload();
			GmListTable.broadcastMessageToGMs("Locations and zones reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_skills"))
		{
			activeChar.sendMessage("Reloading skills...");
			SkillTable.getInstance().reload(true);
			GmListTable.broadcastMessageToGMs("Skill table reloaded by " + activeChar.getName() + ".");
			System.out.println("Skill table reloaded by " + activeChar.getName() + ".");
		}
		else if(command.equalsIgnoreCase("admin_reload_npc"))
		{
			activeChar.sendMessage("Reloading NPCs...");
			NpcTable.getInstance().reloadAllNpc();
			GmListTable.broadcastMessageToGMs("Npc table reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_spawn"))
		{
			activeChar.sendMessage("Reloading spawns...");
			SpawnTable.getInstance().reloadAll();
			GmListTable.broadcastMessageToGMs("All npc respawned.");
		}
		else if(command.equalsIgnoreCase("admin_reload_raidspawns"))
		{
			activeChar.sendMessage("Reloading RaidBossSpawnManager...");
			RaidBossSpawnManager.getInstance().reloadBosses();
			activeChar.sendMessage("RaidBossSpawnManager reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_fish"))
		{
			activeChar.sendMessage("Reloading fish...");
			FishTable.getInstance().reload();
			GmListTable.broadcastMessageToGMs("Fish table reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_abuse"))
		{
			activeChar.sendMessage("Reloading abuse...");
			Config.abuseLoad();
			GmListTable.broadcastMessageToGMs("Abuse reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_translit"))
		{
			activeChar.sendMessage("Reloading translit...");
			Strings.reload();
			GmListTable.broadcastMessageToGMs("Translit reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_shops"))
		{
			activeChar.sendMessage("Reloading shops...");
			TradeController.reload();
			GmListTable.broadcastMessageToGMs("Shops reloaded.");
		}
		else if(command.equalsIgnoreCase("admin_reload_instances"))
		{
			activeChar.sendMessage("Reloading instance zones...");
			InstanceManager.getInstance().reload();
			DimensionalRiftManager.getInstance().reload();
			GmListTable.broadcastMessageToGMs("Instances reloaded.");
		}
		else if(command.startsWith("admin_reload_conf "))
		{
			final String[] data = command.substring(18).split(" ");
			GmListTable.broadcastMessageToGMs(Config.reloadConfigByName(data[0]));
		}
		else if(command.equalsIgnoreCase("admin_reload_locale"))
		{
			CustomMessage.reload();
			GmListTable.broadcastMessageToGMs("Localization reloaded");
		}
		else if(command.equalsIgnoreCase("admin_reload_hwid_bonus"))
		{
			HWID.reloadBannedHWIDs();
			HWID.reloadBonusHWIDs();
			GmListTable.broadcastMessageToGMs("HWID bonus/bans reloaded");
		}
		else if(command.equalsIgnoreCase("admin_reload_nobles"))
		{
			OlympiadDatabase.loadNobles();
			OlympiadDatabase.loadNoblesRank();
			GmListTable.broadcastMessageToGMs("Nobles/Nobles Rank reloaded");
		}
		else if(command.equalsIgnoreCase("admin_reload_qrewards"))
		{
			QuestRewardTable.getInstance().reload();
			GmListTable.broadcastMessageToGMs("Quest Rewards reloaded");
		}

		return true;
	}

	private void reloadQuestStates(final L2Player p)
	{
		for(final QuestState qs : p.getAllQuestsStates())
			p.delQuestState(qs.getQuest().getName());
		Quest.playerEnter(p);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды reload_multisell",
				"Описание команды reload_announcements",
				"Описание команды reload_gmaccess",
				"Описание команды reload_htm",
				"Описание команды reload_qs",
				"Описание команды reload_qs_help",
				"Описание команды reload_loc",
				"Описание команды reload_skills",
				"Описание команды reload_npc",
				"Описание команды reload_spawn",
				"Описание команды reload_raidspawns",
				"Описание команды reload_fish",
				"Описание команды reload_abuse",
				"Описание команды reload_translit",
				"Описание команды reload_shops",
				"Описание команды reload_instances",
				"Описание команды reload_conf",
				"Описание команды reload_dynamic_rate",
				"Описание команды reload_locale",
				"Описание команды reload_hwid_bonus",
				"Описание команды reload_nobles",
				"перегружает награды за квесты " };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD,
				AdminCommandHandler.GROUP_RELOAD };
	}
}
