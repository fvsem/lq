package l2n.game.handler.admincommandhandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.BanIP;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.login.protect.BanInformation;
import l2n.util.Log;
import l2n.util.Util;

import java.util.Collection;

public class AdminIP implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_ipbanlist",
			"admin_ipban",
			"admin_ipblock",
			"admin_ipunban",
			"admin_ipunblock",
			"admin_ipcharban",
			"admin_ipchar",
			"admin_charip" };

	private enum CommandEnum
	{
		admin_ipbanlist,
		admin_ipban,
		admin_ipblock,
		admin_ipunban,
		admin_ipunblock,
		admin_ipcharban,
		admin_ipchar,
		admin_charip
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Ban)
			return false;

		final String[] wordList = command.split(" ");
		CommandEnum cmd;
		try
		{
			cmd = CommandEnum.valueOf(wordList[0]);
		}
		catch(final Exception e)
		{
			return false;
		}

		switch (cmd)
		{
			case admin_ipbanlist:
				try
				{
					final Collection<BanInformation> baniplist = LSConnection.getInstance().getBannedIpList();
					if(baniplist != null && baniplist.size() > 0)
					{
						final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
						final StringBuffer replyMSG = new StringBuffer("<html><body>");

						replyMSG.append("<center>Ban IP List</center><br>");
						replyMSG.append("<center><table width=300><tr><td>");
						replyMSG.append("<center>IP</center></td><td>Banned by</td></tr>");
						for(final BanInformation temp : baniplist)
							replyMSG.append("<tr><td>" + temp.ip + "</td><td>" + temp.admin + "</td><td><button value=\"Unban IP\" action=\"bypass -h admin_unbanip " + temp.ip + "\" width=90 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
						replyMSG.append("</table></center>");
						replyMSG.append("</body></html>");

						adminReply.setHtml(replyMSG.toString());
						activeChar.sendPacket(adminReply);
					}
				}
				catch(final StringIndexOutOfBoundsException e)
				{
					activeChar.sendMessage(new CustomMessage("common.Error", activeChar));
				}
				break;
			case admin_ipban:
			case admin_ipblock:
				if(wordList.length != 2)
				{
					activeChar.sendMessage("Command syntax: //ipban <ip>");
					break;
				}

				if(!validateIP(wordList[1]))
				{
					activeChar.sendMessage("Error: Invalid IP adress: " + wordList[1]);
					break;
				}

				LSConnection.getInstance().sendPacket(new BanIP(wordList[1], activeChar.getName()));
				Log.add("IP " + wordList[1] + ", ban attempt", "gm_ext_actions", activeChar);
				break;
			case admin_ipcharban:
				if(wordList.length != 2)
				{
					activeChar.sendMessage("Command syntax: //ipcharban <char_name>");
					break;
				}

				final L2Player plr = L2ObjectsStorage.getPlayer(wordList[1]);

				if(plr == null)
				{
					activeChar.sendMessage("Character " + wordList[1] + " not found.");
					break;
				}

				final String ip = plr.getIP();
				// Проверку на валидность ip пропускаем, ибо верим серверу

				if(ip.equalsIgnoreCase("<not connected>"))
				{
					activeChar.sendMessage("Character " + wordList[1] + " not found.");
					break;
				}

				LSConnection.getInstance().sendPacket(new BanIP(ip, activeChar.getName()));
				Log.add("IP " + ip + ", ban attempt", "gm_ext_actions", activeChar);
				break;
			case admin_ipchar:
			case admin_charip:
				if(wordList.length != 2)
				{
					activeChar.sendMessage("Command syntax: //charip <char_name>");
					activeChar.sendMessage(" Gets character's IP.");
					break;
				}

				final L2Player pl = L2ObjectsStorage.getPlayer(wordList[1]);

				if(pl == null)
				{
					activeChar.sendMessage("Character " + wordList[1] + " not found.");
					break;
				}

				final String ip_adr = pl.getIP();
				if(ip_adr.equalsIgnoreCase("<not connected>"))
				{
					activeChar.sendMessage("Character " + wordList[1] + " not found.");
					break;
				}

				activeChar.sendMessage("Character's IP: " + ip_adr);
				break;
			case admin_ipunban:
			case admin_ipunblock:
				if(wordList.length != 2)
				{
					activeChar.sendMessage("Command syntax: //ipunban <ip>");
					break;
				}

				if(!validateIP(wordList[1]))
				{
					activeChar.sendMessage("Error: Invalid IP adress: " + wordList[1]);
					break;
				}

				LSConnection.getInstance().sendPacket(new BanIP(wordList[1], activeChar.getName()));
				Log.add("IP " + wordList[1] + ", ban attempt", "gm_ext_actions", activeChar);
				break;
		}
		return true;
	}

	public boolean validateIP(String IP)
	{
		if(!Util.isMatchingRegexp(IP, "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}"))
			return false;

		// Split by dot
		IP = IP.replace(".", ",");
		final String[] IP_octets = IP.split(",");

		for(final String element : IP_octets)
			if(Integer.parseInt(element) > 255)
				return false;

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды ipbanlist",
				"Описание команды ipban",
				"Описание команды ipblock",
				"Описание команды ipunban",
				"Описание команды ipunblock",
				"Описание команды ipcharban",
				"Описание команды ipchar",
				"Описание команды charip" };

	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS };
	}
}
