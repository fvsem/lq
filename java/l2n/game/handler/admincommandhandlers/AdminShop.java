package l2n.game.handler.admincommandhandlers;

import l2n.extensions.Stat;
import l2n.game.TradeController;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExBuySellList;

public class AdminShop implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_buy",
			"admin_gmshop",
			"admin_tax" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().UseGMShop)
			return false;

		if(command.startsWith("admin_buy"))
			try
			{
				handleBuyRequest(activeChar, command.substring(10));
			}
			catch(final IndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Please specify buylist.");
			}
		else if(command.equals("admin_gmshop"))
			AdminHelpPage.showHelpPage(activeChar, "gmshops.htm");
		else if(command.equals("admin_tax"))
			activeChar.sendMessage("TaxSum: " + Stat.getTaxSum());

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleBuyRequest(final L2Player activeChar, final String command)
	{
		int val = -1;

		try
		{
			val = Integer.parseInt(command);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		final NpcTradeList list = TradeController.getInstance().getBuyList(val);
		if(list != null)
		{
			final ExBuySellList bl = new ExBuySellList(list, activeChar, 0);
			activeChar.sendPacket(bl);
		}

		activeChar.sendActionFailed();
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды buy",
				"Описание команды gmshop",
				"Описание команды tax" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5 };
	}
}
