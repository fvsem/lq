package l2n.game.handler.admincommandhandlers;

import l2n.commons.list.primitive.IntArrayList;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SkillCoolTime;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.util.Log;
import l2n.util.Util;

import java.util.Collection;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class AdminSkill implements IAdminCommandHandler
{
	private final static String[] _adminCommands = {
			"admin_show_skills",
			"admin_remove_skills",
			"admin_skill_list",
			"admin_skill_index",
			"admin_add_skill",
			"admin_remove_skill",
			"admin_get_skills",
			"admin_reset_skills",
			"admin_give_all_skills",
			"admin_reset_cool_time",
			"admin_show_skill",
			"admin_show_skilllist",
			"admin_skill_info",
			"admin_show_effects",
			"admin_remove_effect" };

	private static L2Skill[] adminSkills;

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Skills)
			return false;

		if(command.equals("admin_show_skills"))
			showSkillsPage(activeChar);
		else if(command.equals("admin_remove_skills"))
			removeSkillsPage(activeChar);
		else if(command.startsWith("admin_skill_list"))
			AdminHelpPage.showHelpPage(activeChar, "skills.htm");
		else if(command.startsWith("admin_skill_index"))
			try
			{
				final String val = command.substring(18);
				AdminHelpPage.showHelpPage(activeChar, "skills/" + val + ".htm");
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.startsWith("admin_add_skill"))
			try
			{
				final String val = command.substring(15);
				adminAddSkill(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty character name
				activeChar.sendMessage("Error while adding skill.");
			}
		else if(command.startsWith("admin_remove_skill"))
			try
			{
				final String id = command.substring(19);
				final int idval = Integer.parseInt(id);
				adminRemoveSkill(activeChar, idval);
				activeChar.sendPacket(new SkillList(activeChar));
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty character name
				activeChar.sendMessage("Error while removing skill.");
			}
		else if(command.equals("admin_get_skills"))
			adminGetSkills(activeChar);
		else if(command.equals("admin_reset_skills"))
			adminResetSkills(activeChar);
		else if(command.equals("admin_give_all_skills"))
			adminGiveAllSkills(activeChar);
		else if(command.equals("admin_reset_cool_time"))
			adminResetSkillCoolTime(activeChar);
		else if(command.startsWith("admin_show_skill "))
			showSkill(activeChar, command);
		else if(command.equals("admin_show_skilllist"))
			showSkillList(activeChar);
		else if(command.startsWith("admin_skill_info"))
			showSkillInfo(activeChar, command);
		else if(command.equals("admin_show_effects"))
			showEffects(activeChar);
		else if(command.startsWith("admin_remove_effect"))
			removeEffect(activeChar, command);
		return true;
	}

	/**
	 * This function will give all the skills that the gm target can have at its
	 * level to the traget
	 * 
	 * @param activeChar
	 *            : the gm char
	 */
	private void adminGiveAllSkills(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}
		int unLearnable = 0;
		int skillCounter = 0;
		L2SkillLearn[] skills = SkillTreeTable.getInstance().getAvailableSkills(player, player.getClassId());
		while (skills.length > unLearnable)
		{
			unLearnable = 0;
			for(final L2SkillLearn s : skills)
			{
				final L2Skill sk = SkillTable.getInstance().getInfo(s.id, s.skillLevel);
				if(sk == null || !sk.getCanLearn(player.getClassId()))
				{
					unLearnable++;
					continue;
				}
				if(player.getSkillLevel(sk.getId()) == -1)
					skillCounter++;
				player.addSkill(sk, true);
			}
			skills = SkillTreeTable.getInstance().getAvailableSkills(player, player.getClassId());
		}

		player.sendMessage("Admin gave you " + skillCounter + " skills.");
		player.sendPacket(new SkillList(player));
		activeChar.sendMessage("You gave " + skillCounter + " skills to " + player.getName());

		Log.add("gave " + skillCounter + " skills to " + player.getName(), "gm_ext_actions", activeChar);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void removeSkillsPage(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final Collection<L2Skill> skills = player.getAllSkills();

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_show_skills\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<center>Editing character: " + player.getName() + "</center>");
		replyMSG.append("<br><table width=270><tr><td>Lv: " + player.getLevel() + " " + player.getTemplate().className + "</td></tr></table>");
		replyMSG.append("<br><center>Кликните на навык, который требуется удалить:</center>");
		replyMSG.append("<br><table width=270>");
		replyMSG.append("<tr><td width=80>Имя:</td><td width=60>Уровень:</td><td width=40>Id:</td></tr>");
		for(final L2Skill element : skills)
			replyMSG.append("<tr><td width=80><a action=\"bypass -h admin_remove_skill " + element.getId() + "\">" + element.getName() + "</a></td><td width=60>" + element.getLevel() + "</td><td width=40>" + element.getId() + "</td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br><center><table>");
		replyMSG.append("Удалить персонажу навык:");
		replyMSG.append("<tr><td>Id: </td>");
		replyMSG.append("<td><edit var=\"id_to_remove\" width=110></td></tr>");
		replyMSG.append("</table></center>");
		replyMSG.append("<center><button value=\"Удалить навык\" action=\"bypass -h admin_remove_skill $id_to_remove\" width=110 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></center>");
		replyMSG.append("<br><center><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=60 height=15></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	// ok
	private void showSkillsPage(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body><title>Управление навыками</title>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<center>Редактирование персонажа: " + player.getName() + "</center>");
		replyMSG.append("<br><table width=270><tr><td>Ур: " + player.getLevel() + " " + player.getTemplate().className + "</td></tr></table>");
		replyMSG.append("<br><center><table>");
		replyMSG.append("<tr><td><button value=\"Добавить навык\" action=\"bypass -h admin_skill_list\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<tr><td><button value=\"Обзор навыков\" action=\"bypass -h admin_show_skilllist\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Дать навык\" action=\"bypass -h admin_get_skills\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"Удалить навык\" action=\"bypass -h admin_remove_skills\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Сброс навыков\" action=\"bypass -h admin_reset_skills\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"Дать все навыки\" action=\"bypass -h admin_give_all_skills\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Сброс отката\" action=\"bypass -h admin_reset_cool_time\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void adminGetSkills(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		if(player.getName().equals(activeChar.getName()))
			player.sendMessage("There is no point in doing it on your character.");
		else
		{
			final Collection<L2Skill> skills = player.getAllSkills();
			adminSkills = activeChar.getAllSkillsArray();
			for(final L2Skill element : adminSkills)
				activeChar.removeSkill(element, true);
			for(final L2Skill element : skills)
				activeChar.addSkill(element, true);

			activeChar.sendMessage("You now have all the skills of  " + player.getName() + ".");
		}
		showSkillsPage(activeChar);
	}

	private void adminResetSkills(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final L2Skill[] skills = player.getAllSkillsArray();
		int counter = 0;
		for(final L2Skill element : skills)
			if(!element.isCommon() && !SkillTreeTable.getInstance().isSkillPossible(player, element.getId(), element.getLevel()))
			{
				player.removeSkill(element, true);
				counter++;
			}

		player.checkSkills(10);
		player.sendPacket(new SkillList(player));
		player.sendMessage("[GM]" + activeChar.getName() + " has updated your skills.");
		activeChar.sendMessage(counter + " skills removed.");

		showSkillsPage(activeChar);
	}

	private void adminAddSkill(final L2Player activeChar, final String val)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final StringTokenizer st = new StringTokenizer(val);
		if(st.countTokens() != 2)
			showSkillsPage(activeChar);
		else
		{
			final String id = st.nextToken();
			final String level = st.nextToken();
			final int idval = Integer.parseInt(id);
			int levelval = Integer.parseInt(level);

			L2Skill skill = SkillTable.getInstance().getInfo(idval, levelval);
			if(skill != null)
			{
				player.sendMessage("Admin gave you the skill " + skill + ".");
				player.addSkill(skill, true);
				player.sendPacket(new SkillList(player));

				// Admin information
				activeChar.sendMessage("You gave the skill " + skill + " to " + player.getName() + ".");

				Log.add("give the skill " + skill + " to " + player.getName(), "gm_ext_actions", activeChar);
			}
			else
				try
				{
					levelval = SkillTable.getInstance().getBaseLevel(idval);
					skill = SkillTable.getInstance().getInfo(idval, levelval);
					player.sendMessage("Admin gave you the skill " + skill + ".");
					player.addSkill(skill, true);
					player.sendPacket(new SkillList(player));

					// Admin information
					activeChar.sendMessage("You gave the skill " + skill + " to " + player.getName() + ".");

					Log.add("give the skill " + skill + " to " + player.getName(), "gm_ext_actions", activeChar);

				}
				catch(final Exception e)
				{
					activeChar.sendMessage("Error: there is no such skill.");
				}

			showSkillsPage(activeChar); // Back to start
		}
	}

	private void adminRemoveSkill(final L2Player activeChar, final Integer idval)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(idval, player.getSkillLevel(idval));

		if(skill != null)
		{
			player.sendMessage("Admin removed the skill " + skill.getName() + ".");
			player.removeSkill(skill, true);

			// Admin information
			activeChar.sendMessage("You removed the skill " + skill.getName() + " from " + player.getName() + ".");

			Log.add("removed the skill " + skill.getName() + " from " + player.getName(), "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendMessage("Ошибка: нет таких навыков.");

		removeSkillsPage(activeChar); // Back to start
	}

	private void showSkill(final L2Player activeChar, final String command)
	{
		final String[] args = command.split(" ");
		final int skillid = Integer.parseInt(args[1]);
		final int skilllvl = Integer.parseInt(args[2]);
		final L2Skill skill = SkillTable.getInstance().getInfo(skillid, skilllvl);

		if(skill != null)
			activeChar.broadcastPacket(new MagicSkillUse(activeChar, skillid, skilllvl, skill.getHitTime(), skill.getReuseDelay()));
		else
			activeChar.sendPacket(Msg.ActionFail);
	}

	private void showSkillList(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final TreeMap<Integer, L2Skill> skills = player.getAllSkillsSMap();

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body><title>Skill List</title>");
		replyMSG.append("<center>Character: " + player.getName() + "</center>");
		replyMSG.append("<br><table width=270><tr><td>Lv: " + player.getLevel() + " " + player.getTemplate().className + "</td></tr></table>");
		replyMSG.append("<br><center>Кликните на скилл для получения информации:</center>");

		replyMSG.append("<br><table width=270>");
		replyMSG.append("<tr><td width=40>id:</td><td width=40>level:</td><td width=100>name:</td></tr>");
		for(final Entry<Integer, L2Skill> entry : skills.entrySet())
			replyMSG.append("<tr><td width=40><a action=\"bypass -h admin_skill_info " + entry.getKey() + "\">" + entry.getKey() + "</a></td><td width=40>" + entry.getValue().getLevel() + "</td><td width=100>" + entry.getValue().getName() + "</td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br><center><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=60 height=15></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void showSkillInfo(final L2Player activeChar, final String command)
	{
		final String[] args = command.split(" ");
		final int skillid = Integer.parseInt(args[1]);

		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(skillid, player.getSkillLevel(skillid));

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body><title>Skill Info</title>");
		replyMSG.append("<center>Character: " + player.getName() + "Lv: " + player.getLevel() + " " + player.getTemplate().className + "</center>");
		replyMSG.append("<br>");

		replyMSG.append(skill.getHtmlInfo());

		replyMSG.append("<br><center><button value=\"Назад\" action=\"bypass -h admin_show_skilllist\" width=40 height=20></center>");
		replyMSG.append("<br></body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void removeEffect(final L2Player activeChar, final String command)
	{
		final String[] args = command.split(" ");
		final int skillId = Integer.parseInt(args[1]);

		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		player.getEffectList().stopEffect(skillId);
	}

	private void showEffects(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body><title>Навык лист</title>");
		replyMSG.append("<center>Персонаж: " + player.getName() + " Lv: " + player.getLevel() + " " + player.getTemplate().className + "</center>");
		replyMSG.append("<br><center>Effects count: " + player.getEffectList().getAllEffects().length + "</center>");
		replyMSG.append("<br><center><button value=\"");
		replyMSG.append("Refresh");
		replyMSG.append("\" action=\"bypass -h admin_show_effects\" width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center>");
		replyMSG.append("<br>");

		final IntArrayList eskills = new IntArrayList();
		int x = 0;
		for(final L2Effect e : player.getEffectList().getEffects())
		{
			x++;
			if(!eskills.contains(e.getSkill().getId()))
				eskills.add(e.getSkill().getId());
			else
				replyMSG.append("DUPE");
			replyMSG.append("#" + x + " " + e.getSkill().getName() + " " + e.getSkill().getId() + "-" + e.getSkill().getLevel() + ", " + Util.formatTime(e.getTimeLeft() / 1000) + " <a action=\"bypass -h admin_remove_effect " + e.getSkill().getId() + "\"> remove</a><br1>");
		}

		replyMSG.append("<br><center><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=40 height=15></center>");
		replyMSG.append("<br></body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void adminResetSkillCoolTime(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && (activeChar == target || activeChar.getPlayerAccess().Skills))
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		player.resetReuse();
		player.sendPacket(new SkillCoolTime(player.getPlayer()));
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_show_skills",
				"file://com_remove_skills",
				"file://com_skill_list",
				"file://com_skill_index",
				"file://com_add_skill",
				"file://com_remove_skill",
				"file://com_get_skills",
				"file://com_reset_skills",
				"file://com_give_all_skills",
				"file://com_reset_cool_time",
				"file://com_show_skill",
				"file://com_show_skilllist",
				"file://com_skill_info",
				"file://com_show_effects",
				"описание команды remove_effect" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
