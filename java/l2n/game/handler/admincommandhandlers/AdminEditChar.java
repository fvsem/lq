package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.database.utils.mysql;
import l2n.extensions.scripts.Functions;
import l2n.game.Announcements;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.Util;

import java.text.NumberFormat;
import java.util.*;

public class AdminEditChar implements IAdminCommandHandler
{
	private static final class PlayerComparator implements Comparator<L2Player>
	{
		private static final PlayerComparator STATIC_INSTANCE = new PlayerComparator();

		// сортирует от меньшего к большему
		@Override
		public int compare(final L2Player player1, final L2Player player2)
		{
			return (int) (player1.getLastAccess() - player2.getLastAccess());
		}
	}

	private final static String[] _adminCommands = {
			"admin_edit_character",
			"admin_character_actions",
			"admin_current_player",
			"admin_nokarma",
			"admin_setkarma",
			"admin_character_list",
			"admin_show_characters",
			"admin_find_character",
			"admin_save_modifications",
			"admin_rec",
			"admin_settitle",
			"admin_setname",
			"admin_setsex",
			"admin_setcolor",
			"admin_setclass",
			"admin_add_exp_sp_to_character",
			"admin_add_exp_sp",
			"admin_sethero",
			"admin_setnoble",
			"admin_resist",
			"admin_sethero_time",
			"admin_setnoble_time" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar.getPlayerAccess().EditChar)
			if(command.startsWith("admin_settitle"))
				try
				{
					final String val = command.substring(15);
					final L2Object target = activeChar.getTarget();
					L2Player player = null;
					if(target != null && target.isPlayer())
						player = (L2Player) target;
					else
						return false;
					player.setTitle(val);
					player.sendMessage("Ваш титул был изменен  GMом");
					player.sendChanges();

					Log.add("change title for player " + player.getName() + " to " + val, "gm_ext_actions", activeChar);
				}
				catch(final StringIndexOutOfBoundsException e)
				{ // Case of empty character title
					activeChar.sendMessage("Укажите новый Титул.");
				}
			else if(command.startsWith("admin_setname"))
				try
				{
					final String val = command.substring(14);
					final L2Object target = activeChar.getTarget();
					L2Player player;
					if(target != null && target.isPlayer())
						player = (L2Player) target;
					else
						return false;
					if(mysql.simple_get_int("COUNT(*)", "characters", "`char_name` LIKE '" + val + "'") > 0)
					{
						activeChar.sendMessage("Имя уже сушествует.");
						return false;
					}
					Log.add("Character " + player.getName() + " renamed to " + val + " by GM " + activeChar.getName(), "renames");
					Log.add("set name for player " + player.getName() + " to " + val, "gm_ext_actions", activeChar);
					player.setName(val);
					player.sendMessage("Ваше имя изменено GMом");
					player.broadcastUserInfo(true);
				}
				catch(final StringIndexOutOfBoundsException e)
				{ // Case of empty character name
					activeChar.sendMessage("Укажите новое имя.");
				}

		if(!activeChar.getPlayerAccess().EditChar)
			return false;
		else if(command.equals("admin_current_player"))
			showCharacterList(activeChar, null);
		else if(command.startsWith("admin_character_list"))
			try
			{
				final String val = command.substring(21);
				final L2Player target = L2ObjectsStorage.getPlayer(val);
				showCharacterList(activeChar, target);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				// Case of empty character name
			}
		else if(command.startsWith("admin_show_characters"))
			try
			{
				final String val = command.substring(22);
				final int page = Integer.parseInt(val);
				listCharacters(activeChar, page);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				// Case of empty page
			}
		else if(command.startsWith("admin_find_character"))
			try
			{
				final String val = command.substring(21);
				findCharacter(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty character name
				activeChar.sendMessage("Вы не ввели имя персонажа, чтобы найти.");

				listCharacters(activeChar, 0);
			}
		else if(!activeChar.getPlayerAccess().EditChar)
			return false;
		else if(command.equals("admin_edit_character"))
			editCharacter(activeChar);
		else if(command.equals("admin_character_actions"))
			showCharacterActions(activeChar);
		else if(command.equals("admin_nokarma"))
			setTargetKarma(activeChar, 0);
		else if(command.startsWith("admin_setkarma"))
			try
			{
				final String val = command.substring(15);
				final int karma = Integer.parseInt(val);
				setTargetKarma(activeChar, karma);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Please specify new karma value.");
			}
		else if(command.startsWith("admin_save_modifications"))
			try
			{
				final String val = command.substring(24);
				adminModifyCharacter(activeChar, val);

				Log.add("save modifications for player " + val, "gm_ext_actions", activeChar);
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty character name
				activeChar.sendMessage("Error while modifying character.");
				listCharacters(activeChar, 0);
			}
		else if(command.equals("admin_rec"))
		{
			final L2Object target = activeChar.getTarget();
			L2Player player = null;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
				return false;
			player.setRecomHave(player.getRecomHave() + 1);
			player.sendMessage("Вас рекомандовал Гейм Мастер");
			player.broadcastUserInfo(true);

			Log.add("recommend player " + player.getName() + " + 1", "gm_ext_actions", activeChar);
		}
		else if(command.startsWith("admin_rec"))
			try
			{
				final String val = command.substring(10);
				final int recVal = Integer.parseInt(val);
				final L2Object target = activeChar.getTarget();
				L2Player player = null;
				if(target != null && target.isPlayer())
					player = (L2Player) target;
				else
					return false;
				player.setRecomHave(player.getRecomHave() + recVal);
				player.sendMessage("Вы получаете рекомендацию от Гейм Мастера");
				player.broadcastUserInfo(true);

				Log.add("recommend player " + player.getName() + " + " + recVal, "gm_ext_actions", activeChar);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Command format is //rec <number>");
			}
		else if(command.startsWith("admin_sethero_time"))
		{
			int time = 0;
			final L2Object target = activeChar.getTarget();
			L2Player player;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
			{
				activeChar.sendMessage("Command format is //sethero <number> <m or h or d>.");
				activeChar.sendMessage("Example //sethero 11 d. This will give hero status for 11 days.");
				return false;
			}

			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				time = Integer.parseInt(st.nextToken());
				final String timeFormat = st.nextToken();
				if(timeFormat.equalsIgnoreCase("h")) // Если час, то умножаем на количество минут в часе
					time *= 60;
				else if(timeFormat.equalsIgnoreCase("d")) // Если дни, то умножаем на количество минут в сутках
					time *= 1440;

				if(player.isHero())
				{
					player.setHero(false, 2);
					player.unsetVar("hero_time");
					player.broadcastPacket(new SocialAction(player.getObjectId(), 13));
				}
				else
				{
					player.setHero(true, 1);

					player.broadcastPacket(new SocialAction(player.getObjectId(), 3));
					Announcements.announceToAll(player.getName() + " Стал героем.");

					player.setVar("hero_time", "1", time);

					player.sendMessage("Гейм Масер изменили свой статус на героя.");
					player.broadcastUserInfo(true);

					final Calendar cl = Calendar.getInstance();
					cl.setTimeInMillis(System.currentTimeMillis() + time * 60000);

					Log.add("add hero status to player " + player.getName() + ", time: " + Util.formatTime(time * 60) + ", end data: " + cl.getTime(), "gm_ext_actions", activeChar);
				}
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Command format is //sethero <number> <m or h or d>.");
				activeChar.sendMessage("Example //sethero 11 d. This will give hero status for 11 days.");
				return false;
				// e.printStackTrace();
			}
		}
		else if(command.startsWith("admin_sethero"))
		{
			// Статус меняется только на текущую логон сессию
			final String[] wordList = command.split(" ");
			final L2Object target = activeChar.getTarget();
			L2Player player;
			if(wordList.length > 1 && wordList[1] != null)
			{
				player = L2ObjectsStorage.getPlayer(wordList[1]);
				if(player == null)
				{
					activeChar.sendMessage("Персонажа " + wordList[1] + " Нет в Игре.");
					return false;
				}
			}
			else if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
			{
				activeChar.sendMessage("Вы должны Выделить персонажа");
				return false;
			}

			if(player.isHero())
			{
				player.setHero(false, 2);
				player.broadcastPacket(new SocialAction(player.getObjectId(), 13));
			}
			else
			{
				player.setHero(true, 1);

				player.broadcastPacket(new SocialAction(player.getObjectId(), 3));
				Announcements.announceToAll(player.getName() + " Становиться Героем.");
			}

			player.sendMessage("Гейм Мастер изменили свой статус на героя.");
			player.broadcastUserInfo(true);

			Log.add("add hero status to player " + player.getName(), "gm_ext_actions", activeChar);
		}
		else if(command.startsWith("admin_setnoble_time"))
		{
			int time = 0;
			final L2Object target = activeChar.getTarget();
			L2Player player;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
			{
				activeChar.sendMessage("Command format is //setnoble_time <number> <m or h or d>.");
				activeChar.sendMessage("Example //setnoble_time 11 d. This will give noble status for 11 days.");
				return false;
			}

			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				time = Integer.parseInt(st.nextToken());
				final String timeFormat = st.nextToken();
				if(timeFormat.equalsIgnoreCase("h")) // Если час, то умножаем на количество минут в часе
					time *= 60;
				else if(timeFormat.equalsIgnoreCase("d")) // Если дни, то умножаем на количество минут в сутках
					time *= 1440;

				if(player.isNoble())
				{
					Olympiad.removeNoble(player);
					player.setNoble(false, 2);
					player.broadcastPacket(new SocialAction(player.getObjectId(), 13));
					player.unsetVar("noble_time");
				}
				else
				{
					Olympiad.addNoble(player);
					player.setNoble(true, 1);

					player.broadcastPacket(new SocialAction(player.getObjectId(), 3));
					Announcements.announceToAll(player.getName() + " has become a noble.");

					player.setVar("noble_time", "1", time);

					player.sendMessage("Гейм Мастер выдал вам Нублес.");
					player.broadcastUserInfo(true);

					final Calendar cl = Calendar.getInstance();
					cl.setTimeInMillis(System.currentTimeMillis() + time * 60000);

					Log.add("add noble status to player " + player.getName() + ", time: " + Util.formatTime(time * 60) + ", end data: " + cl.getTime(), "gm_ext_actions", activeChar);
				}
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Command format is //setnoble_time <number> <m or h or d>.");
				activeChar.sendMessage("Example //setnoble_time 11 d. This will give hero status for 11 days.");
				return false;
				// e.printStackTrace();
			}
		}
		else if(command.startsWith("admin_setnoble"))
		{
			// Статус сохраняется в базе
			final String[] wordList = command.split(" ");
			final L2Object target = activeChar.getTarget();
			L2Player player;
			if(wordList.length > 1 && wordList[1] != null)
			{
				player = L2ObjectsStorage.getPlayer(wordList[1]);
				if(player == null)
				{
					activeChar.sendMessage("Персонаж " + wordList[1] + " Не в Игре.");
					return false;
				}
			}
			else if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
			{
				activeChar.sendMessage("Вы Должны указать имя или выбрать игрока.");
				return false;
			}

			if(player.isNoble())
			{
				Olympiad.removeNoble(player);
				player.setNoble(false, 2);
				player.broadcastPacket(new SocialAction(player.getObjectId(), 13));
			}
			else
			{
				Olympiad.addNoble(player);
				player.setNoble(true, 1);
				player.broadcastPacket(new SocialAction(player.getObjectId(), 3));
			}

			player.sendMessage("Admin changed your noble status.");
			player.broadcastUserInfo(true);

			Log.add("add noble status to player " + player.getName(), "gm_ext_actions", activeChar);
		}
		else if(command.startsWith("admin_setsex"))
		{
			final L2Object target = activeChar.getTarget();
			L2Player player = null;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
				return false;
			player.changeSex();
			player.sendMessage("Your gender has been changed by a GM");
			player.broadcastUserInfo(true);

			Log.add("change gender to player " + player.getName(), "gm_ext_actions", activeChar);
		}
		else if(command.startsWith("admin_setcolor"))
			try
			{
				final String val = command.substring(15);
				final L2Object target = activeChar.getTarget();
				L2Player player = null;
				if(target != null && target.isPlayer())
					player = (L2Player) target;
				else
					return false;
				player.setNameColor(Integer.decode("0x" + val));
				player.sendMessage("Your name color has been changed by a GM");
				player.broadcastUserInfo(true);

				Log.add("change name color for player " + player.getName(), "gm_ext_actions", activeChar);
			}
			catch(final Exception e)
			{ // Case of empty color
				activeChar.sendMessage("You need to specify the new color.");
			}
		else if(command.startsWith("admin_setclass"))
			try
			{
				final String[] params = command.split(" ");
				if(params.length > 1)
				{
					final String val = params[1];
					final L2Object target = activeChar.getTarget();
					L2Player player = null;
					if(target != null && target.isPlayer())
						player = (L2Player) target;
					else
						return false;
					player.setClassId(Short.parseShort(val), true);
					player.sendMessage("Your class has been changed by a GM");
					player.broadcastUserInfo(true);

					Log.add("change classID for player " + player.getName(), "gm_ext_actions", activeChar);
				}
				else
				{
					String content = Files.read("data/html/admin/setclass.htm", activeChar);
					if(content == null)
						content = "Not found filename: data/html/admin/setclass.htm";
					final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
					adminReply.setHtml(content);
					activeChar.sendPacket(adminReply);
				}
			}
			catch(final Exception e)
			{
				System.out.println(e.getMessage());
				activeChar.sendMessage("Error!");
			}
		else if(command.startsWith("admin_add_exp_sp_to_character"))
			addExpSp(activeChar);
		else if(command.startsWith("admin_add_exp_sp"))
			try
			{
				final String val = command.substring(16);
				adminAddExpSp(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{ // Case of empty character name
				activeChar.sendMessage("Error while adding Exp-Sp.");
			}
		else if(command.equals("admin_resist"))
		{
			final L2Object target = activeChar.getTarget();
			L2Player player = null;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
				player = activeChar;
			resists(activeChar, player);
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void listCharacters(final L2Player activeChar, int page)
	{
		final Collection<L2Player> allPlayers = L2ObjectsStorage.getAllPlayers();
		final L2Player[] players = allPlayers.toArray(new L2Player[allPlayers.size()]);
		Arrays.sort(players, PlayerComparator.STATIC_INSTANCE);

		final int MaxCharactersPerPage = 20;
		int MaxPages = players.length / MaxCharactersPerPage;

		if(players.length > MaxCharactersPerPage * MaxPages)
			MaxPages++;

		// Check if number of users changed
		if(page > MaxPages)
			page = MaxPages;

		final int CharactersStart = MaxCharactersPerPage * page;
		int CharactersEnd = players.length;
		if(CharactersEnd - CharactersStart > MaxCharactersPerPage)
			CharactersEnd = CharactersStart + MaxCharactersPerPage;

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td width=270>Вы можете найти персонажа, написав имя и</td></tr>");
		replyMSG.append("<tr><td width=270>нажать кнопку ниже Найти .<br></td></tr>");
		replyMSG.append("<tr><td width=270>Примечание: Имена должны быть написаны с учетом регистра.</td></tr>");
		replyMSG.append("</table><br>");
		replyMSG.append("<center><table><tr><td>");
		replyMSG.append("<edit var=\"character_name\" width=80></td><td><button value=\"Найти\" action=\"bypass -h admin_find_character $character_name\" width=50 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
		replyMSG.append("</td></tr></table></center><br><br>");

		for(int x = 0; x < MaxPages; x++)
		{
			final int pagenr = x + 1;
			replyMSG.append("<center><a action=\"bypass -h admin_show_characters " + x + "\">Page " + pagenr + "</a></center>");
		}
		replyMSG.append("<br>");

		// List Players in a Table
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td width=80>Имя:</td><td width=110>Класс:</td><td width=40>Уровень:</td></tr>");
		for(int i = CharactersStart; i < CharactersEnd; i++)
		{
			if(players[i].isInOfflineMode() && !Config.SHOW_OFFLINE_PLAYERS)
				continue;

			String status = "В игре";

			if(players[i].isInOfflineMode())
				status = "Торг";
			replyMSG.append("<tr><td width=80>" + "<a action=\"bypass -h admin_character_list " + players[i].getName() + "\">" + players[i].getName() + " [" + status + "] </a></td><td width=110>" + players[i].getTemplate().className + "</td><td width=40>" + players[i].getLevel() + "</td></tr>");
		}
		replyMSG.append("</table>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	public static void showCharacterList(final L2Player activeChar, L2Player player)
	{
		if(player == null)
		{
			final L2Object target = activeChar.getTarget();
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
				return;
		}
		else
			activeChar.setTarget(player);

		String clan = "No Clan";
		if(player.getClan() != null)
			clan = player.getClan().getName() + "/" + player.getClan().getLevel();

		String content = Files.read("data/html/admin/charinfo.htm", activeChar);
		// Character Player Info
		content = content.replace("%name%", player.getName());
		content = content.replace("%account%", player.getAccountName());
		content = content.replace("%ip%", player.getIP());
		content = content.replace("%objectId%", "" + player.getObjectId());
		content = content.replace("%level%", "" + player.getLevel());
		content = content.replace("%clan%", clan);
		content = content.replace("%xp%", "" + player.getExp());
		content = content.replace("%sp%", "" + player.getSp());
		content = content.replace("%class%", player.getTemplate().className);
		content = content.replace("%vpoints%", "" + (int) player.getVitalityPoints());
		content = content.replace("%vlevel%", "" + player.getVitalityLevel());

		// Character ClassID & Coordinates
		content = content.replace("%class_name%", player.getClassId().getNormalName());
		content = content.replace("%class_id%", "" + player.getClassId().getId());
		content = content.replace("%xyz%", player.getLoc().toXYZString());
		content = content.replace("%refId%", "" + player.getReflectionId());
		content = content.replace("%refName%", player.getReflection().getName().length() == 0 ? "NONAME" : player.getReflection().getName());
		content = content.replace("%storedId%", "" + player.getStoredId());

		final NumberFormat df = NumberFormat.getNumberInstance(Locale.ENGLISH);
		df.setMaximumFractionDigits(4);
		df.setMinimumFractionDigits(1);

		// Character Stats
		content = content.replace("%currenthp%", "" + (int) player.getCurrentHp());
		content = content.replace("%currentmp%", "" + (int) player.getCurrentMp());
		content = content.replace("%currentcp%", "" + (int) player.getCurrentCp());
		content = content.replace("%currentload%", "" + player.getCurrentLoad());
		content = content.replace("%maxhp%", "" + player.getMaxHp());
		content = content.replace("%maxmp%", "" + player.getMaxMp());
		content = content.replace("%maxcp%", "" + player.getMaxCp());
		content = content.replace("%maxload%", "" + player.getMaxLoad());

		content = content.replace("%karma%", "" + player.getKarma());
		content = content.replace("%pkkills%", "" + player.getPkKills());
		content = content.replace("%pvpkills%", "" + player.getPvpKills());
		content = content.replace("%fame%", "" + player.getFame());

		content = content.replace("%patk%", "" + player.getPAtk(null));
		content = content.replace("%matk%", "" + player.getMAtk(null, null));
		content = content.replace("%pdef%", "" + player.getPDef(null));
		content = content.replace("%mdef%", "" + player.getMDef(null, null));
		content = content.replace("%accuracy%", "" + player.getAccuracy());
		content = content.replace("%evasion%", "" + player.getEvasionRate(null));
		content = content.replace("%critical%", "" + player.getCriticalHit(null, null));
		content = content.replace("%mcritical%", df.format(player.getCriticalMagic(null, null)) + "%");
		content = content.replace("%runspeed%", "" + player.getRunSpeed());
		content = content.replace("%patkspd%", "" + player.getPAtkSpd());
		content = content.replace("%matkspd%", "" + player.getMAtkSpd());

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setHtml(content);
		activeChar.sendPacket(adminReply);
	}

	public static void resists(final L2Player player, final L2Player target)
	{
		if(player == null || target == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><body><title>Resists Info</title>");
		dialog.append("<BR><center><button value=\"Обновить\" action=\"bypass -h admin_resist\" width=65 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></center><BR>");

		// Различные резисты
		dialog.append("<br>");
		dialog.append(getResist(target));

		// Бонусы элементальной атаки
		dialog.append("<br>");
		dialog.append(getAttackElement(target));

		// Бонусы атаки
		dialog.append("<br>");
		dialog.append(getAttackBonus(target));

		dialog.append("</body></html>");
		Functions.show(dialog.toString(), player);
	}

	/**
	 * @param cha
	 * @return Бонусы элементальной атаки
	 */
	public static String getAttackElement(final L2Player cha)
	{
		if(cha == null)
			return "";

		final StringBuilder dialog = new StringBuilder();
		dialog.append("<center><table width=\"270\" bgcolor=\"666666\"><tr><td><center><font color=\"LEVEL\">Element Attack</font></center></td></tr></table></center>");
		dialog.append("<table width=\"70%\">");
		final int FIRE_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_FIRE, 0, null, null);
		if(FIRE_ATTACK != 0)
			dialog.append("<tr><td>Fire attack</td><td>").append(FIRE_ATTACK).append("</td></tr>");

		final int WIND_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_WIND, 0, null, null);
		if(WIND_ATTACK != 0)
			dialog.append("<tr><td>Wind attack</td><td>").append(WIND_ATTACK).append("</td></tr>");

		final int WATER_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_WATER, 0, null, null);
		if(WATER_ATTACK != 0)
			dialog.append("<tr><td>Water attack</td><td>").append(WATER_ATTACK).append("</td></tr>");

		final int EARTH_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_EARTH, 0, null, null);
		if(EARTH_ATTACK != 0)
			dialog.append("<tr><td>Earth attack</td><td>").append(EARTH_ATTACK).append("</td></tr>");

		final int SACRED_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_SACRED, 0, null, null);
		if(SACRED_ATTACK != 0)
			dialog.append("<tr><td>Light attack</td><td>").append(SACRED_ATTACK).append("</td></tr>");

		final int UNHOLY_ATTACK = (int) cha.calcStat(Stats.ATTACK_ELEMENT_UNHOLY, 0, null, null);
		if(UNHOLY_ATTACK != 0)
			dialog.append("<tr><td>Darkness attack</td><td>").append(UNHOLY_ATTACK).append("</td></tr>");

		if(FIRE_ATTACK == 0 && WIND_ATTACK == 0 && WATER_ATTACK == 0 && EARTH_ATTACK == 0 && SACRED_ATTACK == 0 && UNHOLY_ATTACK == 0)
			dialog.append("</table>No element attack");
		else
			dialog.append("</table>");
		return dialog.toString();
	}

	/**
	 * @param cha
	 * @return Бонусы атаки
	 */
	public static String getAttackBonus(final L2Player cha)
	{
		if(cha == null)
			return "";

		final StringBuilder dialog = new StringBuilder();
		dialog.append("<center><table width=\"270\" bgcolor=\"666666\"><tr><td><center><font color=\"LEVEL\">Attack bonus</font></center></td></tr></table></center>");
		dialog.append("<table width=\"70%\">");

		final int BLEED_POWER = (int) cha.calcStat(Stats.BLEED_POWER, 0, null, null);
		if(BLEED_POWER != 0)
			dialog.append("<tr><td>Bleed power</td><td>").append(BLEED_POWER).append("</td></tr>");

		final int POISON_POWER = (int) cha.calcStat(Stats.POISON_POWER, 0, null, null);
		if(POISON_POWER != 0)
			dialog.append("<tr><td>Poison power</td><td>").append(POISON_POWER).append("</td></tr>");

		final int STUN_POWER = (int) cha.calcStat(Stats.STUN_POWER, 0, null, null);
		if(STUN_POWER != 0)
			dialog.append("<tr><td>Stun power</td><td>").append(STUN_POWER).append("</td></tr>");

		final int ROOT_POWER = (int) cha.calcStat(Stats.ROOT_POWER, 0, null, null);
		if(ROOT_POWER != 0)
			dialog.append("<tr><td>Root power</td><td>").append(ROOT_POWER).append("</td></tr>");

		final int MENTAL_POWER = (int) cha.calcStat(Stats.MENTAL_POWER, 0, null, null);
		if(MENTAL_POWER != 0)
			dialog.append("<tr><td>Mental power</td><td>").append(MENTAL_POWER).append("</td></tr>");

		final int SLEEP_POWER = (int) cha.calcStat(Stats.SLEEP_POWER, 0, null, null);
		if(SLEEP_POWER != 0)
			dialog.append("<tr><td>Sleep power</td><td>").append(SLEEP_POWER).append("</td></tr>");

		final int PARALYZE_POWER = (int) cha.calcStat(Stats.PARALYZE_POWER, 0, null, null);
		if(PARALYZE_POWER != 0)
			dialog.append("<tr><td>Paralyze power</td><td>").append(PARALYZE_POWER).append("</td></tr>");

		final int CANCEL_POWER = (int) cha.calcStat(Stats.CANCEL_POWER, 0, null, null);
		if(CANCEL_POWER != 0)
			dialog.append("<tr><td>Cancel power</td><td>").append(CANCEL_POWER).append("</td></tr>");

		final int DEBUFF_POWER = (int) cha.calcStat(Stats.DEBUFF_POWER, 0, null, null);
		if(DEBUFF_POWER != 0)
			dialog.append("<tr><td>Debuff power</td><td>").append(DEBUFF_POWER).append("</td></tr>");

		if(BLEED_POWER == 0 && POISON_POWER == 0 && STUN_POWER == 0 && ROOT_POWER == 0 && MENTAL_POWER == 0 && SLEEP_POWER == 0 && PARALYZE_POWER == 0 && CANCEL_POWER == 0 && DEBUFF_POWER == 0)
			dialog.append("</table>No attack bonus");
		else
			dialog.append("</table>");

		return dialog.toString();
	}

	/**
	 * @param cha
	 * @return Различные резисты
	 */
	public static String getResist(final L2Player cha)
	{
		if(cha == null)
			return "";

		final StringBuilder dialog = new StringBuilder();
		dialog.append("<center><table width=\"270\" bgcolor=\"666666\"><tr><td><center><font color=\"LEVEL\">Resists</font></center></td></tr></table></center>");
		dialog.append("<table width=\"70%\">");
		final int FIRE_RECEPTIVE = (int) cha.calcStat(Stats.FIRE_RECEPTIVE, 0, null, null);
		if(FIRE_RECEPTIVE != 0)
			dialog.append("<tr><td>Fire</td><td>").append(FIRE_RECEPTIVE).append("</td></tr>");

		final int WIND_RECEPTIVE = (int) cha.calcStat(Stats.WIND_RECEPTIVE, 0, null, null);
		if(WIND_RECEPTIVE != 0)
			dialog.append("<tr><td>Wind</td><td>").append(WIND_RECEPTIVE).append("</td></tr>");

		final int WATER_RECEPTIVE = (int) cha.calcStat(Stats.WATER_RECEPTIVE, 0, null, null);
		if(WATER_RECEPTIVE != 0)
			dialog.append("<tr><td>Water</td><td>").append(WATER_RECEPTIVE).append("</td></tr>");

		final int EARTH_RECEPTIVE = (int) cha.calcStat(Stats.EARTH_RECEPTIVE, 0, null, null);
		if(EARTH_RECEPTIVE != 0)
			dialog.append("<tr><td>Earth</td><td>").append(EARTH_RECEPTIVE).append("</td></tr>");

		final int SACRED_RECEPTIVE = (int) cha.calcStat(Stats.SACRED_RECEPTIVE, 0, null, null);
		if(SACRED_RECEPTIVE != 0)
			dialog.append("<tr><td>Light</td><td>").append(SACRED_RECEPTIVE).append("</td></tr>");

		final int UNHOLY_RECEPTIVE = (int) cha.calcStat(Stats.UNHOLY_RECEPTIVE, 0, null, null);
		if(UNHOLY_RECEPTIVE != 0)
			dialog.append("<tr><td>Darkness</td><td>").append(UNHOLY_RECEPTIVE).append("</td></tr>");

		final int BLEED_RECEPTIVE = (int) cha.calcStat(Stats.BLEED_RECEPTIVE, 0, null, null);
		if(BLEED_RECEPTIVE != 0)
			dialog.append("<tr><td>Bleed</td><td>").append(BLEED_RECEPTIVE).append("</td></tr>");

		final int POISON_RECEPTIVE = (int) cha.calcStat(Stats.POISON_RECEPTIVE, 0, null, null);
		if(POISON_RECEPTIVE != 0)
			dialog.append("<tr><td>Poison</td><td>").append(POISON_RECEPTIVE).append("</td></tr>");

		final int DEATH_RECEPTIVE = (int) cha.calcStat(Stats.DEATH_RECEPTIVE, 0, null, null);
		if(DEATH_RECEPTIVE != 0)
			dialog.append("<tr><td>Death</td><td>").append(DEATH_RECEPTIVE).append("</td></tr>");

		final int STUN_RECEPTIVE = (int) cha.calcStat(Stats.STUN_RECEPTIVE, 0, null, null);
		if(STUN_RECEPTIVE != 0)
			dialog.append("<tr><td>Stun</td><td>").append(STUN_RECEPTIVE).append("</td></tr>");

		final int ROOT_RECEPTIVE = (int) cha.calcStat(Stats.ROOT_RECEPTIVE, 0, null, null);
		if(ROOT_RECEPTIVE != 0)
			dialog.append("<tr><td>Root</td><td>").append(ROOT_RECEPTIVE).append("</td></tr>");

		final int SLEEP_RECEPTIVE = (int) cha.calcStat(Stats.SLEEP_RECEPTIVE, 0, null, null);
		if(SLEEP_RECEPTIVE != 0)
			dialog.append("<tr><td>Sleep</td><td>").append(SLEEP_RECEPTIVE).append("</td></tr>");

		final int PARALYZE_RECEPTIVE = (int) cha.calcStat(Stats.PARALYZE_RECEPTIVE, 0, null, null);
		if(PARALYZE_RECEPTIVE != 0)
			dialog.append("<tr><td>Paralyze</td><td>").append(PARALYZE_RECEPTIVE).append("</td></tr>");

		final int MENTAL_RECEPTIVE = (int) cha.calcStat(Stats.MENTAL_RECEPTIVE, 0, null, null);
		if(MENTAL_RECEPTIVE != 0)
			dialog.append("<tr><td>Mental</td><td>").append(MENTAL_RECEPTIVE).append("</td></tr>");

		final int DEBUFF_RECEPTIVE = (int) cha.calcStat(Stats.DEBUFF_RECEPTIVE, 0, null, null);
		if(DEBUFF_RECEPTIVE != 0)
			dialog.append("<tr><td>Debuff</td><td>").append(DEBUFF_RECEPTIVE).append("</td></tr>");

		final int CANCEL_RECEPTIVE = (int) cha.calcStat(Stats.CANCEL_RECEPTIVE, 0, null, null);
		if(CANCEL_RECEPTIVE != 0)
			dialog.append("<tr><td>Cancel</td><td>").append(CANCEL_RECEPTIVE).append("</td></tr>");

		final int SWORD_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.SWORD_WPN_RECEPTIVE, 100, null, null);
		if(SWORD_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Sword</td><td>").append(SWORD_WPN_RECEPTIVE).append("%</td></tr>");

		final int DUAL_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.DUAL_WPN_RECEPTIVE, 100, null, null);
		if(DUAL_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Dual Sword</td><td>").append(DUAL_WPN_RECEPTIVE).append("%</td></tr>");

		final int BLUNT_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.BLUNT_WPN_RECEPTIVE, 100, null, null);
		if(BLUNT_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Blunt</td><td>").append(BLUNT_WPN_RECEPTIVE).append("%</td></tr>");

		final int DAGGER_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.DAGGER_WPN_RECEPTIVE, 100, null, null);
		if(DAGGER_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Dagger/rapier</td><td>").append(DAGGER_WPN_RECEPTIVE).append("%</td></tr>");

		final int BOW_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.BOW_WPN_RECEPTIVE, 100, null, null);
		if(BOW_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Bow</td><td>").append(BOW_WPN_RECEPTIVE).append("%</td></tr>");

		final int CROSSBOW_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.CROSSBOW_WPN_RECEPTIVE, 100, null, null);
		if(CROSSBOW_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Crossbow</td><td>").append(CROSSBOW_WPN_RECEPTIVE).append("%</td></tr>");

		final int POLE_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.POLE_WPN_RECEPTIVE, 100, null, null);
		if(POLE_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Polearm</td><td>").append(POLE_WPN_RECEPTIVE).append("%</td></tr>");

		final int FIST_WPN_RECEPTIVE = 100 - (int) cha.calcStat(Stats.FIST_WPN_RECEPTIVE, 100, null, null);
		if(FIST_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Fist weapons</td><td>").append(FIST_WPN_RECEPTIVE).append("%</td></tr>");

		final int CRIT_CHANCE_RECEPTIVE = (int) (100 - cha.calcStat(Stats.CRIT_CHANCE_RECEPTIVE, 100, null, null));
		if(CRIT_CHANCE_RECEPTIVE != 0)
			dialog.append("<tr><td>Resist Crit</td><td>").append(CRIT_CHANCE_RECEPTIVE).append("%</td></tr>");

		final int CRIT_DAMAGE_RECEPTIVE = 100 - (int) cha.calcStat(Stats.CRIT_DAMAGE_RECEPTIVE, 100, null, null);
		if(CRIT_DAMAGE_RECEPTIVE != 0)
			dialog.append("<tr><td>Crit get damage</td><td>").append(CRIT_DAMAGE_RECEPTIVE).append("%</td></tr>");

		if(FIRE_RECEPTIVE == 0 && WIND_RECEPTIVE == 0 && WATER_RECEPTIVE == 0 && EARTH_RECEPTIVE == 0 && UNHOLY_RECEPTIVE == 0 && SACRED_RECEPTIVE // primary elements
					== 0 && BLEED_RECEPTIVE == 0 && DEATH_RECEPTIVE == 0 && STUN_RECEPTIVE // phys debuff
					== 0 && POISON_RECEPTIVE == 0 && ROOT_RECEPTIVE == 0 && SLEEP_RECEPTIVE == 0 && PARALYZE_RECEPTIVE == 0 && MENTAL_RECEPTIVE == 0 && DEBUFF_RECEPTIVE == 0 && CANCEL_RECEPTIVE // mag debuff
					== 0 && SWORD_WPN_RECEPTIVE == 0 && DUAL_WPN_RECEPTIVE == 0 && BLUNT_WPN_RECEPTIVE == 0 && DAGGER_WPN_RECEPTIVE == 0 && BOW_WPN_RECEPTIVE == 0 && CROSSBOW_WPN_RECEPTIVE == 0 && POLE_WPN_RECEPTIVE == 0 && FIST_WPN_RECEPTIVE // weapons
					== 0 && CRIT_CHANCE_RECEPTIVE == 0 && CRIT_DAMAGE_RECEPTIVE == 0 // other
		)
			dialog.append("</table>No resists");
		else
			dialog.append("</table>");

		return dialog.toString();
	}

	private void setTargetKarma(final L2Player activeChar, final int newKarma)
	{
		final L2Object target = activeChar.getTarget();
		if(target == null)
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		L2Player player;
		if(target.isPlayer())
			player = (L2Player) target;
		else
			return;

		if(newKarma >= 0)
		{
			final int oldKarma = player.getKarma();
			player.setKarma(newKarma);

			player.sendMessage("Админ изменил вашу карму от " + oldKarma + " к " + newKarma + ".");
			activeChar.sendMessage("Успешно изменена карма " + player.getName() + " от (" + oldKarma + ") к (" + newKarma + ").");

			Log.add("Изменена карма " + player.getName() + " от (" + oldKarma + ") к (" + newKarma + ")", "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendMessage("Вы должны ввести значение для карма больше или равно 0.");
	}

	private void adminModifyCharacter(final L2Player activeChar, final String modifications)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target.isPlayer())
			player = (L2Player) target;
		else
			return;

		final StringTokenizer st = new StringTokenizer(modifications);
		if(st.countTokens() != 6)
			editCharacter(player);
		else
		{
			final String hp = st.nextToken();
			final String mp = st.nextToken();
			// String load = st.nextToken();
			final String karma = st.nextToken();
			final String pvpflag = st.nextToken();
			final String pvpkills = st.nextToken();
			final String classid = st.nextToken();
			final int hpval = Integer.parseInt(hp);
			final int mpval = Integer.parseInt(mp);
			// int loadval = Integer.parseInt(load);
			final int karmaval = Integer.parseInt(karma);
			final int pvpflagval = Integer.parseInt(pvpflag);
			final int pvpkillsval = Integer.parseInt(pvpkills);
			final short classidval = Short.parseShort(classid);

			// Common character information
			player.sendMessage("Admin has changed your stats. Hp: " + hpval + " Mp: " + mpval + " Karma: " + karmaval + " Pvp: " + pvpflagval + " / " + pvpkillsval + " ClassId: " + classidval);

			player.setCurrentHp(hpval, false);
			player.setCurrentMp(mpval);
			player.setKarma(karmaval);
			player.setPvpFlag(pvpflagval);
			player.setPvpKills(pvpkillsval);
			player.setClassId(classidval, true);

			player.sendChanges();

			// Admin information
			activeChar.sendMessage("Changed stats of " + player.getName() + ".  Hp: " + hpval + " Mp: " + mpval + " Karma: " + karmaval + " Pvp: " + pvpflagval + " / " + pvpkillsval + " ClassId: " + classidval);

			showCharacterList(activeChar, null); // Back to start
			player.broadcastUserInfo(true);
			player.decayMe();
			player.spawnMe(activeChar.getLoc());
		}
	}

	private void editCharacter(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer())
			player = (L2Player) target;
		else
			return;

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<center>Редактирование персонажа: " + player.getName() + "</center><br>");
		replyMSG.append("<table width=250>");
		replyMSG.append("<tr><td width=40></td><td width=70>Curent:</td><td width=70>Max:</td><td width=70></td></tr>");
		replyMSG.append("<tr><td width=40>HP:</td><td width=70>" + player.getCurrentHp() + "</td><td width=70>" + player.getMaxHp() + "</td><td width=70>Karma: " + player.getKarma() + "</td></tr>");
		replyMSG.append("<tr><td width=40>MP:</td><td width=70>" + player.getCurrentMp() + "</td><td width=70>" + player.getMaxMp() + "</td><td width=70>Pvp Kills: " + player.getPvpKills() + "</td></tr>");
		replyMSG.append("<tr><td width=40>Load:</td><td width=70>" + player.getCurrentLoad() + "</td><td width=70>" + player.getMaxLoad() + "</td><td width=70>Pvp Flag: " + player.getPvpFlag() + "</td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<table width=270><tr><td>Класс персонажа Id: " + player.getClassId() + "/" + player.getClassId().getId() + "</td></tr></table><br>");
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td>Примечание: Заполните все значения перед сохранением внесенных изменений.</td></tr>");
		replyMSG.append("</table><br>");
		replyMSG.append("<table width=270>");
		replyMSG.append("<tr><td width=50>Hp:</td><td><edit var=\"hp\" width=50></td><td width=50>Mp:</td><td><edit var=\"mp\" width=50></td></tr>");
		replyMSG.append("<tr><td width=50>Pvp Flag:</td><td><edit var=\"pvpflag\" width=50></td><td width=50>Karma:</td><td><edit var=\"karma\" width=50></td></tr>");
		replyMSG.append("<tr><td width=50>Class Id:</td><td><edit var=\"classid\" width=50></td><td width=50>Pvp Kills:</td><td><edit var=\"pvpkills\" width=50></td></tr>");
		replyMSG.append("</table><br>");
		replyMSG.append("<center><button value=\"Сохранить\" action=\"bypass -h admin_save_modifications $hp $mp $karma $pvpflag $pvpkills $classid\" width=100 height=20 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></center><br>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void showCharacterActions(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer())
			player = (L2Player) target;
		else
			return;

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table><br><br>");
		replyMSG.append("<center>Admin Actions for: " + player.getName() + "</center><br>");
		replyMSG.append("<center><table width=200><tr>");
		replyMSG.append("<td width=100>Argument(*):</td><td width=100><edit var=\"arg\" width=100></td>");
		replyMSG.append("</tr></table><br></center>");
		replyMSG.append("<table width=270>");

		replyMSG.append("<tr><td width=90><button value=\"Телепорт\" action=\"bypass -h admin_teleportto " + player.getName() + "\" width=85 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=90><button value=\"К себе\" action=\"bypass -h admin_recall " + player.getName() + "\" width=85 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=90><button value=\"Квесты\" action=\"bypass -h admin_quests " + player.getName() + "\" width=85 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");

		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void findCharacter(final L2Player activeChar, final String CharacterToFind)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		int CharactersFound = 0;

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_show_characters 0\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");

		for(final L2Player element : L2ObjectsStorage.getAllPlayersForIterate())
			if(element.getName().startsWith(CharacterToFind))
			{
				CharactersFound = CharactersFound + 1;
				replyMSG.append("<table width=270>");
				replyMSG.append("<tr><td width=80>Имя</td><td width=110>Class</td><td width=40>Level</td></tr>");
				replyMSG.append("<tr><td width=80><a action=\"bypass -h admin_character_list " + element.getName() + "\">" + element.getName() + "</a></td><td width=110>" + element.getTemplate().className + "</td><td width=40>" + element.getLevel() + "</td></tr>");
				replyMSG.append("</table>");
			}

		if(CharactersFound == 0)
		{
			replyMSG.append("<table width=270>");
			replyMSG.append("<tr><td width=270>По Вашему запросу ничего не найти.</td></tr>");
			replyMSG.append("<tr><td width=270>Пожалуйста, попробуйте еще раз.<br></td></tr>");
			replyMSG.append("</table><br>");
			replyMSG.append("<center><table><tr><td>");
			replyMSG.append("<edit var=\"character_name\" width=80></td><td><button value=\"Найти\" action=\"bypass -h admin_find_character $character_name\" width=55 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\">");
			replyMSG.append("</td></tr></table></center>");
		}
		else
		{
			replyMSG.append("<center><br>Found " + CharactersFound + " character");

			if(CharactersFound == 1)
				replyMSG.append(".");
			else if(CharactersFound > 1)
				replyMSG.append("s.");
		}

		replyMSG.append("</center></body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void addExpSp(final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		L2Player player;
		if(target != null && target.isPlayer() && activeChar.getPlayerAccess().EditChar)
			player = (L2Player) target;
		else
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return;
		}

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Главная\" action=\"bypass -h admin_admin\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=130><center>Выбор персонажа</center></td>");
		replyMSG.append("<td width=40><button value=\"Назад\" action=\"bypass -h admin_current_player\" width=65 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<br><br>");
		replyMSG.append("<table width=270><tr><td>Имя: " + player.getName() + "</td></tr>");
		replyMSG.append("<tr><td>Lv: " + player.getLevel() + " " + player.getTemplate().className + "</td></tr>");
		replyMSG.append("<tr><td>Exp: " + player.getExp() + "</td></tr>");
		replyMSG.append("<tr><td>Sp: " + player.getSp() + "</td></tr></table>");

		replyMSG.append("<center><table><tr>");
		replyMSG.append("<td>Exp: <edit var=\"exp_to_add\" width=50></td>");
		replyMSG.append("<td>Sp:  <edit var=\"sp_to_add\" width=50></td>");
		replyMSG.append("<td>&nbsp;<button value=\"Сохранить\" action=\"bypass -h admin_add_exp_sp $exp_to_add $sp_to_add\" width=100 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table></center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	private void adminAddExpSp(final L2Player activeChar, final String ExpSp)
	{
		if(!activeChar.getPlayerAccess().EditChar)
		{
			activeChar.sendMessage("У вас есть не достаточно привилегий, для использования этой функции.");
			return;
		}

		final L2Object target = activeChar.getTarget();
		if(target == null || !target.isPlayer())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.SELECT_TARGET));
			return;
		}

		final L2Player player = (L2Player) target;
		final StringTokenizer st = new StringTokenizer(ExpSp);
		if(st.countTokens() != 2)
			addExpSp(activeChar);
		else
		{
			final String exp = st.nextToken();
			final String sp = st.nextToken();
			long expval = 0;
			long spval = 0;
			try
			{
				expval = Long.parseLong(exp);
				spval = Long.parseLong(sp);
			}
			catch(final NumberFormatException e)
			{
				// Wrong number (maybe it's too big?)
				activeChar.sendMessage("Wrong number format.");
			}
			if(expval != 0 || spval != 0)
			{
				// Common character information
				player.sendMessage("Admin is adding you " + expval + " exp and " + spval + " SP.");
				player.addExpAndSp(expval, spval, false, false);

				// Admin information
				activeChar.sendMessage("Added " + expval + " exp and " + spval + " SP to " + player.getName() + ".");

				Log.add("Added " + expval + " exp and " + spval + " SP to " + player.getName(), "gm_ext_actions", activeChar);
			}
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_edit_character.htm",
				"file://com_character_actions.htm",
				"file://com_current_player.htm",
				"file://com_nokarma.htm",
				"file://com_setkarma.htm",
				"file://com_character_list.htm",
				"file://com_show_characters.htm",
				"file://com_find_character.htm",
				"file://com_save_modifications.htm",
				"file://com_rec.htm",
				"file://com_settitle.htm",
				"file://com_setname.htm",
				"file://com_setsex.htm",
				"file://com_setcolor.htm",
				"file://com_setclass.htm",
				"file://com_add_exp_sp_to_character.htm",
				"file://com_add_exp_sp.htm",
				"file://com_sethero.htm",
				"file://com_resist.htm",
				"file://com_sethero_time.htm",
				"file://com_setnoble_time.htm",
				"Описание команды //setnoble" };
	}

	@Override
	public String[] getCommandGroup()
	{
		final String[] str = new String[_adminCommands.length];
		for(int i = 0; i < _adminCommands.length; i++)
			str[i] = AdminCommandHandler.GROUP_EDITCHAR;

		return str;
	}
}
