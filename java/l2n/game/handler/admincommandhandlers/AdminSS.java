package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;

import java.util.StringTokenizer;

public class AdminSS implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_ssq_change",
			"admin_ssq_time" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		if(command.startsWith("admin_ssq_change"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			if(st.countTokens() > 2)
			{
				st.nextToken();
				final int period = Integer.parseInt(st.nextToken());
				final int minutes = Integer.parseInt(st.nextToken());
				SevenSigns.getInstance().changePeriod(period, minutes * 60);
			}
			else if(st.countTokens() > 1)
			{
				st.nextToken();
				final int period = Integer.parseInt(st.nextToken());
				SevenSigns.getInstance().changePeriod(period);
			}
			else
				SevenSigns.getInstance().changePeriod();
		}
		if(command.startsWith("admin_ssq_time"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			if(st.countTokens() > 1)
			{
				st.nextToken();
				final int time = Integer.parseInt(st.nextToken());
				SevenSigns.getInstance().setTimeToNextPeriodChange(time);
			}
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды ssq_change",
				"Описание команды ssq_time" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER6,
				AdminCommandHandler.GROUP_MANAGESERVER6 };
	}
}
