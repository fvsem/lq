package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.ArrayList;


public class AdminMammon implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_find_mammon",
			"admin_show_mammon",
			"admin_hide_mammon",
			"admin_list_spawns" };

	ArrayList<Integer> npcIds = new ArrayList<Integer>();

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		npcIds.clear();

		if(!activeChar.getPlayerAccess().Admin)
			return false;

		else if(command.startsWith("admin_find_mammon"))
		{
			npcIds.add(31113);
			npcIds.add(31126);
			npcIds.add(31092); // Add the Marketeer of Mammon also
			int teleportIndex = -1;

			try
			{
				if(command.length() > 16)
					teleportIndex = Integer.parseInt(command.substring(18));
			}
			catch(final Exception NumberFormatException)
			{
				activeChar.sendPacket(SystemMessage.sendString("Command format is find_mammon <teleportIndex>"));
			}

			findAdminNPCs(activeChar, npcIds, teleportIndex, -1);
		}

		else if(command.equals("admin_show_mammon"))
		{
			npcIds.add(31113);
			npcIds.add(31126);

			findAdminNPCs(activeChar, npcIds, -1, 1);
		}

		else if(command.equals("admin_hide_mammon"))
		{
			npcIds.add(31113);
			npcIds.add(31126);

			findAdminNPCs(activeChar, npcIds, -1, 0);
		}

		else if(command.startsWith("admin_list_spawns"))
		{
			int npcId = 0;

			try
			{
				npcId = Integer.parseInt(command.substring(18).trim());
			}
			catch(final Exception NumberFormatException)
			{
				activeChar.sendMessage("Command format is //list_spawns <NPC_ID>");
			}

			npcIds.add(npcId);
			findAdminNPCs(activeChar, npcIds, -1, -1);
		}

		// Used for testing SystemMessage IDs - Use //msg <ID>
		else if(command.startsWith("admin_msg"))
			activeChar.sendPacket(new SystemMessage(Integer.parseInt(command.substring(10).trim())));

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	public void findAdminNPCs(final L2Player activeChar, final ArrayList<Integer> npcIdList, final int teleportIndex, final int makeVisible)
	{
		int index = 0;

		for(final L2NpcInstance npcInst : L2ObjectsStorage.getAllNpcsForIterate())
		{
			final int npcId = npcInst.getNpcId();

			if(npcIdList.contains(npcId))
			{
				if(makeVisible == 1)
					npcInst.spawnMe();
				else if(makeVisible == 0)
					npcInst.decayMe();

				if(npcInst.isVisible())
				{
					index++;

					if(teleportIndex > -1)
					{
						if(teleportIndex == index)
							activeChar.teleToLocation(npcInst.getLoc());
					}
					else
						activeChar.sendMessage(index + " - " + npcInst.getName() + " (" + npcInst.getObjectId() + "): " + npcInst.getX() + " " + npcInst.getY() + " " + npcInst.getZ());
				}
			}
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"admin_find_mammon",
				"admin_show_mammon",
				"admin_hide_mammon",
				"admin_list_spawns" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER3,
				AdminCommandHandler.GROUP_MANAGESERVER3 };
	}
}
