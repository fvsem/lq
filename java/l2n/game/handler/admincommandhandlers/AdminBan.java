package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.model.L2ObjectTasks.UnJailTask;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.ReflectionTable;
import l2n.util.AutoBan;
import l2n.util.HWID;

import java.util.StringTokenizer;

public class AdminBan implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_ban,
		admin_unban,
		admin_chatban,
		admin_ckarma,
		admin_cban,
		admin_chatunban,
		admin_acc_ban,
		admin_acc_unban,
		admin_jail,
		admin_unjail,
		admin_banhwid,
		admin_ban_hwid,
		admin_unban_hwid,
		admin_unbanhwid
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Ban)
			return false;

		final String[] wordList = command.split(" ");

		Commands comm;
		try
		{
			comm = Commands.valueOf(wordList[0].toLowerCase());
		}
		catch(final Exception e)
		{
			return false;
		}
		final StringTokenizer st = new StringTokenizer(command);

		if(activeChar.getPlayerAccess().Ban)
			switch (comm)
			{
				case admin_ban:
					ban(st, activeChar);
					break;
				case admin_unban:
					if(st.countTokens() > 1)
					{
						st.nextToken();
						final String player = st.nextToken();
						final L2Player plyr = L2ObjectsStorage.getPlayer(player);
						if(plyr != null)
						{
							LSConnection.getInstance().sendPacket(new ChangeAccessLevel(plyr.getAccountName(), 0, "command admin_unban", 0));
							activeChar.sendMessage("Вы Разбанили " + player);
						}
						if(AutoBan.Banned(player, 0, 0, "", activeChar.getName()))
							activeChar.sendMessage("Вы Разбанили: " + player);
						else
							activeChar.sendMessage("Невозможно найти персонажа: " + player);
					}
					break;
				case admin_acc_ban:
					if(st.countTokens() > 1)
					{
						st.nextToken();
						int time = 0;
						String reason = "command by " + activeChar.getName();
						String account = st.nextToken();
						if(account.equals("$target"))
						{
							if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
								return false;
							account = ((L2Player) activeChar.getTarget()).getAccountName();
						}
						if(st.hasMoreTokens())
							time = Integer.parseInt(st.nextToken());
						if(st.hasMoreTokens())
							reason = activeChar.getName() + ": " + st.nextToken();
						LSConnection.getInstance().sendPacket(new ChangeAccessLevel(account, -100, reason, time));
						activeChar.sendMessage("Вы забанены " + account + ", причина: " + reason);
						L2Player tokick = null;
						for(final L2Player p : L2ObjectsStorage.getAllPlayersForIterate())
							if(p.getAccountName().equalsIgnoreCase(account))
							{
								tokick = p;
								break;
							}
						if(tokick != null)
						{
							tokick.logout(false, false, true, true);
							activeChar.sendMessage("Игрок" + tokick.getName() + " Выброшен.");
						}
					}
					break;
				case admin_acc_unban:
					if(st.countTokens() > 1)
					{
						st.nextToken();
						final String account = st.nextToken();
						LSConnection.getInstance().sendPacket(new ChangeAccessLevel(account, 0, "command admin_acc_unban", 0));
						activeChar.sendMessage("Вы разбанены: " + account);
					}
					break;
				case admin_chatban:
					try
					{
						st.nextToken();
						final String player = st.nextToken();
						final String srok = st.nextToken();
						final String bmsg = "admin_chatban " + player + " " + srok + " ";
						final String msg = command.substring(bmsg.length(), command.length());

						if(AutoBan.ChatBan(player, Integer.parseInt(srok), msg, activeChar.getName()))
							activeChar.sendMessage("Чат блокирован " + player + ".");
						else
							activeChar.sendMessage("Нет такого персонажа " + player + ".");
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Command syntax: //chatban char_name Время Причина");
					}
					break;
				case admin_chatunban:
					try
					{
						st.nextToken();
						final String player = st.nextToken();

						if(AutoBan.ChatUnBan(player, activeChar.getName()))
							activeChar.sendMessage("Вы разбанили  персонажа " + player + ".");
						else
							activeChar.sendMessage("Can't find char " + player + ".");
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Command syntax: //chatunban char_name");
					}
					break;
				case admin_jail:
					try
					{
						st.nextToken();
						final String player = st.nextToken();
						final String srok = st.nextToken();

						final L2Player target = L2ObjectsStorage.getPlayer(player);

						if(target != null)
						{
							target.setVar("jailedFrom", target.getX() + ";" + target.getY() + ";" + target.getZ());
							target.setVar("jailed", srok); // "jailed" - срок тюряги в минутах
							target._unjailTask = L2GameThreadPools.getInstance().scheduleGeneral(new UnJailTask(target, target.getLoc()), Integer.parseInt(srok) * 60000);
							target.teleToLocation(-114648, -249384, -2984, ReflectionTable.JAIL);
							activeChar.sendMessage("Вы посадили в тюрьму: " + player + " на " + srok + " минут.");
							target.sendMessage("GM " + activeChar.getName() + " посадил вас в тюрьму на " + srok + " минут.");

						}
						else
							activeChar.sendMessage("Нет такого персонажа" + player + ".");
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Command syntax: //jail char_name period_minutes");
					}
					break;
				case admin_unjail:
					try
					{
						st.nextToken();
						final String player = st.nextToken();

						final L2Player target = L2ObjectsStorage.getPlayer(player);
						if(target != null && target.getVar("jailed") != null)
						{
							final String[] re = target.getVar("jailedFrom").split(";");
							target.teleToLocation(Integer.parseInt(re[0]), Integer.parseInt(re[1]), Integer.parseInt(re[2]));
							target.setReflection(0);
							target._unjailTask.cancel(true);
							target._isInJail = false;
							target.unsetVar("jailedFrom");
							target.unsetVar("jailed");
							activeChar.sendMessage("Вы выпустили из тюрьмы: " + player + ".");
							target.sendMessage("GM " + activeChar.getName() + " выпустил вас из тюрьмы.");
						}
						else
							activeChar.sendMessage("Игрок не найден: " + player + ".");
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Command syntax: //unjail char_name");
						e.printStackTrace();
					}
					break;
				case admin_ckarma:
					try
					{
						st.nextToken();
						final String player = st.nextToken();
						final String srok = st.nextToken();
						final String bmsg = "admin_ckarma " + player + " " + srok + " ";
						final String msg = command.substring(bmsg.length(), command.length());

						final L2Player plyr = L2ObjectsStorage.getPlayer(player);
						if(plyr != null)
						{
							final int newKarma = Integer.parseInt(srok) + plyr.getKarma();

							// update karma
							plyr.setKarma(newKarma);

							plyr.sendMessage("You get karma(" + srok + ") by GM " + activeChar.getName());
							AutoBan.Karma(plyr, Integer.parseInt(srok), msg, activeChar.getName());
							activeChar.sendMessage("Вы устанавливаете карму(" + srok + ") " + plyr.getName());
						}
						else if(AutoBan.Karma(player, Integer.parseInt(srok), msg, activeChar.getName()))
							activeChar.sendMessage("Вы устанавливаете карму(" + srok + ") " + player);
						else
							activeChar.sendMessage("Нет такого персонажа: " + player);
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Command syntax: //ckarma char_name Карма причина");
					}
					break;
				case admin_cban:
					AdminHelpPage.showHelpPage(activeChar, "cban.htm");
					break;
				case admin_banhwid:
				case admin_ban_hwid:
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //banhwid char_name|hwid [kick:true|false] [reason]");
						return false;
					}
					try
					{
						if(wordList[1].equals("$target"))
							if(activeChar.getTarget() != null && activeChar.getTarget().isPlayer())
								wordList[1] = activeChar.getTarget().getName();
						activeChar.sendMessage(HWID.handleBanHWID(wordList));
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("USAGE: //banhwid char_name|hwid [kick:true|false] [reason]");
					}
					break;
				case admin_unbanhwid:
				case admin_unban_hwid:
					if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS)
					{
						activeChar.sendMessage("HWID bans feature disabled");
						return false;
					}
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //unbanhwid hwid");
						return false;
					}
					if(wordList[1].length() != 32)
					{
						activeChar.sendMessage(wordList[1] + " is not like HWID");
						return false;
					}
					HWID.UnbanHWID(wordList[1]);
					activeChar.sendMessage("HWID " + wordList[1] + " unbanned");
					break;
			}

		return true;
	}

	private boolean ban(final StringTokenizer st, final L2Player activeChar)
	{
		try
		{
			st.nextToken();

			final String player = st.nextToken();

			int time = 0;
			String msg = "";

			if(st.hasMoreTokens())
				time = Integer.parseInt(st.nextToken());

			if(st.hasMoreTokens())
			{
				msg = "admin_ban " + player + " " + time + " ";
				while (st.hasMoreTokens())
					msg += st.nextToken() + " ";
				msg.trim();
			}

			final L2Player plyr = L2ObjectsStorage.getPlayer(player);
			if(plyr != null)
			{
				plyr.sendMessage(new CustomMessage("scripts.commands.admin.AdminBan.YoureBannedByGM", plyr).addString(activeChar.getName()));
				plyr.setAccessLevel(-100);
				AutoBan.Banned(plyr, time, msg, activeChar.getName());
				plyr.logout(false, false, true, true);
				activeChar.sendMessage("Вы заблокировали " + plyr.getName());
			}
			else if(AutoBan.Banned(player, -100, time, msg, activeChar.getName()))
				activeChar.sendMessage("Вы заблокировали " + player);
			else
				activeChar.sendMessage("Нет такого персонажа: " + player);
		}
		catch(final Exception e)
		{
			activeChar.sendMessage("Command syntax: //ban char_name дни причина");
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		final String[] st = new String[Commands.values().length];
		int i = 0;
		for(final Commands c : Commands.values())
			st[i++] = c.name();

		return st;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_ban.htm",
				"file://com_unban.htm",
				"file://com_chatban.htm",
				"file://com_ckarma.htm",
				"file://com_cban.htm",
				"Описание команды chatunban",
				"Описание команды acc_ban",
				"Описание команды acc_unban",
				"Описание команды jail",
				"Описание команды unjail",
				"Описание команды banhwid",
				"Описание команды ban_hwid",
				"Описание команды unban_hwid",
				"Описание команды unbanhwid" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS };
	}
}
