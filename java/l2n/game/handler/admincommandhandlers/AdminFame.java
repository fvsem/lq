package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

import java.util.StringTokenizer;

public class AdminFame implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_addfame",
			"admin_setfame" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar == null)
			return false;

		if(!activeChar.getPlayerAccess().EditChar)
			return false;

		if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		final L2Player target = (L2Player) activeChar.getTarget();
		final StringTokenizer st = new StringTokenizer(command, " ");
		final String actualCommand = st.nextToken();

		String val = "";
		if(st.countTokens() >= 1)
			val = st.nextToken();

		if(actualCommand.equalsIgnoreCase("admin_addfame"))
			try
			{
				final Integer fame = target.getFame() + Integer.parseInt(val);
				activeChar.addFame(fame);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Wrong Number Format");
			}
		else if(actualCommand.equalsIgnoreCase("admin_setfame"))
			try
			{
				final int fame = Integer.parseInt(val);
				activeChar.setFame(fame);
			}
			catch(final NumberFormatException e)
			{
				// TODO откорректировать Мессаг
				activeChar.sendMessage("Message need the correct");
				return false;
			}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_addfame", "admin_setfame" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
