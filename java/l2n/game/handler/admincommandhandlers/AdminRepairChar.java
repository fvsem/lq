package l2n.game.handler.admincommandhandlers;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;

public class AdminRepairChar implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_restore", "admin_repair" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar.getPlayerAccess() == null || !activeChar.getPlayerAccess().EditChar)
			return false;
		handleRepair(command);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleRepair(final String command)
	{
		final String[] parts = command.split(" ");
		if(parts.length != 2)
			return;

		final String cmd = "UPDATE characters SET x=-84318, y=244579, z=-3730 WHERE char_name=?";
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(cmd);
			statement.setString(1, parts[1]);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("SELECT obj_id FROM characters where char_name=?");
			statement.setString(1, parts[1]);
			rset = statement.executeQuery();
			int objId = 0;
			if(rset.next())
				objId = rset.getInt(1);

			DbUtils.closeQuietly(statement, rset);

			if(objId == 0)
				return;

			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM character_shortcuts WHERE char_obj_id=?");
			statement.setInt(1, objId);
			statement.execute();
			DbUtils.close(statement);

			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE items SET loc='INVENTORY' WHERE owner_id=? AND loc!='WAREHOUSE'");
			statement.setInt(1, objId);
			statement.execute();

		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_restore", "admin_repair" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_EDITCHAR, AdminCommandHandler.GROUP_EDITCHAR };
	}
}
