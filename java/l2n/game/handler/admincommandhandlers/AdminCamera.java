package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.CameraMode;
import l2n.game.network.serverpackets.SpecialCamera;

public class AdminCamera implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_freelook",
			"admin_cinematic" };

	@Override
	public boolean useAdminCommand(String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;
		if(command.startsWith("admin_freelook"))
		{
			if(command.length() > _adminCommands[0].length() + 1)
				command = command.substring(_adminCommands[0].length() + 1);
			else
			{
				activeChar.sendMessage("Usage: //freelook 1 or //freelook 0");
				return false;
			}

			final int mode = Integer.parseInt(command);
			if(mode == 1)
			{
				activeChar.setInvisible(true);
				activeChar.setInvul(true);
				activeChar.setNoChannel(-1);
				activeChar.setFlying(true);
			}
			else
			{
				activeChar.setInvisible(false);
				activeChar.setInvul(false);
				activeChar.setNoChannel(0);
				activeChar.setFlying(false);
			}
			activeChar.sendPacket(new CameraMode(mode));
			return true;
		}
		else if(command.startsWith("admin_cinematic"))
		{
			command = command.substring(_adminCommands[1].length() + 1);

			final String[] params = command.split(" ");

			final int id = Integer.parseInt(params[0]);
			final int dist = Integer.parseInt(params[1]);
			final int yaw = Integer.parseInt(params[2]);
			final int pitch = Integer.parseInt(params[3]);
			final int time = Integer.parseInt(params[4]);
			final int duration = Integer.parseInt(params[5]);

			activeChar.sendPacket(new SpecialCamera(id, dist, yaw, pitch, time, duration));
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_freelook.htm",
				"file://com_cinematic.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER };
	}
}
