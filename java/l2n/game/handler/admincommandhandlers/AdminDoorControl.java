package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.tables.DoorTable;

public class AdminDoorControl implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_open",
			"admin_close",
			"admin_openall",
			"admin_closeall" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Door)
			return false;

		try
		{
			if(command.startsWith("admin_open "))
			{
				final int doorId = Integer.parseInt(command.split(" ")[1]);
				DoorTable.getInstance().getDoor(doorId).openMe();
			}
			else if(command.startsWith("admin_close "))
			{
				final int doorId = Integer.parseInt(command.split(" ")[1]);
				DoorTable.getInstance().getDoor(doorId).closeMe();
			}
			if(command.equals("admin_closeall"))
				for(final L2DoorInstance door : DoorTable.getInstance().getDoors())
					door.closeMe();
			if(command.equals("admin_openall"))
				for(final L2DoorInstance door : DoorTable.getInstance().getDoors())
					door.openMe();
			if(command.equals("admin_open"))
			{
				final L2Object target = activeChar.getTarget();
				if(target instanceof L2DoorInstance)
					((L2DoorInstance) target).openMe();
				else
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
			}

			if(command.equals("admin_close"))
			{
				final L2Object target = activeChar.getTarget();
				if(target instanceof L2DoorInstance)
					((L2DoorInstance) target).closeMe();
				else
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_open.htm",
				"file://com_close.htm",
				"file://com_openall.htm",
				"file://com_closeall.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1 };
	}
}
