package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;

import java.io.File;
import java.io.FileInputStream;
import java.util.StringTokenizer;

public class AdminServer implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_server",
			"admin_gc",
			"admin_test" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		if(command.startsWith("admin_server"))
			try
			{
				final String val = command.substring(13);
				showHelpPage(activeChar, val);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				// case of empty filename
			}
		else if(command.startsWith("admin_gc"))
		{
			try
			{
				System.gc();
				Thread.sleep(1000L);
				System.gc();
				Thread.sleep(1000L);
				System.gc();
			}
			catch(final Exception e)
			{}
			activeChar.sendMessage("OK! - garbage collector called.");
		}
		else if(command.startsWith("admin_test"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			st.nextToken(); // skip command
			@SuppressWarnings("unused")
			String val1 = null;
			if(st.hasMoreTokens())
				val1 = st.nextToken();
			@SuppressWarnings("unused")
			String val2 = null;
			if(st.hasMoreTokens())
				val2 = st.nextToken();

			// Сюда пихать тестовый код
			/**
			 * try
			 * {
			 * activeChar.sendPacket(new SSQInfo(Integer.parseInt(val1), Integer.parseInt(val2)));
			 * }
			 * catch(NumberFormatException e)
			 * {
			 * e.printStackTrace();
			 * }
			 * // тут тестовый код кончается
			 */
			activeChar.sendMessage("Test.");
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	// FIXME: implement method to send html to player in L2Player directly
	// PUBLIC & STATIC so other classes from package can include it directly
	public static void showHelpPage(final L2Player targetChar, final String filename)
	{
		final File file = new File("data/html/admin/" + filename);
		FileInputStream fis = null;

		try
		{
			fis = new FileInputStream(file);
			final byte[] raw = new byte[fis.available()];
			fis.read(raw);

			final String content = new String(raw, "UTF-8");

			final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

			adminReply.setHtml(content);
			targetChar.sendPacket(adminReply);
		}
		catch(final Exception e)
		{
			// problem with adminserver is ignored
		}
		finally
		{
			try
			{
				if(fis != null)
					fis.close();
			}
			catch(final Exception e1)
			{
				// problems ignored
			}
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды server",
				"Описание команды gc",
				"Описание команды test" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5,
				AdminCommandHandler.GROUP_MANAGESERVER5 };
	}
}
