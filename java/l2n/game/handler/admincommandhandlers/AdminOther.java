package l2n.game.handler.admincommandhandlers;

import l2n.game.GameTimeController;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;
import java.util.logging.Logger;

public class AdminOther implements IAdminCommandHandler
{
	protected static final Logger _log = Logger.getLogger(AdminOther.class.getName());

	private static String _adminCommands[] = { "admin_sa", // Социальное действие
			"admin_sm", // Камера
			"admin_setcolh",
			"admin_su",
			"admin_smsg" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.startsWith("admin_sa"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			try
			{
				st.nextToken();
				final int s_id = Integer.parseInt(st.nextToken());

				final L2Character target = (L2Character) activeChar.getTarget();
				if(target != null)
				{
					target.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
					target.broadcastPacket(new SocialAction(target.getObjectId(), s_id));
				}
				else
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
			}
			catch(final Exception e)
			{}
		}
		else if(command.startsWith("admin_sm"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				final int t = GameTimeController.getInstance().getGameTime();
				final int h = t / 60 % 24;
				final int m = t % 60;

				_log.info("0 " + t);

				_log.info("1 " + h + ":" + m);

				_log.info("isNight " + GameTimeController.getInstance().isNowNight());

				activeChar.sendPacket(new ClientSetTime());
			}
			catch(final Exception e)
			{}
		}
		else if(command.startsWith("admin_su"))
			try
			{
				final StringTokenizer st = new StringTokenizer(command);
				st.nextToken();
				final int s_id = Integer.parseInt(st.nextToken());

				final L2Character target = (L2Character) activeChar.getTarget();
				if(target != null)
				{
					target.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
					target.broadcastPacket(new MagicSkillUse(target, target, s_id, 1, SkillTable.getInstance().getInfo(s_id, 1).getHitTime(), 1));
				}
				else
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
			}
			catch(final Exception e)
			{}
		else if(command.startsWith("admin_setcolh"))
		{
			final StringTokenizer st = new StringTokenizer(command);

			try
			{
				st.nextToken();
				final int val = Integer.parseInt(st.nextToken());

				L2NpcInstance npc;
				L2Spawn tempSpawn;
				L2NpcTemplate template;
				try
				{
					template = NpcTable.getTemplate(val);
					tempSpawn = new L2Spawn(template);
					tempSpawn.setLocx(174234);
					tempSpawn.setLocy(-88015);
					tempSpawn.setLocz(-5140);
					tempSpawn.setHeading(16384);
					tempSpawn.setAmount(1);
					tempSpawn.setRespawnDelay(0);
					tempSpawn.setReflection(0);
					tempSpawn.stopRespawn();

					npc = tempSpawn.doSpawn(true);

					npc.setCollisionHeight(600);
					npc.broadcastPacket(new NpcInfo(npc, npc));
					npc.setImmobilized(true);
					npc.setInvul(true);

					L2GameThreadPools.getInstance().scheduleGeneral(new test(1, npc), 5000);
				}
				catch(final Exception e)
				{}
			}
			catch(final Exception e)
			{}
		}
		if(command.startsWith("admin_smsg"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				final int msg_id = Integer.parseInt(st.nextToken());
				activeChar.sendPacket(msg_id);
			}
			catch(final Exception e)
			{}
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"выполняет соц. действие у цели (SocialAction)",
				"цель кастует указанный скил (только анимация MagicSkillUse)",
				"показывает видео (SpecialCamera)",
				"Описание команды setcolh",
				"Описание команды admin_smsg" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4,
				AdminCommandHandler.GROUP_MANAGESERVER4 };
	}

	private class test implements Runnable
	{
		private final int _id;
		private final L2NpcInstance _npc;

		public test(final int id, final L2NpcInstance npc)
		{
			_id = id;
			_npc = npc;
		}

		@Override
		public void run()
		{
			System.out.println(_id);

			switch (_id)
			{
				case 1: // spawn.
					L2GameThreadPools.getInstance().scheduleGeneral(new test(2, _npc), 1000);
					break;
				case 2:
					// show movie
					_npc.broadcastPacket(new SpecialCamera(_npc.getObjectId(), 0, 75, -89, 0, 100));
					// set next task.
					L2GameThreadPools.getInstance().scheduleGeneral(new test(201, _npc), 0);
					break;
				case 201:
					// show movie
					_npc.broadcastPacket(new SpecialCamera(_npc.getObjectId(), 0, 75, -89, 0, 100));
					// set next task.
					L2GameThreadPools.getInstance().scheduleGeneral(new test(3, _npc), 0);
					break;
				case 3:
					// show movie
					_npc.broadcastPacket(new SpecialCamera(_npc.getObjectId(), 300, 90, -10, 6500, 7000));
					System.out.println("Invul " + _npc.isInvul());
					System.out.println("Immobilized " + _npc.isImobilised());
			}
		}
	}
}
