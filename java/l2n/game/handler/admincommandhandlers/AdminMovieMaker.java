package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.MovieMakerManager;
import l2n.game.model.actor.L2Player;

public class AdminMovieMaker implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_addseq",
			"admin_playseqq",
			"admin_delsequence",
			"admin_editsequence",
			"admin_addsequence",
			"admin_playsequence",
			"admin_movie",
			"admin_updatesequence",
			"admin_broadcast",
			"admin_playmovie",
			"admin_brodmovie" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.equals("admin_movie"))
			MovieMakerManager.main_txt(activeChar);
		else if(command.startsWith("admin_playseqq"))
		{
			final String val = command.substring(15);
			final int id = Integer.parseInt(val);
			MovieMakerManager.play_sequence(id, activeChar);
		}
		else if(command.equals("admin_addseq"))
			MovieMakerManager.add_seq(activeChar);
		else if(command.startsWith("admin_addsequence"))
		{
			final String[] args = command.split(" ");
			int targ = 0;
			if(args.length < 10)
			{
				activeChar.sendMessage("Not all arguments were set.");
				return false;
			}
			if(activeChar.getTarget() != null)
				targ = activeChar.getTarget().getObjectId();
			else
			{
				activeChar.sendMessage("You must first select a target.");
				MovieMakerManager.main_txt(activeChar);
				return false;
			}
			MovieMakerManager.add_sequence(activeChar, Integer.parseInt(args[1]), targ, Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]), Integer.parseInt(args[7]), Integer.parseInt(args[8]), Integer.parseInt(args[9]));
		}
		else if(command.startsWith("admin_playsequence"))
		{
			final String[] args = command.split(" ");
			int targ = 0;
			if(args.length < 10)
			{
				activeChar.sendMessage("Not all arguments were set.");
				return false;
			}
			if(activeChar.getTarget() != null)
				targ = activeChar.getTarget().getObjectId();
			else
			{
				activeChar.sendMessage("You must first select a target.");
				MovieMakerManager.main_txt(activeChar);
				return false;
			}
			MovieMakerManager.play_sequence(activeChar, targ, Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]), Integer.parseInt(args[7]), Integer.parseInt(args[8]));
		}
		else if(command.startsWith("admin_editsequence"))
		{
			final String val = command.substring(19);
			final int id = Integer.parseInt(val);
			MovieMakerManager.edit_seq(id, activeChar);
		}
		else if(command.startsWith("admin_updatesequence"))
		{
			final String[] args = command.split(" ");
			int targ = 0;
			if(args.length < 10)
			{
				activeChar.sendMessage("Not all arguments wrtr set.");
				return false;
			}
			if(activeChar.getTarget() != null)
				targ = activeChar.getTarget().getObjectId();
			else
			{
				activeChar.sendMessage("You must first select a target.");
				MovieMakerManager.main_txt(activeChar);
				return false;
			}
			MovieMakerManager.update_sequence(activeChar, Integer.parseInt(args[1]), targ, Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]), Integer.parseInt(args[7]), Integer.parseInt(args[8]), Integer.parseInt(args[9]));
		}
		else if(command.startsWith("admin_delsequence"))
		{
			final String val = command.substring(18);
			final int id = Integer.parseInt(val);
			MovieMakerManager.delete_sequence(id, activeChar);
		}
		else if(command.startsWith("admin_broadcast"))
		{
			final String val = command.substring(15);
			final int id = Integer.parseInt(val);
			MovieMakerManager.brodcast_sequence(id, activeChar);
		}
		else if(command.equals("admin_playmovie"))
			MovieMakerManager.play_movie(0, activeChar);
		else if(command.equals("admin_brodmovie"))
			MovieMakerManager.play_movie(1, activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды addseq",
				"Описание команды playseqq",
				"Описание команды delsequence",
				"Описание команды editsequence",
				"Описание команды addsequence",
				"Описание команды playsequence",
				"Описание команды movie",
				"Описание команды updatesequence",
				"Описание команды broadcast",
				"Описание команды playmovie",
				"Описание команды brodmovie" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER,
				AdminCommandHandler.GROUP_MOVIE_MAKER };
	}
}
