package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.Residence;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.entity.siege.territory.TerritorySiegeDatabase;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ClanTable;

import java.util.StringTokenizer;

public class AdminSiege implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_siege",
			"admin_add_attacker",
			"admin_add_defender",
			"admin_add_guard",
			"admin_list_siege_clans",
			"admin_clear_siege_list",
			"admin_move_defenders",
			"admin_spawn_doors",
			"admin_endsiege",
			"admin_startsiege",
			"admin_setcastle",
			"admin_castledel",
			"admin_territorysiege",
			"admin_startterritorysiege",
			"admin_stopterritorysiege",
			"admin_addterritorymember",
			"admin_addterritoryclan",
			"admin_clearterritorylist",
			"admin_listterritorymembers" };

	@Override
	public boolean useAdminCommand(String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Siege)
			return false;

		final StringTokenizer st = new StringTokenizer(command, " ");
		command = st.nextToken();

		final L2Object target = activeChar.getTarget();
		L2Player player = activeChar;
		if(target != null && target.isPlayer())
			player = (L2Player) target;

		if(command.equalsIgnoreCase("admin_territorysiege"))
		{
			showTerritorySiegePage(activeChar);
			return true;
		}
		else if(command.equalsIgnoreCase("admin_startterritorysiege"))
		{
			TerritorySiege.startSiege();
			showTerritorySiegePage(activeChar);
			return true;
		}
		else if(command.equalsIgnoreCase("admin_stopterritorysiege"))
		{
			TerritorySiege.endSiege();
			showTerritorySiegePage(activeChar);
			return true;
		}
		else if(command.equalsIgnoreCase("admin_addterritorymember"))
		{
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Incorrect territory number!");
				showTerritorySiegePage(activeChar);
				return false;
			}
			final int territoryId = Integer.parseInt(st.nextToken());
			if(territoryId < 1 || territoryId > 9)
			{
				activeChar.sendMessage("Incorrect territory number!");
				showTerritorySiegePage(activeChar);
				return false;
			}
			TerritorySiege.getPlayers().put(player.getObjectId(), territoryId);
			TerritorySiegeDatabase.changeRegistration(player.getObjectId(), territoryId, 0, false);
			showTerritorySiegePage(activeChar);
			return true;
		}
		else if(command.equalsIgnoreCase("admin_addterritoryclan"))
		{
			if(!st.hasMoreTokens())
			{
				activeChar.sendMessage("Incorrect territory number!");
				showTerritorySiegePage(activeChar);
				return false;
			}
			final int territoryId = Integer.parseInt(st.nextToken());
			if(territoryId < 1 || territoryId > 9)
			{
				activeChar.sendMessage("Incorrect territory number!");
				showTerritorySiegePage(activeChar);
				return false;
			}
			final L2Clan clan = player.getClan();
			if(clan == null)
			{
				activeChar.sendMessage("Target must be a clan member!");
				showTerritorySiegePage(activeChar);
				return false;
			}
			TerritorySiege.getClans().put(new SiegeClan(clan.getClanId(), null), territoryId);
			TerritorySiegeDatabase.changeRegistration(clan.getClanId(), territoryId, 1, false);
			showTerritorySiegePage(activeChar);
			return true;
		}
		else if(command.equalsIgnoreCase("admin_clearterritorylist"))
		{
			TerritorySiege.getPlayers().clear();
			TerritorySiege.getClans().clear();
			TerritorySiegeDatabase.saveSiegeMembers();
			showTerritorySiegePage(activeChar);
			return true;
		}

		Residence siegeUnit = null;
		int siegeUnitId = 0;
		if(st.hasMoreTokens())
			siegeUnitId = Integer.parseInt(st.nextToken());

		if(siegeUnitId != 0)
		{
			siegeUnit = CastleManager.getInstance().getCastleByIndex(siegeUnitId);
			if(siegeUnit == null)
				siegeUnit = FortressManager.getInstance().getFortressByIndex(siegeUnitId);
			if(siegeUnit == null)
				siegeUnit = ClanHallManager.getInstance().getClanHall(siegeUnitId);
		}

		if(siegeUnit == null || siegeUnit.getId() < 0 || siegeUnit.getSiege() == null)
			showSiegeUnitSelectPage(activeChar);
		else
		{
			if(command.equalsIgnoreCase("admin_add_attacker"))
				siegeUnit.getSiege().registerAttacker(player, true);
			else if(command.equalsIgnoreCase("admin_add_defender"))
				siegeUnit.getSiege().registerDefender(player, true);
			else if(command.equalsIgnoreCase("admin_add_guard"))
			{
				// Get value
				String val = "";
				if(st.hasMoreTokens())
					val = st.nextToken();

				if(!val.equals(""))
					try
					{
						final int npcId = Integer.parseInt(val);
						siegeUnit.getSiege().getSiegeGuardManager().addSiegeGuard(activeChar, npcId);
					}
					catch(final Exception e)
					{
						activeChar.sendMessage("Value entered for Npc Id wasn't an integer");
					}
				else
					activeChar.sendMessage("Missing Npc Id");
			}
			else if(command.equalsIgnoreCase("admin_clear_siege_list"))
				siegeUnit.getSiege().getDatabase().clearSiegeClan();
			else if(command.equalsIgnoreCase("admin_endsiege"))
				siegeUnit.getSiege().endSiege();
			else if(command.equalsIgnoreCase("admin_list_siege_clans"))
			{
				siegeUnit.getSiege().listRegisterClan(activeChar);
				return true;
			}
			else if(command.equalsIgnoreCase("admin_move_defenders"))
				activeChar.sendPacket(Msg.NOT_WORKING_PLEASE_TRY_AGAIN_LATER);
			else if(command.equalsIgnoreCase("admin_setcastle"))
			{
				if(player == null || player.getClan() == null)
					activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
				else
				{
					siegeUnit.changeOwner(player.getClan());
					System.out.println("Castle " + siegeUnit.getName() + " owned by clan " + player.getClan().getName());
				}
			}
			else if(command.equalsIgnoreCase("admin_castledel"))
				siegeUnit.changeOwner(null);
			else if(command.equalsIgnoreCase("admin_spawn_doors"))
				siegeUnit.spawnDoor();
			else if(command.equalsIgnoreCase("admin_startsiege"))
				siegeUnit.getSiege().startSiege();

			showSiegePage(activeChar, siegeUnit);
		}

		return true;
	}

	public void showSiegeUnitSelectPage(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center><font color=\"LEVEL\">Siege Units</font></center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table><br>");

		replyMSG.append("<table width=260>");
		replyMSG.append("<tr><td>Unit Name</td><td>Owner</td></tr>");

		for(final Castle castle : CastleManager.getInstance().getCastles().values())
			if(castle != null)
			{
				replyMSG.append("<tr><td>");
				replyMSG.append("<a action=\"bypass -h admin_siege " + castle.getId() + "\">" + castle.getName() + "</a>");
				replyMSG.append("</td><td>");

				final L2Clan owner = castle.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(castle.getOwnerId());
				if(owner == null)
					replyMSG.append("NPC");
				else
					replyMSG.append(owner.getName());

				replyMSG.append("</td></tr>");
			}

		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
			if(fortress != null)
			{
				replyMSG.append("<tr><td>");
				replyMSG.append("<a action=\"bypass -h admin_siege " + fortress.getId() + "\">" + fortress.getName() + "</a>");
				replyMSG.append("</td><td>");

				final L2Clan owner = fortress.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(fortress.getOwnerId());
				if(owner == null)
					replyMSG.append("NPC");
				else
					replyMSG.append(owner.getName());

				replyMSG.append("</td></tr>");
			}

		for(final ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			if(clanhall != null && clanhall.getSiege() != null)
			{
				replyMSG.append("<tr><td>");
				replyMSG.append("<a action=\"bypass -h admin_siege " + clanhall.getId() + "\">" + clanhall.getName() + "</a>");
				replyMSG.append("</td><td>");

				final L2Clan owner = clanhall.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(clanhall.getOwnerId());
				if(owner == null)
					replyMSG.append("NPC");
				else
					replyMSG.append(owner.getName());

				replyMSG.append("</td></tr>");
			}

		replyMSG.append("</table>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	public void showSiegePage(final L2Player activeChar, final Residence residence)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center>Siege Menu</center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_siege\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<center>");
		replyMSG.append("<br><br><br>Siege Unit: " + residence.getName() + "<br><br>");
		replyMSG.append("Unit Owner: ");

		final L2Clan owner = residence.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(residence.getOwnerId());
		if(owner == null)
			replyMSG.append("NPC");
		else
			replyMSG.append(owner.getName());

		replyMSG.append("<br><br><table>");
		replyMSG.append("<tr><td><button value=\"Add Attacker\" action=\"bypass -h admin_add_attacker " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Add Defender\" action=\"bypass -h admin_add_defender " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"List Clans\" action=\"bypass -h admin_list_siege_clans " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Clear List\" action=\"bypass -h admin_clear_siege_list " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Move Defenders\" action=\"bypass -h admin_move_defenders " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Spawn Doors\" action=\"bypass -h admin_spawn_doors " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Start Siege\" action=\"bypass -h admin_startsiege " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"End Siege\" action=\"bypass -h admin_endsiege " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Give Unit\" action=\"bypass -h admin_setcastle " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Take Unit\" action=\"bypass -h admin_castledel " + residence.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td>NpcId: <edit var=\"value\" width=40>");
		replyMSG.append("<td><button value=\"Add Guard\" action=\"bypass -h admin_add_guard " + residence.getId() + " $value\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("</center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	public void showTerritorySiegePage(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center>Territory Siege Menu</center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_siege\" width=40 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<center>");

		replyMSG.append("<br><br><table>");
		replyMSG.append("<tr><td>Territory Id: <edit var=\"value\" width=40></td><td></td></tr>");
		replyMSG.append("<tr><td><button value=\"Add Player\" action=\"bypass -h admin_addterritorymember $value\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Add Clan\" action=\"bypass -h admin_addterritoryclan $value\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("<tr><td><button value=\"List Members\" action=\"bypass -h admin_listterritorymembers\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Clear List\" action=\"bypass -h admin_clearterritorylist\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");

		replyMSG.append("<br>");

		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Start Siege\" action=\"bypass -h admin_startterritorysiege\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"End Siege\" action=\"bypass -h admin_stopterritorysiege\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");

		replyMSG.append("</center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды siege",
				"Описание команды add_attacker",
				"Описание команды add_defender",
				"Описание команды add_guard",
				"Описание команды list_siege_clans",
				"Описание команды clear_siege_list",
				"Описание команды move_defenders",
				"Описание команды spawn_doors",
				"Описание команды endsiege",
				"Описание команды startsiege",
				"Описание команды setcastle",
				"Описание команды castledel",
				"Описание команды territorysiege",
				"Описание команды startterritorysiege",
				"Описание команды stopterritorysiege",
				"Описание команды addterritorymember",
				"Описание команды addterritoryclan",
				"Описание команды clearterritorylist",
				"Описание команды listterritorymembers" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE,
				AdminCommandHandler.GROUP_SIEGE };
	}
}
