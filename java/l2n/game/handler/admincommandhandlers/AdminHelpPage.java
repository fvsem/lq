package l2n.game.handler.admincommandhandlers;

import javolution.text.TextBuilder;
import javolution.util.FastList;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.util.Files;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

public class AdminHelpPage implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_help", "admin_showhtml" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Admin)
			return false;

		String args = null;

		if(command.startsWith("admin_help"))
		{
			if(command.length() > _adminCommands[0].length() + 1)
				args = command.substring(_adminCommands[0].length()).trim();

			if(args == null || args.equals("groups"))
			{
				final HashMap<String, Vector<String>> map = getGrouppedMap();

				final TextBuilder tb = TextBuilder.newInstance();
				tb.append("<html><body>");
				tb.append("<center>");
				tb.append("<font color=\"LEVEL\">Команды Админыстратора</font>");
				tb.append("</center><br><br>");

				final FastList<String> groups = FastList.newInstance();
				for(final String group : map.keySet())
					groups.add(group);

				Collections.sort(groups);

				for(final String group : groups)
				{
					tb.append("<center>");
					tb.append("<a action=\"bypass -h admin_help group " + group + "\">" + group + "</a>");
					tb.append("</center>");
					tb.append("<br1>");
				}

				tb.append("<br>");
				tb.append("</body></html>");

				final NpcHtmlMessage html = new NpcHtmlMessage(0);
				html.setHtml(tb.toString());
				activeChar.sendPacket(html);
				TextBuilder.recycle(tb);
				FastList.recycle(groups);
			}
			else if(args.startsWith("group"))
			{
				args = args.substring(5).trim();
				final HashMap<String, Vector<String>> map = getGrouppedMap();

				final Vector<String> group = map.get(args);

				if(group == null)
				{
					final NpcHtmlMessage html = new NpcHtmlMessage(0);
					html.setHtml("<html><body>Sorry, but group \"" + args + "\" does not exists</body></html>");
					activeChar.sendPacket(html);
					return false;
				}

				Collections.sort(group);

				final TextBuilder tb = TextBuilder.newInstance();
				tb.append("<html><body>");
				tb.append("<center>");
				tb.append("<font color=\"LEVEL\">Команды Админыстратора</font>");
				tb.append("<br>");
				tb.append("Current group: " + args);
				tb.append("</center><br><br>");

				for(String cmd : group)
				{
					String desk = AdminCommandHandler.getInstance().getDescriptionForCommand(cmd);
					if(desk.startsWith("file://"))
					{
						cmd = cmd.startsWith("admin_") ? cmd.substring(6) : cmd;

						final String[] split = desk.split("\\|");
						if(split.length == 2)
							desk = "<a action=\"bypass -h admin_showhtml help/" + split[0].substring(7) + "\">" + cmd + " - " + split[1] + "</a>";
						else
							desk = "<a action=\"bypass -h admin_showhtml help/" + desk.substring(7) + "\">" + cmd + "</a>";
						tb.append(desk);
					}
					else
					{
						tb.append("<font color=\"LEVEL\">");
						tb.append(cmd.startsWith("admin_") ? cmd.substring(6) : cmd);
						tb.append("</font>");
						tb.append(" - ");
						tb.append(desk);
					}

					tb.append("<br1>");
				}

				tb.append("<center><a action=\"bypass -h admin_help\">Back</a></center>");
				tb.append("</body></html>");

				final NpcHtmlMessage html = new NpcHtmlMessage(0);
				html.setHtml(tb.toString());
				activeChar.sendPacket(html);
				TextBuilder.recycle(tb);
			}
		}
		else if(command.startsWith("admin_showhtml"))
		{
			if(command.length() > _adminCommands[1].length() + 1)
				args = command.substring(_adminCommands[1].length()).trim();

			if(args == null)
			{
				activeChar.sendMessage("Usage: //showhtml <file>");
				return false;
			}

			showHelpPage(activeChar, args);
		}

		return true;
	}

	/**
	 * Создает мап со всем командам/группами.
	 * 
	 * @return отсортированные каманды по группам
	 */
	private HashMap<String, Vector<String>> getGrouppedMap()
	{
		final HashMap<String, Vector<String>> map = new HashMap<String, Vector<String>>();

		for(final String cmd : AdminCommandHandler.getInstance().getAllCommands())
		{
			final String group = AdminCommandHandler.getInstance().getGroupForCommand(cmd);

			Vector<String> groupped = map.get(group);

			if(groupped == null)
			{
				groupped = new Vector<String>();
				map.put(group, groupped);
			}

			groupped.add(cmd);
		}
		return map;
	}

	/**
	 * FIXME: implement method to send html to player in L2Player directly
	 * PUBLIC & STATIC so other classes from package can include it directly
	 */
	public static void showHelpPage(final L2Player targetChar, final String filename)
	{
		String content = Files.read("data/html/admin/" + filename, targetChar);

		if(filename.startsWith("help/")) // Шаблон для Help'а
		{
			final File f = new File(filename);
			String command = f.getName();
			command = command.substring(4, command.length() - 4); // Убираем com_ и .htm

			content = content.replaceFirst("<html><body>", "");
			content = content.replaceFirst("</body></html>", "");

			final IAdminCommandHandler handler = AdminCommandHandler.getInstance().getAdminCommandHandler(command);
			if(handler == null)
				command = "admin_" + command;

			final TextBuilder tb = TextBuilder.newInstance();
			tb.append("<html><body><center>");
			tb.append("<font color=\"LEVEL\">Команды Админыстратора</font>");
			tb.append("<img src=\"L2UI.SquareBlank\" width=260 height=4>");
			tb.append("<img src=\"L2UI.SquareWhite\" width=260 height=1>");
			tb.append("<img src=\"L2UI.SquareBlank\" width=260 height=4></center>");
			tb.append("<br>");
			tb.append("<br>");
			tb.append(content);
			tb.append("<br>");
			tb.append("<br>");
			tb.append("<center><a action=\"bypass -h admin_help group " + AdminCommandHandler.getInstance().getGroupForCommand(command) + "\">Back</a>");
			tb.append("<br1>");
			tb.append("<img src=\"L2UI.SquareBlank\" width=260 height=4>");
			tb.append("<img src=\"L2UI.SquareWhite\" width=260 height=1>");
			tb.append("<img src=\"L2UI.SquareBlank\" width=260 height=4>");
			tb.append("Сервер.<br1> L2Dream.");
			tb.append("</center></body></html>");
			content = tb.toString();
			TextBuilder.recycle(tb);
		}

		if(content == null)
		{
			targetChar.sendMessage("Not found filename: " + filename);
			return;
		}

		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		adminReply.setHtml(content);
		targetChar.sendPacket(adminReply);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Справка по админ командам",
				"Открытие файла из data/html/admin" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_HELP,
				AdminCommandHandler.GROUP_HELP };
	}

	/**
	 * @param activeChar
	 * @param l2Html
	 */
	public static void showHelpHtml(final L2Player targetChar, final String content)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setHtml(content);
		targetChar.sendPacket(adminReply);
	}
}
