package l2n.game.handler.admincommandhandlers;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class AdminDisconnect implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_disconnect",
			"admin_kick" };

	private enum CommandEnum
	{
		admin_disconnect,
		admin_kick
	}

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Kick)
			return false;

		final String[] wordList = command.split(" ");
		CommandEnum cmd;

		try
		{
			cmd = CommandEnum.valueOf(wordList[0]);
		}
		catch(final Exception e)
		{
			return false;
		}
		switch (cmd)
		{
			case admin_disconnect:
			case admin_kick:
				final L2Player player;
				if(wordList.length == 1)
				{
					// Обработка по таргету
					final L2Object target = activeChar.getTarget();
					if(target == null)
					{
						activeChar.sendMessage("Select character or specify player name.");
						break;
					}
					if(!target.isPlayer())
					{
						activeChar.sendPacket(Msg.INCORRECT_TARGET);
						break;
					}
					player = (L2Player) target;
				}
				else
				{
					// Обработка по нику
					player = L2ObjectsStorage.getPlayer(wordList[1]);
					if(player == null)
					{
						activeChar.sendMessage("Character " + player + " not found in game.");
						break;
					}
				}

				if(player.getObjectId() == activeChar.getObjectId())
				{
					activeChar.sendMessage("You can't logout your character.");
					break;
				}

				activeChar.sendMessage("Character " + player.getName() + " disconnected from server.");

				if(player.isInOfflineMode())
					player.setOfflineMode(false);

				player.sendMessage(new CustomMessage("scripts.commands.admin.AdminDisconnect.YoureKickedByGM", player));
				player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_BEEN_DISCONNECTED_FROM_THE_SERVER_PLEASE_LOGIN_AGAIN));
				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						player.logout(false, false, true, true);
					}
				}, 500);
				break;
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_disconnect.htm",
				"file://com_kick.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_BANS,
				AdminCommandHandler.GROUP_BANS };
	}
}
