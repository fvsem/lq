package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Object;
import l2n.game.model.L2World;
import l2n.game.model.L2WorldRegion;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.Earthquake;
import l2n.game.network.serverpackets.StopMove;
import l2n.game.skills.AbnormalEffect;
import l2n.game.tables.SkillTable;

import java.util.StringTokenizer;

public class AdminEffects implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_invis",
			"admin_vis",
			"admin_earthquake",
			"admin_unpara_all",
			"admin_para_all",
			"admin_unpara",
			"admin_para",
			"admin_polyself",
			"admin_unpolyself",
			"admin_changename",
			"admin_gmspeed",
			"admin_invul",
			"admin_setinvul",
			"admin_abnormal" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().GodMode)
			return false;

		if(command.startsWith("admin_gmspeed"))
			try
			{
				final int val = Integer.parseInt(command.substring(14));
				final L2Effect superhaste = activeChar.getEffectList().getFirstEffect(7029);
				final int sh_level = superhaste == null ? 0 : superhaste.getSkill().getLevel();

				if(val == 0)
				{
					if(sh_level != 0)
						activeChar.doCast(SkillTable.getInstance().getInfo(7029, sh_level), activeChar, true); // снимаем еффект
					activeChar.unsetVar("gm_gmspeed");
				}
				else if(val >= 1 && val <= 4)
				{
					if(Config.SAVE_GM_EFFECTS)
						activeChar.setVar("gm_gmspeed", String.valueOf(val));
					if(val != sh_level)
					{
						if(sh_level != 0)
							activeChar.doCast(SkillTable.getInstance().getInfo(7029, sh_level), activeChar, true); // снимаем еффект
						activeChar.doCast(SkillTable.getInstance().getInfo(7029, val), activeChar, true);
					}
				}
				else
					activeChar.sendMessage("Use //gmspeed value = [0...4].");
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("Use //gmspeed value = [0...4].");
			}
			finally
			{
				activeChar.updateEffectIcons();
			}
		else if(command.equalsIgnoreCase("admin_invis") || command.equalsIgnoreCase("admin_vis"))
		{
			// XXX
			if(activeChar.isInvisible())
			{
				activeChar.stopAbnormalEffect(AbnormalEffect.STEALTH);
				activeChar.setInvisible(false);
				activeChar.broadcastUserInfo(true);
				if(activeChar.getPet() != null)
					activeChar.getPet().broadcastCharInfo();
				activeChar.sendMessage("You visible.");
			}
			else
			{
				activeChar.startAbnormalEffect(AbnormalEffect.STEALTH);
				activeChar.setInvisible(true);
				activeChar.sendUserInfo(true);

				activeChar.sendMessage("You invisible.");

				if(activeChar.getCurrentRegion() != null)
					for(final L2WorldRegion neighbor : activeChar.getCurrentRegion().getNeighbors())
						neighbor.removePlayerFromOtherPlayers(activeChar);
			}
		}
		else if(command.equalsIgnoreCase("admin_earthquake"))
			try
			{
				final String val = command.substring(17);
				final StringTokenizer st = new StringTokenizer(val);
				final String val1 = st.nextToken();
				final int intensity = Integer.parseInt(val1);
				final String val2 = st.nextToken();
				final int duration = Integer.parseInt(val2);
				activeChar.broadcastPacket(new Earthquake(activeChar.getLoc(), intensity, duration));
			}
			catch(final Exception e)
			{}
		else if(command.equalsIgnoreCase("admin_para"))
		{
			String type = "1";
			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				type = st.nextToken();
			}
			catch(final Exception e)
			{}
			try
			{
				final L2Object target = activeChar.getTarget();
				L2Character player;
				if(target.isCharacter())
				{
					player = (L2Character) target;
					if(type.equals("1"))
						player.startAbnormalEffect(AbnormalEffect.HOLD_1);
					else
						player.startAbnormalEffect(AbnormalEffect.HOLD_2);
					player.setParalyzed(true);

					player.broadcastPacket(new StopMove(player));
				}
			}
			catch(final Exception e)
			{}
		}
		else if(command.equalsIgnoreCase("admin_unpara"))
			try
			{
				final L2Object target = activeChar.getTarget();
				if(target.isCharacter())
				{
					((L2Character) target).stopAbnormalEffect(AbnormalEffect.HOLD_1);
					((L2Character) target).stopAbnormalEffect(AbnormalEffect.HOLD_2);
					((L2Character) target).setParalyzed(false);
				}
			}
			catch(final Exception e)
			{}
		else if(command.equalsIgnoreCase("admin_para_all"))
			try
			{
				final GArray<L2Player> players = activeChar.getAroundPlayers(1250);
				for(final L2Player player : players)
					if(!player.isGM())
					{
						player.startAbnormalEffect(AbnormalEffect.HOLD_2);
						player.setParalyzed(true);
						player.broadcastPacket(new StopMove(player));
					}
			}
			catch(final Exception e)
			{}
		else if(command.equalsIgnoreCase("admin_unpara_all"))
			try
			{
				for(final L2Player player : activeChar.getAroundPlayers(2500))
				{
					player.stopAbnormalEffect(AbnormalEffect.HOLD_1);
					player.stopAbnormalEffect(AbnormalEffect.HOLD_2);
					player.setParalyzed(false);
				}
			}
			catch(final Exception e)
			{}
		else if(command.equalsIgnoreCase("admin_polyself"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				final String id = st.nextToken();
				activeChar.setPolyInfo("npc", id);
				activeChar.teleToLocation(activeChar.getLoc());
				activeChar.broadcastUserInfo(true);
			}
			catch(final Exception e)
			{}
		}
		else if(command.equalsIgnoreCase("admin_unpolyself"))
		{
			activeChar.setPolyInfo(null, "1");
			activeChar.decayMe();
			activeChar.spawnMe(activeChar.getLoc());
			activeChar.broadcastUserInfo(true);
		}
		else if(command.equalsIgnoreCase("admin_changename"))
			try
			{
				final String name = command.substring(17);
				String oldName = "null";
				try
				{
					final L2Object target = activeChar.getTarget();
					L2Character player = null;
					if(target == null)
					{
						player = activeChar;
						oldName = activeChar.getName();
					}
					else if(target.isCharacter())
					{
						player = (L2Character) target;
						oldName = player.getName();
					}
					else
						return false;
					if(player.isPlayer())
					{
						L2World.removeObject(player);
						player.decayMe();
					}
					player.setName(name);
					if(player.isPlayer())
						player.spawnMe();

					player.broadcastCharInfo();
					activeChar.sendMessage("Changed name from " + oldName + " to " + name + ".");
				}
				catch(final Exception e)
				{}
			}
			catch(final StringIndexOutOfBoundsException e)
			{}
		else if(command.equalsIgnoreCase("admin_invul"))
		{
			handleInvul(activeChar, activeChar);
			if(activeChar.isInvul())
			{
				if(Config.SAVE_GM_EFFECTS)
					activeChar.setVar("gm_invul", "true");
			}
			else
				activeChar.unsetVar("gm_invul");

		}
		else if(command.equalsIgnoreCase("admin_setinvul"))
		{
			final L2Object target = activeChar.getTarget();
			if(target.isPlayer())
				handleInvul(activeChar, (L2Player) target);
		}
		else if(command.startsWith("admin_abnormal"))
		{
			final String[] wordList = command.split(" ");
			AbnormalEffect ae = AbnormalEffect.NULL;
			final L2Object target = activeChar.getTarget();
			try
			{
				ae = AbnormalEffect.getByName(wordList[1]);
			}
			catch(final Exception e)
			{
				activeChar.sendMessage("USAGE: //abnormal name");
				activeChar.sendMessage("//abnormal - Clears all abnormal effects");
				return false;
			}

			final L2Character effectTarget = target == null ? activeChar : (L2Character) target;

			if(ae == AbnormalEffect.NULL)
			{
				effectTarget.startAbnormalEffect(AbnormalEffect.NULL);
				effectTarget.sendMessage("Abnormal effects clearned by admin.");
				if(effectTarget != activeChar)
					effectTarget.sendMessage("Abnormal effects clearned.");
			}
			else
			{
				effectTarget.startAbnormalEffect(ae);
				effectTarget.sendMessage("Admin added abnormal effect: " + ae.getName());
				if(effectTarget != activeChar)
					effectTarget.sendMessage("Aadded abnormal effect: " + ae.getName());
			}
		}

		return true;
	}

	private void handleInvul(final L2Player activeChar, final L2Player target)
	{
		if(target.isInvul())
		{
			target.setInvul(false);
			target.stopAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
			if(target.getPet() != null)
			{
				target.getPet().setInvul(false);
				target.getPet().stopAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
			}
			activeChar.sendMessage(target.getName() + " is now mortal.");
		}
		else
		{
			target.setInvul(true);
			target.startAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
			if(target.getPet() != null)
			{
				target.getPet().setInvul(true);
				target.getPet().startAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
			}
			activeChar.sendMessage(target.getName() + " is now immortal.");
		}
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды invis",
				"Описание команды vis",
				"Описание команды earthquake",
				"Описание команды unpara_all",
				"Описание команды para_all",
				"Описание команды unpara",
				"Описание команды para",
				"Описание команды polyself",
				"Описание команды unpolyself",
				"Описание команды changename",
				"Описание команды gmspeed",
				"Описание команды invul",
				"Описание команды setinvul",
				"Описание команды abnormal" };

	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2,
				AdminCommandHandler.GROUP_MANAGESERVER2 };
	}
}
