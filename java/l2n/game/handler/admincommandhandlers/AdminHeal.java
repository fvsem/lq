package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

public class AdminHeal implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_heal" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().HealKillRes)
			return false;
		if(command.equals("admin_heal"))
			handleRes(activeChar);
		else if(command.startsWith("admin_heal"))
			try
			{
				final String healTarget = command.substring(11);
				handleRes(activeChar, healTarget);
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Incorrect target/radius specified.");
			}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleRes(final L2Player activeChar)
	{
		handleRes(activeChar, null);
	}

	private void handleRes(final L2Player activeChar, final String player)
	{

		L2Object obj = activeChar.getTarget();
		if(player != null)
		{
			final L2Player plyr = L2ObjectsStorage.getPlayer(player);

			if(plyr != null)
				obj = plyr;
			else
			{
				final int radius = Math.max(Integer.parseInt(player), 100);
				for(final L2Character character : activeChar.getAroundCharacters(radius))
				{
					character.setCurrentHpMp(character.getMaxHp(), character.getMaxMp(), true);
					if(character.isPlayer())
						character.setCurrentCp(character.getMaxCp());
				}
				activeChar.sendMessage("Healed within " + radius + " unit radius.");
				return;
			}
		}

		if(obj == null)
			obj = activeChar;

		if(obj.isCharacter())
		{
			final L2Character target = (L2Character) obj;
			target.setCurrentHpMp(target.getMaxHp(), target.getMaxMp());
			if(target.isPlayer())
				target.setCurrentCp(target.getMaxCp());
			Log.add("heal character " + target.getName(), "gm_ext_actions", activeChar);
		}
		else
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "Описание команды heal" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_EDITCHAR };
	}
}
