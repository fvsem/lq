package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.FenceBuilderManager;
import l2n.game.model.actor.L2Player;

public class AdminFence implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_fence",
			"admin_fbuilder",
			"admin_delallspawned",
			"admin_dellastspawned" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.startsWith("admin_fence"))
		{
			final String[] args = command.split(" ");
			if(args.length < 5)
			{
				activeChar.sendMessage("Usage: //fence <type> <width> <length> <height>");
				return false;
			}
			FenceBuilderManager.spawn_fence(activeChar, Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
		}
		else if(command.equals("admin_delallspawned"))
			FenceBuilderManager.del_all(activeChar);
		else if(command.equals("admin_dellastspawned"))
			FenceBuilderManager.del_last(activeChar);
		else if(command.equals("admin_fbuilder"))
			FenceBuilderManager.main_fence(activeChar);
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды fence",
				"Описание команды fbuilder",
				"Описание команды delallspawned",
				"Описание команды dellastspawned" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER,
				AdminCommandHandler.GROUP_MANAGESERVER };
	}
}
