package l2n.game.handler.admincommandhandlers;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeShowInfoUpdate;
import l2n.game.network.serverpackets.PledgeStatusChanged;
import l2n.game.network.serverpackets.UserInfo;
import l2n.game.tables.ClanTable;

import java.util.StringTokenizer;

public class AdminPledge implements IAdminCommandHandler
{
	private final static String[] _adminCommands = { "admin_pledge" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar.getPlayerAccess() == null || !activeChar.getPlayerAccess().Clan || activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
			return false;

		final L2Player target = (L2Player) activeChar.getTarget();

		if(command.startsWith("admin_pledge"))
		{
			final StringTokenizer st = new StringTokenizer(command);
			st.nextToken();

			final String action = st.nextToken(); // create|dismiss|setlevel|resetcreate|resetwait|addrep|setrep

			if(action.equals("create"))
				try
				{
					final String pledgeName = st.nextToken();
					final L2Clan clan = ClanTable.getInstance().createClan(target, pledgeName);
					if(clan != null)
					{
						target.sendPacket(new PledgeShowInfoUpdate(clan));
						target.sendPacket(new UserInfo(target));
						target.sendPacket(Msg.CLAN_HAS_BEEN_CREATED);
						return true;
					}
				}
				catch(final Exception e)
				{}
			else if(action.equals("dismiss"))
			{
				if(target.getClan() == null || target.getObjectId() != target.getClan().getLeaderId())
				{
					activeChar.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
					return false;
				}

				SiegeManager.removeSiegeSkills(target);
				for(final L2Player clanMember : target.getClan().getOnlineMembers(null))
				{
					clanMember.setClan(null);
					clanMember.setTitle(null);
					clanMember.sendPacket(Msg.CLAN_HAS_DISPERSED);
					clanMember.broadcastUserInfo(true);
				}

				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("UPDATE characters SET clanid = 0 WHERE clanid=?");
					statement.setInt(1, target.getClanId());
					statement.execute();
					DbUtils.close(statement);

					statement = con.prepareStatement("DELETE FROM clan_data WHERE clan_id=?");
					statement.setInt(1, target.getClanId());
					statement.execute();
					DbUtils.close(statement);
					statement = null;
					target.sendPacket(Msg.CLAN_HAS_DISPERSED);
					target.broadcastUserInfo(true);
				}
				catch(final Exception e)
				{}
				finally
				{
					DbUtils.closeQuietly(con, statement);
				}
				return true;
			}
			else if(action.equals("setlevel"))
			{
				if(target.getClan() == null || target.getObjectId() != target.getClan().getLeaderId())
				{
					activeChar.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
					return false;
				}

				try
				{
					final byte level = Byte.parseByte(st.nextToken());
					final L2Clan clan = target.getClan();

					activeChar.sendMessage("You set level " + level + " for clan " + clan.getName());
					clan.setLevel(level);
					clan.updateClanInDB();

					if(level < 4)
						SiegeManager.removeSiegeSkills(target);
					else if(level > 3)
						SiegeManager.addSiegeSkills(target);

					if(level == 5)
						target.sendPacket(Msg.NOW_THAT_YOUR_CLAN_LEVEL_IS_ABOVE_LEVEL_5_IT_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS);

					final PledgeShowInfoUpdate pu = new PledgeShowInfoUpdate(clan);
					final PledgeStatusChanged ps = new PledgeStatusChanged(clan);

					for(final L2Player member : clan.getOnlineMembers(null))
					{
						member.updatePledgeClass();
						member.sendPacket(Msg.CLANS_SKILL_LEVEL_HAS_INCREASED);
						member.sendPacket(pu);
						member.sendPacket(ps);
						member.broadcastUserInfo(true);
					}
					return true;
				}
				catch(final Exception e)
				{}
			}
			else if(action.equals("resetcreate"))
			{
				if(target.getClan() == null)
				{
					activeChar.sendPacket(Msg.INCORRECT_TARGET);
					return false;
				}
				target.getClan().setExpelledMemberTime(0);
				activeChar.sendMessage("The penalty for creating a clan has been lifted for" + target.getName());
			}
			else if(action.equals("resetwait"))
			{
				target.setLeaveClanTime(0);
				activeChar.sendMessage("The penalty for leaving a clan has been lifted for " + target.getName());
			}
			else if(action.equals("addrep"))
			{
				try
				{
					final int rep = Integer.parseInt(st.nextToken());
					if(target.getClan() == null || target.getClan().getLevel() < 5)
					{
						activeChar.sendPacket(Msg.INCORRECT_TARGET);
						return false;
					}
					target.getClan().incReputation(rep, false, "admin_manual");
					activeChar.sendMessage("Added " + rep + " clan points to clan " + target.getClan().getName() + ".");
				}
				catch(final NumberFormatException nfe)
				{
					activeChar.sendMessage("Please specify a number of clan points to add.");
				}
			}
			else if(action.equals("setrep"))
			{
				try
				{
					final int rep = Integer.parseInt(st.nextToken());
					if(target.getClan() == null || target.getClan().getLevel() < 5)
					{
						activeChar.sendPacket(Msg.INCORRECT_TARGET);
						return false;
					}
					target.getClan().setReputationScore(rep);
					activeChar.sendMessage("Set " + rep + " clan points to clan " + target.getClan().getName() + ".");
				}
				catch(final NumberFormatException nfe)
				{
					activeChar.sendMessage("Please specify a number of clan points to set.");
				}
			}
		}

		return false;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "file://com_pledge.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER4 };
	}
}
