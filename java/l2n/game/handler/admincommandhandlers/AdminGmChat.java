package l2n.game.handler.admincommandhandlers;

import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.tables.GmListTable;

public class AdminGmChat implements IAdminCommandHandler
{
	private final static String[] _adminCommands = { "admin_gmchat", "admin_snoop" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.startsWith("admin_gmchat"))
			handleGmChat(command, activeChar);
		else if(command.startsWith("admin_snoop"))
			snoop(command, activeChar);
		return true;
	}

	private void snoop(final String command, final L2Player activeChar)
	{
		final L2Object target = activeChar.getTarget();
		if(target == null)
		{
			activeChar.sendMessage("Выберете правельную цель.");
			return;
		}
		if(!target.isPlayer())
		{
			activeChar.sendMessage("Цель должна быть игроком.");
			return;
		}
		final L2Player player = (L2Player) target;
		player.addSnooper(activeChar.getStoredId());
		activeChar.addSnooped(player.getStoredId());
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void handleGmChat(final String command, final L2Player activeChar)
	{
		try
		{
			final String text = command.replaceFirst("admin_gmchat", "");
			final CreatureSay cs = new CreatureSay(activeChar.getObjectId(), Say2C.GM, activeChar.getName(), text);
			GmListTable.broadcastToGMs(cs);
		}
		catch(final StringIndexOutOfBoundsException e)
		{
			// empty message.. ignore
		}
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_gmchat", "admin_snoop" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] { AdminCommandHandler.GROUP_MANAGESERVER3, AdminCommandHandler.GROUP_EDITCHAR };
	}
}
