package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;

public class AdminRideWyvern implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_ride_wyvern",
			"admin_ride_strider",
			"admin_unride_wyvern",
			"admin_unride_strider",
			"admin_unride",
			"admin_wr",
			"admin_sr",
			"admin_ur" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().Rider)
			return false;

		if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		final L2Player target = (L2Player) activeChar.getTarget();

		if(command.startsWith("admin_ride_wyvern") || command.startsWith("admin_wr"))
		{
			if(activeChar.isMounted() || activeChar.getPet() != null)
			{
				if(target.getVar("lang@").equalsIgnoreCase("en"))
					activeChar.sendMessage("Target already Have a Pet or Mounted.");
				else
					activeChar.sendMessage("Цель уже имеет пета, или находится верхом.");
				return false;
			}
			else if(target == null)
				activeChar.setMount(12621, 0);
			else
				target.setMount(12621, 0);
		}
		else if(command.startsWith("admin_ride_strider") || command.startsWith("admin_sr"))
		{
			if(activeChar.isMounted() || activeChar.getPet() != null)
			{
				if(target.getVar("lang@").equalsIgnoreCase("en"))
					activeChar.sendMessage("Target already Have a Pet or Mounted.");
				else
					activeChar.sendMessage("Цель уже имеет пета, или находится верхом.");
				return false;
			}
			else if(target == null)
				activeChar.setMount(12526, 0);
			else
				target.setMount(12526, 0);
		}
		else if(command.startsWith("admin_unride") || command.startsWith("admin_ur"))
			if(target == null)
				activeChar.setMount(0, 0);
			else
				target.setMount(0, 0);

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"Описание команды ride_wyvern",
				"Описание команды ride_strider",
				"Описание команды unride_wyvern",
				"Описание команды unride_strider",
				"Описание команды unride",
				"Описание команды wr",
				"Описание команды sr",
				"Описание команды ur" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
