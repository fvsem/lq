package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.instancemanager.AuctionManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Object;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Auction.Auction;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.ClanTable;

import java.util.StringTokenizer;

public class AdminClanHall implements IAdminCommandHandler
{
	private static String[] _adminCommands = {
			"admin_clanhall",
			"admin_clanhallset",
			"admin_clanhalldel",
			"admin_clanhallopendoors",
			"admin_clanhallclosedoors",
			"admin_clanhallteleportself" };

	@Override
	public boolean useAdminCommand(String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().ClanHall)
			return false;

		final StringTokenizer st = new StringTokenizer(command, " ");
		command = st.nextToken();

		ClanHall clanhall = null;
		if(command.startsWith("admin_clanhall") && st.hasMoreTokens())
			clanhall = ClanHallManager.getInstance().getClanHall(Integer.parseInt(st.nextToken()));

		if(clanhall == null)
			showClanHallSelectPage(activeChar);
		else
		{
			final L2Object target = activeChar.getTarget();
			L2Player player = activeChar;
			if(target != null && target.isPlayer())
				player = (L2Player) target;
			else
				player = activeChar;

			if(command.equalsIgnoreCase("admin_clanhallset"))
			{
				if(player.getClan() == null)
					activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
				else
				{
					clanhall.changeOwner(player.getClan());
					final Auction auction = AuctionManager.getInstance().getAuction(clanhall.getId());
					if(auction != null)
						auction.cancelAuction();
					System.out.println("ClanHall " + clanhall.getName() + "(id: " + clanhall.getId() + ") owned by clan " + player.getClan().getName());
				}
			}
			else if(command.equalsIgnoreCase("admin_clanhalldel"))
				clanhall.changeOwner(null);
			else if(command.equalsIgnoreCase("admin_clanhallopendoors"))
				clanhall.openCloseDoors(activeChar, true);
			else if(command.equalsIgnoreCase("admin_clanhallclosedoors"))
				clanhall.openCloseDoors(activeChar, false);
			else if(command.equalsIgnoreCase("admin_clanhallteleportself"))
			{
				final L2Zone zone = clanhall.getZone();
				if(zone != null)
					activeChar.teleToLocation(zone.getSpawn());
			}
			showClanHallPage(activeChar, clanhall);
		}

		return true;
	}

	public void showClanHallSelectPage(final L2Player activeChar)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);

		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=268><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center><font color=\"LEVEL\">Clan Halls:</font></center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table><br>");

		replyMSG.append("<table width=268>");
		replyMSG.append("<tr><td width=130>ClanHall Name</td><td width=58>Town</td><td width=80>Owner</td></tr>");

		// TODO: make sort by Location
		for(final ClanHall clanhall : ClanHallManager.getInstance().getClanHalls().values())
			if(clanhall != null)
			{
				replyMSG.append("<tr><td>");
				replyMSG.append("<a action=\"bypass -h admin_clanhall " + clanhall.getId() + "\">" + clanhall.getName() + "</a>");
				replyMSG.append("</td><td>" + clanhall.getLocation() + "</td><td>");

				final L2Clan owner = clanhall.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(clanhall.getOwnerId());
				if(owner == null)
					replyMSG.append("none");
				else
					replyMSG.append(owner.getName());

				replyMSG.append("</td></tr>");
			}

		replyMSG.append("</table>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	public void showClanHallPage(final L2Player activeChar, final ClanHall clanhall)
	{
		final NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		final StringBuffer replyMSG = new StringBuffer("<html><body>");
		replyMSG.append("<table width=260><tr>");
		replyMSG.append("<td width=40><button value=\"Main\" action=\"bypass -h admin_admin\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td width=180><center>ClanHall Name</center></td>");
		replyMSG.append("<td width=40><button value=\"Back\" action=\"bypass -h admin_clanhall\" width=40 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("</tr></table>");
		replyMSG.append("<center>");
		replyMSG.append("<br><br><br>ClanHall: " + clanhall.getName() + "<br>");
		replyMSG.append("Location: " + clanhall.getLocation() + "<br>");
		replyMSG.append("ClanHall Owner: ");
		final L2Clan owner = clanhall.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(clanhall.getOwnerId());
		if(owner == null)
			replyMSG.append("none");
		else
			replyMSG.append(owner.getName());

		replyMSG.append("<br><br><br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Open Doors\" action=\"bypass -h admin_clanhallopendoors " + clanhall.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Close Doors\" action=\"bypass -h admin_clanhallclosedoors " + clanhall.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table>");
		replyMSG.append("<tr><td><button value=\"Give ClanHall\" action=\"bypass -h admin_clanhallset " + clanhall.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
		replyMSG.append("<td><button value=\"Take ClanHall\" action=\"bypass -h admin_clanhalldel " + clanhall.getId() + "\" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("<br>");
		replyMSG.append("<table><tr>");
		replyMSG.append("<td><button value=\"Teleport self\" action=\"bypass -h admin_clanhallteleportself " + clanhall.getId() + " \" width=80 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		replyMSG.append("</table>");
		replyMSG.append("</center>");
		replyMSG.append("</body></html>");

		adminReply.setHtml(replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"file://com_clanhall.htm",
				"file://com_clanhallset.htm",
				"file://com_clanhalldel.htm",
				"file://com_clanhallopendoors.htm",
				"file://com_clanhallclosedoors.htm",
				"file://com_clanhallteleportself.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1 };
	}
}
