package l2n.game.handler.admincommandhandlers;

import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

import java.util.StringTokenizer;

public class AdminVitLevel implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS = {
			"admin_addvitLevel",
			"admin_setvitLevel" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(activeChar == null)
			return false;

		if(!activeChar.getPlayerAccess().EditChar)
			return false;

		if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
		{
			activeChar.sendPacket(Msg.INCORRECT_TARGET);
			return false;
		}

		final L2Object target = activeChar.getTarget();
		L2Player player = null;
		if(target != null && target.isPlayer())
			player = (L2Player) target;
		else
			return false;

		final StringTokenizer st = new StringTokenizer(command, " ");
		final String actualCommand = st.nextToken();

		String val = "";
		if(st.countTokens() >= 1)
			val = st.nextToken();

		if(actualCommand.equalsIgnoreCase("admin_addvitLevel"))
			try
			{
				final Integer level = player.getVitalityLevel() + Integer.parseInt(val);
				if(player != null)
					player.setVitalityLevel(level, true);

				player.sendMessage("GM add your vit level");
				Log.add("add vit level for player " + player.getName() + " to " + val, "gm_ext_actions", activeChar);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Wrong Number Format");
				return false;
			}
		if(actualCommand.equalsIgnoreCase("admin_setvitLevel"))
			try
			{
				final int level = Integer.parseInt(val);
				if(player != null)
					player.setVitalityLevel(level, true);

				player.sendMessage("GM set your vit level");
				Log.add("set vit level for player " + player.getName() + " to " + val, "gm_ext_actions", activeChar);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Wrong Number Format");
				return false;
			}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}

	@Override
	public String[] getDescription()
	{
		return new String[] { "admin_addvitLevel", "admin_setvitLevel" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_EDITCHAR,
				AdminCommandHandler.GROUP_EDITCHAR };
	}
}
