package l2n.game.handler.admincommandhandlers;

import l2n.Config;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Hero;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.olympiad.OlympiadDatabase;
import l2n.game.model.entity.olympiad.OlympiadManager;
import l2n.game.model.entity.olympiad.ValidationTask;
import l2n.util.Log;

public class AdminOlympiad implements IAdminCommandHandler
{
	private static String[] _adminCommands = { "admin_oly_start", "admin_oly_stop", "admin_oly_save", "admin_manualhero", "admin_add_oly_points", "admin_oly_clearhwid" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(command.startsWith("admin_oly_start"))
		{
			Olympiad._manager = new OlympiadManager();
			Olympiad._inCompPeriod = true;

			new Thread(Olympiad._manager).start();

			Announcements.announceToAll(Msg.THE_OLYMPIAD_GAME_HAS_STARTED);
			Olympiad._log.info("Olympiad System: manual olympiad game started");
		}
		if(command.equalsIgnoreCase("admin_oly_clearhwid"))
		{
			Olympiad.clearHWID();
			activeChar.sendMessage("olympaid hwid cleared.");
			Olympiad._log.info("Olympiad System: hwid cleared");
		}
		if(command.startsWith("admin_oly_stop"))
		{
			Olympiad._inCompPeriod = false;
			Announcements.announceToAll(Msg.THE_OLYMPIAD_GAME_HAS_ENDED);
			Olympiad._log.info("Olympiad System: Olympiad Game Ended");
			try
			{
				OlympiadDatabase.save();
			}
			catch(final Exception e)
			{
				Olympiad._log.warning("Olympiad System: Failed to save Olympiad configuration:");
				e.printStackTrace();
			}
		}
		else if(command.startsWith("admin_oly_save"))
		{
			if(!Config.ENABLE_OLYMPIAD)
				return false;

			try
			{
				OlympiadDatabase.save();
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			activeChar.sendMessage("olympaid data saved.");
		}
		else if(command.startsWith("admin_manualhero"))
		{
			if(!Config.ENABLE_OLYMPIAD)
				return false;

			try
			{
				Hero.getInstance().clearHeroes();
				L2GameThreadPools.getInstance().scheduleGeneral(new ValidationTask(), 1000);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			activeChar.sendMessage("Heroes formed.");
		}
		else if(command.startsWith("admin_add_oly_points"))
		{
			final String[] wordList = command.split(" ");
			if(wordList.length < 3)
			{
				activeChar.sendMessage("Command syntax: //add_oly_points <char_name> <point_to_add>");
				activeChar.sendMessage("This command can be applied only for online players.");
				return false;
			}

			final L2Player player = L2ObjectsStorage.getPlayer(wordList[1]);
			if(player == null)
			{
				activeChar.sendMessage("Character " + wordList[1] + " not found in game.");
				return false;
			}

			int pointToAdd;

			try
			{
				pointToAdd = Integer.parseInt(wordList[2]);
			}
			catch(final NumberFormatException e)
			{
				activeChar.sendMessage("Please specify integer value for olympiad points.");
				return false;
			}

			final int curPoints = Olympiad.getNoblePoints(player.getObjectId());
			Olympiad.manualSetNoblePoints(player.getObjectId(), curPoints + pointToAdd);
			final int newPoints = Olympiad.getNoblePoints(player.getObjectId());

			activeChar.sendMessage("Added " + pointToAdd + " points to character " + player.getName());
			activeChar.sendMessage("Old points: " + curPoints + ", new points: " + newPoints);

			Log.add("add olympiad points to player " + player.getName(), "gm_ext_actions", activeChar);
		}

		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	@Override
	public String[] getDescription()
	{
		return new String[]
		{
				"Описание команды oly_start",
				"Описание команды oly_stop",
				"Описание команды oly_save",
				"выдать геройство вручную",
				"Описание команды add_oly_points",
				"отчистка HWID участников"
		};
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[]
		{
				AdminCommandHandler.GROUP_OLYMPIAD,
				AdminCommandHandler.GROUP_OLYMPIAD,
				AdminCommandHandler.GROUP_OLYMPIAD,
				AdminCommandHandler.GROUP_OLYMPIAD,
				AdminCommandHandler.GROUP_OLYMPIAD,
				AdminCommandHandler.GROUP_OLYMPIAD
		};
	}
}
