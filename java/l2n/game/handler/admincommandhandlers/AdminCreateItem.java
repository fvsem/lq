package l2n.game.handler.admincommandhandlers;

import gnu.trove.list.array.TIntArrayList;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;

import java.util.StringTokenizer;

public class AdminCreateItem implements IAdminCommandHandler
{
	private final static String[] _adminCommands = {
			"admin_give_item_target",
			"admin_give_item_party",
			"admin_give_item_cc",
			"admin_give_item_all",
			"admin_itemcreate",
			"admin_create_item",
			"admin_summon",
			"admin_clear_inventory" };

	@Override
	public boolean useAdminCommand(final String command, final L2Player activeChar)
	{
		if(!activeChar.getPlayerAccess().CreateItem)
			return false;

		if(command.equals("admin_itemcreate"))
			AdminHelpPage.showHelpPage(activeChar, "itemcreation.htm");
		else if(command.startsWith("admin_create_item"))
		{
			try
			{
				final String val = command.substring(17);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					final int idval = Integer.parseInt(id);
					final String num = st.nextToken();
					final long numval = Long.parseLong(num);
					createItem(activeChar, idval, numval);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					final int idval = Integer.parseInt(id);
					createItem(activeChar, idval, 1);
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Не возможно создать вещ.");
			}
			AdminHelpPage.showHelpPage(activeChar, "itemcreation.htm");
		}
		else if(command.startsWith("admin_summon"))
			try
			{
				final String val = command.substring(12);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					final int idval = Integer.parseInt(id);
					final String num = st.nextToken();
					final long numval = Long.parseLong(num);
					createItem(activeChar, idval, numval);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					final int idval = Integer.parseInt(id);
					createItem(activeChar, idval, 1);
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
			}
			catch(final StringIndexOutOfBoundsException e)
			{
				activeChar.sendMessage("Не возможно создать вещ.");
			}
		else if(command.equals("admin_clear_inventory"))
		{
			if(activeChar.getTarget().isPlayer())
				removeAllItems((L2Player) activeChar.getTarget());
			else
				removeAllItems(activeChar);
		}
		else if(command.startsWith("admin_give_item_target"))
		{
			if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
				return false;

			final L2Player target = (L2Player) activeChar.getTarget();

			int itemId = 0;
			long count = 0;
			try
			{
				final String val = command.substring(22);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					final String num = st.nextToken();
					count = Long.parseLong(num);
					createItem(target, itemId, count);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					count = 1;
					createItem(target, itemId, count);
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
				return false;
			}
		}
		else if(command.startsWith("admin_give_item_party"))
		{
			if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
				return false;

			final L2Player target = (L2Player) activeChar.getTarget();
			if(target.getParty() == null)
				return false;

			int itemId = 0;
			long count = 0;
			try
			{
				final String val = command.substring(21);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					final String num = st.nextToken();
					count = Long.parseLong(num);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					count = 1;
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
				return false;
			}

			addItem(activeChar, target.getParty().getPartyMembers(), itemId, count, 147);
		}
		else if(command.startsWith("admin_give_item_cc"))
		{
			if(activeChar.getTarget() == null || !activeChar.getTarget().isPlayer())
				return false;

			final L2Player target = (L2Player) activeChar.getTarget();
			if(target.getParty() == null || target.getParty().getCommandChannel() == null)
				return false;

			int itemId = 0;
			long count = 0;
			try
			{
				final String val = command.substring(18);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					final String num = st.nextToken();
					count = Long.parseLong(num);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					count = 1;
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
				return false;
			}

			addItem(activeChar, target.getParty().getCommandChannel().getMembers(), itemId, count, 184);
		}
		else if(command.startsWith("admin_give_item_all"))
		{
			int itemId = 0;
			long count = 0;
			try
			{
				final String val = command.substring(19);
				final StringTokenizer st = new StringTokenizer(val);
				if(st.countTokens() == 2)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					final String num = st.nextToken();
					count = Long.parseLong(num);
				}
				else if(st.countTokens() == 1)
				{
					final String id = st.nextToken();
					itemId = Integer.parseInt(id);
					count = 1;
				}
			}
			catch(final NumberFormatException nfe)
			{
				activeChar.sendMessage("Определите правельный номер");
				return false;
			}

			addItem(activeChar, L2ObjectsStorage.getAllPlayersForIterate(), itemId, count, 216);
		}
		return true;
	}

	@Override
	public String[] getAdminCommandList()
	{
		return _adminCommands;
	}

	private void addItem(final L2Player admin, final Iterable<L2Player> list, final int itemId, final long count, final int logId)
	{
		if(admin.getPlayerAccess().ProhibitedItems.contains(new Integer(itemId)))
		{
			Log.add("try spawned " + count + " prohibited item(s) number " + itemId + " in inventory", "gm_ext_actions", admin);
			return;
		}

		final TIntArrayList iplist = new TIntArrayList();
		for(final L2Player player : list)
			if(player != null && !player.isInOfflineMode() && !iplist.contains(player.getIP().hashCode()))
			{
				iplist.add(player.getIP().hashCode());
				player.addItem(itemId, count, admin.getObjectId(), "AdminCreateItem.admin_give-" + logId);
				player.sendPacket(new ItemList(player, true));
				player.sendPacket(SystemMessage.obtainItems(itemId, count, 0));
			}
	}

	private void createItem(final L2Player activeChar, final int id, final long num)
	{
		if(activeChar.getPlayerAccess().ProhibitedItems.contains(new Integer(id)))
		{
			Log.add("try spawned " + num + " prohibited item(s) number " + id + " in inventory", "gm_ext_actions", activeChar);
			return;
		}

		if(!ItemTable.getInstance().isExists(id))
		{
			activeChar.sendMessage("Not found template for item id: " + id);
			return;
		}

		L2ItemInstance createditem = ItemTable.getInstance().createItem(id, activeChar.getObjectId(), 0, "AdminCreateItem.createItem[74]");
		createditem.setCount(num);
		activeChar.getInventory().addItem(createditem);
		Log.LogItem(activeChar, Log.Adm_AddItem, createditem);
		if(!createditem.isStackable())
			for(long i = 0; i < num - 1; i++)
			{
				createditem = ItemTable.getInstance().createItem(id, activeChar.getObjectId(), 0, "AdminCreateItem.createItem[74]");
				activeChar.getInventory().addItem(createditem);
				Log.LogItem(activeChar, Log.Adm_AddItem, createditem);
			}
		activeChar.sendPacket(new ItemList(activeChar, true));
		activeChar.sendPacket(SystemMessage.obtainItems(id, num, 0));
		Log.add("spawned " + num + " item(s) number " + id + " in inventory", "gm_ext_actions", activeChar);
	}

	private void removeAllItems(final L2Player activeChar)
	{
		for(final L2ItemInstance item : activeChar.getInventory().getItems())
			if(item.getLocation() == L2ItemInstance.ItemLocation.INVENTORY)
				activeChar.getInventory().destroyItem(item.getObjectId(), item.getCount(), true);
		activeChar.sendPacket(new ItemList(activeChar, false));
	}

	@Override
	public String[] getDescription()
	{
		return new String[] {
				"<item_id> [count] выдаёт предмет выделенному игроку",
				"<item_id> [count] выдаёт предмет всем членам группы выделенного игрока",
				"<item_id> [count] выдаёт предмет всем членам командного канала выделенного игрока",
				"<item_id> [count] выдаёт предмет всем игрокам в мире",
				"file://com_itemcreate.htm",
				"file://com_create_item.htm",
				"Описание команды",
				"file://com_clear_inventory.htm" };
	}

	@Override
	public String[] getCommandGroup()
	{
		return new String[] {
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1,
				AdminCommandHandler.GROUP_MANAGESERVER1 };
	}
}
