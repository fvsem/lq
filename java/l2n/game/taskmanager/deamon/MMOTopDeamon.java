package l2n.game.taskmanager.deamon;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 
 * @date 10.04.2011
 * @time 3:34:43
 */
public class MMOTopDeamon
{
	private static final Logger _log = Logger.getLogger(MMOTopDeamon.class.getName());

	private static BufferedReader reader;
	private static ScheduledFuture<AutoReward> checkTask = null;

	private static MMOTopDeamon _instance;

	public static MMOTopDeamon getInstance()
	{
		if(_instance == null)
			_instance = new MMOTopDeamon();
		return _instance;
	}

	private MMOTopDeamon()
	{
		if(Config.MMOTOP_DEMON_ENABLED)
		{
			if(checkTask != null)
			{
				checkTask.cancel(true);
				checkTask = null;
			}
			checkTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new AutoReward(), 60000, Config.MMOTOP_DEMON_POLLINTERVAL * 60000);
		}
	}

	private class AutoReward implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				getPage(Config.MMOTOP_DEMON_URL);
				parse();
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "MMOTopDeamon: error to get data from URL: " + e.getMessage());
			}
		}
	}

	private void getPage(String address)
	{
		try
		{
			URL url = new URL(address);
			reader = new BufferedReader(new InputStreamReader(url.openStream(), "windows-1251"));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "MMOTopDeamon: error to getPage: " + e.getMessage());
		}
	}

	private void parse()
	{
		try
		{
			String line;
			while ((line = reader.readLine()) != null)
			{
				StringTokenizer st = new StringTokenizer(line, "\t. :");
				while (st.hasMoreTokens())
				{
					try
					{
						// 105756 09.04.2011 23:34:22 217.66.152.42 Нежданчик 1
						st.nextToken();
						int day = Integer.parseInt(st.nextToken());
						int month = Integer.parseInt(st.nextToken());
						int year = Integer.parseInt(st.nextToken());
						int hour = Integer.parseInt(st.nextToken());
						int minute = Integer.parseInt(st.nextToken());
						int second = Integer.parseInt(st.nextToken());

						st.nextToken();
						st.nextToken();
						st.nextToken();
						st.nextToken();
						String name = st.nextToken();

						L2Player player;
						if((player = L2ObjectsStorage.getPlayer(name)) != null && player.isOnline())
						{
							Calendar calendar = Calendar.getInstance();
							calendar.set(Calendar.YEAR, year);
							calendar.set(Calendar.MONTH, month);
							calendar.set(Calendar.DAY_OF_MONTH, day);
							calendar.set(Calendar.HOUR_OF_DAY, hour);
							calendar.set(Calendar.MINUTE, minute);
							calendar.set(Calendar.SECOND, second);
							calendar.set(Calendar.MILLISECOND, 0);
							int time = (int) (calendar.getTimeInMillis() / 1000);
							checkAndSave(player, time);
						}
					}
					catch(Exception e)
					{}
				}
			}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "MMOTopDeamon: error parsing data: " + e.getMessage());
		}
	}

	private void checkAndSave(L2Player player, int time)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement stm = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			stm = con.prepareStatement("SELECT * FROM `character_votes_mmotop` WHERE `obj_Id`=? AND `time`=? LIMIT 1");
			stm.setInt(1, player.getObjectId());
			stm.setInt(2, time);
			rs = stm.executeQuery();
			if(!rs.next())
			{
				if(player != null && player.isOnline())
				{
					int[] reward = Config.MMOTOP_DEMON_ITEM.getReward(player.getLevel());
					if(reward == null)
						return;

					Functions.addItem(player, reward[0], reward[1]);
					player.sendMessage("Спасибо, что проголосовали за наш сервер в рейтинге MMOTop!");

					stm = con.prepareStatement("INSERT INTO `character_votes_mmotop`(`obj_Id`,`char_name`,`time`) VALUES (?,?,? )");
					stm.setInt(1, player.getObjectId());
					stm.setString(2, player.getName());
					stm.setInt(3, time);
					stm.execute();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, stm, rs);
		}
	}
}
