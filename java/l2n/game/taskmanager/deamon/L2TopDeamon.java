package l2n.game.taskmanager.deamon;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.database.utils.mysql;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.util.Util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public class L2TopDeamon
{
	private class AutoReward implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				URL url = new URL(getUrl());
				parse(url.openStream());
			}
			catch(Exception e)
			{
				_log.warning("L2TopDeamon: Error while get URL.");
			}
		}
	}

	private static final Logger _log = Logger.getLogger(L2TopDeamon.class.getName());

	private final static String url = "http://l2top.ru/editServ/?adminAct=lastVotes&uid=%serverId%&key=%serverKey%";
	private final static String urlWEB = "http://l2top.ru/editServ/?adminAct=lastVotes&uid=%serverId%_web&key=%serverKey%";
	private final static String urlSMS = "http://l2top.ru/editServ/?adminAct=lastVotes&uid=%serverId%_sms&key=%serverKey%";

	private ScheduledFuture<AutoReward> checkTask = null;
	private boolean _firstRun = false;
	private Timestamp _lastVote;

	public static L2TopDeamon getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final L2TopDeamon _instance = new L2TopDeamon();
	}

	public L2TopDeamon()
	{
		load();
		if(checkTask != null)
		{
			checkTask.cancel(true);
			checkTask = null;
		}
		checkTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new AutoReward(), 60000, Config.L2TOPDEMON_POLLINTERVAL * 60000);
	}

	private String getUrl()
	{
		if(Config.L2TOP_REW_MODE.equalsIgnoreCase("SMS"))
			return urlSMS.replaceAll("%serverId%", String.valueOf(Config.L2TOPDEMON_SERVERID)).replaceAll("%serverKey%", String.valueOf(Config.L2TOPDEMON_KEY));
		if(Config.L2TOP_REW_MODE.equalsIgnoreCase("WEB"))
			return urlWEB.replaceAll("%serverId%", String.valueOf(Config.L2TOPDEMON_SERVERID)).replaceAll("%serverKey%", String.valueOf(Config.L2TOPDEMON_KEY));
		return url.replaceAll("%serverId%", String.valueOf(Config.L2TOPDEMON_SERVERID)).replaceAll("%serverKey%", String.valueOf(Config.L2TOPDEMON_KEY));
	}

	private boolean load()
	{
		_lastVote = null;
		if(Config.L2TOPDEMON_ENABLED)
		{
			ThreadConnection con = null;
			FiltredPreparedStatement stm = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				stm = con.prepareStatement("SELECT MAX(date) FROM `character_votes_l2top`");
				rs = stm.executeQuery();
				if(rs.next())
					_lastVote = rs.getTimestamp(1);
				if(_lastVote == null)
				{
					_firstRun = true;
					_lastVote = new Timestamp(0);
				}
				return true;
			}
			catch(Exception e)
			{
				_log.info("L2Top: Error connection to database: ");
				e.printStackTrace();
			}
			finally
			{
				DbUtils.closeQuietly(stm, rs);
				DbUtils.closeQuietly(con);
			}
		}
		return false;
	}

	private void parse(InputStream is) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is, "windows-1251"));

		int votes = 0;
		Timestamp last = _lastVote;
		String line;
		while ((line = reader.readLine()) != null)
		{
			if(line.indexOf("\t") == -1)
				continue;
			Timestamp voteDate = Timestamp.valueOf(line.substring(0, line.indexOf("\t")).trim());
			if(voteDate.after(_lastVote))
			{
				if(voteDate.after(last))
					last = voteDate;
				String charName = line.substring(line.indexOf("\t") + 1).toLowerCase();

				if(Config.L2TOPDEMON_PREFIX != null && Config.L2TOPDEMON_PREFIX.length() > 0)
					if(charName.startsWith(Config.L2TOPDEMON_PREFIX))
						charName = charName.substring(Config.L2TOPDEMON_PREFIX.length() + 1);

				storeVore(charName, voteDate, !_firstRun);
				votes++;
			}
		}

		_lastVote = last;
		_log.info("L2TopDeamon processed " + votes + " votes.");
	}

	private void storeVore(String charName, Timestamp voteDate, boolean canGiveReward)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement stm = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			stm = con.prepareStatement("insert into `character_votes_l2top` select ?,? from characters where not exists(select * from `character_votes_l2top` where date=? and char_name =?) limit 1");
			stm.setTimestamp(1, voteDate);
			stm.setString(2, charName);
			stm.setTimestamp(3, voteDate);
			stm.setString(4, charName);

			boolean sendPrize = stm.executeUpdate() > 0;
			if(sendPrize && (canGiveReward || Config.L2TOPDEMON_IGNOREFIRST))
				rewardPlayer(charName);
		}
		catch(Exception e)
		{
			_log.info("L2Top: Error connection to database: ");
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, stm);
		}
	}

	private void rewardPlayer(String charName)
	{
		int charId = 0;
		L2Player player = L2ObjectsStorage.getPlayer(charName);
		if(player != null)
		{
			int[] reward = Config.L2TOPDEMON_ITEM.getReward(player.getLevel());
			if(reward == null)
				return;

			Functions.addItem(player, reward[0], reward[1]);

			if(!player.isInOfflineMode())
				player.sendMessage("Спасибо, что проголосовали за наш сервер в рейтинге L2Top!");
		}
		else if((charId = Util.GetCharIDbyName(charName)) > 0)
		{
			Object obj = mysql.get("SELECT level FROM character_subclasses WHERE char_obj_id = " + charId + " AND isBase = 1");

			int[] reward = Config.L2TOPDEMON_ITEM.getReward((Integer) obj);
			if(reward == null)
				return;

			mysql.set("INSERT INTO `items_delayed` (`owner_id`, `item_id`, `count`, `enchant_level`, `flags`, `payment_status`, `description`) VALUES(" + charId + ", " + reward[0] + ", " + reward[1] + ", '0', '0', '0', 'L2TopReward')");
		}
	}
}
