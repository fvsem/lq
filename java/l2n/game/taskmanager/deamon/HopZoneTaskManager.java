package l2n.game.taskmanager.deamon;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HopZoneTaskManager
{
	private static final Logger _log = Logger.getLogger(HopZoneTaskManager.class.getName());

	private static final String HOPZONE = Config.L2HOPZONE_SERVICE_WEB_ADRESS;

	private static final int initialCheck = 180 * 1000;

	private static final int delayForCheck = Config.L2HOPZONE_SERVICE_INTERVAL * 60 * 1000;
	private static final int[] itemId = Config.L2HOPZONE_SERVICE_ITEMID;
	private static final int[] itemCount = Config.L2HOPZONE_SERVICE_COUNT;

	private static final int votesRequiredForReward = Config.L2HOPZONE_SERVICE_VOTES_REQUIRED_FOR_REWARD;

	private URL url;

	// do not change
	private int lastVoteCount = 0;

	private HopZoneTaskManager()
	{
		_log.info("HopZone Vote Reward System Initiated.");

		try
		{
			url = new URL(HOPZONE);
		}
		catch(MalformedURLException e)
		{
			_log.warning("HopZone: error create URL: " + HOPZONE);
		}

		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new AutoReward(), initialCheck, delayForCheck);
	}

	private class AutoReward implements Runnable
	{
		@Override
		public void run()
		{
			int votes = getVotes();
			_log.info("HopZone: server Votes: " + votes);

			if(votes > 0 && lastVoteCount > 0 && votes >= lastVoteCount + votesRequiredForReward)
			{
				ThreadConnection con = null;
				FiltredPreparedStatement statement = null;
				ResultSet rset = null;
				try
				{
					con = L2DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("SELECT c.obj_Id, c.char_name FROM characters AS c LEFT JOIN accounts AS a ON c.account_name = a.login WHERE c.online > 0 GROUP BY a.lastIP");
					rset = statement.executeQuery();
					L2Player player;
					while (rset.next())
					{
						player = L2ObjectsStorage.getPlayer(rset.getInt("obj_Id"));
						if(player != null && !player.isInOfflineMode() && !player.isLogoutStarted() && player.isOnline())
							for(int i = 0; i < itemId.length; i++)
								player.addItem(itemId[i], itemCount[i], 0, "HopZoneReward");
					}
				}
				catch(SQLException e)
				{
					_log.log(Level.WARNING, "HopZone: error AutoReward", e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement, rset);
				}

				lastVoteCount += votesRequiredForReward;
				Announcements.announceToAll("Server Votes: " + votes + " | Next Reward on " + (lastVoteCount + votesRequiredForReward) + " Votes.");
			}
			else if(lastVoteCount > 0)
				Announcements.announceToAll("Server Votes: Need " + (lastVoteCount + votesRequiredForReward - votes) + " votes to reward.");

			if(lastVoteCount == 0)
				lastVoteCount = votes;
		}
	}

	private int getVotes()
	{
		BufferedReader is = null;
		java.net.HttpURLConnection uc = null;
		try
		{
			uc = (java.net.HttpURLConnection) url.openConnection();
			uc.setRequestMethod("GET");

			// add here header
			uc.setRequestProperty("Host", "l2.hopzone.net");
			uc.setRequestProperty("Accept", "text/html");
			uc.setRequestProperty("User-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30");
			uc.setDoOutput(true);
			uc.setDoInput(true);
			uc.connect();

			is = new BufferedReader(new InputStreamReader(uc.getInputStream()));

			String inputLine;
			while ((inputLine = is.readLine()) != null)
				if(inputLine.contains("moreinfo_total_rank_text"))
					return Integer.valueOf(inputLine.split(">")[2].replace("</div", ""));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "HopZone: error while get URL: " + HOPZONE, e);
		}
		finally
		{
			try
			{
				if(is != null)
					is.close();
			}
			catch(IOException e)
			{}
			try
			{
				uc.disconnect();
			}
			catch(Exception e)
			{}
		}
		return 0;
	}

	public static HopZoneTaskManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final HopZoneTaskManager _instance = new HopZoneTaskManager();
	}
}
