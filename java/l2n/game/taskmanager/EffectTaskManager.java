package l2n.game.taskmanager;

import l2n.Config;
import l2n.commons.threading.SteppingRunnableQueueManager;
import l2n.game.L2GameThreadPools;
import l2n.util.Rnd;

import java.util.logging.Logger;

public class EffectTaskManager extends SteppingRunnableQueueManager
{
	private static final Logger _log = Logger.getLogger(EffectTaskManager.class.getName());

	private final static long TICK = 250L;

	private final static EffectTaskManager[] _instances = new EffectTaskManager[Config.EFFECT_TASK_MANAGER_COUNT];
	static
	{
		for(int i = 0; i < _instances.length; i++)
			_instances[i] = new EffectTaskManager();
	}

	private EffectTaskManager()
	{
		super(TICK);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, Rnd.get(TICK), TICK);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				purge();
			}

		}, 30000L, 30000L);
	}

	private static int randomizer = 0;

	public static EffectTaskManager getInstance()
	{
		return _instances[randomizer++ & _instances.length - 1];
	}

	@Override
	protected String getName()
	{
		return "EffectTaskManager";
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
