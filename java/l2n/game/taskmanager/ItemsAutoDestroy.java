package l2n.game.taskmanager;

import l2n.Config;
import l2n.commons.util.CollectionUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemsAutoDestroy
{
	private static final Logger _log = Logger.getLogger(ItemsAutoDestroy.class.getName());

	private static final long herb_sleep = 60 * 1000;
	private static final long item_sleep = Config.AUTODESTROY_ITEM_AFTER * 1000L;

	private final ConcurrentLinkedQueue<L2ItemInstance> _items;
	private final ConcurrentLinkedQueue<L2ItemInstance> _herbs;

	private ItemsAutoDestroy()
	{
		_herbs = new ConcurrentLinkedQueue<L2ItemInstance>();
		if(Config.AUTODESTROY_ITEM_AFTER > 0)
		{
			_items = new ConcurrentLinkedQueue<L2ItemInstance>();
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new CheckItemsForDestroy(), 60 * 1000, 60 * 1000);
		}
		else
			_items = CollectionUtils.emptyConcurrentQueue();
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new CheckHerbsForDestroy(), 1000, 1000);
	}

	public static ItemsAutoDestroy getInstance()
	{
		return SingletonHolder._instance;
	}

	public void addItem(final L2ItemInstance item)
	{
		item.setDropTime(System.currentTimeMillis());
		_items.add(item);
	}

	public void addHerb(final L2ItemInstance herb)
	{
		herb.setDropTime(System.currentTimeMillis());
		_herbs.add(herb);
	}

	private class CheckItemsForDestroy implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final long curtime = System.currentTimeMillis();
				for(final L2ItemInstance item : _items)
					if(item == null || item.getDropTime() == 0 || item.getLocation() != ItemLocation.VOID)
						_items.remove(item);
					else if(item.getDropTime() + item_sleep < curtime)
					{
						item.deleteMe();
						_items.remove(item);
					}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "CheckItemsForDestroy: get error ", e);
			}
		}
	}

	private class CheckHerbsForDestroy implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final long curtime = System.currentTimeMillis();
				for(final L2ItemInstance item : _herbs)
					if(item == null || item.getDropTime() == 0 || item.getLocation() != ItemLocation.VOID)
						_herbs.remove(item);
					else if(item.getDropTime() + herb_sleep < curtime)
					{
						item.deleteMe();
						_herbs.remove(item);
					}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "CheckHerbsForDestroy: get error ", e);
			}
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final ItemsAutoDestroy _instance = new ItemsAutoDestroy();
	}
}
