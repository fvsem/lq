package l2n.game.taskmanager;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.procedure.TLongProcedure;
import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExVitalityPointInfo;

public class VitalityManager
{
	private PlayerContainer[] _tasks = new PlayerContainer[2];
	private int _currentCell = 0;

	private final VitalityProcedure _proc = new VitalityProcedure();

	private VitalityManager()
	{
		if(Config.ENABLE_VITALITY)
			L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new VitalityScheduler(), 2 * 60 * 1000, 2 * 60 * 1000);
	}

	public static VitalityManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public PlayerContainer addVitalityTask(L2Player p)
	{
		int cell = _currentCell + 1;
		if(_tasks.length <= cell)
			cell -= _tasks.length;
		if(_tasks[cell] == null)
			_tasks[cell] = new PlayerContainer();
		_tasks[cell].addPlayer(p);
		return _tasks[cell];
	}

	private class VitalityScheduler implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				PlayerContainer currentContainer = _tasks[_currentCell];
				if(currentContainer != null)
					currentContainer.forEachValue(_proc);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				if(_tasks[_currentCell] != null)
					_tasks[_currentCell].clear();
				_currentCell++;
				if(_currentCell >= _tasks.length)
					_currentCell = 0;
			}
		}
	}

	private class PlayerContainer
	{
		private TIntLongHashMap list = new TIntLongHashMap();

		public void addPlayer(L2Player e)
		{
			if(!list.containsKey(e.getObjectId()))
				list.put(e.getObjectId(), e.getStoredId());
		}

		public void forEachValue(VitalityProcedure procedure)
		{
			list.forEachValue(procedure);
		}

		public void clear()
		{
			synchronized (list)
			{
				list.clear();
			}
		}
	}

	private final class VitalityProcedure implements TLongProcedure
	{
		@Override
		public boolean execute(long playerStoreId)
		{
			L2Player player = L2ObjectsStorage.getAsPlayer(playerStoreId);
			if(player != null && !player.isDeleting() && (player.isConnected() || player.isInOfflineMode()) && player.isInPeaceZone() && player.getVitalityPoints() < L2Player.MAX_VITALITY_POINTS)
			{
				player.addVitalityPoints(Config.RATE_RECOVERY_VITALITY_PEACE_ZONE, true);
				player.sendPacket(new ExVitalityPointInfo((int) player.getVitalityPoints()));

				addVitalityTask(player);
			}
			return true;
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final VitalityManager _instance = new VitalityManager();
	}
}
