package l2n.game.taskmanager;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TObjectProcedure;
import l2n.Config;
import l2n.commons.lang.reference.IHardReference;
import l2n.game.L2GameThreadPools;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

import java.util.logging.Level;
import java.util.logging.Logger;

public class RandomAnimationManager
{
	private static final Logger _log = Logger.getLogger(RandomAnimationManager.class.getName());

	private static final int MINIMUM_TIME = Config.MIN_NPC_ANIMATION;
	private static final int MAXIMUM_TIME = Config.MAX_NPC_ANIMATION;

	private final NpcContainer[] _tasks = new NpcContainer[MAXIMUM_TIME * 2];
	private int _currentCell = 0;

	private RandomAnimationManager()
	{
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new AnimationScheduler(), 1000, 1000);
	}

	public static RandomAnimationManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public void addAnimationTask(final L2NpcInstance npc)
	{
		int cell = _currentCell + Rnd.get(MINIMUM_TIME, MAXIMUM_TIME);
		if(_tasks.length <= cell)
			cell -= _tasks.length;
		if(_tasks[cell] == null)
			_tasks[cell] = new NpcContainer();
		_tasks[cell].addNpc(npc);
	}

	private class AnimationScheduler implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final NpcContainer currentContainer = _tasks[_currentCell];
				if(currentContainer != null)
					currentContainer.forEachValue(AnimationProcedure.STATIC_INSTANCE);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "RandomAnimationManager[92]: ", e);
			}
			finally
			{
				if(_tasks[_currentCell] != null)
					_tasks[_currentCell].clear();

				_currentCell++;
				if(_currentCell >= _tasks.length)
					_currentCell = 0;
			}
		}
	}

	private class NpcContainer
	{
		private final TIntObjectHashMap<IHardReference<L2NpcInstance>> list = new TIntObjectHashMap<IHardReference<L2NpcInstance>>();

		public void addNpc(final L2NpcInstance e)
		{
			if(!list.containsKey(e.getObjectId()))
				list.put(e.getObjectId(), e.getRef());
		}

		public void forEachValue(final TObjectProcedure<? super IHardReference<L2NpcInstance>> procedure)
		{
			list.forEachValue(procedure);
		}

		public void clear()
		{
			synchronized (list)
			{
				list.clear();
			}
		}
	}

	private static final class AnimationProcedure implements TObjectProcedure<IHardReference<L2NpcInstance>>
	{
		private static final AnimationProcedure STATIC_INSTANCE = new AnimationProcedure();

		@Override
		public boolean execute(final IHardReference<L2NpcInstance> ref)
		{
			final L2NpcInstance npc = ref.get();
			if(npc == null)
				return true;

			if(npc.isMonster() || !npc.isInActiveRegion())
			{
				npc.setAnimationManagerStatus(false);
				return true;
			}

			if(npc.isVisible() && !npc.isDead() && !npc.isStunned() && !npc.isSleeping() && !npc.isParalyzed() && !npc.isMoving && !npc.isImobilised() && !npc.isRooted() && !npc.isInCombat())
				npc.onRandomAnimation();

			npc.startRandomAnimation();
			return true;
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final RandomAnimationManager _instance = new RandomAnimationManager();
	}
}
