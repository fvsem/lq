package l2n.game.taskmanager;

import l2n.commons.lang.reference.IHardReference;
import l2n.commons.threading.SteppingRunnableQueueManager;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Spawn;
import l2n.game.model.instances.L2NpcInstance;

import java.util.concurrent.Future;
import java.util.logging.Logger;

public class SpawnTaskManager extends SteppingRunnableQueueManager
{
	private static final Logger _log = Logger.getLogger(SpawnTaskManager.class.getName());

	public SpawnTaskManager()
	{
		super(500L);

		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 500L, 500L);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				SpawnTaskManager.this.purge();
			}

		}, 60000L, 60000L);
	}

	public static SpawnTaskManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public Future<SpawnTask> addSpawnTask(final L2NpcInstance actor, final long interval)
	{
		return schedule(new SpawnTask(actor), interval);
	}

	public static final class SpawnTask implements Runnable
	{
		private final IHardReference<L2NpcInstance> _npcRef;

		public SpawnTask(final L2NpcInstance npc)
		{
			_npcRef = npc.getRef();
		}

		@Override
		public void run()
		{
			final L2NpcInstance npc = getActor();
			L2Spawn spawn = null;
			if(npc == null || (spawn = npc.getSpawn()) == null)
				return;

			spawn.decreaseScheduledCount();
			if(spawn.isDoRespawn())
				spawn.respawnNpc(npc);
		}

		private L2NpcInstance getActor()
		{
			return _npcRef.get();
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final SpawnTaskManager _instance = new SpawnTaskManager();
	}

	@Override
	protected String getName()
	{
		return "SpawnTaskManager";
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
