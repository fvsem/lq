package l2n.game.taskmanager;

import l2n.commons.threading.SteppingRunnableQueueManager;
import l2n.game.L2GameThreadPools;

import java.util.logging.Logger;

public class RegenTaskManager extends SteppingRunnableQueueManager
{
	private static final Logger _log = Logger.getLogger(RegenTaskManager.class.getName());

	public static RegenTaskManager getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final RegenTaskManager _instance = new RegenTaskManager();
	}

	private RegenTaskManager()
	{
		super(1000L);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(this, 1000L, 1000L);
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				RegenTaskManager.this.purge();
			}

		}, 10000L, 10000L);
	}

	@Override
	protected String getName()
	{
		return "RegenTaskManager";
	}

	@Override
	protected Logger getLogger()
	{
		return _log;
	}
}
