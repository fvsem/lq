package l2n.game.taskmanager;

import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Player;
import l2n.game.taskmanager.tasks.BonusPlayerTask;
import l2n.game.taskmanager.tasks.KickPlayerTask;
import l2n.game.taskmanager.tasks.PlayerTask;
import l2n.game.taskmanager.tasks.PlayerTaskType;
import l2n.util.Util;

import java.util.ArrayDeque;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PlayerTaskManager
{
	private static final Logger _log = Logger.getLogger(PlayerTaskManager.class.getName());

	private PlayerTask[] _short_tasks = new PlayerTask[500];

	private int _tasksSize = 0;
	private int _lastWorkSize = 16;

	private final ReentrantLock tasks_lock = new ReentrantLock();

	private PlayerTaskManager()
	{
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new PlayerTaskScheduler(), 1000, 1000);
	}

	public static PlayerTaskManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public final void addPlayerTask(final PlayerTaskType type, final L2Player player, final long delay, final boolean interval)
	{
		removeTask(type, player.getObjectId());
		
		switch (type)
		{
			case KICK:
				addTask(new KickPlayerTask(type, player, interval ? System.currentTimeMillis() + delay : delay));
				break;
			case BONUS:
				addTask(new BonusPlayerTask(type, player, interval ? System.currentTimeMillis() + delay : delay));
				break;
		}
	}

	public void cancelPlayerTask(final PlayerTaskType type, final int objId)
	{
		removeTask(type, objId);
	}

	private void addTask(final PlayerTask task)
	{
		tasks_lock.lock();
		try
		{
			if(_tasksSize >= _short_tasks.length)
			{
				final PlayerTask[] temp = new PlayerTask[_short_tasks.length * 2];
				System.arraycopy(_short_tasks, 0, temp, 0, _short_tasks.length);
				_short_tasks = temp;
			}

			_short_tasks[_tasksSize] = task;
			_tasksSize++;
		}
		finally
		{
			tasks_lock.unlock();
		}
	}

	private void removeTask(final PlayerTaskType type, final int objId)
	{
		tasks_lock.lock();
		try
		{
			if(_tasksSize > 1)
			{
				int k = -1;
				for(int i = 0; i < _tasksSize; i++)
					if(_short_tasks[i].equals(type, objId))
						k = i;

				if(k > -1)
				{
					_short_tasks[k] = _short_tasks[_tasksSize - 1];
					_short_tasks[_tasksSize - 1] = null;
					_tasksSize--;
				}
			}
			else if(_tasksSize == 1 && _short_tasks[0].equals(type, objId))
			{
				_short_tasks[0] = null;
				_tasksSize = 0;
			}
		}
		finally
		{
			tasks_lock.unlock();
		}
	}

	public class PlayerTaskScheduler implements Runnable
	{
		@Override
		public void run()
		{
			if(_tasksSize > 0)
				try
				{
					final ArrayDeque<PlayerTask> works = new ArrayDeque<PlayerTask>(_lastWorkSize);
					tasks_lock.lock();
					try
					{
						final long current = System.currentTimeMillis();

						PlayerTask task;
						int i = _tasksSize;
						L2Player actor;

						while (i-- > 0)
							try
							{
								task = _short_tasks[i];
								if(task != null && task.endtime > 0 && current >= task.endtime)
								{
									actor = task.getActor();
									if(actor != null && actor.getObjectId() == task.objectId)
										works.add(task);

									task.endtime = -1;
								}

								if(task == null || task.getActor() == null || task.endtime < 0)
								{

									if(i == _tasksSize - 1)
										_short_tasks[i] = null;
									else
									{
		
										_short_tasks[i] = _short_tasks[_tasksSize - 1];

										_short_tasks[_tasksSize - 1] = null;
									}

									if(_tasksSize > 0)
										_tasksSize--;
								}
							}
							catch(final Exception e)
							{
								_log.log(Level.WARNING, "PlayerTaskManager: error while search expired task!", e);
							}
					}
					finally
					{
						tasks_lock.unlock();
					}

					final int currentWorksSize = works.size();
					if(currentWorksSize < 16)
						_lastWorkSize = 16;
					else
						_lastWorkSize = currentWorksSize;

					// если задачи для обработки есть, то запускаем
					PlayerTask task;
					while ((task = works.poll()) != null)
						task.execute();
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "PlayerTaskManager: ", e);
				}
		}
	}

	public String getReport()
	{
		final StringBuffer sb = new StringBuffer("============= PlayerTask Manager Report ============\n\r");
		sb.append("Tasks count: ").append(_tasksSize).append("\n\r");
		sb.append("Tasks dump:\n\r");

		PlayerTask task;
		for(int i = 0; i < _tasksSize; i++)
			if((task = _short_tasks[i]) != null)
			{
				sb.append("[" + i + "] " + task.type + ": ").append(task.getActor().toString());
				sb.append(" {").append(Util.datetimeFormatter.format(task.endtime)).append("}\n\r");
			}

		return sb.toString();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final PlayerTaskManager _instance = new PlayerTaskManager();
	}
}
