package l2n.game.taskmanager;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DelayedItemsManager implements Runnable
{
	private static final Logger _log = Logger.getLogger(DelayedItemsManager.class.getName());

	private static final Object _lock = new Object();
	private int last_payment_id = 0;

	public static DelayedItemsManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public DelayedItemsManager()
	{
		ThreadConnection con = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			last_payment_id = get_last_payment_id(con);
		}
		catch(Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con);
		}

		L2GameThreadPools.getInstance().scheduleGeneral(this, Config.DELAYED_ITEMS_UPDATE_INTERVAL);
		_log.info("DelayedItemsManager scheduled, last payment: " + last_payment_id);
	}

	private int get_last_payment_id(ThreadConnection con)
	{
		FiltredPreparedStatement st = null;
		ResultSet rset = null;
		int result = last_payment_id;
		try
		{
			st = con.prepareStatement("SELECT MAX(payment_id) AS last FROM items_delayed");
			rset = st.executeQuery();
			if(rset.next())
				result = rset.getInt("last");
		}
		catch(Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(st, rset);
		}
		return result;
	}

	@Override
	public void run()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		ResultSet rset = null;
		L2Player player = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			int last_payment_id_temp = get_last_payment_id(con);
			if(last_payment_id_temp != last_payment_id)
				synchronized (_lock)
				{
					st = con.prepareStatement("SELECT DISTINCT owner_id FROM items_delayed WHERE payment_status=0 AND payment_id > ?");
					st.setInt(1, last_payment_id);
					rset = st.executeQuery();
					while (rset.next())
						if((player = L2ObjectsStorage.getPlayer(rset.getInt("owner_id"))) != null)
							loadDelayed(player, true);
					last_payment_id = last_payment_id_temp;
				}
		}
		catch(Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, st, rset);
		}

		L2GameThreadPools.getInstance().scheduleGeneral(this, Config.DELAYED_ITEMS_UPDATE_INTERVAL);
	}

	public int loadDelayed(L2Player player, boolean notify)
	{
		if(player == null)
			return 0;
		final int player_id = player.getObjectId();
		final PcInventory inv = player.getInventory();
		if(inv == null)
			return 0;

		ThreadConnection con = null;
		FiltredPreparedStatement st = null, st_delete = null;
		ResultSet rset = null;
		int restored_counter = 0;

		synchronized (_lock)
		{
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				st = con.prepareStatement("SELECT * FROM items_delayed WHERE owner_id=? AND payment_status=0");
				st.setInt(1, player_id);
				rset = st.executeQuery();

				L2ItemInstance item, newItem;
				st_delete = con.prepareStatement("UPDATE items_delayed SET payment_status=1 WHERE payment_id=?");

				while (rset.next())
				{
					final int ITEM_ID = rset.getShort("item_id");
					final long ITEM_COUNT = rset.getLong("count");
					final short ITEM_ENCHANT = rset.getShort("enchant_level");
					final int PAYMENT_ID = rset.getInt("payment_id");
					final int FLAGS = rset.getInt("flags");
					boolean stackable;
					try
					{
						stackable = ItemTable.getInstance().getTemplate(ITEM_ID).isStackable();
					}
					catch(Exception e)
					{
						_log.log(Level.WARNING, "DelayedItemsManager: error load itemId=" + ITEM_ID + ", count=" + ITEM_COUNT + ", ench=" + ITEM_ENCHANT + " for player " + player.getName() + " [ITEM NOT EXIST]");
						continue;
					}

					boolean success = false;
					for(int i = 0; i < (stackable ? 1 : ITEM_COUNT); i++)
					{
						item = ItemTable.getInstance().createItem(ITEM_ID, player_id, 0, "delayed_add");
						if(item.isStackable())
							item.setCount(ITEM_COUNT);
						else
							item.setEnchantLevel(ITEM_ENCHANT);

						item.setLocation(ItemLocation.INVENTORY);
						item.setCustomFlags(FLAGS, false);

						if(ITEM_COUNT > 0)
						{
							newItem = inv.addItem(item);
							if(newItem == null)
							{
								_log.warning("Unable to delayed create item " + ITEM_ID + " request " + PAYMENT_ID);
								continue;
							}
							newItem.updateDatabase(true);
						}

						success = true;
						restored_counter++;
						if(notify && ITEM_COUNT > 0)
							player.sendPacket(SystemMessage.obtainItems(ITEM_ID, stackable ? ITEM_COUNT : 1, ITEM_ENCHANT));
					}
					if(!success)
						continue;

					Log.add("<add owner_id=" + player_id + " item_id=" + ITEM_ID + " count=" + ITEM_COUNT + " enchant_level=" + ITEM_ENCHANT + " payment_id=" + PAYMENT_ID + "/>", "delayed_add");

					st_delete.setInt(1, PAYMENT_ID);
					st_delete.execute();
				}
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "could not load delayed items for player " + player.getName() + ":", e);
			}
			finally
			{
				DbUtils.closeQuietly(st_delete);
				DbUtils.closeQuietly(con, st, rset);
			}
		}
		return restored_counter;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DelayedItemsManager _instance = new DelayedItemsManager();
	}
}
