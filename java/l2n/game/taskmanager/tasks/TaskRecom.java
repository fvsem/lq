package l2n.game.taskmanager.tasks;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.taskmanager.TaskManager;
import l2n.game.taskmanager.TaskManager.ExecutedTask;

public class TaskRecom extends Task
{
	private static final String NAME = "sp_recommendations";

	@Override
	public String getName()
	{
		return NAME;
	}

	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		_log.config("Recommendation Global Task: launched.");
		for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			player.restartRecom();

		_log.config("Recommendation Global Task: completed.");
	}

	@Override
	public void initializate()
	{
		super.initializate();
		TaskManager.addUniqueTask(NAME, TaskTypes.TYPE_GLOBAL_TASK, "1", "13:00:00", "");
	}
}
