package l2n.game.taskmanager.tasks;

import l2n.game.model.actor.L2Player;

public final class KickPlayerTask extends PlayerTask
{
	public KickPlayerTask(final PlayerTaskType type, final L2Player player, final long delay)
	{
		super(type, player, delay);
	}

	@Override
	public void execute()
	{
		final L2Player player = getActor();
		if(player != null)
		{
			player.setOfflineMode(false);
			player.logout(false, false, true, true);
		}
	}
}
