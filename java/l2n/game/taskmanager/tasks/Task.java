package l2n.game.taskmanager.tasks;

import l2n.Config;
import l2n.game.taskmanager.TaskManager.ExecutedTask;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Logger;

public abstract class Task
{
	protected final static Logger _log = Logger.getLogger(Task.class.getName());

	public void initializate()
	{
		if(Config.DEBUG)
			_log.info("Task" + getName() + " inializate");
	}

	public ScheduledFuture<ExecutedTask> launchSpecial(ExecutedTask instance)
	{
		return null;
	}

	public abstract String getName();

	public abstract void onTimeElapsed(ExecutedTask task);

	public void onDestroy()
	{}
}
