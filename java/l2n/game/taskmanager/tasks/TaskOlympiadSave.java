package l2n.game.taskmanager.tasks;

import l2n.Config;
import l2n.game.model.entity.olympiad.OlympiadDatabase;
import l2n.game.taskmanager.TaskManager;
import l2n.game.taskmanager.TaskManager.ExecutedTask;

public class TaskOlympiadSave extends Task
{
	public static final String NAME = "OlympiadSave";

	@Override
	public String getName()
	{
		return NAME;
	}

	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		if(!Config.ENABLE_OLYMPIAD)
			return;
		try
		{
			OlympiadDatabase.save();
			_log.info("Olympiad System: Data updated successfully.");
		}
		catch(Exception e)
		{
			_log.warning("Olympiad System: Failed to save Olympiad configuration: " + e);
		}
	}

	@Override
	public void initializate()
	{
		super.initializate();
		TaskManager.addUniqueTask(NAME, TaskTypes.TYPE_FIXED_SHEDULED, "0", "600000", "");
	}
}
