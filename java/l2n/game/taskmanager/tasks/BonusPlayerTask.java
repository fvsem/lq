package l2n.game.taskmanager.tasks;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExBR_PremiumState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;

public final class BonusPlayerTask extends PlayerTask
{
	public BonusPlayerTask(final PlayerTaskType type, final L2Player player, final long delay)
	{
		super(type, player, delay);
	}

	@Override
	public void execute()
	{
		final L2Player player = getActor();
		if(player != null)
		{
			if(player.getNetConnection() != null)
				player.getNetConnection().setBonus(1);
			player.restoreBonus();
			if(player.getParty() != null)
				player.getParty().recalculatePartyData();
			final String msg = new CustomMessage("scripts.services.RateBonus.LuckEnded", player).toString();
			player.sendPacket(new ExShowScreenMessage(msg, 10000, ScreenMessageAlign.TOP_CENTER, true), new ExBR_PremiumState(player.getObjectId(), 0));
			player.sendMessage(msg);
		}
	}
}
