package l2n.game.taskmanager.tasks;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class RestoreOfflineTraders implements Runnable
{
	private static final Logger _log = Logger.getLogger(RestoreOfflineTraders.class.getName());

	@Override
	public void run()
	{
		int count = 0;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			if(Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK > 0)
			{
				final int expireTimeSecs = (int) (System.currentTimeMillis() / 1000 - Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK);

				statement = con.prepareStatement("DELETE FROM character_variables WHERE name = 'offline' AND value < ?");
				statement.setLong(1, expireTimeSecs);
				statement.executeUpdate();

				DbUtils.closeQuietly(statement);
			}

			statement = con.prepareStatement("DELETE FROM character_variables WHERE name = 'offline' AND obj_id IN (SELECT obj_id FROM characters WHERE accessLevel < 0)");
			statement.executeUpdate();

			DbUtils.closeQuietly(statement);

			statement = con.prepareStatement("SELECT obj_id, value FROM character_variables WHERE name = 'offline'");
			rset = statement.executeQuery();

			int objectId;
			long expireTimeSecs;
			L2Player p;

			while (rset.next())
			{
				objectId = rset.getInt("obj_id");
				expireTimeSecs = (rset.getInt("value") + Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK) * 1000;

				p = L2Player.restore(objectId);
				if(p == null)
					continue;

				if(p.isDead())
				{
					p.logout(false, false, true, true);
					continue;
				}

				p.restoreBonus();
				p.setOnlineStatus(true);
				p.setOfflineMode(true);
				p.setConnected(false);
				p.setNameColor(Config.SERVICES_OFFLINE_TRADE_NAME_COLOR);
				p.restoreEffects();
				p.restoreDisableSkills();
				p.startAbnormalEffect(Config.SERVICES_OFFLINE_ABNORMAL_EFFECT);
				p.broadcastUserInfo(true);

				p.spawnMe();
				p.updateTerritories();

				if(p.getClan() != null && p.getClan().getClanMember(p.getObjectId()) != null)
					p.getClan().getClanMember(p.getObjectId()).setPlayerInstance(p);

				if(Config.SERVICES_OFFLINE_TRADE_DAYS_TO_KICK > 0)
					p.startKickTask(expireTimeSecs, false);

				p.stopUnimportantTimers();
				p.stopMove();

				if(Config.SERVICES_TRADE_ONLY_FAR)
					for(final L2Player player : L2World.getAroundPlayers(p, Config.SERVICES_TRADE_RADIUS, 200))
						if(player.isInStoreMode())
							if(player.isInOfflineMode())
							{
								player.setOfflineMode(false);
								player.logout(false, false, false, true);
								_log.info("Offline trader: " + player + " kicked.");
							}
							else
							{
								player.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
								player.standUp();
								player.broadcastCharInfo();
							}

				count++;
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error while restoring offline traders!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_log.info("Restored " + count + " offline traders");
	}
}
