package l2n.game.taskmanager.tasks;

import l2n.Config;
import l2n.game.taskmanager.TaskManager;
import l2n.game.taskmanager.TaskManager.ExecutedTask;
import l2n.util.Util;

public final class TaskGC extends Task
{
	@Override
	public String getName()
	{
		return "garbage_collector";
	}

	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		_log.config("Executing Garbage Collector task");
		System.runFinalization();
		System.gc();
		for(String line : Util.getMemoryUsageStatistics())
			_log.info(line);
	}

	@Override
	public void initializate()
	{
		super.initializate();
		TaskManager.addUniqueTask("garbage_collector", TaskTypes.TYPE_FIXED_SHEDULED, String.valueOf(Config.GARBAGE_COLLECTOR_TASK_FREQUENCY * 1000), String.valueOf(Config.GARBAGE_COLLECTOR_TASK_FREQUENCY * 1000), "");
	}
}
