package l2n.game.taskmanager.tasks;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

public abstract class PlayerTask
{
	private final long storeId;
	public final int objectId;
	public final PlayerTaskType type;
	public long endtime;

	protected PlayerTask(final PlayerTaskType t, final L2Player player, final long delay)
	{
		type = t;
		storeId = player.getStoredId();
		objectId = player.getObjectId();
		endtime = delay;
	}

	public final L2Player getActor()
	{
		return L2ObjectsStorage.getAsPlayer(storeId);
	}

	public boolean equals(final PlayerTaskType t, final int objId)
	{
		return (type == t || t == null) && objectId == objId;
	}

	public abstract void execute();
}
