package l2n.game.taskmanager;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.procedure.TLongProcedure;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.CharVariables;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CharVariablesManager
{
	private static final Logger _log = Logger.getLogger(AutoSaveManager.class.getName());

	private final PlayerContainer[] _tasks = new PlayerContainer[2];
	private int _currentCell = 0;

	private CharVariablesManager()
	{
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new CheckVarScheduler(), 1000, 2000);
		_tasks[0] = new PlayerContainer();
		_tasks[1] = new PlayerContainer();
	}

	public static CharVariablesManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public void addPlayer(final L2Player p)
	{
		final int cell = _currentCell == 0 ? 1 : 0;
		_tasks[cell].addPlayer(p);
	}

	private class CheckVarScheduler implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final PlayerContainer currentContainer = _tasks[_currentCell];
				if(currentContainer != null)
					currentContainer.forEachValue(CheckVarProcedure.STATIC_INSTANCE);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "CheckVarScheduler error: ", e);
			}
			finally
			{
				if(_tasks[_currentCell] != null)
					_tasks[_currentCell].clear();
				_currentCell++;
				if(_currentCell >= _tasks.length)
					_currentCell = 0;
			}
		}
	}

	private class PlayerContainer
	{
		private final TIntLongHashMap list = new TIntLongHashMap();

		public void addPlayer(final L2Player e)
		{
			if(!list.containsKey(e.getObjectId()))
				list.put(e.getObjectId(), e.getStoredId());
		}

		public void forEachValue(final CheckVarProcedure checkVarProcedure)
		{
			list.forEachValue(checkVarProcedure);
		}

		public void clear()
		{
			synchronized (list)
			{
				list.clear();
			}
		}
	}

	private static final class CheckVarProcedure implements TLongProcedure
	{
		private static final CheckVarProcedure STATIC_INSTANCE = new CheckVarProcedure();

		@Override
		public boolean execute(final long storeId)
		{
			try
			{
				final L2Player p = L2ObjectsStorage.getAsPlayer(storeId);
				if(p == null || !p.isConnected() || p.isLogoutStarted() || p.getNetConnection() == null)
					return true;

				final CharVariables[] vars = p.getTimeLimitedVars(true);
				if(vars.length == 0)
					return true;

				// Перебираем переменные игрока
				for(final CharVariables var : vars)
					if(var.isExpired())
					{
						if(var.getName().equalsIgnoreCase("hero_time"))
							p.setHero(false, 2); // удаляем статус героя и геройские скилы.
						else if(var.getName().equalsIgnoreCase("noble_time"))
							p.setNoble(false, 2); // Удаляем статус дворянина и скилы дворян.

						// Удаляем переменную.
						p.unsetVar(var.getName());
					}

				// добавляем обратно
				CharVariablesManager.getInstance().addPlayer(p);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "CheckVarProcedure error: ", e);
			}

			return true;
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final CharVariablesManager _instance = new CharVariablesManager();
	}
}
