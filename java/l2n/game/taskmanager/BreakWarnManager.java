package l2n.game.taskmanager;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.procedure.TLongProcedure;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.logging.Level;
import java.util.logging.Logger;

public class BreakWarnManager
{
	private static final Logger _log = Logger.getLogger(BreakWarnManager.class.getName());

	private final PlayerContainer[] _tasks = new PlayerContainer[360];

	private final BreakWarnProcedure _proc = new BreakWarnProcedure();
	private int _currentCell = 0;

	private BreakWarnManager()
	{
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new BreakWarnScheduler(), 60 * 1000, 60 * 1000);
	}

	public static BreakWarnManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public PlayerContainer addWarnTask(final L2Player p)
	{
		int cell = _currentCell + 120;
		if(_tasks.length <= cell)
			cell -= _tasks.length;
		if(_tasks[cell] == null)
			_tasks[cell] = new PlayerContainer();
		_tasks[cell].addPlayer(p);
		return _tasks[cell];
	}

	private class BreakWarnScheduler implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final PlayerContainer currentContainer = _tasks[_currentCell];
				if(currentContainer != null)
					currentContainer.forEachValue(_proc);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "BreakWarnManager Error: ", e);
			}
			finally
			{
				if(_tasks[_currentCell] != null)
					_tasks[_currentCell].clear();

				_currentCell++;
				if(_currentCell >= _tasks.length)
					_currentCell = 0;
			}
		}
	}

	private class PlayerContainer
	{
		private final TIntLongHashMap list = new TIntLongHashMap();

		public void addPlayer(final L2Player e)
		{
			if(!list.containsKey(e.getObjectId()))
				list.put(e.getObjectId(), e.getStoredId());
		}

		public void forEachValue(final BreakWarnProcedure procedure)
		{
			list.forEachValue(procedure);
		}

		public void clear()
		{
			synchronized (list)
			{
				list.clear();
			}
		}
	}

	private final class BreakWarnProcedure implements TLongProcedure
	{
		@Override
		public boolean execute(final long storeId)
		{
			try
			{
				final L2Player player = L2ObjectsStorage.getAsPlayer(storeId);
				if(player == null || !player.isConnected() || player.isDeleting() || player.isInOfflineMode())
					return true;

				player.sendPacket(Msg.YOU_HAVE_BEEN_PLAYING_FOR_AN_EXTENDED_PERIOD_OF_TIME_PLEASE_CONSIDER_TAKING_A_BREAK);
				addWarnTask(player);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "BreakWarnProcedure Error: ", e);
			}

			return true;
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final BreakWarnManager _instance = new BreakWarnManager();
	}
}
