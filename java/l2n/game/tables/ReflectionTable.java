package l2n.game.tables;

import gnu.trove.map.hash.TIntLongHashMap;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.Reflection;
import l2n.game.model.entity.KamalokaNightmare;
import l2n.util.Location;

public final class ReflectionTable
{
	// динамические отражения
	public static final int FORTRESS_DANGEON = 200;
	public static final int CASTLE_DANGEON = 201;

	public static final int CASTLE_PAILAKA = 88;
	public static final int FORTRESS_PAILAKA = 109;

	public static final int SOI_HALL_OF_SUFFERING_SECTOR1 = 115;
	public static final int SOI_HALL_OF_SUFFERING_SECTOR2 = 116;
	public static final int SOI_HALL_OF_EROSION_ATTACK = 119;
	public static final int SOI_HALL_OF_EROSION_DEFENCE = 120;
	public static final int SOI_HEART_OF_INFINITY_ATTACK = 121;
	public static final int SOI_HEART_OF_INFINITY_DEFENCE = 122;

	public static final int URBAN_AREA = 300;

	public static final int DARK_CLOUD_MANSION = 9;
	public static final int CRYSTAL_CAVERNS = 10;
	public static final int NORNILS_GARDEN = 11;

	public static final int PAILAKA_SONG_OF_ICE_AND_FIRE = 12;
	public static final int PAILAKA_DEVILS_LEGACY = 13;
	public static final int PAILAKA_INJURED_DRAGON = 14;

	/** Tiat alive */
	public static final int SEED_OF_DESTRUCTION = 401;
	/** Tiat dead */
	public static final int SEED_OF_DESTRUCTION2 = 400;

	// Для серии эпик квестов Seven Signs
	public static final int SANCTUM_OF_THE_LORDS_OF_DAWN = 111;
	public static final int DISCIPLES_NECROPOLIS = 112;
	public static final int HIDEOUT_OF_THE_DAWN = 113;

	public static final int ZAKEN_DAY = 133;
	public static final int ZAKEN_NIGH = 114;

	// Все остальные)
	public static long SOD_REFLECTION_ID = 0;
	public static final long DEFAULT = 0;

	public static final long PARNASSUS = -1;
	public static final long GH = -2;
	public static final long JAIL = -3;
	/** используется для многослойных локаций, там где уровним мобов сильно отличаются, например для игроков 30 и 80 лвлов */
	public static final long MULTILAYER = -4;

	// Используются для эвентов
	public static final long EVENT_TVT = -5;
	public static final long EVENT_DEATHMATCH = -6;
	public static final long EVENT_CTF = -7;
	public static final long EVENT_LASTHERO = -9;

	public static final long EVENT_TOWN_SIEGE = -8;

	private final static Reflection _default = new Reflection(0, true);

	private final TIntLongHashMap _soloKamalokaList;
	/**
	 * массив статичных отражений, используется в ивентах
	 */
	private final Reflection[] staticReflections;

	private ReflectionTable()
	{
		_soloKamalokaList = new TIntLongHashMap();
		staticReflections = new Reflection[10];

		// init jail reflection
		final Reflection r = staticReflections[3] = new Reflection(JAIL, false);
		r.setCoreLoc(new Location(-114648, -249384, -2984));
	}

	public static ReflectionTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final ReflectionTable _instance = new ReflectionTable();
	}

	public final synchronized void addReflection(final Reflection r)
	{
		if(r.getId() <= 0) // отрицательные номера у статичных отражений
		{
			staticReflections[(int) Math.abs(r.getId())] = r;
			return;
		}
		r.setId(r.getStoredId());
	}

	public final Reflection get(final long index, final boolean createIfNonExist)
	{
		Reflection ret = null;
		if(index <= 0)
		{
			if(staticReflections.length > Math.abs(index))
				ret = staticReflections[(int) Math.abs(index)];
		}
		else
			ret = (Reflection) L2ObjectsStorage.get(index);

		if(createIfNonExist && ret == null)
			ret = new Reflection(index, true);
		return ret;
	}

	public final Reflection get(final long index)
	{
		return get(index, false);
	}

	public final Reflection getDefault()
	{
		return _default;
	}

	public final void addSoloKamaloka(final Integer playerObId, final KamalokaNightmare r)
	{
		_soloKamalokaList.put(playerObId, r.getId());
	}

	public final void removeSoloKamaloka(final Integer playerObId)
	{
		_soloKamalokaList.remove(playerObId);
	}

	public final KamalokaNightmare findSoloKamaloka(final Integer playerObId)
	{
		final Long index = _soloKamalokaList.get(playerObId);
		if(index == null)
			return null;

		final Reflection found = get(index);
		if(found == null || !found.isKamalokaNightmare() || ((KamalokaNightmare) found).getPlayerId() != playerObId)
		{
			_soloKamalokaList.remove(playerObId);
			return null;
		}
		return (KamalokaNightmare) get(index);
	}
}
