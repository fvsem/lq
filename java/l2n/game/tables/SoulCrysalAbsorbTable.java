package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 26.10.2010
 * @time 11:47:32
 */
public class SoulCrysalAbsorbTable
{
	public static final int[] REDCRYSTALS = { 4629, 4630, 4631, 4632, 4633, 4634, 4635, 4636, 4637, 4638, 4639, 5577, 5580, 5908, 9570, 10480, 13071 };
	public static final int[] GREENCRYSTALS = { 4640, 4641, 4642, 4643, 4644, 4645, 4646, 4647, 4648, 4649, 4650, 5578, 5581, 5911, 9572, 10482, 13073 };
	public static final int[] BLUECRYSTALS = { 4651, 4652, 4653, 4654, 4655, 4656, 4657, 4658, 4659, 4660, 4661, 5579, 5582, 5914, 9571, 10481, 13072 };

	public static final short REDCURSEDCRYSTAL_LVL14 = 10160;
	public static final short BLUECURSEDCRYSTAL_LVL14 = 10161;
	public static final short GREENCURSEDCRYSTAL_LVL14 = 10162;

	/** Max number of levels a soul crystal may reach */
	public static final short MAX_CRYSTALS_LEVEL = 16;

	/** Chances for each event of a soul crystal */
	public static final float LEVEL_CHANCE = Config.ALT_SOUL_CRYSTAL_LEVEL_CHANCE;

	private static Logger _log = Logger.getLogger(SoulCrysalAbsorbTable.class.getName());
	private static SoulCrysalAbsorbTable _instance;

	private static TIntObjectHashMap<SoulCrystal> npcAbsorbData = new TIntObjectHashMap<SoulCrystal>();

	public static SoulCrysalAbsorbTable getInstance()
	{
		if(_instance == null)
			_instance = new SoulCrysalAbsorbTable();
		return _instance;
	}

	public SoulCrysalAbsorbTable()
	{
		parseData();
	}

	public static SoulCrystal get(int mobId)
	{
		return npcAbsorbData.get(mobId);
	}

	/**
	 * @param mobId
	 *            - npcId мостра
	 * @return минимальный необходимый уровень кристала для повышения, если нету то возвращает MAX_CRYSTALS_LEVEL
	 */
	public static int getMinLevelCrystal(int mobId)
	{
		SoulCrystal sc = npcAbsorbData.get(mobId);
		return sc == null ? MAX_CRYSTALS_LEVEL : sc.getMinLevelCrystal();
	}

	/**
	 * @param mobId
	 *            - npcId мостра
	 * @return максимальный уровень до которого можно повысить
	 */
	public static int getMaxLevelCrystal(int mobId)
	{
		SoulCrystal sc = npcAbsorbData.get(mobId);
		return sc == null ? 0 : sc.getMaxLevelCrystal();
	}

	public static enum AbsorbCrystalType
	{
		NONE,
		/** тому кто нанёс последний удар */
		LAST_HIT,
		/** всем члена пати, которая нанесла последний удар */
		FULL_PARTY,
		/** одному члену пати, которая нанесла последний удар */
		PARTY_ONE_RANDOM
	}

	public static class SoulCrystal
	{
		private int _mobId;
		private int _maxCrystalLevel;
		private AbsorbCrystalType _absorbType;
		private double _chance;
		private int _minCrystalLevel;

		public SoulCrystal(int mobId, int maxCrystalLevel, AbsorbCrystalType absorbType, double absorbChance, int minCrystalLevel)
		{
			_mobId = mobId;
			_maxCrystalLevel = maxCrystalLevel;
			_absorbType = absorbType;
			_chance = absorbChance;
			_minCrystalLevel = minCrystalLevel;
		}

		public int getMobId()
		{
			return _mobId;
		}

		public AbsorbCrystalType getAbsorbType()
		{
			return _absorbType;
		}

		/**
		 * @return шанс успешного повышения уровня
		 */
		public double getChance()
		{
			return _chance;
		}

		/**
		 * @return максимальный уровень до которого можно повысить
		 */
		public int getMaxLevelCrystal()
		{
			return _maxCrystalLevel;
		}

		/**
		 * @return минимальный необходимый уровень кристала для повышения
		 */
		public int getMinLevelCrystal()
		{
			return _minCrystalLevel;
		}
	}

	public void parseData()
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			File file = new File(Config.DATAPACK_ROOT + "/data/soulcrysal_list.xml");
			if(!file.exists())
			{
				_log.warning("Error in loading data, data/soulcrysal_list.xml file not found!");
				return;
			}

			Document doc = factory.newDocumentBuilder().parse(file);
			for(Node generalDoc = doc.getFirstChild(); generalDoc != null; generalDoc = generalDoc.getNextSibling())
				if("list".equalsIgnoreCase(generalDoc.getNodeName()))
					for(Node innerListDoc = generalDoc.getFirstChild(); innerListDoc != null; innerListDoc = innerListDoc.getNextSibling())
						if("mob".equalsIgnoreCase(innerListDoc.getNodeName()))
						{
							int mobId = XMLUtil.getAttributeIntValue(innerListDoc, "id", 0);
							int maxCrystalLevel = XMLUtil.getAttributeIntValue(innerListDoc, "maxCrystalLevel", 0);
							AbsorbCrystalType absorbtype = AbsorbCrystalType.valueOf(XMLUtil.getAttributeValue(innerListDoc, "absorbType", "NONE"));
							double absorbchance = XMLUtil.getAttributeIntValue(innerListDoc, "chance", 0);
							int minCrystalLevel = XMLUtil.getAttributeIntValue(innerListDoc, "minCrystalLevel", 0);
							npcAbsorbData.put(mobId, new SoulCrystal(mobId, maxCrystalLevel, absorbtype, absorbchance, minCrystalLevel));
						}

			_log.info("SoulCrysalTable: loaded " + npcAbsorbData.size() + " SA enchant mobs");
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "SoulCrysalTable: error: ", e);
		}
	}
}
