package l2n.game.tables;

import javolution.util.FastList;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.L2Skill;
import l2n.game.model.base.L2Augmentation;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.skills.Stats;
import l2n.game.templates.L2Item;
import l2n.util.Rnd;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AugmentationData
{
	private static final Logger _log = Logger.getLogger(AugmentationData.class.getName());

	public static AugmentationData getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final AugmentationData _instance = new AugmentationData();
	}

	private static final int STAT_START = 1;
	private static final int STAT_END = 14560;
	private static final int STAT_BLOCKSIZE = 3640;
	private static final int STAT_SUBBLOCKSIZE = 91;
	private static final int STAT_NUM = 13;

	private static final byte[] STATS1_MAP = new byte[STAT_SUBBLOCKSIZE];
	private static final byte[] STATS2_MAP = new byte[STAT_SUBBLOCKSIZE];

	private static final int BLUE_START = 14561;
	private static final int SKILLS_BLOCKSIZE = 178;

	private static final int BASESTAT_STR = 16341;
	private static final int BASESTAT_CON = 16342;
	private static final int BASESTAT_INT = 16343;
	private static final int BASESTAT_MEN = 16344;

	private static final int ACC_START = 16669;
	private static final int ACC_BLOCKS_NUM = 10;
	private static final int ACC_STAT_SUBBLOCKSIZE = 21;
	private static final int ACC_STAT_NUM = 6;

	private static final int ACC_RING_START = ACC_START;
	private static final int ACC_RING_SKILLS = 18;
	private static final int ACC_RING_BLOCKSIZE = ACC_RING_SKILLS + 4 * ACC_STAT_SUBBLOCKSIZE;
	private static final int ACC_RING_END = ACC_RING_START + ACC_BLOCKS_NUM * ACC_RING_BLOCKSIZE - 1;

	private static final int ACC_EAR_START = ACC_RING_END + 1;
	private static final int ACC_EAR_SKILLS = 18;
	private static final int ACC_EAR_BLOCKSIZE = ACC_EAR_SKILLS + 4 * ACC_STAT_SUBBLOCKSIZE;
	private static final int ACC_EAR_END = ACC_EAR_START + ACC_BLOCKS_NUM * ACC_EAR_BLOCKSIZE - 1;

	private static final int ACC_NECK_START = ACC_EAR_END + 1;
	private static final int ACC_NECK_SKILLS = 24;
	private static final int ACC_NECK_BLOCKSIZE = ACC_NECK_SKILLS + 4 * ACC_STAT_SUBBLOCKSIZE;

	private static final int ACC_END = ACC_NECK_START + ACC_BLOCKS_NUM * ACC_NECK_BLOCKSIZE;

	private static final byte[] ACC_STATS1_MAP = new byte[ACC_STAT_SUBBLOCKSIZE];
	private static final byte[] ACC_STATS2_MAP = new byte[ACC_STAT_SUBBLOCKSIZE];

	private ArrayList<?>[] _augStats = new ArrayList[4];
	private ArrayList<?>[] _augAccStats = new ArrayList[4];

	private ArrayList<?>[] _blueSkills = new ArrayList[10];
	private ArrayList<?>[] _purpleSkills = new ArrayList[10];
	private ArrayList<?>[] _redSkills = new ArrayList[10];
	private ArrayList<?>[] _yellowSkills = new ArrayList[10];

	private ConcurrentHashMap<Integer, augmentationSkill> _allSkills = new ConcurrentHashMap<Integer, augmentationSkill>();

	public AugmentationData()
	{
		_log.info("Initializing AugmentationData.");

		_augStats[0] = new ArrayList<augmentationStat>();
		_augStats[1] = new ArrayList<augmentationStat>();
		_augStats[2] = new ArrayList<augmentationStat>();
		_augStats[3] = new ArrayList<augmentationStat>();

		_augAccStats[0] = new ArrayList<augmentationStat>();
		_augAccStats[1] = new ArrayList<augmentationStat>();
		_augAccStats[2] = new ArrayList<augmentationStat>();
		_augAccStats[3] = new ArrayList<augmentationStat>();

		byte idx;
		for(idx = 0; idx < STAT_NUM; idx++)
		{
			STATS1_MAP[idx] = idx;
			STATS2_MAP[idx] = idx;
		}
		for(int i = 0; i < STAT_NUM; i++)
			for(int j = i + 1; j < STAT_NUM; idx++, j++)
			{
				STATS1_MAP[idx] = (byte) i;
				STATS2_MAP[idx] = (byte) j;
			}
		idx = 0;
		for(int i = 0; i < ACC_STAT_NUM - 2; i++)
			for(int j = i; j < ACC_STAT_NUM; idx++, j++)
			{
				ACC_STATS1_MAP[idx] = (byte) i;
				ACC_STATS2_MAP[idx] = (byte) j;
			}
		ACC_STATS1_MAP[idx] = 4;
		ACC_STATS2_MAP[idx++] = 4;
		ACC_STATS1_MAP[idx] = 5;
		ACC_STATS2_MAP[idx++] = 5;
		ACC_STATS1_MAP[idx] = 4;
		ACC_STATS2_MAP[idx] = 5;

		for(int i = 0; i < 10; i++)
		{
			_blueSkills[i] = new ArrayList<Integer>();
			_purpleSkills[i] = new ArrayList<Integer>();
			_redSkills[i] = new ArrayList<Integer>();
			_yellowSkills[i] = new ArrayList<Integer>();
		}

		load();

		_log.info("AugmentationData: Loaded: " + _augStats[0].size() * 4 + " augmentation stats.");
		_log.info("AugmentationData: Loaded: " + _augAccStats[0].size() * 4 + " accessory augmentation stats.");
		_log.info("AugmentationData: Loaded: " + _allSkills.size() + " skills.");
	}

	public class augmentationSkill
	{
		private int _skillId;
		private int _skillLevel;

		public augmentationSkill(int skillId, int skillLevel)
		{
			_skillId = skillId;
			_skillLevel = skillLevel;
		}

		public L2Skill getSkill()
		{
			return SkillTable.getInstance().getInfo(_skillId, _skillLevel);
		}
	}

	public class augmentationStat
	{
		private final Stats _stat;
		private final int _singleSize;
		private final int _combinedSize;
		private final float _singleValues[];
		private final float _combinedValues[];

		public augmentationStat(Stats stat, float sValues[], float cValues[])
		{
			_stat = stat;
			_singleSize = sValues.length;
			_singleValues = sValues;
			_combinedSize = cValues.length;
			_combinedValues = cValues;
		}

		public int getSingleStatSize()
		{
			return _singleSize;
		}

		public int getCombinedStatSize()
		{
			return _combinedSize;
		}

		public float getSingleStatValue(int i)
		{
			if(i >= _singleSize || i < 0)
				return _singleValues[_singleSize - 1];
			return _singleValues[i];
		}

		public float getCombinedStatValue(int i)
		{
			if(i >= _combinedSize || i < 0)
				return _combinedValues[_combinedSize - 1];
			return _combinedValues[i];
		}

		public Stats getStat()
		{
			return _stat;
		}
	}

	@SuppressWarnings("unchecked")
	private final void load()
	{
		try
		{
			int badAugmantData = 0;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			File file = new File(Config.DATAPACK_ROOT + "/data/stats/augmentation/augmentation_skillmap.xml");

			Document doc = factory.newDocumentBuilder().parse(file);
			for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
				if("list".equalsIgnoreCase(n.getNodeName()))
					for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						if("augmentation".equalsIgnoreCase(d.getNodeName()))
						{
							NamedNodeMap attrs = d.getAttributes();
							int skillId = 0, augmentationId = Integer.parseInt(attrs.getNamedItem("id").getNodeValue());
							int skillLvL = 0;
							String type = "blue";

							for(Node cd = d.getFirstChild(); cd != null; cd = cd.getNextSibling())
								if("skillId".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									skillId = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
								}
								else if("skillLevel".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									skillLvL = Integer.parseInt(attrs.getNamedItem("val").getNodeValue());
								}
								else if("type".equalsIgnoreCase(cd.getNodeName()))
								{
									attrs = cd.getAttributes();
									type = attrs.getNamedItem("val").getNodeValue();
								}

							if(skillId == 0)
							{
								badAugmantData++;
								continue;
							}
							else if(skillLvL == 0)
							{
								badAugmantData++;
								continue;
							}

							int k = (augmentationId - BLUE_START) / SKILLS_BLOCKSIZE;
							if(type.equalsIgnoreCase("blue"))
								((ArrayList<Integer>) _blueSkills[k]).add(augmentationId);
							else if(type.equalsIgnoreCase("purple"))
								((ArrayList<Integer>) _purpleSkills[k]).add(augmentationId);
							else if(type.equalsIgnoreCase("red"))
								((ArrayList<Integer>) _redSkills[k]).add(augmentationId);

							_allSkills.put(augmentationId, new augmentationSkill(skillId, skillLvL));
						}

			if(badAugmantData != 0)
				_log.info("AugmentationData: " + badAugmantData + " bad skill(s) were skipped.");
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Error parsing augmentation_skillmap.xml.", e);
			return;
		}

		for(int i = 1; i < 5; i++)
		{
			try
			{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setValidating(false);
				factory.setIgnoringComments(true);

				File file = new File(Config.DATAPACK_ROOT + "/data/stats/augmentation/augmentation_stats" + i + ".xml");
				Document doc = factory.newDocumentBuilder().parse(file);

				for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
					if("list".equalsIgnoreCase(n.getNodeName()))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
							if("stat".equalsIgnoreCase(d.getNodeName()))
							{
								NamedNodeMap attrs = d.getAttributes();
								String statName = attrs.getNamedItem("name").getNodeValue();
								float soloValues[] = null, combinedValues[] = null;

								for(Node cd = d.getFirstChild(); cd != null; cd = cd.getNextSibling())
									if("table".equalsIgnoreCase(cd.getNodeName()))
									{
										attrs = cd.getAttributes();
										String tableName = attrs.getNamedItem("name").getNodeValue();

										StringTokenizer data = new StringTokenizer(cd.getFirstChild().getNodeValue());
										FastList<Float> array = new FastList<Float>();
										while (data.hasMoreTokens())
											array.add(Float.parseFloat(data.nextToken()));

										if(tableName.equalsIgnoreCase("#soloValues"))
										{
											soloValues = new float[array.size()];
											int x = 0;
											for(float value : array)
												soloValues[x++] = value;
										}
										else
										{
											combinedValues = new float[array.size()];
											int x = 0;
											for(float value : array)
												combinedValues[x++] = value;
										}
									}

								((ArrayList<augmentationStat>) _augStats[i - 1]).add(new augmentationStat(Stats.valueOfXml(statName), soloValues, combinedValues));
							}
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "Error parsing augmentation_stats" + i + ".xml.", e);
				return;
			}

			try
			{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setValidating(false);
				factory.setIgnoringComments(true);

				File file = new File(Config.DATAPACK_ROOT + "/data/stats/augmentation/augmentation_jewel_stats" + i + ".xml");
				Document doc = factory.newDocumentBuilder().parse(file);

				for(Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
					if("list".equalsIgnoreCase(n.getNodeName()))
						for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
							if("stat".equalsIgnoreCase(d.getNodeName()))
							{
								NamedNodeMap attrs = d.getAttributes();
								String statName = attrs.getNamedItem("name").getNodeValue();
								float soloValues[] = null, combinedValues[] = null;

								for(Node cd = d.getFirstChild(); cd != null; cd = cd.getNextSibling())
									if("table".equalsIgnoreCase(cd.getNodeName()))
									{
										attrs = cd.getAttributes();
										String tableName = attrs.getNamedItem("name").getNodeValue();

										StringTokenizer data = new StringTokenizer(cd.getFirstChild().getNodeValue());
										FastList<Float> array = new FastList<Float>();
										while (data.hasMoreTokens())
											array.add(Float.parseFloat(data.nextToken()));

										if(tableName.equalsIgnoreCase("#soloValues"))
										{
											soloValues = new float[array.size()];
											int x = 0;
											for(float value : array)
												soloValues[x++] = value;
										}
										else
										{
											combinedValues = new float[array.size()];
											int x = 0;
											for(float value : array)
												combinedValues[x++] = value;
										}
									}

								((ArrayList<augmentationStat>) _augAccStats[i - 1]).add(new augmentationStat(Stats.valueOfXml(statName), soloValues, combinedValues));
							}
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "Error parsing jewel augmentation_stats" + i + ".xml.", e);
				return;
			}
		}
	}

	public L2Augmentation generateRandomAugmentation(int lifeStoneLevel, int lifeStoneGrade, L2ItemInstance targetItem)
	{
		switch (targetItem.getItem().getBodyPart())
		{
			case L2Item.SLOT_L_FINGER:
			case L2Item.SLOT_R_FINGER:
			case L2Item.SLOT_L_EAR:
			case L2Item.SLOT_R_EAR:
			case L2Item.SLOT_LR_FINGER:
			case L2Item.SLOT_LR_EAR:
			case L2Item.SLOT_NECK:
				return generateRandomAccessoryAugmentation(lifeStoneLevel, targetItem);
			default:
				return generateRandomWeaponAugmentation(lifeStoneLevel, lifeStoneGrade, targetItem);
		}
	}

	private L2Augmentation generateRandomWeaponAugmentation(int lifeStoneLevel, int lifeStoneGrade, L2ItemInstance targetItem)
	{
		int stat12 = 0;
		int stat34 = 0;
		boolean generateSkill = false;
		boolean generateGlow = false;

		lifeStoneLevel = Math.min(lifeStoneLevel, 9);

		switch (lifeStoneGrade)
		{
			case 0:
				generateSkill = Rnd.chance(Config.AUGMENTATION_NG_SKILL_CHANCE);
				generateGlow = Rnd.chance(Config.AUGMENTATION_NG_GLOW_CHANCE);
				break;
			case 1:
				generateSkill = Rnd.chance(Config.AUGMENTATION_MID_SKILL_CHANCE);
				generateGlow = Rnd.chance(Config.AUGMENTATION_MID_GLOW_CHANCE);
				break;
			case 2:
				generateSkill = Rnd.chance(Config.AUGMENTATION_HIGH_SKILL_CHANCE);
				generateGlow = Rnd.chance(Config.AUGMENTATION_HIGH_GLOW_CHANCE);
				break;
			case 3:
				generateSkill = Rnd.chance(Config.AUGMENTATION_TOP_SKILL_CHANCE);
				generateGlow = Rnd.chance(Config.AUGMENTATION_TOP_GLOW_CHANCE);
				break;
		}

		if(!generateSkill && Rnd.get(1, 100) <= Config.AUGMENTATION_BASESTAT_CHANCE)
			stat34 = Rnd.get(BASESTAT_STR, BASESTAT_MEN);

		int resultColor = Rnd.get(0, 100);
		if(stat34 == 0 && !generateSkill)
			if(resultColor <= 15 * lifeStoneGrade + 40)
				resultColor = 1;
			else
				resultColor = 0;
		else if(resultColor <= 10 * lifeStoneGrade + 5 || stat34 != 0)
			resultColor = 3;
		else if(resultColor <= 10 * lifeStoneGrade + 10)
			resultColor = 1;
		else
			resultColor = 2;

		L2Skill skill = null;
		if(generateSkill)
		{
			switch (resultColor)
			{
				case 1:
					stat34 = (Integer) _blueSkills[lifeStoneLevel].get(Rnd.get(0, _blueSkills[lifeStoneLevel].size() - 1));
					break;
				case 2:
					stat34 = (Integer) _purpleSkills[lifeStoneLevel].get(Rnd.get(0, _purpleSkills[lifeStoneLevel].size() - 1));
					break;
				case 3:
					stat34 = (Integer) _redSkills[lifeStoneLevel].get(Rnd.get(0, _redSkills[lifeStoneLevel].size() - 1));
					break;
			}
			skill = _allSkills.get(stat34).getSkill();
		}

		int offset;
		if(stat34 == 0)
		{
			int temp = Rnd.get(2, 3);
			int colorOffset = resultColor * 10 * STAT_SUBBLOCKSIZE + temp * STAT_BLOCKSIZE + 1;
			offset = lifeStoneLevel * STAT_SUBBLOCKSIZE + colorOffset;

			stat34 = Rnd.get(offset, offset + STAT_SUBBLOCKSIZE - 1);
			if(generateGlow && lifeStoneGrade >= 2)
				offset = lifeStoneLevel * STAT_SUBBLOCKSIZE + (temp - 2) * STAT_BLOCKSIZE + lifeStoneGrade * 10 * STAT_SUBBLOCKSIZE + 1;
			else
				offset = lifeStoneLevel * STAT_SUBBLOCKSIZE + (temp - 2) * STAT_BLOCKSIZE + Rnd.get(0, 1) * 10 * STAT_SUBBLOCKSIZE + 1;
		}
		else if(!generateGlow)
			offset = lifeStoneLevel * STAT_SUBBLOCKSIZE + Rnd.get(0, 1) * STAT_BLOCKSIZE + 1;
		else
			offset = lifeStoneLevel * STAT_SUBBLOCKSIZE + Rnd.get(0, 1) * STAT_BLOCKSIZE + (lifeStoneGrade + resultColor) / 2 * 10 * STAT_SUBBLOCKSIZE + 1;

		stat12 = Rnd.get(offset, offset + STAT_SUBBLOCKSIZE - 1);

		return new L2Augmentation(targetItem, (stat34 << 16) + stat12, skill, true);
	}

	private L2Augmentation generateRandomAccessoryAugmentation(int lifeStoneLevel, L2ItemInstance targetItem)
	{
		int stat12 = 0;
		int stat34 = 0;
		int base = 0;
		int skillsLength = 0;

		lifeStoneLevel = Math.min(lifeStoneLevel, 9);

		switch (targetItem.getItem().getBodyPart())
		{
			case L2Item.SLOT_L_FINGER:
			case L2Item.SLOT_R_FINGER:
			case L2Item.SLOT_L_FINGER | L2Item.SLOT_R_FINGER:
				base = ACC_RING_START + ACC_RING_BLOCKSIZE * lifeStoneLevel;
				skillsLength = ACC_RING_SKILLS;
				break;
			case L2Item.SLOT_L_EAR:
			case L2Item.SLOT_R_EAR:
			case L2Item.SLOT_L_EAR | L2Item.SLOT_R_EAR:
				base = ACC_EAR_START + ACC_EAR_BLOCKSIZE * lifeStoneLevel;
				skillsLength = ACC_EAR_SKILLS;
				break;
			case L2Item.SLOT_NECK:
				base = ACC_NECK_START + ACC_NECK_BLOCKSIZE * lifeStoneLevel;
				skillsLength = ACC_NECK_SKILLS;
				break;
			default:
				return null;
		}

		int resultColor = Rnd.get(0, 3);
		L2Skill skill = null;

		stat12 = Rnd.get(ACC_STAT_SUBBLOCKSIZE);

		if(Rnd.get(1, 100) <= Config.AUGMENTATION_ACC_SKILL_CHANCE)
		{
			stat34 = base + Rnd.get(skillsLength);
			if(_allSkills.containsKey(stat34))
				skill = _allSkills.get(stat34).getSkill();
		}

		if(skill == null)
		{
			stat34 = (stat12 + 1 + Rnd.get(ACC_STAT_SUBBLOCKSIZE - 1)) % ACC_STAT_SUBBLOCKSIZE;
			stat34 = base + skillsLength + ACC_STAT_SUBBLOCKSIZE * resultColor + stat34;
		}

		stat12 = base + skillsLength + ACC_STAT_SUBBLOCKSIZE * resultColor + stat12;

		return new L2Augmentation(targetItem, (stat34 << 16) + stat12, skill, true);
	}

	public class AugStat
	{
		private final Stats _stat;
		private final float _value;

		public AugStat(Stats stat, float value)
		{
			_stat = stat;
			_value = value;
		}

		public Stats getStat()
		{
			return _stat;
		}

		public float getValue()
		{
			return _value;
		}
	}

	public GArray<AugStat> getAugStatsById(int augmentationId)
	{
		GArray<AugStat> temp = new GArray<AugStat>();
		int stats[] = new int[2];
		stats[0] = 0x0000FFFF & augmentationId;
		stats[1] = augmentationId >> 16;

		for(int id : stats)
			if(id >= STAT_START && id <= STAT_END)
			{
				int base = id - STAT_START;
				int color = base / STAT_BLOCKSIZE;
				int subblock = base % STAT_BLOCKSIZE;
				int level = subblock / STAT_SUBBLOCKSIZE;
				int stat = subblock % STAT_SUBBLOCKSIZE;

				byte stat1 = STATS1_MAP[stat];
				byte stat2 = STATS2_MAP[stat];
				if(stat1 == stat2)
				{
					augmentationStat as = (augmentationStat) _augStats[color].get(stat1);
					temp.add(new AugStat(as.getStat(), as.getSingleStatValue(level)));
				}
				else
				{
					augmentationStat as = (augmentationStat) _augStats[color].get(stat1);
					temp.add(new AugStat(as.getStat(), as.getCombinedStatValue(level)));
					as = (augmentationStat) _augStats[color].get(stat2);
					temp.add(new AugStat(as.getStat(), as.getCombinedStatValue(level)));
				}
			}
			else if(id >= BASESTAT_STR && id <= BASESTAT_MEN)
				switch (id)
				{
					case BASESTAT_STR:
						temp.add(new AugStat(Stats.STAT_STR, 1.0f));
						break;
					case BASESTAT_CON:
						temp.add(new AugStat(Stats.STAT_CON, 1.0f));
						break;
					case BASESTAT_INT:
						temp.add(new AugStat(Stats.STAT_INT, 1.0f));
						break;
					case BASESTAT_MEN:
						temp.add(new AugStat(Stats.STAT_MEN, 1.0f));
						break;
				}
			else if(id >= ACC_START && id <= ACC_END)
			{
				int base, level, subblock;

				if(id <= ACC_RING_END)
				{
					base = id - ACC_RING_START;
					level = base / ACC_RING_BLOCKSIZE;
					subblock = base % ACC_RING_BLOCKSIZE - ACC_RING_SKILLS;
				}
				else if(id <= ACC_EAR_END)
				{
					base = id - ACC_EAR_START;
					level = base / ACC_EAR_BLOCKSIZE;
					subblock = base % ACC_EAR_BLOCKSIZE - ACC_EAR_SKILLS;
				}
				else
				{
					base = id - ACC_NECK_START;
					level = base / ACC_NECK_BLOCKSIZE;
					subblock = base % ACC_NECK_BLOCKSIZE - ACC_NECK_SKILLS;
				}

				if(subblock >= 0)
				{
					int color = subblock / ACC_STAT_SUBBLOCKSIZE;
					int stat = subblock % ACC_STAT_SUBBLOCKSIZE;
					byte stat1 = ACC_STATS1_MAP[stat];
					byte stat2 = ACC_STATS2_MAP[stat];
					if(stat1 == stat2)
					{
						augmentationStat as = (augmentationStat) _augAccStats[color].get(stat1);
						temp.add(new AugStat(as.getStat(), as.getSingleStatValue(level)));
					}
					else
					// combined
					{
						augmentationStat as = (augmentationStat) _augAccStats[color].get(stat1);
						temp.add(new AugStat(as.getStat(), as.getCombinedStatValue(level)));
						as = (augmentationStat) _augAccStats[color].get(stat2);
						temp.add(new AugStat(as.getStat(), as.getCombinedStatValue(level)));
					}
				}
			}

		return temp;
	}

	public L2Skill getAugSkillById(int augmentationId)
	{
		final augmentationSkill temp = _allSkills.get(augmentationId);
		if(temp == null)
			return null;
		return temp.getSkill();
	}
}
