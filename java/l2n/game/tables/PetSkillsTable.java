package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Summon;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 26.10.2009
 * @time 3:36:31
 */
public class PetSkillsTable
{
	private static final Logger _log = Logger.getLogger(PetSkillsTable.class.getName());

	private final TIntObjectHashMap<GArray<L2PetSkillLearn>> _regular_skills;

	public static PetSkillsTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private PetSkillsTable()
	{
		_regular_skills = new TIntObjectHashMap<GArray<L2PetSkillLearn>>();
		load();
	}

	private void load()
	{
		int npcId = 0;
		int count = 0;
		int id = 0;
		int lvl = 0;
		int minLvl = 0;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		FiltredStatement statement2 = null;
		ResultSet petlist = null;
		ResultSet skilltree = null;
		GArray<Integer> badSkill = new GArray<Integer>();
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			petlist = statement.executeQuery("SELECT id, name FROM npc WHERE type = 'L2Pet' ORDER BY id");

			while (petlist.next())
			{
				GArray<L2PetSkillLearn> map = new GArray<L2PetSkillLearn>();
				npcId = petlist.getInt("id");
				try
				{
					statement2 = con.createStatement();
					skilltree = statement2.executeQuery("SELECT minLvl, skillId, skillLvl FROM pets_skills where templateId=" + npcId + " ORDER BY skillId, skillLvl");

					while (skilltree.next())
					{
						id = skilltree.getInt("skillId");
						lvl = skilltree.getInt("skillLvl");
						minLvl = skilltree.getInt("minLvl");

						L2Skill skill = SkillTable.getInstance().getInfo(id, 1);
						if(skill == null || skill.getSkillType() == SkillType.NOTDONE)
						{
							if(!badSkill.contains(id))
								badSkill.add(id);
							continue;
						}

						L2PetSkillLearn skillLearn = new L2PetSkillLearn(id, lvl, minLvl);
						map.add(skillLearn);
					}

					_regular_skills.put(npcId, map);
				}
				catch(Exception e)
				{
					_log.log(Level.SEVERE, "Error while creating pet skill tree (Pet ID: " + npcId + ", skillId: " + id + ", level: " + lvl + ")", e);
				}
				finally
				{
					DbUtils.closeQuietly(statement2, skilltree);
				}

				count += map.size();
			}
		}
		catch(Exception e)
		{
			_log.log(Level.SEVERE, "Error while creating pet skill tree (Pet ID " + npcId + ")", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, petlist);
		}

		for(int badId : badSkill)
			Log.addDev("[" + badId + "] " + SkillTable.getInstance().getInfo(badId, 1), "dev_notdone_pet_skills", false);
		_log.info("PetSkillsTable: loaded " + count + " skills.");
	}

	public int getAvailableLevel(L2Summon cha, int skillId)
	{
		int lvl = 0;
		if(!_regular_skills.containsKey(cha.getNpcId()))
			return lvl;

		GArray<L2PetSkillLearn> skills = _regular_skills.get(cha.getNpcId());
		for(L2PetSkillLearn temp : skills)
		{
			if(temp.getId() != skillId)
				continue;
			if(temp.getLevel() == 0)
			{
				if(cha.getLevel() < 70)
				{
					lvl = cha.getLevel() / 10;
					if(lvl <= 0)
						lvl = 1;
				}
				else
					lvl = 7 + (cha.getLevel() - 70) / 5;

				lvl = Math.min(lvl, SkillTable.getInstance().getMaxLevel(temp.getId()));
				break;
			}
			else if(temp.getMinLevel() <= cha.getLevel() && temp.getLevel() > lvl)
				lvl = temp.getLevel();
		}
		return lvl;
	}

	public GArray<L2PetSkillLearn> getSkills(int summonId)
	{
		return _regular_skills.get(summonId);
	}

	public static final class L2PetSkillLearn
	{
		private final int _id;
		private final int _level;
		private final int _minLevel;
		private final int _hashCode;

		public L2PetSkillLearn(int id, int lvl, int minLvl)
		{
			_id = id;
			_level = lvl;
			_minLevel = minLvl;
			_hashCode = _id * 1023 + _level;
		}

		public int getId()
		{
			return _id;
		}

		public int getLevel()
		{
			return _level;
		}

		public int getMinLevel()
		{
			return _minLevel;
		}

		@Override
		public int hashCode()
		{
			return _hashCode;
		}

		public final L2Skill getSkill()
		{
			return SkillTable.getInstance().getInfo(_id, _level);
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final PetSkillsTable _instance = new PetSkillsTable();
	}
}
