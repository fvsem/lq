package l2n.game.tables;

import l2n.game.model.L2Skill;

import java.util.logging.Logger;

public class HeroSkillTable
{
	private static final L2Skill[] _heroSkills = new L2Skill[5];
	private static HeroSkillTable _instance;

	protected static final Logger _log = Logger.getLogger(HeroSkillTable.class.getName());

	private HeroSkillTable()
	{
		_heroSkills[0] = SkillTable.getInstance().getInfo(395, 1);
		_heroSkills[1] = SkillTable.getInstance().getInfo(396, 1);
		_heroSkills[2] = SkillTable.getInstance().getInfo(1374, 1);
		_heroSkills[3] = SkillTable.getInstance().getInfo(1375, 1);
		_heroSkills[4] = SkillTable.getInstance().getInfo(1376, 1);

		_log.info("HeroSkillTable: loaded " + _heroSkills.length + " hero skills.");
	}

	public static HeroSkillTable getInstance()
	{
		if(_instance == null)
			_instance = new HeroSkillTable();
		return _instance;
	}

	public static L2Skill[] getHeroSkills()
	{
		return _heroSkills;
	}
}
