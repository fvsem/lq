package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TypeFormat;
import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.model.base.L2PledgeSkillLearn;
import l2n.game.tables.SkillTable.SubclassSkills;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SkillTreeTable
{
	public static final int NORMAL_ENCHANT_COST_MULTIPLIER = 1;
	public static final int SAFE_ENCHANT_COST_MULTIPLIER = 5;

	public static final int NORMAL_ENCHANT_BOOK = 6622;
	public static final int SAFE_ENCHANT_BOOK = 9627;
	public static final int CHANGE_ENCHANT_BOOK = 9626;
	public static final int UNTRAIN_ENCHANT_BOOK = 9625;

	private static final L2SkillLearn[] EMPTY_SKILL_LEARN = new L2SkillLearn[0];
	private static final L2EnchantSkillLearn[] EMPTY_ENCHANT_SKILL_LEARN = new L2EnchantSkillLearn[0];

	private final static String LOAD_SKILL_TREES_PLAYER = "SELECT class_id, skill_id, level, name, sp, min_level FROM skill_trees_player where class_id=? ORDER BY skill_id, level";
	private final static String LOAD_SKILL_TREES_TRANSFORMATION = "SELECT race_id, skill_id, item_id, level, name, sp, min_level FROM skill_trees_transform ORDER BY race_id, skill_id, level";
	private final static String LOAD_SKILL_TREES_PLEDGE = "SELECT skill_id, level, name, clan_lvl, repCost, itemId, itemCount FROM skill_trees_pledge ORDER BY skill_id, level";
	private final static String LOAD_SKILL_TREES_SQUAD = "SELECT skill_id, level, name, clan_lvl, repCost, itemId, itemCount FROM skill_trees_squad ORDER BY skill_id, level";
	private final static String LOAD_SKILL_TREES_FISHING = "SELECT skill_id, level, name, sp, min_level, costid, cost FROM skill_trees_fishing ORDER BY skill_id, level";
	private final static String LOAD_SKILL_TREES_SPECIAL = "SELECT skill_id, level, name, costid, cost FROM skill_trees_special ORDER BY skill_id, level";

	private static final Logger _log = Logger.getLogger(SkillTreeTable.class.getName());

	public final static TIntObjectHashMap<GArray<L2EnchantSkillLearn>> _enchantSkillTrees = new TIntObjectHashMap<GArray<L2EnchantSkillLearn>>();

	private final FastMap<ClassId, GArray<L2SkillLearn>> _skillTrees;
	private ArrayList<TIntObjectHashMap<TIntObjectHashMap<L2SkillLearn>>> _skillCostTable;
	private final GArray<L2SkillLearn> _fishingSkills; // скилы рыбалки
	private final GArray<L2SkillLearn> _transformationSkills; // скилы трансформаций
	private final GArray<L2SkillLearn> _specialSkillTrees; // специальные скилы

	private final GArray<L2PledgeSkillLearn> _clanSkills; // клан скилы
	private final GArray<L2PledgeSkillLearn> _squadSkills; // скилы отрядов

	private final GArray<L2SkillLearn> _transferSkills_b;
	private final GArray<L2SkillLearn> _transferSkills_ee;
	private final GArray<L2SkillLearn> _transferSkills_se;

	public static SkillTreeTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private final L2SkillLearn getSkillLearn(final int skillId, final ClassId classId, final int skillLvl)
	{
		for(final L2SkillLearn tmp : _skillTrees.get(classId))
			if(tmp.id == skillId && tmp.skillLevel == skillLvl)
				return tmp;
		return null;
	}

	public boolean checkSkill(final L2Player player, final L2Skill skill, final int maxDiff)
	{
		final L2SkillLearn learn = getSkillLearn(skill.getId(), player.getClassId(), levelWithoutEnchant(skill));
		if(learn == null)
			return false;

		// сбрасываем заточеные скилы
		if(!player.isGM() && skill.getLevel() > 100 && (player.getLevel() < 70 || player.getClassId().getLevel() < 4))
		{
			player.removeSkill(skill, true);
			final L2Skill newSkill = SkillTable.getInstance().getInfo(skill.getId(), skill.getBaseLevel());
			if(newSkill != null)
				player.addSkill(newSkill, true);
		}

		if(learn.getMinLevel() > player.getLevel() + maxDiff)
		{
			player.removeSkill(skill, true);

			// если у нас низкий лвл для скила, то заточка обнуляется 100%
			// и ищем от большего к меньшему подходящий лвл для скила
			for(int i = skill.getBaseLevel(); i != 0; i--)
			{
				final L2SkillLearn learn2 = getSkillLearn(skill.getId(), player.getClassId(), i);
				if(learn2 == null)
					continue;
				if(learn2.getMinLevel() > player.getLevel() + maxDiff)
					continue;

				final L2Skill newSkill = SkillTable.getInstance().getInfo(skill.getId(), i);
				if(newSkill != null)
				{
					player.addSkill(newSkill, true);
					return true;
				}
			}
		}
		return false;
	}

	private static int levelWithoutEnchant(final L2Skill skill)
	{
		return skill.getDisplayLevel() > 100 ? skill.getBaseLevel() : skill.getLevel();
	}

	private SkillTreeTable()
	{
		_skillTrees = new FastMap<ClassId, GArray<L2SkillLearn>>();
		_fishingSkills = new GArray<L2SkillLearn>(117);
		_transformationSkills = new GArray<L2SkillLearn>(50);
		_specialSkillTrees = new GArray<L2SkillLearn>(6);
		_clanSkills = new GArray<L2PledgeSkillLearn>(44);
		_squadSkills = new GArray<L2PledgeSkillLearn>(18);

		int classintid = 0;
		int count = 0;

		ThreadConnection con = null;
		FiltredPreparedStatement classliststatement = null;
		FiltredPreparedStatement skilltreestatement = null;
		ResultSet classlist = null, skilltree = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			classliststatement = con.prepareStatement("SELECT * FROM class_list ORDER BY id");
			skilltreestatement = con.prepareStatement(LOAD_SKILL_TREES_PLAYER);
			classlist = classliststatement.executeQuery();

			while (classlist.next())
			{
				final GArray<L2SkillLearn> list = new GArray<L2SkillLearn>();
				classintid = classlist.getInt("id");
				ClassId classId = ClassId.values()[classintid];

				skilltreestatement.setInt(1, classintid);
				skilltree = skilltreestatement.executeQuery();

				while (skilltree.next())
				{
					final short id = skilltree.getShort("skill_id");
					final byte lvl = skilltree.getByte("level");
					final String name = skilltree.getString("name");
					final int cost = skilltree.getInt("sp");
					final byte minLvl = skilltree.getByte("min_level");

					short itemId = 0;
					int itemCount = 0;

					final FiltredPreparedStatement statement2 = con.prepareStatement("SELECT item_id, item_count FROM skill_spellbooks WHERE skill_id=? AND level=?");
					statement2.setInt(1, id);
					statement2.setInt(2, lvl);
					final ResultSet itemIdCount = statement2.executeQuery();
					if(itemIdCount.next())
					{
						itemId = itemIdCount.getShort("item_id");
						itemCount = itemIdCount.getInt("item_count");
					}

					DbUtils.closeQuietly(statement2, itemIdCount);

					final L2SkillLearn skillLearn = new L2SkillLearn(id, lvl, minLvl, name, cost, itemId, itemCount, skilltree.getInt("class_id") == -1, skilltree.getInt("class_id") == -2, skilltree.getInt("class_id") == -4);
					list.add(skillLearn);
				}

				_skillTrees.put(ClassId.values()[classintid], list);
				count += list.size();

				ClassId secondparent = classId.getParent(1);
				if(secondparent == classId.getParent(0))
					secondparent = null;

				classId = classId.getParent(0);
				while (classId != null)
				{
					final GArray<L2SkillLearn> parentList = _skillTrees.get(classId);
					list.addAll(parentList);
					classId = classId.getParent(0);
					if(classId == null && secondparent != null)
					{
						classId = secondparent;
						secondparent = secondparent.getParent(1);
					}
				}
			}

			DbUtils.closeQuietly(classliststatement, classlist);
			classliststatement = null;
			classlist = null;
			DbUtils.closeQuietly(skilltreestatement, skilltree);

			loadFishingSkills(con);
			loadTransformationSkills(con);
			loadClanSkills(con);
			loadSquadSkills(con);
			loadSpecialSkills(con);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "error while creating skill tree for classId " + classintid, e);
		}
		finally
		{
			DbUtils.closeQuietly(classliststatement, classlist);
			DbUtils.closeQuietly(skilltreestatement, skilltree);
			DbUtils.close(con);
		}

		loadSkillCostTable();

		_log.config("SkillTreeTable: Loaded " + count + " skills.");
		_log.config("SkillTreeTable: Loaded " + _fishingSkills.size() + " fishing skills.");
		_log.config("SkillTreeTable: Loaded " + _transformationSkills.size() + " transformation skills.");
		_log.config("SkillTreeTable: Loaded " + _clanSkills.size() + " clan skills.");
		_log.config("SkillTreeTable: Loaded " + _squadSkills.size() + " squad skills.");
		_log.config("SkillTreeTable: Loaded " + _enchantSkillTrees.size() + " enchanted skills.");
		_log.config("SkillTreeTable: Loaded " + _specialSkillTrees.size() + " special skills.");

		_transferSkills_b = new GArray<L2SkillLearn>();
		_transferSkills_ee = new GArray<L2SkillLearn>();
		_transferSkills_se = new GArray<L2SkillLearn>();

		loadTransferSkills(_transferSkills_b, ClassId.bishop);
		loadTransferSkills(_transferSkills_ee, ClassId.elder);
		loadTransferSkills(_transferSkills_se, ClassId.shillienElder);
	}

	private void loadTransferSkills(final GArray<L2SkillLearn> dest, final ClassId classId)
	{
		for(final L2SkillLearn temp : _skillTrees.get(classId))
			if(temp.getLevel() == SkillTable.getInstance().getBaseLevel(temp.id) && temp.id != 1520 && temp.id != 1521 && temp.id != 1522)
				dest.add(temp);
	}

	private void loadFishingSkills(final ThreadConnection con) throws SQLException
	{
		FiltredPreparedStatement statement = null;
		ResultSet skilltree = null;
		try
		{
			statement = con.prepareStatement(LOAD_SKILL_TREES_FISHING);
			skilltree = statement.executeQuery();
			while (skilltree.next())
			{
				final short id = skilltree.getShort("skill_id");
				final short lvl = skilltree.getShort("level");
				final String name = skilltree.getString("name");

				final byte minLvl = skilltree.getByte("min_level");
				final int cost = skilltree.getInt("sp");
				final short costId = skilltree.getShort("costid");
				final int costCount = skilltree.getInt("cost");

				final L2SkillLearn skill = new L2SkillLearn(id, lvl, minLvl, name, cost, costId, costCount, true, false, false);
				_fishingSkills.add(skill);
			}
		}
		finally
		{
			DbUtils.closeQuietly(statement, skilltree);
		}
	}

	private void loadTransformationSkills(final ThreadConnection con) throws SQLException
	{
		FiltredPreparedStatement statement = null;
		ResultSet skilltree = null;
		try
		{
			statement = con.prepareStatement(LOAD_SKILL_TREES_TRANSFORMATION);
			skilltree = statement.executeQuery();

			while (skilltree.next())
			{
				final int race_id = skilltree.getInt("race_id");
				final int id = skilltree.getInt("skill_id");
				final int item_id = skilltree.getInt("item_id");
				final int lvl = skilltree.getInt("level");
				final String name = skilltree.getString("name");
				final int sp = skilltree.getInt("sp");
				final int minLvl = skilltree.getInt("min_level");

				final L2SkillLearn skl = new L2SkillLearn(id, lvl, (byte) minLvl, name, sp, item_id, 1, false, false, true, race_id);
				_transformationSkills.add(skl);
			}
		}
		finally
		{
			DbUtils.closeQuietly(statement, skilltree);
		}
	}

	/**
	 * загрузка клановых умений
	 */
	private void loadClanSkills(final ThreadConnection con) throws SQLException
	{
		FiltredPreparedStatement statement = null;
		ResultSet skilltree = null;
		try
		{
			statement = con.prepareStatement(LOAD_SKILL_TREES_PLEDGE);
			skilltree = statement.executeQuery();
			while (skilltree.next())
			{
				final int id = skilltree.getInt("skill_id");
				final int lvl = skilltree.getInt("level");
				final String name = skilltree.getString("name");
				final int clanLvl = skilltree.getInt("clan_lvl");
				final int sp = skilltree.getInt("repCost");
				final int itemId = skilltree.getInt("itemId");
				final int itemCount = skilltree.getInt("itemCount");

				final L2PledgeSkillLearn skill = new L2PledgeSkillLearn(id, lvl, clanLvl, name, sp, itemId, itemCount);
				_clanSkills.add(skill);
			}

		}
		finally
		{
			DbUtils.closeQuietly(statement, skilltree);
		}
	}

	/**
	 * загрузка умений отрядов
	 */
	private void loadSquadSkills(final ThreadConnection con) throws SQLException
	{
		FiltredPreparedStatement statement = null;
		ResultSet skilltree = null;
		try
		{
			statement = con.prepareStatement(LOAD_SKILL_TREES_SQUAD);
			skilltree = statement.executeQuery();
			while (skilltree.next())
			{
				final int id = skilltree.getInt("skill_id");
				final int lvl = skilltree.getInt("level");
				final String name = skilltree.getString("name");
				final int clanLvl = skilltree.getInt("clan_lvl");
				final int sp = skilltree.getInt("repCost");
				final int itemId = skilltree.getInt("itemId");
				final int itemCount = skilltree.getInt("itemCount");

				final L2PledgeSkillLearn skill = new L2PledgeSkillLearn(id, lvl, clanLvl, name, sp, itemId, itemCount);
				_squadSkills.add(skill);
			}
		}
		finally
		{
			DbUtils.closeQuietly(statement, skilltree);
		}
	}

	/**
	 * загрузка умений отрядов
	 */
	private void loadSpecialSkills(final ThreadConnection con) throws SQLException
	{
		FiltredPreparedStatement statement = null;
		ResultSet skilltree = null;
		try
		{
			statement = con.prepareStatement(LOAD_SKILL_TREES_SPECIAL);
			skilltree = statement.executeQuery();
			while (skilltree.next())
			{
				final int id = skilltree.getInt("skill_id");
				final int lvl = skilltree.getInt("level");
				final String name = skilltree.getString("name");
				final int costId = skilltree.getInt("costid");
				final int costCount = skilltree.getInt("cost");

				final L2SkillLearn skill = new L2SkillLearn(id, lvl, (byte) 0, name, 0, costId, costCount, false, false, false, -1);
				_specialSkillTrees.add(skill);
			}
		}
		finally
		{
			DbUtils.closeQuietly(statement, skilltree);
		}
	}

	private void loadSkillCostTable()
	{
		_skillCostTable = new ArrayList<TIntObjectHashMap<TIntObjectHashMap<L2SkillLearn>>>(ClassId.values().length + 1);
		for(final ClassId cid : ClassId.values())
			_skillCostTable.add(cid.getId(), new TIntObjectHashMap<TIntObjectHashMap<L2SkillLearn>>());
		for(final ClassId classId : _skillTrees.keySet())
		{
			final TIntObjectHashMap<TIntObjectHashMap<L2SkillLearn>> skt = _skillCostTable.get(classId.getId());

			final GArray<L2SkillLearn> lst = _skillTrees.get(classId);
			for(final L2SkillLearn skl : lst)
			{
				TIntObjectHashMap<L2SkillLearn> skillmap = skt.get(skl.getId());
				if(skillmap == null)
				{
					skillmap = new TIntObjectHashMap<L2SkillLearn>();
					skt.put(skl.getId(), skillmap);
				}
				skillmap.put(skl.getLevel(), skl);
			}
		}
	}

	public L2SkillLearn[] getAvailableSkills(final L2Player cha, final ClassId classId)
	{
		final GArray<L2SkillLearn> skills = _skillTrees.get(classId);
		if(skills == null)
		{
			// the skilltree for this class is undefined, so we give an empty list
			_log.warning("Skilltree for class " + classId + " is not defined !");
			return EMPTY_SKILL_LEARN;
		}

		final GArray<L2SkillLearn> result = new GArray<L2SkillLearn>();
		final L2Skill[] oldSkills = cha.getAllSkillsArray();
		for(final L2SkillLearn temp : skills)
			if(temp.getMinLevel() <= cha.getLevel())
			{
				boolean knownSkill = false;

				for(int j = 0; j < oldSkills.length && !knownSkill; j++)
				{
					if(oldSkills[j] == null)
						continue;

					if(oldSkills[j].getId() == temp.getId())
					{
						knownSkill = true;

						// this is the next level of a skill that we know
						if(oldSkills[j].getLevel() == temp.getLevel() - 1)
							result.add(temp);
					}
				}

				// this is a new skill
				if(!knownSkill && temp.getLevel() == 1)
					result.add(temp);
			}

		return result.toArray(new L2SkillLearn[result.size()]);
	}

	public L2SkillLearn[] getAvailableSpecialSkills(final L2Player cha)
	{
		final GArray<L2SkillLearn> skills = new GArray<L2SkillLearn>();
		skills.addAll(_specialSkillTrees);
		if(skills.size() < 1)
		{
			// the skilltree for this class is undefined, so we give an empty list
			_log.warning("Skilltree for special is not defined !");
			return EMPTY_SKILL_LEARN;
		}

		final GArray<L2SkillLearn> result = new GArray<L2SkillLearn>();
		final L2Skill[] oldSkills = cha.getAllSkillsArray();
		for(final L2SkillLearn temp : skills)
		{
			boolean knownSkill = false;

			for(int j = 0; j < oldSkills.length && !knownSkill; j++)
				if(oldSkills[j].getId() == temp.getId())
				{
					knownSkill = true;

					if(oldSkills[j].getLevel() == temp.getLevel() - 1)
						// this is the next level of a skill that we know
						result.add(temp);
				}

			if(!knownSkill && temp.getLevel() == 1)
				// this is a new skill
				result.add(temp);
		}

		return result.toArray(new L2SkillLearn[result.size()]);
	}

	public GArray<L2SkillLearn> getAvailableTransferSkills(final L2Player cha, final ClassId classId)
	{
		GArray<L2SkillLearn> skills;
		switch (classId)
		{
			case cardinal:
				skills = _transferSkills_b;
				break;
			case evaSaint:
				skills = _transferSkills_ee;
				break;
			case shillienSaint:
				skills = _transferSkills_se;
				break;
			default:
				return new GArray<L2SkillLearn>(0);
		}

		final L2Skill[] oldSkills = cha.getAllSkillsArray();
		final GArray<L2SkillLearn> result = new GArray<L2SkillLearn>();

		for(final L2SkillLearn temp : skills)
			if(temp.minLevel <= cha.getLevel())
			{
				boolean knownSkill = false;
				for(final L2Skill s : oldSkills)
				{
					if(s.getId() != temp.id)
						continue;
					knownSkill = true;
					break;
				}
				if(!knownSkill)
					result.add(temp);
			}
		return result;
	}

	public L2PledgeSkillLearn[] getAvailablePledgeSkills(final L2Clan clan)
	{
		if(_clanSkills == null)
		{
			// the skilltree for this class is undefined, so we give an empty list
			_log.warning("No clan skills defined!");
			return new L2PledgeSkillLearn[0];
		}

		final GArray<L2PledgeSkillLearn> result = new GArray<L2PledgeSkillLearn>();
		final L2Skill[] oldSkills = clan.getAllSkills();
		for(final L2PledgeSkillLearn temp : _clanSkills)
			if(temp.getBaseLevel() <= clan.getLevel())
			{
				boolean knownSkill = false;

				for(int j = 0; j < oldSkills.length && !knownSkill; j++)
					if(oldSkills[j].getId() == temp.getId())
					{
						knownSkill = true;

						if(oldSkills[j].getLevel() == temp.getLevel() - 1)
							// this is the next level of a skill that we know
							result.add(temp);
					}

				if(!knownSkill && temp.getLevel() == 1)
					// this is a new skill
					result.add(temp);
			}

		return result.toArray(new L2PledgeSkillLearn[result.size()]);
	}

	public GArray<L2PledgeSkillLearn> getAvailableSquadSkills(final L2Clan clan)
	{
		if(_squadSkills.isEmpty())
		{
			// the skilltree for this class is undefined, so we give an empty list
			_log.warning("Skilltree for squad skills is not defined !");
			return new GArray<L2PledgeSkillLearn>(0);
		}

		final GArray<L2PledgeSkillLearn> result = new GArray<L2PledgeSkillLearn>();
		final GArray<L2Skill> oldSkills = new GArray<L2Skill>();
		for(final TIntObjectHashMap<L2Skill> map : clan.getSquadSkills().valueCollection())
			oldSkills.addAll(map.valueCollection());

		for(final L2PledgeSkillLearn temp : _squadSkills)
		{
			// make sure clan meets requirement to learn new skill
			if(clan.getLevel() < temp.getBaseLevel())
				continue;
			boolean knownSkill = false;
			for(final L2Skill skill : oldSkills)
			{
				if(knownSkill)
					break;
				if(temp.getId() == skill.getId())
				{
					knownSkill = true;
					if(skill.getLevel() == temp.getLevel() - 1)
						// this is the next level of a skill that we know
						result.add(temp);
				}
			}
			if(!knownSkill && temp.getLevel() == 1)
				// this is a new skill
				result.add(temp);
		}
		return result;
	}

	public L2PledgeSkillLearn getSquadSkill(final int skillId, final int skillLvl)
	{
		for(final L2PledgeSkillLearn skill : _squadSkills)
			if(skill.getId() == skillId && skill.getLevel() == skillLvl)
				return skill;
		return null;
	}

	public L2SkillLearn[] getAvailableTransformationSkills(final L2Player cha)
	{
		if(_transformationSkills == null)
		{
			_log.warning("Transformation skills not defined!");
			return EMPTY_SKILL_LEARN;
		}

		final GArray<L2SkillLearn> result = new GArray<L2SkillLearn>();
		final L2Skill[] oldSkills = cha.getAllSkillsArray();
		for(final L2SkillLearn temp : _transformationSkills)
			if(temp.minLevel <= cha.getLevel() && (temp.raceId == cha.getRace().ordinal() || temp.raceId == -1))
			{
				boolean knownSkill = false;
				for(final L2Skill s : oldSkills)
				{
					if(knownSkill)
						break;
					if(s.getId() == temp.id)
					{
						knownSkill = true;
						if(s.getLevel() == temp.skillLevel - 1)
							// this is the next level of a skill that we know
							result.add(temp);
					}
				}

				if(!knownSkill && temp.skillLevel == 1)
					// this is a new skill
					result.add(temp);
			}
		return result.toArray(new L2SkillLearn[result.size()]);
	}

	public L2SkillLearn[] getAvailableFishingSkills(final L2Player cha)
	{
		if(_fishingSkills == null)
		{
			_log.warning("Fishing skills not defined!");
			return EMPTY_SKILL_LEARN;
		}

		final L2Skill[] oldSkills = cha.getAllSkillsArray();
		final GArray<L2SkillLearn> result = new GArray<L2SkillLearn>();
		for(final L2SkillLearn temp : _fishingSkills)
			if(temp.minLevel <= cha.getLevel())
			{
				boolean knownSkill = false;
				for(final L2Skill s : oldSkills)
				{
					if(knownSkill)
						break;
					if(s.getId() == temp.id)
					{
						knownSkill = true;
						if(s.getLevel() == temp.skillLevel - 1)
							result.add(temp);
					}
				}

				if(!knownSkill && temp.skillLevel == 1)
					result.add(temp);
			}
		return result.toArray(new L2SkillLearn[result.size()]);
	}

	public L2EnchantSkillLearn[] getFirstEnchantsForSkill(final int skillid)
	{
		final GArray<L2EnchantSkillLearn> enchants = _enchantSkillTrees.get(skillid);
		if(enchants == null)
			return EMPTY_ENCHANT_SKILL_LEARN;

		final GArray<L2EnchantSkillLearn> result = new GArray<L2EnchantSkillLearn>();
		for(final L2EnchantSkillLearn e : enchants)
			if(e.getLevel() % 100 == 1)
				result.add(e);
		return result.toArray(new L2EnchantSkillLearn[result.size()]);
	}

	public L2EnchantSkillLearn[] getEnchantsForChange(final int skillid, final int level)
	{
		final GArray<L2EnchantSkillLearn> enchants = _enchantSkillTrees.get(skillid);
		if(enchants == null)
			return EMPTY_ENCHANT_SKILL_LEARN;

		final GArray<L2EnchantSkillLearn> result = new GArray<L2EnchantSkillLearn>();
		for(final L2EnchantSkillLearn e : enchants)
			if(e.getLevel() % 100 == level % 100)
				result.add(e);
		return result.toArray(new L2EnchantSkillLearn[result.size()]);
	}

	public final static boolean isEnchantable(final L2Skill skill)
	{
		if(SkillTable.getInstance().isEnchantable(skill.getId()))
		{
			final GArray<L2EnchantSkillLearn> enchants = _enchantSkillTrees.get(skill.getId());
			if(enchants == null)
				return false;
			for(final L2EnchantSkillLearn e : enchants)
				if(e.getBaseLevel() <= skill.getLevel())
					return true;
		}
		return false;
	}

	public L2EnchantSkillLearn getSkillEnchant(final int skillid, final int level)
	{
		final GArray<L2EnchantSkillLearn> enchants = _enchantSkillTrees.get(skillid);
		if(enchants == null)
			return null;

		for(final L2EnchantSkillLearn e : enchants)
			if(e.getLevel() == level)
				return e;
		return null;
	}

	public int getMinLevelForNewSkill(final L2Player cha, final ClassId classId)
	{
		int minLevel = 0;
		final GArray<L2SkillLearn> skills = _skillTrees.get(classId);
		if(skills == null)
		{
			// the skilltree for this class is undefined, so we give an empty list
			_log.warning("Skilltree for class " + classId + " is not defined !");
			return minLevel;
		}

		for(final L2SkillLearn temp : skills)
			if(temp.getMinLevel() > cha.getLevel() && temp.getSpCost() != 0)
				if(minLevel == 0 || temp.getMinLevel() < minLevel)
					minLevel = temp.getMinLevel();

		return minLevel;
	}

	public int getSkillRepCost(final L2Clan clan, final L2Skill skill)
	{
		int min = 100000000;
		int lvl = clan.getLeader().getPlayer().getSkillLevel(skill.getId());

		if(lvl > 0)
			lvl += 1;
		else
			lvl = 1;
		if(_clanSkills != null)
			for(final L2PledgeSkillLearn tmp : _clanSkills)
			{
				if(tmp.getId() != skill.getId())
					continue;
				if(tmp.getLevel() != lvl)
					continue;
				if(tmp.getBaseLevel() > clan.getLevel())
					continue;
				min = Math.min(min, Math.round(tmp.getRepCost()));
			}
		return min;
	}

	public int getSkillCost(final L2Player player, final L2Skill skill)
	{
		// Скилы трансформации
		if(skill.getSkillType() == L2Skill.SkillType.TRANSFORMATION)
			return 0;

		// TODO снести этот костыль
		switch (skill.getId())
		{
		// Рыбацкие скилы
			case 1312:
			case 1313:
			case 1314:
			case 1315:
			case 1368:
			case 1369:
			case 1370:
			case 1371:
			case 1372:
				return 0;
		}

		final TIntObjectHashMap<TIntObjectHashMap<L2SkillLearn>> skt = _skillCostTable.get(player.getActiveClassId());
		if(skt == null)
			return Integer.MAX_VALUE;

		final TIntObjectHashMap<L2SkillLearn> skillmap = skt.get(skill.getId());
		if(skillmap == null)
			return Integer.MAX_VALUE;

		final L2SkillLearn skl = skillmap.get(1 + Math.max(player.getSkillLevel(skill.getId()), 0));
		if(skl == null)
			return Integer.MAX_VALUE;

		return skl.getSpCost();
	}

	// TODO оптимизировать
	public final L2SkillLearn getSkillLearn(final int skillid, final int level, final ClassId classid, final L2Clan clan, final boolean isTransfer, final int squadIndex)
	{
		if(isTransfer)
		{
			for(final L2SkillLearn tmp : _transferSkills_b)
				if(tmp.id == skillid)
					return tmp;
			for(final L2SkillLearn tmp : _transferSkills_ee)
				if(tmp.id == skillid)
					return tmp;
			for(final L2SkillLearn tmp : _transferSkills_se)
				if(tmp.id == skillid)
					return tmp;
			return null;
		}

		if(clan != null)
		{
			final L2PledgeSkillLearn[] clskills = getAvailablePledgeSkills(clan);
			for(final L2PledgeSkillLearn tmp : clskills)
				if(tmp.getId() == skillid && tmp.getLevel() == level)
				{
					final L2SkillLearn skill = new L2SkillLearn(tmp.getId(), tmp.getLevel(), (byte) 0, tmp.getName(), tmp.getRepCost(), tmp.getItemId(), tmp.getItemCount(), false, true, false);
					return skill;
				}

			if(squadIndex >= 0)
			{
				final GArray<L2PledgeSkillLearn> skills = getAvailableSquadSkills(clan);
				for(final L2PledgeSkillLearn tmp : skills)
					if(tmp.getId() == skillid && tmp.getLevel() == level)
					{
						final L2SkillLearn skill = new L2SkillLearn(tmp.getId(), tmp.getLevel(), (byte) 0, tmp.getName(), tmp.getRepCost(), tmp.getItemId(), tmp.getItemCount(), false, true, false);
						return skill;
					}
			}
		}
		else
		{
			if(_fishingSkills != null)
				for(final L2SkillLearn tmp : _fishingSkills)
					if(tmp.id == skillid && tmp.skillLevel == level)
						return tmp;

			// Проверка скилов трансформации
			if(_transformationSkills != null)
				for(final L2SkillLearn tmp : _transformationSkills)
					if(tmp.id == skillid && tmp.skillLevel == level)
						return tmp;

			for(final L2SkillLearn tmp : _skillTrees.get(classid))
				if(tmp.id == skillid && tmp.skillLevel == level)
					return tmp;

			for(final L2SkillLearn tmp : _specialSkillTrees)
				if(tmp.id == skillid && tmp.skillLevel == level)
					return tmp;
		}
		return null;
	}

	// TODO оптимизировать
	/**
	 * Возвращает true если скилл может быть изучен данным классом. Если falsе то скил удаляется.
	 * 
	 * @param player
	 * @param skill_id
	 * @param skill_level
	 * @return true/false
	 */
	public boolean isSkillPossible(final L2Player player, final int skillId, final int level)
	{
		// Проверка клановых скилов
		for(final L2PledgeSkillLearn tmp : _clanSkills)
			if(tmp.getId() == skillId && tmp.getLevel() <= level)
				return true;

		// Проверка скилов трансформации
		for(final L2SkillLearn tmp : _transformationSkills)
			if(tmp.getId() == skillId && tmp.getLevel() <= level)
				return true;

		for(final L2SkillLearn tmp : _specialSkillTrees)
			if(tmp.id == skillId && tmp.skillLevel == level)
				return true;

		// Проверка скилов сертификации
		for(final int[] tmp : SubclassSkills.skills_certificates_levels)
			if(tmp[0] == skillId)
				return true;

		final GArray<L2SkillLearn> skills = _skillTrees.get(ClassId.values()[player.getActiveClassId()]);
		for(final L2SkillLearn skilllearn : skills)
			if(skilllearn.id == skillId && skilllearn.skillLevel <= level)
				return true;

		// Проверка для трансфера скилов
		final ClassId classId = player.getClassId();
		if(classId != null)
		{
			int item_id = 0;
			switch (classId)
			{
				case cardinal:
					item_id = 15307; // Cardinal (97)
					break;
				case evaSaint:
					item_id = 15308; // Eva's Saint (105)
					break;
				case shillienSaint:
					item_id = 15309; // Shillen Saint (112)
			}

			if(item_id > 0)
			{
				final String var = player.getVar("TransferSkills" + item_id);
				if(var != null && !var.isEmpty())
					for(final String tmp : var.split(";"))
						if(TypeFormat.parseInt(tmp) == skillId)
							return true;
			}
		}

		// Проверяем скилы которые учаться двойным нажатием по книге)
		if(SkillItemsData.isContainsSkillId(skillId))
			return SkillItemsData.checkConditions(player, skillId);

		// Проверка умений отряда
		if(getSquadSkill(skillId, level) != null)
			return true;

		return false;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final SkillTreeTable _instance = new SkillTreeTable();
	}
}
