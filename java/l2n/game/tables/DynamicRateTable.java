package l2n.game.tables;

import javolution.text.TypeFormat;
import l2n.Config;
import l2n.game.model.actor.L2Player;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.StringTokenizer;
import java.util.logging.Logger;

public class DynamicRateTable
{
	private static final Logger _log = Logger.getLogger(DynamicRateTable.class.getName());

	private final static double[] experience_table = new double[Config.ALT_MAX_LEVEL];
	private final static double[] skillpoint_table = new double[Config.ALT_MAX_LEVEL];
	private final static double[] adena_droprate_table = new double[Config.ALT_MAX_LEVEL];
	private final static double[] items_droprate_table = new double[Config.ALT_MAX_LEVEL];
	private final static double[] spoilrate_table = new double[Config.ALT_MAX_LEVEL];

	public static DynamicRateTable getInstance()
	{
		return SingletonHolder._instance;
	}

	public void reload()
	{
		parse();
	}

	public DynamicRateTable()
	{
		parse();
		_log.info("DynamicRateTable: loaded table.");
	}

	private void parse()
	{
		LineNumberReader lnr = null;
		try
		{
			final File data = new File(Config.DATAPACK_ROOT, "data/dynamic_rate.csv");
			lnr = new LineNumberReader(new BufferedReader(new FileReader(data)));
			String line = null;
			while ((line = lnr.readLine()) != null)
			{
				if(line.trim().length() == 0 || line.startsWith("#"))
					continue;
				final StringTokenizer st = new StringTokenizer(line, ";");
				final String type = st.nextToken();
				final int lvl = TypeFormat.parseInt(st.nextToken());
				final double rate = TypeFormat.parseDouble(st.nextToken());

				if(type.equals("exp"))
					experience_table[lvl - 1] = rate;
				else if(type.equals("sp"))
					skillpoint_table[lvl - 1] = rate;
				else if(type.equals("item_rate"))
					items_droprate_table[lvl - 1] = rate;
				else if(type.equals("adena_rate"))
					adena_droprate_table[lvl - 1] = rate;
				else if(type.equals("spoil_rate"))
					spoilrate_table[lvl - 1] = rate;
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(lnr != null)
					lnr.close();
			}
			catch(final Exception e1)
			{}
		}
	}

	public static double getRateXP(final int level)
	{
		return Config.DYNAMIC_RATE_XP ? experience_table[level - 1] : Config.RATE_XP;
	}

	public static double getRateSP(final int level)
	{
		return Config.DYNAMIC_RATE_SP ? skillpoint_table[level - 1] : Config.RATE_SP;
	}

	public static double getRateAdena(final L2Player player)
	{
		if(player == null)
			return Config.RATE_DROP_ADENA;
		return Config.DYNAMIC_RATE_DROP_ADENA ? adena_droprate_table[player.getLevel() - 1] : Config.RATE_DROP_ADENA;
	}

	public static double getRateItems(final L2Player player)
	{
		if(player == null)
			return Config.RATE_DROP_ITEMS;
		return Config.DYNAMIC_RATE_DROP_ITEMS ? items_droprate_table[player.getLevel() - 1] : Config.RATE_DROP_ITEMS;
	}

	public static double getRateSpoil(final L2Player player)
	{
		if(player == null)
			return Config.RATE_DROP_SPOIL;
		return Config.DYNAMIC_RATE_SPOIL ? spoilrate_table[player.getLevel() - 1] : Config.RATE_DROP_SPOIL;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DynamicRateTable _instance = new DynamicRateTable();
	}
}
