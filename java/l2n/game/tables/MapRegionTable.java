package l2n.game.tables;

import l2n.Config;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.Scripts;
import l2n.game.instancemanager.*;
import l2n.game.model.L2Clan;
import l2n.game.model.L2World;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Race;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

import java.sql.ResultSet;
import java.util.logging.Logger;

public class MapRegionTable
{
	private final static Logger _log = Logger.getLogger(MapRegionTable.class.getName());
	private static MapRegionTable _instance;
	private final int[][] _regions = new int[L2World.WORLD_SIZE_X][L2World.WORLD_SIZE_Y];

	public static enum TeleportWhereType
	{
		Castle,
		ClanHall,
		ClosestTown,
		SecondClosestTown,
		Headquarter,
		Fortress
	}

	public static MapRegionTable getInstance()
	{
		if(_instance == null)
			_instance = new MapRegionTable();
		return _instance;
	}

	private MapRegionTable()
	{
		int count = 0;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT * FROM mapregion");
			while (rset.next())
			{
				final int y = rset.getInt("y10_plus");

				for(int i = Config.GEO_X_FIRST; i <= Config.GEO_X_LAST; i++)
				{
					final int region = rset.getInt("x" + i);
					_regions[i - Config.GEO_X_FIRST][y] = region;
					count++;
				}
			}

			_log.info("MapRegionTable: loaded " + count + " mapregions.");
		}
		catch(final Exception e)
		{
			_log.warning("error while creating map region data: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public final int getMapRegion(final int posX, final int posY)
	{
		final int tileX = posX - L2World.MAP_MIN_X >> 15;
		final int tileY = posY - L2World.MAP_MIN_Y >> 15;
		return _regions[tileX][tileY];
	}

	public static Location getTeleToCastle(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.Castle);
	}

	public static Location getTeleToClanHall(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.ClanHall);
	}

	public static Location getTeleToClosestTown(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.ClosestTown);
	}

	public static Location getTeleToSecondClosestTown(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.SecondClosestTown);
	}

	public static Location getTeleToHeadquarter(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.Headquarter);
	}

	public static Location getTeleToFortress(final L2Character activeChar)
	{
		return getInstance().getTeleToLocation(activeChar, TeleportWhereType.Fortress);
	}

	public Location getTeleToLocation(final L2Character activeChar, final TeleportWhereType teleportWhere)
	{
		final L2Player player = activeChar.getPlayer();
		if(player != null)
		{
			final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_ESCAPE);
			for(final EventScript script : scripts)
				script.notifyOnEscape(player);

			final Reflection r = player.getReflection();
			if(r.getId() != 0 && r.getReturnLoc() != null)
				return r.getReturnLoc();

			final L2Clan clan = player.getClan();

			if(clan != null)
			{
				// If teleport to clan hall
				if(teleportWhere == TeleportWhereType.ClanHall && clan.getHasHideout() != 0)
					return ClanHallManager.getInstance().getClanHall(clan.getHasHideout()).getZone().getSpawn();

				// If teleport to castle
				if(teleportWhere == TeleportWhereType.Castle && clan.getHasCastle() != 0)
					return CastleManager.getInstance().getCastleByIndex(clan.getHasCastle()).getZone().getSpawn();

				// If teleport to fortress
				if(teleportWhere == TeleportWhereType.Fortress && clan.getHasFortress() != 0)
					return FortressManager.getInstance().getFortressByIndex(clan.getHasFortress()).getZone().getSpawn();

				// Checking if in siege
				final Siege siege = SiegeManager.getSiege(activeChar, true);
				if(siege != null && siege.isInProgress())
				{
					if(teleportWhere == TeleportWhereType.Headquarter)
					{
						// Check if player's clan is attacker
						final L2NpcInstance flag = siege.getHeadquarter(player.getClan());
						if(flag != null)
							// Спаун рядом с флагом
							return Rnd.coordsRandomize(flag.getLoc(), 49, 51);
						return TownManager.getInstance().getClosestTown(activeChar).getSpawn();
					}

					// Check if player's clan is defender
					if(siege.getDefenderClan(player.getClan()) != null && siege.getSiegeUnit() != null && siege.getSiegeUnit().getZone() != null)
						return player.getKarma() > 1 ? siege.getSiegeUnit().getZone().getPKSpawn() : siege.getSiegeUnit().getZone().getSpawn();
					return player.getKarma() > 1 ? TownManager.getInstance().getClosestTown(activeChar).getPKSpawn() : TownManager.getInstance().getClosestTown(activeChar).getSpawn();
				}

				if(TerritorySiege.checkIfInZone(activeChar))
				{
					if(teleportWhere == TeleportWhereType.Headquarter)
					{
						final L2NpcInstance flag = TerritorySiege.getHeadquarter(player.getClan());
						if(flag != null)
							// Спаун рядом с флагом
							return flag.getLoc().rnd(50, 75, false);
						return TownManager.getInstance().getClosestTown(activeChar).getSpawn();
					}

					return player.getKarma() > 1 ? TownManager.getInstance().getClosestTown(activeChar).getPKSpawn() : TownManager.getInstance().getClosestTown(activeChar).getSpawn();
				}
			}

			// Светлые эльфы не могут воскрешаться в городе темных
			if(player.getRace() == Race.elf && TownManager.getInstance().getClosestTown(activeChar).getTownId() == 3)
				return player.getKarma() > 1 ? TownManager.getInstance().getTown(2).getPKSpawn() : TownManager.getInstance().getTown(2).getSpawn();

			// Темные эльфы не могут воскрешаться в городе светлых
			if(player.getRace() == Race.darkelf && TownManager.getInstance().getClosestTown(activeChar).getTownId() == 2)
				return player.getKarma() > 1 ? TownManager.getInstance().getTown(3).getPKSpawn() : TownManager.getInstance().getTown(3).getSpawn();

			final L2Zone battle = activeChar.getZone(ZoneType.battle_zone);

			// If in battle zone
			if(battle != null && battle.getRestartPoints() != null)
				return player.getKarma() > 1 ? battle.getPKSpawn() : battle.getSpawn();

			// If in pease zone
			if(activeChar.isInZone(ZoneType.peace_zone) && activeChar.getZone(ZoneType.peace_zone).getRestartPoints() != null)
				return player.getKarma() > 1 ? activeChar.getZone(ZoneType.peace_zone).getPKSpawn() : activeChar.getZone(ZoneType.peace_zone).getSpawn();

			// If in offshore zone == pease zone options.
			if(activeChar.isInZone(ZoneType.offshore) && activeChar.getZone(ZoneType.offshore).getRestartPoints() != null)
				return player.getKarma() > 1 ? activeChar.getZone(ZoneType.offshore).getPKSpawn() : activeChar.getZone(ZoneType.offshore).getSpawn();

			return player.getKarma() > 1 ? TownManager.getInstance().getClosestTown(activeChar).getPKSpawn() : TownManager.getInstance().getClosestTown(activeChar).getSpawn();
		}

		return TownManager.getInstance().getClosestTown(activeChar).getSpawn();
	}
}
