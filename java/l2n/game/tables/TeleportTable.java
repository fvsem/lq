package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TypeFormat;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.templates.L2Item;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 18.02.2010
 * @time 7:01:25
 */
public class TeleportTable
{
	private final static Logger _log = Logger.getLogger(TeleportTable.class.getName());

	private final TIntObjectHashMap<TIntObjectHashMap<TeleportLocation[]>> _lists;

	public static TeleportTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private TeleportTable()
	{
		_lists = new TIntObjectHashMap<TIntObjectHashMap<TeleportLocation[]>>();
		try
		{
			final File file = new File(Config.DATAPACK_ROOT + "/data/teleports.xml");
			final DocumentBuilderFactory factory1 = DocumentBuilderFactory.newInstance();
			factory1.setValidating(false);
			factory1.setIgnoringComments(true);
			final Document doc1 = factory1.newDocumentBuilder().parse(file);

			int counter = 0;
			for(Node n1 = doc1.getFirstChild(); n1 != null; n1 = n1.getNextSibling())
				if("list".equalsIgnoreCase(n1.getNodeName()))
					for(Node d1 = n1.getFirstChild(); d1 != null; d1 = d1.getNextSibling())
						if("npc".equalsIgnoreCase(d1.getNodeName()))
						{
							final TIntObjectHashMap<TeleportLocation[]> lists = new TIntObjectHashMap<TeleportLocation[]>();
							for(Node s1 = d1.getFirstChild(); s1 != null; s1 = s1.getNextSibling())
								if("sublist".equalsIgnoreCase(s1.getNodeName()))
								{
									final GArray<TeleportLocation> targets = new GArray<TeleportLocation>();
									for(Node t1 = s1.getFirstChild(); t1 != null; t1 = t1.getNextSibling())
										if("target".equalsIgnoreCase(t1.getNodeName()))
										{
											counter++;
											final String target = t1.getAttributes().getNamedItem("loc").getNodeValue();
											final String[] names = t1.getAttributes().getNamedItem("name").getNodeValue().split(";");
											final int price = TypeFormat.parseInt(t1.getAttributes().getNamedItem("price").getNodeValue());
											final int item = t1.getAttributes().getNamedItem("item") == null ? 57 : TypeFormat.parseInt(t1.getAttributes().getNamedItem("item").getNodeValue());
											final TeleportLocation t = new TeleportLocation(target, item, price, names);
											targets.add(t);
										}
									if(!targets.isEmpty())
										lists.put(TypeFormat.parseInt(s1.getAttributes().getNamedItem("id").getNodeValue()), targets.toArray(new TeleportLocation[targets.size()]));
								}
							if(!lists.isEmpty())
								_lists.put(TypeFormat.parseInt(d1.getAttributes().getNamedItem("id").getNodeValue()), lists);
						}
			_log.config("TeleportController: Loaded " + counter + " locations.");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TeleportTable: Lists could not be initialized.", e);
		}
	}

	// TODO генерировать страницу при загрузке списка
	public TeleportLocation[] getTeleportLocationList(final int npcId, final int listId)
	{
		if(_lists.get(npcId) == null)
		{
			_log.warning("TeleportTable: not found teleport location for npcId: " + npcId + ", listId: " + listId);
			return null;
		}
		return _lists.get(npcId).get(listId);
	}

	public class TeleportLocation
	{
		public final int _price;
		public final L2Item _item;
		private final String[] _name;
		public final String _target;

		public TeleportLocation(final String target, final int itemId, final int price, final String[] name)
		{
			_target = target;
			_price = price;
			_name = name;
			_item = ItemTable.getInstance().getTemplate(itemId);
		}

		public String getName(final L2Player player)
		{
			if(_name.length != 2)
				return _name[0];
			return player.isLangRus() ? _name[1] : _name[0];
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final TeleportTable _instance = new TeleportTable();
	}
}
