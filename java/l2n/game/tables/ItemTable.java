package l2n.game.tables;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TIntObjectProcedure;
import gnu.trove.procedure.TObjectProcedure;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Scripts;
import l2n.extensions.scripts.Scripts.ScriptClassAndMethod;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.DocumentItem;
import l2n.game.templates.*;
import l2n.game.templates.L2Armor.ArmorType;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.game.templates.L2Item.Grade;
import l2n.game.templates.L2Weapon.WeaponType;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ItemTable
{
	private static final Logger _log = Logger.getLogger(ItemTable.class.getName());

	private static final HashMap<String, Integer> _crystalTypes = new HashMap<String, Integer>();
	private static final HashMap<String, WeaponType> _weaponTypes = new HashMap<String, WeaponType>();
	private static final HashMap<String, ArmorType> _armorTypes = new HashMap<String, ArmorType>();
	private static final HashMap<String, Integer> _slots = new HashMap<String, Integer>();

	private static final int CP_DRAIN = 3650;
	private static final int CANCEL = 3651;
	private static final int IGNORE_SHIELD_DEFENSE = 3652;
	private static final int ATTACK_CHANCE = 3653;
	private static final int CASTING = 3654;
	private static final int RAPID_FIRE = 3655;
	private static final int DECREASE_RANGE = 3656;

	static
	{
		_crystalTypes.put("s84", Grade.S84.ordinal());
		_crystalTypes.put("s80", Grade.S80.ordinal());
		_crystalTypes.put("s", Grade.S.ordinal());
		_crystalTypes.put("a", Grade.A.ordinal());
		_crystalTypes.put("b", Grade.B.ordinal());
		_crystalTypes.put("c", Grade.C.ordinal());
		_crystalTypes.put("d", Grade.D.ordinal());
		_crystalTypes.put("none", Grade.NONE.ordinal());

		_weaponTypes.put("blunt", WeaponType.BLUNT);
		_weaponTypes.put("bigblunt", WeaponType.BIGBLUNT);
		_weaponTypes.put("bow", WeaponType.BOW);
		_weaponTypes.put("crossbow", WeaponType.CROSSBOW);
		_weaponTypes.put("dagger", WeaponType.DAGGER);
		_weaponTypes.put("dual", WeaponType.DUAL);
		_weaponTypes.put("dualfist", WeaponType.DUALFIST);
		_weaponTypes.put("etc", WeaponType.ETC);
		_weaponTypes.put("fist", WeaponType.FIST);
		_weaponTypes.put("none", WeaponType.NONE);
		_weaponTypes.put("pole", WeaponType.POLE);
		_weaponTypes.put("sword", WeaponType.SWORD);
		_weaponTypes.put("bigsword", WeaponType.BIGSWORD);
		_weaponTypes.put("rapier", WeaponType.RAPIER);
		_weaponTypes.put("ancientsword", WeaponType.ANCIENTSWORD);
		_weaponTypes.put("pet", WeaponType.PET);
		_weaponTypes.put("rod", WeaponType.ROD);
		_weaponTypes.put("dualdagger", WeaponType.DUALDAGGER);

		_armorTypes.put("none", ArmorType.NONE);
		_armorTypes.put("light", ArmorType.LIGHT);
		_armorTypes.put("heavy", ArmorType.HEAVY);
		_armorTypes.put("magic", ArmorType.MAGIC);
		_armorTypes.put("sigil", ArmorType.SIGIL);
		_armorTypes.put("pet", ArmorType.PET);

		_slots.put("chest", L2Item.SLOT_CHEST);
		_slots.put("belt", L2Item.SLOT_BELT);
		_slots.put("rbracelet", L2Item.SLOT_R_BRACELET);
		_slots.put("lbracelet", L2Item.SLOT_L_BRACELET);
		_slots.put("talisman", L2Item.SLOT_DECO);
		_slots.put("fullarmor", L2Item.SLOT_FULL_ARMOR);
		_slots.put("head", L2Item.SLOT_HEAD);
		_slots.put("hair", L2Item.SLOT_HAIR);
		_slots.put("face", L2Item.SLOT_DHAIR);
		_slots.put("dhair", L2Item.SLOT_HAIRALL);
		_slots.put("underwear", L2Item.SLOT_UNDERWEAR);
		_slots.put("cloak", L2Item.SLOT_CLOAK);
		_slots.put("back", L2Item.SLOT_BACK);
		_slots.put("neck", L2Item.SLOT_NECK);
		_slots.put("legs", L2Item.SLOT_LEGS);
		_slots.put("feet", L2Item.SLOT_FEET);
		_slots.put("gloves", L2Item.SLOT_GLOVES);
		_slots.put("chest,legs", L2Item.SLOT_CHEST | L2Item.SLOT_LEGS);
		_slots.put("rhand", L2Item.SLOT_R_HAND);
		_slots.put("lhand", L2Item.SLOT_L_HAND);
		_slots.put("lrhand", L2Item.SLOT_LR_HAND);
		_slots.put("rear,lear", L2Item.SLOT_R_EAR | L2Item.SLOT_L_EAR);
		_slots.put("rfinger,lfinger", L2Item.SLOT_R_FINGER | L2Item.SLOT_L_FINGER);
		_slots.put("none", L2Item.SLOT_NONE);
		_slots.put("wolf", L2Item.SLOT_WOLF);
		_slots.put("gwolf", L2Item.SLOT_GREATWOLF);
		_slots.put("hatchling", L2Item.SLOT_HATCHLING);
		_slots.put("strider", L2Item.SLOT_STRIDER);
		_slots.put("baby", L2Item.SLOT_BABYPET);
		_slots.put("pendant", L2Item.SLOT_PENDANT);
		_slots.put("formalwear", L2Item.SLOT_FORMAL_WEAR);
		_slots.put("sigil", L2Item.SLOT_SIGIL);
	}

	private L2Item[] _allTemplates = new L2Item[0];
	private int highestId = 0;

	private final L2Weapon[] _all_weapons;
	private final TIntIntHashMap _item_idx;

	private int[][] _weapon_ex;
	private int[][] _armor_ex;

	private static final String[] SQL_ITEM_SELECTS = {
			"SELECT item_id, name, class, icon, crystallizable, item_type, weight, consume_type, crystal_type, durability, price, crystal_count, sellable, skill_id, skill_level, tradeable, dropable, destroyable, temporal FROM etcitem",
			"SELECT item_id, name, additional_name, icon, bodypart, crystallizable, armor_type, player_class, weight, crystal_type, avoid_modify, durability, p_def, m_def, mp_bonus, price, crystal_count, sellable, tradeable, dropable, destroyable, skill_id, skill_level, enchant4_skill_id, enchant4_skill_lvl, temporal FROM armor",
			"SELECT item_id, name, additional_name, icon, bodypart, crystallizable, weight, soulshots, spiritshots, crystal_type, p_dam, rnd_dam, weaponType, critical, hit_modify, avoid_modify, shield_def, shield_def_rate, atk_speed, mp_consume, m_dam, durability, price, crystal_count, sellable, tradeable, dropable, destroyable, skill_id, skill_level, enchant4_skill_id, enchant4_skill_lvl, temporal FROM weapon" };


	public static ItemTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final ItemTable _instance = new ItemTable();
	}

	public Item newItem()
	{
		return new Item();
	}

	private ItemTable()
	{
		ThreadConnection con = null;
		FiltredStatement st = null;
		ResultSet rs = null;

		final TIntObjectHashMap<L2EtcItem> _etcItems = new TIntObjectHashMap<L2EtcItem>();
		final TIntObjectHashMap<L2Armor> _armors = new TIntObjectHashMap<L2Armor>();
		final TIntObjectHashMap<L2Weapon> _weapons = new TIntObjectHashMap<L2Weapon>();

		TIntObjectHashMap<Item> itemData = new TIntObjectHashMap<Item>();
		TIntObjectHashMap<Item> weaponData = new TIntObjectHashMap<Item>();
		TIntObjectHashMap<Item> armorData = new TIntObjectHashMap<Item>();
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.createStatement();

			for(final String selectQuery : SQL_ITEM_SELECTS)
			{
				rs = st.executeQuery(selectQuery);
				while (rs.next())
					if(selectQuery.endsWith("etcitem"))
					{
						final Item newItem = readItem(rs);
						itemData.put(newItem.id, newItem);
					}
					else if(selectQuery.endsWith("armor"))
					{
						final Item newItem = readArmor(rs);
						armorData.put(newItem.id, newItem);
					}
					else if(selectQuery.endsWith("weapon"))
					{
						final Item newItem = readWeapon(rs);
						weaponData.put(newItem.id, newItem);
					}
				DbUtils.close(rs);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "data error on item: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st, rs);
		}

		armorData.forEachEntry(new TIntObjectProcedure<Item>()
		{
			@Override
			public boolean execute(final int id, final Item item)
			{
				_armors.put(id, new L2Armor((ArmorType) item.type, item.set));
				return true;
			}
		});

		itemData.forEachEntry(new TIntObjectProcedure<Item>()
		{
			@Override
			public boolean execute(final int id, final Item item)
			{
				_etcItems.put(id, new L2EtcItem((EtcItemType) item.type, item.set));
				return true;
			}
		});

		weaponData.forEachEntry(new TIntObjectProcedure<Item>()
		{
			@Override
			public boolean execute(final int id, final Item item)
			{
				_weapons.put(id, new L2Weapon((WeaponType) item.type, item.set));
				return true;
			}
		});

		_log.config("ItemTable: Loaded " + _armors.size() + " Armors.");
		_log.config("ItemTable: Loaded " + _etcItems.size() + " Items.");
		_log.config("ItemTable: Loaded " + _weapons.size() + " Weapons.");

		itemData.clear();
		armorData.clear();
		weaponData.clear();

		buildFastLookupTable(_etcItems, _armors, _weapons);

		_item_idx = new TIntIntHashMap();

		_all_weapons = _weapons.values(new L2Weapon[_weapons.size()]);

		loadExtendedWeaponsTable();
		loadExtendedArmorTable();

		for(final int[] item : _weapon_ex)
		{
			if(item == null)
				continue;
			if(item[WEX_RARE] > 0)
				_allTemplates[item[WEX_RARE]].setRare(true);
			if(item[WEX_RARE_SA1] > 0)
			{
				_allTemplates[item[WEX_RARE_SA1]].setRare(true);
				_allTemplates[item[WEX_RARE_SA1]].setSa(true);
			}
			if(item[WEX_RARE_SA2] > 0)
			{
				_allTemplates[item[WEX_RARE_SA2]].setRare(true);
				_allTemplates[item[WEX_RARE_SA2]].setSa(true);
			}
			if(item[WEX_RARE_SA3] > 0)
			{
				_allTemplates[item[WEX_RARE_SA3]].setRare(true);
				_allTemplates[item[WEX_RARE_SA3]].setSa(true);
			}
			if(item[WEX_RARE_PVP1] > 0)
			{
				_allTemplates[item[WEX_RARE_PVP1]].setRare(true);
				_allTemplates[item[WEX_RARE_PVP1]].setPvP(true);
				_allTemplates[item[WEX_RARE_PVP1]].setSa(true);
			}
			if(item[WEX_RARE_PVP2] > 0)
			{
				_allTemplates[item[WEX_RARE_PVP2]].setRare(true);
				_allTemplates[item[WEX_RARE_PVP2]].setPvP(true);
				_allTemplates[item[WEX_RARE_PVP2]].setSa(true);
			}
			if(item[WEX_RARE_PVP3] > 0)
			{
				_allTemplates[item[WEX_RARE_PVP3]].setRare(true);
				_allTemplates[item[WEX_RARE_PVP3]].setPvP(true);
				_allTemplates[item[WEX_RARE_PVP3]].setSa(true);
			}
			if(item[WEX_PVP1] > 0)
			{
				_allTemplates[item[WEX_PVP1]].setPvP(true);
				_allTemplates[item[WEX_PVP1]].setSa(true);
			}
			if(item[WEX_PVP2] > 0)
			{
				_allTemplates[item[WEX_PVP2]].setPvP(true);
				_allTemplates[item[WEX_PVP2]].setSa(true);
			}
			if(item[WEX_PVP3] > 0)
			{
				_allTemplates[item[WEX_PVP3]].setPvP(true);
				_allTemplates[item[WEX_PVP3]].setSa(true);
			}
			if(item[WEX_SA1] > 0)
				_allTemplates[item[WEX_SA1]].setSa(true);
			if(item[WEX_SA2] > 0)
				_allTemplates[item[WEX_SA2]].setSa(true);
			if(item[WEX_SA3] > 0)
				_allTemplates[item[WEX_SA3]].setSa(true);
		}

		for(final int[] item : _armor_ex)
		{
			if(item == null)
				continue;
			if(item[AEX_SEALED_RARE_1] > 0)
				_allTemplates[item[AEX_SEALED_RARE_1]].setRare(true);
			if(item[AEX_SEALED_RARE_2] > 0)
				_allTemplates[item[AEX_SEALED_RARE_2]].setRare(true);
			if(item[AEX_SEALED_RARE_3] > 0)
				_allTemplates[item[AEX_SEALED_RARE_3]].setRare(true);
			if(item[AEX_UNSEALED_RARE_1] > 0)
				_allTemplates[item[AEX_UNSEALED_RARE_1]].setRare(true);
			if(item[AEX_UNSEALED_RARE_2] > 0)
				_allTemplates[item[AEX_UNSEALED_RARE_2]].setRare(true);
			if(item[AEX_UNSEALED_RARE_3] > 0)
				_allTemplates[item[AEX_UNSEALED_RARE_3]].setRare(true);
			if(item[AEX_PvP] > 0)
				_allTemplates[item[AEX_PvP]].setPvP(true);
		}

		for(final L2Item item : _allTemplates)
			if(item != null && item.isPvP() && item instanceof L2Weapon)
				switch ((L2Weapon.WeaponType) item.type)
				{
					case BOW:
					case CROSSBOW:
						item.attachSkill(SkillTable.getInstance().getInfo(RAPID_FIRE, 1));
						break;
					case ANCIENTSWORD:
					case BIGBLUNT:
					case BIGSWORD:
						if(item.isMageSA())
							item.attachSkill(SkillTable.getInstance().getInfo(CASTING, 1)); 
						else
							item.attachSkill(SkillTable.getInstance().getInfo(ATTACK_CHANCE, 1));
						break;
					case BLUNT:
					case SWORD:
					case RAPIER:
						if(item.isMageSA())
							item.attachSkill(SkillTable.getInstance().getInfo(CASTING, 1));
						else
							item.attachSkill(SkillTable.getInstance().getInfo(CP_DRAIN, 1));
						break;
					case DUALFIST:
					case DAGGER:
					case DUALDAGGER:
						item.attachSkill(SkillTable.getInstance().getInfo(CANCEL, 1));
						item.attachSkill(SkillTable.getInstance().getInfo(IGNORE_SHIELD_DEFENSE, 1));
						break;
					case POLE:
						item.attachSkill(SkillTable.getInstance().getInfo(ATTACK_CHANCE, 1));
						break;
					case DUAL:
						item.attachSkill(SkillTable.getInstance().getInfo(DECREASE_RANGE, 1));
				}

		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					Thread.sleep(1000);
				}
				catch(final InterruptedException e)
				{}

				for(final File f : new File("./data/stats/items/").listFiles())
					if(!f.isDirectory())
						new DocumentItem(f);
			}
		}).start();

		itemData.clear();
		armorData.clear();
		weaponData.clear();
		_etcItems.clear();
		_armors.clear();
		_weapons.clear();
		itemData = null;
		armorData = null;
		weaponData = null;
	}

	private Item readWeapon(final ResultSet rset) throws SQLException
	{
		final Item item = new Item();
		item.set = new StatsSet();
		item.id = rset.getInt("item_id");
		item.type = _weaponTypes.get(rset.getString("weaponType"));
		if(item.type == null)
			System.out.println("Error in weapons table: unknown weapon type " + rset.getString("weaponType") + " for item " + item.id);
		item.name = rset.getString("name");
		item.set.set("class", "EQUIPMENT");

		item.set.set("item_id", item.id);
		item.set.set("name", item.name);
		item.set.set("additional_name", rset.getString("additional_name"));

		if(item.type == WeaponType.NONE)
		{
			item.set.set("type1", L2Item.TYPE1_SHIELD_ARMOR);
			item.set.set("type2", L2Item.TYPE2_SHIELD_ARMOR);
		}
		else
		{
			item.set.set("type1", L2Item.TYPE1_WEAPON_RING_EARRING_NECKLACE);
			item.set.set("type2", L2Item.TYPE2_WEAPON);
		}

		item.set.set("bodypart", _slots.get(rset.getString("bodypart")));
		item.set.set("crystal_type", _crystalTypes.get(rset.getString("crystal_type")));
		item.set.set("crystallizable", Boolean.valueOf(rset.getString("crystallizable")).booleanValue());
		item.set.set("weight", rset.getInt("weight"));
		item.set.set("soulshots", rset.getInt("soulshots"));
		item.set.set("spiritshots", rset.getInt("spiritshots"));
		item.set.set("p_dam", rset.getInt("p_dam"));
		item.set.set("rnd_dam", rset.getInt("rnd_dam"));
		item.set.set("critical", rset.getInt("critical"));
		item.set.set("hit_modify", rset.getDouble("hit_modify"));
		item.set.set("avoid_modify", rset.getInt("avoid_modify"));
		item.set.set("shield_def", rset.getInt("shield_def"));
		item.set.set("shield_def_rate", rset.getInt("shield_def_rate"));
		item.set.set("atk_speed", rset.getInt("atk_speed"));
		item.set.set("mp_consume", rset.getInt("mp_consume"));
		item.set.set("m_dam", rset.getInt("m_dam"));
		item.set.set("durability", rset.getInt("durability"));
		item.set.set("price", rset.getInt("price"));
		item.set.set("crystal_count", rset.getInt("crystal_count"));
		item.set.set("sellable", Boolean.valueOf(rset.getString("sellable")));
		item.set.set("tradeable", rset.getInt("tradeable") > 0);
		item.set.set("dropable", rset.getInt("dropable") > 0);
		item.set.set("destroyable", rset.getInt("destroyable") > 0);
		item.set.set("temporal", rset.getInt("temporal") > 0);
		item.set.set("skill_id", rset.getString("skill_id"));
		item.set.set("skill_level", rset.getString("skill_level"));
		item.set.set("enchant4_skill_id", rset.getInt("enchant4_skill_id"));
		item.set.set("enchant4_skill_lvl", rset.getInt("enchant4_skill_lvl"));
		item.set.set("icon", rset.getString("icon"));

		if(item.type == WeaponType.PET)
		{
			item.set.set("type1", L2Item.TYPE1_WEAPON_RING_EARRING_NECKLACE);

			if(item.set.getInteger("bodypart") == L2Item.SLOT_WOLF)
				item.set.set("type2", L2Item.TYPE2_PET_WOLF);

			else if(item.set.getInteger("bodypart") == L2Item.SLOT_GREATWOLF)
				item.set.set("type2", L2Item.TYPE2_PET_GREATWOLF);

			if(item.set.getInteger("bodypart") == L2Item.SLOT_HATCHLING)
				item.set.set("type2", L2Item.TYPE2_PET_HATCHLING);

			else if(item.set.getInteger("bodypart") == L2Item.SLOT_BABYPET)
				item.set.set("type2", L2Item.TYPE2_PET_BABYPET);

			else if(item.set.getInteger("bodypart") == L2Item.SLOT_STRIDER)
				item.set.set("type2", L2Item.TYPE2_PET_STRIDER);

			item.set.set("bodypart", L2Item.SLOT_R_HAND);
		}

		return item;
	}

	private Item readArmor(final ResultSet rset) throws SQLException
	{
		final Item item = new Item();
		item.set = new StatsSet();
		item.type = _armorTypes.get(rset.getString("armor_type"));
		item.id = rset.getInt("item_id");
		item.name = rset.getString("name");
		item.set.set("class", "EQUIPMENT");

		item.set.set("item_id", item.id);
		item.set.set("name", item.name);
		final int bodypart = _slots.get(rset.getString("bodypart"));
		item.set.set("bodypart", bodypart);
		item.set.set("player_class", rset.getString("player_class"));
		item.set.set("crystallizable", Boolean.valueOf(rset.getString("crystallizable")));
		item.set.set("crystal_count", rset.getInt("crystal_count"));
		item.set.set("sellable", Boolean.valueOf(rset.getString("sellable")));

		if(bodypart == L2Item.SLOT_NECK || (bodypart & L2Item.SLOT_L_EAR) != 0 || (bodypart & L2Item.SLOT_L_FINGER) != 0)
		{
			item.set.set("type1", L2Item.TYPE1_WEAPON_RING_EARRING_NECKLACE);
			item.set.set("type2", L2Item.TYPE2_ACCESSORY);
		}
		else if(bodypart == L2Item.SLOT_HAIR || bodypart == L2Item.SLOT_DHAIR || bodypart == L2Item.SLOT_HAIRALL)
		{
			item.set.set("type1", L2Item.TYPE2_ACCESSORY);
			item.set.set("type2", L2Item.TYPE2_OTHER);
		}
		else
		{
			item.set.set("type1", L2Item.TYPE1_SHIELD_ARMOR);
			item.set.set("type2", L2Item.TYPE2_SHIELD_ARMOR);
		}

		item.set.set("weight", rset.getInt("weight"));
		item.set.set("crystal_type", _crystalTypes.get(rset.getString("crystal_type")));
		item.set.set("avoid_modify", rset.getInt("avoid_modify"));
		item.set.set("durability", rset.getInt("durability"));
		item.set.set("p_def", rset.getInt("p_def"));
		item.set.set("m_def", rset.getInt("m_def"));
		item.set.set("mp_bonus", rset.getInt("mp_bonus"));
		item.set.set("price", rset.getInt("price"));
		item.set.set("tradeable", rset.getInt("tradeable") > 0);
		item.set.set("dropable", rset.getInt("dropable") > 0);
		item.set.set("destroyable", rset.getInt("destroyable") > 0);
		item.set.set("temporal", rset.getInt("temporal") > 0);
		item.set.set("skill_id", rset.getString("skill_id"));
		item.set.set("skill_level", rset.getString("skill_level"));
		item.set.set("enchant4_skill_id", rset.getInt("enchant4_skill_id"));
		item.set.set("enchant4_skill_lvl", rset.getInt("enchant4_skill_lvl"));
		item.set.set("icon", rset.getString("icon"));

		if(item.type == ArmorType.PET)
		{
			item.set.set("type1", L2Item.TYPE1_SHIELD_ARMOR);

			if(item.set.getInteger("bodypart") == L2Item.SLOT_WOLF)
			{
				item.set.set("type2", L2Item.TYPE2_PET_WOLF);
				item.set.set("bodypart", L2Item.SLOT_CHEST);
			}
			else if(item.set.getInteger("bodypart") == L2Item.SLOT_GREATWOLF)
			{
				item.set.set("type2", L2Item.TYPE2_PET_GREATWOLF);
				item.set.set("bodypart", L2Item.SLOT_CHEST);
			}
			else if(item.set.getInteger("bodypart") == L2Item.SLOT_HATCHLING)
			{
				item.set.set("type2", L2Item.TYPE2_PET_HATCHLING);
				item.set.set("bodypart", L2Item.SLOT_CHEST);
			}
			else if(item.set.getInteger("bodypart") == L2Item.SLOT_PENDANT)
			{
				item.set.set("type2", L2Item.TYPE2_PENDANT);
				item.set.set("bodypart", L2Item.SLOT_NECK);
			}
			else if(item.set.getInteger("bodypart") == L2Item.SLOT_BABYPET)
			{
				item.set.set("type2", L2Item.TYPE2_PET_BABYPET);
				item.set.set("bodypart", L2Item.SLOT_CHEST);
			}
			else
			{
				item.set.set("type2", L2Item.TYPE2_PET_STRIDER);
				item.set.set("bodypart", L2Item.SLOT_CHEST);
			}
		}

		return item;
	}

	private Item readItem(final ResultSet rset) throws SQLException
	{
		final Item item = new Item();
		item.set = new StatsSet();
		item.id = rset.getInt("item_id");

		item.set.set("item_id", item.id);
		item.set.set("crystallizable", Boolean.valueOf(rset.getString("crystallizable")));
		item.set.set("type1", L2Item.TYPE1_ITEM_QUESTITEM_ADENA);
		item.set.set("type2", L2Item.TYPE2_OTHER);
		item.set.set("bodypart", 0);
		item.set.set("crystal_count", rset.getInt("crystal_count"));
		item.set.set("sellable", Boolean.valueOf(rset.getString("sellable")));
		item.set.set("temporal", rset.getInt("temporal") > 0);
		item.set.set("icon", rset.getString("icon"));
		item.set.set("class", rset.getString("class"));
		final String itemType = rset.getString("item_type");
		if(itemType.equals("none"))
			item.type = EtcItemType.OTHER;
		else if(itemType.equals("mticket"))
			item.type = EtcItemType.SCROLL;
		else if(itemType.equals("material"))
			item.type = EtcItemType.MATERIAL;
		else if(itemType.equals("pet_collar"))
			item.type = EtcItemType.PET_COLLAR;
		else if(itemType.equals("potion"))
			item.type = EtcItemType.POTION;
		else if(itemType.equals("recipe"))
			item.type = EtcItemType.RECIPE;
		else if(itemType.equals("scroll"))
			item.type = EtcItemType.SCROLL;
		else if(itemType.equals("seed"))
			item.type = EtcItemType.SEED;
		else if(itemType.equals("spellbook"))
			item.type = EtcItemType.SPELLBOOK;
		else if(itemType.equals("shot"))
			item.type = EtcItemType.SHOT;
		else if(itemType.equals("rune"))
			item.type = EtcItemType.RUNE;
		else if(itemType.equals("herb"))
			item.type = EtcItemType.HERB;
		else if(itemType.equals("arrow"))
		{
			item.type = EtcItemType.ARROW;
			item.set.set("bodypart", L2Item.SLOT_L_HAND);
		}
		else if(itemType.equals("bolt"))
		{
			item.type = EtcItemType.BOLT;
			item.set.set("bodypart", L2Item.SLOT_L_HAND);
		}
		else if(itemType.equals("bait"))
		{
			item.type = EtcItemType.BAIT;
			item.set.set("bodypart", L2Item.SLOT_L_HAND);
		}
		else if(itemType.equals("quest"))
		{
			item.type = EtcItemType.QUEST;
			item.set.set("type2", L2Item.TYPE2_QUEST);
		}
		else
		{
			_log.fine("unknown etcitem type:" + itemType);
			item.type = EtcItemType.OTHER;
		}

		final String consume = rset.getString("consume_type");
		if(consume.equals("asset"))
		{
			item.type = EtcItemType.MONEY;
			item.set.set("stackable", true);
			item.set.set("type2", L2Item.TYPE2_MONEY);
		}
		else if(consume.equals("stackable"))
			item.set.set("stackable", true);
		else
			item.set.set("stackable", false);

		final int crystal = _crystalTypes.get(rset.getString("crystal_type"));
		item.set.set("crystal_type", crystal);

		final int weight = rset.getInt("weight");
		item.set.set("weight", weight);
		item.name = rset.getString("name");
		item.set.set("name", item.name);

		item.set.set("durability", rset.getInt("durability"));
		item.set.set("price", rset.getInt("price"));
		item.set.set("skill_id", rset.getString("skill_id"));
		item.set.set("skill_level", rset.getString("skill_level"));
		item.set.set("tradeable", rset.getInt("tradeable") > 0);
		item.set.set("dropable", rset.getInt("dropable") > 0);
		item.set.set("destroyable", rset.getInt("destroyable") > 0);

		return item;
	}

	private void buildFastLookupTable(final TIntObjectHashMap<L2EtcItem> etcItems, final TIntObjectHashMap<L2Armor> armors, final TIntObjectHashMap<L2Weapon> weapons)
	{
		armors.forEachValue(new TObjectProcedure<L2Armor>()
		{
			@Override
			public boolean execute(final L2Armor item)
			{
				if(item.getItemId() > highestId)
					highestId = item.getItemId();
				return true;
			}
		});

		etcItems.forEachValue(new TObjectProcedure<L2EtcItem>()
		{
			@Override
			public boolean execute(final L2EtcItem item)
			{
				if(item.getItemId() > highestId)
					highestId = item.getItemId();
				return true;
			}
		});

		weapons.forEachValue(new TObjectProcedure<L2Weapon>()
		{
			@Override
			public boolean execute(final L2Weapon item)
			{
				if(item.getItemId() > highestId)
					highestId = item.getItemId();
				return true;
			}
		});

		if(Config.DEBUG)
			_log.info("highest item id used:" + highestId);
		_allTemplates = new L2Item[highestId + 1];

		for(final L2Armor item : armors.valueCollection())
		{
			assert _allTemplates[item.getItemId()] == null;
			_allTemplates[item.getItemId()] = item;
		}

		for(final L2Weapon item : weapons.valueCollection())
		{
			assert _allTemplates[item.getItemId()] == null;
			_allTemplates[item.getItemId()] = item;
		}

		for(final L2EtcItem item : etcItems.valueCollection())
		{
			assert _allTemplates[item.getItemId()] == null;
			_allTemplates[item.getItemId()] = item;
		}
	}

	public L2ItemInstance createItem(final int itemId, final int destination, final int source, String create_type)
	{
		if(create_type == null)
			create_type = "";

		if(Config.DISABLE_CREATION_ID_LIST.contains(itemId) && !create_type.equals("delayed_add"))
		{
			_log.log(Level.WARNING, "Try creating DISABLE_CREATION item " + itemId + " " + create_type + ": ", new Exception());
			return null;
		}


		final L2ItemInstance temp = new L2ItemInstance(IdFactory.getInstance().getNextId(), itemId, destination, source, create_type);
		if(Config.DEBUG)
			_log.fine("ItemTable: Item created	oid:" + temp.getObjectId() + " itemid:" + itemId);
		return temp;
	}

	public L2ItemInstance createDummyItem(final int itemId)
	{
		return new L2ItemInstance(0, itemId, 0, 0, "");
	}

	public static boolean useHandler(final L2Playable self, final L2ItemInstance item, final boolean ctrl)
	{
		L2Player player;
		if(self.isPlayer())
			player = (L2Player) self;
		else if(self.isPet())
			player = self.getPlayer();
		else
			return false;

		final GArray<ScriptClassAndMethod> handlers = Scripts.itemHandlers.get(item.getItemId());
		if(handlers != null && handlers.size() > 0)
		{
			if(player.isInFlyingTransform())
			{
				player.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
				return false;
			}

			final Object[] script_args = new Object[] { player };
			for(final ScriptClassAndMethod handler : handlers)
				player.callScripts(handler.scriptClass, handler.method, script_args);

			return true;
		}

		final L2Skill[] skills = item.getItem().getAttachedSkills();
		if(skills != null && skills.length > 0)
		{
			L2Character aimingTarget;
			for(int i = 0; i < skills.length; i++)
			{
				aimingTarget = skills[i].getAimingTarget(player, player.getTarget());
				if(skills[i].checkCondition(player, aimingTarget, false, false, i == 0))
					player.getAI().Cast(skills[i], aimingTarget, false, false);
			}
			return true;
		}
		return false;
	}

	public L2Item getTemplate(final int id)
	{
		if(id >= _allTemplates.length)
		{
			_log.log(Level.WARNING, "ItemTable[604]: Not defined item_id=" + id + "; out of range", new Exception("ItemTable[604]"));
			return null;
		}
		return _allTemplates[id];
	}

	public L2Item[] getAllTemplates()
	{
		return _allTemplates;
	}

	public boolean isExists(final int id)
	{
		return id < _allTemplates.length && id >= 0 && _allTemplates[id] != null;
	}

	public L2Weapon[] getAllWeapons()
	{
		return _all_weapons;
	}

	/**
	 * Returns if ItemTable initialized
	 */
	public boolean isInitialized()
	{
		return _allTemplates != null && _allTemplates.length > 0;
	}

	public static final int WEX_FOUNDATION = 0;
	public static final int WEX_RARE = 1;
	public static final int WEX_SA1 = 2;
	public static final int WEX_SA2 = 3;
	public static final int WEX_SA3 = 4;
	public static final int WEX_RARE_SA1 = 5;
	public static final int WEX_RARE_SA2 = 6;
	public static final int WEX_RARE_SA3 = 7;
	public static final int WEX_PVP1 = 8;
	public static final int WEX_PVP2 = 9;
	public static final int WEX_PVP3 = 10;
	public static final int WEX_RARE_PVP1 = 11;
	public static final int WEX_RARE_PVP2 = 12;
	public static final int WEX_RARE_PVP3 = 13;
	public static final int WEX_COMMON = 14;
	public static final int WEX_KAMAEL_EX = 15;
	public static final int WEX_SA_CRY1 = 16;
	public static final int WEX_SA_CRY2 = 17;
	public static final int WEX_SA_CRY3 = 18;
	public static final int WEX_VARNISH_COUNT = 19;

	private void loadExtendedWeaponsTable()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT * FROM weapon_ex WHERE item_id NOT IN (SELECT item_id FROM armor_ex)");
			final TIntObjectHashMap<int[]> res = new TIntObjectHashMap<int[]>();
			while (rset.next())
			{
				final int item_id = rset.getInt("item_id");
				final int[] item = new int[] {
						rset.getInt("foundation"),
						rset.getInt("rare"),
						rset.getInt("sa1"),
						rset.getInt("sa2"),
						rset.getInt("sa3"),
						rset.getInt("raresa1"),
						rset.getInt("raresa2"),
						rset.getInt("raresa3"),
						rset.getInt("pvp1"),
						rset.getInt("pvp2"),
						rset.getInt("pvp3"),
						rset.getInt("rarepvp1"),
						rset.getInt("rarepvp2"),
						rset.getInt("rarepvp3"),
						rset.getInt("common"),
						rset.getInt("kamaex"),
						rset.getInt("sacry1"),
						rset.getInt("sacry2"),
						rset.getInt("sacry3"),
						rset.getInt("varnish"), };
				res.put(item_id, item);
				for(final int i : item)
					if(i > 0)
						_item_idx.put(i, item_id);
			}
			_weapon_ex = new int[_allTemplates.length + 1][];
			res.forEachEntry(new TIntObjectProcedure<int[]>()
			{
				@Override
				public boolean execute(final int item_id, final int[] item)
				{
					for(final int i : item)
						if(i > 0)
						{
							_weapon_ex[item_id] = item;
							break;
						}
					return true;
				}
			});
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static final int AEX_UNSEALED_1 = 0;
	public static final int AEX_UNSEALED_2 = 1;
	public static final int AEX_UNSEALED_3 = 2;
	public static final int AEX_FOUNDATION = 3;
	public static final int AEX_SEALED_RARE_1 = 4;
	public static final int AEX_SEALED_RARE_2 = 5;
	public static final int AEX_SEALED_RARE_3 = 6;
	public static final int AEX_UNSEALED_RARE_1 = 7;
	public static final int AEX_UNSEALED_RARE_2 = 8;
	public static final int AEX_UNSEALED_RARE_3 = 9;
	public static final int AEX_PvP = 10;
	public static final int AEX_COMMON = 11;
	public static final int AEX_UNSEALED_COMMON_1 = 12;
	public static final int AEX_UNSEALED_COMMON_2 = 13;
	public static final int AEX_UNSEALED_COMMON_3 = 14;
	public static final int AEX_VARNISH = 15;
	public static final int AEX_UNSEAL_PRICE = 16;
	public static final int AEX_RESEAL_PRICE = 17;

	private void loadExtendedArmorTable()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT * FROM armor_ex");

			final TIntObjectHashMap<int[]> res = new TIntObjectHashMap<int[]>();
			while (rset.next())
			{
				final int item_id = rset.getInt("item_id");
				final int[] item = new int[] {
						rset.getInt("uns1"),
						rset.getInt("uns2"),
						rset.getInt("uns3"),
						rset.getInt("foundation"),
						rset.getInt("srare1"),
						rset.getInt("srare2"),
						rset.getInt("srare3"),
						rset.getInt("rare1"),
						rset.getInt("rare2"),
						rset.getInt("rare3"),
						rset.getInt("pvp"),
						rset.getInt("common"),
						rset.getInt("unsc1"),
						rset.getInt("unsc2"),
						rset.getInt("unsc3"),
						rset.getInt("varnish"),
						rset.getInt("unseal"),
						rset.getInt("reseal"), };
				res.put(item_id, item);
				for(final int i : item)
					if(i > 0)
						_item_idx.put(i, item_id);
			}
			_armor_ex = new int[_allTemplates.length + 1][];
			res.forEachEntry(new TIntObjectProcedure<int[]>()
			{
				@Override
				public boolean execute(final int item_id, final int[] item)
				{
					for(final int i : item)
						if(i > 0)
						{
							_armor_ex[item_id] = item;
							break;
						}
					return true;
				}
			});
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public int findBaseId(final int id)
	{
		return _item_idx.get(id);
	}

	public int[][] getWeaponEx()
	{
		return _weapon_ex;
	}

	public int[][] getArmorEx()
	{
		return _armor_ex;
	}
}
