package l2n.game.tables;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TypeFormat;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.L2SkillLearnItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.base.ClassLevel;
import l2n.game.model.base.ClassType;
import l2n.game.model.base.Race;
import l2n.util.ArrayUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Таблица итемов с помощью которых учаться скилы (дабл-кликом)
 * 
 * @author L2System Project
 * @date 03.06.2010
 * @time 9:55:22
 */
public class SkillItemsData
{
	private static final Logger _log = Logger.getLogger(SkillItemsData.class.getName());

	private final TIntIntHashMap _skillsItems;
	private final TIntObjectHashMap<GArray<L2SkillLearnItem>> _giveSkillItems;

	public static SkillItemsData getInstance()
	{
		return SingletonHolder._instance;
	}

	public SkillItemsData()
	{
		_skillsItems = new TIntIntHashMap();
		_giveSkillItems = new TIntObjectHashMap<GArray<L2SkillLearnItem>>();

		try
		{
			load();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Load SkillItemsData", e);
		}
	}

	public GArray<L2SkillLearnItem> getSkillGiveItem(final int itemId)
	{
		return _giveSkillItems.get(itemId);
	}

	public static boolean isContainsSkillId(final int skillId)
	{
		return getInstance()._skillsItems.containsKey(skillId);
	}

	public static int getItemId(final int skillId)
	{
		return getInstance()._skillsItems.get(skillId);
	}

	public static boolean checkConditions(final L2Player activeChar, final int skillId)
	{
		final GArray<L2SkillLearnItem> skills = getInstance().getSkillGiveItem(getItemId(skillId));
		for(final L2SkillLearnItem sl : skills)
			if(sl.checkConditions(activeChar, true))
				return true;
		return false;
	}

	public int[] itemIDs()
	{
		return _giveSkillItems.keys();
	}

	public void load() throws SAXException, IOException, ParserConfigurationException
	{
		final File f = new File(Config.DATAPACK_ROOT, "data/get_skill_items.xml");
		if(!f.exists())
		{
			_log.warning("Error in loading Item Give Skills data, data/get_skill_items.xml file not found!");
			return;
		}
		_giveSkillItems.clear();
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setIgnoringComments(true);
		final Document doc = factory.newDocumentBuilder().parse(f);
		for(Node main = doc.getFirstChild(); main != null; main = main.getNextSibling())
			if("list".equalsIgnoreCase(main.getNodeName()))
				for(Node setting = main.getFirstChild(); setting != null; setting = setting.getNextSibling())
					if("item".equalsIgnoreCase(setting.getNodeName()))
					{
						final NamedNodeMap arg = setting.getAttributes();
						final Node tid = arg.getNamedItem("itemId");
						if(tid == null)
							continue;

						final int itemId = TypeFormat.parseInt(tid.getNodeValue());
						final L2SkillLearnItem skillLearnItem = new L2SkillLearnItem(itemId);
						for(Node dnode = setting.getFirstChild(); dnode != null; dnode = dnode.getNextSibling())
							if("giveSkill".equalsIgnoreCase(dnode.getNodeName()))
							{
								final NamedNodeMap arg2 = dnode.getAttributes();
								parseGiveSkillNode(arg2, skillLearnItem);
							}
							// conditions
							else if("conditions".equalsIgnoreCase(dnode.getNodeName()))
								for(Node d2node = dnode.getFirstChild(); d2node != null; d2node = d2node.getNextSibling())
									if("playerClass".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final String[] classId = arg2.getNamedItem("val").getNodeValue().split(",");
											final GArray<ClassId> _classes = new GArray<ClassId>(classId.length);
											for(final String cls : classId)
												_classes.add(ClassId.valueOf(cls));
											skillLearnItem.setPlayerClassIDs(_classes.toArray(new ClassId[_classes.size()]));
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerClassId".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final int[] classId = ArrayUtil.toIntArray(arg2.getNamedItem("val").getNodeValue(), ",");
											final GArray<ClassId> _classes = new GArray<ClassId>(classId.length);
											for(final int cls : classId)
												_classes.add(ClassId.getById(cls));
											skillLearnItem.setPlayerClassIDs(_classes.toArray(new ClassId[_classes.size()]));
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerMinLevel".equalsIgnoreCase(d2node.getNodeName()))
									{
										int val = 1;
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											val = TypeFormat.parseInt(arg2.getNamedItem("val").getNodeValue());
											skillLearnItem.setPlayerMinLevel(val);
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									}
									else if("playerClassLevel".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final String[] classLevels = arg2.getNamedItem("val").getNodeValue().split(",");
											final GArray<ClassLevel> _cl = new GArray<ClassLevel>(classLevels.length);
											for(final String cls : classLevels)
												_cl.add(ClassLevel.valueOf(cls));
											skillLearnItem.setPlayerClassLevel(_cl.toArray(new ClassLevel[_cl.size()]));
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerClassType".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final String[] classTypes = arg2.getNamedItem("val").getNodeValue().split(",");
											final GArray<ClassType> _ct = new GArray<ClassType>(classTypes.length);
											for(final String type : classTypes)
												_ct.add(ClassType.valueOf(type));
											skillLearnItem.setPlayerClassType(_ct.toArray(new ClassType[_ct.size()]));
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerRace".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final String[] playerRaces = arg2.getNamedItem("val").getNodeValue().split(",");
											final GArray<Race> _pr = new GArray<Race>(playerRaces.length);
											for(final String race : playerRaces)
												_pr.add(Race.valueOf(race));
											skillLearnItem.setPlayerRace(_pr.toArray(new Race[_pr.size()]));
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerKnownSkill".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final int id = TypeFormat.parseInt(arg2.getNamedItem("skillId").getNodeValue());
											final int lv = TypeFormat.parseInt(arg2.getNamedItem("level").getNodeValue());
											final boolean ex = TypeFormat.parseBoolean(arg2.getNamedItem("exactlyLevel").getNodeValue());
											skillLearnItem.setPlayerKnownSkill(id, lv, ex);
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}
									else if("playerSpReq".equalsIgnoreCase(d2node.getNodeName()))
										try
										{
											final NamedNodeMap arg2 = d2node.getAttributes();
											final int sp = TypeFormat.parseInt(arg2.getNamedItem("val").getNodeValue());
											skillLearnItem.setPlayerSpReq(sp);
										}
										catch(final Exception e)
										{
											e.printStackTrace();
										}

						GArray<L2SkillLearnItem> learnItems = _giveSkillItems.get(itemId);
						if(learnItems == null)
							learnItems = new GArray<L2SkillLearnItem>();
						learnItems.add(skillLearnItem);

						_giveSkillItems.put(itemId, learnItems);
					}

		_log.info("SkillItemsData: Loaded " + _giveSkillItems.size() + " learn skill items.");
	}

	private void parseGiveSkillNode(final NamedNodeMap arg2, final L2SkillLearnItem skillLearnItem)
	{
		int skillId = 0;
		try
		{
			skillId = TypeFormat.parseInt(arg2.getNamedItem("id").getNodeValue());
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		int skillLv = 0;
		try
		{
			skillLv = TypeFormat.parseInt(arg2.getNamedItem("lvl").getNodeValue());
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}

		if(SkillTable.getInstance().getInfo(skillId, skillLv) != null)
		{
			skillLearnItem.addGiveSkill(skillId, skillLv);
			_skillsItems.put(skillId, skillLearnItem.getItemId());
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final SkillItemsData _instance = new SkillItemsData();
	}
}
