package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2PetData;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.base.Experience;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.PetSkillsTable.L2PetSkillLearn;
import l2n.util.ArrayUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PetDataTable
{
	private static final Logger _log = Logger.getLogger(PetDataTable.class.getName());

	private static final L2PetSkillLearn[] EMPTY_SKILL_LEARNS = new L2PetSkillLearn[0];

	private static PetDataTable _instance = new PetDataTable();

	private final TIntObjectHashMap<L2PetData[]> _pet_data;
	private static L2Pet[] _pets;

	public final static int PET_WOLF_ID = 12077;

	public final static int HATCHLING_WIND_ID = 12311;
	public final static int HATCHLING_STAR_ID = 12312;
	public final static int HATCHLING_TWILIGHT_ID = 12313;

	public final static int STRIDER_WIND_ID = 12526;
	public final static int STRIDER_STAR_ID = 12527;
	public final static int STRIDER_TWILIGHT_ID = 12528;

	public final static int RED_STRIDER_WIND_ID = 16038;
	public final static int RED_STRIDER_STAR_ID = 16039;
	public final static int RED_STRIDER_TWILIGHT_ID = 16040;

	public final static int WYVERN_ID = 12621;

	public final static int BABY_BUFFALO_ID = 12780;
	public final static int BABY_KOOKABURRA_ID = 12781;
	public final static int BABY_COUGAR_ID = 12782;

	public final static int IMPROVED_BABY_BUFFALO_ID = 16034;
	public final static int IMPROVED_BABY_KOOKABURRA_ID = 16035;
	public final static int IMPROVED_BABY_COUGAR_ID = 16036;

	public final static int SIN_EATER_ID = 12564;

	public final static int GREAT_WOLF_ID = 16025;
	public final static int WGREAT_WOLF_ID = 16037;
	public final static int FENRIR_WOLF_ID = 16041;
	public final static int WFENRIR_WOLF_ID = 16042;

	public final static int LIGHT_PURPLE_MANED_HORSE_ID = 13130;
	public final static int TAWNY_MANED_LION_ID = 13146;
	public final static int STEAM_BEATLE_ID = 13147;

	public final static int AURA_BIRD_FALCON_ID = 13144;
	public final static int AURA_BIRD_OWL_ID = 13145;

	public final static int FOX_SHAMAN_ID = 16043;
	public final static int WILD_BEAST_FIGHTER_ID = 16044;
	public final static int WHITE_WEASEL_ID = 16045;
	public final static int FAIRY_PRINCESS_ID = 16046;
	public final static int OWL_MONK_ID = 16050;

	public final static int SPIRIT_SHAMAN_ID = 16051;
	public final static int TOY_KNIGHT_ID = 16052;
	public final static int TURTLE_ASCETIC_ID = 16053;

	public final static int DEINONYCHUS_ID = 16067;
	public final static int GUARDIANS_STRIDER_ID = 16068;

	public static final class L2Pet
	{
		private final int _npcId;
		private final int _controlItemId;
		private final int _foodId;
		private final boolean _isMountable;
		private final int _minLevel; // Уровень, нУже которого не может опускаться пет
		private final int _addFed; // На сколько процентов увелУчУвается полоска еды, прУ кормленУУ
		private final float _expPenalty;
		private final int _soulshots;
		private final int _spiritshots;

		private L2Pet(final StatsSet data)
		{
			_npcId = data.getInteger("npcId");
			_controlItemId = data.getInteger("controlItemId");
			_foodId = data.getInteger("foodId");
			_isMountable = data.getBool("isMountable");
			_minLevel = data.getInteger("minLevel");
			_addFed = data.getInteger("addFed");
			_expPenalty = data.getFloat("expPenalty");
			_soulshots = data.getInteger("soulshots");
			_spiritshots = data.getInteger("spiritshots");
		}

		public int getNpcId()
		{
			return _npcId;
		}

		public int getControlItemId()
		{
			return _controlItemId;
		}

		public int getFoodId()
		{
			return _foodId;
		}

		public boolean isMountable()
		{
			return _isMountable;
		}

		public int getMinLevel()
		{
			return _minLevel;
		}

		public int getAddFed()
		{
			return _addFed;
		}

		public float getExpPenalty()
		{
			return _expPenalty;
		}

		public int getSoulshots()
		{
			return _soulshots;
		}

		public int getSpiritshots()
		{
			return _spiritshots;
		}
	}

	public static PetDataTable getInstance()
	{
		return _instance;
	}

	public void reload()
	{
		_instance = new PetDataTable();
	}

	private PetDataTable()
	{
		_pet_data = new TIntObjectHashMap<L2PetData[]>();
		_pets = new L2Pet[0];
		loadPets();
		loadDataTable();
		_log.info("PetDataTable: loaded " + _pet_data.size() + " pets.");
	}

	public final L2PetData getInfo(final int summonId, final int level)
	{
		if(level < 1 || level > Experience.MAX_LEVEL)
			return null;

		final L2PetData[] datas = _pet_data.get(summonId);
		if(datas != null)
			return datas[level - 1];

		return null;
	}

	private void loadPets()
	{
		final File file = new File(Config.DATAPACK_ROOT, "data/pets.xml");
		if(!file.exists())
		{
			_log.warning("File " + file.getAbsolutePath() + " not exists");
			return;
		}

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setIgnoringComments(true);

		try
		{
			final Document doc = factory.newDocumentBuilder().parse(file);
			for(Node iz = doc.getFirstChild(); iz != null; iz = iz.getNextSibling())
				if("list".equalsIgnoreCase(iz.getNodeName()))
					for(Node area = iz.getFirstChild(); area != null; area = area.getNextSibling())
						if("pet".equalsIgnoreCase(area.getNodeName()))
						{
							final NamedNodeMap attrs = area.getAttributes();
							final StatsSet data = new StatsSet();
							for(int i = attrs.getLength(); i-- > 0;)
							{
								final Node n = attrs.item(i);
								if(n != null)
									data.set(n.getNodeName(), n.getNodeValue());
							}

							for(Node settings = area.getFirstChild(); settings != null; settings = settings.getNextSibling())
								if("set".equalsIgnoreCase(settings.getNodeName()))
								{
									final String name = settings.getAttributes().getNamedItem("name").getNodeValue().trim();
									final String value = settings.getAttributes().getNamedItem("val").getNodeValue().trim();
									data.set(name, value);
								}

							final L2Pet pet = new L2Pet(data);
							_pets = ArrayUtil.arrayAdd(_pets, pet);
						}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "PetDataTable: load pets error ", e);
		}
	}

	private void loadDataTable()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT id, level, exp, exp_penalty, hp, mp, patk, pdef, matk, mdef, acc, evasion, crit, speed, atk_speed, cast_speed, max_meal, battle_meal, normal_meal, loadMax, hpregen, mpregen FROM pet_data ORDER BY id, level");
			while (rset.next())
			{
				final int summonId = rset.getInt("id");
				final int summonLevel = rset.getInt("level");
				L2PetData[] petDatas = _pet_data.get(summonId);
				if(petDatas == null)
				{
					petDatas = new L2PetData[Experience.MAX_LEVEL + 1];
					_pet_data.put(summonId, petDatas);
				}

				final L2PetData petData = new L2PetData();
				petDatas[summonLevel - 1] = petData;

				petData.setID(summonId);
				petData.setLevel(summonLevel);
				petData.setExp(rset.getLong("exp"));
				petData.setHP(rset.getInt("hp"));
				petData.setMP(rset.getInt("mp"));
				petData.setPAtk(rset.getInt("patk"));
				petData.setPDef(rset.getInt("pdef"));
				petData.setMAtk(rset.getInt("matk"));
				petData.setMDef(rset.getInt("mdef"));
				petData.setAccuracy(rset.getInt("acc"));
				petData.setEvasion(rset.getInt("evasion"));
				petData.setCritical(rset.getInt("crit"));
				petData.setSpeed(rset.getInt("speed"));
				petData.setAtkSpeed(rset.getInt("atk_speed"));
				petData.setCastSpeed(rset.getInt("cast_speed"));
				petData.setFeedMax(rset.getInt("max_meal"));
				petData.setFeedBattle(rset.getInt("battle_meal"));
				petData.setFeedNormal(rset.getInt("normal_meal"));
				petData.setMaxLoad(rset.getInt("loadMax"));
				petData.setHpRegen(rset.getFloat("hpregen"));
				petData.setMpRegen(rset.getFloat("mpregen"));
				petData.setExpPenalty(rset.getFloat("exp_penalty") / 100f);

				petData.setControlItemId(getControlItemId(petData.getID()));
				petData.setFoodId(getFoodId(petData.getID()));
				petData.setMountable(isMountable(petData.getID()));
				petData.setMinLevel(getMinLevel(petData.getID()));
				petData.setAddFed(getAddFed(petData.getID()));

				petData.setPetSupportSkills(computeSkills(SupportPetsSkillsTable.getInstance().getSkills(summonId), summonLevel));
				petData.setPetRegularSkills(computeSkills(PetSkillsTable.getInstance().getSkills(summonId), summonLevel));
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Cannot fill up PetDataTable: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static void deletePet(final L2ItemInstance item, final L2Character owner)
	{
		int petObjectId = 0;
		ThreadConnection con = null;
		FiltredStatement statement = null;
		FiltredStatement statement2 = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT objId FROM pets WHERE item_obj_id=" + item.getObjectId());
			while (rset.next())
				petObjectId = rset.getInt("objId");
			DbUtils.closeQuietly(statement, rset);

			final L2Summon summon = owner.getPet();
			if(summon != null && summon.getObjectId() == petObjectId)
				summon.unSummon();

			final L2Player player = owner.getPlayer();
			if(player != null && player.isMounted() && player.getMountObjId() == petObjectId)
				player.setMount(0, 0);

			// if it's a pet control item, delete the pet
			statement2 = con.createStatement();
			statement2.executeUpdate("DELETE FROM pets WHERE item_obj_id=" + item.getObjectId());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore pet objectid: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement2, rset);
		}
	}

	public static void unSummonPet(final L2ItemInstance oldItem, final L2Character owner)
	{
		int petObjectId = 0;
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT objId FROM pets WHERE item_obj_id=" + oldItem.getObjectId());

			while (rset.next())
				petObjectId = rset.getInt("objId");

			if(owner == null)
				return;

			final L2Summon summon = owner.getPet();
			if(summon != null && summon.getObjectId() == petObjectId)
				summon.unSummon();

			final L2Player player = owner.getPlayer();
			if(player != null && player.isMounted() && player.getMountObjId() == petObjectId)
				player.setMount(0, 0);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore pet objectid:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public static int getControlItemId(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getControlItemId();
		return 1;
	}

	public static int getFoodId(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getFoodId();
		return 1;
	}

	public static boolean isMountable(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.isMountable();
		return false;
	}

	public static int getMinLevel(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getMinLevel();
		return 1;
	}

	public static int getAddFed(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getAddFed();
		return 1;
	}

	public static float getExpPenalty(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getExpPenalty();
		return 0f;
	}

	public static int getSoulshots(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getSoulshots();
		return 2;
	}

	public static int getSpiritshots(final int npcId)
	{
		for(final L2Pet pet : _pets)
			if(pet.getNpcId() == npcId)
				return pet.getSpiritshots();
		return 2;
	}

	public static int getSummonId(final L2ItemInstance item)
	{
		for(final L2Pet pet : _pets)
			if(pet.getControlItemId() == item.getItemId())
				return pet.getNpcId();
		return 0;
	}

	public static int[] getPetControlItems()
	{
		final int[] items = new int[_pets.length];
		int i = 0;
		for(final L2Pet pet : _pets)
			items[i++] = pet.getControlItemId();
		return items;
	}

	/**
	 * Return true if item is Pet Control Item (bugle)
	 * 
	 * @return boolean
	 */
	public static boolean isPetControlItem(final L2ItemInstance item)
	{
		for(final L2Pet pet : _pets)
			if(pet.getControlItemId() == item.getItemId())
				return true;
		return false;
	}

	/**
	 * Return true if npc is Baby Pet
	 * 
	 * @return boolean
	 */
	public static boolean isBabyPet(final int id)
	{
		switch (id)
		{
			case BABY_BUFFALO_ID:
			case BABY_KOOKABURRA_ID:
			case BABY_COUGAR_ID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Return true if npc is Improved Baby Pet
	 * 
	 * @return boolean
	 */
	public static boolean isImprovedBabyPet(final int id)
	{
		switch (id)
		{
			case IMPROVED_BABY_BUFFALO_ID:
			case IMPROVED_BABY_KOOKABURRA_ID:
			case IMPROVED_BABY_COUGAR_ID:
			case FAIRY_PRINCESS_ID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Return true if npc is Support Pet
	 * 
	 * @return boolean
	 */
	public static boolean isSupportPet(final int id)
	{
		switch (id)
		{
			case BABY_BUFFALO_ID:
			case BABY_KOOKABURRA_ID:
			case BABY_COUGAR_ID:
			case IMPROVED_BABY_BUFFALO_ID:
			case IMPROVED_BABY_KOOKABURRA_ID:
			case IMPROVED_BABY_COUGAR_ID:
			case WHITE_WEASEL_ID:
			case FAIRY_PRINCESS_ID:
			case SPIRIT_SHAMAN_ID:
			case TOY_KNIGHT_ID:
			case TURTLE_ASCETIC_ID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Return true if npc is Wolf
	 * 
	 * @return boolean
	 */
	public static boolean isWolf(final int id)
	{
		return id == PET_WOLF_ID;
	}

	/**
	 * Return true if npc is Hatchling
	 * 
	 * @return boolean
	 */
	public static boolean isHatchling(final int id)
	{
		switch (id)
		{
			case HATCHLING_WIND_ID:
			case HATCHLING_STAR_ID:
			case HATCHLING_TWILIGHT_ID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Return true if npc is Strider
	 * 
	 * @return boolean
	 */
	public static boolean isStrider(final int id)
	{
		switch (id)
		{
			case STRIDER_WIND_ID:
			case STRIDER_STAR_ID:
			case STRIDER_TWILIGHT_ID:
			case RED_STRIDER_WIND_ID:
			case RED_STRIDER_STAR_ID:
			case RED_STRIDER_TWILIGHT_ID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Return true if npc is GWolf
	 * 
	 * @return boolean
	 */
	public static boolean isGWolf(final int id)
	{
		switch (id)
		{
			case GREAT_WOLF_ID:
			case WGREAT_WOLF_ID:
			case FENRIR_WOLF_ID:
			case WFENRIR_WOLF_ID:
				return true;
			default:
				return false;
		}
	}

	public static boolean isVitaminPet(final int id)
	{
		switch (id)
		{
			case FOX_SHAMAN_ID:
			case WILD_BEAST_FIGHTER_ID:
			case WHITE_WEASEL_ID:
			case FAIRY_PRINCESS_ID:
			case OWL_MONK_ID:
			case SPIRIT_SHAMAN_ID:
			case TOY_KNIGHT_ID:
			case TURTLE_ASCETIC_ID:
				return true;
			default:
				return false;
		}
	}

	public static final L2PetSkillLearn[] computeSkills(final GArray<L2PetSkillLearn> skills, final int petLevel)
	{
		if(skills == null || skills.isEmpty())
			return EMPTY_SKILL_LEARNS;

		GArray<L2PetSkillLearn> computed = null;
		L2PetSkillLearn skill;
		int skillLevel;

		for(int i = skills.size(); i-- > 0;)
		{
			skill = skills.getUnsafe(i);

			if(skill.getMinLevel() <= petLevel)
				if(skill.getLevel() <= 0)
				{
					if(petLevel < 70)
					{
						skillLevel = petLevel / 10;
						if(skillLevel <= 0)
							skillLevel = 1;
					}
					else
						skillLevel = 7 + (petLevel - 70) / 5;

					// formula usable for skill that have 10 or more skill levels
					skillLevel = Math.min(SkillTable.getInstance().getMaxLevel(skill.getId()), skillLevel);

					if(computed == null)
						computed = new GArray<L2PetSkillLearn>();

					computed.add(skill);
				}
				else
				{
					if(computed == null)
						computed = new GArray<L2PetSkillLearn>();

					computed.add(skill);
				}
		}

		return computed != null ? computed.toArray(new L2PetSkillLearn[computed.size()]) : EMPTY_SKILL_LEARNS;
	}
}
