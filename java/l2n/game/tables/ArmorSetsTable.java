package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2ArmorSet;
import l2n.game.model.L2Skill;

import java.sql.ResultSet;
import java.util.logging.Logger;

public class ArmorSetsTable
{
	private final static Logger _log = Logger.getLogger(ArmorSetsTable.class.getName());
	private boolean _initialized = false;

	private TIntObjectHashMap<L2ArmorSet> _armorSets;

	public static ArmorSetsTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final ArmorSetsTable _instance = new ArmorSetsTable();
	}

	private ArmorSetsTable()
	{
		_armorSets = new TIntObjectHashMap<L2ArmorSet>();
		loadData();
	}

	private void loadData()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT chest, legs, head, gloves, feet, skill, shield, shield_skill_id, enchant6skill, mw_legs, mw_head, mw_gloves, mw_feet, mw_shield FROM armorsets");
			rset = statement.executeQuery();

			while (rset.next())
			{
				int chest = rset.getInt("chest");
				int legs = rset.getInt("legs");
				int head = rset.getInt("head");
				int gloves = rset.getInt("gloves");
				int feet = rset.getInt("feet");
				int shield = rset.getInt("shield");
				String[] skills = rset.getString("skill").split(";");
				L2Skill shield_skill = SkillTable.getInstance().getInfo(rset.getInt("shield_skill_id"), 1);
				L2Skill enchant6skill = SkillTable.getInstance().getInfo(rset.getInt("enchant6skill"), 1);
				int mwork_legs = rset.getInt("mw_legs");
				int mwork_head = rset.getInt("mw_head");
				int mwork_gloves = rset.getInt("mw_gloves");
				int mwork_feet = rset.getInt("mw_feet");
				int mwork_shield = rset.getInt("mw_shield");

				_armorSets.put(chest, new L2ArmorSet(chest, legs, head, gloves, feet, skills, shield, shield_skill, enchant6skill, mwork_legs, mwork_head, mwork_gloves, mwork_feet, mwork_shield));
			}

			_log.config("ArmorSetsTable: Loaded " + _armorSets.size() + " armor sets.");
			_initialized = true;
		}
		catch(Exception e)
		{
			_log.severe("ArmorSetsTable: Error reading ArmorSets table: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public boolean setExists(int chestId)
	{
		return _armorSets.containsKey(chestId);
	}

	public L2ArmorSet getSet(int chestId)
	{
		return _armorSets.get(chestId);
	}

	public boolean isInitialized()
	{
		return _initialized;
	}
}
