package l2n.game.tables;

import javolution.text.TypeFormat;
import l2n.Config;
import l2n.util.ArrayUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Logger;

public class EnchantChanceSkillsTable
{
	private static final Logger _log = Logger.getLogger(EnchantChanceSkillsTable.class.getName());

	private static EnchantChanceSkillsTable _instance;

	public final static int[][] _chance30 = new int[30][];
	public final static int[][] _chance15 = new int[15][];

	public static EnchantChanceSkillsTable getInstance()
	{
		if(_instance == null)
			_instance = new EnchantChanceSkillsTable();
		return _instance;
	}

	public EnchantChanceSkillsTable()
	{
		load();
		_log.info("EnchantChanceSkillsTable: loaded chance table.");
	}

	public void load()
	{
		final File f = new File(Config.DATAPACK_ROOT, "data/chance_enchant.xml");
		if(!f.exists())
		{
			_log.warning("Error in loading data, data/chance_enchant.xml file not found!");
			return;
		}

		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		factory.setIgnoringComments(true);
		try
		{
			Document doc = factory.newDocumentBuilder().parse(f);
			for(Node main = doc.getFirstChild(); main != null; main = main.getNextSibling())
				if("list".equalsIgnoreCase(main.getNodeName()))
					for(Node setting = main.getFirstChild(); setting != null; setting = setting.getNextSibling())
						if("enchant".equalsIgnoreCase(setting.getNodeName()))
						{
							final NamedNodeMap arg = setting.getAttributes();
							final Node tid = arg.getNamedItem("maxLvl");
							if(tid == null)
								continue;

							final int maxLvl = TypeFormat.parseInt(tid.getNodeValue());
							for(Node dnode = setting.getFirstChild(); dnode != null; dnode = dnode.getNextSibling())
								if("enchantSkill".equalsIgnoreCase(dnode.getNodeName()))
								{
									final NamedNodeMap attrs = dnode.getAttributes();
									int level = TypeFormat.parseInt(attrs.getNamedItem("lvl").getNodeValue());
									int[] cnances = ArrayUtil.toIntArray(attrs.getNamedItem("chance").getNodeValue(), ",");

									if(maxLvl > 15)
										_chance30[level - 1] = cnances;
									else
										_chance15[level - 1] = cnances;
								}
						}
		}
		catch(Exception e)
		{
			_log.warning("Error in loading data, data/chanceEnchant.xml file not found!");
			e.printStackTrace();
		}
	}
}
