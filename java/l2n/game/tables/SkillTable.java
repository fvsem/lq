package l2n.game.tables;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Skill;
import l2n.game.network.serverpackets.FlyToLocation.FlyType;
import l2n.game.skills.SkillsEngine;
import l2n.util.ArrayUtil;
import l2n.util.Log;
import l2n.util.Util;

import java.sql.ResultSet;

public class SkillTable
{
	public static final L2Skill[] EMPTY_ARRAY = new L2Skill[0];
	public static final L2Skill[][] EMPTY_ARRAY_2D = new L2Skill[0][];

	private boolean _initialized = false;

	private L2Skill[][][] _skills;

	private final int[] _baseLevels;
	private final int[] _maxSQLLevels;

	private static final int MAX_SKILL_LEVELS = 816;

	public static SkillTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private SkillTable()
	{
		reload(false);

		_baseLevels = new int[_skills.length + 1];
		_maxSQLLevels = new int[_skills.length + 1];

		loadBaseLevels();
		loadSqlSkills();

		// Reloading as well FrequentSkill enumeration values
		for(final FrequentSkill sk : FrequentSkill.values())
			sk._skill = getInfo(sk.id, sk.level);

		setInitialized(true);
	}

	public final void reload(final boolean reload)
	{
		_skills = new L2Skill[0][][];
		if(reload)
		{
			final long val = Util.gc(1, 1000);
			_skills = SkillsEngine.getInstance().loadAllSkills();
			loadBaseLevels();
			loadSqlSkills();

			// Reloading as well FrequentSkill enumeration values
			for(final FrequentSkill sk : FrequentSkill.values())
				sk._skill = getInfo(sk.id, sk.level);

			GmListTable.broadcastMessageToGMs("Skill table reloaded: " + Util.formatBytes(val) + " free.");
		}
		else
			_skills = SkillsEngine.getInstance().loadAllSkills();
	}

	/**
	 * Загружаем таблУцы с уровнямУ скУлов
	 */
	private void loadBaseLevels()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();

			rset = statement.executeQuery("SELECT id, MAX(level) AS level FROM skills WHERE level < 100 GROUP BY id");
			while (rset.next())
				_baseLevels[rset.getInt("id")] = rset.getInt("level"); // запУсываем максУмальный уровень скУла Уз клУента, без учета энчанта
			DbUtils.close(rset);

			rset = statement.executeQuery("SELECT id, MAX(level) AS level FROM skills GROUP BY id"); // запУсываем максУмальный уровень скУла Уз клУента с учетом энчанта
			while (rset.next())
				_maxSQLLevels[rset.getInt("id")] = rset.getInt("level");
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void loadSqlSkills()
	{
		final GArray<Integer> _incorrectSkills = new GArray<Integer>();
		final GArray<Integer> _notFoundSkills = new GArray<Integer>();
		final GArray<Integer> _incorrectLevel = new GArray<Integer>();

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT * FROM skills ORDER BY id, level ASC");
			int lastid = 0;
			int last_magic_level = 0;
			while (rset.next())
			{
				final int id = rset.getInt("id");
				final int level = rset.getInt("level");
				final int magic_level = rset.getInt("magic_level");
				final String name = rset.getString("name");
				final String icon = rset.getString("icon");

				final boolean is_magic = rset.getInt("is_magic") == 1;
				final int mp_initial_consume = rset.getInt("mp_initial_consume");
				final int mp_consume = rset.getInt("mp_consume");
				final int hp_consume = rset.getInt("hp_consume");
				final int cast_range = rset.getInt("cast_range");
				final int hit_time = rset.getInt("hit_time");
				final int power = rset.getInt("power");
				// int learn = rset.getInt("learn");
				final int reuse = rset.getInt("reuse");
				// boolean is_enchant = !rset.getString("enchant").isEmpty();

				if(lastid != id)
				{
					lastid = id;
					last_magic_level = magic_level;
				}
				last_magic_level = Math.max(last_magic_level, magic_level);

				final int baseLevel = _baseLevels[id];
				// Проверем, есть лУ вообще такой скУл у нас в xml
				final L2Skill base = getInfo(id, 1);
				if(base == null)
				{
					// еслУ такого скУла нет в xml - дальше не проверяем его
					if(_notFoundSkills.contains(id))
						continue;

					_incorrectSkills.add(id);
					_notFoundSkills.add(id);
					Log.addDev("Missing Skill [" + id + "]\t(id = " + id + ", name = " + name + ")", "sql_skill_missing", false);
				}

				// Проверяем налУчУе всех уровней скУла в xml
				L2Skill skill = getInfo(id, level);
				if(skill == null)
				{
					if(_incorrectSkills.contains(id))
						continue;

					_incorrectSkills.add(id);
					if(level < 100)
						Log.addDev("Incorrect skill levels [" + id + "]\t(id = " + id + ", level = " + level + ", name = " + name + ")", "sql_skill_levels", false);
					else
						Log.addDev("Not found enchant [" + id + "]\t(id = " + id + ", level = " + level + ", name = " + name + ")", "sql_skill_enchant_levels", false);
				}

				// для некоторых скУллов колУчество уровней в сервере больше чем в клУенте
				// по хорошему это надо делать только для последнего определенного в sql уровня
				final int maxSQL = _maxSQLLevels[id];
				for(int i = level; i < MAX_SKILL_LEVELS && (i > maxSQL || i == level); i++)
				{
					skill = getInfo(id, i);
					if(skill == null)
						continue;

					// Загружаем реюз
					if(reuse >= 0)
						skill.setReuseDelay(reuse);

					// КорректУруем уровнУ скУллов, в основном для энчантов
					// Для нашей сУстемы это скорее всего не надо)
					if(skill.getLevel() != level && !_incorrectLevel.contains(id))
					{
						_incorrectLevel.add(id);
						Log.addDev("Incorrect display level [" + id + "]\tid = " + id + ", [XmlMax = " + getMaxLevel(id) + " / ClientMax = " + getBaseLevel(id) + "]", "sql_skill_display_levels", false);
					}

					if(skill.getPower() == 0 && power > 0)
					{
						skill.setPower(power);
						Log.addDev("Not found power [" + id + "]\t(id = " + id + ", level = " + level + ", name = " + name + ")", "sql_skill_without_power", false);
					}
					else if(power > 0 && skill.getPower() != power)
					{
						skill.setPower(power);
						Log.addDev("Incorrect power [" + id + "]\t(id = " + id + ", level = " + level + ", name = " + name + ")", "sql_skill_power", false);
					}

					skill.setBaseLevel((short) baseLevel);
					skill.setMagicLevel((byte) magic_level);

					skill.setCastRange(cast_range);
					if(skill.getFlyType() != FlyType.NONE && skill.getFlyRadius() > 0 && skill.getCastRange() > 0 && skill.getCastRange() < skill.getFlyRadius())
						skill.setCastRange(skill.getFlyRadius());

					skill.setName(name);
					skill.setIcon(icon.equalsIgnoreCase("skill0000") ? null : icon);
					skill.setHitTime(hit_time);

					if(skill.getSkillInterruptTime() == 0)
						skill.setSkillInterruptTime(skill.getHitTime() * 3 / 4);

					skill.setIsMagic(is_magic);
					skill.setOverhit(skill.isOverhit() || !is_magic && Config.ALT_ALL_PHYS_SKILLS_OVERHIT);

					// skill.setDisplayLevel((short) display_level);

					skill.setHpConsume(hp_consume);
					if(mp_consume > 0)
					{
						skill.setMpInitialConsume(mp_initial_consume);
						skill.setMpConsume(mp_consume);
					}

					// if(!(lastlevel == level && lastid == id))
					// break;
				}

				/**
				 * operate_type<BR>
				 * 0 - в основном фУз.<BR>
				 * 1 - в основном маг.<BR>
				 * 2 - в основном селфы, Уногда бафы.<BR>
				 * 3 - дебафы.<BR>
				 * 4 - героУ, нублесы, скУллы захвата замка.<BR>
				 * 5 - рыбные, предметные, аугментацУя, трансформацУя, SA.<BR>
				 * 6 - ауры<BR>
				 * 7 - трансформацУУ<BR>
				 * 11 - пассУвкУ Угроков<BR>
				 * 12 - еще пассУвкУ Угроков, У мобов<BR>
				 * 13 - спец. пассУвкУ Угроков, характерные только для конкретной расы. Расы мобов.<BR>
				 * 14 - аналогУчно, только еще Divine Inspiration, пассУвные Final скУллы, У всякая фУгня намешана<BR>
				 * 15 - клановые скУллы, скУллы фортов<BR>
				 * 16 - сеты, эпУкУ, SA, аугментацУя, все пассУвное<BR>
				 * <BR>
				 * OP_PASSIVE: 11, 12, 13, 14, 15, 16<BR>
				 * OP_ACTIVE: 0, 1, 2, 3, 4, 5, 7<BR>
				 * OP_TOGGLE: 6<BR>
				 * OP_ON_ATTACK: 5<BR>
				 * OP_ON_CRIT: 5<BR>
				 * OP_ON_MAGIC_ATTACK: 5<BR>
				 * OP_ON_UNDER_ATTACK: 5<BR>
				 * OP_ON_MAGIC_SUPPORT: 5<BR>
				 */
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	/**
	 * @param skillId
	 *            The id of the skill.
	 * @param level
	 *            The level of the skill (inclusive enchant, for example 1 - 1020).
	 * @return The skill if found or null.
	 */
	public final L2Skill getInfo(final int skillId, int level)
	{
		if(skillId < 1 || skillId > _skills.length || level < 1)
			return null;

		final int tree = level / 100;
		level = level % 100;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(tree >= trees.length)
			return null;

		final L2Skill[] levels = trees[tree];
		if(level > levels.length || level <= 0)
			return null;

		return levels[level - 1];
	}

	/**
	 * @param skillId
	 *            The id of the skill.
	 * @param tree
	 *            The tree of the skill. 0 = Base, 1 - 10 (inclusive) = Enchant.
	 * @param level
	 *            The level of the skill (exclusive enchant, for example 1 - 20).
	 * @return The skill if found or null.
	 */
	public final L2Skill getInfo(final int skillId, final int tree, final int level)
	{
		if(skillId < 1 || skillId > _skills.length || tree < 0 || level < 1)
			return null;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(tree >= trees.length)
			return null;

		final L2Skill[] levels = trees[tree];
		if(level > levels.length)
			return null;

		return levels[level - 1];
	}

	/**
	 * Если уровень скилла больше максимального (без учёта заточки), то вернет максимальный базовый уровень скилла.
	 */
	public L2Skill getInfoSafe(final int skillId, int level)
	{
		if(skillId < 1 || skillId > _skills.length || level < 1)
			return null;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(trees.length <= 0)
			return null;

		// получаем базовые скилы
		final L2Skill[] levels = trees[0];
		if(level > levels.length)
			level = levels.length;

		if(getBaseLevel(skillId) < level)
			level = getBaseLevel(skillId);

		return levels[level - 1];
	}

	/**
	 * Returns the max skill level (exclusive enchant).<br>
	 * 
	 * @param skillId
	 *            The id of the skill.
	 * @return The max possible skill level (exclusive enchant, for example: 1 - 30).
	 */
	public final int getMaxLevel(final int skillId)
	{
		if(skillId < 1 || skillId > _skills.length)
			return 0;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(trees.length == 0)
			return 0;

		return trees[0].length;
	}

	/**
	 * Returns the max skill level.<br>
	 * 
	 * @param skillId
	 *            The id of the skill.
	 * @param tree
	 *            The tree of the skill. 0 = Base, 1 - 10 (inclusive) = Enchant.
	 * @return The max possible skill level (for example: 1 - 30).
	 */
	public final int getMaxLevel(final int skillId, final int tree)
	{
		if(skillId < 1 || skillId > _skills.length || tree < 0)
			return 0;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(tree >= trees.length)
			return 0;

		return trees[tree].length;
	}

	public final boolean isEnchantable(final int skillId)
	{
		if(skillId < 1 || skillId > _skills.length)
			return false;

		return _skills[skillId - 1].length > 1;
	}

	public L2Skill[] getAllLevels(final int skillId)
	{
		if(skillId < 1 || skillId > _skills.length)
			return EMPTY_ARRAY;

		final L2Skill[][] trees = _skills[skillId - 1];
		if(trees.length == 0)
			return EMPTY_ARRAY;

		L2Skill[] skills = null;
		for(int tree = trees.length; tree-- > 0;)
			skills = ArrayUtil.arrayAddToArray(skills, trees[tree], L2Skill.class);

		return skills;
	}

	public int getBaseLevel(final int skillId)
	{
		return _baseLevels[skillId];
	}

	/**
	 * @param initialized
	 *            The initialized to set.
	 */
	public void setInitialized(final boolean initialized)
	{
		_initialized = initialized;
	}

	/**
	 * @return Returns the initialized.
	 */
	public boolean isInitialized()
	{
		return _initialized;
	}

	/**
	 * Список наиболее частых скилов, используемых в ядре
	 */
	public static enum FrequentSkill
	{
		RAID_CURSE(4215, 1),
		RAID_CURSE2(4515, 1),
		SEAL_OF_RULER(246, 1),
		BUILD_HEADQUARTERS(247, 1),
		LUCKY(194, 1),
		DWARVEN_CRAFT(1321, 1),
		COMMON_CRAFT(1322, 1),
		WYVERN_BREATH(4289, 1),
		STRIDER_SIEGE_ASSAULT(325, 1),
		FAKE_PETRIFICATION(4616, 1),
		FIREWORK(5965, 1),
		LARGE_FIREWORK(2025, 1),
		BLESSING_OF_PROTECTION(5182, 1),
		ARENA_CP_RECOVERY(4380, 1),
		VOID_BURST(3630, 1),
		VOID_FLOW(3631, 1),
		THE_VICTOR_OF_WAR(5074, 1),
		THE_VANQUISHED_OF_WAR(5075, 1),
		SPECIAL_TREE_RECOVERY_BONUS(2139, 1);

		private final int id;
		private final int level;
		private L2Skill _skill = null;

		private FrequentSkill(final int id, final int level)
		{
			this.id = id;
			this.level = level;
		}

		public L2Skill getSkill()
		{
			return _skill;
		}
	}

	public static enum SubclassSkills
	{
		EmergentAbilityAttack(631),
		EmergentAbilityDefense(632),
		EmergentAbilityEmpower(633),
		EmergentAbilityMagicDefense(634),
		MasterAbilityAttack(637),
		MasterAbilityEmpower(638),
		MasterAbilityCasting(639),
		MasterAbilityFocus(640),
		MasterAbilityDefense(799),
		MasterAbilityMagicDefense(800),
		KnightAbilityBoostHP(641),
		KnightAbilityDefense(652),
		KnightAbilityResistCritical(804),
		EnchanterAbilityBoostMana(642),
		EnchanterAbilityManaRecycle(647),
		EnchanterAbilityBarrier(655),
		SummonerAbilityBoostHPMP(643),
		SummonerAbilityResistAttribute(1489),
		SummonerAbilitySpirit(1491),
		RogueAbilityEvasion(644),
		RogueAbilityLongShot(645),
		RogueAbilityCriticalChance(653),
		WizardAbilityManaGain(646),
		WizardAbilityManaSteal(654),
		WizardAbilityAntimagic(802),
		HealerAbilityPrayer(648),
		HealerAbilityHeal(1490),
		HealerAbilityDivineProtection(803),
		WarriorAbilityResistTrait(650),
		WarriorAbilityHaste(651),
		WarriorAbilityBoostCP(801),
		TransformDivineWarrior(656),
		TransformDivineKnight(657),
		TransformDivineRogue(658),
		TransformDivineWizard(659),
		TransformDivineSummoner(660),
		TransformDivineHealer(661),
		TransformDivineEnchanter(662);

		private final int _id;

		private SubclassSkills(final int id)
		{
			_id = id;
		}

		public int getId()
		{
			return _id;
		}

		public static boolean isSubclassSkill(final int id)
		{
			for(final SubclassSkills value : values())
				if(value.getId() == id)
					return true;
			return false;
		}

		// Certificates (Spellbooks) IDs
		public final static int CertificateEmergentAbility = 10280; // lvl 65

		public final static int CertificateMasterAbility = 10612; // lvl 75 - Master
		// level 75 - class based
		public final static int CertificateWarriorAbility = 10281;
		public final static int CertificateKnightAbility = 10282;
		public final static int CertificateRogueAbility = 10283;
		public final static int CertificateWizardAbility = 10284;
		public final static int CertificateHealerAbility = 10285;
		public final static int CertificateSummonerAbility = 10286;
		public final static int CertificateEnchanterAbility = 10287;
		// level 80 - Transform Sealbooks (class based)
		public final static int TransformSealbookDivineKnight = 10288;
		public final static int TransformSealbookDivineWarrior = 10289;
		public final static int TransformSealbookDivineRogue = 10290;
		public final static int TransformSealbookDivineHealer = 10291;
		public final static int TransformSealbookDivineWizard = 10292;
		public final static int TransformSealbookDivineEnchanter = 10293;
		public final static int TransformSealbookDivineSummoner = 10294;

		public static final int[] spellbooks = new int[] { 10612, 10280, 10281, 10282, 10283, 10284, 10285, 10286, 10287, 10288, 10289, 10290, 10291, 10292, 10293, 10294 };

		/**
		 * [0]Скилл <BR>
		 * [1]сертификат <BR>
		 * [2]минимальный уровень отображения
		 */
		public static final int[][] skills_certificates_levels = new int[][]
		{
				// СкУлл - сертУфУкат - мУнУмальный уровень отображенУя
				{ EmergentAbilityAttack.getId(), CertificateEmergentAbility, 65 },
				{ EmergentAbilityDefense.getId(), CertificateEmergentAbility, 65 },
				{ EmergentAbilityEmpower.getId(), CertificateEmergentAbility, 65 },
				{ EmergentAbilityMagicDefense.getId(), CertificateEmergentAbility, 65 },

				{ MasterAbilityAttack.getId(), CertificateMasterAbility, 75 },
				{ MasterAbilityEmpower.getId(), CertificateMasterAbility, 75 },
				{ MasterAbilityCasting.getId(), CertificateMasterAbility, 75 },
				{ MasterAbilityFocus.getId(), CertificateMasterAbility, 75 },
				{ MasterAbilityDefense.getId(), CertificateMasterAbility, 75 },
				{ MasterAbilityMagicDefense.getId(), CertificateMasterAbility, 75 },

				{ KnightAbilityBoostHP.getId(), CertificateKnightAbility, 75 },
				{ KnightAbilityDefense.getId(), CertificateKnightAbility, 75 },
				{ KnightAbilityResistCritical.getId(), CertificateKnightAbility, 75 },

				{ EnchanterAbilityBoostMana.getId(), CertificateEnchanterAbility, 75 },
				{ EnchanterAbilityManaRecycle.getId(), CertificateEnchanterAbility, 75 },
				{ EnchanterAbilityBarrier.getId(), CertificateEnchanterAbility, 75 },

				{ SummonerAbilityBoostHPMP.getId(), CertificateSummonerAbility, 75 },
				{ SummonerAbilityResistAttribute.getId(), CertificateSummonerAbility, 75 },
				{ SummonerAbilitySpirit.getId(), CertificateSummonerAbility, 75 },

				{ RogueAbilityEvasion.getId(), CertificateRogueAbility, 75 },
				{ RogueAbilityLongShot.getId(), CertificateRogueAbility, 75 },
				{ RogueAbilityCriticalChance.getId(), CertificateRogueAbility, 75 },

				{ WizardAbilityManaGain.getId(), CertificateWizardAbility, 75 },
				{ WizardAbilityManaSteal.getId(), CertificateWizardAbility, 75 },
				{ WizardAbilityAntimagic.getId(), CertificateWizardAbility, 75 },

				{ HealerAbilityPrayer.getId(), CertificateHealerAbility, 75 },
				{ HealerAbilityHeal.getId(), CertificateHealerAbility, 75 },
				{ HealerAbilityDivineProtection.getId(), CertificateHealerAbility, 75 },

				{ WarriorAbilityResistTrait.getId(), CertificateWarriorAbility, 75 },
				{ WarriorAbilityHaste.getId(), CertificateWarriorAbility, 75 },
				{ WarriorAbilityBoostCP.getId(), CertificateWarriorAbility, 75 },

				{ TransformDivineWarrior.getId(), TransformSealbookDivineWarrior, 80 },
				{ TransformDivineKnight.getId(), TransformSealbookDivineKnight, 80 },
				{ TransformDivineRogue.getId(), TransformSealbookDivineRogue, 80 },
				{ TransformDivineWizard.getId(), TransformSealbookDivineWizard, 80 },
				{ TransformDivineSummoner.getId(), TransformSealbookDivineSummoner, 80 },
				{ TransformDivineHealer.getId(), TransformSealbookDivineHealer, 80 },
				{ TransformDivineEnchanter.getId(), TransformSealbookDivineEnchanter, 80 }
		};

		// class based skills on level 75
		/**
		 * [0]Класс <BR>
		 * [1]второй сертификат (75) <BR>
		 * [2]третий сертификат (80)
		 */
		public static final int[][] classes_certificates = new int[][]
		{

				// --- Warriors ---

				// Gladiator
				{ 2, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 88, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Warlord
				{ 3, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 89, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Bounty Hunter
				{ 55, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 117, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Tyrant
				{ 48, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 114, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Destroyer
				{ 46, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 113, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Soul Breaker
				{ 128, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 132, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Soul Breaker
				{ 129, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 133, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// Berserker
				{ 127, CertificateWarriorAbility, TransformSealbookDivineWarrior },
				{ 131, CertificateWarriorAbility, TransformSealbookDivineWarrior },

				// --- Knights ---

				// Paladin
				{ 5, CertificateKnightAbility, TransformSealbookDivineKnight },
				{ 90, CertificateKnightAbility, TransformSealbookDivineKnight },

				// Dark Avenger
				{ 6, CertificateKnightAbility, TransformSealbookDivineKnight },
				{ 91, CertificateKnightAbility, TransformSealbookDivineKnight },

				// Temple Knight
				{ 20, CertificateKnightAbility, TransformSealbookDivineKnight },
				{ 99, CertificateKnightAbility, TransformSealbookDivineKnight },

				// Shillien Knight
				{ 33, CertificateKnightAbility, TransformSealbookDivineKnight },
				{ 106, CertificateKnightAbility, TransformSealbookDivineKnight },

				// --- Rogues ---

				// Hawkeye
				{ 9, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 92, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Silver Ranger
				{ 24, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 102, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Phantom Ranger
				{ 37, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 109, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Treasure Hunter
				{ 8, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 93, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Plains Walker
				{ 23, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 101, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Abyss Walker
				{ 36, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 108, CertificateRogueAbility, TransformSealbookDivineRogue },

				// Arbalester
				{ 130, CertificateRogueAbility, TransformSealbookDivineRogue },
				{ 134, CertificateRogueAbility, TransformSealbookDivineRogue },

				// --- Wizards ---

				// Sorcecer
				{ 12, CertificateWizardAbility, TransformSealbookDivineWizard },
				{ 94, CertificateWizardAbility, TransformSealbookDivineWizard },

				// Spellsinger
				{ 27, CertificateWizardAbility, TransformSealbookDivineWizard },
				{ 103, CertificateWizardAbility, TransformSealbookDivineWizard },

				// Spellhowler
				{ 40, CertificateWizardAbility, TransformSealbookDivineWizard },
				{ 110, CertificateWizardAbility, TransformSealbookDivineWizard },

				// Necromancer
				{ 13, CertificateWizardAbility, TransformSealbookDivineWizard },
				{ 95, CertificateWizardAbility, TransformSealbookDivineWizard },

				// --- Healers ---

				// Shillien Elder
				{ 43, CertificateHealerAbility, TransformSealbookDivineHealer },
				{ 112, CertificateHealerAbility, TransformSealbookDivineHealer },

				// Elder
				{ 30, CertificateHealerAbility, TransformSealbookDivineHealer },
				{ 105, CertificateHealerAbility, TransformSealbookDivineHealer },

				// Bishop
				{ 16, CertificateHealerAbility, TransformSealbookDivineHealer },
				{ 97, CertificateHealerAbility, TransformSealbookDivineHealer },

				// --- Summoners ---

				// Warlock
				{ 14, CertificateSummonerAbility, TransformSealbookDivineSummoner },
				{ 96, CertificateSummonerAbility, TransformSealbookDivineSummoner },

				// Elemental Summoner
				{ 28, CertificateSummonerAbility, TransformSealbookDivineSummoner },
				{ 104, CertificateSummonerAbility, TransformSealbookDivineSummoner },

				// Phantom Summoner
				{ 41, CertificateSummonerAbility, TransformSealbookDivineSummoner },
				{ 111, CertificateSummonerAbility, TransformSealbookDivineSummoner },

				// --- Enchanters ---

				// Prophet
				{ 17, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },
				{ 98, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },

				// Warcryer
				{ 52, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },
				{ 116, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },

				// Inspector
				{ 135, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },
				{ 136, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },

				// Sword Singer
				{ 21, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },
				{ 100, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },

				// Bladedancer
				{ 34, CertificateEnchanterAbility, TransformSealbookDivineEnchanter },
				{ 107, CertificateEnchanterAbility, TransformSealbookDivineEnchanter }
		};

		/**
		 * ПолучУть ИД спелбука для УзученУя данного субскУлла
		 * 
		 * @param skill_id
		 *            ИД субскУлла
		 * @return УтемИД спеллбука
		 */
		public static int getCertificateForSkill(final int skill_id)
		{
			for(final int[] tmp : skills_certificates_levels)
				if(tmp[0] == skill_id)
					return tmp[1];
			return -1;
		}
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final SkillTable _instance = new SkillTable();
	}
}
