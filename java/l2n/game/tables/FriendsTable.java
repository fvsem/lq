package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.primitive.IntArrayList;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.FriendAddRequest;
import l2n.game.network.serverpackets.L2Friend;
import l2n.game.network.serverpackets.SystemMessage;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FriendsTable
{
	private static final Logger _log = Logger.getLogger(FriendsTable.class.getName());

	private static final IntArrayList EMPTY_FRIENDS = new IntArrayList(0);

	private TIntObjectHashMap<IntArrayList> _friends;

	public static FriendsTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private FriendsTable()
	{
		_friends = new TIntObjectHashMap<IntArrayList>();
		restoreFriendsData();
	}

	private void restoreFriendsData()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet friendsdata = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_id, friend_id FROM character_friends");
			friendsdata = statement.executeQuery();

			int i = 0;
			while (friendsdata.next())
			{
				add(friendsdata.getInt("char_id"), friendsdata.getInt("friend_id"));
				i++;
			}

			_log.config("FriendsTable: Loaded " + i + " friends.");
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Error while loading friends table!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, friendsdata);
		}
	}

	private void add(int char_id, int friend_id)
	{
		IntArrayList friends = _friends.get(char_id);
		if(friends == null)
		{
			friends = new IntArrayList(1);
			_friends.put(char_id, friends);
		}
		friends.add(friend_id);
	}

	public void addFriend(L2Player player1, L2Player player2)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO character_friends (char_id,friend_id) VALUES(?,?)");
			statement.setInt(1, player1.getObjectId());
			statement.setInt(2, player2.getObjectId());
			statement.execute();
			DbUtils.closeQuietly(statement);

			statement = con.prepareStatement("REPLACE INTO character_friends (char_id,friend_id) VALUES(?,?)");
			statement.setInt(1, player2.getObjectId());
			statement.setInt(2, player1.getObjectId());
			statement.execute();

			add(player1.getObjectId(), player2.getObjectId());
			add(player2.getObjectId(), player1.getObjectId());

			player1.sendPacket(Msg.YOU_HAVE_SUCCEEDED_IN_INVITING_A_FRIEND, new SystemMessage(SystemMessage.S1_HAS_BEEN_ADDED_TO_YOUR_FRIEND_LIST).addString(player2.getName()), new L2Friend(player2, true));
			player2.sendPacket(new SystemMessage(SystemMessage.S1_HAS_JOINED_AS_A_FRIEND).addString(player1.getName()), new L2Friend(player1, true));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Error while add friends!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean TryFriendDelete(L2Player activeChar, String delFriend)
	{
		if(activeChar == null || delFriend == null || delFriend.isEmpty())
			return false;

		delFriend = delFriend.trim();

		L2Player friendChar = L2ObjectsStorage.getPlayer(delFriend);
		if(friendChar != null)
			delFriend = friendChar.getName();

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT obj_Id FROM characters WHERE char_name LIKE ? LIMIT 1");
			statement.setString(1, delFriend);
			rset = statement.executeQuery();
			if(!rset.next())
			{
				_log.log(Level.WARNING, "FriendsTable: not found char to delete: " + delFriend);
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_ON_YOUR_FRIEND_LIST).addString(delFriend));
				return false;
			}

			int friendId = rset.getInt("obj_Id");
			if(!checkIsFriends(activeChar.getObjectId(), friendId))
			{
				_log.log(Level.WARNING, "FriendsTable: not in friend list: " + activeChar.getObjectId() + ", " + delFriend);
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_ON_YOUR_FRIEND_LIST).addString(delFriend));
				return false;
			}

			DbUtils.closeQuietly(statement, rset);
			rset = null;

			statement = con.prepareStatement("DELETE FROM character_friends WHERE (char_id=? AND friend_id=?) OR (char_id=? AND friend_id=?)");
			statement.setInt(1, activeChar.getObjectId());
			statement.setInt(2, friendId);
			statement.setInt(3, friendId);
			statement.setInt(4, activeChar.getObjectId());
			statement.execute();

			IntArrayList friends = _friends.get(activeChar.getObjectId());
			if(friends != null)
				friends.removeValue(friendId);

			friends = _friends.get(friendId);
			if(friends != null)
				friends.removeValue(activeChar.getObjectId());

			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_BEEN_REMOVED_FROM_YOUR_FRIEND_LIST).addString(delFriend), new L2Friend(delFriend, false, friendChar != null, friendId));

			if(friendChar != null)
				friendChar.sendPacket(new SystemMessage(SystemMessage.S1__HAS_BEEN_DELETED_FROM_YOUR_FRIENDS_LIST).addString(activeChar.getName()), new L2Friend(activeChar, false));
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "Error while try delete friends!", e);
			return false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return true;
	}

	public boolean TryFriendInvite(L2Player activeChar, String addFriend)
	{
		if(activeChar == null || addFriend == null || addFriend.isEmpty())
			return false;

		if(activeChar.isInTransaction())
		{
			activeChar.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
			return false;
		}

		if(activeChar.getName().equalsIgnoreCase(addFriend))
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_ADD_YOURSELF_TO_YOUR_OWN_FRIEND_LIST);
			return false;
		}

		L2Player friendChar = L2ObjectsStorage.getPlayer(addFriend);
		if(friendChar == null)
		{
			activeChar.sendPacket(Msg.THE_USER_WHO_REQUESTED_TO_BECOME_FRIENDS_IS_NOT_FOUND_IN_THE_GAME);
			return false;
		}

		if(friendChar.isBlockAll() || friendChar.isInBlockList(activeChar) || friendChar.getMessageRefusal())
		{
			activeChar.sendPacket(Msg.THE_PERSON_IS_IN_A_MESSAGE_REFUSAL_MODE);
			return false;
		}

		if(friendChar.isInTransaction())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(friendChar.getName()));
			return false;
		}

		if(FriendsTable.getInstance().checkIsFriends(activeChar.getObjectId(), activeChar.getObjectId()))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_ALREADY_ON_YOUR_FRIEND_LIST).addString(friendChar.getName()));
			return false;
		}

		new Transaction(TransactionType.FRIEND, activeChar, friendChar, 10000);
		friendChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_REQUESTED_TO_BECOME_FRIENDS).addString(activeChar.getName()), new FriendAddRequest(activeChar.getName()));

		return true;
	}

	public IntArrayList getFriendsList(int char_id)
	{
		IntArrayList friends = _friends.get(char_id);
		if(friends == null)
			return EMPTY_FRIENDS;
		return friends;
	}

	public boolean checkIsFriends(int char_id, int friend_id)
	{
		for(int obj_id : getFriendsList(char_id))
			if(obj_id == friend_id)
				return true;
		for(int obj_id : getFriendsList(friend_id))
			if(obj_id == char_id)
			{
				_log.log(Level.WARNING, "FriendsTable: corrupted friends table! " + char_id + "," + friend_id);
				return true;
			}
		return false;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final FriendsTable _instance = new FriendsTable();
	}
}
