package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.tables.PetSkillsTable.L2PetSkillLearn;
import l2n.util.Log;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author L2System Project
 * @date 04.05.2011
 * @time 12:04:54
 */
public class SupportPetsSkillsTable
{
	private static final Logger _log = Logger.getLogger(SupportPetsSkillsTable.class.getName());

	private final TIntObjectHashMap<GArray<L2PetSkillLearn>> _support_skills;

	public static SupportPetsSkillsTable getInstance()
	{
		return SingletonHolder._instance;
	}

	public SupportPetsSkillsTable()
	{
		_support_skills = new TIntObjectHashMap<GArray<L2PetSkillLearn>>();
		load();
	}

	private void load()
	{
		GArray<Integer> badSkill = new GArray<Integer>();
		int count = 0;
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			File f = new File(Config.DATAPACK_ROOT, "data/support_pet_skills.xml");
			if(!f.exists())
			{
				_log.warning("Error in loading support pet skills, data/support_pet_skills.xml file not found!");
				return;
			}

			Document doc = factory.newDocumentBuilder().parse(f);
			for(Node main = doc.getFirstChild(); main != null; main = main.getNextSibling())
				if("list".equalsIgnoreCase(main.getNodeName()))
					for(Node setting = main.getFirstChild(); setting != null; setting = setting.getNextSibling())
						if("pet".equalsIgnoreCase(setting.getNodeName()))
						{
							int petId = XMLUtil.getAttributeIntValue(setting, "id", 0);
							if(petId == 0)
								continue;

							// Обрабатываем двери для ключа
							GArray<L2PetSkillLearn> skills = new GArray<L2PetSkillLearn>();
							for(Node dnode = setting.getFirstChild(); dnode != null; dnode = dnode.getNextSibling())
								if("skill".equalsIgnoreCase(dnode.getNodeName()))
								{
									int skillId = XMLUtil.getAttributeIntValue(dnode, "skillId", 0);
									int skillLvl = XMLUtil.getAttributeIntValue(dnode, "skillLvl", 0);
									int minLvl = XMLUtil.getAttributeIntValue(dnode, "minLvl", 0);

									L2Skill skill = SkillTable.getInstance().getInfo(skillId, 1);
									if(skill == null || skill.getSkillType() == SkillType.NOTDONE)
									{
										if(!badSkill.contains(skillId))
											badSkill.add(skillId);
										continue;
									}

									if(skillId == 0 || skillLvl == 0)
										continue;

									skills.add(new L2PetSkillLearn(skillId, skillLvl, minLvl));
								}
							_support_skills.put(petId, skills);
							count += skills.size();
						}
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "SupportPetsSkillsTable: error: ", e);
		}

		for(int badId : badSkill)
			Log.addDev("[" + badId + "] " + SkillTable.getInstance().getInfo(badId, 1), "dev_notdone_support_pet_skills", false);

		_log.info("SupportPetsSkillsTable: loaded skills for " + _support_skills.size() + " pets.");
		_log.info("SupportPetsSkillsTable: loaded " + count + " skills.");
	}

	public GArray<L2PetSkillLearn> getSkills(int summonId)
	{
		return _support_skills.get(summonId);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final SupportPetsSkillsTable _instance = new SupportPetsSkillsTable();
	}
}
