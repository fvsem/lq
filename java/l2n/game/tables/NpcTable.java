package l2n.game.tables;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TextBuilder;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Scripts;
import l2n.game.ai.model.AdditionalAIParams;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.DropListCache;
import l2n.game.model.Elementals;
import l2n.game.model.L2MinionData;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.base.ClassId;
import l2n.game.model.drop.L2DropData;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2SiegeGuardInstance;
import l2n.game.model.instances.L2TamedBeastInstance;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.DropList;
import l2n.util.Log;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NpcTable
{
	private static final Logger _log = Logger.getLogger(NpcTable.class.getName());

	private static L2NpcTemplate[] _npcs;

	private static TIntObjectHashMap<AdditionalAIParams> _ai_params;
	private static HashMap<String, L2NpcTemplate> _npcsNames;
	private static GArray<L2NpcTemplate>[] _npcsByLevel;

	private boolean _initialized = false;

	private static final double[] hprateskill = { 0.0, 1.0, 1.2, 1.3, 2.0, 2.0, 4.0, 4.0, 0.25, 0.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0 };

	public static NpcTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final NpcTable _instance = new NpcTable();
	}

	@SuppressWarnings("unchecked")
	private NpcTable()
	{
		_npcsByLevel = new GArray[100];
		_npcsNames = new HashMap<String, L2NpcTemplate>();
		_ai_params = new TIntObjectHashMap<AdditionalAIParams>();
		restoreNpcData();
	}

	@SuppressWarnings("unchecked")
	private void restoreNpcData()
	{
		if(_npcs != null && _npcs.length != 0)
		{
			_npcsByLevel = new GArray[100];
			_npcsNames = new HashMap<String, L2NpcTemplate>();
			_ai_params = new TIntObjectHashMap<AdditionalAIParams>();
			Util.gc(1, 1000);
		}

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			try
			{
				rs = statement.executeQuery("SELECT * FROM ai_params");
				loadAIParams(rs);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error while creating ai params ", e);
			}
			finally
			{
				DbUtils.close(rs);
			}

			try
			{
				rs = statement.executeQuery("SELECT * FROM npc AS c LEFT JOIN npc_elementals AS cs ON (c.id = cs.npc_id) WHERE ai_type IS NOT NULL");
				fillNpcTable(rs);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error while creating npc table ", e);
			}
			finally
			{
				DbUtils.close(rs);
			}

			try
			{
				rs = statement.executeQuery("SELECT npcid, skillid, level FROM npcskills");
				L2NpcTemplate npcDat;
				L2Skill npcSkill;

				final TIntArrayList unimpl = new TIntArrayList();
				int counter = 0;
				while (rs.next())
				{
					final int mobId = rs.getInt("npcid");
					npcDat = _npcs[mobId];
					if(npcDat == null)
						continue;

					final int skillId = rs.getInt("skillid");
					final byte level = rs.getByte("level");

					// Для определенУя расы Успользуется скУлл 4416
					if(skillId == 4416)
						npcDat.setRace(level);
					// continue;
					else if(skillId >= 4290 && skillId <= 4302)
					{
						_log.info("Warning! Skill " + skillId + " not used, use 4416 instead.");
						continue;
					}

					// проверка на множУтель ХП
					if(skillId == 4408)
						npcDat.setRateHp(hprateskill[level]);

					npcSkill = SkillTable.getInstance().getInfo(skillId, level);

					if(!unimpl.contains(skillId) && (npcSkill == null || npcSkill.getSkillType() == SkillType.NOTDONE))
						unimpl.add(skillId);

					if(npcSkill == null)
						continue;

					npcDat.addSkill(npcSkill);
					counter++;
				}

				if(!unimpl.isEmpty())
				{
					unimpl.sort();
					final int[] skillIds = unimpl.toArray();
					for(final int skillId : skillIds)
						Log.addDev("[" + skillId + "] " + SkillTable.getInstance().getInfo(skillId, 1), "dev_notdone_npc_skills", false);
				}

				_log.info("NpcTable: Loaded " + counter + " npc skills.");

			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error while reading npcskills table ", e);
			}
			finally
			{
				DbUtils.close(rs);
			}

			try
			{
				rs = statement.executeQuery("SELECT mobId, itemId, min, max, chance, category FROM droplist ORDER BY mobId, category, chance");
				L2DropData dropDat = null;
				L2NpcTemplate npcDat = null;

				while (rs.next())
				{
					final int mobId = rs.getInt("mobId");
					if(mobId > _npcs.length)
					{
						_log.log(Level.SEVERE, "NpcTable: error reading drops for npc " + mobId);
						continue;
					}
					npcDat = _npcs[mobId];
					if(npcDat != null)
					{
						dropDat = new L2DropData();

						final int id = rs.getInt("itemId");
						if(ItemTable.getInstance().getTemplate(id).isCommonItem())
						{
							if(Config.ALT_ALLOW_DROP_COMMON)
							{
								dropDat.setChance(rs.getInt("chance") * Config.RATE_DROP_COMMON_ITEMS);
								dropDat.setItemId(id);
							}
							else
							{
								final int normalid = ItemTable.getInstance().findBaseId(id);
								dropDat.setChance(rs.getInt("chance") * ItemTable.getInstance().getTemplate(id).getReferencePrice() / ItemTable.getInstance().getTemplate(normalid).getReferencePrice());
								dropDat.setItemId(normalid);
							}
						}
						else
						{
							dropDat.setItemId(id);
							dropDat.setChance(rs.getInt("chance"));
						}
						dropDat.setMinDrop(rs.getInt("min"));
						dropDat.setMaxDrop(rs.getInt("max"));
						dropDat.setSweep(rs.getInt("category") == -1);
						if(dropDat.getItem().isArrow() || dropDat.getItemId() == 1419)
							dropDat.setGroupId(Byte.MAX_VALUE); // группа для нерейтуемых предметов, сюда же надо всякую фУгню
						else
							dropDat.setGroupId(rs.getInt("category"));
						npcDat.addDropData(dropDat);
					}
				}

				int errors = 0;
				for(final L2NpcTemplate temp : getAll())
					if(temp != null && temp.getDropData() != null && !temp.getDropData().validate() || temp != null && temp.getDropData() != null && !temp.isInstanceOf(L2MonsterInstance.class) && !temp.isInstanceOf(L2SiegeGuardInstance.class))
					{
						errors++;
						Log.addDev(temp.name + "\t" + temp.npcId, "dev_droplist", false);
					}

				if(errors > 0)
					_log.info("NpcTable: droplist problems in " + errors + " mobs!");

				if(Config.ALT_GAME_SHOW_DROPLIST && !Config.ALT_GAME_GEN_DROPLIST_ON_DEMAND)
					fillDropList();
				else
					_log.info("NpcTable: Players droplist load skipped");

				if(Config.KILL_COUNTER_PRELOAD && Config.KILL_COUNTER)
					loadKillCount();
				else
					_log.info("NpcTable: Kills counter load skipped");

			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error reading npc drops ", e);
			}
			finally
			{
				DbUtils.close(rs);
			}

			try
			{
				rs = statement.executeQuery("SELECT boss_id, minion_id, amount FROM minions");
				L2MinionData minionDat = null;
				L2NpcTemplate npcDat = null;
				int cnt = 0;

				while (rs.next())
				{
					final int raidId = rs.getInt("boss_id");
					npcDat = _npcs[raidId];
					minionDat = new L2MinionData();
					minionDat.setMinionId(rs.getInt("minion_id"));
					minionDat.setAmount(rs.getByte("amount"));
					npcDat.addRaidData(minionDat);
					cnt++;
				}

				_log.info("NpcTable: Loaded " + cnt + " Minions.");
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error loading minions", e);
			}
			finally
			{
				DbUtils.close(rs);
			}

			try
			{
				rs = statement.executeQuery("SELECT npc_id, class_id FROM skill_learn");
				L2NpcTemplate npcDat = null;
				int cnt = 0;

				while (rs.next())
				{
					npcDat = _npcs[rs.getInt(1)];
					npcDat.addTeachInfo(ClassId.values()[rs.getInt(2)]);
					cnt++;
				}

				_log.info("NpcTable: Loaded " + cnt + " SkillLearn entrys.");
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "error loading skill learn.", e);
			}
			finally
			{
				DbUtils.close(rs);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Cannot find connection to database");
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		_initialized = true;

		Scripts.getInstance();
	}

	private static void loadAIParams(final ResultSet data) throws Exception
	{
		int ai_params_counter = 0;

		AdditionalAIParams options;
		AiOptionsType type;
		String value;
		while (data.next())
		{
			final int npc_id = data.getInt("npc_id");
			type = AiOptionsType.getByName(data.getString("param"));
			value = data.getString("value");
			if(_ai_params.containsKey(npc_id))
				options = _ai_params.get(npc_id);
			else
			{
				options = new AdditionalAIParams();
				_ai_params.put(npc_id, options);
			}
			options.addOption(npc_id, type, value);
			ai_params_counter++;
		}
		_log.info("NpcTable: Loaded " + ai_params_counter + " AI params for " + _ai_params.size() + " NPCs.");
	}

	private static StatsSet fillNpcTable(final ResultSet data) throws Exception
	{
		StatsSet npcDat = null;
		int maxId = 0;
		final GArray<L2NpcTemplate> temp = new GArray<L2NpcTemplate>(10000);
		while (data.next())
		{
			npcDat = new StatsSet();
			final int id = data.getInt("id");
			final int level = data.getByte("level");

			if(maxId < id)
				maxId = id;

			npcDat.set("npcId", id);
			npcDat.set("displayId", data.getInt("displayId"));
			npcDat.set("level", level);
			npcDat.set("jClass", data.getString("class"));

			npcDat.set("name", data.getString("name"));
			npcDat.set("title", data.getString("title"));
			npcDat.set("collision_radius", data.getDouble("collision_radius"));
			npcDat.set("collision_height", data.getDouble("collision_height"));
			npcDat.set("sex", data.getString("sex"));
			npcDat.set("type", data.getString("type"));
			npcDat.set("ai_type", data.getString("ai_type"));
			npcDat.set("baseAtkRange", data.getInt("attackrange"));
			npcDat.set("revardExp", data.getInt("exp"));
			npcDat.set("revardSp", data.getInt("sp"));
			npcDat.set("basePAtkSpd", data.getInt("atkspd"));
			npcDat.set("baseMAtkSpd", data.getInt("matkspd"));
			npcDat.set("aggroRange", data.getShort("aggro"));
			npcDat.set("rhand", data.getInt("rhand"));
			npcDat.set("lhand", data.getInt("lhand"));
			npcDat.set("armor", data.getInt("armor"));
			npcDat.set("baseWalkSpd", data.getInt("walkspd"));
			npcDat.set("baseRunSpd", data.getInt("runspd"));

			npcDat.set("baseHpReg", data.getDouble("base_hp_regen"));
			npcDat.set("baseCpReg", 0);
			npcDat.set("baseMpReg", data.getDouble("base_mp_regen"));

			npcDat.set("baseSTR", data.getInt("str"));
			npcDat.set("baseCON", data.getInt("con"));
			npcDat.set("baseDEX", data.getInt("dex"));
			npcDat.set("baseINT", data.getInt("int"));
			npcDat.set("baseWIT", data.getInt("wit"));
			npcDat.set("baseMEN", data.getInt("men"));

			npcDat.set("baseHpMax", data.getInt("hp"));
			npcDat.set("baseCpMax", 0);
			npcDat.set("baseMpMax", data.getInt("mp"));
			npcDat.set("basePAtk", data.getInt("patk"));
			npcDat.set("basePDef", data.getInt("pdef"));
			npcDat.set("baseMAtk", data.getInt("matk"));
			npcDat.set("baseMDef", data.getInt("mdef"));

			npcDat.set("baseShldDef", data.getInt("shield_defense"));
			npcDat.set("baseShldRate", data.getInt("shield_defense_rate"));

			if(data.getString("type").equalsIgnoreCase("L2Pet"))
				if(data.getString("name").equalsIgnoreCase("Cursed Man"))
					npcDat.set("baseCritRate", 80);
				else
					npcDat.set("baseCritRate", 44);
			else
				npcDat.set("baseCritRate", Math.max(1, data.getInt("base_critical")) * 10);

			final String factionId = data.getString("faction_id");
			if(factionId != null)
				factionId.trim();
			npcDat.set("factionId", factionId);
			npcDat.set("factionRange", factionId == null || factionId.equals("") ? 0 : data.getShort("faction_range"));

			npcDat.set("drop_herbs", data.getInt("drop_herbs"));

			npcDat.set("shots", data.getString("shots"));

			// load elementals data
			if(Config.ALT_LOAD_ELEMENTSTABLE)
				if(data.getInt("npc_id") > 0 && data.getInt("npc_id") == id)
				{
					switch (data.getByte("elem_atk_type"))
					{
						case Elementals.ATTRIBUTE_FIRE:
							npcDat.set("baseFireAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_WATER:
							npcDat.set("baseWaterAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_WIND:
							npcDat.set("baseWindAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_EARTH:
							npcDat.set("baseEarthAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_SACRED:
							npcDat.set("baseSacredAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_UNHOLY:
							npcDat.set("baseUnholyAttack", data.getInt("elem_atk_value"));
							break;
						case Elementals.ATTRIBUTE_NONE:
							break;
						default:
							_log.severe("NpcTable: elementals error with id: " + id + "; unknown elementType: " + data.getByte("elem_atk_type"));
					}

					// defens value
					npcDat.set("baseFireDefend", data.getInt("fire_def_value"));
					npcDat.set("baseWaterDefend", data.getInt("water_def_value"));
					npcDat.set("baseWindDefend", data.getInt("wind_def_value"));
					npcDat.set("baseEarthDefend", data.getInt("earth_def_value"));
					npcDat.set("baseSacredDefend", data.getInt("holy_def_value"));
					npcDat.set("baseUnholyDefend", data.getInt("dark_def_value"));
				}

			final L2NpcTemplate template = new L2NpcTemplate(npcDat, _ai_params.get(id));
			temp.add(template);
			if(_npcsByLevel[level] == null)
				_npcsByLevel[level] = new GArray<L2NpcTemplate>();
			_npcsByLevel[level].add(template);
			_npcsNames.put(data.getString("name").toLowerCase(), template);
		}

		_npcs = new L2NpcTemplate[maxId + 1];
		for(final L2NpcTemplate template : temp)
			_npcs[template.npcId] = template;

		_log.info("NpcTable: Loaded " + temp.size() + " Npc Templates.");

		return npcDat;
	}

	// just wrapper
	public void reloadAllNpc()
	{
		restoreNpcData();
	}

	public boolean isInitialized()
	{
		return _initialized;
	}

	public static void replaceTemplate(final L2NpcTemplate npc)
	{
		_npcs[npc.npcId] = npc;
		_npcsNames.put(npc.name.toLowerCase(), npc);
	}

	public static L2NpcTemplate getTemplate(final int id)
	{
		return _npcs[id];
	}

	public static L2NpcTemplate getTemplateByName(final String name)
	{
		return _npcsNames.get(name.toLowerCase());
	}

	public static GArray<L2NpcTemplate> getAllOfLevel(final int lvl)
	{
		return _npcsByLevel[lvl];
	}

	public static L2NpcTemplate[] getAll()
	{
		return _npcs;
	}

	public void fillDropList()
	{
		for(final L2NpcTemplate npc : getAll())
			if(npc != null)
				DropListCache.add(npc.npcId, DropList.generateDroplist(npc, null, 1., null));
		_log.info("NpcTable: Players droplist was cached");
	}

	public void applyServerSideTitle()
	{
		if(Config.SERVER_SIDE_NPC_TITLE_WITH_LVL)
			for(final L2NpcTemplate npc : _npcs)
				if(npc != null && npc.isInstanceOf(L2MonsterInstance.class) && !npc.isInstanceOf(L2TamedBeastInstance.class))
				{
					String title = "L" + npc.level;
					if(npc.aggroRange != 0 || npc.factionRange != 0)
						title += " " + (npc.aggroRange != 0 ? "A" : "") + (npc.factionRange != 0 ? "S" : "");
					title += " ";
					npc.title = title + npc.title;
				}
	}

	public static void storeKillsCount()
	{
		ThreadConnection con = null;
		FiltredStatement fs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			final TextBuilder sb = TextBuilder.newInstance();
			fs = con.createStatement();

			for(final L2NpcTemplate t : getAll())
			{
				if(t == null || t.killscount <= 0)
					continue;

				fs.addBatch("REPLACE INTO `killcount` SET `npc_id`=" + t.npcId + ", `count`=" + t.killscount + ", `char_id`=-1");
				sb.clear();
			}
			TextBuilder.recycle(sb);
			fs.executeBatch();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, fs);
		}
	}

	private void loadKillCount()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet list = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM `killcount` WHERE `char_id`=-1");
			list = statement.executeQuery();
			while (list.next())
			{
				final L2NpcTemplate t = getTemplate(list.getInt("npc_id"));
				if(t != null)
					t.killscount = list.getInt("count");
			}
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, list);
		}
	}
}
