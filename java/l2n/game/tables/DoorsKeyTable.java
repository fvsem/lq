package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DoorsKeyTable
{
	public static class L2KeyAction
	{
		public int doorId;
		public int chance;
		public String failed_msg;
		public String openmsg;
		public boolean disappear;

		public L2KeyAction(int doorid, int _chance, String _failed_msg, String _openmsg, boolean _disappear)
		{
			doorId = doorid;
			chance = _chance;
			failed_msg = _failed_msg;
			openmsg = _openmsg;
			disappear = _disappear;
		}
	}

	private final static Logger _log = Logger.getLogger(DoorsKeyTable.class.getName());

	private final static TIntObjectHashMap<L2KeyAction[]> _keysActions = new TIntObjectHashMap<L2KeyAction[]>();

	public static DoorsKeyTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DoorsKeyTable _instance = new DoorsKeyTable();
	}

	public DoorsKeyTable()
	{
		parseData();
	}

	public void parseData()
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			File f = new File(Config.DATAPACK_ROOT, "data/door_keys.xml");
			if(!f.exists())
			{
				_log.warning("Error in loading door keys data, data/door_keys.xml file not found!");
				return;
			}

			Document doc = factory.newDocumentBuilder().parse(f);
			for(Node main = doc.getFirstChild(); main != null; main = main.getNextSibling())
				if("list".equalsIgnoreCase(main.getNodeName()))
					for(Node setting = main.getFirstChild(); setting != null; setting = setting.getNextSibling())
						if("key".equalsIgnoreCase(setting.getNodeName()))
						{
							int itemid = XMLUtil.getAttributeIntValue(setting, "item_id", 0);
							if(itemid == 0)
								continue;

							GArray<L2KeyAction> actions = new GArray<L2KeyAction>();
							for(Node dnode = setting.getFirstChild(); dnode != null; dnode = dnode.getNextSibling())
								if("door".equalsIgnoreCase(dnode.getNodeName()))
								{
									int doorId = XMLUtil.getAttributeIntValue(dnode, "id", 0);
									int chance = XMLUtil.getAttributeIntValue(dnode, "chance", 0);
									if(doorId == 0 || chance == 0)
										continue;

									if(DoorTable.getInstance().getDoor(doorId) == null)
										_log.warning("DoorsKeyTable: door [" + doorId + "] not exist!");

									String failed_msg = XMLUtil.getAttributeValue(dnode, "failed_msg", null);
									String openmsg = XMLUtil.getAttributeValue(dnode, "open_msg", null);
									boolean disappear = XMLUtil.getAttributeBooleanValue(dnode, "disappear", true);
									actions.add(new L2KeyAction(doorId, chance, failed_msg, openmsg, disappear));
								}
							_keysActions.put(itemid, actions.toArray(new L2KeyAction[actions.size()]));
						}

			_log.info("DoorsKeyTable: Loaded " + _keysActions.size() + " keys actions.");
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "DoorsKeyTable: error: ", e);
		}
	}

	public static L2KeyAction[] get(Integer key)
	{
		return _keysActions.get(key);
	}

	public static int[] keys()
	{
		return _keysActions.keys();
	}
}
