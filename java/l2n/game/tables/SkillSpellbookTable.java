package l2n.game.tables;

import gnu.trove.map.hash.TIntIntHashMap;
import l2n.Config;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SkillSpellbookTable
{
	private static final Logger _log = Logger.getLogger(SkillTreeTable.class.getName());

	private static final SkillSpellbookTable _instance = new SkillSpellbookTable();

	public static TIntIntHashMap _skillSpellbooks;

	public static SkillSpellbookTable getInstance()
	{
		return _instance;
	}

	private SkillSpellbookTable()
	{
		_skillSpellbooks = new TIntIntHashMap();
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet spbooks = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();

			/**
			 * Раньше еслУ ALT_DISABLE_SPELLBOOKS = true, то не отображалУсь яйца для клан скУлов. А так же не запрашУвал Утемы для fish скУлов У тд.
			 */
			if(Config.ALT_DISABLE_SPELLBOOKS)
			{
				_log.config("SkillSpellbookTable: Spellbooks disabled, loading skipped.");
				_log.config("SkillSpellbookTable: Loading clan, fish, transform skillbooks");
				spbooks = statement.executeQuery("SELECT skill_id, level, item_id FROM skill_spellbooks WHERE skill_id NOT IN (SELECT DISTINCT(skill_id) FROM skill_trees_player)");
			}
			else
				spbooks = statement.executeQuery("SELECT skill_id, level, item_id FROM skill_spellbooks");

			while (spbooks.next())
				_skillSpellbooks.put(hashCode(new int[] { spbooks.getShort("skill_id"), spbooks.getShort("level") }), spbooks.getInt("item_id"));

			_log.config("SkillSpellbookTable: Loaded " + _skillSpellbooks.size() + " Spellbooks.");
		}
		catch(Exception e)
		{
			_log.log(Level.WARNING, "error while loading spellbooks	", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, spbooks);
		}
	}

	public static int hashCode(int a[])
	{
		if(a == null)
			return 0;
		return a[1] + (a[0] << 16);
	}
}
