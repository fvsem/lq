package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Territory;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.templates.L2CharTemplate;
import l2n.util.Log;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DoorTable
{
	private static final Logger _log = Logger.getLogger(DoorTable.class.getName());

	private final TIntObjectHashMap<L2DoorInstance> _doorList;

	public static DoorTable getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DoorTable _instance = new DoorTable();
	}

	public DoorTable()
	{
		_doorList = new TIntObjectHashMap<L2DoorInstance>();
		restoreDoors();
	}

	private void restoreDoors()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rs = statement.executeQuery("SELECT * FROM doors");
			fillDoors(rs);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Cannot load doors", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	private void fillDoors(final ResultSet DoorData) throws Exception
	{
		final StatsSet baseDat = new StatsSet();
		baseDat.set("level", 0);
		baseDat.set("jClass", "door");
		baseDat.set("baseSTR", 0);
		baseDat.set("baseCON", 0);
		baseDat.set("baseDEX", 0);
		baseDat.set("baseINT", 0);
		baseDat.set("baseWIT", 0);
		baseDat.set("baseMEN", 0);
		baseDat.set("baseShldDef", 0);
		baseDat.set("baseShldRate", 0);
		baseDat.set("baseAccCombat", 38);
		baseDat.set("baseEvasRate", 38);
		baseDat.set("baseCritRate", 38);
		baseDat.set("collision_radius", 5);
		baseDat.set("collision_height", 0);
		baseDat.set("sex", "male");
		baseDat.set("type", "");
		baseDat.set("baseAtkRange", 0);
		baseDat.set("baseMpMax", 0);
		baseDat.set("baseCpMax", 0);
		baseDat.set("revardExp", 0);
		baseDat.set("revardSp", 0);
		baseDat.set("basePAtk", 0);
		baseDat.set("baseMAtk", 0);
		baseDat.set("basePAtkSpd", 0);
		baseDat.set("aggroRange", 0);
		baseDat.set("baseMAtkSpd", 0);
		baseDat.set("rhand", 0);
		baseDat.set("lhand", 0);
		baseDat.set("armor", 0);
		baseDat.set("baseWalkSpd", 0);
		baseDat.set("baseRunSpd", 0);
		baseDat.set("baseHpReg", 0);
		baseDat.set("baseCpReg", 0);
		baseDat.set("baseMpReg", 0);
		baseDat.set("siege_weapon", false);
		baseDat.set("geodata", true);

		StatsSet npcDat;
		while (DoorData.next())
		{
			npcDat = baseDat.clone();
			final int id = DoorData.getInt("id");
			final int zmin = DoorData.getInt("minz");
			final int zmax = DoorData.getInt("maxz");
			final int posx = DoorData.getInt("posx");
			final int posy = DoorData.getInt("posy");

			final int hp = DoorData.getInt("hp");
			final int pdef = DoorData.getInt("pdef");
			final int mdef = DoorData.getInt("mdef");
			final String doorname = DoorData.getString("name");

			npcDat.set("npcId", id);
			npcDat.set("name", doorname);
			npcDat.set("baseHpMax", hp);
			npcDat.set("basePDef", pdef);
			npcDat.set("baseMDef", mdef);

			final L2CharTemplate template = new L2CharTemplate(npcDat);
			final L2DoorInstance door = new L2DoorInstance(IdFactory.getInstance().getNextId(), template, id, doorname, DoorData.getBoolean("unlockable"), DoorData.getBoolean("showHp"));
			_doorList.put(id, door);
			final L2Territory pos = new L2Territory(id);
			door.setGeoPos(pos);
			pos.add(DoorData.getInt("ax"), DoorData.getInt("ay"), zmin, zmax);
			pos.add(DoorData.getInt("bx"), DoorData.getInt("by"), zmin, zmax);
			pos.add(DoorData.getInt("cx"), DoorData.getInt("cy"), zmin, zmax);
			pos.add(DoorData.getInt("dx"), DoorData.getInt("dy"), zmin, zmax);
			door.getTemplate().collisionHeight = zmax - zmin & 0xfff0;
			door.getTemplate().collisionRadius = Math.max(50, Math.min(posx - pos.getXmin(), posy - pos.getYmin()));
			if(pos.getXmin() == pos.getXmax())
				Log.addDev(id + "|has zero size|[Xmin: " + pos.getXmin() + " - Xmax: " + pos.getXmax() + "]", "dev_doors_zero_size", false);
			else if(pos.getYmin() == pos.getYmax())
				Log.addDev(id + "|has zero size|[Ymin: " + pos.getYmin() + " - Ymax: " + pos.getYmax() + "]", "dev_doors_zero_size", false);
			door.setXYZInvisible(posx, posy, zmin + 32);
			door.setCurrentHpMp(door.getMaxHp(), door.getMaxMp(), true);
			door.setOpen(false);

			door.level = DoorData.getByte("level");
			door.key = DoorData.getInt("key");

			door.setSiegeWeaponOlnyAttackable(DoorData.getBoolean("siege_weapon"));
			door.setGeodata(DoorData.getBoolean("geodata"));
			door.spawnMe(door.getLoc());
		}
		_log.info("DoorTable: Loaded " + _doorList.size() + " doors.");
	}

	public L2DoorInstance getDoor(final int doorId)
	{
		return _doorList.get(doorId);
	}

	public L2DoorInstance[] getDoors()
	{
		return _doorList.values(new L2DoorInstance[_doorList.size()]);
	}

	public void checkAutoOpen()
	{
		for(final L2DoorInstance doorInst : getDoors())

			if(doorInst.getDoorName().startsWith("Eva_"))
				doorInst.setAutoActionDelay(420000);

			else if(doorInst.getDoorName().startsWith("hubris_"))
				doorInst.setAutoActionDelay(300000);

			else if(doorInst.getDoorName().startsWith("Devil.opendoor"))
				doorInst.setAutoActionDelay(300000);

			else if(doorInst.getDoorName().startsWith("Coral_garden"))
				doorInst.setAutoActionDelay(900000);

			else if(doorInst.getDoorName().startsWith("Normils_cave"))
				doorInst.setAutoActionDelay(300000);

			else if(doorInst.getDoorName().startsWith("Normils_garden"))
				doorInst.setAutoActionDelay(900000);
	}
}
