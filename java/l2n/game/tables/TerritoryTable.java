package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TObjectProcedure;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2RoundTerritory;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;

import java.sql.ResultSet;
import java.util.logging.Logger;

public class TerritoryTable
{
	private final static Logger _log = Logger.getLogger(TerritoryTable.class.getName());

	private static TIntObjectHashMap<L2Territory> _territory;

	private static int registered;

	public static TerritoryTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private TerritoryTable()
	{
		// load all data at server start
		reloadData();
	}

	public L2Territory getLocation(int terr)
	{
		L2Territory t = _territory.get(terr);
		if(t == null)
			_log.warning("TerritoryTable: getLocation[40] territory " + terr + " not found.");
		return t;
	}

	public int[] getRandomPoint(int terr)
	{
		L2Territory t = _territory.get(terr);
		if(t == null)
		{
			_log.warning("TerritoryTable: getRandomPoint[49] territory " + terr + " not found.");
			return new int[3];
		}
		return t.getRandomPoint();
	}

	public int getMinZ(int terr)
	{
		L2Territory t = _territory.get(terr);
		if(t == null)
		{
			_log.warning("TerritoryTable: getMinZ[60] territory " + terr + " not found.");
			return 0;
		}
		return t.getZmin();
	}

	public int getMaxZ(int terr)
	{
		L2Territory t = _territory.get(terr);
		if(t == null)
		{
			_log.warning("TerritoryTable: getMaxZ[71] territory " + terr + " not found.");
			return 0;
		}
		return t.getZmax();
	}

	public void reloadData()
	{
		if(_territory != null && !_territory.isEmpty())
			_territory.forEachValue(new TObjectProcedure<L2Territory>()
			{
				@Override
				public boolean execute(L2Territory t)
				{
					if(t.isWorldTerritory())
						L2World.removeTerritory(t);
					return true;
				}
			});

		_territory = new TIntObjectHashMap<L2Territory>();
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT loc_id, loc_x, loc_y, loc_zmin, loc_zmax, radius FROM `locations`");
			while (rset.next())
			{
				int loc_id = rset.getInt("loc_id");

				int loc_x = rset.getInt("loc_x");
				int loc_y = rset.getInt("loc_y");
				int loc_zmin = rset.getInt("loc_zmin");
				int loc_zmax = rset.getInt("loc_zmax");
				int radius = rset.getInt("radius");

				if(radius > 0)
				{
					if(_territory.get(loc_id) == null)
					{
						L2RoundTerritory t = new L2RoundTerritory(loc_id, loc_x, loc_y, radius, loc_zmin, loc_zmax);
						_territory.put(loc_id, t);
					}
				}
				else
				{
					if(_territory.get(loc_id) == null)
					{
						L2Territory t = new L2Territory(loc_id);
						_territory.put(loc_id, t);
					}
					_territory.get(loc_id).add(loc_x, loc_y, loc_zmin, loc_zmax);
				}
			}
		}
		catch(Exception e1)
		{
			// problem with initializing spawn, go to next one
			_log.warning("locations couldnt be initialized:" + e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_territory.forEachValue(new TObjectProcedure<L2Territory>()
		{
			@Override
			public boolean execute(L2Territory t)
			{
				t.validate();
				return true;
			}
		});

		_log.config("TerritoryTable: Loaded " + _territory.size() + " locations");
	}

	public void registerZones()
	{
		registered = 0;
		_territory.forEachValue(new TObjectProcedure<L2Territory>()
		{
			@Override
			public boolean execute(L2Territory t)
			{
				if(t.isWorldTerritory())
				{
					L2World.addTerritory(t);
					registered++;
				}
				return true;
			}
		});

		_log.config("TerritoryTable: Added " + registered + " locations to L2World");
	}

	public TIntObjectHashMap<L2Territory> getLocations()
	{
		return _territory;
	}

	public static void unload()
	{
		_territory.clear();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TerritoryTable _instance = new TerritoryTable();
	}
}
