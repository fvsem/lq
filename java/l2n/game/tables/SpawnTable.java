package l2n.game.tables;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Scripts;
import l2n.game.instancemanager.CatacombSpawnManager;
import l2n.game.instancemanager.DayNightSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2World;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2SiegeGuardInstance;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SpawnTable
{
	private static final Logger _log = Logger.getLogger(SpawnTable.class.getName());

	private GArray<L2Spawn> _spawntable;
	private int _spawnId;

	private int _npcSpawnCount;
	private int _spawnCount;

	public static SpawnTable getInstance()
	{
		return SingletonHolder._instance;
	}

	private SpawnTable()
	{
		ReflectionTable.getInstance().get(ReflectionTable.MULTILAYER, true);
		NpcTable.getInstance().applyServerSideTitle();
		if(!Config.DONTLOADSPAWN)
			fillSpawnTable(true);
		else
		{
			_log.info("SpawnTable: Spawn Correctly Disabled!");
			Scripts.getInstance().callOnLoad();
		}
	}

	public GArray<L2Spawn> getSpawnTable()
	{
		return _spawntable;
	}

	private void fillSpawnTable(boolean scripts)
	{
		_spawntable = new GArray<L2Spawn>(20000);

		if(!scripts)
			Util.gc(1, 1000);

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT * FROM spawnlist ORDER by npc_templateid");

			L2Spawn spawnDat;
			L2NpcTemplate template;
			_spawnId = 0;
			_npcSpawnCount = 0;
			_spawnCount = 0;

			while (rset.next())
			{
				template = NpcTable.getTemplate(rset.getInt("npc_templateid"));
				if(template != null)
				{           // Don't spawn Siege Guard
					if(template.isInstanceOf(L2SiegeGuardInstance.class))
					{
                                            
					}
					else
					{
						spawnDat = new L2Spawn(template);
						spawnDat.setAmount(rset.getInt("count") * (Config.ALT_DOUBLE_SPAWN && !template.isRaid ? 2 : 1));
						spawnDat.setLocx(rset.getInt("locx"));
						spawnDat.setLocy(rset.getInt("locy"));
						spawnDat.setLocz(rset.getInt("locz"));
						spawnDat.setHeading(rset.getInt("heading"));
						spawnDat.setRespawnDelay(rset.getInt("respawn_delay"), rset.getInt("respawn_delay_rnd"));
						spawnDat.setLocation(rset.getInt("loc_id"));
						spawnDat.setReflection(rset.getInt("reflection"));
						spawnDat.setRespawnTime(0);

						spawnDat.setId(_spawnId);
						_spawnId++;

						if(template.isInstanceOf(L2MonsterInstance.class))
							if(template.name.contains("Lilim") || template.name.contains("Lith"))
								CatacombSpawnManager.getInstance().addDawnMob(spawnDat);
							else if(template.name.contains("Nephilim") || template.name.contains("Gigant"))
								CatacombSpawnManager.getInstance().addDuskMob(spawnDat);

						if(template.isRaid)
							RaidBossSpawnManager.getInstance().addNewSpawn(spawnDat);

						switch (rset.getInt("periodOfDay"))
						{
							case 0: // default
								if(!Config.DELAYED_SPAWN)
									_npcSpawnCount += spawnDat.init();
								_spawntable.add(spawnDat);
								break;
							case 1: // Day
								DayNightSpawnManager.getInstance().addDayMob(spawnDat);
								break;
							case 2: // Night
								DayNightSpawnManager.getInstance().addNightMob(spawnDat);
								break;
						}
						_spawnCount++;
						if(_npcSpawnCount % 1000 == 0 && _npcSpawnCount != 0)
							_log.info("Spawned " + _npcSpawnCount + " npc");
					}
				}
				else
					_log.warning("mob data for id:" + rset.getInt("npc_templateid") + " missing in npc table");
			}
			DayNightSpawnManager.getInstance().notifyChangeMode();
			CatacombSpawnManager.getInstance().notifyChangeMode();
		}
		catch(Exception e1)
		{
			// problem with initializing spawn, go to next one
			_log.log(Level.WARNING, "Spawn couldnt be initialized: ", e1);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_log.config("SpawnTable: Loaded " + _spawnCount + " Npc Spawn Locations. Total NPCs: " + _npcSpawnCount);

		if(scripts)
			Scripts.getInstance().callOnLoad();
	}

	// just wrapper
	public void reloadAll()
	{
		L2World.deleteVisibleNpcSpawns();
		CatacombSpawnManager.getInstance().cleanUp();
		DayNightSpawnManager.getInstance().cleanUp();
		fillSpawnTable(false);
		RaidBossSpawnManager.getInstance().reloadBosses();
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final SpawnTable _instance = new SpawnTable();
	}
}
