package l2n.game.tables;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Rnd;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.logging.Logger;

public class ExtractableItemsTable
{
	private static Logger _log = Logger.getLogger(ExtractableItemsTable.class.getName());
	private static ExtractableItemsTable _instance;

	private static TIntObjectHashMap<L2ExtractableItem> _lists;

	public static ExtractableItemsTable getInstance()
	{
		if(_instance == null)
			_instance = new ExtractableItemsTable();
		return _instance;
	}

	public static void reload()
	{
		_instance = new ExtractableItemsTable();
	}

	public ExtractableItemsTable()
	{
		_lists = new TIntObjectHashMap<L2ExtractableItem>();
		load();
		_log.info("ExtractableItemsTable: Loaded " + _lists.size() + " extractable items.");
	}

	private void load()
	{
		try
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);

			final File file = new File(Config.DATAPACK_ROOT + "/data/extractable_items.csv");
			if(!file.exists())
			{
				_log.warning("Error in loading data, data/extractable_items.csv file not found!");
				return;
			}

			LineNumberReader lnr = new LineNumberReader(new BufferedReader(new FileReader(file)));

			String line = null;
			int lineCount = 0;
			while ((line = lnr.readLine()) != null)
			{
				lineCount++;
				if(line.trim().length() == 0 || line.startsWith("#"))
					continue;

				String[] lineSplit = line.split(";");
				int id = Integer.parseInt(lineSplit[0]);
				if(id == 0)
					continue;
				int type = Integer.parseInt(lineSplit[1]);
				L2ExtractableItem ei = new L2ExtractableItem(id);
				ei.setType(type);

				for(int i = 1; i < lineSplit.length - 1; i++)
				{
					String[] lineSplit2 = lineSplit[i + 1].split(",");

					if(lineSplit2.length < 3)
					{
						_log.warning("Extractable items data: Error in line " + lineCount + " -> wrong seperator!");
						continue;
					}
					int itemId = Integer.parseInt(lineSplit2[0]);
					int count = Integer.parseInt(lineSplit2[1]);
					int chance = Integer.parseInt(lineSplit2[2]);

					L2ExtractableProduct item = new L2ExtractableProduct();
					item.setProductId(itemId);
					item.setCount(count);
					item.setChance(chance);
					ei.addItem(item);
				}
				_lists.put(id, ei);
			}
		}
		catch(final Exception e)
		{
			_log.severe("ExtractableItems: Error parsing extractable_items file. " + e);
		}
	}

	public L2ExtractableItem getExtractableItem(int itemId)
	{
		return _lists.get(itemId);
	}

	public int[] itemIDs()
	{
		return _lists.keys();
	}

	public class L2ExtractableItem
	{
		private final GArray<L2ExtractableProduct> _items = new GArray<L2ExtractableProduct>();
		private final int _itemId;
		private int _type;

		public L2ExtractableItem(int itemId)
		{
			_itemId = itemId;
		}

		public void setType(int type)
		{
			_type = type;
		}

		public int getItemId()
		{
			return _itemId;
		}

		public int getType()
		{
			return _type;
		}

		public void addItem(L2ExtractableProduct item)
		{
			_items.add(item);
		}

		public int getSize()
		{
			return _items.size();
		}

		public boolean getItemByChance(L2ItemInstance item, L2Playable player)
		{
			int chancesumm = 0;
			int productId = 0;
			int chance = Rnd.get(100);
			int count = 0;

			player.getInventory().destroyItem(item, 1, true);
			player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(item.getItemId()));

			for(L2ExtractableProduct product : _items)
			{
				chancesumm += product.getChance();
				if(chancesumm > chance)
				{
					productId = product.getProductId();
					count = product.getCount();
					player.getInventory().addItem(productId, count, 0, "ExtractableItems");
					player.sendPacket(SystemMessage.obtainItems(productId, count, 0));
					return true;
				}
			}
			return true;
		}

		public boolean getAllItems(L2ItemInstance item, L2Playable player)
		{
			int productId = 0;
			int count = 0;

			player.getInventory().destroyItem(item, 1, true);
			player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(item.getItemId()));

			for(L2ExtractableProduct product : _items)
				if(Rnd.chance(product.getChance()))
				{
					productId = product.getProductId();
					count = product.getCount();
					player.getInventory().addItem(productId, count, 0, "ExtractableItems");
					player.sendPacket(SystemMessage.obtainItems(productId, count, 0));
				}
			return true;
		}
	}

	public class L2ExtractableProduct
	{
		public int _productId;
		public int _chance;
		public int _count;

		public void setProductId(int productId)
		{
			_productId = productId;
		}

		public void setChance(int chance)
		{
			_chance = chance;
		}

		public void setCount(int count)
		{
			_count = count;
		}

		public int getProductId()
		{
			return _productId;
		}

		public int getChance()
		{
			return _chance;
		}

		public int getCount()
		{
			return _count;
		}
	}
}
