package l2n.game.ai;

import l2n.Config;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.Duel;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.instances.L2SiegeGuardInstance;
import l2n.game.model.instances.L2TrapInstance;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.MagicSkillUse;

public class Trap extends DefaultAI
{
	private Boolean thinking = false; // to prevent recursive thinking

	public Trap(L2Character actor)
	{
		super(actor);
		_isMobile = false;
	}

	@Override
	public void run()
	{
		onEvtThink();
	}

	@Override
	protected void onEvtThink()
	{
		L2TrapInstance trap = getActor();

		synchronized (thinking)
		{
			if(thinking)
				return;
			thinking = true;
		}

		try
		{
			for(L2Character cha : trap.getAroundCharacters(trap.getTrapSkill().getSkillRadius(), 200))
			{
				if(cha == null || cha == trap.getOwner())
					continue;

				if(cha.isRaid() && trap.getTrapSkill().getSkillType().equals(L2Skill.SkillType.DEBUFF))
					continue;

				if(canAttack(trap, cha) && trap.getTrapSkill().checkTarget(trap.getOwner(), cha, null, true, false) == null)
				{
					trap.broadcastPacket(new MagicSkillUse(trap, cha, trap.getTrapSkill().getId(), trap.getSkillLevel(), 0, 0));
					trap.broadcastPacket(new MagicSkillLaunched(trap.getObjectId(), trap.getTrapSkill().getId(), trap.getSkillLevel(), trap.getTrapSkill().isOffensive(), cha));
					trap.getTrapSkill().useSkill(trap, cha);
					trap.deleteMe();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			thinking = false;
		}
	}

	public boolean canAttack(L2TrapInstance trap, L2Character target)
	{
		if(trap.getOwner() == null || trap.getOwner() == target || target == trap || trap.isDead())
			return false;

		if(trap.isInZoneOlympiad())
		{
			L2Player enemy = target.isPlayer() ? target.getPlayer() : ((L2Summon) target).getPlayer();
			if(!trap.getOwner().isPlayer() || !enemy.isInOlympiadMode() || enemy.getOlympiadGameId() != trap.getOwner().getPlayer().getOlympiadGameId() || trap.getOwner().getPlayer().getOlympiadGameId() != enemy.getOlympiadGameId())
				return false;
			else
				return true;
		}

		// Автоатака на дуэлях, только враг и только если он еше не проиграл.
		if(trap.getOwner().getDuel() != null && target.getDuel() == trap.getOwner().getDuel())
		{
			// Тут не может быть ClassCastException у attacker, т.к. L2Character.getDuel() всегда возвращает null
			L2Player enemy = target.isPlayer() ? target.getPlayer() : ((L2Summon) target).getPlayer();

			if(enemy.getDuel().getTeamForPlayer(enemy) != trap.getOwner().getDuel().getTeamForPlayer(trap.getOwner().getPlayer()) && trap.getDuel().getDuelState(trap.getOwner().getPlayer().getStoredId()) == Duel.DuelState.Fighting && trap.getOwner().getDuel().getDuelState(enemy.getStoredId()) == Duel.DuelState.Fighting)
				return true;
		}

		if(!GeoEngine.canSeeTarget(target, trap, false) || trap.getReflection() != target.getReflection())
			return false;

		// TODO: check for friendly mobs
		if(target.isMonster())
			return true;
		final L2Player pcAttacker = target.getPlayer();
		final L2Clan clan1 = trap.getOwner().getPlayer().getClan();
		if(pcAttacker != null && trap.getOwner().getPlayer() != null)
		{
			if(trap.getOwner().getPlayer().getParty() != null && trap.getOwner().getPlayer().getParty().getPartyMembers().contains(pcAttacker))
				return false;
			// Если цель либо атакующий находится в мирной зоне - атаковать нельзя
			if((trap.isInZonePeace() || pcAttacker.isInZonePeace()) && !trap.getOwner().getPlayer().getPlayerAccess().PeaceAttack)
				if(!Config.ALT_GAME_KARMAPLAYERCANBEKILLEDINPEACEZONE || trap.getKarma() <= 0 && pcAttacker.getKarma() <= 0)
					return false;
			if(trap.isInZoneBattle() && target.isInZoneBattle())
				return true;
			final L2Clan clan2 = pcAttacker.getClan();
			if(clan1 != null && clan2 != null)
			{
				if(clan1.getClanId() == clan2.getClanId())
					return false;
				if(pcAttacker.atMutualWarWith(trap.getOwner().getPlayer()))
					return true;
			}
			if(trap.isInZoneSiege() && target.isInZoneSiege())
			{
				if(clan1 == null || clan2 == null)
					return true;
				if(!clan1.isDefender() && !clan1.isAttacker())
					return true;
				if(!clan2.isDefender() && !clan2.isAttacker())
					return true;
				if(clan1.isDefender() && clan2.isAttacker() || clan2.isDefender() && clan1.isAttacker())
					return true;
			}
			if(trap.getOwner().getKarma() > 0 || trap.getOwner().getPvpFlag() != 0)
				return true;
		}
		else if(target instanceof L2SiegeGuardInstance)
			if(clan1 != null)
			{
				Siege siege = SiegeManager.getSiege(trap, true);
				return siege != null && siege.checkIsAttacker(clan1);
			}
		return false;
	}

	@Override
	public L2TrapInstance getActor()
	{
		return (L2TrapInstance) super.getActor();
	}
}
