package l2n.game.ai;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.math.random.RndSelector;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.model.AIThreadController;
import l2n.game.ai.model.AdditionalAIParams;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.ai.model.Trigger;
import l2n.game.ai.tasks.DefaultAITaskFactory;
import l2n.game.ai.tasks.Task;
import l2n.game.ai.tasks.TaskFactory;
import l2n.game.ai.tasks.random.RandomTask;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.*;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Character.HateInfo;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.instances.*;
import l2n.game.model.instances.L2NpcInstance.AggroInfo;
import l2n.game.model.quest.QuestEventType;
import l2n.game.model.quest.QuestState;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.skills.Stats;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.TerritoryTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.MinionList;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.*;

public class DefaultAI extends L2CharacterAI implements Runnable, Trigger
{
	// NPC Ultimate Defense level 1
	private static final L2Skill ud_skill = SkillTable.getInstance().getInfo(Config.ALT_MOBS_UD_SKILL_ID, Config.ALT_MOBS_UD_SKILL_LVL);

	private static final Comparator<Task> TASK_COMPARATOR = new Comparator<Task>()
	{
		@Override
		public int compare(final Task o1, final Task o2)
		{
			if(o1 == null || o2 == null)
				return 0;
			return o2.weight - o1.weight;
		}
	};

	private static final DefaultAITaskFactory TASK_FACTORY = new DefaultAITaskFactory();

	protected final static Logger _log = Logger.getLogger(DefaultAI.class.getName());

	protected int AI_TASK_DELAY = Config.AI_TASK_DELAY;
	protected int AI_TASK_ACTIVE_DELAY = Config.AI_TASK_ACTIVE_DELAY;

	protected int MAX_PURSUE_RANGE;

	protected int MAX_Z_AGGRO_RANGE = 300;

	protected AdditionalAIParams this_params = null;

	/** The L2NpcInstance AI task executed every 1s (call onEvtThink method) */
	protected ScheduledFuture<?> _runningTask;
	protected ScheduledFuture<?> _madnessTask;

	/** The flag used to indicate that a thinking action is in progress */
	private boolean _thinking = false;

	/** The L2NpcInstance aggro counter */
	protected long _globalAggro;

	protected long _randomAnimationEnd;
	protected boolean _isMobile = true;
	protected int _pathfind_fails;

	/** Список заданий */
	protected ConcurrentSkipListSet<Task> _task_list = new ConcurrentSkipListSet<Task>(TASK_COMPARATOR);
	/** Список заданий которые выполняются периодически, список не отчищается */
	protected ConcurrentLinkedQueue<RandomTask> _random_task_list;

	/** Показывает, есть ли задания */
	protected boolean _def_think = false;

	public final L2Skill[] _dam_skills, _dot_skills, _debuff_skills, _heal, _buff, _stun;

	private long _lastActiveCheck;

	/** индикатор, указывающий находиться ли AI в пуле на выполнение */
	private volatile boolean _isActive = false;

	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * Trigger data members.
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

	private long nextFireTime = 0;

	public static enum TaskType
	{
		MOVE,
		ATTACK,
		CAST,
		BUFF
	}

	public class Teleport implements Runnable
	{
		private final Location _destination;

		public Teleport(final Location destination)
		{
			_destination = destination;
		}

		@Override
		public void run()
		{
			final L2NpcInstance actor = getActor();
			if(actor != null)
				actor.teleToLocation(_destination);
		}
	}

	public class RunningTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2NpcInstance actor = getActor();
			if(actor != null)
				actor.setRunning();
			_runningTask = null;
		}
	}

	public class MadnessTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2NpcInstance actor = getActor();
			if(actor != null)
				actor.stopConfused();
			_madnessTask = null;
		}
	}

	public void addUseSkillDesire(final L2Character target, final L2Skill skill, final int weight)
	{
		final Task task = skill.isOffensive() ? getTaskFactory().createCastTask(skill, target, weight) : getTaskFactory().createBuffTask(skill, target, weight);
		_task_list.add(task);
		_def_think = true;
	}

	public void addTaskCast(final L2Character target, final L2Skill skill)
	{
		final Task task = getTaskFactory().createCastTask(skill, target);
		_task_list.add(task);
		_def_think = true;
	}

	public void addTaskCast(final L2Character target, final L2Skill skill, final int weight)
	{
		final Task task = getTaskFactory().createCastTask(skill, target, weight);
		_task_list.add(task);
		_def_think = true;
	}

	public void addTaskBuff(final L2Character target, final L2Skill skill)
	{
		final Task task = getTaskFactory().createBuffTask(skill, target);
		_task_list.add(task);
		_def_think = true;
	}

	@Override
	public void addTaskAttack(final L2Character target)
	{
		final Task task = getTaskFactory().createAttackTask(target);
		_task_list.add(task);
		_def_think = true;
	}

	@Override
	public void addTaskMove(final Location loc)
	{
		final Task task = getTaskFactory().createMoveTask(loc);
		_task_list.add(task);
		_def_think = true;
	}

	@Override
	public void addTaskMove(final Location loc, final int weight)
	{
		final Task task = getTaskFactory().createMoveTask(loc, weight);
		_task_list.add(task);
		_def_think = true;
	}

	public void addRandomTask(final RandomTask task)
	{
		if(_random_task_list == null)
			_random_task_list = new ConcurrentLinkedQueue<RandomTask>();

		if(!_random_task_list.contains(task))
			_random_task_list.add(task);
	}

	public void addTaskMove(final int locX, final int locY, final int locZ)
	{
		addTaskMove(new Location(locX, locY, locZ));
	}

	public DefaultAI(final L2Character actor)
	{
		super(actor);

		// A L2Character isn't aggressive during 10s after its spawn because _globalAggro is set to -10
		setGlobalAggro(System.currentTimeMillis() + 10000);
		final L2NpcInstance thisActor = (L2NpcInstance) actor;
		thisActor.setAttackTimeout(Long.MAX_VALUE);

		_dam_skills = thisActor.getTemplate().getDamageSkills();
		_dot_skills = thisActor.getTemplate().getDotSkills();
		_debuff_skills = thisActor.getTemplate().getDebuffSkills();
		_heal = thisActor.getTemplate().getHealSkills();
		_buff = thisActor.getTemplate().getBuffSkills();
		_stun = thisActor.getTemplate().getStunSkills();

		// Preload some AI params
		setMaxPursueRange(getInt(AiOptionsType.MAX_PURSUE_RANGE, actor.isRaid() ? Config.MAX_PURSUE_RANGE_RAID : Config.MAX_PURSUE_RANGE));
		thisActor.minFactionNotifyInterval = getInt(AiOptionsType.FACTION_NOTIFY_INTERVAL, thisActor.minFactionNotifyInterval);
	}

	@Override
	public void run()
	{
		// если не активно, то ничего не делаем
		if(!isActive())
			return;

		final long currentTimeMillis = System.currentTimeMillis();
		// trigger actions
		updateAfterFire(currentTimeMillis); // обновляем данные для следующего запуска

		// Если AI не глобальное и прошла минута с последней проверки...
		if(!isGlobalAI() && currentTimeMillis - _lastActiveCheck > 60 * 1000)
		{
			_lastActiveCheck = currentTimeMillis;
			final L2NpcInstance actor = getActor();
			final L2WorldRegion region = actor == null ? null : actor.getCurrentRegion();

			// Если регион не установлени или регион и примыкающие к нему пусты, то выключаем AI
			if(region == null || region.areNeighborsEmpty())
			{
				// удаляем AI из очереди если оно было включено
				if(isActive())
				{
					setActive(false);
					AIThreadController.getInstance().remove(this);
				}
				return;
			}
		}
		onEvtThink();
	}

	@Override
	public void startAITask()
	{
		// If not idle - create an AI task (schedule onEvtThink repeatedly)
		if(!isActive())
		{
			setActive(true);
			// после активации AI стартует через 0-AI_TASK_ACTIVE_DELAY секунд
			setNextFireTime(System.currentTimeMillis() + Rnd.get(AI_TASK_ACTIVE_DELAY));
			AIThreadController.getInstance().put(this);
			setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		}
	}

	@Override
	public void stopAITask()
	{
		// выключаем AI
		if(isActive())
		{
			setActive(false);
			AIThreadController.getInstance().remove(this);
			setIntention(CtrlIntention.AI_INTENTION_IDLE);
		}
	}

	/**
	 * Определяет, может ли этот тип АИ видеть персонажей в режиме Silent Move.
	 *
	 * @param target
	 *            L2Playable цель
	 * @return Параметр заданный в ai_params имеет большие приоритет, если он НЕ задан, то возвращает (true если цель не видна в режиме Silent Move)
	 */
	@Override
	public boolean canSeeInSilentMove(final L2Playable target)
	{
		if(getBool(AiOptionsType.IS_SILENT_MOVE_VISIBLE, false))
			return true;
		return !target.isSilentMoving();
	}

	@Override
	public void checkAggression(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE || !isGlobalAggro())
			return;
		if(actor instanceof L2FestivalMonsterInstance && !target.getPlayer().isFestivalParticipant())
			return;
		if(!actor.isAggressive() || !Util.checkIfInRange(actor.getAggroRange(), actor.getX(), actor.getY(), actor.getZ(), target.getX(), target.getY(), target.getZ(), false) || Math.abs(actor.getZ() - target.getZ()) > MAX_Z_AGGRO_RANGE)
			return;
		if(target.isPlayable() && !canSeeInSilentMove((L2Playable) target))
			return;
		if(actor.getFactionId().equalsIgnoreCase("varka_silenos_clan") && target.getPlayer() != null && target.getPlayer().getVarka() > 0)
			return;
		if(actor.getFactionId().equalsIgnoreCase("ketra_orc_clan") && target.getPlayer() != null && target.getPlayer().getKetra() > 0)
			return;
		if(target.isInZonePeace())
			return;
		if(target.isFollow && !target.isPlayer() && target.getFollowTarget() != null && target.getFollowTarget().isPlayer())
			return;
		if(target.isPlayer() && ((L2Player) target).isInvisible())
			return;
		if(!actor.canAttackCharacter(target))
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;

		if((target.isSummon() || target.isPet()) && target.getPlayer() != null)
			target.getPlayer().addDamageHate(actor, 0, 1);

		target.addDamageHate(actor, 0, 2);

		startRunningTask(2000);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}

	public void setIsInRandomAnimation(final long time)
	{
		_randomAnimationEnd = System.currentTimeMillis() + time;
	}

	protected boolean randomAnimation()
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isMoving && actor.hasRandomAnimation() && Rnd.chance(Config.RND_ANIMATION_RATE))
		{
			setIsInRandomAnimation(1000 * Rnd.get(Config.MIN_MONSTER_ANIMATION, Config.MAX_MONSTER_ANIMATION));
			actor.onRandomAnimation();
			return true;
		}
		return false;
	}

	/**
	 * Параметр заданный в ai_params имеет большие приоритет
	 */
	protected boolean randomWalk()
	{
		if(getBool(AiOptionsType.NO_RANDOM_WALK, false))
			return false;
		final L2Character actor = getActor();
		return actor != null && !actor.isMoving && maybeMoveToHome();
	}

	/**
	 * @return true если действие выполнено, false если нет
	 */
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(actor.isBlocked() || _randomAnimationEnd > System.currentTimeMillis())
			return true;

		if(_def_think)
		{
			if(doTask())
				clearTasks();
			return true;
		}

		// Аггрится даже на неподвижных игроков
		if(actor.isAggressive() && Rnd.chance(getInt(AiOptionsType.SELF_AGGRESSIVE, 0)))
			for(final L2Playable obj : L2World.getAroundPlayables(actor))
				if(obj != null && !obj.isAlikeDead() && !obj.isInvul() && obj.isVisible())
					checkAggression(obj);

		// If this is a festival monster or chest, then it remains in the same location
		if(actor instanceof L2FestivalMonsterInstance || actor instanceof L2ChestInstance || actor instanceof L2TamedBeastInstance)
			return false;

		if(actor.isMinion())
		{
			final L2MonsterInstance leader = ((L2MinionInstance) actor).getLeader();
			if(leader == null)
				return false;
			final double distance = actor.getDistance(leader.getX(), leader.getY());
			if(distance > 1000)
				actor.teleToLocation(leader.getMinionPosition());
			else if(distance > 200)
				addTaskMove(leader.getMinionPosition());
			return false;
		}

		if(randomAnimation())
			return true;

		if(randomWalk())
			return true;

		return false;
	}

	public boolean checkTarget(final L2NpcInstance actor, final L2Character target, final boolean canSelf, final int range)
	{
		if(actor == null || target == null || target == actor && !canSelf || target.isAlikeDead() || !actor.isInRange(target, range))
			return true;

		if(actor.isConfused() || target instanceof L2DecoyInstance)
			return false;

		if(!canSelf && target.getHateList().get(actor) == null)
			return true;
		return false;
	}

	protected void thinkAttack()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getMaxPursueRange() > 0)
		{
			final Location loc = actor.getSpawnedLoc();
			if(loc.x != 0 && loc.y != 0 && !actor.isInRange(loc, getMaxPursueRange()))
			{
				teleportHome(true);
				return;
			}
		}

		if(doTask() && !actor.isAttackingNow() && !actor.isCastingNow())
			createNewTask();
	}

	@Override
	protected void onEvtReadyToAct()
	{
		onEvtThink();
	}

	@Override
	protected void onEvtArrivedTarget()
	{
		onEvtThink();
	}

	@Override
	protected void onEvtArrived()
	{
		onEvtThink();
	}

	public boolean tryMoveToTarget(final L2Character target)
	{
		return tryMoveToTarget(target, 0);
	}

	public boolean tryMoveToTarget(final L2Character target, int range)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return false;
		range = range == 0 ? actor.getPhysicalAttackRange() : Math.max(0, range);

		if(!actor.followToCharacter(target, actor.getPhysicalAttackRange(), true))
			_pathfind_fails++;
		if(_pathfind_fails >= getMaxPathfindFails() && System.currentTimeMillis() - (actor.getAttackTimeout() - getMaxAttackTimeout()) < getTeleportTimeout() && actor.isInRange(target, 2000))
		{
			_pathfind_fails = 0;
			final HateInfo hate = target.getHateList().get(actor);
			if(hate == null || hate.damage < 100 && hate.hate < 100)
			{
				returnHome(true);
				return false;
			}
			Location loc = GeoEngine.moveCheckForAI(target.getLoc(), actor.getLoc());
			if(!GeoEngine.canMoveToCoord(actor.getX(), actor.getY(), actor.getZ(), loc.x, loc.y, loc.z)) // Для подстраховки
				loc = target.getLoc();
			actor.teleToLocation(loc);
			return false;
		}

		return true;
	}

	public boolean maybeNextTask(final Task currentTask)
	{
		// Следующее задание
		_task_list.remove(currentTask);
		// Если заданий больше нет - определить новое
		if(_task_list.size() == 0)
			return true;
		return false;
	}

	/**
	 * @return true если нет больше заданий - определить новое
	 */
	protected boolean doTask()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		if(_task_list.isEmpty())
		{
			_def_think = false;
			return true;
		}

		Task currentTask = null;
		try
		{
			currentTask = _task_list.pollFirst();
		}
		catch(final Exception e)
		{}

		if(currentTask == null)
			clearTasks();

		if(!_def_think)
			return true;

		if(actor.isDead() || actor.isAttackingNow() || actor.isCastingNow())
			return false;

		assert currentTask != null;

		// выполняем задание
		return currentTask.execute(actor, this);
	}

	protected boolean createNewTask()
	{
		return false;
	}

	protected boolean defaultNewTask()
	{
		clearTasks();

		final L2NpcInstance actor = getActor();
		L2Character target;
		if(actor == null || (target = prepareTarget()) == null)
			return false;

		final double distance = actor.getDistance(target);
		return chooseTaskAndTargets(null, target, distance);
	}

	@Override
	protected void onIntentionActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor != null)
			actor.setAttackTimeout(Long.MAX_VALUE);

		clientStopMoving();

		if(getIntention() != AI_INTENTION_ACTIVE && actor != null && !actor.isDead())
			changeIntention(AI_INTENTION_ACTIVE, null, null);
	}

	@Override
	protected void onIntentionAttack(final L2Character target)
	{
		// Удаляем все задания
		clearTasks();

		setAttackTarget(target);
		clientStopMoving();

		if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
		{
			changeIntention(CtrlIntention.AI_INTENTION_ATTACK, target, null);
			// на всякий случай, чтоб наверника атаковать начинали
			_randomAnimationEnd = 0;
			// чтоб атака началась сразу же
			setNextFireTime(-1);
		}
	}

	@Override
	protected void onEvtThink()
	{
		final L2NpcInstance actor = getActor();
		if(_thinking || actor == null || actor.isActionsDisabled() || actor.isAfraid() || actor.isDead())
			return;

		// FIXME обработка рандомных задач
		if(_random_task_list != null && !_random_task_list.isEmpty())
		{
			final long currentTimeMillis = System.currentTimeMillis();
			for(final RandomTask task : _random_task_list)
				if(task.checkCondition(actor, getIntention(), currentTimeMillis))
					task.execute(actor);
		}

		if(actor.isBlocked() || _randomAnimationEnd > System.currentTimeMillis() && getIntention() != AI_INTENTION_ATTACK)
			return;

		if(actor.isRaid() && (actor.isInZonePeace() || actor.isInZone(ZoneType.battle_zone) || actor.isInZone(ZoneType.Siege)))
			teleportHome(true);

		// Start thinking action
		_thinking = true;
		try
		{
			if(!Config.BLOCK_ACTIVE_TASKS && (getIntention() == AI_INTENTION_ACTIVE || getIntention() == AI_INTENTION_IDLE))
				thinkActive();
			else if(getIntention() == AI_INTENTION_ATTACK)
				thinkAttack();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
		finally
		{
			// Stop thinking action
			_thinking = false;
		}
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2NpcInstance actor = getActor();

		// Обработка AI Params
		final int transformer = getInt(AiOptionsType.TRANSFORM_ON_DEAD, 0);
		final int chance = getInt(AiOptionsType.TRANSFORM_CHANCE, 100);
		// Обработка трансформации при смерти
		if(transformer > 0 && Rnd.chance(chance))
			try
			{
				if(actor != null)
				{
					final Reflection r = actor.getReflection();
					final L2MonsterInstance npc = (L2MonsterInstance) NpcTable.getTemplate(transformer).getNewInstance();
					npc.setSpawnedLoc(actor.getLoc());
					npc.setReflection(r);
					npc.onSpawn();
					npc.spawnMe(npc.getSpawnedLoc());

					if(r.getId() > 0)
						r.addSpawn(npc.getSpawn());

					if(killer != null && killer.isPlayable())
					{
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, 100);
						killer.setTarget(npc);
						killer.sendPacket(npc.makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
					}
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}

		stopAITask();

		if(actor != null)
		{
			actor.clearAggroList(false);
			actor.setAttackTimeout(Long.MAX_VALUE);
		}

		// Удаляем все задания
		clearTasks();
		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtClanAttacked(final L2Character attacked_member, final L2Character attacker, final int damage)
	{
		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE || !isGlobalAggro())
			return;

		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || !actor.isInRange(attacked_member, actor.getFactionRange()) || actor.getChampion() > 0 && !Config.CHAMPION_CAN_BE_SOCIAL)
			return;

		if(Math.abs(attacker.getZ() - actor.getZ()) > MAX_Z_AGGRO_RANGE)
			return;

		if(GeoEngine.canSeeTarget(actor, attacked_member, false))
			notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, attacker.isSummon() ? damage : 3);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		// Обработка AI Params
		if(!actor.isDead())
		{
			// Обработка шансовой трансформации
			final int transformer = getInt(AiOptionsType.TRANSFORM_ON_UNDERATTACK, 0);
			if(transformer > 0)
				try
				{
					final int chance = getInt(AiOptionsType.TRANSFORM_CHANCE, 5);
					if(chance == 100 || ((L2MonsterInstance) actor).getChampion() == 0 && actor.getCurrentHpPercents() > 50 && Rnd.chance(chance))
					{
						final Reflection r = actor.getReflection();
						final L2MonsterInstance npc = (L2MonsterInstance) NpcTable.getTemplate(transformer).getNewInstance();
						npc.setSpawnedLoc(actor.getLoc());
						npc.setReflection(r);
						npc.onSpawn();
						npc.setChampion(((L2MonsterInstance) actor).getChampion());
						npc.setCurrentHpMp(npc.getMaxHp(), npc.getMaxMp(), true);
						npc.spawnMe(npc.getSpawnedLoc());
						if(r.getId() > 0)
							r.addSpawn(npc.getSpawn());
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 100);
						actor.decayMe();
						actor.doDie(actor);
						attacker.setTarget(npc);
						attacker.sendPacket(npc.makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
						return;
					}
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "", e);
				}
		}

		if(!actor.canAttackCharacter(attacker))
			return;

		final L2Player player = attacker.getPlayer();
		if(player != null)
		{
			final GArray<QuestState> quests = player.getQuestsForEvent(actor, QuestEventType.MOBGOTATTACKED);
			if(quests != null)
				for(final QuestState qs : quests)
					qs.getQuest().notifyAttack(actor, qs);

			// 1 хейт добавляется хозяину суммона, чтобы после смерти суммона, моб накинулся на хозяина.
			if(damage > 0 && attacker.isSummon() || attacker.isPet())
				attacker.getPlayer().addDamageHate(actor, 0, 1);
		}

		actor.callFriends(attacker, damage);
	}

	@Override
	protected void onEvtAggression(final L2Character attacker, final int aggro)
	{
		if(attacker == null || attacker.getPlayer() == null)
			return;

		final L2NpcInstance actor = getActor();
		if(actor == null || !actor.canAttackCharacter(attacker))
			return;

		final L2Player player = attacker.getPlayer();

		if(Config.ALT_SEVEN_SIGNS_RESTRICT)
			if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK && (SevenSigns.getInstance().isSealValidationPeriod() || SevenSigns.getInstance().isCompResultsPeriod()) && actor.isSevenSignsMonster())
			{
				final int pcabal = SevenSigns.getInstance().getPlayerCabal(player);
				final int wcabal = SevenSigns.getInstance().getCabalHighestScore();
				if(pcabal != wcabal && wcabal != SevenSigns.CABAL_NULL)
				{
					player.sendPacket(CustomSystemMessageId.L2PLAYER_SEVEN_SIGNS_TELEPORTED_BEGINN.getPacket());
					player.teleToClosestTown();
					return;
				}
			}

		actor.setAttackTimeout(getMaxAttackTimeout() + System.currentTimeMillis());
		setGlobalAggro(0);

		// если мертвый, всё остальное можно уже не выполнять
		if(!actor.isDead())
		{
			// 1 хейт добавляется хозяину суммона, чтобы после смерти суммона моб накинулся на хозяина.
			if(player != null && aggro > 0 && attacker.getPlayer() != null && (attacker.isSummon() || attacker.isPet()))
				attacker.getPlayer().addDamageHate(actor, 0, 1);

			attacker.addDamageHate(actor, 0, aggro);

			if(!actor.isRunning())
				startRunningTask(1000);

			// на всякий случай, чтоб наверника атаковать начинали
			_randomAnimationEnd = 0;

			if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
			{
				// Показываем анимацию зарядки шотов, если есть таковые.
				switch (actor.getTemplate().shots)
				{
					case SOUL:
						actor.unChargeShots(false);
						break;
					case SPIRIT:
					case BSPIRIT:
						actor.unChargeShots(true);
						break;
					case SOUL_SPIRIT:
					case SOUL_BSPIRIT:
						actor.unChargeShots(false);
						actor.unChargeShots(true);
						break;
				}

				setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
			}
		}
	}

	@Override
	protected void onEvtFinishCasting(final L2Skill skill)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null)
			// FIXME фикс когда показывается что монстр кастует, но скила нету и он зависает.
			if(actor.getCastingSkill() == null && actor.isCastingNow())
				actor.clearCastVars();
	}

	protected boolean maybeMoveToHome()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		final boolean randomWalk = actor.hasRandomWalk();
		final Location sloc = actor.getSpawnedLoc();
		if(sloc == null)
			return false;

		// Моб попал на другой этаж
		if(Math.abs(sloc.z - actor.getZ()) > L2Skill.HEIGHT && !isGlobalAI())
		{
			teleportHome(true);
			return true;
		}

		// Random walk or not?
		if(randomWalk && (!Config.RND_WALK || !Rnd.chance(Config.RND_WALK_RATE)))
			return false;

		final boolean isInRange = actor.isInRangeZ(sloc, Config.MAX_DRIFT_RANGE);
		if(!randomWalk && isInRange)
			return false;

		final int x = sloc.x + Rnd.get(-Config.MAX_DRIFT_RANGE, Config.MAX_DRIFT_RANGE);
		final int y = sloc.y + Rnd.get(-Config.MAX_DRIFT_RANGE, Config.MAX_DRIFT_RANGE);
		final int z = GeoEngine.getHeight(x, y, sloc.z);

		if(Math.abs(sloc.z - z) > 64)
			return false;

		final L2Spawn spawn = actor.getSpawn();
		if(spawn != null && spawn.getLocation() != 0 && !TerritoryTable.getInstance().getLocation(spawn.getLocation()).isInside(x, y))
			return false;

		actor.setWalking();

		// Телепортируемся домой, только если далеко от дома
		if(!actor.moveToLocation(x, y, z, 0, false) && !isInRange)
			teleportHome(true);

		return true;
	}

	public void returnHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(!actor.isMovementDisabled())
		{
			if(clearAggro)
				actor.clearAggroList(true);

			setIntention(AI_INTENTION_ACTIVE);
			// Удаляем все задания
			clearTasks();

			// Прибежать в заданную точку и переключиться в состояние AI_INTENTION_ACTIVE
			final Location sloc = actor.getSpawnedLoc();
			if(sloc == null)
				return;
			if(!clearAggro)
				actor.setRunning();
			addTaskMove(sloc);
		}
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(AI_INTENTION_ACTIVE);
		// Удаляем все задания
		clearTasks();

		final Location sloc = actor.getSpawnedLoc();
		if(sloc != null)
		{
			actor.broadcastPacketToOthers(new MagicSkillUse(actor, actor, 2036, 1, 500, 0));
			actor.teleToLocation(sloc.x, sloc.y, GeoEngine.getHeight(sloc));
		}
	}

	@Override
	public boolean isActive()
	{
		return _isActive;
	}

	/**
	 * Переключение статуса AI
	 *
	 * @param value
	 *            - статус на который меняем
	 * @return
	 */
	private final synchronized void setActive(final boolean value)
	{
		_isActive = value;
	}

	public void resetPathfindFails()
	{
		_pathfind_fails = 0;
	}

	@Override
	public void clearTasks()
	{
		_def_think = false;
		_task_list.clear();
	}

	/**
	 * Шедюлит мобу переход в режим бега через определенный интервал времени
	 *
	 * @param interval
	 */
	public void startRunningTask(final int interval)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && _runningTask == null && !actor.isRunning())
			_runningTask = L2GameThreadPools.getInstance().scheduleAi(new RunningTask(), interval, false);
	}

	public final AdditionalAIParams getAIParams()
	{
		if(this_params != null)
			return this_params;
		final L2NpcInstance actor = getActor();
		if(actor != null)
			return actor.getTemplate().getAIParams();
		this_params = L2NpcTemplate.EMPTY_AI_PARAMS;
		return this_params;
	}

	public static String debugTask(final Task task)
	{
		if(task == null)
			return "NULL";
		else
			return "type=" + task.type + "; weight=" + task.weight + "; skill=" + task.skill + "; target=" + task.getTarget() + "; loc=" + task.loc;
	}

	public void debugTasks()
	{
		_log.info("== [" + getActor() + "] ==");
		if(_task_list.size() == 0)
		{
			System.out.println("++ No Tasks");
			return;
		}

		int i = 0;
		for(final Task task : _task_list)
		{
			_log.info("++ task [" + i + "]: " + debugTask(task));
			i++;
		}
	}

	public boolean getBool(final AiOptionsType type, final boolean _defult)
	{
		return getAIParams().getBool(type, _defult);
	}

	public int getInt(final AiOptionsType type, final int _defult)
	{
		return getAIParams().getInt(type, _defult);
	}

	public long getLong(final AiOptionsType type, final long _defult)
	{
		return getAIParams().getLong(type, _defult);
	}

	public float getFloat(final AiOptionsType type, final float _defult)
	{
		return getAIParams().getFloat(type, _defult);
	}

	public String getString(final AiOptionsType type, final String _defult)
	{
		return getAIParams().getString(type, _defult);
	}

	protected boolean defaultThinkBuff(final int rateSelf)
	{
		return defaultThinkBuff(rateSelf, 0);
	}

	protected boolean defaultThinkBuff(final int rateSelf, final int rateFriends)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		// TODO сделать более разумный выбор баффа, сначала выбирать подходящие а потом уже рандомно 1 из них
		if(_buff.length > 0)
		{
			if(Rnd.chance(rateSelf))
			{
				final L2Skill r_skill = _buff[Rnd.get(_buff.length)];
				if(actor.getEffectList().getFirstEffect(r_skill) == null)
				{
					// Добавить новое задание
					addTaskBuff(actor, r_skill);
					return true;
				}
			}

			if(Rnd.chance(rateFriends))
			{
				final L2Skill r_skill = _buff[Rnd.get(_buff.length)];
				double bestDistance = 1000000;
				L2NpcInstance target = null;
				for(final L2NpcInstance npc : actor.getActiveFriends(true, true))
					if(npc != null && npc.getEffectList().getFirstEffect(r_skill) == null)
					{
						final double distance = actor.getDistance(npc);
						if(target == null || bestDistance > distance)
						{
							target = npc;
							bestDistance = distance;
						}

					}

				if(target != null)
				{
					// Добавить новое задание
					addTaskBuff(target, r_skill);
					return true;
				}
			}
		}

		return false;
	}

	protected boolean defaultFightTask()
	{
		clearTasks();

		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		double distance = actor.getDistance(target);

		if(actor.isAttackMuted() || Rnd.chance(getRatePHYS()))
			return chooseTaskAndTargets(null, target, distance);

		final double target_hp_precent = target.getCurrentHpPercents();
		final double actor_hp_precent = actor.getCurrentHpPercents();

		// Изначально все дружественные цели живые
		double frendly_target_hp_precent = 100;
		L2MonsterInstance targetToHeal = null;
		if(actor.isMinion())
		{
			// Ищем самую дохлую дружественную цель
			final L2MonsterInstance master = ((L2MinionInstance) actor).getLeader();
			if(master != null && !master.isDead() && master.isInCombat())
			{
				if(frendly_target_hp_precent > master.getCurrentHpPercents())
				{
					targetToHeal = master;
					frendly_target_hp_precent = master.getCurrentHpPercents();
				}

				final MinionList list = master.getMinionList();
				if(list != null)
					for(final L2MinionInstance m : list.getSpawnedMinions())
						if(m != actor && frendly_target_hp_precent > m.getCurrentHpPercents())
						{
							targetToHeal = m;
							frendly_target_hp_precent = m.getCurrentHpPercents();
						}
			}
		}

		final L2Skill[] dam_skills = getRateDAM() > 0 ? selectUsableSkills(target, distance, _dam_skills) : null;
		final L2Skill[] dot_skills = getRateDOT() > 0 && target_hp_precent > 10 ? selectUsableSkills(target, distance, _dot_skills) : null;
		final L2Skill[] debuff_skills = getRateDEBUFF() > 0 && target_hp_precent > 10 ? selectUsableSkills(target, distance, _debuff_skills) : null;
		final L2Skill[] stun = getRateSTUN() > 0 ? selectUsableSkills(target, distance, _stun) : null;
		final L2Skill[] heal = getRateHEAL() > 0 && (actor_hp_precent < 85 || frendly_target_hp_precent < 95) ? selectUsableSkills(actor, 0, _heal) : null;
		final L2Skill[] buff = getRateBUFF() > 0 ? selectUsableSkills(actor, 0, _buff) : null;

		final RndSelector<L2Skill[]> rnd = new RndSelector<L2Skill[]>();
		rnd.add(dam_skills, getRateDAM());
		rnd.add(dot_skills, getRateDOT());
		rnd.add(debuff_skills, getRateDEBUFF());
		rnd.add(heal, getRateHEAL());
		rnd.add(buff, getRateBUFF());
		rnd.add(stun, getRateSTUN());

		final L2Skill[] selected_skills = rnd.select();
		rnd.clear();

		if(selected_skills == dam_skills || selected_skills == dot_skills)
			return chooseTaskAndTargets(selectTopSkillByDamage(actor, target, distance, selected_skills), target, distance);

		if(selected_skills == debuff_skills || selected_skills == stun)
			return chooseTaskAndTargets(selectTopSkillByDebuff(actor, target, distance, selected_skills), target, distance);

		// TODO сделать баф дружественных целей
		if(selected_skills == buff)
			return chooseTaskAndTargets(selectTopSkillByBuff(actor, selected_skills), actor, distance);

		// TODO сделать хил дружественный целей для обычных мобов
		if(selected_skills == heal)
			if(actor_hp_precent < frendly_target_hp_precent)
				return chooseTaskAndTargets(selectTopSkillByHeal(actor, selected_skills), actor, distance);
			else
			{
				distance = actor.getDistance(targetToHeal);
				return chooseTaskAndTargets(selectTopSkillByHeal(targetToHeal, selected_skills), targetToHeal, distance);
			}

		return chooseTaskAndTargets(null, target, distance);
	}

	protected L2Character prepareTarget()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return null;

		// Новая цель исходя из агрессивности
		final L2Character hated = actor.isConfused() && getAttackTarget() != actor ? getAttackTarget() : actor.getMostHated();

		// Для "двинутых" боссов, иногда, выбираем случайную цель
		if(!actor.isConfused() && Rnd.chance(getInt(AiOptionsType.IS_MADNESS, 1)))
		{
			final L2Character randomHated = actor.getRandomHated();
			if(randomHated != null && randomHated != hated && randomHated != actor)
			{
				setAttackTarget(randomHated);
				if(_madnessTask == null && !actor.isConfused())
				{
					actor.startConfused();
					_madnessTask = L2GameThreadPools.getInstance().scheduleAi(new MadnessTask(), 10000, false);
				}
				return randomHated;
			}
		}

		if(hated != null && hated != actor)
		{
			setAttackTarget(hated);
			return hated;
		}

		returnHome(false);
		return null;
	}

	protected boolean canUseSkill(final L2Skill sk, final L2Character target)
	{
		return canUseSkill(sk, target, 0);
	}

	protected boolean canUseSkill(final L2Skill sk, final L2Character target, final double distance)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || sk == null || sk.isNotUsedByAI() || actor.isSkillDisabled(sk))
			return false;

		double mpConsume2 = sk.getMpConsume();
		if(sk.isMagic())
			mpConsume2 = actor.calcStat(Stats.MP_MAGIC_SKILL_CONSUME, mpConsume2, null, sk);
		else
			mpConsume2 = actor.calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, mpConsume2, null, sk);
		if(actor.getCurrentMp() < mpConsume2)
			return false;

		if(actor.isMuted(sk))
			return false;

		final SkillType st = sk.getSkillType();
		if(st == SkillType.CANCEL && !Rnd.chance(target.getEffectList().getAllCancelableEffectsCount(-1) * 20))
			return false;

		final int castRange = sk.getAOECastRange();
		if(castRange <= 200 && distance > 200)
			return false;

		return target.getEffectList().getFirstEffect(sk) == null;
	}

	protected L2Skill[] selectUsableSkills(final L2Character target, final double distance, final L2Skill... skills)
	{
		if(skills == null || skills.length == 0 || target == null)
			return null;
		L2Skill[] ret = null;
		int usable = 0;

		for(final L2Skill skill : skills)
			if(canUseSkill(skill, target, distance))
			{
				if(ret == null)
					ret = new L2Skill[skills.length];
				ret[usable++] = skill;
			}

		if(ret == null || usable == skills.length)
			return ret;
		final L2Skill[] ret_resized = new L2Skill[usable];
		System.arraycopy(ret, 0, ret_resized, 0, usable);
		return ret_resized;
	}

	protected static L2Skill selectTopSkillByDamage(final L2Character actor, final L2Character target, final double distance, final L2Skill... skills)
	{
		if(skills == null || skills.length == 0)
			return null;

		final RndSelector<L2Skill> rnd = new RndSelector<L2Skill>(skills.length);
		double weight;
		for(final L2Skill skill : skills)
		{
			weight = skill.getSimpleDamage(actor, target) * skill.getAOECastRange() / distance;
			if(weight <= 0)
				weight = 1;
			rnd.add(skill, (int) weight);
		}
		return rnd.select();
	}

	protected static L2Skill selectTopSkillByDebuff(final L2Character actor, final L2Character target, final double distance, final L2Skill... skills)
	{
		if(skills == null || skills.length == 0)
			return null;

		final RndSelector<L2Skill> rnd = new RndSelector<L2Skill>(skills.length);
		double weight;
		for(final L2Skill skill : skills)
		{
			if(skill.getSameByStackType(target) != null)
				continue;
			if((weight = 100f * skill.getAOECastRange() / distance) <= 0)
				weight = 1;
			rnd.add(skill, (int) weight);
		}
		return rnd.select();
	}

	protected static L2Skill selectTopSkillByBuff(final L2Character target, final L2Skill... skills)
	{
		if(skills == null || skills.length == 0)
			return null;

		final RndSelector<L2Skill> rnd = new RndSelector<L2Skill>(skills.length);
		double weight;
		for(final L2Skill skill : skills)
		{
			if(skill.getSameByStackType(target) != null)
				continue;
			if((weight = skill.getPower()) <= 0)
				weight = 1;
			rnd.add(skill, (int) weight);
		}
		return rnd.select();
	}

	protected static L2Skill selectTopSkillByHeal(final L2Character target, final L2Skill... skills)
	{
		if(skills == null || skills.length == 0)
			return null;

		final double hp_reduced = target.getMaxHp() - target.getCurrentHp();
		if(hp_reduced < 1)
			return null;

		final RndSelector<L2Skill> rnd = new RndSelector<L2Skill>(skills.length);
		double weight;
		for(final L2Skill skill : skills)
		{
			if((weight = Math.abs(skill.getPower() - hp_reduced)) <= 0)
				weight = 1;
			rnd.add(skill, (int) weight);
		}
		return rnd.select();
	}

	protected void addDesiredSkill(final Map<L2Skill, Integer> skill_list, final L2Character target, final double distance, final L2Skill[] skills_for_use)
	{
		if(skills_for_use == null || skills_for_use.length == 0 || target == null)
			return;
		for(final L2Skill sk : skills_for_use)
			addDesiredSkill(skill_list, target, distance, sk);
	}

	protected void addDesiredSkill(final Map<L2Skill, Integer> skill_list, final L2Character target, final double distance, final L2Skill skill_for_use)
	{
		if(skill_for_use == null || target == null || !canUseSkill(skill_for_use, target))
			return;
		int weight = (int) -Math.abs(skill_for_use.getAOECastRange() - distance);
		if(skill_for_use.getAOECastRange() >= distance)
			weight += 1000000;
		else if(skill_for_use.isNotTargetAoE() && skill_for_use.getTargets(getActor(), target, false).length == 0)
			return;
		skill_list.put(skill_for_use, weight);
	}

	protected void addDesiredHeal(final Map<L2Skill, Integer> skill_list, final L2Skill[] skills_for_use)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || skills_for_use == null || skills_for_use.length == 0)
			return;
		final double hp_reduced = actor.getMaxHp() - actor.getCurrentHp();
		final double hp_precent = actor.getCurrentHpPercents();
		if(hp_reduced < 1)
			return;
		int weight;
		for(final L2Skill sk : skills_for_use)
			if(canUseSkill(sk, actor) && sk.getPower() <= hp_reduced)
			{
				weight = (int) sk.getPower();
				if(hp_precent < 50)
					weight += 1000000;
				skill_list.put(sk, weight);
			}
	}

	protected void addDesiredBuff(final Map<L2Skill, Integer> skill_list, final L2Skill[] skills_for_use)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || skills_for_use == null || skills_for_use.length == 0)
			return;
		for(final L2Skill sk : skills_for_use)
			if(canUseSkill(sk, actor))
				skill_list.put(sk, 1000000);
	}

	protected L2Skill selectTopSkill(final Map<L2Skill, Integer> skill_list)
	{
		if(skill_list == null || skill_list.isEmpty())
			return null;
		int next_weight, top_weight = Integer.MIN_VALUE;
		for(final L2Skill next : skill_list.keySet())
			if((next_weight = skill_list.get(next)) > top_weight)
				top_weight = next_weight;
		if(top_weight == Integer.MIN_VALUE)
			return null;
		for(final L2Skill next : skill_list.keySet())
			if(skill_list.get(next) < top_weight)
				skill_list.remove(next);
		next_weight = skill_list.size();
		return skill_list.keySet().toArray(new L2Skill[next_weight])[Rnd.get(next_weight)];
	}

	protected boolean chooseTaskAndTargets(final L2Skill r_skill, L2Character target, final double distance)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		// Использовать скилл если можно, иначе атаковать
		if(r_skill != null && !actor.isMuted(r_skill))
		{
			// Проверка цели, и смена если необходимо
			if(r_skill.getTargetType() == L2Skill.SkillTargetType.TARGET_SELF)
				target = actor;
			else if(actor.isMovementDisabled() && r_skill.isOffensive() && distance > r_skill.getAOECastRange() + 60)
			{
				final GArray<L2Playable> targets = new GArray<L2Playable>();
				for(final AggroInfo ai : actor.getAggroList())
					if(ai.attacker != null && actor.getDistance(ai.attacker) <= r_skill.getAOECastRange() + 60)
						targets.add(ai.attacker);
				if(!targets.isEmpty())
					target = targets.getUnsafe(Rnd.get(targets.size()));
			}

			if(actor.isAttackMuted())
				return false;

			// Добавить новое задание
			final Task task = r_skill.isOffensive() ? getTaskFactory().createCastTask(r_skill, target) : getTaskFactory().createBuffTask(r_skill, target);
			_task_list.add(task);
			_def_think = true;
			return true;
		}

		// Смена цели, если необходимо
		if(actor.isMovementDisabled() && distance > actor.getPhysicalAttackRange() + 40)
		{
			final GArray<L2Playable> targets = new GArray<L2Playable>();
			for(final AggroInfo ai : actor.getAggroList())
				if(ai.attacker != null && actor.getDistance(ai.attacker) <= actor.getPhysicalAttackRange() + 40)
					targets.add(ai.attacker);
			if(!targets.isEmpty())
				target = targets.getUnsafe(Rnd.get(targets.size()));
		}

		// Добавить новое задание
		addTaskAttack(target);
		return true;
	}

	public int getRatePHYS()
	{
		return 100;
	}

	public int getRateDOT()
	{
		return 0;
	}

	public int getRateDEBUFF()
	{
		return 0;
	}

	public int getRateDAM()
	{
		return 0;
	}

	public int getRateSTUN()
	{
		return 0;
	}

	public int getRateHEAL()
	{
		return 0;
	}

	public int getRateBUFF()
	{
		return 0;
	}

	public int getMaxPursueRange()
	{
		return MAX_PURSUE_RANGE;
	}

	@Override
	public void setMaxPursueRange(final int range)
	{
		MAX_PURSUE_RANGE = range;
	}

	public int getMaxPathfindFails()
	{
		return 3;
	}

	public int getMaxAttackTimeout()
	{
		return 20000;
	}

	public int getTeleportTimeout()
	{
		return 10000;
	}

	@Override
	public boolean isGlobalAggro()
	{
		if(_globalAggro == 0)
			return true;
		if(_globalAggro < System.currentTimeMillis())
		{
			_globalAggro = 0;
			return true;
		}
		return false;
	}

	@Override
	public void setGlobalAggro(final long value)
	{
		_globalAggro = value;
	}

	public final long getGlobalAggro()
	{
		return _globalAggro;
	}

	public final int getTaskDelay()
	{
		return AI_TASK_DELAY;
	}

	public final int getTaskActiveDelay()
	{
		return AI_TASK_ACTIVE_DELAY;
	}

	public final boolean isThinking()
	{
		return _thinking;
	}

	public final boolean isMobile()
	{
		return _isMobile;
	}

	public final boolean isDefThink()
	{
		return _def_think;
	}

	public final int getTasksSize()
	{
		return _task_list == null ? 0 : _task_list.size();
	}

	public final int getRandomTasksSize()
	{
		return _random_task_list == null ? 0 : _random_task_list.size();
	}

	private boolean _canUseUD = false; // можно ли использовать НПЦ УД (по умолчанию - нет)
	private long _lastUDUseTime = 0; // время последнего использования НПЦ УД

	/** вычисляет, можно ли использовать УД */
	private boolean canUseUD()
	{
		// можно разрешить повторное использование УД через определенный промежуток времени,
		// но только если оно уже было хоть раз использовано
		if(!_canUseUD && _lastUDUseTime > 0)
		{
			// если прошло 5 минут с момента последнего каста УД, разрешим его снова использовать
			final long curTime = System.currentTimeMillis();
			if(curTime - _lastUDUseTime > 300000)
				_canUseUD = true;
		}
		return _canUseUD;
	}

	/** разрешает/запрещает использование УД для этого АИ (независимо от времени) */
	@Override
	public void setCanUseUD(final boolean can)
	{
		_canUseUD = can;
		if(!can)
			_lastUDUseTime = 0;
		else
			_lastUDUseTime = 1;
	}

	/** Кастует УД (если разрешено) */
	@Override
	public void useNpcUltimateDefense()
	{
		if(!canUseUD())
			return;

		final L2Character actor = getActor();
		if(actor == null)
			return;

		_canUseUD = false; // запретим повторное использование УД
		_lastUDUseTime = System.currentTimeMillis(); // запомним время последнего юза УД
		actor.doCast(ud_skill, actor, false);
	}

	@Override
	public TaskFactory getTaskFactory()
	{
		return TASK_FACTORY;
	}

	@Override
	public L2NpcInstance getActor()
	{
		return (L2NpcInstance) super.getActor();
	}

	// ---------------------------------------------
	// Time methods
	// ---------------------------------------------

	@Override
	public boolean mayFireNow(final long currentTime)
	{
		return getNextFireTime() <= currentTime;
	}

	@Override
	public final long getNextFireTime()
	{
		return isActive() ? nextFireTime : NO_FIRE_MORE;
	}

	@Override
	public final void setNextFireTime(final long nextFireTime)
	{
		this.nextFireTime = nextFireTime;
	}

	@Override
	public final long getRepeatInterval()
	{
		// в спокойном состоянии повтор действий реже
		return getIntention().isIdle() ? getTaskActiveDelay() : getTaskDelay();
	}

	@Override
	public final void updateAfterFire(final long currentTime)
	{
		setNextFireTime(currentTime + getRepeatInterval());
	}
}
