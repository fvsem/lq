package l2n.game.ai;

import l2n.Config;
import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.DefaultListenerEngine;
import l2n.extensions.listeners.engine.ListenerEngine;
import l2n.extensions.listeners.events.AbstractAI.AbstractAINotifyEvent;
import l2n.extensions.listeners.events.AbstractAI.AbstractAISetIntention;
import l2n.game.ai.tasks.TaskFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.util.Location;

import java.util.logging.Logger;

import static l2n.game.ai.CtrlIntention.*;

public abstract class AbstractAI
{
	protected static final Logger _log = Logger.getLogger(AbstractAI.class.getName());

	private IHardReference<? extends L2Character> _actor = HardReferences.emptyRef();
	private IHardReference<? extends L2Character> _attack_target = HardReferences.emptyRef();

	private CtrlIntention _intention = AI_INTENTION_IDLE;

	private ListenerEngine<AbstractAI> listenerEngine;

	protected AbstractAI(final L2Character actor)
	{
		refreshActor(actor);
	}

	public void refreshActor(final L2Character actor)
	{
		_actor = actor.getRef();
	}

	public void changeIntention(final CtrlIntention intention, final Object arg0, final Object arg1)
	{
		if(Config.DEBUG)
			_log.info("AbstractAI: " + getActor() + ".changeIntention(" + intention + ", " + arg0 + ", " + arg1 + ")");
		_intention = intention;
		if(intention != AI_INTENTION_CAST && intention != AI_INTENTION_ATTACK)
			setAttackTarget(null);
	}

	public final void setIntention(final CtrlIntention intention)
	{
		setIntention(intention, null, null);
	}

	public final void setIntention(final CtrlIntention intention, final Object arg0)
	{
		setIntention(intention, arg0, null);
	}

	public void setIntention(CtrlIntention intention, final Object arg0, final Object arg1)
	{
		if(intention != AI_INTENTION_CAST && intention != AI_INTENTION_ATTACK)
			setAttackTarget(null);

		final L2Character actor = getActor();
		if(actor == null)
			return;

		if(!actor.isVisible())
		{
			if(_intention == AI_INTENTION_IDLE)
				return;
			intention = AI_INTENTION_IDLE;
		}

		getListenerEngine().fireMethodInvoked(new AbstractAISetIntention(MethodCollection.AbstractAIsetIntention, this, new Object[] { intention, arg0, arg1 }));

		switch (intention)
		{
			case AI_INTENTION_IDLE:
				onIntentionIdle();
				break;
			case AI_INTENTION_ACTIVE:
				onIntentionActive();
				break;
			case AI_INTENTION_REST:
				onIntentionRest();
				break;
			case AI_INTENTION_ATTACK:
				onIntentionAttack((L2Character) arg0);
				break;
			case AI_INTENTION_CAST:
				onIntentionCast((L2Skill) arg0, (L2Character) arg1);
				break;
			case AI_INTENTION_PICK_UP:
				onIntentionPickUp((L2Object) arg0);
				break;
			case AI_INTENTION_INTERACT:
				onIntentionInteract((L2Object) arg0);
				break;
			case AI_INTENTION_FOLLOW:
				onIntentionFollow((L2Character) arg0, (Integer) arg1);
				break;
		}
	}

	public final void notifyEvent(final CtrlEvent evt)
	{
		notifyEvent(evt, null, null, null);
	}

	public final void notifyEvent(final CtrlEvent evt, final Object arg0)
	{
		notifyEvent(evt, arg0, null, null);
	}

	public final void notifyEvent(final CtrlEvent evt, final Object arg0, final Object arg1)
	{
		notifyEvent(evt, arg0, arg1, null);
	}

	public void notifyEvent(final CtrlEvent evt, final Object arg0, final Object arg1, final Object arg2)
	{
		final L2Character actor = getActor();
		if(actor == null || !actor.isVisible())
			return;

		getListenerEngine().fireMethodInvoked(new AbstractAINotifyEvent(MethodCollection.AbstractAInotifyEvent, this, new Object[] { evt, arg0, arg1 }));

		switch (evt)
		{
			case EVT_THINK:
				onEvtThink();
				break;
			case EVT_ATTACKED:
				onEvtAttacked((L2Character) arg0, ((Number) arg1).intValue());
				break;
			case EVT_CLAN_ATTACKED:
				onEvtClanAttacked((L2Character) arg0, (L2Character) arg1, ((Number) arg2).intValue());
				break;
			case EVT_AGGRESSION:
				onEvtAggression((L2Character) arg0, ((Number) arg1).intValue());
				break;
			case EVT_READY_TO_ACT:
				onEvtReadyToAct();
				break;
			case EVT_ARRIVED:
				onEvtArrived();
				break;
			case EVT_ARRIVED_TARGET:
				onEvtArrivedTarget();
				break;
			case EVT_ARRIVED_BLOCKED:
				onEvtArrivedBlocked((Location) arg0);
				break;
			case EVT_FORGET_OBJECT:
				onEvtForgetObject((L2Object) arg0);
				break;
			case EVT_DEAD:
				onEvtDead((L2Character) arg0);
				break;
			case EVT_FAKE_DEATH:
				onEvtFakeDeath();
				break;
			case EVT_FINISH_CASTING:
				onEvtFinishCasting(arg0 == null ? null : (L2Skill) arg0);
				break;
			case EVT_SEE_SPELL:
				onEvtSeeSpell((L2Skill) arg0, (L2Character) arg1);
				break;
			case EVT_SPAWN:
				onEvtSpawn();
				break;
		}
	}

	protected void clientActionFailed()
	{
		final L2Character actor = getActor();
		if(actor != null && actor.isPlayer())
			actor.sendActionFailed();
	}

	/**
	 * Останавливает движение и рассылает ValidateLocation
	 */
	public void clientStopMoving()
	{
		final L2Character actor = getActor();
		if(actor == null)
			return;

		actor.stopMove();
	}

	/**
	 * Останавливает движение
	 * 
	 * @param validate
	 *            - рассылать ли ValidateLocation
	 */
	public void clientStopMoving(final boolean validate)
	{
		final L2Character actor = getActor();
		if(actor == null)
			return;

		actor.stopMove(validate);
	}

	public TaskFactory getTaskFactory()
	{
		return null;
	}

	public L2Character getActor()
	{
		return _actor.get();
	}

	public void removeActor()
	{
		_actor = HardReferences.<L2Character> emptyRef();
		_attack_target = HardReferences.<L2Character> emptyRef();
	}

	public CtrlIntention getIntention()
	{
		return _intention;
	}

	public final void setAttackTarget(final L2Character target)
	{
		_attack_target = target == null ? HardReferences.<L2Character> emptyRef() : target.getRef();
	}

	public final L2Character getAttackTarget()
	{
		return _attack_target.get();
	}

	/** Означает, что AI всегда включен, независимо от состояния региона */
	public boolean isGlobalAI()
	{
		return false;
	}

	public void setGlobalAggro(final long value)
	{}

	public void setMaxPursueRange(final int range)
	{}

	@Override
	public String toString()
	{
		return getL2ClassShortName() + " for " + getActor();
	}

	public String getL2ClassShortName()
	{
		if(getClass() != null && getClass().getName() != null)
			return getClass().getName().replaceAll("^.*\\.(.*?)$", "$1");
		return "";
	}

	protected abstract void onIntentionIdle();

	protected abstract void onIntentionActive();

	protected abstract void onIntentionRest();

	protected abstract void onIntentionAttack(L2Character target);

	protected abstract void onIntentionCast(L2Skill skill, L2Character target);

	protected abstract void onIntentionPickUp(L2Object item);

	protected abstract void onIntentionInteract(L2Object object);

	protected abstract void onEvtThink();

	protected abstract void onEvtAttacked(L2Character attacker, int damage);

	protected abstract void onEvtClanAttacked(L2Character attackedMember, L2Character attacker, int damage);

	/** Increase/decrease aggression towards a target, or reduce global aggression if target is null */
	protected abstract void onEvtAggression(L2Character target, int aggro);

	protected abstract void onEvtReadyToAct();

	/**
	 * The actor arrived to assigned location, or it's a time to modify movement destination (follow, interact, random move and others intentions).
	 */
	protected abstract void onEvtArrived();

	/** The actor arrived to assigned location. */
	protected abstract void onEvtArrivedTarget();

	/** The actor cannot move anymore. */
	protected abstract void onEvtArrivedBlocked(Location blocked_at_pos);

	protected abstract void onEvtForgetObject(L2Object object);

	/** The actor is dead */
	protected abstract void onEvtDead(L2Character killer);

	/** The actor looks like dead */
	protected abstract void onEvtFakeDeath();

	protected abstract void onEvtFinishCasting(L2Skill skill);

	/** The character finish casting **/
	protected abstract void onEvtSeeSpell(L2Skill skill, L2Character caster);

	protected abstract void onEvtSpawn();

	protected abstract void onEvtDeSpawn();

	protected abstract void onScriptEvent(final int eventId, final Object... args);

	protected abstract void onIntentionFollow(L2Character target, Integer offset);

	public ListenerEngine<AbstractAI> getListenerEngine()
	{
		if(listenerEngine == null)
			listenerEngine = new DefaultListenerEngine<AbstractAI>(this);
		return listenerEngine;
	}
}
