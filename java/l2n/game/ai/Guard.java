package l2n.game.ai;

import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;

public class Guard extends Fighter implements Runnable
{
	public Guard(L2Character actor)
	{
		super(actor);
		MAX_Z_AGGRO_RANGE = 1300;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	public void checkAggression(L2Character target)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || target.getKarma() <= 0)
			return;
		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE)
			return;
		if(_globalAggro < 0)
			return;
		if(target.getHateList().get(actor) == null && !actor.isInRange(target, 600))
			return;
		if(Math.abs(target.getZ() - actor.getZ()) > MAX_Z_AGGRO_RANGE)
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;
		if(target.isPlayer() && ((L2Player) target).isInvisible())
			return;

		if((target.isSummon() || target.isPet()) && target.getPlayer() != null)
			target.getPlayer().addDamageHate(actor, 0, 1);

		target.addDamageHate(actor, 0, 2);
		startRunningTask(2000);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}
}
