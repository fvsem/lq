package l2n.game.ai;

import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2SiegeGuardInstance;
import l2n.util.Rnd;

import java.lang.ref.WeakReference;

public class L2StaticObjectAI extends L2CharacterAI
{
	private long _attacker;
	private WeakReference<Siege> _siege = null;

	public L2StaticObjectAI(L2Character actor)
	{
		super(actor);
	}

	private Siege getSiege()
	{
		Siege result = _siege == null ? null : _siege.get();
		if(result == null)
		{
			result = SiegeManager.getSiege(getActor(), true);
			_siege = result != null ? new WeakReference<Siege>(result) : null;
		}
		return result;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2Character actor;
		if(attacker == null || (actor = getActor()) == null || !actor.isDoor())
			return;

		L2Player player = attacker.getPlayer();
		if(player != null)
		{
			L2Clan clan = player.getClan();
			Siege siege = SiegeManager.getSiege(actor, true);

			if(siege == null)
				return;

			// TODO присвоить осаду обьекту при спавне, чтобы избавиться от перебора
			if(clan != null && siege == clan.getSiege() && clan.isDefender())
				return;

			for(L2NpcInstance npc : actor.getAroundNpc(900, 200))
			{
				if(!(npc instanceof L2SiegeGuardInstance))
					continue;

				if(Rnd.chance(20))
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 10000);
				else
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 2000);
			}
		}
	}

	@Override
	protected void onEvtAggression(L2Character attacker, int aggro)
	{
		L2Character actor;
		L2Player player;
		if(attacker == null || (player = attacker.getPlayer()) == null || (actor = getActor()) == null)
			return;

		if(actor.isArtefact())
		{
			L2Clan clan = player.getClan();
			if(clan == null || !clan.isDefender() || getSiege() != clan.getSiege())
				L2GameThreadPools.getInstance().scheduleAi(new notifyGuard(player), 1000, false);
		}
	}

	public class notifyGuard implements Runnable
	{
		notifyGuard(L2Player attacker)
		{
			_attacker = attacker.getStoredId();
		}

		@Override
		public void run()
		{
			L2Character actor;
			L2Player attacker = L2ObjectsStorage.getAsPlayer(_attacker);
			if(attacker == null || (actor = getActor()) == null)
				return;

			for(L2NpcInstance npc : actor.getAroundNpc(1500, 200))
				if(npc instanceof L2SiegeGuardInstance && Rnd.chance(20))
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 5000);

			if(attacker.getCastingSkill() != null && attacker.getCastingSkill().getTargetType() == SkillTargetType.TARGET_HOLY)
				L2GameThreadPools.getInstance().scheduleAi(this, 10000, false);
		}
	}
}
