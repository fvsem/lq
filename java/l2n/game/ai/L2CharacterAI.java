package l2n.game.ai;

import l2n.Config;
import l2n.commons.Universe;
import l2n.game.ai.L2PlayableAI.nextAction;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.network.serverpackets.Die;
import l2n.util.Location;

import static l2n.game.ai.CtrlIntention.*;

public class L2CharacterAI extends AbstractAI
{
	public L2CharacterAI(final L2Character actor)
	{
		super(actor);
	}

	/** разрешает/запрещает использование УД для этого АИ (независимо от времени) */
	public void setCanUseUD(final boolean can)
	{}

	/** Кастует УД (если разрешено) */
	public void useNpcUltimateDefense()
	{}

	@Override
	protected void onIntentionIdle()
	{
		clientStopMoving();
		changeIntention(AI_INTENTION_IDLE, null, null);
	}

	@Override
	protected void onIntentionActive()
	{
		clientStopMoving();
		changeIntention(AI_INTENTION_ACTIVE, null, null);
	}

	@Override
	protected void onIntentionAttack(final L2Character target)
	{
		setAttackTarget(target);
		changeIntention(AI_INTENTION_ATTACK, target, null);
		onEvtThink();
	}

	@Override
	protected void onIntentionCast(final L2Skill skill, final L2Character target)
	{
		setAttackTarget(target);
		changeIntention(AI_INTENTION_CAST, skill, target);
		onEvtThink();
	}

	@Override
	protected void onIntentionFollow(final L2Character target, final Integer offset)
	{
		changeIntention(AI_INTENTION_FOLLOW, target, offset);
		final L2Character actor = getActor();
		if(actor != null)
			actor.followToCharacter(target, offset, false);
	}

	@Override
	protected void onEvtArrivedBlocked(final Location blocked_at_pos)
	{
		final L2Character actor = getActor();
		if(actor != null)
		{
			final Location loc = ((L2Player) actor).getLastServerPosition();
			if(loc != null)
				actor.setXYZ(loc.x, loc.y, loc.z, true);

			actor.stopMove();

			if(Config.ACTIVATE_POSITION_RECORDER && Universe.getInstance().shouldLog(actor.getObjectId()) && !actor.isFlying())
				Universe.getInstance().registerObstacle(blocked_at_pos.x, blocked_at_pos.y, blocked_at_pos.z);
		}
		onEvtThink();
	}

	@Override
	protected void onEvtForgetObject(final L2Object object)
	{
		final L2Character actor = getActor();
		if(actor == null || object == null)
			return;

		if(actor.isAttackingNow() && getAttackTarget() == object)
			actor.abortAttack();

		if(actor.isCastingNow() && getAttackTarget() == object)
			actor.abortCast();

		if(getAttackTarget() == object)
			setAttackTarget(null);

		if(actor.getTargetId() == object.getObjectId())
			actor.setTarget(null);
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2Character actor = getActor();
		if(actor != null)
		{
			actor.abortAttack();
			actor.breakCast(true);
			actor.stopMove();
			actor.broadcastPacket(new Die(actor));
		}
		setIntention(AI_INTENTION_IDLE);
	}

	@Override
	protected void onEvtFakeDeath()
	{
		clientStopMoving();
		setIntention(AI_INTENTION_IDLE);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{

	}

	@Override
	protected void onEvtClanAttacked(final L2Character attackedMember, final L2Character attacker, final int damage)
	{}

	public void Attack(final L2Object target, final boolean forceUse, final boolean dontMove)
	{
		setIntention(AI_INTENTION_ATTACK, target);
	}

	public void Cast(final L2Skill skill, final L2Character target)
	{
		Cast(skill, target, false, false);
	}

	public void Cast(final L2Skill skill, final L2Character target, final boolean forceUse, final boolean dontMove)
	{
		setIntention(AI_INTENTION_ATTACK, target);
	}

	@Override
	protected void onEvtThink()
	{}

	@Override
	protected void onEvtAggression(final L2Character target, final int aggro)
	{}

	@Override
	protected void onEvtFinishCasting(final L2Skill skill)
	{}

	@Override
	protected void onIntentionInteract(final L2Object object)
	{}

	@Override
	protected void onIntentionPickUp(final L2Object item)
	{}

	@Override
	protected void onEvtReadyToAct()
	{}

	@Override
	protected void onEvtArrived()
	{
		final L2Character actor = getActor();
		if(actor != null && actor.isVehicle())
			((L2Vehicle) actor).VehicleArrived();
	}

	@Override
	public void onScriptEvent(final int eventId, final Object... args)
	{}

	@Override
	protected void onEvtArrivedTarget()
	{}

	@Override
	protected void onIntentionRest()
	{}

	@Override
	protected void onEvtSeeSpell(final L2Skill skill, final L2Character caster)
	{}

	@Override
	protected void onEvtSpawn()
	{}

	@Override
	public void onEvtDeSpawn()
	{}

	public void stopAITask()
	{}

	public void startAITask()
	{}

	public void setNextAction(final nextAction action, final Object arg0, final Object arg1, final boolean arg2, final boolean arg3)
	{}

	public void clearNextAction()
	{}

	public void teleportHome(final boolean clearAggro)
	{}

	public void checkAggression(final L2Character target)
	{}

	public boolean isActive()
	{
		return true;
	}

	public void clearTasks()
	{}

	public boolean isGlobalAggro()
	{
		return true;
	}

	public void addTaskMove(final Location loc)
	{}

	public void addTaskMove(final Location loc, final int weight)
	{}

	public void addTaskAttack(final L2Character target)
	{}

	public boolean canSeeInSilentMove(final L2Playable target)
	{
		return true;
	}
}
