package l2n.game.ai.tasks.random;

import l2n.game.ai.CtrlIntention;
import l2n.game.model.instances.L2NpcInstance;

public interface RandomTask
{
	public boolean execute(final L2NpcInstance actor);

	public boolean checkCondition(final L2NpcInstance actor, final CtrlIntention currentIntention, final long currentTimeMillis);
}
