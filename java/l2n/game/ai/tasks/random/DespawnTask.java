package l2n.game.ai.tasks.random;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.instances.L2NpcInstance;


public final class DespawnTask implements RandomTask
{
	private final String _msg;
	private long _startTime;
	private long _deleteTime;

	public DespawnTask(final String msg, final long deleteTime)
	{
		_msg = msg;
		_deleteTime = deleteTime;
		_startTime = System.currentTimeMillis();
	}

	@Override
	public boolean execute(final L2NpcInstance actor)
	{
		if(actor != null && !actor.isDead())
		{
			if(_msg != null)
				Functions.npcSay(actor, _msg);
			actor.deleteMe();
			return true;
		}
		return false;
	}

	@Override
	public boolean checkCondition(final L2NpcInstance actor, final CtrlIntention currentIntention, final long currentTimeMillis)
	{
		if(currentTimeMillis - _startTime > _deleteTime)
		{
			_startTime = currentTimeMillis;
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof DespawnTask))
			return false;
		final DespawnTask task = (DespawnTask) obj;
		if(!task._msg.equalsIgnoreCase(_msg))
			return false;
		return true;
	}
}
