package l2n.game.ai.tasks.random;

import l2n.commons.list.GArray;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.SkillUtil;


public final class BuffTask implements RandomTask
{
	private final long _repeat_time;
	private final L2Skill _buff;

	private long _lastBuffTime;

	public BuffTask(final int skillId, final int skill_level, final long repeat_time)
	{
		_buff = SkillTable.getInstance().getInfo(skillId, skill_level);
		_repeat_time = repeat_time;
		_lastBuffTime = System.currentTimeMillis();
	}

	@Override
	public boolean execute(final L2NpcInstance actor)
	{
		if(actor != null && !actor.isDead())
		{
			final GArray<L2Playable> targets = new GArray<L2Playable>();
			for(final L2Playable pl : L2World.getAroundPlayables(actor, 100, 200))
				if(pl != null && !pl.isDead() && pl.getEffectList().getFirstEffect(_buff) == null)
					if(_buff.checkTarget(actor, pl, pl, true, false) == null)
						targets.add(pl);

			final L2Playable[] victims = targets.toArray(new L2Playable[targets.size()]);
			if(victims != null && victims.length != 0)
			{
				actor.callSkill(_buff, false, victims);
				SkillUtil.broadcastUseAnimation(_buff, actor, victims);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean checkCondition(final L2NpcInstance actor, final CtrlIntention currentIntention, final long currentTimeMillis)
	{
		if(currentTimeMillis - _lastBuffTime > _repeat_time)
		{
			_lastBuffTime = currentTimeMillis;
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if(!(obj instanceof BuffTask))
			return false;
		BuffTask task = (BuffTask) obj;
		if(!task._buff.equals(_buff))
			return false;
		if(task._repeat_time != _repeat_time)
			return false;
		return true;
	}
}
