package l2n.game.ai.tasks;

import l2n.game.ai.DefaultAI;
import l2n.game.ai.DefaultAI.TaskType;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * Задание "добежать - применить скилл"
 */
public class BuffTask extends Task
{
	protected BuffTask(final L2Skill skill, final L2Character target, final int weight)
	{
		super(TaskType.BUFF, skill, target, null, weight);
	}

	@Override
	public boolean execute(final L2NpcInstance actor, final DefaultAI ai)
	{
		if(actor.isMuted(skill) || actor.isSkillDisabled(skill))
			return true;

		final L2Character attack_target = getTarget();
		if(attack_target == null || attack_target.isAlikeDead() || !actor.isInRange(attack_target, 2000))
			return true;

		final boolean isAoE = skill.getTargetType() == SkillTargetType.TARGET_AURA;
		final int castRange = skill.getAOECastRange();

		if(actor.isMoving)
			return Rnd.chance(10);

		if(actor.getRealDistance(attack_target) <= castRange + 60 && GeoEngine.canSeeTarget(actor, attack_target, false))
		{
			ai.clientStopMoving();
			ai.resetPathfindFails();
			actor.doCast(skill, isAoE ? actor : attack_target, !attack_target.isPlayable());
			return ai.maybeNextTask(this);
		}

		if(actor.isMovementDisabled() || !ai.isMobile())
			return true;

		ai.tryMoveToTarget(attack_target);
		return false;
	}
}
