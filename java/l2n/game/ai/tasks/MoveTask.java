package l2n.game.ai.tasks;

import l2n.game.ai.DefaultAI;
import l2n.game.ai.DefaultAI.TaskType;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;

public class MoveTask extends Task
{
	protected MoveTask(final Location loc, final int weight)
	{
		super(TaskType.MOVE, null, null, loc, weight);
	}

	@Override
	public boolean execute(final L2NpcInstance actor, final DefaultAI ai)
	{
		if(actor.isMovementDisabled() || !ai.isMobile())
			return true;

		if(actor.isInRange(loc, 100))
			return ai.maybeNextTask(this);

		if(actor.isMoving)
			return false;

		if(!actor.moveToLocation(loc, 0, true))
		{
			ai.clientStopMoving();
			ai.resetPathfindFails();
			actor.teleToLocation(loc);
			return ai.maybeNextTask(this);
		}

		return false;
	}
}
