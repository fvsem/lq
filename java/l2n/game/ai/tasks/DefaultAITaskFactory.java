package l2n.game.ai.tasks;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.util.Location;

public class DefaultAITaskFactory implements TaskFactory
{
	private static final int TASK_DEFAULT_WEIGHT = 10000;

	@Override
	public Task createMoveTask(final Location loc)
	{
		return new MoveTask(loc, TASK_DEFAULT_WEIGHT);
	}

	@Override
	public Task createMoveTask(final Location loc, final int weight)
	{
		return new MoveTask(loc, weight);
	}

	@Override
	public Task createAttackTask(final L2Character target)
	{
		return new AttackTask(target, TASK_DEFAULT_WEIGHT);
	}

	@Override
	public Task createCastTask(final L2Skill skill, final L2Character target)
	{
		return new CastTask(skill, target, TASK_DEFAULT_WEIGHT);
	}

	@Override
	public Task createCastTask(final L2Skill skill, final L2Character target, final int weight)
	{
		return new CastTask(skill, target, weight);
	}

	@Override
	public Task createBuffTask(final L2Skill skill, final L2Character target)
	{
		return new BuffTask(skill, target, TASK_DEFAULT_WEIGHT);
	}

	@Override
	public Task createBuffTask(final L2Skill skill, final L2Character target, final int weight)
	{
		return new BuffTask(skill, target, weight);
	}
}
