package l2n.game.ai.tasks;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.util.Location;


public interface TaskFactory
{
	public Task createMoveTask(final Location loc);

	public Task createMoveTask(final Location loc, final int weight);

	public Task createAttackTask(final L2Character target);

	public Task createCastTask(final L2Skill skill, final L2Character target);

	public Task createCastTask(final L2Skill skill, final L2Character target, final int weight);

	public Task createBuffTask(final L2Skill skill, final L2Character target);

	public Task createBuffTask(final L2Skill skill, final L2Character target, final int weight);
}
