package l2n.game.ai.tasks;

import l2n.game.ai.DefaultAI;
import l2n.game.ai.DefaultAI.TaskType;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * Задание "добежать - атаковать скиллом"
 */
public class CastTask extends Task
{
	protected CastTask(final L2Skill skill, final L2Character target, final int weight)
	{
		super(TaskType.CAST, skill, target, null, weight);
	}

	@Override
	public boolean execute(final L2NpcInstance actor, final DefaultAI ai)
	{
		if(actor.isMuted(skill) || actor.isSkillDisabled(skill))
			return true;

		final boolean isAoE = skill.getTargetType() == SkillTargetType.TARGET_AURA;

		final L2Character temp_attack_target = getTarget();
		if(ai.checkTarget(actor, temp_attack_target, false, 3000))
			return true;

		ai.setAttackTarget(temp_attack_target);

		final int castRange = skill.getAOECastRange();

		if(actor.getRealDistance(temp_attack_target) <= castRange + 60 && GeoEngine.canSeeTarget(actor, temp_attack_target, false))
		{
			ai.clientStopMoving();
			ai.resetPathfindFails();
			actor.setAttackTimeout(ai.getMaxAttackTimeout() + System.currentTimeMillis());
			actor.doCast(skill, isAoE ? actor : temp_attack_target, !temp_attack_target.isPlayable());
			return ai.maybeNextTask(this);
		}

		if(actor.isMoving)
			return Rnd.chance(10);

		if(actor.isMovementDisabled() || !ai.isMobile())
			return true;

		ai.tryMoveToTarget(temp_attack_target, castRange);
		return false;
	}
}
