package l2n.game.ai.tasks;

import l2n.game.ai.DefaultAI;
import l2n.game.ai.DefaultAI.TaskType;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class AttackTask extends Task
{
	protected AttackTask(final L2Character target, final int weight)
	{
		super(TaskType.ATTACK, null, target, null, weight);
	}

	@Override
	public boolean execute(final L2NpcInstance actor, final DefaultAI ai)
	{
		final L2Character attack_target = getTarget();
		if(ai.checkTarget(actor, attack_target, false, 2000))
			return true;

		ai.setAttackTarget(attack_target);

		if(actor.isMoving)
			return Rnd.chance(25);

		if(actor.getRealDistance(attack_target) <= actor.getPhysicalAttackRange() + 40 && GeoEngine.canSeeTarget(actor, attack_target, false))
		{
			ai.clientStopMoving();
			ai.resetPathfindFails();
			actor.setAttackTimeout(ai.getMaxAttackTimeout() + System.currentTimeMillis());
			actor.doAttack(attack_target);
			return ai.maybeNextTask(this);
		}

		if(actor.isMovementDisabled() || !ai.isMobile())
			return true;

		ai.tryMoveToTarget(attack_target);
		return false;
	}
}
