package l2n.game.ai.tasks;

import l2n.commons.lang.reference.HardReferences;
import l2n.commons.lang.reference.IHardReference;
import l2n.game.ai.DefaultAI;
import l2n.game.ai.DefaultAI.TaskType;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;

public abstract class Task
{
	public final TaskType type;
	public final L2Skill skill;
	public final Location loc;
	public final int weight;

	private final IHardReference<? extends L2Character> _target;

	protected Task(final TaskType type, final L2Skill skill, final L2Character target, final Location loc, final int weight)
	{
		this.type = type;
		this.skill = skill;
		_target = target == null ? HardReferences.<L2Character> emptyRef() : target.getRef();
		this.loc = loc;
		this.weight = weight;
	}

	public final L2Character getTarget()
	{
		return _target.get();
	}

	public abstract boolean execute(L2NpcInstance actor, final DefaultAI ai);
}
