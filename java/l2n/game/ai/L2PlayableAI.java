package l2n.game.ai;

import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.NextAction;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.util.Location;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;

import static l2n.game.ai.CtrlIntention.*;

public class L2PlayableAI extends L2CharacterAI
{
	private volatile int thinking = 0; // to prevent recursive thinking
	private Object intentionArg0 = null, intentionArg1 = null;
	private L2Skill _skill;

	private nextAction _nextAction;
	private Object _nextAction_arg0;
	private Object _nextAction_arg1;
	private boolean _nextAction_arg2;
	private boolean _nextAction_arg3;

	private boolean _forceUse;
	private boolean _dontMove;

	private ScheduledFuture<?> _followTask;
	private static final int FOLLOW_INTERVAL = 1000;

	public L2PlayableAI(final L2Playable actor)
	{
		super(actor);
	}

	public static enum nextAction
	{
		ATTACK,
		CAST,
		MOVE,
		REST,
		PICKUP,
		INTERACT
	}

	@Override
	public void changeIntention(final CtrlIntention intention, final Object arg0, final Object arg1)
	{
		super.changeIntention(intention, arg0, arg1);
		intentionArg0 = arg0;
		intentionArg1 = arg1;
	}

	@Override
	public void setIntention(final CtrlIntention intention, final Object arg0, final Object arg1)
	{
		intentionArg0 = null;
		intentionArg1 = null;
		super.setIntention(intention, arg0, arg1);
	}

	@Override
	protected void onIntentionCast(final L2Skill skill, final L2Character target)
	{
		if(skill.getHitTime() > 50)
		{
			final L2Playable actor = getActor();
			if(actor != null)
				actor.abortCast();
		}

		// Set the AI skill used by INTENTION_CAST
		_skill = skill;
		super.onIntentionCast(skill, target);
	}

	@Override
	public void setNextAction(final nextAction action, final Object arg0, final Object arg1, final boolean arg2, final boolean arg3)
	{
		_nextAction = action;
		_nextAction_arg0 = arg0;
		_nextAction_arg1 = arg1;
		_nextAction_arg2 = arg2;
		_nextAction_arg3 = arg3;
	}

	public boolean setNextIntention()
	{
		final nextAction nextAiAction = _nextAction;
		final Object nextAction_arg0 = _nextAction_arg0;
		final Object nextAction_arg1 = _nextAction_arg1;
		final boolean nextAction_arg2 = _nextAction_arg2;
		final boolean nextAction_arg3 = _nextAction_arg3;

		final L2Playable actor = getActor();
		if(nextAiAction == null || actor == null)
			return false;

		L2Skill skill;
		L2Character target;
		L2Object object;

		switch (nextAiAction)
		{
			case ATTACK:
				if(nextAction_arg0 == null)
					return false;
				target = (L2Character) nextAction_arg0;
				_forceUse = nextAction_arg2;
				_dontMove = nextAction_arg3;
				clearNextAction();
				setIntention(AI_INTENTION_ATTACK, target);
				break;
			case CAST:
				if(nextAction_arg0 == null || nextAction_arg1 == null)
					return false;
				skill = (L2Skill) nextAction_arg0;
				target = (L2Character) nextAction_arg1;
				_forceUse = nextAction_arg2;
				_dontMove = nextAction_arg3;
				clearNextAction();
				if(!skill.checkCondition(actor, target, _forceUse, _dontMove, true))
				{
					if(skill.getNextAction() == NextAction.ATTACK && !actor.equals(target) && !_forceUse)
					{
						setNextAction(nextAction.ATTACK, target, null, _forceUse, false);
						return setNextIntention();
					}
					return false;
				}
				setIntention(AI_INTENTION_CAST, skill, target);
				break;
			case MOVE:
				if(nextAction_arg0 == null || nextAction_arg1 == null)
					return false;
				final Location loc = (Location) nextAction_arg0;
				final Integer offset = (Integer) nextAction_arg1;
				clearNextAction();
				actor.moveToLocation(loc, offset, nextAction_arg2);
				break;
			case REST:
				actor.sitDown();
				break;
			case INTERACT:
				if(nextAction_arg0 == null)
					return false;
				object = (L2Object) nextAction_arg0;
				clearNextAction();
				onIntentionInteract(object);
				break;
			case PICKUP:
				if(nextAction_arg0 == null)
					return false;
				object = (L2Object) nextAction_arg0;
				clearNextAction();
				onIntentionPickUp(object);
				break;
			default:
				return false;
		}
		return true;
	}

	@Override
	public void clearNextAction()
	{
		_nextAction = null;
		_nextAction_arg0 = null;
		_nextAction_arg1 = null;
		_nextAction_arg2 = false;
		_nextAction_arg3 = false;
	}

	@Override
	protected void onEvtFinishCasting(final L2Skill skill)
	{
		if(!setNextIntention())
			setIntention(AI_INTENTION_ACTIVE);
	}

	@Override
	protected void onEvtReadyToAct()
	{
		if(!setNextIntention())
			onEvtThink();
	}

	@Override
	protected void onEvtArrived()
	{
		if(!setNextIntention())
			if(getIntention() == AI_INTENTION_INTERACT || getIntention() == AI_INTENTION_PICK_UP)
				onEvtThink();
			else
				changeIntention(AI_INTENTION_ACTIVE, null, null);
	}

	@Override
	protected void onEvtArrivedTarget()
	{
		try
		{
			switch (getIntention())
			{
				case AI_INTENTION_ATTACK:
					thinkAttack(false);
					break;
				case AI_INTENTION_CAST:
					thinkCast(false);
					break;
				case AI_INTENTION_FOLLOW:
					thinkFollow();
					break;
				default:
					onEvtThink();
					break;
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Exception onEvtArrivedTarget(): ", e);
		}
	}

	@Override
	protected void onEvtThink()
	{
		final L2Playable actor = getActor();
		if(actor == null || actor.isActionsDisabled())
			return;

		try
		{
			if(thinking++ > 1)
				return;

			switch (getIntention())
			{
				case AI_INTENTION_ACTIVE:
					thinkActive();
					break;
				case AI_INTENTION_ATTACK:
					thinkAttack(true);
					break;
				case AI_INTENTION_CAST:
					thinkCast(true);
					break;
				case AI_INTENTION_PICK_UP:
					thinkPickUp();
					break;
				case AI_INTENTION_INTERACT:
					thinkInteract();
					break;
				case AI_INTENTION_FOLLOW:
					thinkFollow();
					break;
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Exception onEvtThink(): ", e);
		}
		finally
		{
			thinking--;
		}
	}

	protected void thinkActive()
	{

	}

	protected void thinkFollow()
	{
		final L2Playable actor = getActor();

		final L2Character target = (L2Character) intentionArg0;
		final Integer offset = (Integer) intentionArg1;

		// Находимся слишком далеко цели, либо цель не пригодна для следования
		if(target == null || target.isAlikeDead() || actor.getDistance(target) > 4000 || offset == null)
		{
			clientActionFailed();
			return;
		}

		// Уже следуем за этой целью
		if(actor.isFollow && actor.getFollowTarget() == target)
		{
			clientActionFailed();
			return;
		}

		// Находимся достаточно близко или не можем двигаться - побежим потом ?
		if(actor.isInRange(target, offset + 20) || actor.isMovementDisabled())
			clientActionFailed();

		if(_followTask != null)
		{
			_followTask.cancel(false);
			_followTask = null;
		}
		_followTask = L2GameThreadPools.getInstance().scheduleMove(new ThinkFollow(), FOLLOW_INTERVAL);
	}

	@Override
	protected void onIntentionInteract(final L2Object object)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		if(actor.isActionsDisabled())
		{
			setNextAction(nextAction.INTERACT, object, null, false, false);
			clientActionFailed();
			return;
		}

		clearNextAction();
		changeIntention(AI_INTENTION_INTERACT, object, null);
		onEvtThink();
	}

	protected void thinkInteract()
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		final L2Object target = (L2Object) intentionArg0;

		if(target == null)
		{
			setIntention(AI_INTENTION_ACTIVE);
			return;
		}

		final int range = (int) (Math.max(30, actor.getMinDistance(target)) + 20);

		if(actor.isInRangeZ(target, range))
		{
			if(actor.isPlayer())
				actor.getPlayer().doInteract(target);
			setIntention(AI_INTENTION_IDLE);
		}
		else
		{
			actor.moveToLocation(target.getLoc(), 10, true);
			setNextAction(nextAction.INTERACT, target, null, false, false);
		}
	}

	@Override
	protected void onIntentionPickUp(final L2Object object)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		if(actor.isActionsDisabled())
		{
			setNextAction(nextAction.PICKUP, object, null, false, false);
			clientActionFailed();
			return;
		}

		clearNextAction();
		changeIntention(AI_INTENTION_PICK_UP, object, null);
		onEvtThink();
	}

	protected void thinkPickUp()
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		final L2Object target = (L2Object) intentionArg0;
		if(target == null)
		{
			setIntention(AI_INTENTION_ACTIVE);
			return;
		}

		if(actor.isInRange(target, 30) && Math.abs(actor.getZ() - target.getZ()) < 50)
		{
			if(actor.isPlayer() || actor.isPet())
				actor.doPickupItem(target);
			setIntention(AI_INTENTION_ACTIVE);
		}
		else
			L2GameThreadPools.getInstance().executeGeneral(new ThinkPickUp(actor, target));
	}

	protected void thinkAttack(final boolean checkRange)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		final L2Player player = actor.getPlayer();
		if(player == null)
		{
			onAttackFail();
			return;
		}

		if(actor.isActionsDisabled() || actor.isAttackingDisabled())
		{
			actor.sendActionFailed();
			return;
		}

		final boolean isPosessed = actor instanceof L2Summon && ((L2Summon) actor).isPosessed();

		final L2Character attack_target = getAttackTarget();
		if(attack_target == null || attack_target.isDead() || !isPosessed && !(_forceUse ? attack_target.isAttackable(player) : attack_target.isAutoAttackable(player)))
		{
			onAttackFail();
			actor.sendActionFailed();
			return;
		}

		if(!checkRange)
		{
			clientStopMoving();
			actor.doAttack(attack_target);
			return;
		}

		int range = actor.getPhysicalAttackRange();
		if(range < 10)
			range = 10;

		final boolean canSee = GeoEngine.canSeeTarget(actor, attack_target, false);
		if(!canSee && (range > 200 || Math.abs(actor.getZ() - attack_target.getZ()) > 200))
		{
			actor.sendPacket(Msg.CANNOT_SEE_TARGET);
			onAttackFail();
			actor.sendActionFailed();
			return;
		}

		range += actor.getMinDistance(attack_target);

		if(actor.isFakeDeath())
			actor.breakFakeDeath();

		if(actor.isInRangeZ(attack_target, range))
		{
			if(!canSee)
			{
				actor.sendPacket(Msg.CANNOT_SEE_TARGET);
				onAttackFail();
				actor.sendActionFailed();
				return;
			}

			clientStopMoving(false);
			actor.doAttack(attack_target);
		}
		else if(!_dontMove)
			L2GameThreadPools.getInstance().executeGeneral(new ExecuteFollow(actor, attack_target, range - 20));
		else
			actor.sendActionFailed();
	}

	/**
	 * Нужен для оверрайда саммоном, чтобы он не прекращал фоллов.
	 */
	protected void onAttackFail()
	{
		setIntention(AI_INTENTION_ACTIVE);
	}

	protected void thinkCast(final boolean checkRange)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		final L2Character attack_target = getAttackTarget();

		if(_skill.getSkillType() == SkillType.CRAFT || _skill.isToggle())
		{
			if(_skill.checkCondition(actor, attack_target, _forceUse, _dontMove, true))
				actor.doCast(_skill, attack_target, _forceUse);
			return;
		}

		if(attack_target == null || attack_target.isDead() != _skill.isCorpse() && !_skill.isNotTargetAoE())
		{
			setIntention(AI_INTENTION_ACTIVE);
			actor.sendActionFailed();
			return;
		}

		if(!checkRange)
		{
			// Если скилл имеет следующее действие, назначим это действие после окончания действия скилла
			if(!actor.equals(attack_target) && (_skill.getNextAction() == NextAction.ATTACK || _skill.isOffensive() && actor instanceof L2Summon) && !_forceUse)
				setNextAction(nextAction.ATTACK, attack_target, null, _forceUse, false);
			else
				clearNextAction();

			if(!_skill.isToggle())
				clientStopMoving();

			if(_skill.checkCondition(actor, attack_target, _forceUse, _dontMove, true))
			{
				actor.doCast(_skill, attack_target, _forceUse);
			}
			else
			{
				setNextIntention();
				if(getIntention() == CtrlIntention.AI_INTENTION_ATTACK)
					thinkAttack(true);
			}

			return;
		}

		int range = actor.getMagicalAttackRange(_skill);
		if(range < 10)
			range = 10;

		final boolean canSee = _skill.getSkillType() == SkillType.TAKECASTLE || _skill.getSkillType() == SkillType.TAKEFORT || GeoEngine.canSeeTarget(actor, attack_target, actor.isFlying());
		final boolean noRangeSkill = _skill.getCastRange() == Short.MAX_VALUE;

		// фикс каста с площядок для виверн у замки :Gludio, Oren, Giran, Dion, Innadril
		if((_skill.getSkillType() == SkillType.TAKECASTLE || _skill.getSkillType() == SkillType.TAKEFORT) && Math.abs(actor.getZ() - attack_target.getZ()) > 200)
		{
			actor.sendPacket(Msg.CANNOT_SEE_TARGET);
			setIntention(AI_INTENTION_ACTIVE);
			actor.sendActionFailed();
			return;
		}

		if(!noRangeSkill && !canSee && (range > 200 || Math.abs(actor.getZ() - attack_target.getZ()) > 200))
		{
			actor.sendPacket(Msg.CANNOT_SEE_TARGET);
			setIntention(AI_INTENTION_ACTIVE);
			actor.sendActionFailed();
			return;
		}

		range += actor.getMinDistance(attack_target);
		if(actor.isFakeDeath())
			actor.breakFakeDeath();

		if(actor.isInRangeZ(attack_target, range) || noRangeSkill)
		{
			if(!noRangeSkill && !canSee)
			{
				actor.sendPacket(Msg.CANNOT_SEE_TARGET);
				setIntention(AI_INTENTION_ACTIVE);
				actor.sendActionFailed();
				return;
			}

			// Если скилл имеет следующее действие, назначим это действие после окончания действия скилла
			if(!actor.equals(attack_target) && (_skill.getNextAction() == NextAction.ATTACK || _skill.isOffensive() && actor instanceof L2Summon) && !_forceUse)
				setNextAction(nextAction.ATTACK, attack_target, null, _forceUse, false);
			else
				clearNextAction();

			// clientStopMoving();

			if(_skill.checkCondition(actor, attack_target, _forceUse, _dontMove, true))
			{
				clientStopMoving(false);
				actor.doCast(_skill, attack_target, _forceUse);
			}
			else
			{
				setNextIntention();
				if(getIntention() == CtrlIntention.AI_INTENTION_ATTACK)
					thinkAttack(true);
			}
		}
		else if(!_dontMove)
			L2GameThreadPools.getInstance().executeGeneral(new ExecuteFollow(actor, attack_target, range - 20));
		else
		{
			actor.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
			setIntention(AI_INTENTION_ACTIVE);
			actor.sendActionFailed();
		}
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		clearNextAction();
		final L2Playable actor = getActor();
		if(actor != null)
			actor.clearHateList(true);

		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtFakeDeath()
	{
		clearNextAction();
		super.onEvtFakeDeath();
	}

	public void lockTarget(final L2Character target)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		if(target == null)
			actor.setAggressionTarget(null);
		else if(actor.getAggressionTarget() == null)
		{
			final L2Object actorStoredTarget = actor.getTarget();
			actor.setAggressionTarget(target);
			actor.setTarget(target);

			clearNextAction();
			switch (getIntention())
			{
				case AI_INTENTION_ATTACK:
					setAttackTarget(target);
					break;
				case AI_INTENTION_CAST:
					L2Skill skill = actor.getCastingSkill();
					if(skill == null)
						skill = _skill;
					if(skill != null && !skill.isUsingWhileCasting())
						switch (skill.getTargetType())
						{
							case TARGET_ONE:
							case TARGET_AREA:
							case TARGET_MULTIFACE:
							case TARGET_TUNNEL:
								setAttackTarget(target);
								actor.setCastingTarget(target);
								break;
						}
					break;
			}

			if(actorStoredTarget != target)
				actor.sendPacket(new MyTargetSelected(target.getObjectId(), 0));
		}
	}

	@Override
	public void Attack(final L2Object target, final boolean forceUse, final boolean dontMove)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		// Если не можем атаковать, то атаковать позже
		if(target.isCharacter() && (actor.isActionsDisabled() || actor.isAttackingDisabled()))
		{
			// Если не можем атаковать, то атаковать позже
			setNextAction(nextAction.ATTACK, target, null, forceUse, false);
			actor.sendActionFailed();
			return;
		}

		_dontMove = dontMove;
		_forceUse = forceUse;
		clearNextAction();
		setIntention(AI_INTENTION_ATTACK, target);
	}

	@Override
	public void Cast(final L2Skill skill, final L2Character target, final boolean forceUse, final boolean dontMove)
	{
		final L2Playable actor = getActor();
		if(actor == null)
			return;

		// Если скилл альтернативного типа (например, бутылка на хп),
		// то он может использоваться во время каста других скиллов, или во время атаки, или на бегу.
		// Поэтому пропускаем дополнительные проверки.
		if(skill.isAltUse() || skill.isToggle())
		{
			if(skill.isToggle() && actor.isToggleDisabled() || skill.isHandler() && actor.isPotionsDisabled())
				clientActionFailed();
			else
				actor.altUseSkill(skill, target);
			return;
		}

		// Если не можем кастовать, то использовать скилл позже
		if(actor.isActionsDisabled())
		{
			// if(!actor.isSkillDisabled(skill.getId()))
			setNextAction(nextAction.CAST, skill, target, forceUse, dontMove);
			clientActionFailed();
			return;
		}

		// _actor.stopMove(null);
		_forceUse = forceUse;
		_dontMove = dontMove;
		clearNextAction();
		setIntention(CtrlIntention.AI_INTENTION_CAST, skill, target);
	}

	private class ThinkPickUp implements Runnable
	{
		private final L2Playable _actor;
		private final L2Object _target;

		private ThinkPickUp(final L2Playable actor, final L2Object target)
		{
			_actor = actor;
			_target = target;
		}

		@Override
		public void run()
		{
			_actor.moveToLocation(_target.getLoc(), 10, true);
			setNextAction(nextAction.PICKUP, _target, null, false, false);
		}
	}

	private class ExecuteFollow implements Runnable
	{
		private final L2Playable _player;
		private final L2Character _target;
		private final int _range;

		private ExecuteFollow(final L2Playable player, final L2Character target, final int range)
		{
			_player = player;
			_target = target;
			_range = range;
		}

		@Override
		public void run()
		{
			if(!_target.isInvisible())
				_player.followToCharacter(_target, _range, true);
		}
	}

	private class ThinkFollow implements Runnable
	{
		private L2Playable getActor()
		{
			return L2PlayableAI.this.getActor();
		}

		@Override
		public void run()
		{
			_followTask = null;
			final L2Playable actor = getActor();
			if(actor == null)
				return;

			if(getIntention() != AI_INTENTION_FOLLOW)
			{
				// Если пет прекратил преследование, меняем статус, чтобы не пришлось щелкать на кнопку следования 2 раза.
				if((actor.isPet() || actor.isSummon()) && getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
					actor.setFollowStatus(false, false);
				return;
			}

			final L2Character target = (L2Character) intentionArg0;
			final Integer offset = (Integer) intentionArg1;
			if(target == null || target.isAlikeDead() || actor.getDistance(target) > 4000)
			{
				setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				return;
			}
			final L2Player actorPlayer = actor.getPlayer();
			if(actorPlayer == null || !actorPlayer.isConnected() || (actor.isPet() || actor.isSummon()) && actorPlayer.getPet() != actor)
			{
				setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				return;
			}
			if(!actor.isInRange(target, offset + 20) && (!actor.isFollow || actor.getFollowTarget() != target))
				actor.followToCharacter(target, offset, false);
			_followTask = L2GameThreadPools.getInstance().scheduleMove(this, FOLLOW_INTERVAL);
		}
	}

	@Override
	public L2Playable getActor()
	{
		return (L2Playable) super.getActor();
	}

	public boolean isForceCast()
	{
		return _forceUse;
	}
}
