package l2n.game.ai.model;

import l2n.Config;
import l2n.game.ai.DefaultAI;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class AIThreadController
{
	private static final Logger _log = Logger.getLogger(AIThreadController.class.getName());

	private final class AiThread extends Thread
	{
		public AiThread()
		{
			super("AiThread");
			setDaemon(true);
		}

		@Override
		public void run()
		{
			while (true)
			{
				try
				{
					Thread.sleep(Config.ALT_AI_ENGINE_THREAD_SLEEP);
				}
				catch(final Exception e)
				{}

				try
				{
					// skip if queue is empty
					if(_queue.isEmpty())
						continue;

					final long currentTime = System.currentTimeMillis();

					DefaultAI ai = null;
					final int max = _queue.size();
					for(int i = 0; i < max; i++)
					{
						try
						{
							ai = _queue.get(i);
						}
						catch(IndexOutOfBoundsException e)
						{
							// fuck)
							continue;
						}
						if(ai == null)
							continue;

						// если время запуска пришло
						if(ai.mayFireNow(currentTime))
							ai.run(); // MemoryArea.getMemoryArea(this).executeInArea(ai);
					}
				}
				catch(final Exception e)
				{
					_log.log(Level.SEVERE, "AiThread get error " + e.getMessage(), e);
				}
			}
		}
	}

	/** The L2NpcInstance AI task executed every 1s (call onEvtThink method) */
	protected final CopyOnWriteArrayList<DefaultAI> _queue;

	private final AiThread _aiThread;

	public static AIThreadController getInstance()
	{
		return SingletonHolder._instance;
	}

	private AIThreadController()
	{
		_queue = new CopyOnWriteArrayList<DefaultAI>();

		// start ai thread daemon
		_aiThread = new AiThread();
		_aiThread.start();
		_log.info("AIThreadController: AiThread started.");
	}

	public void put(final DefaultAI ai)
	{
		_queue.add(ai);
	}

	public void remove(final DefaultAI ai)
	{
		_queue.remove(ai);
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final AIThreadController _instance = new AIThreadController();
	}
}
