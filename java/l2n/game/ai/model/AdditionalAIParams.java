package l2n.game.ai.model;

import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AdditionalAIParams
{
	private static final Logger _log = Logger.getLogger(AdditionalAIParams.class.getName());

	public static enum AiOptionsType
	{
		SEARCHING_MASTER("searchingMaster"),
		IS_SILENT_MOVE_VISIBLE("canSeeInSilentMove"),
		NO_RANDOM_WALK("noRandomWalk"),
		IS_MADNESS("isMadness"),
		TRANSFORM_ON_UNDERATTACK("transformOnUnderAttack"),
		TRANSFORM_ON_DEAD("transformOnDead"),
		TRANSFORM_CHANCE("transformChance"),
		SELF_AGGRESSIVE("SelfAggressive"),
		MAX_PURSUE_RANGE("MaxPursueRange"),
		FACTION_NOTIFY_INTERVAL("FactionNotifyInterval"),
		IS_FLYING("isFlying"),
		RANDOM_ANIMATION_DISABLED("randomAnimationDisabled"),
		ON_ACTION_DISABLED("onActionDisabled"),
		CHAT_WINDOW_DISABLED("chatWindowDisabled"),
		PARALIZE_ON_ATTACK("ParalizeOnAttack"),
		SPAWN_ANIMATION_DISABLED("spawnAnimationDisabled"),
		IS_LETHAL_IMMUNE("lethalImmune"),
		OPTIONS("Options");

		private final String paramName;

		private AiOptionsType(final String name)
		{
			paramName = name;
		}

		public final String getName()
		{
			return paramName;
		}

		public static AiOptionsType getByName(final String name)
		{
			for(final AiOptionsType param : values())
				if(param.getName().equals(name))
					return param;

			throw new NoSuchElementException("ParamType not found for name: '" + name + "'.\n Please check " + AiOptionsType.class.getCanonicalName());
		}
	}

	public static final class EmptyAdditionalAIParams extends AdditionalAIParams
	{
		public EmptyAdditionalAIParams()
		{}

		@Override
		public boolean isEmpty()
		{
			return true;
		}

		@Override
		public boolean getBool(final AiOptionsType type, final boolean def)
		{
			return def;
		}

		@Override
		public int getInt(final AiOptionsType type, final int def)
		{
			return def;
		}

		@Override
		public long getLong(final AiOptionsType type, final long def)
		{
			return def;
		}

		@Override
		public float getFloat(final AiOptionsType type, final float def)
		{
			return def;
		}

		@Override
		public String getString(final AiOptionsType type, final String def)
		{
			return def;
		}

		@Override
		public void addOption(final int npcId, final AiOptionsType type, final String value)
		{
			throw new UnsupportedOperationException();
		}
	}

	private final Object[] params;


	public AdditionalAIParams()
	{
		params = new Object[AiOptionsType.values().length];
	}

	public boolean isEmpty()
	{
		return false;
	}

	public boolean getBool(final AiOptionsType type, final boolean def)
	{
		final Object val = params[type.ordinal()];
		if(val == null)
			return def;
		if(val instanceof Boolean)
			return ((Boolean) val).booleanValue();
		try
		{
			params[type.ordinal()] = Boolean.parseBoolean((String) val);
			return (Boolean) params[type.ordinal()];
		}
		catch(final Exception e)
		{
			throw new IllegalArgumentException("AIParams: boolean value required, but found: " + val);
		}
	}

	public int getInt(final AiOptionsType type, final int def)
	{
		final Object val = params[type.ordinal()];
		if(val == null)
			return def;
		if(val instanceof Number)
			return ((Number) val).intValue();
		try
		{
			params[type.ordinal()] = Integer.parseInt((String) val);
			return (Integer) params[type.ordinal()];
		}
		catch(final Exception e)
		{
			throw new IllegalArgumentException("AIParams: Integer value required, but found: " + val);
		}
	}

	public long getLong(final AiOptionsType type, final long def)
	{
		final Object val = params[type.ordinal()];
		if(val == null)
			return def;
		if(val instanceof Number)
			return ((Number) val).longValue();
		try
		{
			params[type.ordinal()] = Long.parseLong((String) val);
			return (Long) params[type.ordinal()];
		}
		catch(final Exception e)
		{
			throw new IllegalArgumentException("AIParams: Long value required, but found: " + val);
		}
	}

	public float getFloat(final AiOptionsType type, final float def)
	{
		final Object val = params[type.ordinal()];
		if(val == null)
			return def;
		if(val instanceof Number)
			return ((Number) val).floatValue();
		try
		{
			params[type.ordinal()] = Float.parseFloat((String) val);
			return (Float) params[type.ordinal()];
		}
		catch(final Exception e)
		{
			throw new IllegalArgumentException("AIParams: Float value required, but found: " + val);
		}
	}

	public String getString(final AiOptionsType type, final String def)
	{
		final Object val = params[type.ordinal()];
		if(val == null)
			return def;
		return String.valueOf(val);
	}

	public void addOption(final int npcId, final AiOptionsType type, final String value)
	{
		if(params[type.ordinal()] != null)
			_log.log(Level.WARNING, "AIParams: param[" + type.getName() + "] replace new value[" + value + "] for npcId " + npcId);
		params[type.ordinal()] = value;
	}
}
