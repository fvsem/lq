package l2n.game.ai.model;

public interface Trigger
{
	static final long NO_FIRE_MORE = Long.MAX_VALUE;

	/**
	 * Возвращает следующее время (in milliseconds) когда <code>Trigger</code> должен будет запущен. Если триггер не должен будет запуститься ещё, то вернёт <code>NO_FIRE_MORE</code>.
	 */
	public long getNextFireTime();

	/**
	 * Устанавливает время следующего запуска триггера
	 *
	 * @param nextFireTime
	 *            (in milliseconds)
	 */
	public void setNextFireTime(long nextFireTime);

	/**
	 * Возвращает временной интервал (in milliseconds) в котором триггер должен повториться.
	 */
	public long getRepeatInterval();

	/**
	 * Проверяет возможен ли запуск триггера сейчас
	 *
	 * @return <code>true</code> если триггера можно запустить сейчас
	 */
	public boolean mayFireNow(long currentTime);

	/**
	 * Обновляет параметры после запуска триггера
	 */
	public void updateAfterFire(long currentTime);
}
