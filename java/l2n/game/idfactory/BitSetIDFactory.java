package l2n.game.idfactory;

import l2n.commons.math.PrimeFinder;
import l2n.game.L2GameThreadPools;

import java.util.BitSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BitSetIDFactory extends IdFactory
{
	private static final Logger _log = Logger.getLogger(BitSetIDFactory.class.getName());

	private BitSet freeIds;
	private AtomicInteger freeIdCount;
	private AtomicInteger nextFreeId;

	private class BitSetCapacityCheck implements Runnable
	{
		@Override
		public void run()
		{
			if(reachingBitSetCapacity())
				increaseBitSetCapacity();
		}
	}

	protected BitSetIDFactory()
	{
		super();
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new BitSetCapacityCheck(), 30000, 30000);

		initialize();
		_log.info("IdFactory: " + freeIds.size() + " id's available.");
	}

	private synchronized void initialize()
	{
		try
		{
			freeIds = new BitSet(PrimeFinder.nextPrime(100000));
			freeIds.clear();
			freeIdCount = new AtomicInteger(FREE_OBJECT_ID_SIZE);

			for(int usedObjectId : extractUsedObjectIDTable())
			{
				int objectID = usedObjectId - FIRST_OID;
				if(objectID < 0)
				{
					_log.warning("Object ID " + usedObjectId + " in DB is less than minimum ID of " + FIRST_OID);
					continue;
				}
				freeIds.set(usedObjectId - FIRST_OID);
				freeIdCount.decrementAndGet();
			}

			nextFreeId = new AtomicInteger(freeIds.nextClearBit(0));
			initialized = true;
		}
		catch(final Exception e)
		{
			initialized = false;
			_log.log(Level.SEVERE, "BitSet ID Factory could not be initialized correctly", e);
		}
	}

	@Override
	public synchronized void releaseId(int objectID)
	{
		if(objectID - FIRST_OID > -1)
		{
			freeIds.clear(objectID - FIRST_OID);
			freeIdCount.incrementAndGet();
			super.releaseId(objectID);
		}
		else
			_log.warning("BitSet ID Factory: release objectID " + objectID + " failed (< " + FIRST_OID + ")");
	}

	@Override
	public synchronized int getNextId()
	{
		int newID = nextFreeId.get();
		freeIds.set(newID);
		freeIdCount.decrementAndGet();

		int nextFree = freeIds.nextClearBit(newID);

		if(nextFree < 0)
			nextFree = freeIds.nextClearBit(0);
		if(nextFree < 0)
			if(freeIds.size() < FREE_OBJECT_ID_SIZE)
				increaseBitSetCapacity();
			else
				throw new NullPointerException("Ran out of valid Id's.");

		nextFreeId.set(nextFree);

		return newID + FIRST_OID;
	}

	@Override
	public synchronized int size()
	{
		return freeIdCount.get();
	}

	protected synchronized int usedIdCount()
	{
		return size() - FIRST_OID;
	}

	private synchronized boolean reachingBitSetCapacity()
	{
		return PrimeFinder.nextPrime(usedIdCount() * 11 / 10) > freeIds.size();
	}

	private synchronized void increaseBitSetCapacity()
	{
		BitSet newBitSet = new BitSet(PrimeFinder.nextPrime(usedIdCount() * 11 / 10));
		newBitSet.or(freeIds);
		freeIds = newBitSet;
	}
}
