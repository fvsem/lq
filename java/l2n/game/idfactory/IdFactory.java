package l2n.game.idfactory;

import l2n.Server;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.idfactory.Tasks.ClearQuery;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

public abstract class IdFactory
{
	private static final Logger _log = Logger.getLogger(IdFactory.class.getName());

	protected boolean initialized;

	protected long releasedCount = 0;

	public static final int FIRST_OID = 0x10000000;
	public static final int LAST_OID = 0x7FFFFFFF;
	public static final int FREE_OBJECT_ID_SIZE = LAST_OID - FIRST_OID;

	protected static final IdFactory _instance = new BitSetIDFactory();

	protected IdFactory()
	{
		setAllCharacterOffline();
		cleanUpDB();
	}

	private void setAllCharacterOffline()
	{
		ThreadConnection conn = null;
		FiltredStatement stmt = null;
		try
		{
			conn = L2DatabaseFactory.getInstance().getConnection();
			stmt = conn.createStatement();
			stmt.executeUpdate("UPDATE characters SET online = 0");
			stmt.executeUpdate("UPDATE characters SET accesslevel = 0 WHERE accesslevel = -1");
			_log.info("Clear characters online status and accesslevel.");
		}
		catch(SQLException e)
		{}
		finally
		{
			DbUtils.closeQuietly(conn, stmt);
		}
	}

	private void cleanUpDB()
	{
		for(ClearQuery q : ClearQuery.values())
			q.run();
		_log.info("Total cleaned: " + ClearQuery.totalDeleted + ", updated: " + ClearQuery.totalUpdated + " elements in database.");
	}

	protected int[] extractUsedObjectIDTable() throws SQLException
	{
		ThreadConnection con = null;
		FiltredStatement s = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			s = con.createStatement();

			String query = "SELECT " + Tasks.objTables[0][1] + ", 0 AS i FROM " + Tasks.objTables[0][0];
			for(int i = 1; i < Tasks.objTables.length; i++)
				query += " UNION SELECT " + Tasks.objTables[i][1] + ", " + i + " FROM " + Tasks.objTables[i][0];

			rs = s.executeQuery("SELECT COUNT(*), COUNT(DISTINCT " + Tasks.objTables[0][1] + ") FROM ( " + query + " ) AS all_ids");
			if(!rs.next())
				throw new Exception("IdFactory: can't extract count ids");
			if(rs.getInt(1) != rs.getInt(2))
				throw new Exception("IdFactory: there are duplicates in object ids");

			int[] result = new int[rs.getInt(1)];
			DbUtils.closeQuietly(rs);
			// _log.info("IdFactory: Extracting " + result.length + " used id's from data tables...");

			rs = s.executeQuery(query);
			int idx = 0;
			while (rs.next())
				result[idx++] = rs.getInt(1);

			_log.info("IdFactory: Extracted " + idx + " used id's from data tables.");
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Server.exit(0, "IdFactory");
			return null;
		}
		finally
		{
			DbUtils.closeQuietly(con, s, rs);
		}
	}

	public boolean isInitialized()
	{
		return initialized;
	}

	public static IdFactory getInstance()
	{
		return _instance;
	}

	public abstract int getNextId();

	public void releaseId(int id)
	{
		releasedCount++;
	}

	public long getReleasedCount()
	{
		return releasedCount;
	}

	public abstract int size();
}
