package l2n.game.idfactory;

import l2n.Config;
import l2n.Server;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;

import java.sql.ResultSet;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tasks
{
	private final static Logger _log = Logger.getLogger(IdFactory.class.getName());

	public static final String[][] objTables = {
			{ "characters", "obj_id" },
			{ "items", "object_id" },
			{ "clan_data", "clan_id" },
			{ "ally_data", "ally_id" },
			{ "pets", "objId" },
			{ "couples", "id" } };

	public static enum ClearQuery
			implements
			Runnable
	{
		// игроки
		couples("DELETE FROM couples WHERE couples.player1Id NOT IN (SELECT obj_Id FROM characters) OR couples.player2Id NOT IN (SELECT obj_Id FROM characters);"),
		character_variables("DELETE FROM character_variables WHERE character_variables.obj_id = '0';"),
		character_friends("DELETE FROM character_friends WHERE character_friends.char_id NOT IN (SELECT obj_Id FROM characters) OR character_friends.friend_id NOT IN (SELECT obj_Id FROM characters);"),
		character_blocklist("DELETE FROM character_blocklist WHERE character_blocklist.obj_Id NOT IN (SELECT obj_Id FROM characters) OR character_blocklist.target_Id NOT IN (SELECT obj_Id FROM characters);"),
		character_hennas("DELETE FROM character_hennas WHERE character_hennas.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_macroses("DELETE FROM character_macroses WHERE character_macroses.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_quests("DELETE FROM character_quests WHERE character_quests.char_id NOT IN (SELECT obj_Id FROM characters);"),
		character_shortcuts("DELETE FROM character_shortcuts WHERE character_shortcuts.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_effects_save("DELETE FROM character_effects_save WHERE character_effects_save.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		summon_effects_save("DELETE FROM summon_effects_save WHERE summon_effects_save.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_skills_save("DELETE FROM character_skills_save WHERE character_skills_save.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_buff_schemes("DELETE FROM character_buff_schemes WHERE character_buff_schemes.char_obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_recipebook("DELETE FROM character_recipebook WHERE character_recipebook.char_id NOT IN (SELECT obj_Id FROM characters);", true),
		character_skills("DELETE FROM character_skills WHERE character_skills.char_obj_id NOT IN (SELECT obj_Id FROM characters);", true),
		character_subclasses("DELETE FROM character_subclasses WHERE character_subclasses.char_obj_id NOT IN (SELECT obj_Id FROM characters);", true),
		character_mail("DELETE FROM character_mail WHERE character_mail.obj_id NOT IN (SELECT obj_Id FROM characters);"),
		character_tpbookmark("DELETE FROM character_tpbookmark WHERE character_tpbookmark.char_id NOT IN (SELECT obj_Id FROM characters);"),
		// коммьюнити
		community_board_teleport("DELETE FROM community_board_teleport WHERE community_board_teleport.charId NOT IN (SELECT obj_Id FROM characters);"),
		community_buffs_group("DELETE FROM community_buffs_group WHERE community_buffs_group.objectId NOT IN (SELECT obj_Id FROM characters);"),
		community_buffs_group_skills("DELETE FROM community_buffs_group_skills WHERE community_buffs_group_skills.group_id NOT IN (SELECT groupId FROM community_buffs_group);", false, community_buffs_group),
		// олимпиада
		heroes("DELETE FROM heroes WHERE heroes.char_id NOT IN (SELECT obj_Id FROM characters);", true),
		olympiad_nobles("DELETE FROM olympiad_nobles WHERE olympiad_nobles.char_id NOT IN (SELECT obj_Id FROM characters);", true),
		// Кланы и Альянсы
		clan_data("DELETE FROM clan_data WHERE clan_data.leader_id NOT IN (SELECT obj_Id FROM characters);", true),
		clan_notices("DELETE FROM clan_notices WHERE clan_notices.clanID NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		clan_privs("DELETE FROM clan_privs WHERE clan_privs.clan_id NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		clan_skills("DELETE FROM clan_skills WHERE clan_skills.clan_id NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		clan_subpledges("DELETE FROM clan_subpledges WHERE clan_subpledges.clan_id NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		clan_wars("DELETE FROM clan_wars where clan1 not in (select clan_id FROM clan_data) or clan2 not in (select clan_id FROM clan_data);", false, clan_data),
		ally_data("DELETE FROM ally_data WHERE (ally_data.leader_id NOT IN (SELECT clan_id FROM clan_data)) OR (ally_id NOT IN (SELECT ally_id FROM clan_data));", false, clan_data),
		siege_clans("DELETE FROM siege_clans WHERE siege_clans.clan_id NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		siege_territory_members("DELETE FROM siege_territory_members WHERE siege_territory_members.obj_Id NOT IN (SELECT clan_id FROM clan_data) AND type = 1;", false, clan_data),
		update_characters("UPDATE characters SET clanid=0,pledge_type=0,pledge_rank=0,lvl_joined_academy=0,apprentice=0 WHERE clanid!=0 AND clanid NOT IN (SELECT clan_id FROM clan_data);", false, clan_data),
		update_clan_data("UPDATE clan_data SET ally_id=0 WHERE ally_id!=0 AND ally_id NOT IN (SELECT ally_id FROM ally_data);", false, ally_data),
		// предметы
		items(Config.HARD_DB_CLEANUP_ON_START ? "DELETE FROM items WHERE (count = 0) OR (owner_id NOT IN (SELECT obj_Id FROM characters) AND owner_id NOT IN (SELECT clan_id FROM clan_data) AND owner_id NOT IN (SELECT objId FROM pets));" : "DELETE FROM items WHERE count = 0;", false, Config.HARD_DB_CLEANUP_ON_START ? clan_data : null),
		pets("DELETE FROM pets WHERE pets.item_obj_id NOT IN (SELECT object_id FROM items);", true, items),
		augmentations("DELETE FROM `augmentations` WHERE `item_id` NOT IN (SELECT object_id FROM items);", false, items),
		// разное
		bonus("DELETE FROM bonus WHERE bonus.login NOT IN (SELECT account_name FROM characters);"),
		
		update_characters_clan("UPDATE `characters` SET `leaveclan` = 0, `deleteclan` = 0, `pledge_type` = 0, `pledge_rank` = 0, `apprentice` = 0, `clanid` = 0 WHERE `clanid` > 0 AND `clanid` NOT IN (SELECT `clan_id` FROM `clan_data`);");

		public static int totalDeleted = 0, totalUpdated = 0;
		private static ReentrantLock totalLock = new ReentrantLock();
		private final ClearQuery _parent;
		public final String _query, _table;
		public final boolean _hard, _update;
		public boolean compleated;

		private ClearQuery(String query, boolean hard, ClearQuery parent)
		{
			compleated = false;
			_query = query;
			_hard = hard;
			_parent = parent;
			_update = name().startsWith("update_");
			_table = _update ? name().replaceFirst("update_", "") : name();
		}

		private ClearQuery(String query, boolean hard)
		{
			this(query, hard, null);
		}

		private ClearQuery(String query)
		{
			this(query, false);
		}

		@Override
		public void run()
		{
			ThreadConnection con = null;
			FiltredStatement s = null;
			ResultSet rs = null;

			try
			{
				if(!Config.HARD_DB_CLEANUP_ON_START && _hard)
					return;
				if(_parent != null)
				{
					while (!_parent.compleated)
						synchronized (_parent)
						{
							_parent.wait();
						}
				}
				con = L2DatabaseFactory.getInstance().getConnection();
				s = con.createStatement();
				int currCount = s.executeUpdate(_query);
				if(currCount > 0)
				{
					totalLock.lock();
					if(_update)
						totalUpdated += currCount;
					else
						totalDeleted += currCount;
					totalLock.unlock();
					if(_update)
						_log.info("Updated " + currCount + " elements in table " + _table + ".");
					else
						_log.info("Cleaned " + currCount + " elements from table " + _table + ".");
				}
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "", e);
				Server.exit(0, "IdFactory::DBCleaner::" + _query);
			}
			finally
			{
				DbUtils.closeQuietly(con, s, rs);
				compleated = true;
				synchronized (this)
				{
					notifyAll();
				}
			}
		}
	}

	public static class CountObjectIds implements Runnable
	{
		final String[] _objTable;
		final int[] _objCountPut;

		public CountObjectIds(String[] objTable, int[] objCountPut)
		{
			super();
			_objTable = objTable;
			_objCountPut = objCountPut;
		}

		@Override
		public void run()
		{
			ThreadConnection con = null;
			FiltredStatement s = null;
			ResultSet rs = null;

			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				s = con.createStatement();
				rs = s.executeQuery("SELECT COUNT(*) FROM " + _objTable[0]);
				if(!rs.next())
					throw new Exception("IdFactory: can't extract count ids :: " + _objTable[0]);
				_objCountPut[0] = rs.getInt(1);
				_log.info("IdFactory: Table " + _objTable[0] + " contains " + _objCountPut[0] + " rows");
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "", e);
				Server.exit(0, "IdFactory::CountObjectIds::" + _objTable[0]);
			}
			finally
			{
				DbUtils.closeQuietly(con, s, rs);
			}
		}
	}

	public static class ExtractObjectIds implements Runnable
	{
		final String[] _objTable;
		final int[] _resultArray;
		int startIdx;

		public ExtractObjectIds(String[] objTable, int[] objCount, int[] resultArray)
		{
			super();
			_objTable = objTable;
			_resultArray = resultArray;
			startIdx = objCount[1];
		}

		@Override
		public void run()
		{
			ThreadConnection con = null;
			FiltredStatement s = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				s = con.createStatement();
				rs = s.executeQuery("SELECT " + _objTable[1] + " FROM " + _objTable[0]);
				int idx = 0;
				while (rs.next())
					_resultArray[startIdx + idx++] = rs.getInt(1);
				_log.info("IdFactory: Extracted " + idx + " used id's from " + _objTable[0]);
			}
			catch(Exception e)
			{
				_log.log(Level.SEVERE, "", e);
				Server.exit(0, "IdFactory::ExtractObjectIds::" + _objTable[0] + "::" + startIdx);
			}
			finally
			{
				DbUtils.closeQuietly(con, s, rs);
			}
		}
	}
}
