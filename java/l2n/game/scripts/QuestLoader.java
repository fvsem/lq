package l2n.game.scripts;

import l2n.game.instancemanager.QuestManager;
import l2n.game.scripts.quests._726_LightWithinTheDarkness;
import l2n.game.scripts.quests._727_HopeWithinTheDarkness;

public class QuestLoader
{
	public static void load()
	{
		QuestManager.addQuest(new _726_LightWithinTheDarkness());
		QuestManager.addQuest(new _727_HopeWithinTheDarkness());
	}
}
