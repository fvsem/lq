package l2n.game.scripts.quests;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Party;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class _727_HopeWithinTheDarkness extends Quest
{
	private static final FastMap<Integer, CastlePailakaWorld> _worlds = new FastMap<Integer, CastlePailakaWorld>().shared();

	private final static int KnightsEpaulette = 9912;

	private final static long TIME_LIMIT = 4 * 60 * 60 * 1000;

	// raid boss
	public static final int invader_pole = 25653;
	public static final int invader_bow = 25654;
	public static final int invader_fist = 25655;
	// monster
	private static final int invader_a = 25656;
	private static final int invader_b = 25657;
	private static final int invader_c = 25658;

	// npc помошники
	private static final int knight = 36562;
	private static final int ranger = 36563;
	private static final int mage = 36564;
	private static final int warrior = 36565;

	private static final Location[] to_move = new Location[]
	{
			new Location(49243, -12552, -9388),
			new Location(49244, -12340, -9380),
			new Location(49241, -12134, -9381),
			new Location(49242, -11917, -9389),
			new Location(49376, -12461, -9392),
			new Location(49381, -12021, -9390),
			new Location(49505, -12407, -9392),
			new Location(49502, -12239, -9380),
			new Location(49506, -12054, -9390)
	};

	private static final String[] helper_message = new String[] { "I can't stand it anymore. Aah...", "Kyaaak!", "Gasp! How can this be?" };

	private static final class PailakaStages
	{
		private final Location[] stage1Spawn;
		private final Location[] stage2Spawn;
		private final Location[] stage3Spawn;

		private PailakaStages()
		{
			stage1Spawn = new Location[]
			{
					new Location(50943, -12224, -9321, 32768, invader_pole),
					new Location(50343, -12552, -9388, 32768, invader_a),
					new Location(50344, -12340, -9380, 32768, invader_a),
					new Location(50341, -12134, -9381, 32768, invader_a),
					new Location(50342, -11917, -9389, 32768, invader_a),
					new Location(50476, -12461, -9392, 32768, invader_a),
					new Location(50481, -12021, -9390, 32768, invader_a),
					new Location(50605, -12407, -9392, 32768, invader_a),
					new Location(50602, -12239, -9380, 32768, invader_a),
					new Location(50606, -12054, -9390, 32768, invader_a)
			};

			stage2Spawn = new Location[]
			{
					new Location(50943, -12224, -9321, 32768, invader_bow),
					new Location(50343, -12552, -9388, 32768, invader_b),
					new Location(50344, -12340, -9380, 32768, invader_b),
					new Location(50341, -12134, -9381, 32768, invader_b),
					new Location(50342, -11917, -9389, 32768, invader_b),
					new Location(50476, -12461, -9392, 32768, invader_b),
					new Location(50481, -12021, -9390, 32768, invader_b),
					new Location(50605, -12407, -9392, 32768, invader_b),
					new Location(50602, -12239, -9380, 32768, invader_b),
					new Location(50606, -12054, -9390, 32768, invader_b)
			};

			stage3Spawn = new Location[]
			{
					new Location(50943, -12004, -9321, 32768, invader_fist),
					new Location(50943, -12475, -9321, 32768, invader_fist),
					new Location(50343, -12552, -9388, 32768, invader_a),
					new Location(50344, -12340, -9380, 32768, invader_a),
					new Location(50341, -12134, -9381, 32768, invader_a),
					new Location(50342, -11917, -9389, 32768, invader_a),
					new Location(50476, -12461, -9392, 32768, invader_c),
					new Location(50481, -12021, -9390, 32768, invader_c),
					new Location(50605, -12407, -9392, 32768, invader_c),
					new Location(50602, -12239, -9380, 32768, invader_c),
					new Location(50606, -12054, -9390, 32768, invader_c)
			};
		}

		public Location[] getSpawnStage1()
		{
			return stage1Spawn;
		}

		public Location[] getSpawnStage2()
		{
			return stage2Spawn;
		}

		public Location[] getSpawnStage3()
		{
			return stage3Spawn;
		}
	}

	private static class CastlePailakaWorld
	{
		private class SpawnTask implements Runnable
		{
			@Override
			public void run()
			{
				changeStage();
			}
		}

		private final int _castleId;
		private final long _reflectionId;
		private final long _lastEnter;

		private int currentStage = 1;

		public CastlePailakaWorld(final int id, final Reflection r)
		{
			_reflectionId = r.getId();
			_castleId = id;
			_lastEnter = System.currentTimeMillis();
		}

		public void startSpawn(final long time)
		{
			L2GameThreadPools.getInstance().scheduleGeneral(new SpawnTask(), time);
		}

		private void spawnStage(final Location[] locs, final int offset)
		{
			L2NpcInstance npc;
			final int size = locs.length;
			for(int i = 0; i < size; i++)
			{
				npc = addSpawnToInstance(locs[i].id, locs[i], false, _reflectionId);
				if(npc.getNpcId() >= invader_a)
					npc.getAI().addTaskMove(to_move[i - offset]);
			}
		}

		public void changeStage()
		{
			switch (currentStage)
			{
				case 1:
				{
					spawnStage(stages.getSpawnStage1(), 1);
					currentStage++;

					// запускаем 2 волну через 8 минут
					startSpawn(8 * 60 * 1000);
					return;
				}
				case 2:
				{
					spawnStage(stages.getSpawnStage2(), 1);
					currentStage++;

					// запускаем 3 волну через 10 минут
					startSpawn(10 * 60 * 1000);
					return;
				}
				case 3:
				{
					spawnStage(stages.getSpawnStage3(), 2);
					currentStage++;
					return;
				}
				case 4:
				{
					currentStage++;
					return;
				}
				case 5: // finish
				{
					final Reflection r = ReflectionTable.getInstance().get(_reflectionId);
					if(r != null)
						r.startCollapseTimer(5 * 60 * 1000);

					final L2Party party = r.getParty();
					if(party != null)
						for(final L2Player member : party.getPartyMembers())
						{
							final QuestState qs = member.getQuestState("_727_HopeWithinTheDarkness");
							if(qs != null && qs.isStarted())
							{
								qs.setCond(3);
								qs.playSound(SOUND_MIDDLE);
								qs.getPlayer().sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(5));
							}
						}
					else
						for(final L2Player player : r.getPlayers())
						{
							final QuestState qs = player.getQuestState("_727_HopeWithinTheDarkness");
							if(qs != null && qs.isStarted())
							{
								qs.setCond(3);
								qs.playSound(SOUND_MIDDLE);
								qs.getPlayer().sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(5));
							}
						}

					for(final L2NpcInstance npc : r.getNpcs())
						if(npc.getNpcId() == knight)
						{
							Functions.npcShout(npc, "You've done it! We believed in you, warrior. We want to show our sincerity, though it is small. Please give me some of your time.");
							break;
						}
					break;
				}
			}
		}

		public long getReflectionId()
		{
			return _reflectionId;
		}

		public int getCastleId()
		{
			return _castleId;
		}

		public boolean isLocked()
		{
			return System.currentTimeMillis() - _lastEnter < TIME_LIMIT;
		}
	}

	private final static PailakaStages stages = new PailakaStages();

	/**
	 * <h2>Прохождение квеста</h2>
	 * <ol>
	 * <li><b>Underground Prison Manager</b> посылает вашу группу внутрь <b>Castle Pailaka</b>. Для этого каждый член группы должен взять этот квест. Ваше задание — спасти группу из 4 исследователей, которые там заперты. Внутри вас будут атаковать 3
	 * волны монстров, которые появляются через определенные интервалы времени. Поэтому вам нужно побыстрее убивать каждую группу монстров, пока не появилась следующая.</li>
	 * <li>Первая волна: <b>Kanadis Herald (81 ур.)</b>, несколько <b>Kanadis Fanatic (79 ур.)</b></li>
	 * <li>Вторая волна: <b>Kanadis Herald (2) (83 ур.)</b>, несколько <b>Kanadis Fanatic (2) (81 ур.)</b></li>
	 * <li>Третья волна: <b>Kanadis Herald (3) (85 ур.)</b>, несколько <b>Kanadis Fanatic (3) (83 ур.)</b></li>
	 * <li>Исследователи будут помогать вам убивать монстров, если ваша группа будет подводить их на достаточное для атаки расстояние.</li>
	 * <li>Как только все монстры будут убиты, исследователи поблагодарят вас и ваша группа покинет подземелье.</li>
	 * <li>Поговорите с <b>Underground Prison Manager</b> чтобы получить награду.</li>
	 * </ol>
	 */
	public _727_HopeWithinTheDarkness()
	{
		super(727, "_727_HopeWithinTheDarkness", null, PARTY_NONE);
		// Wardens
		addStartNpc(36403, 36404, 36405, 36406, 36407, 36408, 36409, 36410, 36411);
		addKillId(invader_pole, invader_bow, invader_fist, knight, ranger, mage, warrior);
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("quest_accept"))
		{
			st.setState(STARTED);
			st.setCond(1);
			st.playSound(SOUND_ACCEPT);
			return "prison_keeper_q0727_05.htm";
		}
		else if(event.equalsIgnoreCase("exit"))
		{
			// FIXME по идее квест отменять не надо
			st.exitCurrentQuest(true);
			return "prison_keeper_q0727_14.htm";
		}
		else if(event.equalsIgnoreCase("liveInstance"))
		{
			st.getPlayer().getReflection().startCollapseTimer(30 * 1000);
			return null;
		}
		else if(event.equalsIgnoreCase("enter"))
		{
			final L2Player player = st.getPlayer();
			if(player == null)
				return null;

			final Castle castle = CastleManager.getInstance().getCastleByObject(player);
			if(!check(player))
				return "prison_keeper_q0727_08.htm";

			boolean contract = false;
			// для входа нужно заключить договор хотя бы с одним фортом
			for(int i = 101; i <= 121; i++)
			{
				final Fortress fort = FortressManager.getInstance().getFortressByIndex(i);
				if(fort.getFortState() == 2 && fort.getCastleId() == castle.getId())
					contract = true;
			}

			if(!contract)
				return "prison_keeper_q0727_13a.htm";

			return enterInPailaka(player, castle);
		}
		return event;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		if(check(st.getPlayer()))
		{
			final int cond = st.getCond();
			switch (cond)
			{
				case 0:
				{
					if(st.getPlayer().getLevel() >= 70 && st.getState() == CREATED)
						return "prison_keeper_q0727_01.htm";
					else
						return "prison_keeper_q0727_04.htm";
				}
				case 1:
					return "prison_keeper_q0727_15.htm";
				case 3:
				{
					st.giveItems(KnightsEpaulette, 159);
					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(true);
					return "prison_keeper_q0727_16.htm";
				}
				default:
					return null;
			}
		}
		else
			return "prison_keeper_q0727_03.htm";
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		for(final CastlePailakaWorld pailaka : _worlds.values())
			if(pailaka.getReflectionId() == npc.getReflection().getId())
				switch (npc.getNpcId())
				{
					case invader_fist:
						pailaka.changeStage();
						break;
					// fail
					case knight:
					case ranger:
					case mage:
					case warrior:
					{
						Functions.npcSay(npc, helper_message[Rnd.get(helper_message.length)]);
						// TODO fail?
						break;
					}
				}

		return null;
	}

	private String enterInPailaka(final L2Player player, final Castle castle)
	{
		final L2Party party = player.getParty();
		final L2Clan clan = player.getClan();

		if(party == null || clan == null)
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return null;
		}

		// если не лидер группы
		if(!party.isLeader(player))
		{
			player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
			return null;
		}

		// проверяем преналдежность к клану
		for(final L2Player member : party.getPartyMembers())
			if(member.getClanId() != clan.getClanId())
			{
				showHtmlFile(player, "prison_keeper_q0727_11.htm", "%name%", member.getName());
				return null;
			}

		// проверяем наличие квеста
		for(final L2Player member : party.getPartyMembers())
		{
			final QuestState qs = member.getQuestState("_727_HopeWithinTheDarkness");
			if(qs == null || qs.getState() != Quest.STARTED)
			{
				showHtmlFile(player, "prison_keeper_q0727_12.htm", "%name%", member.getName());
				return null;
			}
		}

		final InstanceManager ilm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> ils = ilm.getById(ReflectionTable.CASTLE_PAILAKA);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return null;
		}

		final Instance il = ils.get(0);
		assert il != null;

		final String name = il.getName();
		final int timelimit = il.getDuration();
		final int min_level = il.getMinLevel();
		final int max_level = il.getMaxLevel();
		final int maxParty = il.getMaxParty();

		// FIXME можно возращаться назад
		if(party.isInReflection())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
			return null;
		}

		for(final L2Player member : party.getPartyMembers())
			if(ilm.getTimeToNextEnterInstance(name, member) > 0)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
				return "EnterRestricted.htm";
			}

		if(party.getMemberCount() > maxParty)
		{
			player.sendPacket(Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT);
			return null;
		}

		for(final L2Player member : party.getPartyMembers())
		{
			if(member.getLevel() < min_level || member.getLevel() > max_level)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				member.sendPacket(sm);
				player.sendPacket(sm);
				return null;
			}
			if(!player.isInRange(member, 500))
			{
				member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				return null;
			}
		}

		CastlePailakaWorld pailaka = null;
		if(!_worlds.isEmpty())
		{
			pailaka = _worlds.get(castle.getId());
			if(pailaka != null && pailaka.isLocked())
				return "EnterRestricted.htm";
		}

		final Reflection r = new Reflection(name);
		if(r.getTeleportLoc() == null)
			r.setTeleportLoc(il.getTeleportCoords());
		r.fillSpawns(il.getSpawnsInfo());
		r.setReturnLoc(player.getLoc());

		pailaka = new CastlePailakaWorld(castle.getId(), r);
		_worlds.put(pailaka.getCastleId(), pailaka);

		for(final L2Player member : party.getPartyMembers())
		{
			if(member != player)
				newQuestState(member, STARTED);
			member.setReflection(r);
			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.teleToLocation(il.getTeleportCoords());
			member.setVar(name, String.valueOf(System.currentTimeMillis()));
		}

		party.setReflection(r);
		r.setParty(party);
		r.setEmptyDestroyTime(10 * 60 * 1000);
		r.startCollapseTimer(timelimit * 60 * 1000L);
		party.broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));

		// запускаем спаун через 2 минуты
		pailaka.startSpawn(2 * 60 * 1000);

		return null;
	}

	@Override
	protected boolean isCoreQuest()
	{
		return true;
	}

	private static boolean check(final L2Player player)
	{
		final Castle castle = CastleManager.getInstance().getCastleByObject(player);
		if(castle == null)
			return false;
		final L2Clan clan = player.getClan();
		if(clan == null)
			return false;
		if(clan.getClanId() != castle.getOwnerId())
			return false;
		return true;
	}
}
