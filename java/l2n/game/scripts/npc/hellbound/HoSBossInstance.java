package l2n.game.scripts.npc.hellbound;

import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Данный инстанс используется босами в Hall of Suffering.
 * Босов 2, очистку рефлекшена при смерти не делаем.
 * 
 * @author SYS
 */
public class HoSBossInstance extends L2ReflectionBossInstance
{
	private boolean _startBuff = false;

	public HoSBossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public boolean isStartBuffTask()
	{
		return _startBuff;
	}

	public void startBuffTask()
	{
		_startBuff = true;
	}

	@Override
	protected void clearReflection()
	{}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
