package l2n.game.scripts.npc.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.instancemanager.boss.BelethManager;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Надгробие у Белефа
 * 
 * @Доработан полностью
 * 
 */
public class StoneCoffin extends L2NpcInstance
{
	public StoneCoffin(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("tryOpen"))
		{
			if(!player.isInParty())
			{
				showChatWindow(player, 2);
				return;
			}

			// Опция доступна только лидеру чата командования
			final L2CommandChannel channel = player.getParty().getCommandChannel();
			if(channel == null)
			{
				showChatWindow(player, 2);
				return;
			}

			// доступно только лидеру канала
			if(channel.getChannelLeader().getObjectId() != player.getObjectId())
			{
				showChatWindow(player, 2);
				return;
			}

			showChatWindow(player, 1);
			// Даём Beleth's Ring ?
			Functions.addItem(player, 10314, 1);
			channel.teleToLocation(BelethManager.ENTRANCE_LOCATIONOFF_B, 300, 500);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
