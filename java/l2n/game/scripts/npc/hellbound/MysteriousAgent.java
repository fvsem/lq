package l2n.game.scripts.npc.hellbound;

import l2n.Config;
import l2n.extensions.scripts.Events;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;

/**
 * инстанс для Mysterious Agent, управляет переходом на 7 этаж с 6 и 8 этажей. Так же даёт баф.
 * 
 * @author L2System Project
 * @date 29.07.2011
 * @time 21:30:11
 */
public class MysteriousAgent extends L2NpcInstance
{
	private static final L2Skill skill_buff = SkillTable.getInstance().getInfo(5526, 1);
	private static final int buff_range = 400;

	private int mode = 0;

	public MysteriousAgent(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public void setMode(final int value)
	{
		mode = value;
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		player.setLastNpc(this);
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
				player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
			player.sendPacket(new ValidateLocation(this), Msg.ActionFail);
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}

		// С NPC нельзя разговаривать мертвым и сидя
		if(!Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || player.isAlikeDead())
		{
			player.sendActionFailed();
			return;
		}

		if(mode == 1)
			showChatWindow(player, 2);
		else
			showChatWindow(player, 1);
		player.sendActionFailed();
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("accept"))
		{
			if(player.isInParty())
			{
				for(L2Player member : player.getParty().getPartyMembers())
				{
					if(!member.isInRange(this, buff_range))
					{
						showChatWindow(player, 12);
						return;
					}

					broadcastPacket(new MagicSkillUse(this, member, skill_buff.getDisplayId(), 1, skill_buff.getHitTime(), 0));
					skill_buff.getEffects(this, member, false, false);
					member.teleToLocation(TullyWorkshopManager.TullyRoofLocationPoint);
				}
			}
			else
			{
				if(player.isInRange(this, buff_range))
				{
					broadcastPacket(new MagicSkillUse(this, player, skill_buff.getDisplayId(), 1, skill_buff.getHitTime(), 0));
					skill_buff.getEffects(this, player, false, false);
					player.teleToLocation(TullyWorkshopManager.TullyRoofLocationPoint);
				}
				else
					showChatWindow(player, 12);
			}
		}
		else if(command.equalsIgnoreCase("reject"))
			TullyWorkshopManager.getInstance().rejectKillChallanges(this, player);
		else if(command.equalsIgnoreCase("to7Floor"))
		{
			if(player.isInParty())
			{
				if(!player.isPartyLeader())
				{
					showChatWindow(player, 11);
					return;
				}
				player.getParty().teleport(TullyWorkshopManager.TullyFloor7LocationPoint);
			}
			else
				player.teleToLocation(TullyWorkshopManager.TullyFloor7LocationPoint);
			deleteMe();
		}
		else if(command.equalsIgnoreCase("no7Floor"))
			deleteMe();
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom = npcId + "-" + val;
		String temp = "data/html/hellbound/tully/mysterious/" + pom + ".htm";
		File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}
}
