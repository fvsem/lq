package l2n.game.scripts.npc.hellbound;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.DoorTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

import java.util.StringTokenizer;

/**
 * Инстанс для Tully's Gatekeeper - открывают двери на 5 этаже в Tully Workshop (для классов Warsmith/Maestro без фейлов)
 * 
 * @author L2System Project
 * @date 26.07.2011
 * @time 19:38:33
 */
public class TullyGatekeeper extends L2NpcInstance
{
	private final class CloseDoor implements Runnable
	{
		@Override
		public void run()
		{
			for(int doorId : doors)
				DoorTable.getInstance().getDoor(doorId).closeMe();
			setBusy(false);
		}
	}

	private final class Repair implements Runnable
	{
		@Override
		public void run()
		{
			setBusy(false);
		}
	}

	private final int[] doors;

	public TullyGatekeeper(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		doors = ArrayUtil.toIntArray(template.getAIParams().getString(AiOptionsType.OPTIONS, ""), ",");
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		String filename;
		if(player.getClassId() == ClassId.warsmith || player.getClassId() == ClassId.maestro)
			filename = "data/html/hellbound/tully/device_operate001c.htm";
		else
			filename = "data/html/hellbound/tully/device_operate001.htm";
		player.sendPacket(new NpcHtmlMessage(player, this, filename, val));
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(isBusy())
		{
			showBusyWindow(player);
			return;
		}

		StringTokenizer st = new StringTokenizer(command);
		if(st.nextToken().equals("openDoor"))
		{
			if(player.getClassId() == ClassId.warsmith || player.getClassId() == ClassId.maestro)
			{
				for(int doorId : doors)
					DoorTable.getInstance().getDoor(doorId).openMe();
				L2GameThreadPools.getInstance().scheduleAi(new CloseDoor(), 2 * 60 * 1000, false);
			}
			else if(Rnd.chance(25))
			{
				for(int doorId : doors)
					DoorTable.getInstance().getDoor(doorId).openMe();
				L2GameThreadPools.getInstance().scheduleAi(new CloseDoor(), 2 * 60 * 1000, false);
			}
			else
			{
				player.sendPacket(new NpcHtmlMessage(player, this, "data/html/hellbound/tully/device_operate001a.htm", 0));
				L2GameThreadPools.getInstance().scheduleAi(new Repair(), 2 * 60 * 1000, false);
				setBusy(true);
			}
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showBusyWindow(L2Player player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile("data/html/hellbound/tully/device_operate001b.htm");
		player.sendPacket(html);
	}
}
