package l2n.game.scripts.npc.hellbound;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Location;

import java.util.StringTokenizer;

/**
 * Данный инстанс используется в городе-инстансе на Hellbound как точка выхода
 * Вставьте Ключ Дьявольского Глаза в Стеллу и через 5 минут вас телепортирует в SC BS (Base Tower).<BR>
 * В течение этого времени следите за монстрами - то, что вы убили РБ не означает, что они не будут вас трогать
 */
public final class MoonlightTombstoneInstance extends L2NpcInstance
{
	private static final int KEY_ID = 9714;
	private final static long COLLAPSE_TIME = 5; // 5 мин
	private boolean _activated = false;

	public MoonlightTombstoneInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		StringTokenizer st = new StringTokenizer(command);
		if(st.nextToken().equals("insertKey") && player.getReflection() != null)
		{
			if(player.getParty() == null)
			{
				player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
				return;
			}

			if(player.getParty().getPartyLeaderOID() != player.getObjectId())
			{
				player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
				return;
			}

			GArray<L2Player> partyMembers = player.getParty().getPartyMembers();
			for(L2Player partyMember : partyMembers)
				if(!isInRange(partyMember, INTERACTION_DISTANCE * 2))
				{
					// Члены партии слишком далеко
					Functions.show(Files.read("data/html/default/32343-3.htm", player), player);
					return;
				}

			if(_activated)
			{
				// Уже активировано
				Functions.show(Files.read("data/html/default/32343-1.htm", player), player);
				return;
			}

			if(Functions.getItemCount(player, KEY_ID) > 0)
			{
				Functions.removeItem(player, KEY_ID, 1);
				player.getReflection().startCollapseTimer(COLLAPSE_TIME * 60 * 1000L);
				_activated = true;
				broadcastPacketToOthers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(COLLAPSE_TIME));
				player.getReflection().setCoreLoc(player.getReflection().getReturnLoc());
				player.getReflection().setReturnLoc(new Location(16280, 283448, -9704));
				Functions.show(Files.read("data/html/default/32343-1.htm", player), player);
				return;
			}
			// Нет ключа
			Functions.show(Files.read("data/html/default/32343-2.htm", player), player);
			return;
		}
		super.onBypassFeedback(player, command);
	}
}
