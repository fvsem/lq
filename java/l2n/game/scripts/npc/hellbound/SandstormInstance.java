package l2n.game.scripts.npc.hellbound;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Данный инстанс используется NPC Sandstorm в локации Hellbound
 * 
 * @author SYS
 */
public class SandstormInstance extends L2NpcInstance
{
	public SandstormInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		player.sendActionFailed();
	}
}
