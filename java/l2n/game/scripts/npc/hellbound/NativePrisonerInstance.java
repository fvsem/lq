package l2n.game.scripts.npc.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.AbnormalEffect;
import l2n.game.templates.L2NpcTemplate;

import java.util.StringTokenizer;

/**
 * Данный инстанс используется в городе-инстансе на Hellbound
 * 
 * @author L2System Project
 * @date 15.09.2010
 * @time 2:22:03
 */
public final class NativePrisonerInstance extends L2NpcInstance
{
	public NativePrisonerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onSpawn()
	{
		startAbnormalEffect(AbnormalEffect.HOLD_2);
		super.onSpawn();
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this) || isBusy())
			return;

		StringTokenizer st = new StringTokenizer(command);
		if(st.nextToken().equals("rescue"))
		{
			stopAbnormalEffect(AbnormalEffect.HOLD_2);
			Functions.npcSay(this, "Thank you for saving me! Guards are coming, run!");
			Functions.callScripts("services.Caravan", "changeConfidence", new Object[] { player, Long.valueOf(20) }, null); // Цифра с потолка
			deleteMe();
		}
		else
			super.onBypassFeedback(player, command);
	}
}
