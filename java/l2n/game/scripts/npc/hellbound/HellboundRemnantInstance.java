package l2n.game.scripts.npc.hellbound;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

public class HellboundRemnantInstance extends L2MonsterInstance
{
	public HellboundRemnantInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void reduceCurrentHp(double i, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		super.reduceCurrentHp(Math.min(i, getCurrentHp() - 1), attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	public void onUseHolyWater(L2Character user)
	{
		if(getCurrentHp() < 100)
			doDie(user);
	}
}
