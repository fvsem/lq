package l2n.game.scripts.npc.hellbound;

import l2n.Config;
import l2n.extensions.scripts.Events;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;

/**
 * Инстанс для Teleportation Cubic на 6,7,8 этаже. Отвечает за телепорт между этими этажами и на крышу.
 * 
 * @author L2System Project
 * @date 27.07.2011
 * @time 2:31:00
 */
public class TeleportationCubic extends L2NpcInstance
{
	private final int mode;

	public TeleportationCubic(int objectId, L2NpcTemplate template, final int mode)
	{
		super(objectId, template);
		this.mode = mode;
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		player.setLastNpc(this);
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
				player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
			player.sendPacket(new ValidateLocation(this), Msg.ActionFail);
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}

		// С NPC нельзя разговаривать мертвым и сидя
		if(!Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || player.isAlikeDead())
		{
			player.sendActionFailed();
			return;
		}

		int val;
		if(mode == 1)
			val = 1;
		else if(mode == 2)
			val = 2;
		else
			val = 3;
		showChatWindow(player, val);
		player.sendActionFailed();
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("toSixFloor"))
		{
			if(player.isInParty())
			{
				if(player.getParty().getPartyLeader().isInRange(this, 3000))
					player.getParty().teleport(TullyWorkshopManager.TullyFloor6LocationPoint);
				else
					showChatWindow(player, 4);
			}
			else
				player.teleToLocation(TullyWorkshopManager.TullyFloor6LocationPoint);
		}
		else if(command.equalsIgnoreCase("toEightFloor"))
		{
			if(player.isInParty())
			{
				if(player.getParty().getPartyLeader().isInRange(this, 3000))
					player.getParty().teleport(TullyWorkshopManager.TullyFloor8LocationPoint);
				else
					showChatWindow(player, 4);
			}
			else
				player.teleToLocation(TullyWorkshopManager.TullyFloor8LocationPoint);
		}
		else if(command.equalsIgnoreCase("toRoof"))
		{
			if(player.isInParty())
			{
				if(player.getParty().getPartyLeader().isInRange(this, 3000))
					player.getParty().teleport(TullyWorkshopManager.TullyRoofLocationPoint);
				else
					showChatWindow(player, 4);
			}
			else
				player.teleToLocation(TullyWorkshopManager.TullyRoofLocationPoint);
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom = npcId + "-" + val;
		String temp = "data/html/hellbound/tully/cube68/" + pom + ".htm";
		File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}
}
