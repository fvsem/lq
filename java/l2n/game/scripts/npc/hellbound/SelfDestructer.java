package l2n.game.scripts.npc.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;

/**
 * Инстанс для Ingenious Contraption. Отвечает за выключение сигнализации, включение зоны с дамагом, выдачу медалей для последующего доступа в Anomic Foundry (1 нпс даёт 1 тип медалей за раз).
 * 
 * @author L2System Project
 * @date 26.07.2011
 * @time 17:40:48
 */
public class SelfDestructer extends L2NpcInstance
{
	private int itemId = 0;

	public SelfDestructer(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public void setItem(final int id)
	{
		itemId = id;
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("giveMedal"))
			giveMedals(player);
		else if(command.equalsIgnoreCase("disarm"))
			// FIXME не работает пока((
			player.sendActionFailed();
		else
			super.onBypassFeedback(player, command);
	}

	public void giveMedals(final L2Player player)
	{
		if(!player.getVarB("tullyMedal", false) && itemId > 0)
		{
			Functions.addItem(player, itemId, 1);
			// Чтоб получить можно было один раз за убийство Tully
			itemId = 0;
			player.setVar("tullyMedal", "true");
		}
		else
			showChatWindow(player, 5);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		String temp = "data/html/hellbound/tully/" + pom + ".htm";
		File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}
}
