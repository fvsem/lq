package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2RaidBossInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 28.04.2013
 * @time 9:43:57
 */
public class PailakaBossInstance extends L2RaidBossInstance
{
	public PailakaBossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return attacker.isPlayable() || attacker instanceof PailakaHelpersInstance;
	}

	@Override
	public boolean canAttackCharacter(final L2Character target)
	{
		return target.isPlayable() || target instanceof PailakaHelpersInstance;
	}
}
