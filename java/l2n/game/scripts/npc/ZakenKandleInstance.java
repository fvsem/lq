package l2n.game.scripts.npc;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.boss.ZakenManager;
import l2n.game.instancemanager.boss.ZakenManager.ZakenInstanceInfo;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

/**
 * @author L2System Project
 * @date 17.02.2011
 * @time 2:25:16
 */
public class ZakenKandleInstance extends L2NpcInstance
{
	public ZakenKandleInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}

		ZakenInstanceInfo instanceInfo;
		if((instanceInfo = InstanceManager.getInstance().getWorld(getReflectionId(), ZakenInstanceInfo.class)) != null)
		{
			if(player.getParty() == null || !player.getParty().isLeader(player))
			{
				// Если лидер пати, то продолжаем проверку, если нет, выходим
				player.sendActionFailed();
				return;
			}

			if(player.getParty().getCommandChannel() != null && player.getParty().getCommandChannel().getChannelLeader() != player)
			{
				// Если есть CC, то бочки может зажигать только лидер CC
				player.sendActionFailed();
				return;
			}

			int id = 0;
			if(instanceInfo.getZakenLoc().z == getLoc().z && instanceInfo.getZakenLoc().distance(getLoc()) < 1200)
			{
				if(!instanceInfo.getBlueKandles().contains(getObjectId()))
				{
					id = 15302;
					instanceInfo.getBlueKandles().add(getObjectId());
					if(instanceInfo.getBlueKandles().size() == 4)
					{
						try
						{
							L2Spawn spawn = new L2Spawn(instanceInfo.getZakenId());
							getReflection().addSpawn(spawn);
							spawn.setReflection(getReflectionId());
							spawn.setRespawnDelay(0, 0);
							spawn.setLocation(0);
							spawn.setLoc(instanceInfo.getZakenLoc());
							L2NpcInstance zaken = spawn.doSpawn(true);
							ZakenManager.calcZakenStat(zaken, instanceInfo);
							spawn.stopRespawn();
						}
						catch(ClassNotFoundException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
			else if(!instanceInfo.getRedKandles().contains(getObjectId()))
			{
				id = 15281;
				instanceInfo.getRedKandles().add(getObjectId());

				// Спаун охраны в ближайшей комнате
				int room = Integer.MAX_VALUE;
				double distance = Double.MAX_VALUE;
				for(int i = 0; i < ZakenManager.ROOM_CENTER_COORDS.length; i++)
				{
					if(getLoc().z == ZakenManager.ROOM_CENTER_COORDS[i][2] && getLoc().distance(new Location(ZakenManager.ROOM_CENTER_COORDS[i])) < distance)
					{
						room = i;
						distance = getLoc().distance(new Location(ZakenManager.ROOM_CENTER_COORDS[i]));
					}
				}
				spawnGuards(player, room, 29023, 29023, 29024, 29024, 29024, 29026, 29026, 29026, 29027, 29027);
			}

			if(id != 0)
			{
				setRHandId(15280);// Анимация зажигания
				broadcastCharInfo();// отсылка NpcInfo всем вокруг
				L2GameThreadPools.getInstance().scheduleGeneral(new KandleFireTask(this, id), 3000);
			}
		}
		player.sendActionFailed();
	}

	private void spawnGuards(L2Player player, int room, int... ids)
	{
		Location loc = new Location(ZakenManager.ROOM_CENTER_COORDS[room]);
		L2NpcInstance npc;
		for(int id : ids)
		{
			try
			{
				L2Spawn spawn = new L2Spawn(id);
				getReflection().addSpawn(spawn);
				spawn.setReflection(getReflectionId());
				spawn.setRespawnDelay(0, 0);
				spawn.setLocation(0);
				spawn.setLoc(loc.rnd(50, 100, false));
				npc = spawn.doSpawn(true);
				if(npc != null)
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, getRandomTarget(npc, player), Rnd.get(1, 100));
				spawn.stopRespawn();
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

	private L2Player getRandomTarget(L2NpcInstance npc, L2Player player)
	{
		GArray<L2Player> list = npc.getAroundPlayers(1200);
		if(list.isEmpty())
			return player;
		return list.get(Rnd.get(list.size()));
	}

	private class KandleFireTask implements Runnable
	{
		private ZakenKandleInstance npc;
		private int id;

		public KandleFireTask(ZakenKandleInstance npc, int id)
		{
			this.npc = npc;
			this.id = id;
		}

		@Override
		public void run()
		{
			npc.setRHandId(id);
			npc.broadcastCharInfo();
		}
	}
}
