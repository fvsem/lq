package l2n.game.scripts.npc;

import l2n.Config;
import l2n.extensions.scripts.Events;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.game.templates.L2NpcTemplate;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 28.04.2013
 * @time 4:27:22
 */
public class PailakaHelpersInstance extends L2MonsterInstance
{
	private final String quest;

	/**
	 * Помощники для квестов {@link l2n.game.scripts.quests._726_LightWithinTheDarkness} и {@link l2n.game.scripts.quests._727_HopeWithinTheDarkness}
	 * 
	 * @param objectId
	 * @param template
	 */
	public PailakaHelpersInstance(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
		switch (template.getNpcId())
		{
			case 36566:
			case 36567:
			case 36568:
			case 36569:
			{
				quest = "_726_LightWithinTheDarkness";
				break;
			}
			default:
				quest = "_727_HopeWithinTheDarkness";
		}
	}

	@Override
	public void reduceCurrentHp(final double damage, final L2Character attacker, final L2Skill skill, final boolean awake, final boolean standUp, final boolean directHp, final boolean canReflect, final boolean isCounteAttack)
	{
		if(attacker != null && attacker.isPlayable())
			return;

		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public void onAction(final L2Player player, final boolean shift)
	{
		player.setLastNpc(this);
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
			{
				player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
				player.sendPacket(makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
			}
			player.sendPacket(new ValidateLocation(this), Msg.ActionFail);
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}

		// С NPC нельзя разговаривать мертвым и сидя
		if(!Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || player.isAlikeDead())
		{
			player.sendActionFailed();
			return;
		}

		player.sendActionFailed();

		if(getAI().getIntention() == CtrlIntention.AI_INTENTION_ATTACK || getAI().getIntention() == CtrlIntention.AI_INTENTION_CAST)
			player.sendPacket(new NpcHtmlMessage(player, this, "data/html/instance/pailaka/pailaka_bold002.htm", 0));
		else if(player.isPartyLeader())
		{
			final QuestState st = player.getQuestState(quest);
			if(st != null && st.getCond() == 3)
			{
				final NpcHtmlMessage msg = new NpcHtmlMessage(player, this, "data/html/instance/pailaka/pailaka_bold003.htm", 0);
				msg.replace("%quest%", quest);
				player.sendPacket(msg);
			}
			else
				player.sendPacket(new NpcHtmlMessage(player, this, "data/html/instance/pailaka/pailaka_bold001.htm", 0));
		}
		else
			player.sendPacket(new NpcHtmlMessage(player, this, "data/html/instance/pailaka/pailaka_bold001.htm", 0));
	}

	@Override
	public boolean isAutoAttackable(final L2Character attacker)
	{
		return !attacker.isPlayable();
	}

	@Override
	public boolean canAttackCharacter(final L2Character target)
	{
		return target instanceof PailakaBossInstance || target instanceof PailakaInvaderInstance;
	}
}
