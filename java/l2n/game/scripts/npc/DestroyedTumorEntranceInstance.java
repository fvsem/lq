package l2n.game.scripts.npc;

import l2n.game.instancemanager.boss.EkimusManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Для инстанса Seed of Infinity
 * Отвечает за телепорт к Экимусу
 * Отвечает за смену групп у Экимуса
 * 
 * @author L2System Project
 * @date 24.03.2011
 * @time 12:37:04
 */
public class DestroyedTumorEntranceInstance extends L2NpcInstance
{
	public DestroyedTumorEntranceInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("view"))
			showChatWindow(player, getHtmlPath(getNpcId(), 1));
		else if(command.equalsIgnoreCase("enter"))
		{
			// если без пати и не лидер
			if(player.getParty() == null || !player.getParty().isLeader(player))
				showChatWindow(player, getHtmlPath(getNpcId(), 2));
			else if(!EkimusManager.teleToHeart(player, this))
				showChatWindow(player, getHtmlPath(getNpcId(), 3));
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		if(val == 0)
			return "data/html/instance/SeedOfInfinity/TumorEntrance.htm";
		return "data/html/instance/SeedOfInfinity/TumorEntrance-" + val + ".htm";
	}
}
