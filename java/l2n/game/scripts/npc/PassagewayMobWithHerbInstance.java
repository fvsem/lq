package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Моб при смерти дропает херб "Fiery Demon Blood"
 * 
 * @date 21.07.2010
 * @time 0:34:28
 */
public final class PassagewayMobWithHerbInstance extends L2MonsterInstance
{
	public PassagewayMobWithHerbInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public static final int FieryDemonBloodHerb = 9849;

	@Override
	protected void calculateRewards(L2Character lastAttacker)
	{
		if(lastAttacker == null)
			return;

		super.calculateRewards(lastAttacker);

		if(lastAttacker.isPlayable())
			dropItem(lastAttacker.getPlayer(), FieryDemonBloodHerb, 1);
	}
}
