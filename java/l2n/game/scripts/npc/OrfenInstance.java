package l2n.game.scripts.npc;

import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2BossInstance;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

public class OrfenInstance extends L2BossInstance
{
	public static final Location nest = new Location(43728, 17220, -4342);

	public static final Location[] locs = new Location[] { new Location(55024, 17368, -5412), new Location(53504, 21248, -5496), new Location(53248, 24576, -5272) };

	private boolean _teleportedToNest;

	public OrfenInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	/**
	 * Used by Orfen to set 'teleported' flag, when hp goes to <50%
	 */
	public void setTeleported(boolean flag)
	{
		_teleportedToNest = flag;
		Location loc = flag ? nest : locs[Rnd.get(locs.length)];
		setSpawnedLoc(loc);
		clearAggroList(true);
		getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, null, null);
		teleToLocation(loc);
	}

	@Override
	public void spawnMe()
	{
		super.spawnMe();
		setTeleported(false);
		broadcastPacketToOthers(new PlaySound(1, "BS01_A", 1, 0, getLoc()));
	}

	@Override
	public void doDie(L2Character killer)
	{
		broadcastPacketToOthers(new PlaySound(1, "BS02_D", 1, 0, getLoc()));
		super.doDie(killer);
	}

	@Override
	public void reduceCurrentHp(double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
		if(!isTeleported() && getCurrentHpPercents() <= 50)
			setTeleported(true);
	}

	public boolean isTeleported()
	{
		return _teleportedToNest;
	}
}
