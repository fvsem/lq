package l2n.game.scripts.npc;

import l2n.game.geodata.GeoControl;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2RoundTerritory;
import l2n.game.model.L2Territory;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

import java.util.HashMap;

/**
 * @author L2System Project
 * @date 13.05.2013
 * @time 23:49:34
 */
public class ObeliskInstance extends L2MonsterInstance implements GeoControl
{
	public ObeliskInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		int id = IdFactory.getInstance().getNextId();
		L2Territory pos = new L2RoundTerritory(id, -245825, 217075, 230, -12208, -12000);
		setGeoPos(pos);
		GeoEngine.applyControl(this);
	}

	private L2Territory geoPos;
	private HashMap<Long, Byte> geoAround;

	@Override
	public L2Territory getGeoPos()
	{
		return geoPos;
	}

	@Override
	public void setGeoPos(L2Territory value)
	{
		geoPos = value;
	}

	@Override
	public HashMap<Long, Byte> getGeoAround()
	{
		return geoAround;
	}

	@Override
	public void setGeoAround(HashMap<Long, Byte> value)
	{
		geoAround = value;
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}

	@Override
	public boolean isGeoCloser()
	{
		return false;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}
}
