package l2n.game.scripts.npc.naia;

import l2n.game.instancemanager.TowerOfNaiaManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Для обработки смертей в Tower of Naia
 * 
 * @author L2System Project
 * @date 01.08.2011
 * @time 2:59:39
 */
public class NaiaMonster extends L2MonsterInstance
{
	public NaiaMonster(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void doDie(L2Character killer)
	{
		TowerOfNaiaManager.getInstance().checkCurrentRoom(killer.getPlayer(), this);
		super.doDie(killer);
	}
}
