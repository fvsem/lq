package l2n.game.scripts.npc.naia;

import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TowerOfNaiaManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;

/**
 * @author L2System Project
 * @date 30.07.2011
 * @time 4:46:21
 */
public class NaiaController extends L2NpcInstance
{
	private final class Repair implements Runnable
	{
		@Override
		public void run()
		{
			setBusy(false);
		}
	}

	public NaiaController(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		// не пускаем если одна группа уже там и ещё не прошла до Эпидоса
		if(isBusy() || TowerOfNaiaManager.getInstance().ifPartyExist())
		{
			showBusyWindow(player);
			return;
		}

		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_CAN_OPERATE_THE_MACHINE_WHEN_YOU_PARTICIPATE_IN_THE_PARTY);
			return;
		}

		if(command.equalsIgnoreCase("tryEnter"))
		{
			if(TowerOfNaiaManager.getInstance().checkLockHealPoint())
				showChatWindow(player, 5);
			else
			{
				showChatWindow(player, 2);
				setBusy(true);
				L2GameThreadPools.getInstance().scheduleAi(new Repair(), 5 * 60 * 1000, false);
			}
		}
		else if(command.equalsIgnoreCase("enter"))
		{
			// реюз 20 мин
			setBusy(true);
			L2GameThreadPools.getInstance().scheduleAi(new Repair(), 20 * 60 * 1000, false);

			player.getParty().teleport(TowerOfNaiaManager.ENTRANCE_LOCATION);
			TowerOfNaiaManager.getInstance().setParty(player.getParty());
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showBusyWindow(L2Player player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(player, this);
		html.setFile("data/html/hellbound/naia/naia_key/18492-2.htm");
		player.sendPacket(html);
	}

	@Override
	public String getHtmlPath(int npcId, int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		String temp = "data/html/hellbound/naia/naia_key/" + pom + ".htm";
		File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}
}
