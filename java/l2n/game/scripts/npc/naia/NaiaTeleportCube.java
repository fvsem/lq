package l2n.game.scripts.npc.naia;

import l2n.Config;
import l2n.game.instancemanager.boss.BelethManager;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

import java.io.File;

/**
 * @author L2System Project
 * @date 03.08.2011
 * @time 4:43:47
 */
public class NaiaTeleportCube extends L2NpcInstance
{
	public NaiaTeleportCube(final int objectId, final L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(final L2Player player, final String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("toBeleth"))
		{
			if(!player.isInParty())
			{
				showChatWindow(player, 1);
				return;
			}

			// // Опция доступна только лидеру чата командования из 36 и более человек.
			final L2CommandChannel channel = player.getParty().getCommandChannel();
			if(channel == null)
			{
				showChatWindow(player, 1);
				return;
			}

			// доступно только лидеру канала
			if(channel.getChannelLeader().getObjectId() != player.getObjectId())
			{
				showChatWindow(player, 1);
				return;
			}

			// проверяем минимальное количество игроков
			if(channel.getMemberCount() < Config.BELETH_MIN_PLAYER_COUNT)
			{
				showChatWindow(player, 1);
				return;
			}

			// Белеф уже участвует в битве с другими.
			if(BelethManager.getInstance().isActivated())
			{
				showChatWindow(player, 2);
				return;
			}

			// Белефа здесь нет = респаун похоже
			if(!BelethManager.getInstance().isEnableEnter())
			{
				showChatWindow(player, 3);
				return;
			}

			// телепортируем
			channel.teleToLocation(BelethManager.ENTRANCE_LOCATION, 300, 500);
			// запускам спаун Белета
			BelethManager.getInstance().showMustGoOn();
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public String getHtmlPath(final int npcId, final int val)
	{
		String pom;
		if(val == 0)
			pom = "" + npcId;
		else
			pom = npcId + "-" + val;
		final String temp = "data/html/hellbound/naia/" + pom + ".htm";
		final File mainText = new File(temp);
		if(mainText.exists())
			return temp;

		// If the file is not found, the standard message "I have nothing to say to you" is returned
		return "data/html/npcdefault.htm";
	}
}
