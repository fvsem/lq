package l2n.game.scripts.npc.naia;

import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TowerOfNaiaManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.tables.DoorTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.ArrayUtil;

/**
 * Отвечает за прохождения этажей в Tower of Naia
 * 
 * @author L2System Project
 * @date 30.07.2011
 * @time 5:50:02
 */
public class IngeniousContraption extends L2NpcInstance
{
	private final int floor;
	/** в этих комнатах есть зоны с дебафам */
	private final boolean isTrap; // TODO
	private final int door1;
	/** дверь для прохода в следующую комнату */
	private final int door2;

	private boolean alreadyUsed = false;

	public IngeniousContraption(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		floor = template.getNpcId() - 18493;
		int[] data = ArrayUtil.toIntArray(template.getAIParams().getString(AiOptionsType.OPTIONS, ""), ",");
		door1 = data[0];
		door2 = data[1];
		isTrap = data[2] == 1;
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(alreadyUsed)
		{
			if(player.isGM())
				player.sendMessage("Switcher already used, please wait.");
			return;
		}

		// дальше не пускаем, если не убиты все монстры в комнате
		if(!TowerOfNaiaManager.getInstance().checkTalkCondition(player, this))
		{
			player.sendActionFailed();
			return;
		}

		if(command.equalsIgnoreCase("startRoom"))
		{
			TowerOfNaiaManager.getInstance().startRoom(floor, player);
			closeDoor(true);
			alreadyUsed = true;
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void onSpawn()
	{
		// первую дверь открываем при спауне, чтоб можно было зайти в комнату
		openDoor(true);
		super.onSpawn();
	}

	/**
	 * закрывает двери перед стартом команты
	 */
	public void closeDoor(boolean first)
	{
		DoorTable.getInstance().getDoor(first ? door1 : door2).closeMe();
	}

	/**
	 * Открывает двери если, если комната пройдена
	 */
	public void openDoor(boolean first)
	{
		DoorTable.getInstance().getDoor(first ? door1 : door2).openMe();
	}

	/**
	 * Возобновляем начальное состояние НПС и комнаты
	 */
	public void renewSwitcher()
	{
		DoorTable.getInstance().getDoor(door1).openMe();
		DoorTable.getInstance().getDoor(door2).closeMe();
		alreadyUsed = false;
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_CAN_OPERATE_THE_MACHINE_WHEN_YOU_PARTICIPATE_IN_THE_PARTY);
			return;
		}

		player.sendPacket(new NpcHtmlMessage(player, this, "data/html/hellbound/naia/switcher/tower_switch.htm", val));
	}
}
