package l2n.game.scripts.npc;

import l2n.Config;
import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.instancemanager.games.HandysBlockCheckerManager.ArenaParticipantsHolder;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExCubeGameChangeTimeToStart;
import l2n.game.network.serverpackets.ExCubeGameRequestReady;
import l2n.game.network.serverpackets.ExCubeGameTeamList;
import l2n.game.templates.L2NpcTemplate;

/**
 * Отвечает за регистрацию на арене
 * 
 * @author L2System Project
 * @date 21.12.2010
 * @time 10:44:58
 */
public class L2EntranceManagerInstance extends L2NpcInstance
{
	// Arena Managers
	private static final int A_MANAGER_1 = 32521;
	private static final int A_MANAGER_2 = 32522;
	private static final int A_MANAGER_3 = 32523;
	private static final int A_MANAGER_4 = 32524;

	public L2EntranceManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		// если ивент выключен
		if(!Config.ENABLE_BLOCK_CHECKER_EVENT)
		{
			showChatWindow(player, "data/html/npcdefault.htm");
			return;
		}

		int npcId = getTemplate().npcId;

		int arena = -1;
		switch (npcId)
		{
			case A_MANAGER_1:
				arena = 0;
				break;
			case A_MANAGER_2:
				arena = 1;
				break;
			case A_MANAGER_3:
				arena = 2;
				break;
			case A_MANAGER_4:
				arena = 3;
				break;
		}

		if(arena != -1)
		{
			if(eventIsFull(arena))
			{
				player.sendMessage("The Arena " + (arena + 1) + " queue is full. Try again later");
				return;
			}
			if(HandysBlockCheckerManager.getInstance().arenaIsBeingUsed(arena))
			{
				player.sendMessage("The Arena is alredy holding an event");
				return;
			}
			if(HandysBlockCheckerManager.getInstance().addPlayerToArena(player, arena))
			{
				ArenaParticipantsHolder holder = HandysBlockCheckerManager.getInstance().getHolder(arena);

				final ExCubeGameTeamList tl = new ExCubeGameTeamList(holder.getRedPlayers(), holder.getBluePlayers(), arena);

				player.sendPacket(tl);

				int countBlue = holder.getBlueTeamSize();
				int countRed = holder.getRedTeamSize();
				int minMembers = Config.MIN_BLOCK_CHECKER_TEAM_MEMBERS;

				if(countBlue >= minMembers && countRed >= minMembers)
				{
					holder.updateEvent();
					holder.broadCastPacketToTeam(new ExCubeGameRequestReady());
					holder.broadCastPacketToTeam(new ExCubeGameChangeTimeToStart(10));
				}
			}
		}
	}

	private boolean eventIsFull(int arena)
	{
		if(HandysBlockCheckerManager.getInstance().getHolder(arena).getAllPlayers().size() == 12)
			return true;
		return false;
	}

}
