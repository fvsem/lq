package l2n.game.scripts.npc;

import l2n.Config;
import l2n.extensions.scripts.Events;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2SpecialMonsterInstance;
import l2n.game.network.serverpackets.MyTargetSelected;
import l2n.game.network.serverpackets.NpcInfo;
import l2n.game.network.serverpackets.ValidateLocation;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 20.12.2010
 * @time 16:36:13
 */
public class L2BlockInstance extends L2SpecialMonsterInstance
{
	/**
	 * <font color=blue>1 = Blue</font>, <font color=red>2 = red</font>
	 */
	private int _team;

	public L2BlockInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
		setHalfHeightName(true);
		setInvul(true);
	}

	/**
	 * Will change the color of the block and update
	 * the appearance in the known players clients
	 */
	public void changeColor(L2Player attacker)
	{
		// Do not update color while sending old info
		synchronized (this)
		{
			if(_team == 2)
			{
				// Change color
				_team = 1;
				// BroadCast to all known players
				broadcastPacket(new NpcInfo(this, attacker));
			}
			else
			{
				// Change color
				_team = 2;
				// BroadCast to all known players
				broadcastPacket(new NpcInfo(this, attacker));
			}
		}
	}

	@Override
	public int getTeam()
	{
		return _team;
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		if(player.getTarget() != this)
		{
			player.setTarget(this);
			if(player.getTarget() == this)
				player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));

			player.sendPacket(new ValidateLocation(this), Msg.ActionFail);
			return;
		}

		if(Events.onAction(player, this, shift))
		{
			player.sendActionFailed();
			return;
		}

		if(!isInRange(player, INTERACTION_DISTANCE))
		{
			if(player.getAI().getIntention() != CtrlIntention.AI_INTENTION_INTERACT)
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this, null);
			return;
		}
		// С NPC нельзя разговаривать мертвым и сидя
		if(!Config.ALLOW_TALK_WHILE_SITTING && player.isSitting() || player.isAlikeDead())
		{
			player.sendActionFailed();
			return;
		}

		player.sendActionFailed();
	}

	/**
	 * Sets if the block is red or blue. Mainly used in
	 * block spawn
	 * 
	 * @param isRed
	 */
	public void setRed(boolean isRed)
	{
		_team = isRed ? 2 : 1;
	}

	/**
	 * Return if the block is red at this momment
	 * 
	 * @return
	 */
	public boolean isRed()
	{
		return _team == 2;
	}
}
