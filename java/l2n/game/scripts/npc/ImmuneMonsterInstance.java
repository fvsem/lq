package l2n.game.scripts.npc;

import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

public class ImmuneMonsterInstance extends L2MonsterInstance
{
	public ImmuneMonsterInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}
}
