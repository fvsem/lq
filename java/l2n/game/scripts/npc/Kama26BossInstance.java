package l2n.game.scripts.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.templates.L2NpcTemplate;

import java.util.concurrent.ScheduledFuture;

public class Kama26BossInstance extends L2ReflectionBossInstance
{
	private ScheduledFuture<?> _spawner;
	private static int GUARD1 = 18556;

	public Kama26BossInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void notifyMinionDied(L2MinionInstance minion)
	{
		if(minionList != null)
			minionList.removeSpawnedMinion(minion);
		_spawner = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new MinionSpawner(this), 60000, 60000, false);
	}

	@Override
	public void onSpawn()
	{
		setNewMinionList();
		minionList.spawnSingleMinionSync(18556);
		super.onSpawn();
	}

	@Override
	public void doDie(L2Character killer)
	{
		if(_spawner != null)
			_spawner.cancel(true);
		super.doDie(killer);
	}

	public static class MinionSpawner implements Runnable
	{
		private L2ReflectionBossInstance _boss;

		public MinionSpawner(L2ReflectionBossInstance boss)
		{
			_boss = boss;
		}

		@Override
		public void run()
		{
			try
			{
				if(_boss != null && !_boss.isDead() && _boss.getTotalSpawnedMinionsInstances() == 0)
				{
					if(_boss.getMinionList() == null)
						_boss.new MinionMaintainTask().run();
					_boss.getMinionList().spawnSingleMinionSync(GUARD1);
					Functions.npcShoutCustomMessage(_boss, "Kama26Boss.helpme");
				}
			}
			catch(Throwable e)
			{
				e.printStackTrace();
			}
		}
	}
}
