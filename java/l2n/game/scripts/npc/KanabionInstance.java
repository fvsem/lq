package l2n.game.scripts.npc;

import l2n.game.ai.CtrlEvent;
import l2n.game.geodata.GeoEngine;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Skill;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.KamalokaNightmare;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

import java.util.logging.Level;

public class KanabionInstance extends L2MonsterInstance
{
	public KanabionInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void reduceCurrentHp(double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		if(Rnd.chance(damage * 37. / getMaxHp()))
			try
			{
				KanabionInstance npc = new KanabionInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(getNext()));
				npc.setSpawnedLoc(GeoEngine.findPointToStay(getX(), getY(), getZ(), 100, 120));
				npc.setReflection(getReflection().getId());
				npc.onSpawn();
				npc.spawnMe(npc.getSpawnedLoc());
				npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
		super.reduceCurrentHp(damage, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public void doDie(L2Character killer)
	{
		Reflection r = getReflection();
		if(r instanceof KamalokaNightmare)
			((KamalokaNightmare) r).registerKilled(getTemplate());
		super.doDie(killer);
	}

	private int getNext()
	{
		int i = Rnd.get(0, 1);
		switch (getNpcId())
		{
			case 22452: // White Skull Kanabion
				return 22453; // Doppler
			case 22453:
				return 22453 + i; // Doppler or Void
			case 22454:
				return 22454; // Void

			case 22455: // Begrudged Kanabion
				return 22456; // Doppler
			case 22456:
				return 22456 + i; // Doppler or Void
			case 22457:
				return 22457; // Void

			case 22458: // Rotten Kanabion
				return 22459; // Doppler
			case 22459:
				return 22459 + i; // Doppler or Void
			case 22460:
				return 22460; // Void

			case 22461: // Gluttonous Kanabion
				return 22462; // Doppler
			case 22462:
				return 22462 + i; // Doppler or Void
			case 22463:
				return 22463; // Void

			case 22464: // Callous Kanabion
				return 22465; // Doppler
			case 22465:
				return 22465 + i; // Doppler or Void
			case 22466:
				return 22466; // Void

			case 22467: // Savage Kanabion
				return 22468; // Doppler
			case 22468:
				return 22468 + i; // Doppler or Void
			case 22469:
				return 22469; // Void

			case 22470: // Peerless Kanabion
				return 22471; // Doppler
			case 22471:
				return 22471 + i; // Doppler or Void
			case 22472:
				return 22472; // Void

			case 22473: // Massive Kanabion
				return 22474; // Doppler
			case 22474:
				return 22474 + i; // Doppler or Void
			case 22475:
				return 22475; // Void

			case 22476: // Fervent Kanabion
				return 22477; // Doppler
			case 22477:
				return 22477 + i; // Doppler or Void
			case 22478:
				return 22478; // Void

			case 22479: // Ruptured Kanabion
				return 22480; // Doppler
			case 22480:
				return 22480 + i; // Doppler or Void
			case 22481:
				return 22481; // Void

			case 22482: // Sword Kanabion
				return 22483; // Doppler
			case 22483:
				return 22483 + i; // Doppler or Void
			case 22484:
				return 22484; // Void

			default:
				return 0; // такого быть не должно
		}
	}
}
