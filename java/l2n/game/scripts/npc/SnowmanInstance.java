package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Данный инстанс используется NPC Snowman в эвенте Saving Snowman
 * 
 * @date 21.07.2010
 * @time 0:39:40
 */
public class SnowmanInstance extends L2NpcInstance
{
	public SnowmanInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{
		player.sendActionFailed();
	}
}
