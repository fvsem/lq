package l2n.game.scripts.npc;

import l2n.game.instancemanager.SeedOfInfinityManager;
import l2n.game.instancemanager.boss.EkimusManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 04.03.2011
 * @time 12:53:23
 */
public class GatekeeperOfAbyssInstance extends L2NpcInstance
{
	public GatekeeperOfAbyssInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		int cycle = SeedOfInfinityManager.getCurrentCycle();
		if(command.equalsIgnoreCase("enter"))
		{
			if(cycle == 1)
				showChatWindow(player, 3);
			else if(cycle == 2 || cycle == 5)
				showChatWindow(player, 2);
			else
				showChatWindow(player, 1);
		}
		else if(command.equalsIgnoreCase("toSeed"))
		{
			// сбор камней
			if(cycle == 3 || cycle == 4)
				SeedOfInfinityManager.enterToGathering(player);
			else
				showChatWindow(player, 3);
		}
		else if(command.equalsIgnoreCase("toInfinity"))
		{
			if(cycle == 2) // вход к экимосу
				EkimusManager.enterInstance(player);
			else if(cycle == 5) // вход в _698_BlocktheLordsEscape
				showQuestWindow(player, "_698_BlocktheLordsEscape");
			else
				showChatWindow(player, 3);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
