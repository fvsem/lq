package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * для квеста Collecting in the Air (Сбор Звездных Камней)
 * 
 * @author L2System Project
 * @date 08.02.2010
 * @time 10:15:57
 */
public final class L2StarStoneInstance extends L2NpcInstance
{
	/**
	 * @param objectId
	 * @param template
	 */
	public L2StarStoneInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(L2Player player, int val)
	{}

	@Override
	public void showChatWindow(L2Player player, String filename)
	{}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{}
}
