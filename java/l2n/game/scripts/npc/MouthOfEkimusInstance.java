package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * @author L2System Project
 * @date 04.03.2011
 * @time 11:45:57
 */
public final class MouthOfEkimusInstance extends L2NpcInstance
{
	public MouthOfEkimusInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(L2Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		super.onBypassFeedback(player, command);
	}
}
