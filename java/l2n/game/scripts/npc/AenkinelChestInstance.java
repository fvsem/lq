package l2n.game.scripts.npc;

import l2n.commons.list.GArray;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

import static l2n.game.scripts.ai.raidboss.Aenkinel.MUTANT_TREASURE_CHEST;
import static l2n.game.scripts.ai.raidboss.Aenkinel.NIHIL_INVADER_TREASURE_CHEST;

/**
 * Инстанс для сундуков в Гранях Реальности которые появляеются после смерти РБ Aenkinel
 * 
 * @see l2n.game.scripts.ai.raidboss.Aenkinel
 * @author L2System Project
 * @date 01.11.2010
 * @time 13:20:00
 */
public class AenkinelChestInstance extends L2MonsterInstance
{
	private boolean isFakeChest = true;

	public boolean isFakeChest()
	{
		return isFakeChest;
	}

	/**
	 * @param objectId
	 * @param template
	 */
	public AenkinelChestInstance(int objectId, L2NpcTemplate template, boolean isFake)
	{
		super(objectId, template);
		isFakeChest = isFake;
	}

	@Override
	public void doDie(L2Character killer)
	{
		Reflection r = getReflection();
		if(r == null || !r.isDelusionChamber())
			return;

		// если убивают фейковый сундук, то удаляем все сундуки
		if(isFakeChest)
		{
			GArray<L2NpcInstance> npcs = r.getNpcs();
			for(L2NpcInstance npc : npcs)
				if(npc != null && (npc.getNpcId() == NIHIL_INVADER_TREASURE_CHEST || npc.getNpcId() == MUTANT_TREASURE_CHEST))
					npc.deleteMe();
			return;
		}

		super.doDie(killer);
	}

	@Override
	protected void calculateRewards(L2Character lastAttacker)
	{
		Reflection r = getReflection();
		if(r == null || !r.isDelusionChamber())
			return;

		// удалеям только фейковые, основной сам удалиться)
		GArray<L2NpcInstance> npcs = r.getNpcs();
		for(L2NpcInstance npc : npcs)
			if(npc != null && (npc.getNpcId() == NIHIL_INVADER_TREASURE_CHEST || npc.getNpcId() == MUTANT_TREASURE_CHEST) && ((AenkinelChestInstance) npc).isFakeChest())
				npc.deleteMe();

		super.calculateRewards(lastAttacker);
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
