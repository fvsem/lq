package l2n.game.scripts.npc;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Данный инстанс используется мобом Thomas D. Turkey в эвенте Saving Snowman
 */
public class ThomasInstance extends L2MonsterInstance
{
	public ThomasInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void reduceCurrentHp(double i, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		i = 10;
		if(attacker.getActiveWeaponInstance() != null)
			switch (attacker.getActiveWeaponInstance().getItemId())
			{
			// Хроно оружие наносит больший урон
				case 4202: // Chrono Cithara
				case 5133: // Chrono Unitus
				case 5817: // Chrono Campana
				case 7058: // Chrono Darbuka
				case 8350: // Chrono Maracas
					i = 100;
					break;
				default:
					i = 10;
			}

		super.reduceCurrentHp(i, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public void doDie(L2Character killer)
	{
		L2Character topdam = getTopDamager(getAggroList());
		if(topdam == null)
			topdam = killer;
		topdam.callScripts("events_off.SavingSanta.SavingSnowman", "freeSnowman", new Object[] { topdam });
		super.doDie(killer);
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
