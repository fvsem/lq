package l2n.game.scripts.npc;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.entity.KamalokaAbyssLabyrinth;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Инстанс для РБ 4-ой комнаты из Лабиринт Бездны.<br>
 * <li>Убийство настоящего монстра в 1-ой комнате уменьшает физическую защиту на 1/3</li><br>
 * <li>Убийство всех монстров во 2-ой комнате уменьшает магическую защиту на 1/3</li><br>
 * <li>Убийство РБ в 3-ей комнате уменьшает силу физической атаки на 1/3</li><br>
 * 
 * @author L2System Project
 * @date 21.09.2010
 * @time 0:18:23
 */
public class LostCaptainInstance extends L2ReflectionBossInstance
{
	/**
	 * @param objectId
	 * @param template
	 */
	public LostCaptainInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	/** уменьшает физическую защиту на 1/3 */
	@Override
	public int getPDef(L2Character target)
	{
		if(getReflection() != null && getReflection().isKamalokaAbyssLabyrinth())
			return (int) (((KamalokaAbyssLabyrinth) getReflection()).isFirstRoomMonsterIsDead() ? super.getPDef(target) / 3.0 : super.getPDef(target));
		return super.getPDef(target);
	}

	/** уменьшает магическую защиту на 1/3 */
	@Override
	public int getMDef(L2Character target, L2Skill skill)
	{
		if(getReflection() != null && getReflection().isKamalokaAbyssLabyrinth())
			return (int) (((KamalokaAbyssLabyrinth) getReflection()).isSecondRoomMonsterIsDead() ? super.getMDef(target, skill) / 3.0 : super.getMDef(target, skill));
		return super.getMDef(target, skill);
	}

	/** уменьшает силу физической атаки на 1/3 */
	@Override
	public int getPAtk(L2Character target)
	{
		if(getReflection() != null && getReflection().isKamalokaAbyssLabyrinth())
			return (int) (((KamalokaAbyssLabyrinth) getReflection()).isThirdRoomMonsterIsDead() ? super.getPAtk(target) / 3.0 : super.getPAtk(target));
		return super.getPAtk(target);
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
