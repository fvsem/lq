package l2n.game.scripts.npc;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.templates.L2NpcTemplate;

/**
 * Данный инстанс используется при смерти Tumor of Death
 * 
 * @author L2System Project
 * @date 06.12.2010
 * @time 13:53:39
 */
public class DestroyedTumorInstance extends L2NpcInstance
{
	public DestroyedTumorInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onAction(L2Player player, boolean shift)
	{
		player.sendActionFailed();
	}
}
