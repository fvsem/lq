package l2n.game.scripts.npc;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2SpecialMonsterInstance;
import l2n.game.templates.L2NpcTemplate;

public class SquashInstance extends L2SpecialMonsterInstance
{
	public final static int Young_Squash = 12774;
	public final static int High_Quality_Squash = 12775;
	public final static int Low_Quality_Squash = 12776;
	public final static int Large_Young_Squash = 12777;
	public final static int High_Quality_Large_Squash = 12778;
	public final static int Low_Quality_Large_Squash = 12779;
	public final static int King_Squash = 13016;
	public final static int Emperor_Squash = 13017;

	private long _spawnerId;

	public SquashInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}

	public void setSpawner(L2Player spawner)
	{
		_spawnerId = spawner != null ? spawner.getStoredId() : 0;
	}

	public L2Player getSpawner()
	{
		return L2ObjectsStorage.getAsPlayer(_spawnerId);
	}

	@Override
	public void reduceCurrentHp(double i, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp, boolean canReflect, boolean isCounteAttack)
	{
		if(attacker.getActiveWeaponInstance() == null)
			return;

		int weaponId = attacker.getActiveWeaponInstance().getItemId();

		if(getNpcId() == Low_Quality_Large_Squash || getNpcId() == High_Quality_Large_Squash || getNpcId() == Emperor_Squash)

			if(weaponId != 4202 && weaponId != 5133 && weaponId != 5817 && weaponId != 7058 && weaponId != 8350)
				return;

		i = 1;

		super.reduceCurrentHp(i, attacker, skill, awake, standUp, directHp, canReflect, isCounteAttack);
	}

	@Override
	public long getRegenTick()
	{
		return 0;
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}
