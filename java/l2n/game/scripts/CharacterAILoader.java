package l2n.game.scripts;

import gnu.trove.map.hash.THashMap;
import l2n.commons.compiler.Compiler;
import l2n.commons.compiler.MemoryClassLoader;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFileLoader;
import l2n.game.ai.*;
import l2n.game.model.actor.L2Character;
import l2n.game.scripts.ai.*;
import l2n.game.scripts.ai.event.HandysBlock;
import l2n.game.scripts.ai.event.Thomas;
import l2n.game.scripts.ai.grandboss.*;
import l2n.game.scripts.ai.hellbound.*;
import l2n.game.scripts.ai.kamaloka.Kama56Boss;
import l2n.game.scripts.ai.kamaloka.Kama56Minion;
import l2n.game.scripts.ai.kamaloka.Kama63Minion;
import l2n.game.scripts.ai.npc.*;
import l2n.game.scripts.ai.quests.*;
import l2n.game.scripts.ai.raidboss.*;
import l2n.game.scripts.ai.sod.DimensionMovingDevice;
import l2n.game.scripts.ai.sod.NoRndWalkMonster;
import l2n.game.scripts.ai.sod.Tiat;
import l2n.game.scripts.ai.soi.*;
import l2n.game.scripts.ai.stakato.*;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 
 * @date 29.08.2011
 * @time 9:58:13
 */
public class CharacterAILoader extends ScriptFileLoader
{
	protected static final Logger _log = Logger.getLogger(CharacterAILoader.class.getName());

	public final THashMap<String, Constructor<? extends L2CharacterAI>> ai_constructors;

	public static CharacterAILoader getInstance()
	{
		return SingletonHolder._instance;
	}

	private CharacterAILoader()
	{
		ai_constructors = new THashMap<String, Constructor<? extends L2CharacterAI>>();
		// загружаем
		load(false);
		ai_constructors.trimToSize();
	}

	public void load(final boolean reload)
	{
		if(reload)
			ai_constructors.clear();

		loadStaticAI();
		final int count = loadCustomAI();
		_log.info("CharacterAILoader: loaded " + ai_constructors.size() + " (" + count + " custom) AIs.");
	}

	/**
	 * Загружает AI которые в ядре
	 */
	private void loadStaticAI()
	{
		// Основные типы ИИ
		loadConstructor("Fighter", Fighter.class);
		loadConstructor("Guard", Guard.class);
		loadConstructor("Mystic", Mystic.class);
		loadConstructor("Priest", Priest.class);
		loadConstructor("RaceManager", RaceManager.class);
		loadConstructor("Ranger", Ranger.class);
		loadConstructor("SiegeGuard", SiegeGuard.class);
		loadConstructor("SiegeGuardFighter", SiegeGuardFighter.class);
		loadConstructor("SiegeGuardMystic", SiegeGuardMystic.class);
		loadConstructor("SiegeGuardPriest", SiegeGuardPriest.class);
		loadConstructor("SiegeGuardRanger", SiegeGuardRanger.class);
		loadConstructor("Trap", Trap.class);

		// Дополнительные типы ИИ
		loadConstructor("AirshipGuard1", AirshipGuard1.class);
		loadConstructor("AirshipGuard2", AirshipGuard2.class);

		// RaidBoss
		loadConstructor("Aenkinel", Aenkinel.class);
		loadConstructor("Antharas", Antharas.class);
		loadConstructor("Baium", Baium.class);
		loadConstructor("BaiumNpc", BaiumNpc.class);
		loadConstructor("Baylor", Baylor.class);
		loadConstructor("Darnel", Darnel.class);
		loadConstructor("Core", Core.class);
		loadConstructor("Gordon", Gordon.class);
		loadConstructor("RankuScapegoat", RankuScapegoat.class);
		loadConstructor("Orfen", Orfen.class);
		loadConstructor("Orfen_RibaIren", Orfen_RibaIren.class);
		loadConstructor("Tears", Tears.class);
		loadConstructor("Tiat", Tiat.class);
		loadConstructor("Tyrannosaurus", Tyrannosaurus.class);
		loadConstructor("Valakas", Valakas.class);
		loadConstructor("Zaken", Zaken.class);

		loadConstructor("Alhena", Alhena.class);
		loadConstructor("AncientEgg", AncientEgg.class);
		loadConstructor("AttackMobNotPlayerFighter", AttackMobNotPlayerFighter.class);
		loadConstructor("BehemothDragon", BehemothDragon.class);
		loadConstructor("BodyDestroyer", BodyDestroyer.class);
		loadConstructor("CaughtFighter", CaughtFighter.class);
		loadConstructor("CaughtMystic", CaughtMystic.class);
		loadConstructor("Chimeras", Chimeras.class);
		loadConstructor("CrystallineGolem", CrystallineGolem.class);
		loadConstructor("DaimonTheWhiteEyed", DaimonTheWhiteEyed.class);
		loadConstructor("DarkWaterDragon", DarkWaterDragon.class);
		loadConstructor("DeluLizardmanSpecialAgent", DeluLizardmanSpecialAgent.class);
		loadConstructor("DeluLizardmanSpecialCommander", DeluLizardmanSpecialCommander.class);
		loadConstructor("DimensionMovingDevice", DimensionMovingDevice.class);
		loadConstructor("Edwin", Edwin.class);
		loadConstructor("EdwinFollower", EdwinFollower.class);
		loadConstructor("Elpy", Elpy.class);
		loadConstructor("EvasGiftBox", EvasGiftBox.class);
		loadConstructor("EvilNpc", EvilNpc.class);
		loadConstructor("FafurionKindred", FafurionKindred.class);
		loadConstructor("FieldMachine", FieldMachine.class);
		loadConstructor("FrightenedRagnaOrc", FrightenedRagnaOrc.class);
		loadConstructor("FrostBuffalo", FrostBuffalo.class);
		loadConstructor("GatekeeperZombie", GatekeeperZombie.class);
		loadConstructor("GhostOfVonHellmannsPage", GhostOfVonHellmannsPage.class);
		loadConstructor("GraveRobberSummoner", GraveRobberSummoner.class);
		loadConstructor("GuardianAngel", GuardianAngel.class);
		loadConstructor("GuardRndWalkAndAnim", GuardRndWalkAndAnim.class);

		// AI для Seed Of Infinity
		loadConstructor("HallOfSufferingBoss", HallOfSufferingBoss.class);
		loadConstructor("Cohemenes", Cohemenes.class);
		loadConstructor("UnstableSeed", UnstableSeed.class);
		loadConstructor("AbstractSOIMonster", AbstractSOIMonster.class);
		loadConstructor("FeralHound", FeralHound.class);

		loadConstructor("HotSpringsMob", HotSpringsMob.class);
		loadConstructor("InfinitumFighter", InfinitumFighter.class);
		loadConstructor("InfinitumMystic", InfinitumMystic.class);
		loadConstructor("IsleOfPrayerFighter", IsleOfPrayerFighter.class);
		loadConstructor("IsleOfPrayerMystic", IsleOfPrayerMystic.class);
		loadConstructor("Jaradine", Jaradine.class);
		loadConstructor("Kama56Boss", Kama56Boss.class);
		loadConstructor("Kama56Minion", Kama56Minion.class);
		loadConstructor("Kama63Minion", Kama63Minion.class);
		loadConstructor("KarulBugbear", KarulBugbear.class);
		loadConstructor("KashasEye", KashasEye.class);
		loadConstructor("Kasiel", Kasiel.class);
		loadConstructor("Keltirs", Keltirs.class);
		loadConstructor("Kreed", Kreed.class);
		loadConstructor("Leandro", Leandro.class);
		loadConstructor("Leogul", Leogul.class);
		loadConstructor("MercenaryCaptain", MercenaryCaptain.class);
		loadConstructor("MoSMonk", MoSMonk.class);
		loadConstructor("NightAgressionMystic", NightAgressionMystic.class);

		loadConstructor("NoRndWalkFighter", NoRndWalkFighter.class);
		loadConstructor("NoRndWalkMystic", NoRndWalkMystic.class);
		loadConstructor("NoRndWalkMonster", NoRndWalkMonster.class);

		loadConstructor("OlMahumGeneral", OlMahumGeneral.class);
		loadConstructor("PaganGuard", PaganGuard.class);
		loadConstructor("PrisonGuard", PrisonGuard.class);
		loadConstructor("Pronghorn", Pronghorn.class);
		loadConstructor("Pterosaur", Pterosaur.class);
		loadConstructor("QueenAntNurse", QueenAntNurse.class);
		loadConstructor("RagnaHealer", RagnaHealer.class);
		loadConstructor("Remy", Remy.class);
		loadConstructor("RndTeleportFighter", RndTeleportFighter.class);
		loadConstructor("RndWalkAndAnim", RndWalkAndAnim.class);
		loadConstructor("Rogin", Rogin.class);
		loadConstructor("Rokar", Rokar.class);
		loadConstructor("Rooney", Rooney.class);
		loadConstructor("Sandstorm", Sandstorm.class);
		loadConstructor("Scarlet", Scarlet.class);
		loadConstructor("Shade", Shade.class);
		loadConstructor("SteelCitadelKeymaster", SteelCitadelKeymaster.class);
		loadConstructor("SuspiciousMerchantAaru", SuspiciousMerchantAaru.class);
		loadConstructor("SuspiciousMerchantAntharas", SuspiciousMerchantAntharas.class);
		loadConstructor("SuspiciousMerchantArchaic", SuspiciousMerchantArchaic.class);
		loadConstructor("SuspiciousMerchantBayou", SuspiciousMerchantBayou.class);
		loadConstructor("SuspiciousMerchantBorderland", SuspiciousMerchantBorderland.class);
		loadConstructor("SuspiciousMerchantCloud", SuspiciousMerchantCloud.class);
		loadConstructor("SuspiciousMerchantDemon", SuspiciousMerchantDemon.class);
		loadConstructor("SuspiciousMerchantDragonspine", SuspiciousMerchantDragonspine.class);
		loadConstructor("SuspiciousMerchantFloran", SuspiciousMerchantFloran.class);
		loadConstructor("SuspiciousMerchantHive", SuspiciousMerchantHive.class);
		loadConstructor("SuspiciousMerchantHunters", SuspiciousMerchantHunters.class);
		loadConstructor("SuspiciousMerchantIvoryTower", SuspiciousMerchantIvoryTower.class);
		loadConstructor("SuspiciousMerchantMarshland", SuspiciousMerchantMarshland.class);
		loadConstructor("SuspiciousMerchantMonastic", SuspiciousMerchantMonastic.class);
		loadConstructor("SuspiciousMerchantShanty", SuspiciousMerchantShanty.class);
		loadConstructor("SuspiciousMerchantSouthernGludio", SuspiciousMerchantSouthernGludio.class);
		loadConstructor("SuspiciousMerchantTanor", SuspiciousMerchantTanor.class);
		loadConstructor("SuspiciousMerchantValley", SuspiciousMerchantValley.class);
		loadConstructor("SuspiciousMerchantWestern", SuspiciousMerchantWestern.class);
		loadConstructor("SuspiciousMerchantWhiteSands", SuspiciousMerchantWhiteSands.class);
		loadConstructor("Tate", Tate.class);
		loadConstructor("Taurin", Taurin.class);
		loadConstructor("Thomas", Thomas.class);
		loadConstructor("TimakOrcTroopLeader", TimakOrcTroopLeader.class);
		loadConstructor("TorturedNative", TorturedNative.class);
		loadConstructor("TownGuard", TownGuard.class);
		loadConstructor("Turnir", Turnir.class);
		loadConstructor("WatchmanMonster", WatchmanMonster.class);
		loadConstructor("WitchWarder", WitchWarder.class);
		loadConstructor("Yakand", Yakand.class);

		// AI для монстров в Forge of Gods
		loadConstructor("ForgeOfGods", ForgeOfGods.class);
		loadConstructor("TarBeetle", TarBeetle.class);
		loadConstructor("Lavasaurus", Lavasaurus.class);

		// AI для монстров в Stakato
		loadConstructor("QueenShyeed", QueenShyeed.class);
		loadConstructor("StakatoCheif", StakatoCheif.class);
		loadConstructor("BizarreCocoon", BizarreCocoon.class);
		loadConstructor("FemaleSpikedStakato", FemaleSpikedStakato.class);
		loadConstructor("SpikedStakatoNurse", SpikedStakatoNurse.class);
		loadConstructor("StakatoFollower", StakatoFollower.class);

		// Различные квесты
		loadConstructor("Quest024Fighter", Quest024Fighter.class);
		loadConstructor("Quest024Mystic", Quest024Mystic.class);
		loadConstructor("Quest421FairyTree", Quest421FairyTree.class);
		loadConstructor("EpicQuestLilith", EpicQuestLilith.class);
		loadConstructor("EpicQuestAnakim", EpicQuestAnakim.class);
		loadConstructor("EpicQuestMage", EpicQuestMage.class);
		loadConstructor("EpicQuestFighter", EpicQuestFighter.class);
		loadConstructor("SealDevice", SealDevice.class);
		// FIXME конструктор тут другой, можно не грузить loadConstructor("GuardsOfDawn", GuardsOfDawn.class);
		loadConstructor("Lematan", Lematan.class);

		loadConstructor("PailakaBoss", PailakaBoss.class);
		loadConstructor("PailakaInvader", PailakaInvader.class);
		loadConstructor("PailakaHelperFighter", PailakaHelperFighter.class);
		loadConstructor("PailakaHelperMystic", PailakaHelperMystic.class);
		loadConstructor("PailakaHelperRanger", PailakaHelperRanger.class);

		// Hellbound
		loadConstructor("Darion", Darion.class);
		loadConstructor("DemonPrince", DemonPrince.class);
		loadConstructor("Ranku", Ranku.class);
		loadConstructor("Kechi", Kechi.class);
		loadConstructor("MasterZelos", MasterZelos.class);
		loadConstructor("MasterFestina", MasterFestina.class);
		loadConstructor("Tully", Tully.class);
		loadConstructor("TullyWarden", TullyWarden.class);
		loadConstructor("TullyServant", TullyServant.class);
		loadConstructor("ExperimentalGolem", ExperimentalGolem.class);
		loadConstructor("FloatingGhost", FloatingGhost.class);
		loadConstructor("SoulCrystalPillar", SoulCrystalPillar.class);
		loadConstructor("Challenger", Challenger.class);
		loadConstructor("MutatedElpy", MutatedElpy.class);
		loadConstructor("BelethSlave", BelethSlave.class);
		loadConstructor("NaiaSpore", NaiaSpore.class);
		loadConstructor("Epidos", Epidos.class);
		loadConstructor("Beleth", Beleth.class);
		loadConstructor("BelethMinion", BelethMinion.class);
		loadConstructor("Shadai", Shadai.class);

		loadConstructor("ZoneSwither", ZoneSwither.class);
		// event Handys Block Checker
		loadConstructor("HandysBlock", HandysBlock.class);
	}

	private int loadCustomAI()
	{
		final GArray<File> ai_files = new GArray<File>();
		parseClasses(new File("./data/scripts/ai"), ai_files);
		if(Compiler.getInstance().compile(ai_files, System.out))
		{
			int count = 0;
			final MemoryClassLoader classLoader = Compiler.getInstance().getClassLoader();
			for(final String name : classLoader.getClasses())
			{
				if(name.contains("$"))
					continue; // пропускаем вложенные классы
				try
				{
					@SuppressWarnings("unchecked")
					final Class<? extends L2CharacterAI> c = (Class<? extends L2CharacterAI>) classLoader.loadClass(name);
					if(loadConstructor(c))
						count++;
				}
				catch(final Exception e)
				{
					_log.warning("Can't load custom AI script class[" + name + "]:" + e.getMessage());
				}
			}
			Compiler.getInstance().clearClassLoader();
			return count;
		}
		else
			_log.warning("Can't compile custom AI scripts!");
		return 0;
	}

	protected void loadConstructor(final String nameAI, final Class<? extends L2CharacterAI> c)
	{
		Constructor<? extends L2CharacterAI> constructor = null;
		try
		{
			constructor = c.getConstructor(L2Character.class);
		}
		catch(final SecurityException e)
		{
			_log.log(Level.WARNING, "", e);
		}
		catch(final NoSuchMethodException e)
		{
			// _log.log(Level.WARNING, "", e);
		}

		if(constructor != null)
			ai_constructors.put(nameAI, constructor);
	}

	protected boolean loadConstructor(final Class<? extends L2CharacterAI> c)
	{
		Constructor<? extends L2CharacterAI> constructor = null;
		try
		{
			constructor = c.getConstructor(L2Character.class);
		}
		catch(final SecurityException e)
		{
			_log.log(Level.WARNING, "", e);
		}
		catch(final NoSuchMethodException e)
		{
			// _log.log(Level.WARNING, "", e);
		}

		if(constructor != null)
		{
			ai_constructors.put(c.getSimpleName(), constructor);
			return true;
		}
		return false;
	}

	/**
	 * @param aiType
	 *            - название AI
	 * @return true если AI с таким названием есть
	 */
	public boolean isAvailable(final String aiType)
	{
		return ai_constructors.containsKey(aiType);
	}

	public L2CharacterAI getAI(final String aiType, final L2Character actor)
	{
		final Constructor<? extends L2CharacterAI> constructor = ai_constructors.get(aiType);
		if(constructor != null)
			try
			{
				return constructor.newInstance(actor);
			}
			catch(final Exception e)
			{
				_log.log(Level.SEVERE, "CharacterAILoader getAI error: ", e);
				return null;
			}
		return null;
	}

	@Override
	protected boolean checkFileCondition(final File file, final boolean dir)
	{
		if(dir)
		{
			if(file.isHidden() || file.getName().equals(".svn"))
				return false;
		}
		else if(file.isHidden() || !file.getName().contains(".java"))
			return false;

		return true;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final CharacterAILoader _instance = new CharacterAILoader();
	}
}
