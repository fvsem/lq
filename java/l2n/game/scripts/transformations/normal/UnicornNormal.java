package l2n.game.scripts.transformations.normal;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class UnicornNormal extends L2DefaultTransformation
{
	public UnicornNormal()
	{
		// id, colRadius, colHeight
		super(205, 8.0, 25.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 563, 3); // Horn of Doom
		addSkill(player, 564, 3); // Gravity Control
		addSkill(player, 565, 3); // Horn Assault
		addSkill(player, 567, 3); // Light of Heal
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 563); // Horn of Doom
		removeSkill(player, 564); // Gravity Control
		removeSkill(player, 565); // Horn Assault
		removeSkill(player, 567); // Light of Heal
	}
}
