package l2n.game.scripts.transformations.normal;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GolemGuardianNormal extends L2DefaultTransformation
{
	public GolemGuardianNormal()
	{
		// id, colRadius, colHeight
		super(211, 8.0, 23.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 572, 3); // Double Slasher
		addSkill(player, 573, 3); // Earthquake
		addSkill(player, 574, 3); // Bomb Installation
		addSkill(player, 575, 3); // Steel Cutter
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 572); // Double Slasher
		removeSkill(player, 573); // Earthquake
		removeSkill(player, 574); // Bomb Installation
		removeSkill(player, 575); // Steel Cutter
	}
}
