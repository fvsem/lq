package l2n.game.scripts.transformations.cursed;

import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

public class Akamanah extends L2Transformation
{
	public Akamanah()
	{
		// id, colRadius, colHeight
		super(302, 10.0, 32.73);
	}

	@Override
	public void onTransform(L2Player player)
	{
		player.setVisName("Akamanah");
		player.setVisTitle("");
		addSkill(player, 3630, 1); // Void Burst
		addSkill(player, 3631, 1); // Void Flow
		addSkill(player, 3328, 1);
		addSkill(player, 3330, 1);
		addSkill(player, 3331, 1);
	}

	@Override
	public void onUntransform(L2Player player)
	{
		player.setVisName(null);
		player.setVisTitle(null);
		removeSkill(player, 3630); // Void Burst
		removeSkill(player, 3631); // Void Flow
		removeSkill(player, 3328);
		removeSkill(player, 3330);
		removeSkill(player, 3331);
	}
}
