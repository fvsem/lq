package l2n.game.scripts.transformations.cursed;

import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

public class Zariche extends L2Transformation
{
	public Zariche()
	{
		// id, colRadius, colHeight
		super(301, 9.0, 31.0);
	}

	@Override
	public void onTransform(L2Player player)
	{
		player.setVisName("Zariche");
		player.setVisTitle("");
		addSkill(player, 3630, 1); // Void Burst
		addSkill(player, 3631, 1); // Void Flow
		addSkill(player, 3329, 1);
		addSkill(player, 3330, 1);
		addSkill(player, 3331, 1);
	}

	@Override
	public void onUntransform(L2Player player)
	{
		player.setVisName(null);
		player.setVisTitle(null);
		removeSkill(player, 3630); // Void Burst
		removeSkill(player, 3631); // Void Flow
		removeSkill(player, 3329);
		removeSkill(player, 3330);
		removeSkill(player, 3331);
	}
}
