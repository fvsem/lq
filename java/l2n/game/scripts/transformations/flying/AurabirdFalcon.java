package l2n.game.scripts.transformations.flying;

import l2n.commons.list.GArray;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 27.02.2010
 * @time 1:47:24
 */
public class AurabirdFalcon extends L2Transformation
{
	private static final GArray<L2ShortCut> SHORTCUTS = new GArray<L2ShortCut>();

	static
	{
		SHORTCUTS.add(new L2ShortCut(6, 10, 2, 885, 1));
		SHORTCUTS.add(new L2ShortCut(7, 10, 2, 894, 1));

		SHORTCUTS.add(new L2ShortCut(0, 10, 2, 884, 1));
		SHORTCUTS.add(new L2ShortCut(1, 10, 2, 886, 1));
		SHORTCUTS.add(new L2ShortCut(2, 10, 2, 888, 1));
		SHORTCUTS.add(new L2ShortCut(3, 10, 2, 890, 1));
		SHORTCUTS.add(new L2ShortCut(4, 10, 2, 891, 1));
		SHORTCUTS.add(new L2ShortCut(5, 10, 2, 911, 1));
	}

	private void addShortCuts(L2Player player)
	{
		for(L2ShortCut sc : player.getAllShortCuts())
			if(sc.page == 10)
				return;
		for(L2ShortCut sc : SHORTCUTS)
			player.registerShortCut(sc);
	}

	public AurabirdFalcon()
	{
		// id, colRadius, colHeight
		super(8, 38, 14.25);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() == 8 || !player.isCursedWeaponEquipped())
		{
			allowedSkills = new int[] { 884, 885, 886, 888, 890, 891, 894, 911, 932, 619 };
			// Disable all character skills.
			checkPlayerSkill(player);

			// give transformation skills
			transformedSkills(player);
			// регистрация скилов на панели
			addShortCuts(player);

			player.decayMe();
			player.setFlying(true);
			player.setLoc(player.getLoc().changeZ(32));
			player.spawnMe();
		}
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);

		player.decayMe();
		player.setFlying(false);
		player.setLoc(player.getLoc().correctGeoZ());
		player.spawnMe();
	}

	public void transformedSkills(L2Player player)
	{
		// Air Blink
		if(player.getLevel() >= 75)
			addSkill(player, 885, 1);

		// Exhilarate
		if(player.getLevel() >= 83)
			addSkill(player, 894, 1);

		int level = player.getLevel() - 74;
		if(level > 0)
		{
			addSkill(player, 884, level); // Air Assault
			addSkill(player, 886, level); // Air Shock Bomb
			addSkill(player, 888, level); // Energy Storm
			addSkill(player, 890, level); // Prodigious Flare
			addSkill(player, 891, level); // Energy Shot
			addSkill(player, 911, level); // Energy Burst
		}

		addSkill(player, 619, 1); // Transform Dispel
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 885); // Air Blink

		removeSkill(player, 894); // Exhilarate

		removeSkill(player, 884); // Air Assault
		removeSkill(player, 886); // Air Shock Bomb
		removeSkill(player, 888); // Energy Storm
		removeSkill(player, 890); // Prodigious Flare
		removeSkill(player, 891); // Energy Shot
		removeSkill(player, 911); // Energy Burst

		removeSkill(player, 619); // Transform Dispel
	}
}
