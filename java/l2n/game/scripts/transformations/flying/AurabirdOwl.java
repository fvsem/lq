package l2n.game.scripts.transformations.flying;

import l2n.commons.list.GArray;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 27.02.2010
 * @time 1:47:31
 */
public class AurabirdOwl extends L2Transformation
{
	private static final GArray<L2ShortCut> SHORTCUTS = new GArray<L2ShortCut>();

	static
	{
		SHORTCUTS.add(new L2ShortCut(0, 10, 2, 884, 1));
		SHORTCUTS.add(new L2ShortCut(1, 10, 2, 887, 1));
		SHORTCUTS.add(new L2ShortCut(2, 10, 2, 889, 1));
		SHORTCUTS.add(new L2ShortCut(3, 10, 2, 892, 1));
		SHORTCUTS.add(new L2ShortCut(4, 10, 2, 911, 1));
		SHORTCUTS.add(new L2ShortCut(5, 10, 2, 885, 1));
	}

	private void addShortCuts(L2Player player)
	{
		for(L2ShortCut sc : player.getAllShortCuts())
			if(sc.page == 10)
				return;
		for(L2ShortCut sc : SHORTCUTS)
			player.registerShortCut(sc);
	}

	public AurabirdOwl()
	{
		// id, colRadius, colHeight
		super(9, 40, 18.57);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 9 || player.isCursedWeaponEquipped())
			return;

		allowedSkills = new int[] { 884, 885, 887, 889, 892, 893, 895, 911, 932, 619 };
		// Disable all character skills.
		checkPlayerSkill(player);

		// give transformation skills
		transformedSkills(player);
		// регистрация скилов на панели
		addShortCuts(player);

		player.decayMe();
		player.setFlying(true);
		player.setLoc(player.getLoc().changeZ(32));
		player.spawnMe();
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);

		player.decayMe();
		player.setFlying(false);
		player.setLoc(player.getLoc().correctGeoZ());
		player.spawnMe();
	}

	public void transformedSkills(L2Player player)
	{
		// Air Blink
		if(player.getLevel() >= 75)
			addSkill(player, 885, 1);

		// Exhilarate
		if(player.getLevel() >= 83)
			addSkill(player, 895, 1);

		int level = player.getLevel() - 74;
		if(level > 0)
		{
			addSkill(player, 884, level); // Air Assault
			addSkill(player, 887, level); // Sky Clutch
			addSkill(player, 889, level); // Energy Storm
			addSkill(player, 892, level); // Energy Shot
			addSkill(player, 893, level); // Concentrated Energy Shot
			addSkill(player, 911, level); // Energy Burst
		}

		addSkill(player, 619, 1); // Transform Dispel
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 885); // Air Blink

		removeSkill(player, 895); // Exhilarate

		removeSkill(player, 884); // Air Assault
		removeSkill(player, 887); // Sky Clutch
		removeSkill(player, 889); // Energy Storm
		removeSkill(player, 892); // Energy Shot
		removeSkill(player, 893); // Concentrated Energy Shot
		removeSkill(player, 911); // Energy Burst

		removeSkill(player, 619); // Transform Dispel
	}
}
