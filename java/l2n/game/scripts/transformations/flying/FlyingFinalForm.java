package l2n.game.scripts.transformations.flying;

import l2n.commons.list.GArray;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 27.02.2010
 * @time 1:47:37
 */
public class FlyingFinalForm extends L2Transformation
{
	private static final GArray<L2ShortCut> SHORTCUTS = new GArray<L2ShortCut>();

	static
	{
		SHORTCUTS.add(new L2ShortCut(0, 10, 2, 539, 1));
		SHORTCUTS.add(new L2ShortCut(1, 10, 2, 540, 1));
		SHORTCUTS.add(new L2ShortCut(4, 10, 2, 953, 1));
		SHORTCUTS.add(new L2ShortCut(2, 10, 2, 1471, 1));
		SHORTCUTS.add(new L2ShortCut(3, 10, 2, 1472, 1));
	}

	private void addShortCuts(L2Player player)
	{
		for(L2ShortCut sc : player.getAllShortCuts())
			if(sc.page == 10)
				return;
		for(L2ShortCut sc : SHORTCUTS)
			player.registerShortCut(sc);
	}

	public FlyingFinalForm()
	{
		// id, colRadius, colHeight
		super(260, 9, 38);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 260 || player.isCursedWeaponEquipped())
			return;

		allowedSkills = new int[] { 932, 950, 951, 953, 1544, 1545, 619 };

		// Disable all character skills.
		checkPlayerSkill(player);

		// give transformation skills
		transformedSkills(player);
		// регистрация скилов на панели
		addShortCuts(player);

		player.decayMe();
		player.setFlying(true);
		player.setLoc(player.getLoc().changeZ(32));
		player.spawnMe();
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);

		player.decayMe();
		player.setFlying(false);
		player.setLoc(player.getLoc().correctGeoZ());
		player.spawnMe();
	}

	public void transformedSkills(L2Player player)
	{
		addSkill(player, 953, 1); // Life to Soul
		addSkill(player, 1545, 1); // Soul Sucking

		int level = player.getLevel() - 78;
		if(level > 0)
		{
			addSkill(player, 950, level); // Nail Attack
			addSkill(player, 951, level); // Wing Assault
			addSkill(player, 1544, level); // Death Beam
		}

		addSkill(player, 619, 1); // Transform Dispel
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 953); // Life to Soul
		removeSkill(player, 1545);// Soul Sucking

		removeSkill(player, 950); // Nail Attack
		removeSkill(player, 951); // Wing Assault
		removeSkill(player, 1544); // Death Beam

		removeSkill(player, 619); // Transform Dispel
	}
}
