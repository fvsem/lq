package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Zombie extends L2DefaultTransformation
{
	public Zombie()
	{
		// id, colRadius, colHeight
		super(303, 11.0, 25.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
