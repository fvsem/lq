package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Pixy extends L2DefaultTransformation
{
	public Pixy()
	{
		// id, colRadius, colHeight
		super(304, 5.0, 25.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
