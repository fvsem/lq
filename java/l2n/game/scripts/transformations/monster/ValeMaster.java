package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class ValeMaster extends L2DefaultTransformation
{
	public ValeMaster()
	{
		// id, colRadius, colHeight
		super(4, 12.0, 40.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() >= 76)
		{
			addSkill(player, 742, 3);
			addSkill(player, 743, 3);
			addSkill(player, 744, 3);
			addSkill(player, 745, 3);
		}
		else if(player.getLevel() >= 73)
		{
			addSkill(player, 742, 2);
			addSkill(player, 743, 2);
			addSkill(player, 744, 2);
			addSkill(player, 745, 2);
		}
		else if(player.getLevel() >= 70)
		{
			addSkill(player, 742, 1);
			addSkill(player, 743, 1);
			addSkill(player, 744, 1);
			addSkill(player, 745, 1);
		}
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 742);
		removeSkill(player, 743);
		removeSkill(player, 744);
		removeSkill(player, 745);
	}
}
