package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DollBlader extends L2DefaultTransformation
{
	public DollBlader()
	{
		// id, colRadius, colHeight
		super(7, 6.0, 12.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() >= 76)
		{
			addSkill(player, 754, 1);
			addSkill(player, 752, 3);
			addSkill(player, 753, 3);
		}
		else if(player.getLevel() >= 73)
		{
			addSkill(player, 754, 1);
			addSkill(player, 752, 2);
			addSkill(player, 753, 2);
		}
		else if(player.getLevel() >= 70)
		{
			addSkill(player, 754, 1);
			addSkill(player, 752, 1);
			addSkill(player, 753, 1);
		}
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 754);
		removeSkill(player, 752);
		removeSkill(player, 753);
	}
}
