package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class SaberToothTiger extends L2DefaultTransformation
{
	public SaberToothTiger()
	{
		// id, colRadius, colHeight
		super(5, 34.0, 28.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 619, 1); // Transfrom Dispel
		addSkill(player, 5491, 1); // Decrease Bow/Crossbow Attack Speed
		addSkill(player, 746, 1); // Saber Tooth Tiger Bite
		addSkill(player, 747, 1); // Saber Tooth Tiger Fear
		addSkill(player, 748, 1); // Saber Tooth Tiger Sprint
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 619); // Transfrom Dispel
		removeSkill(player, 5491); // Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 746); // Saber Tooth Tiger Bite
		removeSkill(player, 747); // / Saber Tooth Tiger Fear
		removeSkill(player, 748); // Saber Tooth Tiger Sprint
	}
}
