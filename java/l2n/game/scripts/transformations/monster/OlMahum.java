package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class OlMahum extends L2DefaultTransformation
{
	public OlMahum()
	{
		// id, colRadius, colHeight
		super(6, 23.0, 61.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() >= 76)
		{
			addSkill(player, 750, 1);
			addSkill(player, 749, 3);
			addSkill(player, 751, 3);
		}
		else if(player.getLevel() >= 73)
		{
			addSkill(player, 750, 1);
			addSkill(player, 749, 2);
			addSkill(player, 751, 2);
		}
		else if(player.getLevel() >= 70)
		{
			addSkill(player, 750, 1);
			addSkill(player, 749, 1);
			addSkill(player, 751, 1);
		}
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 750);
		removeSkill(player, 749);
		removeSkill(player, 751);
	}
}
