package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Buffalo extends L2DefaultTransformation
{
	public Buffalo()
	{
		// id, colRadius, colHeight
		super(103, 22.0, 31.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
