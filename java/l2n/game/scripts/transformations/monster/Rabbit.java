package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Rabbit extends L2DefaultTransformation
{
	public Rabbit()
	{
		// id, colRadius, colHeight
		super(105, 5.0, 4.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 629, 1);
		addSkill(player, 630, 1);

	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 629);
		removeSkill(player, 630);
	}
}
