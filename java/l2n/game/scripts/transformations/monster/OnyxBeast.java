package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class OnyxBeast extends L2DefaultTransformation
{
	public OnyxBeast()
	{
		// id, colRadius, colHeight
		super(1, 14.0, 15.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 584, 1); // Power Claw
		addSkill(player, 585, 1); // Fast Moving
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 584); // Power Claw
		removeSkill(player, 585); // Fast Moving
	}
}
