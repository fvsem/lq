package l2n.game.scripts.transformations.monster;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DoomWraith extends L2DefaultTransformation
{
	public DoomWraith()
	{
		// id, colRadius, colHeight
		super(2, 8.0, 22.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 586, 2); // Rolling Attack
		addSkill(player, 588, 2); // Curse of Darkness
		addSkill(player, 587, 2); // Dig Attack
		addSkill(player, 589, 2); // Darkness Energy Drain
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 586); // Rolling Attack
		removeSkill(player, 588); // Curse of Darkness
		removeSkill(player, 587); // Dig Attack
		removeSkill(player, 589); // Darkness Energy Drain
	}
}
