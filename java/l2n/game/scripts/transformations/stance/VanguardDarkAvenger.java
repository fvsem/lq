package l2n.game.scripts.transformations.stance;

import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

public class VanguardDarkAvenger extends L2Transformation
{
	public VanguardDarkAvenger()
	{
		// id, colRadius, colHeight
		super(313, 8.0, 23.0);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() == 313 && !player.isCursedWeaponEquipped())
		{
			int level = player.getLevel();
			if(level > 43)
			{
				allowedSkills = new int[] { 18, 28, 65, 86, 144, 283, 401, 815, 817, 838 };
				if(level > 48)
					allowedSkills = new int[] { 18, 28, 65, 86, 144, 283, 401, 815, 817, 838, 958 };
				if(level > 57)
					allowedSkills = new int[] { 18, 28, 65, 86, 144, 283, 401, 815, 817, 838, 956, 958 };
			}
			else
				allowedSkills = new int[] { 18, 28, 65, 86, 283, 401, 838 };

			// Disable all character skills.
			checkPlayerSkill(player);

			// give transformation skills
			transformedSkills(player);
		}
	}

	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() > 43)
		{
			int level = player.getLevel() - 43;
			addSkill(player, 815, level); // Blade Hurricane
			addSkill(player, 817, level); // Double Strike
			addSkill(player, 144, level); // Dual Weapon Mastery

			if(player.getLevel() > 48) // Triple Blade Slash
				addSkill(player, 958, player.getLevel() - 48);

			if(player.getLevel() > 73) // Boost Morale 3
				addSkill(player, 956, 3);
			else if(player.getLevel() > 65) // Boost Morale 2
				addSkill(player, 956, 2);
			else if(player.getLevel() > 57) // Boost Morale 1
				addSkill(player, 956, 1);
		}
		addSkill(player, 838, 1); // Switch Stance
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 815); // Blade Hurricane
		removeSkill(player, 817); // Double Strike
		removeSkill(player, 144); // Dual Sword Mastery

		removeSkill(player, 958); // Triple Blade Slash
		removeSkill(player, 956); // Boost Morale

		removeSkill(player, 838); // Switch Stance
	}
}
