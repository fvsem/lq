package l2n.game.scripts.transformations.stance;

import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

public class InquisitorElvenElder extends L2Transformation
{
	public InquisitorElvenElder()
	{
		// id, colRadius, colHeight
		super(317, 7.0, 24.0);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() == 317 && !player.isCursedWeaponEquipped())
		{
			int level = player.getLevel();
			if(level > 43)
				allowedSkills = new int[] { 838, 1523, 1528, 1524, 1525, 1430, 1043, 1400, 1303 };
			else
				allowedSkills = new int[] { 838, 1430, 1043, 1400, 1303 };

			// Disable all character skills.
			checkPlayerSkill(player);
			// give transformation skills
			transformedSkills(player);
			return;
		}
	}

	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() > 43)
		{
			int level = player.getLevel() - 43;
			addSkill(player, 1523, level); // Divine Punishment
			addSkill(player, 1524, level); // Surrender to the Holy
			addSkill(player, 1525, level); // Divine Curse
			addSkill(player, 1528, level); // Divine Flash
		}

		addSkill(player, 838, 1); // Switch Stance
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 1523); // Divine Punishment
		removeSkill(player, 1524); // Surrender to the Holy
		removeSkill(player, 1525); // Divine Curse
		removeSkill(player, 1528); // Divine Flash
		removeSkill(player, 838); // Switch Stance
	}
}
