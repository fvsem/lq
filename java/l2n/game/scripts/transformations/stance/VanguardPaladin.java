package l2n.game.scripts.transformations.stance;

import l2n.game.model.L2Transformation;
import l2n.game.model.actor.L2Player;

public class VanguardPaladin extends L2Transformation
{
	public VanguardPaladin()
	{
		// id, colRadius, colHeight
		super(312, 8.0, 23.0);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() == 312 && !player.isCursedWeaponEquipped())
		{
			int level = player.getLevel();
			if(level > 43)
			{
				allowedSkills = new int[] { 18, 28, 196, 197, 293, 400, 406, 814, 816, 838 };

				if(level > 48)
					allowedSkills = new int[] { 18, 28, 196, 197, 293, 400, 406, 814, 816, 838, 957 };

				if(level > 57)
					allowedSkills = new int[] { 18, 28, 196, 197, 293, 400, 406, 814, 816, 838, 956, 957 };
			}
			else
				allowedSkills = new int[] { 18, 28, 196, 197, 400, 406, 838 };

			// Disable all character skills.
			checkPlayerSkill(player);
			// give transformation skills
			transformedSkills(player);
		}
	}

	public void transformedSkills(L2Player player)
	{
		if(player.getLevel() > 43)
		{
			int level = player.getLevel() - 43;
			addSkill(player, 814, level); // Full Swing
			addSkill(player, 816, level); // Cleave
			addSkill(player, 293, level); // Two-handed Weapon Mastery

			if(player.getLevel() > 48)
			{
				addSkill(player, 957, player.getLevel() - 48); // Guillotine Attack

				if(player.getLevel() > 73) // Boost Morale 3
					addSkill(player, 956, 3);
				else if(player.getLevel() > 65) // Boost Morale 2
					addSkill(player, 956, 2);
				else if(player.getLevel() > 57) // Boost Morale 1
					addSkill(player, 956, 1);
			}
		}
		addSkill(player, 838, 1); // Switch Stance
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		// remove transformation skills
		removeSkills(player);
	}

	public void removeSkills(L2Player player)
	{
		removeSkill(player, 814); // Full Swing
		removeSkill(player, 816); // Power Divide
		removeSkill(player, 293); // Two-handed Weapon Mastery

		removeSkill(player, 957); // Triple Blade Slash
		removeSkill(player, 956); // Boost Morale

		removeSkill(player, 838); // Switch Stance
	}
}
