package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Ranku extends L2DefaultTransformation
{
	public Ranku()
	{
		// id, colRadius, colHeight
		super(309, 13.0, 29.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 731, 1); // Ranku Dark Explosion
		addSkill(player, 732, 1); // Ranku Stun Attack
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 731); // Ranku Dark Explosion
		removeSkill(player, 732); // Ranku Stun Attack
	}
}
