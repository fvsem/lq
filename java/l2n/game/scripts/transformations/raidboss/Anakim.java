package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Anakim extends L2DefaultTransformation
{
	public Anakim()
	{
		// id, colRadius, colHeight
		super(306, 15.5, 29.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 720, 2); // Anakim Holy Light Burst
		addSkill(player, 721, 2); // Anakim Energy Attack
		addSkill(player, 722, 2); // Anakim Holy Beam
		addSkill(player, 723, 1); // Anakim Sunshine
		addSkill(player, 724, 1); // Anakim Clans
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 720); // Anakim Holy Light Burst
		removeSkill(player, 721); // Anakim Energy Attack
		removeSkill(player, 722); // Anakim Holy Beam
		removeSkill(player, 723); // Anakim Sunshine
		removeSkill(player, 724); // Anakim Clans
	}
}
