package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Zaken extends L2DefaultTransformation
{
	public Zaken()
	{
		// id, colRadius, colHeight
		super(305, 16.0, 32.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 715, 4); // Zaken Energy Drain
		addSkill(player, 716, 4); // Zaken Hold
		addSkill(player, 717, 4); // Zaken Concentrated Attack
		addSkill(player, 718, 4); // Zaken Dancing Sword
		addSkill(player, 719, 1); // Zaken Vampiric Rage
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 715); // Zaken Energy Drain
		removeSkill(player, 716); // Zaken Hold
		removeSkill(player, 717); // Zaken Concentrated Attack
		removeSkill(player, 718); // Zaken Dancing Sword
		removeSkill(player, 719); // Zaken Vampiric Rage
	}
}
