package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Kiyachi extends L2DefaultTransformation
{
	public Kiyachi()
	{
		// id, colRadius, colHeight
		super(310, 12.0, 29.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 733, 1); // Kechi Double Cutter
		addSkill(player, 734, 1); // Kechi Air Blade
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 733); // Kechi Double Cutter
		removeSkill(player, 734); // Kechi Air Blade
	}
}
