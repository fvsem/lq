package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DemonPrince extends L2DefaultTransformation
{
	public DemonPrince()
	{
		// id, colRadius, colHeight
		super(311, 33.0, 49.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 735, 1); // Devil Spinning Weapon
		addSkill(player, 736, 1); // Devil Seed
		addSkill(player, 737, 1); // Devil Ultimate Defense
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 735); // Devil Spinning Weapon
		removeSkill(player, 736); // Devil Seed
		removeSkill(player, 737); // Devil Ultimate Defense
	}
}
