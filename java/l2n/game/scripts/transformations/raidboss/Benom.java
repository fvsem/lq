package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Benom extends L2DefaultTransformation
{
	public Benom()
	{
		// id, colRadius, colHeight
		super(307, 10.0, 57.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 725, 2); // Venom Power Smash
		addSkill(player, 726, 2); // Venom Sonic Storm
		addSkill(player, 727, 1); // Venom Disillusion
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 725); // Venom Power Smash
		removeSkill(player, 726); // Venom Sonic Storm
		removeSkill(player, 727); // Venom Disillusion
	}
}
