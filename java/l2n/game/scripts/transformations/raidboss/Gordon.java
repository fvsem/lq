package l2n.game.scripts.transformations.raidboss;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Gordon extends L2DefaultTransformation
{
	public Gordon()
	{
		// id, colRadius, colHeight
		super(308, 43.0, 46.6);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 728, 1); // Gordon Beast Attack
		addSkill(player, 729, 1); // Gordon Sword Stab
		addSkill(player, 730, 1); // Gordon Press
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 728); // Gordon Beast Attack
		removeSkill(player, 729); // Gordon Sword Stab
		removeSkill(player, 730); // Gordon Press
	}
}
