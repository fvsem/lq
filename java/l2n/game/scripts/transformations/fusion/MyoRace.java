package l2n.game.scripts.transformations.fusion;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class MyoRace extends L2DefaultTransformation
{
	public MyoRace()
	{
		// id, colRadius, colHeight
		super(219, 10, 23);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() == 219 && !player.isCursedWeaponEquipped())
		{
			allowedSkills = new int[] { 896, 897, 898, 899, 900, 5491, 619 };

			// Disable all character skills.
			checkPlayerSkill(player);
			transformedSkills(player);
		}
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Rolling Step (up to 6 levels)
		addSkill(player, 896, 4);
		// Double Blast (up to 6 levels)
		addSkill(player, 897, 4);
		// Tornado Slash (up to 6 levels)
		addSkill(player, 898, 4);
		// Cat Roar (up to 6 levels)
		addSkill(player, 899, 4);
		// Energy Blast (up to 6)
		addSkill(player, 900, 4);
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Rolling Step (up to 6 levels)
		removeSkill(player, 896);
		// Double Blast (up to 6 levels)
		removeSkill(player, 897);
		// Tornado Slash (up to 6 levels)
		removeSkill(player, 898);
		// Cat Roar (up to 6 levels)
		removeSkill(player, 899);
		// Energy Blast (up to 6)
		removeSkill(player, 900);
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
