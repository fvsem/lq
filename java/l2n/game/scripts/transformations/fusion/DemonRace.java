package l2n.game.scripts.transformations.fusion;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DemonRace extends L2DefaultTransformation
{
	public DemonRace()
	{
		// id, colRadius, colHeight
		super(221, 11, 27);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 221 || player.isCursedWeaponEquipped())
			return;

		allowedSkills = new int[] { 901, 902, 903, 904, 905, 5491, 619 };

		// Disable all character skills.
		checkPlayerSkill(player);
		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Dark Strike (up to 6)
		addSkill(player, 901, 4);
		// Bursting Flame (up to 6)
		addSkill(player, 902, 4);
		// Stratum Explosion (up to 6)
		addSkill(player, 903, 4);
		// Corpse Burst (up to 6)
		addSkill(player, 904, 4);
		// Dark Detonation (up to 6)
		addSkill(player, 905, 4);
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);
	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Dark Strike
		removeSkill(player, 901);
		// Bursting Flame
		removeSkill(player, 902);
		// Stratum Explosion
		removeSkill(player, 903);
		// Corpse Burst
		removeSkill(player, 904);
		// Dark Detonation
		removeSkill(player, 905);
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);
	}
}
