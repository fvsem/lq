package l2n.game.scripts.transformations.fusion;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Unicorniun extends L2DefaultTransformation
{
	public Unicorniun()
	{
		// id, colRadius, colHeight
		super(220, 8, 30);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 220 || player.isCursedWeaponEquipped())
			return;

		allowedSkills = new int[] { 906, 907, 908, 909, 910, 5491, 619 };

		// Disable all character skills.
		checkPlayerSkill(player);
		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Lance Step (up to 6 levels)
		addSkill(player, 906, 4);
		// Aqua Blast (up to 6 levels)
		addSkill(player, 907, 4);
		// Spin Slash (up to 6 levels)
		addSkill(player, 908, 4);
		// Ice Focus (up to 6 levels)
		addSkill(player, 909, 4);
		// Water Jet (up to 6 levels)
		addSkill(player, 910, 4);
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		allowedSkills = null;
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Lance Step (up to 6 levels)
		removeSkill(player, 906);
		// Aqua Blast (up to 6 levels)
		removeSkill(player, 907);
		// Spin Slash (up to 6 levels)
		removeSkill(player, 908);
		// Ice Focus (up to 6 levels)
		removeSkill(player, 909);
		// Water Jet (up to 6 levels)
		removeSkill(player, 910);
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
