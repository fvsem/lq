package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Native extends L2DefaultTransformation
{
	public Native()
	{
		// id, colRadius, colHeight
		super(101, 8.0, 23.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{}

	@Override
	public void removeSkills(L2Player player)
	{}
}
