package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class AquaElf extends L2DefaultTransformation
{
	public AquaElf()
	{
		// id, colRadius, colHeight
		super(125, 12, 27.50);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 125 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Transform Dispel
		removeSkill(player, 619);

	}
}
