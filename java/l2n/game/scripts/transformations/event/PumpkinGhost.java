package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class PumpkinGhost extends L2DefaultTransformation
{
	public PumpkinGhost()
	{
		// id, colRadius, colHeight
		super(108, 12, 18);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 108 || player.isCursedWeaponEquipped())
			return;
		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5437, 2);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5437);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
