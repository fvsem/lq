package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class EpicQuestNative extends L2DefaultTransformation
{
	public EpicQuestNative()
	{
		// id, colRadius, colHeight
		super(124, 8, 23.5);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 124 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Dissonance
		addSkill(player, 5437, 1);
		// Swift Dash
		addSkill(player, 961, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Dissonance
		removeSkill(player, 5437);
		// Swift Dash
		removeSkill(player, 961);

	}
}
