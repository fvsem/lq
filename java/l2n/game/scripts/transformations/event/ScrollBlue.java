package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

/**
 * TODO: Buffs disappear once you get transformed, but reappear after the transformed state wears off. Skills involved in the minigame but are not assigned directly to players: Flip Nearby Blocks - 5847 - For Flip Block, there are two skills, one for
 * each side (makes sense). For this, there is only one skill. Thus it is probably not assigned to the transformation. Block Trigger Slow - 5848 - This may be assigned to players, unsure. Decrease Speed - 5849 - This is possibly assigned to all
 * players to set all players to the same running speed for the duration of the game. Block Trigger Stun - 5849 - From L2Vault: "The squares gives drops of "bond" and "landmine
 * ". I wasn't able to figure out what the bond did as it wasn't anything that seemed to go into your inventory. However, Landmine did appear in your inventory which allows you to use it before flipping a square which will give the other team a state of stun when they attempt to flip the same square (from what I can gather, it all happens so quickly ;) "
 * Shock - 5851 - Stun effect from 5849 More Info: http://l2vault.ign.com/wiki/index.php/Handy%E2%80%99s_Block_Checker
 */
public class ScrollBlue extends L2DefaultTransformation
{
	public ScrollBlue()
	{
		// id, colRadius, colHeight
		super(122, 12, 28);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 122 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Flip Block
		addSkill(player, 5852, 1);
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		// addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Flip Block
		removeSkill(player, 5852);
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		// removeSkill(player, 619, 1);

	}
}
