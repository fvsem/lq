package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Teleporter extends L2DefaultTransformation
{
	public Teleporter()
	{
		// id, colRadius, colHeight
		super(319, 8, 24);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 319 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		/*
		 * Commented out until we figure out how to remove the skills properly. What happens if a player transforms at level 40, gets the level 40 version of the skill, then somehow levels up? Then when we untransform, the script will look for the
		 * level 41 version of the skill, right? Or will it still remove the level 40 skill? Needs to be tested. // Gatekeeper Aura Flare addSkill(player, 5656, playergetLevel()); // Gatekeeper Prominence addSkill(player, 5657, playergetLevel()); //
		 * Gatekeeper Flame Strike addSkill(player, 5658, playergetLevel()); // Gatekeeper Berserker Spirit (there are two levels, when do players get access to level 2?) addSkill(player, 5659, 1);
		 */
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Cancel Gatekeeper Transformation
		addSkill(player, 8248, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
/*
 * Commented out until we figure out how to remove the skills properly. What happens if a player transforms at level 40, gets the level 40 version of the skill, then somehow levels up? Then when we untransform, the script will look for the level 41
 * version of the skill, right? Or will it still remove the level 40 skill? Needs to be tested. // Gatekeeper Aura Flare removeSkill(player, 5656, playergetLevel()); // Gatekeeper Prominence removeSkill(player, 5657, playergetLevel()); // Gatekeeper
 * Flame Strike removeSkill(player, 5658, playergetLevel()); // Gatekeeper Berserker Spirit (there are two levels, when do players get access to level 2?) removeSkill(player, 5659, 1);
 */
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Cancel Gatekeeper Transformation
		removeSkill(player, 8248);

	}
}
