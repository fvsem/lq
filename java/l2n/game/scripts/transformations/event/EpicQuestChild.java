package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class EpicQuestChild extends L2DefaultTransformation
{
	public EpicQuestChild()
	{
		// id, colRadius, colHeight
		super(112, 5, 12.3);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 112 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Dissonance
		addSkill(player, 5437, 1);
		// Race Running
		addSkill(player, 960, 1);
	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Dissonance
		removeSkill(player, 5437);
		// Race Running
		removeSkill(player, 960);
	}
}
