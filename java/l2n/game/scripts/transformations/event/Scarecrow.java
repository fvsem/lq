package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Scarecrow extends L2DefaultTransformation
{
	public Scarecrow()
	{
		// id, colRadius, colHeight
		super(115, 13, 30);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 115 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Fake Attack
		addSkill(player, 940, 1);
		// Special Motion
		addSkill(player, 942, 1);
		// Dissonance
		addSkill(player, 5437, 2);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Fake Attack
		removeSkill(player, 940);
		// Special Motion
		removeSkill(player, 942);
		// Dissonance
		removeSkill(player, 5437);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
