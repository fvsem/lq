package l2n.game.scripts.transformations.event;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Tow extends L2DefaultTransformation
{
	public Tow()
	{
		// id, colRadius, colHeight
		super(117, 4.4, 8.25);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 117 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
