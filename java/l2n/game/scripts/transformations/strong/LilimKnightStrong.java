package l2n.game.scripts.transformations.strong;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class LilimKnightStrong extends L2DefaultTransformation
{
	public LilimKnightStrong()
	{
		// id, colRadius, colHeight
		super(207, 8.0, 24.4);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 568, 4); // Attack Buster
		addSkill(player, 569, 4); // Attack Storm
		addSkill(player, 570, 4); // Attack Rage
		addSkill(player, 571, 4); // Poison Dust
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 568); // Attack Buster
		removeSkill(player, 569); // Attack Storm
		removeSkill(player, 570); // Attack Rage
		removeSkill(player, 571); // Poison Dust
	}
}
