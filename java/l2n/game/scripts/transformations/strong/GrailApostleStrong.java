package l2n.game.scripts.transformations.strong;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GrailApostleStrong extends L2DefaultTransformation
{
	public GrailApostleStrong()
	{
		// id, colRadius, colHeight
		super(201, 8.0, 30.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 559, 4); // Spear
		addSkill(player, 560, 4); // Power Slash
		addSkill(player, 561, 4); // Bless of Angel
		addSkill(player, 562, 4); // Wind of Angel
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 559); // Spear
		removeSkill(player, 560); // Power Slash
		removeSkill(player, 561); // Bless of Angel
		removeSkill(player, 562); // Wind of Angel
	}
}
