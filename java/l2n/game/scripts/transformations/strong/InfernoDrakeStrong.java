package l2n.game.scripts.transformations.strong;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class InfernoDrakeStrong extends L2DefaultTransformation
{
	public InfernoDrakeStrong()
	{
		// id, colRadius, colHeight
		super(213, 8.0, 22.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 576, 4); // Paw Strike
		addSkill(player, 577, 4); // Fire Breath
		addSkill(player, 578, 4); // Blaze Quake
		addSkill(player, 579, 4); // Fire Armor
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 576); // Paw Strike
		removeSkill(player, 577); // Fire Breath
		removeSkill(player, 578); // Blaze Quake
		removeSkill(player, 579); // Fire Armor
	}
}
