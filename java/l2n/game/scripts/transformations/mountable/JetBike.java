package l2n.game.scripts.transformations.mountable;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class JetBike extends L2DefaultTransformation
{
	public JetBike()
	{
		// id, colRadius, colHeight
		super(20001, 25, 22.5);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 20001 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Dismount
		addSkill(player, 839, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Dismount
		removeSkill(player, 839);

	}
}
