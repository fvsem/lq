package l2n.game.scripts.transformations.mountable;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class LightPurpleManedHorse extends L2DefaultTransformation
{
	public LightPurpleManedHorse()
	{
		// id, colRadius, colHeight
		super(106, 31, 32.5);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 106 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Dismount
		addSkill(player, 839, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Dismount
		removeSkill(player, 839);

	}
}
