package l2n.game.scripts.transformations.battle;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class ArcherCaptain extends L2DefaultTransformation
{
	public ArcherCaptain()
	{
		// id, colRadius, colHeight
		super(17, 10, 23.5);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 17 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);
	}
}
