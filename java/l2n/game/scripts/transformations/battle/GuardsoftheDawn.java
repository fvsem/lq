package l2n.game.scripts.transformations.battle;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GuardsoftheDawn extends L2DefaultTransformation
{
	public GuardsoftheDawn()
	{
		// id, colRadius, colHeight
		super(113, 8, 23.5);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 113 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Dissonance
		addSkill(player, 5437, 1);
		// Guard's March
		addSkill(player, 962, 1);
		// Guard's Ambush
		addSkill(player, 963, 1);
	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Dissonance
		removeSkill(player, 5437);
		removeSkill(player, 962);
		removeSkill(player, 963);
	}
}
