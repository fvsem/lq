package l2n.game.scripts.transformations.battle;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class FortressCaptain extends L2DefaultTransformation
{
	public FortressCaptain()
	{
		// id, colRadius, colHeight
		super(21, 14, 27);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 21 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		addSkill(player, 5491, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Decrease Bow/Crossbow Attack Speed
		removeSkill(player, 5491);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
