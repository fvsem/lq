package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Kamael extends L2DefaultTransformation
{
	public Kamael()
	{
		// id, colRadius, colHeight
		super(251, 9.0, 30.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 539, 1); // Nail Attack
		addSkill(player, 540, 1); // Wing Assault
		addSkill(player, 1471, 1); // Soul Sucking
		addSkill(player, 1472, 1); // Death Beam
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 539); // Nail Attack
		removeSkill(player, 540); // Wing Assault
		removeSkill(player, 1471); // Soul Sucking
		removeSkill(player, 1472); // Death Beam
	}
}
