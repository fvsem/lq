package l2n.game.scripts.transformations.divine;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DivineKnight extends L2DefaultTransformation
{
	public DivineKnight()
	{
		// id, colRadius, colHeight
		super(252, 12.0, 30.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 680, 1); // Divine Knight Hate
		addSkill(player, 681, 1); // Divine Knight Hate Aura
		addSkill(player, 682, 1); // Divine Knight Stun Attack
		addSkill(player, 683, 1); // Divine Knight Thunder Storm
		addSkill(player, 684, 1); // Divine Knight Ultimate Defense
		addSkill(player, 685, 1); // Sacrifice Knight
		addSkill(player, 795, 1); // Divine Knight Brandish
		addSkill(player, 796, 1); // Divine Knight Explosion
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 680); // Divine Knight Hate
		removeSkill(player, 681); // Divine Knight Hate Aura
		removeSkill(player, 682); // Divine Knight Stun Attack
		removeSkill(player, 683); // Divine Knight Thunder Storm
		removeSkill(player, 684); // Divine Knight Ultimate Defense
		removeSkill(player, 685); // Sacrifice Knight
		removeSkill(player, 795); // Divine Knight Brandish
		removeSkill(player, 796); // Divine Knight Explosion
	}
}
