package l2n.game.scripts.transformations.divine;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DivineRogue extends L2DefaultTransformation
{
	public DivineRogue()
	{
		// id, colRadius, colHeight
		super(254, 12.0, 30.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 686, 1); // Divine Rogue Stun Shot
		addSkill(player, 687, 1); // Divine Rogue Double Shot
		addSkill(player, 688, 1); // Divine Rogue Bleed Attack
		addSkill(player, 689, 1); // Divine Rogue Deadly Blow
		addSkill(player, 690, 1); // Divine Rogue Agility
		addSkill(player, 691, 1); // Sacrifice Rogue
		addSkill(player, 797, 1); // Divine Rogue Piercing Attack
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 686); // Divine Rogue Stun Shot
		removeSkill(player, 687); // Divine Rogue Double Shot
		removeSkill(player, 688); // Divine Rogue Bleed Attack
		removeSkill(player, 689); // Divine Rogue Deadly Blow
		removeSkill(player, 690); // Divine Rogue Agility
		removeSkill(player, 691); // Sacrifice Rogue
		removeSkill(player, 797); // Divine Rogue Piercing Attack
	}
}
