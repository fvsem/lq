package l2n.game.scripts.transformations.divine;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DivineEnchanter extends L2DefaultTransformation
{
	public DivineEnchanter()
	{
		// id, colRadius, colHeight
		super(257, 12.0, 24.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 704, 1); // Divine Enchanter Water Spirit
		addSkill(player, 705, 1); // Divine Enchanter Fire Spirit
		addSkill(player, 706, 1); // Divine Enchanter Wind Spirit
		addSkill(player, 707, 1); // Divine Enchanter Hero Spirit
		addSkill(player, 708, 1); // Divine Enchanter Mass Binding
		addSkill(player, 709, 1); // Sacrifice Enchanter
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 704); // Divine Enchanter Water Spirit
		removeSkill(player, 705); // Divine Enchanter Fire Spirit
		removeSkill(player, 706); // Divine Enchanter Wind Spirit
		removeSkill(player, 707); // Divine Enchanter Hero Spirit
		removeSkill(player, 708); // Divine Enchanter Mass Binding
		removeSkill(player, 709); // Sacrifice Enchanter
	}
}
