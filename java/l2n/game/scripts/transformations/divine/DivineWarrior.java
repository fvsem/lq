package l2n.game.scripts.transformations.divine;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DivineWarrior extends L2DefaultTransformation
{
	public DivineWarrior()
	{
		// id, colRadius, colHeight
		super(253, 12.0, 30.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 675, 1); // Cross Slash
		addSkill(player, 676, 1); // Sonic Blaster
		addSkill(player, 677, 1); // Transfixition of Earth
		addSkill(player, 678, 1); // Divine Warrior War Cry
		addSkill(player, 679, 1); // Sacrifice Warrior
		addSkill(player, 798, 1); // Divine Warrior Assault Attack
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 675); // Cross Slash
		removeSkill(player, 676); // Sonic Blaster
		removeSkill(player, 677); // Transfixition of Earth
		removeSkill(player, 678); // Divine Warrior War Cry
		removeSkill(player, 679); // Sacrifice Warrior
		removeSkill(player, 798); // Divine Warrior Assault Attack
	}
}
