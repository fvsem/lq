package l2n.game.scripts.transformations.divine;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DivineSummoner extends L2DefaultTransformation
{
	public DivineSummoner()
	{
		// id, colRadius, colHeight
		super(258, 12.0, 24.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 710, 1); // Divine Summoner Summon Divine Beast
		addSkill(player, 711, 1); // Divine Summoner Transfer Pain
		addSkill(player, 712, 1); // Divine Summoner Final Servitor
		addSkill(player, 713, 1); // Divine Summoner Servitor Hill
		addSkill(player, 714, 1); // Sacrifice Summoner
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 710); // Divine Summoner Summon Divine Beast
		removeSkill(player, 711); // Divine Summoner Transfer Pain
		removeSkill(player, 712); // Divine Summoner Final Servitor
		removeSkill(player, 713); // Divine Summoner Servitor Hill
		removeSkill(player, 714); // Sacrifice Summoner

		// Убираем Divine Beast на всякий
		if(player.getPet() != null)
			player.getPet().unSummon();
	}
}
