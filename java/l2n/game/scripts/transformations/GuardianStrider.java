package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GuardianStrider extends L2DefaultTransformation
{
	public GuardianStrider()
	{
		// id, colRadius, colHeight
		super(123, 13, 40);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 123 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Dismount
		addSkill(player, 839, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Dismount
		removeSkill(player, 839);

	}
}
