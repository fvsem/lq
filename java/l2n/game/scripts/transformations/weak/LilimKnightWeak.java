package l2n.game.scripts.transformations.weak;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class LilimKnightWeak extends L2DefaultTransformation
{
	public LilimKnightWeak()
	{
		// id, colRadius, colHeight
		super(209, 8.0, 24.4);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 568, 2); // Attack Buster
		// addSkill(player, 569, 2); // Attack Storm - Too few charges reachable to use this skill
		addSkill(player, 570, 2); // Attack Rage
		addSkill(player, 571, 2); // Poison Dust
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 568); // Attack Buster
		// removeSkill(player, 569); // Attack Storm - Too few charges reachable to use this skill
		removeSkill(player, 570); // Attack Rage
		removeSkill(player, 571); // Poison Dust
	}
}
