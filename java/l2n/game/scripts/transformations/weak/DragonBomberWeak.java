package l2n.game.scripts.transformations.weak;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DragonBomberWeak extends L2DefaultTransformation
{
	public DragonBomberWeak()
	{
		// id, colRadius, colHeight
		super(218, 8.0, 22.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 580, 2); // Death Blow
		addSkill(player, 581, 2); // Sand Cloud
		addSkill(player, 582, 2); // Scope Bleed
		addSkill(player, 583, 2); // Assimilation
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 580); // Death Blow
		removeSkill(player, 581); // Sand Cloud
		removeSkill(player, 582); // Scope Bleed
		removeSkill(player, 583); // Assimilation
	}
}
