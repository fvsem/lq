package l2n.game.scripts.transformations.weak;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class GolemGuardianWeak extends L2DefaultTransformation
{
	public GolemGuardianWeak()
	{
		// id, colRadius, colHeight
		super(212, 8.0, 23.5);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 572, 2); // Double Slasher
		addSkill(player, 573, 2); // Earthquake
		addSkill(player, 574, 2); // Bomb Installation
		addSkill(player, 575, 2); // Steel Cutter
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 572); // Double Slasher
		removeSkill(player, 573); // Earthquake
		removeSkill(player, 574); // Bomb Installation
		removeSkill(player, 575); // Steel Cutter
	}
}
