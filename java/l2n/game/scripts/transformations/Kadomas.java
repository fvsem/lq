package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class Kadomas extends L2DefaultTransformation
{
	public Kadomas()
	{
		// id, colRadius, colHeight
		super(20000, 24, 14);
	}

	@Override
	public void onTransform(L2Player player)
	{
		if(player.getTransformationId() != 20000 || player.isCursedWeaponEquipped())
			return;

		transformedSkills(player);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		// Kadomas Special Skill - Fireworks
		addSkill(player, 23154, 1);
		// Transform Dispel
		addSkill(player, 619, 1);

	}

	@Override
	public void onUntransform(L2Player player)
	{
		removeSkills(player);
	}

	@Override
	public void removeSkills(L2Player player)
	{
		// Kadomas Special Skill - Fireworks
		removeSkill(player, 23154);
		// Transform Dispel
		removeSkill(player, 619);

	}
}
