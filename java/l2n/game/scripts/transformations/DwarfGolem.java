package l2n.game.scripts.transformations;

import l2n.game.model.L2DefaultTransformation;
import l2n.game.model.actor.L2Player;

public class DwarfGolem extends L2DefaultTransformation
{
	public DwarfGolem()
	{
		// id, colRadius, colHeight
		super(259, 35.0, 46.0);
	}

	@Override
	public void transformedSkills(L2Player player)
	{
		addSkill(player, 806, 1); // Magic Obstacle
		addSkill(player, 807, 1); // Over-hit
		addSkill(player, 808, 1); // Golem Punch
		addSkill(player, 809, 1); // Golem Tornado
	}

	@Override
	public void removeSkills(L2Player player)
	{
		removeSkill(player, 806); // Magic Obstacle
		removeSkill(player, 807); // Over-hit
		removeSkill(player, 808); // Golem Punch
		removeSkill(player, 809); // Golem Tornado
	}
}
