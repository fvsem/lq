package l2n.game.scripts;

import gnu.trove.map.hash.THashMap;
import l2n.commons.compiler.Compiler;
import l2n.commons.compiler.MemoryClassLoader;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFileLoader;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.*;
import l2n.game.scripts.npc.hellbound.*;
import l2n.game.scripts.npc.naia.IngeniousContraption;
import l2n.game.scripts.npc.naia.NaiaController;
import l2n.game.scripts.npc.naia.NaiaMonster;
import l2n.game.scripts.npc.naia.NaiaTeleportCube;

import java.io.File;
import java.util.logging.Logger;

public class NPCInstanceLoader extends ScriptFileLoader
{
	protected static final Logger _log = Logger.getLogger(NPCInstanceLoader.class.getName());

	public final THashMap<String, Class<L2NpcInstance>> npc_instances;

	public static NPCInstanceLoader getInstance()
	{
		return SingletonHolder._instance;
	}

	private NPCInstanceLoader()
	{
		npc_instances = new THashMap<String, Class<L2NpcInstance>>();
		// загружаем
		load(false);
		npc_instances.trimToSize();
	}

	public void load(final boolean reload)
	{
		if(reload)
			npc_instances.clear();

		loadStaticAI();
		final int count = loadCustomAI();
		_log.info("NPCInstanceLoader: loaded " + npc_instances.size() + " (" + count + " custom) models.");
	}

	/**
	 * Загружает AI которые в ядре
	 */
	private void loadStaticAI()
	{
		loadClass("NewbieGuide", NewbieGuideInstance.class);
		loadClass("AenkinelChest", AenkinelChestInstance.class);
		loadClass("DestroyedTumorEntrance", DestroyedTumorEntranceInstance.class);
		loadClass("DestroyedTumor", DestroyedTumorInstance.class);
		loadClass("FakeObelisk", FakeObeliskInstance.class);
		loadClass("GatekeeperOfAbyss", GatekeeperOfAbyssInstance.class);
		loadClass("HellboundRemnant", HellboundRemnantInstance.class);
		loadClass("HoSBoss", HoSBossInstance.class);
		loadClass("ImmuneMonster", ImmuneMonsterInstance.class);
		loadClass("Kama26Boss", Kama26BossInstance.class);
		loadClass("Kanabion", KanabionInstance.class);
		loadClass("L2Block", L2BlockInstance.class);
		loadClass("L2EnergySeed", L2EnergySeedInstance.class);
		loadClass("L2EntranceManager", L2EntranceManagerInstance.class);
		loadClass("L2StarStone", L2StarStoneInstance.class);
		loadClass("L2TriolsMirror", L2TriolsMirrorInstance.class);
		loadClass("LostCaptain", LostCaptainInstance.class);
		loadClass("MoonlightTombstone", MoonlightTombstoneInstance.class);
		loadClass("MouthOfEkimus", MouthOfEkimusInstance.class);
		loadClass("NativePrisoner", NativePrisonerInstance.class);
		loadClass("Obelisk", ObeliskInstance.class);
		loadClass("Orfen", OrfenInstance.class);
		loadClass("PassagewayMobWithHerb", PassagewayMobWithHerbInstance.class);
		loadClass("QueenAnt", QueenAntInstance.class);
		loadClass("QueenAntLarva", QueenAntLarvaInstance.class);
		loadClass("RoyalRushTeleporter", RoyalRushTeleporterInstance.class);
		loadClass("Sandstorm", SandstormInstance.class);

		// Tully
		loadClass("DwarvenGhost", DwarvenGhost.class);
		loadClass("TeleportationCubic", TeleportationCubic.class);
		loadClass("TullyGatekeeper", TullyGatekeeper.class);
		loadClass("SelfDestructer", SelfDestructer.class);
		loadClass("MysteriousAgent", MysteriousAgent.class);
		// Naia
		loadClass("NaiaController", NaiaController.class);
		loadClass("IngeniousContraption", IngeniousContraption.class);
		loadClass("NaiaMonster", NaiaMonster.class);
		loadClass("NaiaTeleportCube", NaiaTeleportCube.class);
		loadClass("StoneCoffin", StoneCoffin.class);

		loadClass("Snowman", SnowmanInstance.class);
		loadClass("SoulTrader", SoulTraderInstance.class);
		loadClass("Squash", SquashInstance.class);
		loadClass("Thomas", ThomasInstance.class);
		loadClass("ZakenKandle", ZakenKandleInstance.class);

		loadClass("PailakaHelpers", PailakaHelpersInstance.class);
		loadClass("PailakaBoss", PailakaBossInstance.class);
		loadClass("PailakaInvader", PailakaInvaderInstance.class);
	}

	private int loadCustomAI()
	{
		final GArray<File> npc_model = new GArray<File>();
		parseClasses(new File("./data/scripts/npc"), npc_model);
		if(Compiler.getInstance().compile(npc_model, System.out))
		{
			int count = 0;
			final MemoryClassLoader classLoader = Compiler.getInstance().getClassLoader();
			for(final String name : classLoader.getClasses())
			{
				if(name.contains("$"))
					continue; // пропускаем вложенные классы
				try
				{
					final Class<?> c = classLoader.loadClass(name);
					loadClass(c.getSimpleName(), c);
					count++;
				}
				catch(final ClassNotFoundException e)
				{
					_log.warning("Can't load custom NPC script class[" + name + "]:" + e.getMessage());
				}
			}
			Compiler.getInstance().clearClassLoader();
			return count;
		}
		else
			_log.warning("Can't compile custom NPC scripts!");
		return 0;
	}

	@SuppressWarnings("unchecked")
	private void loadClass(final String nameAI, final Class<?> c)
	{
		npc_instances.put(nameAI, (Class<L2NpcInstance>) c);
	}

	/**
	 * @param type
	 *            - название инстанса
	 * @return true если инстанс с таким названием есть
	 */
	public boolean isAvailable(final String type)
	{
		return npc_instances.containsKey(type);
	}

	public Class<L2NpcInstance> getModel(final String type)
	{
		return npc_instances.get(type);
	}

	@Override
	protected boolean checkFileCondition(final File file, final boolean dir)
	{
		if(dir)
		{
			if(file.isHidden() || file.getName().equals(".svn"))
				return false;
		}
		else if(file.isHidden() || !file.getName().contains(".java"))
			return false;

		return true;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final NPCInstanceLoader _instance = new NPCInstanceLoader();
	}
}
