package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class GuardianAngel extends DefaultAI
{
	public GuardianAngel(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		// Пока дерется, будет кричать. Вызов раз в секунду, так что не зафлудит :)
		if(Rnd.nextBoolean())
			Functions.npcShout(actor, "Вааааах!Отойдите от проклятый ящик! Я возьму это сам!");
		else
			Functions.npcShout(actor, "Гррр!Кто вы такие и почему вы остановили меня?");

		return super.thinkActive();
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		Functions.npcShout(actor, "Грр. Я был поражен ...");
		super.onEvtDead(killer);
	}
}
