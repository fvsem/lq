package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.model.L2World;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * AI для Lilim Fighter's. <BR>
 * ID: 27373,27377,27371,27379<BR>
 * При агре ругаются матом. <BR>
 * При смерти включают Соц Экшан "Негодование" :D<BR>
 * Так же используется для Минионов Анакима и Лилит.<BR>
 * Минионы анакима берёт цель на миниона файтера лилит.<BR>
 * Минион файтел лилит выбирает рандомно одного из минионов анакима для атаки.
 */
public class EpicQuestFighter extends Fighter
{
	private L2NpcInstance lilithMinion;
	private L2NpcInstance anakimMinion;
	private static final int[] minions = { 32719, 32720, 32721 };

	public EpicQuestFighter(final L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	public void onIntentionAttack(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE)
			return;

		switch (getActor().getNpcId())
		{
			case 27371:
			case 27379:
				Functions.npcSay(actor, "This place once belonged to Lord Shilen.");
				break;
			case 27373:
				Functions.npcSay(actor, "Those who are afraid should get away and those who are brave should fight!");
				break;
			case 27377:
				Functions.npcSay(actor, "Leave now!");
				break;
		}
		super.onIntentionAttack(target);
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		switch (getActor().getNpcId())
		{
			case 27373:
			case 27379:
				Functions.npcSay(actor, "Why are you getting in our way?");
				break;
			case 27377:
				Functions.npcSay(actor, "For Shilen!");
				break;
		}

		super.onEvtDead(killer);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return true;
		switch (getActor().getNpcId())
		{
			case 32719:
			case 32720:
			case 32721:
				if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
				{
					if(lilithMinion == null)
						for(final L2NpcInstance npc : L2World.getAroundNpc(actor, 1000, 200))
							if(npc.getNpcId() == 32717)
							{
								npc.addDamageHate(actor, 0, 100);
								lilithMinion = npc;
							}
					if(lilithMinion != null)
						setIntention(CtrlIntention.AI_INTENTION_ATTACK, lilithMinion);
				}
				break;
			case 32717:
				if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
				{
					if(anakimMinion == null)
						for(final L2NpcInstance npc : L2World.getAroundNpc(actor, 1000, 200))
							if(npc.getNpcId() == minions[Rnd.get(minions.length)])
							{
								npc.addDamageHate(actor, 0, 100);
								anakimMinion = npc;
							}
					if(anakimMinion != null)
						setIntention(CtrlIntention.AI_INTENTION_ATTACK, anakimMinion);
				}
				break;
		}
		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		final Reflection r = actor.getReflection();

		for(final L2Player pl : r.getPlayers())
			if(pl != null && Functions.getItemCount(pl, 13846) >= 4)
				actor.deleteMe();

		super.thinkAttack();
	}
}
