package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.model.L2World;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * AI для Anakim ID: 27388
 * Сцена перепалки с Лилит.<BR>
 * Ругается в шаут некультурным англ. матом. :)<BR>
 * Ругается в ПМ игроку который в комнате.<BR>
 * Не реагирует на атаки со стороны чаров(Неуязвим)<BR>
 * Делетит себя если у персонажа в сумке 4 или больше итемов по квесту.<BR>
 * Рейт использования физ атаки 0 (Использует только 1 скилл и то магичиский...)<BR>
 * Нет рандомного передвижения.
 */
public class EpicQuestAnakim extends Mystic
{
	private long _lastSay;
	private L2NpcInstance lilith;

	private static final String[] say = {
			"For the eternity of Einhasad!!!",
			"I'll show you the real power of Einhasad!",
			"Dear Military Force of Light! Go destroy the offspring of Shilien!!!",
			"Dear Shilien's offspring! You are not capable of confronting us!" };
	private static final String[] sayToPlayer = {
			"My power's weakening. Hurry and turn on the sealing device!!!",
			"Lilith's attack is getting stronger! Go ahead and turn it on!",
			"All 4 sealing devices must be turned on!!!" };

	public EpicQuestAnakim(final L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
		actor.setInvul(true);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
		{
			if(lilith == null)
				for(final L2NpcInstance npc : L2World.getAroundNpc(actor, 1000, 200))
					if(npc.getNpcId() == 32715) // Lilith
					{
						npc.addDamageHate(actor, 0, 100);
						lilith = npc;
					}

			if(lilith != null)
				setIntention(CtrlIntention.AI_INTENTION_ATTACK, lilith);
		}
		return true;
	}

	@Override
	protected void thinkAttack()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		final Reflection r = actor.getReflection();
		// Ругаемся не чаще, чем раз в 10 секунд
		if(System.currentTimeMillis() - _lastSay > 10000)
		{
			Functions.npcShout(actor, say[Rnd.get(say.length)]);
			_lastSay = System.currentTimeMillis();

			for(final L2Player pl : r.getPlayers())
				if(pl != null && Rnd.chance(40))
					if(Rnd.chance(20))// с шансорм 20% посылает игроку это сообщение если нет то рандомно одно из остальных 3-х.
						Functions.npcSayToPlayer(actor, "Dear " + pl.getName() + ", give me more strength.", pl);
					else
						Functions.npcSayToPlayer(actor, sayToPlayer[Rnd.get(sayToPlayer.length)], pl);
		}
		for(final L2Player pl : r.getPlayers())
			if(pl != null && Functions.getItemCount(pl, 13846) >= 4) // Seal of Binding
				actor.deleteMe();

		super.thinkAttack();
	}
}
