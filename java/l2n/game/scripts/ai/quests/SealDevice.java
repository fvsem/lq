package l2n.game.scripts.ai.quests;

import l2n.game.ai.DefaultAI;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Env;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

/**
 * AI рейд босов SealDevice в 5 части эпик цепочки
 */
public class SealDevice extends DefaultAI
{
	private static final FuncTemplate ft1 = new FuncTemplate(null, null, "Set", Stats.REFLECT_DAMAGE_PERCENT, 0x10, 3);
	private static final FuncTemplate ft2 = new FuncTemplate(null, null, "Set", Stats.REFLECT_PSKILL_DAMAGE_PERCENT, 0x10, 3);
	private static final FuncTemplate ft3 = new FuncTemplate(null, null, "Set", Stats.REFLECT_MSKILL_DAMAGE_PERCENT, 0x10, 3);
	private static final L2Skill skill = SkillTable.getInstance().getInfo(5980, 1);

	public SealDevice(final L2Character actor)
	{
		super(actor);
		actor.addStatFunc(ft1.getFunc(new Env(), this));
		actor.addStatFunc(ft2.getFunc(new Env(), this));
		actor.addStatFunc(ft3.getFunc(new Env(), this));
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 5000;
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtThink()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		// Кастует скил
		if(Rnd.chance(50))
			actor.doCast(skill, actor, true);

		super.onEvtThink();
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{}

	@Override
	protected void onEvtAggression(final L2Character target, final int aggro)
	{}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
