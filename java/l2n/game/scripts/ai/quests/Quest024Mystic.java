package l2n.game.scripts.ai.quests;

import l2n.game.ai.Mystic;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;

public class Quest024Mystic extends Mystic
{
	private final String myEvent;

	public Quest024Mystic(final L2Character actor)
	{
		super(actor);
		myEvent = "playerInMobRange_" + actor.getNpcId();
	}

	@Override
	protected boolean thinkActive()
	{
		final Quest q = QuestManager.getQuest(24);
		if(q != null)
			for(final L2Player player : L2World.getAroundPlayers(getActor(), 900, 200))
				player.processQuestEvent(q.getName(), myEvent, null);

		return super.thinkActive();
	}
}
