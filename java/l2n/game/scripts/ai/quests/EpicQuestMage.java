package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.model.L2World;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * AI для Lilim Magus & Lilim Great Magus.<BR>
 * ID: 27372/27378
 * При агре ругаются матом.<BR>
 * При смерти включают Соц Экшан "Негодование" :D<BR>
 * Используется так же для Миниона лилит визарда.<BR>
 * Выбирает рандомно одного из минионов Анакима для атаки.<BR>
 */
public class EpicQuestMage extends Mystic
{
	private L2NpcInstance anakimMinion;
	private static final int[] minions = { 32719, 32720, 32721 };

	public EpicQuestMage(final L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	public void onIntentionAttack(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE)
			return;

		switch (getActor().getNpcId())
		{
			case 27372:
			case 27378:
				Functions.npcSay(actor, "Who dares enter this place?");
				break;
		}
		super.onIntentionAttack(target);
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		switch (getActor().getNpcId())
		{
			case 27372:
			case 27378:
				Functions.npcSay(actor, "Lord Shilen... some day... you will accomplish... this mission...");
				break;
		}

		super.onEvtDead(killer);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return true;
		switch (getActor().getNpcId())
		{
			case 32716:
				if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
				{
					if(anakimMinion == null)
						for(final L2NpcInstance npc : L2World.getAroundNpc(actor, 1000, 200))
							if(npc.getNpcId() == minions[Rnd.get(minions.length)])
							{
								npc.addDamageHate(actor, 0, 100);
								anakimMinion = npc;
							}
					if(anakimMinion != null)
						setIntention(CtrlIntention.AI_INTENTION_ATTACK, anakimMinion);
				}
				break;
		}
		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		final Reflection r = actor.getReflection();

		for(final L2Player pl : r.getPlayers())
			if(pl != null && Functions.getItemCount(pl, 13846) >= 4) // Seal of Binding
				actor.deleteMe();

		super.thinkAttack();
	}
}
