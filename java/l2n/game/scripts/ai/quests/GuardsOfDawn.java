package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.DefaultAI;
import l2n.game.model.L2ObjectTasks.TeleportTask;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;

/**
 * Для квеста _195_SevenSignsSecretRitualPriests
 * Бегает от стенке к стенке)
 * 
 * @see quests._195_SevenSignsSecretRitualPriests._195_SevenSignsSecretRitualPriests
 */
public class GuardsOfDawn extends DefaultAI
{
	private static final String[] _text =
	{
			"Who are you?! A new face like you can't approach this place!",
			"Intruder! Protect the Priests of Dawn!",
			"How dare you intrude with that transformation! Get lost!"
	};

	// Intruder! Protect the Priests of Dawn!
	// How dare you intrude with that transformation! Get lost!

	private static final int SKILL_SSQ_TELEPORT = 5978;

	private static final Location pos1 = new Location(-75775, 213415, -7120);
	private static final Location pos2 = new Location(-74959, 209240, -7472);
	private static final Location pos3 = new Location(-77699, 208905, -7640);
	private static final Location pos4 = new Location(-79939, 205857, -7888);

	private class ScanTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
				return;

			if(System.currentTimeMillis() > last_check_time)
				for(final L2Player player : actor.getAroundPlayers(agro_range))
				{
					// охранники с id 18834, 18835 видят через инвиз
					final boolean invis = player.getEffectList().getFirstEffect(963) != null && actor.getNpcId() != 18834 && actor.getNpcId() != 18835;
					if(!invis)
					{
						// Посылаем анимацию
						last_check_time = System.currentTimeMillis() + 1200;
						actor.broadcastPacket(new MagicSkillUse(actor, player, SKILL_SSQ_TELEPORT, 1, 0, 0));
						Functions.npcSayToPlayer(actor, _text[Rnd.get(_text.length)], player);
						L2GameThreadPools.getInstance().scheduleAi(new TeleportTask(player, getTeleportLocation(my_position), player.getReflectionId()), 1000, false);
					}
				}
		}
	}

	private static Location getTeleportLocation(final int position)
	{
		switch (position)
		{
			case 1:
				return pos1;
			case 2:
				return pos2;
			case 3:
				return pos3;
			case 4:
				return pos4;
		}
		return pos1;
	}

	private int cycle;
	private boolean isArrived = true;

	private ScheduledFuture<ScanTask> _scanTask = null;

	/** точка спауна */
	private final Location t1;
	/** вторая точка куда нужно идти */
	private final Location t2;
	private final int my_position;
	private final int agro_range;

	private long last_check_time = 0;

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	/**
	 * @param actor
	 */
	public GuardsOfDawn(final L2Character actor, final Location spawn, final Location way, final int position, final int agro)
	{
		super(actor);
		t1 = spawn;
		t2 = way;
		my_position = position;
		agro_range = agro;
		AI_TASK_DELAY = 1500;
		AI_TASK_ACTIVE_DELAY = 1500;
	}

	@Override
	protected void onEvtSpawn()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
		{
			if(_scanTask != null)
			{
				_scanTask.cancel(false);
				_scanTask = null;
			}
			return;
		}

		cycle = 1;
		if(_scanTask != null)
		{
			_scanTask.cancel(false);
			_scanTask = null;
		}
		_scanTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new ScanTask(), 500, 500, false);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected void onEvtArrived()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
		{
			if(_scanTask != null)
			{
				_scanTask.cancel(false);
				_scanTask = null;
			}
			return;
		}

		isArrived = true;
		if(cycle == 1)
			cycle = 0;
		else
			cycle = 1;

		thinkActive();
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
		{
			if(_scanTask != null)
			{
				_scanTask.cancel(false);
				_scanTask = null;
			}
			return true;
		}

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(isArrived && t1 != null && t2 != null)
		{
			switch (cycle)
			{
				case 0:
					addTaskMove(t1);
					break;
				case 1:
					addTaskMove(t2);
					break;
			}

			isArrived = false;
			doTask();
			return true;
		}

		return false;
	}

	@Override
	public void stopAITask()
	{
		if(_scanTask != null)
		{
			_scanTask.cancel(false);
			_scanTask = null;
		}
		super.stopAITask();
	}
}
