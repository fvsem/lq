package l2n.game.scripts.ai.quests;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.Priest;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.SkillTable;

import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;

/**
 * Lematan's Follower[18634] (минионы Lematan'a): каждые 15 секунд бафают Lematan'а баффом повышающим регенирацию HP.
 * 
 */
public class LematanMinion extends Priest
{
	private static final L2Skill heal_skill = SkillTable.getInstance().getInfo(5712, 1);

	private ScheduledFuture<HealTask> _heal_task;
	private L2MonsterInstance lematan = null;

	/**
	 * @param actor
	 */
	public LematanMinion(final L2Character actor, final L2MonsterInstance boss)
	{
		super(actor);
		lematan = boss;
		_heal_task = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new HealTask(), 4 * 1000, 15 * 1000, false);
	}

	private class HealTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2MonsterInstance actor = getActor();
			if(actor != null && !actor.isDead() && lematan != null && !lematan.isDead() && lematan.getCurrentHpPercents() <= 40)
				try
				{
					addUseSkillDesire(lematan, heal_skill, 800000);
				}
				catch(final Throwable e)
				{
					_log.log(Level.WARNING, "", e);
				}

			if(actor == null || actor.isDead())
				if(_heal_task != null)
				{
					_heal_task.cancel(false);
					_heal_task = null;
				}
		}
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		if(_heal_task != null)
		{
			_heal_task.cancel(false);
			_heal_task = null;
		}
		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}

	@Override
	public L2MonsterInstance getActor()
	{
		return (L2MonsterInstance) super.getActor();
	}
}
