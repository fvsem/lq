package l2n.game.scripts.ai.quests;

import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;

public class Quest421FairyTree extends Fighter
{
	public Quest421FairyTree(final L2Character actor)
	{
		super(actor);
		actor.setImmobilized(true);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && attacker.isPlayer())
		{
			final L2Skill skill = SkillTable.getInstance().getInfo(5423, 12);
			skill.getEffects(actor, attacker, false, false);
			return;
		}
		if(attacker.isPet())
		{
			super.onEvtAttacked(attacker, damage);
			return;
		}
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
