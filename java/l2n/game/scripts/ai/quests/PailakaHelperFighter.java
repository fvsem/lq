package l2n.game.scripts.ai.quests;

import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.ai.tasks.random.BuffTask;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Util;

public class PailakaHelperFighter extends Fighter
{
	private final int buff_id;

	/**
	 * @param actor
	 */
	public PailakaHelperFighter(final L2Character actor)
	{
		super(actor);
		buff_id = getAIParams().getInt(AiOptionsType.OPTIONS, 0);
		AI_TASK_ACTIVE_DELAY = 2000;
	}

	@Override
	public void startAITask()
	{
		if(!isActive())
			addRandomTask(new BuffTask(buff_id, 1, 2 * 60 * 1000));
		super.startAITask();
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(actor.isBlocked() || _randomAnimationEnd > System.currentTimeMillis())
			return true;

		if(_def_think)
		{
			if(doTask())
				clearTasks();
			return true;
		}

		for(final L2NpcInstance obj : L2World.getAroundNpc(actor, 300, 200))
			if(obj != null && !obj.isAlikeDead() && obj.isVisible())
				checkAggression(obj);

		return defaultThinkBuff(10);
	}

	@Override
	public void checkAggression(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE || !isGlobalAggro())
			return;
		if(target.isPlayable())
			return;
		if(!Util.checkIfInRange(300, actor.getX(), actor.getY(), actor.getZ(), target.getX(), target.getY(), target.getZ(), false) || Math.abs(actor.getZ() - target.getZ()) > MAX_Z_AGGRO_RANGE)
			return;
		if(!actor.canAttackCharacter(target))
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;

		target.addDamageHate(actor, 0, 2);

		startRunningTask(2000);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}

	@Override
	protected void onEvtAggression(final L2Character attacker, final int aggro)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		actor.setAttackTimeout(getMaxAttackTimeout() + System.currentTimeMillis());
		setGlobalAggro(0);

		// если мертвый, всё остальное можно уже не выполнять
		if(!actor.isDead())
		{
			attacker.addDamageHate(actor, 0, aggro);

			if(!actor.isRunning())
				startRunningTask(1000);

			// на всякий случай, чтоб наверника атаковать начинали
			_randomAnimationEnd = 0;
			if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
				setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
		}
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		if(!actor.canAttackCharacter(attacker))
			return;

		if(damage > 0)
			attacker.addDamageHate(actor, damage, 2);

		actor.callFriends(attacker, damage);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
