package l2n.game.scripts.ai.quests;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.model.L2ObjectTasks.TeleportTask;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

/**
 * Lematan[18633] (РБ в Pailaka Devils Legacy): при HP меньше 50% бежит к кораблю; телепорт на корабль; спаун минионов после атаки; проверяет расстояние от центра корабля.
 * 
 */
public class Lematan extends Fighter
{
	private static final Location TeleZone = new Location(86116, -209117, -3774);
	private static final Location TeleOnTheShip = new Location(85000, -208699, -3336);

	private static final Location[] minions_locs = new Location[] { new Location(84704, -208726, -3336, 0),
			new Location(84820, -208431, -3336, 53247),
			new Location(85136, -208430, -3336, 45055),
			new Location(85271, -208726, -3336, 32768),
			new Location(84851, -209024, -3336, 8192),
			new Location(85144, -208995, -3336, 24576) };

	private static final L2NpcTemplate lematan_minion = NpcTable.getTemplate(18634);

	private static final int summon_private = 5756;
	private static final int teleport = 4671;

	private boolean move_finished = false;
	private boolean minion_spawn = false;
	private long _wait_timeout = 0;

	public Lematan(final L2Character actor)
	{
		super(actor);
		setMaxPursueRange(-1);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2MonsterInstance actor = getActor();
		if(actor == null)
			return;

		if(actor.getCurrentHpPercents() < 50 && !move_finished)
		{
			clearTasks();
			addTaskMove(TeleZone);
			return;
		}

		// проверяем расстояние от центра (уже после тп на корабль), если больше 800 то телепортируем
		if(System.currentTimeMillis() > _wait_timeout && minion_spawn)
		{
			_wait_timeout = System.currentTimeMillis() + 15000;
			if(actor.getDistance(85010, -208700) > 800)
				actor.teleToLocation(TeleOnTheShip);
		}

		// спауним миньёнов после телепортации на корабль
		if(move_finished && !minion_spawn)
		{
			minion_spawn = true;
			actor.broadcastPacket(new MagicSkillUse(actor, actor, summon_private, 1, 2500, 0));
			for(final Location loc : minions_locs)
				spawn(loc, actor.getReflectionId(), actor);
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtArrived()
	{
		final L2MonsterInstance boss = getActor();
		if(boss == null || boss.isDead())
			return;

		if(!move_finished)
		{
			move_finished = true;
			clearTasks();
			boss.clearAggroList(false);
			boss.broadcastPacket(new MagicSkillUse(boss, boss, teleport, 1, 2500, 0));
			L2GameThreadPools.getInstance().scheduleAi(new TeleportTask(boss, TeleOnTheShip, boss.getReflectionId()), 500, false);
		}
		else
			super.onEvtArrived();
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		for(final L2NpcInstance npc : killer.getAroundNpc(1800, 200))
			if(npc.getNpcId() == 18634)
				npc.deleteMe();

		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	public void returnHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	public L2MonsterInstance getActor()
	{
		return (L2MonsterInstance) super.getActor();
	}

	private void spawn(final Location loc, final long refId, final L2MonsterInstance boss)
	{
		L2Spawn spawn;
		try
		{
			spawn = new L2Spawn(lematan_minion);
			spawn.setLoc(loc);
			spawn.setReflection(refId);
			final L2NpcInstance npc = spawn.doSpawn(true);
			final LematanMinion ai = new LematanMinion(npc, boss);
			npc.setAI(ai);
			ai.startAITask();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
	}
}
