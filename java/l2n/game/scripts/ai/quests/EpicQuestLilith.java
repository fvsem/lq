package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.model.L2World;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * AI для Lilith ID: 27385
 * Сцена перепалки с Анаким. <BR>
 * Ругается в шаут некультурным англ. матом. :)<BR>
 * Не реагирует на атаки со стороны чаров(Неуязвим)<BR>
 * Делетит себя если у персонажа в сумке 4 или больше итемов по квесту.<BR>
 * Рейт использования физ атаки 0(Использует только 1 скилл и то магичиский...)<BR>
 * Нет рандомного передвижения.
 */
public class EpicQuestLilith extends Mystic
{
	private long _lastSay;
	private L2NpcInstance anakim;
	private static final String[] say = {
			"You, such a fool! The victory over this war belongs to Shilien!!!",
			"How dare you try to contend against me in strength? Ridiculous.",
			"Anakim! In the name of Great Shilien, I will cut your throat!",
			"You cannot be the match of Lilith. I'll teach you a lesson!" };

	public EpicQuestLilith(final L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
		actor.setInvul(true);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
		{
			if(anakim == null)
				for(final L2NpcInstance npc : L2World.getAroundNpc(actor, 1000, 200))
					if(npc.getNpcId() == 32718)
					{
						npc.addDamageHate(actor, 0, 100);
						anakim = npc;
					}

			if(anakim != null)
				setIntention(CtrlIntention.AI_INTENTION_ATTACK, anakim);
		}

		return true;
	}

	@Override
	protected void thinkAttack()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		final Reflection r = actor.getReflection();

		// Ругаемся не чаще, чем раз в 10 секунд
		if(System.currentTimeMillis() - _lastSay > 10000)
		{
			Functions.npcShout(actor, say[Rnd.get(say.length)]);
			_lastSay = System.currentTimeMillis();
		}
		for(final L2Player pl : r.getPlayers())
			if(pl != null && Functions.getItemCount(pl, 13846) >= 4)
				actor.deleteMe();

		super.thinkAttack();
	}
}
