package l2n.game.scripts.ai.quests;

import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.PailakaHelpersInstance;
import l2n.util.Rnd;
import l2n.util.Util;

public class PailakaInvader extends Fighter
{
	public PailakaInvader(final L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(actor.isBlocked() || _randomAnimationEnd > System.currentTimeMillis())
			return true;

		if(_def_think)
		{
			if(doTask())
				clearTasks();
			return true;
		}

		// Аггрится даже на неподвижных игроков
		if(actor.isAggressive() && Rnd.chance(10))
			for(final L2Character obj : L2World.getAroundCharacters(actor))
				if(obj != null && !obj.isAlikeDead() && !obj.isInvul() && obj.isVisible() && (obj.isPlayable() || obj instanceof PailakaHelpersInstance))
					checkAggression(obj);

		if(randomAnimation())
			return true;

		if(randomWalk())
			return true;

		return defaultThinkBuff(10);
	}

	@Override
	public void checkAggression(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE || !isGlobalAggro())
			return;
		if(!actor.isAggressive() || !Util.checkIfInRange(actor.getAggroRange(), actor.getX(), actor.getY(), actor.getZ(), target.getX(), target.getY(), target.getZ(), false) || Math.abs(actor.getZ() - target.getZ()) > MAX_Z_AGGRO_RANGE)
			return;
		if(target.isPlayable() && !canSeeInSilentMove((L2Playable) target))
			return;
		if(target.isInZonePeace())
			return;
		if(target.isFollow && !target.isPlayer() && target.getFollowTarget() != null && target.getFollowTarget().isPlayer())
			return;
		if(target.isPlayer() && ((L2Player) target).isInvisible())
			return;
		if(target.getPlayer() == null && !(target instanceof PailakaHelpersInstance))
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;

		if((target.isSummon() || target.isPet()) && target.getPlayer() != null)
			target.getPlayer().addDamageHate(actor, 0, 1);

		target.addDamageHate(actor, 0, 2);

		startRunningTask(2000);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}

	@Override
	protected void onEvtAggression(final L2Character attacker, final int aggro)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		actor.setAttackTimeout(getMaxAttackTimeout() + System.currentTimeMillis());
		setGlobalAggro(0);

		// если мертвый, всё остальное можно уже не выполнять
		if(!actor.isDead())
		{
			attacker.addDamageHate(actor, 0, aggro);

			if(!actor.isRunning())
				startRunningTask(1000);

			// на всякий случай, чтоб наверника атаковать начинали
			_randomAnimationEnd = 0;

			if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
			{
				// Показываем анимацию зарядки шотов, если есть таковые.
				switch (actor.getTemplate().shots)
				{
					case SOUL:
						actor.unChargeShots(false);
						break;
					case SPIRIT:
					case BSPIRIT:
						actor.unChargeShots(true);
						break;
					case SOUL_SPIRIT:
					case SOUL_BSPIRIT:
						actor.unChargeShots(false);
						actor.unChargeShots(true);
						break;
				}

				setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
			}
		}
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	public void returnHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}
}
