package l2n.game.scripts.ai.quests;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.scripts.npc.PailakaHelpersInstance;
import l2n.game.scripts.quests._726_LightWithinTheDarkness;
import l2n.game.scripts.quests._727_HopeWithinTheDarkness;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

public class PailakaBoss extends Fighter
{
	private static final String stage1 = "Begin stage 1!";
	private static final String stage2 = "Begin stage 2!";
	private static final String stage3 = "Begin stage 3!";

	private static final String msg1300145 = "I'll rip the flesh from your bones!";
	private static final String msg1300146 = "You'll flounder in delusion for the rest of your life!";

	private static final String msg1300147 = "There is no escape from this place!";
	private static final String msg1300148 = "How dare you!";

	private static final Location to_move = new Location(49432, -12239, -9386);

	private final String spawn_msg;
	private final String spawn_screen_msg;

	public PailakaBoss(final L2Character actor)
	{
		super(actor);
		final int npcId = actor.getNpcId();
		switch (npcId)
		{
			case _726_LightWithinTheDarkness.invader_pole:
			case _727_HopeWithinTheDarkness.invader_pole:
				spawn_msg = msg1300145;
				spawn_screen_msg = stage1;
				break;
			case _726_LightWithinTheDarkness.invader_bow:
			case _727_HopeWithinTheDarkness.invader_bow:
				spawn_msg = msg1300146;
				spawn_screen_msg = stage2;
				break;
			case _726_LightWithinTheDarkness.invader_fist:
			case _727_HopeWithinTheDarkness.invader_fist:
				spawn_msg = msg1300146;
				spawn_screen_msg = stage3;
				break;
			default:
			{
				spawn_msg = null;
				spawn_screen_msg = null;
			}
		}
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(actor.isBlocked() || _randomAnimationEnd > System.currentTimeMillis())
			return true;

		if(_def_think)
		{
			if(doTask())
				clearTasks();
			return true;
		}

		// Аггрится даже на неподвижных игроков
		if(actor.isAggressive() && Rnd.chance(50))
			for(final L2Character obj : L2World.getAroundCharacters(actor))
				if(obj != null && !obj.isAlikeDead() && !obj.isInvul() && obj.isVisible() && (obj.isPlayable() || obj instanceof PailakaHelpersInstance))
					checkAggression(obj);

		if(randomAnimation())
			return true;

		if(randomWalk())
			return true;

		return false;
	}

	@Override
	public void checkAggression(final L2Character target)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE || !isGlobalAggro())
			return;
		if(!actor.isAggressive() || !Util.checkIfInRange(actor.getAggroRange(), actor.getX(), actor.getY(), actor.getZ(), target.getX(), target.getY(), target.getZ(), false) || Math.abs(actor.getZ() - target.getZ()) > MAX_Z_AGGRO_RANGE)
			return;
		if(target.isPlayable() && !canSeeInSilentMove((L2Playable) target))
			return;
		if(target.isInZonePeace())
			return;
		if(target.isFollow && !target.isPlayer() && target.getFollowTarget() != null && target.getFollowTarget().isPlayer())
			return;
		if(target.isPlayer() && ((L2Player) target).isInvisible())
			return;
		if(target.getPlayer() == null && !(target instanceof PailakaHelpersInstance))
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;

		if((target.isSummon() || target.isPet()) && target.getPlayer() != null)
			target.getPlayer().addDamageHate(actor, 0, 1);

		target.addDamageHate(actor, 0, 2);

		startRunningTask(2000);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}

	@Override
	protected void onEvtAggression(final L2Character attacker, final int aggro)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		actor.setAttackTimeout(getMaxAttackTimeout() + System.currentTimeMillis());
		setGlobalAggro(0);

		// если мертвый, всё остальное можно уже не выполнять
		if(!actor.isDead())
		{
			attacker.addDamageHate(actor, 0, aggro);

			if(!actor.isRunning())
				startRunningTask(1000);

			// на всякий случай, чтоб наверника атаковать начинали
			_randomAnimationEnd = 0;

			if(getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
			{
				// Показываем анимацию зарядки шотов, если есть таковые.
				switch (actor.getTemplate().shots)
				{
					case SOUL:
						actor.unChargeShots(false);
						break;
					case SPIRIT:
					case BSPIRIT:
						actor.unChargeShots(true);
						break;
					case SOUL_SPIRIT:
					case SOUL_BSPIRIT:
						actor.unChargeShots(false);
						actor.unChargeShots(true);
						break;
				}

				setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
			}
		}
	}

	@Override
	protected void onEvtSpawn()
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isDead())
		{
			addTaskMove(to_move);
			if(spawn_msg != null)
				Functions.npcSay(actor, spawn_msg);

			if(spawn_screen_msg != null)
				actor.broadcastPacketToOthers(new ExShowScreenMessage(spawn_screen_msg, 4000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, true));
		}

		super.onEvtSpawn();
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null)
			Functions.npcSay(actor, msg1300148);
		super.onEvtDead(killer);
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	public void returnHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}
}
