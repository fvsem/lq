package l2n.game.scripts.ai.grandboss;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.game.ai.DefaultAI;
import l2n.game.instancemanager.boss.BaiumManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class Baium extends DefaultAI
{
	private final L2Skill baium_normal_attack, energy_wave, earth_quake, thunderbolt, group_hold;

	public Baium(L2Character actor)
	{
		super(actor);
		TIntObjectHashMap<L2Skill> skills = getActor().getTemplate().getSkills();
		baium_normal_attack = skills.get(4127);
		energy_wave = skills.get(4128);
		earth_quake = skills.get(4129);
		thunderbolt = skills.get(4130);
		group_hold = skills.get(4131);

		AI_TASK_DELAY = 1000;
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		BaiumManager.setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean createNewTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		if(!BaiumManager.getZone().checkIfInZone(actor))
		{
			teleportHome(true);
			return false;
		}

		clearTasks();

		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		if(!BaiumManager.getZone().checkIfInZone(target))
		{
			target.removeFromHatelist(actor, false);
			return false;
		}

		int s_energy_wave = 20;
		int s_earth_quake = 20;
		int s_group_hold = actor.getCurrentHpPercents() > 50 ? 0 : 20;
		int s_thunderbolt = actor.getCurrentHpPercents() > 25 ? 0 : 20;

		L2Skill r_skill = null;

		if(actor.isRooted())
			r_skill = thunderbolt;
		else if(!Rnd.chance(100 - s_thunderbolt - s_group_hold - s_energy_wave - s_earth_quake))
		{
			FastMap<L2Skill, Integer> d_skill = new FastMap<L2Skill, Integer>();
			double distance = actor.getDistance(target);

			addDesiredSkill(d_skill, target, distance, energy_wave);
			addDesiredSkill(d_skill, target, distance, earth_quake);
			if(s_group_hold > 0)
				addDesiredSkill(d_skill, target, distance, group_hold);
			if(s_thunderbolt > 0)
				addDesiredSkill(d_skill, target, distance, thunderbolt);
			r_skill = selectTopSkill(d_skill);
		}


		if(r_skill == null)
			r_skill = baium_normal_attack;
		else if(r_skill.getTargetType() == L2Skill.SkillTargetType.TARGET_SELF)
			target = actor;


		addTaskCast(target, r_skill);
		r_skill = null;
		return true;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return false;
		if(actor != null && !BaiumManager.getZone().checkIfInZone(actor))
			teleportHome(true);
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
