package l2n.game.scripts.ai.grandboss;

import l2n.game.ai.Priest;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.ValakasManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class Valakas extends Priest
{
	private static final L2Skill valakas_lava_skin = SkillTable.getInstance().getInfo(4680, 1);

	public Valakas(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		ValakasManager.setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean createNewTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		// Если стоим на лаве, то используем защитный скилл
		if(Rnd.chance(30) && ZoneManager.getInstance().checkIfInZone(ZoneType.poison, actor))
		{
			clearTasks();
			return chooseTaskAndTargets(valakas_lava_skin, actor, 0);
		}

		return super.createNewTask();
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		L2NpcInstance actor = getActor();
		if(actor != null && !ValakasManager.getZone().checkIfInZone(actor.getX(), actor.getY()))
			teleportHome(true);
		return false;
	}
}
