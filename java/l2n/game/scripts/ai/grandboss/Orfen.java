package l2n.game.scripts.ai.grandboss;

import l2n.commons.text.PrintfFormat;
import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.scripts.npc.OrfenInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Orfen extends Fighter
{
	public static final PrintfFormat[] MsgOnRecall = {
			new PrintfFormat("%s. Перестанте обманывать себя вы тут безсильны!"),
			new PrintfFormat("%s. Я заставлю тебя почувствовать, что такое настоящий страх!"),
			new PrintfFormat("Ты действительно глуп, что бросил мне вызов. %s! Приготовся!"),
			new PrintfFormat("%s. Вы думаете, что это будет работать?!") };

	public final L2Skill[] _paralyze;

	public Orfen(L2Character actor)
	{
		super(actor);
		_paralyze = getActor().getTemplate().getDamageSkills();
	}

	@Override
	protected boolean thinkActive()
	{
		if(super.thinkActive())
			return true;
		OrfenInstance actor = getActor();
		if(actor == null)
			return true;

		if(actor.isTeleported() && actor.getCurrentHpPercents() > 95)
		{
			actor.setTeleported(false);
			return true;
		}

		return false;
	}

	@Override
	protected boolean createNewTask()
	{
		return defaultNewTask();
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		super.onEvtAttacked(attacker, damage);
		OrfenInstance actor = getActor();
		if(actor == null || actor.isCastingNow())
			return;

		double distance = actor.getDistance(attacker);

		// if(attacker.isMuted() &&)
		if(distance > 300 && distance < 1000 && _dam_skills.length > 0 && Rnd.chance(10))
		{
			Functions.npcSay(actor, MsgOnRecall[Rnd.get(MsgOnRecall.length - 1)].sprintf(attacker.getName()));
			teleToLocation(attacker, Location.getAroundPosition(actor, attacker, 0, 50, 3));
			L2Skill r_skill = _dam_skills[Rnd.get(_dam_skills.length)];
			if(canUseSkill(r_skill, attacker, -1))
				addUseSkillDesire(attacker, r_skill, 1000000);
		}
		else if(_paralyze.length > 0 && Rnd.chance(20))
		{
			L2Skill r_skill = _paralyze[Rnd.get(_paralyze.length)];
			if(canUseSkill(r_skill, attacker, -1))
				addUseSkillDesire(attacker, r_skill, 1000000);
		}
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		super.onEvtSeeSpell(skill, caster);
		OrfenInstance actor = getActor();
		if(actor == null || actor.isCastingNow())
			return;

		double distance = actor.getDistance(caster);
		if(_dam_skills.length > 0 && skill.getAggroPoints() > 0 && distance < 1000 && Rnd.chance(20))
		{
			Functions.npcSay(actor, MsgOnRecall[Rnd.get(MsgOnRecall.length)].sprintf(caster.getName()));
			teleToLocation(caster, Location.getAroundPosition(actor, caster, 0, 50, 3));
			L2Skill r_skill = _dam_skills[Rnd.get(_dam_skills.length)];
			if(canUseSkill(r_skill, caster, -1))
				addUseSkillDesire(caster, r_skill, 1000000);
		}
	}

	@Override
	public OrfenInstance getActor()
	{
		return (OrfenInstance) super.getActor();
	}

	private void teleToLocation(L2Character attacker, Location loc)
	{
		attacker.setLoc(loc);
		attacker.setLastClientPosition(loc);
		attacker.setLastServerPosition(loc);
		attacker.validateLocation(1);
	}
}
