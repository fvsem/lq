package l2n.game.scripts.ai.grandboss;

import l2n.game.ai.Priest;
import l2n.game.instancemanager.boss.AntharasManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

public class Antharas extends Priest
{
	public Antharas(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		AntharasManager.setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		L2NpcInstance actor = getActor();
		if(actor != null && !AntharasManager.getZone().checkIfInZone(actor.getX(), actor.getY()))
			teleportHome(true);
		return false;
	}
}
