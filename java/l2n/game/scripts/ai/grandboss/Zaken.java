package l2n.game.scripts.ai.grandboss;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.DayNightChangeListener;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.game.GameTimeController;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.DefaultAI;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.instancemanager.boss.ZakenManager.ZakenInstanceInfo;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExSendUIEvent;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class Zaken extends DefaultAI
{
	// Npc Day and Night
	private static final int[] mobs = { 29023, 29024, 29026, 29027 };

	// Skills
	private final int FaceChanceNightToDay = 4223;
	private final int FaceChanceDayToNight = 4224;
	private final L2Skill AbsorbHPMP;
	private final L2Skill Hold;
	private final L2Skill DeadlyDualSwordWeapon;
	private final L2Skill DeadlyDualSwordWeaponRangeAttack;
	// Other
	private final float _baseHpReg;
	private final float _baseMpReg;
	private boolean _isInLightRoom = false;

	// Listener
	private static final L2Zone _zone = ZoneManager.getInstance().getZoneById(ZoneType.no_restart, 1335, true);
	private ZoneListener _zoneListener = new ZoneListener();
	private NightInvulDayNightListener _timeListener = new NightInvulDayNightListener();

	private long lastSpawnTime = 0;

	public Zaken(L2Character actor)
	{
		super(actor);

		TIntObjectHashMap<L2Skill> skills = getActor().getTemplate().getSkills();

		AbsorbHPMP = skills.get(4218);
		Hold = skills.get(4219);
		DeadlyDualSwordWeapon = skills.get(4220);
		DeadlyDualSwordWeaponRangeAttack = skills.get(4221);

		_baseHpReg = actor.getTemplate().baseHpReg;
		_baseMpReg = actor.getTemplate().baseMpReg;
	}

	@Override
	protected void thinkAttack()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(lastSpawnTime < System.currentTimeMillis())
		{
			spawnGuards(mobs[Rnd.get(mobs.length)], actor.getLoc(), actor.getReflection(), actor);
			spawnGuards(mobs[Rnd.get(mobs.length)], actor.getLoc(), actor.getReflection(), actor);
			spawnGuards(mobs[Rnd.get(mobs.length)], actor.getLoc(), actor.getReflection(), actor);
			lastSpawnTime = System.currentTimeMillis() + 120000;
		}

		super.thinkAttack();
	}

	private void spawnGuards(int mob, Location loc, Reflection ref, L2NpcInstance actor)
	{
		L2NpcInstance npc;
		try
		{
			L2Spawn spawn = new L2Spawn(NpcTable.getTemplate(mob));
			ref.addSpawn(spawn);
			spawn.setReflection(ref.getId());
			spawn.setRespawnDelay(0, 0);
			spawn.setLocation(0);
			spawn.setLoc(loc.rnd(50, 100, false));
			npc = spawn.doSpawn(true);
			if(npc != null)
				npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, getRandomTarget(actor), Rnd.get(1, 100));
			spawn.stopRespawn();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	private L2Character getRandomTarget(L2NpcInstance npc)
	{
		GArray<L2Player> list = npc.getAroundPlayers(400);
		if(list.isEmpty())
			return npc.getRandomHated();
		return list.get(Rnd.get(list.size()));
	}

	@Override
	protected boolean createNewTask()
	{
		clearTasks();
		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		int rnd_per = Rnd.get(100);

		double distance = actor.getDistance(target);

		if(!actor.isAttackMuted() && rnd_per < 75)
			return chooseTaskAndTargets(null, target, distance);

		FastMap<L2Skill, Integer> d_skill = new FastMap<L2Skill, Integer>();

		addDesiredSkill(d_skill, target, distance, DeadlyDualSwordWeapon);
		addDesiredSkill(d_skill, target, distance, DeadlyDualSwordWeaponRangeAttack);
		addDesiredSkill(d_skill, target, distance, Hold);
		addDesiredSkill(d_skill, target, distance, AbsorbHPMP);

		L2Skill r_skill = selectTopSkill(d_skill);

		return chooseTaskAndTargets(r_skill, target, distance);
	}

	/**
	 * Метод вызываемый при смерте закена.
	 */
	@Override
	protected void onEvtDead(final L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null)
			actor.broadcastPacket(new PlaySound(1, "BS02_D", 1, actor.getObjectId(), actor.getLoc()));

		Reflection r = actor.getReflection();
		String name = InstanceManager.getInstance().getById(r.getInstancedZoneId()).get(0).getName();
		ZakenInstanceInfo instanceInfo = InstanceManager.getInstance().getWorld(actor.getReflectionId(), ZakenInstanceInfo.class);
		if(instanceInfo != null)
		{
			for(L2Player member : instanceInfo.getPlayers())
			{
				if(member != null)
				{
					member.sendPacket(new ExSendUIEvent(member, true, true, 0, 10, ""));
					member.setVar(name, String.valueOf(System.currentTimeMillis()));
				}
			}
		}
		super.onEvtDead(killer);
	}

	/**
	 * Запуск АИ
	 */
	@Override
	public void startAITask()
	{
		if(!isActive())
		{
			GameTimeController.getInstance().getListenerEngine().addPropertyChangeListener(PropertyCollection.GameTimeControllerDayNightChange, _timeListener);
			_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);
		}
		super.startAITask();
	}

	/**
	 * Остановка АИ.
	 */
	@Override
	public void stopAITask()
	{
		if(isActive())
		{
			GameTimeController.getInstance().getListenerEngine().removePropertyChangeListener(PropertyCollection.GameTimeControllerDayNightChange, _timeListener);
			_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
		}
		super.stopAITask();
	}

	/**
	 * Листенер времени суток. (День, ночь)
	 */
	private class NightInvulDayNightListener extends DayNightChangeListener
	{
		private NightInvulDayNightListener()
		{
			if(GameTimeController.getInstance().isNowNight())
				switchToNight();
			else
				switchToDay();
		}

		/**
		 * Вызывается, когда на сервере наступает ночь
		 */
		@Override
		public void switchToNight()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
			{
				if(_isInLightRoom)
				{
					actor.getTemplate().baseHpReg = (float) (_baseHpReg * 7.5);
					actor.getTemplate().baseMpReg = (float) (_baseMpReg * 7.5);
				}
				else
				{
					actor.getTemplate().baseHpReg = (float) (_baseHpReg * 10.);
					actor.getTemplate().baseMpReg = (float) (_baseMpReg * 10.);
				}
				actor.broadcastPacket(new MagicSkillUse(actor, actor, FaceChanceDayToNight, 1, 1100, 0));
			}
		}

		/**
		 * Вызывается, когда на сервере наступает день
		 */
		@Override
		public void switchToDay()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
			{
				actor.getTemplate().baseHpReg = _baseHpReg;
				actor.getTemplate().baseMpReg = _baseMpReg;
				actor.broadcastPacket(new MagicSkillUse(actor, actor, FaceChanceNightToDay, 1, 1100, 0));
			}
		}
	}

	/**
	 * Листенер зон (вход - выход).
	 */
	private class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(L2Zone zone, L2Character character)
		{
			L2NpcInstance actor = getActor();
			if(actor == null)
				return;
			actor.getTemplate().baseHpReg = (float) (_baseHpReg * 0.75);
			actor.getTemplate().baseMpReg = (float) (_baseMpReg * 0.75);
			_isInLightRoom = true;
		}

		@Override
		public void objectLeaved(L2Zone zone, L2Character character)
		{
			L2NpcInstance actor = getActor();
			if(actor == null)
				return;
			actor.getTemplate().baseHpReg = _baseHpReg;
			actor.getTemplate().baseMpReg = _baseMpReg;
			_isInLightRoom = false;
		}
	}
}
