package l2n.game.scripts.ai;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;


public class Leogul extends Fighter
{
	public Leogul(L2Character actor)
	{
		super(actor);
	}

	@Override
	public void checkAggression(L2Character target)
	{
		CtrlIntention curIntention = getIntention();
		super.checkAggression(target);

		if(curIntention != CtrlIntention.AI_INTENTION_ACTIVE || getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
			return;

		L2NpcInstance actor = getActor();
		if(actor == null || target == null || target.getPlayer() == null)
			return;

		Functions.npcSayCustomMessage(actor, "scripts.ai.Leogul");
		GArray<L2NpcInstance> around = actor.getAroundNpc(800, 128);
		if(!around.isEmpty())
			for(L2NpcInstance npc : around)
				if(npc.isMonster() && npc.getNpcId() >= 22660 && npc.getNpcId() <= 22677)
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, target, 5000);
	}
}
