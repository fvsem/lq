package l2n.game.scripts.ai.event;

import l2n.game.ai.DefaultAI;
import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.instancemanager.games.HandysBlockCheckerManager.ArenaParticipantsHolder;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.BlockCheckerEngine;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExCubeGameChangePoints;
import l2n.game.network.serverpackets.ExCubeGameExtendedChangePoints;
import l2n.game.scripts.npc.L2BlockInstance;
import l2n.game.tables.ItemTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class HandysBlock extends DefaultAI
{
	public HandysBlock(L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || skill == null || !caster.isPlayer())
			return;

		L2Player player = caster.getPlayer();
		if(actor instanceof L2BlockInstance && player.getTarget() == actor)
			if(skill.getId() == 5852 || skill.getId() == 5853)
			{
				L2BlockInstance block = (L2BlockInstance) actor;
				ArenaParticipantsHolder holder = HandysBlockCheckerManager.getInstance().getHolder(player.getHandysBlockCheckerEventArena());

				if(holder.getPlayerTeam(player) == 0 && !block.isRed())
				{
					block.changeColor(player);
					increaseTeamPointsAndSend(player, holder.getEvent());
				}
				else if(holder.getPlayerTeam(player) == 1 && block.isRed())
				{
					block.changeColor(player);
					increaseTeamPointsAndSend(player, holder.getEvent());
				}
				else
					return;

				// 30% chance to drop the event items
				int random = Rnd.get(100);
				// Bond
				if(random > 69 && random <= 84)
					dropItem(block, 13787, holder.getEvent(), player);
				// Land Mine
				else if(random > 84)
					dropItem(block, 13788, holder.getEvent(), player);
			}
	}

	private void increaseTeamPointsAndSend(L2Player player, BlockCheckerEngine eng)
	{
		int team = eng.getHolder().getPlayerTeam(player);
		eng.increasePlayerPoints(player, team);

		int timeLeft = (int) ((eng.getStarterTime() - System.currentTimeMillis()) / 1000);
		boolean isRed = eng.getHolder().getRedPlayers().contains(player);

		ExCubeGameChangePoints changePoints = new ExCubeGameChangePoints(timeLeft, eng.getBluePoints(), eng.getRedPoints());
		ExCubeGameExtendedChangePoints secretPoints = new ExCubeGameExtendedChangePoints(timeLeft, eng.getBluePoints(), eng.getRedPoints(), isRed, player, eng.getPlayerPoints(player, isRed));

		eng.getHolder().broadCastPacketToTeam(changePoints);
		eng.getHolder().broadCastPacketToTeam(secretPoints);
	}

	private void dropItem(L2BlockInstance block, int id, BlockCheckerEngine eng, L2Player player)
	{
		L2ItemInstance drop = ItemTable.getInstance().createItem(id, 0, block.getNpcId(), "HandysBlockAI");
		int x = block.getX() + Rnd.get(50);
		int y = block.getY() + Rnd.get(50);
		int z = block.getZ();

		drop.dropToTheGround(block, new Location(x, y, z));

		eng.addNewDrop(drop);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}
