package l2n.game.scripts.ai.raidboss;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;

/**
 * АИ для мобов Tyrannosaurus
 */
public class Tyrannosaurus extends Fighter
{
	public Tyrannosaurus(L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean canSeeInSilentMove(L2Playable target)
	{
		// Tyrannosaurus всегда видит игроков в режиме Silent Move
		return true;
	}
}
