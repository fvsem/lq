package l2n.game.scripts.ai.raidboss;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.DefaultAI;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2TrapInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class Darnel extends DefaultAI
{
	private class TrapTask implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
				return;

			// Спавним 10 ловушек
			for(int i = 0; i < 10; i++)
				new L2TrapInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(13037), actor, trapSkills[Rnd.get(trapSkills.length)], new Location(Rnd.get(151896, 153608), Rnd.get(145032, 146808), -12584));
		}
	}

	final L2Skill[] trapSkills = new L2Skill[] {
			SkillTable.getInstance().getInfo(5267, 1),
			SkillTable.getInstance().getInfo(5268, 1),
			SkillTable.getInstance().getInfo(5269, 1),
			SkillTable.getInstance().getInfo(5270, 1) };

	final L2Skill Poison;
	final L2Skill Paralysis;

	public Darnel(L2Character actor)
	{
		super(actor);

		TIntObjectHashMap<L2Skill> skills = getActor().getTemplate().getSkills();

		Poison = skills.get(4182);
		Paralysis = skills.get(4189);
	}

	@Override
	protected boolean createNewTask()
	{
		clearTasks();
		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		int rnd_per = Rnd.get(100);

		if(rnd_per < 5)
		{
			actor.broadcastPacketToOthers(new MagicSkillUse(actor, actor, 5440, 1, 3000, 0));
			L2GameThreadPools.getInstance().scheduleGeneral(new TrapTask(), 3000);
			return true;
		}

		double distance = actor.getDistance(target);

		if(!actor.isAttackMuted() && rnd_per < 75)
			return chooseTaskAndTargets(null, target, distance);

		FastMap<L2Skill, Integer> d_skill = new FastMap<L2Skill, Integer>();

		addDesiredSkill(d_skill, target, distance, Poison);
		addDesiredSkill(d_skill, target, distance, Paralysis);

		L2Skill r_skill = selectTopSkill(d_skill);

		return chooseTaskAndTargets(r_skill, target, distance);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
