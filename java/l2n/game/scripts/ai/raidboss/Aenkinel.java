package l2n.game.scripts.ai.raidboss;

import l2n.game.ai.Fighter;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.AenkinelChestInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

/**
 * AI рейд босов Aenkinel в Delusion Chamber
 * 
 */
public class Aenkinel extends Fighter
{
	public final static int NIHIL_INVADER_TREASURE_CHEST = 18820;
	public final static int MUTANT_TREASURE_CHEST = 18823;

	public Aenkinel(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		// Устанавливаем реюз для Tower и Great Seal
		// По оффу после убийства Рб в зал башней нельзя заходить сутки.
		if(actor.getNpcId() == 25694 || actor.getNpcId() == 25695)
		{
			String refName = actor.getReflection().getName();
			for(L2Player p : L2World.getAroundPlayers(actor))
				if(p != null)
					p.setVar(refName, String.valueOf(System.currentTimeMillis()));
		}

		// спаун сундуков
		// босс 25694 - сундук 18820, 4 шт
		// босс 25695 - сундук 18823, 4 шт
		int chestId = actor.getNpcId() == 25694 ? NIHIL_INVADER_TREASURE_CHEST : actor.getNpcId() == 25695 ? MUTANT_TREASURE_CHEST : 0;
		if(chestId != 0)
		{
			// координаты спауна, спауним квадратом)
			Location[] loc = {
					new Location(actor.getX(), actor.getY() - 40, actor.getZ(), 0),
					new Location(actor.getX(), actor.getY() + 40, actor.getZ(), 0),
					new Location(actor.getX() + 40, actor.getY(), actor.getZ(), 0),
					new Location(actor.getX() - 40, actor.getY(), actor.getZ(), 0),
			};

			// запоминаем точку куда спауним настоящего
			int realChest = Rnd.get(loc.length);
			for(int i = 0; i < 4; i++)
			{
				try
				{
					AenkinelChestInstance npc = new AenkinelChestInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(chestId), i == realChest ? false : true);
					npc.setSpawnedLoc(loc[i]);
					npc.setReflection(actor.getReflection().getId());
					npc.onSpawn();
					npc.spawnMe(npc.getSpawnedLoc());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}

		super.onEvtDead(killer);
	}
}
