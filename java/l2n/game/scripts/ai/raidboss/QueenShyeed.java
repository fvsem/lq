package l2n.game.scripts.ai.raidboss;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager.RaidBossStatus;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.logging.Level;


public class QueenShyeed extends Fighter
{
	private static final L2Zone stakato_mob_buff = ZoneManager.getInstance().getZoneById(ZoneType.poison, 900001, false);
	private static final L2Zone stakato_mob_buff_display = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 900002, false);
	private static final L2Zone stakato_pc_buff = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 900003, true);
	private static final L2Zone stakato_announce = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 900004, true);

	private static final SystemMessage SPAWN_MSG = new SystemMessage(SystemMessage.SHYEED_S_ROAR_FILLED_WITH_WRATH_RINGS_THROUGHOUT_THE_STAKATO_NEST);
	private static final SystemMessage DYING_MSG = SystemMessage.sendString("Shyeed's cry is steadily dying down.");

	public QueenShyeed(L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtSpawn()
	{
		// при спауне запускаем таймер
		L2GameThreadPools.getInstance().scheduleAi(new ActivateZone(), 15 * 1000, false);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		stakato_announce.broadcastPacket(DYING_MSG);

		// при смерте меняет зоны.
		stakato_mob_buff.setActive(false);
		stakato_mob_buff_display.setActive(false);
		stakato_pc_buff.setActive(true);
		super.onEvtDead(killer);
	}

	private final class ActivateZone implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				L2NpcInstance actor = getActor();
				if(actor == null || actor.isDead())
					return;

				if(RaidBossSpawnManager.getInstance().getRaidBossStatusId(actor.getNpcId()) != RaidBossStatus.DEAD)
				{
					stakato_announce.broadcastPacket(SPAWN_MSG);
					// меняем активность зоны
					stakato_mob_buff.setActive(true);
					stakato_mob_buff_display.setActive(true);
					stakato_pc_buff.setActive(false);
				}
			}
			catch(Throwable e)
			{
				_log.log(Level.SEVERE, "", e);
			}
		}
	}
}
