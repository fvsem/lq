package l2n.game.scripts.ai.raidboss;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.DefaultAI;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Party;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;

public class Tears extends DefaultAI
{
	private class DeSpawnTask implements Runnable
	{
		@Override
		public void run()
		{
			for(L2NpcInstance npc : spawns)
				if(npc != null)
					npc.deleteMe();
			spawns.clear();
			despawnTask = null;
		}
	}

	private class SpawnMobsTask implements Runnable
	{
		@Override
		public void run()
		{
			spawnMobs();
			spawnTask = null;
		}
	}

	final L2Skill Invincible;
	final L2Skill Freezing;

	private static final int Water_Dragon_Scale = 2369;
	private static final int Tears_Copy = 25535;

	ScheduledFuture<?> spawnTask;
	ScheduledFuture<?> despawnTask;

	GArray<L2NpcInstance> spawns = new GArray<L2NpcInstance>();

	private boolean _isUsedInvincible = false;

	private int _scale_count = 0;
	private long _last_scale_time = 0;

	public Tears(L2Character actor)
	{
		super(actor);

		TIntObjectHashMap<L2Skill> skills = getActor().getTemplate().getSkills();

		Invincible = skills.get(5420);
		Freezing = skills.get(5238);
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || skill == null || caster == null)
			return;

		if(System.currentTimeMillis() - _last_scale_time > 5000)
			_scale_count = 0;

		if(skill.getId() == Water_Dragon_Scale)
		{
			_scale_count++;
			_last_scale_time = System.currentTimeMillis();
		}

		L2Player player = caster.getPlayer();
		if(player == null)
			return;

		int count = 1;
		L2Party party = player.getParty();
		if(party != null)
			count = party.getMemberCount();

		// Снимаем неуязвимость
		if(_scale_count >= count)
		{
			_scale_count = 0;
			actor.getEffectList().stopEffect(Invincible.getId());
		}
	}

	@Override
	protected boolean createNewTask()
	{
		clearTasks();
		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		double distance = actor.getDistance(target);
		double actor_hp_precent = actor.getCurrentHpPercents();
		int rnd_per = Rnd.get(100);

		if(actor_hp_precent < 15 && !_isUsedInvincible)
		{
			_isUsedInvincible = true;
			addTaskBuff(actor, Invincible);
			Functions.npcSay(actor, "  !!!");
			return true;
		}

		if(rnd_per < 5 && spawnTask == null && despawnTask == null)
		{
			actor.broadcastPacketToOthers(new MagicSkillUse(actor, actor, 5441, 1, 3000, 0));
			spawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnMobsTask(), 3000);
			return true;
		}

		if(rnd_per < 75)
			return chooseTaskAndTargets(null, target, distance);

		return chooseTaskAndTargets(Freezing, target, distance);
	}

	private void spawnMobs()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		Location pos;
		L2Character hated;

		// 9
		for(int i = 0; i < 9; i++)
			try
			{
				pos = GeoEngine.findPointToStay(144298, 154420, -11854, 300, 320);
				L2Spawn sp = new L2Spawn(NpcTable.getTemplate(Tears_Copy));
				sp.setLoc(pos);
				sp.setReflection(actor.getReflection().getId());
				L2NpcInstance copy = sp.doSpawn(true);
				spawns.add(copy);

				//
				hated = actor.getRandomHated();
				if(hated != null)
					copy.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, hated, Rnd.get(1, 100));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

		//
		pos = GeoEngine.findPointToStay(144298, 154420, -11854, 300, 320);
		actor.teleToLocation(pos);

		//
		hated = actor.getRandomHated();
		if(hated != null)
			actor.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, hated, Rnd.get(1, 100));

		if(despawnTask != null)
			despawnTask.cancel(false);
		despawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new DeSpawnTask(), 30000);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
