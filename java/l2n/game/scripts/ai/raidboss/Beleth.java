package l2n.game.scripts.ai.raidboss;

import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.instancemanager.boss.BelethManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class Beleth extends Mystic
{
	public Beleth(final L2Character actor)
	{
		super(actor);
		setMaxPursueRange(-1);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isDead())
		{
			// смотрим сколько ХП в процентах
			final int hp = (int) actor.getCurrentHpPercents();
			if(hp > 2 && hp < 50)
				if(Rnd.chance(2))
					BelethManager.getInstance().sendEventIdMinions();
		}

		BelethManager.getInstance().setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		BelethManager.getInstance().onBelethDie();
		super.onEvtDead(killer);
	}

	@Override
	public final boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}
