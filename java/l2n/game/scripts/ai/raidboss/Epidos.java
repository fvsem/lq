package l2n.game.scripts.ai.raidboss;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.instancemanager.boss.EpidosManager;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.ai.hellbound.NaiaSpore;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.logging.Level;

public class Epidos extends Mystic
{
	private final class ManageSporeSpawn implements Runnable
	{
		private final L2NpcInstance epidos;

		private ManageSporeSpawn(L2NpcInstance actor)
		{
			epidos = actor;
		}

		@Override
		public void run()
		{
			while (epidos != null && !epidos.isDead())
			{
				if(Rnd.chance(75))
				{
					double hp = epidos.getCurrentHpPercents();
					if(hp > 5 && hp <= 15)
					{
						int spore = getSporeCount();
						if(spore <= 9)
							spawnSpore(epidos.getLoc(), 3);
					}
					else if(hp > 15 && hp <= 30 || hp > 1 && hp <= 5)
					{
						int spore = getSporeCount();
						if(spore <= 6)
							spawnSpore(epidos.getLoc(), 6);
					}

					int spore = getSporeCount();
					if(spore == 0)
						spawnSpore(epidos.getLoc(), 12);
				}

				try
				{
					Thread.sleep(10 * 1000);
				}
				catch(Exception e)
				{
					_log.log(Level.WARNING, "ManageSporeSpawn error: ", e);
				}
			}
		}
	}

	private final int aspect_myself;
	private L2Spawn spore;

	public Epidos(L2Character actor)
	{
		super(actor);
		aspect_myself = getAIParams().getInt(AiOptionsType.OPTIONS, 0);
		setMaxPursueRange(-1);

		L2NpcTemplate template = NpcTable.getTemplate(getSporeId());
		try
		{
			spore = new L2Spawn(template);
			spore.setAmount(1);
			spore.setRespawnDelay(0);
			spore.setRespawnTime(0);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Epidos: fail spawn spore: ", e);
		}
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		EpidosManager.getInstance().setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	public void onEvtSpawn()
	{
		L2GameThreadPools.getInstance().scheduleAi(new ManageSporeSpawn(getActor()), 5000, false);
		// спускаемся вниз после спауна
		addTaskMove(NaiaSpore.MERGE_POINTS[Rnd.get(NaiaSpore.MERGE_POINTS.length)], Integer.MAX_VALUE);
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		spore.despawnAll();
		spore = null;

		EpidosManager.getInstance().onEpidosDie();
		super.onEvtDead(killer);
	}

	@Override
	public void onScriptEvent(final int eventId, final Object... args)
	{
		if(eventId == 78010035 && spore != null)
		{
			spore.despawnAll();
			spore = null;
		}
		super.onScriptEvent(eventId, args);
	}

	/**
	 * спуанит споры рядом с Эпидосом
	 * 
	 * @param loc
	 * @param count
	 */
	private void spawnSpore(final Location loc, int count)
	{
		spore.setLoc(loc);

		L2NpcInstance npc;
		while (count-- > 0)
		{
			npc = spore.spawnOne();
			npc.getAI().startAITask();
		}
	}

	private int getSporeCount()
	{
		return spore.getAllLiveSpawned().size();
	}

	private int getSporeId()
	{
		switch (aspect_myself)
		{
			case 0:
				return EpidosManager.SPORE_FIRE;
			case 1:
				return EpidosManager.SPORE_WATER;
			case 2:
				return EpidosManager.SPORE_WIND;
			case 3:
				return EpidosManager.SPORE_EARTH;
		}
		return -1;
	}

	@Override
	public final boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}
}
