package l2n.game.scripts.ai.raidboss;

import l2n.commons.list.GArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager.RaidBossStatus;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.L2ObjectProcedures;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.npc.hellbound.DwarvenGhost;
import l2n.game.scripts.npc.hellbound.SelfDestructer;
import l2n.game.tables.DoorTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * упарвляет прохождением 5 этажа. Включает таймер на включение зоны, спаунит Ingenious Contraption для получения медалей и тд.
 * 
 */
public class Tully extends Fighter
{
	private final static Logger _log = Logger.getLogger(Tully.class.getName());

	private final class ActivateTully implements Runnable
	{
		@Override
		public void run()
		{
			// если РБ Tully уже был убит, спауним гномов)
			if(RaidBossSpawnManager.getInstance().getRaidBossStatusId(NPC_ID) == RaidBossStatus.DEAD)
				spawnDwarvenGhost();
			// всех деспауним и отчищаем списки
			else
			{
				despawnAll();
				// выключаем зону при спауне РБ
				TullyWorkshopManager.destruction_zone.setActive(false);
			}
		}
	}

	private static final int[] DOORS = new int[] { 19260051, 19260052 };

	public static final int NPC_ID = 25544;

	private static final int FAILED_TIMETWISTER = 22392;
	public static final int GHOST_TOLLES = 32370;
	private static final int TOMBSTONE_EVIL_FACTORY = 32344;
	private static final int SELF_DESTRUCTER = 32371;

	private static final GArray<L2Spawn> self_destructers = new GArray<L2Spawn>();
	private static final GArray<L2Spawn> failed_timetwisters = new GArray<L2Spawn>();
	private static final GArray<L2Spawn> ghost_tolles = new GArray<L2Spawn>();

	// с помощью них можно отключить включение зоны с дамагом
	private static final Location[] AFTER_DEATH_SELF_DESTRUCTER = new Location[]
	{
			new Location(-12524, 273932, -9014, 49151, 10427), // self_destructer[32371] -- mode 0, item 10427 (Tully's Platinum Medal)
			new Location(-10831, 273890, -9040, 81895, 10428), // self_destructer[32371] -- mode 1, room 0, item 10428 (Tully's Tin Medal)
			new Location(-10817, 273986, -9040, -16452, 10429), // self_destructer[32371] -- mode 1, room 1, item 10429 (Tully's Lead Medal)
			new Location(-13773, 275119, -9040, 8428, 10430), // self_destructer[32371] -- mode 1, room 2, item 10430 (Tully's Zinc Medal)
			new Location(-11547, 271772, -9040, -19124, 10431) // self_destructer[32371] -- mode 1, room 3, item 10431 (Tully's Copper Medal)
	};

	// Увеличивают время до срабатывания зоны с дамагом
	private static final Location[] AFTER_DEATH_FAILED_TIMETWISTER = new Location[]
	{
			new Location(-10832, 273808, -9040, 0, FAILED_TIMETWISTER), // failed_timetwister[22392]
			new Location(-10816, 274096, -9040, 14964, FAILED_TIMETWISTER), // failed_timetwister[22392]
			new Location(-13824, 275072, -9040, -24644, FAILED_TIMETWISTER), // failed_timetwister[22392]
			new Location(-11504, 271952, -9040, 9328, FAILED_TIMETWISTER) // failed_timetwister[22392]
	};

	private static final Location[] IF_DEATH = new Location[]
	{
			// получается спауняться после смерти Tully или если уже мертвый РБ
			new Location(-11984, 272928, -9040, 23644, GHOST_TOLLES), // ghost_tolles[32370] -- mode 1
			new Location(-14643, 274588, -9040, 49152, GHOST_TOLLES), // ghost_tolles[32370] -- mode 2

			new Location(-14756, 274788, -9040, -13868, TOMBSTONE_EVIL_FACTORY), // tombstone_evil_factory[32344] //FIXME поправить этого НПС
	};

	private static final int TIME_ALARM = 10 * 60;

	public Tully(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		// при спауне запускаем таймер
		L2GameThreadPools.getInstance().scheduleAi(new ActivateTully(), 15 * 1000, false);
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcTemplate template;
		L2Spawn spawnData;

		despawnAll();
		spawnDwarvenGhost();

		// закрываем двери после смерти
		// FIXME нихуя не понятно, если двери закрыть после убийства, то как игроки будут бегать по лабиринту оО
		for(int doorId : DOORS)
			DoorTable.getInstance().getDoor(doorId).openMe();

		SelfDestructer npc;
		for(Location loc : AFTER_DEATH_SELF_DESTRUCTER)
			try
			{
				template = NpcTable.getTemplate(SELF_DESTRUCTER);
				if(template == null)
				{
					_log.warning("Tully: mob with ID = " + SELF_DESTRUCTER + " can't be spawned!");
					continue;
				}
				spawnData = new L2Spawn(template);
				spawnData.setAmount(1);
				spawnData.setLoc(loc);
				spawnData.setRespawnDelay(0);
				npc = (SelfDestructer) spawnData.doSpawn(true);
				npc.setItem(loc.id);
				self_destructers.add(spawnData);
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "Tully: error spawn self_destructer: ", e);
			}

		for(Location loc : AFTER_DEATH_FAILED_TIMETWISTER)
			try
			{
				template = NpcTable.getTemplate(loc.id);
				if(template == null)
				{
					_log.warning("Tully: mob with ID = " + loc.id + " can't be spawned!");
					continue;
				}
				spawnData = new L2Spawn(template);
				spawnData.setAmount(1);
				spawnData.setLoc(loc);
				spawnData.setRespawnDelay(0);
				spawnData.doSpawn(true);
				failed_timetwisters.add(spawnData);
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "Tully: error spawn failed_timetwister: ", e);
			}

		// Запускаем таймер включения зоны
		TullyWorkshopManager.getInstance().runAlarmTask(true, TIME_ALARM, self_destructers.getFirst().getLastSpawn());

		super.onEvtDead(killer);
	}

	private void spawnDwarvenGhost()
	{
		L2NpcTemplate template;
		L2Spawn spawnData;
		L2NpcInstance npc;
		for(Location loc : IF_DEATH)
			try
			{
				template = NpcTable.getTemplate(loc.id);
				if(template == null)
				{
					_log.warning("Tully: mob with ID = " + loc.id + " can't be spawned!");
					continue;
				}
				spawnData = new L2Spawn(template);
				spawnData.setAmount(1);
				spawnData.setLoc(loc);
				spawnData.setRespawnDelay(0);
				npc = spawnData.doSpawn(true);
				if(loc.id == GHOST_TOLLES)
				{
					DwarvenGhost dwarven = (DwarvenGhost) npc;
					dwarven.sedMode(loc.y == 272928 ? 1 : 2);
				}
				ghost_tolles.add(spawnData);
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "Tully: error spawn ghost_tolles: ", e);
			}
	}

	private void despawnAll()
	{
		// всех деспауним и отчищаем списки
		self_destructers.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);
		failed_timetwisters.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);
		ghost_tolles.forEach(L2ObjectProcedures.PROC_DESPAWN_ALL);
		self_destructers.clear();
		failed_timetwisters.clear();
		ghost_tolles.clear();
	}
}
