package l2n.game.scripts.ai;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;


public class NoRndWalkFighter extends Fighter
{
	public NoRndWalkFighter(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
