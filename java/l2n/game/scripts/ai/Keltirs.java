package l2n.game.scripts.ai;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;


public class Keltirs extends Fighter implements ScriptFile
{
	private static final int[] _list = {
			20481,
			20529,
			20530,
			20531,
			20532,
			20533,
			20534,
			20535,
			20536,
			20537,
			20538,
			20539,
			20544,
			20545,
			22229,
			22230,
			22231,
			18003 };

	@Override
	public void onLoad()
	{
		if(Config.ALT_AI_KELTIRS)
			for(int id : _list)
			{
				for(L2NpcInstance i : L2ObjectsStorage.getAllByNpcId(id, false))
					i.setAI(new Keltirs(i));
				NpcTable.getTemplate(id).ai_type = "Keltirs";
			}
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// Радиус на который будут отбегать келтиры.
	private static final int range = 600;
	// Время в мс. через которое будет потвторяться Rnd фраза.
	private static final int voicetime = 8000;
	private long _lastAction;
	private static String[] _retreatText = {
			"Не трогай меня, я боюсь!",
			"Ты страшный! Братья, убегаем!",
			"Полундра! Сезон охоты открыт!!!",
			"Если еще раз меня ударишь - у тебя будут неприятности!",
			"Браконьер, я тебя сдам правоохранительным органам!",
			"Делаем ноги, за 60 сек ^-_-^",
			"Нас не догонят, нас не догонят..." };

	private static String[] _fightText = {
			"Всех убью, один останусь!",
			"Рррррррр!",
			"Бей гада!",
			"Хочешь, за жопу укушу",
			"Щас КУСЬ всем сделаю..." };

	public Keltirs(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean createNewTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		if(Rnd.chance(60))
		{
			// Удаляем все задания
			clearTasks();

			L2Character target;
			if((target = prepareTarget()) == null)
				return false;

			// Добавить новое задание
			addTaskAttack(target);

			if(System.currentTimeMillis() - _lastAction > voicetime)
			{
				Functions.npcShout(actor, _fightText[Rnd.get(_fightText.length)]);
				_lastAction = System.currentTimeMillis();
			}
			return true;
		}

		Location sloc = actor.getSpawnedLoc();
		int spawnX = sloc.getX();
		int spawnY = sloc.getY();
		int spawnZ = sloc.getZ();

		int x = spawnX + Rnd.get(2 * range) - range;
		int y = spawnY + Rnd.get(2 * range) - range;
		int z = GeoEngine.getHeight(x, y, spawnZ);

		actor.setRunning();

		actor.moveToLocation(x, y, z, 0, true);

		addTaskMove(new Location(spawnX, spawnY, spawnZ));

		if(System.currentTimeMillis() - _lastAction > voicetime)
		{
			Functions.npcShout(actor, _retreatText[Rnd.get(_retreatText.length)]);
			_lastAction = System.currentTimeMillis();
		}
		return true;
	}

}
