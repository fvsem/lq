package l2n.game.scripts.ai;

import l2n.game.ai.Mystic;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;


public class Lavasaurus extends Mystic
{
	public Lavasaurus(L2Character actor)
	{
		super(actor);
		actor.setImmobilized(true);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	public boolean checkTarget(L2NpcInstance actor, L2Character target, boolean canSelf, int range)
	{
		if(actor != null && target != null && !actor.isInRange(target, actor.getAggroRange()))
		{
			target.removeFromHatelist(actor, true);
			return false;
		}
		return super.checkTarget(actor, target, canSelf, range);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
