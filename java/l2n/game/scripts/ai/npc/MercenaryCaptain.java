package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class MercenaryCaptain extends DefaultAI
{
	private static final String[] dialogs = {
			"Вы хотите воевать? Вы боитесь? Независимо от того, как трудно Вы пробуете, вам некуда бежать. Но если вы перед этим головой, наш отряд наемников поможет Вам!",
			"Мужество! Амбиции! Страсть! Наемники, которые хотят реализовать свою мечту боевых действий в войне территории, приди ко мне! Удача и слава ждут Вас!",
			"Вы хотите воевать? Вы боитесь? Независимо от того, как трудно Вы пробуете, вам некуда бежать. Но если вы перед этим головой, наш отряд наемников поможет Вам!" };

	private long wait_timeout = 0;
	// 1 час
	private static int TIME_OUT = 60 * 60 * 1000;

	/**
	 * @param actor
	 */
	public MercenaryCaptain(L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(System.currentTimeMillis() > wait_timeout)
		{
			wait_timeout = System.currentTimeMillis() + TIME_OUT;
			int text = Rnd.get(0, 2);
			Functions.npcShout(actor, dialogs[text]);
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
