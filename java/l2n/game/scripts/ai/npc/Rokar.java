package l2n.game.scripts.ai.npc;

import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Rokar extends DefaultAI
{
	private Location[] points = new Location[10];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Rokar(L2Character actor)
	{
		super(actor);
		points[0] = new Location(-46516, -117700, -264);
		points[1] = new Location(-45550, -115420, -256);
		points[2] = new Location(-44052, -114575, -256);
		points[3] = new Location(-44024, -112688, -256);
		points[4] = new Location(-45748, -111665, -256);
		points[5] = new Location(-46512, -109390, -232);
		points[6] = new Location(-45748, -111665, -256);
		points[7] = new Location(-44024, -112688, -256);
		points[8] = new Location(-44052, -114575, -256);
		points[9] = new Location(-45550, -115420, -256);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 5:
						wait_timeout = System.currentTimeMillis() + 30000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
