package l2n.game.scripts.ai.npc;

import l2n.game.GameTimeController;
import l2n.game.ai.DefaultAI;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public final class Shadai extends DefaultAI
{
	private static final long CHECK_LIMIT = 3 * 60 * 1000; // 3 min
	private static final int chance = 40;
	
	private long _lastCheck = System.currentTimeMillis();

	public Shadai(final L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		if(System.currentTimeMillis() - _lastCheck < CHECK_LIMIT)
			return false;

		final L2NpcInstance actor = getActor();
		if(ServerVariables.getInt("HellboundConfidence", 0) < 1000000)
			if(GameTimeController.getInstance().isNowNight() == true)
			{
				if(!actor.isVisible())
					if(Rnd.chance(chance))
						actor.toggleVisible();
			}
			else if(actor.isVisible())
				actor.toggleVisible();

		_lastCheck = System.currentTimeMillis();

		return true;
	}

	@Override
	protected void onEvtSpawn()
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isVisible())
			actor.toggleVisible();
	}
	
	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
