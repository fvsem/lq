package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Remy extends DefaultAI
{
	private Location[] points = new Location[17];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Remy(L2Character actor)
	{
		super(actor);
		points[0] = new Location(-81926, 243894, -3712);
		points[1] = new Location(-82134, 243600, -3728);
		points[2] = new Location(-83165, 243987, -3728);
		points[3] = new Location(-84501, 243245, -3728);
		points[4] = new Location(-85100, 243285, -3728);
		points[5] = new Location(-86152, 242898, -3728);
		points[6] = new Location(-86288, 242962, -3720);
		points[7] = new Location(-86348, 243223, -3720);
		points[8] = new Location(-86522, 242762, -3720);
		points[9] = new Location(-86500, 242615, -3728);
		points[10] = new Location(-86123, 241606, -3728);
		points[11] = new Location(-85167, 240589, -3728);
		points[12] = new Location(-84323, 241245, -3728);
		points[13] = new Location(-83215, 241170, -3728);
		points[14] = new Location(-82364, 242944, -3728);
		points[15] = new Location(-81674, 243391, -3712);
		points[16] = new Location(-81926, 243894, -3712);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 0:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Доставка для Лектера? Очень хорошо!");
						wait = true;
						return true;
					case 3:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Мне нужен перерыв!");
						wait = true;
						return true;
					case 7:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Здравствуйте, г-н Лектор! Давно не виделись, мистер Джексон!");
						wait = true;
						return true;
					case 12:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Лилу!");
						wait = true;
						return true;
					case 15:
						wait_timeout = System.currentTimeMillis() + 60000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Remy всегда бегает
			actor.setRunning();

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
