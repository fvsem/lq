package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Kreed extends DefaultAI
{
	private Location[] points = new Location[9];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Kreed(L2Character actor)
	{
		super(actor);
		points[0] = new Location(23436, 11164, -3728);
		points[1] = new Location(20256, 11104, -3728);
		points[2] = new Location(17330, 13579, -3720);
		points[3] = new Location(17415, 13044, -3736);
		points[4] = new Location(20153, 12880, -3728);
		points[5] = new Location(21621, 13349, -3648);
		points[6] = new Location(20686, 10432, -3720);
		points[7] = new Location(22426, 10260, -3648);
		points[8] = new Location(23436, 11164, -3728);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 3:
						wait_timeout = System.currentTimeMillis() + 15000;
						wait = true;
						return true;
					case 7:
						wait_timeout = System.currentTimeMillis() + 60000;
						Functions.npcShout(actor, "Массовая тьма начнется через пару дней. Уделите больше внимания к охране!");
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
