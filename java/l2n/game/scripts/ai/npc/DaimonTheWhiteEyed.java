package l2n.game.scripts.ai.npc;

import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class DaimonTheWhiteEyed extends DefaultAI
{
	private Location[] points = new Location[24];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public DaimonTheWhiteEyed(L2Character actor)
	{
		super(actor);
		points[0] = new Location(191276, -49556, -2960);
		points[1] = new Location(193537, -47182, -2984);
		points[2] = new Location(194317, -43736, -2872);
		points[3] = new Location(193336, -42510, -2888);
		points[4] = new Location(194633, -40843, -2872);
		points[5] = new Location(194498, -39516, -2912);
		points[6] = new Location(191985, -35868, -2904);
		points[7] = new Location(190083, -35015, -2912);
		points[8] = new Location(186256, -35136, -3072);
		points[9] = new Location(186256, -35136, -3072);
		points[10] = new Location(184477, -36749, -3080);
		points[11] = new Location(180834, -37288, -3104);
		points[12] = new Location(179653, -38946, -3176);
		points[13] = new Location(179854, -42412, -3248);
		points[14] = new Location(177627, -43341, -3336);
		points[15] = new Location(177842, -45723, -3456);
		points[16] = new Location(180459, -47145, -3256);
		points[17] = new Location(175858, -51288, -3496);
		points[18] = new Location(173028, -49337, -3520);
		points[19] = new Location(171936, -46364, -3472);
		points[20] = new Location(173074, -44264, -3488);
		points[21] = new Location(172575, -42937, -3464);
		points[22] = new Location(170964, -41753, -3464);
		points[23] = new Location(170428, -39132, -3432);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 23:
						wait_timeout = System.currentTimeMillis() + 5000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;
			current_point++;

			if(current_point >= points.length)
				current_point = 0;

			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
