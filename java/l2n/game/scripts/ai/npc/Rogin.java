package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Rogin extends DefaultAI
{
	private Location[] points = new Location[10];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Rogin(L2Character actor)
	{
		super(actor);
		points[0] = new Location(115756, -183472, -1480);
		points[1] = new Location(115866, -183287, -1480);
		points[2] = new Location(116280, -182918, -1520);
		points[3] = new Location(116587, -184306, -1568);
		points[4] = new Location(116392, -184090, -1560);
		points[5] = new Location(117083, -182538, -1528);
		points[6] = new Location(117802, -182541, -1528);
		points[7] = new Location(116720, -182479, -1528);
		points[8] = new Location(115857, -183295, -1480);
		points[9] = new Location(115756, -183472, -1480);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 3:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Видели ли вы сегодня Торокко?");
						wait = true;
						return true;
					case 6:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Видели ли вы Торокко?");
						wait = true;
						return true;
					case 7:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Где это дурак скрываться?");
						wait = true;
						return true;
					case 8:
						wait_timeout = System.currentTimeMillis() + 60000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
