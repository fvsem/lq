package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class EvilNpc extends DefaultAI
{
	private long _lastAction;
	private static String[] _txt = {
			"Что ты меня трогаешь? Не нравлюсь? На себя в зеркало посмотри! Крокодил!!!",
			"А с разбегу об стенку замка? Слабо?",
			"Сейчас придет начальник и поломает тебе ногу или руку!",
			"Отреж себе голову, идиот!",
			"За нападение ты можешь сесть в тюрьму!",
			"Да ты Изврашенец..Фуууу!",
			"Да смотрю для такого много  ума не надо!",
			"Знаешь, я тоже дратся умею, так что не выпендривайся!" };

	public EvilNpc(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || attacker == null || attacker.getPlayer() == null)
			return;

		actor.startAttackStanceTask();

		// Ругаемся и кастуем скилл не чаще, чем раз в 3 секунды
		if(System.currentTimeMillis() - _lastAction > 3000)
		{
			int chance = Rnd.get(0, 100);
			if(chance < 2)
			{
				attacker.getPlayer().setKarma(attacker.getPlayer().getKarma() + 5);
				attacker.sendChanges();
			}
			else if(chance < 4)
				actor.doCast(SkillTable.getInstance().getInfo(4578, 1), attacker, true); // Petrification
			else
				actor.doCast(SkillTable.getInstance().getInfo(4185, 7), attacker, true); // Sleep

			Functions.npcShout(actor, attacker.getName() + ", " + _txt[Rnd.get(_txt.length)]);
			_lastAction = System.currentTimeMillis();
		}
	}
}
