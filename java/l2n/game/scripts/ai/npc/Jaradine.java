package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Jaradine extends DefaultAI
{
	private Location[] points = new Location[7];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Jaradine(L2Character actor)
	{
		super(actor);
		points[0] = new Location(44964, 50568, -3056);
		points[1] = new Location(44435, 50025, -3056);
		points[2] = new Location(44399, 49078, -3056);
		points[3] = new Location(45058, 48437, -3056);
		points[4] = new Location(46132, 48724, -3056);
		points[5] = new Location(46452, 49743, -3056);
		points[6] = new Location(45730, 50590, -3056);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 3:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Древа Жизни медленно умирает.");
						wait = true;
						return true;
					case 4:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Как мы можем сохранить Древа?");
						wait = true;
						return true;
					case 6:
						wait_timeout = System.currentTimeMillis() + 60000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
