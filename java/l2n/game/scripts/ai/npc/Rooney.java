package l2n.game.scripts.ai.npc;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.util.Location;
import l2n.util.Rnd;

public class Rooney extends DefaultAI
{
	private Location[] _points = new Location[5];
	private static long TELEPORT_PERIOD = 15 * 60 * 1000; // 15 min
	private long _lastTeleport;

	public Rooney(L2Character actor)
	{
		super(actor);
		_points[0] = new Location(184022, -117083, -3342);
		_points[1] = new Location(183516, -118815, -3093);
		_points[2] = new Location(185007, -115651, -1587);
		_points[3] = new Location(186191, -116465, -1587);
		_points[4] = new Location(189630, -115611, -1587);
		_lastTeleport = System.currentTimeMillis();
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || System.currentTimeMillis() - _lastTeleport < TELEPORT_PERIOD)
			return false;

		for(int i = 0; i < _points.length; i++)
		{
			Location loc = _points[Rnd.get(_points.length)];
			if(actor.getLoc().equals(loc))
				continue;

			actor.broadcastPacketToOthers(new MagicSkillUse(actor, actor, 4671, 1, 500, 0));
			L2GameThreadPools.getInstance().scheduleAi(new Teleport(loc), 500, false);
			_lastTeleport = System.currentTimeMillis();
			break;
		}
		return true;
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}
}
