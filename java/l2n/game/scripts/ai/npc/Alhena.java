package l2n.game.scripts.ai.npc;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class Alhena extends DefaultAI
{
	private Location[] points = new Location[14];
	private int current_point = -1;
	private long wait_timeout = 0;
	private boolean wait = false;

	public Alhena(L2Character actor)
	{
		super(actor);
		points[0] = new Location(10968, 14620, -4248);
		points[1] = new Location(11308, 15847, -4584);
		points[2] = new Location(12119, 16441, -4584);
		points[3] = new Location(15104, 15661, -4376);
		points[4] = new Location(15265, 16288, -4376);
		points[5] = new Location(12292, 16934, -4584);
		points[6] = new Location(11777, 17669, -4584);
		points[7] = new Location(11229, 17650, -4576);
		points[8] = new Location(10641, 17282, -4584);
		points[9] = new Location(7683, 18034, -4376);
		points[10] = new Location(10551, 16775, -4584);
		points[11] = new Location(11004, 15942, -4584);
		points[12] = new Location(10827, 14757, -4248);
		points[13] = new Location(10968, 14620, -4248);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout && (current_point > -1 || Rnd.chance(5)))
		{
			if(!wait)
				switch (current_point)
				{
					case 3:
						wait_timeout = System.currentTimeMillis() + 15000;
						wait = true;
						return true;
					case 4:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Ты труженик, Раула!");
						wait = true;
						return true;
					case 9:
						wait_timeout = System.currentTimeMillis() + 15000;
						Functions.npcShout(actor, "Ты труженик!");
						wait = true;
						return true;
					case 12:
						wait_timeout = System.currentTimeMillis() + 60000;
						wait = true;
						return true;
				}

			wait_timeout = 0;
			wait = false;

			if(current_point >= points.length - 1)
				current_point = -1;

			current_point++;

			// Добавить новое задание
			addTaskMove(points[current_point]);
			doTask();
			return true;
		}

		if(randomAnimation())
			return true;

		return false;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
