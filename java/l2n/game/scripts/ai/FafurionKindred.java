package l2n.game.scripts.ai;

import l2n.commons.list.GCSArray;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncMul;
import l2n.game.skills.funcs.LambdaConst;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;


public class FafurionKindred extends Fighter
{
	private static final int DETRACTOR1 = 22270;
	private static final int DETRACTOR2 = 22271;

	private static final int Spirit_of_the_Lake = 2368;

	private static final int Water_Dragon_Scale = 9691;
	private static final int Water_Dragon_Claw = 9700;

	private ScheduledFuture<?> spawnTask1;
	private ScheduledFuture<?> spawnTask2;
	private ScheduledFuture<?> spawnTask3;
	private ScheduledFuture<?> spawnTask4;
	private ScheduledFuture<?> poisonTask;
	private ScheduledFuture<?> despawnTask;

	GCSArray<L2NpcInstance> spawns = new GCSArray<L2NpcInstance>();

	public FafurionKindred(L2Character actor)
	{
		super(actor);
		actor.addStatFunc(new FuncMul(Stats.HEAL_EFFECTIVNESS, 0x90, this, new LambdaConst(0)));
	}

	@Override
	public void startAITask()
	{
		if(!isActive())
		{
			spawns.clear();

			spawnMob(DETRACTOR1);
			spawnMob(DETRACTOR2);
			spawnMob(DETRACTOR1);
			spawnMob(DETRACTOR2);

			spawnTask1 = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SpawnTask(DETRACTOR1), 2000, 40000);
			spawnTask2 = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SpawnTask(DETRACTOR2), 4000, 40000);
			spawnTask3 = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SpawnTask(DETRACTOR1), 8000, 40000);
			spawnTask4 = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new SpawnTask(DETRACTOR2), 10000, 40000);
			poisonTask = L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new PoisonTask(), 3000, 3000);
			despawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new DeSpawnTask(), 300000);
		}
		super.startAITask();
	}

	@Override
	public void stopAITask()
	{
		if(isActive())
		{
			if(spawnTask1 != null)
			{
				spawnTask1.cancel(false);
				spawnTask1 = null;
			}
			if(spawnTask2 != null)
			{
				spawnTask2.cancel(false);
				spawnTask2 = null;
			}
			if(spawnTask3 != null)
			{
				spawnTask3.cancel(false);
				spawnTask3 = null;
			}
			if(spawnTask4 != null)
			{
				spawnTask4.cancel(false);
				spawnTask4 = null;
			}
			if(poisonTask != null)
			{
				poisonTask.cancel(false);
				poisonTask = null;
			}
			if(despawnTask != null)
			{
				despawnTask.cancel(false);
				despawnTask = null;
			}
			for(L2NpcInstance npc : spawns)
				if(npc != null)
					npc.deleteMe();
			spawns.clear();
		}
		super.stopAITask();
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || skill == null)
			return;
		// Лечим
		if(skill.getId() == Spirit_of_the_Lake)
			actor.setCurrentHp(actor.getCurrentHp() + 3000, false);
		caster.removeFromHatelist(actor, true);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	private class SpawnTask implements Runnable
	{
		private final int _id;

		public SpawnTask(int id)
		{
			_id = id;
		}

		@Override
		public void run()
		{
			spawnMob(_id);
		}
	}

	private class PoisonTask implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
				actor.reduceCurrentHp(500, actor, null, true, false, true, false, false); // Травим дракошу ядом
		}
	}

	private class DeSpawnTask implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
			{
				// Если продержались 5 минут, то выдаем награду, и деспавним
				dropItem(actor, Water_Dragon_Scale, Rnd.get(1, 2));
				if(Rnd.chance(36))
					dropItem(actor, Water_Dragon_Claw, Rnd.get(1, 3));

				actor.deleteMe();
			}
		}
	}

	private void spawnMob(int id)
	{
		L2NpcInstance actor = getActor();
		if(actor != null)
			try
			{
				Location pos = GeoEngine.findPointToStay(actor.getX(), actor.getY(), actor.getZ(), 100, 120);
				L2Spawn sp = new L2Spawn(NpcTable.getTemplate(id));
				sp.setLoc(pos);
				spawns.add(sp.doSpawn(true));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	private void dropItem(L2NpcInstance actor, int id, int count)
	{
		L2ItemInstance item = ItemTable.getInstance().createItem(id, 0, actor.getNpcId(), "ai.FafurionKindred.dropItem");
		item.setCount(count);
		item.dropToTheGround(null, actor);
	}
}
