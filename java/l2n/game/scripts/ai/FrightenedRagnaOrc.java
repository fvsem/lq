package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

/**
 * AI моба Frightened Ragna Orc для Den of Evil.<br>
 * - При его атаке пишет в чат сообщения, когда высвечивается сообщении со взяткой продолжаем бить или останавливаемся.
 * - Если останавливаемся, то он исчезает и ничего нам не даёт, если продолжаем то он умирает с сообщением в чат.
 */
public class FrightenedRagnaOrc extends Fighter
{
	private boolean canSay = true;
	private long _lastAttackTime = 0;
	private static final long DESPAWN = 6 * 1000; // 6 сек
	private static String[] says = new String[] {
			"Я... не хочу бороться ...",
			"Это действительно необходимо? ..." };
	private static String[] says2 = new String[] {
			"Ты довольно глупый мне поверить!",
			"Спасибо, но то о 10000000 аден была ложь! Увидимся!!" };

	public FrightenedRagnaOrc(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void thinkAttack()
	{
		L2NpcInstance actor = getActor();
		if(_lastAttackTime != 0 && _lastAttackTime + DESPAWN < System.currentTimeMillis())
		{
			if(actor.getCurrentHpPercents() < 30)
			{
				Functions.npcSay(actor, says2[Rnd.get(says2.length)]);
				actor.decayMe();
			}
			_lastAttackTime = 0;
		}
		super.thinkAttack();
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || attacker == null || attacker.getPlayer() == null)
			return;

		if(Rnd.chance(13))
			Functions.npcSay(actor, says[Rnd.get(says.length)]);

		if(actor.getCurrentHpPercents() < 30)
		{
			_lastAttackTime = System.currentTimeMillis();
			if(actor.getCurrentHpPercents() > 1)
			{
				if(canSay == true)
				{
					Functions.npcSay(actor, "Подождите ... Подождите! Стоп! Спаси меня, и я `дам вам 10000000 адены!!");
					canSay = false;
				}
			}
		}
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		Functions.npcSay(actor, "Тьфу ...Проклятие на вас ...!");
		canSay = true;
		_lastAttackTime = 0;

		super.onEvtDead(killer);
	}
}
