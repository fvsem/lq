package l2n.game.scripts.ai;

import l2n.game.ai.DefaultAI;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class RndWalkAndAnim extends DefaultAI
{
	protected static final int PET_WALK_RANGE = 100;

	public RndWalkAndAnim(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isMoving)
			return false;

		int val = Rnd.get(100);

		if(val < 20)
			randomWalk();
		else if(val < 40)
			actor.onRandomAnimation();

		return false;
	}

	@Override
	protected boolean randomWalk()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		int spawnX = actor.getSpawnedLoc().x;
		int spawnY = actor.getSpawnedLoc().y;
		int spawnZ = actor.getSpawnedLoc().z;

		int x = spawnX + Rnd.get(2 * PET_WALK_RANGE) - PET_WALK_RANGE;
		int y = spawnY + Rnd.get(2 * PET_WALK_RANGE) - PET_WALK_RANGE;
		int z = GeoEngine.getHeight(x, y, spawnZ);

		actor.setRunning();
		actor.moveToLocation(x, y, z, 0, true);

		return true;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{}
}
