package l2n.game.scripts.ai.hellbound;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.idfactory.IdFactory;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class MasterFestina extends Fighter
{
	private final static Location TullyFloor4LocationPoint = new Location(-14238, 273002, -10496);
	private static L2Zone _zone;

	private final static Location[] _mysticSpawnPoints = new Location[] { new Location(-11480, 273992, -11768), new Location(-11128, 273992, -11864),
			new Location(-10696, 273992, -11936), new Location(-12552, 274920, -11752),
			new Location(-12568, 275320, -11864), new Location(-12568, 275784, -11936),
			new Location(-13480, 273880, -11752), new Location(-13880, 273880, -11864),
			new Location(-14328, 273880, -11936), new Location(-12456, 272968, -11752),
			new Location(-12456, 272552, -11864), new Location(-12456, 272120, -11936) };
	private final static Location[] _spiritGuardSpawnPoints = new Location[] { new Location(-12552, 272168, -11936),
			new Location(-12552, 272520, -11872), new Location(-12552, 272984, -11744),
			new Location(-13432, 273960, -11736), new Location(-13864, 273960, -11856),
			new Location(-14296, 273976, -11936), new Location(-12504, 275736, -11936),
			new Location(-12472, 275288, -11856), new Location(-12472, 274888, -11744),
			new Location(-11544, 273912, -11752), new Location(-11160, 273912, -11856),
			new Location(-10728, 273896, -11936) };

	private final static int FOUNDRY_MYSTIC_ID = 22387;
	private final static int FOUNDRY_SPIRIT_GUARD_ID = 22389;
	private long _lastFactionNotifyTime = 0;

	public MasterFestina(L2Character actor)
	{
		super(actor);
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 704010, true);
	}

	@Override
	protected void onEvtSpawn()
	{
		// Спауним охрану
		L2MonsterInstance mob;
		for(Location loc : _mysticSpawnPoints)
		{
			mob = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(FOUNDRY_MYSTIC_ID));
			mob.setSpawnedLoc(loc);
			mob.onSpawn();
			mob.spawnMe(loc);
		}
		for(Location loc : _spiritGuardSpawnPoints)
		{
			mob = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(FOUNDRY_SPIRIT_GUARD_ID));
			mob.setSpawnedLoc(loc);
			mob.onSpawn();
			mob.spawnMe(loc);
		}
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(System.currentTimeMillis() - _lastFactionNotifyTime > actor.minFactionNotifyInterval)
		{
			for(L2NpcInstance npc : actor.getAroundNpc(3000, 500))
				if(npc.getNpcId() == FOUNDRY_MYSTIC_ID || npc.getNpcId() == FOUNDRY_SPIRIT_GUARD_ID)
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));

			_lastFactionNotifyTime = System.currentTimeMillis();
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_lastFactionNotifyTime = 0;
		super.onEvtDead(killer);
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		// Удаляем охрану
		for(L2NpcInstance npc : actor.getAroundNpc(3000, 500))
			if(npc.getNpcId() == FOUNDRY_MYSTIC_ID || npc.getNpcId() == FOUNDRY_SPIRIT_GUARD_ID)
				npc.deleteMe();

		L2GameThreadPools.getInstance().scheduleAi(new TeleportTask(), 10000, true);
	}

	public class TeleportTask implements Runnable
	{
		@Override
		public void run()
		{
			for(L2Player p : _zone.getInsidePlayersIncludeZ())
				if(p != null)
					p.teleToLocation(TullyFloor4LocationPoint);
		}
	}
}
