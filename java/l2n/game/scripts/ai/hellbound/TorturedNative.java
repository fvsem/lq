package l2n.game.scripts.ai.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

public class TorturedNative extends Fighter
{
	public TorturedNative(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(Rnd.chance(1))
			if(Rnd.chance(10))
				Functions.npcSay(actor, "Ик ... Я чувствую себя больным ...  ...!");
			else
				Functions.npcSay(actor, "Это ... будет ... убить ... каждый ...!");

		return super.thinkActive();
	}
}
