package l2n.game.scripts.ai.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

public class SteelCitadelKeymaster extends Fighter
{
	private boolean _firstTimeAttacked = true;
	private static final int AMASKARI_ID = 22449;

	public SteelCitadelKeymaster(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		if(_firstTimeAttacked)
		{
			_firstTimeAttacked = false;
			Functions.npcSay(actor, "Ты хорошо поработал в поисках меня, но я просто не могу передать вам ключ!");
			for(L2NpcInstance npc : L2World.getAroundNpc(actor))
				if(npc.getNpcId() == AMASKARI_ID && npc.getReflection().getId() == actor.getReflection().getId() && !npc.isDead())
				{
					npc.teleToLocation(GeoEngine.findPointToStay(actor.getX(), actor.getY(), actor.getZ(), 150, 180));
					break;
				}
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_firstTimeAttacked = true;
		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
