package l2n.game.scripts.ai.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.scripts.ai.raidboss.Tully;

public class SoulCrystalPillar extends Fighter
{
	public SoulCrystalPillar(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null)
		{
			Functions.spawn(actor.getLoc().rnd(30, 30, false), Tully.GHOST_TOLLES);
			actor.decayMe();
		}
		super.onEvtDead(killer);
	}
}
