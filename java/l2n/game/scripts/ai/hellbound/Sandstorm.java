package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.DefaultAI;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class Sandstorm extends DefaultAI
{
	private static final int AGGRO_RANGE = 200;
	private static final L2Skill SKILL1 = SkillTable.getInstance().getInfo(5435, 1);
	private static final L2Skill SKILL2 = SkillTable.getInstance().getInfo(5494, 1);
	private long lastThrow = 0;
	protected static final int WALK_RANGE = 300;

	public Sandstorm(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		if(lastThrow + 5000 < System.currentTimeMillis())
			for(L2Playable target : L2World.getAroundPlayables(actor, AGGRO_RANGE, AGGRO_RANGE))
				if(target != null && !target.isAlikeDead() && !target.isInvul() && target.isVisible() && GeoEngine.canSeeTarget(actor, target, false))
				{
					actor.doCast(SKILL1, target, true);
					actor.doCast(SKILL2, target, true);
					lastThrow = System.currentTimeMillis();
					break;
				}

		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{}

	@Override
	protected void onIntentionAttack(L2Character target)
	{}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character attacker, int aggro)
	{}


	@Override
	protected boolean randomWalk()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		Location sloc = actor.getSpawnedLoc();

		int x = sloc.x + Rnd.get(2 * WALK_RANGE) - WALK_RANGE;
		int y = sloc.y + Rnd.get(2 * WALK_RANGE) - WALK_RANGE;
		int z = GeoEngine.getHeight(x, y, sloc.z);

		actor.setRunning();
		actor.moveToLocation(x, y, z, 0, true);

		return true;
	}
}
