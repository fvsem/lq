package l2n.game.scripts.ai.hellbound;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.SkillTable;

import java.util.concurrent.ScheduledFuture;

public class BodyDestroyer extends Fighter
{
	private class KillTask implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
				return;

			L2Player player = L2ObjectsStorage.getPlayer(playeObjId);
			if(player == null)
				return;

			player.doDie(actor);
		}
	}

	private final static L2Skill deathSkill = SkillTable.getInstance().getInfo(5256, 1);

	private boolean effected = false;
	private ScheduledFuture<KillTask> killTask = null;
	private int playeObjId = 0;

	/**
	 * @param actor
	 */
	public BodyDestroyer(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAggression(L2Character attacker, int damage)
	{
		if(!effected)
		{
			L2NpcInstance actor = getActor();
			if(actor == null || attacker == null || attacker.getPlayer() == null)
				return;

			L2Player player = attacker.getPlayer();
			playeObjId = player.getObjectId();
			deathSkill.getEffects(actor, player, false, false);
			actor.broadcastPacket(new MagicSkillUse(actor, player, deathSkill.getId(), 1, deathSkill.getHitTime(), 0));
			if(killTask == null)
				killTask = L2GameThreadPools.getInstance().scheduleAi(new KillTask(), 30 * 1000, false);
			effected = true;
		}

		super.onEvtAggression(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		if(killTask != null)
			killTask.cancel(true);
		killTask = null;
		effected = false;

		L2Player player = L2ObjectsStorage.getPlayer(playeObjId);
		if(player != null)
			player.getEffectList().stopEffect(5256);

		playeObjId = 0;
		super.onEvtDead(killer);
	}
}
