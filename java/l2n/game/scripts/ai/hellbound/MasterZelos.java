package l2n.game.scripts.ai.hellbound;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.Fighter;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class MasterZelos extends Fighter
{
	private static Location TullyFloor2LocationPoint = new Location(-14180, 273060, -13600);
	private static L2Zone _zone;

	public MasterZelos(L2Character actor)
	{
		super(actor);
		_zone = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 704009, true);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		super.onEvtDead(killer);
		L2GameThreadPools.getInstance().scheduleAi(new TeleportTask(), 10000, true);
	}

	public class TeleportTask implements Runnable
	{
		@Override
		public void run()
		{
			for(L2Player p : _zone.getInsidePlayersIncludeZ())
				if(p != null)
					p.teleToLocation(TullyFloor2LocationPoint);
		}
	}
}
