package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

public class Challenger extends Fighter
{
	public Challenger(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null && killer.getPlayer() != null)
			TullyWorkshopManager.getInstance().killChallanger(actor, killer.getPlayer());
		super.onEvtDead(killer);
	}
}
