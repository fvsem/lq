package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.DoorTable;

public class Leodas extends Fighter
{
	private static final int[] doors = { 19250003, 19250004 };

	public Leodas(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		for(int i : doors)
			DoorTable.getInstance().getDoor(i).closeMe();

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		for(int i : doors)
		{
			DoorTable.getInstance().getDoor(i).openMe();
			DoorTable.getInstance().getDoor(i).onOpen();
		}
		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtSpawn()
	{
		for(int i : doors)
			DoorTable.getInstance().getDoor(i).closeMe();
		super.onEvtSpawn();
	}
}
