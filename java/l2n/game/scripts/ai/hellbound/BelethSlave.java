package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Mystic;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;


public class BelethSlave extends Mystic
{
	private static final L2Skill self_explosion = SkillTable.getInstance().getInfo(5528, 1);
	private static final L2Skill sd_skill = SkillTable.getInstance().getInfo(4032, 11);

	private boolean explosion = false;
	private long lastExplosion = 0;

	public BelethSlave(final L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		if(lastExplosion > 0 && lastExplosion < System.currentTimeMillis())
			explosion = false;

		if(actor.getCurrentHpPercents() <= 30 && !explosion)
		{
			addUseSkillDesire(attacker, self_explosion, Integer.MAX_VALUE);
			explosion = true;
			lastExplosion = System.currentTimeMillis() + 10000;
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtSeeSpell(final L2Skill skill, final L2Character caster)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && skill.getId() == self_explosion.getId() && !explosion)
		{
			addUseSkillDesire(caster, self_explosion, Integer.MAX_VALUE);
			explosion = true;
		}
		super.onEvtSeeSpell(skill, caster);
	}

	@Override
	protected void onEvtFinishCasting(final L2Skill skill)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null)
			if(skill != null && skill.getId() == self_explosion.getId() && explosion)
			{
				for(final L2NpcInstance slave : actor.getAroundNpc(300, 150))
					if(slave.getNpcId() == 18490)
						slave.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, self_explosion, actor.getCastingTarget());
			}
			else if(Rnd.chance(20))
			{
				final L2Character target = actor.getMostHated();
				if(target != null)
					addUseSkillDesire(target, sd_skill, Integer.MAX_VALUE);
			}
		super.onEvtFinishCasting(skill);
	}

}
