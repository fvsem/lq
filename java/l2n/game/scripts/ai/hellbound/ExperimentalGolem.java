package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.actor.L2Character;

public class ExperimentalGolem extends Fighter
{
	public ExperimentalGolem(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		// Увеличиваем время до сигнализации
		TullyWorkshopManager.getInstance().increaseAlarmTime();
		super.onEvtDead(killer);
	}
}
