package l2n.game.scripts.ai.hellbound;

import l2n.commons.list.GArray;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Priest;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class FloatingGhost extends Priest
{
	private static final L2Skill cursed_magic = SkillTable.getInstance().getInfo(5593, 1);

	public FloatingGhost(L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 2000;
	}

	@Override
	protected void onEvtThink()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isActionsDisabled() || actor.isAfraid() || actor.isDead())
			return;

		L2Character target;
		if((target = prepareTarget()) != null && getIntention() != CtrlIntention.AI_INTENTION_ATTACK)
		{
			addTaskCast(target, cursed_magic);
			notifyEvent(CtrlEvent.EVT_AGGRESSION, target, Rnd.get(1, 100));
		}
		else
			super.onEvtThink();
	}

	@Override
	protected boolean createNewTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		// Удаляем все задания
		clearTasks();

		L2Character target;
		if((target = prepareTarget()) == null)
			return false;

		// Добавить новое задание
		addTaskCast(target, cursed_magic);
		return true;
	}

	@Override
	protected L2Character prepareTarget()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return null;

		GArray<L2Player> list = actor.getAroundPlayers(500);
		if(list.isEmpty())
			return actor.getRandomHated();
		return list.get(Rnd.get(list.size()));
	}
}
