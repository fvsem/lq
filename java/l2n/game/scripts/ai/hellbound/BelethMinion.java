package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.instancemanager.boss.BelethManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class BelethMinion extends Mystic
{
	private static final L2Skill self_blasting = SkillTable.getInstance().getInfo(5498, 1);
	private static final L2Skill kamigaze = SkillTable.getInstance().getInfo(5376, 5);

	private long lastAttack = 0;

	public BelethMinion(final L2Character actor)
	{
		super(actor);
		setMaxPursueRange(-1);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		// иногда кастует скил который агрит игроков
		if(Rnd.chance(2) && lastAttack < System.currentTimeMillis())
		{
			final L2NpcInstance actor = getActor();
			if(actor != null && !actor.isDead())
			{
				final L2Character target = actor.getMostHated();
				if(target != null)
				{
					lastAttack = System.currentTimeMillis() + 2000;
					addUseSkillDesire(target, self_blasting, Integer.MAX_VALUE);
				}
			}
		}
		BelethManager.getInstance().setLastAttackTime();
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	public void onScriptEvent(final int eventId, final Object... args)
	{
		// вызывается Beleth'ом когда у него HP от 2% до 50%
		if(eventId == 1109239)
		{
			final L2NpcInstance actor = getActor();
			if(actor != null && !actor.isDead())
			{
				final L2Character target = actor.getMostHated();
				if(target != null && Rnd.chance(30))
					addUseSkillDesire(target, kamigaze, Integer.MAX_VALUE);
			}
		}
	}

	@Override
	public final boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}
