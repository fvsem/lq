package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.instancemanager.boss.EpidosManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class NaiaSpore extends Mystic
{
	private static final Location[] MOVE_POINTS = new Location[]
	{
			new Location(-46080, 246368, -14183),
			new Location(-44816, 246368, -14183),
			new Location(-44224, 247440, -14184),
			new Location(-44896, 248464, -14183),
			new Location(-46064, 248544, -14183),
			new Location(-46720, 247424, -14183)
	};

	public static final Location[] MERGE_POINTS = new Location[]
	{
			new Location(-45488, 246768, -14183),
			new Location(-44767, 247419, -14183),
			new Location(-46207, 247417, -14183),
			new Location(-45462, 248174, -14183)
	};

	public NaiaSpore(L2Character actor)
	{
		super(actor);
		AI_TASK_ACTIVE_DELAY = 1000;
		setMaxPursueRange(-1);
	}

	@Override
	protected void onEvtSpawn()
	{
		addTaskMove(getRndLocation().rnd(100, 100, false), Integer.MAX_VALUE);
		super.onEvtSpawn();
	}

	@Override
	public void onScriptEvent(final int eventId, final Object... args)
	{
		if(eventId == 78010012)
		{
			clearTasks();
			addTaskMove(MERGE_POINTS[Rnd.get(MERGE_POINTS.length)], Integer.MAX_VALUE);
		}
		super.onScriptEvent(eventId, args);
	}

	private Location getRndLocation()
	{
		return MOVE_POINTS[Rnd.get(MOVE_POINTS.length)];
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null)
		{
			// Считаем количество убитых спор
			EpidosManager.getInstance().calculateCounter(actor, killer.getPlayer());
			// Спауним другие споры после смерти этой
			EpidosManager.getInstance().spawnSpore(actor.getNpcId());
		}

		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtArrived()
	{
		onEvtThink();
	}

	@Override
	public final boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	public void teleportHome(final boolean clearAggro)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(clearAggro)
			actor.clearAggroList(true);

		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);

		// Удаляем все задания
		clearTasks();
	}
}
