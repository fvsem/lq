package l2n.game.scripts.ai.hellbound;

import l2n.game.ai.Fighter;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.instancemanager.TownManager;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Rnd;

public class Chimeras extends Fighter
{
	private static final int CELTUS = 22353;
	private static final int MAGIC_BOTTLE_SKILL = 2359;

	private static boolean HELLBOUND_OPEN = false;

	public Chimeras(L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		if(skill.getId() != MAGIC_BOTTLE_SKILL)
			return;

		L2NpcInstance actor = getActor();
		if(actor == null || actor.getCurrentHpPercents() > 10 || actor.isDead()) // 10% ХП для использования бутылки
		{
			caster.sendPacket(new SystemMessage(SystemMessage.TARGET_IS_INCORRECT));
			return;
		}

		actor.dropItem(caster.getPlayer(), actor.getNpcId() == CELTUS ? 9682 : Rnd.chance(20) ? 9681 : 9680, 1);
		actor.decayMe();
		actor.doDie(actor);
	}

	@Override
	protected void onEvtAggression(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || attacker == null || attacker.getPlayer() == null)
			return;

		// чтоб не проверять постоянно когда доступ к ХБ откроется
		if(!HELLBOUND_OPEN)
			HELLBOUND_OPEN = ServerVariables.getBool("HellboundOpen", false);
		// при попытке удара Химер телепортирует в Heine при закрытом Hellbound
		if(!HELLBOUND_OPEN && !attacker.getPlayer().isGM())
		{
			attacker.teleToLocation(TownManager.getInstance().getTown(13).getSpawn());
			return;
		}

		super.onEvtAggression(attacker, damage);
	}

	@Override
	protected boolean randomWalk()
	{
		return true;
	}
}
