package l2n.game.scripts.ai.hellbound;

import javolution.util.FastMap;
import l2n.Config;
import l2n.game.ai.Fighter;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Object;
import l2n.game.model.L2Party;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.logging.Logger;

public class InfinitumFighter extends Fighter
{
	private static FastMap<Integer, L2Zone> _floorZones = null;
	private int _currentFloor;
	private static final int ZONE_OFFSET = 705000;

	protected static final Logger _log = Logger.getLogger(InfinitumFighter.class.getName());

	public InfinitumFighter(final L2Character actor)
	{
		super(actor);
		initZone();
	}

	private static void initZone()
	{
		if(_floorZones == null)
		{
			_floorZones = new FastMap<Integer, L2Zone>(10);
			for(int i = 1; i < 11; i++)
				_floorZones.put(i, ZoneManager.getInstance().getZoneById(ZoneType.dummy, ZONE_OFFSET + i, true));
		}
	}

	private static int getCurrentFloor(final L2Object o)
	{
		if(_floorZones == null)
			return 0;
		for(final L2Zone floor : _floorZones.values())
			if(floor.checkIfInZone(o))
				return floor.getId() - ZONE_OFFSET;
		return 0;
	}

	private static final int getRateUp(final int currentFloor)
	{
		if(currentFloor > 5)
			return Config.INFINITUM_RATE_DOWN;
		return Config.INFINITUM_RATE_UP;
	}

	private static final int getRateDown(final int currentFloor)
	{
		if(currentFloor > 5)
			return Config.INFINITUM_RATE_UP;
		return Config.INFINITUM_RATE_DOWN;
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || attacker == null)
			return;

		if(_currentFloor == 0)
			_currentFloor = actor.getSpawn().getLocation() - 700050;

		// С 5 и 10 этажей никуда не портает, и вообще там этих мобов быть не должно.
		if(_currentFloor == 5 || _currentFloor == 10)
		{
			super.onEvtAttacked(attacker, damage);
			return;
		}

		Location teleToPoint = null;
		L2Zone floor = null;
		final int chance = Rnd.get(10000);

		// отображаем шанс для ГМов
		if(attacker.isPlayer() && attacker.getPlayer().isGM())
			attacker.getPlayer().sendMessage("onAttack: chance:" + chance + ", up:" + getRateUp(_currentFloor) + ", down:" + getRateDown(_currentFloor));

		// на 1 и 6 этаже телепортируем только наверх
		if(_currentFloor == 1 || _currentFloor == 6)
		{
			if(getRateUp(_currentFloor) >= chance)
				floor = _floorZones.get(_currentFloor + 1);
		}
		else
		{
			// Портает вверх
			if(getRateUp(_currentFloor) >= chance)
				floor = _floorZones.get(_currentFloor + 1);
			// Портает вниз
			else if(getRateDown(_currentFloor) >= chance)
				floor = _floorZones.get(_currentFloor - 1);
		}

		if(floor != null)
		{
			// TODO: на 1 этаже партия должна проводить в среднем от 20 мин до 1 часа.
			teleToPoint = floor.getSpawn();
			final L2Party party = attacker.getPlayer().getParty();

			if(teleToPoint != null)
				if(party != null)
				{
					for(final L2Player member : party.getPartyMembers())
						if(member != null && _currentFloor == getCurrentFloor(member))
							member.teleToLocation(teleToPoint);
				}
				else
					attacker.teleToLocation(teleToPoint);
		}

		super.onEvtAttacked(attacker, damage);
	}
}
