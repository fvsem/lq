package l2n.game.scripts.ai.hellbound;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.ai.model.AdditionalAIParams.AiOptionsType;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

public class TullyServant extends Fighter
{
	private final int level;

	public TullyServant(L2Character actor)
	{
		super(actor);
		level = actor.getNpcId() - 22404;
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null && killer != null)
			if(level == 3 || level == 6)
				Functions.npcSayInRange(actor, "Мне не Удалось ... Пожалуйста, простите меня, Дарион ...", 300);
			else
				Functions.npcSayInRange(actor, killer.getName() + ", Я вернусь ... и буду сильней ...", 300);

		TullyWorkshopManager.getInstance().trySpawnMysteriousAgent(getInt(AiOptionsType.OPTIONS, 0));
		super.onEvtDead(killer);
	}
}
