package l2n.game.scripts.ai.soi;

import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.NpcSay;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncAdd;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;

public class AbstractSOIMonster extends Fighter
{
	private static final String death_message = "I am already dead. You cannot kill me again...";

	private static final L2Skill[] undying_mummifing_aura = new L2Skill[] {
			SkillTable.getInstance().getInfo(5915, 1),
			SkillTable.getInstance().getInfo(5915, 2),
			SkillTable.getInstance().getInfo(5915, 3),
			SkillTable.getInstance().getInfo(5915, 4),
			SkillTable.getInstance().getInfo(5915, 5),
			SkillTable.getInstance().getInfo(5915, 6),
			SkillTable.getInstance().getInfo(5915, 7),
			SkillTable.getInstance().getInfo(5915, 8),
			SkillTable.getInstance().getInfo(5915, 9) };

	private static final L2Skill[] undying_grudging_aura = new L2Skill[] {
			SkillTable.getInstance().getInfo(5916, 1),
			SkillTable.getInstance().getInfo(5916, 2),
			SkillTable.getInstance().getInfo(5916, 3),
			SkillTable.getInstance().getInfo(5916, 4),
			SkillTable.getInstance().getInfo(5916, 5),
			SkillTable.getInstance().getInfo(5916, 6),
			SkillTable.getInstance().getInfo(5916, 7),
			SkillTable.getInstance().getInfo(5916, 8),
			SkillTable.getInstance().getInfo(5916, 9) };

	private static final L2Skill[] undying_shrouding_aura = new L2Skill[] {
			SkillTable.getInstance().getInfo(5917, 1),
			SkillTable.getInstance().getInfo(5917, 2),
			SkillTable.getInstance().getInfo(5917, 3),
			SkillTable.getInstance().getInfo(5917, 4),
			SkillTable.getInstance().getInfo(5917, 5),
			SkillTable.getInstance().getInfo(5917, 6),
			SkillTable.getInstance().getInfo(5917, 7),
			SkillTable.getInstance().getInfo(5917, 8),
			SkillTable.getInstance().getInfo(5917, 9) };

	private static final L2Skill[][] buff_skills = new L2Skill[][] { undying_mummifing_aura, undying_grudging_aura, undying_shrouding_aura, };

	private boolean send_message = false;
	private int skill_level = 0;

	public AbstractSOIMonster(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(attacker == null || actor == null)
			return;

		L2Skill skill = null;
		int rnd = Rnd.get(3);
		if(skill_level <= 0 && actor.getCurrentHpPercents() <= 90)
		{
			skill_level = 1;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 1 && actor.getCurrentHpPercents() <= 80)
		{
			skill_level = 2;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 2 && actor.getCurrentHpPercents() <= 70)
		{
			skill_level = 3;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 3 && actor.getCurrentHpPercents() <= 60)
		{
			skill_level = 4;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 4 && actor.getCurrentHpPercents() <= 50)
		{
			skill_level = 5;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 5 && actor.getCurrentHpPercents() <= 40)
		{
			skill_level = 6;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 6 && actor.getCurrentHpPercents() <= 30)
		{
			skill_level = 7;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 7 && actor.getCurrentHpPercents() <= 20)
		{
			skill_level = 8;
			skill = buff_skills[rnd][skill_level - 1];
		}
		else if(skill_level <= 8 && actor.getCurrentHpPercents() <= 10)
		{
			skill_level = 9;
			skill = buff_skills[rnd][skill_level - 1];
		}

		if(skill != null)
			addUseSkillDesire(actor, skill, 1000000);

		if(actor.getCurrentHpPercents() <= 10 && Rnd.chance(20) && !send_message)
		{
			send_message = true;
			NpcSay cs = new NpcSay(actor, 0, death_message);
			Iterable<L2Player> players = L2World.getAroundPlayers(actor, 500, 200);
			for(L2Player player : players)
				if(player != null)
					player.sendPacket(cs);

			// увеличиваем регенирацию хп
			actor.addStatFunc(new FuncAdd(Stats.REGENERATE_HP_RATE, 0x40, this, 10));
		}

		super.onEvtAttacked(attacker, damage);
	}
}
