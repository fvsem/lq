package l2n.game.scripts.ai.soi;

import l2n.game.ai.Fighter;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;

/**
 * AI для РБ в _698_BlocktheLordsEscape
 * идет по заданым координатам
 * на атаки не реагирует
 * 
 */
public class SoulDevourer extends Fighter
{
	private static final Location[][] move_points = new Location[][] {
			{
					new Location(-179551, 207088, -15488),
					new Location(-179550, 207375, -15496),
					new Location(-180162, 207441, -15496),
					new Location(-180717, 207687, -15496),
					new Location(-181138, 208053, -15496),
					new Location(-181565, 208512, -15496),
					new Location(-181726, 209028, -15496),
					new Location(-181794, 209631, -15496),
					new Location(-181714, 210148, -15496),
					new Location(-181520, 210667, -15496),
					new Location(-181140, 211155, -15496),
					new Location(-180684, 211526, -15496),
					new Location(-180233, 211774, -15496),
					new Location(-179550, 211888, -15496),
					new Location(-179547, 211251, -15472) },

			{
					new Location(-179550, 207088, -15488),
					new Location(-179557, 207387, -15496),
					new Location(-178982, 207412, -15496),
					new Location(-178433, 207653, -15496),
					new Location(-177962, 208020, -15496),
					new Location(-177596, 208520, -15496),
					new Location(-177366, 209054, -15496),
					new Location(-177315, 209603, -15496),
					new Location(-177373, 210173, -15496),
					new Location(-177614, 210708, -15496),
					new Location(-177968, 211173, -15496),
					new Location(-178424, 211562, -15496),
					new Location(-178898, 211747, -15496),
					new Location(-179549, 211884, -15496),
					new Location(-179550, 211248, -15472) } };

	private int current_point = -1;
	private boolean arrived = true;
	private int move_point_type = -1;
	private long center_tumor_storedId = 0;

	public SoulDevourer(L2Character actor, L2NpcInstance center_tumor, int type)
	{
		super(actor);
		move_point_type = type;
		center_tumor_storedId = center_tumor.getStoredId();
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(arrived)
		{
			arrived = false;
			current_point++;

			if(current_point >= move_points[move_point_type].length)
			{
				stopAITask();
				L2NpcInstance tumor;
				if((tumor = L2ObjectsStorage.getAsNpc(center_tumor_storedId)) != null)
				{
					// Обработка смерти в квесте
					actor.doDie(tumor);
				}

				actor.deleteMe();
				return false;
			}

			addTaskMove(move_points[move_point_type][current_point]);
			doTask();
			return true;
		}

		return false;
	}

	@Override
	protected void onEvtArrived()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		arrived = true;
		thinkActive();
	}

	@Override
	public void checkAggression(L2Character target)
	{}

	@Override
	protected void onIntentionAttack(L2Character target)
	{}

	@Override
	protected void onEvtClanAttacked(L2Character attacked_member, L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}

	@Override
	protected void onEvtAggression(L2Character attacker, int aggro)
	{}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
