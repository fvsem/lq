package l2n.game.scripts.ai.soi;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

public class NpcAttacker extends AbstractSOIMonster
{
	private static final L2Skill[] attack_skills = new L2Skill[] { SkillTable.getInstance().getInfo(5909, 1), SkillTable.getInstance().getInfo(5910, 1) };

	private int[] targets;

	public NpcAttacker(L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	public NpcAttacker(L2Character actor, int... npcIds)
	{
		super(actor);
		AI_TASK_DELAY = 1000;
		AI_TASK_ACTIVE_DELAY = 1000;
		targets = npcIds;
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		if(getActor().isConfused())
			getActor().stopConfused();

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		if(actor.getTarget() == null || !actor.getTarget().isMonster())
			for(L2NpcInstance npc : actor.getAroundNpc(1000, 200))
				if(ArrayUtil.arrayContains(targets, npc.getNpcId()))
				{
					actor.startConfused();
					actor.setTarget(npc);
					actor.addDamageHate(npc, 0, 500);
					setIntention(CtrlIntention.AI_INTENTION_ATTACK, npc, null);
					break;
				}

		return super.thinkActive();
	}

	@Override
	protected boolean defaultFightTask()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return false;

		L2Character target = getAttackTarget();
		if(target != null && ArrayUtil.arrayContains(targets, target.getNpcId()))
		{
			// Добавить новое задание
			L2Skill skill = attack_skills[Rnd.get(2)];
			addTaskCast(target, skill);
			target.getAI().notifyEvent(CtrlEvent.EVT_SEE_SPELL, skill, actor);
			return true;
		}

		return super.defaultFightTask();
	}

}
