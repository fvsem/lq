package l2n.game.scripts.ai.soi;

import l2n.game.ai.DefaultAI;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.ItemTable;
import l2n.util.Rnd;

/**
 * AI нпс Unstable Seed для Seed of Infinity<br>
 * <li>при смерти отображает анимацию</li><br>
 * <li>считает количество атак на себя определённым скилом, при превышении определённого количества умирает ;-(</li> <br>
 * <li>периодически дропает Tear of a Freed Soul с шансом 30%</li>
 * 
 */
public class UnstableSeed extends DefaultAI
{
	/** показывает при смерти */
	private static final int Skill_dying_display = 6037;

	/** считает сколько раз на него кастуют этоти скилы */
	private static final int NPC_Attack_Skill_C = 5909;
	private static final int NPC_Attack_Skill_L = 5910;

	private static final int reward_siege = 13797;
	private static final int reward_rate = 30;

	private int attackCount = 4000;

	public UnstableSeed(L2Character actor)
	{
		super(actor);
		AI_TASK_DELAY = 60000;
		AI_TASK_ACTIVE_DELAY = 60000;
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtThink()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		// дропает Tear of a Freed Soul
		if(Rnd.chance(reward_rate))
		{
			L2ItemInstance reward = ItemTable.getInstance().createItem(reward_siege, 0, actor.getNpcId(), "DefendtheHallofErosion");
			reward.dropToTheGround(actor, actor.getLoc().rnd(100, 150, false));
		}

		super.onEvtThink();
	}

	/** The character finish casting **/
	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || skill == null || caster == null || caster.isPlayable() || actor.isDead())
			return;

		if(skill.getId() == NPC_Attack_Skill_C || skill.getId() == NPC_Attack_Skill_L)
		{
			attackCount--;
			if(attackCount <= 0)
			{
				// при смерти отображает анимацию
				actor.broadcastPacket(new MagicSkillUse(actor, actor, Skill_dying_display, 1, 500, 0));
				actor.doDie(caster);
				actor.deleteMe();
			}
		}

		super.onEvtSeeSpell(skill, caster);
	}
}
