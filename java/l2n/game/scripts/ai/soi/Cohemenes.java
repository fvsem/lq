package l2n.game.scripts.ai.soi;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

public class Cohemenes extends Fighter
{
	private static final String MSG1 = "Clinging on won't help you! Ultimate forgotten magic, Blade Turn!";
	private static final String MSG2 = "Even special sauce can't help you! Ultimate forgotten magic, Force Shield!";

	private static final String RESPAWN_HELPER = "Kahahaha! That guy's nothing! He can't even kill without my permission! See here! Ultimate forgotten magic! Deathless Guardian!";
	private static final String SPAWN_HELPER = "Impressive.... Hahaha it's so much fun, but I need to chill a little while.  Argekunte, clear the way!";

	private static final L2Skill SKILL_SBOSSDEF = SkillTable.getInstance().getInfo(5930, 1);
	private static final L2Skill SKILL_SBOSSDEF2 = SkillTable.getInstance().getInfo(5929, 1);

	private static final int TIME_DEFENSE_CHANGE = 60; // каждую минуту меняет защиту

	private int _magicAttack = 0;
	private int _physAttack = 0;
	private int lastTimeChangeDefens = 0;

	// Argekunte
	private final static int _brotherId = 25635;
	private L2NpcInstance _brother;
	private long _wait_timeout = 0;

	public Cohemenes(L2Character actor)
	{
		super(actor);
	}

	private void spawnBrother()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(_brother == null)
		{
			L2NpcTemplate npc = NpcTable.getTemplate(_brotherId);
			L2NpcInstance raidboss = npc.getNewInstance();
			raidboss.setSpawnedLoc(actor.getLoc());
			raidboss.setReflection(actor.getReflectionId());
			raidboss.onSpawn();
			raidboss.setCurrentHpMp(raidboss.getMaxHp(), raidboss.getMaxMp(), true);
			raidboss.spawnMe(actor.getLoc().rnd(60, 60, false));

			_brother = raidboss;
			_brother.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, actor.getRandomHated(), Rnd.get(1, 100));
			Functions.npcShout(actor, 10000, SPAWN_HELPER);
		}
	}

	private boolean searchBrother()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return false;

		if(_brother == null)
			// Ищем брата не чаще, чем раз в 15 секунд, если по каким-то причинам его нету
			if(System.currentTimeMillis() > _wait_timeout)
			{
				_wait_timeout = System.currentTimeMillis() + 15000;
				for(L2NpcInstance npc : L2World.getAroundNpc(actor))
					if(npc.getNpcId() == _brotherId)
					{
						_brother = npc;
						return true;
					}
			}
		return false;
	}

	@Override
	protected boolean thinkActive()
	{
		if(_brother == null)
			searchBrother();

		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(_brother == null)
			spawnBrother();
		else if(_brother.isDead())
		{
			_brother.deleteMe();
			_brother = null;

			L2NpcTemplate npc = NpcTable.getTemplate(_brotherId);
			L2NpcInstance raidboss = npc.getNewInstance();
			raidboss.setSpawnedLoc(actor.getLoc());
			raidboss.setReflection(actor.getReflectionId());
			raidboss.onSpawn();
			raidboss.setCurrentHpMp(raidboss.getMaxHp(), raidboss.getMaxMp(), true);
			raidboss.spawnMe(actor.getLoc().rnd(60, 60, false));

			raidboss.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, actor.getRandomHated(), Rnd.get(1, 100));

			_brother = raidboss;

			Functions.npcShout(actor, 10000, RESPAWN_HELPER);
		}

		super.thinkAttack();
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(attacker != null)
		{
			if(actor.getDistance(attacker) > 200 || attacker.getCastingSkill() != null && attacker.getCastingSkill().isMagic())
			{
				_magicAttack++;
				_physAttack--;
			}
			else
			{
				_magicAttack--;
				_physAttack++;
			}

			if(lastTimeChangeDefens < System.currentTimeMillis() / 1000)
			{
				if(_physAttack > 100 && _physAttack > _magicAttack * 4)
				{
					Functions.npcShout(actor, 10000, MSG1);
					actor.getEffectList().stopEffect(SKILL_SBOSSDEF);
					addUseSkillDesire(actor, SKILL_SBOSSDEF2, 1000000);
				}
				else if(_magicAttack > 50 && _magicAttack > _physAttack * 2)
				{
					Functions.npcShout(actor, 10000, MSG2);
					actor.getEffectList().stopEffect(SKILL_SBOSSDEF2);
					addUseSkillDesire(actor, SKILL_SBOSSDEF, 1000000);
				}

				_magicAttack = 0;
				_physAttack = 0;

				lastTimeChangeDefens = (int) (System.currentTimeMillis() / 1000 + TIME_DEFENSE_CHANGE);
			}
		}

		if(_brother == null)
			searchBrother();

		super.onEvtAttacked(attacker, damage);
	}
}
