package l2n.game.scripts.ai.soi;

import l2n.game.ai.Fighter;
import l2n.game.ai.tasks.Task;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2NpcInstance;

public class FeralHound extends Fighter
{
	private boolean ingore = false;
	private static final int stop_Attack = 5909;
	private static final int start_Attack = 5910;

	public FeralHound(L2Character actor)
	{
		super(actor);
		actor.setInvul(true);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean createNewTask()
	{
		return ingore ? false : super.createNewTask();
	}

	@Override
	protected boolean doTask()
	{
		if(ingore)
		{
			L2NpcInstance actor = getActor();
			if(actor == null)
				return false;

			if(_task_list.size() == 0)
			{
				clearTasks();
				return true;
			}

			Task currentTask = null;
			try
			{
				currentTask = _task_list.first();
			}
			catch(Exception e)
			{}

			if(currentTask == null)
				clearTasks();

			if(!_def_think)
				return true;

			assert currentTask != null;

			switch (currentTask.type)
			{
			// Задание "прибежать в заданные координаты"
				case MOVE:
				{
					if(actor.isMoving)
						return false;

					if(!actor.moveToLocation(currentTask.loc, 0, true))
					{
						clientStopMoving();
						_pathfind_fails = 0;
						actor.teleToLocation(currentTask.loc);
						return maybeNextTask(currentTask);
					}
				}
					break;
			}
			return false;
		}
		return super.doTask();

	}

	@Override
	public void checkAggression(L2Character target)
	{
		if(!ingore)
			super.checkAggression(target);
	}

	@Override
	protected void onIntentionAttack(L2Character target)
	{
		if(!ingore)
			super.onIntentionAttack(target);
	}

	@Override
	protected void onEvtClanAttacked(L2Character attacked_member, L2Character attacker, int damage)
	{
		if(!ingore)
			super.onEvtClanAttacked(attacked_member, attacker, damage);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		if(!ingore)
			super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtAggression(L2Character attacker, int aggro)
	{
		if(!ingore)
			super.onEvtAggression(attacker, aggro);
	}

	/** The character finish casting **/
	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || skill == null || actor.isDead())
			return;

		if(skill.getId() == stop_Attack)
		{
			clearTasks();
			addTaskMove(actor.getSpawnedLoc());
			ingore = true;
		}
		else if(skill.getId() == start_Attack)
		{
			for(L2Playable obj : L2World.getAroundPlayables(actor))
				if(obj != null && !obj.isAlikeDead() && !obj.isInvul() && obj.isVisible())
					checkAggression(obj);
			ingore = false;
		}

		super.onEvtSeeSpell(skill, caster);
	}
}
