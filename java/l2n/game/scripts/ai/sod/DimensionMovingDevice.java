package l2n.game.scripts.ai.sod;

import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.DefaultAI;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.concurrent.ScheduledFuture;

public class DimensionMovingDevice extends DefaultAI
{
	private static final int TIAT_NPC_ID = 29163;
	private static final int INIT_DELAY = 5 * 1000; // 5 сек
	private static final int MOBS_DELAY = 3 * 1000; // 3 сек
	private static final int MOBS_WAVE_DELAY = 90 * 1000; // 1.5 мин между волнами мобов

	private static final int[] MOBS = {
			22538, // Dragon Steed Troop Commander
			22540, // White Dragon Leader
			22547, // Dragon Steed Troop Healer
			22542, // Dragon Steed Troop Magic Leader
			22548 // Dragon Steed Troop Javelin Thrower
	};

	private static final Location TIATROOM_LOC = new Location(-250408, 208568, -11968);
	private ScheduledFuture<?> _mobssSpawnTask;
	private int _tiat = 0;
	private long _search_timeout = 0;
	private static long _spawnTime = 0; // static для всех ловушек

	private static L2Skill decrease_pdef = SkillTable.getInstance().getInfo(5699, 7);
	private static L2Skill decrease_mdef = SkillTable.getInstance().getInfo(5700, 7);

	public DimensionMovingDevice(L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtSpawn()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		_spawnTime = System.currentTimeMillis();

		if(_mobssSpawnTask == null)
			_mobssSpawnTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new MobssSpawnTask(), INIT_DELAY, MOBS_WAVE_DELAY, false);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		if(_mobssSpawnTask != null)
		{
			_mobssSpawnTask.cancel(true);
			_mobssSpawnTask = null;
		}

		_spawnTime = 0;

		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(checkAllDestroyed(actor.getNpcId(), actor.getReflection().getId()))
		{
			L2NpcInstance tiat = findTiat(actor.getReflection().getId());
			if(tiat != null && !tiat.isDead())
			{
				// Вы пожалете что вызвали меня!!!
				tiat.broadcastPacket(new ExShowScreenMessage("Вы пожалете что вызвали меня!!!!", 3000, ScreenMessageAlign.MIDDLE_CENTER, false));
				// накладываем на Тиата дебаф
				decrease_pdef.getEffects(tiat, tiat, false, false); // Decrease P. Def
				decrease_mdef.getEffects(tiat, tiat, false, false); // Magic Resistance Decrease
			}
		}

		super.onEvtDead(killer);
	}

	/**
	 * Возвращает L2NpcInstance боса Tiat в отражении actor-а.
	 * Если не найден, возвращает null.
	 */
	private L2NpcInstance findTiat(long refId)
	{
		if(_tiat != 0)
		{
			L2NpcInstance tiatNPC = L2ObjectsStorage.getNpc(_tiat);
			if(tiatNPC != null)
				return tiatNPC;
		}

		// Ищем Тиата не чаще, чем раз в 10 секунд, если по каким-то причинам его нету
		if(System.currentTimeMillis() > _search_timeout)
		{
			_search_timeout = System.currentTimeMillis() + 10000;
			for(L2NpcInstance npc : L2ObjectsStorage.getAllByNpcId(TIAT_NPC_ID, true))
				if(npc.getReflection().getId() == refId)
				{
					_tiat = npc.getObjectId();
					return npc;
				}
		}
		return null;
	}

	/**
	 * Проверяет, уничтожены ли все Dimension Moving Device в текущем измерении
	 * 
	 * @return true если все уничтожены
	 */
	private static boolean checkAllDestroyed(int mobId, long refId)
	{
		for(L2NpcInstance npc : L2ObjectsStorage.getAllByNpcId(mobId, true))
			if(npc.getReflection().getId() == refId)
				return false;
		return true;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	/**
	 * Таск запускает волну спауна мобов
	 */
	public class MobssSpawnTask implements Runnable
	{
		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
			{
				if(_mobssSpawnTask != null)
				{
					_mobssSpawnTask.cancel(true);
					_mobssSpawnTask = null;
				}
				return;
			}

			// Не кричим при первой волне мобов
			L2NpcInstance tiat = findTiat(actor.getReflection().getId());
			if(tiat != null && !tiat.isDead() && _spawnTime + MOBS_WAVE_DELAY < System.currentTimeMillis())
				tiat.broadcastPacket(new ExShowScreenMessage("Whoaaaaaa!!!!", 3000, ScreenMessageAlign.MIDDLE_CENTER, false));

			long delay = 0;
			for(int mobId : MOBS)
			{
				L2GameThreadPools.getInstance().scheduleAi(new SpawnerTask(mobId), delay, false);
				delay += MOBS_DELAY;
			}
		}
	}

	/**
	 * Таск спаунит мобов в волне
	 */
	public class SpawnerTask implements Runnable
	{
		private int _mobId;

		public SpawnerTask(int mobId)
		{
			_mobId = mobId;
		}

		@Override
		public void run()
		{
			L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
				return;

			Reflection r = actor.getReflection();
			L2MonsterInstance mob = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(_mobId));
			mob.setSpawnedLoc(actor.getLoc());
			mob.setReflection(r);
			mob.onSpawn();
			mob.spawnMe(mob.getSpawnedLoc());
			r.addSpawn(mob.getSpawn());

			mob.setRunning();
			((DefaultAI) mob.getAI()).setMaxPursueRange(-1);
			mob.getAI().setGlobalAggro(0);

			// После спауна мобы бегут к тиату или в фортресс, если тиата еще нету
			L2NpcInstance tiat = findTiat(r.getId());
			Location homeLoc;
			if(tiat != null && !tiat.isDead())
			{
				homeLoc = Rnd.coordsRandomize(tiat.getLoc(), 200, 500);
				mob.setSpawnedLoc(homeLoc);
				mob.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, tiat.getRandomHated(), 1);
				mob.getAI().addTaskMove(homeLoc);
			}
			else
			{
				homeLoc = Rnd.coordsRandomize(TIATROOM_LOC, 200, 500);
				mob.setSpawnedLoc(homeLoc);
				mob.getAI().addTaskMove(homeLoc);
			}
		}
	}
}
