package l2n.game.scripts.ai.sod;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Skill;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;

import java.util.ArrayList;
import java.util.concurrent.ScheduledFuture;

public class Tiat extends Fighter
{
	private static final int TIAT_TRANSFORMATION_SKILL_ID = 5974;
	private static final L2Skill TIAT_TRANSFORMATION_SKILL = SkillTable.getInstance().getInfo(TIAT_TRANSFORMATION_SKILL_ID, 1);
	private boolean _notUsedTransform = true;
	private static final int TRAPS_COUNT = 4;

	private static final Location[] TRAP_LOCS = {
			new Location(-248776, 206872, -11968),
			new Location(-252024, 206872, -11968),
			new Location(-251544, 209592, -11968),
			new Location(-249256, 209592, -11968) };

	private final ArrayList<L2MonsterInstance> _traps = new ArrayList<L2MonsterInstance>(TRAPS_COUNT);
	private ScheduledFuture<?> _trapsSpawnTask;
	private static final long TRAPS_SPAWN_INTERVAL = 3 * 60 * 1000; // 3 мин
	private static final long COLLAPSE_BY_INACTIVITY_INTERVAL = 10 * 60 * 1000; // 10 мин
	private long _lastAttackTime = 0;
	private static final int TRAP_NPC_ID = 18696;
	private static final int TIAT_MINION_ID = 29162;

	private long _lastFactionNotifyTime = 0;

	public Tiat(final L2Character actor)
	{
		super(actor);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void onEvtSpawn()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		actor.setImmobilized(true);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		_lastAttackTime = System.currentTimeMillis();

		if(_notUsedTransform && actor.getCurrentHpPercents() < 50)
		{
			actor.setImmobilized(false);
			_notUsedTransform = false;

			// Если вдруг запущен таск, останавливаем его
			if(_trapsSpawnTask != null)
			{
				System.out.println("WARNING! Tiat AI: _trapsSpawnTask already running!");
				_trapsSpawnTask.cancel(true);
				_trapsSpawnTask = null;
			}
			_trapsSpawnTask = L2GameThreadPools.getInstance().scheduleAiAtFixedRate(new TrapsSpawnTask(), 1, TRAPS_SPAWN_INTERVAL, false);

			clearTasks();
			addTaskBuff(actor, TIAT_TRANSFORMATION_SKILL);
		}

		// Агрим миньёнов
		if(System.currentTimeMillis() - _lastFactionNotifyTime > actor.minFactionNotifyInterval)
		{
			for(final L2NpcInstance npc : actor.getAroundNpc(10000, 500))
				if(npc.getNpcId() == TIAT_MINION_ID)
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 1);

			_lastFactionNotifyTime = System.currentTimeMillis();
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtSeeSpell(final L2Skill skill, final L2Character caster)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || skill == null || !caster.isPlayer())
			return;

		// Обработка для Sacred Light Fragment
		if(skill.getId() == 2839 && !_notUsedTransform)
			actor.breakCast(true);

		super.onEvtSeeSpell(skill, caster);
	}

	@Override
	protected boolean thinkActive()
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		// Коллапсируем инстанс, если Тиата не били более 10 мин
		if(_lastAttackTime != 0 && _lastAttackTime + COLLAPSE_BY_INACTIVITY_INTERVAL < System.currentTimeMillis())
		{
			final Reflection r = actor.getReflection();

			// Очищаем инстанс, запускаем 5 мин коллапс
			r.clearReflection(5, true);

			// отменяем таск на спаун ловушек
			if(_trapsSpawnTask != null)
			{
				_trapsSpawnTask.cancel(true);
				_trapsSpawnTask = null;
			}

			// удаляем ловушки
			for(final L2MonsterInstance mob : _traps)
				if(mob != null)
					mob.deleteMe();

			// убираем самого Тиата
			actor.deleteMe();

			// Показываем финальный ролик при фейле через секунду после очистки инстанса
			L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public void run()
				{
					for(final L2Player pl : r.getPlayers())
						if(pl != null)
							pl.showQuestMovie(ExStartScenePlayer.SCENE_TIAT_FAIL);
				}
			}, 1000);

			return true;
		}

		return super.thinkActive();
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		_notUsedTransform = true;
		_lastAttackTime = 0;
		_lastFactionNotifyTime = 0;
		if(_trapsSpawnTask != null)
		{
			_trapsSpawnTask.cancel(true);
			_trapsSpawnTask = null;
		}

		// удаляем ловушки
		for(final L2MonsterInstance mob : _traps)
			if(mob != null)
				mob.deleteMe();
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	public class TrapsSpawnTask implements Runnable
	{
		@Override
		public void run()
		{
			final L2NpcInstance actor = getActor();
			if(actor == null || actor.isDead())
			{
				if(_trapsSpawnTask != null)
				{
					_trapsSpawnTask.cancel(true);
					_trapsSpawnTask = null;
				}
				return;
			}

			sendScreenMessage("quests_custom._1203_SeedOfDestruction._1203_SeedOfDestruction.TiatAttacked");
			final Reflection r = actor.getReflection();
			for(int index = 0; index < TRAPS_COUNT; index++)
			{
				// Не спауним ловушки, если они уже есть в том месте
				L2MonsterInstance oldTrap = null;
				if(index < _traps.size())
					oldTrap = _traps.get(index);
				if(oldTrap != null && !oldTrap.isDead())
					continue;

				final L2MonsterInstance trap = new L2MonsterInstance(IdFactory.getInstance().getNextId(), NpcTable.getTemplate(TRAP_NPC_ID));
				trap.setSpawnedLoc(TRAP_LOCS[index]);
				trap.setReflection(r);
				trap.onSpawn();
				trap.spawnMe(trap.getSpawnedLoc());
				r.addSpawn(trap.getSpawn());
				if(index < _traps.size())
					_traps.remove(index);
				_traps.add(index, trap);
			}
		}
	}

	private void sendScreenMessage(final String address)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		final Reflection r = actor.getReflection();
		for(final L2Player pl : r.getPlayers())
			if(pl != null)
				pl.sendPacket(new ExShowScreenMessage(new CustomMessage(address, pl).toString(), 10000, ScreenMessageAlign.MIDDLE_CENTER, false, 1, 0, false));
	}

}
