package l2n.game.scripts.ai.sod;

import l2n.game.ai.DefaultAI;
import l2n.game.model.actor.L2Character;

public class NoRndWalkMonster extends DefaultAI
{
	/**
	 * @param actor
	 */
	public NoRndWalkMonster(L2Character actor)
	{
		super(actor);
		_isMobile = false;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}
