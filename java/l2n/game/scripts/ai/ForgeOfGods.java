package l2n.game.scripts.ai;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Rnd;


public class ForgeOfGods extends Fighter
{
	private static final int MOBS[] = { 18799, 18800, 18801, 18802, 18803 };

	public ForgeOfGods(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(actor.isDead())
		{
			if(Rnd.chance(40)) // TODO: Уточнить шанс
				for(int i = 0; i < 1; i++)
					try
					{
						L2Spawn spawn = new L2Spawn(NpcTable.getTemplate(MOBS[Rnd.get(MOBS.length)]));
						spawn.setLoc(actor.getLoc());
						L2NpcInstance npc = spawn.doSpawn(true);
						npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, Rnd.get(1, 100));
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
		}
		super.onEvtDead(killer);
	}
}
