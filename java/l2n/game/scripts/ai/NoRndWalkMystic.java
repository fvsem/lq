package l2n.game.scripts.ai;

import l2n.game.ai.Mystic;
import l2n.game.model.actor.L2Character;


public class NoRndWalkMystic extends Mystic
{
	public NoRndWalkMystic(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
