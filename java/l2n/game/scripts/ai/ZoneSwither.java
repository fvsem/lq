package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.DefaultAI;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;


public class ZoneSwither extends DefaultAI
{
	private static final String msg = "The Poison device has been activated.";

	private final int zoneId;

	public ZoneSwither(L2Character actor)
	{
		super(actor);
		zoneId = 4637 + actor.getNpcId() - 32057;
	}

	@Override
	protected void onEvtSpawn()
	{
		ZoneManager.getInstance().getZoneById(ZoneType.poison, zoneId, false).setActive(true);
		L2NpcInstance actor = getActor();
		if(actor != null)
			Functions.npcSay(actor, msg);
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		ZoneManager.getInstance().getZoneById(ZoneType.poison, zoneId, false).setActive(false);
		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}
}
