package l2n.game.scripts.ai;

import l2n.extensions.listeners.DayNightChangeListener;
import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.game.GameTimeController;
import l2n.game.ai.Mystic;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

public class NightAgressionMystic extends Mystic implements PropertyCollection
{
	public NightAgressionMystic(L2Character actor)
	{
		super(actor);
		GameTimeController.getInstance().getListenerEngine().addPropertyChangeListener(GameTimeControllerDayNightChange, new NightAgressionDayNightListener());
	}

	private class NightAgressionDayNightListener extends DayNightChangeListener
	{
		private NightAgressionDayNightListener()
		{
			if(GameTimeController.getInstance().isNowNight())
				switchToNight();
			else
				switchToDay();
		}

		/**
		 * Вызывается, когда на сервере наступает ночь
		 */
		@Override
		public void switchToNight()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
				actor.setAggroRange(-1);
		}

		/**
		 * Вызывается, когда на сервере наступает день
		 */
		@Override
		public void switchToDay()
		{
			L2NpcInstance actor = getActor();
			if(actor != null)
				actor.setAggroRange(0);
		}
	}
}
