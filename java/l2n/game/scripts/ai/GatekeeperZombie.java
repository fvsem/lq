package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Mystic;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.instances.L2NpcInstance;


public class GatekeeperZombie extends Mystic
{
	public GatekeeperZombie(L2Character actor)
	{
		super(actor);
		actor.setImmobilized(true);
	}

	@Override
	public void checkAggression(L2Character target)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || target == null || !target.isPlayable())
			return;
		if(getIntention() != CtrlIntention.AI_INTENTION_ACTIVE)
			return;
		if(_globalAggro < 0)
			return;
		if(!actor.isInRange(target, actor.getAggroRange()))
			return;
		if(Math.abs(target.getZ() - actor.getZ()) > 400)
			return;
		if(Functions.getItemCount((L2Playable) target, 8067) != 0 || Functions.getItemCount((L2Playable) target, 8064) != 0)
			return;
		if(!GeoEngine.canSeeTarget(actor, target, false))
			return;
		target.addDamageHate(actor, 0, 1);
		setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
