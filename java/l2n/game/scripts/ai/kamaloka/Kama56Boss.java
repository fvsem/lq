package l2n.game.scripts.ai.kamaloka;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.util.MinionList;
import l2n.util.Rnd;

import java.util.ArrayList;
import java.util.List;

public class Kama56Boss extends Fighter
{
	private long _nextOrderTime = 0;
	private L2Player _lastMinionsTarget = null;

	public Kama56Boss(L2Character actor)
	{
		super(actor);
	}

	private void sendOrderToMinions(L2NpcInstance actor)
	{
		if(!actor.isInCombat())
		{
			_lastMinionsTarget = null;
			return;
		}

		MinionList ml = ((L2ReflectionBossInstance) actor).getMinionList();
		if(ml == null || !ml.hasMinions())
		{
			_lastMinionsTarget = null;
			return;
		}

		long now = System.currentTimeMillis();
		if(_nextOrderTime > now && _lastMinionsTarget != null)
		{
			L2Player old_target = _lastMinionsTarget;
			if(old_target != null && !old_target.isAlikeDead())
			{
				for(L2MinionInstance m : ml.getSpawnedMinions())
					if(m.getAI().getAttackTarget() != old_target)
					{
						m.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, old_target, 10000000);
						m.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, old_target);
					}
				return;
			}
		}

		_nextOrderTime = now + 30000;

		GArray<L2Player> pl = L2World.getAroundPlayers(actor);
		if(pl.isEmpty())
		{
			_lastMinionsTarget = null;
			return;
		}

		List<L2Player> alive = new ArrayList<L2Player>(6);
		for(L2Player p : pl)
			if(!p.isAlikeDead())
				alive.add(p);
		if(alive.isEmpty())
		{
			_lastMinionsTarget = null;
			return;
		}

		L2Player target = alive.get(Rnd.get(alive.size()));
		_lastMinionsTarget = target;

		Functions.npcShoutCustomMessage(actor, "Kama56Boss.attack", target.getName());
		for(L2MinionInstance m : ml.getSpawnedMinions())
		{
			m.clearAggroList(false);
			m.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, target, 10000000);
			m.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
		}
	}

	@Override
	protected void thinkAttack()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		sendOrderToMinions(actor);
		super.thinkAttack();
	}
}
