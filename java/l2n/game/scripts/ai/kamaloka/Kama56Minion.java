package l2n.game.scripts.ai.kamaloka;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;

public class Kama56Minion extends Fighter
{
	public Kama56Minion(L2Character actor)
	{
		super(actor);
		actor.setInvul(true);
	}

	@Override
	protected void onEvtAggression(L2Character attacker, int aggro)
	{
		if(aggro < 10000000)
			return;
		super.onEvtAggression(attacker, aggro);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{}
}
