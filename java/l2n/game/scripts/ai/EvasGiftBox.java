package l2n.game.scripts.ai;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;


public class EvasGiftBox extends Fighter
{
	private static final String KISS_OF_EVA = "breath";

	private static final int Red_Coral = 9692;
	private static final int Crystal_Fragment = 9693;

	public EvasGiftBox(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor != null && killer != null)
		{
			L2Player player = killer.getPlayer();
			if(player != null && player.getEffectList().getFirstEffect(KISS_OF_EVA) != null)
				actor.dropItem(player, Rnd.chance(50) ? Red_Coral : Crystal_Fragment, 1);
		}
		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
