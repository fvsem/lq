package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Ranger;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;


public class KarulBugbear extends Ranger
{
	private boolean _firstTimeAttacked = true;

	public KarulBugbear(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(_firstTimeAttacked)
		{
			_firstTimeAttacked = false;
			if(Rnd.chance(10))
				Functions.npcShout(actor, "Ваша задниче практически неохраняема!");
		}
		else if(Rnd.chance(5))
			Functions.npcShout(actor, "Смотрите назад!");
		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_firstTimeAttacked = true;
		super.onEvtDead(killer);
	}
}
