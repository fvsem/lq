package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.model.L2ObjectTasks;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;


public class WatchmanMonster extends Fighter
{
	private L2Character _attacker = null;

	private long _lastSearch = 0;
	private boolean isSearching = false;
	private static final String[] flood = { "Я вернусь", "Ты сильнее, чем ожидалось" };
	private static final String[] flood2 = { "Помогите мне!", "Тревога! Мы находимся под атакой!" };

	public WatchmanMonster(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;
		if(attacker != null && !actor.getFactionId().isEmpty() && actor.getCurrentHpPercents() < 50 && _lastSearch < System.currentTimeMillis() - 15000)
		{
			_lastSearch = System.currentTimeMillis();
			_attacker = attacker;

			if(findHelp())
				return;
			Functions.npcSay(actor, "Кто то, Помогите Мне!");
		}
		super.onEvtAttacked(attacker, damage);
	}

	private boolean findHelp()
	{
		isSearching = false;
		final L2NpcInstance actor = getActor();
		if(actor == null || _attacker == null)
			return false;

		for(final L2NpcInstance npc : actor.getAroundNpc(1000, 150))
			if(!actor.isDead() && npc.getFactionId().equals(actor.getFactionId()) && !npc.isInCombat())
			{
				clearTasks();
				isSearching = true;
				addTaskMove(npc.getLoc());
				Functions.npcSay(actor, flood[Rnd.get(flood.length)]);
				return true;
			}
		return false;
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_lastSearch = 0;
		_attacker = null;
		isSearching = false;
		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtArrived()
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;
		if(isSearching)
		{
			if(_attacker != null)
			{
				Functions.npcSay(actor, flood2[Rnd.get(flood2.length)]);
				actor.callFriends(_attacker, 100);
				L2GameThreadPools.getInstance().executeAi(new L2ObjectTasks.NotifyFaction(actor, _attacker, 100), false);
			}
			isSearching = false;
			notifyEvent(CtrlEvent.EVT_AGGRESSION, _attacker, 100);
		}
		else
			super.onEvtArrived();
	}

	@Override
	protected void onEvtAggression(L2Character target, int aggro)
	{
		if(!isSearching)
			super.onEvtAggression(target, aggro);
	}
}
