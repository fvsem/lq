package l2n.game.scripts.ai;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.Fighter;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.util.Location;
import l2n.util.Rnd;


public class FrostBuffalo extends Fighter
{
	private boolean _mobsNotSpawned = true;
	private static final int MOBS = 22093;
	private static final int MOBS_COUNT = 4;

	public FrostBuffalo(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		if(_mobsNotSpawned)
		{
			if(attacker.isSummon() || attacker.isPet())
				attacker = attacker.getPlayer();
			_mobsNotSpawned = false;
			for(int i = 0; i < MOBS_COUNT; i++)
				try
				{
					Location pos = GeoEngine.findPointToStay(actor.getX(), actor.getY(), actor.getZ(), 100, 120);
					L2Spawn sp = new L2Spawn(NpcTable.getTemplate(MOBS));
					sp.setLoc(pos);
					L2NpcInstance npc = sp.doSpawn(true);
					npc.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, Rnd.get(1, 100));
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		_mobsNotSpawned = true;
		super.onEvtDead(killer);
	}

	@Override
	protected boolean randomWalk()
	{
		return _mobsNotSpawned;
	}
}
