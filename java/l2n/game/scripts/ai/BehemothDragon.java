package l2n.game.scripts.ai;

import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.ItemTable;

/**
 * При смерти дропает кучу хербов.
 */
public class BehemothDragon extends Fighter
{
	public BehemothDragon(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(L2Character killer)
	{
		L2NpcInstance actor = getActor();
		if(actor == null)
			return;
		if(killer != null)
		{
			L2Player player = killer.getPlayer();
			if(player != null)
				for(int i = 0; i < 10; i++)
				{
					ItemTable.getInstance().createItem(8604, 0, 0, "[AI] BehemothDragon").dropToTheGround(player, actor); // Greater Herb of Mana
					ItemTable.getInstance().createItem(8601, 0, 0, "[AI] BehemothDragon").dropToTheGround(player, actor); // Greater Herb of Life
				}
		}
		super.onEvtDead(killer);
	}
}
