package l2n.game.scripts.ai;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Rnd;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;


public class MoSMonk extends Fighter
{
	public MoSMonk(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onIntentionAttack(L2Character target)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return;

		if(getIntention() == AI_INTENTION_ACTIVE && Rnd.chance(50))
			Functions.npcShout(actor, "Вы не можете носить оружие без разрешения!");
		super.onIntentionAttack(target);
	}

	@Override
	public void checkAggression(L2Character target)
	{
		if(target.getActiveWeaponInstance() == null)
			return;
		super.checkAggression(target);
	}
}
