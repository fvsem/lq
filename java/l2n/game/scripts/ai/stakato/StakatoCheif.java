package l2n.game.scripts.ai.stakato;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.ai.tasks.random.DespawnTask;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Rnd;


public class StakatoCheif extends Fighter
{
	// [s_stakato_nest_mass_slow1] = mass_slow 6237
	// [s_stakato_nest_earthquake1] = mass_slow_damage 6238

	// 20 мин
	private static final long DELETE_TIME = 15 * 60 * 1000;

	private static final L2Skill party_recall = SkillTable.getInstance().getInfo(6236, 1);
	private static final L2Skill mass_slow = SkillTable.getInstance().getInfo(6237, 1);
	private static final L2Skill mass_slow_damage = SkillTable.getInstance().getInfo(6238, 1);

	private long last_skill_timer = 0;

	/**
	 * @param actor
	 */
	public StakatoCheif(final L2Character actor)
	{
		super(actor);
	}

	@Override
	public void startAITask()
	{
		if(!isActive())
			addRandomTask(new DespawnTask("Is that it? Is that the extent of your abilities? Put in a little more effort!!", DELETE_TIME));
		super.startAITask();
	}

	@Override
	protected void onEvtAttacked(final L2Character attacker, final int damage)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isDead())
			if(actor.getCurrentHpPercents() <= 33 && System.currentTimeMillis() - last_skill_timer > 45 * 1000)
			{
				Functions.npcSay(actor, "Вы находитесь под большим пальцем!!");
				addUseSkillDesire(actor, party_recall, Integer.MAX_VALUE);
				last_skill_timer = System.currentTimeMillis();
			}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected void onEvtFinishCasting(final L2Skill skill)
	{
		final L2NpcInstance actor = getActor();
		if(actor != null && !actor.isDead() && skill != null)
			if(skill.getId() == party_recall.getId())
			{
				final L2Character target = actor.getMostHated();
				if(target != null)
				{
					if(actor.isInRange(target, 2000))
						actor.teleToLocation(target.getLoc());
					addTaskCast(target, mass_slow, Integer.MAX_VALUE);
				}
			}
			else if(skill.getId() == mass_slow.getId())
			{
				final L2Character target = actor.getMostHated();
				addTaskCast(target, mass_slow_damage, Integer.MAX_VALUE);
			}

		super.onEvtFinishCasting(skill);
	}

	@Override
	protected void onEvtDead(final L2Character killer)
	{
		final L2NpcInstance actor = getActor();
		if(actor == null)
			return;

		// Пробуем выдать награду
		if(killer != null && killer.getPlayer() != null)
		{
			final L2Player player = killer.getPlayer();
			if(player != null)
			{
				// 20% что даст Large Stakato Cocoon
				int rewardId = 0;
				// если в пати, то даём всем пати мемберам
				if(player.isInParty())
					for(final L2Player member : player.getParty().getPartyMembers())
					{
						rewardId = Rnd.chance(20) ? 14834 : 14833;
						member.addItem(rewardId, 1, actor.getNpcId(), "StakatoChief");
						member.sendPacket(SystemMessage.obtainItems(rewardId, 1, 0));
					}
				else
				{
					rewardId = Rnd.chance(20) ? 14834 : 14833;
					player.addItem(rewardId, 1, actor.getNpcId(), "StakatoChief");
					player.sendPacket(SystemMessage.obtainItems(rewardId, 1, 0));
				}
			}
		}

		super.onEvtDead(killer);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}
}
