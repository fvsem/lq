package l2n.game.scripts.ai.stakato;

import l2n.game.ai.CtrlEvent;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.DefaultAI;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;

import static l2n.game.ai.CtrlIntention.AI_INTENTION_ACTIVE;

public class BizarreCocoon extends DefaultAI
{
	// FIXME по идее все можно вынести в БД, а инвул давать в другом месте

	public BizarreCocoon(L2Character actor)
	{
		super(actor);
		// бесмертные
		actor.setInvul(true);
	}

	@Override
	protected void onEvtAttacked(L2Character attacker, int damage)
	{
		L2NpcInstance actor = getActor();
		if(attacker == null || actor == null || actor.isDead())
			return;

		if(getIntention() == CtrlIntention.AI_INTENTION_ACTIVE)
		{
			// при атака агрят мобов вокруг себя с рейнджем 1000
			for(L2NpcInstance friend : L2World.getAroundNpc(actor, 1000, 200))
				if(friend != null && friend.getAI().getIntention() == AI_INTENTION_ACTIVE && GeoEngine.canSeeTarget(friend, actor, false))
					friend.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 10);
		}

		super.onEvtAttacked(attacker, damage);
	}

	@Override
	protected boolean randomAnimation()
	{
		return false;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}
