package l2n.game.communitybbs.handlers;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.network.serverpackets.WareHouseDepositList;
import l2n.game.network.serverpackets.WareHouseWithdrawList;
import l2n.util.Files;

import java.util.logging.Logger;

public final class WarehouseHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler
{
	private final static Logger _log = Logger.getLogger(WarehouseHandler.class.getName());

	private static final String[] commands = new String[]
	{
			"_bbs_withdraw_private",
			"_bbs_deposit_private",
			"_bbs_withdraw_clan",
			"_bbs_deposit_clan"
	};

	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbs_withdraw_private"))
		{
			if(params == null || params.length <= 1)
			{
				final String content = Files.read(Config.BBS_HOME_DIR +"/handlers/wh_private.htm", activeChar);
				separateAndSend(content, activeChar);
			}
			else
			{
				final int val = Integer.parseInt(params[1]);
				if(val > 8 || val < 0)
				{
					activeChar.sendActionFailed();
					return true;
				}
				activeChar.setUsingWarehouseType(WarehouseType.PRIVATE);
				activeChar.sendPacket(new WareHouseWithdrawList(activeChar, WarehouseType.PRIVATE, ItemClass.values()[val]));
			}
		}
		else if(command.equalsIgnoreCase("_bbs_deposit_private"))
		{
			activeChar.setUsingWarehouseType(WarehouseType.PRIVATE);
			activeChar.tempInventoryDisable();
			activeChar.sendPacket(new WareHouseDepositList(activeChar, WarehouseType.PRIVATE), Msg.ActionFail);
		}
		else if(command.equalsIgnoreCase("_bbs_withdraw_clan"))
		{
			if(params == null || params.length <= 1)
			{
				final String content = Files.read(Config.BBS_HOME_DIR +"/handlers/wh_clan.htm", activeChar);
				separateAndSend(content, activeChar);
			}
			else
			{
				// Разрешить брать вещи из клан-верхауза не только КЛу.
				if(!(activeChar.isClanLeader() || Config.ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE && (activeChar.getClanPrivileges() & L2Clan.CP_CL_VIEW_WAREHOUSE) == L2Clan.CP_CL_VIEW_WAREHOUSE))
				{
					activeChar.sendPacket(Msg.ITEMS_LEFT_AT_THE_CLAN_HALL_WAREHOUSE_CAN_ONLY_BE_RETRIEVED_BY_THE_CLAN_LEADER_DO_YOU_WANT_TO_CONTINUE);
					return false;
				}

				if((activeChar.getClanPrivileges() & L2Clan.CP_CL_VIEW_WAREHOUSE) == L2Clan.CP_CL_VIEW_WAREHOUSE)
				{
					final int val = Integer.parseInt(params[1]);
					if(val > 8 || val < 0)
					{
						activeChar.sendActionFailed();
						return true;
					}

					activeChar.setUsingWarehouseType(WarehouseType.CLAN);
					activeChar.tempInventoryDisable();
					activeChar.sendPacket(new WareHouseWithdrawList(activeChar, WarehouseType.CLAN, ItemClass.values()[val]));
				}
				else
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_THE_RIGHT_TO_USE_THE_CLAN_WAREHOUSE, Msg.ActionFail);
			}
		}
		else if(command.equalsIgnoreCase("_bbs_deposit_clan"))
		{
			activeChar.setUsingWarehouseType(WarehouseType.CLAN);
			activeChar.tempInventoryDisable();
			activeChar.sendPacket(new WareHouseDepositList(activeChar, WarehouseType.CLAN));
		}

		return false;
	}

	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		if(!activeChar.getPlayerAccess().UseWarehouse)
			return false;

		// lil check to prevent enchant exploit
		if(activeChar.getActiveEnchantItem() != null || activeChar.getActiveEnchantAttrItem() != null)
		{
			_log.info("Player " + activeChar.getName() + " trying to use enchant exploit, ban this player!");
			activeChar.sendActionFailed();
			return false;
		}

		// Проверяем не торгует ли уже чар.
		if(activeChar.isInStoreMode())
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return false;
		}

		// Во время обмена невозможно
		if(activeChar.isInTransaction() && activeChar.getTransaction().isTypeOf(TransactionType.TRADE))
		{
			activeChar.sendActionFailed();
			return false;
		}

		if(command.equalsIgnoreCase("_bbs_withdraw_clan") || command.equalsIgnoreCase("_bbs_deposit_clan"))
		{
			if(activeChar.getClan() == null)
			{
				activeChar.sendActionFailed();
				return false;
			}

			final L2Clan _clan = activeChar.getClan();
			if(_clan.getLevel() == 0)
			{
				activeChar.sendPacket(Msg.ONLY_CLANS_OF_CLAN_LEVEL_1_OR_HIGHER_CAN_USE_A_CLAN_WAREHOUSE, Msg.ActionFail);
				return false;
			}
		}

		return true;
	}

	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
