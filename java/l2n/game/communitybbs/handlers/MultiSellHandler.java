package l2n.game.communitybbs.handlers;

import l2n.Config;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.communitybbs.Manager.TopBBSManager;
import l2n.game.model.L2Multisell;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.util.IllegalPlayerAction;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.CommunityBoardType;
import l2n.util.Util;

public final class MultiSellHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler
{
	private static final String[] commands = new String[]
	{
			"_bbsmultisell"
	};

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#execute(l2n.game.model.actor.L2Player, java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbsmultisell"))
		{
			TopBBSManager.getInstance().parsecmd("_bbstop;" + params[1], activeChar);
			final int listId = Integer.parseInt(params[2]);

			if(!Config.COMMUNITY_BOARD_AVAILABLE_MULTISELL.contains(listId))
			{
				Util.handleIllegalPlayerAction(activeChar, "CommunityBoard[112]", "Try use not allowed multisell", IllegalPlayerAction.CRITICAL);
				return false;
			}

			// ID мультиселлов которые запрещены для игроков без ПА
			if(Config.COMMUNITY_BOARD_DISABLED_MULTISELL.contains(listId) && !activeChar.hasPremiumAccount())
				PremiumAccess.sendPage(activeChar);
			else
				L2Multisell.getInstance().separateAndSend(listId, activeChar, 0);
		}
		return true;
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#checkCondition(l2n.game.model.actor.L2Player, java.lang.String)
	 */
	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		if(!Config.COMMUNITY_BOARD_ALLOW_SHOP)
		{
			ShowBoard.disabled(activeChar);
			return false;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessCommunity(activeChar, CommunityBoardType.SHOP))
			return false;

		return true;
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#getCommands()
	 */
	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
