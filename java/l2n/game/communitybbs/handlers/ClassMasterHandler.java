package l2n.game.communitybbs.handlers;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Files;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.CommunityBoardType;
import l2n.util.Util;

public final class ClassMasterHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler
{
	private static final String[] commands = new String[]
	{
			"_bbsclass",
			"_bbsclass_change"
	};

	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbsclass"))
			makeMessage(activeChar);
		else if(command.equalsIgnoreCase("_bbsclass_change"))
		{
			final short id = Short.parseShort(params[1]);
			// проверяем можно ли менять на этот класс
			if(!ClassId.values()[id].equalsOrChildOf(ClassId.values()[activeChar.getActiveClassId()]))
			{
				sendMessage(activeChar, Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.CanNotInTransform", activeChar)));
				return false;
			}

			final int jobLevel = activeChar.getClassId().getLevel();

			// Проверка на наличие предметов у игрока для смены класса.
			if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel) != null && Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).size() > 0)
			{
				for(final int _itemId : Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).keys())
				{
					final int _count = Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).get(_itemId);
					final L2ItemInstance item = activeChar.getInventory().getItemByItemId(_itemId);
					if(item == null || item.getCount() < _count)
					{
						activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT);
						return false;
					}
				}
			}

			// Удаление всех предметов необходимых для смены класса.
			if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel) != null && Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).size() > 0)
				for(final int itemId : Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).keys())
				{
					final int count = Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).get(itemId);
					final L2ItemInstance item = activeChar.getInventory().getItemByItemId(itemId);

					if(item != null && item.getCount() >= count)
						activeChar.getInventory().destroyItem(item, count, true);
				}

			// Выдача награды за смену класса.
			if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRewardItems(jobLevel) != null && Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRewardItems(jobLevel).size() > 0)
				for(final int _itemId : Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRewardItems(jobLevel).keys())
				{
					final long _count = Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRewardItems(jobLevel).get(_itemId);
					final L2Item item = ItemTable.getInstance().getTemplate(_itemId);
					activeChar.getInventory().addItem(_itemId, _count, 0, "Class Master");
					final SystemMessage sm = new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S);
					sm.addString(item.getName());
					sm.addNumber(_count);
					activeChar.sendPacket(sm);
				}

			changeClass(activeChar, id);

			// Выдаём скилы
			activeChar.rewardSkills();
			sendMessage(activeChar, Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.YouNowBecome", activeChar)) + " <font color=\"LEVEL\">" + activeChar.getClassId().getNormalName(-1) + "</font>!");
			// Update the overloaded status of the L2PcInstance
			activeChar.refreshOverloaded();
			// Update the expertise status of the L2PcInstance
			activeChar.refreshExpertisePenalty();
		}
		return true;
	}

	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS_LINE.equalsIgnoreCase("false"))
		{
			ShowBoard.disabled(activeChar);
			return false;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessCommunity(activeChar, CommunityBoardType.CLASS))
			return false;

		// Профу нельзя получить, пока используется скилл или персонаж находится в режиме трансформации
		if(activeChar.isActionsDisabled() || activeChar.getTransformationId() != 0)
		{
			sendMessage(activeChar, Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.CanNotInTransform", activeChar)));
			return false;
		}

		return true;
	}

	private void makeMessage(final L2Player player)
	{
		final StringBuilder html = new StringBuilder();

		final ClassId classId = player.getClassId();
		final int jobLevel = classId.getLevel();
		final int level = player.getLevel();

		final int newJobLevel = jobLevel + 1;

		if((level >= 20 && jobLevel == 1 || level >= 40 && jobLevel == 2 || level >= 76 && jobLevel == 3) && Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(jobLevel))
		{
			html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.YouCanChangeProfession", player)) + "<br>");

			for(final ClassId child : ClassId.values())
			{
				// Инспектор является наследником trooper и warder, но сменить его как профессию нельзя,
				// т.к. это сабкласс. Наследуется с целью получения скилов родителей.
				if(child == ClassId.inspector)
					continue;

				if(child.childOf(classId) && child.getLevel() == newJobLevel)
					html.append("<button value=\"").append(child.getNormalName(-1)).append("\" action=\"bypass -h _bbsclass_change ").append(child.getId()).append("\" width=110 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"><br1>");
			}

			if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel) != null && Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).size() > 0)
			{
				html.append("<br><br>" + Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ToChangeYouMust", player)));
				html.append("<table width=270>");
				for(final int _itemId : Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).keys())
				{
					final int _count = Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.getRequireItems(jobLevel).get(_itemId);
					html.append("<tr><td><font color=\"LEVEL\">" + _count + "</font></td><td>" + ItemTable.getInstance().getTemplate(_itemId).getName() + "</td></tr>");
				}
				html.append("</table>");
			}
			html.append("<br>");
		}
		else
		{
			switch (jobLevel)
			{
				case 1:
					if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(1))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenLevel", player).addNumber(20)) + "<br>");
					else if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(2))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenProfession", player).addNumber(1)) + "<br>");
					else if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(3))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenProfession", player).addNumber(2)) + "<br>");
					else
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.AlreadyAllClasses", player)) + "<br>");
					break;
				case 2:
					if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(2))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenLevel", player).addNumber(40)) + "<br>");
					else if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(3))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenProfession", player).addNumber(2)) + "<br>");
					else
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.CanNotChangeProfession", player)) + "<br>");
					break;
				case 3:
					if(Config.COMMUNITY_BOARD_CLASS_MASTER_SETTINGS.isAllowed(3))
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.ReturnWhenLevel", player).addNumber(76)) + "<br>");
					else
						html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.CanNotChangeProfession", player)) + "<br>");
					break;
				case 4:
					html.append(Util.printCM(new CustomMessage("print.l2n.game.model.instances.L2ClassMasterInstance.AlreadyAllClasses", player)) + "<br>");
					break;
			}
			// If the player hasn't available class , he can change pet too...
			html.append("<br>");
		}

		String content = Files.read(Config.BBS_HOME_DIR +"/class_master.htm", player);
		content = content.replace("%class_master%", html.toString());
		separateAndSend(content, player);
	}

	private void changeClass(final L2Player player, final short val)
	{
		if(player.getClassId().getLevel() == 3)
			player.sendPacket(Msg.YOU_HAVE_COMPLETED_THE_QUEST_FOR_3RD_OCCUPATION_CHANGE_AND_MOVED_TO_ANOTHER_CLASS_CONGRATULATIONS); // для 3 профы
		else
			player.sendPacket(Msg.CONGRATULATIONS_YOU_HAVE_TRANSFERRED_TO_A_NEW_CLASS); // для 1 и 2 профы

		player.setClassId(val, false);
		player.broadcastUserInfo(true);
	}

	private void sendMessage(final L2Player activeChar, final String text)
	{
		String content = Files.read(Config.BBS_HOME_DIR +"/class_master.htm", activeChar);
		content = content.replace("%class_master%", text);
		separateAndSend(content, activeChar);
	}

	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
