package l2n.game.communitybbs.handlers;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.text.TextBuilder;
import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.instancemanager.RaidBossSpawnManager.RaidBossStatus;
import l2n.game.instancemanager.boss.*;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.tables.NpcTable;
import l2n.util.Files;
import l2n.util.Util;

import java.sql.ResultSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StatisticHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler
{
	private static final Logger _log = Logger.getLogger(StatisticHandler.class.getName());

	private static final class PlayerStat
	{
		public String name = "";
		public int game_time = 0;
		public int count_pk = 0;
		public int count_pvp = 0;
	}

	private static final class CastleStat
	{
		public String name = "none";
		public String percent;
		public String siege_date;
		public String owner = "NPC";
		public String color = "FFFFFF";
	}
	private static final class FortesStat
	{
        public String name = "none";
        public String siege_date;
        public String color_siege_date = "FFFFFF";
        public String owner = "NPC";
        public String color_owner = "FFFFFF";
	}
	private static final class RaidBossStat
	{
		public String name = "none";
		public int level = 0;
		public String status = "undefined";
		public String color = "00CC00";
		public String respawn_date = "";
	}

	private static final int[] RAID_BOSS = new int[]
	{
			29001, // Queen Ant
			29006, // Core
			29014, // Orfen
			25325, // Flame of Splendor Barakiel
			29062 // High Priestess van Halter
	};

	private static final TIntObjectHashMap<String> stat_cache = new TIntObjectHashMap<String>();

	/**
	 * время обновления статистики
	 */
	private static final long CACHE_UPDATE_TIME = 10 * 60 * 1000;

	private long pvp_last_update = 0;
	private long pk_last_update = 0;
	private long castle_last_update = 0;
	private long fortes_last_update = 0;
	private long raidboss_last_update = 0;

	private static final String[] commands = new String[]
	{
			"_bbsstat_pvp", // 0-4
			"_bbsstat_pk", // 1-5
			"_bbsstat_castle", // 2-6
			"_bbsstat_raidboss",//3 -7
			"_bbsstat_fortes"// 8-12
	};

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#execute(l2n.game.model.actor.L2Player, java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbsstat_pvp"))
		{
			PlayerStat tp;
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT * FROM characters WHERE accesslevel = '0' ORDER BY pvpkills DESC LIMIT 20;");
				rs = statement.executeQuery();

				final TextBuilder html = new TextBuilder();
				html.append(Files.read(Config.BBS_HOME_DIR +"/statPvP.htm", activeChar));
				while (rs.next())
				{
					tp = new PlayerStat();
					tp.name = rs.getString("char_name");
					tp.game_time = rs.getInt("onlinetime");
					tp.count_pk = rs.getInt("pkkills");
					tp.count_pvp = rs.getInt("pvpkills");
					final String online = rs.getInt("online") == 1 ? "В Игре" : "Не В Игре";
					final String color = rs.getInt("online") == 1 ? "00CC00" : "D70000";
					final String sex = rs.getInt("sex") == 1 ? "Ж" : "M";

					html.append("<tr>");
					html.append("<td width=250>" + tp.name + "</td>");
					html.append("<td width=50>" + sex + "</td>");
					html.append("<td width=100>" + onlineTime(tp.game_time) + "</td>");
					html.append("<td width=50>" + tp.count_pk + "</td>");
					html.append("<td width=50><font color=00CC00>" + tp.count_pvp + "</font></td>");
					html.append("<td width=100><font color=" + color + ">" + online + "</font></td>");
					html.append("</tr>");
				}
				html.append("</table>");

				String content = Files.read(Config.BBS_HOME_DIR +"/statistic.htm", activeChar);
				content = content.replace("%stat%", html.toString());
				TextBuilder.recycle(html);

				// ложим в кеш
				addContentInCache(activeChar.isLangRus() ? 0 : 4, content);
				separateAndSend(content, activeChar);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "StatisticHandler: get pvp stat error ", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
		else if(command.equalsIgnoreCase("_bbsstat_pk"))
		{
			PlayerStat tp;
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT * FROM characters WHERE accesslevel = '0' ORDER BY pkkills DESC LIMIT 20;");
				rs = statement.executeQuery();

				final TextBuilder html = new TextBuilder();
				html.append(Files.read(Config.BBS_HOME_DIR +"/statPK.htm", activeChar));
				while (rs.next())
				{
					tp = new PlayerStat();
					tp.name = rs.getString("char_name");
					tp.game_time = rs.getInt("onlinetime");
					tp.count_pk = rs.getInt("pkkills");
					tp.count_pvp = rs.getInt("pvpkills");

					final String online = rs.getInt("online") == 1 ? "В Игре" : "Не В Игре";
					final String color = rs.getInt("online") == 1 ? "00CC00" : "D70000";
					final String sex = rs.getInt("sex") == 1 ? "Ж" : "M";

					html.append("<tr>");
					html.append("<td width=250>" + tp.name + "</td>");
					html.append("<td width=50>" + sex + "</td>");
					html.append("<td width=100>" + onlineTime(tp.game_time) + "</td>");
					html.append("<td width=50><font color=00CC00>" + tp.count_pk + "</font></td>");
					html.append("<td width=50>" + tp.count_pvp + "</td>");
					html.append("<td width=100><font color=" + color + ">" + online + "</font></td>");
					html.append("</tr>");
				}
				html.append("</table>");

				String content = Files.read(Config.BBS_HOME_DIR +"/statistic.htm", activeChar);
				content = content.replace("%stat%", html.toString());
				TextBuilder.recycle(html);

				// ложим в кеш
				addContentInCache(activeChar.isLangRus() ? 1 : 5, content);
				separateAndSend(content, activeChar);
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "StatisticHandler: get pk stat error ", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
		}
		else if(command.equalsIgnoreCase("_bbsstat_castle"))
		{
			CastleStat cstat;
			final TextBuilder html = new TextBuilder();
			html.append(Files.read(Config.BBS_HOME_DIR +"/statCastle.htm", activeChar));
			for(final Castle castle : CastleManager.getInstance().getCastles().values())
			{
				cstat = new CastleStat();
				cstat.name = castle.getName();
				cstat.percent = castle.getTaxPercent() + "%";
				cstat.siege_date = Util.datetimeFormatter.format(castle.getSiege().getSiegeDate().getTime());
				final L2Clan owner = castle.getOwner();
				if(owner != null)
				{
					cstat.owner = owner.getName();
					cstat.color = "00CC00";
				}

				html.append("<tr>");
				html.append("<td width=100>" + cstat.name + "</td>");
				html.append("<td width=70>" + cstat.percent + "</td>");
				html.append("<td width=200><font color=" + cstat.color + ">" + cstat.owner + "</font></td>");
				html.append("<td width=230>" + cstat.siege_date + "</td>");
				html.append("</tr>");
			}
			html.append("</table>");
			String content = Files.read(Config.BBS_HOME_DIR +"/statistic.htm", activeChar);
			content = content.replace("%stat%", html.toString());
			TextBuilder.recycle(html);

			// ложим в кеш
			addContentInCache(activeChar.isLangRus() ? 2 : 6, content);
			separateAndSend(content, activeChar);
		}
		else if(command.equalsIgnoreCase("_bbsstat_fortes"))
		{
			FortesStat fstat;
            final TextBuilder html = new TextBuilder();
            html.append(Files.read(Config.BBS_HOME_DIR +"/statFortress.htm", activeChar));
            for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
            {
				fstat = new FortesStat();
				fstat.name = fortress.getName();
                Date startTime;
                if(fortress.getSiege().isInProgress() || TerritorySiege.isInProgress()){
                	fstat.siege_date = activeChar.isLangRus() ? "Война" : "War";
                	fstat.color_siege_date = "FF0000";
                }else{
                    if(System.currentTimeMillis() < fortress.getSiege().getSiegeDate().getTimeInMillis()){
                        startTime = fortress.getSiege().getSiegeDate().getTime();
                    }else{
                        startTime = new Date(System.currentTimeMillis());
                    }
                    fstat.siege_date = Util.datetimeFormatter.format(startTime);
                }
                final L2Clan owner = fortress.getOwner();
                if(owner != null)
                {
                	fstat.owner = owner.getName();
                	fstat.color_owner = "00CC00";
                }

                html.append("<tr>");
                html.append("<td width=100>" + fstat.name + "</td>");
                html.append("<td width=100><font color=" + fstat.color_owner + ">" + fstat.owner + "</font></td>");
                html.append("<td width=120><font color=" + fstat.color_siege_date + ">" + fstat.siege_date + "</font></td>");
                html.append("</tr>");
            }
            html.append("</table>");
            String content = Files.read(Config.BBS_HOME_DIR +"/statistic.htm", activeChar);
            content = content.replace("%stat%", html.toString());
            TextBuilder.recycle(html);

            // ложим в кеш
            addContentInCache(activeChar.isLangRus() ? 8 : 9, content);
            separateAndSend(content, activeChar);
        }

		else if(command.equalsIgnoreCase("_bbsstat_raidboss"))
		{
			RaidBossStat rbstat;
			final TextBuilder html = new TextBuilder();
			html.append(Files.read(Config.BBS_HOME_DIR +"/statRB.htm", activeChar));

			// load RB
			for(final int bossId : RAID_BOSS)
			{
				rbstat = new RaidBossStat();
				rbstat.name = NpcTable.getTemplate(bossId).name;
				rbstat.level = NpcTable.getTemplate(bossId).level;
				final RaidBossStatus status = RaidBossSpawnManager.getInstance().getRaidBossStatusId(bossId);
				rbstat.status = status.toString().toLowerCase();
				if(status == RaidBossStatus.DEAD)
				{
					rbstat.color = "D70000";
					rbstat.status = "Убит";
					// respawn_delay
					final StatsSet info = RaidBossSpawnManager.getInstance().getInfo(bossId);
					if(info == null)
						rbstat.respawn_date = "undefined";
					else
						rbstat.respawn_date = Util.datetimeFormatter.format(info.getLong("respawn_delay") * 1000);
				}

				addContent(html, rbstat);
			}

			// Baium
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = BaiumManager.getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Antharas
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = AntharasManager.getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Sailren
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = SailrenManager.getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Epidos
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = EpidosManager.getInstance().getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Beleth
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = BelethManager.getInstance().getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Valakas
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = ValakasManager.getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			// Scarlet van Halisha
			{
				rbstat = new RaidBossStat();
				final EpicBossState state = FrintezzaManager.getState();
				rbstat.name = NpcTable.getTemplate(state.getBossId()).name;
				rbstat.level = NpcTable.getTemplate(state.getBossId()).level;
				setRespawnDate(state, rbstat);
				addContent(html, rbstat);
			}

			html.append("</table>");
			String content = Files.read(Config.BBS_HOME_DIR +"/statistic.htm", activeChar);
			content = content.replace("%stat%", html.toString());
			TextBuilder.recycle(html);

			// ложим в кеш
			addContentInCache(activeChar.isLangRus() ? 3 : 7, content);
			separateAndSend(content, activeChar);
		}
		return false;
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#checkCondition(l2n.game.model.actor.L2Player, java.lang.String)
	 */
	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		if(command.equalsIgnoreCase("_bbsstat_pvp"))
		{
			// есил в кеше ещё нет этой страницы
			final String content = stat_cache.get(activeChar.isLangRus() ? 0 : 4);
			if(content == null)
				return true;

			// проверяем время последнего обращения к статистике
			final long currentTime = System.currentTimeMillis();
			if(currentTime - pvp_last_update > CACHE_UPDATE_TIME)
			{
				pvp_last_update = currentTime;
				return true;
			}

			// отправляем данные из кеша
			separateAndSend(content, activeChar);
		}
		else if(command.equalsIgnoreCase("_bbsstat_pk"))
		{
			// есил в кеше ещё нет этой страницы
			final String content = stat_cache.get(activeChar.isLangRus() ? 1 : 5);
			if(content == null)
				return true;

			final long currentTime = System.currentTimeMillis();
			if(currentTime - pk_last_update > CACHE_UPDATE_TIME)
			{
				pk_last_update = currentTime;
				return true;
			}

			// отправляем данные из кеша
			separateAndSend(content, activeChar);
		}
		else if(command.equalsIgnoreCase("_bbsstat_castle"))
		{
			// есил в кеше ещё нет этой страницы
			final String content = stat_cache.get(activeChar.isLangRus() ? 2 : 6);
			if(content == null)
				return true;

			final long currentTime = System.currentTimeMillis();
			if(currentTime - castle_last_update > CACHE_UPDATE_TIME)
			{
				castle_last_update = currentTime;
				return true;
			}

			// отправляем данные из кеша
			separateAndSend(content, activeChar);
		}
		else if(command.equalsIgnoreCase("_bbsstat_fortes"))
		{
			// есил в кеше ещё нет этой страницы
			final String content = stat_cache.get(activeChar.isLangRus() ? 8 : 12);
			if(content == null)
				return true;

			final long currentTime = System.currentTimeMillis();
			if(currentTime - fortes_last_update > CACHE_UPDATE_TIME)
			{
				castle_last_update = currentTime;
				return true;
			}

			// отправляем данные из кеша
			separateAndSend(content, activeChar);
		}
		else if(command.equalsIgnoreCase("_bbsstat_raidboss"))
		{
			// есил в кеше ещё нет этой страницы
			final String content = stat_cache.get(activeChar.isLangRus() ? 3 : 7);
			if(content == null)
				return true;

			final long currentTime = System.currentTimeMillis();
			if(currentTime - raidboss_last_update > CACHE_UPDATE_TIME)
			{
				raidboss_last_update = currentTime;
				return true;
			}

			// отправляем данные из кеша
			separateAndSend(content, activeChar);
		}

		return false;
	}

	private void addContentInCache(final int key, final String value)
	{
		synchronized (stat_cache)
		{
			stat_cache.put(key, value);
		}
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#getCommands()
	 */
	@Override
	public String[] getCommands()
	{
		return commands;
	}

	public static final void clearCache()
	{
		synchronized (stat_cache)
		{
			stat_cache.clear();
		}
		_log.log(Level.INFO, "StatisticHandler: HTML cache clearned.");
	}

	private static final void addContent(final TextBuilder html, final RaidBossStat rbstat)
	{
		html.append("<tr>");
		html.append("<td width=230>" + rbstat.name + "</td>");
		html.append("<td width=70><center>" + rbstat.level + "</center></td>");
		html.append("<td width=100><center><font color=" + rbstat.color + ">" + rbstat.status + "</font></center></td>");
		html.append("<td width=200>" + rbstat.respawn_date + "</td>");
		html.append("</tr>");
	}

	private static final void setRespawnDate(final EpicBossState state, final RaidBossStat rbstat)
	{
		if(state.isInterval())
		{
			rbstat.status = "Убит";
			rbstat.color = "D70000";
			rbstat.respawn_date = Util.datetimeFormatter.format(state.getRespawnDate());
		}
		else if(state.isNotSpawn())
			rbstat.status = "Не вызван";
		else if(state.isAlive())
		{
			rbstat.status = state.getState().toString().toLowerCase();
			rbstat.status = "Вызван";
			rbstat.color = "FAAC58";
		}
		else
			rbstat.status = state.getState().toString().toLowerCase();
	}

	private static final String onlineTime(final int time)
	{
		long onlinetimeH;
		int onlinetimeM;
		if(time / 60 / 60 - 0.5 <= 0)
			onlinetimeH = 0;
		else
			onlinetimeH = Math.round(time / 60 / 60 - 0.5);
		onlinetimeM = Math.round((time / 60 / 60 - onlinetimeH) * 60);
		return "" + onlinetimeH + " Ч. " + onlinetimeM + " м.";
	}
}
