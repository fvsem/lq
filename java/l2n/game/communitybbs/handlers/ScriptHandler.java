package l2n.game.communitybbs.handlers;

import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.communitybbs.Manager.TopBBSManager;
import l2n.game.model.actor.L2Player;

import java.util.HashMap;


public final class ScriptHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler
{
	private static final HashMap<String, Object> variables = new HashMap<String, Object>();
	static
	{
		variables.put("npc", null);
	}

	private static final String[] commands = new String[]
	{
			"_bbsscripts",
			"_bbsscripts_ret"
	};

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#execute(l2n.game.model.actor.L2Player, java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbsscripts"))
		{
			TopBBSManager.getInstance().showTopPage(activeChar, params[1], null);
			final String path[] = params[2].split(":");
			if(path.length != 2)
			{
				if(activeChar.isGM())
					activeChar.sendMessage("Bad Script bypass: " + params[2]);
				return false;
			}

			String[] args = null;
			if(params.length > 3)
				// всего один параметр
				if(params.length == 4)
					args = new String[] { params[3] };
				else
				{
					args = new String[params.length - 3];
					for(int i = 3; i < params.length; i++)
						args[i - 3] = params[i];
				}

			activeChar.callScripts(path[0], path[1], params.length < 4 ? null : new Object[] { args }, variables);
		}
		else if(command.equalsIgnoreCase("_bbsscripts_ret"))
		{
			final String path[] = params[2].split(":");
			if(path.length != 2)
			{
				if(activeChar.isGM())
					activeChar.sendMessage("Bad Script bypass: " + params[2]);
				return false;
			}

			String[] args = null;
			if(params.length > 3)
				// всего один параметр
				if(params.length == 4)
					args = new String[] { params[3] };
				else
				{
					args = new String[params.length - 3];
					for(int i = 3; i < params.length; i++)
						args[i - 3] = params[i];
				}

			final Object subcontent = activeChar.callScripts(path[0], path[1], params.length < 4 ? null : new Object[] { args }, variables);
			TopBBSManager.getInstance().showTopPage(activeChar, params[1], String.valueOf(subcontent));
		}
		return true;
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#checkCondition(l2n.game.model.actor.L2Player, java.lang.String)
	 */
	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		return true;
	}

	/**
	 * @see l2n.game.communitybbs.ICommunityBoardHandler#getCommands()
	 */
	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
