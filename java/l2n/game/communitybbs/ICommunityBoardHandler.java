package l2n.game.communitybbs;

import l2n.game.model.actor.L2Player;

public interface ICommunityBoardHandler
{
	public boolean execute(L2Player activeChar, String command, String[] params, String source);

	public boolean checkCondition(L2Player activeChar, String command);


	public String[] getCommands();
}
