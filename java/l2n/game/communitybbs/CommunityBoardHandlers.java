package l2n.game.communitybbs;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.game.communitybbs.handlers.*;
import l2n.game.handler.interfaces.ICommandHandler;
import l2n.game.model.actor.L2Player;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CommunityBoardHandlers implements ICommandHandler
{
	private static final Logger _log = Logger.getLogger(CommunityBoardHandlers.class.getName());

	private final TIntObjectHashMap<ICommunityBoardHandler> _cbHandlers;

	public static CommunityBoardHandlers getInstance()
	{
		return SingletonHolder._instance;
	}

	private CommunityBoardHandlers()
	{
		_cbHandlers = new TIntObjectHashMap<ICommunityBoardHandler>();
		registerHandler(new ClassMasterHandler());
		registerHandler(new MultiSellHandler());
		registerHandler(new ScriptHandler());
		registerHandler(new StatisticHandler());
		registerHandler(new WarehouseHandler());
	}

	public void registerHandler(final ICommunityBoardHandler handler)
	{
		final String[] commands = handler.getCommands();
		for(final String element : commands)
			_cbHandlers.put(element.hashCode(), handler);
	}

	public ICommunityBoardHandler getHandler(String command)
	{
		if(command.indexOf(";") != -1)
			command = command.substring(0, command.indexOf(";"));
		return _cbHandlers.get(command.hashCode());
	}

	public boolean useHandler(final L2Player activeChar, final String bypass)
	{
		final String[] wordList = bypass.split(" ");
		final ICommunityBoardHandler handler = _cbHandlers.get(wordList[0].hashCode());
		if(handler != null)
			try
			{
				if(handler.checkCondition(activeChar, wordList[0]))
					handler.execute(activeChar, wordList[0], wordList, bypass);
				return true;
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
				return true;
			}

		return false;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final CommunityBoardHandlers _instance = new CommunityBoardHandlers();
	}

	@Override
	public int size()
	{
		return _cbHandlers.size();
	}

	public void clear()
	{
		_cbHandlers.clear();
	}
}
