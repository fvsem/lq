package l2n.game.communitybbs;

import javolution.text.TextBuilder;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShowBoard;

import java.util.logging.Logger;

public abstract class AbstractCommunityBoardHandler
{
	protected final static Logger _log = Logger.getLogger(AbstractCommunityBoardHandler.class.getName());

	protected void separateAndSend(TextBuilder html, L2Player acha)
	{
		try
		{
			separateAndSend(html.toString(), acha);
		}
		finally
		{
			TextBuilder.recycle(html);
		}
	}

	protected void separateAndSend(String html, L2Player acha)
	{
		ShowBoard.separateAndSend(acha, html);
	}

	protected void notImplementedYet(L2Player activeChar, String command)
	{
		ShowBoard.notImplementedYet(activeChar, command);
	}

	protected void send1001(String html, L2Player activeChar)
	{
		ShowBoard.send1001(html, activeChar);
	}

	protected void send1002(L2Player activeChar)
	{
		ShowBoard.send1002(activeChar, " ", " ", "0");
	}

	protected void send1002(L2Player activeChar, String string, String string2, String string3)
	{
		ShowBoard.send1002(activeChar, string, string2, string3);
	}
}
