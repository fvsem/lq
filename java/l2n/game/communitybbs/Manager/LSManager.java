package l2n.game.communitybbs.Manager;

import gnu.trove.map.hash.TIntIntHashMap;
import l2n.Config;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2Augmentation;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Item.Grade;
import l2n.util.Files;
import l2n.util.Rnd;
import l2n.util.StringUtil;
import l2n.util.XMLUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * 
 */
public class LSManager extends AbstractBBSManager {

    private static final Logger _log = Logger.getLogger(LSManager.class.getName());

    private static final int MAXPAGES = 5; //максимальное количество переключателей (для корректного отображения, кнопки динам)
    private static final int column = 2; //количество столбцов
    private static final int rows = 2; //количество строк
    private static final ArrayList<Integer> _id = new ArrayList<Integer>();
    private static final TIntIntHashMap _statsId = new TIntIntHashMap();
    private static final TIntIntHashMap _skillId = new TIntIntHashMap();
    private static final TIntIntHashMap _skillLvl = new TIntIntHashMap();
    private static final Map<Integer, String> _type = new HashMap<Integer, String>();
    private static final TIntIntHashMap _itemId = new TIntIntHashMap();
    private static final TIntIntHashMap _itemCount = new TIntIntHashMap();
    private static final Map<Integer, String> _desc = new HashMap<Integer, String>();

    private static final ArrayList<Integer> _active = new ArrayList<Integer>();
    private static final ArrayList<Integer> _passive = new ArrayList<Integer>();
    private static final ArrayList<Integer> _chance = new ArrayList<Integer>();
    private static final ArrayList<Integer> _jewerly = new ArrayList<Integer>();
    
    @SuppressWarnings("synthetic-access")
    private static class SingletonHolder
    {
		protected static final LSManager _instance = new LSManager();
    }

    public static LSManager getInstance()
    {
		return SingletonHolder._instance;
    }

    public LSManager()
    {
		load();
    }
    
    @Override
    public void parsecmd(String command, L2Player player) {
        final String[] commands = command.split(" ");
        if (commands.length == 0) {
            _log.info("Wtf");
            return;
        }

        if (commands[0].equals("_bbsls") && commands.length == 1) {
            showLSIndexPage(player);
        } else if (commands[1].equalsIgnoreCase("showskillslist")) {
            final int page = Integer.parseInt(commands[2]);
            showSkillsList(player, page);
        } else if (commands[1].equalsIgnoreCase("selectlsskill")) {
            final int id = Integer.parseInt(commands[2]);
            if (!_id.contains(id)) {
                _log.info("LifeStoneCommunity: invalid id: " + id);
                return;
            }
            player.setSelectCommunitySkillLS(id);
            showLSIndexPage(player);
        } else if (commands[1].equalsIgnoreCase("itemselect")) {
            String content = Files.read(Config.BBS_HOME_DIR + "/ls/item_select.htm", player);

            final StringBuilder res = StringUtil.startAppend(400, "<table width=600>");
            for (L2ItemInstance iteminstance : player.getInventory().getItems()) {
                L2Item item = iteminstance.getItem();

                if (checkItemValiable(iteminstance)) {
                    StringUtil.append(res, "<tr>");
                    StringUtil.append(res, "<td width=34 height=34><img src=\"icon." + item.getIcon() + "\" width=32 height=32></td><td width=511>" + item.getName() + " " + item.getAdditionalName() + " " + (iteminstance.getEnchantLevel() == 0 ? "" : "+" + iteminstance.getEnchantLevel()) + "</td><td width=55><button value=\"Выбрать\" action=\"bypass _bbsls itemselected " + iteminstance.getObjectId() + "\" width=50 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
                    StringUtil.append(res, "</tr>");
                }
            }
            StringUtil.append(res, "</table>");
            content = content.replace("%item_select%", res.toString());
            ShowBoard.separateAndSend(player, content);
            StringUtil.recycle(res);
        } else if (commands[1].equalsIgnoreCase("itemselected")) {
            final int weaponObjId = Integer.parseInt(commands[2]);
            L2ItemInstance iteminstance = player.getInventory().getItemByObjectId(weaponObjId);
            if (iteminstance == null || !checkItemValiable(iteminstance)) {
                _log.info("LifeStoneCommunity: invalid item id: Character objID: " + player.getObjectId());
                return;
            }
            player.setSelectCommunityItemLS(iteminstance);
            showLSIndexPage(player);
        } else if (commands[1].equalsIgnoreCase("filter")) {
            final String filtertype = commands[2];
            if (!filtertype.equalsIgnoreCase("all") && !filtertype.equalsIgnoreCase("active") && !filtertype.equalsIgnoreCase("passive") && !filtertype.equalsIgnoreCase("chance") && !filtertype.equalsIgnoreCase("jewerly")) {
                _log.info("LifeStoneCommunity: invalid filter type: Character objID: " + player.getObjectId() + "; filterType=" + filtertype);
                return;
            }
            player.setCommunityFilterType(filtertype);

            showLSIndexPage(player);
        } 
        else if (commands[1].equalsIgnoreCase("insertlsinitem")) {
        	
        	int stat = 0;
        	if(commands[2].equalsIgnoreCase("RANDOM"))
        	{
        		stat = 0;
        	}
        	else if(commands[2].equalsIgnoreCase("STR"))
        	{
        		stat = 1;
        	}
        	else if(commands[2].equalsIgnoreCase("CON"))
        	{
        		stat = 2;
        	}
        	else if(commands[2].equalsIgnoreCase("MEN"))
        	{
        		stat = 3;
        	}       	
        	else if(commands[2].equalsIgnoreCase("INT"))
        	{
        		stat = 4;
        	}
        	
        	int id = player.getSelectCommunitySkillLS();
               
        	if (id == 0) {
                player.sendMessage("Вы не выбрали скилл");
            } else if (player.getSelectCommunityItemLS() == null) {
                player.sendMessage("Вы не выбрали оружие");
            } else if (!checkItemValiable(player.getSelectCommunityItemLS())) {
                player.sendMessage("Нельзя точить вещи ниже C грейда и рейд бижу");
            } else if (player.getSelectCommunityItemLS().isAugmented()) {
                player.sendMessage("Предмет уже зачарован");
            //} else if (player.getInventory().destroyItemByItemId(_itemId.get(id), _itemCount.get(id), true)) {
                //player.sendMessage("У вас нехватает предметов");
            } else if(!doPay(_itemId.get(id), _itemCount.get(id), player)) {
                player.sendMessage("У вас нехватает предметов");
            } else {
                L2ItemInstance iteminstance = player.getSelectCommunityItemLS();
                L2Item item = iteminstance.getItem();
                String localtype = _type.get(id);
                if (item.getType2() == L2Item.TYPE2_WEAPON && (localtype.equalsIgnoreCase("chance") || localtype.equalsIgnoreCase("passive") || localtype.equalsIgnoreCase("active"))) {
                    setAugmentation(iteminstance, id, player, stat);
                } else if (item.getType1() == L2Item.TYPE1_WEAPON_RING_EARRING_NECKLACE && localtype.equalsIgnoreCase("jewerly") && !item.isWeapon()) {
                    setAugmentation(iteminstance, id, player, 0);
                } else {
                    player.sendMessage("Вы не можете вставить скилл бижутерии в оружие, так же вы не можете вставить скилл оружия в бижутерию.");
                }
            }
        }
    }
    
    private void load() {
        final File localFile = new File("./config/CommunityLifeStoneConfig.xml");
        if (!localFile.exists()) {
            System.out.println("File CommunityLifeStoneConfig.xml not found!");
            return;
        }
        Document localDocument = null;
        try {
            final DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            localDocumentBuilderFactory.setValidating(false);
            localDocumentBuilderFactory.setIgnoringComments(true);
            localDocument = localDocumentBuilderFactory.newDocumentBuilder().parse(localFile);
        } catch (final Exception e1) {
            e1.printStackTrace();
        }
        try {
            parseFile(localDocument);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private void parseFile(final Document doc) {
        for (Node il = doc.getFirstChild(); il != null; il = il.getNextSibling()) {
            if ("list".equalsIgnoreCase(il.getNodeName())) {
                for (Node area = il.getFirstChild(); area != null; area = area.getNextSibling()) {
                    if ("aug".equalsIgnoreCase(area.getNodeName())) {
                        int id = XMLUtil.getAttributeIntValue(area, "id", 0);
                        int statsId = XMLUtil.getAttributeIntValue(area, "statsId", 0);
                        int skillId = XMLUtil.getAttributeIntValue(area, "skillId", 0);
                        int skillLvl = XMLUtil.getAttributeIntValue(area, "skillLvl", 0);
                        String type = XMLUtil.getAttributeValue(area, "type");
                        int itemId = XMLUtil.getAttributeIntValue(area, "itemId", 0);
                        int itemCount = XMLUtil.getAttributeIntValue(area, "itemCount", 0);
                        String desc = XMLUtil.getAttributeValue(area, "desc");

                        _id.add(id);
                        _statsId.put(id, statsId);
                        _skillId.put(id, skillId);
                        _skillLvl.put(id, skillLvl);
                        _type.put(id, type);
                        _itemId.put(id, itemId);
                        _itemCount.put(id, itemCount);
                        _desc.put(id, desc);

                        if (type.equalsIgnoreCase("active"))
                            _active.add(id);
                        else if (type.equalsIgnoreCase("passive"))
                            _passive.add(id);
                        else if (type.equalsIgnoreCase("chance"))
                            _chance.add(id);
                        else if (type.equalsIgnoreCase("jewerly"))
                            _jewerly.add(id);

                        if (SkillTable.getInstance().getBaseLevel(skillId) < skillLvl)
                            _log.info("CommunityLS: skillId: " + skillId + " maxlevel = " + SkillTable.getInstance().getBaseLevel(skillId) + " current = " + skillLvl);
                    }

                }
            }
        }
    }


    private boolean checkItemValiable(L2ItemInstance im) {
        L2Item item = im.getItem();
        boolean ok = false;

        if ((item.getType1() == L2Item.TYPE1_WEAPON_RING_EARRING_NECKLACE || item.getType2() == L2Item.TYPE2_WEAPON) && !im.isAugmented()) {
            Grade grade = item.getItemGrade();
            if ((grade != Grade.NONE) && (grade != Grade.D)) {
                int id = item.getItemId();
                if ((id != 6659) && (id != 6656) && (id != 6657) && (id != 8191) && (id != 6658) && (id != 6662) && (id != 6661) && (id != 6660))
                    ok = true;
            }
        }
        return ok;
    }

    private void setAugmentation(L2ItemInstance iteminstance, int id, L2Player player, int stat)
    {
        int stat34 = _statsId.get(id);

        int offset = 9 * 91 + Rnd.get(0, 1) * 3640 + (3 + 3) / 2 * 10 * 91 + 1;

        int stat12 = Rnd.get(offset, offset + 91 - 1);
        switch(stat)
        {
        	case 1:
        		stat12 = 16341;
        		break;
        	case 2:
        		stat12 = 16342;
        		break;
        	case 3:
        		stat12 = 16344;
        		break;
        	case 4:
        		stat12 = 16343;
        		break;
        		
        }
        if (iteminstance.isEquipped())
            player.getInventory().unEquipItem(iteminstance);
        final L2Skill skill = SkillTable.getInstance().getInfo(_skillId.get(id), _skillLvl.get(id));
        L2Augmentation augm = new L2Augmentation(iteminstance, (stat34 << 16) + stat12, skill, true);
        iteminstance.setAugmentation(augm);
        player.getInventory().equipItem(iteminstance, false);
        player.sendPacket(new InventoryUpdate(iteminstance, L2ItemInstance.MODIFIED));
        player.sendChanges();
        player.broadcastUserInfo(true);
        player.setSelectCommunitySkillLS(0);
        player.setSelectCommunityItemLS(null);
        player.sendMessage("Поздравляем с покупкой LS!");
        showLSIndexPage(player);
    }

    private void showSkillsList(L2Player player, int page) {
        int numpages = 0;

        if (player.getCommunityFilterType().equalsIgnoreCase("all"))
            numpages = getNumPages(_id.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("active"))
            numpages = getNumPages(_active.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("passive"))
            numpages = getNumPages(_passive.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("chance"))
            numpages = getNumPages(_chance.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("jewerly"))
            numpages = getNumPages(_jewerly.size());

        final StringBuilder html = StringUtil.startAppend(600, buildTable(page, player));

        String content = Files.read(Config.BBS_HOME_DIR + "/ls/ls.htm", player);

        final L2ItemInstance iteminstance = player.getSelectCommunityItemLS();

        if (player.getSelectCommunitySkillLS() == 0) {
            content = content.replace("%ls_icon%", "<img src=\"icon.skill3808\" width=32 height=32>");
            content = content.replace("%ls_name%", "");
            content = content.replace("%ls_type%", "");
            content = content.replace("%ls_level%", "");
            content = content.replace("%ls_price%", "");
            content = content.replace("%item_select%", (iteminstance == null ? "" : "<img src=\"icon." + iteminstance.getItem().getIcon() + "\" width=32 height=32> &nbsp;&nbsp;" + iteminstance.getItem().getName() + " " + iteminstance.getItem().getAdditionalName() + " " + (iteminstance.getEnchantLevel() == 0 ? "" : "+" + iteminstance.getEnchantLevel())));
            content = content.replace("%desc%", "");
            content = content.replace("%pagelist%", page_list(numpages, page, "showskillslist"));
            content = content.replace("%skilllist%", html.toString());
        } else {
            final int charId = player.getSelectCommunitySkillLS();
            final int skill_id = _skillId.get(charId);
            final int skill_level = _skillLvl.get(charId);
            L2Skill skill = SkillTable.getInstance().getInfo(skill_id, skill_level);
            if (skill == null) 
            {
                _log.info("LSBBSManager: - skill[" + skill_id + "] is NULL! Character objID: " + player.getObjectId());
                return;
            }

            content = content.replace("%ls_icon%", "<img src=\"icon." + skill.getIcon() + "\" width=32 height=32>");
            content = content.replace("%ls_name%", skill.getName());
            content = content.replace("%ls_type%", _type.get(charId));
            content = content.replace("%ls_level%", "" + skill.getLevel());
            content = content.replace("%ls_price%", "" + _itemCount.get(charId));
            content = content.replace("%item_select%", (iteminstance == null ? "" : "<img src=\"icon." + iteminstance.getItem().getIcon() + "\" width=32 height=32> &nbsp;&nbsp;" + iteminstance.getItem().getName() + " " + iteminstance.getItem().getAdditionalName() + " " + (iteminstance.getEnchantLevel() == 0 ? "" : "+" + iteminstance.getEnchantLevel())));
            content = content.replace("%desc%", _desc.get(charId));
            content = content.replace("%pagelist%", page_list(numpages, page, "showskillslist"));
            content = content.replace("%skilllist%", html.toString());
        }

        ShowBoard.separateAndSend(player, content);
        StringUtil.recycle(html);
    }

    private String buildTable(final int page, L2Player player) {
        L2Skill skill;
        final StringBuilder res = StringUtil.startAppend(400, "<table width=600>");
        String userFilter = player.getCommunityFilterType();

        int c = 0;

        if (userFilter.equalsIgnoreCase("all") || userFilter.equalsIgnoreCase("active")) {
            if (page == 1)
                c = 1;
            else
                c = (((page - 1) * column * rows) + 1);
        } else if (userFilter.equalsIgnoreCase("passive")) {
            if (page == 1)
                c = _passive.get(0);
            else
                c = (((page - 1) * column * rows) + _passive.get(0));
        } else if (userFilter.equalsIgnoreCase("chance")) {
            if (page == 1)
                c = _chance.get(0);
            else
                c = (((page - 1) * column * rows) + _chance.get(0));
        } else if (userFilter.equalsIgnoreCase("jewerly")) {
            if (page == 1)
                c = _jewerly.get(0);
            else
                c = (((page - 1) * column * rows) + _jewerly.get(0));
        }

        for (int i = 0; i < rows; i++) {
            StringUtil.append(res, "<tr>");
            for (int j = 0; j < column; j++) {
                if (_id.size() >= c) {
                    final int skill_id = _skillId.get(c);
                    final int skill_level = _skillLvl.get(c);
                    final String type_ls = _type.get(c);

                    skill = SkillTable.getInstance().getInfo(skill_id, skill_level);
                    if (skill == null) {
                        _log.warning("LSBBSManager: buildTable - skill[" + skill_id + "] is NULL!");
                        c++;
                        continue;
                    }

                    if (userFilter.equalsIgnoreCase("all") || userFilter.equalsIgnoreCase(type_ls)) {
                        StringUtil.append(res, "<td width=55><center><img src=\"icon." + skill.getIcon() + "\" width=32 height=32><br><button value=\"Выбрать\" action=\"bypass _bbsls selectlsskill " + c + "\" width=55 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></td>");
                        StringUtil.append(res, "<td width=245>Название: " + skill.getName() + "<br> Тип: " + _type.get(c) + "<br> Уровень: " + skill.getLevel() + "<br></td>");
                    }
                } else
                    StringUtil.append(res, "<td width=150><center></center></td>");
                c++;
            }
            StringUtil.append(res, "</tr>");
        }

        StringUtil.append(res, "</table><br>");
        return res.toString();
    }

    private void showLSIndexPage(L2Player player) {
        int numpages = 0;

        if (player.getCommunityFilterType().equalsIgnoreCase("all"))
            numpages = getNumPages(_id.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("active"))
            numpages = getNumPages(_active.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("passive"))
            numpages = getNumPages(_passive.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("chance"))
            numpages = getNumPages(_chance.size());
        else if (player.getCommunityFilterType().equalsIgnoreCase("jewerly"))
            numpages = getNumPages(_jewerly.size());

        String content = Files.read(Config.BBS_HOME_DIR + "/ls/ls.htm", player);

        final L2ItemInstance iteminstance = player.getSelectCommunityItemLS();

        if (player.getSelectCommunitySkillLS() == 0) {
            content = content.replace("%ls_icon%", "<img src=\"icon.skill3808\" width=32 height=32>");
            content = content.replace("%ls_name%", "");
            content = content.replace("%ls_type%", "");
            content = content.replace("%ls_level%", "");
            content = content.replace("%ls_price%", "");
            content = content.replace("%item_select%", (iteminstance == null ? "" : "<img src=\"icon." + iteminstance.getItem().getIcon() + "\" width=32 height=32> &nbsp;&nbsp;" + iteminstance.getItem().getName() + " " + iteminstance.getItem().getAdditionalName() + " " + (iteminstance.getEnchantLevel() == 0 ? "" : "+" + iteminstance.getEnchantLevel())));
            content = content.replace("%desc%", "");
            content = content.replace("%pagelist%", page_list(numpages, 1, "showskillslist"));
            content = content.replace("%skilllist%", buildTable(1, player));
        } else {
            final int charId = player.getSelectCommunitySkillLS();
            final int skill_id = _skillId.get(charId);
            final int skill_level = _skillLvl.get(charId);
            L2Skill skill = SkillTable.getInstance().getInfo(skill_id, skill_level);
            if (skill == null) {
                _log.warning("LSBBSManager: - skill[" + skill_id + "] is NULL! Character objID: " + player.getObjectId());
                return;
            }

            content = content.replace("%ls_icon%", "<img src=\"icon." + skill.getIcon() + "\" width=32 height=32>");
            content = content.replace("%ls_name%", skill.getName());
            content = content.replace("%ls_type%", _type.get(charId));
            content = content.replace("%ls_level%", "" + skill.getLevel());
            content = content.replace("%ls_price%", "" + _itemCount.get(charId));
            content = content.replace("%item_select%", (iteminstance == null ? "" : "<img src=\"icon." + iteminstance.getItem().getIcon() + "\" width=32 height=32> &nbsp;&nbsp;" + iteminstance.getItem().getName() + " " + iteminstance.getItem().getAdditionalName() + " " + (iteminstance.getEnchantLevel() == 0 ? "" : "+" + iteminstance.getEnchantLevel())));
            content = content.replace("%desc%", _desc.get(charId));
            content = content.replace("%pagelist%", page_list(numpages, 1, "showskillslist"));
            content = content.replace("%skilllist%", buildTable(1, player));
        }

        ShowBoard.separateAndSend(player,content);
    }

    private String page_list(final double numpages, final int cur_page, final String bypass) {
        if (numpages == 1)
            return null;

        final StringBuilder navpage = StringUtil.startAppend(200, "");

        if (numpages <= MAXPAGES || cur_page < MAXPAGES) {
            for (int page = 1; page <= MAXPAGES; page++) {
                if (page == cur_page)
                    StringUtil.append(navpage, "<td align=center width=25>", page, "</td>");
                else
                    StringUtil.append(navpage, "<td align=rigt width=25><button value=\"", page, "\" action=\"bypass _bbsls ", bypass, " ", page, "\" width=25 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
            }
        } else if (cur_page >= MAXPAGES) {
            for (int page = (cur_page - ((MAXPAGES + 1) / 2)); page <= cur_page + 1; page++) {
                if (page <= numpages) {
                    if (page == cur_page)
                        StringUtil.append(navpage, "<td align=center width=25>", page, "</td>");
                    else
                        StringUtil.append(navpage, "<td align=rigt width=25><button value=\"", page, "\" action=\"bypass _bbsls ", bypass, " ", page, "\" width=25 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
                }
            }
        }

        return navpage.toString();
    }

    private static boolean doPay(final int id, final int count, final L2Player player){
        L2ItemInstance pay = player.getInventory().getItemByItemId(id);
        if(pay != null && pay.getCount() >= count)
        {
            player.getInventory().destroyItem(pay, count, true);
            return true;
        }    
        return false;
		
    }
    
    private static int getNumPages(final int size) {
        return (int) Math.ceil((double) size / (column * rows));
    }
    
    @Override
    public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player activeChar) {
        
    }
    
}
