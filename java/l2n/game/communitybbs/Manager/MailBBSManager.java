package l2n.game.communitybbs.Manager;

import javolution.text.TextBuilder;
import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExMailArrived;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Files;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

public class MailBBSManager extends AbstractBBSManager
{
	private static final Logger _log = Logger.getLogger(MailBBSManager.class.getName());

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final MailBBSManager _instance = new MailBBSManager();
	}

	public static MailBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private class UpdateMail
	{
		private int obj_id;
		private int letterId;
		private int senderId;
		private String location;
		private String recipientNames;
		private String subject;
		private String message;
		private String sentDateFormated;
		private long sendDate;
		private String deleteDateFormated;
		private long deleteDate;
		private String unread;
	}

	public GArray<UpdateMail> getMail(final L2Player activeChar)
	{
		final GArray<UpdateMail> _letters = new GArray<UpdateMail>();

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT * FROM character_mail WHERE obj_id = " + activeChar.getObjectId() + " ORDER BY letterId DESC");
			while (result.next())
			{
				final UpdateMail letter = new UpdateMail();
				letter.obj_id = result.getInt("obj_id");
				letter.letterId = result.getInt("letterId");
				letter.senderId = result.getInt("senderId");
				letter.location = result.getString("location");
				letter.recipientNames = result.getString("recipientNames");
				letter.subject = result.getString("subject");
				letter.message = result.getString("message");
				letter.sendDate = result.getLong("sendDate");
				letter.sentDateFormated = new SimpleDateFormat("yyyy-MM-dd").format(new Date(letter.sendDate));
				letter.deleteDate = result.getLong("deleteDate");
				letter.deleteDateFormated = new SimpleDateFormat("yyyy-MM-dd").format(new Date(letter.deleteDate));
				letter.unread = result.getString("unread");
				_letters.add(letter);
			}
		}
		catch(final Exception e)
		{
			_log.warning("couldnt load mail for " + activeChar.getName());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}
		return _letters;
	}

	private UpdateMail getLetter(final L2Player activeChar, final int letterId)
	{
		UpdateMail letter = new UpdateMail();
		for(final UpdateMail temp : getMail(activeChar))
		{
			letter = temp;
			if(letter.letterId == letterId)
				break;
		}
		return letter;
	}

	@Override
	public void parsecmd(final String command, final L2Player activeChar)
	{
		if(command.equals("_maillist_0_1_0_"))
			showInbox(activeChar, 1);
		else if(command.startsWith("_maillist_0_1_0_ "))
			showInbox(activeChar, Integer.parseInt(command.substring(17)));
		else if(command.equals("_maillist_0_1_0_sentbox"))
			showSentbox(activeChar, 1);
		else if(command.startsWith("_maillist_0_1_0_sentbox "))
			showSentbox(activeChar, Integer.parseInt(command.substring(24)));
		else if(command.equals("_maillist_0_1_0_archive"))
			showMailArchive(activeChar, 1);
		else if(command.startsWith("_maillist_0_1_0_archive "))
			showMailArchive(activeChar, Integer.parseInt(command.substring(24)));
		else if(command.equals("_maillist_0_1_0_temp_archive"))
			showTempMailArchive(activeChar, 1);
		else if(command.startsWith("_maillist_0_1_0_temp_archive "))
			showTempMailArchive(activeChar, Integer.parseInt(command.substring(29)));
		else if(command.equals("_maillist_0_1_0_write"))
			showWriteView(activeChar);
		else if(command.startsWith("_maillist_0_1_0_view "))
		{
			final UpdateMail letter = getLetter(activeChar, Integer.parseInt(command.substring(21)));
			showLetterView(activeChar, letter);
			if(!letter.unread.equals("false"))
				setLetterToRead(letter.letterId);
		}
		else if(command.startsWith("_maillist_0_1_0_reply "))
		{
			final UpdateMail letter = getLetter(activeChar, Integer.parseInt(command.substring(22)));
			showWriteView(activeChar, getCharName(letter.senderId), letter);
		}
		else if(command.startsWith("_maillist_0_1_0_delete "))
		{
			final UpdateMail letter = getLetter(activeChar, Integer.parseInt(command.substring(23)));
			/*
			 * if(Config.MAIL_STORE_DELETED_LETTERS)
			 * storeLetter(letter.letterId);
			 */
			deleteLetter(letter.letterId);
			showInbox(activeChar, 1);
		}
		else
			notImplementedYet(activeChar, command);
		// System.out.println("notImplementedYet2\n\r");
	}

	private String abbreviate(final String s, int maxWidth)
	{
		return s.length() > maxWidth ? s.substring(0, maxWidth--) + "..." : s;
	}

	private void showInbox(final L2Player activeChar, final int page)
	{
		int index = 0, minIndex = 0, maxIndex = 0;
		maxIndex = page == 1 ? page * 14 : page * 15 - 1;
		minIndex = maxIndex - 14;

		final TextBuilder html = TextBuilder.newInstance();
		html.append("<html>");
		html.append("<body><br><br>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810><tr><td width=10></td><td width=800 height=30 align=left>");
		html.append("<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_\">Входящие</a>");
		html.append("</td></tr>");
		html.append("</table>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810 bgcolor=808080>");
		html.append("<tr><td height=10></td></tr>");
		html.append("<tr>");
		html.append("<td fixWIDTH=5></td>");
		html.append("<td fixWIDTH=760>");
		html.append("<a action=\"bypass _maillist_0_1_0_\">[Входящие]</a>(").append(countLetters(activeChar, "inbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_sentbox\">[Сохраненные]</a>(").append(countLetters(activeChar, "sentbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_archive\">[Архив]</a>(").append(countLetters(activeChar, "archive")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_temp_archive\">[Временный Почтовый архив]</a>(").append(countLetters(activeChar, "temparchive")).append(")</td>");
		html.append("<td fixWIDTH=5></td>");
		html.append("</tr>");
		html.append("<tr><td height=10></td></tr>");
		html.append("</table>");
		if(countLetters(activeChar, "inbox") == 0)
			html.append("<br><center>Ваш почтовый ящик пуст.</center>");
		else
		{
			html.append("<br>");
			html.append("<table border=0 cellspacing=0 cellpadding=2 bgcolor=808080 width=770>");
			html.append("<tr>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("<td FIXWIDTH=150 align=center>Автор</td>");
			html.append("<td FIXWIDTH=460 align=left>Титут</td>");
			html.append("<td FIXWIDTH=150 align=center>Авторская Дата</td>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("</tr></table>");
			for(final UpdateMail letter : getMail(activeChar))
				if(activeChar.getObjectId() == letter.obj_id && letter.location.equals("inbox"))
				{
					if(index < minIndex)
					{
						index++;
						continue;
					}
					if(index > maxIndex)
						break;
					final String tempName = getCharName(letter.senderId);
					html.append("<table border=0 cellspacing=0 cellpadding=2 width=770>");
					html.append("<tr>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(abbreviate(tempName, 6)).append("</td>");
					html.append("<td FIXWIDTH=460 align=left><a action=\"bypass _maillist_0_1_0_view ").append(letter.letterId).append("\">").append(abbreviate(letter.subject, 51)).append("</a></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(letter.sentDateFormated).append("</td>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("</tr></table>");
					html.append("<img src=\"L2UI.SquareBlank\" width=\"770\" height=\"3\">");
					html.append("<img src=\"L2UI.SquareGrey\" width=\"770\" height=\"1\">");
					index++;
				}
		}
		html.append("<table width=770><tr>");
		html.append("<td align=right><button value=\"Запись\" action=\"bypass _maillist_0_1_0_write\" width=60 height=30 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table>");
		html.append("<center><table width=770><tr>");
		html.append("<td align=right><button action=\"bypass _maillist_0_1_0_ ").append(page == 1 ? page : page - 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_left_down\" fore=\"L2UI_ct1.button_df_left\"></td>");
		for(int i = 1; i <= 7; i++)
			html.append("<td align=center fixedwidth=10><a action=\"bypass _maillist_0_1_0_ ").append(i).append("\">").append(i).append("</a></td>");
		html.append("<td align=left><button action=\"bypass _maillist_0_1_0_ ").append(page + 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_right_down\" fore=\"L2UI_ct1.button_df_right\"></td>");
		html.append("</tr></table>");
		html.append("<table><tr>");
		html.append("<td align=right><combobox width=65 var=combo list=\"Writer\"></td>");
		html.append("<td align=center><edit var=\"Ключевое слово\" width=130 height=11 length=\"16\"></td>");
		html.append("<td align=left><button value=\"Поиск\" action=\"bypass _maillist_0_1_0_search $combo $keyword\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table></center>");
		html.append("</body></html>");
		separateAndSend(html, activeChar);
	}

	private void showLetterView(final L2Player activeChar, final UpdateMail letter)
	{
		final TextBuilder html = TextBuilder.newInstance();
		html.append("<html>");
		html.append("<body><br><br>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=770><tr><td width=10></td><td height=30 width=760 align=left>");
		html.append("<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_\">Входящие</a>");
		html.append("</td></tr>");
		html.append("</table>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=770 bgcolor=808080>");
		html.append("<tr><td height=10></td></tr>");
		html.append("<tr>");
		html.append("<td FIXWIDTH=5 height=20></td>");
		html.append("<td FIXWIDTH=100 height=20 align=right>Отправитель:&nbsp;</td>");
		html.append("<td FIXWIDTH=360 height=20 align=left>").append(getCharName(letter.senderId)).append("</td>");//
		html.append("<td FIXWIDTH=150 height=20 align=right>Время Отправки:&nbsp;</td>");
		html.append("<td FIXWIDTH=150 height=20 align=left>").append(letter.sentDateFormated).append("</td>");//
		html.append("<td fixWIDTH=5 height=20></td>");
		html.append("</tr><tr>");
		html.append("<td FIXWIDTH=5 height=20></td>");
		html.append("<td FIXWIDTH=100 height=20 align=right>Получатель:&nbsp;</td>");
		html.append("<td FIXWIDTH=360 height=20 align=left>").append(letter.recipientNames).append("</td>");//
		html.append("<td FIXWIDTH=150 height=20 align=right>Удалить в заданное время:&nbsp;</td>");
		html.append("<td FIXWIDTH=150 height=20 align=left>").append(letter.deleteDateFormated).append("</td>");//
		html.append("<td fixWIDTH=5 height=20></td>");
		html.append("</tr><tr>");
		html.append("<td FIXWIDTH=5 height=20></td>");
		html.append("<td FIXWIDTH=100 height=20 align=right>Титул:&nbsp;</td>");
		html.append("<td FIXWIDTH=360 height=20 align=left>").append(letter.subject).append("</td>");//
		html.append("<td FIXWIDTH=150 height=20></td>");
		html.append("<td FIXWIDTH=150 height=20></td>");
		html.append("<td fixWIDTH=5 height=20></td>");
		html.append("</tr>");
		html.append("<tr><td height=10></td></tr>");
		html.append("</table>");
		html.append("<table width=770><tr>");
		html.append("<td height=10></td>");
		html.append("<td height=10></td>");
		html.append("<td height=10></td>");
		html.append("</tr><tr>");
		html.append("<td FIXWIDTH=100></td>");
		html.append("<td FIXWIDTH=560>").append(letter.message).append("</td>");
		html.append("<td FIXWIDTH=100></td>");
		html.append("</tr></table>");
		html.append("<img src=\"L2UI.SquareBlank\" width=\"770\" height=\"3\">");
		html.append("<img src=\"L2UI.SquareGrey\" width=\"770\" height=\"1\">");
		html.append("<table width=770><tr>");
		html.append("<td align=left><button value=\"Показать список\" action=\"bypass _maillist_0_1_0_\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("<td FIXWIDTH=300></td>");
		html.append("<td align=right><button value=\"Ответ\" action=\"bypass _maillist_0_1_0_reply ").append(letter.letterId).append("\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("<td align=right><button value=\"Доставленные\" action=\"bypass _maillist_0_1_0_deliver\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("<td align=right><button value=\"Удалить\" action=\"bypass _maillist_0_1_0_delete ").append(letter.letterId).append("\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("<td align=right><button value=\"Магазин\" action=\"bypass _maillist_0_1_0_store ").append(letter.letterId).append("\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("<td align=right><button value=\"Почта написание\" action=\"bypass _maillist_0_1_0_write\" width=70 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table>");
		html.append("</body></html>");
		separateAndSend(html, activeChar);
	}

	private void showSentbox(final L2Player activeChar, final int page)
	{
		int index = 0, minIndex = 0, maxIndex = 0;
		maxIndex = page == 1 ? page * 14 : page * 15 - 1;
		minIndex = maxIndex - 14;

		final TextBuilder html = TextBuilder.newInstance();
		html.append("<html>");
		html.append("<body><br><br>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810><tr><td width=10></td><td width=800 height=30 align=left>");
		html.append("<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_sentbox\">Сохраненные</a>");
		html.append("</td></tr>");
		html.append("</table>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810 bgcolor=808080>");
		html.append("<tr><td height=10></td></tr>");
		html.append("<tr>");
		html.append("<td fixWIDTH=5></td>");
		html.append("<td fixWIDTH=760>");
		html.append("<a action=\"bypass _maillist_0_1_0_\">[Входящие]</a>(").append(countLetters(activeChar, "inbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_sentbox\">[Сохраненные]</a>(").append(countLetters(activeChar, "sentbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_archive\">[Архив]</a>(").append(countLetters(activeChar, "archive")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_temp_archive\">[Временные Архив]</a>(").append(countLetters(activeChar, "temparchive")).append(")</td>");
		html.append("<td fixWIDTH=5></td>");
		html.append("</tr>");
		html.append("<tr><td height=10></td></tr>");
		html.append("</table>");
		if(countLetters(activeChar, "sentbox") == 0)
			html.append("<br><center>У вас нет отправленных.</center>");
		else
		{
			html.append("<br>");
			html.append("<table border=0 cellspacing=0 cellpadding=2 bgcolor=808080 width=770>");
			html.append("<tr>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("<td FIXWIDTH=150 align=center>Автор</td>");
			html.append("<td FIXWIDTH=460 align=left>Титул</td>");
			html.append("<td FIXWIDTH=150 align=center>Авторская Дата</td>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("</tr></table>");
			for(final UpdateMail letter : getMail(activeChar))
				if(activeChar.getObjectId() == letter.obj_id && letter.location.equals("sentbox"))
				{
					if(index < minIndex)
					{
						index++;
						continue;
					}
					if(index > maxIndex)
						break;
					final String tempName = getCharName(letter.senderId);
					html.append("<table border=0 cellspacing=0 cellpadding=2 width=770>");
					html.append("<tr>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(abbreviate(tempName, 6)).append("</td>");
					html.append("<td FIXWIDTH=460 align=left><a action=\"bypass _maillist_0_1_0_view ").append(letter.letterId).append("\">").append(abbreviate(letter.subject, 51)).append("</a></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(letter.sentDateFormated).append("</td>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("</tr></table>");
					html.append("<img src=\"L2UI.SquareBlank\" width=\"770\" height=\"3\">");
					html.append("<img src=\"L2UI.SquareGrey\" width=\"770\" height=\"1\">");
					index++;
				}
		}
		html.append("<table width=770><tr>");
		html.append("<td align=right><button value=\"Запись\" action=\"bypass _maillist_0_1_0_write\" width=60 height=30 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table>");
		html.append("<center><table width=770><tr>");
		html.append("<td align=right><button action=\"bypass _maillist_0_1_0_sentbox ").append(page == 1 ? page : page - 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_left_down\" fore=\"L2UI_ct1.button_df_left\"></td>");
		for(int i = 1; i <= 7; i++)
			html.append("<td align=center fixedwidth=10><a action=\"bypass _maillist_0_1_0_sentbox ").append(i).append("\">").append(i).append("</a></td>");
		html.append("<td align=left><button action=\"bypass _maillist_0_1_0_sentbox ").append(page + 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_right_down\" fore=\"L2UI_ct1.button_df_right\"></td>");
		html.append("</tr></table>");
		html.append("<table><tr>");
		html.append("<td align=right><combobox width=65 var=combo list=\"Writer\"></td>");
		html.append("<td align=center><edit var=\"Ключевое слово\" width=130 height=11 length=\"16\"></td>");
		html.append("<td align=left><button value=\"Поиск\" action=\"bypass _maillist_0_1_0_search $combo $keyword\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table></center>");
		html.append("</body></html>");
		separateAndSend(html, activeChar);
	}

	private void showMailArchive(final L2Player activeChar, final int page)
	{
		int index = 0, minIndex = 0, maxIndex = 0;
		maxIndex = page == 1 ? page * 14 : page * 15 - 1;
		minIndex = maxIndex - 14;

		final TextBuilder html = TextBuilder.newInstance();
		html.append("<html>");
		html.append("<body><br><br>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810><tr><td width=10></td><td width=800 height=30 align=left>");
		html.append("<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_archive\">Архив</a>");
		html.append("</td></tr>");
		html.append("</table>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810 bgcolor=808080>");
		html.append("<tr><td height=10></td></tr>");
		html.append("<tr>");
		html.append("<td fixWIDTH=5></td>");
		html.append("<td fixWIDTH=760>");
		html.append("<a action=\"bypass _maillist_0_1_0_\">[Входящие]</a>(").append(countLetters(activeChar, "inbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_sentbox\">[Архив]</a>(").append(countLetters(activeChar, "sentbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_archive\">[Архив]</a>(").append(countLetters(activeChar, "archive")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_temp_archive\">[Временный архив]</a>(").append(countLetters(activeChar, "temparchive")).append(")</td>");
		html.append("<td fixWIDTH=5></td>");
		html.append("</tr>");
		html.append("<tr><td height=10></td></tr>");
		html.append("</table>");
		if(countLetters(activeChar, "archive") == 0)
			html.append("<br><center>Ваша почтовый архив пуст.</center>");
		else
		{
			html.append("<br>");
			html.append("<table border=0 cellspacing=0 cellpadding=2 bgcolor=808080 width=770>");
			html.append("<tr>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("<td FIXWIDTH=150 align=center>Автор</td>");
			html.append("<td FIXWIDTH=460 align=left>Титул</td>");
			html.append("<td FIXWIDTH=150 align=center>Авторская Дата</td>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("</tr></table>");
			for(final UpdateMail letter : getMail(activeChar))
				if(activeChar.getObjectId() == letter.obj_id && letter.location.equals("archive"))
				{
					if(index < minIndex)
					{
						index++;
						continue;
					}
					if(index > maxIndex)
						break;
					final String tempName = getCharName(letter.senderId);
					html.append("<table border=0 cellspacing=0 cellpadding=2 width=770>");
					html.append("<tr>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(abbreviate(tempName, 6)).append("</td>");
					html.append("<td FIXWIDTH=460 align=left><a action=\"bypass _maillist_0_1_0_view ").append(letter.letterId).append("\">").append(abbreviate(letter.subject, 51)).append("</a></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(letter.sentDateFormated).append("</td>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("</tr></table>");
					html.append("<img src=\"L2UI.SquareBlank\" width=\"770\" height=\"3\">");
					html.append("<img src=\"L2UI.SquareGrey\" width=\"770\" height=\"1\">");
					index++;
				}
		}
		html.append("<table width=770><tr>");
		html.append("<td align=right><button value=\"Запись\" action=\"bypass _maillist_0_1_0_write\" width=60 height=30 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table>");
		html.append("<center><table width=770><tr>");
		html.append("<td align=right><button action=\"bypass _maillist_0_1_0_archive ").append(page == 1 ? page : page - 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_left_down\" fore=\"L2UI_ct1.button_df_left\"></td>");
		for(int i = 1; i <= 7; i++)
			html.append("<td align=center fixedwidth=10><a action=\"bypass _maillist_0_1_0_archive ").append(i).append("\">").append(i).append("</a></td>");
		html.append("<td align=left><button action=\"bypass _maillist_0_1_0_archive ").append(page + 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_right_down\" fore=\"L2UI_ct1.button_df_right\"></td>");
		html.append("</tr></table>");
		html.append("<table><tr>");
		html.append("<td align=right><combobox width=65 var=combo list=\"Writer\"></td>");
		html.append("<td align=center><edit var=\"Ключевое слово\" width=130 height=11 length=\"16\"></td>");
		html.append("<td align=left><button value=\"Поиск\" action=\"bypass _maillist_0_1_0_search $combo $keyword\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table></center>");
		html.append("</body></html>");
		separateAndSend(html, activeChar);
	}

	private void showTempMailArchive(final L2Player activeChar, final int page)
	{
		int index = 0, minIndex = 0, maxIndex = 0;
		maxIndex = page == 1 ? page * 14 : page * 15 - 1;
		minIndex = maxIndex - 14;

		final TextBuilder html = TextBuilder.newInstance();
		html.append("<html>");
		html.append("<body><br><br>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810><tr><td width=10></td><td width=800 height=30 align=left>");
		html.append("<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_temp_archive\">Временный архив</a>");
		html.append("</td></tr>");
		html.append("</table>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=810 bgcolor=808080>");
		html.append("<tr><td height=10></td></tr>");
		html.append("<tr>");
		html.append("<td fixWIDTH=5></td>");
		html.append("<td fixWIDTH=760>");
		html.append("<a action=\"bypass _maillist_0_1_0_\">[Входящие]</a>(").append(countLetters(activeChar, "inbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_sentbox\">[Архив]</a>(").append(countLetters(activeChar, "sentbox")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_archive\">[Архив]</a>(").append(countLetters(activeChar, "archive")).append(")&nbsp;").append("<a action=\"bypass _maillist_0_1_0_temp_archive\">[Временный архив]</a>(").append(countLetters(activeChar, "temparchive")).append(")</td>");
		html.append("<td fixWIDTH=5></td>");
		html.append("</tr>");
		html.append("<tr><td height=10></td></tr>");
		html.append("</table>");
		if(countLetters(activeChar, "temparchive") == 0)
			html.append("<br><center>Ваш архив временной почты пуст.</center>");
		else
		{
			html.append("<br>");
			html.append("<table border=0 cellspacing=0 cellpadding=2 bgcolor=808080 width=770>");
			html.append("<tr>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("<td FIXWIDTH=150 align=center>Автор</td>");
			html.append("<td FIXWIDTH=460 align=left>Титул</td>");
			html.append("<td FIXWIDTH=150 align=center>Авторская Дата</td>");
			html.append("<td FIXWIDTH=5></td>");
			html.append("</tr></table>");
			for(final UpdateMail letter : getMail(activeChar))
				if(activeChar.getObjectId() == letter.obj_id && letter.location.equals("temparchive"))
				{
					if(index < minIndex)
					{
						index++;
						continue;
					}
					if(index > maxIndex)
						break;
					final String tempName = getCharName(letter.senderId);
					html.append("<table border=0 cellspacing=0 cellpadding=2 width=770>");
					html.append("<tr>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(abbreviate(tempName, 6)).append("</td>");
					html.append("<td FIXWIDTH=460 align=left><a action=\"bypass _maillist_0_1_0_view ").append(letter.letterId).append("\">").append(abbreviate(letter.subject, 51)).append("</a></td>");
					html.append("<td FIXWIDTH=150 align=center>").append(letter.sentDateFormated).append("</td>");
					html.append("<td FIXWIDTH=5></td>");
					html.append("</tr></table>");
					html.append("<img src=\"L2UI.SquareBlank\" width=\"770\" height=\"3\">");
					html.append("<img src=\"L2UI.SquareGrey\" width=\"770\" height=\"1\">");
					index++;
				}
		}
		html.append("<table width=770><tr>");
		html.append("<td align=right><button value=\"Запись\" action=\"bypass _maillist_0_1_0_write\" width=60 height=30 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table>");
		html.append("<center><table width=770><tr>");
		html.append("<td align=right><button action=\"bypass _maillist_0_1_0_temp_archive ").append(page == 1 ? page : page - 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_left_down\" fore=\"L2UI_ct1.button_df_left\"></td>");
		for(int i = 1; i <= 7; i++)
			html.append("<td align=center fixedwidth=10><a action=\"bypass _maillist_0_1_0_temp_archive ").append(i).append("\">").append(i).append("</a></td>");
		html.append("<td align=left><button action=\"bypass _maillist_0_1_0_temp_archive ").append(page + 1).append("\" width=16 height=16 back=\"L2UI_ct1.button_df_right_down\" fore=\"L2UI_ct1.button_df_right\"></td>");
		html.append("</tr></table>");
		html.append("<table><tr>");
		html.append("<td align=right><combobox width=65 var=combo list=\"Writer\"></td>");
		html.append("<td align=center><edit var=\"Ключевые слова\" width=130 height=11 length=\"16\"></td>");
		html.append("<td align=left><button value=\"Поиск\" action=\"bypass _maillist_0_1_0_search $combo $keyword\" width=60 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td>");
		html.append("</tr></table></center>");
		html.append("</body></html>");
		separateAndSend(html, activeChar);
	}

	private void showWriteView(final L2Player activeChar)
	{
		String content = Files.read("data/html/CommunityBoard/tamplate/bbs_mail_write.htm", activeChar);
		content = content.replace("%TREE%", "<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_\">Входящие</a>");
		content = content.replace("%SEND_ACTION%", "Mail Send _");
		content = content.replace("%DELETE_ACTION%", "bypass _maillist_0_1_0_delete 0");
		separateAndSend(content, activeChar);
	}

	private void showWriteView(final L2Player activeChar, final String parcipientName, final UpdateMail letter)
	{
		String content = Files.read("data/html/CommunityBoard/tamplate/bbs_writeform.htm", activeChar);
		content = content.replace("%TREE%", "<a action=\"bypass _bbshome\">Главная</a>&nbsp;&gt;&nbsp;<a action=\"bypass _maillist_0_1_0_\">Входящие</a>");
		content = content.replace("%LIST_NAME%", "Recipient");
		content = content.replace("%LIST%", parcipientName);
		content = content.replace("%SEND_ACTION%", "Write Mail Send _ Recipient Title Content");
		content = content.replace("%CANCEL_ACTION%", "bypass _maillist_0_1_0_");
		content = content.replace("%DELETE_ACTION%", "bypass _maillist_0_1_0_delete 0");

		send1001(content, activeChar);
		send1002(activeChar, " ", "Re: " + letter.subject, "0");
	}

	private void sendLetter(final String recipients, String subject, String message, final L2Player activeChar)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		FiltredPreparedStatement statement2 = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			final String[] recipAr = recipients.split(";");
			message = Strings.edtiPlayerTxT(message);
			boolean sent = false;
			final long date = Calendar.getInstance().getTimeInMillis();
			int countRecips = 0;
			int countTodaysLetters = 0;

			if(subject.isEmpty())
				subject = "(no subject)";
			else
				subject = Strings.edtiPlayerTxT(subject);

			for(final UpdateMail letter : getMail(activeChar))
				if(date < letter.sendDate + Long.valueOf("86400000") && letter.location.equals("sentbox"))
					countTodaysLetters++;

			if(countTodaysLetters >= 10)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.NO_MORE_MESSAGES_MAY_BE_SENT_AT_THIS_TIME_EACH_ACCOUNT_IS_ALLOWED_10_MESSAGES_PER_DAY));
				return;
			}

			for(final String recipient : recipAr)
			{
				final int recipId = getobj_id(recipient.trim());
				if(recipId == 0)
					activeChar.sendMessage("Could not find " + recipient.trim() + ", Therefor will not get mail.");
				else if(isGM(recipId) && !activeChar.isGM())
					activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_SEND_MAIL_TO_A_GM_SUCH_AS_S1));
				else if(isBlocked(activeChar, recipId) && !activeChar.isGM())
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.S1_HAS_BLOCKED_YOU_YOU_CANNOT_SEND_MAIL_TO_S1_);

					final L2Player player = L2ObjectsStorage.getPlayer(recipId);
					if(player.isOnline() && player != null)
						sm.addName(player);
					activeChar.sendPacket(sm);
				}
				else if(isRecipInboxFull(recipId) && !activeChar.isGM())
				{
					activeChar.sendMessage(recipient.trim() + "'s inbox is full.");
					activeChar.sendPacket(new SystemMessage(SystemMessage.THE_MESSAGE_WAS_NOT_SENT));

					final L2Player player = L2ObjectsStorage.getPlayer(recipId);
					if(player.isOnline() && player != null)
						player.sendPacket(new SystemMessage(SystemMessage.MAILBOX_IS_FULL100_MESSAGE_MAXIMUM));
				}
				else if(countRecips < 5 && !activeChar.isGM() || activeChar.isGM())
				{
					statement = con.prepareStatement("INSERT INTO character_mail (obj_id, senderId, location, recipientNames, subject, message, sendDate, deleteDate, unread) VALUES (?,?,?,?,?,?,?,?,?)");
					statement.setInt(1, recipId);
					statement.setInt(2, activeChar.getObjectId());
					statement.setString(3, "inbox");
					statement.setString(4, recipients);
					statement.setString(5, subject);
					statement.setString(6, message);
					statement.setLong(7, date);
					statement.setLong(8, date + Long.valueOf("7948804000"));
					statement.setString(9, "true");
					statement.execute();
					DbUtils.close(statement);
					sent = true;
					countRecips++;

					for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
						if(player.getObjectId() == recipId && player.isOnline())
						{
							player.sendPacket(new SystemMessage(SystemMessage.YOUVE_GOT_MAIL));
							player.sendPacket(ExMailArrived.STATIC_PACKET);
						}

				}
			}
			// Create a copy into activeChar's sent box
			statement2 = con.prepareStatement("INSERT INTO character_mail (obj_id, senderId, location, recipientNames, subject, message, sendDate, deleteDate, unread) VALUES (?,?,?,?,?,?,?,?,?)");
			statement2.setInt(1, activeChar.getObjectId());
			statement2.setInt(2, activeChar.getObjectId());
			statement2.setString(3, "sentbox");
			statement2.setString(4, recipients);
			statement2.setString(5, subject);
			statement2.setString(6, message);
			statement2.setLong(7, date);
			statement2.setLong(8, date + Long.valueOf("7948804000"));
			statement2.setString(9, "false");
			statement2.execute();
			DbUtils.close(statement2);

			if(countRecips > 5 && !activeChar.isGM())
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ARE_LIMITED_TO_FIVE_RECIPIENTS_AT_A_TIME));

			if(sent)
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOUVE_SENT_MAIL));
		}
		catch(final Exception e)
		{
			_log.warning("couldnt send letter for " + activeChar.getName());
			e.printStackTrace();
		}
		finally
		{
			DbUtils.close(con);
		}
	}

	private int countLetters(final L2Player activeChar, final String location)
	{
		int count = 0;
		for(final UpdateMail letter : getMail(activeChar))
			if(activeChar.getObjectId() == letter.obj_id && letter.location.equals(location))
				count++;
		return count;
	}

	private boolean isBlocked(final L2Player activeChar, final int recipId)
	{
		final L2Player player = L2ObjectsStorage.getPlayer(recipId);
		if(player != null)
			if(player.isBlockAll() || player.getBlockList().contains(activeChar))
				return true;
		return false;
	}

	public void storeLetter(final int letterId)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		FiltredPreparedStatement statement2 = null;
		ResultSet result = null;
		try
		{
			int ownerId, senderId;
			long date;
			String recipientNames, subject, message;

			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT obj_id, senderId, recipientNames, subject, message, sendDate  FROM character_mail WHERE letterId = ?");
			statement.setInt(1, letterId);
			result = statement.executeQuery();
			result.next();
			ownerId = result.getInt("obj_id");
			senderId = result.getInt("senderId");
			recipientNames = result.getString("recipientNames");
			subject = result.getString("subject");
			message = result.getString("message");
			date = result.getLong("sendDate");

			statement2 = con.prepareStatement("INSERT INTO character_mail_deleted (ownerId, letterId, senderId, recipientNames, subject, message, date) VALUES (?,?,?,?,?,?,?)");
			statement2.setInt(1, ownerId);
			statement2.setInt(2, letterId);
			statement2.setInt(3, senderId);
			statement2.setString(4, recipientNames);
			statement2.setString(5, subject);
			statement2.setString(6, message);
			statement2.setLong(7, date);
			statement2.execute();
		}
		catch(final Exception e)
		{
			_log.warning("couldnt store letter " + letterId);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
			DbUtils.close(statement2);
		}
	}

	public void deleteLetter(final int letterId)
	{
		// mysql.set("DELETE FROM character_mail WHERE letterId = " + letterId);

		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("DELETE FROM character_mail WHERE letterId = " + letterId);
		}
		catch(final Exception e)
		{
			_log.warning("couldnt delete letter " + letterId);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void setLetterToRead(final int letterId)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate("UPDATE character_mail SET unread = false WHERE letterId = " + letterId);
		}
		catch(final Exception e)
		{
			_log.warning("couldnt set unread to false for " + letterId);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private String getCharName(final int obj_id)
	{
		final L2Player player = L2ObjectsStorage.getPlayer(obj_id);
		return player == null ? "No Name" : player.getName();
	}

	private int getobj_id(final String charName)
	{
		final L2Player player = L2ObjectsStorage.getPlayer(charName);
		return player == null ? 0 : player.getObjectId();
	}

	private boolean isGM(final int obj_id)
	{
		boolean isGM = false;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT accesslevel FROM characters WHERE obj_id = " + obj_id);
			result.next();
			isGM = result.getInt(1) > 0;
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}
		return isGM;
	}

	private boolean isRecipInboxFull(final int obj_id)
	{
		boolean isFull = false;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT COUNT(*) FROM character_mail WHERE obj_id = " + obj_id + " AND location = inbox");
			result.next();
			isFull = result.getInt(1) >= 100;
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}
		return isFull;
	}

	/** FIXME is there a better way? */
	public boolean hasUnreadMail(final L2Player activeChar)
	{
		boolean hasUnreadMail = false;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet result = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			result = statement.executeQuery("SELECT COUNT(*) FROM character_mail WHERE obj_id = " + activeChar.getObjectId() + " AND location = inbox AND unread = true");
			result.next();
			hasUnreadMail = result.getInt(1) > 0;
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, result);
		}
		return hasUnreadMail;
	}

	@Override
	public void parsewrite(final String ar1, final String ar2, final String ar3, final String ar4, final String ar5, final L2Player activeChar)
	{
		if(ar1.equals("Send"))
		{
			sendLetter(ar3, ar4, ar5, activeChar);
			showSentbox(activeChar, 0);
		}
	}
}
