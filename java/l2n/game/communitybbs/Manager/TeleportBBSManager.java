package l2n.game.communitybbs.Manager;

import javolution.text.TextBuilder;
import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.TownManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.ZoneInfo;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.residence.Castle;
import l2n.game.network.serverpackets.ShowBoard;
import l2n.util.Files;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.logging.Logger;

public class TeleportBBSManager extends AbstractBBSManager
{
	private final static Logger _log = Logger.getLogger(TeleportBBSManager.class.getName());

	public static final class CBteleport
	{
		public int TpId = 0; // Teport loc ID
		public String TpName = ""; // Loc name
		public int PlayerId = 0; // charID
		public int xC = 0; // Location coords
		public int yC = 0; //
		public int zC = 0; //
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TeleportBBSManager _instance = new TeleportBBSManager();
	}

	public static TeleportBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	public String points[][];

	@Override
	public void parsecmd(final String command, final L2Player player)
	{
		if(command.equals("_bbsteleport;"))
			showTp(player);
		else if(command.startsWith("_bbsteleport;delete;"))
		{
			final StringTokenizer stDell = new StringTokenizer(command, ";");
			stDell.nextToken();
			stDell.nextToken();
			final int TpNameDell = Integer.parseInt(stDell.nextToken());
			delTp(player, TpNameDell);
			showTp(player);
		}
		else if(command.startsWith("_bbsteleport;save;"))
		{
			final StringTokenizer stAdd = new StringTokenizer(command, ";");
			stAdd.nextToken();
			stAdd.nextToken();

			String TpNameAdd = null;
			try
			{
				TpNameAdd = stAdd.nextToken();
			}
			catch(final Exception e)
			{
				showTp(player);
				return;
			}

			AddTp(player, TpNameAdd.trim());
			showTp(player);
		}
		else if(command.startsWith("_bbsteleport;teleport;"))
		{
			final StringTokenizer stGoTp = new StringTokenizer(command, " ");
			stGoTp.nextToken();
			final int xTp = Integer.parseInt(stGoTp.nextToken());
			final int yTp = Integer.parseInt(stGoTp.nextToken());
			final int zTp = Integer.parseInt(stGoTp.nextToken());
			final int priceTp = Integer.parseInt(stGoTp.nextToken());
			goTp(player, xTp, yTp, zTp, priceTp);
			showTp(player);
		}
		else if(command.startsWith("_bbsteleport;player_teleport;"))
		{
			final StringTokenizer stGoTp = new StringTokenizer(command, " ");
			stGoTp.nextToken();
			final int xTp = Integer.parseInt(stGoTp.nextToken());
			final int yTp = Integer.parseInt(stGoTp.nextToken());
			final int zTp = Integer.parseInt(stGoTp.nextToken());
			goTp(player, xTp, yTp, zTp, -1);
			showTp(player);
		}
		else
			ShowBoard.notImplementedYet(player, command);
	}

	private void goTp(final L2Player player, final int xTp, final int yTp, final int zTp, final int priceTp)
	{
		if(player.isDead() || player.isAlikeDead() || player.isFlying() || player.isTerritoryFlagEquipped() || player.isInZone(ZoneType.no_escape) || player.inObserverMode())
		{
			player.sendMessage("Телепортация невозможна");
			return;
		}

		if(!Config.COMMUNITY_BOARD_TELEPORT_FIGHT)
			if(player.isInCombat() || player.isAttackingNow() || player.isCastingNow())
			{
				player.sendMessage("Телепортация невозможна");
				return;
			}

		// На оли тп нельзя) после регистрации тоже
		if(player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player) || player.getTeam() != 0)
			{
				player.sendMessage("Телепортация невозможна");
				return;
			}

			// Можно ли использовать в отражениях
		if(!Config.COMMUNITY_BOARD_TELEPORT_ALLOW_INSTANCE) 
		if (player.getReflectionId() > 0)
			{
				player.sendMessage("Телепортация невозможна");
				return;
			}
			
		if(Config.COMMUNITY_BOARD_TELEPORT_ALLOW_PK && player.getKarma() > 0)
						{
				player.sendMessage("Телепортация невозможна");
				return;
			}
			
		if(!Config.COMMUNITY_BOARD_TELEPORT_NO_IS_IN_PEACE && !player.isInPeaceZone())
		{
				player.sendMessage("Эта функция доступна только в Городе");
				return;
		}

		if(priceTp > 0 && player.getAdena() < priceTp)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		// проверяем пользовательские телепорты
		if(priceTp == -1)
		{
			if(Functions.getItemCount(player, Config.COMMUNITY_BOARD_TELEPORT_ITEM) < Config.COMMUNITY_BOARD_TELEPORT_PRICE)
			{
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
				return;
			}
		}

		// Можно ли телепортироваться в города, где идет осада
		if(!Config.COMMUNITY_BOARD_TELEPORT_IN_SIEGE_TOWN)
		{
			// Нельзя телепортироваться в города, где идет осада
			// изнаем, идет ли осада в ближайшем замке к точке телепортации
			final Castle castle = TownManager.getInstance().getClosestTown(xTp, yTp).getCastle();
			if(castle != null && castle.getSiege().isInProgress())
			{
				player.sendPacket(Msg.YOU_CANNOT_TELEPORT_TO_A_VILLAGE_THAT_IS_IN_A_SIEGE);
				return;
			}
		}
		if(!Config.COMMUNITY_BOARD_TELEPORT_IN_SIEGE_ZONE)
			if(isOnSiegeField() || isInZoneBattle() || isInZone(ZoneType.Siege))
		{
				player.sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_A_LARGE_SCALE_BATTLE_SUCH_AS_A_CASTLE_SIEGE);
				return;
			}
			else if(isOnSiegeField() || isInZoneBattle() || isInZone(ZoneType.Siege))
			{
				player.sendPacket(Msg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_A_LARGE_SCALE_BATTLE_SUCH_AS_A_CASTLE_SIEGE);
				return;
			}
			
		// Можно ли телепортироваться в зону, где идет осада
		if(!Config.COMMUNITY_BOARD_TELEPORT_IN_SIEGE_ZONE)
			if(ZoneManager.getInstance().checkIfInZone(ZoneType.Siege, xTp, yTp))
			{
				player.sendPacket(Msg.YOU_CANNOT_TELEPORT_TO_A_VILLAGE_THAT_IS_IN_A_SIEGE);
				return;
			}
			else if(ZoneManager.getInstance().checkIfInZone(ZoneType.siege_residense, xTp, yTp))
			{
				player.sendPacket(Msg.YOU_CANNOT_TELEPORT_TO_A_VILLAGE_THAT_IS_IN_A_SIEGE);
				return;
			}

		if(priceTp > 0)
			player.reduceAdena(priceTp, false);
		else if(priceTp == -1)
			Functions.removeItem(player, Config.COMMUNITY_BOARD_TELEPORT_ITEM, Config.COMMUNITY_BOARD_TELEPORT_PRICE);

		player.teleToLocation(xTp, yTp, zTp);
	}

	private boolean isInZone(ZoneType siege) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean isInZoneBattle() {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean isOnSiegeField() {
		// TODO Auto-generated method stub
		return false;
	}

	private void showTp(final L2Player player)
	{
		CBteleport tp;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM community_board_teleport WHERE charId=?;");
			statement.setLong(1, player.getObjectId());
			rs = statement.executeQuery();
			final TextBuilder html = new TextBuilder();
			html.append("<table width=220>");
			while (rs.next())
			{
				tp = new CBteleport();
				tp.TpId = rs.getInt("TpId");
				tp.TpName = rs.getString("name");
				tp.PlayerId = rs.getInt("charId");
				tp.xC = rs.getInt("xPos");
				tp.yC = rs.getInt("yPos");
				tp.zC = rs.getInt("zPos");
				html.append("<tr>");
				html.append("<td>");
				html.append("<button value=\"" + tp.TpName + "\" action=\"bypass -h _bbsteleport;player_teleport; " + tp.xC + " " + tp.yC + " " + tp.zC + "\" width=100 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				html.append("</td>");
				html.append("<td>");
				html.append("<button value=\"Удалить\" action=\"bypass -h _bbsteleport;delete;" + tp.TpId + "\" width=100 height=20 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
				html.append("</td>");
				html.append("</tr>");
			}
			html.append("</table>");

			String content = Files.read(Config.BBS_HOME_DIR +"/50.htm", player);
			content = content.replace("%tp%", html.toString());
			separateAndSend(content, player);
		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

	}

	private void delTp(final L2Player player, final int TpNameDell)
	{
		ThreadConnection conDel = null;
		FiltredPreparedStatement statementDel = null;
		try
		{
			conDel = L2DatabaseFactory.getInstance().getConnection();
			statementDel = conDel.prepareStatement("DELETE FROM community_board_teleport WHERE charId=? AND TpId=?;");
			statementDel.setInt(1, player.getObjectId());
			statementDel.setInt(2, TpNameDell);
			statementDel.execute();
		}
		catch(final Exception e)
		{
			_log.warning("data error on Delete Teleport: " + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(conDel);
		}

	}

	private void AddTp(final L2Player player, final String TpNameAdd)
	{
		if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isAttackingNow() || player.inObserverMode())
		{
			player.sendMessage("Сохранить закладку в вашем состоянии нельзя");
			return;
		}

		if(player.isInCombat())
		{
			player.sendMessage("Сохранить закладку в режиме боя нельзя");
			return;
		}

		if(Config.COMMUNITY_BOARD_TELEPORT_ADDITIONAL_RULES)
			if(player.isInZone(ZoneInfo.ZONE_NO_SAVE_BOOKMARK))
			{
				player.sendMessage("Сохранить закладку в вашем состоянии нельзя");
				return;
			}

		if(TpNameAdd.equals("") || TpNameAdd.equals(null))
		{
			player.sendMessage("Вы не ввели Имя закладки");
			return;
		}
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT COUNT(*) FROM community_board_teleport WHERE charId=?;");
			statement.setLong(1, player.getObjectId());
			rs = statement.executeQuery();
			rs.next();
			if(rs.getInt(1) <= Config.COMMUNITY_BOARD_TELEPORT_COUNT - 1)
			{
				statement = con.prepareStatement("SELECT COUNT(*) FROM community_board_teleport WHERE charId=? AND name=?;");
				statement.setLong(1, player.getObjectId());
				statement.setString(2, TpNameAdd);
				rs = statement.executeQuery();
				rs.next();
				if(rs.getInt(1) == 0)
				{
					statement = con.prepareStatement("INSERT INTO community_board_teleport (charId,xPos,yPos,zPos,name) VALUES(?,?,?,?,?)");
					statement.setInt(1, player.getObjectId());
					statement.setInt(2, player.getX());
					statement.setInt(3, player.getY());
					statement.setInt(4, player.getZ());
					statement.setString(5, TpNameAdd);
					statement.execute();
				}
				else
				{
					statement = con.prepareStatement("UPDATE community_board_teleport SET xPos=?, yPos=?, zPos=? WHERE charId=? AND name=?;");
					statement.setInt(1, player.getObjectId());
					statement.setInt(2, player.getX());
					statement.setInt(3, player.getY());
					statement.setInt(4, player.getZ());
					statement.setString(5, TpNameAdd);
					statement.execute();
				}
			}
			else
				player.sendMessage("Вы не можете сохранить более 10 закладок");

		}
		catch(final Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}
	}

	@Override
	public void parsewrite(final String ar1, final String ar2, final String ar3, final String ar4, final String ar5, final L2Player activeChar)
	{}
}
