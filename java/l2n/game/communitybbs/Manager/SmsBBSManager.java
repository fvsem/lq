package l2n.game.communitybbs.Manager;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

public class SmsBBSManager extends AbstractBBSManager
{
	public static SmsBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private SmsBBSManager()
	{}

	@Override
	public void parsecmd(String command, L2Player player)
	{
		String content = Files.read(Config.BBS_HOME_DIR +"/303.htm", player);
		content = content.replace("%name%", player.getName());
		separateAndSend(content, player);
	}

	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player player)
	{

	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final SmsBBSManager _instance = new SmsBBSManager();
	}
}
