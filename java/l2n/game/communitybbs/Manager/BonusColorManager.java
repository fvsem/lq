package l2n.game.communitybbs.Manager;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BonusColorManager extends AbstractBBSManager
{
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final BonusColorManager _instance = new BonusColorManager();
	}
	
	public static BonusColorManager getInstance()
	{
		return SingletonHolder._instance;
	}
    
    @Override
    public void parsecmd(String command, L2Player player){
        final String[] cmd = command.split(":");
        if(command.startsWith("_bbsobtc"))
            if(getPlayerStatus(player.getName()))
            {
                player.setNameColor(Integer.decode("0x"+ cmd[1].substring(4, 6) + cmd[1].substring(2, 4) + cmd[1].substring(0, 2)));
                player.broadcastUserInfo(true);
                deletName(player.getName());
            }
            else
                player.sendMessage(player.isLangRus() ? "Вас нет в списках тестеров, или вы уже получили бонус" : "You are not in the bonus list");
        }    
    private boolean getPlayerStatus(String name)
    {
    	
        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        ResultSet rset = null;
        List<String> names = new ArrayList<String>();
        try
		{
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("SELECT name FROM community_bonuscolor");
            rset = statement.executeQuery();
            while(rset.next())
				names.add(rset.getString("name"));
		}
		catch (SQLException e)
		{
            e.printStackTrace();
            return false;
		}
		finally
		{
            DbUtils.closeQuietly(con, statement, rset);
		}
        if(names.contains(name))
            return true;
        return false;
    }
    
    private void deletName(String name)
    {
        ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
        try
		{
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("DELETE FROM community_bonuscolor WHERE name=?");
            statement.setString(1, name);
            statement.execute();
        }
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
            DbUtils.closeQuietly(con, statement);
		}
    }
    
	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player player)
	{

	}

}
