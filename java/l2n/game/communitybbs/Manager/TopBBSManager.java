package l2n.game.communitybbs.Manager;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.StringUtil;

import java.util.StringTokenizer;

public class TopBBSManager extends AbstractBBSManager
{
	@Override
	public void parsecmd(final String command, final L2Player activeChar)
	{
		if(Config.COMMUNITYBOARDLOGGIN)
			Log.add("CommunityBoard|TopBBS|AccountName=" + activeChar.getAccountName() + "|CharName=" + activeChar.getName() + "|IP=" + activeChar.getIP() + "|Command=" + command, "CommunityBoard");
		if(command.equals("_bbstop"))
		{
			String content = Files.read(Config.BBS_HOME_DIR +"/index.htm", activeChar);
			if(content == null)
				content = new String("<html><body><br><br><center>404 :Нет Файлов в:' "+Config.BBS_HOME_DIR +"index.htm' </center></body></html>");
			separateAndSend(content, activeChar);
		}
		else if(command.equals("_bbshome"))
		{
			String content = Files.read(Config.BBS_HOME_DIR +"/index.htm", activeChar);
			if(content == null)
				content = new String("<html><body><br><br><center>404 :Нет Файлов в:' "+Config.BBS_HOME_DIR +"index.htm' </center></body></html>");
			separateAndSend(content, activeChar);
		}
		else if(command.startsWith("_bbstop;"))
		{
			final StringTokenizer st = new StringTokenizer(command, ";");
			st.nextToken();
			final String idp = st.nextToken();
			String content = Files.read(Config.BBS_HOME_DIR  + idp + ".htm", activeChar);
			if(content == null)
				content = new String("<html><body><br><br><center>404 :Нет Файлов в:' "+Config.BBS_HOME_DIR +" "+ idp + ".htm' </center></body></html>");
			separateAndSend(content, activeChar);
		}
		else
			notImplementedYet(activeChar, command);
	}

	public void showTopPage(final L2Player activeChar, String page, final String subcontent)
	{
		if(page == null || page.isEmpty())
			page = "index";
		else
			page = page.replace("../", "").replace("..\\", "");
		page = StringUtil.concat(Config.BBS_HOME_DIR +"", page, ".htm");
		String content = Files.read(page, activeChar);
		if(content == null)
			if(subcontent == null)
				content = StringUtil.concat("<html><body><br><br><center>Нет файлов в:' ", page, "</center></body></html>");
			else
				content = "<html><body>%content%</body></html>";
		if(subcontent != null)
			content = content.replace("%content%", subcontent);
		separateAndSend(content, activeChar);
	}

	@Override
	public void parsewrite(final String ar1, final String ar2, final String ar3, final String ar4, final String ar5, final L2Player activeChar)
	{}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final TopBBSManager _instance = new TopBBSManager();
	}

	public static TopBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
}
