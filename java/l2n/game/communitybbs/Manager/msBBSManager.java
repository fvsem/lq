package l2n.game.communitybbs.Manager;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

import java.util.Date;

public class msBBSManager extends AbstractBBSManager
{
	public static msBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private msBBSManager()
	{}

	@Override
	public void parsecmd(String command, L2Player player)
	{

		String content = Files.read(Config.BBS_HOME_DIR +"acc.htm", player);
		content = content.replace("%clan%", player.getClan() != null ? String.valueOf(player.getClan().getName()) : "<font color=\"FF0000\">Вы не состоите в клане</font>");
		content = content.replace("%ip%", player.getIP());
		content = content.replace("%name%", player.getName());
		content = content.replace("%title%", String.valueOf(player.getTitle() != null ? player.getTitle() : "нет"));
		content = content.replace("%account%", player.getAccountName());
		content = content.replace("%objectId%", "" + player.getObjectId());
		content = content.replace("%level%", "" + player.getLevel());
		content = content.replace("%noobl%", player.isNoble() ? String.valueOf("да") : "<font color=\"FF0000\">Требуется саб. 75 ур</font>");
		content = content.replace("%xp%", "" + player.getExp());
		content = content.replace("%sp%", "" + player.getSp());
		content = content.replace("%aly%", String.valueOf(player.getAlliance() != null ? player.getAlliance().getAllyName() : "<font color=\"FF0000\">Вы не состоите в альянсе</font>"));
		content = content.replace("%sex%", String.valueOf(player.getSex() == 0 ? "Мужской" : "Женский"));
		content = content.replace("%sub%", String.valueOf(player.getSubClasses().size() - 1));
		content = content.replace("%class%", player.getTemplate().className);
		content = content.replace("%class_name%", player.getClassId().getNormalName());
		content = content.replace("%currenthp%", "" + (int) player.getCurrentHp());
		content = content.replace("%currentmp%", "" + (int) player.getCurrentMp());
		content = content.replace("%currentcp%", "" + (int) player.getCurrentCp());
		content = content.replace("%currentload%", "" + player.getCurrentLoad());
		content = content.replace("%maxhp%", "" + player.getMaxHp());
		content = content.replace("%maxmp%", "" + player.getMaxMp());
		content = content.replace("%maxcp%", "" + player.getMaxCp());
		content = content.replace("%maxload%", "" + player.getMaxLoad());
		content = content.replace("%karma%", "" + player.getKarma());
		content = content.replace("%pkkills%", "" + player.getPkKills());
		content = content.replace("%pvpkills%", "" + player.getPvpKills());
		 if(player.getNetConnection().getBonus() > 1)
	        {
	            long endtime = player.getNetConnection().getBonusExpire();
	            if(endtime > 0)
	            	content = content.replace("%premium%", new Date(endtime * 1000).toString());
	        }
	        else
	        	content = content.replace("%premium%", "<font color=\"FF0000\">Нет Премиум Аккаунта</font>" + "<a action=\"bypass -h _bbsscripts 60 services.RateBonus.RateBonus:list\">Купить ПА</a>");
			
		separateAndSend(content, player);
	}

	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player player)
	{

	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final msBBSManager _instance = new msBBSManager();
	}
}
