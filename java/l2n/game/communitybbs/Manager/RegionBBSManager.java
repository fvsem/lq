package l2n.game.communitybbs.Manager;

import javolution.text.TextBuilder;
import l2n.Config;
import l2n.game.GameTimeController;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class RegionBBSManager extends AbstractBBSManager
{
	private static final String tdClose = "</td>";
	private static final String tdOpen = "<td align=left valign=top>";
	private static final String trClose = "</tr>";
	private static final String trOpen = "<tr>";
	private static final String colSpacer = "<td FIXWIDTH=15></td>";
	private static final SimpleDateFormat format = new SimpleDateFormat("H:mm");

	// Кешировать то что запрашивают

	@Override
	public void parsecmd(String command, L2Player activeChar)
	{
		if(Config.COMMUNITYBOARDLOGGIN)
			Log.add("CommunityBoard|RegionBBS|AccountName=" + activeChar.getAccountName() + "|CharName=" + activeChar.getName() + "|IP=" + activeChar.getIP() + "|Command=" + command, "CommunityBoard");
		if(command.equals("_bbsloc"))
			showRegion(activeChar, 0);
		else if(command.startsWith("_bbsloc;page;"))
		{
			StringTokenizer st = new StringTokenizer(command, ";");
			st.nextToken();
			st.nextToken();
			int index = 0;
			try
			{
				index = Integer.parseInt(st.nextToken());
				showRegion(activeChar, index);
			}
			catch(NumberFormatException nfe)
			{
				separateAndSend("<html><body><br><br><center>Error!</center><br><br></body></html>", activeChar);
			}
		}
		else
			separateAndSend("<html><body><br><br><center>the command: " + command + " is not implemented yet</center><br><br></body></html>", activeChar);
	}

	private void showRegion(L2Player activeChar, int startIndex)
	{
		TextBuilder htmlCode = new TextBuilder("<html><body><br>");

		htmlCode.append("<table>");

		Calendar cal = Calendar.getInstance();
		int t = GameTimeController.getInstance().getGameTime();
		
		htmlCode.append(trOpen);
		htmlCode.append("<td>Время Сервера: " + format.format(cal.getTime()) + tdClose);
		htmlCode.append(colSpacer);
		cal.set(Calendar.HOUR_OF_DAY, t / 60);
		cal.set(Calendar.MINUTE, t % 60);
		htmlCode.append("<td>Игровое Время: " + format.format(cal.getTime()) + tdClose);
		htmlCode.append(trClose);

		htmlCode.append("</table>");
		htmlCode.append(trOpen);
		htmlCode.append("<td><img src=\"L2UI.SquareWhite\" width=725 height=1><br></td>");
		htmlCode.append(trClose);
		htmlCode.append("<table>");

				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты XP: x " + Config.RATE_XP + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты SP: x " + Config.RATE_SP + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты дроп Адена: x " + Config.RATE_DROP_ADENA + tdClose);
				htmlCode.append(trClose);
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты дроп Итем: x " + Config.RATE_DROP_ITEMS + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Спойл: x " + Config.RATE_DROP_SPOIL + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Квест XP: x " + Config.RATE_QUESTS_REWARD_XP + tdClose);
				htmlCode.append(trClose);
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты Пати XP: x " + Config.RATE_XP_PARTY + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Пати SP: x " + Config.RATE_SP_PARTY + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Квест SP: x " + Config.RATE_QUESTS_REWARD_SP + tdClose);
				htmlCode.append(trClose);
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты Пати Адена: x " + Config.RATE_DROP_ADENA_PARTY + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Пати Итемс: x " + Config.RATE_DROP_ITEMS_PARTY + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Квест Итемс: x " + Config.RATE_QUESTS_DROP + tdClose);
				htmlCode.append(trClose);
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты Рейд Босс: x " + Config.RATE_DROP_RAIDBOSS + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Эпик Босс: x " + Config.RATE_DROP_EPIC_RAIDBOSS + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Квест Адена: x " + Config.RATE_QUESTS_REWARD_ADENA + tdClose);
				htmlCode.append(trClose);		
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Рейты Рыбалки: x " + Config.RATE_FISH_DROP_COUNT + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Манора: x " + Config.RATE_MANOR + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты дроп Эполетов: x " + Config.EPAULETTE_DROP_RATE + tdClose);
				htmlCode.append(trClose);				
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Шанс Крафта RAR: % " + Config.RATE_MASTERWORK + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Рейты Хеллбоунд очей: x " + Config.RATE_HELLBOUND_POINTS + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Убийств Тиад для открытия(СОД): -" + Config.SOD_TIAT_KILL_COUNT + tdClose);
				htmlCode.append(trClose);									
				htmlCode.append(trOpen);
				htmlCode.append(tdOpen + "Шанс заточки Оружия: % " + Config.ENCHANT_CHANCE_WEAPON + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Шанс заточки Брони: % " + Config.ENCHANT_CHANCE_ARMOR + tdClose);
				htmlCode.append(colSpacer);
				htmlCode.append(tdOpen + "Шанс заточки Аксесуаров: % " + Config.ENCHANT_CHANCE_ACCESSORY + tdClose);
				htmlCode.append(trClose);
		htmlCode.append("</table>");
		htmlCode.append("<table>");

		htmlCode.append(trOpen);
		htmlCode.append("<td><img src=\"L2UI.SquareWhite\" width=725 height=1><br></td>");
		htmlCode.append(trClose);

		if(Config.REGION_PAGE_SHOW_ONLINE)
		{
			htmlCode.append(trOpen);
			int offliners = L2ObjectsStorage.getAllOfflineCount();
			int online = L2ObjectsStorage.getAllPlayersCount();
			if(Config.SHOW_OFFLINE_TRADERS)
				htmlCode.append(tdOpen + online + " Игроков , " + offliners + " Игроков Торгуют</td>");
			else
				htmlCode.append(tdOpen + online + " Игроков Онлайн:</td>");
			htmlCode.append(trClose);
		}

		htmlCode.append("</table>");
		
		if(Config.REGION_PAGE_SHOW_PLAYERS)
		{
			TreeSet<String> temp = new TreeSet<String>();
			for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			{
				if(player == null || player.getName() == null)
					continue;
				if(player.isGM() && player.isInvisible() && !activeChar.isGM())
					continue;

				if(Config.SHOW_OFFLINE_TRADERS && player.isInOfflineMode())
				{
					temp.add("<font color=\"" + Config.SERVICES_OFFLINE_TRADE_NAME_COLOR + "\">" + player.getName() + "</font>");
					continue;
				}
				temp.add(player.getName());
			}
                        
			String[] player_names = temp.toArray(new String[temp.size()]);

			htmlCode.append("<table border=0>");
			htmlCode.append("<tr><td><table border=0>");

			int cell = 0;
			int n = startIndex;
			for(int i = startIndex; i < startIndex + Config.NAME_PAGE_SIZE_COMMUNITYBOARD; i++)
			{
				if(i >= player_names.length)
					break;

				String player = player_names[i]; // Get the current record

				cell++;

				if(cell == 1)
					htmlCode.append(trOpen);

				htmlCode.append("<td align=left valign=top FIXWIDTH=75>");
				htmlCode.append(player);
				htmlCode.append(tdClose);

				if(cell < Config.NAME_PER_ROW_COMMUNITYBOARD)
					htmlCode.append(colSpacer);

				if(cell == Config.NAME_PER_ROW_COMMUNITYBOARD)
				{
					cell = 0;
					htmlCode.append(trClose);
				}
				n++;
			}

			if(cell > 0 && cell < Config.NAME_PER_ROW_COMMUNITYBOARD)
				htmlCode.append(trClose);

			htmlCode.append("</table></td></tr>");

			if(player_names.length > Config.NAME_PAGE_SIZE_COMMUNITYBOARD)
			{
				htmlCode.append("<tr><td align=center valign=top>Показано " + (startIndex + 1) + " - " + n + " Игроков</td></tr>");

				htmlCode.append("<tr><td align=center valign=top>");

				htmlCode.append("<table border=0 width=610><tr>");

				if(startIndex == 0)
					htmlCode.append("<td><button value=\"Назад\" width=50 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
				else
					htmlCode.append("<td><button value=\"Назад\" action=\"bypass _bbsloc;page;" + (startIndex - Config.NAME_PAGE_SIZE_COMMUNITYBOARD) + "\" width=50 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");

				htmlCode.append(colSpacer);

				if(player_names.length <= startIndex + Config.NAME_PAGE_SIZE_COMMUNITYBOARD)
					htmlCode.append("<td><button value=\"Вперед\" width=50 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");
				else
					htmlCode.append("<td><button value=\"Вперед\" action=\"bypass _bbsloc;page;" + (startIndex + Config.NAME_PAGE_SIZE_COMMUNITYBOARD) + "\" width=50 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\"></td>");

				htmlCode.append("</tr></table>");

				htmlCode.append("</td></tr>");
			}

			htmlCode.append("</table>");
		}

		htmlCode.append("</body></html>");
		separateAndSend(htmlCode.toString(), activeChar);
	}

	@Override
	public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player activeChar)
	{
		if(activeChar == null)
			return;
		separateAndSend("<html><body><br><br><center>команда: " + ar1 + " пока не реализована</center><br><br></body></html>", activeChar);;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final RegionBBSManager _instance = new RegionBBSManager();
	}

	public static RegionBBSManager getInstance()
	{
		return SingletonHolder._instance;
	}
}
