package l2n.game.communitybbs.Manager;

import javolution.text.TextBuilder;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShowBoard;

public abstract class AbstractBBSManager
{
	public abstract void parsecmd(String command, L2Player activeChar);

	public abstract void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player activeChar);

	protected void separateAndSend(TextBuilder html, L2Player acha)
	{
		try
		{
			separateAndSend(html.toString(), acha);
		}
		finally
		{
			TextBuilder.recycle(html);
		}
	}

	protected void separateAndSend(String html, L2Player acha)
	{
		ShowBoard.separateAndSend(acha, html);
	}

	protected void notImplementedYet(L2Player activeChar, String command)
	{
		ShowBoard.notImplementedYet(activeChar, command);
	}

	protected void send1001(String html, L2Player activeChar)
	{
		ShowBoard.send1001(html, activeChar);
	}

	protected void send1002(L2Player activeChar)
	{
		ShowBoard.send1002(activeChar, " ", " ", "0");
	}

	protected void send1002(L2Player activeChar, String string, String string2, String string3)
	{
		ShowBoard.send1002(activeChar, string, string2, string3);
	}
}
