package l2n.game.communitybbs.Manager;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dislike
 */
public class CustonManager extends AbstractBBSManager{
	
	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		protected static final CustonManager _instance = new CustonManager();
	}

	public static CustonManager getInstance()
	{
		return SingletonHolder._instance;
	}
	
    @Override
    
    public void parsecmd(String command, L2Player player) {
        final String[] cmd = command.split(":");
        if(cmd[1].startsWith("bonus"))
        {
            if(checkCondition(player))
                if(addReward(player.getClan()))
                {
                    deletRecord(player.getClan());
                    player.sendMessage(player.isLangRus() ? "Вас нет в списках  или вы уже получили бонус" : "You are not in the bonus list");

                }
        }
    }
    
    private boolean checkCondition(L2Player player)
    {
        if(player == null)
            return false;
        
        L2Clan clan  = player.getClan();
        
        if(clan == null)
        {
            player.sendMessage("Вы не состоите в клане.");
            return false;
        }
        
        if(!player.isClanLeader())
        {
            player.sendMessage("Вы не являетесь главой клана.");
            return false;
        }
        
        ThreadConnection con = null;
        FiltredPreparedStatement statement = null;
        ResultSet rset = null;
        List<String> names = new ArrayList<String>();
        try
		{
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("SELECT name FROM community_clanbonus");
            rset = statement.executeQuery();
            while(rset.next())
				names.add(rset.getString("name"));
		}
		catch (SQLException e)
		{
            e.printStackTrace();
            return false;
		}
		finally
		{
            DbUtils.closeQuietly(con, statement, rset);
		}
        
        if(!names.contains(clan.getName()))
        {
            player.sendMessage("Вашего клана нет в списке.");
            return false;
        }
        
        if(!checkPlayersCount(clan))
        {
            player.sendMessage("Условия не соблюдены.");
            return false;
        }
        
        return true;
        
    }
    
    private boolean checkPlayersCount(L2Clan clan)
    {
        Map<String, String> ips = new HashMap<String, String>();
        int realOnline = clan.getOnlineMembers(0).length;
        int need = 0;
        //проверка на твинков
        for(L2Player p : clan.getOnlineMembers(0))
            ips.put(p.getIP(), p.getIP());
        if(realOnline != ips.size())
            return false;
        
        ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("SELECT players_count FROM community_clanbonus WHERE name=? LIMIT 1");
            statement.setString(1, clan.getName());
            rset = statement.executeQuery();
            if(rset.next())
            {
                need = rset.getInt("players_count");

            }
		}
		catch(SQLException e)
		{
            e.printStackTrace();
            return false;
		}
		finally
		{
            DbUtils.closeQuietly(con, statement, rset);
		}
        
        if(need > realOnline)
            return false;
        
        return true;
        
    }
    
    private boolean addReward(L2Clan clan)
    {     
        ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
        
        byte lvl = 0;
        int rep = 0;
        //Clan clan = player.getClan();
    
		try
		{
            con = L2DatabaseFactory.getInstance().getConnection();
            statement = con.prepareStatement("SELECT reward_lvl, reward_rep FROM community_clanbonus WHERE name=? LIMIT 1");
            statement.setString(1, clan.getName());
            rset = statement.executeQuery();
            if(rset.next())
            {
                lvl = rset.getByte("reward_lvl");
                rep = rset.getInt("reward_rep");
            }
		}
		catch(SQLException e)
		{
            e.printStackTrace();
            return false;
		}
		finally
		{
            DbUtils.closeQuietly(con, statement, rset);
		}
        
        if(lvl > clan.getLevel())
            clan.setLevel(lvl);
        clan.incReputation(rep, true, null);
        clan.broadcastClanStatus(true, true, false);
        return true;
	}
    
    private void deletRecord(L2Clan clan)
    {
         ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM community_clanbonus WHERE name = ?");
			statement.setString(1, clan.getName());
			statement.execute();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
    }

    @Override
    public void parsewrite(String ar1, String ar2, String ar3, String ar4, String ar5, L2Player activeChar) {}
    
}