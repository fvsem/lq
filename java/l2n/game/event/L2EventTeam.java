package l2n.game.event;

import l2n.commons.list.GArray;
import l2n.commons.list.GCSArray;
import l2n.game.L2GameThreadPools;
import l2n.game.model.EffectList;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.restrictions.IPlayerRestriction;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.Revive;
import l2n.game.network.serverpackets.SkillList;
import l2n.game.tables.HeroSkillTable;
import l2n.game.tables.SkillTable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2EventTeam implements Comparable<L2EventTeam>
{
	private static final Logger _log = Logger.getLogger(L2EventTeam.class.getName());

	private final int _teamId;
	private final String _teamName;

	private final AtomicInteger _teamPoints;
	private final GCSArray<Long> _players;
	private GCSArray<Long> _live_players;
	/** очки участников ивента */
	private final Map<Long, L2EventPlayerInfo> _playerInfos;

	public L2EventTeam(final int teamId, final String teamName)
	{
		_teamId = teamId;
		_teamName = teamName;
		_teamPoints = new AtomicInteger(0);

		_players = new GCSArray<Long>();
		_live_players = new GCSArray<Long>();
		_playerInfos = new ConcurrentHashMap<Long, L2EventPlayerInfo>();
	}

	public final int getTeamId()
	{
		return _teamId;
	}

	public final String getTeamName()
	{
		return _teamName;
	}

	public final int increaseTeamPoints()
	{
		return _teamPoints.incrementAndGet();
	}

	public final int decreaseTeamPoints()
	{
		return _teamPoints.decrementAndGet();
	}

	public final int getTeamPoints()
	{
		return _teamPoints.get();
	}

	public final void addPlayer(final L2Player player)
	{
		if(player == null)
			return;

		final Long playerStoredId = player.getStoredId();
		_players.add(playerStoredId);
		_live_players.add(playerStoredId);
		_playerInfos.put(playerStoredId, new L2EventPlayerInfo(playerStoredId));
	}

	public final void removePlayer(final L2Player player)
	{
		if(player == null)
			return;

		final Long playerStoredId = player.getStoredId();
		_players.remove(playerStoredId);
		_live_players.remove(playerStoredId);
		_playerInfos.remove(playerStoredId);
		player.setTeam(0, false);
	}

	public final void removeFromLive(final L2Player player)
	{
		if(player == null)
			return;
		_live_players.remove(player.getStoredId());
	}

	public final void removeAura()
	{
		final GArray<L2Player> result = getLivePlayers();
		for(final L2Player player : result)
			if(player != null)
				player.setTeam(0, false);
	}

	public final boolean isInTeam(final Long playerStoreId)
	{
		return _players.contains(playerStoreId);
	}

	public final boolean isInTeam(final L2Player player)
	{
		return isInTeam(player.getStoredId());
	}

	public final boolean isLive(final Long playerStoreId)
	{
		return _live_players.contains(playerStoreId);
	}

	public final boolean isLive(final L2Player player)
	{
		return isLive(player.getStoredId());
	}

	public final int getPlayersCount()
	{
		return _players.size();
	}

	public final boolean isEmpty()
	{
		return _players.isEmpty();
	}

	public final int getLivePlayersCount()
	{
		return _live_players.size();
	}

	public final Collection<L2EventPlayerInfo> getPlayerInfos()
	{
		return _playerInfos.values();
	}

	public final L2EventPlayerInfo[] getPlayerInfosArray()
	{
		return _playerInfos.values().toArray(new L2EventPlayerInfo[_playerInfos.size()]);
	}

	public final L2EventPlayerInfo getPlayerInfo(final Long playerStoredId)
	{
		return _playerInfos.get(playerStoredId);
	}

	/**
	 * Отчищает списки: _players, _live_players, _playerInfos
	 */
	public final void clear()
	{
		_players.clear();
		_live_players.clear();
		_playerInfos.clear();
	}

	public final L2Player getTopPlayer()
	{
		final L2EventPlayerInfo[] players = getPlayerInfosArray();
		Arrays.sort(players, L2EventUtil.SCORE_COMPARATOR);

		if(players.length == 0)
			return null;

		final L2EventPlayerInfo winnerInfo = players[0];
		if(winnerInfo != null && winnerInfo.getKillsCount() > 0)
			return winnerInfo.getPlayer();

		return null;
	}

	public final GArray<L2Player> getAllPlayers()
	{
		final GArray<L2Player> result = new GArray<L2Player>(_players.size());
		for(final Long storeId : _players)
		{
			final L2Player player = L2ObjectsStorage.getAsPlayer(storeId);
			if(player != null)
				result.addLastUnsafe(player);
		}
		return result;
	}

	public final GArray<L2Player> getLivePlayers()
	{
		final GArray<L2Player> result = new GArray<L2Player>(_live_players.size());
		for(final Long storeId : _live_players)
		{
			final L2Player player = L2ObjectsStorage.getAsPlayer(storeId);
			if(player != null)
				result.addLastUnsafe(player);
		}
		return result;
	}

	public final void updateLivePlayers()
	{
		final GCSArray<Long> new_live_list = new GCSArray<Long>();
		for(final Long storeId : _live_players)
		{
			final L2Player player = L2ObjectsStorage.getAsPlayer(storeId);
			if(player != null)
				new_live_list.add(storeId);
		}
		_live_players = new_live_list;
	}

	public final void paralyzePlayers()
	{
		final L2Skill revengeSkill = SkillTable.FrequentSkill.RAID_CURSE2.getSkill();
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			if(player != null)
			{
				player.getEffectList().stopEffects(EffectList.PROC_STOP_PVPEVENT);
				revengeSkill.getEffectsSelf(player, 120000);
				if(player.getPet() != null)
					revengeSkill.getEffectsSelf(player.getPet(), 120000);
			}
	}

	public final void upParalyzePlayers(final boolean drawnParty)
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
		{
			player.getEffectList().stopEffect(L2Skill.SKILL_RAID_CURSE);
			if(player.getPet() != null)
				player.getPet().getEffectList().stopEffect(L2Skill.SKILL_RAID_CURSE);

			if(drawnParty && player.getParty() != null)
				player.getParty().oustPartyMember(player);
		}
	}

	public final void ressurectPlayers()
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			if(player != null && player.isDead())
			{
				player.restoreExp();
				player.setCurrentCp(player.getMaxCp());
				player.setCurrentHp(player.getMaxHp(), true);
				player.setCurrentMp(player.getMaxMp());
				player.broadcastPacket(new Revive(player));
			}
	}

	public final void healPlayers()
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			if(player != null)
			{
				player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
				player.setCurrentCp(player.getMaxCp());
			}
	}

	public final void saveBackCoords()
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			player.setVar("event_backcoords", player.getBackCoords());
	}

	public final void addRestrictions(final IPlayerRestriction... restrictions)
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			for(final IPlayerRestriction r : restrictions)
				player.getRestrictions().addRestriction(r);
	}

	public final void removeRestrictions(final IPlayerRestriction... restrictions)
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			for(final IPlayerRestriction r : restrictions)
				player.getRestrictions().removeRestriction(r);
	}

	public final void teleportToSavedCoords(final boolean checkSkills)
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			try
			{
				final String var = player.getVar("event_backcoords");
				if(var == null || var.equals(""))
					continue;
				final String[] coords = var.split(" ");
				if(coords.length != 4)
					continue;

				if(checkSkills)
				{
					// Add clan skill
					if(player.getClan() != null)
						for(final L2Skill skill : player.getClan().getAllSkills())
							if(skill.getMinPledgeClass() <= player.getPledgeClass())
								player.addSkill(skill, false);

					// Add Hero Skills
					if(player.isHero())
						for(final L2Skill sk : HeroSkillTable.getHeroSkills())
							player.addSkill(sk);

					// обновляем скилл лист, после добавления скилов
					player.sendPacket(new SkillList(player));
				}

				// отправляем назад
				player.teleToLocation(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]), Long.parseLong(coords[3]));
				player.unsetVar("event_backcoords");
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "", e);
			}
	}

	public void sendPacketToLive(final L2GameServerPacket packet)
	{
		final GArray<L2Player> players = getLivePlayers();
		for(final L2Player player : players)
			if(player != null && player.isOnline())
				player.sendPacket(packet);
	}

	public void sendPacketToAll(final L2GameServerPacket packet)
	{
		final GArray<L2Player> players = getAllPlayers();
		for(final L2Player player : players)
			if(player != null && player.isOnline())
				player.sendPacket(packet);
	}

	public void buffAllPlayers(final L2Event event)
	{
		final GArray<L2Player> players = getLivePlayers();
		for(final L2Player player : players)
			if(player != null && player.isOnline())
				addEventBuffs(player, event);
	}

	public static final void addEventBuffs(final L2Player player, final L2Event event)
	{
		if(player != null && event != null)
		{
			final L2Skill[] buffs = player.isMageClass() ? event.getMageBuffs() : event.getFighterBuffs();
			if(buffs == null || buffs.length == 0)
				return;

			L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public final void run()
				{
					for(final L2Skill skill : buffs)
						if(skill != null)
							skill.getEffectsSelf(player, 0);
				}
			}, 100);
		}
	}

	@Override
	public int compareTo(final L2EventTeam team)
	{
		return team.getTeamPoints() - getTeamPoints();
	}
}
