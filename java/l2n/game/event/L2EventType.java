package l2n.game.event;

/**
 * Список с типами ивентов
 */
public enum L2EventType
{
	NONE(0, "None", "Никакой"),
	LAST_HERO(1, "Last Hero", "Последний Герой"),
	TVT(2, "Team vs Team", "Команда против Команды"),
	DEATHMATCH(3, "Deathmatch", "Бой Насмерть"),
	CTF(4, "Capture The Flag", "Захват флага"),
	TOWN_SIEGE(5, "Town Siege", "Осада Города");

	private final int _id;
	private final String _nameEng;
	private final String _nameRus;

	private L2EventType(final int id, final String nameEn, final String nameRus)
	{
		_id = id;
		_nameEng = nameEn;
		_nameRus = nameRus;
	}

	public int getId()
	{
		return _id;
	}

	public String getName(final boolean rus)
	{
		return rus ? _nameRus : _nameEng;
	}

	@Override
	public String toString()
	{
		return _nameEng;
	}
}
