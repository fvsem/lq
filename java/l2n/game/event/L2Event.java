package l2n.game.event;

import javolution.text.TextBuilder;
import l2n.extensions.scripts.EventScript;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.L2ObjectProcedures.SendPacketProc;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.network.serverpackets.Revive;
import l2n.game.tables.SkillTable;
import l2n.util.Location;

import java.util.logging.Level;

public abstract class L2Event extends EventScript
{
	public static final int BLUE_TEAM = 0;
	public static final int RED_TEAM = 1;

	protected static final class SirenTask implements Runnable
	{
		private int _count;

		public SirenTask(final int count)
		{
			_count = count;
		}

		@Override
		public void run()
		{
			final PlaySound siren = new PlaySound("ItemSound3.sys_siren");
			L2ObjectsStorage.forEachPlayer(new SendPacketProc(siren));
			_count--;
			if(_count > 0)
				L2GameThreadPools.getInstance().scheduleGeneral(new SirenTask(_count), 5500);
		}
	}

	protected static class TeleportTask implements Runnable
	{
		private final Location loc;
		private final L2Character target;

		public TeleportTask(final L2Character t, final Location l)
		{
			target = t;
			loc = l;
			target.startStunning();
		}

		@Override
		public void run()
		{
			target.stopStunning();
			target.teleToLocation(loc);
		}
	}

	public class StartTask implements Runnable
	{
		private final String _category;
		private final String _autocontinue;

		public StartTask(final int category, final int autocontinue)
		{
			_category = Integer.toString(category);
			_autocontinue = Integer.toString(autocontinue);
		}

		public StartTask()
		{
			_category = "1";
			_autocontinue = "1";
		}

		@Override
		public void run()
		{
			if(!isActive())
				return;

			if(TerritorySiege.isInProgress())
			{
				_log.info(getEventType() + " not started: TerritorySiege in progress");
				return;
			}

			for(final Castle c : CastleManager.getInstance().getCastles().values())
				if(c.getSiege() != null && c.getSiege().isInProgress())
				{
					_log.info(getEventType() + " not started: CastleSiege in progress");
					return;
				}

			start(_category, _autocontinue);
		}
	}

	public void registerPlayer(final L2Player player)
	{}

	public void deletePlayer(final L2Player player)
	{}

	public void participateAnswer(final L2Player player, final int answer)
	{}

	/**
	 * @return вернёт true если Ивент активирован
	 */
	public abstract boolean isActive();

	public abstract L2EventType getEventType();

	public abstract void activateEvent();

	public abstract void deactivateEvent();

	public abstract void start(final String... var);

	public abstract void go();

	public L2Skill[] getFighterBuffs()
	{
		return SkillTable.EMPTY_ARRAY;
	}

	public L2Skill[] getMageBuffs()
	{
		return SkillTable.EMPTY_ARRAY;
	}

	protected static int getMinLevelForCategory(final int category)
	{
		switch (category)
		{
			case 1:
				return 20;
			case 2:
				return 30;
			case 3:
				return 40;
			case 4:
				return 52;
			case 5:
				return 62;
			case 6:
				return 76;
		}
		return 0;
	}

	protected static int getMaxLevelForCategory(final int category)
	{
		switch (category)
		{
			case 1:
				return 29;
			case 2:
				return 39;
			case 3:
				return 51;
			case 4:
				return 61;
			case 5:
				return 75;
			case 6:
				return 85;
		}
		return 0;
	}

	protected static int getCategory(final int level)
	{
		if(level >= 20 && level <= 29)
			return 1;
		else if(level >= 30 && level <= 39)
			return 2;
		else if(level >= 40 && level <= 51)
			return 3;
		else if(level >= 52 && level <= 61)
			return 4;
		else if(level >= 62 && level <= 75)
			return 5;
		else if(level >= 76)
			return 6;
		return 0;
	}
	
	protected static final void ressurectPlayer(final L2Player player)
	{
		if(player != null && player.isDead())
		{
			player.restoreExp();
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentHp(player.getMaxHp(), true);
			player.setCurrentMp(player.getMaxMp());
			player.broadcastPacket(new Revive(player));
		}
	}

	protected static final L2GameServerPacket getFinitshStatistics(final L2EventTeam team, final String event)
	{
		final NpcHtmlMessage msg = new NpcHtmlMessage(1).setItemId(1);
		try
		{
			final TextBuilder txt = new TextBuilder();
			final L2EventPlayerInfo[] inf = L2EventUtil.sortAndTrimPlayerInfos(team.getPlayerInfosArray(), 25);

			txt.append("<html><title>%replace%</title><body><center><br><table width=265 border=0 bgcolor=\"000000\"><tr><td width=40><font color=\"LEVEL\">Ранг</font></td><td><font color=\"LEVEL\">Имя</font></td><td width=40><font color=\"LEVEL\">Очки</font></td></tr>");

			int poz = 1;
			for(int i = 0; i < inf.length; i++)
			{
				if(inf[i] == null)
					continue;

				txt.append("<tr><td>" + poz + ".</td><td>" + (inf[i].getPlayer() == null ? "D/C" : inf[i].getPlayer().getName()) + "</td><td>" + inf[i].getKillsCount() + "</td></tr>");
				poz++;
			}
			// TOP 15
			txt.append("</table></center></body></head>");
			msg.setHtml(txt.toString());
			msg.replace("%replace%", event);
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "", e);
		}
		return msg;
	}

	protected static void sayToAll(final String address, final String... replacements)
	{
		Announcements.announceByCustomMessage(address, replacements, Say2C.CRITICAL_ANNOUNCEMENT);
	}
}
