package l2n.game.event;

import java.util.Arrays;
import java.util.Comparator;

public final class L2EventUtil
{
	private L2EventUtil()
	{
		// no constructor
	}

	public static final Comparator<L2EventPlayerInfo> SCORE_COMPARATOR = new Comparator<L2EventPlayerInfo>()
	{
		@Override
		public int compare(final L2EventPlayerInfo o1, final L2EventPlayerInfo o2)
		{
			if(o1 == null || o2 == null)
				return 0;
			return o2.getKillsCount() - o1.getKillsCount();
		}
	};

	public static L2EventPlayerInfo[] sortAndTrimPlayerInfos(final L2EventPlayerInfo[] initial)
	{
		return sortAndTrimPlayerInfos(initial, 9);
	}

	public static L2EventPlayerInfo[] sortAndTrimPlayerInfos(final L2EventPlayerInfo[] initial, final int limit)
	{
		// this is just classic insert sort
		// If u can find better sort for max 20-30 units, rewrite this... :)
		int max, index = 0;
		L2EventPlayerInfo pom;
		for(int i = 0; i < initial.length; i++)
		{
			max = initial[i].getKillsCount();
			for(int j = i; j < initial.length; j++)
				if(initial[j].getKillsCount() >= max)
				{
					max = initial[j].getKillsCount();
					index = j;

				}
			pom = initial[i];
			initial[i] = initial[index];
			initial[index] = pom;
		}

		return Arrays.copyOfRange(initial, 0, Math.min(initial.length, limit));
	}
}
