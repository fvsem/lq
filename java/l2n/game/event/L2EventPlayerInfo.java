package l2n.game.event;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.util.concurrent.atomic.AtomicInteger;

public class L2EventPlayerInfo
{
	private final long storeId;
	private final AtomicInteger _kills;
	private final AtomicInteger _deaths;

	public L2EventPlayerInfo(final long storedId)
	{
		storeId = storedId;
		_kills = new AtomicInteger(0);
		_deaths = new AtomicInteger(0);
	}

	public L2EventPlayerInfo(final L2Player player)
	{
		this(player.getStoredId());
	}

	public int incrementKills()
	{
		return _kills.incrementAndGet();
	}

	public int incrementDeaths()
	{
		return _deaths.incrementAndGet();
	}

	public int getKillsCount()
	{
		return _kills.get();
	}

	public int getDeathsCount()
	{
		return _deaths.get();
	}

	public final L2Player getPlayer()
	{
		return L2ObjectsStorage.getAsPlayer(storeId);
	}
}
