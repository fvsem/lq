package l2n.game.templates;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.commons.list.GArray;
import l2n.commons.util.StatsSet;
import l2n.game.ai.model.AdditionalAIParams;
import l2n.game.ai.model.AdditionalAIParams.EmptyAdditionalAIParams;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2MinionData;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillTargetType;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.base.ClassId;
import l2n.game.model.drop.L2Drop;
import l2n.game.model.drop.L2DropData;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2RaidBossInstance;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestEventType;
import l2n.game.scripts.NPCInstanceLoader;
import l2n.game.skills.effects.EffectTemplate;
import l2n.util.ArrayUtil;
import l2n.util.Log;
import l2n.util.TroveUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This cl contains all generic data of a L2Spawn object.<BR>
 * <BR>
 * <B><U> Data</U> :</B><BR>
 * <li>npcId, type, name, sex</li><BR>
 * <li>revardExp, revardSp</li><BR>
 * <li>aggroRange, factionId, factionRange</li><BR>
 * <li>rhand, lhand, armor</li><BR>
 * <li>_drops</li><BR>
 * <li>_minions</li><BR>
 * <li>_teachInfo</li><BR>
 * <li>_skills</li><BR>
 * <li>_questsStart</li><BR>
 */
@SuppressWarnings("unchecked")
public final class L2NpcTemplate extends L2CharTemplate
{
	private static final Logger _log = Logger.getLogger(L2NpcTemplate.class.getName());

	private final static TIntObjectHashMap<L2Skill> EMPTY_SKILL_MAP = new TIntObjectHashMap<L2Skill>(0);
	public final static AdditionalAIParams EMPTY_AI_PARAMS = new EmptyAdditionalAIParams();

	public static enum ShotsType
	{
		NONE,
		SOUL,
		SPIRIT,
		BSPIRIT,
		SOUL_SPIRIT,
		SOUL_BSPIRIT
	}

	public static enum Race
	{
		UNDEAD,
		MAGICCREATURE,
		BEAST,
		ANIMAL,
		PLANT,
		HUMANOID,
		SPIRIT,
		ANGEL,
		DEMON,
		DRAGON,
		GIANT,
		BUG,
		FAIRIE,
		HUMAN,
		ELVE,
		DARKELVE,
		ORC,
		DWARVE,
		OTHER,
		NONLIVING,
		SIEGEWEAPON,
		DEFENDINGARMY,
		MERCENARIE,
		UNKNOWN,
		KAMAEL,
		NONE
	}

	public final int npcId;
	public String type;
	public String ai_type;
	public final String name;
	public String title;
	public final byte level;
	public final int revardExp;
	public final int revardSp;
	public final short aggroRange;
	public int rhand;
	public final int lhand;
	public final int armor;
	public final String factionId;
	public final short factionRange;
	public final String jClass;
	public int displayId = 0;
	public int drop_herbs = 0;
	public final ShotsType shots;
	public boolean isRaid;

	private final AdditionalAIParams _ai_options;
	private Class<L2NpcInstance> this_class;

	private L2Skill[] _dam_skills = ArrayUtil.EMPTY_SKILLS_ARRAY, _dot_skills = ArrayUtil.EMPTY_SKILLS_ARRAY, _debuff_skills = ArrayUtil.EMPTY_SKILLS_ARRAY,
			_buff_skills = ArrayUtil.EMPTY_SKILLS_ARRAY,
			_stun_skills = ArrayUtil.EMPTY_SKILLS_ARRAY, _heal_skills = ArrayUtil.EMPTY_SKILLS_ARRAY;

	private Race _race = Race.NONE;
	public double rateHp = 1;

	/** The object containing all Item that can be dropped by L2NpcInstance using this L2NpcTemplate */
	private L2Drop _drop = null;
	public int killscount = 0;

	/** The table containing all Minions that must be spawn with the L2NpcInstance using this L2NpcTemplate */
	private GArray<L2MinionData> _minions = GArray.emptyCollection();

	private GArray<ClassId> _teachInfo = null;

	/** fixed skills */
	private TIntObjectHashMap<L2Skill> _skills = TroveUtils.emptyIntObjectMap();

	private Quest[][] _questEvents;

	/**
	 * Constructor of L2Character.<BR>
	 * <BR>
	 * 
	 * @param set
	 *            The StatsSet object to transfer data to the method
	 */
	public L2NpcTemplate(final StatsSet set, final AdditionalAIParams options)
	{
		super(set);
		npcId = set.getInteger("npcId");
		displayId = set.getInteger("displayId");

		type = set.getString("type").intern();
		ai_type = set.getString("ai_type").intern();
		name = set.getString("name").intern();
		title = set.getString("title").intern();

		level = set.getByte("level");
		revardExp = set.getInteger("revardExp");
		revardSp = set.getInteger("revardSp");
		aggroRange = set.getShort("aggroRange");
		rhand = set.getInteger("rhand");
		lhand = set.getInteger("lhand");

		armor = set.getInteger("armor");
		jClass = set.getString("jClass", null);
		final String f = set.getString("factionId", null);
		factionId = f == null ? "" : f.intern();
		factionRange = set.getShort("factionRange");
		drop_herbs = set.getInteger("drop_herbs");
		shots = set.getEnum("shots", ShotsType.class, ShotsType.NONE);
		_ai_options = options == null ? EMPTY_AI_PARAMS : options;
		setInstance(type, npcId);
	}

	public Class<L2NpcInstance> getInstanceClass()
	{
		return this_class;
	}

	public Constructor<?> getInstanceConstructor()
	{
		return this_class == null ? null : this_class.getConstructors()[0];
	}

	public boolean isInstanceOf(final Class<?> _class)
	{
		return this_class != null && _class.isAssignableFrom(this_class);
	}

	/**
	 * Создает новый инстанс NPC. Для него следует вызывать (именно в этом порядке): <br>
	 * setSpawnedLoc (обязательно) <br>
	 * setReflection (если reflection не базовый) <br>
	 * onSpawn (обязательно) <br>
	 * setChampion (опционально) <br>
	 * setCurrentHpMp (если вызывался setChampion) <br>
	 * spawnMe (в качестве параметра брать getSpawnedLoc)
	 */
	public L2NpcInstance getNewInstance()
	{
		try
		{
			return (L2NpcInstance) getInstanceConstructor().newInstance(IdFactory.getInstance().getNextId(), this);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Unable to create instance of NPC " + npcId, e);
		}
		return null;
	}

	public void setInstance(final String type, final int npcId)
	{
		Class<L2NpcInstance> _this_class = null;
		try
		{
			_this_class = (Class<L2NpcInstance>) Class.forName("l2n.game.model.instances." + type + "Instance");
		}
		catch(final ClassNotFoundException e)
		{
			try
			{
				_this_class = NPCInstanceLoader.getInstance().getModel(type);
			}
			catch(final Exception e1)
			{}
		}

		// Двери а то заносит в список
		if(_this_class == null && npcId < 10000000)
		{
			final String log = "npcId: " + npcId + " - not found type: " + type;
			Log.addDev(log, "dev_npc_notfoundtype", false);
			if(npcId == 0)
				Thread.dumpStack();
		}

		this_class = _this_class;
		isRaid = isInstanceOf(L2RaidBossInstance.class) && !isInstanceOf(L2ReflectionBossInstance.class);
	}

	public L2NpcTemplate(final StatsSet set)
	{
		this(set, null);
	}

	public void addTeachInfo(final ClassId classId)
	{
		if(_teachInfo == null)
			_teachInfo = new GArray<ClassId>();
		_teachInfo.add(classId);
	}

	public ClassId[] getTeachInfo()
	{
		if(_teachInfo == null)
			return null;
		return _teachInfo.toArray(new ClassId[_teachInfo.size()]);
	}

	public boolean canTeach(final ClassId classId)
	{
		if(_teachInfo == null)
			return false;
		return _teachInfo.contains(classId);
	}

	public void addDropData(final L2DropData drop)
	{
		if(_drop == null)
			_drop = new L2Drop();
		_drop.addData(drop);
	}

	public void addRaidData(final L2MinionData minion)
	{
		if(_minions.isEmpty())
			_minions = new GArray<L2MinionData>();
		_minions.add(minion);
	}

	public void addSkill(final L2Skill skill)
	{
		if(_skills.isEmpty())
			_skills = new TIntObjectHashMap<L2Skill>();
		_skills.put(skill.getId(), skill);

		// TODO перенести в AI
		if(skill.isNotUsedByAI() || skill.getTargetType() == SkillTargetType.TARGET_NONE || skill.getSkillType() == SkillType.NOTDONE || !skill.isActive())
			return;

		final SkillType type = skill.getSkillType();
		switch (type)
		{
			case PDAM:
			case MANADAM:
			case MDAM:
			case DRAIN:
			case DRAIN_SOUL:
			{
				boolean added = false;

				if(skill.hasEffects())
					for(final EffectTemplate eff : skill.getEffectTemplates())
						switch (eff.getEffectType())
						{
							case Stun:
								_stun_skills = ArrayUtils.add(_stun_skills, skill);
								added = true;
								break;
							case DamOverTime:
							case DamOverTimeLethal:
							case ManaDamOverTime:
							case LDManaDamOverTime:
								_dot_skills = ArrayUtils.add(_dot_skills, skill);
								added = true;
								break;
						}

				if(!added)
					_dam_skills = ArrayUtils.add(_dam_skills, skill);

				break;
			}
			case DOT:
			case MDOT:
			case POISON:
			case BLEED:
				_dot_skills = ArrayUtils.add(_dot_skills, skill);
				break;
			case DEBUFF:
			case SLEEP:
			case ROOT:
			case PARALYZE:
			case MUTE:
			case TELEPORT_NPC:
			case AGGRESSION:
				_debuff_skills = ArrayUtils.add(_debuff_skills, skill);
				break;
			case BUFF:
				_buff_skills = ArrayUtils.add(_buff_skills, skill);
				break;
			case STUN:
				_stun_skills = ArrayUtils.add(_stun_skills, skill);
				break;
			case HEAL:
			case HEAL_PERCENT:
			case HOT:
				_heal_skills = ArrayUtils.add(_heal_skills, skill);
				break;
			default:
				break;
		}
	}

	/**
	 * Return the list of all possible drops of this L2NpcTemplate.<BR>
	 * <BR>
	 */
	public L2Drop getDropData()
	{
		return _drop;
	}

	/**
	 * Обнуляет дроплист моба
	 */
	public void clearDropData()
	{
		_drop = null;
	}

	/**
	 * Return the list of all Minions that must be spawn with the L2NpcInstance using this L2NpcTemplate.<BR>
	 * <BR>
	 */
	public GArray<L2MinionData> getMinionData()
	{
		return _minions;
	}

	public TIntObjectHashMap<L2Skill> getSkills()
	{
		return _skills == null ? EMPTY_SKILL_MAP : _skills;
	}

	public void addQuestEvent(final QuestEventType eventType, final Quest q)
	{
		if(_questEvents == null)
			_questEvents = new Quest[QuestEventType.values().length][];

		final int index = eventType.ordinal();
		if(_questEvents[index] == null)
			_questEvents[index] = new Quest[] { q };
		else
		{
			final Quest[] _quests = _questEvents[index];
			final int len = _quests.length;

			// if only one registration per npc is allowed for this event type
			// then only register this NPC if not already registered for the specified event.
			// if a quest allows multiple registrations, then register regardless of count
			if(eventType.isMultipleRegistrationAllowed() || len < 1)
			{
				final Quest[] tmp = new Quest[len + 1];
				for(int i = 0; i < len; i++)
				{
					if(_quests[i].getName().equals(q.getName()))
					{
						_quests[i] = q;
						return;
					}
					tmp[i] = _quests[i];
				}
				tmp[len] = q;
				_questEvents[index] = tmp;
			}
			else
				_log.warning("Quest event not allowed in multiple quests. Skipped addition of Event Type \"" + eventType + "\" for NPC \"" + name + "\" and quest \"" + q.getName() + "\".");
		}
	}

	public Quest[] getEventQuests(final QuestEventType eventType)
	{
		if(_questEvents == null)
			return null;
		return _questEvents[eventType.ordinal()];
	}

	public int getRewardExp()
	{
		return revardExp;
	}

	public void setRace(final int raceId)
	{
		switch (raceId)
		{
			case 1:
				_race = Race.UNDEAD;
				break;
			case 2:
				_race = Race.MAGICCREATURE;
				break;
			case 3:
				_race = Race.BEAST;
				break;
			case 4:
				_race = Race.ANIMAL;
				break;
			case 5:
				_race = Race.PLANT;
				break;
			case 6:
				_race = Race.HUMANOID;
				break;
			case 7:
				_race = Race.SPIRIT;
				break;
			case 8:
				_race = Race.ANGEL;
				break;
			case 9:
				_race = Race.DEMON;
				break;
			case 10:
				_race = Race.DRAGON;
				break;
			case 11:
				_race = Race.GIANT;
				break;
			case 12:
				_race = Race.BUG;
				break;
			case 13:
				_race = Race.FAIRIE;
				break;
			case 14:
				_race = Race.HUMAN;
				break;
			case 15:
				_race = Race.ELVE;
				break;
			case 16:
				_race = Race.DARKELVE;
				break;
			case 17:
				_race = Race.ORC;
				break;
			case 18:
				_race = Race.DWARVE;
				break;
			case 19:
				_race = Race.OTHER;
				break;
			case 20:
				_race = Race.NONLIVING;
				break;
			case 21:
				_race = Race.SIEGEWEAPON;
				break;
			case 22:
				_race = Race.DEFENDINGARMY;
				break;
			case 23:
				_race = Race.MERCENARIE;
				break;
			case 24:
				_race = Race.UNKNOWN;
				break;
			case 25:
				_race = Race.KAMAEL;
				break;
			default:
				_race = Race.NONE;
				break;
		}
	}

	public final Race getRace()
	{
		return _race;
	}

	public boolean isUndead()
	{
		return _race == Race.UNDEAD;
	}

	public void setRateHp(final double newrate)
	{
		rateHp = newrate;
	}

	@Override
	public String toString()
	{
		return "Npc template " + name + "[" + npcId + "]";
	}

	@Override
	public int getNpcId()
	{
		return npcId;
	}

	public final String getJClass()
	{
		return jClass;
	}

	public final AdditionalAIParams getAIParams()
	{
		return _ai_options;
	}

	/**
	 * @return the rhand
	 */
	public int getRhand()
	{
		return rhand;
	}

	/**
	 * @param rhand
	 *            the rhand to set
	 */
	public void setRhand(final int rhand)
	{
		this.rhand = rhand;
	}

	public L2Skill[] getDamageSkills()
	{
		return _dam_skills;
	}

	public L2Skill[] getDotSkills()
	{
		return _dot_skills;
	}

	public L2Skill[] getDebuffSkills()
	{
		return _debuff_skills;
	}

	public L2Skill[] getHealSkills()
	{
		return _heal_skills;
	}

	public L2Skill[] getBuffSkills()
	{
		return _buff_skills;
	}

	public L2Skill[] getStunSkills()
	{
		return _stun_skills;
	}

	public static StatsSet getEmptyStatsSet()
	{
		final StatsSet npcDat = L2CharTemplate.getEmptyStatsSet();
		npcDat.set("npcId", 0);
		npcDat.set("displayId", 0);
		npcDat.set("level", 0);
		npcDat.set("name", "");
		npcDat.set("title", "");
		npcDat.set("sex", "male");
		npcDat.set("type", "");
		npcDat.set("ai_type", "npc");
		npcDat.set("revardExp", 0);
		npcDat.set("revardSp", 0);
		npcDat.set("aggroRange", 0);
		npcDat.set("rhand", 0);
		npcDat.set("lhand", 0);
		npcDat.set("armor", 0);
		npcDat.set("factionId", "");
		npcDat.set("factionRange", 0);
		npcDat.set("drop_herbs", 0);

		return npcDat;
	}
}
