package l2n.game.templates;

import l2n.commons.util.StatsSet;
import l2n.game.templates.L2Armor.ArmorType;
import l2n.game.templates.L2Weapon.WeaponType;

/**
 * This class is dedicated to the management of EtcItem.
 * 
 * @L2System Project
 */
public final class L2EtcItem extends L2Item
{
	public static enum EtcItemType
			implements
			ItemType
	{
		ARROW(0, "Arrow"),
		MATERIAL(1, "Material"),
		PET_COLLAR(2, "PetCollar"),
		POTION(3, "Potion"),
		RECIPE(4, "Recipe"),
		SCROLL(5, "Scroll"),
		QUEST(6, "Quest"),
		MONEY(7, "Money"),
		OTHER(8, "Other"),
		SPELLBOOK(9, "Spellbook"),
		SEED(10, "Seed"),
		BAIT(11, "Bait"),
		SHOT(12, "Shot"),
		BOLT(13, "Bolt"),
		RUNE(15, "Rune"),
		HERB(16, "Herb"); // immediate_effect=1 ex_immediate_effect=1

		private final int _id;
		private final String _name;

		EtcItemType(int id, String name)
		{
			_id = id;
			_name = name;
		}

		@Override
		public long mask()
		{
			return 1L << _id + WeaponType.values().length + ArmorType.values().length;
		}

		@Override
		public String toString()
		{
			return _name;
		}
	}

	/**
	 * Constructor for EtcItem.
	 * 
	 * @see L2Item constructor
	 * @param type
	 *            : L2EtcItemType designating the type of object Etc
	 * @param set
	 *            : StatsSet designating the set of couples (key,value) for description of the Etc
	 */
	public L2EtcItem(EtcItemType type, StatsSet set)
	{
		super(type, set);
	}

	/**
	 * Returns the type of Etc Item
	 * 
	 * @return L2EtcItemType
	 */
	@Override
	public EtcItemType getItemType()
	{
		return (EtcItemType) super.type;
	}

	/**
	 * Returns the ID of the Etc item after applying the mask.
	 * 
	 * @return int : ID of the EtcItem
	 */
	@Override
	public long getItemMask()
	{
		return getItemType().mask();
	}

	@Override
	public final boolean isShadowItem()
	{
		return false;
	}
}
