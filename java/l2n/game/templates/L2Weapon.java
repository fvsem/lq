package l2n.game.templates;

import l2n.Config;
import l2n.commons.util.StatsSet;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncTemplate;
import l2n.game.tables.SkillTable;
import l2n.util.Log;
import l2n.util.Rnd;

/**
 * @author L2System Project <BR>
 *         Description of Weapon Type
 */
public final class L2Weapon extends L2Item
{
	private final int _soulShotCount;
	private final int _spiritShotCount;
	private final int _pDam;
	private final int _rndDam;
	private final int _atkReuse;
	private final int _mpConsume;
	private final int _mDam;
	private final int _aSpd;
	private final int _critical;
	private final double _accmod;
	private final double _evsmod;
	private final int _sDef;
	private final int _rShld;

	private final boolean _isMonsterWeapon;

	public static enum WeaponType
			implements
			ItemType
	{
		NONE(1, "Shield", null), // mask 2
		SWORD(2, "Sword", Stats.SWORD_WPN_RECEPTIVE), // mask 4
		BLUNT(3, "Blunt", Stats.BLUNT_WPN_RECEPTIVE), // mask 8
		DAGGER(4, "Dagger", Stats.DAGGER_WPN_RECEPTIVE), // mask 16
		BOW(5, "Bow", Stats.BOW_WPN_RECEPTIVE), // mask 32
		POLE(6, "Pole", Stats.POLE_WPN_RECEPTIVE), // mask 64
		ETC(7, "Etc", null), // mask 128
		FIST(8, "Fist", Stats.FIST_WPN_RECEPTIVE), // mask 256
		DUAL(9, "Dual Sword", Stats.DUAL_WPN_RECEPTIVE), // mask 512
		DUALFIST(10, "Dual Fist", Stats.FIST_WPN_RECEPTIVE), // mask 1024
		BIGSWORD(11, "Big Sword", Stats.SWORD_WPN_RECEPTIVE), // mask 2048
		PET(12, "Pet", Stats.FIST_WPN_RECEPTIVE), // mask 4096
		ROD(13, "Rod", null), // mask 8192
		BIGBLUNT(14, "Big Blunt", Stats.BLUNT_WPN_RECEPTIVE), // mask 16384
		CROSSBOW(15, "Crossbow", Stats.CROSSBOW_WPN_RECEPTIVE), // mask 32768
		RAPIER(16, "Rapier", Stats.DAGGER_WPN_RECEPTIVE), // mask 65536
		ANCIENTSWORD(17, "Ancient Sword", Stats.SWORD_WPN_RECEPTIVE), // mask 131072
		DUALDAGGER(18, "Dual Dagger", Stats.DAGGER_WPN_RECEPTIVE); // mask 262144

		private final int _id;
		private final String _name;
		private final Stats _defence;

		/**
		 * Constructor of the L2WeaponType.
		 * 
		 * @param id
		 *            : int designating the ID of the WeaponType
		 * @param name
		 *            : String designating the name of the WeaponType
		 */
		private WeaponType(int id, String name, Stats defence)
		{
			_id = id;
			_name = name;
			_defence = defence;
		}

		/**
		 * Returns the ID of the item after applying the mask.
		 * 
		 * @return int : ID of the item
		 */
		@Override
		public long mask()
		{
			return 1L << _id;
		}

		public Stats getDefence()
		{
			return _defence;
		}

		/**
		 * Returns the name of the WeaponType
		 * 
		 * @return String
		 */
		@Override
		public String toString()
		{
			return _name;
		}
	}

	/**
	 * Constructor for Weapon.<BR>
	 * <BR>
	 * <U><I>Variables filled :</I></U><BR>
	 * <LI>_soulShotCount & _spiritShotCount</LI> <LI>_pDam & _mDam & _rndDam</LI> <LI>_critical</LI> <LI>_hitModifier</LI> <LI>_avoidModifier</LI> <LI>_shieldDes & _shieldDefRate</LI> <LI>_atkSpeed & _AtkReuse</LI> <LI>_mpConsume</LI>
	 * 
	 * @param type
	 *            : L2ArmorType designating the type of armor
	 * @param set
	 *            : StatsSet designating the set of couples (key,value) caracterizing the armor
	 * @see L2Item constructor
	 */
	public L2Weapon(WeaponType type, StatsSet set)
	{
		super(type, set);
		_soulShotCount = set.getInteger("soulshots");
		_spiritShotCount = set.getInteger("spiritshots");
		_pDam = set.getInteger("p_dam");
		_rndDam = set.getInteger("rnd_dam");
		// TODO у разных луков, разное время отката у выстрелов
		_atkReuse = set.getInteger("atk_reuse", type == WeaponType.BOW ? Config.BOW_ATK_SPEED : type == WeaponType.CROSSBOW ? Config.CROSS_BOW_ATK_SPEED : 0);
		_mpConsume = set.getInteger("mp_consume");
		_mDam = set.getInteger("m_dam");
		_aSpd = set.getInteger("atk_speed");
		_critical = set.getInteger("critical");
		_accmod = set.getDouble("hit_modify");
		_evsmod = set.getDouble("avoid_modify");
		_sDef = set.getInteger("shield_def");
		_rShld = set.getInteger("shield_def_rate");

		if(_addname.length() > 0 && _skills == null)
			Log.addDev("id=" + _itemId + " name=" + _name + " [" + _addname + "]", "dev_item_unimplemented_sa", false);

		if(type == WeaponType.POLE)
			attachSkill(SkillTable.getInstance().getInfo(3599, 1)); // ничего не делает, просто для красоты

		int sId = set.getInteger("enchant4_skill_id");
		int sLv = set.getInteger("enchant4_skill_lvl");
		if(sId > 0 && sLv > 0)
			_enchant4Skill = SkillTable.getInstance().getInfo(sId, sLv);
		if(_enchant4Skill == null && type == WeaponType.RAPIER)
			_enchant4Skill = SkillTable.getInstance().getInfo(3426, 1);

		if(_pDam != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.POWER_ATTACK, 0x10, _pDam));
		if(_mDam != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.MAGIC_ATTACK, 0x10, _mDam));
		if(_critical != 0)
			attachFunction(new FuncTemplate(null, null, "Set", Stats.CRITICAL_BASE, 0x08, _critical * 10));
		if(_aSpd != 0)
			attachFunction(new FuncTemplate(null, null, "Set", Stats.ATK_BASE, 0x08, _aSpd));
		if(_sDef != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.SHIELD_DEFENCE, 0x10, _sDef));
		if(_accmod != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.ACCURACY_COMBAT, 0x10, _accmod));
		if(_evsmod != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.EVASION_RATE, 0x10, _evsmod));
		if(_rShld != 0)
			attachFunction(new FuncTemplate(null, null, "Add", Stats.SHIELD_RATE, 0x10, _rShld));

		if(_crystalType != Grade.NONE)
		{
			if(_sDef > 0)
			{
				attachFunction(new FuncTemplate(null, null, "Enchant", Stats.SHIELD_DEFENCE, 0x0C, 0));
				if(set.getInteger("type2") == L2Item.TYPE2_SHIELD_ARMOR)
					attachFunction(new FuncTemplate(null, null, "Enchant", Stats.MAX_HP, 0x0C, 0));
			}
			if(_pDam > 0)
				attachFunction(new FuncTemplate(null, null, "Enchant", Stats.POWER_ATTACK, 0x0C, 0));
			if(_mDam > 0)
				attachFunction(new FuncTemplate(null, null, "Enchant", Stats.MAGIC_ATTACK, 0x0C, 0));
		}

		if(_icon.contains("weapon_monster"))
			_isMonsterWeapon = true;
		else
			_isMonsterWeapon = false;
	}

	/**
	 * Returns the type of Weapon
	 * 
	 * @return L2WeaponType
	 */
	@Override
	public WeaponType getItemType()
	{
		return (WeaponType) type;
	}

	/**
	 * Возвращает базовую скорость атаки
	 */
	public int getBaseSpeed()
	{
		return _aSpd;
	}

	public int getCritical()
	{
		return _critical;
	}

	/**
	 * Returns the ID of the Etc item after applying the mask.
	 * 
	 * @return int : ID of the Weapon
	 */
	@Override
	public long getItemMask()
	{
		return getItemType().mask();
	}

	/**
	 * Returns the quantity of SoulShot used.
	 * 
	 * @return int
	 */
	public int getSoulShotCount()
	{
		return _soulShotCount;
	}

	/**
	 * Returns the quatity of SpiritShot used.
	 * 
	 * @return int
	 */
	public int getSpiritShotCount()
	{
		return _spiritShotCount;
	}

	/**
	 * Returns the physical damage.
	 * 
	 * @return int
	 */
	public int getPDamage()
	{
		return _pDam;
	}

	/**
	 * Returns the random damage inflicted by the weapon
	 * 
	 * @return int
	 */
	public int getRandomDamage()
	{
		return _rndDam;
	}

	/**
	 * Return the Attack Reuse Delay of the L2Weapon.<BR>
	 * <BR>
	 * 
	 * @return int
	 */
	public int getAttackReuseDelay()
	{
		return _atkReuse;
	}

	/**
	 * Returns the magical damage inflicted by the weapon
	 * 
	 * @return int
	 */
	public int getMDamage()
	{
		return _mDam;
	}

	/**
	 * Returns the MP consumption with the weapon
	 * 
	 * @return int
	 */
	public int getMpConsume()
	{
		return _mpConsume;
	}

	public void getEffect(boolean critNotCast, L2Character effector, L2Character effected, boolean offensive)
	{
		if(effector == null || effected == null)
			return;

		if(_skillOnAction == null)
			return;

		if(_skillOnCritNotCast != critNotCast || _offensive != offensive)
			return;

		if(!Rnd.chance(_chance))
			return;

		if((effected.isRaid() || effected.isPlayer()) && (_skillOnAction.getSkillType() == SkillType.PARALYZE || _skillOnAction.getSkillType() == SkillType.ROOT || _skillOnAction.getSkillType() == SkillType.STUN || _skillOnAction.getSkillType() == SkillType.MUTE))
			_skillOnAction.getEffects(effector, effected, true, false);
		else
			_skillOnAction.getEffects(effector, effected, false, false);

		effector.sendPacket(new SystemMessage(SystemMessage.S1_HAS_SUCCEEDED).addString("Your weapon's special ability"));
	}

	public boolean isMonsterWeapon()
	{
		return _isMonsterWeapon;
	}

	/**
	 * Возвращает разницу между длиной этого оружия и стандартной, то есть x-40
	 */
	public int getAttackRange()
	{
		switch (getItemType())
		{
			case BOW:
				return 460;
			case CROSSBOW:
				return 360;
			case POLE:
				return 40;
			default:
				return 0;
		}
	}
}
