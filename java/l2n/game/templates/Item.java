package l2n.game.templates;

import l2n.commons.util.StatsSet;

/**
 * This class ...
 * 
 * @L2System Project
 */
public class Item
{
	public int id;
	@SuppressWarnings("rawtypes")
	public Enum type;
	public String name;
	public StatsSet set;
	public int currentLevel;
	public L2Item item;
}
