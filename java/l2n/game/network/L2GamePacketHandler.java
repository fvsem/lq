package l2n.game.network;

import l2n.Config;
import l2n.commons.network.*;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.*;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.util.HexUtil;
import l2n.util.Log;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.logging.Logger;

public final class L2GamePacketHandler extends TCPHeaderHandler<L2GameClient> implements IPacketHandler<L2GameClient, L2GameClientPacket, L2GameServerPacket>, IClientFactory<L2GameClient, L2GameClientPacket, L2GameServerPacket>
{
	private static final Logger _log = Logger.getLogger(L2GamePacketHandler.class.getName());

	public L2GamePacketHandler()
	{
		super(null);
	}

	@Override
	public L2GameClientPacket handlePacket(final ByteBuffer buff, final L2GameClient client, int opcode)
	{
		if(client == null || client.isPacketsFailed())
			return null;

		L2GameClientPacket msg = null;
		int id2 = 0;
		int id3 = 0;

		if(Config.PROTOCOL_ENABLE_OBFUSCATION)
			opcode = client.getObfuscator().decodeSingleOpcode(opcode);

		switch (client.getState())
		{
			case CONNECTED:
				switch (opcode)
				{
					case 0x00:
						msg = new RequestStatus();
						break;
					case 0x0e:
						msg = new ProtocolVersion();
						break;
					case 0x2b:
						msg = new AuthLogin();
						break;
					case 0xcb:
						msg = new ReplyGameGuardQuery();
						break;
					default:
						printDebug(buff, client, true, opcode);
						break;
				}
				break;
			case AUTHED:
				switch (opcode)
				{
					case 0x00:
						msg = new Logout();
						break;
					case 0x0c:
						msg = new CharacterCreate(); // RequestCharacterCreate();
						break;
					case 0x0d:
						msg = new CharacterDelete(); // RequestCharacterDelete();
						break;
					case 0x0f:
						break;
					case 0x12:
						msg = new CharacterSelected(); // CharacterSelect();
						break;
					case 0x13:
						msg = new NewCharacter(); // RequestNewCharacter();
						break;
					case 0x57:
						break;
					case 0x59:
						// wtf
						break;
					case 0x7b:
						msg = new CharacterRestore(); // RequestCharacterRestore();
						break;
					case 0xa6: // RequestSkillCoolTime Wtf?
						break;
					case 0xcb:
						msg = new ReplyGameGuardQuery();
						break;
					case 0xd0:
						if(!buff.hasRemaining())
						{
							handleIncompletePacket(client);
							break;
						}
						id2 = buff.getShort() & 0xffff;
						if(Config.PROTOCOL_ENABLE_OBFUSCATION)
							id2 = client.getObfuscator().decodeDoubleOpcode(id2);
						switch (id2)
						{
							case 0x36:
								msg = new RequestGotoLobby();
								break;
							case 0x5a:
								msg = new RequestExCubeGameChangeTeam();
								break;
							default:
								printDebug(buff, client, true, opcode, id2);
								break;
						}
						break;
					default:
						printDebug(buff, client, true, opcode);
						break;
				}
				break;
			case IN_GAME:
				switch (opcode)
				{
					case 0x00:
						msg = new Logout();
						break;
					case 0x01:
						msg = new AttackRequest();
						break;
					case 0x03:
						msg = new RequestStartPledgeWar();
						break;
					case 0x04:
						msg = new RequestReplyStartPledgeWar();
						break;
					case 0x05:
						msg = new RequestStopPledgeWar();
						break;
					case 0x06:
						msg = new RequestReplyStopPledgeWar();
						break;
					case 0x07:
						msg = new RequestSurrenderPledgeWar();
						break;
					case 0x08:
						msg = new RequestReplySurrenderPledgeWar();
						break;
					case 0x09:
						msg = new RequestSetPledgeCrest();
						break;
					case 0x0b:
						msg = new RequestGiveNickName();
						break;
					case 0x0f:
						msg = new MoveBackwardToLocation();
						break;
					case 0x10:
						break;
					case 0x11:
						msg = new EnterWorld();
						break;
					case 0x12:
						break;
					case 0x13:
						break;
					case 0x14:
						msg = new RequestItemList();
						break;
					case 0x15:
						break;
					case 0x16:
						msg = new RequestUnEquipItem();
						break;
					case 0x17:
						msg = new RequestDropItem();
						break;
					case 0x19:
						msg = new UseItem(); // RequestUseItem();
						break;
					case 0x1a:
						msg = new TradeRequest();
						break;
					case 0x1b:
						msg = new AddTradeItem(); // RequestAddTradeItem();
						break;
					case 0x1c:
						msg = new TradeDone(); // RequestTradeDone();
						break;
					case 0x1f:
						msg = new Action();
						break;
					case 0x22:
						msg = new RequestLinkHtml();
						break;
					case 0x23:
						msg = new RequestBypassToServer();
						break;
					case 0x24:
						msg = new RequestBBSwrite(); // RequestBBSWrite();
						break;
					case 0x25:
						msg = new RequestCreatePledge();
						break;
					case 0x26:
						msg = new RequestJoinPledge();
						break;
					case 0x27:
						msg = new RequestAnswerJoinPledge();
						break;
					case 0x28:
						msg = new RequestWithdrawalPledge();
						break;
					case 0x29:
						msg = new RequestOustPledgeMember();
						break;
					case 0x2c:
						msg = new RequestGetItemFromPet();
						break;
					case 0x2e:
						msg = new RequestAllyInfo();
						break;
					case 0x2f:
						msg = new RequestCrystallizeItem();
						break;
					case 0x30:
						break;
					case 0x31:
						msg = new SetPrivateStoreList();
						break;
					case 0x32:
						break;
					case 0x33:
						break;
					case 0x34:
						msg = new RequestSocialAction(); // SocialAction();
						break;
					case 0x35:
						break;
					case 0x36:
						break;
					case 0x37:
						msg = new RequestSellItem();
						break;
					case 0x38:
						msg = new RequestMagicSkillList();
						break;
					case 0x39:
						msg = new RequestMagicSkillUse();
						break;
					case 0x3a:
						msg = new Appearing(); // (after death)
						break;
					case 0x3b:
						if(Config.ALLOW_WAREHOUSE)
							msg = new SendWareHouseDepositList();
						break;
					case 0x3c:
						msg = new SendWareHouseWithDrawList();
						break;
					case 0x3d:
						msg = new RequestShortCutReg();
						break;
					case 0x3e:
						break;
					case 0x3f:
						msg = new RequestShortCutDel();
						break;
					case 0x40:
						msg = new RequestBuyItem();
						break;
					case 0x41:
						break;
					case 0x42:
						msg = new RequestJoinParty();
						break;
					case 0x43:
						msg = new RequestAnswerJoinParty();
						break;
					case 0x44:
						msg = new RequestWithDrawalParty();
						break;
					case 0x45:
						msg = new RequestOustPartyMember();
						break;
					case 0x46:
						break;
					case 0x47:
						msg = new CannotMoveAnymore();
						break;
					case 0x48:
						msg = new RequestTargetCanceld(); // RequestTargetCancel();
						break;
					case 0x49:
						msg = new Say2C();
						break;
					case 0x4a:
						if(!buff.hasRemaining())
						{
							handleIncompletePacket(client);
							break;
						}
						id2 = buff.get() & 0xff;
						if(Config.PROTOCOL_ENABLE_OBFUSCATION)
							id2 = client.getObfuscator().decodeDoubleOpcode(id2);
						switch (id2)
						{
							case 0x00:
								break;
							case 0x01:
								break;
							case 0x02:
								break;
							case 0x03:
								break;
							default:
								printDebug(buff, client, true, opcode, id2);
								break;
						}
						break;
					case 0x4d:
						msg = new RequestPledgeMemberList();
						break;
					case 0x4f:
						break;
					case 0x50:
						msg = new RequestSkillList(); // Format: c ?
						break;
					case 0x52:
						msg = new MoveWithDelta(); // Format: cddd ?
						break;
					case 0x53:
						msg = new RequestGetOnVehicle(); // GetOnVehicle();
						break;
					case 0x54:
						msg = new RequestGetOffVehicle(); // GetOffVehicle();
						break;
					case 0x55:
						msg = new AnswerTradeRequest();
						break;
					case 0x56:
						msg = new RequestActionUse();
						break;
					case 0x57:
						msg = new RequestRestart();
						break;
					case 0x58:
						msg = new RequestDominionInfo(); // //TODO
						break;
					case 0x59:
						msg = new ValidatePosition();
						break;
					case 0x5a:
						break;
					case 0x5b:
						msg = new StartRotating();
						break;
					case 0x5c:
						msg = new FinishRotating();
						break;
					case 0x5e:
						msg = new RequestShowBoard(); // RequestShowboard();
						break;
					case 0x5f:
						msg = new RequestEnchantItem();
						break;
					case 0x60:
						msg = new RequestDestroyItem();
						break;
					case 0x62:
						msg = new RequestQuestList();
						break;
					case 0x63:
						msg = new RequestDestroyQuest(); // RequestDestroyQuest();
						break;
					case 0x65:
						msg = new RequestPledgeInfo();
						break;
					case 0x66:
						msg = new RequestPledgeExtendedInfo();
						break;
					case 0x67:
						msg = new RequestPledgeCrest();
						break;
					case 0x6b:
						msg = new RequestSendL2FriendSay(); // ?
						break;
					case 0x6c:
						msg = new RequestOpenMinimap(); // RequestOpenMinimap();
						break;
					case 0x6d:
						break;
					case 0x6e:
						msg = new RequestReload();
						break;
					case 0x6f:
						msg = new RequestHennaEquip();
						break;
					case 0x70:
						msg = new RequestHennaUnequipList();
						break;
					case 0x71:
						msg = new RequestHennaUnequipInfo();
						break;
					case 0x72:
						msg = new RequestHennaUnequip();
						break;
					case 0x73:
						msg = new RequestAquireSkillInfo();
						break;
					case 0x74:
						msg = new SendBypassBuildCmd();
						break;
					case 0x75:
						msg = new RequestMoveToLocationInVehicle();
						break;
					case 0x76:
						msg = new CannotMoveAnymoreInVehicle();
						break;
					case 0x77:
						msg = new RequestFriendInvite();
						break;
					case 0x78:
						msg = new RequestFriendAddReply();
						break;
					case 0x79:
						msg = new RequestFriendList();
						break;
					case 0x7a:
						msg = new RequestFriendDel();
						break;
					case 0x7c:
						msg = new RequestAquireSkill();
						break;
					case 0x7d:
						msg = new RequestRestartPoint();
						break;
					case 0x7e:
						msg = new RequestGMCommand();
						break;
					case 0x7f:
						msg = new RequestPartyMatchConfig(); // format: cddd ?
						break;
					case 0x80:
						msg = new RequestPartyMatchList(); // format: cdddddS ?
						break;
					case 0x81:
						msg = new RequestPartyMatchDetail(); // format: cdddd ?
						break;
					case 0x83:
						msg = new RequestPrivateStoreBuy();
						break;
					case 0x84:
						break;
					case 0x85:
						msg = new RequestTutorialLinkHtml();
						break;
					case 0x86:
						msg = new RequestTutorialPassCmdToServer();
						break;
					case 0x87:
						msg = new RequestTutorialQuestionMark(); // RequestTutorialQuestionMarkPressed();
						break;
					case 0x88:
						msg = new RequestExSetTutorial(); // тоже самое что и RequestTutorialClientEvent();
						break;
					case 0x89:
						msg = new RequestPetition();
						break;
					case 0x8a:
						msg = new RequestPetitionCancel();
						break;
					case 0x8b:
						msg = new RequestGmList(); // RequestGMList();
						break;
					case 0x8c:
						msg = new RequestJoinAlly();
						break;
					case 0x8d:
						msg = new RequestAnswerJoinAlly();
						break;
					case 0x8e:
						msg = new RequestWithdrawAlly();
						break;
					case 0x8f:
						msg = new RequestOustAlly();
						break;
					case 0x90:
						msg = new RequestDismissAlly();
						break;
					case 0x91:
						msg = new RequestSetAllyCrest();
						break;
					case 0x92:
						msg = new RequestAllyCrest();
						break;
					case 0x93:
						msg = new RequestChangePetName();
						break;
					case 0x94:
						msg = new RequestPetUseItem();
						break;
					case 0x95:
						msg = new RequestGiveItemToPet();
						break;
					case 0x96:
						msg = new RequestPrivateStoreQuitSell(); // ?
						break;
					case 0x97:
						msg = new SetPrivateStoreMsgSell(); // ?
						break;
					case 0x98:
						msg = new RequestPetGetItem();
						break;
					case 0x99:
						msg = new RequestPrivateStoreBuyManage();
						break;
					case 0x9a:
						msg = new SetPrivateStoreBuyList(); // ?
						break;
					case 0x9c:
						msg = new RequestPrivateStoreQuitBuy();
						break;
					case 0x9d:
						msg = new SetPrivateStoreMsgBuy();
						break;
					case 0x9f:
						msg = new SendPrivateStoreBuyList(); // ?
						break;
					case 0xa0:
						break;
					case 0xa6:
						break;
					case 0xa7:
						break;
					case 0xa8:
						break;
					case 0xa9:
						msg = new RequestBlock();
						break;
					case 0xaa:
						break;
					case 0xab:
						msg = new RequestSiegeAttackerList(); // RequestCastleSiegeAttackerList();
						break;
					case 0xac:
						msg = new RequestSiegeDefenderList(); // RequestCastleSiegeDefenderList();
						break;
					case 0xad:
						msg = new RequestJoinSiege(); // RequestJoinCastleSiege();
						break;
					case 0xae:
						msg = new RequestConfirmSiegeWaitingList(); // RequestConfirmCastleSiegeWaitingList();
						break;
					case 0xaf:
						break;
					case 0xb0:
						msg = new RequestMultiSellChoose();
						break;
					case 0xb1:
						msg = new NetPing();
						break;
					case 0xb2:
						msg = new RequestRemainTime();
						break;
					case 0xb3:
						msg = new BypassUserCmd();
						break;
					case 0xb4:
						msg = new SnoopQuit();
						break;
					case 0xb5:
						msg = new RequestRecipeBookOpen();
						break;
					case 0xb6:
						msg = new RequestRecipeItemDelete();
						break;
					case 0xb7:
						msg = new RequestRecipeItemMakeInfo();
						break;
					case 0xb8:
						msg = new RequestRecipeItemMakeSelf();
						break;
					case 0xb9:
						break;
					case 0xba:
						msg = new RequestRecipeShopMessageSet();
						break;
					case 0xbb:
						msg = new RequestRecipeShopListSet();
						break;
					case 0xbc:
						msg = new RequestRecipeShopManageQuit();
						break;
					case 0xbd:
						msg = new RequestRecipeShopManageCancel();
						break;
					case 0xbe:
						msg = new RequestRecipeShopMakeInfo();
						break;
					case 0xbf:
						msg = new RequestRecipeShopMakeDo();
						break;
					case 0xc0:
						msg = new RequestRecipeShopSellList();
						break;
					case 0xc1:
						msg = new ObserverReturn(); // RequestObserverEndPacket();
						break;
					case 0xc2:
						msg = new VoteSociality();
						break;
					case 0xc3:
						msg = new RequestHennaList();
						break;
					case 0xc4:
						msg = new RequestHennaItemInfo();
						break;
					case 0xc5:
						msg = new RequestBuySeed();
						break;
					case 0xc6:
						msg = new ConfirmDlg();
						break;
					case 0xc7:
						msg = new RequestPreviewItem();
						break;
					case 0xc8:
						msg = new RequestSSQStatus();
						break;
					case 0xc9:
						break;
					case 0xcb:
						msg = new ReplyGameGuardQuery();
						break;
					case 0xcc:
						msg = new RequestPledgePower();
						break;
					case 0xcd:
						msg = new RequestMakeMacro();
						break;
					case 0xce:
						msg = new RequestDeleteMacro();
						break;
					case 0xcf:
						msg = new RequestProcureCrop();
						break;
					case 0xd0:
						if(!buff.hasRemaining())
						{
							handleIncompletePacket(client);
							break;
						}
						id2 = buff.getShort() & 0xffff;
						if(Config.PROTOCOL_ENABLE_OBFUSCATION)
							id2 = client.getObfuscator().decodeDoubleOpcode(id2);
						switch (id2)
						{
							case 0x01:
								msg = new RequestManorList();
								break;
							case 0x02:
								msg = new RequestProcureCropList();
								break;
							case 0x03:
								msg = new RequestSetSeed();
								break;
							case 0x04:
								msg = new RequestSetCrop();
								break;
							case 0x05:
								msg = new RequestWriteHeroWords();
								break;
							case 0x06:
								msg = new RequestExAskJoinMPCC();
								break;
							case 0x07:
								msg = new RequestExAcceptJoinMPCC();
								break;
							case 0x08:
								msg = new RequestExOustFromMPCC();
								break;
							case 0x09:
								msg = new RequestOustFromPartyRoom();
								break;
							case 0x0a:
								msg = new RequestDismissPartyRoom();
								break;
							case 0x0b:
								msg = new RequestWithdrawPartyRoom();
								break;
							case 0x0c:
								msg = new RequestHandOverPartyMaster();
								break;
							case 0x0d:
								msg = new RequestAutoSoulShot();
								break;
							case 0x0e:
								msg = new RequestExEnchantSkillInfo();
								break;
							case 0x0f:
								msg = new RequestExEnchantSkill();
								break;
							case 0x10:
								msg = new RequestPledgeCrestLarge(); // RequestExPledgeCrestLarge();
								break;
							case 0x11:
								msg = new RequestSetPledgeCrestLarge(); // RequestExSetPledgeCrestLarge();
								break;
							case 0x12:
								msg = new RequestPledgeSetAcademyMaster();
								break;
							case 0x13:
								msg = new RequestPledgePowerGradeList();
								break;
							case 0x14:
								msg = new RequestPledgeMemberPowerInfo();
								break;
							case 0x15:
								msg = new RequestPledgeSetMemberPowerGrade();
								break;
							case 0x16:
								msg = new RequestPledgeMemberInfo();
								break;
							case 0x17:
								msg = new RequestPledgeWarList();
								break;
							case 0x18:
								msg = new RequestExFishRanking();
								break;
							case 0x19:
								msg = new RequestPCCafeCouponUse();
								break;
							case 0x1a:
								break;
							case 0x1b:
								msg = new RequestDuelStart();
								break;
							case 0x1c:
								msg = new RequestDuelAnswerStart();
								break;
							case 0x1d:
								msg = new RequestExSetTutorial();
								break;
							case 0x1e:
								msg = new RequestExRqItemLink(); // chat item links
								break;
							case 0x1f:
								break;
							case 0x20:
								msg = new RequestMoveToLocationInAirShip();
								break;
							case 0x21:
								msg = new RequestKeyMapping();
								break;
							case 0x22:
								msg = new RequestSaveKeyMapping();
								break;
							case 0x23:
								msg = new RequestExRemoveItemAttribute();
								break;
							case 0x24:
								msg = new RequestSaveInventoryOrder(); // сохранение порядка инвентаря
								break;
							case 0x25:
								msg = new RequestExitPartyMatchingWaitingRoom();
								break;
							case 0x26:
								msg = new RequestConfirmTargetItem();
								break;
							case 0x27:
								msg = new RequestConfirmRefinerItem();
								break;
							case 0x28:
								msg = new RequestConfirmGemStone();
								break;
							case 0x29:
								msg = new RequestOlympiadObserverEnd();
								break;
							case 0x2a:
								msg = new RequestCursedWeaponList();
								break;
							case 0x2b:
								msg = new RequestCursedWeaponLocation();
								break;
							case 0x2c:
								msg = new RequestPledgeReorganizeMember();
								break;
							case 0x2d:
								msg = new RequestExMPCCShowPartyMembersInfo();
								break;
							case 0x2e:
								msg = new RequestOlympiadMatchList();
								break;
							case 0x2f:
								msg = new RequestAskJoinPartyRoom();
								break;
							case 0x30:
								msg = new AnswerJoinPartyRoom();
								break;
							case 0x31:
								msg = new RequestListPartyMatchingWaitingRoom();
								break;
							case 0x32:
								msg = new RequestExEnchantSkillSafe();
								break;
							case 0x33:
								msg = new RequestExEnchantSkillUntrain();
								break;
							case 0x34:
								msg = new RequestExEnchantSkillRouteChange(); // format: (ch)dddd ?
								break;
							case 0x35:
								msg = new RequestEnchantItemAttribute();
								break;
							case 0x36:
								msg = new RequestExGetOnAirShip();
								break;
							case 0x38:
								msg = new RequestExMoveToLocationAirShip(); // format: (ch)ddd ?
								break;
							case 0x39:
								msg = new RequestBidItemAuction(); // format: (ch)dd ?
								break;
							case 0x3a:
								msg = new RequestInfoItemAuction(); // format: (ch)d ?
								break;
							case 0x3b:
								break;
							case 0x3c:
								msg = new RequestAllCastleInfo();
								break;
							case 0x3d:
								msg = new RequestAllFortressInfo();
								break;
							case 0x3e:
								msg = new RequestAllAgitInfo();
								break;
							case 0x3f:
								msg = new RequestFortressSiegeInfo(); // ?
								break;
							case 0x40:
								msg = new RequestGetBossRecord();
								break;
							case 0x41:
								msg = new RequestRefine();
								break;
							case 0x42:
								msg = new RequestConfirmCancelItem();
								break;
							case 0x43:
								msg = new RequestRefineCancel();
								break;
							case 0x44:
								msg = new RequestExMagicSkillUseGround();
								break;
							case 0x45:
								msg = new RequestDuelSurrender(); // RequestDuelSurrender();
								break;
							case 0x46:
								msg = new RequestExEnchantSkillInfoDetail(); // format: (ch)ddd ?
								break;
							case 0x48:
								msg = new RequestFortressMapInfo(); // format: (ch)d ?
								break;
							case 0x49:
								msg = new RequestPVPMatchRecord(); // TODO
								break;
							case 0x4a:
								msg = new SetPrivateStoreWholeMsg();
								break;
							case 0x4b:
								msg = new RequestDispel();
								break;
							case 0x4c:
								msg = new RequestExTryToPutEnchantTargetItem();
								break;
							case 0x4d:
								msg = new RequestExTryToPutEnchantSupportItem();
								break;
							case 0x4e:
								msg = new RequestExCancelEnchantItem();
								break;
							case 0x4f:
								msg = new RequestChangeNicknameColor();
								break;
							case 0x50:
								msg = new RequestResetNickname();
								break;
							case 0x51:
								if(buff.remaining() < 4)
								{
									handleIncompletePacket(client);
									break;
								}
								id3 = buff.getInt();
								if(Config.PROTOCOL_ENABLE_OBFUSCATION)
									id3 = client.getObfuscator().decodeTripleOpcode(id3);
								switch (id3)
								{
									case 0x00:
										msg = new RequestBookMarkSlotInfo();
										break;
									case 0x01:
										msg = new RequestSaveBookMarkSlot();
										break;
									case 0x02:
										msg = new RequestModifyBookMarkSlot();
										break;
									case 0x03:
										msg = new RequestDeleteBookMarkSlot();
										break;
									case 0x04:
										msg = new RequestTeleportBookMark();
										break;
									case 0x05:
										break;
									default:
										printDebug(buff, client, true, opcode, id2, id3);
										break;
								}
								break;
							case 0x52:
								break;
							case 0x53:
								msg = new RequestJump();
								break;
							case 0x54:
								break;
							case 0x55:
								break;
							case 0x56:
								break;
							case 0x57:
								msg = new RequestJoinDominionWar();
								break;
							case 0x58:
								msg = new RequestDominionInfo();
								break;
							case 0x59:
								break;
							case 0x5a:
								msg = new RequestExCubeGameChangeTeam();
								break;
							case 0x5b:
								msg = new RequestExEndScenePlayer();
								break;
							case 0x5c:
								msg = new RequestExCubeGameReadyAnswer();
								break;
							case 0x5d:
								break;
							case 0x5e:
								break;
							case 0x5f:
								break;
							case 0x60:
								break;
							case 0x61:
								break;
							case 0x62:
								break;
							case 0x63:
								msg = new RequestSeedPhase();
								break;
							case 0x64:
								break;
							case 0x65:
								msg = new RequestPostItemList(); // mail system
								break;
							case 0x66:
								msg = new RequestSendPost(); // mail system
								break;
							case 0x67:
								msg = new RequestRequestReceivedPostList(); // mail system
								break;
							case 0x68:
								msg = new RequestDeleteReceivedPost(); // mail system
								break;
							case 0x69:
								msg = new RequestRequestReceivedPost(); // mail system
								break;
							case 0x6a:
								msg = new RequestReceivePost(); // mail system
								break;
							case 0x6b:
								msg = new RequestRejectPost(); // mail system
								break;
							case 0x6c:
								msg = new RequestSentPostList(); // mail system
								break;
							case 0x6d:
								msg = new RequestDeleteSentPost(); // mail system
								break;
							case 0x6e:
								msg = new RequestRequestSentPost(); // mail system
								break;
							case 0x6f:
								msg = new RequestCancelSentPost(); // mail system
								break;
							case 0x70:
								break;
							case 0x71:
								break;
							case 0x72:
								break;
							case 0x73:
								break;
							case 0x75:
								msg = new RequestRefundItem();
								break;
							case 0x76:
								msg = new RequestBuySellUIClose();
								break;
							case 0x77:
								break;
							case 0x78:
								break;
							case 0x79:
								break;
							case 0x7a:
								break;
							case 0x7b:
								break;
							case 0x7c:
								break;
							case 0x7d:
								msg = new BrEventRankerList();
								break;
							case 0x7e:
								break;
							case 0x7f:
								break;
							case 0x80:
								break;
							default:
								printDebug(buff, client, true, opcode, id2);
								break;
						}
						break;

					default:
						printDebug(buff, client, true, opcode);
						break;
				}
				break;
		}
		return msg;
	}

	// impl
	@Override
	public L2GameClient create(final MMOConnection<L2GameClient, L2GameClientPacket, L2GameServerPacket> con)
	{
		return new L2GameClient(con);
	}

	public void handleIncompletePacket(final L2GameClient client)
	{
		final L2Player activeChar = client.getActiveChar();

		if(activeChar == null)
			_log.warning("Packet not completed. Maybe cheater. IP: " + client.getIpAddr() + ", account: " + client.getLoginName());
		else
			_log.warning("Packet not completed. Maybe cheater. IP: " + client.getIpAddr() + ", account: " + client.getLoginName() + ", character:" + activeChar.getName());

		client.onClientPacketFail();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public HeaderInfo handleHeader(final SelectionKey key, final ByteBuffer buf)
	{
		if(buf.remaining() >= 2)
		{
			final int dataPending = (buf.getShort() & 0xffff) - 2;
			final L2GameClient client = ((MMOConnection<L2GameClient, L2GameClientPacket, L2GameServerPacket>) key.attachment()).getClient();
			return getHeaderInfoReturn().set(0, dataPending, false, client);
		}
		final L2GameClient client = ((MMOConnection<L2GameClient, L2GameClientPacket, L2GameServerPacket>) key.attachment()).getClient();
		return getHeaderInfoReturn().set(2 - buf.remaining(), 0, false, client);
	}

	private void printDebug(final ByteBuffer buf, final L2GameClient client, final boolean isUnknown, final int... opcodes)
	{
		if(!Config.DEBUG_PACKETS)
			return;

		if(!isUnknown)
		{
			_log.info("client_packet: " + getOpcodes(opcodes) + ", Client: " + client);
			return;
		}

		String data = "";
		final byte[] array = new byte[buf.remaining()];
		buf.get(array);
		for(final String line : HexUtil.printData(array).split("\n"))
			data += line;

		Log.addDev("Unknown Packet: " + getOpcodes(opcodes) + ", Client: " + client + ", Data: " + data, "unknown_packets");

		client.onClientPacketFail();

		final StringBuilder sb = new StringBuilder("Unknown Packet: ");
		for(int i = 0; i < opcodes.length; i++)
		{
			if(i != 0)
				sb.append(" : ");

			sb.append("0x").append(Integer.toHexString(opcodes[i]));
		}

		sb.append(", Client: ").append(client);
		_log.info(sb.toString());
	}

	private String getOpcodes(final int... opcodes)
	{
		final StringBuilder sb = new StringBuilder();
		for(int i = 0; i < opcodes.length; i++)
		{
			if(i != 0)
				sb.append(" : ");
			sb.append("0x").append(Integer.toHexString(opcodes[i]));
		}

		return sb.toString();
	}
}
