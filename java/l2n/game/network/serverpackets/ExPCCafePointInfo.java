package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: ch ddcdc
 * 
 * @author L2System Project
 * @date 17.11.2009
 * @time 21:58:15
 */
public class ExPCCafePointInfo extends L2GameServerPacket
{
	private static final String _S__FE_32_EXPCCAFEPOINTINFO = "[S] FE:32 ExPCCafePointInfo";
	private final int _points;
	private final int _mAddPoint;
	private int _mPeriodType;
	private final int _remainTime;
	private int _pointType = 0;

	public ExPCCafePointInfo(final L2Player player)
	{
		_points = player.getPcBangPoints();
		_mAddPoint = 0;
		_remainTime = 1;
		_mPeriodType = 2;
		_pointType = 2;
	}

	public ExPCCafePointInfo()
	{
		_points = 0;
		_mAddPoint = 0;
		_remainTime = 0;
		_mPeriodType = 0;
		_pointType = 0;
	}

	public ExPCCafePointInfo(final int points, final int modify_points, final boolean mod, final boolean _double, final int hours_left)
	{
		_points = points;
		_mAddPoint = modify_points;
		_remainTime = hours_left;
		if(mod && _double)
		{
			_mPeriodType = 1;
			_pointType = 0;
		}
		else if(mod)
		{
			_mPeriodType = 1;
			_pointType = 1;
		}
		else
		{
			_mPeriodType = 2;
			_pointType = 2;
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x32);
		writeD(_points);// num points
		writeD(_mAddPoint);// points inc display
		writeC(_mPeriodType);// period(0=don't show window,1=acquisition,2=use points)
		writeD(_remainTime);// period hours left
		writeC(_pointType);// points inc display color(0=yellow,1=cyan-blue,2=red,all other black)
	}

	@Override
	public String getType()
	{
		return _S__FE_32_EXPCCAFEPOINTINFO;
	}
}
