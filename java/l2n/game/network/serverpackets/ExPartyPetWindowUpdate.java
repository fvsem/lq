package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Summon;

public class ExPartyPetWindowUpdate extends L2GameServerPacket
{
	private static final String _S_FE_19_EXPARTYPETWINDOWUPDATE = "[S] FE 19 ExPartyPetWindowUpdate";
	private int owner_obj_id, npc_id, _type, curHp, maxHp,
			curMp, maxMp, level;
	private int obj_id = 0;
	private String _name;

	public ExPartyPetWindowUpdate(final L2Summon summon)
	{
		if(summon == null)
			return;

		obj_id = summon.getObjectId();
		owner_obj_id = summon.getPlayer().getObjectId();
		npc_id = summon.getTemplate().npcId + 1000000;
		_type = summon.getSummonType();
		_name = summon.getName();
		curHp = (int) summon.getCurrentHp();
		maxHp = summon.getMaxHp();
		curMp = (int) summon.getCurrentMp();
		maxMp = summon.getMaxMp();
		level = summon.getLevel();
	}

	@Override
	protected final void writeImpl()
	{
		if(obj_id == 0)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x19);
		writeD(obj_id);
		writeD(npc_id);
		writeD(_type);
		writeD(owner_obj_id);
		writeS(_name);
		writeD(curHp);
		writeD(maxHp);
		writeD(curMp);
		writeD(maxMp);
		writeD(level);
	}

	@Override
	public String getType()
	{
		return _S_FE_19_EXPARTYPETWINDOWUPDATE;
	}
}
