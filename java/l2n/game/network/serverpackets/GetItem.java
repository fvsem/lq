package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2ItemInstance;
import l2n.util.Location;

/**
 * sample
 * 0000: 17 1a 95 20 48 9b da 12 40 44 17 02 00 03 f0 fc ff 98 f1 ff ff .....
 * format ddddd
 */
public class GetItem extends L2GameServerPacket
{
	private static final String _S__17_GETITEM = "[S] 17 GetItem";
	private final int _playerId, _itemObjId;
	private final Location _loc;

	public GetItem(final L2ItemInstance item, final int playerId)
	{
		_itemObjId = item.getObjectId();
		_loc = item.getLoc();
		_playerId = playerId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x17);
		writeD(_playerId);
		writeD(_itemObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _S__17_GETITEM;
	}
}
