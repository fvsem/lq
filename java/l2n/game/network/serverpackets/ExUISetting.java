package l2n.game.network.serverpackets;

public class ExUISetting extends L2GameServerPacket
{
	private final byte data[];

	public ExUISetting(final byte[] _data)
	{
		data = _data;
	}

	@Override
	protected void writeImpl()
	{
		if(data == null)
			return;
		writeC(EXTENDED_PACKET);
		writeH(0x70);
		writeD(data.length);
		writeB(data);
	}

	/**
	 * @return A String with this packet name for debugging purposes
	 */
	@Override
	public String getType()
	{
		return "[S] FE:70 ExUISetting";
	}
}
