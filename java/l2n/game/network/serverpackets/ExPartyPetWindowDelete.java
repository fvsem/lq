package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Summon;

public class ExPartyPetWindowDelete extends L2GameServerPacket
{
	private static final String _S_FE_6A_EXPARTYPETWINDOWDELETE = "[S] FE 6a ExPartyPetWindowDelete";
	private final int _summonObjectId;
	private final int _ownerObjectId;
	private final String _summonName;

	public ExPartyPetWindowDelete(final L2Summon summon)
	{
		_summonObjectId = summon.getObjectId();
		_summonName = summon.getName();
		_ownerObjectId = summon.getPlayer().getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x6a);
		writeD(_summonObjectId);
		writeD(_ownerObjectId);
		writeS(_summonName);
	}

	@Override
	public String getType()
	{
		return _S_FE_6A_EXPARTYPETWINDOWDELETE;
	}
}
