package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.tables.ClanTable;

/**
 * @author L2System Project
 * @date 23.02.2010
 * @time 10:42:45
 */
public class ExShowDominionRegistry extends L2GameServerPacket
{
	private int _territoryId;
	private String _territoryOwnerClanName;
	private String _territoryOwnerLeaderName;
	private String _territoryOwnerAllyName;
	private int _clanReq;
	private int _mercReq;
	private int _warTime;
	private int _currentTime;
	private int _registredAsPlayer;
	private int _registredAsClan;
	private final GArray<CastleFlagsInfo> _cfi = new GArray<CastleFlagsInfo>();

	public ExShowDominionRegistry(final L2Player activeChar, final int terrId)
	{
		_territoryId = terrId;
		final Castle castle = CastleManager.getInstance().getCastleByIndex(terrId);
		if(castle == null)
		{
			System.out.println("Cant find castle with ID " + terrId);
			return;
		}
		final L2Clan clan = ClanTable.getInstance().getClan(castle.getOwnerId());
		_territoryOwnerClanName = clan == null ? "No Clan" : clan.getName();
		_territoryOwnerLeaderName = clan == null ? "No Owner" : clan.getLeaderName();
		_territoryOwnerAllyName = clan == null || clan.getAlliance() == null ? "No Ally" : clan.getAlliance().getAllyName();
		_warTime = (int) (TerritorySiege.getSiegeDate().getTimeInMillis() / 1000);
		_currentTime = (int) (System.currentTimeMillis() / 1000);
		_mercReq = TerritorySiege.getPlayersForTerritory(terrId);
		_clanReq = TerritorySiege.getClansForTerritory(terrId);
		_registredAsPlayer = TerritorySiege.getTerritoryForPlayer(activeChar.getObjectId());
		_registredAsClan = TerritorySiege.getTerritoryForClan(activeChar.getClanId());
		for(final Castle c : CastleManager.getInstance().getCastles().values())
			_cfi.add(new CastleFlagsInfo(c.getId(), c.getFlags()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x90);

		writeD(_territoryId); // Current Territory Id
		writeS(_territoryOwnerClanName); // Owners Clan
		writeS(_territoryOwnerLeaderName); // Owner Clan Leader
		writeS(_territoryOwnerAllyName); // Owner Alliance
		writeD(_clanReq); // Clan Request
		writeD(_mercReq); // Merc Request
		writeD(_warTime); // War Time
		writeD(_currentTime); // Current Time
		writeD(_registredAsClan == _territoryId ? 1 : 0); // is Cancel clan registration
		writeD(_registredAsPlayer == _territoryId ? 1 : 0); // is Cancel mercenaries registration
		writeD(0x01);
		writeD(_cfi.size()); // Territory Count
		for(final CastleFlagsInfo cf : _cfi)
		{
			writeD(80 + cf.id); // Territory Id
			writeD(cf.flags.length); // Emblem Count
			for(final int flag : cf.flags)
				writeD(80 + flag); // Emblem ID - should be in for loop for emblem count
		}
	}

	private class CastleFlagsInfo
	{
		public int id;
		public int[] flags;

		public CastleFlagsInfo(final int id_, final int[] flags_)
		{
			id = id_;
			flags = flags_;
		}
	}

	@Override
	public String getType()
	{
		return "[S] FE:90 ExShowDominionRegistry";
	}
}
