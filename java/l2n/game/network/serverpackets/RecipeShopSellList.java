package l2n.game.network.serverpackets;

import l2n.game.model.L2ManufactureItem;
import l2n.game.model.L2ManufactureList;
import l2n.game.model.actor.L2Player;

/**
 * dddd d(ddd)
 */
public class RecipeShopSellList extends L2GameServerPacket
{
	private static final String _S__df_RecipeShopSellList = "[S] df RecipeShopSellList";
	public int obj_id, curMp, maxMp;
	private final long buyer_adena;
	private final L2ManufactureList createList;

	public RecipeShopSellList(final L2Player buyer, final L2Player manufacturer)
	{
		obj_id = manufacturer.getObjectId();
		curMp = (int) manufacturer.getCurrentMp();
		maxMp = manufacturer.getMaxMp();
		buyer_adena = buyer.getAdena();
		createList = manufacturer.getCreateList();
	}

	@Override
	protected final void writeImpl()
	{
		if(createList == null)
			return;

		writeC(0xdf);
		writeD(obj_id);
		writeD(curMp);// Creator's MP
		writeD(maxMp);// Creator's MP
		writeQ(buyer_adena);// Buyer Adena
		writeD(createList.size());
		for(final L2ManufactureItem temp : createList.getList())
		{
			writeD(temp.getRecipeId());
			writeD(0x00); // unknown
			writeQ(temp.getCost());
		}
	}

	@Override
	public String getType()
	{
		return _S__df_RecipeShopSellList;
	}
}
