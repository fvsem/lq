package l2n.game.network.serverpackets;

public class SunRise extends L2GameServerPacket
{
	private static final String _S__12_SUNRISE = "[S] 12 SunRise";

	@Override
	protected final void writeImpl()
	{
		writeC(0x12);
	}

	@Override
	public String getType()
	{
		return _S__12_SUNRISE;
	}
}
