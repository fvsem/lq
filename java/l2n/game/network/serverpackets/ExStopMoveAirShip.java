package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2AirShip;

/**
 * @author L2System Project
 *         06.11.2009 16:41:33
 */
public class ExStopMoveAirShip extends L2GameServerPacket
{
	private final L2AirShip _ship;

	public ExStopMoveAirShip(final L2AirShip ship)
	{
		_ship = ship;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xfe);
		writeH(0x66);
		writeD(_ship.getObjectId());
		writeD(_ship.getX());
		writeD(_ship.getY());
		writeD(_ship.getZ());
		writeD(_ship.getHeading());
	}

	@Override
	public String getType()
	{
		return "[S] FE:66 ExStopMoveAirShip";
	}
}
