package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;

/**
 * Format: ch d[Sdd]
 */
public class ExMPCCShowPartyMemberInfo extends L2GameServerPacket
{
	private static final String _S__FE_4B_EXMPCCSHOWPARTYMEMBERINFO = "[S] FE:4b ExMPCCShowPartyMemberInfo";
	private GArray<PartyMemberInfo> members;

	public ExMPCCShowPartyMemberInfo(final L2Player partyLeader)
	{
		if(!partyLeader.isInParty())
			return;

		final L2Party _party = partyLeader.getParty();
		if(_party == null)
			return;

		if(!_party.isInCommandChannel())
			return;

		members = new GArray<PartyMemberInfo>();
		for(final L2Player _member : _party.getPartyMembers())
			members.add(new PartyMemberInfo(_member.getName(), _member.getObjectId(), _member.getClassId().getId()));
	}

	@Override
	protected final void writeImpl()
	{
		if(members == null)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x4b);
		writeD(members.size()); // Количество членов в пати
		for(final PartyMemberInfo _member : members)
		{
			writeS(_member._name); // Имя члена пати
			writeD(_member.object_id); // object Id члена пати
			writeD(_member.class_id); // id класса члена пати
		}
		members.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_4B_EXMPCCSHOWPARTYMEMBERINFO;
	}

	static class PartyMemberInfo
	{
		public String _name;
		public int object_id, class_id;

		public PartyMemberInfo(final String __name, final int _object_id, final int _class_id)
		{
			_name = __name;
			object_id = _object_id;
			class_id = _class_id;
		}
	}
}
