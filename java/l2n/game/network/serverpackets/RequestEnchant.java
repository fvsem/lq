package l2n.game.network.serverpackets;

public class RequestEnchant extends L2GameServerPacket
{
	private final int _unk;
	private static final String _S__FE_81_REQUESTENCHANT = "[S] FE:81 RequestEnchant";

	public RequestEnchant(final int value)
	{
		_unk = value;
	}

	@Override
	public String getType()
	{
		return _S__FE_81_REQUESTENCHANT;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x81);
		writeD(_unk);
	}
}
