package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Summon;

public class PetStatusShow extends L2GameServerPacket
{
	private static final String _S__b1_PETSTATUSSHOW = "[S] b1 PetStatusShow";
	private final int _summonType;

	public PetStatusShow(final L2Summon summon)
	{
		_summonType = summon.getSummonType();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xb1);
		writeD(_summonType);
	}

	@Override
	public String getType()
	{
		return _S__b1_PETSTATUSSHOW;
	}
}
