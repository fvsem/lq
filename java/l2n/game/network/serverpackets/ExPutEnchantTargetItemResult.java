package l2n.game.network.serverpackets;

public class ExPutEnchantTargetItemResult extends L2GameServerPacket
{
	private static final String _S__87_EXPUTENCHANTTARGETITEMRESULT = "[S] 87 ExPutEnchantTargetItemResult";

	public static final ExPutEnchantTargetItemResult FAIL = new ExPutEnchantTargetItemResult(0);
	public static final ExPutEnchantTargetItemResult SACCESS = new ExPutEnchantTargetItemResult(1);

	private final int _result;

	public ExPutEnchantTargetItemResult(final int result)
	{
		_result = result;
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#getType()
	 */
	@Override
	public String getType()
	{
		return _S__87_EXPUTENCHANTTARGETITEMRESULT;
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#writeImpl()
	 */
	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x81);
		writeD(_result);
	}
}
