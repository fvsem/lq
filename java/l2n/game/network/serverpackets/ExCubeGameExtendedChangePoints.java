package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: (chd) dddddd
 * d: Time Left
 * d: Blue Points
 * d: Red Points
 * d: Player Team
 * d: Player Object ID
 * d: Player Points
 */
public class ExCubeGameExtendedChangePoints extends L2GameServerPacket
{
	private static final String ExCubeGameExtendedChangePoints = "[S] FE:98:00 ExCubeGameExtendedChangePoints";

	private final int _timeLeft;
	private final int _bluePoints;
	private final int _redPoints;
	private final boolean _isRedTeam;
	private final L2Player _player;
	private final int _playerPoints;

	/**
	 * Update a Secret Point Counter (used by client when receive ExCubeGameEnd)
	 * 
	 * @param timeLeft
	 *            Time Left before Minigame's End
	 * @param bluePoints
	 *            Current Blue Team Points
	 * @param redPoints
	 *            Current Blue Team points
	 * @param isRedTeam
	 *            Is Player from Red Team?
	 * @param player
	 *            Player Instance
	 * @param playerPoints
	 *            Current Player Points
	 */
	public ExCubeGameExtendedChangePoints(final int timeLeft, final int bluePoints, final int redPoints, final boolean isRedTeam, final L2Player player, final int playerPoints)
	{
		_timeLeft = timeLeft;
		_bluePoints = bluePoints;
		_redPoints = redPoints;
		_isRedTeam = isRedTeam;
		_player = player;
		_playerPoints = playerPoints;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x98);
		writeD(0x00);

		writeD(_timeLeft);
		writeD(_bluePoints);
		writeD(_redPoints);

		writeD(_isRedTeam ? 0x01 : 0x00);
		writeD(_player.getObjectId());
		writeD(_playerPoints);
	}

	@Override
	public final String getType()
	{
		return ExCubeGameExtendedChangePoints;
	}
}
