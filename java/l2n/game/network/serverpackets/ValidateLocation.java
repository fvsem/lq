package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;
import l2n.util.Location;

public class ValidateLocation extends L2GameServerPacket
{
	private static final String _S__79_SETTOLOCATION = "[S] 79 ValidateLocation";
	private final int _chaObjId;
	private final Location _loc;

	public ValidateLocation(final L2Character cha)
	{
		_chaObjId = cha.getObjectId();
		_loc = cha.getLoc();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x79);

		writeD(_chaObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
	}

	@Override
	public String getType()
	{
		return _S__79_SETTOLOCATION;
	}
}
