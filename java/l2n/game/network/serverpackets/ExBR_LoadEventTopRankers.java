package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 18:34:22
 */
public class ExBR_LoadEventTopRankers extends L2GameServerPacket
{
	private final int _eventId;
	private final int _day;
	private final int _count;
	private final int _bestScore;
	private final int _myScore;

	public ExBR_LoadEventTopRankers(final int eventId, final int day, final int count, final int bestScore, final int myScore)
	{
		_eventId = eventId;
		_day = day;
		_count = count;
		_bestScore = bestScore;
		_myScore = myScore;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xC1);
		writeD(_eventId);
		writeD(_day);
		writeD(_count);
		writeD(_bestScore);
		writeD(_myScore);
	}

	@Override
	public String getType()
	{
		return "[S] FE:C1 ExBrLoadEventTopRankers";
	}
}
