package l2n.game.network.serverpackets;

/**
 * Format: ch S
 */
public class ExAskJoinPartyRoom extends L2GameServerPacket
{
	private static final String _S__FE_35_EXASKJOINPARTYROOM = "[S] FE:35 ExAskJoinPartyRoom";
	private final String _charName;

	public ExAskJoinPartyRoom(final String charName)
	{
		_charName = charName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x35);
		writeS(_charName);
	}

	@Override
	public String getType()
	{
		return _S__FE_35_EXASKJOINPARTYROOM;
	}
}
