package l2n.game.network.serverpackets;

public class JoinPledge extends L2GameServerPacket
{
	private static final String _S__2d_JOINPLEDGE = "[S] 2d JoinPledge";

	private final int _pledgeId;

	public JoinPledge(final int pledgeId)
	{
		_pledgeId = pledgeId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x2d);
		writeD(_pledgeId);
	}

	@Override
	public String getType()
	{
		return _S__2d_JOINPLEDGE;
	}
}
