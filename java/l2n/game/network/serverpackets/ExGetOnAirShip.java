package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.util.Location;

public class ExGetOnAirShip extends L2GameServerPacket
{
	private static String _C_36_EXGETONAIRSHIP = "[C] 0x2d:0x36 ExGetOnAirShip";

	private final int _char_id;
	private final int _boat_id;
	private final Location _loc;

	public ExGetOnAirShip(final L2Player player, final L2AirShip ship, final Location loc)
	{
		_char_id = player.getObjectId();
		_boat_id = ship.getObjectId();
		_loc = loc;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x63);
		writeD(_char_id);
		writeD(_boat_id);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _C_36_EXGETONAIRSHIP;
	}
}
