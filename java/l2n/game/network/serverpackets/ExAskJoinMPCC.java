package l2n.game.network.serverpackets;

/**
 * Asks the player to join a Command Channel
 */
public class ExAskJoinMPCC extends L2GameServerPacket
{

	private static final String _S__FE_1A_EXASKJOINMPCC = "[S] FE:1a ExAskJoinMPCC";
	private final String _requestorName;

	/**
	 * @param String
	 *            Name of CCLeader
	 */
	public ExAskJoinMPCC(final String requestorName)
	{
		_requestorName = requestorName;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x1a);
		writeS(_requestorName); // лидер CC
		writeD(0x00);
	}

	@Override
	public String getType()
	{
		return _S__FE_1A_EXASKJOINMPCC;
	}
}
