package l2n.game.network.serverpackets;

public class AutoAttackStart extends L2GameServerPacket
{
	private static final String _S__25_AUTOATTACKSTART = "[S] 25 AutoAttackStart";
	private final int _targetId;

	public AutoAttackStart(final int targetId)
	{
		_targetId = targetId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x25);
		writeD(_targetId);
	}

	@Override
	public String getType()
	{
		return _S__25_AUTOATTACKSTART;
	}
}
