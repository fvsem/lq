package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;
import l2n.util.Location;

/**
 * Format (ch)ddddd
 */
public class ExFishingStart extends L2GameServerPacket
{
	private static final String _S__FE_1E_EXFISHINGSTART = "[S] FE:1e ExFishingStart";
	private final int _charObjId;
	private final Location _loc;
	private final int _fishType;
	private final boolean _isNightLure;

	public ExFishingStart(final L2Character character, final int fishType, final Location loc, final boolean isNightLure)
	{
		_charObjId = character.getObjectId();
		_fishType = fishType;
		_loc = loc;
		_isNightLure = isNightLure;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x1e);
		writeD(_charObjId);
		writeD(_fishType); // fish type
		writeD(_loc.x); // x poisson
		writeD(_loc.y); // y poisson
		writeD(_loc.z); // z poisson
		writeC(_isNightLure ? 0x01 : 0x00); // 0 = day lure 1 = night lure
		writeC(0x00);
	}

	@Override
	public String getType()
	{
		return _S__FE_1E_EXFISHINGSTART;
	}
}
