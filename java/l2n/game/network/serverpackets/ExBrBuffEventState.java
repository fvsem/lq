package l2n.game.network.serverpackets;

/**
 * Eva's Inferno event packet.<br>
 * time info params: type (1 - %, 2 - npcId), value (depending on type: for type 1 - % value; for type 2 - 20573-20575), state (0-1), endtime (only when type 2)
 * 
 * @author L2System Project
 * @date 18.02.2011
 * @time 10:50:37
 */
public class ExBrBuffEventState extends L2GameServerPacket
{
	private final int _type; // 1 - %, 2 - npcId
	private final int _value; // depending on type: for type 1 - % value; for type 2 - 20573-20575
	private final int _state; // 0-1
	private final int _endtime; // only when type 2 as unix time in seconds from 1970

	public ExBrBuffEventState(final int type, final int value, final int state, final int endtime)
	{
		_type = type;
		_value = value;
		_state = state;
		_endtime = endtime;
	}

	@Override
	public String getType()
	{
		return "[S] FE:bF ExBrBuffEventState".intern();
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xbf);
		writeD(_type);
		writeD(_value);
		writeD(_state);
		writeD(_endtime);
	}
}
