package l2n.game.network.serverpackets;

import l2n.game.RecipeController;
import l2n.game.model.L2RecipeList;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

/**
 * format ddddd
 */
public class RecipeItemMakeInfo extends L2GameServerPacket
{
	private static final Logger _log = Logger.getLogger(RecipeItemMakeInfo.class.getName());

	private static final String _S__dd_RECIPEITEMMAKEINFO = "[S] dd RecipeItemMakeInfo";
	private final int _id;
	private final boolean _success;
	private final int _CurMP;
	private final int _MaxMP;

	public RecipeItemMakeInfo(final int id, final L2Player pl, final boolean status)
	{
		_id = id;
		_success = status;
		_CurMP = (int) pl.getCurrentMp();
		_MaxMP = pl.getMaxMp();
	}

	public RecipeItemMakeInfo(final int id, final L2Player pl)
	{
		_id = id;
		_success = true;
		_CurMP = (int) pl.getCurrentMp();
		_MaxMP = pl.getMaxMp();
	}

	@Override
	protected final void writeImpl()
	{
		final L2RecipeList recipeList = RecipeController.getInstance().getRecipeList(_id);
		if(recipeList == null)
		{
			_log.severe("RecipeItemMakeInfo: No recipe found with ID = " + _id);
			return;
		}

		writeC(0xDd); // Точно: назначение пакета
		writeD(_id); // Точно: ID рецепта
		writeD(recipeList.isDwarvenRecipe() ? 0 : 1); // 0 = Dwarven - 1 = Common
		writeD(_CurMP); // Точно: текущее состояние полоски Creator MP
		writeD(_MaxMP); // Точно: максимальное состояние полоски Creator MP
		writeD(_success ? 1 : 0); // Точно: итог крафта; 0xFFFFFFFF нет статуса, 0 удача, 1 провал
	}

	@Override
	public String getType()
	{
		return _S__dd_RECIPEITEMMAKEINFO;
	}
}
