package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.util.Location;

/**
 * Format: (ch) d[ddddd]
 * Живой пример с оффа:
 * FE 46 00 01 00 00 00 FE 1F 00 00 01 00 00 00 03 A9 FF FF E7 5C FF FF 60 D5 FF FF
 */
public class ExCursedWeaponLocation extends L2GameServerPacket
{
	private static final String _S__FE_47_EXCURSEDWEAPONLOCATION = "[S] FE:47 ExCursedWeaponLocation";
	private final GArray<CursedWeaponInfo> _cursedWeaponInfo;

	public ExCursedWeaponLocation(final GArray<CursedWeaponInfo> cursedWeaponInfo)
	{
		_cursedWeaponInfo = cursedWeaponInfo;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x47);

		if(!_cursedWeaponInfo.isEmpty())
		{
			writeD(_cursedWeaponInfo.size());
			for(final CursedWeaponInfo w : _cursedWeaponInfo)
			{
				writeD(w._id);
				writeD(w._status);

				writeD(w._pos.x);
				writeD(w._pos.y);
				writeD(w._pos.z);
			}
		}
		else
		{
			writeD(0);
			writeD(0);
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_47_EXCURSEDWEAPONLOCATION;
	}

	public static class CursedWeaponInfo
	{
		public Location _pos;
		public int _id;
		public int _status;

		public CursedWeaponInfo(final Location p, final int ID, final int status)
		{
			_pos = p;
			_id = ID;
			_status = status;
		}
	}
}
