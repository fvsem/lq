package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2Ship;
import l2n.util.Location;

public class VehicleInfo extends L2GameServerPacket
{
	private static final String _S__6C_VEHICLEINFO = "[S] 60 VehicleInfo";
	private final int _boatObjId;
	private final Location _loc;

	public VehicleInfo(final L2Ship boat)
	{
		_boatObjId = boat.getObjectId();
		_loc = boat.getLoc();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x60);
		writeD(_boatObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
	}

	@Override
	public String getType()
	{
		return _S__6C_VEHICLEINFO;
	}
}
