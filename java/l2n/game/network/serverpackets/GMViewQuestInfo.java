package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class GMViewQuestInfo extends L2GameServerPacket
{
	private static final String _S__99_GMVIEWQUESTINFO = "[S] 99 GMViewQuestInfo";

	private final L2Player _cha;

	public GMViewQuestInfo(final L2Player cha)
	{
		_cha = cha;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x99);
		writeS(_cha.getName());

		final Quest[] quests = _cha.getAllActiveQuests();
		if(quests.length == 0)
		{
			writeH(0);
			writeH(0);
			return;
		}

		writeH(quests.length);
		for(final Quest q : quests)
		{
			writeD(q.getQuestIntId());
			final QuestState qs = _cha.getQuestState(q.getName());
			writeD(qs == null ? 0 : qs.getInt("cond"));
		}
		writeH(0); // количество элементов типа: ddQd , как-то связано с предметами
	}

	@Override
	public String getType()
	{
		return _S__99_GMVIEWQUESTINFO;
	}
}
