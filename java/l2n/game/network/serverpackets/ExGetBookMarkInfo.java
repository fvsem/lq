package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Player.TeleportBookmark;

/**
 * @author L2System Project
 * @date 15.12.2009
 * @time 23:07:29
 */
public class ExGetBookMarkInfo extends L2GameServerPacket
{
	private static final String _S__FE_84_EXGETBOOKMARKINFOPACKET = "[S] FE:84 ExGetBookMarkInfo";

	private final L2Player player;

	public ExGetBookMarkInfo(final L2Player cha)
	{
		player = cha;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xFE);
		writeH(0x84);
		writeD(0x00); // Dummy
		writeD(player.getBookMarkSlot());
		writeD(player.tpbookmark.size());

		for(final TeleportBookmark tpbm : player.tpbookmark)
		{
			writeD(tpbm._id);
			writeD(tpbm._x);
			writeD(tpbm._y);
			writeD(tpbm._z);
			writeS(tpbm._name);
			writeD(tpbm._icon);
			writeS(tpbm._tag);
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_84_EXGETBOOKMARKINFOPACKET;
	}
}
