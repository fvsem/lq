package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 06.07.2010
 * @time 13:21:58
 */
public class ExEnchantSkillResult extends L2GameServerPacket
{
	private static final String _S__A7_EXENCHANTSKILLRESULT = "[S] A7 ExEnchantSkillResult";
	private final int _result;

	public static final ExEnchantSkillResult SUCCESS = new ExEnchantSkillResult(1);
	public static final ExEnchantSkillResult FAIL = new ExEnchantSkillResult(0);

	public ExEnchantSkillResult(final int result)
	{
		_result = result;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xA7);
		writeD(_result);
	}

	@Override
	public String getType()
	{
		return _S__A7_EXENCHANTSKILLRESULT;
	}
}
