package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.instances.L2ItemInstance;

/**
 * 15
 * ee cc 11 43 object id
 * 39 00 00 00 item id
 * 8f 14 00 00 x
 * b7 f1 00 00 y
 * 60 f2 ff ff z
 * 01 00 00 00 show item count
 * 7a 00 00 00 count .
 * format dddddddd
 */
public class SpawnItem extends L2GameServerPacket
{
	private static final String _S__05_SPAWNITEM = "[S] 05 SpawnItem";
	private final int _objectId;
	private final int _itemId;
	private final int _x, _y, _z;
	private final int _stackable;
	private final long _count;

	public SpawnItem(final L2ItemInstance item)
	{
		_objectId = item.getObjectId();
		_itemId = item.getItemId();
		_x = item.getX();
		_y = item.getY();
		_z = item.getZ();
		_stackable = item.isStackable() ? 0x01 : 0x00;
		_count = item.getCount();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x05);
		writeD(_objectId);
		writeD(_itemId);

		writeD(_x);
		writeD(_y);
		writeD(_z + Config.CLIENT_Z_SHIFT);
		// only show item count if it is a stackable item
		writeD(_stackable);
		writeQ(_count);
		writeD(0x00); // c2
	}

	@Override
	public String getType()
	{
		return _S__05_SPAWNITEM;
	}
}
