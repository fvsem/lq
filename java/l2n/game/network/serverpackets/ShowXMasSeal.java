package l2n.game.network.serverpackets;

public class ShowXMasSeal extends L2GameServerPacket
{
	private final int _item;

	public ShowXMasSeal(final int item)
	{
		_item = item;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xf8);
		writeD(_item);
	}

	@Override
	public String getType()
	{
		return "[S] f8 ShowXMasSeal";
	}
}
