package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 18:28:00
 */
public class ExRefundList extends L2GameServerPacket
{
	private final ConcurrentLinkedQueue<L2ItemInstance> _refundList;
	private final long _adena;

	public ExRefundList(final L2Player cha)
	{
		_adena = cha.getAdena();
		_refundList = cha.getInventory().getRefundItemsList();
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xA8);

		writeQ(_adena);

		if(_refundList == null)
			writeH(0);
		else
		{
			writeH(_refundList.size());
			for(final L2ItemInstance item : _refundList)
			{
				writeD(item.getObjectId());
				writeD(item.getItemId());
				writeQ(item.getCount());
				writeH(item.getItem().getType2ForPackets());
				writeH(item.getEnchantLevel());
				writeH(0);
				writeQ(item.getItem().getReferencePrice() / 2);
				writeItemElements(item);
			}
		}
	}
}
