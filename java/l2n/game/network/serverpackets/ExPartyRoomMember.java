package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;

/**
 * Format:(ch) d d [dsdddd]
 * 
 * @author Crion/kombat
 */

public class ExPartyRoomMember extends L2GameServerPacket
{
	private boolean can_writeImpl = false;
	private boolean isLeader;
	private GArray<PartyRoomMemberInfo> members_list = null;

	public ExPartyRoomMember(final PartyRoom room, final L2Player activeChar)
	{
		if(room == null || activeChar == null)
			return;

		final L2Player leader = room.getLeader();
		if(leader == null)
			return;

		isLeader = activeChar.equals(leader);
		members_list = new GArray<PartyRoomMemberInfo>(room.getMembersSize());
		L2Player member;
		for(final Long storedId : room.getMembers())
			if((member = L2ObjectsStorage.getAsPlayer(storedId)) != null)
				members_list.add(new PartyRoomMemberInfo(member, leader));

		can_writeImpl = true;
	}

	// dd
	// dSdddd

	@Override
	protected void writeImpl()
	{
		if(!can_writeImpl)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x08);

		// 0x01 - we are leader
		// 0x00 - we are not leader
		writeD(isLeader ? 0x01 : 0x00);
		writeD(members_list.size()); // size
		for(final PartyRoomMemberInfo member_info : members_list)
		{
			writeD(member_info.objectId);
			writeS(member_info.name);
			writeD(member_info.classId);
			writeD(member_info.level);
			writeD(member_info.location);
			writeD(member_info.memberType);// 1-leader 2-party member 0-not party member
		}
	}

	private static class PartyRoomMemberInfo
	{
		public final int objectId, classId, level, location, memberType;
		public final String name;

		public PartyRoomMemberInfo(final L2Player member, final L2Player leader)
		{
			objectId = member.getObjectId();
			name = member.getName();
			classId = member.getClassId().ordinal();
			level = member.getLevel();
			location = PartyRoomManager.getInstance().getLocation(member);
			memberType = member.equals(leader) ? 0x01 : member.getParty() != null && leader.getParty() == member.getParty() ? 0x02 : 0x00;
		}
	}

	@Override
	public String getType()
	{
		return "FE_08_ExPartyRoomMember";
	}
}
