package l2n.game.network.serverpackets;

import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;

public class PledgeShowMemberListUpdate extends L2GameServerPacket
{
	private static final String _S__5b_PLEDGESHOWMEMBERLISTUPDATE = "[S] 5b PledgeShowMemberListUpdate";
	private final String _name;
	private final int _lvl;
	private final int _classId;
	private final int _sex;
	private final int _isOnline;
	private final int _objectId;
	private final int _pledgeType;
	private int _isApprentice = 0;

	public PledgeShowMemberListUpdate(final L2Player player)
	{
		_name = player.getName();
		_lvl = player.getLevel();
		_classId = player.getClassId().getId();
		_sex = player.getSex();
		_objectId = player.getObjectId();
		_isOnline = player.isOnline() ? 1 : 0;
		_pledgeType = player.getPledgeType();
		if(player.getClan() != null && player.getClan().getClanMember(_objectId) != null)
			_isApprentice = player.getClan().getClanMember(_objectId).hasSponsor() ? 1 : 0;
	}

	public PledgeShowMemberListUpdate(final L2ClanMember cm)
	{
		_name = cm.getName();
		_lvl = cm.getLevel();
		_classId = cm.getClassId();
		_sex = cm.getSex();
		_objectId = cm.getObjectId();
		_isOnline = cm.isOnline() ? 1 : 0;
		_pledgeType = cm.getPledgeType();
		_isApprentice = cm.hasSponsor() ? 1 : 0;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x5b);
		writeS(_name);
		writeD(_lvl);
		writeD(_classId);
		writeD(_sex);
		writeD(_objectId);
		writeD(_isOnline); // 1=online 0=offline
		writeD(_pledgeType);
		writeD(_isApprentice); // does a clan member have a sponsor
	}

	@Override
	public String getType()
	{
		return _S__5b_PLEDGESHOWMEMBERLISTUPDATE;
	}
}
