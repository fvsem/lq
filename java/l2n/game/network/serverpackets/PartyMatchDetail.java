package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;

/**
 * Format:(c) dddddds
 * 
 * @author Crion/kombat
 */
public class PartyMatchDetail extends L2GameServerPacket
{
	private static final String _S__B0_PARTYMATCHDETAIL = "[S] 97 PartyMatchDetail";
	private final GArray<PartyRoom> _rooms;
	private final int _fullSize;

	public PartyMatchDetail(final L2Player player)
	{
		this(player.getPartyMatchingRegion(), player.getPartyMatchingLevels(), 1, player);
	}

	public PartyMatchDetail(final int region, final int lvlRst, final int page, final L2Player activeChar)
	{
		final int first = (page - 1) * 64;
		final int firstNot = page * 64;
		_rooms = new GArray<PartyRoom>();

		int i = 0;
		final GArray<PartyRoom> temp = PartyRoomManager.getInstance().getRooms(region, lvlRst, activeChar);
		_fullSize = temp.size();
		for(final PartyRoom room : temp)
		{
			if(i < first || i >= firstNot)
				continue;
			_rooms.add(room);
			i++;
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x9c);
		writeD(_fullSize); // unknown
		writeD(_rooms.size()); // room count

		for(final PartyRoom room : _rooms)
		{
			writeD(room.getId()); // room id
			writeS(room.getTitle()); // room name
			writeD(room.getLocation()); // Location (смотерть список ниже)
			writeD(room.getMinLevel()); // min level
			writeD(room.getMaxLevel()); // max level
			writeD(room.getMembersSize()); // members count
			writeD(room.getMaxMembers()); // max members count
			writeS(room.getLeader() == null ? "None" : room.getLeader().getName()); // leader name
		}
	}

	@Override
	public String getType()
	{
		return _S__B0_PARTYMATCHDETAIL;
	}
}
