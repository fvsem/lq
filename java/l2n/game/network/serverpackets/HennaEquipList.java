package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;

public class HennaEquipList extends L2GameServerPacket
{
	private static final String _S__ee_HennaEquipList = "[S] ee HennaEquipList";
	private final int HennaEmptySlots;
	private final long char_adena;
	private final GArray<L2HennaInstance> availHenna = new GArray<L2HennaInstance>();

	public HennaEquipList(final L2Player player, final L2HennaInstance[] hennaEquipList)
	{
		char_adena = player.getAdena();
		HennaEmptySlots = player.getHennaEmptySlots();
		for(final L2HennaInstance element : hennaEquipList)
			if(player.getInventory().findItemByItemId(element.getItemIdDye()) != null)
				availHenna.add(element);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xee);
		writeQ(char_adena);
		writeD(HennaEmptySlots);
		if(availHenna.size() != 0)
		{
			writeD(availHenna.size());
			for(final L2HennaInstance henna : availHenna)
			{
				writeD(henna.getSymbolId()); // symbolid
				writeD(henna.getItemIdDye()); // itemid of dye
				writeQ(henna.getAmountDyeRequire()); // amount of dye require
				writeQ(henna.getPrice()); // amount of aden require
				writeD(1); // meet the requirement or not
			}
		}
		else
		{
			writeD(0x01);
			writeD(0x00);
			writeD(0x00);
			writeQ(0x00);
			writeQ(0x00);
			writeD(0x00);
		}
	}

	@Override
	public String getType()
	{
		return _S__ee_HennaEquipList;
	}
}
