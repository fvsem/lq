package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class PrivateStoreMsgSell extends L2GameServerPacket
{
	private static final String _S__A2_PRIVATESTOREMSGSELL = "[S] A2 PrivateStoreMsgSell";
	private final int char_obj_id;
	private final String store_name;
	private boolean WholeMsg;

	/**
	 * Название личного магазина продажи
	 * 
	 * @param player
	 */
	public PrivateStoreMsgSell(final L2Player player)
	{
		WholeMsg = player.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE;
		char_obj_id = player.getObjectId();
		store_name = player.getTradeList() == null ? "" : player.getTradeList().getSellStoreName();
	}

	public PrivateStoreMsgSell(final L2Player player, final boolean _WholeMsg)
	{
		this(player);
		WholeMsg = _WholeMsg;
	}

	@Override
	protected final void writeImpl()
	{
		if(WholeMsg)
		{
			writeC(EXTENDED_PACKET);
			writeH(0x80);
		}
		else
			writeC(0xA2);

		writeD(char_obj_id);
		writeS(store_name);
	}

	@Override
	public String getType()
	{
		return _S__A2_PRIVATESTOREMSGSELL;
	}
}
