package l2n.game.network.serverpackets;

public class ExOlympiadMode extends L2GameServerPacket
{
	// chc
	private static final String _S__FE_7C_OLYMPIADMODE = "[S] FE:7c ExOlympiadMode";
	private final int _mode;

	public static final L2GameServerPacket RETURN = new ExOlympiadMode(0);
	public static final L2GameServerPacket SPECTATE = new ExOlympiadMode(3);

	/**
	 * @param _mode
	 *            (0 = return, 3 = spectate)
	 */
	public ExOlympiadMode(final int mode)
	{
		_mode = mode;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x7c);
		writeC(_mode);
	}

	@Override
	public String getType()
	{
		return _S__FE_7C_OLYMPIADMODE;
	}
}
