package l2n.game.network.serverpackets;

/**
 * Send Private (Friend) Message
 * Format: c dSSS
 * d: Unknown
 * S: Sending Player
 * S: Receiving Player
 * S: Message
 */
public class L2FriendSay extends L2GameServerPacket
{
	private static final String _S__78_L2FRIENDSAY = "[S] 78 L2FriendSay";
	private final String _sender, _receiver, _message;

	public L2FriendSay(final String sender, final String reciever, final String message)
	{
		_sender = sender;
		_receiver = reciever;
		_message = message;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x78);
		writeD(0);
		writeS(_receiver);
		writeS(_sender);
		writeS(_message);
	}

	@Override
	public String getType()
	{
		return _S__78_L2FRIENDSAY;
	}
}
