package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Weapon;

public class GMViewWarehouseWithdrawList extends L2GameServerPacket
{
	private static final String _S__95_GMViewWarehouseWithdrawList = "[S] 95 GMViewWarehouseWithdrawList";

	private final L2ItemInstance[] _items;
	private final String _charName;
	private final long _charAdena;

	public GMViewWarehouseWithdrawList(final L2Player cha)
	{
		_charName = cha.getName();
		_charAdena = cha.getAdena();
		_items = cha.getWarehouse().listItems(ItemClass.ALL);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x9b);
		writeS(_charName);
		writeQ(_charAdena);
		writeH(_items.length);

		for(final L2ItemInstance temp : _items)
		{
			writeH(temp.getItem().getType1());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeH(temp.getItem().getType2ForPackets());
			writeH(temp.getCustomType1());

			writeD(temp.getBodyPart());
			writeH(temp.getEnchantLevel());

			if(temp.getItem().getType2() == L2Item.TYPE2_WEAPON)
			{
				writeH(((L2Weapon) temp.getItem()).getSoulShotCount());
				writeH(((L2Weapon) temp.getItem()).getSpiritShotCount());
			}
			else
			{
				writeH(0x00);
				writeH(0x00);
			}

			if(temp.isAugmented())
			{
				writeD(temp.getAugmentationId() & 0xFFFF);
				writeD(temp.getAugmentationId() >> 16);
			}
			else
			{
				writeD(0);
				writeD(0);
			}

			writeD(temp.getObjectId());
			writeItemElements(temp);

			writeD(temp.isShadowItem() ? temp.getLifeTimeRemaining() : -1);
			writeD(temp.isTemporalItem() ? temp.getLifeTimeRemaining() : 0); // limited time item life remaining

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__95_GMViewWarehouseWithdrawList;
	}
}
