package l2n.game.network.serverpackets;

import l2n.game.event.L2EventPlayerInfo;
import l2n.game.event.L2EventTeam;
import l2n.game.event.L2EventUtil;
import l2n.game.model.actor.L2Player;

/**
 * @<a href="L2System Project">L2s</a>
 * @date 06.09.2011
 * @time 3:56:41
 */
public class ExPVPMatchRecord extends L2GameServerPacket
{
	private static final String _S__FE_7E_EXPVPMATCHRECORD = "[S] FE:7E ExPVPMatchRecord";

	private final L2EventPlayerInfo[][] _teams;
	private final boolean _blue_team_win;
	private final boolean _red_team_win;
	private final int[] _points;

	/**
	 * @param blue_team
	 *            Team 1's player records
	 * @param red_team
	 *            Team 2's player records
	 * @param t1wins
	 *            UNK
	 */
	public ExPVPMatchRecord(final L2EventTeam blue_team, final L2EventTeam red_team)
	{
		_teams = new L2EventPlayerInfo[2][];
		_teams[0] = L2EventUtil.sortAndTrimPlayerInfos(blue_team.getPlayerInfosArray());
		_teams[1] = L2EventUtil.sortAndTrimPlayerInfos(red_team.getPlayerInfosArray());
		_blue_team_win = blue_team.getTeamPoints() > red_team.getTeamPoints();
		_red_team_win = red_team.getTeamPoints() > blue_team.getTeamPoints();

		_points = new int[2];
		_points[0] = blue_team.getTeamPoints();
		_points[1] = red_team.getTeamPoints();
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x7e);

		writeD(0x02); // state, 2 = end,
		// ничья?
		if(!_blue_team_win && !_red_team_win)
		{
			writeD(0x00);
			writeD(0x00);
		}
		else
		{
			writeD(_blue_team_win ? 0x01 : 0x02); // winner team no?
			writeD(_red_team_win ? 0x01 : 0x02); // looser team no?
		}

		writeD(_points[0]); // team1 kills
		writeD(_points[1]); // team2 kills

		for(final L2EventPlayerInfo[] team : _teams)
		{
			writeD(team.length);
			for(int i = 0; i < team.length; i++)
				if(team[i] != null)
				{
					final L2Player p = team[i].getPlayer();
					if(p != null)
						writeS(p.getName()); // player name
					else
						writeS("Disconnected");

					writeD(team[i].getKillsCount()); // kills
					writeD(team[i].getDeathsCount()); // deaths
				}
				else
				{
					writeS("Disconnected");
					writeD(0x00); // kills
					writeD(0x00); // deaths
				}
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_7E_EXPVPMATCHRECORD;
	}
}
