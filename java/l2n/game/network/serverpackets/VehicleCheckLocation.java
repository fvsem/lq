package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2Ship;
import l2n.util.Location;

public class VehicleCheckLocation extends L2GameServerPacket
{
	private static final String _S__6d_VehicleCheckLocation = "[S] 6d VehicleCheckLocation";
	private final int _boatObjId;
	private final Location _loc;

	/**
	 * @param instance
	 */
	public VehicleCheckLocation(final L2Ship instance)
	{
		_boatObjId = instance.getObjectId();
		_loc = instance.getLoc();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x6d);
		writeD(_boatObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
	}

	@Override
	public String getType()
	{
		return _S__6d_VehicleCheckLocation;
	}
}
