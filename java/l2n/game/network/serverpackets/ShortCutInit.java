package l2n.game.network.serverpackets;

import l2n.game.model.L2ShortCut;
import l2n.game.model.actor.L2Player;

import java.util.Collection;

public class ShortCutInit extends L2GameServerPacket
{
	private static final String _S__45_SHORTCUTINIT = "[S] 45 ShortCutInit";
	private final Collection<L2ShortCut> _shortCuts;

	public ShortCutInit(final L2Player pl)
	{
		_shortCuts = pl.getAllShortCuts();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x45);
		writeD(_shortCuts.size());

		for(final L2ShortCut sc : _shortCuts)
		{
			writeD(sc.type);
			writeD(sc.slot + sc.page * 12);
			writeD(sc.id);

			switch (sc.type)
			{
				case L2ShortCut.TYPE_ITEM:
					writeD(0x01);
					writeD(-1);
					writeD(0x00);
					writeD(0x00);
					writeD(0x00);
					break;
				case L2ShortCut.TYPE_SKILL:
					writeD(sc.level);
					writeC(0x00);
					writeD(0x01);
					break;
				case L2ShortCut.TYPE_ACTION:
				case L2ShortCut.TYPE_MACRO:
				case L2ShortCut.TYPE_RECIPE:
					writeD(0x01);
			}
		}
	}

	@Override
	public String getType()
	{
		return _S__45_SHORTCUTINIT;
	}
}
