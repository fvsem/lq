package l2n.game.network.serverpackets;

public class CreatureSay extends L2GameServerPacket
{
	private static final String _S__4A_CREATURESAY = "[S] 4A CreatureSay [ddss]";

	private final int _objectId;
	private final int _textType;
	private final String _charName;
	private final String _text;

	public CreatureSay(final int objectId, final int messageType, final String charName, final String text)
	{
		_objectId = objectId;
		_textType = messageType;
		_charName = charName;
		_text = text;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x4A);
		writeD(_objectId);
		writeD(_textType);
		writeS(_charName);
		writeS(_text);
	}

	@Override
	public String getType()
	{
		return _S__4A_CREATURESAY;
	}
}
