package l2n.game.network.serverpackets;

/**
 * Format: (chd)
 */
public class ExCubeGameRequestReady extends L2GameServerPacket
{
	private static final String ExCubeGameRequestReady = "[S] FE:97:04 ExCubeGameRequestReady";

	/**
	 * Show Confirm Dialog for 10 seconds
	 */
	public ExCubeGameRequestReady()
	{}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x04);
	}

	@Override
	public final String getType()
	{
		return ExCubeGameRequestReady;
	}

}
