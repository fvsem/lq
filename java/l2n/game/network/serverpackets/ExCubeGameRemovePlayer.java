package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: (chd) ddd
 * d: Always -1
 * d: Player Team
 * d: Player Object ID
 * 
 * @author mrTJO
 */
public class ExCubeGameRemovePlayer extends L2GameServerPacket
{
	private static final String ExCubeGameRemovePlayer = "[S] FE:97:02 ExCubeGameRemovePlayer";

	private final L2Player _player;
	private final boolean _isRedTeam;

	/**
	 * Remove Player from Minigame Waiting List
	 * 
	 * @param player
	 *            : Player to Remove
	 * @param isRedTeam
	 *            : Is Player from Red Team?
	 */
	public ExCubeGameRemovePlayer(final L2Player player, final boolean isRedTeam)
	{
		_player = player;
		_isRedTeam = isRedTeam;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x02);

		writeD(0xffffffff);

		writeD(_isRedTeam ? 0x01 : 0x00);
		writeD(_player.getObjectId());
	}

	@Override
	public final String getType()
	{
		return ExCubeGameRemovePlayer;
	}
}
