package l2n.game.network.serverpackets;

import l2n.util.Location;

/**
 * format dddddd
 */
public class Earthquake extends L2GameServerPacket
{
	private static final String _S__d3_EARTHQUAKE = "[S] d3 Earthquake";
	private final Location _loc;
	private final int _intensity;
	private final int _duration;

	public Earthquake(final Location loc, final int intensity, final int duration)
	{
		_loc = loc;
		_intensity = intensity;
		_duration = duration;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xd3);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_intensity);
		writeD(_duration);
		writeD(0x00); // Unknown
	}

	@Override
	public String getType()
	{
		return _S__d3_EARTHQUAKE;
	}
}
