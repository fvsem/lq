package l2n.game.network.serverpackets;

public class AcquireSkillDone extends L2GameServerPacket
{
	public static final L2GameServerPacket STATIC = new AcquireSkillDone();

	@Override
	protected final void writeImpl()
	{
		writeC(0x94);
	}

	@Override
	public String getType()
	{
		return "[S] 94 AcquireSkillDone";
	}
}
