package l2n.game.network.serverpackets;

public class TradePressOtherOkPacket extends L2GameServerPacket
{
	private static final String _S__82_TRADEPRESSOTHEROK = "[S] 82 TradePressOtherOkPacket";

	@Override
	protected final void writeImpl()
	{
		writeC(0x82);
	}

	@Override
	public String getType()
	{
		return _S__82_TRADEPRESSOTHEROK;
	}
}
