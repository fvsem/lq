package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2TradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

public class PrivateStoreManageListBuy extends L2GameServerPacket
{
	private static final String _S__BD_PRIVATESTOREMANAGELISTBUY = "[S] BD PrivateStoreManageListSellBuy";
	private final GArray<BuyItemInfo> buylist = new GArray<BuyItemInfo>();
	private final int buyer_id;
	private final long buyer_adena;
	private final L2TradeList _list;

	/**
	 * Окно управления личным магазином продажи
	 * 
	 * @param buyer
	 */
	public PrivateStoreManageListBuy(final L2Player buyer)
	{
		buyer_id = buyer.getObjectId();
		buyer_adena = buyer.getAdena();

		int _id, body_part, type2;
		long count, store_price, owner_price;
		L2Item tempItem;
		for(final TradeItem e : buyer.getBuyList())
		{
			_id = e.getItemId();
			tempItem = ItemTable.getInstance().getTemplate(_id);
			if(tempItem == null)
				continue;

			count = e.getCount();
			store_price = e.getStorePrice();
			body_part = tempItem.getBodyPart();
			type2 = tempItem.getType2();
			owner_price = e.getOwnersPrice();
			buylist.add(new BuyItemInfo(_id, count, store_price, body_part, type2, owner_price));
		}

		_list = new L2TradeList(0);
		for(final L2ItemInstance item : buyer.getInventory().getItems())
			if(item != null && item.canBeTraded(buyer) && item.getItemId() != L2Item.ITEM_ID_ADENA)
			{
				for(final TradeItem ti : buyer.getBuyList())
					if(ti.getItemId() == item.getItemId())
						continue;
				_list.addItem(item);
			}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xBD);
		// section 1
		writeD(buyer_id);
		writeQ(buyer_adena);

		// section2
		writeD(_list.getItems().size());// for potential sells
		for(final L2ItemInstance temp : _list.getItems())
		{
			writeD(temp.getItemId());
			writeH(0); // show enchant lvl as 0, as you can't buy enchanted weapons
			writeQ(temp.getCount());
			writeQ(temp.getPriceToSell());
			writeH(0);
			writeD(temp.getBodyPart());
			writeH(temp.getItem().getType2ForPackets());
			writeItemElements(temp);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}

		// section 3
		writeD(buylist.size());// count for any items already added for sell
		for(final BuyItemInfo e : buylist)
		{
			writeD(e._id);
			writeH(0);
			writeQ(e.count);
			writeQ(e.store_price);
			writeH(0);
			writeD(e.body_part);
			writeH(e.type2);
			writeQ(e.owner_price);// your price
			writeQ(e.store_price);// fixed store price
			writeItemElements();

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__BD_PRIVATESTOREMANAGELISTBUY;
	}

	static class BuyItemInfo
	{
		public int _id, body_part, type2;
		public long count, store_price, owner_price;

		public BuyItemInfo(final int __id, final long _count, final long _store_price, final int _body_part, final int _type2, final long _owner_price)
		{
			_id = __id;
			count = _count;
			store_price = _store_price;
			body_part = _body_part;
			type2 = _type2;
			owner_price = _owner_price;
		}
	}
}
