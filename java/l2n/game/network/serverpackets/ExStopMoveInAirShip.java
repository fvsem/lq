package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;

/**
 * @author L2System Project
 * @date 01.12.2009
 * @time 0:15:14
 */
public class ExStopMoveInAirShip extends L2GameServerPacket
{
	private final L2AirShip _ship;
	private final L2Player _activeChar;

	public ExStopMoveInAirShip(final L2Player player, final L2AirShip ship)
	{
		_activeChar = player;
		_ship = ship;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xfe);
		writeH(0x6e);
		writeD(_activeChar.getObjectId());
		writeD(_ship.getObjectId());
		writeD(_activeChar.getX());
		writeD(_activeChar.getY());
		writeD(_activeChar.getZ());
		writeD(_activeChar.getHeading());
	}

	@Override
	public String getType()
	{
		return "[S] FE:6e ExStopMoveAirShip";
	}
}
