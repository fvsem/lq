package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

public class NickNameChanged extends L2GameServerPacket
{
	private final int objectId;
	private final String title;

	public NickNameChanged(final L2Character cha)
	{
		objectId = cha.getObjectId();
		title = cha.getTitle();
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xCC);
		writeD(objectId);
		writeS(title);
	}

	/**
	 * @return A String with this packet name for debugging purposes
	 */
	@Override
	public String getType()
	{
		return "[S] CC TitleUpdate";
	}
}
