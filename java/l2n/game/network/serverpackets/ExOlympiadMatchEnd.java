package l2n.game.network.serverpackets;

/**
 * @author XoTTa6bI4
 */
public class ExOlympiadMatchEnd extends L2GameServerPacket
{
	public ExOlympiadMatchEnd()
	{}

	@Override
	protected final void writeImpl()
	{
		writeC(0xFE);
		writeH(0x2D);
	}

	@Override
	public String getType()
	{
		return "[S] FE:2D ExOlympiadMatchEnd";
	}
}
