package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.SkillTreeTable;

/**
 * format d (dddc)
 */
public class SkillList extends L2GameServerPacket
{
	private static final String _S__5f_SKILLLIST = "[S] 5f SkillList";
	private final GArray<L2Skill> _skills;

	public SkillList(final L2Player p)
	{
		_skills = new GArray<L2Skill>();
		_skills.addAll(p.getAllSkills());
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x5f);
		writeD(_skills.size());

		for(final L2Skill temp : _skills)
		{
			writeD(temp.isActive() || temp.isToggle() ? 0 : 1);
			writeD(temp.getLevel());
			writeD(temp.getDisplayId());
			writeC(0x00); // 1 = Disabled (gray) e.g. when transformed
			writeC(SkillTreeTable.isEnchantable(temp) ? 1 : 0);
		}
	}

	@Override
	public String getType()
	{
		return _S__5f_SKILLLIST;
	}
}
