package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2FenceInstance;

/**
 * @author L2System Project
 * @date 07.10.2010
 * @time 14:23:31
 *       Format:
 *       object id
 *       type (00 - no fence, 01 - only 4 columns, 02 - columns with fences)
 *       x coord
 *       y coord
 *       z coord
 *       width
 *       height
 */
public class ExColosseumFenceInfo extends L2GameServerPacket
{
	private static final String _S__FE_03_EXCOLOSSEUMFENCEINFOPACKET = "[S] FE:03 ExColosseumFenceInfoPacket";
	private final int _type;
	private final L2FenceInstance _activeChar;
	private final int _width;
	private final int _height;

	public ExColosseumFenceInfo(final L2FenceInstance activeChar)
	{
		_activeChar = activeChar;
		_type = activeChar.getType();
		_width = activeChar.getWidth();
		_height = activeChar.getHeight();
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x03);

		writeD(_activeChar.getObjectId()); // ?
		writeD(_type);
		writeD(_activeChar.getX());
		writeD(_activeChar.getY());
		writeD(_activeChar.getZ());
		writeD(_width);
		writeD(_height);
	}

	@Override
	public String getType()
	{
		return _S__FE_03_EXCOLOSSEUMFENCEINFOPACKET;
	}
}
