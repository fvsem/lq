package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;

/**
 * @author L2System Project
 * @date 17.02.2011
 * @time 2:32:35
 */
public class ExSendUIEvent extends L2GameServerPacket
{
	private static final String _S__FE_8E_EXSENDUIEVENT = "[S] FE:8E ExSendUIEvent";
	private final int _objectId;
	private final boolean _isHide;
	private final boolean _isIncrease;
	private final int _startTime;
	private final int _endTime;
	private final String _text;

	public ExSendUIEvent(final L2Object player, final boolean isHide, final boolean isIncrease, final int startTime, final int endTime, final String text)
	{
		_objectId = player.getObjectId();
		_isHide = isHide;
		_isIncrease = isIncrease;
		_startTime = startTime;
		_endTime = endTime;
		_text = text;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x8E);
		writeD(_objectId);
		writeD(_isHide ? 0x01 : 0x00); // 0: show timer, 1: hide timer
		writeD(0x00); // unknown
		writeD(0x00); // unknown
		writeS(_isIncrease ? "1" : "0"); // "0": count negative, "1": count positive
		writeS(String.valueOf(_startTime / 60)); // timer starting minute(s)
		writeS(String.valueOf(_startTime % 60)); // timer starting second(s)
		writeS(_text); // text above timer
		writeS(String.valueOf(_endTime / 60)); // timer length minute(s) (timer will disappear 10 seconds before it ends)
		writeS(String.valueOf(_endTime % 60)); // timer length second(s) (timer will disappear 10 seconds before it ends)
	}

	@Override
	public String getType()
	{
		return _S__FE_8E_EXSENDUIEVENT;
	}
}
