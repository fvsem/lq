package l2n.game.network.serverpackets;

/**
 * Чисто технический пакет, либо пакет С5. Во всяком случе С4 клиент на него никак не реагирует.
 */
public class ServerCloseSocket extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(0xb0);
		writeD(0x01); // Always 1??!?!?!
	}

	@Override
	public String getType()
	{
		return "[S] b0 ServerCloseSocket";
	}
}
