package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

/**
 * Format (ch)dddcccd
 * d: cahacter oid
 * d: time left
 * d: fish hp
 * c:
 * c:
 * c: 00 if fish gets damage 02 if fish regens
 * d:
 */
public class ExFishingHpRegen extends L2GameServerPacket
{
	private static final String _S__FE_28_EXFISHINGHPREGEN = "[S] FE:28 ExFishingHpRegen";
	private final int _time, _fishHP, _HPmode, _Anim, _GoodUse,
			_Penalty, _hpBarColor;
	private final int char_obj_id;

	public ExFishingHpRegen(final L2Character character, final int time, final int fishHP, final int HPmode, final int GoodUse, final int anim, final int penalty, final int hpBarColor)
	{
		char_obj_id = character.getObjectId();
		_time = time;
		_fishHP = fishHP;
		_HPmode = HPmode;
		_GoodUse = GoodUse;
		_Anim = anim;
		_Penalty = penalty;
		_hpBarColor = hpBarColor;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x28);
		writeD(char_obj_id);
		writeD(_time);
		writeD(_fishHP);
		writeC(_HPmode); // 0 = HP stop, 1 = HP raise
		writeC(_GoodUse); // 0 = none, 1 = success, 2 = failed
		writeC(_Anim); // Anim: 0 = none, 1 = reeling, 2 = pumping
		writeD(_Penalty); // Penalty
		writeC(_hpBarColor); // 0 = normal hp bar, 1 = purple hp bar

	}

	@Override
	public String getType()
	{
		return _S__FE_28_EXFISHINGHPREGEN;
	}
}
