package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

/**
 * format ddddd
 * sample
 * 0000: 59 1a 95 20 48 44 17 02 00 03 f0 fc ff 98 f1 ff Y.. HD..........
 * 0010: ff c1 1a 00 00 .....
 */
public class StopMove extends L2GameServerPacket
{
	private static final String _S__47_STOPMOVE = "[S] 47 StopMove";
	private final int _objectId;
	private final int _x;
	private final int _y;
	private final int _z;
	private final int _heading;

	public StopMove(final L2Character cha)
	{
		_objectId = cha.getObjectId();
		_x = cha.getX();
		_y = cha.getY();
		_z = cha.getZ();
		_heading = cha.getHeading();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x47);
		writeD(_objectId);
		writeD(_x);
		writeD(_y);
		writeD(_z);
		writeD(_heading);
	}

	@Override
	public String getType()
	{
		return _S__47_STOPMOVE;
	}
}
