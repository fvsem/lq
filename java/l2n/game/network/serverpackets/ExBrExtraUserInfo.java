package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class ExBrExtraUserInfo extends L2GameServerPacket
{
	private final int _charObjId;

	// private final int _val;

	public ExBrExtraUserInfo(final L2Player player)
	{
		_charObjId = player.getObjectId();
		// _val = player.getAfroHaircutId();
	}

	/**
	 * This packet should belong to Quest windows, not UserInfo in T3.
	 */
	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xbe);
		writeD(_charObjId); // object id of player
		writeD(0x00); // afro hair cut
		// writeC(0x00); // Event flag, added only if event is active
	}

	@Override
	public String getType()
	{
		return "[S] FE:8D ExBrExtraUSerInfo";
	}
}
