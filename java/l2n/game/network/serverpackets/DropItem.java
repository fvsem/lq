package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2ItemInstance;
import l2n.util.Location;

/**
 * 16
 * d6 6d c0 4b player id who dropped it
 * ee cc 11 43 object id
 * 39 00 00 00 item id
 * 8f 14 00 00 x
 * b7 f1 00 00 y
 * 60 f2 ff ff z
 * 01 00 00 00 show item-count 1=yes
 * 7a 00 00 00 count .
 * format dddddddd rev 377
 * ddddddddd rev 417
 * 
 * @version $Revision: 1.3.2.1.2.3 $ $Date: 2005/03/27 15:29:39 $
 */
public class DropItem extends L2GameServerPacket
{
	private static final String _S__16_DROPITEM = "[S] 16 DropItem";
	private final Location _loc;
	private final int _playerId, item_obj_id, item_id, stackable;
	private final long _count;

	/**
	 * Constructor of the DropItem server packet
	 * 
	 * @param item
	 *            : L2ItemInstance designating the item
	 * @param playerId
	 *            : int designating the player ID who dropped the item
	 */
	public DropItem(final L2ItemInstance item, final int playerId)
	{
		_playerId = playerId;
		item_obj_id = item.getObjectId();
		item_id = item.getItemId();
		_loc = item.getLoc();
		stackable = item.isStackable() ? 1 : 0;
		_count = item.getCount();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x16);
		writeD(_playerId);
		writeD(item_obj_id);
		writeD(item_id);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(stackable);
		writeQ(_count);
		writeD(1); // unknown
	}

	@Override
	public String getType()
	{
		return _S__16_DROPITEM;// + "; object_id = "+_item.getObjectId();
	}
}
