package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Clan;
import l2n.game.model.L2Skill;

public class PledgeSkillList extends L2GameServerPacket
{
	private static final String _S__FE_39_PLEDGESKILLLIST = "[S] FE:3a PledgeSkillList";
	private final GArray<SkillInfo> infos = new GArray<SkillInfo>();

	public PledgeSkillList(final L2Clan clan)
	{
		for(final L2Skill sk : clan.getAllSkills())
			infos.add(new SkillInfo(sk.getId(), sk.getLevel()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x3a);
		writeD(infos.size());
		writeD(0x00);
		for(final SkillInfo info : infos)
		{
			writeD(info._id);
			writeD(info._level);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_39_PLEDGESKILLLIST;
	}

	private static class SkillInfo
	{
		public int _id;
		public int _level;

		public SkillInfo(final int id, final int level)
		{
			_id = id;
			_level = level;
		}
	}
}
