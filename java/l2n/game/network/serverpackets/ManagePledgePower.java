package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class ManagePledgePower extends L2GameServerPacket
{
	private static final String _S__2a_MANAGEPLEDGEPOWER = "[S] 2a ManagePledgePower";
	private final int _action, _clanId, privs;

	public ManagePledgePower(final L2Player player, final int action, final int rank)
	{
		_clanId = player.getClanId();
		_action = action;
		privs = player.getClan().getRankPrivsForPacket(rank);
		player.sendPacket(new PledgeReceiveUpdatePower(privs));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x2a);
		writeD(_clanId);
		writeD(_action);
		writeD(privs);
	}

	@Override
	public String getType()
	{
		return _S__2a_MANAGEPLEDGEPOWER;
	}
}
