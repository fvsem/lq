package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 05.08.2010
 * @time 5:34:24
 */
public class ExBR_PremiumState extends L2GameServerPacket
{
	private static final String _S__FE_BC_EXBRPREMIUMSTATE = "[S] FE:BC ExBrPremiumState";
	private final int _objId;
	private final int _state;

	public ExBR_PremiumState(final int objId, final int state)
	{
		_objId = objId;
		_state = state;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xbc);
		writeD(_objId);
		writeC(_state); // 0x00 - Не показывает рамку ПА на лвле; 0x01 - Показывает;
	}

	@Override
	public String getType()
	{
		return _S__FE_BC_EXBRPREMIUMSTATE;
	}
}
