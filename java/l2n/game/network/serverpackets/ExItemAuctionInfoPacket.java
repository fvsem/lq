package l2n.game.network.serverpackets;

import l2n.game.instancemanager.itemauction.ItemAuction;
import l2n.game.instancemanager.itemauction.ItemAuctionBid;
import l2n.game.instancemanager.itemauction.ItemAuctionState;
import l2n.game.network.model.ItemInfo;

/**
 * @<a href="L2System Project">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 9:59:36
 */
public class ExItemAuctionInfoPacket extends L2GameServerPacket
{
	private final boolean _refresh;
	private final int _timeRemaining;
	private final ItemAuction _currentAuction;
	private final ItemAuction _nextAuction;

	public ExItemAuctionInfoPacket(final boolean refresh, final ItemAuction currentAuction, final ItemAuction nextAuction)
	{
		if(currentAuction == null)
			throw new NullPointerException();

		if(currentAuction.getAuctionState() != ItemAuctionState.STARTED)
			_timeRemaining = 0;
		else
			_timeRemaining = (int) (currentAuction.getFinishingTimeRemaining() / 1000); // in seconds

		_refresh = refresh;
		_currentAuction = currentAuction;
		_nextAuction = nextAuction;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x68);

		// cdQd
		writeC(_refresh ? 0x00 : 0x01); // c
		writeD(_currentAuction.getInstanceId()); // d

		final ItemAuctionBid highestBid = _currentAuction.getHighestBid();
		writeQ(highestBid != null ? highestBid.getLastBid() : _currentAuction.getAuctionInitBid()); // q

		writeD(_timeRemaining); // d

		// d Qhh d hhhh hhhhhhhh
		writeItemInfo(_currentAuction.getItemInfo());

		if(_nextAuction != null)
		{
			writeQ(_nextAuction.getAuctionInitBid());
			writeD((int) (_nextAuction.getStartingTime() / 1000)); // unix time in seconds
			writeItemInfo(_nextAuction.getItemInfo());
		}
	}

	private final void writeItemInfo(final ItemInfo item)
	{
		writeD(item.getItemId()); // d
		writeQ(item.getCount()); // q
		writeH(item.getType2()); // h
		writeH(item.getCustomType1()); // h
		writeD(item.getBodyPart()); // d

		writeH(item.getEnchantLevel());
		writeH(item.getCustomType2());
		writeD(item.getAugmentationId());
		writeD(item.getShadowLifeTime());

		writeItemElements(item);
		writeEnchantEffect(item);
	}

	@Override
	public final String getType()
	{
		return "[S] fe:68:00 ExItemAuctionInfoPacket";
	}
}
