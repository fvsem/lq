package l2n.game.network.serverpackets;

public class ExAutoSoulShot extends L2GameServerPacket
{
	private static final String _S__FE_0C_EXAUTOSOULSHOT = "[S] FE:0C ExAutoSoulShot";
	private final int _itemId;
	private final boolean _type;

	public ExAutoSoulShot(final int itemId, final boolean type)
	{
		_itemId = itemId;
		_type = type;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x0c);

		writeD(_itemId);
		writeD(_type ? 1 : 0);
	}

	@Override
	public String getType()
	{
		return _S__FE_0C_EXAUTOSOULSHOT;
	}
}
