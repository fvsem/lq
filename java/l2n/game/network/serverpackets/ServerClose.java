package l2n.game.network.serverpackets;

public class ServerClose extends L2GameServerPacket
{
	private static final String _S__20_SERVERCLOSE = "[S] 20 ServerClose";

	@Override
	protected void writeImpl()
	{
		writeC(0x20);
	}

	@Override
	public String getType()
	{
		return _S__20_SERVERCLOSE;
	}
}
