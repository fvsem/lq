package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.util.Location;

public class GetOnVehicle extends L2GameServerPacket
{
	private final int _x, _y, _z, char_obj_id, boat_obj_id;

	/**
	 * @param activeChar
	 * @param boat
	 * @param x
	 * @param y
	 * @param z
	 */
	public GetOnVehicle(final L2Player activeChar, final L2Ship boat, final int x, final int y, final int z)
	{
		_x = x;
		_y = y;
		_z = z;
		char_obj_id = activeChar.getObjectId();
		boat_obj_id = boat.getObjectId();
	}

	public GetOnVehicle(final L2Player activeChar, final L2Ship boat, final Location loc)
	{
		_x = loc.getX();
		_y = loc.getY();
		_z = loc.getZ();
		char_obj_id = activeChar.getObjectId();
		boat_obj_id = boat.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x6e);
		writeD(char_obj_id);
		writeD(boat_obj_id);
		writeD(_x);
		writeD(_y);
		writeD(_z);
	}

	@Override
	public String getType()
	{
		return "[S] 6e GetOnVehicle";
	}
}
