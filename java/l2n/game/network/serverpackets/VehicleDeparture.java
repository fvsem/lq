package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2Ship;
import l2n.util.Location;

public class VehicleDeparture extends L2GameServerPacket
{
	private static final String _S__6C_VEHICLEDEPARTURE = "[S] 6C VehicleDeparture";
	private final int _moveSpeed, _rotationSpeed;
	private final int _boatObjId;
	private final Location _loc;

	/**
	 * @param _boat
	 */
	public VehicleDeparture(final L2Ship boat)
	{
		_boatObjId = boat.getObjectId();
		_moveSpeed = boat.getMoveSpeed();
		_rotationSpeed = boat.getRotationSpeed();
		_loc = boat.getDestination();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x6c);
		writeD(_boatObjId);
		writeD(_moveSpeed);
		writeD(_rotationSpeed);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _S__6C_VEHICLEDEPARTURE;
	}
}
