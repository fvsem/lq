package l2n.game.network.serverpackets;

/**
 * Format: ch dddd
 * Этот пакет показывает откат у элексиров.
 * Вот живой пример с оффа:
 * FE 49 00 AE 21 00 00 01 00 00 00 2C 01 00 00 2C 01 00 00
 * Где:
 * AE 21 00 00 - id предмета (8622 Elixir of Life (No Grade))
 * 2C 01 00 00 - время отката 300 сек
 */
public class ExUseSharedGroupItem extends L2GameServerPacket
{
	private static final String _S__FE_4A_EXUSESHAREDGROUPITEM = "[S] FE:4a ExUseSharedGroupItem";
	private final int _itemId, _grpId, _remainedTime, _totalTime;

	public ExUseSharedGroupItem(final int itemId, final int grpId, final int remainedTime, final int totalTime)
	{
		_itemId = itemId;
		_grpId = grpId;
		_remainedTime = remainedTime / 1000;
		_totalTime = totalTime / 1000;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x4a);

		writeD(_itemId);
		writeD(_grpId);
		writeD(_remainedTime);
		writeD(_totalTime);
	}

	@Override
	public String getType()
	{
		return _S__FE_4A_EXUSESHAREDGROUPITEM;
	}
}
