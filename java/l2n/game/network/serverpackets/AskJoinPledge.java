package l2n.game.network.serverpackets;

public class AskJoinPledge extends L2GameServerPacket
{
	private static final String _S__2c_ASKJOINPLEDGE = "[S] 2c AskJoinPledge";

	private final int _requestorId;
	private final String _pledgeName;

	public AskJoinPledge(final int requestorId, final String pledgeName)
	{
		_requestorId = requestorId;
		_pledgeName = pledgeName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x2c);
		writeD(_requestorId);
		writeS(_pledgeName);
	}

	@Override
	public String getType()
	{
		return _S__2c_ASKJOINPLEDGE;
	}
}
