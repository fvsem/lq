package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.Arrays;
import java.util.logging.Level;

public class ItemList extends L2GameServerPacket
{
	private static final String _S__27_ITEMLIST = "[S] 27 ItemList";
	private L2ItemInstance[] _items;
	private int length;
	private final boolean _showWindow;

	public ItemList(final L2Player cha, final boolean showWindow)
	{
		_items = cha.getInventory().getItems();
		_showWindow = showWindow;
		length = _items.length;

		if(length > 400)
		{
			_log.log(Level.WARNING, "Character " + cha + " have more then 400 items in his inventory!");
			length = 400;
			_items = Arrays.copyOf(_items, 400);
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x11);
		writeH(_showWindow ? 0x01 : 0x00);

		writeH(_items.length);

		// 76 bytes each loop, ByteBuffer is 1024*64 bytes so limit is at 800+
		for(final L2ItemInstance temp : _items)
		{
			if(temp == null || temp.getItem() == null)
				continue;

			writeH(temp.getItem().getType1()); // item type1
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeD(temp.getEquipSlot()); // order
			writeQ(temp.getCount());
			writeH(temp.getItem().getType2ForPackets()); // item type2
			writeH(temp.getCustomType1()); // item type3
			writeH(temp.isEquipped() ? 0x01 : 0x00);
			writeD(temp.getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
			writeH(temp.getEnchantLevel()); // enchant level
			writeH(temp.getCustomType2()); // item type3

			if(temp.isAugmented())
				writeD(temp.getAugmentation().getAugmentationId());
			else
				writeD(0x00);

			writeD(temp.isShadowItem() ? temp.getLifeTimeRemaining() : -1);
			//
			writeItemElements(temp);
			// T2
			writeD(temp.isTemporalItem() ? temp.getLifeTimeRemaining() : -1); // limited time item life remaining

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__27_ITEMLIST;
	}
}
