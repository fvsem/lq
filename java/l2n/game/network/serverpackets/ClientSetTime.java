package l2n.game.network.serverpackets;

import l2n.game.GameTimeController;

public class ClientSetTime extends L2GameServerPacket
{
	private static final String _S__f2_CLIENTSETTIME = "[S] f2 ClientSetTime";

	@Override
	protected final void writeImpl()
	{
		writeC(0xf2);
		writeD(GameTimeController.getInstance().getGameTime()); // time in client minutes
		writeD(6); // constant to match the server time( this determines the speed of the client clock)
	}

	@Override
	public String getType()
	{
		return _S__f2_CLIENTSETTIME;
	}
}
