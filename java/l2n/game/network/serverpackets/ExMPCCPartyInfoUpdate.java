package l2n.game.network.serverpackets;

public class ExMPCCPartyInfoUpdate extends L2GameServerPacket
{
	private static final String _S__FE_5B_EXMPCCPARTYINFOUPDATE = "[S] FE:5B ExMPCCPartyInfoUpdate";
	private final String leader_name;
	private final int leader_objId, members_count, _mode;

	/**
	 * @param party
	 * @param mode
	 *            0 = Remove, 1 = Add
	 */
	public ExMPCCPartyInfoUpdate(final String leaderName, final int leaderObjectId, final int memberCount, final int mode)
	{
		leader_name = leaderName;
		leader_objId = leaderObjectId;
		members_count = memberCount;
		_mode = mode;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x5B);
		writeS(leader_name);
		writeD(leader_objId);
		writeD(members_count);
		writeD(_mode);
	}

	@Override
	public String getType()
	{
		return _S__FE_5B_EXMPCCPARTYINFOUPDATE;
	}
}
