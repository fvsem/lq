package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class StopMoveInVehicle extends L2GameServerPacket
{
	private static final String _S__7F_STOPMOVEINVEHICLE = "[S] 7F StopMoveInVehicle";
	private final int _boatid, char_obj_id, char_heading;
	private final Location _loc;

	/**
	 * @param player
	 * @param boatid
	 */
	public StopMoveInVehicle(final L2Player player, final int boatid)
	{
		_boatid = boatid;
		char_obj_id = player.getObjectId();
		_loc = player.getInVehiclePosition();
		char_heading = player.getHeading();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x7f);
		writeD(char_obj_id);
		writeD(_boatid);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(char_heading);
	}

	@Override
	public String getType()
	{
		return _S__7F_STOPMOVEINVEHICLE;
	}
}
