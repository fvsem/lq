package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.util.Location;

public class ExAirShipInfo extends L2GameServerPacket
{
	private static final String _S__FE_60_EXAIRSHIPINFO = "[S] FE:60 ExAirShipInfo";

	private int _objId, _speed1, _speed2, _fuel, _maxFuel, _driverObjId, _helmId;
	private final Location _loc;

	public ExAirShipInfo(final L2AirShip ship)
	{
		_objId = ship.getObjectId();
		_loc = ship.getLoc();
		_speed1 = ship.getRunSpeed();
		_speed2 = ship.getRotationSpeed();
		if(ship.isClanAirShip())
		{
			_fuel = ship.getFuel();
			_maxFuel = L2AirShip.MAX_FUEL;
			_driverObjId = ship.getDriver() == null ? 0 : ship.getDriver().getObjectId();
			_helmId = ship.getOwner() == null ? 0 : ship.getOwner().getClanId();
		}
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x60);

		writeD(_objId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
		writeD(_driverObjId); // object id of player who control ship
		writeD(_speed1);
		writeD(_speed2);

		// clan airship related info
		writeD(_helmId); // clan-owner object id?

		// Clan Airship related info
		if(_helmId != 0)
		{
			writeD(0x16e); // Controller X
			writeD(0x00); // Controller Y
			writeD(0x6b); // Controller Z
			writeD(0x15c); // Captain X
			writeD(0x00); // Captain Y
			writeD(0x69); // Captain Z

		}
		else
		{
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
		}

		writeD(_fuel); // current fuel
		writeD(_maxFuel); // max fuel
	}

	@Override
	public String getType()
	{
		return _S__FE_60_EXAIRSHIPINFO;
	}
}
