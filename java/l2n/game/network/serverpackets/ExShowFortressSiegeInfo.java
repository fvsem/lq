package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.siege.SiegeSpawn;

/**
 * @author L2System Project
 */
public class ExShowFortressSiegeInfo extends L2GameServerPacket
{
	private final Fortress _fortress;
	private final int _fortId;
	private final int _barraksCount;

	public ExShowFortressSiegeInfo(final Fortress fort)
	{
		_fortress = fort;
		_fortId = fort.getId();
		_barraksCount = fort.getFortSize();
	}

	@Override
	public String getType()
	{
		return "[S] FE:17 ExShowFortressSiegeInfo";
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x17);

		writeD(_fortId); // Fortress Id
		writeD(_barraksCount); // Total Barracks Count
		final GArray<SiegeSpawn> commanders = FortressSiegeManager.getCommanderSpawnList(_fortId);
		if(commanders != null && !commanders.isEmpty())
			switch (commanders.size())
			{
				case 3:
					switch (_fortress.getSiege().getCommanders().size())
					{
						case 0:
							writeD(0x03);
							break;
						case 1:
							writeD(0x02);
							break;
						case 2:
							writeD(0x01);
							break;
						case 3:
							writeD(0x00);
							break;
					}
					break;
				case 4: // TODO: change 4 to 5 once control room supported
					switch (_fortress.getSiege().getCommanders().size())
					// TODO: once control room supported, update writeD(0x0x) to support 5th room
					{
						case 0:
							writeD(0x05);
							break;
						case 1:
							writeD(0x04);
							break;
						case 2:
							writeD(0x03);
							break;
						case 3:
							writeD(0x02);
							break;
						case 4:
							writeD(0x01);
							break;
					}
					break;
			}
		else
			for(int i = 0; i < _barraksCount; i++)
				writeD(0x00);
	}
}
