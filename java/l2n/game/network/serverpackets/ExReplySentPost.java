package l2n.game.network.serverpackets;

import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.items.MailParcelController.Letter;
import l2n.game.templates.L2Item;

/**
 * @author L2System Project
 * @date 30.07.2010
 * @time 11:59:12
 */
public class ExReplySentPost extends L2GameServerPacket
{
	private static final String _S__FE_AD_EXSHOWSENTPOST = "[S] FE:AD ExReplySentPost";

	private Letter _letter;

	public ExReplySentPost(final L2Player cha, final int post)
	{
		_letter = MailParcelController.getInstance().getLetter(post);
		if(_letter != null)
			return;

		_letter = new Letter();
		cha.sendPacket(new ExShowSentPostList(cha.getObjectId()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xad);

		writeD(_letter.id);
		writeD(_letter.type);

		writeS(_letter.receiverName);
		writeS(_letter.topic);
		writeS(_letter.body);

		writeD(_letter.attached.size());
		for(final TradeItem temp : _letter.attached)
		{
			final L2Item item = temp.getItem();
			writeH(item.getType1());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeD(temp.getEnchantLevel());

			writeH(0x00); // writeH(item.getCustomType2());
			writeH(0x00); // unknown
			writeD(0x00); // unknown

			writeD(0x00); // writeD(item.getAugmentation().getAugmentationId());

			writeD(0x00); // unknown

			writeItemElements(temp);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}

		writeQ(_letter.price);

		writeD(0x01); // Unknown why 1
		writeD(0x00); // Unknown
	}

	@Override
	public String getType()
	{
		return _S__FE_AD_EXSHOWSENTPOST;
	}
}
