package l2n.game.network.serverpackets;

/**
 * Opens the CommandChannel Information window
 */
public class ExMPCCOpen extends L2GameServerPacket
{

	private static final String _S__FE_12_EXMPCCOPEN = "[S] FE:12 ExMPCCOpen";

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x12);
	}

	@Override
	public String getType()
	{
		return _S__FE_12_EXMPCCOPEN;
	}
}
