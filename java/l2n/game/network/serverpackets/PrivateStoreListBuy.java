package l2n.game.network.serverpackets;

import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.util.concurrent.ConcurrentLinkedQueue;

public class PrivateStoreListBuy extends L2GameServerPacket
{
	private static final String _S__BE_PRIVATESTORELISTBUY = "[S] BE PrivateStoreListBuy";
	private final int buyer_id;
	private final long seller_adena;
	private final ConcurrentLinkedQueue<TradeItem> _buyerslist;

	/**
	 * Список вещей в личном магазине покупки, показываемый продающему
	 * 
	 * @param seller
	 * @param storePlayer
	 */
	public PrivateStoreListBuy(final L2Player seller, final L2Player storePlayer)
	{
		seller_adena = seller.getAdena();
		buyer_id = storePlayer.getObjectId();

		final ConcurrentLinkedQueue<L2ItemInstance> sellerItems = seller.getInventory().getItemsList();
		_buyerslist = new ConcurrentLinkedQueue<TradeItem>();
		_buyerslist.addAll(storePlayer.getBuyList());

		for(final TradeItem buyListItem : _buyerslist)
			buyListItem.setCurrentValue(0);

		for(final L2ItemInstance sellerItem : sellerItems)
			for(final TradeItem buyListItem : _buyerslist)
				if(sellerItem.getItemId() == buyListItem.getItemId() && sellerItem.canBeTraded(seller))
				{
					buyListItem.setCurrentValue(Math.min(buyListItem.getCount(), sellerItem.getCount()));
					continue;
				}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xBE);

		writeD(buyer_id);
		writeQ(seller_adena);
		writeD(_buyerslist.size());
		L2Item tmp;
		for(final TradeItem buyersitem : _buyerslist)
		{
			tmp = ItemTable.getInstance().getTemplate(buyersitem.getItemId());

			writeD(buyersitem.getObjectId());
			writeD(buyersitem.getItemId());
			writeH(buyersitem.getEnchantLevel());
			writeQ(buyersitem.getCurrentValue()); // give max possible sell amount
			writeQ(tmp.getReferencePrice());

			writeH(0);

			writeD(tmp.getBodyPart());
			writeH(tmp.getType2ForPackets());

			writeQ(buyersitem.getOwnersPrice());// buyers price
			writeQ(buyersitem.getCount()); // maximum possible tradecount

			writeItemElements(buyersitem);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__BE_PRIVATESTORELISTBUY;
	}
}
