package l2n.game.network.serverpackets;

/**
 * Format: (chd) ddd
 * d: Time Left
 * d: Blue Points
 * d: Red Points
 */
public class ExCubeGameChangePoints extends L2GameServerPacket
{
	private static final String ExCubeGameChangePoints = "[S] FE:98:02 ExCubeGameChangePoints";

	private final int _timeLeft;
	private final int _bluePoints;
	private final int _redPoints;

	/**
	 * Change Client Point Counter
	 * 
	 * @param timeLeft
	 *            Time Left before Minigame's End
	 * @param bluePoints
	 *            Current Blue Team Points
	 * @param redPoints
	 *            Current Red Team Points
	 */
	public ExCubeGameChangePoints(final int timeLeft, final int bluePoints, final int redPoints)
	{
		_timeLeft = timeLeft;
		_bluePoints = bluePoints;
		_redPoints = redPoints;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x98);
		writeD(0x02);

		writeD(_timeLeft);
		writeD(_bluePoints);
		writeD(_redPoints);
	}

	@Override
	public final String getType()
	{
		return ExCubeGameChangePoints;
	}
}
