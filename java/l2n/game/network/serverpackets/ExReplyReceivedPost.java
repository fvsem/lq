package l2n.game.network.serverpackets;

import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.items.MailParcelController.Letter;
import l2n.game.templates.L2Item;

/**
 * @author L2System Project
 * @date 30.07.2010
 * @time 0:17:35
 */
public class ExReplyReceivedPost extends L2GameServerPacket
{
	private static final String _S__FE_AB_EXSHOWRECEIVEDPOST = "[S] FE:AB ExShowReceivedPost";

	private Letter _letter;

	public ExReplyReceivedPost(final L2Player cha, final int post)
	{
		_letter = MailParcelController.getInstance().getLetter(post);
		if(_letter == null)
			_letter = new Letter();
		else if(_letter.unread > 0)
		{
			MailParcelController.getInstance().markMailRead(post);
			_letter.unread = 0;
		}

		if(cha != null)
			cha.sendPacket(new ExShowReceivedPostList(cha.getObjectId()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0xab);

		writeD(_letter.id);
		writeD(_letter.type);
		writeD(0x00); // Unknown
		writeS(_letter.senderName);
		writeS(_letter.topic);
		writeS(_letter.body);

		writeD(_letter.attached.size());
		for(final TradeItem temp : _letter.attached)
		{
			final L2Item item = temp.getItem();
			writeH(item.getType1());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeD(0x00); // unknown
			writeD(temp.getEnchantLevel());

			writeH(0x00); // writeH(item.getCustomType2());
			writeH(0x00);
			writeD(0x00);// writeD(item.isAugmented() ? item.getAugmentation().getAugmentationId() : 0x00);
			writeD(0x00);

			writeItemElements(temp);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}

		writeQ(_letter.price);
		writeD(_letter.attachments > 0 ? 0x01 : 0x00);
		writeD(_letter.system);
	}

	@Override
	public String getType()
	{
		return _S__FE_AB_EXSHOWRECEIVEDPOST;
	}
}
