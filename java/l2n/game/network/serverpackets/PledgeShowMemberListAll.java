package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.L2Clan.SubPledge;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;

import java.util.ArrayList;
import java.util.List;

public class PledgeShowMemberListAll extends L2GameServerPacket
{
	private static final String _S__5a_PLEDGESHOWMEMBERLISTALL = "[S] 5a PledgeShowMemberListAll";
	private final int clan_id, clan_crest_id, level, rank, rep,
			ally_id;
	private int ally_crest_id;
	private final int _pledgeType, HasCastle, HasHideout,
			HasFortress, AtWar;
	private final String clan_name, leader_name;
	private String ally_name;
	private final List<ClanMemberInfo> infos = new ArrayList<ClanMemberInfo>();

	public PledgeShowMemberListAll(final L2Clan _clan, final L2Player _activeChar)
	{
		_pledgeType = 0;
		clan_id = _clan.getClanId();
		clan_name = _clan.getName();
		leader_name = _clan.getLeaderName();
		clan_crest_id = _clan.getCrestId();
		level = _clan.getLevel();
		HasCastle = _clan.getHasCastle();
		HasHideout = _clan.getHasHideout();
		HasFortress = _clan.getHasFortress();
		rank = _clan.getRank();
		rep = _clan.getReputationScore();
		ally_id = _clan.getAllyId();
		if(_clan.getAlliance() != null)
		{
			ally_name = _clan.getAlliance().getAllyName();
			ally_crest_id = _clan.getAlliance().getAllyCrestId();
		}
		else
		{
			ally_name = "";
			ally_crest_id = 0;
		}
		AtWar = _clan.isAtWarOrUnderAttack();

		for(final SubPledge element : _clan.getAllSubPledges())
			_activeChar.sendPacket(new PledgeReceiveSubPledgeCreated(element));

		for(final L2ClanMember m : _clan.getMembers())
			if(m.getPledgeType() == _pledgeType)
				infos.add(new ClanMemberInfo(m.getName(), m.getLevel(), m.getClassId(), m.getSex(), m.getObjectId(), m.isOnline() ? 1 : 0, m.hasSponsor() ? 1 : 0));
			else
				_activeChar.sendPacket(new PledgeShowMemberListAdd(m));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x5a);

		writeD(0/* mainOrSubpledge */); // c5 main clan 0 or any subpledge 1?
		writeD(clan_id);
		writeD(_pledgeType); // c5 - possibly pledge type?
		writeS(clan_name);
		writeS(leader_name);
		writeD(clan_crest_id); // crest id .. is used again
		writeD(level);
		writeD(HasCastle);
		writeD(HasHideout);
		writeD(HasFortress);
		writeD(rank);
		writeD(rep);
		writeD(0);
		writeD(0);
		writeD(ally_id);
		writeS(ally_name);
		writeD(ally_crest_id);
		writeD(AtWar);

		writeD(0); // Territory castle ID

		writeD(infos.size());
		for(final ClanMemberInfo _info : infos)
		{
			writeS(_info._name);
			writeD(_info.level);
			writeD(_info.class_id);
			writeD(_info.sex); // Пол (не отображается)
			writeD(_info.obj_id); // writeD(1);
			writeD(_info.online); // 1=online 0=offline
			writeD(_info.has_sponsor);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__5a_PLEDGESHOWMEMBERLISTALL;
	}

	static class ClanMemberInfo
	{
		public String _name;
		public int level, class_id, sex, obj_id, online, has_sponsor;

		public ClanMemberInfo(final String __name, final int _level, final int _class_id, final int _sex, final int _obj_id, final int _online, final int _has_sponsor)
		{
			_name = __name;
			level = _level;
			class_id = _class_id;
			sex = _sex;
			obj_id = _obj_id;
			online = _online;
			has_sponsor = _has_sponsor;
		}
	}
}
