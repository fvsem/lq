package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.FortressSiegeManager;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.siege.SiegeSpawn;
import l2n.game.model.instances.L2CommanderInstance;

/**
 * @author L2System Project
 * @date 30.11.2009
 * @time 23:47:01
 */
public class ExShowFortressMapInfo extends L2GameServerPacket
{
	private final Fortress _fortress;

	public ExShowFortressMapInfo(final Fortress fortress)
	{
		_fortress = fortress;
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#getType()
	 */
	@Override
	public String getType()
	{
		return "[S] FE:7D ExShowFortressMapInfo";
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#writeImpl()
	 */
	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x7d);

		writeD(_fortress.getId());
		writeD(_fortress.getSiege().getIsInProgress() ? 1 : 0); // fortress siege status
		writeD(_fortress.getFortSize()); // barracks count

		final GArray<SiegeSpawn> commanders = FortressSiegeManager.getCommanderSpawnList(_fortress.getId());
		if(commanders != null && !commanders.isEmpty() && _fortress.getSiege().getIsInProgress())
			switch (commanders.size())
			{
				case 3:
				{
					for(final SiegeSpawn spawn : commanders)
						if(isSpawned(spawn.getNpcId()))
							writeD(0);
						else
							writeD(1);
					break;
				}
				case 4: // TODO: change 4 to 5 once control room supported
				{
					int count = 0;
					for(final SiegeSpawn spawn : commanders)
					{
						count++;
						if(count == 4)
							writeD(1); // TODO: control room emulated
						if(isSpawned(spawn.getNpcId()))
							writeD(0);
						else
							writeD(1);
					}
					break;
				}
			}
		else
			for(int i = 0; i < _fortress.getFortSize(); i++)
				writeD(0);
	}

	/**
	 * @param npcId
	 * @return
	 */
	private boolean isSpawned(final int npcId)
	{
		boolean ret = false;
		for(final L2CommanderInstance spawn : _fortress.getSiege().getCommanders())
			if(spawn.getNpcId() == npcId)
				ret = true;
		return ret;
	}
}
