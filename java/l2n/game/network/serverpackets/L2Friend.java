package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class L2Friend extends L2GameServerPacket
{
	private static final String _S__76_L2FRIEND = "[S] 0x76 L2FRIEND";

	private final boolean _add;
	private final String _name;
	private final boolean _online;
	private final int _object_id;

	public L2Friend(final L2Player player, final boolean add)
	{
		_add = add;
		_name = player.getName();
		_object_id = player.getObjectId();
		_online = true;
	}

	public L2Friend(final String name, final boolean add, final boolean online, final int object_id)
	{
		_name = name;
		_add = add;
		_object_id = object_id;
		_online = online;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x76);
		writeD(_add ? 1 : 3); // 1 - добавить друга в спикок, 3 удалить друга со списка
		writeD(0); // и снова тут идет ID персонажа в списке оффа, не object id
		writeS(_name);
		writeD(_online ? 1 : 0); // онлайн или оффлайн
		writeD(_object_id); // object_id if online
	}

	@Override
	public String getType()
	{
		return _S__76_L2FRIEND;
	}
}
