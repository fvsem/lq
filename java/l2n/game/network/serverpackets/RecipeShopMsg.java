package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class RecipeShopMsg extends L2GameServerPacket
{
	private static final String _S__e1_RecipeShopMsg = "[S] e1 RecipeShopMsg";
	private int _chaObjectId;
	private String _chaStoreName;

	public RecipeShopMsg(final L2Player player)
	{
		if(player.getCreateList() == null || player.getCreateList().getStoreName() == null)
			return;
		_chaObjectId = player.getObjectId();
		_chaStoreName = player.getCreateList().getStoreName();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe1);
		writeD(_chaObjectId);
		writeS(_chaStoreName);
	}

	@Override
	public String getType()
	{
		return _S__e1_RecipeShopMsg;
	}
}
