package l2n.game.network.serverpackets;

public class ExPledgeCrestLarge extends L2GameServerPacket
{
	private static final String _S__FE_1b_PLEDGECRESTLARGE = "[S] FE:1b PledgeCrestLarge";

	private final int _crestId;
	private final byte[] _data;

	public ExPledgeCrestLarge(final int crestId, final byte[] data)
	{
		_crestId = crestId;
		_data = data;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x1b);

		writeD(0x00);
		writeD(_crestId);
		writeD(_data.length);
		writeB(_data);
	}

	@Override
	public String getType()
	{
		return _S__FE_1b_PLEDGECRESTLARGE;
	}
}
