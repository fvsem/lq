package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 03.10.2010
 * @time 20:30:27
 */
public class ExSubPledgeSkillAdd extends L2GameServerPacket
{
	private final int skill_id;
	private final int skill_lvl;
	private final int _clanSubId;

	public ExSubPledgeSkillAdd(final int id, final int lvl, final int sub)
	{
		skill_id = id;
		skill_lvl = lvl;
		_clanSubId = sub;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x76);

		writeD(_clanSubId);
		writeD(skill_id);
		writeD(skill_lvl);
	}

	@Override
	public final String getType()
	{
		return "[S] FE:76 ExSubPledgetSkillAdd";
	}
}
