package l2n.game.network.serverpackets;

public class PledgeReceiveUpdatePower extends L2GameServerPacket
{
	private static final String _S__FE_42_PLEDGERECEIVEUPDATEPOWER = "[S] FE:42 PledgeReceiveUpdatePower";
	private final int _privs;

	public PledgeReceiveUpdatePower(final int privs)
	{
		_privs = privs;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x42);
		writeD(_privs); // Filler??????
	}

	@Override
	public String getType()
	{
		return _S__FE_42_PLEDGERECEIVEUPDATEPOWER;
	}
}
