package l2n.game.network.serverpackets;

public class ExVariationCancelResult extends L2GameServerPacket
{
	private static final String _S__FE_58_EXVARIATIONCANCELRESULT = "[S] FE:58 ExVariationCancelResult";
	private final int _closeWindow;
	private final int _unk1;

	public static final ExVariationCancelResult STATIC_FAIL = new ExVariationCancelResult(0);
	public static final ExVariationCancelResult STATIC_SUCCESS = new ExVariationCancelResult(1);

	public ExVariationCancelResult(final int result)
	{
		_closeWindow = 1;
		_unk1 = result;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x58);
		writeD(_closeWindow);
		writeD(_unk1);
	}

	@Override
	public String getType()
	{
		return _S__FE_58_EXVARIATIONCANCELRESULT;
	}
}
