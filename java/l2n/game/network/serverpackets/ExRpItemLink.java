package l2n.game.network.serverpackets;

import javolution.util.FastMap;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.instances.L2ItemInstance;

public class ExRpItemLink extends L2GameServerPacket
{
	private static final String ExRpItemLink = "[S] FE 6C ExRpItemLink";

	private static final FastMap<Integer, L2ItemInstance> _cache = new FastMap<Integer, L2ItemInstance>().shared();
	private L2ItemInstance _item;

	/**
	 * Вызывается из GameTimeController.BroadcastSunState.run()
	 * просто чтобы не заводить лишний таймер
	 */
	public static void cleanCache()
	{
		_cache.clear();
	}

	public ExRpItemLink(final int id)
	{
		_item = _cache.get(id);
		if(_item == null)
		{
			final L2Object tmp = L2ObjectsStorage.findObject(id);
			if(tmp != null && tmp.isItem())
			{
				_item = (L2ItemInstance) tmp;
				_cache.put(id, _item);
			}
		}
	}

	@Override
	protected final void writeImpl()
	{
		if(_item == null)
			return;
		// dddhdhhhdddddddddd
		writeC(EXTENDED_PACKET);
		writeH(0x6c);

		writeD(_item.getObjectId());
		writeD(_item.getItemId());
		writeQ(_item.getCount());
		writeH(_item.getItem().getType2ForPackets());
		writeD(_item.getItem().getBodyPart());
		writeH(_item.getEnchantLevel());
		writeH(_item.getCustomType2()); // item type3
		writeH(0); // unknown
		writeD(_item.getAugmentationId());
		writeD(_item.isShadowItem() ? _item.getLifeTimeRemaining() : -1); // unknown
		// T1
		writeItemElements(_item);

		writeH(0x00); // Enchant effect 1
		writeH(0x00); // Enchant effect 2
		writeH(0x00); // Enchant effect 3
	}

	@Override
	public String getType()
	{
		return ExRpItemLink;
	}
}
