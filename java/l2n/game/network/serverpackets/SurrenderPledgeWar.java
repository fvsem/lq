package l2n.game.network.serverpackets;

public class SurrenderPledgeWar extends L2GameServerPacket
{
	private static final String _S__67_SURRENDERPLEDGEWAR = "[S] 67 SurrenderPledgeWar";
	private final String _pledgeName;
	private final String _char;

	public SurrenderPledgeWar(final String pledge, final String charName)
	{
		_pledgeName = pledge;
		_char = charName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x67);
		writeS(_pledgeName);
		writeS(_char);
	}

	@Override
	public String getType()
	{
		return _S__67_SURRENDERPLEDGEWAR;
	}
}
