package l2n.game.network.serverpackets;

public class ChooseInventoryItem extends L2GameServerPacket
{
	private static final String _S__7c_CHOOSEINVENTORYITEM = "[S] 7c ChooseInventoryItem";
	private final int ItemID;

	public ChooseInventoryItem(final int id)
	{
		ItemID = id;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x7c);
		writeD(ItemID);
	}

	@Override
	public String getType()
	{
		return _S__7c_CHOOSEINVENTORYITEM;
	}
}
