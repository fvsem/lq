package l2n.game.network.serverpackets;

public class RestartResponse extends L2GameServerPacket
{
	private static final String _S__71_RESTARTRESPONSE = "[S] 71 RestartResponse";

	public static final RestartResponse OK = new RestartResponse(1);
	public static final RestartResponse FAIL = new RestartResponse(0);
	private final String _message;
	private final int _param;

	public RestartResponse(final int param)
	{
		_message = "bye";
		_param = param;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x71);
		writeD(_param);
		writeS(_message);
	}

	@Override
	public String getType()
	{
		return _S__71_RESTARTRESPONSE;
	}
}
