package l2n.game.network.serverpackets;

/**
 * Открывает окно аугмента, название от фонаря.
 */
public class ExShowVariationMakeWindow extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x51);
	}

	@Override
	public String getType()
	{
		return "[S] FE:51 ExShowVariationMakeWindow";
	}
}
