package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.actor.L2Player;

public class ExListPartyMatchingWaitingRoom extends L2GameServerPacket
{
	private final GArray<L2Player> _waiting;
	private final int _fullSize;

	public ExListPartyMatchingWaitingRoom(final L2Player searcher, final int minLevel, final int maxLevel, final int page)
	{
		_waiting = new GArray<L2Player>();
		final int first = (page - 1) * 64;
		final int firstNot = page * 64;

		int i = 0;
		final GArray<L2Player> temp = PartyRoomManager.getInstance().getWaitingList(minLevel, maxLevel);
		_fullSize = temp.size();
		for(final L2Player pc : temp)
		{
			if(i < first || i >= firstNot)
				continue;
			_waiting.add(pc);
			i++;
		}
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x36);

		writeD(_fullSize);

		writeD(_waiting.size());
		for(final L2Player p : _waiting)
		{
			writeS(p.getName());
			writeD(p.getClassId().getId());
			writeD(p.getLevel());
		}
	}

	@Override
	public String getType()
	{
		return "FE_36_ExListPartyMatchingWaitingRoom";
	}
}
