package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

/**
 * sample
 * 06 8f19904b 2522d04b 00000000 80 950c0000 4af50000 08f2ffff 0000 - 0 damage (missed 0x80)
 * 06 85071048 bc0e504b 32000000 10 fc41ffff fd240200 a6f5ffff 0100 bc0e504b 33000000 10 3....
 * format
 * dddc dddh (ddc)
 * 
 * @version $Revision: 1.3.2.1.2.4 $ $Date: 2005/03/27 15:29:39 $
 */
public class Attack extends L2GameServerPacket
{
	private class Hit
	{
		int _targetId;
		int _damage;
		int _flags;

		Hit(final L2Object target, final int damage, final boolean miss, final boolean crit, final boolean shld)
		{
			_targetId = target.getObjectId();
			_damage = damage;
			if(_soulshot)
				_flags = 0x10 | _ssGrade;
			if(crit)
				_flags |= 0x20;
			// dirty fix for lags on olympiad
			if(shld && !(target.isPlayer() && ((L2Player) target).getOlympiadGameId() > -1))
				_flags |= 0x40;
			if(miss)
				_flags |= 0x80;
		}
	}

	// dh

	private static final String _S__33_ATTACK = "[S] 33 Attack";
	private final int _attackerId;
	private final int _targetObjId;
	public final boolean _soulshot;
	public final int _ssGrade;
	private final int _x;
	private final int _y;
	private final int _z;
	private final int _targetX;
	private final int _targetY;
	private final int _targetZ;
	private Hit[] hits;

	/**
	 * @param attacker
	 *            the attacker L2Character
	 * @param ss
	 *            true if useing SoulShots
	 */
	public Attack(final L2Character attacker, final L2Character target, final boolean ss, final int ssGrade)
	{
		_attackerId = attacker.getObjectId();
		_targetObjId = target.getObjectId();
		_soulshot = ss;
		_ssGrade = ssGrade;
		_x = attacker.getX();
		_y = attacker.getY();
		_z = attacker.getZ();
		_targetX = target.getX();
		_targetY = target.getY();
		_targetZ = target.getZ();
		hits = new Hit[0];
	}

	/**
	 * Add this hit (target, damage, miss, critical, shield) to the Server-Client packet Attack.<BR>
	 * <BR>
	 */
	public void addHit(final L2Object target, final int damage, final boolean miss, final boolean crit, final boolean shld)
	{
		// Get the last position in the hits table
		final int pos = hits.length;

		// Create a new Hit object
		final Hit[] tmp = new Hit[pos + 1];

		// Add the new Hit object to hits table
		System.arraycopy(hits, 0, tmp, 0, hits.length);
		tmp[pos] = new Hit(target, damage, miss, crit, shld);
		hits = tmp;
	}

	/**
	 * Return True if the Server-Client packet Attack conatins at least 1 hit.<BR>
	 * <BR>
	 */
	public boolean hasHits()
	{
		return hits.length > 0;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x33);

		writeD(_attackerId);
		writeD(_targetObjId);
		writeD(hits[0]._damage);
		writeC(hits[0]._flags);
		writeD(_x);
		writeD(_y);
		writeD(_z);
		writeH(hits.length - 1);
		// prevent sending useless packet while there is only one target.
		if(hits.length > 1)
			for(int i = 1; i < hits.length; i++)
			{
				writeD(hits[i]._targetId);
				writeD(hits[i]._damage);
				writeC(hits[i]._flags);
			}
		// ct2.3 final
		writeD(_targetX);
		writeD(_targetY);
		writeD(_targetZ);
	}

	@Override
	public String getType()
	{
		return _S__33_ATTACK;
	}
}
