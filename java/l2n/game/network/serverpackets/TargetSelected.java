package l2n.game.network.serverpackets;

import l2n.util.Location;

/**
 * format dddddd
 * sample
 * 0000: 23 0b 07 10 48 3e 31 10 48 3a f6 00 00 91 5b 00
 * 0010: 00 4c f1 ff ff 00 00 00 00
 */
public class TargetSelected extends L2GameServerPacket
{
	private static final String _S__23_TARGETSELECTED = "[S] 23 TargetSelected";
	private final int _objectId;
	private final int _targetId;
	private final Location _loc;

	public TargetSelected(final int objectId, final int targetId, final Location loc)
	{
		_objectId = objectId;
		_targetId = targetId;
		_loc = loc;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x23);
		writeD(_objectId);
		writeD(_targetId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(0x00);
	}

	@Override
	public String getType()
	{
		return _S__23_TARGETSELECTED;
	}
}
