package l2n.game.network.serverpackets;

import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;

/**
 * sample
 * format
 * d
 */
public class ShowMiniMap extends L2GameServerPacket
{
	private static final String _S__a3_SHOWMINIMAP = "[S] a3 ShowMiniMap";
	private int _mapId, period;
	private boolean canWriteImpl = true;

	public ShowMiniMap(final L2Player player, final int mapId)
	{
		_mapId = mapId;
		period = SevenSigns.getInstance().getCurrentPeriod();
		final L2Zone hellBoundTerrytory = ZoneManager.getInstance().getZoneById(ZoneType.dummy, 704004, false);
		if(hellBoundTerrytory == null || !player.isInZone(hellBoundTerrytory) || Functions.getItemCount(player, 9994) != 0)
			return;
		player.sendPacket(Msg.THIS_IS_AN_AREA_WHERE_YOU_CANNOT_USE_THE_MINI_MAP_THE_MINI_MAP_WILL_NOT_BE_OPENED);

		canWriteImpl = false;
	}

	@Override
	protected final void writeImpl()
	{
		if(!canWriteImpl)
			return;
		writeC(0xa3);
		writeD(_mapId);
		writeD(period);
	}

	@Override
	public String getType()
	{
		return _S__a3_SHOWMINIMAP;
	}
}
