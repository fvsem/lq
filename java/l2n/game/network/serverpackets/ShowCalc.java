package l2n.game.network.serverpackets;

public class ShowCalc extends L2GameServerPacket
{
	private static final String _S__DC_SHOWCALCULATOR = "[S] e2 ShowCalculator";
	private final int _calculatorId;

	public ShowCalc(final int calculatorId)
	{
		_calculatorId = calculatorId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe2);
		writeD(_calculatorId);
	}

	@Override
	public String getType()
	{
		return _S__DC_SHOWCALCULATOR;
	}
}
