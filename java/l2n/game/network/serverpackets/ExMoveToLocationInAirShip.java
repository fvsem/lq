package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.util.Location;

/**
 * @author L2System Project
 * @date 30.11.2009
 * @time 23:05:34
 */
public class ExMoveToLocationInAirShip extends L2GameServerPacket
{
	private static final String _S__6D_EXAIRSHIPMOVETOLOCATION = "[S] 6D MoveToLocationInAirShip";

	private final int char_id;
	private final int boat_id;
	private final Location _origin;
	private final Location _destination;

	public ExMoveToLocationInAirShip(final L2Player cha, final L2AirShip boat, final Location origin, final Location destination)
	{
		char_id = cha.getObjectId();
		boat_id = boat.getObjectId();
		_origin = origin;
		_destination = destination;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x6D);

		writeD(char_id);
		writeD(boat_id);
		writeD(_destination.x);
		writeD(_destination.y);
		writeD(_destination.z);
		writeD(_origin.x);
		writeD(_origin.y);
		writeD(_origin.z);
	}

	@Override
	public String getType()
	{
		return _S__6D_EXAIRSHIPMOVETOLOCATION;
	}
}
