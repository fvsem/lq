package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 07.07.2010
 * @time 19:01:14
 */
public class SendTradeDone extends L2GameServerPacket
{
	private static final String _S__1c_SENDTRADEDONE = "[S] 1c SendTradeDone";

	public static final SendTradeDone Success = new SendTradeDone(1);
	public static final SendTradeDone Fail = new SendTradeDone(0);

	private final int _num;

	public SendTradeDone(final int num)
	{
		_num = num;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x1c);
		writeD(_num);
	}

	@Override
	public String getType()
	{
		return _S__1c_SENDTRADEDONE;
	}
}
