package l2n.game.network.serverpackets;

/**
 * Opens a dialog to change your title (nickname) and name color.
 * 
 * @author L2System Project
 * @date 02.12.2010
 * @time 12:42:08
 */
public class ExChangeNicknameNColor extends L2GameServerPacket
{
	private static final String _S__EXCHANGENICKNAMENCOLOR = "[S] FE:83 ExChangeNicknameNColor";

	private final int _itemObjectId;

	public ExChangeNicknameNColor(final int itemObjectId)
	{
		_itemObjectId = itemObjectId;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x83);
		writeD(_itemObjectId);
	}

	@Override
	public String getType()
	{
		return _S__EXCHANGENICKNAMENCOLOR;
	}
}
