package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManorManager.SeedProduction;
import l2n.game.model.L2Manor;

/**
 * format(packet 0xFE)
 * ch ddd [dddddcdcd]
 * c - id
 * h - sub id
 * d - manor id
 * d
 * d - size
 * [
 * d - seed id
 * d - left to buy
 * d - started amount
 * d - sell price
 * d - seed level
 * c
 * d - reward 1 id
 * c
 * d - reward 2 id
 * ]
 */
public class ExShowSeedInfo extends L2GameServerPacket
{
	private static final String _S__FE_23_EXSHOWSEEDINFO = "[S] FE:23 ExShowSeedInfo";
	private GArray<SeedProduction> _seeds;
	private final int _manorId;

	public ExShowSeedInfo(final int manorId, final GArray<SeedProduction> seeds)
	{
		_manorId = manorId;
		_seeds = seeds;
		if(_seeds == null)
			_seeds = new GArray<SeedProduction>();
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET); // Id
		writeH(0x23); // SubId
		writeC(0);
		writeD(_manorId); // Manor ID
		writeD(0);
		if(_seeds == null)
		{
			writeD(0);
			return;
		}
		writeD(_seeds.size());
		for(final SeedProduction seed : _seeds)
		{
			writeD(seed.getId()); // Seed id
			writeQ(seed.getCanProduce()); // Left to buy
			writeQ(seed.getStartProduce()); // Started amount
			writeQ(seed.getPrice()); // Sell Price
			writeD(L2Manor.getInstance().getSeedLevel(seed.getId())); // Seed Level
			writeC(1); // reward 1 Type
			writeD(L2Manor.getInstance().getRewardItemBySeed(seed.getId(), 1)); // Reward 1 Type Item Id
			writeC(1); // reward 2 Type
			writeD(L2Manor.getInstance().getRewardItemBySeed(seed.getId(), 2)); // Reward 2 Type Item Id
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_23_EXSHOWSEEDINFO;
	}
}
