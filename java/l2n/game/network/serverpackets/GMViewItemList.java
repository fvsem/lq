package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

public class GMViewItemList extends L2GameServerPacket
{
	private static final String _S__AD_GMVIEWITEMLIST = "[S] 94 GMViewItemList";
	private final L2ItemInstance[] _items;
	private final L2Player _player;
	private final String _playerName;

	public GMViewItemList(final L2Player cha)
	{
		_items = cha.getInventory().getItems();
		_playerName = cha.getName();
		_player = cha;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x9a);
		writeS(_playerName);
		writeD(_player.getInventoryLimit()); // inventory limit
		writeH(0x01); // show window ??
		writeH(_items.length);

		for(final L2ItemInstance temp : _items)
		{
			if(temp == null || temp.getItem() == null)
				continue;

			writeH(temp.getItem().getType1()); // item type1

			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeD(temp.getEquipSlot()); // T1
			writeQ(temp.getCount());
			writeH(temp.getItem().getType2ForPackets()); // item type2
			writeH(temp.getCustomType1()); // ?
			writeH(temp.isEquipped() ? 0x01 : 0x00);
			writeD(temp.getItem().getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
			writeH(temp.getEnchantLevel()); // enchant level
			writeH(temp.getCustomType2()); // ?

			if(temp.isAugmented())
				writeD(temp.getAugmentation().getAugmentationId());
			else
				writeD(0x00);

			writeD(temp.isShadowItem() ? temp.getLifeTimeRemaining() : -1); // interlude
			writeItemElements(temp);
			// t2
			writeD(temp.isTemporalItem() ? temp.getLifeTimeRemaining() : 0); // limited time item life remaining

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__AD_GMVIEWITEMLIST;
	}
}
