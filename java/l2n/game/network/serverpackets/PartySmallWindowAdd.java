package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class PartySmallWindowAdd extends L2GameServerPacket
{
	// dddSdddddddddd
	// dddSdddddddddddddd
	private static final String _S__4F_PARTYSMALLWINDOWADD = "[S] 4F PartySmallWindowAdd";
	private final PartySmallWindowAll.MemberInfo member;

	public PartySmallWindowAdd(final L2Player _member)
	{
		member = new PartySmallWindowAll.MemberInfo(_member);
	}

	@Override
	protected final void writeImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		writeC(0x4F);
		writeD(player.getObjectId());
		writeD(0);

		writeD(member._id);
		writeS(member._name);
		writeD(member.curCp);
		writeD(member.maxCp);
		writeD(member.curHp);
		writeD(member.maxHp);
		writeD(member.curMp);
		writeD(member.maxMp);
		writeD(member.level);
		writeD(member.class_id);
		writeD(0);
		writeD(0);
	}

	@Override
	public String getType()
	{
		return _S__4F_PARTYSMALLWINDOWADD;
	}
}
