package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class EtcStatusUpdate extends L2GameServerPacket
{
	/**
	 * Packet for lvl 3 client buff line
	 * Example:(C4)
	 * F9 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 - empty statusbar
	 * F9 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 - increased force lvl 1
	 * F9 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 - weight penalty lvl 1
	 * F9 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 - chat banned
	 * F9 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 - Danger Area lvl 1
	 * F9 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 - lvl 1 grade penalty
	 * packet format: cdd //and last three are ddd???
	 * Some test results:
	 * F9 07 00 00 00 04 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 - lvl 7 increased force lvl 4 weight penalty
	 * Example:(C5 709)
	 * F9 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 0F 00 00 00 - lvl 1 charm of courage lvl 15 Death Penalty
	 * NOTE:
	 * End of buff:
	 * You must send empty packet
	 * F9 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
	 * to remove the statusbar or just empty value to remove some icon.
	 */

	private static final String _S__f9_ETCSTATUSUPDATE = "[S] f9 EtcStatusUpdate";
	private int increased_force, weight_penalty, message_refusal, danger_area;
	private int expertise_weapon_penalty, expertise_armor_penalty, charm_courage, death_penalty_level, consumed_souls;
	private boolean can_writeImpl = false;

	public EtcStatusUpdate(final L2Player player)
	{
		if(player == null || player.getActiveClass() == null)
			return;
		increased_force = player.getIncreasedForce();
		weight_penalty = player.getWeightPenalty();
		message_refusal = player.getMessageRefusal() || player.getNoChannel() != 0 || player.isBlockAll() ? 1 : 0;
		danger_area = player.isInDangerArea() ? 1 : 0;
		expertise_weapon_penalty = player.getWeaponsExpertisePenalty();
		expertise_armor_penalty = player.getArmorExpertisePenalty();
		charm_courage = player.isCharmOfCourage() ? 1 : 0;
		death_penalty_level = player.getDeathPenalty() == null ? 0 : player.getDeathPenalty().getLevel();
		consumed_souls = player.getConsumedSouls();
		can_writeImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!can_writeImpl)
			return;

		// dddddddd
		writeC(0xf9); // Packet type
		writeD(increased_force); // skill id 4271, 7 lvl
		writeD(weight_penalty); // skill id 4270, 4 lvl
		writeD(message_refusal); // skill id 4269, 1 lvl
		writeD(danger_area); // skill id 4268, 1 lvl
		writeD(expertise_weapon_penalty); // Weapon Grade Penalty [1-4]
		writeD(expertise_armor_penalty); // Armor Grade Penalty [1-4]
		writeD(charm_courage); // Charm of Courage, "Prevents experience value decreasing if killed during a siege war".
		writeD(death_penalty_level); // Death Penalty max lvl 15, "Combat ability is decreased due to death."
		writeD(consumed_souls);
	}

	@Override
	public String getType()
	{
		return _S__f9_ETCSTATUSUPDATE;
	}
}
