package l2n.game.network.serverpackets;

import l2n.game.event.L2EventType;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.Siege;
import l2n.game.model.entity.siege.SiegeClan;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.ReflectionTable;

public class Die extends L2GameServerPacket
{
	public static final int FEATHER_OF_BLESSING_1 = 10649;
	public static final int FEATHER_OF_BLESSING_2 = 13300;
	public static final int PHOENIX_FEATHER = 13128;

	public static final int FEATHERS[] = new int[] { FEATHER_OF_BLESSING_1, FEATHER_OF_BLESSING_2, PHOENIX_FEATHER };

	private static final String _S__00_DIE = "[S] 00 Die";
	private final int _chaId;
	private final boolean is_fake;
	private boolean is_sweepable;
	private L2Clan _clan;
	private final L2Character _cha;
	private int to_village, to_hideaway, to_castle, to_siege_HQ, to_fortress, to_resurrect_fixed;
	private boolean showFeather = false;

	/**
	 * @param _characters
	 */
	public Die(final L2Character cha)
	{
		_cha = cha;
		to_village = 0x01; // по умочанию - можно рес в ближайшую деревню

		if(cha.isPlayer())
		{
			final L2Player player = (L2Player) cha;
			to_resurrect_fixed = player.getPlayerAccess().ResurectFixed ? 0x01 : 0x00;
			_clan = player.getClan();
			// в тюряге ресаться только на месте
			if(player.isInJail())
			{
				to_village = 0x00; // рес в деревню нельзя
				to_resurrect_fixed = 0x01; // разрешим в этот раз рес на месте (как ГМ)
			}
			else
				for(final int itemId : FEATHERS)
					if(player.getInventory().getItemByItemId(itemId) != null)
					{
						showFeather = true;
						break;
					}
		}
		_chaId = cha.getObjectId();
		is_fake = !cha.isDead();
		if(cha.isMonster())
			is_sweepable = ((L2MonsterInstance) cha).isSweepActive();

		if(_clan != null)
		{
			SiegeClan siegeClan = null;
			final Siege siege = SiegeManager.getSiege(_cha, true);
			if(siege != null)
				siegeClan = siege.getAttackerClan(_clan);
			if(TerritorySiege.checkIfInZone(_cha))
				siegeClan = TerritorySiege.getSiegeClan(_clan);

			to_hideaway = _clan.getHasHideout() > 0 ? 0x01 : 0x00;
			to_castle = _clan.getHasCastle() > 0 ? 0x01 : 0x00;
			to_siege_HQ = siegeClan != null && siegeClan.getHeadquarter() != null ? 0x01 : 0x00;
			to_fortress = _clan.getHasFortress() > 0 ? 0x01 : 0x00;
		}
		else
		{
			to_hideaway = 0;
			to_castle = 0;
			to_siege_HQ = 0;
			to_fortress = 0;
		}

		// если на эвенте то не отображаем рес.
		if(_cha.isPlayer() && _cha.getPlayer().isInEvent(L2EventType.NONE) && _cha.getReflectionId() != ReflectionTable.EVENT_TOWN_SIEGE)
		{
			to_village = 0;
			to_hideaway = 0;
			to_castle = 0;
			to_siege_HQ = 0;
			to_fortress = 0;
			to_resurrect_fixed = 0;
			showFeather = false;
		}
	}

	@Override
	protected final void writeImpl()
	{
		if(!is_fake)
		{
			writeC(0x00);
			writeD(_chaId);
			writeD(to_village); // to nearest village
			writeD(to_hideaway); // to hide away (clanhall)
			writeD(to_castle); // to castle
			writeD(to_siege_HQ); // to siege HQ
			writeD(is_sweepable ? 0x01 : 0x00); // sweepable (blue glow)
			writeD(to_resurrect_fixed); // resurrect fixed
			writeD(to_fortress); // to fortress

			writeC(0x00); // show die animation
			writeD(showFeather ? 0x01 : 0x00); // Feather of Blessing ress button
			writeD(0x00); // additional free space
		}
	}

	@Override
	public String getType()
	{
		return _S__00_DIE;
	}
}
