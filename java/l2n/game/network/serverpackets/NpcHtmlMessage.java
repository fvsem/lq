package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.Scripts;
import l2n.extensions.scripts.Scripts.ScriptClassAndMethod;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Files;
import l2n.util.StringUtil;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * the HTML parser in the client knowns these standard and non-standard tags and attributes
 * VOLUMN
 * UNKNOWN
 * UL
 * U
 * TT
 * TR
 * TITLE
 * TEXTCODE
 * TEXTAREA
 * TD
 * TABLE
 * SUP
 * SUB
 * STRIKE
 * SPIN
 * SELECT
 * RIGHT
 * PRE
 * P
 * OPTION
 * OL
 * MULTIEDIT
 * LI
 * LEFT
 * INPUT
 * IMG
 * I
 * HTML
 * H7
 * H6
 * H5
 * H4
 * H3
 * H2
 * H1
 * FONT
 * EXTEND
 * EDIT
 * COMMENT
 * COMBOBOX
 * CENTER
 * BUTTON
 * BR
 * BODY
 * BAR
 * ADDRESS
 * A
 * SEL
 * LIST
 * VAR
 * FORE
 * READONL
 * ROWS
 * VALIGN
 * FIXWIDTH
 * BORDERCOLORLI
 * BORDERCOLORDA
 * BORDERCOLOR
 * BORDER
 * BGCOLOR
 * BACKGROUND
 * ALIGN
 * VALU
 * READONLY
 * MULTIPLE
 * SELECTED
 * TYP
 * TYPE
 * MAXLENGTH
 * CHECKED
 * SRC
 * Y
 * X
 * QUERYDELAY
 * NOSCROLLBAR
 * IMGSRC
 * B
 * FG
 * SIZE
 * FACE
 * COLOR
 * DEFFON
 * DEFFIXEDFONT
 * WIDTH
 * VALUE
 * TOOLTIP
 * NAME
 * MIN
 * MAX
 * HEIGHT
 * DISABLED
 * ALIGN
 * MSG
 * LINK
 * HREF
 * ACTION
 */
public class NpcHtmlMessage extends L2GameServerPacket
{

	private static final Logger _log = Logger.getLogger(NpcHtmlMessage.class.getName());
	// d S
	// d is usually 0, S is the html text starting with <html> and ending with </html>
	//
	private static final String _S__19_NPCHTMLMESSAGE = "[S] 19 NpcHtmlMessage";
	private int _npcObjId;
	private int _questId;
	private String _html;
	private String _file = null;
	private final GArray<String> replaces = new GArray<String>();
	private int item_id = 0;
	private boolean have_appends = false;

	private static final Pattern objectId = Pattern.compile("%objectId%");
	private static final Pattern playername = Pattern.compile("%playername%");

	public NpcHtmlMessage(final L2Player player, final L2NpcInstance npc, final String filename, final int val)
	{
		_npcObjId = npc.getObjectId();

		player.setLastNpc(npc);

		final GArray<ScriptClassAndMethod> appends = Scripts.dialogAppends.get(npc.getNpcId());
		if(appends != null && appends.size() > 0)
		{
			have_appends = true;
			if(filename != null && filename.equalsIgnoreCase("data/html/npcdefault.htm"))
				setHtml(""); // контент задается скриптами через DialogAppend_
			else
				setFile(filename);

			String replaces = "";

			// Добавить в конец странички текст, определенный в скриптах.
			final Object[] script_args = new Object[] { new Integer(val) };
			for(final ScriptClassAndMethod append : appends)
			{
				final Object obj = player.callScripts(append.scriptClass, append.method, script_args);
				if(obj != null)
					replaces += obj;
			}

			if(!replaces.equals(""))
				replace("</body>", "\n" + Strings.bbParse(replaces) + "</body>");
		}
		else
			setFile(filename);

		replace("%npcId%", String.valueOf(npc.getNpcId()));
		replace("%npcname%", npc.getName());
		replace("%festivalMins%", SevenSignsFestival.getInstance().getTimeToNextFestivalStr());
	}

	public NpcHtmlMessage(final L2Player player, final L2NpcInstance npc)
	{
		if(npc == null)
		{
			_npcObjId = 5;
			player.setLastNpc(null);
		}
		else
		{
			_npcObjId = npc.getObjectId();
			player.setLastNpc(npc);
		}
	}

	public NpcHtmlMessage(final int npcObjId)
	{
		_npcObjId = npcObjId;
	}

	public final NpcHtmlMessage setHtml(String text)
	{
		if(!text.contains("<html>"))
			text = StringUtil.concat("<html><body>", text, "</body></html>");
		_html = text;
		return this;
	}

	public final NpcHtmlMessage setFile(final String file)
	{
		_file = file;
		return this;
	}

	public final NpcHtmlMessage setItemId(final int _item_id)
	{
		item_id = _item_id;
		return this;
	}

	public void setQuest(final int quest)
	{
		_questId = quest;
	}

	protected String html_load(final String name, final String lang)
	{
		String content = Files.read(name, lang);
		if(content == null)
			content = "Can't find file'" + name + "'";
		return content;
	}

	public NpcHtmlMessage replace(final String pattern, final String value)
	{
		replaces.add(pattern);
		replaces.add(value);
		return this;
	}

	@Override
	protected final void writeImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		if(_file != null)
		{
			if(player.isGM())
				Functions.sendDebugMessage(player, "HTML: " + _file);
			final String content = Files.read(_file, player);
			if(content == null)
				setHtml(have_appends && _file.endsWith(".htm") ? "" : _file);
			else
				setHtml(content);
		}

		for(int i = 0; i < replaces.size(); i += 2)
			_html = _html.replaceAll(replaces.get(i), replaces.get(i + 1));

		final Matcher m = objectId.matcher(_html);
		if(m != null)
			_html = m.replaceAll(String.valueOf(_npcObjId));
		_html = playername.matcher(_html).replaceAll(player.getName());

		player.cleanBypasses(false);
		_html = player.encodeBypasses(_html, false);

		if(_html.length() > Config.MAX_LENGTH_HTML)
		{
			_log.info("To long html (" + _html.length() + "), client crash prevented. File: '" + _file + "', Character: " + player.getName());
			_html = "<html><body><center>Слишком длинный список.</center></body></html>";
		}

		if(_questId > 0)
		{
			writeC(EXTENDED_PACKET);
			writeH(0x8D);
			writeD(_npcObjId);
			writeS(_html);
			writeD(_questId);
		}
		else
		{
			writeC(0x19);
			writeD(_npcObjId);
			writeS(_html);
			writeD(item_id);
		}
	}

	@Override
	public String getType()
	{
		return _S__19_NPCHTMLMESSAGE;
	}
}
