package l2n.game.network.serverpackets;

/**
 * sample
 * <p>
 * 4c 01 00 00 00
 * <p>
 * format cd
 */
public class JoinParty extends L2GameServerPacket
{
	private static final String _S__3A_JOINPARTY = "[S] 3A JoinParty";
	public static final JoinParty STATIC_PACKET_ACCEPTED = new JoinParty(1);
	public static final JoinParty STATIC_PACKET_DECLINED = new JoinParty(0);

	private final int _response;

	public JoinParty(final int response)
	{
		_response = response;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x3a);
		writeD(_response);
	}

	@Override
	public String getType()
	{
		return _S__3A_JOINPARTY;
	}
}
