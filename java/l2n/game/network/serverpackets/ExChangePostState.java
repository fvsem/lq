package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 23:53:23
 */
public class ExChangePostState extends L2GameServerPacket
{
	private static final String _S__FE_B3_EXCHANGEPOSTSTATE = "[S] B3 ExChangePostState";

	private final boolean _receivedBoard;
	private final int[] _changedMsgIds;
	private final int _changeId;

	public ExChangePostState(final boolean receivedBoard, final int[] changedMsgIds, final int changeId)
	{
		_receivedBoard = receivedBoard;
		_changedMsgIds = changedMsgIds;
		_changeId = changeId;
	}

	public ExChangePostState(final boolean receivedBoard, final int changedMsgId, final int changeId)
	{
		_receivedBoard = receivedBoard;
		_changedMsgIds = new int[] { changedMsgId };
		_changeId = changeId;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xb3);
		writeD(_receivedBoard ? 1 : 0);
		writeD(_changedMsgIds.length);
		for(final int postId : _changedMsgIds)
		{
			writeD(postId); // postId
			writeD(_changeId); // state
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_B3_EXCHANGEPOSTSTATE;
	}
}
