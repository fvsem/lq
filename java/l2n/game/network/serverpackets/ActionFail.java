package l2n.game.network.serverpackets;

public class ActionFail extends L2GameServerPacket
{
	private static final String _S__1f_ACTIONFAILED = "[S] 1f ActionFail";
	public static final ActionFail STATIC_PACKET = new ActionFail();

	public ActionFail()
	{}

	@Override
	protected final void writeImpl()
	{
		writeC(0x1f);
	}

	@Override
	public String getType()
	{
		return _S__1f_ACTIONFAILED;
	}
}
