package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

/**
 * Format (ch)dddcc
 */
public class ExFishingStartCombat extends L2GameServerPacket
{
	private static final String _S__FE_27_EXFISHINGSTARTCOMBAT = "[S] FE:27 ExFishingStartCombat";
	int _time, _hp;
	int _lureType, _deceptiveMode, _mode;
	private final int char_obj_id;

	public ExFishingStartCombat(final L2Character character, final int time, final int hp, final int mode, final int lureType, final int deceptiveMode)
	{
		char_obj_id = character.getObjectId();
		_time = time;
		_hp = hp;
		_mode = mode;
		_lureType = lureType;
		_deceptiveMode = deceptiveMode;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x27);

		writeD(char_obj_id);
		writeD(_time);
		writeD(_hp);
		writeC(_mode); // mode: 0 = resting, 1 = fighting
		writeC(_lureType); // 0 = newbie lure, 1 = normal lure, 2 = night lure
		writeC(_deceptiveMode); // Fish Deceptive Mode: 0 = no, 1 = yes
	}

	@Override
	public String getType()
	{
		return _S__FE_27_EXFISHINGSTARTCOMBAT;
	}
}
