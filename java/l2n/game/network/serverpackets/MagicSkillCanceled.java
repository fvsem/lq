package l2n.game.network.serverpackets;

public class MagicSkillCanceled extends L2GameServerPacket
{
	private static final String _S__49_MAGICSKILLCANCELD = "[S] 49 MagicSkillCanceled";

	private final int _objectId;

	public MagicSkillCanceled(final int objectId)
	{
		_objectId = objectId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x49);
		writeD(_objectId);
	}

	@Override
	public String getType()
	{
		return _S__49_MAGICSKILLCANCELD;
	}
}
