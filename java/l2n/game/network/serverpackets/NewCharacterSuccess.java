package l2n.game.network.serverpackets;

import l2n.game.templates.L2PlayerTemplate;

import java.util.ArrayList;

public class NewCharacterSuccess extends L2GameServerPacket
{
	// dddddddddddddddddddd
	private static final String _S__0d_CHARTEMPLATES = "[S] 0d CharTemplates";
	private final ArrayList<L2PlayerTemplate> _chars = new ArrayList<L2PlayerTemplate>();

	public void addChar(final L2PlayerTemplate template)
	{
		_chars.add(template);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x0d);
		writeD(_chars.size());

		for(final L2PlayerTemplate temp : _chars)
		{
			writeD(temp.race.ordinal());
			writeD(temp.classId.getId());
			writeD(0x46);
			writeD(temp.baseSTR);
			writeD(0x0a);
			writeD(0x46);
			writeD(temp.baseDEX);
			writeD(0x0a);
			writeD(0x46);
			writeD(temp.baseCON);
			writeD(0x0a);
			writeD(0x46);
			writeD(temp.baseINT);
			writeD(0x0a);
			writeD(0x46);
			writeD(temp.baseWIT);
			writeD(0x0a);
			writeD(0x46);
			writeD(temp.baseMEN);
			writeD(0x0a);
		}
	}

	@Override
	public String getType()
	{
		return _S__0d_CHARTEMPLATES;
	}
}
