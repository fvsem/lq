package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.Vector;

public class ExShowBaseAttributeCancelWindow extends L2GameServerPacket
{
	private static final String _S__FE_74_EXCSHOWBASEATTRIBUTECANCELWINDOW = "[S] FE 74 ExShowBasAttributeCancelWindow";
	private final Vector<L2ItemInstance> _items = new Vector<L2ItemInstance>();
	private final int _price;

	public ExShowBaseAttributeCancelWindow(final L2Player activeChar, final int price)
	{
		for(final L2ItemInstance i : activeChar.getInventory().getItemsList())
		{
			if(i.getAttributeElementValue() == 0)
				continue;
			_items.add(i);
		}
		_price = price;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x74);
		writeD(_items.size());
		if(_items.size() > 0)
			for(final L2ItemInstance i : _items)
			{
				writeD(i.getObjectId());
				writeQ(_price);
			}
		else
			writeD(0);
	}

	@Override
	public String getType()
	{
		return _S__FE_74_EXCSHOWBASEATTRIBUTECANCELWINDOW;
	}
}
