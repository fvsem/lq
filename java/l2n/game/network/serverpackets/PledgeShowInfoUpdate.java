package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan;

public class PledgeShowInfoUpdate extends L2GameServerPacket
{
	private static final String _S__8e_PLEDGESHOWINFOUPDATE = "[S] 8e PledgeShowInfoUpdate";
	private final int clan_id, clan_level, clan_rank, clan_rep, crest_id, ally_id, ally_crest_id, isAtWar;
	private final int HasCastle, HasHideout, HasFortress;

	public PledgeShowInfoUpdate(final L2Clan clan)
	{
		clan_id = clan.getClanId();
		crest_id = clan.getCrestId();
		clan_level = clan.getLevel();
		HasCastle = clan.getHasCastle();
		HasHideout = clan.getHasHideout();
		HasFortress = clan.getHasFortress();
		clan_rank = clan.getRank();
		clan_rep = clan.getReputationScore();
		ally_id = clan.getAllyId();
		ally_crest_id = clan.getAlliance() != null ? clan.getAlliance().getAllyCrestId() : 0;
		isAtWar = clan.isAtWar();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x8e);
		// sending empty data so client will ask all the info in response ;)
		writeD(clan_id);
		writeD(crest_id);
		writeD(clan_level);
		writeD(HasCastle);
		writeD(HasHideout);
		writeD(HasFortress);
		writeD(clan_rank);// displayed in the "tree" view (with the clan skills)
		writeD(clan_rep);
		writeD(0);
		writeD(0);

		writeD(ally_id); // c5
		writeS(""); // c5
		writeD(ally_crest_id); // c5
		writeD(isAtWar); // c5
	}

	@Override
	public String getType()
	{
		return _S__8e_PLEDGESHOWINFOUPDATE;
	}
}
