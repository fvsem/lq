package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;

import java.util.Collection;
import java.util.Collections;

public class RelationChanged extends L2GameServerPacket
{
	private static final String _S__CE_RELATIONCHANGED = "[S] CE RelationChanged";
	private final boolean _isAutoAttackable;
	private final int _relation, karma, pvp_flag;
	private int char_obj_id = 0;

	/**
	 * @param targetChar
	 *            игрок, отношение к которому изменилось
	 * @param activeChar
	 *            игрок, которому будет отослан пакет с результатом
	 */
	public static Collection<L2GameServerPacket> update(final L2Player targetChar, final L2Player activeChar)
	{
		if(targetChar == null || activeChar == null || targetChar.isInOfflineMode())
			return Collections.emptyList();

		final L2Summon pet = targetChar.getPet();
		final Collection<L2GameServerPacket> result = new GArray<L2GameServerPacket>(pet != null ? 2 : 1);

		final int relation = targetChar.getRelation(activeChar);
		result.add(new RelationChanged(targetChar, targetChar.isAutoAttackable(activeChar), relation));

		if(pet != null)
			result.add(new RelationChanged(pet, pet.isAutoAttackable(activeChar), relation));
		return result;
	}

	public RelationChanged(final L2Playable cha, final boolean isAutoAttackable, final int relation)
	{
		_isAutoAttackable = isAutoAttackable;
		_relation = relation;
		char_obj_id = cha.getObjectId();
		karma = cha.getKarma();
		pvp_flag = cha.getPvpFlag();
	}

	@Override
	protected final void writeImpl()
	{
		if(char_obj_id == 0)
			return;

		writeC(0xCE);
		writeD(0x01);
		writeD(char_obj_id);
		writeD(_relation);
		writeD(_isAutoAttackable ? 0x01 : 0x00);
		writeD(karma);
		writeD(pvp_flag);
	}

	@Override
	public String getType()
	{
		return _S__CE_RELATIONCHANGED;
	}
}
