package l2n.game.network.serverpackets;

public class TradeDone extends L2GameServerPacket
{
	private static final String _S__32_SENDTRADEDONE = "[S] 1c SendTradeDone";
	private final int _num;

	public TradeDone(final int num)
	{
		_num = num;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x1c);
		writeD(_num);
	}

	@Override
	public String getType()
	{
		return _S__32_SENDTRADEDONE;
	}

}
