package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.templates.L2Item;

public class WareHouseDepositList extends L2GameServerPacket
{
	private static final String _S__41_WAREHOUSEDEPOSITLIST = "[S] 41 WareHouseDepositList";
	private final int _whtype;
	private final long char_adena;
	private final GArray<L2ItemInstance> _itemslist = new GArray<L2ItemInstance>();

	public WareHouseDepositList(final L2Player cha, final WarehouseType whtype)
	{
		_whtype = whtype.getPacketValue();
		char_adena = cha.getAdena();
		for(final L2ItemInstance item : cha.getInventory().getItems())
			if(!item.isEquipped() && item.getItem().getType2() != L2Item.TYPE2_QUEST && (_whtype == 1 || item.canBeStored(cha, false)))
				_itemslist.add(item);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x41);
		writeH(_whtype);
		writeQ(char_adena);
		writeH(_itemslist.size());
		for(final L2ItemInstance temp : _itemslist)
		{
			final L2Item item = temp.getItem();
			writeH(item.getType1());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeH(item.getType2ForPackets());
			writeH(temp.getCustomType1());
			writeD(temp.getBodyPart());
			writeH(temp.getEnchantLevel());
			writeH(0x00); // ? 200
			writeH(temp.getCustomType2());
			writeD(temp.getObjectId()); // return value for define item (object_id)
			if(temp.isAugmented())
			{
				writeD(temp.getAugmentationId() & 0x0000FFFF);
				writeD(temp.getAugmentationId() >> 16);
			}
			else
				writeQ(0x00);

			// t1
			writeItemElements(temp);

			// T2
			writeD(temp.getLifeTimeRemaining());
			writeD(temp.isTemporalItem() ? temp.getLifeTimeRemaining() : -1);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
		_itemslist.clear();
	}

	@Override
	public String getType()
	{
		return _S__41_WAREHOUSEDEPOSITLIST;
	}
}
