package l2n.game.network.serverpackets;

/**
 * sample
 * <p>
 * 7d c1 b2 e0 4a 00 00 00 00
 * <p>
 * format cdd
 * 
 * @version $Revision: 1.1.2.1.2.3 $ $Date: 2005/03/27 15:29:57 $
 */
public class AskJoinAlliance extends L2GameServerPacket
{
	private static final String _S__bb_ASKJOINALLY = "[S] bb AskJoinAlly";

	private final String _requestorName;
	private final String _requestorAllyName;
	private final int _requestorId;

	public AskJoinAlliance(final int requestorId, final String requestorName, final String requestorAllyName)
	{
		_requestorName = requestorName;
		_requestorAllyName = requestorAllyName;
		_requestorId = requestorId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xbb);
		writeD(_requestorId);
		writeS(_requestorName);
		writeS("");
		writeS(_requestorAllyName);
	}

	@Override
	public String getType()
	{
		return _S__bb_ASKJOINALLY;
	}
}
