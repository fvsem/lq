package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;

public class ExCursedWeaponList extends L2GameServerPacket
{
	private static final String _S__FE_46_EXCURSEDWEAPONLIST = "[S] FE:46 ExCursedWeaponList";
	private int[] cursedWeapon_ids;

	public ExCursedWeaponList(final GArray<Integer> cursedWeaponIds)
	{
		cursedWeapon_ids = new int[cursedWeaponIds.size()];
		for(int i = 0; i < cursedWeaponIds.size(); i++)
			cursedWeapon_ids[i] = cursedWeaponIds.get(i);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x46);

		writeD(cursedWeapon_ids.length);
		for(final int element : cursedWeapon_ids)
			writeD(element);
		cursedWeapon_ids = null;
	}

	@Override
	public String getType()
	{
		return _S__FE_46_EXCURSEDWEAPONLIST;
	}
}
