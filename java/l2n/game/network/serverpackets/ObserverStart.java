package l2n.game.network.serverpackets;

import l2n.util.Location;

/**
 * @author L2System Project
 * @date 16.08.2010
 * @time 1:15:17
 */
public class ObserverStart extends L2GameServerPacket
{
	// ddSS
	private static final String _S__DF_OBSERVMODE = "[S] DF ObservationMode";
	private final Location _loc;

	/**
	 * @param _characters
	 */
	public ObserverStart(final Location loc)
	{
		_loc = loc;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xeb);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeC(0x00);
		writeC(0xc0);
		writeC(0x00);
	}

	@Override
	public String getType()
	{
		return _S__DF_OBSERVMODE;
	}
}
