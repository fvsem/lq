/**
 * 
 */
package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 *         06.11.2009 16:52:12
 */
public class ExValidateLocationInAirShip extends L2GameServerPacket
{
	private int player, vehicle, x, y, z, h;

	public ExValidateLocationInAirShip(final L2Player pl)
	{
		if(pl.getVehicle() == null)
			return;
		player = pl.getObjectId();
		vehicle = pl.getVehicle().getObjectId();
		x = pl.getX();
		y = pl.getY();
		z = pl.getZ();
		h = pl.getHeading();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xfe);
		writeH(0x6F);
		writeD(player);
		writeD(vehicle);
		writeD(x);
		writeD(y);
		writeD(z);
		writeD(h);
	}

	@Override
	public String getType()
	{
		return "[S] FE:6F ExValidateLocationInAirShip";
	}
}
