package l2n.game.network.serverpackets;

import l2n.commons.network.SendablePacket;
import l2n.game.model.TradeItem;
import l2n.game.model.base.MultiSellIngredient;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.L2GameClient;
import l2n.game.network.clientpackets.L2GameClientPacket;
import l2n.game.network.model.ItemInfo;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class L2GameServerPacket extends SendablePacket<L2GameClient, L2GameClientPacket, L2GameServerPacket>
{
	protected static final Logger _log = Logger.getLogger(L2GameServerPacket.class.getName());

	@Override
	protected void write()
	{
		try
		{
			writeImpl();
		}
		catch(final Throwable t)
		{
			_log.log(Level.WARNING, "Failed writing: " + getType(), t);
		}
	}

	protected abstract void writeImpl();

	/**
	 * @return A String with this packet name for debugging purposes
	 */
	public String getType()
	{
		return "[S] " + super.getClass().getSimpleName();
	}

	protected final static int EXTENDED_PACKET = 0xFE;

	@Override
	protected int getHeaderSize()
	{
		return 2;
	}

	@Override
	protected void writeHeader(final int dataSize)
	{
		writeH(dataSize + getHeaderSize());
	}

	protected void writeEnchantEffect(final L2ItemInstance item)
	{
		writeH(0);
		writeH(0);
		writeH(0);
	}

	protected void writeEnchantEffect(final TradeItem item)
	{
		writeH(0);
		writeH(0);
		writeH(0);
	}

	protected void writeEnchantEffect(final ItemInfo item)
	{
		writeH(0);
		writeH(0);
		writeH(0);
	}

	protected void writeEnchantEffect()
	{
		writeH(0);
		writeH(0);
		writeH(0);
	}

	protected void writeItemElements(final L2ItemInstance item)
	{
		writeH(item.getAttackElement()[0]);
		writeH(item.getAttackElement()[1]);
		writeH(item.getDefenceFire());
		writeH(item.getDefenceWater());
		writeH(item.getDefenceWind());
		writeH(item.getDefenceEarth());
		writeH(item.getDefenceHoly());
		writeH(item.getDefenceUnholy());
	}

	protected void writeItemElements(final ItemInfo item)
	{
		writeH(item.getAttackElement()[0]);
		writeH(item.getAttackElement()[1]);
		writeH(item.getDefenceFire());
		writeH(item.getDefenceWater());
		writeH(item.getDefenceWind());
		writeH(item.getDefenceEarth());
		writeH(item.getDefenceHoly());
		writeH(item.getDefenceUnholy());
	}

	protected void writeItemElements(final TradeItem item)
	{
		writeH(item.getAttackElement()[0]);
		writeH(item.getAttackElement()[1]);
		writeH(item.getDefenceFire());
		writeH(item.getDefenceWater());
		writeH(item.getDefenceWind());
		writeH(item.getDefenceEarth());
		writeH(item.getDefenceHoly());
		writeH(item.getDefenceUnholy());
	}

	protected void writeItemElements(final MultiSellIngredient item)
	{
		if(item.getItemId() <= 0)
		{
			writeItemElements();
			return;
		}
		final L2Item i = ItemTable.getInstance().getTemplate(item.getItemId());
		if(i.isWeapon() && i.isRare())
		{
			writeH(item.getElement());
			writeH(item.getElementValue());
			writeH(0);
			writeH(0);
			writeH(0);
			writeH(0);
			writeH(0);
			writeH(0);
		}
		else if(i.isArmor())
		{
			writeH(-2);
			writeH(0);
			writeH(item.getElement() == 0 ? item.getElementValue() : 0);
			writeH(item.getElement() == 1 ? item.getElementValue() : 0);
			writeH(item.getElement() == 2 ? item.getElementValue() : 0);
			writeH(item.getElement() == 3 ? item.getElementValue() : 0);
			writeH(item.getElement() == 4 ? item.getElementValue() : 0);
			writeH(item.getElement() == 5 ? item.getElementValue() : 0);
		}
		else
			writeItemElements();
	}

	protected void writeItemElements()
	{
		writeH(-2);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
	}
}
