package l2n.game.network.serverpackets;

import l2n.game.model.L2ShortCut;

/**
 * sample
 * 56
 * 01000000 04000000 dd9fb640 01000000
 * 56
 * 02000000 07000000 38000000 03000000 01000000
 * 56
 * 03000000 00000000 02000000 01000000
 * format dd d/dd/d d
 */
public class ShortCutRegister extends L2GameServerPacket
{
	private static final String _S__44_SHORTCUTREGISTER = "[S] 44 ShortCutRegister";
	private final L2ShortCut sc;

	/**
	 * Register new skill shortcut
	 */
	public ShortCutRegister(final L2ShortCut _sc)
	{
		sc = _sc;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x44);

		writeD(sc.type);
		writeD(sc.slot + sc.page * 12); // C4 Client
		switch (sc.getType())
		{
			case L2ShortCut.TYPE_ITEM: // 1
				writeD(sc.getId());
				writeD(sc.getCharacterType());
				writeD(-1); // here should be item type
				writeD(0x00); // unknown
				writeD(0x00); // unknown
				writeD(0x00); // item augment id
				break;
			case L2ShortCut.TYPE_SKILL: // 2
				writeD(sc.getId());
				writeD(sc.getLevel());
				writeC(0x00); // C5
				writeD(sc.getCharacterType());
				break;
			default:
				writeD(sc.getId());
				writeD(sc.getCharacterType());
		}
	}

	@Override
	public String getType()
	{
		return _S__44_SHORTCUTREGISTER;
	}
}
