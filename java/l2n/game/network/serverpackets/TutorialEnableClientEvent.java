package l2n.game.network.serverpackets;

public class TutorialEnableClientEvent extends L2GameServerPacket
{
	private int _event = 0;

	public TutorialEnableClientEvent(final int event)
	{
		_event = event;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xa8);
		writeD(_event);
	}

	@Override
	public String getType()
	{
		return "[S] a8 TutorialEnableClientEvent";
	}
}
