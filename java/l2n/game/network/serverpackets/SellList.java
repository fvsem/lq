package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

@Deprecated
public class SellList extends L2GameServerPacket
{
	private static final String _S__06_SELLLIST = "[S] 06 SellList";
	private final long _money;
	private final GArray<L2ItemInstance> _selllist = new GArray<L2ItemInstance>();

	/**
	 * Список вещей для продажи в обычный магазин
	 * 
	 * @param player
	 */
	public SellList(final L2Player player)
	{
		_money = player.getAdena();

		for(final L2ItemInstance item : player.getInventory().getItems())
			if(!item.isEquipped() && item.getItem().isSellable() && item.canBeTraded(player))
				_selllist.add(item);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x06);
		writeQ(_money);
		writeD(0x00);
		writeH(_selllist.size());

		for(final L2ItemInstance item : _selllist)
		{
			writeH(item.getItem().getType1());
			writeD(item.getObjectId());
			writeD(item.getItemId());

			writeQ(item.getCount());
			writeH(item.getItem().getType2ForPackets());
			writeH(item.getCustomType1());
			writeD(item.getBodyPart());
			writeH(item.getEnchantLevel());
			writeH(item.getCustomType2());
			writeH(0x00); // unknown

			writeQ(item.getItem().getReferencePrice() / 2);
			writeItemElements(item);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__06_SELLLIST;
	}
}
