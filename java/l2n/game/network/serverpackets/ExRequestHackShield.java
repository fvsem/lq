package l2n.game.network.serverpackets;

public class ExRequestHackShield extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x49);
	}

	@Override
	public String getType()
	{
		return "[S] FE:49 ExRequestHackShield";
	}
}
