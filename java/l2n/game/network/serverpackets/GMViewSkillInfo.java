package l2n.game.network.serverpackets;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.SkillTreeTable;

import java.util.Collection;

public class GMViewSkillInfo extends L2GameServerPacket
{
	private static final String _S__97_GMViewSkillInfo = "[S] 97 GMViewSkillInfo";
	private final String char_name;
	private final Collection<L2Skill> _skills;

	// private boolean _isClanSkillsDisabled;

	public GMViewSkillInfo(final L2Player cha)
	{
		char_name = cha.getName();
		_skills = cha.getAllSkills();
		// _isClanSkillsDisabled = cha.getClan() != null && cha.getClan().getReputationScore() < 0;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x97);
		writeS(char_name);
		writeD(_skills.size());
		for(final L2Skill skill : _skills)
		{
			if(skill.getId() > 9000)
				continue; // fake skills to change base stats

			writeD(skill.isLikePassive() ? 0x01 : 0x00);
			writeD(skill.getDisplayLevel());
			writeD(skill.getId());
			// writeC(_isClanSkillsDisabled && skill.isClanSkill() ? 0x01 : 0x00); TODO
			writeC(0x00);
			writeC(SkillTreeTable.isEnchantable(skill) ? 1 : 0);
		}
	}

	@Override
	public String getType()
	{
		return _S__97_GMViewSkillInfo;
	}
}
