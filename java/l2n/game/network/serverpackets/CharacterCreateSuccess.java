package l2n.game.network.serverpackets;

public class CharacterCreateSuccess extends L2GameServerPacket
{
	private static final String _S__0f_CHARCREATEOK = "[S] 0f CharCreateOk";

	@Override
	protected final void writeImpl()
	{
		writeC(0x0f);
		writeD(0x01);
	}

	@Override
	public String getType()
	{
		return _S__0f_CHARCREATEOK;
	}
}
