package l2n.game.network.serverpackets;

import l2n.game.model.entity.SevenSigns;

/**
 * Seven Signs Info
 * packet id 0x73
 * format: cc
 * Пример пакета с оффа (828 протокол):
 * 73 01 01
 * Возможные варианты использования данного пакета:
 * 0 0 - Обычное небо???
 * 1 1 - Dusk Sky
 * 2 2 - Dawn Sky???
 * 3 3 - Небо постепенно краснеет (за 10 секунд)
 * Возможно и другие вариации, эффект не совсем понятен.
 * 1 0
 * 0 1
 */
public class SSQInfo extends L2GameServerPacket
{
	private static final String _S__76_SSQInfo = "[S] fb SSQInfo";

	public static final SSQInfo STATIC_PACKET_DAWN = new SSQInfo(258);
	public static final SSQInfo STATIC_PACKET_DUSK = new SSQInfo(257);
	public static final SSQInfo STATIC_PACKET_NULL = new SSQInfo(256);

	public static final SSQInfo currentSSQInfo()
	{
		final int compWinner = SevenSigns.getInstance().getCabalHighestScore();
		if(SevenSigns.getInstance().isSealValidationPeriod())
			switch (compWinner)
			{
				case SevenSigns.CABAL_DAWN:
					return STATIC_PACKET_DAWN;

				case SevenSigns.CABAL_DUSK:
					return STATIC_PACKET_DUSK;

				default:
					return STATIC_PACKET_NULL;
			}
		else
			return STATIC_PACKET_NULL;
	}

	private final int _skyId;

	private SSQInfo(final int skyId)
	{
		_skyId = skyId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x73);
		writeH(_skyId);
	}

	@Override
	public String getType()
	{
		return _S__76_SSQInfo;
	}
}
