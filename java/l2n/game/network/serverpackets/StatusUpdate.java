package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;

import java.util.logging.Level;

/**
 * Даные параметры актуальны для С6(Interlude), 04/10/2007, протокол 746
 */
public class StatusUpdate extends L2GameServerPacket
{
	private static final String _S__18_STATUSUPDATE = "[S] 18 StatusUpdate";

	/**
	 * Даный параметр отсылается оффом в паре с MAX_HP
	 * Сначала CUR_HP, потом MAX_HP
	 */
	public final static int CUR_HP = 0x09;
	public final static int MAX_HP = 0x0a;

	/**
	 * Даный параметр отсылается оффом в паре с MAX_MP
	 * Сначала CUR_MP, потом MAX_MP
	 */
	public final static int CUR_MP = 0x0b;
	public final static int MAX_MP = 0x0c;

	/** Меняется отображение только в инвентаре, для статуса требуется UserInfo */
	public final static int CUR_LOAD = 0x0e;

	/** Меняется отображение только в инвентаре, для статуса требуется UserInfo */
	public final static int MAX_LOAD = 0x0f;

	public final static int PVP_FLAG = 0x1a;
	public final static int KARMA = 0x1b;

	/**
	 * Даный параметр отсылается оффом в паре с MAX_CP
	 * Сначала CUR_CP, потом MAX_CP
	 */
	public final static int CUR_CP = 0x21;
	public final static int MAX_CP = 0x22;

	private final int _objectId;
	private final int[][] _attributes;
	private int _currentAttribute;
	private final GArray<int[]> _tempAttributes;

	/**
	 * @param objectId
	 *            The ObjectId this StatusUpdate is referenced to
	 * @param changedAttributes
	 *            The count of attributes u want to change
	 */
	public StatusUpdate(final int objectId, final int changedAttributes)
	{
		_objectId = objectId;
		_attributes = new int[changedAttributes][2];
		_tempAttributes = null;
	}

	/**
	 * U should only use this if you are not totally
	 * sure how much attributes will change
	 * 
	 * @param objectId
	 *            The ObjectId this StatusUpdate is referenced to
	 */
	public StatusUpdate(final int objectId)
	{
		_objectId = objectId;
		_attributes = null;
		_tempAttributes = new GArray<int[]>(3);
	}

	public void addAttribute(final int id, final int value)
	{
		if(_attributes == null)
			_tempAttributes.addLastUnsafe(new int[] { id, value });
		else
			try
			{
				_attributes[_currentAttribute][0] = id;
				_attributes[_currentAttribute++][1] = value;
			}
			catch(final ArrayIndexOutOfBoundsException e)
			{
				_log.log(Level.WARNING, "StatusUpdate, Invalid params count", e);
			}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x18);
		writeD(_objectId);

		if(_attributes == null)
		{
			writeD(_tempAttributes.size());

			for(final int[] attribute : _tempAttributes)
			{
				writeD(attribute[0]);
				writeD(attribute[1]);
			}
		}
		else
		{
			writeD(_attributes.length);

			for(final int[] attribute : _attributes)
			{
				writeD(attribute[0]);
				writeD(attribute[1]);
			}
		}
	}

	public boolean hasAttributes()
	{
		return _attributes != null && _attributes.length > 0 || _tempAttributes != null && !_tempAttributes.isEmpty();
	}

	@Override
	public final String getType()
	{
		return _S__18_STATUSUPDATE;
	}
}
