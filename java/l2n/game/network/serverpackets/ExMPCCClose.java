package l2n.game.network.serverpackets;

/**
 * Close the CommandChannel Information window
 */
public class ExMPCCClose extends L2GameServerPacket
{

	private static final String _S__FE_13_EXMPCCCLOSE = "[S] FE:13 ExMPCCClose";

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x13);
	}

	@Override
	public String getType()
	{
		return _S__FE_13_EXMPCCCLOSE;
	}
}
