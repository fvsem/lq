package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;
import l2n.util.Location;

/**
 * format ddddd
 * sample
 * 0000: 24 69 08 10 48 02 c1 00 00 f7 56 00 00 89 ea ff
 * 0010: ff 0c b2 d8 61 01 00 00 00
 */
public class TargetUnselected extends L2GameServerPacket
{
	private static final String _S__24_TARGETUNSELECTED = "[S] 24 TargetUnselected";
	private final int _targetId;
	private final Location _loc;

	/**
	 * @param character
	 */
	public TargetUnselected(final L2Character cha)
	{
		_targetId = cha.getObjectId();
		_loc = cha.getLoc();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x24);
		writeD(_targetId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(0x00); // иногда бывает 1
	}

	@Override
	public String getType()
	{
		return _S__24_TARGETUNSELECTED;
	}
}
