package l2n.game.network.serverpackets;

import l2n.game.GameTimeController;
import l2n.game.model.actor.L2Player;
import l2n.util.Location;

/**
 * Пакет высылается сервером в ответ на клиентский CharacterSelect при выборе чара
 * 
 * @author minlexx
 */
public class CharSelected extends L2GameServerPacket
{
	// SdSddddddddddffddddddddddddddddddddddddddddddddddddddddd d
	private static final String _S__0b_CHARSELECTED = "[S] 0b CharSelected";
	private final int _sessionId, char_id, clan_id, sex, race,
			class_id;
	private final String _name, _title;
	private final Location _loc;
	private final double curHp, curMp;
	private final int _sp, level, karma, _int, _str, _con, _men,
			_dex, _wit, _pkKills;
	private final long _exp;
	private final int _obfuscationKey;

	public CharSelected(final L2Player cha, final int sessionId, final int obfuscationKey)
	{
		_sessionId = sessionId;

		_name = cha.getVisName();
		char_id = cha.getCharId(); // ??
		_title = cha.getVisTitle();
		clan_id = cha.getClanId();
		sex = cha.getSex();
		race = cha.getRace().ordinal();
		class_id = cha.getClassId().getId();
		_loc = cha.getLoc();
		curHp = cha.getCurrentHp();
		curMp = cha.getCurrentMp();
		_sp = cha.getSp();
		_exp = cha.getExp();
		level = cha.getLevel();
		karma = cha.getKarma();
		_pkKills = cha.getPkKills();
		_int = cha.getINT();
		_str = cha.getSTR();
		_con = cha.getCON();
		_men = cha.getMEN();
		_dex = cha.getDEX();
		_wit = cha.getWIT();
		_obfuscationKey = obfuscationKey;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x0b);

		writeS(_name);
		writeD(char_id);
		writeS(_title);
		writeD(_sessionId);
		writeD(clan_id);
		writeD(0x00); // ??
		writeD(sex);
		writeD(race);
		writeD(class_id);
		writeD(0x01); // active ??
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);

		writeF(curHp);
		writeF(curMp);
		writeD(_sp);
		writeQ(_exp);
		writeD(level);
		writeD(karma); // ?
		writeD(_pkKills); // ?
		writeD(_int);
		writeD(_str);
		writeD(_con);
		writeD(_men);
		writeD(_dex);
		writeD(_wit);

		writeD(GameTimeController.getInstance().getGameTime() % (24 * 60)); // "reset" on 24th hour
		writeD(0x00);
		writeD(class_id); // wtf? (c) l2jsf

		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00); // 4 x 0x00

		for(int i = 0; i < 16; i++)
			// some 16 DWORDs
			writeD(0x00);

		writeD(_obfuscationKey); // obfuscation key used by client to mix its opcode tables
	}

	@Override
	public String getType()
	{
		return _S__0b_CHARSELECTED;
	}
}
