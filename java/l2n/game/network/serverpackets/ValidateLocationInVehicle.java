package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class ValidateLocationInVehicle extends L2GameServerPacket
{
	private static final String _S__80_ValidateLocationInVehicle = "[S] 80 ValidateLocationInVehicle";
	private int _charObjId;
	private Location _loc;
	private int _boatObjId;

	/**
	 * 0x73 ValidateLocationInVehicle hdd
	 */
	public ValidateLocationInVehicle(final L2Player player)
	{
		if(player.getVehicle() == null)
			return;
		_charObjId = player.getObjectId();
		_loc = player.getInVehiclePosition();
		_boatObjId = player.getVehicle().getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x80);
		writeD(_charObjId);
		writeD(_boatObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
	}

	@Override
	public String getType()
	{
		return _S__80_ValidateLocationInVehicle;
	}
}
