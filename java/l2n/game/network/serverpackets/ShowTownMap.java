package l2n.game.network.serverpackets;

public class ShowTownMap extends L2GameServerPacket
{
	/**
	 * Format: csdd
	 */
	private static final String _S__ea_SHOWTOWNMAP = "[S] ea ShowTownMap";

	String _texture;
	int _x;
	int _y;

	public ShowTownMap(final String texture, final int x, final int y)
	{
		_texture = texture;
		_x = x;
		_y = y;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xea);
		writeS(_texture);
		writeD(_x);
		writeD(_y);
	}

	@Override
	public String getType()
	{
		return _S__ea_SHOWTOWNMAP;
	}
}
