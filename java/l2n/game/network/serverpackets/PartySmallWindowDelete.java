package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class PartySmallWindowDelete extends L2GameServerPacket
{
	private static final String _S__51_PARTYSMALLWINDOWDELETE = "[S] 51 PartySmallWindowDelete";
	private final int member_obj_id;
	private final String member_name;

	public PartySmallWindowDelete(final L2Player member)
	{
		member_obj_id = member.getObjectId();
		member_name = member.getName();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x51);
		writeD(member_obj_id);
		writeS(member_name);
	}

	@Override
	public String getType()
	{
		return _S__51_PARTYSMALLWINDOWDELETE;
	}
}
