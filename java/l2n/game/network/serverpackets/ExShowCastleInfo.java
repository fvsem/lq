package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.entity.residence.Castle;
import l2n.game.tables.ClanTable;

public class ExShowCastleInfo extends L2GameServerPacket
{
	private static final String _S__FE_14_EXSHOWCASTLEINFO = "[S] FE:14 ExShowCastleInfo";
	private final GArray<CastleInfo> infos = new GArray<CastleInfo>();

	public ExShowCastleInfo()
	{
		String owner_name;
		int _id, tax, next_siege;

		for(final Castle castle : CastleManager.getInstance().getCastles().values())
		{
			owner_name = castle.getOwnerId() == 0 ? "" : ClanTable.getInstance().getClan(castle.getOwnerId()).getName();
			_id = castle.getId();
			tax = castle.getTaxPercent();
			next_siege = (int) (castle.getSiege().getSiegeDate().getTimeInMillis() / 1000);
			infos.add(new CastleInfo(owner_name, _id, tax, next_siege));
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x14);
		writeD(infos.size());
		for(final CastleInfo _info : infos)
		{
			writeD(_info._id);
			writeS(_info.owner_name);
			writeD(_info.tax);
			writeD(_info.next_siege);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_14_EXSHOWCASTLEINFO;
	}

	static class CastleInfo
	{
		public String owner_name;
		public int _id, tax, next_siege;

		public CastleInfo(final String _owner_name, final int __id, final int _tax, final int _next_siege)
		{
			owner_name = _owner_name;
			_id = __id;
			tax = _tax;
			next_siege = _next_siege;
		}
	}
}
