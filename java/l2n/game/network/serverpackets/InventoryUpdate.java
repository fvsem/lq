package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.model.ItemInfo;

/**
 * sample
 * 21 // packet type
 * 01 00 // item count
 * 03 00 // update type 01-added?? 02-modified 03-removed
 * 04 00 // itemType1 0-weapon/ring/earring/necklace 1-armor/shield 4-item/questitem/adena
 * c6 37 50 40 // objectId
 * cd 09 00 00 // itemId
 * 05 00 00 00 // count
 * 05 00 // itemType2 0-weapon 1-shield/armor 2-ring/earring/necklace 3-questitem 4-adena 5-item
 * 00 00 // always 0 ??
 * 00 00 // equipped 1-yes
 * 00 00 00 00 // slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
 * 00 00 // enchant level
 * 00 00 // always 0 ??
 * 00 00 00 00 // augmentation id
 * ff ff ff ff // shadow weapon time remaining
 * format h (hh dddhhhh hh) revision 377
 * format h (hh dddhhhd hh) revision 415
 * format h (hh dddhhhd hh dd) revision 740
 */
public class InventoryUpdate extends L2GameServerPacket
{
	private static final String _S__37_INVENTORYUPDATE = "[S] 37 InventoryUpdate";

	private final GArray<ItemInfo> _items;
	private final ItemInfo _itemInfo;

	public InventoryUpdate()
	{
		_items = new GArray<ItemInfo>();
		_itemInfo = null;
	}

	public InventoryUpdate(final L2ItemInstance item, final byte lastChange)
	{
		item.setLastChange(lastChange);
		_itemInfo = new ItemInfo(item);
		_items = GArray.emptyCollection();
	}

	private InventoryUpdate addNewItem(final L2ItemInstance item)
	{
		item.setLastChange(L2ItemInstance.ADDED);
		_items.add(new ItemInfo(item));
		return this;
	}

	public InventoryUpdate addModifiedItem(final L2ItemInstance item)
	{
		item.setLastChange(L2ItemInstance.MODIFIED);
		_items.add(new ItemInfo(item));
		return this;
	}

	private InventoryUpdate addRemovedItem(final L2ItemInstance item)
	{
		item.setLastChange(L2ItemInstance.REMOVED);
		_items.add(new ItemInfo(item));
		return this;
	}

	public InventoryUpdate addItem(final L2ItemInstance item)
	{
		if(item == null)
			return null;

		switch (item.getLastChange())
		{
			case L2ItemInstance.ADDED:
			{
				addNewItem(item);
				break;
			}
			case L2ItemInstance.MODIFIED:
			{
				addModifiedItem(item);
				break;
			}
			case L2ItemInstance.REMOVED:
			{
				addRemovedItem(item);
				break;
			}
		}
		return this;
	}

	@Override
	protected final void writeImpl()
	{
		if(_itemInfo != null)
		{
			writeC(0x21);
			writeH(1);
			writeH(_itemInfo.getLastChange());
			writeH(_itemInfo.getType1()); // item type1
			writeD(_itemInfo.getObjectId());
			writeD(_itemInfo.getItemId());
			writeD(_itemInfo.getEquipSlot()); // order
			writeQ(_itemInfo.getCount());
			writeH(_itemInfo.getType2()); // item type2
			writeH(_itemInfo.getCustomType1());// Filler (always 0)
			writeH(_itemInfo.isEquipped() ? 1 : 0); // Equipped : 00-No, 01-yes
			writeD(_itemInfo.getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
			writeH(_itemInfo.getEnchantLevel()); // Enchant level (pet level shown in control item)
			writeH(_itemInfo.getCustomType2()); // Pet name exists or not shown in control item
			writeD(_itemInfo.getAugmentationId());
			writeD(_itemInfo.getShadowLifeTime()); // interlude FF FF FF FF
			//
			writeItemElements(_itemInfo);
			// T2
			writeD(_itemInfo.getTemporalLifeTime()); // limited time item life remaining

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
		else if(!_items.isEmpty())
		{
			writeC(0x21);
			writeH(_items.size());
			for(final ItemInfo temp : _items)
			{
				writeH(temp.getLastChange());
				writeH(temp.getType1()); // item type1
				writeD(temp.getObjectId());
				writeD(temp.getItemId());
				writeD(temp.getEquipSlot()); // order
				writeQ(temp.getCount());
				writeH(temp.getType2()); // item type2
				writeH(temp.getCustomType1());// Filler (always 0)
				writeH(temp.isEquipped() ? 1 : 0); // Equipped : 00-No, 01-yes
				writeD(temp.getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
				writeH(temp.getEnchantLevel()); // Enchant level (pet level shown in control item)
				writeH(temp.getCustomType2()); // Pet name exists or not shown in control item
				writeD(temp.getAugmentationId());
				writeD(temp.getShadowLifeTime()); // interlude FF FF FF FF
				//
				writeItemElements(temp);
				// T2
				writeD(temp.getTemporalLifeTime()); // limited time item life remaining

				writeH(0x00); // Enchant effect 1
				writeH(0x00); // Enchant effect 2
				writeH(0x00); // Enchant effect 3
			}
		}
	}

	@Override
	public String getType()
	{
		return _S__37_INVENTORYUPDATE;
	}
}
