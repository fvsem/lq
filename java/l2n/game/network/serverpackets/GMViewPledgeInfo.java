package l2n.game.network.serverpackets;

import javolution.util.FastList;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;

public class GMViewPledgeInfo extends L2GameServerPacket
{
	private static final String _S__96_GMVIEWPLEDGEINFO = "[S] 96 GMViewPledgeInfo";
	private String char_name;
	private final String clan_name;
	private final String leader_name;
	private String ally_name;
	private int clan_id, clan_crest_id, clan_level;
	private final int rank;
	private int rep;
	private final int ally_id;
	private int ally_crest_id;
	private final int hasCastle, hasHideout, hasFortress,
			atWar;
	private final FastList<PledgeMemberInfo> infos = new FastList<PledgeMemberInfo>();

	public GMViewPledgeInfo(final L2Clan clan, final L2Player activeChar)
	{
		for(final L2ClanMember member : clan.getMembers())
		{
			if(member == null)
				continue;
			char_name = member.getName();
			clan_level = member.getLevel();
			clan_id = member.getClassId();
			clan_crest_id = member.isOnline() ? member.getObjectId() : 0;
			rep = member.getSponsor() != 0 ? 1 : 0;
			infos.add(new PledgeMemberInfo(char_name, clan_level, clan_id, clan_crest_id, member.getSex(), 1, rep));
		}

		char_name = activeChar.getName();
		clan_id = clan.getClanId();
		clan_name = clan.getName();
		leader_name = clan.getLeaderName();
		clan_crest_id = clan.getCrestId();
		clan_level = clan.getLevel();
		hasCastle = clan.getHasCastle();
		hasHideout = clan.getHasHideout();
		hasFortress = clan.getHasFortress();
		rank = clan.getRank();
		rep = clan.getReputationScore();
		ally_id = clan.getAllyId();
		if(clan.getAlliance() != null)
		{
			ally_name = clan.getAlliance().getAllyName();
			ally_crest_id = clan.getAlliance().getAllyCrestId();
		}
		else
		{
			ally_name = "";
			ally_crest_id = 0;
		}
		atWar = clan.isAtWar();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x96);
		writeS(char_name);
		writeD(clan_id);
		writeD(0x00);
		writeS(clan_name);
		writeS(leader_name);
		writeD(clan_crest_id);
		writeD(clan_level);
		writeD(hasCastle);
		writeD(hasHideout);
		writeD(hasFortress);
		writeD(rank);
		writeD(rep);
		writeD(0);
		writeD(0);
		writeD(ally_id);
		writeS(ally_name);
		writeD(ally_crest_id);
		writeD(atWar);
		writeD(0x00);

		writeD(infos.size());
		for(final PledgeMemberInfo _info : infos)
		{
			writeS(_info._name);
			writeD(_info.level);
			writeD(_info.class_id);
			writeD(_info.sex);
			writeD(_info.race);
			writeD(_info.online);
			writeD(_info.sponsor);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__96_GMVIEWPLEDGEINFO;
	}

	static class PledgeMemberInfo
	{
		public String _name;
		public int level, class_id, online, sex, race, sponsor;

		public PledgeMemberInfo(final String __name, final int _level, final int _class_id, final int _online, final int _sex, final int _race, final int _sponsor)
		{
			_name = __name;
			level = _level;
			class_id = _class_id;
			online = _online;
			sex = _sex;
			race = _race;
			sponsor = _sponsor;
		}
	}
}
