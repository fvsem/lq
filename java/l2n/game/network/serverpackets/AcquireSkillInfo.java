package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.L2Clan;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.base.ClassId;
import l2n.game.tables.SkillSpellbookTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;

public class AcquireSkillInfo extends L2GameServerPacket
{
	private static final String _S__91_ACQUIRESKILLINFO = "[S] 91 AquireSkillInfo";
	private final GArray<Req> _reqs;
	private final int _id;
	private final int _level;
	private int _spCost;
	private final int _mode;
	private final ClassId _classId;
	private final L2Clan _clan;

	private class Req
	{
		public int id;
		public long count;
		public int type;
		public int unk;

		public Req(final int _type, final int _id, final long _count, final int _unk)
		{
			id = _id;// 0
			type = _type;// 2
			count = _count;// count spb
			unk = _unk;// 2
		}
	}

	public AcquireSkillInfo(final int id, final int level, final ClassId classid, final L2Clan clan, final int mode)
	{
		_reqs = new GArray<Req>();
		_id = id;
		_level = level;
		_classId = classid;
		_clan = clan;
		_mode = mode;
		fillRequirements();
	}

	private void fillRequirements()
	{
		boolean isTransferSkill = false;
		switch (_mode)
		{
			case AcquireSkillList.BISHOP_TRANSFER:
			case AcquireSkillList.ELDER_TRANSFER:
			case AcquireSkillList.SILEN_ELDER_TRANSFER:
				isTransferSkill = true;
				break;
		}

		L2SkillLearn skillLearn = null;
		if(_mode != AcquireSkillList.SUBCLASS_SKILLS)
		{
			skillLearn = SkillTreeTable.getInstance().getSkillLearn(_id, _level, _classId, _clan, isTransferSkill, _mode == AcquireSkillList.CLAN_ADDITIONAL ? 0 : -1);
			if(skillLearn == null)
				return;
			_spCost = _clan != null ? skillLearn.getRepCost() : skillLearn.getSpCost();
		}

		switch (_mode)
		{
			case AcquireSkillList.BISHOP_TRANSFER:
			case AcquireSkillList.ELDER_TRANSFER:
			case AcquireSkillList.SILEN_ELDER_TRANSFER:
			{
				int id = 0;
				switch (_classId)
				{
					case cardinal:
						id = 15307; // Cardinal (97)
						break;
					case evaSaint:
						id = 15308; // Eva's Saint (105)
						break;
					case shillienSaint:
						id = 15309; // Shillen Saint (112)
				}

				_reqs.add(new Req(99, id, 1, 50));
				_spCost = 0;
				break;
			}
			case AcquireSkillList.CLAN_ADDITIONAL:
			{
				_reqs.add(new Req(AcquireSkillList.CLAN_ADDITIONAL, skillLearn.getItemId(), skillLearn.getItemCount(), 0));
				break;
			}
			case AcquireSkillList.COLLECT:
			{
				_reqs.add(new Req(AcquireSkillList.COLLECT, skillLearn.getItemId(), skillLearn.getItemCount(), 50));
				_spCost = 0;
				break;
			}
			case AcquireSkillList.TRANSFORMATION:
			{
				if(skillLearn.getItemId() > 0)
					_reqs.add(new Req(AcquireSkillList.TRANSFORMATION, skillLearn.getItemId(), skillLearn.getItemCount(), 50));
				break;
			}
			case AcquireSkillList.SUBCLASS_SKILLS:
			{
				if(!Config.ALLOW_LEARN_SUBCLASS_SKILLS_WO_ITEM)
				{
					final int spellbookId = SkillTable.SubclassSkills.getCertificateForSkill(_id);
					_reqs.add(new Req(AcquireSkillList.SUBCLASS_SKILLS, spellbookId, 1, 2));
				}
				break;
			}
			default:
			{
				final int spb_id = SkillSpellbookTable._skillSpellbooks.get(SkillSpellbookTable.hashCode(new int[] { _id, _level }));
				if(spb_id > 0)
					_reqs.add(new Req(skillLearn.common ? AcquireSkillList.USUAL : _clan != null ? AcquireSkillList.CLAN : 99, spb_id, skillLearn.getItemCount(), skillLearn.common || _clan != null ? 2 : 50));
			}
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x91);
		writeD(_id);
		writeD(_level);
		writeD(_spCost);
		writeD(_mode);

		writeD(_reqs.size());

		for(final Req temp : _reqs)
		{
			writeD(temp.type);
			writeD(temp.id);
			writeQ(temp.count);
			writeD(temp.unk);
		}
	}

	@Override
	public String getType()
	{
		return _S__91_ACQUIRESKILLINFO;
	}
}
