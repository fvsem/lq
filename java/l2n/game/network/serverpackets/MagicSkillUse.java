package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;

public class MagicSkillUse extends L2GameServerPacket
{
	private static final String _S__5A_MAGICSKILLUSER = "[S] 5A MagicSkillUser";
	private final int _targetId, _tx, _ty, _tz;
	private final int _chaId, _x, _y, _z;
	private final int _skillId;
	private final int _skillLevel;
	private final int _hitTime;
	private final long _reuseDelay;

	public int getSkillId()
	{
		return _skillId;
	}

	public MagicSkillUse(final L2Character cha, final L2Character target, final int skillId, final int skillLevel, final int hitTime, final int reuseDelay)
	{
		_chaId = cha.getObjectId();
		_targetId = target.getObjectId();
		_skillId = skillId;
		_skillLevel = skillLevel;
		_hitTime = hitTime;
		_reuseDelay = reuseDelay;
		_x = cha.getX();
		_y = cha.getY();
		_z = cha.getZ();
		_tx = target.getX();
		_ty = target.getY();
		_tz = target.getZ();

		// Мобы говорят названия скилов при касте. Нужно для отладки.
		if(Config.SAY_CASTING_SKILL_NAME && cha.isNpc())
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(_skillId, _skillLevel);
			Functions.npcSay((L2NpcInstance) cha, "Casting " + sk.getName() + "[" + _skillId + "." + _skillLevel + "]");
		}
	}

	public MagicSkillUse(final L2Character cha, final int skillId, final int skillLevel, final int hitTime, final int reuseDelay)
	{
		_chaId = cha.getObjectId();
		_targetId = cha.getTargetId();
		_skillId = skillId;
		_skillLevel = skillLevel;
		_hitTime = hitTime;
		_reuseDelay = reuseDelay;
		_x = cha.getX();
		_y = cha.getY();
		_z = cha.getZ();
		_tx = cha.getX();
		_ty = cha.getY();
		_tz = cha.getZ();

		// Мобы говорят названия скилов при касте. Нужно для отладки
		if(Config.SAY_CASTING_SKILL_NAME && cha.isNpc())
		{
			final L2Skill sk = SkillTable.getInstance().getInfo(_skillId, _skillLevel);
			Functions.npcSay((L2NpcInstance) cha, "Casting " + sk.getName() + "[" + _skillId + "." + _skillLevel + "]");
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x48);
		writeD(_chaId);
		writeD(_targetId);
		writeD(_skillId);
		writeD(_skillLevel);
		writeD(_hitTime);
		writeD((int) _reuseDelay);
		writeD(_x);
		writeD(_y);
		writeD(_z);
		writeD(0x00); // unknown
		writeD(_tx);
		writeD(_ty);
		writeD(_tz);
	}

	@Override
	public String getType()
	{
		return _S__5A_MAGICSKILLUSER;
	}
}
