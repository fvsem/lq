package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.skills.SkillTimeStamp;

import java.util.Collection;
import java.util.Iterator;

public class SkillCoolTime extends L2GameServerPacket
{
	private static final String _S__c7_SKILLCOOLTIME = "[S] c7 SkillCoolTime";

	private final Collection<SkillTimeStamp> _reuseTimeStamps;

	public SkillCoolTime(final L2Player player)
	{
		_reuseTimeStamps = player.getSkillReuses();
		final Iterator<SkillTimeStamp> iter = _reuseTimeStamps.iterator();
		while (iter.hasNext())
			if(!iter.next().hasNotPassed()) // remove expired timestamps
				iter.remove();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xc7); // packet type
		writeD(_reuseTimeStamps.size()); // Size of list
		for(final SkillTimeStamp ts : _reuseTimeStamps)
		{
			writeD(ts.getId()); // Skill Id
			writeD(ts.getLevel()); // Skill Level
			writeD((int) ts.getReuseBasic() / 1000); // Total reuse delay, seconds
			writeD((int) ts.getReuseCurrent() / 1000); // Time remaining, seconds
		}
	}

	@Override
	public String getType()
	{
		return _S__c7_SKILLCOOLTIME;
	}
}
