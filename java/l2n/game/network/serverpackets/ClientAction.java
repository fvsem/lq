package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 16:04:59
 */
public class ClientAction extends L2GameServerPacket
{
	private static final String _S__8F_ACTIONFAILED = "[S] 8f ActionFail";

	@Override
	protected void writeImpl()
	{
		writeC(0X8F);
	}

	@Override
	public String getType()
	{
		return _S__8F_ACTIONFAILED;
	}
}
