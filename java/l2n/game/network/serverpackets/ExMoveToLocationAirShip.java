/**
 * 
 */
package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.util.Location;

/**
 * @author L2System Project
 *         06.11.2009 16:36:45
 */
public class ExMoveToLocationAirShip extends L2GameServerPacket
{
	private static final String _S__FE_65_EXAIRSHIPMOVETOLOCATION = "[S] FE:65 ExGetOnAirShip";

	private final int airShipId;
	private final Location _origin, _destination;

	public ExMoveToLocationAirShip(final L2AirShip boat, final Location origin, final Location destination)
	{
		airShipId = boat.getObjectId();
		_origin = origin;
		_destination = destination;
	}

	@Override
	protected void writeImpl()
	{
		if(_destination == null)
			return;

		writeC(0xfe);
		writeH(0x65);

		writeD(airShipId);
		writeD(_destination.getX());
		writeD(_destination.getY());
		writeD(_destination.getZ());
		writeD(_origin.getX());
		writeD(_origin.getY());
		writeD(_origin.getZ());
	}

	@Override
	public String getType()
	{
		return _S__FE_65_EXAIRSHIPMOVETOLOCATION;
	}
}
