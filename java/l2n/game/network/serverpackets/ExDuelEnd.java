package l2n.game.network.serverpackets;

public class ExDuelEnd extends L2GameServerPacket
{
	int _duelType;

	public ExDuelEnd(final int duelType)
	{
		_duelType = duelType;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x4f);
		writeD(_duelType); // хз почему 0, возможно 1 для party duel. нет возможности проверить.
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return "[S] FE : 4F ExDuelEnd";
	}
}
