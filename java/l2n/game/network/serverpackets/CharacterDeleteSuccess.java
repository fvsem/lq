package l2n.game.network.serverpackets;

public class CharacterDeleteSuccess extends L2GameServerPacket
{
	private static final String _S__1d_CHARDELETEOK = "[S] 1d CharDeleteOk";

	@Override
	protected final void writeImpl()
	{
		writeC(0x1d);
	}

	@Override
	public String getType()
	{
		return _S__1d_CHARDELETEOK;
	}
}
