package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: (chd) ddd
 * d: Always -1
 * d: Origin Team
 * d: Destination Team
 */
public class ExCubeGameChangeTeam extends L2GameServerPacket
{
	private final L2Player _player;
	private final boolean _fromRedTeam;

	/**
	 * Move Player from Team x to Team y
	 * 
	 * @param player
	 *            Player Instance
	 * @param fromRedTeam
	 *            Is Player from Red Team?
	 */
	public ExCubeGameChangeTeam(final L2Player player, final boolean fromRedTeam)
	{
		_player = player;
		_fromRedTeam = fromRedTeam;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x05);

		writeD(_player.getObjectId());
		writeD(_fromRedTeam ? 0x01 : 0x00);
		writeD(_fromRedTeam ? 0x00 : 0x01);
	}
}
