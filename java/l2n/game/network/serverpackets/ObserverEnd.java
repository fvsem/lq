package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class ObserverEnd extends L2GameServerPacket
{
	// ddSS
	private static final String _S__E0_OBSERVRETURN = "[S] ec ObservationReturn";
	private final Location _loc;

	public ObserverEnd(final L2Player observer)
	{
		_loc = observer.getLoc();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xec);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _S__E0_OBSERVRETURN;
	}
}
