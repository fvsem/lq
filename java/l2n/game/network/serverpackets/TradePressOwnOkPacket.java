package l2n.game.network.serverpackets;

public class TradePressOwnOkPacket extends L2GameServerPacket
{
	private static final String _S__53_TRADEPRESSOWNOK = "[S] 53 TradePressOwnOkPacket";

	@Override
	protected final void writeImpl()
	{
		writeC(0x53);
	}

	@Override
	public String getType()
	{
		return _S__53_TRADEPRESSOWNOK;
	}
}
