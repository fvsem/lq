package l2n.game.network.serverpackets;

public class PledgeShowMemberListDelete extends L2GameServerPacket
{
	private static final String _S__5d_PLEDGESHOWMEMBERLISTDELETE = "[S] 5d PledgeShowMemberListDelete";
	private final String _player;

	public PledgeShowMemberListDelete(final String playerName)
	{
		_player = playerName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x5d);
		writeS(_player);
	}

	@Override
	public String getType()
	{
		return _S__5d_PLEDGESHOWMEMBERLISTDELETE;
	}
}
