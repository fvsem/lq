package l2n.game.network.serverpackets;

import l2n.game.model.L2Skill.Element;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.Inventory;
import l2n.game.model.items.PcInventory;
import l2n.util.Location;

public class GMViewCharacterInfo extends L2GameServerPacket
{
	private static final String _S__8F_GMVIEWCHARINFO = "[S] 8F GMViewCharacterInfo";
	private final Location _loc;
	private final PcInventory _inv;
	private final int obj_id, _race, _sex, class_id, pvp_flag, karma,
			level, mount_type, fame, vitPoints;
	private final int _str, _con, _dex, _int, _wit, _men, _sp;
	private final int curHp, maxHp, curMp, maxMp, curCp, maxCp,
			curLoad, maxLoad, rec_left, rec_have;
	private final int _patk, _patkspd, _pdef, evasion, accuracy,
			crit, _matk, _matkspd;
	private final int _mdef, hair_style, hair_color, face,
			gm_commands;
	private final int clan_id, clan_crest_id, ally_id, title_color;
	private final int noble, hero, private_store, name_color,
			pk_kills, pvp_kills;
	private final int _runSpd, _walkSpd, _swimSpd, DwarvenCraftLevel,
			running, pledge_class;
	private final String _name, title;
	private final long _exp;
	private final float move_speed, attack_speed, col_radius,
			col_height;
	private final int[] attackElement;
	private final int DefenceFire, DefenceWater, DefenceWind,
			DefenceEarth, DefenceHoly, DefenceUnholy;

	public GMViewCharacterInfo(final L2Player cha)
	{
		_inv = cha.getInventory();
		_loc = cha.getLoc();
		obj_id = cha.getObjectId();
		_name = cha.getName();
		_race = cha.getRace().ordinal();
		_sex = cha.getSex();
		class_id = cha.getClassId().getId();
		level = cha.getLevel();
		_exp = cha.getExp();
		_str = cha.getSTR();
		_dex = cha.getDEX();
		_con = cha.getCON();
		_int = cha.getINT();
		_wit = cha.getWIT();
		_men = cha.getMEN();
		curHp = (int) cha.getCurrentHp();
		maxHp = cha.getMaxHp();
		curMp = (int) cha.getCurrentMp();
		maxMp = cha.getMaxMp();
		_sp = cha.getSp();
		curLoad = cha.getCurrentLoad();
		maxLoad = cha.getMaxLoad();
		_patk = cha.getPAtk(null);
		_patkspd = cha.getPAtkSpd();
		_pdef = cha.getPDef(null);
		evasion = cha.getEvasionRate(null);
		accuracy = cha.getAccuracy();
		crit = cha.getCriticalHit(null, null);
		_matk = cha.getMAtk(null, null);
		_matkspd = cha.getMAtkSpd();
		_mdef = cha.getMDef(null, null);
		pvp_flag = cha.getPvpFlag();
		karma = cha.getKarma();
		_runSpd = cha.getRunSpeed();
		_walkSpd = cha.getWalkSpeed();
		_swimSpd = cha.getSwimSpeed();
		move_speed = cha.getMovementSpeedMultiplier();
		attack_speed = cha.getAttackSpeedMultiplier();
		mount_type = cha.getMountType();
		col_radius = cha.getColRadius();
		col_height = cha.getColHeight();
		hair_style = cha.getHairStyle();
		hair_color = cha.getHairColor();
		face = cha.getFace();
		gm_commands = cha.isGM() ? 1 : 0;
		title = cha.getTitle();
		clan_id = cha.getClanId();
		clan_crest_id = cha.getClanCrestId(); // clan crest
		ally_id = cha.getAllyId();
		private_store = cha.getPrivateStoreType();
		DwarvenCraftLevel = Math.max(cha.getSkillLevel(1320), 0);
		pk_kills = cha.getPkKills();
		pvp_kills = cha.getPvpKills();
		rec_left = cha.getRecomLeft(); // c2 recommendations remaining
		rec_have = cha.getRecomHave(); // c2 recommendations received
		curCp = (int) cha.getCurrentCp();
		maxCp = cha.getMaxCp();
		running = cha.isRunning() ? 0x01 : 0x00;
		pledge_class = cha.getPledgeClass();
		noble = cha.isNoble() ? 1 : 0; // 0x01: symbol on char menu ctrl+I
		hero = cha.isHero() ? 1 : 0; // 0x01: Hero Aura and symbol
		name_color = cha.getNameColor();
		title_color = cha.getTitleColor();
		attackElement = cha.getAttackElement();
		DefenceFire = cha.getDefence(Element.FIRE);
		DefenceWater = cha.getDefence(Element.WATER);
		DefenceWind = cha.getDefence(Element.WIND);
		DefenceEarth = cha.getDefence(Element.EARTH);
		DefenceHoly = cha.getDefence(Element.SACRED);
		DefenceUnholy = cha.getDefence(Element.UNHOLY);
		fame = cha.getFame();
		vitPoints = (int) cha.getVitalityPoints();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x95);

		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_loc.h);
		writeD(obj_id);
		writeS(_name);
		writeD(_race);
		writeD(_sex);
		writeD(class_id);
		writeD(level);
		writeQ(_exp);
		writeD(_str);
		writeD(_dex);
		writeD(_con);
		writeD(_int);
		writeD(_wit);
		writeD(_men);
		writeD(maxHp);
		writeD(curHp);
		writeD(maxMp);
		writeD(curMp);
		writeD(_sp);
		writeD(curLoad);
		writeD(maxLoad);
		writeD(pk_kills);

		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_UNDER));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_REAR));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LEAR));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_NECK));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_RFINGER));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LFINGER));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_HEAD));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_RHAND));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LHAND));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_GLOVES));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_CHEST));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LEGS));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_FEET));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_HAIR));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LRHAND));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_HAIR));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DHAIR));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_RBRACELET));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_LBRACELET));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO1));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO2));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO3));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO4));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO5));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_DECO6));
		writeD(_inv.getPaperdollObjectId(Inventory.PAPERDOLL_BELT)); // T3 Unknown

		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_UNDER));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_REAR));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LEAR));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_NECK));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_RFINGER));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LFINGER));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_HEAD));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_RHAND));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LHAND));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_GLOVES));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_CHEST));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LEGS));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_FEET));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_HAIR));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LRHAND));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_HAIR));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DHAIR));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_RBRACELET));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_LBRACELET));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO1));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO2));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO3));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO4));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO5));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_DECO6));
		writeD(_inv.getPaperdollItemId(Inventory.PAPERDOLL_BELT)); // T3 Unknown
		writeD(0); // T3 Unknown
		writeD(0); // T3 Unknown

		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeD(_inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_RHAND));
		writeH(0x00);

		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeD(_inv.getPaperdollAugmentationId(Inventory.PAPERDOLL_LRHAND));
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);

		// start of T1 new h's
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		writeH(0x00);
		// end of T1 new h's
		writeD(0x00); // T3 Unknown

		writeD(_patk);
		writeD(_patkspd);
		writeD(_pdef);
		writeD(evasion);
		writeD(accuracy);
		writeD(crit);
		writeD(_matk);
		writeD(_matkspd);
		writeD(_patkspd);
		writeD(_mdef);
		writeD(pvp_flag);
		writeD(karma);
		writeD(_runSpd);
		writeD(_walkSpd);
		writeD(_swimSpd); // swimspeed
		writeD(_swimSpd); // swimspeed
		writeD(_runSpd);
		writeD(_walkSpd);
		writeD(_runSpd);
		writeD(_walkSpd);
		writeF(move_speed);
		writeF(attack_speed);
		writeF(col_radius);
		writeF(col_height);
		writeD(hair_style);
		writeD(hair_color);
		writeD(face);
		writeD(gm_commands);
		writeS(title);
		writeD(clan_id);
		writeD(clan_crest_id);
		writeD(ally_id);
		writeC(mount_type);
		writeC(private_store);
		writeC(DwarvenCraftLevel); // _cha.getDwarvenCraftLevel() > 0 ? 1 : 0
		writeD(pk_kills);
		writeD(pvp_kills);
		writeH(rec_left);
		writeH(rec_have); // Blue value for name (0 = white, 255 = pure blue)
		writeD(class_id);
		writeD(0x00); // special effects? circles around player...
		writeD(maxCp);
		writeD(curCp);
		writeC(running); // changes the Speed display on Status Window
		writeC(321);
		writeD(pledge_class); // changes the text above CP on Status Window
		writeC(noble);
		writeC(hero);
		writeD(name_color);
		writeD(title_color);

		writeD(attackElement == null ? -2 : attackElement[0]);
		writeD(attackElement == null ? 0 : attackElement[1]);
		writeD(DefenceFire);
		writeD(DefenceWater);
		writeD(DefenceWind);
		writeD(DefenceEarth);
		writeD(DefenceHoly);
		writeD(DefenceUnholy);
		writeD(fame);
		writeD(vitPoints);
	}

	@Override
	public String getType()
	{
		return _S__8F_GMVIEWCHARINFO;
	}
}
