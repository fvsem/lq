package l2n.game.network.serverpackets;

public class SendTradeRequest extends L2GameServerPacket
{
	private static final String _S__70_SENDTRADEREQUEST = "[S] 70 SendTradeRequest";
	private final int _senderID;

	public SendTradeRequest(final int senderID)
	{
		_senderID = senderID;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x70);
		writeD(_senderID);
	}

	@Override
	public String getType()
	{
		return _S__70_SENDTRADEREQUEST;
	}
}
