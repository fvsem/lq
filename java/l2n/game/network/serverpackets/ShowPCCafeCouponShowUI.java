package l2n.game.network.serverpackets;

/**
 * Даный пакет показывает менюшку для ввода серийника. Можно что-то придумать :)<br>
 * Format: (ch)
 */
public class ShowPCCafeCouponShowUI extends L2GameServerPacket
{
	private static final String _S__FE_44_SHOWPCCAFECOUPONSHOWUI = "[S] FE:44 ShowPCCafeCouponShowUI";

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x44);
	}

	@Override
	public String getType()
	{
		return _S__FE_44_SHOWPCCAFECOUPONSHOWUI;
	}
}
