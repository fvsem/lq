package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;

/**
 * sample a3 05000000 03000000 03000000 06000000 3c000000 00000000 power strike 10000000 02000000 06000000 3c000000 00000000 mortal blow 38000000 04000000 06000000 36010000 00000000 power shot
 * 4d000000 01000000 01000000 98030000 01000000 ATTACK aura 920sp 8e000000 03000000 03000000 cc010000 00000000 Armor Mastery format d (ddddd) skillid, level, maxlevel?,
 */
public class AcquireSkillList extends L2GameServerPacket
{
	private static final String _S__90_ACQUIRESKILLLIST = "[S] 90 AquireSkillList";
	private final GArray<Skill> _skills;
	private final int _skillsType;

	public static final int USUAL = 0;
	public static final int FISHING = 1;
	public static final int CLAN = 2;
	public static final int CLAN_ADDITIONAL = 3; // Squad скилы похоже
	public static final int TRANSFORMATION = 4;
	public static final int SUBCLASS_SKILLS = 5;
	public static final int COLLECT = 6;

	public static final int BISHOP_TRANSFER = 7;
	public static final int ELDER_TRANSFER = 8;
	public static final int SILEN_ELDER_TRANSFER = 9;

	private class Skill
	{
		public int id;
		public int nextLevel;
		public int maxLevel;
		public int spCost;
		public int requirements;

		public Skill(final int _id, final int _nextLevel, final int _maxLevel, final int _spCost, final int _requirements)
		{
			id = _id;
			nextLevel = _nextLevel;
			maxLevel = _maxLevel;
			spCost = _spCost;
			requirements = _requirements;
		}
	}

	public AcquireSkillList(final int type)
	{
		_skills = new GArray<Skill>();
		_skillsType = type;
	}

	public void addSkill(final int id, final int nextLevel, final int maxLevel, final int Cost, final int requirements)
	{
		_skills.add(new Skill(id, nextLevel, maxLevel, Cost, requirements));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x90);
		writeD(_skillsType); // Kamael: 0: standart, 1: fishing, 2: clans, 3: ?, 4: transformation
		writeD(_skills.size());

		for(final Skill temp : _skills)
		{
			writeD(temp.id);
			writeD(temp.nextLevel);
			writeD(temp.maxLevel);
			writeD(temp.spCost);
			writeD(temp.requirements);
			if(_skillsType == CLAN_ADDITIONAL)
				writeD(0);
		}
	}

	@Override
	public String getType()
	{
		return _S__90_ACQUIRESKILLLIST;
	}
}
