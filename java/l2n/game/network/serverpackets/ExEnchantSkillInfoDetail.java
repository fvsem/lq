package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.tables.SkillTreeTable;

import java.util.logging.Logger;

public class ExEnchantSkillInfoDetail extends L2GameServerPacket
{
	private static final Logger _log = Logger.getLogger(ExEnchantSkillInfoDetail.class.getName());

	private static final int TYPE_NORMAL_ENCHANT = 0;
	private static final int TYPE_SAFE_ENCHANT = 1;
	private static final int TYPE_UNTRAIN_ENCHANT = 2;
	private static final int TYPE_CHANGE_ENCHANT = 3;

	private int bookId = 0;
	private int reqCount = 0;
	private int _type;
	private int _skillid;
	private int _skilllvl;
	private int _chance;
	private int _sp;
	private int _adenacount;

	/**
	 * @param type
	 * @param skillId
	 * @param skillLvl
	 * @param activeChar
	 */
	public ExEnchantSkillInfoDetail(final int type, final int skillId, int skillLvl, final L2Player activeChar)
	{
		if(activeChar == null)
			return;

		skillLvl = Math.max(skillLvl, 1);
		L2EnchantSkillLearn esd = null;

		try
		{
			if(skillLvl % 100 == 0 && type == TYPE_UNTRAIN_ENCHANT) // При откате скила на базовый уровень
				esd = SkillTreeTable.getInstance().getEnchantsForChange(skillId, skillLvl + 1)[0];
			else if(skillLvl > 100)
				esd = SkillTreeTable.getInstance().getSkillEnchant(skillId, skillLvl);

			if(esd == null)
			{
				activeChar.sendActionFailed();
				return;
			}
		}
		catch(final Exception e)
		{
			activeChar.sendActionFailed();
			return;
		}

		_type = type;
		_chance = esd.getRate(activeChar);
		_skillid = skillId;

		if(skillLvl % 100 == 0 && type == TYPE_UNTRAIN_ENCHANT) // При откате скила на базовый уровень
			_skilllvl = esd.getBaseLevel();
		else
			_skilllvl = skillLvl;

		double spMult = 1;
		switch (_type)
		{
			case TYPE_NORMAL_ENCHANT:
				bookId = SkillTreeTable.NORMAL_ENCHANT_BOOK;
				reqCount = _skilllvl % 100 > 1 ? 0 : 1;
				break;
			case TYPE_SAFE_ENCHANT:
				bookId = SkillTreeTable.SAFE_ENCHANT_BOOK;
				reqCount = 1;
				spMult = 5;
				break;
			case TYPE_UNTRAIN_ENCHANT:
				bookId = SkillTreeTable.UNTRAIN_ENCHANT_BOOK;
				reqCount = 1;
				break;
			case TYPE_CHANGE_ENCHANT:
				bookId = SkillTreeTable.CHANGE_ENCHANT_BOOK;
				reqCount = 1;
				spMult = 0.8;
				break;
			default:
				_log.warning("Unknown skill enchant type: " + _type);
				return;
		}

		spMult *= esd.getCostMult();
		final int[] cost = esd.getCost();

		_sp = (int) (cost[1] * spMult);

		if(_type != TYPE_UNTRAIN_ENCHANT)
			_adenacount = (int) (cost[0] * spMult);
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x5e);

		writeD(_type);
		writeD(_skillid);
		writeD(_skilllvl);
		writeD(_sp);
		writeD(_chance);

		writeD(2); // items count?
		writeD(57); // adena //TODO unhardcode me
		writeD(_adenacount);
		writeD(bookId); // ItemId Required
		writeD(Config.ALT_DISABLE_ENCHANT_BOOKS ? 0 : reqCount);
	}

	@Override
	public String getType()
	{
		return "[S] FE:5E ExEnchantSkillInfoDetail";
	}
}
