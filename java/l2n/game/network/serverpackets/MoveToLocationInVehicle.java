package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.util.Location;

public class MoveToLocationInVehicle extends L2GameServerPacket
{
	private static final String _S__7E_MOVETOLOCATIONINVEHICLE = "[S] 7E MoveToLocationInVehicle";
	private Location _origin, _destination;
	private int char_obj_id, boat_obj_id;
	private boolean can_writeimpl = false;

	/**
	 * @param actor
	 * @param destination
	 * @param origin
	 */
	public MoveToLocationInVehicle(final L2Player cha, final L2Ship boat, final Location origin, final Location destination)
	{
		if(cha == null)
			return;

		char_obj_id = cha.getObjectId();
		boat_obj_id = boat.getObjectId();
		_destination = destination;
		_origin = origin;
		can_writeimpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!can_writeimpl)
			return;

		writeC(0x7e);
		writeD(char_obj_id);
		writeD(boat_obj_id);
		writeD(_destination.x);
		writeD(_destination.y);
		writeD(_destination.z);
		writeD(_origin.x);
		writeD(_origin.y);
		writeD(_origin.z);
	}

	@Override
	public String getType()
	{
		return _S__7E_MOVETOLOCATIONINVEHICLE;
	}
}
