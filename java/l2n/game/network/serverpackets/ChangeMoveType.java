package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

/**
 * sample
 * 0000: 3e 2a 89 00 4c 01 00 00 00 .|...
 * format dd
 * 
 * @version $Revision: 1.3.2.1.2.4 $ $Date: 2005/03/27 15:29:57 $
 */
public class ChangeMoveType extends L2GameServerPacket
{
	private static final String _S__28_CHANGEMOVETYPE = "[S] 28 ChangeMoveType";
	public static int WALK = 0;
	public static int RUN = 1;

	private final int _chaId;
	private final boolean _running;

	public ChangeMoveType(final L2Character cha)
	{
		_chaId = cha.getObjectId();
		_running = cha.isRunning();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x28);
		writeD(_chaId);
		writeD(_running ? RUN : WALK);
		writeD(0x00); // c2
	}

	@Override
	public String getType()
	{
		return _S__28_CHANGEMOVETYPE;
	}
}
