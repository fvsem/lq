package l2n.game.network.serverpackets;

public class ExChooseInventoryAttributeItem extends L2GameServerPacket
{
	private static final String _S__FE_62_EXCHOOSEINVENTORYATTRIBUTEITEM = "[S] FE 62 ExChooseInventoryAttributeItem";
	private final int _itemId;

	public ExChooseInventoryAttributeItem(final int itemId)
	{
		_itemId = itemId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x62);
		writeD(_itemId);
	}

	@Override
	public String getType()
	{
		return _S__FE_62_EXCHOOSEINVENTORYATTRIBUTEITEM;
	}
}
