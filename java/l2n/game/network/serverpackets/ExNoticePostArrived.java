package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 30.07.2010
 * @time 0:01:35
 */
public class ExNoticePostArrived extends L2GameServerPacket
{
	private static final String _S__FE_A9_EXNOTICEPOSTARRIVED = "[S] A9 ExNoticePostArrived";

	public static final int RESULT_NOT_ENOUGH_POINTS = -1;
	public static final int RESULT_WRONG_PRODUCT = -2;
	public static final int RESULT_INVENTORY_FULL = -4;
	public static final int RESULT_SALE_PERIOD_ENDED = -7;
	public static final int RESULT_WRONG_USER_STATE = -9;
	public static final int RESULT_WRONG_PRODUCT_ITEM = -10;

	private final int _anim;

	public ExNoticePostArrived(final int useAnim)
	{
		_anim = useAnim;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xa9);
		writeD(_anim);
	}

	@Override
	public String getType()
	{
		return _S__FE_A9_EXNOTICEPOSTARRIVED;
	}
}
