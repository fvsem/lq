package l2n.game.network.serverpackets;

import l2n.game.instancemanager.GraciaSeedManager;
import l2n.game.instancemanager.SeedOfInfinityManager;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 23:17:10
 */
public class ExShowSeedMapInfo extends L2GameServerPacket
{
	// Идет Осада 1-го уровня Семени Бессмертия
	public static final int SEED_OF_INFINITY_STAGE_1_ATTACK_IN_PROGRESS = 2766;
	// Идет Осада 2-го уровня Семени Бессмертия
	public static final int SEED_OF_INFINITY_STAGE_2_ATTACK_IN_PROGRESS = 2767;
	// Семя Бессмертия захвачено
	public static final int SEED_OF_INFINITY_CONQUEST_COMPLETE = 2768;
	// Идет Оборона 1-го уровня Семени Бессмертия
	public static final int SEED_OF_INFINITY_STAGE_1_DEFENSE_IN_PROGRESS = 2769;
	// Идет Оборона 2-го уровня Семени Бессмертия
	public static final int SEED_OF_INFINITY_STAGE_2_DEFENSE_IN_PROGRESS = 2770;

	// Проходит Осада Семени Разрушения
	public static final int SEED_OF_DESTRUCTION_ATTACK_IN_PROGRESS = 2770;
	// Семя Разрушения захвачено
	public static final int SEED_OF_DESTRUCTION_CONQUEST_COMPLETE = 2772;
	// Идет Оборона Семени Разрушения
	public static final int SEED_OF_DESTRUCTION_DEFENSE_IN_PROGRESS = 2773;

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xA1);

		writeD(2); // seed count

		/** Seed of Destruction */
		writeD(-246857); // x coord
		writeD(251960); // y coord
		writeD(4331); // z coord
		writeD(2770 + GraciaSeedManager.getInstance().getSoDState()); // sys msg id

		/** Seed of Infinity */
		writeD(-213770); // x coord
		writeD(210760); // y coord
		writeD(4400); // z coord
		writeD(2765 + SeedOfInfinityManager.getCurrentCycle()); // sys msg id
	}
}
