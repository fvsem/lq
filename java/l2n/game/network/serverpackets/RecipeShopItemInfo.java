package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;

/**
 * ddddd
 */
public class RecipeShopItemInfo extends L2GameServerPacket
{

	private static final String _S__e0_RecipeShopItemInfo = "[S] e0 RecipeShopItemInfo";
	private int _recipeId;
	private int _shopId;
	private int curMp;
	private int maxMp;
	private int _success = 0xFFFFFFFF;
	private long _price;
	private boolean can_writeImpl = false;

	public RecipeShopItemInfo(final int shopId, final int recipeId, final long price, final int success, final L2Player activeChar)
	{
		_recipeId = recipeId;
		_shopId = shopId;
		_price = price;
		_success = success;

		final L2Object manufacturer = activeChar.getVisibleObject(_shopId);
		if(manufacturer == null)
			return;
		if(!manufacturer.isPlayer())
			return;

		curMp = (int) ((L2Player) manufacturer).getCurrentMp();
		maxMp = ((L2Player) manufacturer).getMaxMp();
		can_writeImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!can_writeImpl)
			return;

		writeC(0xE0);
		writeD(_shopId);
		writeD(_recipeId);
		writeD(curMp);
		writeD(maxMp);
		writeD(_success);
		writeQ(_price);
	}

	@Override
	public String getType()
	{
		return _S__e0_RecipeShopItemInfo;
	}
}
