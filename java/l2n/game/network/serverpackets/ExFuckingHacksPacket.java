package l2n.game.network.serverpackets;

import l2n.util.Rnd;

/**
 * ExFuckingHacksPacket - Bringing bullshits to the next level!
 * 
 * @author L2System Project
 * @date 11.04.2011
 * @time 17:50:56
 */
public class ExFuckingHacksPacket extends L2GameServerPacket
{
	private static final String _S__ZZ_69_L33TSHIT = "[S] ZZ:69 L33TSHIT";

	@Override
	protected final void writeImpl()
	{
		// (0x0E); // Unknown Packet - Disables LEFT/RIGHT Clicks. - Interesting.
		writeC(0xAE);
		writeS("Good luck with reverse engineering and figuring a bypass. b00h! Evil Rabbits are owning you.");
		writeD(Rnd.get(10));
		writeD(Rnd.get(100));
		writeD(Rnd.get(1000));
		writeD(Rnd.get(1));
	}

	@Override
	public final String getType()
	{
		return _S__ZZ_69_L33TSHIT;
	}
}
