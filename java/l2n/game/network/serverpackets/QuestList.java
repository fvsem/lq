package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.QuestState;

public class QuestList extends L2GameServerPacket
{
	private static final String _S__86_QUESTLIST = "[S] 86 QuestList";

	private final GArray<int[]> questlist = new GArray<int[]>();
	private final static byte[] wtf = new byte[128];

	public QuestList(final L2Player player)
	{
		if(player == null)
			return;

		for(final QuestState quest : player.getAllQuestsStates())
			if(quest != null && (quest.getQuest().getQuestIntId() < 999 || quest.getQuest().getQuestIntId() > 10000) && quest.getQuest().getQuestIntId() != 255 && quest.isStarted())
				questlist.add(new int[] { quest.getQuest().getQuestIntId(), quest.getInt("cond") });
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x86);
		writeH(questlist.size());
		for(final int[] q : questlist)
		{
			writeD(q[0]);
			writeD(q[1]);
		}
		writeB(wtf);
	}

	@Override
	public String getType()
	{
		return _S__86_QUESTLIST;
	}
}
