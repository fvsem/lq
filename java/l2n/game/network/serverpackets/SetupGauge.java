package l2n.game.network.serverpackets;

/**
 * sample 0000: 85 00 00 00 00 f0 1a 00 00
 */
public class SetupGauge extends L2GameServerPacket
{
	private static final String _S__6b_SETUPGAUGE = "[S] 6b SetupGauge";
	public static final int BLUE = 0;
	public static final int RED = 1;
	public static final int CYAN = 2;
	public static final int GREEN = 3;

	private final int _color;
	private final int _time;

	public SetupGauge(final int color, final int time)
	{
		_color = color;// color 0-blue 1-red 2-cyan 3-
		_time = time;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x6b);
		writeD(_color);
		writeD(_time);

		writeD(_time); // c2
	}

	@Override
	public String getType()
	{
		return _S__6b_SETUPGAUGE;
	}
}
