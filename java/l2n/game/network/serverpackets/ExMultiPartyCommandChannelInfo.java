package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2Party;

/**
 * Update a Command Channel
 * Format:
 * ch sddd[sdd]
 * Пример пакета с оффа (828 протокол):
 * fe 31 00
 * 62 00 75 00 73 00 74 00 65 00 72 00 00 00 - имя лидера СС
 * 00 00 00 00 - ? Looting type
 * 19 00 00 00 - общее число человек в СС
 * 04 00 00 00 - общее число партий в СС
 * [
 * 62 00 75 00 73 00 74 00 65 00 72 00 00 00 - лидер партии 1
 * 36 46 70 4c - ObjId пати лидера 1
 * 08 00 00 00 - количество мемберов в пати 1
 * 4e 00 31 00 67 00 68 00 74 00 48 00 75
 * ]
 */
public class ExMultiPartyCommandChannelInfo extends L2GameServerPacket
{

	private static final String _S__FE_31_EXMULTYPARTYCOMMANDCHANNELINFO = "[S] FE:31 ExMultiPartyCommandChannelInfo";
	private String ChannelLeaderName;
	private int MemberCount;
	private GArray<ChannelPartyInfo> parties;

	/**
	 * @param L2CommandChannel
	 *            CommandChannel
	 */
	public ExMultiPartyCommandChannelInfo(final L2CommandChannel channel)
	{
		if(channel == null)
			return;
		ChannelLeaderName = channel.getChannelLeader().getName();
		MemberCount = channel.getMemberCount();

		parties = new GArray<ChannelPartyInfo>();
		for(final L2Party party : channel.getParties())
			parties.add(new ChannelPartyInfo(party.getPartyLeader().getName(), party.getPartyLeaderOID(), party.getMemberCount()));
	}

	@Override
	protected void writeImpl()
	{
		if(parties == null)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x31);
		writeS(ChannelLeaderName); // имя лидера CC
		writeD(0); // Channelloot 0 or 1
		writeD(MemberCount); // общее число человек в СС
		writeD(parties.size()); // общее число партий в СС

		for(final ChannelPartyInfo party : parties)
		{
			writeS(party.Leader_name); // имя лидера партии
			writeD(party.Leader_obj_id); // ObjId пати лидера
			writeD(party.MemberCount); // количество мемберов в пати
		}
		parties.clear();
	}

	static class ChannelPartyInfo
	{
		public String Leader_name;
		public int Leader_obj_id, MemberCount;

		public ChannelPartyInfo(final String _Leader_name, final int _Leader_obj_id, final int _MemberCount)
		{
			Leader_name = _Leader_name;
			Leader_obj_id = _Leader_obj_id;
			MemberCount = _MemberCount;
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_31_EXMULTYPARTYCOMMANDCHANNELINFO;
	}
}
