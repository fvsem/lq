package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: (chd) dddS
 * d: Always -1
 * d: Player Team
 * d: Player Object ID
 * S: Player Name
 */
public class ExCubeGameAddPlayer extends L2GameServerPacket
{
	private static final String ExCubeGameAddPlayer = "[S] FE:97:01 ExCubeGameAddPlayer";

	private final L2Player _player;
	private final boolean _isRedTeam;

	/**
	 * Add Player To Minigame Waiting List
	 * 
	 * @param player
	 *            Player Instance
	 * @param isRedTeam
	 *            Is Player from Red Team?
	 */
	public ExCubeGameAddPlayer(final L2Player player, final boolean isRedTeam)
	{
		_player = player;
		_isRedTeam = isRedTeam;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x01);

		writeD(0xffffffff);

		writeD(_isRedTeam ? 0x01 : 0x00);
		writeD(_player.getObjectId());
		writeS(_player.getName());
	}

	@Override
	public final String getType()
	{
		return ExCubeGameAddPlayer;
	}
}
