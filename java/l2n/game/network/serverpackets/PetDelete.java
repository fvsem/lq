package l2n.game.network.serverpackets;

public class PetDelete extends L2GameServerPacket
{
	private static final String _S__b7_PETDELETE = "[S] b7 PetDelete";
	private final int _petId;
	private final int _petnum;

	public PetDelete(final int petId, final int petnum)
	{
		_petId = petId;
		_petnum = petnum;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xb7);
		writeD(_petId);// dont really know what these two are since i never needed them
		writeD(_petnum);
	}

	@Override
	public String getType()
	{
		return _S__b7_PETDELETE;
	}
}
