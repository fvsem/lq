package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2DoorInstance;

/**
 * 61
 * d6 6d c0 4b door id
 * 8f 14 00 00 x
 * b7 f1 00 00 y
 * 60 f2 ff ff z
 * 00 00 00 00 ??
 * format dddd rev 377 ID:%d X:%d Y:%d Z:%d
 * ddddd rev 419
 * 
 * @version $Revision: 1.3.2.2.2.3 $ $Date: 2005/03/27 15:29:57 $
 */
public class DoorStatusUpdate extends L2GameServerPacket
{
	private static final String _S__4D_DOORSTATUSUPDATE = "[S] 4D DoorStatusUpdate";
	private final int obj_id, door_id, _opened, dmg, isenemy, curHp,
			maxHp;

	public DoorStatusUpdate(final L2DoorInstance door)
	{
		obj_id = door.getObjectId();
		door_id = door.getDoorId();
		_opened = door.isOpen() ? 0 : 1;
		dmg = door.getDamage();
		isenemy = door.isEnemyOf(getClient().getActiveChar()) ? 1 : 0;
		curHp = (int) door.getCurrentHp();
		maxHp = door.getMaxHp();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x4D);
		writeD(obj_id);
		writeD(_opened);
		writeD(dmg);
		writeD(isenemy);
		writeD(door_id);
		writeD(maxHp); // у сф наоборот, сначало curHp, потом maxHp
		writeD(curHp);
	}

	@Override
	public String getType()
	{
		return _S__4D_DOORSTATUSUPDATE;
	}
}
