package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.util.Location;

/**
 * @author L2System Project
 *         06.11.2009 16:53:17
 */
public class ExGetOffAirShip extends L2GameServerPacket
{
	private static final String _S__FE_63_EXGETOFFAIRSHIP = "[S] FE:63 ExGetOffAirShip";

	private boolean _canWriteImpl = false;
	private int _charObjId, _shipObjId;
	private Location _loc;

	public ExGetOffAirShip(final L2Player cha, final L2AirShip boat, final Location loc)
	{
		if(loc == null)
			return;

		_charObjId = cha.getObjectId();
		_shipObjId = boat.getObjectId();
		_loc = loc;

		_canWriteImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!_canWriteImpl)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x64);
		writeD(_charObjId);
		writeD(_shipObjId);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _S__FE_63_EXGETOFFAIRSHIP;
	}
}
