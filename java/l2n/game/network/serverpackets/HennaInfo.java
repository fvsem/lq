package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;

public class HennaInfo extends L2GameServerPacket
{
	private static final String _S__e5_HennaInfo = "[S] e5 HennaInfo";
	private final L2HennaInstance[] _hennas = new L2HennaInstance[3];
	private final int _count, _str, _con, _dex, _int, _wit, _men;

	public HennaInfo(final L2Player player)
	{
		int j = 0;
		for(int i = 0; i < 3; i++)
		{
			final L2HennaInstance h = player.getHenna(i + 1);
			if(h != null)
				_hennas[j++] = h;
		}
		_count = j;

		_str = player.getHennaStatSTR();
		_con = player.getHennaStatCON();
		_dex = player.getHennaStatDEX();
		_int = player.getHennaStatINT();
		_wit = player.getHennaStatWIT();
		_men = player.getHennaStatMEN();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe5);
		writeC(_int); // equip INT
		writeC(_str); // equip STR
		writeC(_con); // equip CON
		writeC(_men); // equip MEM
		writeC(_dex); // equip DEX
		writeC(_wit); // equip WIT
		writeD(3); // interlude, slots?
		writeD(_count);
		for(int i = 0; i < _count; i++)
			if(_hennas[i] != null)
			{
				writeD(_hennas[i].getSymbolId());
				writeD(_hennas[i].getSymbolId());
			}
	}

	@Override
	public String getType()
	{
		return _S__e5_HennaInfo;
	}
}
