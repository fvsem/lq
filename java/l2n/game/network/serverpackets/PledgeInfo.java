package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan;

public class PledgeInfo extends L2GameServerPacket
{
	private static final String _S__89_PLEDGEINFO = "[S] 89 PledgeInfo";
	private final int clan_id;
	private final String clan_name, ally_name;

	public PledgeInfo(final L2Clan clan)
	{
		clan_id = clan.getClanId();
		clan_name = clan.getName();
		ally_name = clan.getAlliance() == null ? "" : clan.getAlliance().getAllyName();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x89);
		writeD(clan_id);
		writeS(clan_name);
		writeS(ally_name);
	}

	@Override
	public String getType()
	{
		return _S__89_PLEDGEINFO;
	}
}
