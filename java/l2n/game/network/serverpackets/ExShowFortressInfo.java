package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.L2Clan;
import l2n.game.model.entity.residence.Fortress;

public class ExShowFortressInfo extends L2GameServerPacket
{
	private static final String _S__FE_15_EXSHOWFORTRESSINFO = "[S] FE:15 ExShowFortressInfo";
	private final GArray<FortressInfo> infos = new GArray<FortressInfo>();

	private static class FortressInfo
	{
		public int fort_id, fort_status, fort_siege;
		public String fort_owner;

		public FortressInfo(final String _fort_owner, final int _fort_id, final int _fort_status, final int _fort_siege)
		{
			fort_owner = _fort_owner;
			fort_id = _fort_id;
			fort_status = _fort_status;
			fort_siege = _fort_siege;
		}
	}

	public ExShowFortressInfo()
	{
		int fort_id, fort_status, fort_siege;
		L2Clan fort_owner;

		for(final Fortress fortress : FortressManager.getInstance().getFortresses().values())
		{
			fort_id = fortress.getId();
			fort_owner = fortress.getOwner();

			fort_status = fortress.getSiege().getAttackerClans().size() <= 0 ? 0 : 1;
			fort_siege = fort_owner == null ? 0 : (int) (System.currentTimeMillis() / 1000L - fortress.getOwnDate());
			infos.add(new FortressInfo(fort_owner == null ? "" : fort_owner.getName(), fort_id, fort_status, fort_siege)); // time held в секундах
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x15);
		writeD(infos.size());
		for(final FortressInfo _info : infos)
		{
			writeD(_info.fort_id);
			writeS(_info.fort_owner);
			writeD(_info.fort_status);
			writeD(_info.fort_siege);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_15_EXSHOWFORTRESSINFO;
	}
}
