package l2n.game.network.serverpackets;

/**
 * Format:(ch)
 * 
 * @author Crion/kombat
 */
public class ExClosePartyRoom extends L2GameServerPacket
{
	public static final ExClosePartyRoom STATIC_PACKET = new ExClosePartyRoom();

	public ExClosePartyRoom()
	{}

	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x09);
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return "FE_09_ExClosePartyRoom";
	}
}
