package l2n.game.network.serverpackets;

import l2n.game.model.L2RecipeList;

import java.util.Collection;

public class RecipeBookItemList extends L2GameServerPacket
{
	private static final String _S__dc_RECIPEBOOKITEMLIST = "[S] dc RecipeBookItemList";
	private Collection<L2RecipeList> _recipes;

	private final boolean _isDwarvenCraft;
	private final int _CurMP;

	public RecipeBookItemList(final boolean isDwarvenCraft, final int CurMP)
	{
		_isDwarvenCraft = isDwarvenCraft;
		_CurMP = CurMP;
	}

	public void setRecipes(final Collection<L2RecipeList> recipeBook)
	{
		_recipes = recipeBook;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xdc); // Точно: назначение пакета

		writeD(_isDwarvenCraft ? 0x00 : 0x01); // Точно: 0 = Dwarven 1 = Common
		writeD(_CurMP); // Точно: текущее количество MP

		if(_recipes == null)
			writeD(0);
		else
		{
			writeD(_recipes.size()); // Точно: количество рецептов в книге

			for(final L2RecipeList recipe : _recipes)
			{
				writeD(recipe.getId()); // Точно: ID рецепта
				writeD(1); // Вероятно заглушка
			}
		}
	}

	@Override
	public String getType()
	{
		return _S__dc_RECIPEBOOKITEMLIST;
	}
}
