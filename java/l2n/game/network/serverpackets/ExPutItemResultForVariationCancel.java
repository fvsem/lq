package l2n.game.network.serverpackets;

public class ExPutItemResultForVariationCancel extends L2GameServerPacket
{
	private static final String _S__FE_57_EXPUTITEMRESULTFORVARIATIONCANCEL = "[S] FE:57 ExPutItemResultForVariationCancel";

	private final int _itemObjId;
	private final int _price;

	public ExPutItemResultForVariationCancel(final int itemObjId, final int price)
	{
		_itemObjId = itemObjId;
		_price = price;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x57);
		writeD(0x40A97712);
		writeD(_itemObjId);
		writeD(0x27);
		writeD(0x2006);
		writeQ(_price);
		writeD(0x01);
	}

	@Override
	public String getType()
	{
		return _S__FE_57_EXPUTITEMRESULTFORVARIATIONCANCEL;
	}
}
