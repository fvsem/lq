package l2n.game.network.serverpackets;

public class ExPutItemResultForVariationMake extends L2GameServerPacket
{
	private static final String _S__FE_53_EXPUTITEMRESULTFORVARIATIONMAKE = "[S] FE:53 ExPutItemResultForVariationMake";

	private final int _itemObjId;
	private final int _unk1;
	private final int _unk2;

	public ExPutItemResultForVariationMake(final int itemObjId)
	{
		_itemObjId = itemObjId;
		_unk1 = 1;
		_unk2 = 1;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x53);
		writeD(_itemObjId);
		writeD(_unk1);
		writeD(_unk2);
	}

	@Override
	public String getType()
	{
		return _S__FE_53_EXPUTITEMRESULTFORVARIATIONMAKE;
	}
}
