package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

public class StopRotation extends L2GameServerPacket
{
	private static final String _S__61_STOPROTATION = "[S] 61 StopRotation";
	private final int _charId;
	private final int _degree;
	private final int _speed;

	public StopRotation(final L2Character player, final int degree, final int speed)
	{
		_charId = player.getObjectId();
		_degree = degree;
		_speed = speed;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x61);
		writeD(_charId);
		writeD(_degree);
		writeD(_speed);
		writeC(0); // ?
	}

	@Override
	public String getType()
	{
		return _S__61_STOPROTATION;
	}
}
