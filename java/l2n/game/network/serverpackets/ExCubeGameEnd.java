package l2n.game.network.serverpackets;

/**
 * Format: (chd) ddd
 * d: Winner Team
 */
public class ExCubeGameEnd extends L2GameServerPacket
{
	private static final String ExCubeGameEnd = "[S] FE:98:01 ExCubeGameEnd";

	private final boolean _isRedTeamWin;

	/**
	 * Show Minigame Results
	 * 
	 * @param isRedTeamWin
	 *            : Is Red Team Winner?
	 */
	public ExCubeGameEnd(final boolean isRedTeamWin)
	{
		_isRedTeamWin = isRedTeamWin;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x98);
		writeD(0x01);

		writeD(_isRedTeamWin ? 0x01 : 0x00);
	}

	@Override
	public final String getType()
	{
		return ExCubeGameEnd;
	}
}
