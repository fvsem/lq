package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;

public class PledgeReceivePowerInfo extends L2GameServerPacket
{
	private static final String _S__FE_3D_PLEDGERECEIVEPOWERINFO = "[S] FE:3d PledgeReceivePowerInfo";
	private final int powerGrade;
	private int privs;
	private final String member_name;

	public PledgeReceivePowerInfo(final L2ClanMember member)
	{
		powerGrade = member.getPowerGrade();
		member_name = member.getName();
		if(member.isClanLeader())
			privs = L2Clan.CP_ALL;
		else
		{
			final L2Clan clan = member.getClan();
			if(clan == null)
				privs = 0;
			else
				privs = clan.getRankPrivsForPacket(powerGrade);
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x3d);
		writeD(powerGrade);
		writeS(member_name);
		writeD(privs);
	}

	@Override
	public String getType()
	{
		return _S__FE_3D_PLEDGERECEIVEPOWERINFO;
	}
}
