package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2NpcInstance;

public class NpcSay extends L2GameServerPacket
{
	private static final String _S__30_NPCSAY = "[S] 30 NpcSay";

	private final int _objId;
	private final int _type;
	private final int _id;
	private final String _text;

	public NpcSay(final L2NpcInstance npc, final int chatType, final String text)
	{
		_objId = npc.getObjectId();
		_type = chatType;
		_text = text;
		_id = 1000000 + npc.getNpcId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x30);
		writeD(_objId); // object_id отсылающего
		writeD(_type); // Тип чата, 0 = tell, 1 = shout, 2 = pm, 3 = party... Совпадает с Say2
		writeD(_id); // npc id от кого отправлен пакет, клиент получает по нему имя.
		writeS(_text); // текст для отправки.
	}

	@Override
	public String getType()
	{
		return _S__30_NPCSAY;
	}
}
