package l2n.game.network.serverpackets;

public class ExShowQuestInfo extends L2GameServerPacket
{
	private static final String _S__FE_20_EXSHOWQUESTINFO = "[S] FE:20 ExShowQuestInfo";

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x20);
	}

	@Override
	public String getType()
	{
		return _S__FE_20_EXSHOWQUESTINFO;
	}
}
