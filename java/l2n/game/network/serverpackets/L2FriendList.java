package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.util.logging.Level;

public class L2FriendList extends L2GameServerPacket
{
	private static final String _S__75_FRIENDLIST = "[S] 75 FriendList";
	private final GArray<FriendInfo> friends = new GArray<FriendInfo>();
	private final L2Player _cha;
	private boolean _message = false;
	private boolean _packet = false;

	public L2FriendList(final L2Player cha)
	{
		_cha = cha;
		_message = true;
		_packet = false;
		common();
	}

	public L2FriendList(final L2Player cha, final boolean sendMessage)
	{
		_cha = cha;
		_message = sendMessage;
		_packet = true;
		common();
	}

	private void common()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		FriendInfo friendinfo;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT friend_id, char_name FROM character_friends LEFT JOIN characters ON ( character_friends.friend_id = characters.obj_Id ) WHERE char_id=?");
			statement.setInt(1, _cha.getObjectId());
			rset = statement.executeQuery();

			if(_message)
				_cha.sendPacket(Msg._FRIENDS_LIST_);
			while (rset.next())
			{
				friendinfo = new FriendInfo(rset.getString("char_name"));
				final L2Player friend = L2ObjectsStorage.getPlayer(friendinfo.name);
				if(friend == null)
				{
					if(_message)
						_cha.sendPacket(new SystemMessage(SystemMessage.S1_CURRENTLY_OFFLINE).addString(friendinfo.name));
				}
				else
				{
					if(_message)
						_cha.sendPacket(new SystemMessage(SystemMessage.S1_CURRENTLY_ONLINE).addString(friendinfo.name));
					friendinfo.id = friend.getObjectId();
				}
				friends.add(friendinfo);
			}
			if(_message)
				_cha.sendPacket(Msg.__EQUALS__);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error in friendlist ", e);
			_packet = false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private class FriendInfo
	{
		public final String name;
		public int id = 0;

		public FriendInfo(final String _name)
		{
			name = _name;
		}
	}

	@Override
	protected final void writeImpl()
	{
		if(!_packet)
			return;

		writeC(0x75);
		writeD(friends.size());
		for(final FriendInfo friend : friends)
		{
			writeD(0);
			writeS(friend.name);
			writeD(friend.id > 0 ? 1 : 0); // online or offline
			writeD(friend.id); // object_id
		}
	}

	@Override
	public String getType()
	{
		return _S__75_FRIENDLIST;
	}
}
