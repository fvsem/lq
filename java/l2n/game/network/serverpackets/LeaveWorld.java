package l2n.game.network.serverpackets;

public class LeaveWorld extends L2GameServerPacket
{
	private static final String _S__84_LEAVEWORLD = "[S] 84 LeaveWorld";

	@Override
	protected final void writeImpl()
	{
		writeC(0x84);
	}

	@Override
	public String getType()
	{
		return _S__84_LEAVEWORLD;
	}
}
