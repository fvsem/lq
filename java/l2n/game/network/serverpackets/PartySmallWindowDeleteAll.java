package l2n.game.network.serverpackets;

public class PartySmallWindowDeleteAll extends L2GameServerPacket
{
	private static final String _S__50_PARTYSMALLWINDOWDELETEALL = "[S] 50 PartySmallWindowDeleteAll";

	@Override
	protected final void writeImpl()
	{
		writeC(0x50);
	}

	@Override
	public String getType()
	{
		return _S__50_PARTYSMALLWINDOWDELETEALL;
	}
}
