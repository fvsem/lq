package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.items.MailParcelController.Letter;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 23:36:04
 */
public class ExShowReceivedPostList extends L2GameServerPacket
{
	private static final String _S__FE_AA_EXSHOWRECEIVEDPOSTLIST = "[S] FE:AA ExShowReceivedPostList";

	private static final GArray<Letter> EMPTY_LETTERS = new GArray<Letter>(0);
	private GArray<Letter> letters;

	public ExShowReceivedPostList(final int objId)
	{
		letters = MailParcelController.getInstance().getReceived(objId);
		if(letters == null)
			letters = EMPTY_LETTERS;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xaa);
		writeD((int) (System.currentTimeMillis() / 1000));

		writeD(letters.size());
		for(final Letter letter : letters)
		{
			if(letter == null)
				continue;

			writeD(letter.id);
			writeS(letter.topic);
			writeS(letter.senderName);
			writeD(letter.type);
			writeD(letter.validtime);
			writeD(letter.unread);
			writeD(0x01);
			writeD(letter.attachments);
			writeD(0x00);
			writeD(letter.system);
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_AA_EXSHOWRECEIVEDPOSTLIST;
	}
}
