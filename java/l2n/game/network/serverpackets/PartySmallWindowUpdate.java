package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class PartySmallWindowUpdate extends L2GameServerPacket
{
	private static final String _S__52_PARTYSMALLWINDOWUPDATE = "[S] 52 PartySmallWindowUpdate";
	private final int obj_id, class_id, level;
	private final int curCp, maxCp, curHp, maxHp, curMp, maxMp;
	private final String obj_name;

	public PartySmallWindowUpdate(final L2Player member)
	{
		obj_id = member.getObjectId();
		obj_name = member.getName();
		curCp = (int) member.getCurrentCp();
		maxCp = member.getMaxCp();
		curHp = (int) member.getCurrentHp();
		maxHp = member.getMaxHp();
		curMp = (int) member.getCurrentMp();
		maxMp = member.getMaxMp();
		level = member.getLevel();
		class_id = member.getClassId().getId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x52);
		// dSdddddddd
		writeD(obj_id);
		writeS(obj_name);
		writeD(curCp);
		writeD(maxCp);
		writeD(curHp);
		writeD(maxHp);
		writeD(curMp);
		writeD(maxMp);
		writeD(level);
		writeD(class_id);
	}

	@Override
	public String getType()
	{
		return _S__52_PARTYSMALLWINDOWUPDATE;
	}
}
