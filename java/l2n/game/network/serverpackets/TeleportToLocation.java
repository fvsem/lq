package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.L2Object;
import l2n.util.Location;

/**
 * format dddd
 * sample
 * 0000: 3a 69 08 10 48 02 c1 00 00 f7 56 00 00 89 ea ff :i..H.....V.....
 * 0010: ff 0c b2 d8 61 ....a
 */
public class TeleportToLocation extends L2GameServerPacket
{
	private static final String _S__22_TELEPORTTOLOCATION = "[S] 22 TeleportToLocation";
	private final int _targetId;
	private final int _x;
	private final int _y;
	private final int _z;
	private final int _heading;

	public TeleportToLocation(final L2Object cha, final int x, final int y, final int z)
	{
		_targetId = cha.getObjectId();
		_x = x;
		_y = y;
		_z = z;
		_heading = cha.getHeading();
	}

	public TeleportToLocation(final L2Object cha, final Location loc)
	{
		_targetId = cha.getObjectId();
		_x = loc.getX();
		_y = loc.getY();
		_z = loc.getZ();
		_heading = loc.getHeading();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x22);
		writeD(_targetId);
		writeD(_x);
		writeD(_y);
		writeD(_z + Config.CLIENT_Z_SHIFT);
		writeD(0x00); // isValidation ??
		writeD(_heading); // nYaw
	}

	@Override
	public String getType()
	{
		return _S__22_TELEPORTTOLOCATION;
	}
}
