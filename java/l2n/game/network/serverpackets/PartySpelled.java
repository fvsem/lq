package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Effect;
import l2n.game.model.actor.L2Character;
import l2n.util.EffectsComparator;

import java.util.Arrays;

public class PartySpelled extends L2GameServerPacket
{
	private static final String _S__f4_PartySpelled = "[S] f4 PartySpelled";
	private int char_type;
	private int char_obj_id = 0;
	private GArray<int[]> _effects;

	public PartySpelled(final L2Character activeChar, final boolean full)
	{
		if(activeChar == null)
			return;

		char_obj_id = activeChar.getObjectId();
		char_type = activeChar.isPet() ? 1 : activeChar.isSummon() ? 2 : 0;
		// 0 - L2Player // 1 - петы // 2 - саммоны
		_effects = new GArray<int[]>();
		if(full)
		{
			final L2Effect[] effects = activeChar.getEffectList().getAllFirstEffects();
			Arrays.sort(effects, EffectsComparator.STATIC_INSTANCE);
			for(final L2Effect effect : effects)
				if(effect != null && effect.isInUse())
					effect.addPartySpelledIcon(this);
		}
	}

	public PartySpelled(final L2Character activeChar)
	{
		if(activeChar == null)
			return;

		char_obj_id = activeChar.getObjectId();
		char_type = activeChar.isPet() ? 1 : activeChar.isSummon() ? 2 : 0;
		_effects = new GArray<int[]>();
	}

	@Override
	protected final void writeImpl()
	{
		if(char_obj_id == 0)
			return;

		writeC(0xf4);
		writeD(char_type);
		writeD(char_obj_id);
		writeD(_effects.size());
		for(final int[] effect : _effects)
		{
			writeD(effect[0]);
			writeH(effect[1]);
			writeD(effect[2]);
		}
	}

	public void addPartySpelledEffect(final int skillId, final int level, final int duration)
	{
		_effects.add(new int[] { skillId, level, duration });
	}

	@Override
	public String getType()
	{
		return _S__f4_PartySpelled;
	}
}
