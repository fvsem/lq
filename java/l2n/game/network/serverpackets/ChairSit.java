package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * sample
 * format
 * d
 */
public class ChairSit extends L2GameServerPacket
{
	private static final String _S__ed_CHAIRSIT = "[S] ed ChairSit";

	private final L2Player _activeChar;
	private final int _staticObjectId;

	public ChairSit(final L2Player player, final int staticObjectId)
	{
		_activeChar = player;
		_staticObjectId = staticObjectId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xed);
		writeD(_activeChar.getObjectId());
		writeD(_staticObjectId);
	}

	@Override
	public String getType()
	{
		return _S__ed_CHAIRSIT;
	}
}
