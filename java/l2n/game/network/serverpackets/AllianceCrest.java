package l2n.game.network.serverpackets;

public class AllianceCrest extends L2GameServerPacket
{
	private static final String _S__af_ALLYCREST = "[S] af AllianceCrest";
	private final int _crestId;
	private byte[] _data;

	public AllianceCrest(final int crestId, final byte[] data)
	{
		_crestId = crestId;
		_data = data;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xaf);
		writeD(_crestId);
		writeD(_data.length);
		writeB(_data);
		_data = null;
	}

	@Override
	public String getType()
	{
		return _S__af_ALLYCREST;
	}
}
