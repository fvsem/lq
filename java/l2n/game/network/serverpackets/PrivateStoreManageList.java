package l2n.game.network.serverpackets;

import l2n.game.model.L2TradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.util.concurrent.ConcurrentLinkedQueue;

public class PrivateStoreManageList extends L2GameServerPacket
{
	private static final String _S__A0_PRIVATESTOREMANAGELISTSELL = "[S] A0 PrivateStoreManageListSell";
	private final int seller_id;
	private final long seller_adena;
	private boolean _package = false;
	private final ConcurrentLinkedQueue<TradeItem> _sellList;
	private final ConcurrentLinkedQueue<TradeItem> _haveList;

	/**
	 * Окно управления личным магазином покупки
	 * 
	 * @param seller
	 */
	public PrivateStoreManageList(final L2Player seller, final boolean pkg)
	{
		seller_id = seller.getObjectId();
		seller_adena = seller.getAdena();
		_package = pkg;

		// Проверяем список вещей в инвентаре, если вещь остутствует - убираем из списка продажи
		_sellList = new ConcurrentLinkedQueue<TradeItem>();
		L2ItemInstance inst;
		for(final TradeItem i : seller.getSellList())
		{
			inst = seller.getInventory().getItemByObjectId(i.getObjectId());
			if(i.getCount() <= 0 || inst == null || inst.isWear())
				continue;
			if(inst.getCount() < i.getCount())
				i.setCount(inst.getCount());
			_sellList.add(i);
		}

		final L2TradeList _list = new L2TradeList(0);
		// Строим список вещей, годных для продажи имеющихся в инвентаре
		for(final L2ItemInstance item : seller.getInventory().getItemsList())
			if(item != null && item.canBeTraded(seller) && item.getItemId() != L2Item.ITEM_ID_ADENA)
				_list.addItem(item);

		_haveList = new ConcurrentLinkedQueue<TradeItem>();

		// Делаем список для собственно передачи с учетом количества
		for(final L2ItemInstance item : _list.getItems())
		{
			final TradeItem ti = new TradeItem();
			ti.setObjectId(item.getObjectId());
			ti.setItemId(item.getItemId());
			ti.setCount(item.getCount());
			ti.setEnchantLevel(item.getEnchantLevel());
			ti.setCustomType2(item.getItem().getType2ForPackets());
			ti.setAttackElement(item.getAttackElement());
			ti.setDefenceFire(item.getDefenceFire());
			ti.setDefenceWater(item.getDefenceWater());
			ti.setDefenceWind(item.getDefenceWind());
			ti.setDefenceEarth(item.getDefenceEarth());
			ti.setDefenceHoly(item.getDefenceHoly());
			ti.setDefenceUnholy(item.getDefenceUnholy());
			_haveList.add(ti);
		}

		// Убираем совпадения между списками, в сумме оба списка должны совпадать с содержимым инвентаря
		if(_sellList.size() > 0)
			for(final TradeItem itemOnSell : _sellList)
			{
				_haveList.remove(itemOnSell);
				boolean added = false;
				for(final TradeItem itemInInv : _haveList)
					if(itemInInv.getObjectId() == itemOnSell.getObjectId())
					{
						added = true;
						itemOnSell.setCount(Math.min(itemOnSell.getCount(), itemInInv.getCount()));
						if(itemOnSell.getCount() == itemInInv.getCount())
							_haveList.remove(itemInInv);
						else if(itemOnSell.getCount() > 0)
							itemInInv.setCount(itemInInv.getCount() - itemOnSell.getCount());
						else
							_sellList.remove(itemOnSell);
						break;
					}
				if(!added)
					_sellList.remove(itemOnSell);
			}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xA0);
		// section 1
		writeD(seller_id);
		writeD(_package ? 1 : 0);
		writeQ(seller_adena);

		// Список имеющихся вещей
		writeD(_haveList.size());
		for(final TradeItem temp : _haveList)
		{
			final L2Item tempItem = ItemTable.getInstance().getTemplate(temp.getItemId());
			writeD(tempItem.getType2ForPackets());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeH(0);
			writeH(temp.getEnchantLevel());// enchant lvl
			writeH(temp.getCustomType2());
			writeD(tempItem.getBodyPart());
			writeQ(tempItem.getReferencePrice()); // store price

			// T1
			writeItemElements(temp);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}

		// Список вещей уже поставленых на продажу
		writeD(_sellList.size());
		for(final TradeItem temp2 : _sellList)
		{
			final L2Item tempItem = ItemTable.getInstance().getTemplate(temp2.getItemId());
			writeD(tempItem.getType2ForPackets());
			writeD(temp2.getObjectId());
			writeD(temp2.getItemId());
			writeQ(temp2.getCount());
			writeH(0);
			writeH(temp2.getEnchantLevel());// enchant lvl
			writeH(0);
			writeD(tempItem.getBodyPart());
			writeQ(temp2.getOwnersPrice());// your price
			writeQ(temp2.getStorePrice()); // store price

			writeItemElements(temp2);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__A0_PRIVATESTOREMANAGELISTSELL;
	}
}
