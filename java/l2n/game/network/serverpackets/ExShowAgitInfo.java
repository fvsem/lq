package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.model.L2Clan;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.tables.ClanTable;

public class ExShowAgitInfo extends L2GameServerPacket
{
	private static final String _S__FE_16_EXSHOWAGITINFO = "[S] FE:16 ExShowAgitInfo";
	private final GArray<AgitInfo> infos = new GArray<AgitInfo>();

	public ExShowAgitInfo()
	{
		String clan_name, leader_name;
		int ch_id, lease;

		for(final ClanHall clanHall : ClanHallManager.getInstance().getClanHalls().values())
		{
			ch_id = clanHall.getId();
			lease = clanHall.getLease() == 0 ? 1 : 0;

			final L2Clan clan = ClanTable.getInstance().getClan(clanHall.getOwnerId());
			clan_name = clanHall.getOwnerId() == 0 || clan == null ? "" : clan.getName();
			leader_name = clanHall.getOwnerId() == 0 || clan == null ? "" : clan.getLeader().getName();
			infos.add(new AgitInfo(clan_name, leader_name, ch_id, lease));
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x16);
		writeD(infos.size());
		for(final AgitInfo _info : infos)
		{
			writeD(_info.ch_id);
			writeS(_info.clan_name);
			writeS(_info.leader_name);
			writeD(_info.lease);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__FE_16_EXSHOWAGITINFO;
	}

	private static class AgitInfo
	{
		public String clan_name, leader_name;
		public int ch_id, lease;

		public AgitInfo(final String _clan_name, final String _leader_name, final int _ch_id, final int _lease)
		{
			clan_name = _clan_name;
			leader_name = _leader_name;
			ch_id = _ch_id;
			lease = _lease;
		}
	}
}
