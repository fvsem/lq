package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Summon;
import l2n.util.Location;

public class PetStatusUpdate extends L2GameServerPacket
{
	private static final String _S__b6_PETSTATUSSHOW = "[S] b6 PetStatusUpdate";
	private final int type, obj_id, level;
	private final int maxFed, curFed, maxHp, curHp, maxMp, curMp;
	private final long exp, exp_this_lvl, exp_next_lvl;
	private final Location _loc;
	private final String title;

	public PetStatusUpdate(final L2Summon summon)
	{
		type = summon.getSummonType();
		obj_id = summon.getObjectId();
		_loc = summon.getLoc();
		title = summon.getTitle();
		curHp = (int) summon.getCurrentHp();
		maxHp = summon.getMaxHp();
		curMp = (int) summon.getCurrentMp();
		maxMp = summon.getMaxMp();
		curFed = summon.getCurrentFed();
		maxFed = summon.getMaxFed();
		level = summon.getLevel();
		exp = summon.getExp();
		exp_this_lvl = summon.getExpForThisLevel();
		exp_next_lvl = summon.getExpForNextLevel();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xb6);
		writeD(type);
		writeD(obj_id);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeS(title);
		writeD(curFed);
		writeD(maxFed);
		writeD(curHp);
		writeD(maxHp);
		writeD(curMp);
		writeD(maxMp);
		writeD(level);
		writeQ(exp);
		writeQ(exp_this_lvl);// 0% absolute value
		writeQ(exp_next_lvl);// 100% absolute value
	}

	@Override
	public String getType()
	{
		return _S__b6_PETSTATUSSHOW;
	}
}
