package l2n.game.network.serverpackets;

/**
 * sample
 * <p>
 * 7d c1 b2 e0 4a 00 00 00 00
 * <p>
 * format cdd
 */
public class FriendAddRequest extends L2GameServerPacket
{
	private static final String _S__83_ASKJOINFRIEND = "[S] 83 AskJoinFriend";

	private final String _requestorName;

	public FriendAddRequest(final String requestorName)
	{
		_requestorName = requestorName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x83);
		writeS(_requestorName);
		writeD(0);
	}

	@Override
	public String getType()
	{
		return _S__83_ASKJOINFRIEND;
	}
}
