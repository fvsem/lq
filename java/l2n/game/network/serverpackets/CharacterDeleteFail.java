package l2n.game.network.serverpackets;

public class CharacterDeleteFail extends L2GameServerPacket
{
	private static final String _S__1e_CHARDELETEFAIL = "[S] 1e CharDeleteFail";

	public static int REASON_DELETION_FAILED = 0x01;
	public static int REASON_YOU_MAY_NOT_DELETE_CLAN_MEMBER = 0x02;
	public static int REASON_CLAN_LEADERS_MAY_NOT_BE_DELETED = 0x03;
	int _error;

	public CharacterDeleteFail(final int error)
	{
		_error = error;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x1e);
		writeD(_error);
	}

	@Override
	public String getType()
	{
		return _S__1e_CHARDELETEFAIL;
	}
}
