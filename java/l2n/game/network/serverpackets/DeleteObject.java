package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

/**
 * Пример:
 * 08
 * a5 04 31 48 ObjectId
 * 00 00 00 7c unk
 * format d
 */
public class DeleteObject extends L2GameServerPacket
{
	private static final String _S__08_DELETEOBJECT = "[S] 08 DeleteObject";
	private final int _objectId;

	public DeleteObject(final L2Object obj)
	{
		_objectId = obj.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		if(_objectId == 0)
			return;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || activeChar.getObjectId() == _objectId)
			return;

		writeC(0x08);
		writeD(_objectId);
		writeD(0x00); // unknown
	}

	@Override
	public String getType()
	{
		return _S__08_DELETEOBJECT + " " + L2ObjectsStorage.findObject(_objectId) + " (" + _objectId + ")";
	}
}
