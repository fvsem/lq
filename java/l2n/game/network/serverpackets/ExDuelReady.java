package l2n.game.network.serverpackets;

public class ExDuelReady extends L2GameServerPacket
{
	private static final String _S__FE_4D_EXDUELREADY = "[S] FE:4d ExDuelReady";

	private final int _unk1;

	public ExDuelReady(final int unk1)
	{
		_unk1 = unk1;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x4d);

		writeD(_unk1);
	}

	@Override
	public String getType()
	{
		return _S__FE_4D_EXDUELREADY;
	}
}
