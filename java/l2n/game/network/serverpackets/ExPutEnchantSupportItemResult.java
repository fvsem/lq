package l2n.game.network.serverpackets;

public class ExPutEnchantSupportItemResult extends L2GameServerPacket
{
	private static final String _S__87_EXPUTENCHANTSUPPORTITEMRESULT = "[S] 87 ExPutEnchantSupportItemResult";

	public static final ExPutEnchantSupportItemResult FAIL = new ExPutEnchantSupportItemResult(0);
	public static final ExPutEnchantSupportItemResult SACCESS = new ExPutEnchantSupportItemResult(1);

	private final int _result;

	/**
	 * 
	 */
	public ExPutEnchantSupportItemResult(final int result)
	{
		_result = result;
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#getType()
	 */
	@Override
	public String getType()
	{
		return _S__87_EXPUTENCHANTSUPPORTITEMRESULT;
	}

	/**
	 * @see l2n.game.network.serverpackets.L2GameServerPacket#writeImpl()
	 */
	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x82);
		writeD(_result);
	}
}
