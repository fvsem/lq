package l2n.game.network.serverpackets;

public class ExRedSky extends L2GameServerPacket
{
	private static final String _S__FE_41_EXREDSKY = "[S] FE:41 ExRedSky";
	private final int _duration;

	/**
	 * 0xfe:0x40 ExRedSky d
	 * 
	 * @param _characters
	 */
	public ExRedSky(final int duration/* , int type, int v3, int v4, int v5 */)
	{
		_duration = duration;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x41); // sub id
		writeD(_duration);
	}

	@Override
	public String getType()
	{
		return _S__FE_41_EXREDSKY;
	}
}
