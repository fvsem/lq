package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.model.instances.L2CubicInstance;
import l2n.game.model.items.Inventory;
import l2n.util.Colors;
import l2n.util.Location;

public class CharInfo extends L2GameServerPacket
{
	private static final String _S__31_CHARINFO = "[S] 31 CharInfo";

	private L2Player _cha;
	private Inventory _inv;
	private float _moveMultiplier, speed_move, speed_atack, col_radius, col_height;
	private Location _fishLoc;
	private String _name, _title;
	private int _x, _y, _z, _heading, _mAtkSpd, _pAtkSpd, _runSpd, _walkSpd, _swimSpd, _flRunSpd, _flWalkSpd, _flyRunSpd, _flyWalkSpd, _objId, _race, _sex, base_class, pvp_flag, karma, rec_have,
			hair_style, hair_color, face, _abnormalEffect, _abnormalEffect2, clan_id, clan_crest_id, large_clan_crest_id, ally_id, ally_crest_id, class_id, _sit, _run, _combat, _dead, private_store,
			_enchant, _noble, _hero, _fishing, mount_type, plg_class, pledge_type, clan_rep_score, cw_level, mount_id, _nameColor, _title_color, _transform, _agathion, _territoryId, _vehicleId,
			_airShipHelm;
	private L2CubicInstance[] cubics;
	private boolean can_writeImpl = false, partyRoom = false, isFlying = false;

	public static final byte[] PAPERDOLL_ORDER = {
			Inventory.PAPERDOLL_UNDER,
			Inventory.PAPERDOLL_HEAD,
			Inventory.PAPERDOLL_RHAND,
			Inventory.PAPERDOLL_LHAND,
			Inventory.PAPERDOLL_GLOVES,
			Inventory.PAPERDOLL_CHEST,
			Inventory.PAPERDOLL_LEGS,
			Inventory.PAPERDOLL_FEET,
			Inventory.PAPERDOLL_BACK,
			Inventory.PAPERDOLL_LRHAND,
			Inventory.PAPERDOLL_HAIR,
			Inventory.PAPERDOLL_DHAIR,
			Inventory.PAPERDOLL_RBRACELET,
			Inventory.PAPERDOLL_LBRACELET,
			Inventory.PAPERDOLL_DECO1,
			Inventory.PAPERDOLL_DECO2,
			Inventory.PAPERDOLL_DECO3,
			Inventory.PAPERDOLL_DECO4,
			Inventory.PAPERDOLL_DECO5,
			Inventory.PAPERDOLL_DECO6,
			Inventory.PAPERDOLL_BELT };

	public CharInfo(final L2Player cha)
	{
		if((_cha = cha) == null || _cha.isInvisible() || _cha.isDeleting())
			return;

		if(_cha.isCursedWeaponEquipped())
		{
			_name = _cha.getVisName();
			_title = _cha.getVisTitle();
			clan_id = 0;
			clan_crest_id = 0;
			ally_id = 0;
			ally_crest_id = 0;
			large_clan_crest_id = 0;
			cw_level = CursedWeaponsManager.getInstance().getLevel(_cha.getCursedWeaponEquippedId());
		}
		else
		{
			_name = _cha.getVisName();
			_title = _cha.getVisTitle();
			clan_id = _cha.getClanId();
			clan_crest_id = _cha.getClanCrestId();
			ally_id = _cha.getAllyId();
			ally_crest_id = _cha.getAllyCrestId();
			large_clan_crest_id = _cha.getClanCrestLargeId();
			cw_level = 0;
		}

		_nameColor = _cha.getNameColor(); // New C5
		_title_color = Config.TITLE_PVP_MODE ? Colors.getColor(_cha.getPvpKills()) : _cha.getTitleColor();

		if(!_cha.isConnected() && !Config.PLAYER_DISCONNECT_TITILE.isEmpty() && !_cha.isInOfflineMode())
		{
			_title = Config.PLAYER_DISCONNECT_TITILE;
			_title_color = 255;
		}

		if(_cha.isMounted())
		{
			_enchant = 0;
			mount_id = _cha.getMountNpcId() + 1000000;
			mount_type = _cha.getMountType();
		}
		else
		{
			_enchant = _cha.getEnchantEffect();
			mount_id = 0;
			mount_type = 0;
		}

		_inv = _cha.getInventory();
		_mAtkSpd = _cha.getMAtkSpd();
		_pAtkSpd = _cha.getPAtkSpd();
		_moveMultiplier = _cha.getMovementSpeedMultiplier();
		_runSpd = (int) (_cha.getRunSpeed() / _moveMultiplier);
		_walkSpd = (int) (_cha.getWalkSpeed() / _moveMultiplier);

		_flRunSpd = 0;
		_flWalkSpd = 0;

		if(_cha.isFlying())
		{
			_flyRunSpd = _runSpd;
			_flyWalkSpd = _walkSpd;
		}
		else
		{
			_flyRunSpd = 0;
			_flyWalkSpd = 0;
		}

		_swimSpd = _cha.getSwimSpeed();

		_x = _cha.getX();
		_y = _cha.getY();
		_z = _cha.getZ();
		_heading = _cha.getHeading();

		if(_cha.getVehicle() != null && _cha.getInVehiclePosition() != null)
		{
			final L2Vehicle vehicle = _cha.getVehicle();
			_vehicleId = vehicle.getObjectId();
			if(vehicle.isClanAirShip() && vehicle.getDriver() == _cha)
				_airShipHelm = L2AirShip.HELM;
			else
				_airShipHelm = 0x00;
		}
		else
		{
			_vehicleId = 0x00;
			_airShipHelm = 0x00;
		}

		_objId = _cha.getObjectId();
		_race = _cha.getBaseTemplate().race.ordinal();
		_sex = _cha.getSex();
		base_class = _cha.getBaseClassId();
		pvp_flag = _cha.getPvpFlag();
		karma = _cha.getKarma();
		speed_move = _cha.getMovementSpeedMultiplier();
		speed_atack = _cha.getAttackSpeedMultiplier();
		col_radius = _cha.getColRadius();
		col_height = _cha.getColHeight();
		hair_style = _cha.getHairStyle();
		hair_color = _cha.getHairColor();
		face = _cha.getFace();
		if(clan_id > 0 && _cha.getClan() != null)
			clan_rep_score = _cha.getClan().getReputationScore();
		else
			clan_rep_score = 0;
		_sit = _cha.isSitting() ? 0 : 1; // standing = 1 sitting = 0
		_run = _cha.isRunning() ? 1 : 0; // running = 1 walking = 0
		_combat = _cha.isInCombat() ? 1 : 0;
		_dead = _cha.isAlikeDead() ? 1 : 0;

		private_store = _cha.getPrivateStoreType(); // 1 - sellshop
		cubics = _cha.getCubics().toArray(new L2CubicInstance[0]);
		_abnormalEffect = _cha.getAbnormalEffect();
		_abnormalEffect2 = _cha.getSpecialEffect();
		rec_have = _cha.isGM() ? 0 : _cha.getRecomHave();
		class_id = _cha.getClassId().getId();

		_noble = _cha.isNoble() ? 1 : 0; // 0x01: symbol on char menu ctrl+I
		_hero = _cha.isHero() || _cha.isGM() && Config.GM_HERO_AURA ? 1 : 0; // 0x01: Hero Aura
		_fishing = _cha.isFishing() ? 1 : 0;
		_fishLoc = _cha.getFishLoc();
		plg_class = _cha.getPledgeClass();
		pledge_type = _cha.getPledgeType();
		_transform = _cha.getTransformationId();
		_agathion = _cha.getAgathion() != null ? _cha.getAgathion().getId() : 0;
		partyRoom = PartyRoomManager.getInstance().isLeader(_cha);
		isFlying = _cha.isInFlyingTransform();
		_territoryId = _cha.getTerritorySiege();

		can_writeImpl = true;
	}

	// dddddSddddddddddddddddddddddddhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhdddddddddddddddffffdddSddddccccccch
	@Override
	protected final void writeImpl()
	{
		if(!can_writeImpl)
			return;
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(activeChar.equals(_cha))
		{
			_log.severe("You cant send CharInfo about his character to active user!!!");
			Thread.dumpStack();
			return;
		}

		writeC(0x31);
		writeD(_x);
		writeD(_y);
		writeD(_z + Config.CLIENT_Z_SHIFT);
		writeD(_vehicleId);
		writeD(_objId);
		writeS(_name);
		writeD(_race);
		writeD(_sex);
		writeD(base_class);

		if(_airShipHelm == 0)
		{
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				writeD(_inv.getPaperdollItemId(PAPERDOLL_ID));
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				writeD(_inv.getPaperdollAugmentationId(PAPERDOLL_ID));
		}
		else
		{
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				if(PAPERDOLL_ID == Inventory.PAPERDOLL_RHAND)
					writeD(_airShipHelm);
				else if(PAPERDOLL_ID == Inventory.PAPERDOLL_LHAND)
					writeD(0x00);
				else
					writeD(_inv.getPaperdollItemId(PAPERDOLL_ID));
			for(final byte PAPERDOLL_ID : PAPERDOLL_ORDER)
				if(PAPERDOLL_ID == Inventory.PAPERDOLL_RHAND)
					writeD(_airShipHelm);
				else if(PAPERDOLL_ID == Inventory.PAPERDOLL_LHAND)
					writeD(0x00);
				else
					writeD(_inv.getPaperdollAugmentationId(PAPERDOLL_ID));
		}

		writeD(0x01); // ? GraciaFinal
		writeD(0x00); // ? GraciaFinal

		writeD(pvp_flag);
		writeD(karma);

		writeD(_mAtkSpd);
		writeD(_pAtkSpd);

		writeD(pvp_flag);
		writeD(karma);

		writeD(_runSpd); // TODO: the order of the speeds should be confirmed
		writeD(_walkSpd);
		writeD(_swimSpd/* 0x32 */); // swimspeed
		writeD(_swimSpd/* 0x32 */); // swimspeed
		writeD(_flRunSpd);
		writeD(_flWalkSpd);
		writeD(_flyRunSpd);
		writeD(_flyWalkSpd);
		writeF(speed_move); // _cha.getProperMultiplier()
		writeF(speed_atack); // _cha.getAttackSpeedMultiplier()
		writeF(col_radius);
		writeF(col_height);
		writeD(hair_style);
		writeD(hair_color);
		writeD(face);
		writeS(_title);
		writeD(clan_id);
		writeD(clan_crest_id);
		writeD(ally_id);
		writeD(ally_crest_id);

		writeD(0);

		writeC(_sit);
		writeC(_run);
		writeC(_combat);
		writeC(_dead);
		writeC(0);
		writeC(mount_type); // 1-on Strider, 2-on Wyvern, 3-on Great Wolf, 0-no mount
		writeC(private_store);
		writeH(cubics.length);
		for(final L2CubicInstance cubic : cubics)
			writeH(cubic == null ? 0 : cubic.getId());
		writeC(partyRoom ? 1 : 0);
		writeD(_abnormalEffect);
		writeC(isFlying ? 2 : 0);
		writeH(rec_have);
		writeD(mount_id);
		writeD(class_id);
		writeD(0); // ?
		writeC(_enchant);

		// team circle around feet 1 = Blue, 2 = red
		if(_cha.getTeam() < 3)
			writeC(_cha.getTeam());
		else if(activeChar.getTeam() == 0)
			writeC(0);
		else
			writeC(activeChar.getTeam() == _cha.getTeam() ? 1 : 2);
		writeD(large_clan_crest_id);
		writeC(_noble);
		writeC(_hero);

		writeC(_fishing);
		writeD(_fishLoc.x);
		writeD(_fishLoc.y);
		writeD(_fishLoc.z);

		writeD(_nameColor);
		writeD(_heading);
		writeD(plg_class);
		writeD(pledge_type);
		writeD(_title_color);
		writeD(cw_level);
		writeD(clan_rep_score);
		writeD(_transform);
		writeD(_agathion);

		writeD(0x01);

		// T2.3
		writeD(_abnormalEffect2);
		writeD(_territoryId > 0 ? 80 + _territoryId : 0);
		writeD(0x00);
		writeD(_territoryId > 0 ? 80 + _territoryId : 0);
	}

	@Override
	public String getType()
	{
		return _S__31_CHARINFO + ": " + _cha;
	}
}
