package l2n.game.network.serverpackets;

/**
 * format d rev 417
 */
public class LoginFail extends L2GameServerPacket
{
	private static final String _S__0a_AUTHLOGINFAIL = "[S] 0a AuthLoginFail";
	public static int NO_TEXT = 0;
	public static int SYSTEM_ERROR_LOGIN_LATER = 1;
	public static int PASSWORD_DOES_NOT_MATCH_THIS_ACCOUNT = 2;
	public static int PASSWORD_DOES_NOT_MATCH_THIS_ACCOUNT2 = 3;
	public static int ACCESS_FAILED_TRY_LATER = 4;
	public static int INCORRECT_ACCOUNT_INFO_CONTACT_CUSTOMER_SUPPORT = 5;
	public static int ACCESS_FAILED_TRY_LATER2 = 6;
	public static int ACOUNT_ALREADY_IN_USE = 7;
	public static int ACCESS_FAILED_TRY_LATER3 = 8;
	public static int ACCESS_FAILED_TRY_LATER4 = 9;
	public static int ACCESS_FAILED_TRY_LATER5 = 10;

	private final int _reason;

	public LoginFail(final int reason)
	{
		_reason = reason;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x0a);
		writeD(_reason);
	}

	@Override
	public String getType()
	{
		return _S__0a_AUTHLOGINFAIL;
	}
}
