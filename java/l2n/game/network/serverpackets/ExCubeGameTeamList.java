package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;

/**
 * Format: (chd) ddd[dS]d[dS]
 * d: unknown
 * d: always -1
 * d: blue players number
 * [
 * d: player object id
 * S: player name
 * ]
 * d: blue players number
 * [
 * d: player object id
 * S: player name
 * ]
 */
public class ExCubeGameTeamList extends L2GameServerPacket
{
	// Players Lists
	private final GArray<L2Player> _bluePlayers;
	private final GArray<L2Player> _redPlayers;

	// Common Values
	private final int _roomNumber;

	/**
	 * Show Minigame Waiting List to Player
	 * 
	 * @param list
	 *            Red Players list
	 * @param list2
	 *            Blue Players list
	 * @param roomNumber
	 *            Arena/Room ID
	 */
	public ExCubeGameTeamList(final GArray<L2Player> list, final GArray<L2Player> list2, final int roomNumber)
	{
		_redPlayers = list;
		_bluePlayers = list2;
		_roomNumber = roomNumber - 1;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x00);

		writeD(_roomNumber);
		writeD(0xffffffff);

		writeD(_bluePlayers.size());
		for(final L2Player player : _bluePlayers)
		{
			writeD(player.getObjectId());
			writeS(player.getName());
		}

		writeD(_redPlayers.size());
		for(final L2Player player : _redPlayers)
		{
			writeD(player.getObjectId());
			writeS(player.getName());
		}
	}

	@Override
	public final String getType()
	{
		return "[S] FE:97:00 ExCubeGameTeamList";
	}
}
