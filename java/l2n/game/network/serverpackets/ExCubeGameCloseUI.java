package l2n.game.network.serverpackets;

/**
 * Format: (chd)
 */
public class ExCubeGameCloseUI extends L2GameServerPacket
{
	private static final String _S__FE_97_FFFFFFFF_EXCUBEGAMECLOSEUI = "[S] FE:97:FFFFFFFF ExCubeGameCloseUI";
	private int _seconds;

	/**
	 * Close Minigame Waiting List
	 */
	public ExCubeGameCloseUI()
	{}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0xffffffff);
	}

	@Override
	public String getType()
	{
		return _S__FE_97_FFFFFFFF_EXCUBEGAMECLOSEUI;
	}
}
