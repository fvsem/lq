package l2n.game.network.serverpackets;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;

public class MagicSkillLaunched extends L2GameServerPacket
{
	private static final String _S__54_MAGICSKILLLAUNCHED = "[S] 54 MagicSkillLaunched";
	private final int _casterId, _skillId, _skillLevel;
	private final boolean _isOffensive;
	private L2Object[] _targets;
	private final int _numberOfTargets;
	private int _singleTargetId = 0;

	public boolean isOffensive()
	{
		return _isOffensive;
	}

	public MagicSkillLaunched(final int casterId, final int skillId, final int skillLevel, final boolean isOffensive, final L2Character... targets)
	{
		_casterId = casterId;
		_skillId = skillId;
		_skillLevel = skillLevel;
		_targets = targets;
		_isOffensive = isOffensive;

		_numberOfTargets = targets.length;
		if(_numberOfTargets == 0)
			_singleTargetId = casterId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x54);
		writeD(_casterId);
		writeD(_skillId);
		writeD(_skillLevel);
		writeD(_numberOfTargets); // also failed or not?
		if(_singleTargetId != 0 || _numberOfTargets == 0)
			writeD(_singleTargetId);
		else
			for(final L2Object target : _targets)
				writeD(target.getObjectId());
	}

	@Override
	public String getType()
	{
		return _S__54_MAGICSKILLLAUNCHED;
	}
}
