package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 15.02.2011
 * @time 0:25:10
 */
public class NetPing extends L2GameServerPacket
{
	private final int _objectId;

	public NetPing(final int objectId)
	{
		_objectId = objectId;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xd9);
		writeD(_objectId);
	}

	@Override
	public String getType()
	{
		return "NetPing";
	}
}
