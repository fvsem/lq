package l2n.game.network.serverpackets;

public class ExPutIntensiveResultForVariationMake extends L2GameServerPacket
{
	private static final String _S__FE_54_EXPUTINTENSIVERESULTFORVARIATIONMAKE = "[S] FE:54 ExPutIntensiveResultForVariationMake";

	private final int _refinerItemObjId;
	private final int _lifestoneItemId;
	private final int _gemstoneItemId;
	private final int _gemstoneCount;
	private final int _unk2;

	public ExPutIntensiveResultForVariationMake(final int refinerItemObjId, final int lifeStoneId, final int gemstoneItemId, final int gemstoneCount)
	{
		_refinerItemObjId = refinerItemObjId;
		_lifestoneItemId = lifeStoneId;
		_gemstoneItemId = gemstoneItemId;
		_gemstoneCount = gemstoneCount;
		_unk2 = 1;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x54);
		writeD(_refinerItemObjId);
		writeD(_lifestoneItemId);
		writeD(_gemstoneItemId);
		writeQ(_gemstoneCount);
		writeD(_unk2);
	}

	@Override
	public String getType()
	{
		return _S__FE_54_EXPUTINTENSIVERESULTFORVARIATIONMAKE;
	}
}
