package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Clan;

public class PledgeReceiveWarList extends L2GameServerPacket
{
	private static final String _S__FE_3F_PLEDGERECEIVEWARLIST = "[S] FE:3f PledgeReceiveWarList";
	private final GArray<WarInfo> infos = new GArray<WarInfo>();
	private static int _updateType;
	@SuppressWarnings("unused")
	private static int _page;

	public PledgeReceiveWarList(final L2Clan clan, final int type, final int page)
	{
		_updateType = type;
		_page = page;
		infos.clear();
		final GArray<L2Clan> clans = _updateType == 1 ? clan.getAttackerClans() : clan.getEnemyClans();
		for(final L2Clan _clan : clans)
		{
			if(_clan == null)
				continue;
			infos.add(new WarInfo(_clan.getName(), _updateType, 0));
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x3f);
		writeD(_updateType); // which type of war list sould be revamped by this packet
		writeD(0x00); // page number goes here(_page ), made it static cuz not sure how many war to add to one page so TODO here
		writeD(infos.size());
		for(final WarInfo _info : infos)
		{
			writeS(_info.clan_name);
			writeD(_info.unk1);
			writeD(_info.unk2); // filler ??
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_3F_PLEDGERECEIVEWARLIST;
	}

	static class WarInfo
	{
		public String clan_name;
		public int unk1, unk2;

		public WarInfo(final String _clan_name, final int _unk1, final int _unk2)
		{
			clan_name = _clan_name;
			unk1 = _unk1;
			unk2 = _unk2;
		}
	}
}
