package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.tables.SkillTreeTable;

public class ExEnchantSkillInfo extends L2GameServerPacket
{
	private static final String _S__FE_2A_EXENCHANTSKILLINFO = "[S] FE:2a ExEnchantSkillInfo";

	private final GArray<Integer> _routes;
	private final int _id;
	private final int _level;
	private int _canAdd;
	private int canDecrease;

	public ExEnchantSkillInfo(final int id, final int level)
	{
		_routes = new GArray<Integer>();
		_id = id;
		_level = level;

		// skill already enchanted?
		if(_level > 100)
		{
			canDecrease = 1;

			final L2EnchantSkillLearn esd = SkillTreeTable.getInstance().getSkillEnchant(_id, _level + 1);
			if(esd != null)
			{
				addEnchantSkillDetail(esd.getLevel());
				_canAdd = 1;
			}

			for(final L2EnchantSkillLearn el : SkillTreeTable.getInstance().getEnchantsForChange(_id, _level))
				addEnchantSkillDetail(el.getLevel());
		}
		else
			for(final L2EnchantSkillLearn esd : SkillTreeTable.getInstance().getFirstEnchantsForSkill(_id))
			{
				addEnchantSkillDetail(esd.getLevel());
				_canAdd = 1;
			}
	}

	public void addEnchantSkillDetail(final int level)
	{
		_routes.add(level);
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x2a);

		writeD(_id);
		writeD(_level);
		writeD(_canAdd);
		writeD(canDecrease);

		writeD(_routes.size());
		for(final int route : _routes)
			writeD(route);
	}

	@Override
	public String getType()
	{
		return _S__FE_2A_EXENCHANTSKILLINFO;
	}
}
