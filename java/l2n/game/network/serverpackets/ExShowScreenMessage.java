package l2n.game.network.serverpackets;

public class ExShowScreenMessage extends L2GameServerPacket
{
	public static enum ScreenMessageAlign
	{
		TOP_LEFT,
		TOP_CENTER,
		TOP_RIGHT,
		MIDDLE_LEFT,
		MIDDLE_CENTER,
		MIDDLE_RIGHT,
		BOTTOM_CENTER,
		BOTTOM_RIGHT,
		UNDER_BOTTOM_CENTER
	}

	private static final String _S__FE_39_ExShowScreenMessage = "[S] FE:39 ExShowScreenMessage";
	private final int _type;
	private final int _sysMessageId;
	private final boolean _big_font;
	private final boolean _effect;
	private final ScreenMessageAlign _text_align;
	private final int _time;
	private final String _text;
	private int _unk1 = 0;
	private int _unk2 = 0;
	private int _unk3 = 0;
	private int _unk4 = 1;

	public ExShowScreenMessage(final String text, final int time, final ScreenMessageAlign text_align, final boolean big_font)
	{
		_type = 1;
		_sysMessageId = -1;
		_text = text;
		_time = time;
		_text_align = text_align;
		_big_font = big_font;
		_effect = false;
	}

	public ExShowScreenMessage(final String text, final int time, final ScreenMessageAlign text_align, final boolean big_font, final boolean showEffect)
	{
		_type = 1;
		_sysMessageId = -1;
		_text = text;
		_time = time;
		_text_align = text_align;
		_big_font = big_font;
		_effect = showEffect;
	}

	public ExShowScreenMessage(final int messageId, final int time, final ScreenMessageAlign text_align, final boolean big_font)
	{
		_type = 0;
		_sysMessageId = messageId;
		_text = "";
		_time = time;
		_text_align = text_align;
		_big_font = big_font;
		_effect = false;
	}

	public ExShowScreenMessage(final String text, final int time, final ScreenMessageAlign text_align)
	{
		this(text, time, text_align, true);
	}

	public ExShowScreenMessage(final String text, final int time)
	{
		this(text, time, ScreenMessageAlign.MIDDLE_CENTER);
	}

	public ExShowScreenMessage(final String text, final int time, final ScreenMessageAlign text_align, final boolean big_font, final int type, final int messageId, final boolean showEffect)
	{
		_type = type;
		_sysMessageId = messageId;
		_text = text;
		_time = time;
		_text_align = text_align;
		_big_font = big_font;
		_effect = showEffect;
	}

	public ExShowScreenMessage(final String text, final int time, final ScreenMessageAlign text_align, final boolean big_font, final int type, final int messageId, final boolean showEffect, final int unk1, final int unk2, final int unk3, final int unk4)
	{
		this(text, time, text_align, big_font, type, messageId, showEffect);
		_unk1 = unk1;
		_unk2 = unk2;
		_unk3 = unk3;
		_unk4 = unk4;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x39);

		writeD(_type); // 0 - system messages, 1 - your defined text
		writeD(_sysMessageId); // system message id (_type must be 0 otherwise no effect)
		writeD(_text_align.ordinal() + 1); // размещение текста
		writeD(_unk1);
		writeD(_big_font ? 0 : 1); // font size 0 - normal, 1 - small
		writeD(_unk2);
		writeD(_unk3);
		writeD(_effect == true ? 1 : 0); // upper effect (0 - disabled, 1 enabled) - _position must be 2 (center) otherwise no effect
		writeD(_time); // время отображения сообщения в милисекундах
		writeD(_unk4);
		writeS(_text); // your text (_type must be 1, otherwise no effect)
	}

	@Override
	public String getType()
	{
		return _S__FE_39_ExShowScreenMessage;
	}
}
