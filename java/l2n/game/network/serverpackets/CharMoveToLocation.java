package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.game.model.actor.L2Character;
import l2n.util.Location;

/**
 * 0000: 01 7a 73 10 4c b2 0b 00 00 a3 fc 00 00 e8 f1 ff .zs.L...........
 * 0010: ff bd 0b 00 00 b3 fc 00 00 e8 f1 ff ff .............
 * ddddddd
 */
public class CharMoveToLocation extends L2GameServerPacket
{
	private static final String _S__2f_CHARMOVETOLOCATION = "[S] 2f CharMoveToLocation";

	private final int _objectId;
	private int _client_z_shift;
	private final Location _current;
	private final Location _destination;

	public CharMoveToLocation(final L2Character cha)
	{
		_objectId = cha.getObjectId();
		_current = cha.getLoc();
		_destination = cha.getDestination();
		_client_z_shift = cha.isFlying() || cha.isSwimming() ? 0 : Config.CLIENT_Z_SHIFT;
	}

	public CharMoveToLocation(final int objectId, final Location from, final Location to)
	{
		_objectId = objectId;
		_current = from;
		_destination = to;
	}

	@Override
	protected final void writeImpl()
	{
		if(_destination == null)
			return;

		writeC(0x2f);
		writeD(_objectId);

		writeD(_destination.x);
		writeD(_destination.y);
		writeD(_destination.z + _client_z_shift);

		writeD(_current.x);
		writeD(_current.y);
		writeD(_current.z + _client_z_shift);
	}

	@Override
	public String getType()
	{
		return _S__2f_CHARMOVETOLOCATION;
	}
}
