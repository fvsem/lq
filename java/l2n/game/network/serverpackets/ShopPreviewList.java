package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.model.TradeItem;
import l2n.game.templates.L2Item;

public class ShopPreviewList extends L2GameServerPacket
{
	private static final String _S__F5_SHOPPREVIEWLIST = "[S] F5 ShopPreviewList";
	private final int _listId;
	private final TradeItem[] _list;
	private final long _money;
	private final int _expertise;

	public ShopPreviewList(final NpcTradeList list, final long currentMoney, final int expertiseIndex)
	{
		_listId = list.getListId();
		final GArray<TradeItem> lst = list.getItems();
		_list = lst.toArray(new TradeItem[lst.size()]);
		_money = currentMoney;
		_expertise = expertiseIndex;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xf5);
		writeC(0xc0); // ? по структуре тут идет writeD
		writeC(0x13); // ?
		writeC(0x00); // ?
		writeC(0x00); // ?
		writeQ(_money); // current money // TODO: ShopPreviewList
		writeD(_listId);

		int newlength = 0;
		for(final TradeItem item : _list)
			if(item.getItem().getCrystalType().ordinal() <= _expertise && item.getItem().isEquipable())
				newlength++;
		writeH(newlength);

		for(final TradeItem item : _list)
			if(item.getItem().getCrystalType().ordinal() <= _expertise && item.getItem().isEquipable())
			{
				writeD(item.getItemId());
				writeH(item.getItem().getType2ForPackets()); // item type2
				writeH(item.getItem().getType1() == L2Item.TYPE1_ITEM_QUESTITEM_ADENA ? 0x00 : item.getItem().getBodyPart());
				writeD(10); // у сф writeCompQ(Config.WEAR_PRICE);
			}
	}

	@Override
	public String getType()
	{
		return _S__F5_SHOPPREVIEWLIST;
	}
}
