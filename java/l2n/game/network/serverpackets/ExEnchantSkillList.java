package l2n.game.network.serverpackets;

import java.util.ArrayList;
import java.util.List;

public class ExEnchantSkillList extends L2GameServerPacket
{
	private static final String _S__FE_29_EXENCHANTSKILLLIST = "[S] FE:29 ExEnchantSkillList";

	public enum EnchantSkillType
	{
		NORMAL,
		SAFE,
		UNTRAIN,
		CHANGE_ROUTE,
	}

	private final List<Skill> _skills;
	private final EnchantSkillType _type;

	class Skill
	{
		public int id;
		public int level;

		Skill(final int pId, final int nextLevel)
		{
			id = pId;
			level = nextLevel;
		}
	}

	public void addSkill(final int id, final int level)
	{
		_skills.add(new Skill(id, level));
	}

	public ExEnchantSkillList(final EnchantSkillType type)
	{
		_type = type;
		_skills = new ArrayList<Skill>();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x29);

		writeD(_type.ordinal());
		writeD(_skills.size());
		for(final Skill sk : _skills)
		{
			writeD(sk.id);
			writeD(sk.level);
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_29_EXENCHANTSKILLLIST;
	}
}
