package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

public class L2FriendStatus extends L2GameServerPacket
{
	private static final String _S__77_FRIENDSTATUS = "[S] 77 FriendStatus";
	private String char_name;
	private boolean _login = false;

	public L2FriendStatus(final L2Player player, final boolean login)
	{
		if(player == null)
			return;
		_login = login;
		char_name = player.getName();
	}

	@Override
	protected final void writeImpl()
	{
		if(char_name == null)
			return;
		writeC(0x77);
		writeD(_login ? 1 : 0); // Logged in 1 logged off 0
		writeS(char_name);
		writeD(0); // id персонажа с базы оффа, не object_id
	}

	@Override
	public String getType()
	{
		return _S__77_FRIENDSTATUS;
	}
}
