package l2n.game.network.serverpackets;

/**
 * Format: ch (trigger)
 */
public class ExShowAdventurerGuideBook extends L2GameServerPacket
{
	private static final String _S__FE_38_EXSHOWADVENTURERGUIDEBOOK = "[S] FE:38 ExShowAdventurerGuideBook";

	@Override
	protected final void writeImpl()
	{
		// System.out.println("ExShowAdventurerGuideBook[24]");
		writeC(EXTENDED_PACKET);
		writeH(0x38);
	}

	@Override
	public String getType()
	{
		return _S__FE_38_EXSHOWADVENTURERGUIDEBOOK;
	}
}
