package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

import java.util.logging.Logger;

/**
 * 0000: 75 7a 07 80 49 63 27 00 4a ea 01 00 00 c1 37 fe uz..Ic'.J.....7.
 * <p>
 * 0010: ff 9e c3 03 00 8f f3 ff ff .........
 * <p>
 * <p>
 * format dddddd (player id, target id, distance, startx, starty, startz)
 * <p>
 */
public class MoveToPawn extends L2GameServerPacket
{
	private static Logger _log = Logger.getLogger(MoveToPawn.class.getName());

	private static final String _S__72_MOVETOPAWN = "[S] 72 MoveToPawn";
	private int _chaId;
	private int _targetId;
	private int _distance;
	private int _x, _y, _z, _tx, _ty, _tz;

	private L2Character cha;
	private L2Character target;

	public MoveToPawn(final L2Character cha, final L2Character target, final int distance)
	{
		if(cha == target)
		{
			_log.warning("Try pawn to yourself!");
			Thread.dumpStack();
			_chaId = 0;
			return;
		}

		this.cha = cha;
		this.target = target;
		_chaId = cha.getObjectId();
		_targetId = target.getObjectId();
		_distance = distance;
		_x = cha.getX();
		_y = cha.getY();
		_z = cha.getZ();
		_tx = target.getX();
		_ty = target.getY();
		_tz = target.getZ();
	}

	@Override
	protected final void writeImpl()
	{
		if(_chaId == 0)
			return;

		writeC(0x72);

		writeD(_chaId);
		writeD(_targetId);
		writeD(_distance);

		writeD(_x);
		writeD(_y);
		writeD(_z);

		if(_tx != 0)
			writeD(_tx);
		else
			writeD(_x);
		if(_ty != 0)
			writeD(_ty);
		else
			writeD(_y);
		if(_tz != 0)
			writeD(_tz);
		else
			writeD(_z);
	}

	@Override
	public String getType()
	{
		return _S__72_MOVETOPAWN + ": " + cha + ", target=" + target + "; dist=" + _distance;
	}
}
