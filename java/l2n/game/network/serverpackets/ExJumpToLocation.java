package l2n.game.network.serverpackets;

import l2n.util.Location;

/**
 * @author L2System Project
 * @date 06.07.2010
 * @time 17:35:00
 */
public class ExJumpToLocation extends L2GameServerPacket
{
	private final int _objectId;
	private final Location _current;
	private final Location _destination;

	public ExJumpToLocation(final int objectId, final Location from, final Location to)
	{
		_objectId = objectId;
		_current = from;
		_destination = to;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeC(0x88);

		writeD(_objectId);

		writeD(_destination.x);
		writeD(_destination.y);
		writeD(_destination.z);

		writeD(_current.x);
		writeD(_current.y);
		writeD(_current.z);
	}

	@Override
	public String getType()
	{
		return "[S] FE:88 ExJumpToLocation";
	}
}
