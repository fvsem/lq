package l2n.game.network.serverpackets;

import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.util.concurrent.ConcurrentLinkedQueue;

public class PrivateStoreListSell extends L2GameServerPacket
{
	private static final String _S__A1_PRIVATESTORELISTSELL = "[S] A1 PrivateStoreListSell";
	private final int seller_id;
	private final long buyer_adena;
	private final boolean _package;
	private final ConcurrentLinkedQueue<TradeItem> _sellList;

	/**
	 * Список вещей в личном магазине продажи, показываемый покупателю
	 * 
	 * @param buyer
	 * @param seller
	 */
	public PrivateStoreListSell(final L2Player buyer, final L2Player seller)
	{
		seller_id = seller.getObjectId();
		buyer_adena = buyer.getAdena();
		_package = seller.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE;
		_sellList = seller.getSellList();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xA1);
		writeD(seller_id);
		writeD(_package ? 1 : 0);
		writeQ(buyer_adena);

		writeD(_sellList.size());
		for(final TradeItem ti : _sellList)
		{
			final L2Item tempItem = ItemTable.getInstance().getTemplate(ti.getItemId());
			writeD(tempItem.getType2ForPackets());
			writeD(ti.getObjectId());
			writeD(ti.getItemId());
			writeQ(ti.getCount());
			writeH(0x00);
			writeH(ti.getEnchantLevel());
			writeH(ti.getCustomType2());
			writeD(tempItem.getBodyPart());
			writeQ(ti.getOwnersPrice());// your price
			writeQ(ti.getStorePrice()); // store price

			writeItemElements(ti);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__A1_PRIVATESTORELISTSELL;
	}
}
