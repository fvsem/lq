package l2n.game.network.serverpackets;

public class PledgeShowMemberListDeleteAll extends L2GameServerPacket
{
	private static final String _S__88_PLEDGESHOWMEMBERLISTDELETEALL = "[S] 88 PledgeShowMemberListDeleteAll";

	@Override
	protected final void writeImpl()
	{
		writeC(0x88);
	}

	@Override
	public String getType()
	{
		return _S__88_PLEDGESHOWMEMBERLISTDELETEALL;
	}
}
