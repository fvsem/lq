package l2n.game.network.serverpackets;

public class ExDuelStart extends L2GameServerPacket
{
	int _duelType;

	public ExDuelStart(final int duelType)
	{
		_duelType = duelType;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x4e);
		writeD(_duelType); // неизвестный, возможно тип дуэли.
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return "[S] FE:4E ExDuelStart";
	}
}
