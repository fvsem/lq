package l2n.game.network.serverpackets;

import javolution.util.FastMap;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;
import l2n.game.model.entity.residence.Castle;

/**
 * format(packet 0xFE)
 * ch dd [dddc]
 * c - id
 * h - sub id
 * d - crop id
 * d - size
 * [
 * d - manor name
 * d - buy residual
 * d - buy price
 * c - reward type
 * ]
 */
public class ExShowProcureCropDetail extends L2GameServerPacket
{
	private static final String _S__FE_78_EXSHOWPROCURECROPDETAIL = "[S] FE:78 ExShowProcureCropDetail";

	private final int _cropId;
	private final FastMap<Integer, CropProcure> _castleCrops;

	public ExShowProcureCropDetail(final int cropId)
	{
		_cropId = cropId;
		_castleCrops = new FastMap<Integer, CropProcure>();

		for(final Castle c : CastleManager.getInstance().getCastles().values())
		{
			final CropProcure cropItem = c.getCrop(_cropId, CastleManorManager.PERIOD_CURRENT);
			if(cropItem != null && cropItem.getAmount() > 0)
				_castleCrops.put(c.getId(), cropItem);
		}
	}

	@Override
	public void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x78);

		writeD(_cropId); // crop id
		writeD(_castleCrops.size()); // size

		for(final int manorId : _castleCrops.keySet())
		{
			final CropProcure crop = _castleCrops.get(manorId);
			writeD(manorId); // manor name
			writeQ(crop.getAmount()); // buy residual
			writeQ(crop.getPrice()); // buy price
			writeC(crop.getReward()); // reward type
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_78_EXSHOWPROCURECROPDETAIL;
	}
}
