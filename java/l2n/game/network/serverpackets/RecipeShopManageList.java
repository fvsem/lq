package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2ManufactureItem;
import l2n.game.model.L2RecipeList;
import l2n.game.model.actor.L2Player;

import java.util.Collection;

/**
 * dd d(dd) d(ddd)
 */
public class RecipeShopManageList extends L2GameServerPacket
{
	private static final String _S__de_RecipeShopManageList = "[S] de RecipeShopManageList";
	private final GArray<CreateItemInfo> infos = new GArray<CreateItemInfo>();
	private final GArray<RecipeInfo> recipes = new GArray<RecipeInfo>();
	private final int seller_id;
	private final long seller_adena;
	private final boolean _isDwarven;

	public RecipeShopManageList(final L2Player seller, final boolean isDvarvenCraft)
	{
		seller_id = seller.getObjectId();
		seller_adena = seller.getAdena();

		_isDwarven = isDvarvenCraft;
		Collection<L2RecipeList> _recipes;
		if(_isDwarven)
			_recipes = seller.getDwarvenRecipeBook();
		else
			_recipes = seller.getCommonRecipeBook();

		int i = 1;
		for(final L2RecipeList r : _recipes)
			recipes.add(new RecipeInfo(r.getId(), i++));

		if(seller.getCreateList() != null)
			for(final L2ManufactureItem item : seller.getCreateList().getList())
				infos.add(new CreateItemInfo(item.getRecipeId(), 0, item.getCost()));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xde);
		writeD(seller_id);
		writeD((int) Math.min(seller_adena, Integer.MAX_VALUE));
		writeD(_isDwarven ? 0x00 : 0x01);

		writeD(recipes.size());
		for(final RecipeInfo _recipe : recipes)
		{
			writeD(_recipe._id);
			writeD(_recipe.n);
		}
		recipes.clear();

		writeD(infos.size());
		for(final CreateItemInfo _info : infos)
		{
			writeD(_info._id);
			writeD(_info.unk1);
			writeQ(_info.cost);
		}
		infos.clear();
	}

	@Override
	public String getType()
	{
		return _S__de_RecipeShopManageList;
	}

	static class RecipeInfo
	{
		public int _id, n;

		public RecipeInfo(final int __id, final int _n)
		{
			_id = __id;
			n = _n;
		}
	}

	static class CreateItemInfo
	{
		public int _id, unk1;
		public long cost;

		public CreateItemInfo(final int __id, final int _unk1, final long _cost)
		{
			_id = __id;
			unk1 = _unk1;
			cost = _cost;
		}
	}
}
