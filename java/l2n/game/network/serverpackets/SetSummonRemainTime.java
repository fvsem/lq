package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Summon;

public class SetSummonRemainTime extends L2GameServerPacket
{
	private static final String _S__D1_PETLIVETIME = "[S] D1 PetLiveTime";

	private final int _maxFed;
	private final int _curFed;

	/**
	 * packet type id 0xd1
	 * format: cdd
	 * 
	 * @param summon
	 */
	public SetSummonRemainTime(final L2Summon summon)
	{
		_curFed = summon.getCurrentFed();
		_maxFed = summon.getMaxFed();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xD1);
		writeD(_maxFed);
		writeD(_curFed);
	}

	@Override
	public String getType()
	{
		return _S__D1_PETLIVETIME;
	}
}
