package l2n.game.network.serverpackets;

import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.Map;

public class SellListProcure extends L2GameServerPacket
{
	private static final String _S__ef_SELLLISTPROCURE = "[S] ef SellListProcure";
	private final long _money;
	private final Map<L2ItemInstance, Long> _sellList = new FastMap<L2ItemInstance, Long>();
	private GArray<CropProcure> _procureList = new GArray<CropProcure>();
	private final int _castle;

	public SellListProcure(final L2Player player, final int castleId)
	{
		_money = player.getAdena();
		_castle = castleId;
		_procureList = CastleManager.getInstance().getCastleByIndex(_castle).getCropProcure(0);
		for(final CropProcure c : _procureList)
		{
			final L2ItemInstance item = player.getInventory().getItemByItemId(c.getId());
			if(item != null && c.getAmount() > 0)
				_sellList.put(item, c.getAmount());
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xef);
		writeQ(_money); // money
		writeD(0x00); // lease ?
		writeH(_sellList.size()); // list size

		for(final L2ItemInstance item : _sellList.keySet())
		{
			writeH(item.getItem().getType1());
			writeD(item.getObjectId());
			writeD(item.getItemId());
			writeQ(_sellList.get(item)); // count
			writeH(item.getItem().getType2ForPackets());
			writeH(0); // unknown
			writeQ(0); // price, u shouldnt get any adena for crops, only raw materials
		}
	}

	@Override
	public String getType()
	{
		return _S__ef_SELLLISTPROCURE;
	}
}
