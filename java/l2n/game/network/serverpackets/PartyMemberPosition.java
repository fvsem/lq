package l2n.game.network.serverpackets;

import javolution.util.FastMap;
import l2n.game.model.actor.L2Player;

import java.util.Collection;

public class PartyMemberPosition extends L2GameServerPacket
{
	private static final String _S__BA_PARTYMEMBERPOSITION = "[S] BA PartyMemberPosition";
	private final FastMap<Integer, int[]> positions = new FastMap<Integer, int[]>();

	public PartyMemberPosition add(final Collection<L2Player> members)
	{
		if(members != null)
			for(final L2Player member : members)
				add(member);
		return this;
	}

	public PartyMemberPosition add(final L2Player actor)
	{
		if(actor != null)
			positions.put(actor.getObjectId(), new int[]
			{
					actor.getX(), actor.getY(), actor.getZ()
			});
		return this;
	}

	public void clear()
	{
		positions.clear();
	}

	public int size()
	{
		return positions.size();
	}

	@Override
	protected final void writeImpl()
	{
		if(positions.isEmpty())
			return;

		final L2Player player = getClient().getActiveChar();
		if(player == null || player.getParty() == null)
			return;

		final int sz = positions.size();
		if(sz < 1)
			return;

		writeC(0xba);
		writeD(sz);

		for(final int id : positions.keySet())
		{
			final int[] pos = positions.get(id);
			writeD(id);
			writeD(pos[0]);
			writeD(pos[1]);
			writeD(pos[2]);
		}
	}

	@Override
	public String getType()
	{
		return _S__BA_PARTYMEMBERPOSITION;
	}
}
