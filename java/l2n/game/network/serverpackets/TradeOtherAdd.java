package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2ItemInstance;

public class TradeOtherAdd extends L2GameServerPacket
{
	private static final String _S__1b_TRADEOTHERADD = "[S] 1b TradeOtherAdd";
	private final L2ItemInstance temp;
	private final long _amount;

	public TradeOtherAdd(final L2ItemInstance x, final long amount)
	{
		temp = x;
		_amount = amount;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x1b);

		writeH(1); // item count

		writeH(temp.getItem().getType1()); // item type1
		writeD(temp.getObjectId());
		writeD(temp.getItemId());
		writeQ(_amount);
		writeH(temp.getItem().getType2ForPackets()); // item type2
		writeH(0x00); // ?

		writeD(temp.getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
		writeH(temp.getEnchantLevel()); // enchant level
		writeH(0x00); // ?
		writeH(temp.getCustomType2());

		// T1
		writeItemElements(temp);

		writeH(0x00); // Enchant effect 1
		writeH(0x00); // Enchant effect 2
		writeH(0x00); // Enchant effect 3
	}

	@Override
	public String getType()
	{
		return _S__1b_TRADEOTHERADD;
	}
}
