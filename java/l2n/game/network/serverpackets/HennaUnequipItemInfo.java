package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;

/**
 * @author L2System Project
 * @date 01.12.2009
 * @time 1:44:18
 */
public class HennaUnequipItemInfo extends L2GameServerPacket
{
	private static final String _S__E3_HennaItemRemoveInfo = "[S] e7 HennaUnequipItemInfo";

	private final L2Player _activeChar;
	private final int _str, _con, _dex, _int, _wit, _men;
	private final L2HennaInstance _henna;

	public HennaUnequipItemInfo(final L2HennaInstance henna, final L2Player player)
	{
		_henna = henna;
		_activeChar = player;
		_str = player.getSTR();
		_dex = player.getDEX();
		_con = player.getCON();
		_int = player.getINT();
		_wit = player.getWIT();
		_men = player.getMEN();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe7);
		writeD(_henna.getSymbolId()); // symbol ID
		writeD(_henna.getItemIdDye()); // item ID of dye
		writeQ(0x00); // total amount of dye required
		writeQ(_henna.getPrice() / 5); // total amount of adena required to remove symbol
		// able to remove or not 0 is false and 1 is true
		writeD(1);
		writeQ(_activeChar.getAdena());
		writeD(_int); // current INT
		writeC(_int + _henna.getStatINT()); // equip INT
		writeD(_str); // current STR
		writeC(_str + _henna.getStatSTR()); // equip STR
		writeD(_con); // current CON
		writeC(_con + _henna.getStatCON()); // equip CON
		writeD(_men); // current MEM
		writeC(_men + _henna.getStatMEM()); // equip MEM
		writeD(_dex); // current DEX
		writeC(_dex + _henna.getStatDEX()); // equip DEX
		writeD(_wit); // current WIT
		writeC(_wit + _henna.getStatWIT()); // equip WIT
	}

	@Override
	public String getType()
	{
		return _S__E3_HennaItemRemoveInfo;
	}
}
