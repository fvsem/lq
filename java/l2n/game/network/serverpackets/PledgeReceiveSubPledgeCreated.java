package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan.SubPledge;

public class PledgeReceiveSubPledgeCreated extends L2GameServerPacket
{
	private static final String _S__FE_40_PLEDGERECEIVESUBPLEDGECREATED = "[S] FE:40 PledgeReceiveSubPledgeCreated";
	private final int type;
	private final String _name, leader_name;

	public PledgeReceiveSubPledgeCreated(final SubPledge subPledge)
	{
		type = subPledge.getType();
		_name = subPledge.getName();
		leader_name = subPledge.getLeaderName();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x40);

		writeD(0x01);
		writeD(type);
		writeS(_name);
		writeS(leader_name);
	}

	@Override
	public String getType()
	{
		return _S__FE_40_PLEDGERECEIVESUBPLEDGECREATED;
	}
}
