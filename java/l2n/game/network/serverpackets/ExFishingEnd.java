package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;

/**
 * Format: (ch) dc
 * d: character object id
 * c: 1 if won 0 if failed
 */
public class ExFishingEnd extends L2GameServerPacket
{
	private static final String _S__FE_1F_EXFISHINGEND = "[S] FE:1f ExFishingEnd";
	private final int char_obj_id;
	private final boolean _win;

	public ExFishingEnd(final boolean win, final L2Player character)
	{
		_win = win;
		char_obj_id = character.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x1f);
		writeD(char_obj_id);
		writeC(_win ? 1 : 0);
	}

	@Override
	public String getType()
	{
		return _S__FE_1F_EXFISHINGEND;
	}
}
