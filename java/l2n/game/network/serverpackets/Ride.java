package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class Ride extends L2GameServerPacket
{
	private static final String _S__8c_Ride = "[S] 8c Ride";
	private boolean _canWriteImpl = false;
	private int _mountType, _id, _rideClassID;
	private Location _loc;

	/**
	 * 0x8c Ride dddd
	 */
	public Ride(final L2Player cha)
	{
		if(cha == null)
			return;

		_id = cha.getObjectId();
		_mountType = cha.getMountType();
		_rideClassID = cha.getMountNpcId() + 1000000;
		_loc = cha.getLoc();

		_canWriteImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!_canWriteImpl)
			return;

		writeC(0x8c);
		writeD(_id);
		writeD(_mountType == 0 ? 0 : 1);
		writeD(_mountType);
		writeD(_rideClassID);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
	}

	@Override
	public String getType()
	{
		return _S__8c_Ride;
	}
}
