package l2n.game.network.serverpackets;

import l2n.commons.util.StatsSet;
import l2n.game.model.entity.Hero;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.util.StringUtil;

import java.util.Map;

/**
 * Format: (ch) d [SdSdSdd]
 * d: size
 * [
 * S: hero name
 * d: hero class ID
 * S: hero clan name
 * d: hero clan crest id
 * S: hero ally name
 * d: hero Ally id
 * d: count
 * ]
 */
public class ExHeroList extends L2GameServerPacket
{
	private static final String _S__FE_23_EXHEROLIST = "[S] FE:79 ExHeroList";
	private final Map<Integer, StatsSet> _heroList;

	public ExHeroList()
	{
		_heroList = Hero.getInstance().getHeroes();
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x79);
		writeD(_heroList.size());

		for(final int heroId : _heroList.keySet())
		{
			final StatsSet hero = _heroList.get(heroId);
			writeS(hero.getString(Olympiad.CHAR_NAME, StringUtil.EMPTY_STRING));
			writeD(hero.getInteger(Olympiad.CLASS_ID));
			writeS(hero.getString(Hero.CLAN_NAME, StringUtil.EMPTY_STRING));
			writeD(hero.getInteger(Hero.CLAN_CREST, 0));
			writeS(hero.getString(Hero.ALLY_NAME, StringUtil.EMPTY_STRING));
			writeD(hero.getInteger(Hero.ALLY_CREST, 0));
			writeD(hero.getInteger(Hero.COUNT));
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_23_EXHEROLIST;
	}
}
