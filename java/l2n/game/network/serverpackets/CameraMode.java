package l2n.game.network.serverpackets;

public class CameraMode extends L2GameServerPacket
{
	int _mode;

	private static final String _S__f7_CAMERAMODE = "[S] f7 CameraMode";

	/**
	 * Forces client camera mode change
	 * 
	 * @param mode
	 *            0 - third person cam
	 *            1 - first person cam
	 */
	public CameraMode(final int mode)
	{
		_mode = mode;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xf7);
		writeD(_mode);
	}

	@Override
	public String getType()
	{
		return _S__f7_CAMERAMODE;
	}
}
