package l2n.game.network.serverpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.model.L2Multisell.MultiSellListContainer;
import l2n.game.model.base.MultiSellEntry;
import l2n.game.model.base.MultiSellIngredient;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

public class MultiSellList extends L2GameServerPacket
{
	private static final String _S__D0_MULTISELLLIST = "[S] D0 MultiSellList";

	protected int _page;
	protected int _finished;

	private final int _listId;
	private GArray<MultiSellEntry> _possiblelist = new GArray<MultiSellEntry>();

	public MultiSellList(final MultiSellListContainer list, final int page, final int finished)
	{
		_possiblelist = list.getEntries();
		_listId = list.getListId();
		_page = page;
		_finished = finished;
	}

	@Override
	protected final void writeImpl()
	{
		// ddddd (dchddddddddddhh (ddhdhdddddddddd)(dhdhdddddddddd))
		writeC(0xD0);
		writeD(_listId); // list id
		writeD(_page); // page
		writeD(_finished); // finished
		writeD(Config.MULTISELL_SIZE); // size of pages
		writeD(_possiblelist != null ? _possiblelist.size() : 0); // list lenght

		if(_possiblelist == null)
			return;

		GArray<MultiSellIngredient> ingredients;
		for(final MultiSellEntry ent : _possiblelist)
		{
			ingredients = fixIngredients(ent.getIngredients());
			writeD(ent.getEntryId());
			writeC(ent.getProduction().get(0).isStackable() ? 1 : 0); // stackable?
			writeH(0x00); // unknown
			writeD(0x00); // инкрустация
			writeD(0x00); // инкрустация

			writeItemElements();

			writeH(ent.getProduction().size());
			writeH(ingredients.size());

			for(final MultiSellIngredient prod : ent.getProduction())
			{
				final int itemId = prod.getItemId();
				final L2Item template = itemId > 0 ? ItemTable.getInstance().getTemplate(prod.getItemId()) : null;
				writeD(itemId);
				writeD(itemId > 0 ? template.getBodyPart() : 0);
				writeH(itemId > 0 ? template.getType2ForPackets() : 0);
				writeQ(prod.getItemCount());
				writeH(prod.getItemEnchant());
				writeD(0x00); // инкрустация
				writeD(0x00); // инкрустация
				writeItemElements(prod);
			}

			for(final MultiSellIngredient i : ingredients)
			{
				final int itemId = i.getItemId();
				final L2Item item = itemId > 0 ? ItemTable.getInstance().getTemplate(i.getItemId()) : null;
				writeD(itemId); // ID
				writeH(itemId > 0 ? item.getType2() : 0xffff);
				writeQ(i.getItemCount()); // Count
				writeH((itemId > 0 ? item.getType2() : 0) <= L2Item.TYPE2_ACCESSORY ? i.getItemEnchant() : 0); // Enchant Level
				writeD(0x00); // инкрустация
				writeD(0x00); // инкрустация
				writeItemElements(i);
			}
		}
	}

	private static GArray<MultiSellIngredient> fixIngredients(final GArray<MultiSellIngredient> ingredients)
	{
		// FIXME временная затычка, пока NCSoft не починят в клиенте отображение мультиселов где кол-во больше Integer.MAX_VALUE
		int needFix = 0;
		for(final MultiSellIngredient ingredient : ingredients)
			if(ingredient.getItemCount() > Integer.MAX_VALUE)
				needFix++;

		if(needFix == 0)
			return ingredients;

		MultiSellIngredient temp;
		final GArray<MultiSellIngredient> result = new GArray<MultiSellIngredient>(ingredients.size() + needFix);
		for(MultiSellIngredient ingredient : ingredients)
		{
			ingredient = ingredient.clone();
			while (ingredient.getItemCount() > Integer.MAX_VALUE)
			{
				temp = ingredient.clone();
				temp.setItemCount(2000000000);
				result.add(temp);
				ingredient.setItemCount(ingredient.getItemCount() - 2000000000);
			}
			if(ingredient.getItemCount() > 0)
				result.add(ingredient);
		}

		return result;
	}

	@Override
	public String getType()
	{
		return _S__D0_MULTISELLLIST;
	}
}
