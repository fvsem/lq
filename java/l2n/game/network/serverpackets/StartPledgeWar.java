package l2n.game.network.serverpackets;

public class StartPledgeWar extends L2GameServerPacket
{
	private static final String _S__63_STARTPLEDGEWAR = "[S] 63 StartPledgeWar";
	private final String _pledgeName;
	private final String _char;

	public StartPledgeWar(final String pledge, final String charName)
	{
		_pledgeName = pledge;
		_char = charName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x63);
		writeS(_char);
		writeS(_pledgeName);
	}

	@Override
	public String getType()
	{
		return _S__63_STARTPLEDGEWAR;
	}
}
