package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.model.ItemInfo;

public class PetInventoryUpdate extends L2GameServerPacket
{
	private static final String _S__37_INVENTORYUPDATE = "[S] b4 InventoryUpdate";

	private final ItemInfo _itemInfo;

	public PetInventoryUpdate(final L2ItemInstance item, final byte lastChange)
	{
		item.setLastChange(lastChange);
		_itemInfo = new ItemInfo(item);
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xb4);
		writeH(1);

		writeH(_itemInfo.getLastChange());
		writeH(_itemInfo.getType1()); // item type1
		writeD(_itemInfo.getObjectId());
		writeD(_itemInfo.getItemId());
		writeQ(_itemInfo.getCount());
		writeH(_itemInfo.getType2()); // item type2
		writeH(0x00); // ?
		writeH(_itemInfo.isEquipped() ? 1 : 0);
		writeD(_itemInfo.getBodyPart()); // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
		writeH(_itemInfo.getEnchantLevel()); // enchant level
		writeH(0x00); // ?

		writeItemElements(_itemInfo);

		writeH(0x00); // Enchant effect 1
		writeH(0x00); // Enchant effect 2
		writeH(0x00); // Enchant effect 3
	}

	@Override
	public String getType()
	{
		return _S__37_INVENTORYUPDATE;
	}
}
