package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.util.Location;

import java.util.Map.Entry;

/**
 * @author L2System Project
 * @date 19.04.2010
 * @time 5:52:03
 */
public class ExShowOwnthingPos extends L2GameServerPacket
{
	private final GArray<Location> _locs = new GArray<Location>();

	public ExShowOwnthingPos()
	{
		for(final Entry<Integer, Location> e : TerritorySiege.getWardsLoc().entrySet())
			_locs.add(e.getValue().setH(e.getKey()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x93);
		writeD(_locs.size());
		for(final Location loc : _locs)
		{
			writeD(80 + loc.h);
			writeD(loc.x);
			writeD(loc.y);
			writeD(loc.z);
		}
	}

	@Override
	public String getType()
	{
		return "[S] FE 93 ExShowOwnthingPos";
	}
}
