package l2n.game.network.serverpackets;

public class ExPutCommissionResultForVariationMake extends L2GameServerPacket
{
	private static final String _S__FE_55_EXPUTCOMMISSIONRESULTFORVARIATIONMAKE = "[S] FE:55 ExPutCommissionResultForVariationMake";

	private final int _gemstoneObjId;
	private final int _itemId;
	private final long _gemstoneCount;
	private final int _unk1;
	private final int _unk2;
	private final int _unk3;

	public ExPutCommissionResultForVariationMake(final int gemstoneObjId, final long count, final int itemId)
	{
		_gemstoneObjId = gemstoneObjId;
		_itemId = itemId;
		_gemstoneCount = count;
		_unk1 = 1;
		_unk2 = 1;
		_unk3 = 1;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x55);
		writeD(_gemstoneObjId);
		writeD(_itemId);
		writeQ(_gemstoneCount);
		writeD(_unk1);
		writeD(_unk2);
		writeD(_unk3);
	}

	@Override
	public String getType()
	{
		return _S__FE_55_EXPUTCOMMISSIONRESULTFORVARIATIONMAKE;
	}
}
