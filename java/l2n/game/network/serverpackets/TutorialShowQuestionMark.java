package l2n.game.network.serverpackets;

public class TutorialShowQuestionMark extends L2GameServerPacket
{
	/**
	 * После клика по знаку вопроса клиент попросит html-ку с этим номером.
	 */
	private static final String _S__a7_TUTORIALSHOWQUESTIONMARK = "[S] a7 TutorialShowQuestionMark";
	private final int _number;

	public TutorialShowQuestionMark(final int number)
	{
		_number = number;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xa7);
		writeD(_number);
	}

	@Override
	public String getType()
	{
		return _S__a7_TUTORIALSHOWQUESTIONMARK;
	}
}
