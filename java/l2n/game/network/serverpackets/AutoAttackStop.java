package l2n.game.network.serverpackets;

public class AutoAttackStop extends L2GameServerPacket
{
	private static final String _S__26_AUTOATTACKSTOP = "[S] 26 AutoAttackStop";
	private final int _targetId;

	/**
	 * @param _characters
	 */
	public AutoAttackStop(final int targetId)
	{
		_targetId = targetId;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x26);
		writeD(_targetId);
	}

	@Override
	public String getType()
	{
		return _S__26_AUTOATTACKSTOP;
	}
}
