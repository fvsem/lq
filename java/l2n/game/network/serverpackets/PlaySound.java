package l2n.game.network.serverpackets;

import l2n.util.Location;

public class PlaySound extends L2GameServerPacket
{
	private static final String _S__9e_PlaySound = "[S] 9e PlaySound";
	private final int _mode;
	private final String _soundFile;
	private final int _unknown3;
	private final int _unknown4;
	private Location _loc = new Location();

	public PlaySound(final String soundFile)
	{
		_mode = 0;
		_soundFile = soundFile;
		_unknown3 = 0;
		_unknown4 = 0;
	}

	public PlaySound(final int mode, final String soundFile, final int unknown3, final int unknown4, final Location loc)
	{
		_mode = mode;
		_soundFile = soundFile;
		_unknown3 = unknown3;
		_unknown4 = unknown4;
		_loc = loc;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x9e);
		writeD(_mode); // unknown 0 for quest and ship, c4 toturial = 2
		writeS(_soundFile);
		writeD(_unknown3); // unknown 0 for quest; 1 for ship;
		writeD(_unknown4); // 0 for quest; objectId of ship
		writeD(_loc.x); // x
		writeD(_loc.y); // y
		writeD(_loc.z); // z
	}

	@Override
	public String getType()
	{
		return _S__9e_PlaySound;
	}
}
