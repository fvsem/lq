package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;

/**
 * sample 4E 01 00 00 00 count c1 b2 e0 4a object id 54 00 75 00 65 00 73 00 64 00 61 00 79 00 00 00 name 5a 01 00 00 hp 5a 01 00 00 hp max 89 00 00 00 mp 89 00 00 00 mp max 0e 00 00 00 level 12 00 00
 * 00 class 00 00 00 00 01 00 00 00 format ddd (dSddddddddddd)
 */
public class PartySmallWindowAll extends L2GameServerPacket
{
	private static final String _S__4E_PARTYSMALLWINDOWALL = "[S] 4E PartySmallWindowAll";

	private final int leader_id;
	private final int loot;
	private final GArray<MemberInfo> members = new GArray<MemberInfo>();

	public PartySmallWindowAll(final L2Party party, final L2Player exclude)
	{
		leader_id = party.getPartyLeaderOID();
		loot = party.getLootDistribution();

		for(final L2Player member : party.getPartyMembers())
			if(member.getObjectId() != exclude.getObjectId())
				members.add(new MemberInfo(member));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x4E);
		writeD(leader_id); // c3 party leader id
		writeD(loot); // c3 party loot type (0,1,2,....)
		writeD(members.size());
		for(final MemberInfo member : members)
		{
			writeD(member._id);
			writeS(member._name);
			writeD(member.curCp);
			writeD(member.maxCp);
			writeD(member.curHp);
			writeD(member.maxHp);
			writeD(member.curMp);
			writeD(member.maxMp);
			writeD(member.level);
			writeD(member.class_id);
			writeD(0x00);// writeD(0x01); ??
			writeD(member.race_id);
			writeD(0x00); // T2.3
			writeD(0x00); // T2.3

			if(member.pet_id != 0)
			{
				writeD(member.pet_id);
				writeD(member.pet_NpcId);
				writeD(member.pet_type);
				writeS(member.pet_Name);
				writeD(member.pet_curHp);
				writeD(member.pet_maxHp);
				writeD(member.pet_curMp);
				writeD(member.pet_maxMp);
				writeD(member.pet_level);
			}
			else
				writeD(0x00);
		}
		members.clear();
	}

	@Override
	public String getType()
	{
		return _S__4E_PARTYSMALLWINDOWALL;
	}

	public static class MemberInfo
	{
		public String _name, pet_Name;
		public int _id, curCp, maxCp, curHp, maxHp, curMp, maxMp, level,
				class_id, race_id, pet_id, pet_NpcId, pet_curHp, pet_maxHp,
				pet_curMp, pet_maxMp, pet_level, pet_type;

		public MemberInfo(final L2Player member)
		{
			_name = member.getName();
			_id = member.getObjectId();
			curCp = (int) member.getCurrentCp();
			maxCp = member.getMaxCp();
			curHp = (int) member.getCurrentHp();
			maxHp = member.getMaxHp();
			curMp = (int) member.getCurrentMp();
			maxMp = member.getMaxMp();
			level = member.getLevel();
			class_id = member.getClassId().getId();
			race_id = member.getRace().ordinal();

			final L2Summon pet = member.getPet();
			if(pet != null)
			{
				pet_id = pet.getObjectId();
				pet_NpcId = pet.getNpcId() + 1000000;
				pet_type = pet.getSummonType();
				pet_Name = pet.getName();
				pet_curHp = (int) pet.getCurrentHp();
				pet_maxHp = pet.getMaxHp();
				pet_curMp = (int) pet.getCurrentMp();
				pet_maxMp = pet.getMaxMp();
				pet_level = pet.getLevel();
			}
			else
				pet_id = 0;

		}
	}
}
