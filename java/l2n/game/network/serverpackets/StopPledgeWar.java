package l2n.game.network.serverpackets;

public class StopPledgeWar extends L2GameServerPacket
{
	private static final String _S__65_STOPPLEDGEWAR = "[S] 65 StopPledgeWar";
	private final String _pledgeName;
	private final String _char;

	public StopPledgeWar(final String pledge, final String charName)
	{
		_pledgeName = pledge;
		_char = charName;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x65);
		writeS(_pledgeName);
		writeS(_char);
	}

	@Override
	public String getType()
	{
		return _S__65_STOPPLEDGEWAR;
	}
}
