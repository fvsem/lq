package l2n.game.network.serverpackets;

/**
 * Format: (chd) d
 * d: Seconds Left
 */
public class ExCubeGameChangeTimeToStart extends L2GameServerPacket
{
	private static final String ExCubeGameChangeTimeToStart = "[S] FE:97:03 ExCubeGameChangeTimeToStart";

	private final int _seconds;

	/**
	 * Update Minigame Waiting List Time to Start
	 * 
	 * @param seconds
	 */
	public ExCubeGameChangeTimeToStart(final int seconds)
	{
		_seconds = seconds;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xfe);
		writeH(0x97);
		writeD(0x03);

		writeD(_seconds);
	}

	@Override
	public final String getType()
	{
		return ExCubeGameChangeTimeToStart;
	}
}
