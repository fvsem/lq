package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;

public class ExOlympiadSpelledInfo extends L2GameServerPacket
{
	// chdd(dhd)
	private static final String _S__FE_2A_OLYMPIADSPELLEDINFO = "[S] FE:2A ExOlympiadSpelledInfo";

	private int char_obj_id = 0;
	private final GArray<Effect> _effects;

	private static class Effect
	{
		public final int skillId;
		public final int dat;
		public final int duration;

		public Effect(final int skillId, final int dat, final int duration)
		{
			this.skillId = skillId;
			this.dat = dat;
			this.duration = duration;
		}
	}

	public ExOlympiadSpelledInfo()
	{
		_effects = new GArray<Effect>();
	}

	public void addEffect(final int skillId, final int level, final int duration)
	{
		_effects.add(new Effect(skillId, level, duration));
	}

	public void addSpellRecivedPlayer(final L2Player cha)
	{
		if(cha != null)
			char_obj_id = cha.getObjectId();
	}

	@Override
	protected final void writeImpl()
	{
		if(char_obj_id == 0)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x7b);

		writeD(char_obj_id);
		writeD(_effects.size());
		for(final Effect temp : _effects)
		{
			writeD(temp.skillId);
			writeH(temp.dat);
			writeD(temp.duration);
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_2A_OLYMPIADSPELLEDINFO;
	}
}
