package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.siege.territory.TerritorySiege;

/**
 * @author L2System Project
 * @date 19.04.2010
 * @time 5:44:09
 */
public class ExReplyDominionInfo extends L2GameServerPacket
{
	private final GArray<TerritoryInfo> _ti = new GArray<TerritoryInfo>();

	public ExReplyDominionInfo()
	{
		for(final Castle c : CastleManager.getInstance().getCastles().values())
			_ti.add(new TerritoryInfo(c.getId(), c.getName(), c.getOwner() == null ? "" : c.getOwner().getName(), c.getFlags()));
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x92);
		writeD(_ti.size());
		for(final TerritoryInfo cf : _ti)
		{
			writeD(80 + cf.id);
			writeS(cf.terr);
			writeS(cf.clan);
			writeD(cf.flags.length);
			for(final int f : cf.flags)
				writeD(80 + f);
			writeD((int) (TerritorySiege.getSiegeDate().getTimeInMillis() / 1000));
		}
	}

	private class TerritoryInfo
	{
		public int id;
		public String terr;
		public String clan;
		public int[] flags;

		public TerritoryInfo(final int id_, final String terr_, final String clan_, final int[] flags_)
		{
			id = id_;
			terr = terr_;
			clan = clan_;
			flags = flags_;
		}
	}

	@Override
	public String getType()
	{
		return "[S] FE:92 ExReplyDominionInfo";
	}
}
