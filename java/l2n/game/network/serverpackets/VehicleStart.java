package l2n.game.network.serverpackets;

import l2n.game.model.entity.vehicle.L2Ship;

public class VehicleStart extends L2GameServerPacket
{
	private final L2Ship _boat;

	private final int _state;

	/**
	 * @param instance
	 */
	public VehicleStart(final L2Ship boat, final int state)
	{
		_boat = boat;
		_state = state;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xC0);
		writeD(_boat.getObjectId());
		writeD(_state);
	}

	@Override
	public String getType()
	{
		return "[S] C0 VehicleStarted";
	}
}
