package l2n.game.network.serverpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.templates.L2Item;

import java.util.NoSuchElementException;

public class WareHouseWithdrawList extends L2GameServerPacket
{
	private static final String _S__54_WAREHOUSEWITHDRAWALLIST = "[S] 42 WareHouseWithdrawalList";

	private long _money;
	private L2ItemInstance[] _items;
	private int _type;
	private boolean can_writeImpl = false;

	public WareHouseWithdrawList(final L2Player cha, final WarehouseType type, final ItemClass clss)
	{
		if(cha == null)
			return;

		_money = cha.getAdena();
		_type = type.getPacketValue();
		switch (type)
		{
			case PRIVATE:
				_items = cha.getWarehouse().listItems(clss);
				break;
			case CLAN:
			case CASTLE:
				_items = cha.getClan().getWarehouse().listItems(clss);
				break;
			default:
				throw new NoSuchElementException("Invalid value of 'type' argument");
		}

		if(_items.length == 0)
		{
			cha.sendPacket(Msg.YOU_HAVE_NOT_DEPOSITED_ANY_ITEMS_IN_YOUR_WAREHOUSE);
			return;
		}

		can_writeImpl = true;
	}

	@Override
	protected final void writeImpl()
	{
		if(!can_writeImpl)
			return;

		writeC(0x42);
		writeH(_type);
		writeQ(_money);
		writeH(_items.length);
		for(final L2ItemInstance temp : _items)
		{
			final L2Item item = temp.getItem();
			writeH(item.getType1());
			writeD(temp.getObjectId());
			writeD(temp.getItemId());
			writeQ(temp.getCount());
			writeH(item.getType2ForPackets());
			writeH(temp.getCustomType1());
			writeD(temp.getBodyPart());
			writeH(temp.getEnchantLevel());
			writeH(0x00); // ?
			writeH(temp.getCustomType2());
			writeD(temp.getObjectId()); // return value for define item (object_id)
			if(temp.isAugmented())
			{
				writeD(temp.getAugmentationId() & 0x0000FFFF);
				writeD(temp.getAugmentationId() >> 16);
			}
			else
				writeQ(0x00);

			writeItemElements(temp);

			writeD(temp.getLifeTimeRemaining());
			writeD(temp.isTemporalItem() ? temp.getLifeTimeRemaining() : -1);

			writeH(0x00); // Enchant effect 1
			writeH(0x00); // Enchant effect 2
			writeH(0x00); // Enchant effect 3
		}
	}

	@Override
	public String getType()
	{
		return _S__54_WAREHOUSEWITHDRAWALLIST;
	}
}
