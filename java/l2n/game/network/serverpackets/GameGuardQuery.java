package l2n.game.network.serverpackets;

public class GameGuardQuery extends L2GameServerPacket
{
	private static final String _S__F9_GAMEGUARDQUERY = "[S] F9 GameGuardQuery";

	@Override
	protected final void writeImpl()
	{
		getClient().setGameGuardOk(false);
		writeC(0x74);
		writeD(0x00); // ? - Меняется при каждом перезаходе.
		writeD(0x00); // ? - Меняется при каждом перезаходе.
		writeD(0x00); // ? - Меняется при каждом перезаходе.
		writeD(0x00); // ? - Меняется при каждом перезаходе.
	}

	@Override
	public String getType()
	{
		return _S__F9_GAMEGUARDQUERY;
	}
}
