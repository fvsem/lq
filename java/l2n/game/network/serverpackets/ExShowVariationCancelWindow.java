package l2n.game.network.serverpackets;

public class ExShowVariationCancelWindow extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x52);
	}

	@Override
	public String getType()
	{
		return "[S] FE:52 ExShowVariationCancelWindow";
	}
}
