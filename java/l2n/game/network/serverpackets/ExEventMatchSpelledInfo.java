package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.actor.L2Player;

/**
 * Хз для чего этот пакет)
 * 
 * @author L2System Project
 * @date 28.04.2013
 * @time 16:50:21
 */
public class ExEventMatchSpelledInfo extends L2GameServerPacket
{
	private static final String _S__FE_04_ExEventMatchSpelledInfo = "[S] FE:04 ExEventMatchSpelledInfo";

	private int char_obj_id = 0;
	private final GArray<Effect> _effects;

	public ExEventMatchSpelledInfo()
	{
		_effects = new GArray<Effect>();
	}

	public void addEffect(final int skillId, final int dat, final int duration)
	{
		_effects.add(new Effect(skillId, dat, duration));
	}

	public void addSpellRecivedPlayer(final L2Player cha)
	{
		if(cha != null)
			char_obj_id = cha.getObjectId();
	}

	@Override
	protected void writeImpl()
	{
		if(char_obj_id == 0)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x04);

		writeD(char_obj_id);
		writeD(_effects.size());
		for(final Effect temp : _effects)
		{
			writeD(temp.skillId);
			writeH(temp.dat);
			writeD(temp.duration);
		}
	}

	class Effect
	{
		int skillId;
		int dat;
		int duration;

		public Effect(final int skillId, final int dat, final int duration)
		{
			this.skillId = skillId;
			this.dat = dat;
			this.duration = duration;
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_04_ExEventMatchSpelledInfo;
	}

}
