package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;

/**
 * Format: ch ddd [ddd]
 */
public class ExGetBossRecord extends L2GameServerPacket
{
	private static final String _S__FE_34_EXGETBOSSRECORD = "[S] FE:34 ExGetBossRecord";

	private final GArray<BossRecordInfo> _bossRecordInfo;
	private final int _ranking;
	private final int _totalPoints;

	public ExGetBossRecord(final int ranking, final int totalScore, final GArray<BossRecordInfo> bossRecordInfo)
	{
		_ranking = ranking; // char ranking
		_totalPoints = totalScore; // char total points
		_bossRecordInfo = bossRecordInfo;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x34);

		writeD(_ranking); // char ranking
		writeD(_totalPoints); // char total points

		if(_bossRecordInfo == null)
		{
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
		}
		else
		{
			writeD(_bossRecordInfo.size()); // list size
			for(final BossRecordInfo w : _bossRecordInfo)
			{
				writeD(w._bossId);
				writeD(w._points);
				writeD(w._unk1);// don`t know
			}
		}
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _S__FE_34_EXGETBOSSRECORD;
	}

	public static class BossRecordInfo
	{
		public int _bossId;
		public int _points;
		public int _unk1;

		public BossRecordInfo(final int bossId, final int points, final int unk1)
		{
			_bossId = bossId;
			_points = points;
			_unk1 = unk1;
		}
	}
}
