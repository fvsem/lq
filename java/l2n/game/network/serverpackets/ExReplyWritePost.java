package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 * @date 30.07.2010
 * @time 0:01:07
 */
public class ExReplyWritePost extends L2GameServerPacket
{
	private static final String _S__FE_B4_EXNOTICEPOSTSENT = "[S] B4 ExNoticePostSent";

	private final int _reply;

	public ExReplyWritePost(final int i)
	{
		_reply = i;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xb4);
		writeD(_reply);
	}

	@Override
	public String getType()
	{
		return _S__FE_B4_EXNOTICEPOSTSENT;
	}
}
