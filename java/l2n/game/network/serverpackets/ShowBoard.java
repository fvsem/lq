package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;

public class ShowBoard extends L2GameServerPacket
{
	private static final String _S__6E_SHOWBOARD = "[S] 6e ShowBoard";
	private String _htmlCode;
	private String _id;
	private GArray<String> _arg;

	private static final String NOT_IMPLEMENTED_RUS = new CustomMessage("common.NotImplemented", "ru").toString();
	private static final String NOT_IMPLEMENTED_ENG = new CustomMessage("common.NotImplemented", "en").toString();
	private static final String DISABLED_RUS = new CustomMessage("common.Disabled", "ru").toString();
	private static final String DISABLED_ENG = new CustomMessage("common.Disabled", "en").toString();

	static final ShowBoard CACHE_NULL_102 = new ShowBoard(null, "102"), CACHE_NULL_103 = new ShowBoard(null, "103");

	public static void notImplementedYet(final L2Player activeChar, final String command)
	{
		if(activeChar == null || command == null)
			return;
		separateAndSend(activeChar, "<html><body><br><br><center>" + (activeChar.isLangRus() ? NOT_IMPLEMENTED_RUS : NOT_IMPLEMENTED_ENG) + "</center><br><br></body></html>");
	}

	public static void disabled(final L2Player activeChar)
	{
		if(activeChar == null)
			return;
		separateAndSend(activeChar, "<html><body><br><br><center>" + (activeChar.isLangRus() ? DISABLED_RUS : DISABLED_ENG) + "</center><br><br></body></html>");
	}

	public static void sendMassage(final L2Player activeChar, final String text)
	{
		if(activeChar == null || text == null)
			return;
		separateAndSend(activeChar, "<html><body><br><br><center>" + text + "</center><br><br></body></html>");
	}

	public static void separateAndSend(final L2Player activeChar, String html)
	{
		if(activeChar == null || html == null)
			return;

		activeChar.cleanBypasses(true);
		html = activeChar.encodeBypasses(html, true);

		if(html.length() < 8180)
			activeChar.sendPacket(new ShowBoard(html, "101"), CACHE_NULL_102, CACHE_NULL_103);
		else if(html.length() < 8180 * 2)
			activeChar.sendPacket(new ShowBoard(html.substring(0, 8180), "101"), new ShowBoard(html.substring(8180, html.length()), "102"), CACHE_NULL_103);
		else if(html.length() < 8180 * 3)
			activeChar.sendPacket(new ShowBoard(html.substring(0, 8180), "101"), new ShowBoard(html.substring(8180, 8180 * 2), "102"), new ShowBoard(html.substring(8180 * 2, html.length()), "103"));
	}

	public static void send1001(final String html, final L2Player activeChar)
	{
		if(html.length() < 8180)
			activeChar.sendPacket(new ShowBoard(html, "1001"));
	}

	public static void send1002(final L2Player activeChar, final String string, final String string2, final String string3)
	{
		final GArray<String> _arg = new GArray<String>();
		_arg.add("0");
		_arg.add("0");
		_arg.add("0");
		_arg.add("0");
		_arg.add("0");
		_arg.add("0");
		_arg.add(activeChar.getName());
		_arg.add(Integer.toString(activeChar.getObjectId()));
		_arg.add(activeChar.getAccountName());
		_arg.add("9");
		_arg.add(string2);
		_arg.add(string2);
		_arg.add(string);
		_arg.add(string3);
		_arg.add(string3);
		_arg.add("0");
		_arg.add("0");
		activeChar.sendPacket(new ShowBoard(_arg));
	}

	private ShowBoard(final String htmlCode, final String id)
	{
		if(htmlCode != null && htmlCode.length() > 8192) // html code must not exceed 8192 bytes
		{
			_log.warning("Html '" + htmlCode + "' is too long! this will crash the client!");
			_htmlCode = "<html><body>Html was too long</body></html>";
			return;
		}
		_id = id;
		_htmlCode = htmlCode;
	}

	private ShowBoard(final GArray<String> arg)
	{
		_id = "1002";
		_htmlCode = null;
		_arg = arg;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x7b);
		writeC(0x01); // c4 1 to show community 00 to hide
		writeS("bypass _bbshome"); // top
		writeS("bypass _bbsgetfav"); // favorite
		writeS("bypass _bbsloc"); // region
		writeS("bypass _bbsclan"); // clan
		writeS("bypass _bbsmemo"); // memo
		writeS("bypass _bbsmail"); // mail
		writeS("bypass _bbsfriends"); // friends
		writeS("bypass bbs_add_fav"); // add fav.

		String str = _id + "\u0008";
		if(_id.equals("1002"))
			for(final String arg : _arg)
				str += arg + " \u0008";
		else if(_htmlCode != null)
			str += _htmlCode;
		writeS(str);
	}

	@Override
	public String getType()
	{
		return _S__6E_SHOWBOARD;
	}
}
