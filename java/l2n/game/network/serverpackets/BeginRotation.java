package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;

public class BeginRotation extends L2GameServerPacket
{
	private static final String _S__7a_BEGINROTATION = "[S] 7a BeginRotation";
	private final int _charId;
	private final int _degree;
	private final int _side;
	private final int _speed;

	public BeginRotation(final L2Character cha, final int degree, final int side, final int speed)
	{
		_charId = cha.getObjectId();
		_degree = degree;
		_side = side;
		_speed = speed;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x7a);
		writeD(_charId);
		writeD(_degree);
		writeD(_side);
		writeD(_speed);
	}

	@Override
	public String getType()
	{
		return _S__7a_BEGINROTATION;
	}
}
