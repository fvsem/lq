package l2n.game.network.serverpackets;

/**
 * @author L2System Project
 */
public class GMHide extends L2GameServerPacket
{
	private final int _mode;

	/**
	 * @param _mode
	 *            (0 = display windows, 1 = hide windows)
	 */
	public GMHide(final int mode)
	{
		_mode = mode;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x93);
		writeD(_mode);
	}

	@Override
	public String getType()
	{
		return "[S] 93 GMHide";
	}
}
