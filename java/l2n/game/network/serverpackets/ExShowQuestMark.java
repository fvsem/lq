package l2n.game.network.serverpackets;

public class ExShowQuestMark extends L2GameServerPacket
{
	private final int _questId;

	public ExShowQuestMark(final int questId)
	{
		_questId = questId;
	}

	@Override
	public String getType()
	{
		return "[S] FE:21 ExShowQuestMark";
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x21);
		writeD(_questId);
	}
}
