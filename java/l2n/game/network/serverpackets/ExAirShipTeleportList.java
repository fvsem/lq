package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.tables.AirShipDocksTable;
import l2n.game.tables.AirShipDocksTable.AirShipDock;

/**
 * @author L2System Project
 * @date 13.04.2011
 * @time 14:43:46
 */
public class ExAirShipTeleportList extends L2GameServerPacket
{
	private int _fuel;
	private GArray<AirShipDock> _airports;
	private boolean _canWriteImpl = false;

	public ExAirShipTeleportList(final L2AirShip ship)
	{
		final int currentDockNpcId = ship.getCurrentDockNpcId();
		if(currentDockNpcId == 0)
			return;
		_fuel = ship.getFuel();
		_airports = AirShipDocksTable.getInstance().getAirShipDocksForTeleports(currentDockNpcId);
		_canWriteImpl = true;
	}

	@Override
	protected void writeImpl()
	{
		if(!_canWriteImpl)
			return;

		writeC(EXTENDED_PACKET);
		writeH(0x9A);
		writeD(_fuel); // current fuel
		writeD(_airports.size());

		for(final AirShipDock airport : _airports)
		{
			writeD(airport.getId()); // AirportID
			writeD(airport.getFuel()); // need fuel
			writeD(airport.getLoc().x); // Airport x
			writeD(airport.getLoc().y); // Airport y
			writeD(airport.getLoc().z); // Airport z
		}
	}
}
