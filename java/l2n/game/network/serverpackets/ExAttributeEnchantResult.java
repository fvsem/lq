package l2n.game.network.serverpackets;

public class ExAttributeEnchantResult extends L2GameServerPacket
{
	private static final String _S__FE_61_EXATTRIBUTEENCHANTRESULT = "[S] FE:61 ExAttributeEnchantResult";
	private final int _result;

	public static final ExAttributeEnchantResult FAIL = new ExAttributeEnchantResult(2);

	public ExAttributeEnchantResult(final int result)
	{
		_result = result;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xfe);
		writeH(0x61);
		writeD(_result); // 2 - отменено/неудачно
	}

	@Override
	public String getType()
	{
		return _S__FE_61_EXATTRIBUTEENCHANTRESULT;
	}
}
