package l2n.game.network.serverpackets;

import l2n.util.Location;

/**
 * Примеры пакетов:
 * Ставит флажок на карте и показывает стрелку на компасе:
 * EB 00 00 00 00 01 00 00 00 40 2B FF FF 8C 3C 02 00 A0 F6 FF FF
 * Убирает флажок и стрелку
 * EB 02 00 00 00 02 00 00 00 40 2B FF FF 8C 3C 02 00 A0 F6 FF FF
 */
public class RadarControl extends L2GameServerPacket
{
	private static final String _S__f1_RadarControl = "[S] f1 RadarControl";
	private final int _showRadar;
	private final int _type, _x, _y, _z;

	/**
	 * 0xEB RadarControl ddddd
	 */
	public RadarControl(final int showRadar, final int type, final Location loc)
	{
		this(showRadar, type, loc.x, loc.y, loc.z);
	}

	public RadarControl(final int showRadar, final int type, final int x, final int y, final int z)
	{
		_showRadar = showRadar; // showRader?? 0 = showradar; 1 = delete radar;
		_type = type; // radar type??
		_x = x;
		_y = y;
		_z = z;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xf1);
		writeD(_showRadar);
		writeD(_type); // maybe type
		writeD(_x); // x
		writeD(_y); // y
		writeD(_z); // z
	}

	@Override
	public String getType()
	{
		return _S__f1_RadarControl;
	}
}
