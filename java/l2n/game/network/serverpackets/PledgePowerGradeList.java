package l2n.game.network.serverpackets;

import l2n.game.model.L2Clan.RankPrivs;

public class PledgePowerGradeList extends L2GameServerPacket
{
	private static final String _S__FE_3C_PLEDGEPOWERGRADELIST = "[S] FE:3c PledgePowerGradeList";
	private final RankPrivs[] _privs;

	public PledgePowerGradeList(final RankPrivs[] privs)
	{
		_privs = privs;
	}

	@Override
	protected final void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0x3c);
		writeD(_privs.length);
		for(final RankPrivs element : _privs)
		{
			writeD(element.getRank());
			writeD(element.getParty());
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_3C_PLEDGEPOWERGRADELIST;
	}
}
