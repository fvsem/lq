package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;

/**
 * @author L2System Project
 * @date 01.12.2009
 * @time 1:38:23
 */
public class HennaUnequipList extends L2GameServerPacket
{
	private static final String _S__E2_HennaRemoveList = "[S] E6 HennaUnequipList";
	private final L2Player _player;

	public HennaUnequipList(final L2Player player)
	{
		_player = player;
	}

	private final int getHennaUsedSlots()
	{
		switch (_player.getHennaEmptySlots())
		{
			case 0:
				return 3;
			case 1:
				return 2;
			case 2:
				return 1;
				// case 3: return 0;
			default:
				return 0;
		}
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xe6);
		writeQ(_player.getAdena());
		writeD(0x00);
		writeD(getHennaUsedSlots());

		for(int i = 1; i <= 3; i++)
		{
			final L2HennaInstance henna = _player.getHenna(i);
			if(henna != null)
			{
				writeD(henna.getSymbolId());
				writeD(henna.getItemIdDye());
				writeQ(henna.getAmountDyeRequire() / 2);
				writeD(0x00);
				writeQ(henna.getPrice() / 5);
				writeD(0x00);
				writeD(0x01);
			}
		}
	}

	@Override
	public String getType()
	{
		return _S__E2_HennaRemoveList;
	}
}
