package l2n.game.network.serverpackets;

import l2n.game.model.actor.L2Character;
import l2n.util.Location;

public class FlyToLocation extends L2GameServerPacket
{
	private static final String _S_d4_FLYTOLOCATION = "[S] 0xd4 FlyToLocation";
	private final int _chaObjId;
	private final FlyType _type;
	private final Location _loc;
	private final Location _destLoc;

	public enum FlyType
	{
		THROW_UP,
		THROW_HORIZONTAL,
		DUMMY,
		CHARGE,
		NONE
	}

	public FlyToLocation(final L2Character cha, final Location destLoc, final FlyType type)
	{
		_destLoc = destLoc;
		_type = type;
		_chaObjId = cha.getObjectId();
		_loc = cha.getLoc();
	}

	@Override
	protected void writeImpl()
	{
		writeC(0xd4);
		writeD(_chaObjId);
		writeD(_destLoc.x);
		writeD(_destLoc.y);
		writeD(_destLoc.z);
		writeD(_loc.x);
		writeD(_loc.y);
		writeD(_loc.z);
		writeD(_type.ordinal());
	}

	@Override
	public String getType()
	{
		return _S_d4_FLYTOLOCATION;
	}
}
