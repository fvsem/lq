package l2n.game.network.serverpackets;

import l2n.commons.list.GArray;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.items.MailParcelController.Letter;

/**
 * @author L2System Project
 * @date 30.07.2010
 * @time 2:49:15
 */
public class ExShowSentPostList extends L2GameServerPacket
{
	private static final String _S__FE_AC_EXSHOWSENTPOSTLIST = "[S] FE:AC ExShowSentPostList";

	private static final GArray<Letter> EMPTY_LETTERS = new GArray<Letter>(0);
	private GArray<Letter> letters;

	public ExShowSentPostList(final int objId)
	{
		letters = MailParcelController.getInstance().getSent(objId);
		if(letters == null)
			letters = EMPTY_LETTERS;
	}

	@Override
	protected void writeImpl()
	{
		writeC(EXTENDED_PACKET);
		writeH(0xac);
		writeD((int) (System.currentTimeMillis() / 1000));

		writeD(letters.size());
		for(final Letter letter : letters)
		{
			writeD(letter.id);
			writeS(letter.topic);
			writeS(letter.receiverName);
			writeD(letter.type);
			writeD(letter.validtime);
			writeD(letter.unread);
			writeD(0x01);
			writeD(Math.min(letter.attachments, 1));
		}
	}

	@Override
	public String getType()
	{
		return _S__FE_AC_EXSHOWSENTPOSTLIST;
	}
}
