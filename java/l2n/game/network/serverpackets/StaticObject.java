package l2n.game.network.serverpackets;

import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2StaticObjectInstance;

public class StaticObject extends L2GameServerPacket
{
	private static final String _S__9f_STATICOBJECT = "[S] 9f StaticObject";
	private final int _id, _obj_id;
	private final int _type, _isTargetable, _meshIndex;
	private final int _isClosed;
	private final int _isEnemy;
	private final int _maxHp;
	private final int _currentHp;
	private final int _showHp;
	private final int _damageGrade;

	/**
	 * [S]0x9f StaticObjectPacket dd
	 */
	public StaticObject(final L2StaticObjectInstance staticObject)
	{
		_id = staticObject.getStaticObjectId();
		_obj_id = staticObject.getObjectId();
		_type = 0;
		_isTargetable = 1; // Kamael // isTargetable
		_meshIndex = staticObject.getMeshIndex();
		_isClosed = 0;
		_isEnemy = 0;
		_maxHp = 0;
		_currentHp = 0;
		_showHp = 0;
		_damageGrade = 0;
	}

	public StaticObject(final L2DoorInstance door)
	{
		_id = door.getDoorId();
		_obj_id = door.getObjectId();
		_type = 1; // type
		_isTargetable = 1; // isTargtable
		_meshIndex = 1; // meshIndex
		_isClosed = door.isOpen() ? 0 : 1; // opened 0 /closed 1
		_isEnemy = 0;
		_currentHp = (int) door.getCurrentHp();
		_maxHp = door.getMaxHp();
		_showHp = door.isHPVisible() ? 1 : 0; // TODO [G1ta0] статус двери для осаждающих
		_damageGrade = door.getDamage();
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x9f);
		writeD(_id);
		writeD(_obj_id);
		writeD(_type); // type (1-door,0-staticObject0
		writeD(_isTargetable); // isTargetable
		writeD(_meshIndex); // meshIndex
		writeD(_isClosed);
		writeD(_isEnemy);
		writeD(_currentHp);
		writeD(_maxHp);
		writeD(_showHp);
		writeD(_damageGrade);
	}

	@Override
	public String getType()
	{
		return _S__9f_STATICOBJECT;
	}
}
