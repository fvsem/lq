package l2n.game.network.serverpackets;

/**
 * @author minlexx
 */
public class KeyPacket extends L2GameServerPacket
{
	private static final String _S__2e_KEYPACKET = "[S] 2e KeyPacket";

	private final byte[] _key;
	private final int _obfuscationKey;

	public KeyPacket(final byte[] key, final int obfuscationKey)
	{
		_key = key;
		_obfuscationKey = obfuscationKey;
	}

	@Override
	public void writeImpl()
	{
		writeC(0x2e); // 0x2E = KeyPacket opcode
		writeC(_key == null ? 0x00 : 0x01); // 0x01, если версия протокола клиента верна, 0х00 - клиент выводит сообщение о неверном протоколе
		if(_key != null)
		{
			writeB(_key);// первые 8 байт XOR ключа (остальные 8 статичны, клиент сам их знает, как и сервер впрочем)
			writeD(0x01); // неизвестно, всегда 0x01
			writeD(0x01); // тут по идее gameServerID (0x01 для Bartz, итд - клиент игнорит, можно всегда слать 1)
			writeC(0x01); // неизвестно, всегда 0x01
			writeD(_obfuscationKey); // стартовое значение, используемое клиентом для перемешивания таблиц опкодов
		}
	}

	@Override
	public String getType()
	{
		return _S__2e_KEYPACKET;
	}
}
