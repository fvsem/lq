package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.handler.ItemHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.restrictions.PlayerRestrictionType;
import l2n.game.network.serverpackets.ShowCalc;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.skills.EffectType;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.nio.BufferUnderflowException;
import java.util.logging.Level;

public class UseItem extends L2GameClientPacket
{
	private static final String _C__14_USEITEM = "[C] 14 UseItem";

	private int _objectId;
	private boolean ctrl_pressed;

	/**
	 * packet type id 0x19
	 * format: cdd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		try
		{
			_objectId = readD();
			ctrl_pressed = readD() == 1;
		}
		catch(final BufferUnderflowException e)
		{
			_log.log(Level.WARNING, "Attention! Possible cheater found! Login:" + getClient().getLoginName(), e);
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getUnstuck() != 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isInStoreMode())
		{
			activeChar.sendPacket(Msg.YOU_MAY_NOT_USE_ITEMS_IN_A_PRIVATE_STORE_OR_PRIVATE_WORK_SHOP, Msg.ActionFail);
			return;
		}

		// NOTE: disabled due to deadlocks
		synchronized (activeChar.getInventory())
		{
			final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
			if(item == null)
			{
				activeChar.sendActionFailed();
				return;
			}

			// Items that cannot be used
			final int itemId = item.getItemId();
			if(itemId == 57)
			{
				activeChar.sendActionFailed();
				return;
			}

			if(!Config.ALT_GAME_KARMA_PLAYER_CAN_TELEPORT && activeChar.getKarma() > 0 && (itemId == 736 || itemId == 1538 || itemId == 1829 || itemId == 1830 || itemId == 3958 || itemId == 5858 || itemId == 5859 || itemId == 6663 || itemId == 6664 || itemId >= 7117 && itemId <= 7135 || itemId >= 7554 && itemId <= 7559 || itemId == 7618 || itemId == 7619 || itemId == 10129 || itemId == 10130) || itemId >= 13395 && itemId <= 13414)
			{
				activeChar.sendActionFailed();
				return;
			}

			if(activeChar.isFishing() && (itemId < 6535 || itemId > 6540))
			{
				// You cannot do anything else while fishing
				activeChar.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
				return;
			}

			if(activeChar.isDead())
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
				return;
			}

			if(item.getItem().isForPet())
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_EQUIP_A_PET_ITEM).addItemName(itemId));
				return;
			}

			// Маги не могут вызывать Baby Buffalo Improved
			if(Config.ALT_IMPROVED_PETS_LIMITED_USE && activeChar.isMageClass() && item.getItemId() == 10311)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
				return;
			}

			// Войны не могут вызывать Improved Baby Kookaburra
			if(Config.ALT_IMPROVED_PETS_LIMITED_USE && !activeChar.isMageClass() && item.getItemId() == 10313)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
				return;
			}

			if(item.isEquipable())
			{
				if(activeChar.getEffectList().getEffectByType(EffectType.Disarm) != null && item.isWeapon())
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
					return;
				}

				// No unequipping/equipping while the player is in special conditions
				if(activeChar.isCastingNow())
				{
					activeChar.sendPacket(Msg.YOU_CANNOT_USE_EQUIPMENT_WHEN_USING_OTHER_SKILLS_OR_MAGIC);
					return;
				}

				// Нельзя снимать/одевать любое снаряжение при этих условиях
				if(activeChar.isStunned() || activeChar.isSleeping() || activeChar.isParalyzed() || activeChar.isAlikeDead())
				{
					activeChar.sendActionFailed();
					return;
				}

				final int bodyPart = item.getBodyPart();

				// Нельзя снимать/одевать оружие, сидя на пете
				if(activeChar.isMounted() && (bodyPart == L2Item.SLOT_LR_HAND || bodyPart == L2Item.SLOT_L_HAND || bodyPart == L2Item.SLOT_R_HAND))
					return;

				// Нельзя снимать/одевать проклятое оружие
				if(CursedWeaponsManager.getInstance().isCursed(itemId))
					return;

				// Нельзя снимать/одевать проклятое оружие
				if(activeChar.isCursedWeaponEquipped() || activeChar.isCombatFlagEquipped() || activeChar.isTerritoryFlagEquipped())
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(itemId));
					return;
				}

				// Don't allow weapon/shield hero equipment during Olympiads
				if(activeChar.isInOlympiadMode() && item.isHeroItem())
				{
					activeChar.sendActionFailed();
					return;
				}

				if(item.isEquipped())
				{
					activeChar.getInventory().unEquipItemInBodySlotAndNotify(item.getBodyPart(), item);
					return;
				}

				// Equip or unEquip
				activeChar.getInventory().equipItem(item, true);
				if(!item.isEquipped())
				{
					activeChar.sendActionFailed();
					return;
				}

				SystemMessage sm;
				if(item.getEnchantLevel() > 0)
				{
					sm = new SystemMessage(SystemMessage.EQUIPPED__S1_S2);
					sm.addNumber(item.getEnchantLevel());
					sm.addItemName(itemId);
				}
				else
					sm = new SystemMessage(SystemMessage.YOU_HAVE_EQUIPPED_YOUR_S1).addItemName(itemId);

				activeChar.sendPacket(sm);
				return;
			}

			// Don't allow restricted items during Olympiad
			if(activeChar.isInOlympiadMode() && item.isOlyRestrictedItem())
			{
				activeChar.sendPacket(Msg.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT, Msg.ActionFail);
				return;
			}

			// проверяем условия 'ITEM_USE'
			if(!activeChar.getRestrictions().test(PlayerRestrictionType.ITEM_USE, activeChar, null, null, item))
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_AVAILABLE).addItemName(itemId), Msg.ActionFail);
				return;
			}

			// Калькулятор используем)
			if(itemId == 4393)
			{
				activeChar.sendPacket(new ShowCalc(itemId));
				return;
			}

			// использование скилов и скриптов
			if(ItemTable.useHandler(activeChar, item, ctrl_pressed))
				return;

			final IItemHandler handler = ItemHandler.getInstance().getItemHandler(itemId);
			if(handler != null)
				handler.useItem(activeChar, item);
		}
	}

	@Override
	public String getType()
	{
		return _C__14_USEITEM;
	}
}
