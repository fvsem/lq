package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.CharacterSelectionInfo;

/**
 * @author L2System
 * @date 07.07.2010
 * @time 12:17:46
 */
public class RequestGotoLobby extends L2GameClientPacket
{
	private static String _C__D0_38_REQUESTGOTOLOBBY = "[C] D0:38 RequestGotoLobby";

	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final CharacterSelectionInfo cl = new CharacterSelectionInfo(getClient().getLoginName(), getClient().getSessionId().playOkID1);
		sendPacket(cl);
	}

	@Override
	public String getType()
	{
		return _C__D0_38_REQUESTGOTOLOBBY;
	}
}
