package l2n.game.network.clientpackets;

import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.network.serverpackets.SiegeDefenderList;
import l2n.game.tables.ClanTable;

public class RequestConfirmSiegeWaitingList extends L2GameClientPacket
{
	private static String _C__AE_RequestConfirmSiegeWaitingList = "[C] AE RequestConfirmSiegeWaitingList";
	// format: cddd

	private int _Approved;
	private int _CastleId;
	private int _ClanId;

	@Override
	public void readImpl()
	{
		_CastleId = readD();
		_ClanId = readD();
		_Approved = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		// Check if the player has a clan
		if(activeChar.getClan() == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		Residence unit = CastleManager.getInstance().getCastleByIndex(_CastleId);
		if(unit == null)
			unit = FortressManager.getInstance().getFortressByIndex(_CastleId);
		if(unit == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		// Check if leader of the clan who owns the castle?
		if(unit.getOwnerId() != activeChar.getClanId() || !activeChar.isClanLeader())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Clan clan = ClanTable.getInstance().getClan(_ClanId);
		if(clan == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!unit.getSiege().isRegistrationOver())
			if(_Approved == 1)
			{
				if(unit.getSiege().checkIsDefenderWaiting(clan) || unit.getSiege().checkIsDefenderRefused(clan))
					unit.getSiege().approveSiegeDefenderClan(_ClanId);
				else
					return;
			}
			else if(unit.getSiege().checkIsDefenderWaiting(clan) || unit.getSiege().checkIsDefender(clan, false))
				unit.getSiege().refuseSiegeDefenderClan(_ClanId);

		// Update the defender list
		activeChar.sendPacket(new SiegeDefenderList(unit));

	}

	@Override
	public String getType()
	{
		return _C__AE_RequestConfirmSiegeWaitingList;
	}
}
