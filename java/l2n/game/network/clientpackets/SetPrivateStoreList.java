package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.L2TradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.PrivateStoreManageList;
import l2n.game.network.serverpackets.PrivateStoreMsgSell;
import l2n.game.templates.L2Item;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Это список вещей которые игрок хочет продать в создаваемом им приватном магазине
 * Старое название SetPrivateStoreListSell
 */
public class SetPrivateStoreList extends L2GameClientPacket
{
	private final static String _C__31_SETPRIVATESTORELISTSELL = "[C] 31 SetPrivateStoreList";
	// Format: cddb, b = array of (ddd)

	private int _count;
	private boolean _package;
	private long[] _items; // count * 3

	@Override
	public void readImpl()
	{
		_package = readD() == 1;
		_count = readD();
		// Иначе нехватит памяти при создании массива.
		if(_count < 1 || _count > Config.MAX_ITEM_IN_PACKET || _count * 20 != _buf.remaining())
		{
			_items = null;
			return;
		}

		_items = new long[_count * 3];
		for(int i = 0; i < _count; i++)
		{
			final int itemId = readD();
			final long cnt = readQ();
			final long price = readQ();

			if(itemId < 1 || cnt < 1 || price < 0)
			{
				_items = null;
				return;
			}

			_items[i * 3 + 0] = itemId; // objectId
			_items[i * 3 + 1] = cnt; // count
			_items[i * 3 + 2] = price; // price
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_items == null || _count <= 0 || !activeChar.checksForShop(false))
		{
			L2TradeList.cancelStore(activeChar);
			return;
		}

		final int maxSlots = activeChar.getTradeLimit();
		if(_count > maxSlots)
		{
			activeChar.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
			L2TradeList.cancelStore(activeChar);
			activeChar.sendPacket(new PrivateStoreManageList(activeChar, _package));
			return;
		}

		TradeItem temp;
		final ConcurrentLinkedQueue<TradeItem> listsell = new ConcurrentLinkedQueue<TradeItem>();

		int count = _count;
		for(int x = 0; x < _count; x++)
		{
			final int objectId = (int) _items[x * 3 + 0];
			long cnt = _items[x * 3 + 1];
			final long price = _items[x * 3 + 2];

			final L2ItemInstance itemToSell = activeChar.getInventory().getItemByObjectId(objectId);

			if(cnt < 1 || itemToSell == null || !itemToSell.canBeTraded(activeChar) || itemToSell.getItemId() == L2Item.ITEM_ID_ADENA)
			{
				count--;
				continue;
			}

			// If player sells the enchant scroll he is using, deactivate it
			if(activeChar.getActiveEnchantItem() != null && itemToSell.getObjectId() == activeChar.getActiveEnchantItem().getObjectId())
				activeChar.setActiveEnchantItem(null);

			if(cnt > itemToSell.getCount())
				cnt = itemToSell.getCount();

			temp = new TradeItem();
			temp.setObjectId(objectId);
			temp.setCount(cnt);
			temp.setOwnersPrice(price);
			temp.setItemId(itemToSell.getItemId());
			temp.setEnchantLevel(itemToSell.getEnchantLevel());
			temp.setCustomType2(itemToSell.getItem().getType2ForPackets());
			temp.setAttackElement(itemToSell.getAttackElement());
			temp.setDefenceFire(itemToSell.getDefenceFire());
			temp.setDefenceWater(itemToSell.getDefenceWater());
			temp.setDefenceWind(itemToSell.getDefenceWind());
			temp.setDefenceEarth(itemToSell.getDefenceEarth());
			temp.setDefenceHoly(itemToSell.getDefenceHoly());
			temp.setDefenceUnholy(itemToSell.getDefenceUnholy());

			listsell.add(temp);
		}

		if(count != 0)
		{
			activeChar.setSellList(listsell);
			activeChar.setPrivateStoreType(_package ? L2Player.STORE_PRIVATE_SELL_PACKAGE : L2Player.STORE_PRIVATE_SELL);
			activeChar.broadcastPacket(new PrivateStoreMsgSell(activeChar));
			activeChar.sitDown();
			activeChar.broadcastCharInfo();
		}
		else
			L2TradeList.cancelStore(activeChar);
	}

	@Override
	public String getType()
	{
		return _C__31_SETPRIVATESTORELISTSELL;
	}
}
