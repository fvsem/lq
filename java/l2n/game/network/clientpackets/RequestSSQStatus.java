package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.network.serverpackets.SSQStatus;

/**
 * Seven Signs Record Update Request
 * packet type id 0xc8
 * format: cc
 */
public class RequestSSQStatus extends L2GameClientPacket
{
	private static String _C__C8_REQUESTSSQSTATUS = "[C] C8 RequestSSQStatus";

	private int _page;

	@Override
	public void readImpl()
	{
		_page = readC();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if((SevenSigns.getInstance().isSealValidationPeriod() || SevenSigns.getInstance().isCompResultsPeriod()) && _page == 4)
			return;

		sendPacket(new SSQStatus(activeChar, _page));
	}

	@Override
	public String getType()
	{
		return _C__C8_REQUESTSSQSTATUS;
	}
}
