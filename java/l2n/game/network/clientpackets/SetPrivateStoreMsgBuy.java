package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.L2TradeList;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PrivateStoreMsgBuy;
import l2n.util.StringUtil;

import java.util.regex.Pattern;

public class SetPrivateStoreMsgBuy extends L2GameClientPacket
{
	private static String _C__9D_SETPRIVATESTOREMSGBUY = "[C] 9d SetPrivateStoreMsgBuy";
	private String _storename;

	@Override
	public void readImpl()
	{
		_storename = readS(32);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(Config.PRIVATE_STORE_MSG_FILTER)
		{
			String _textCheck = _storename.toLowerCase();
			for(final String[] altLetter : Say2C.ALT_LETTERS)
				_textCheck = _textCheck.replaceAll(altLetter[0], altLetter[1]);

			final String[] arrayText = _textCheck.split(" ");
			final String[] originalText = _storename.split(" ");
			final int length = arrayText.length;

			for(final Pattern pattern : Config.OBSCENE_LIST)
				for(int i = 0; i < length; i++)
					if(pattern.matcher(arrayText[i]).matches())
						originalText[i] = arrayText[i] = Config.MAT_REPLACE_STRING;

			final StringBuilder clearText = StringUtil.startAppend(length);
			for(int i = 0; i < length; i++)
				StringUtil.append(clearText, originalText[i], " ");

			_storename = clearText.toString();;
		}
		String checkName = _storename;
		if(Config.ENABLE_TRADE_BLOCKSPAM)
		{
			checkName = checkName.replace( " ", "" );
			for(String symbol : Config.TRADE_LIST_SYMBOLS)
			{
				checkName = checkName.replace( ""+symbol+"", "" );
			}
			
			for(String nameBlock : Config.TRADE_LIST)
			{
				if(checkName.toLowerCase().contains(nameBlock))
				{
					activeChar.sendMessage("Incorrect store name.");
					return;
				}
			}
		}
		final L2TradeList tradeList = activeChar.getTradeList();
		if(tradeList != null)
			tradeList.setBuyStoreName(_storename);

		sendPacket(new PrivateStoreMsgBuy(activeChar));
	}

	@Override
	public String getType()
	{
		return _C__9D_SETPRIVATESTOREMSGBUY;
	}
}
