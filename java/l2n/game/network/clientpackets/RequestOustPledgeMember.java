package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeShowMemberListDelete;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestOustPledgeMember extends L2GameClientPacket
{
	private static String _C__29_REQUESTOUSTPLEDGEMEMBER = "[C] 29 RequestOustPledgeMember";

	private String _target;

	@Override
	public void readImpl()
	{
		_target = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || !((activeChar.getClanPrivileges() & L2Clan.CP_CL_DISMISS) == L2Clan.CP_CL_DISMISS))
			return;

		final L2Clan clan = activeChar.getClan();
		final L2ClanMember member = clan.getClanMember(_target);
		if(member == null)
		{
			activeChar.sendPacket(Msg.THE_TARGET_MUST_BE_A_CLAN_MEMBER);
			return;
		}

		if(member.isOnline() && member.getPlayer().isInCombat())
		{
			activeChar.sendPacket(Msg.A_CLAN_MEMBER_MAY_NOT_BE_DISMISSED_DURING_COMBAT);
			return;
		}

		if(member.isClanLeader())
		{
			activeChar.sendMessage("A clan leader may not be dismissed.");
			return;
		}

		clan.removeClanMember(member.getObjectId());
		clan.broadcastToOnlineMembers(new SystemMessage(SystemMessage.CLAN_MEMBER_S1_HAS_BEEN_EXPELLED).addString(_target), new PledgeShowMemberListDelete(_target));

		// при исключении членов Академии штрафы на клан не накладываются
		if(member.getPledgeType() != L2Clan.SUBUNIT_ACADEMY)
			clan.setExpelledMember();

		final L2Player player = member.getPlayer();
		if(player != null)
		{
			if(player.getPledgeType() == L2Clan.SUBUNIT_ACADEMY)
				player.setLvlJoinedAcademy(0);
			else
				// при исключении членов Академии штрафы на персонажа не накладываются
				player.setLeaveClanCurTime();
			player.setClan(null);
			if(!player.isNoble())
				player.setTitle("");

			player.broadcastUserInfo(true);
			player.broadcastRelationChanged();

			// disable clan tab
			player.sendPacket(Msg.YOU_HAVE_RECENTLY_BEEN_DISMISSED_FROM_A_CLAN_YOU_ARE_NOT_ALLOWED_TO_JOIN_ANOTHER_CLAN_FOR_24_HOURS, Msg.PledgeShowMemberListDeleteAll);
		}
	}

	@Override
	public String getType()
	{
		return _C__29_REQUESTOUSTPLEDGEMEMBER;
	}
}
