package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;

/**
 * This class ...
 * 
 * @version $Revision: 1.3.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestWithDrawalParty extends L2GameClientPacket
{
	private static String _C__44_REQUESTWITHDRAWALPARTY = "[C] 44 RequestWithDrawalParty";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!activeChar.isInParty() || !activeChar.canLeaveParty())
			return;

		final Reflection r = activeChar.getParty().getReflection();
		if(r != null && r.isDimensionalRift() && activeChar.getReflection().equals(r))
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestWithDrawalParty.Rift", activeChar));
		else if(r != null && activeChar.isInCombat())
			activeChar.sendMessage("Вы не можете сейчас выйти из группы.");
		else
			activeChar.getParty().oustPartyMember(activeChar);
	}

	@Override
	public String getType()
	{
		return _C__44_REQUESTWITHDRAWALPARTY;
	}
}
