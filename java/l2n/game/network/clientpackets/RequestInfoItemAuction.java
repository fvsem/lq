package l2n.game.network.clientpackets;

import l2n.game.instancemanager.itemauction.ItemAuction;
import l2n.game.instancemanager.itemauction.ItemAuctionInstance;
import l2n.game.instancemanager.itemauction.ItemAuctionManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExItemAuctionInfoPacket;

/**
 * @<a href="http://L2System/">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 10:52:33
 */
public class RequestInfoItemAuction extends L2GameClientPacket
{
	private int _instanceId;

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#readImpl()
	 */
	@Override
	protected void readImpl()
	{
		_instanceId = readD();
	}

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#runImpl()
	 */
	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(System.currentTimeMillis() - activeChar.getLastItemAuctionPacket() < 2000)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.setLastItemAuctionPacket();

		final ItemAuctionInstance instance = ItemAuctionManager.getInstance().getManagerInstance(_instanceId);
		if(instance == null)
			return;

		final ItemAuction auction = instance.getCurrentAuction();
		if(auction == null)
			return;

		activeChar.sendPacket(new ExItemAuctionInfoPacket(true, auction, instance.getNextAuction()));
	}

	@Override
	public final String getType()
	{
		return "[C] D0:3A RequestBidItemAuction";
	}
}
