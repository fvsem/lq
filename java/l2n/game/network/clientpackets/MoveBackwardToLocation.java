package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.send.ChangeAccessLevel;
import l2n.game.model.actor.L2Player;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.CharMoveToLocation;
import l2n.util.Location;

// cdddddd(d)
public class MoveBackwardToLocation extends L2GameClientPacket
{
	private final static String _C__0F_MOVEBACKWARDTOLOC = "[C] 0F MoveBackwardToLoc";

	private final Location _targetLoc;
	private final Location _originLoc;
	private int _moveMovement;

	public MoveBackwardToLocation()
	{
		_targetLoc = new Location();
		_originLoc = new Location();
	}

	/**
	 * packet type id 0x0f
	 */
	@Override
	public void readImpl()
	{
		_targetLoc.x = readD();
		_targetLoc.y = readD();
		_targetLoc.z = readD();

		_originLoc.x = readD();
		_originLoc.y = readD();
		_originLoc.z = readD();

		final L2GameClient client = getClient();
		final L2Player activeChar = client.getActiveChar();
		if(activeChar == null)
			return;

		if(_buf.hasRemaining())
			_moveMovement = readD();
		else
		{
			_log.warning("Incompatible client found: L2Walker? " + client.getLoginName() + "/" + client.getIpAddr());
			if(Config.L2WALKER_PUNISHMENT != 0)
			{
				if(Config.L2WALKER_PUNISHMENT == 2)
				{
					LSConnection.getInstance().sendPacket(new ChangeAccessLevel(client.getLoginName(), -66, "Walker Autoban", -1));
					activeChar.setAccessLevel(-66);
				}
				activeChar.logout(false, false, true, true);
			}
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		activeChar.setLoc(_originLoc);//Enderkind: GeoMove fix

		if(_moveMovement == 0 && (!Config.ALLOW_KEYBOARD_MOVE || activeChar.getReflection().getId() > 0))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(System.currentTimeMillis() - activeChar.getLastMovePacket() < Config.MOVE_PACKET_DELAY)
		{
			activeChar.sendActionFailed();
			return;
		}
		activeChar.setLastMovePacket();

		if(activeChar.isTeleporting())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.inObserverMode())
		{
			if(activeChar.getOlympiadObserveId() == -1)
				activeChar.sendActionFailed();
			else
				activeChar.sendPacket(new CharMoveToLocation(activeChar.getObjectId(), _originLoc, _targetLoc));
			return;
		}

		if(activeChar.isOutOfControl() && activeChar.getOlympiadGameId() == -1)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getTeleMode() > 0)
		{
			if(activeChar.getTeleMode() == 1)
				activeChar.setTeleMode(0);
			activeChar.sendActionFailed();
			activeChar.teleToLocation(_targetLoc);
			return;
		}

		final int water_z = activeChar.getWaterZ();
		if(water_z != Integer.MIN_VALUE && _targetLoc.z > water_z)
			_targetLoc.z = water_z;

		if(activeChar.isInFlyingTransform())
			_targetLoc.z = Math.min(5950, Math.max(50, _targetLoc.z));

		if(activeChar.isInVehicle())
		{
			if(activeChar.getDistance(_targetLoc) > 400)
			{
				activeChar.sendActionFailed();
				return;
			}
			activeChar.setVehicle(null);
			activeChar.setLastClientPosition(null);
			activeChar.setLastServerPosition(null);
			activeChar.setLoc(_targetLoc.changeZ(64).correctGeoZ());
			activeChar.validateLocation(1);
			activeChar.stopMove(false);
			activeChar.sendActionFailed();
			return;
		}

		L2GameThreadPools.getInstance().executeMove(new Runnable()
		{
			@Override
			public void run()
			{
				activeChar.moveToLocation(_targetLoc, 0, _moveMovement != 0);
			}
		});
	}

	@Override
	public String getType()
	{
		return _C__0F_MOVEBACKWARDTOLOC;
	}
}
