package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExVariationResult;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.tables.AugmentationData;

public final class RequestRefine extends AbstractRefinePacket
{
	private static final String _C__D0_2C_REQUESTREFINE = "[C] D0:2C RequestRefine";

	private int _targetItemObjId;
	private int _refinerItemObjId;
	private int _gemStoneItemObjId;
	private long _gemStoneCount;

	@Override
	protected void readImpl()
	{
		_targetItemObjId = readD();
		_refinerItemObjId = readD();
		_gemStoneItemObjId = readD();
		_gemStoneCount = readQ();
	}

	@Override
	protected void runImpl()
	{
		if(_gemStoneCount <= 0)
			return;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance targetItem = activeChar.getInventory().getItemByObjectId(_targetItemObjId);
		final L2ItemInstance refinerItem = activeChar.getInventory().getItemByObjectId(_refinerItemObjId);
		final L2ItemInstance gemStoneItem = activeChar.getInventory().getItemByObjectId(_gemStoneItemObjId);

		if(targetItem == null || refinerItem == null || gemStoneItem == null || !isValid(activeChar, targetItem, refinerItem, gemStoneItem))
		{
			activeChar.sendPacket(new ExVariationResult(0, 0, 0), Msg.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS);
			return;
		}

		if(targetItem.isEquipped())
			activeChar.getInventory().unEquipItem(targetItem);

		if(tryAugmentItem(activeChar, targetItem, refinerItem, gemStoneItem))
		{
			final int stat12 = 0x0000FFFF & targetItem.getAugmentation().getAugmentationId();
			final int stat34 = targetItem.getAugmentation().getAugmentationId() >> 16;
			activeChar.sendPacket(new ExVariationResult(stat12, stat34, 1), Msg.THE_ITEM_WAS_SUCCESSFULLY_AUGMENTED);
		}
		else
			activeChar.sendPacket(new ExVariationResult(0, 0, 0), Msg.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS);
	}

	private boolean tryAugmentItem(final L2Player player, final L2ItemInstance targetItem, final L2ItemInstance refinerItem, final L2ItemInstance gemStoneItem)
	{

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return false;

		final LifeStone ls = getLifeStone(refinerItem.getItemId());
		if(ls == null)
			return false;

		final int lifeStoneLevel = ls.getLevel();
		final int lifeStoneGrade = ls.getGrade();
		if(_gemStoneCount != getGemStoneCount(targetItem.getItem().getItemGrade(), lifeStoneGrade))
			return false;

		if(!isValid(activeChar))
			return false;

		// consume items
		player.getInventory().destroyItem(_gemStoneItemObjId, _gemStoneCount, true);
		player.getInventory().destroyItem(refinerItem, 1, true);

		targetItem.setAugmentation(AugmentationData.getInstance().generateRandomAugmentation(lifeStoneLevel, lifeStoneGrade, targetItem));

		if(targetItem.isEquipped())
			targetItem.getAugmentation().applyBoni(player);

		// send an inventory update packet
		player.sendPacket(new InventoryUpdate(targetItem, L2ItemInstance.MODIFIED));
		player.sendChanges();
		return true;
	}

	@Override
	public String getType()
	{
		return _C__D0_2C_REQUESTREFINE;
	}
}
