package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.network.serverpackets.ExGetOnAirShip;
import l2n.util.Location;

/**
 * @author L2System
 * @date 10.12.2009
 * @time 23:27:30
 */
public class RequestExGetOnAirShip extends L2GameClientPacket
{
	private int _shipId;
	private Location loc;

	@Override
	protected void readImpl()
	{
		loc.x = readD();
		loc.y = readD();
		loc.z = readD();
		_shipId = readD();
	}

	@Override
	protected void runImpl()
	{
		System.out.println("[T1:ExGetOnAirShip] loc: " + loc);
		System.out.println("[T1:ExGetOnAirShip] ship ID: " + _shipId);
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		final L2AirShip boat = (L2AirShip) L2VehicleManager.getInstance().getBoat(_shipId);
		if(boat == null)
			return;
		activeChar.stopMove();
		activeChar.setVehicle(boat);
		activeChar.setInVehiclePosition(loc);
		activeChar.setLoc(boat.getLoc());
		activeChar.broadcastPacket(new ExGetOnAirShip(activeChar, boat, loc));
	}
}
