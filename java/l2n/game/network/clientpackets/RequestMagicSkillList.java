package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SkillList;

public class RequestMagicSkillList extends L2GameClientPacket
{
	private static String _C__38_REQUESTMAGICSKILLLIST = "[C] 38 RequestMagicSkillList";

	// private static Logger _log = Logger.getLogger(RequestSkillList.class.getName());

	/**
	 * packet type id 0x38
	 * format: c
	 * 
	 * @param rawPacket
	 */

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		sendPacket(new SkillList(activeChar));
	}

	@Override
	public String getType()
	{
		return _C__38_REQUESTMAGICSKILLLIST;
	}
}
