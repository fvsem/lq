package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.CrestCache;
import l2n.game.network.serverpackets.AllianceCrest;

import java.util.logging.Logger;

public class RequestAllyCrest extends L2GameClientPacket
{
	private static String _C__92_REQUESTALLYCREST = "[C] 92 RequestAllyCrest";
	// format: cd
	private static Logger _log = Logger.getLogger(RequestAllyCrest.class.getName());

	private int _crestId;

	@Override
	public void readImpl()
	{
		_crestId = readD();
	}

	@Override
	public void runImpl()
	{
		if(_crestId == 0)
			return;
		if(Config.DEBUG)
			_log.fine("allycrestid " + _crestId + " requested");
		final byte[] data = CrestCache.getAllyCrest(_crestId);
		if(data != null)
		{
			final AllianceCrest ac = new AllianceCrest(_crestId, data);
			sendPacket(ac);
		}
		else if(Config.DEBUG)
			_log.fine("allycrest is missing:" + _crestId);
	}

	@Override
	public String getType()
	{
		return _C__92_REQUESTALLYCREST;
	}
}
