package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.AdminCommandHandler;
import l2n.game.handler.interfaces.IAdminCommandHandler;
import l2n.game.model.actor.L2Player;
import l2n.util.IllegalPlayerAction;
import l2n.util.Util;

import java.util.logging.Level;

/**
 * This class handles all GM commands triggered by //command
 */
public class SendBypassBuildCmd extends L2GameClientPacket
{
	private final static String _C__74_SENDBYPASSBUILDCMD = "[C] 74 SendBypassBuildCmd";

	private String _command;

	@Override
	public void readImpl()
	{
		_command = readS();
		if(_command != null)
			_command = _command.trim();

	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!(activeChar.getPlayerAccess().IsGM || activeChar.getPlayerAccess().UseGMComand))
		{
			_log.log(Level.SEVERE, "Non GM Player '" + activeChar.getName() + "', trying to exploit admin cmd: " + _command + ", IP: " + getClient().getIpAddr());
			return;
		}

		if(!activeChar.isOnline())
		{
			_log.log(Level.SEVERE, "Non Online GM '" + activeChar.getName() + "', trying to exploit admin cmd: " + _command + ", IP: " + getClient().getIpAddr());
			return;
		}

		final String cmd = "admin_" + _command;
		final IAdminCommandHandler ach = AdminCommandHandler.getInstance().getAdminCommandHandler("admin_" + _command.split(" ")[0]);
		if(ach != null)
		{
			if(!(activeChar.getPlayerAccess().IsGM || activeChar.getPlayerAccess().UseGMComand))
			{
				Util.handleIllegalPlayerAction(activeChar, "SendBypassBuildCmd[42]", "user use adm command admin_" + _command, IllegalPlayerAction.INFO);
				return;
			}
			try
			{
				ach.useAdminCommand(cmd, activeChar);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			return;
		}
		activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.SendBypassBuildCmd.NoCommandOrAccess", activeChar).addString(_command));
	}

	@Override
	public String getType()
	{
		return _C__74_SENDBYPASSBUILDCMD;
	}
}
