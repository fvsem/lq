package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Duel;

public class RequestDuelSurrender extends L2GameClientPacket
{
	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player p = getClient().getActiveChar();

		if(p == null)
			return;

		final Duel d = p.getDuel();

		if(d == null)
			return;

		d.doSurrender(p);
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return "[C] D0:48 RequestDuelSurrender";
	}
}
