package l2n.game.network.clientpackets;

import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2WorldRegion;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;

import java.util.logging.Level;

public class Action extends L2GameClientPacket
{
	private static final String ACTION__C__1F = "[C] 1F Action";

	private int _objectId;
	private int _actionId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		readD();
		readD();
		readD();
		_actionId = readC();// 0 for simple click 1 for shift click
	}

	@Override
	public void runImpl()
	{
		try
		{
			final L2Player activeChar = getClient().getActiveChar();
			if(activeChar == null)
				return;

			if(activeChar.isOutOfControl())
			{
				activeChar.sendActionFailed();
				return;
			}

			if(activeChar.inObserverMode() && activeChar.getObserverRegion() != null)
				for(final L2WorldRegion region : activeChar.getObserverRegion().getNeighbors())
					for(final L2Object obj : region.getObjectsList(new GArray<L2Object>(region.getObjectsSize()), activeChar.getObjectId(), activeChar.getReflection()))
						if(obj != null && obj.getObjectId() == _objectId && activeChar.getTarget() != obj)
						{
							obj.onAction(activeChar, false);
							return;
						}

			if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			{
				activeChar.sendActionFailed();
				return;
			}

			L2Object obj = activeChar.getVisibleObject(_objectId);
			// Для провалившихся предметов, чтобы можно было все равно поднять
			if(obj == null && ((obj = L2ObjectsStorage.getItemByObjId(_objectId)) == null || !activeChar.isInRange(obj, 1000)))
			{
				activeChar.sendActionFailed();
				return;
			}

			if(activeChar.getAggressionTarget() != null && activeChar.getAggressionTarget() != obj)
			{
				activeChar.sendPacket(Msg.FAILED_TO_CHANGE_ATTACK_TARGET, Msg.ActionFail);
				return;
			}

			switch (_actionId)
			{
				case 0:
					if(obj.isMonster() && (((L2MonsterInstance) obj).isDying() || ((L2MonsterInstance) obj).isDead()))
						activeChar.sendActionFailed();
					else
						obj.onAction(activeChar, false);
					break;
				case 1:
					if(obj.isCharacter() && ((L2Character) obj).isAlikeDead())
						obj.onAction(activeChar, false);
					else
						obj.onAction(activeChar, true);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
	}

	@Override
	public String getType()
	{
		return ACTION__C__1F;
	}
}
