package l2n.game.network.clientpackets;

import l2n.game.model.L2ManufactureItem;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.RecipeShopItemInfo;

/**
 * cdd
 */
public class RequestRecipeShopMakeInfo extends L2GameClientPacket
{
	private static String _C__BE_RequestRecipeShopMakeInfo = "[C] BE RequestRecipeShopMakeInfo";

	private int _playerObjectId;
	private int _recipeId;

	@Override
	public void readImpl()
	{
		_playerObjectId = readD();
		_recipeId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getDuel() != null)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Player target = (L2Player) activeChar.getVisibleObject(_playerObjectId);

		if(target == null || target.getPrivateStoreType() != L2Player.STORE_PRIVATE_MANUFACTURE)
		{
			activeChar.sendActionFailed();
			return;
		}

		long price = 0xFFFFFFFF;
		for(final L2ManufactureItem i : target.getCreateList().getList())
			if(i.getRecipeId() == _recipeId)
			{
				price = i.getCost();
				break;
			}

		if(price == 0xFFFFFFFF)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.sendPacket(new RecipeShopItemInfo(_playerObjectId, _recipeId, price, 0xFFFFFFFF, activeChar));
	}

	@Override
	public String getType()
	{
		return _C__BE_RequestRecipeShopMakeInfo;
	}
}
