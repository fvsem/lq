package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestDestroyQuest extends L2GameClientPacket
{
	private static String _C__63_REQUESTQUESTABORT = "[C] 63 RequestDestroyQuest";

	private int _questId;

	/**
	 * packet type id 0x63
	 * format: cd
	 */
	@Override
	public void readImpl()
	{
		_questId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || QuestManager.getQuest(_questId) == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(activeChar.isOutOfControl())
		{
			activeChar.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		final QuestState qs = activeChar.getQuestState(QuestManager.getQuest(_questId).getName());
		if(qs != null && !qs.isCompleted())
		{
			qs.abortQuest();
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_ABORTED).addString(QuestManager.getQuest(_questId).getDescr(activeChar)));
		}
	}

	@Override
	public String getType()
	{
		return _C__63_REQUESTQUESTABORT;
	}
}
