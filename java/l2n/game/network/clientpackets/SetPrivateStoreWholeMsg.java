package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.L2TradeList;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PrivateStoreMsgSell;

/**
 * @author L2System Project
 * @date 11.12.2009
 * @time 1:35:44
 */
public class SetPrivateStoreWholeMsg extends L2GameClientPacket
{
	private String _storename;

	@Override
	public void readImpl()
	{
		_storename = readS(32);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2TradeList tradeList = activeChar.getTradeList();
		if(tradeList == null)
			return;
		
		String checkName = _storename;
		if(Config.ENABLE_TRADE_BLOCKSPAM)
		{
			checkName = checkName.replace( " ", "" );
			for(String symbol : Config.TRADE_LIST_SYMBOLS)
			{
				checkName = checkName.replace( ""+symbol+"", "" );
			}
			
			for(String nameBlock : Config.TRADE_LIST)
			{
				if(checkName.toLowerCase().contains(nameBlock))
				{
					activeChar.sendMessage("Incorrect store name.");
					return;
				}
			}
		}
		tradeList.setSellStoreName(_storename);
		sendPacket(new PrivateStoreMsgSell(activeChar, true));
	}
}
