package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExPutEnchantSupportItemResult;

/**
 * @author L2System
 * @date 03.12.2009
 * @time 22:10:14
 */
public class RequestExTryToPutEnchantSupportItem extends AbstractEnchantPacket
{
	private static final String _C__D0_80_REQUESTEXTRYTOPUTENCHANTSUPPORTITEM = "[C] D0 50 RequestExTryToPutEnchantSupportItem";

	private int _enchantObjectId;
	private int _supportObjectId;

	@Override
	protected void readImpl()
	{
		_supportObjectId = readD();
		_enchantObjectId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!activeChar.isEnchanting() || activeChar.isOutOfControl() || activeChar.isActionsDisabled())
			return;

		final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_enchantObjectId);
		final L2ItemInstance support = activeChar.getInventory().getItemByObjectId(_supportObjectId);

		if(item == null || support == null)
			return;

		final EnchantItem supportTemplate = getSupportItem(support);

		if(supportTemplate == null || !supportTemplate.isValid(activeChar, item))
		{
			// message may be custom
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS);
			activeChar.setActiveEnchantSupportItem(null);
			activeChar.sendPacket(ExPutEnchantSupportItemResult.FAIL);
			return;
		}

		activeChar.setActiveEnchantSupportItem(support);
		activeChar.sendPacket(ExPutEnchantSupportItemResult.SACCESS);
	}

	@Override
	public String getType()
	{
		return _C__D0_80_REQUESTEXTRYTOPUTENCHANTSUPPORTITEM;
	}
}
