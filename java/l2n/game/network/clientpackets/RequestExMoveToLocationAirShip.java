package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2VehiclePoint;
import l2n.game.tables.AirShipDocksTable;
import l2n.game.tables.AirShipDocksTable.AirShipDock;

/**
 
 * @date 13.04.2011
 * @time 15:41:17
 */
public class RequestExMoveToLocationAirShip extends L2GameClientPacket
{
	@Override
	protected void runImpl()
	{}

	@Override
	protected void readImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || activeChar.getVehicle() == null || !activeChar.getVehicle().isAirShip())
			return;

		final int MoveType = readD();
		final L2AirShip airship = (L2AirShip) activeChar.getVehicle();
		if(airship.getDriver() == activeChar)
			switch (MoveType)
			{
				case 4: // AirShipTeleport
					final int dockID = readD();
					final int currentDockNpcId = airship.getCurrentDockNpcId();
					final AirShipDock curAD = AirShipDocksTable.getInstance().getAirShipDockByNpcId(currentDockNpcId);
					if(curAD == null)
					{
						_log.warning("RequestExMoveToLocationAirShip: null dock for npc " + currentDockNpcId);
						return;
					}
					if(curAD.getId() == dockID)
					{
						airship.SetTrajet1(curAD.getDepartureTrajetId(), 0, null, null);
						airship._cycle = 1;
						airship.begin();
					}
					else
					{
						airship.SetTrajet1(curAD.getDepartureTrajetId(), 0, null, null);

						final L2VehiclePoint bp = new L2VehiclePoint();
						bp.speed1 = airship._speed1;
						bp.speed2 = airship._speed2;
						final AirShipDock destAD = AirShipDocksTable.getInstance().getAirShipDock(dockID);
						bp.x = destAD.getLoc().x;
						bp.y = destAD.getLoc().y;
						bp.z = destAD.getLoc().z;
						bp.heading = 1;

						if(airship.getFuel() < destAD.getFuel())
						{
							activeChar.sendMessage("Not enough EP."); // TODO правильное сообщение
							return;
						}

						airship.getTrajet1().addPathPoint(bp);
						airship._cycle = 1;
						airship.begin();
						airship.setFuel(airship.getFuel() - destAD.getFuel());
					}
					break;
				case 0: // Free move
					if(airship.isDocked() || !airship.isArrived())
						break;
					airship.moveToLocation(airship.getLoc().setX(readD()).setY(readD()), 0, false);
					break;
				case 2: // Up
					if(airship.isDocked() || !airship.isArrived())
						break;
					readD(); // ?
					readD(); // ?
					airship.moveToLocation(airship.getLoc().changeZ(100), 0, false);
					break;
				case 3: // Down
					if(airship.isDocked() || !airship.isArrived())
						break;
					readD(); // ?
					readD(); // ?
					airship.moveToLocation(airship.getLoc().changeZ(-100), 0, false);
					break;
			}
	}
}
