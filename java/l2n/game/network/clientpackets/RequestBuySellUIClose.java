package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ItemList;

/**
 * @author L2System
 * @date 12.07.2010
 * @time 18:29:45
 */
public class RequestBuySellUIClose extends L2GameClientPacket
{
	@Override
	public void runImpl()
	{}

	@Override
	public void readImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || !activeChar.getPlayerAccess().UseInventory || activeChar.isInventoryDisabled())
			return;

		activeChar.setBuyListId(0);
		activeChar.sendPacket(new ItemList(activeChar, true));
	}
}
