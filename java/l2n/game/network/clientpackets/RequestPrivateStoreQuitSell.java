package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SendTradeDone;

public class RequestPrivateStoreQuitSell extends L2GameClientPacket
{
	private static String _C__76_REQUESTPRIVATESTOREQUITSELL = "[C] 96 RequestPrivateStoreQuitSell";

	@Override
	public void readImpl()
	{
		// trigger
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTradeList() != null)
		{
			activeChar.getTradeList().removeAll();
			activeChar.sendPacket(new SendTradeDone(0));
			activeChar.setTradeList(null);
			if(activeChar.isInTransaction())
				activeChar.getTransaction().cancel();
		}
		activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
		activeChar.standUp();
		activeChar.broadcastCharInfo();
	}

	@Override
	public String getType()
	{
		return _C__76_REQUESTPRIVATESTOREQUITSELL;
	}
}
