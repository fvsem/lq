package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 16.12.2009
 * @time 0:36:28
 */
public class RequestTeleportBookMark extends L2GameClientPacket
{
	private static final String _C__51_REQUESTTELEPORTBOOKMARK = "[C] 51 RequestTeleportBookMark";

	private int id;

	@Override
	protected void readImpl()
	{
		id = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.teleportBookmarkGo(id);
		activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTTELEPORTBOOKMARK;
	}
}
