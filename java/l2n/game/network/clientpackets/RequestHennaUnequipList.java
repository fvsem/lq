package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.HennaUnequipList;

/**
 * @author L2System
 * @date 01.03.2010
 * @time 1:15:31
 */
public class RequestHennaUnequipList extends L2GameClientPacket
{
	private static final String _C__70_RequestHennaRemoveList = "[C] 70 RequestHennaUnequipList";

	@Override
	protected void readImpl()
	{
		/* _unknown = */readD(); // ?? just a trigger packet
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		sendPacket(new HennaUnequipList(activeChar));
	}

	@Override
	public String getType()
	{
		return _C__70_RequestHennaRemoveList;
	}
}
