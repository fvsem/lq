package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;

public class RequestCrystallizeItem extends L2GameClientPacket
{
	private static String _C__2F_REQUESTDCRYSTALLIZEITEM = "[C] 2F RequestCrystallizeItem";

	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		readQ();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM, Msg.ActionFail);
			return;
		}

		final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
		if(item == null || !item.canBeCrystallized(activeChar, true) || item.isWear())
		{
			activeChar.sendActionFailed();
			return;
		}

		crystallize(activeChar, item);
	}

	public static void crystallize(final L2Player activeChar, final L2ItemInstance item)
	{
		activeChar.getInventory().destroyItem(item, 1, true);

		// add crystals
		final int crystalAmount = item.getItem().getCrystalCount();
		final int crystalId = item.getItem().getCrystalType().cry;

		final L2ItemInstance createditem = ItemTable.getInstance().createItem(crystalId, activeChar.getObjectId(), item.getItem().getItemId(), "RequestCrystallizeItem[83]");
		createditem.setCount(crystalAmount);
		final L2ItemInstance addedItem = activeChar.getInventory().addItem(createditem);
		activeChar.sendPacket(Msg.THE_ITEM_HAS_BEEN_SUCCESSFULLY_CRYSTALLIZED, new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S2_S1).addItemName(crystalId).addNumber(crystalAmount));

		Log.LogItem(activeChar, Log.CrystalizeItem, item);
		Log.LogItem(activeChar, Log.Sys_GetItem, addedItem);

		activeChar.updateStats();

		L2World.removeObject(item);
	}

	@Override
	public String getType()
	{
		return _C__2F_REQUESTDCRYSTALLIZEITEM;
	}
}
