package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.model.Pinger;

/**
 * @author L2System
 * @date 15.02.2011
 * @time 0:42:10
 */
public class NetPing extends L2GameClientPacket
{
	private int objectId;
	private int ping;
	private int mtu;

	@Override
	protected void readImpl()
	{
		objectId = readD();
		ping = readD();
		mtu = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
			Pinger.getInstance().answerPing(activeChar.getObjectId());
	}

	@Override
	public String getType()
	{
		return "[C] B1 NetPing";
	}
}
