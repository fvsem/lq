package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExRpItemLink;

public class RequestExRqItemLink extends L2GameClientPacket
{
	private static String RequestExRqItemLink = "[C] D0:1E RequestExRqItemLink";
	// format: (ch)d
	int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
			activeChar.sendPacket(new ExRpItemLink(_objectId));
	}

	@Override
	public String getType()
	{
		return RequestExRqItemLink;
	}
}
