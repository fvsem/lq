package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2CubicInstance;
import l2n.game.model.instances.L2CubicInstance.CubicType;

/**
 * This class ...
 * 
 * @version $Revision: 1.3.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestTargetCanceld extends L2GameClientPacket
{
	private static String _C__48_REQUESTTARGETCANCELD = "[C] 48 RequestTargetCanceld";

	private int _unselect;

	/**
	 * packet type id 0x48
	 * format: ch
	 * 
	 * @param rawPacket
	 */
	@Override
	public void readImpl()
	{
		_unselect = readH();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getAggressionTarget() != null)
		{
			activeChar.sendPacket(Msg.FAILED_TO_DISABLE_ATTACK_TARGET);
			return;
		}

		if(activeChar.getUnstuck() == 1)
		{
			activeChar.setUnstuck(0);
			activeChar.unblock();
		}

		for(final L2CubicInstance cubic : activeChar.getCubics())
			if(cubic.getType() != CubicType.LIFE_CUBIC)
				cubic.stopAttackAction();

		if(activeChar.getAgathion() != null)
			activeChar.getAgathion().stopAction();

		if(_unselect == 0)
		{
			if(activeChar.isCastingNow())
				activeChar.abortCast();
			else if(activeChar.getTarget() != null)
				activeChar.setTarget(null);
		}
		else if(activeChar.getTarget() != null)
			activeChar.setTarget(null);
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.clientpackets.ClientBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__48_REQUESTTARGETCANCELD;
	}
}
