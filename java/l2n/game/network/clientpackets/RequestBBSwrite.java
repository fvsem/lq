package l2n.game.network.clientpackets;

import l2n.game.communitybbs.CommunityBoard;

/**
 * Format SSSSSS
 */
public class RequestBBSwrite extends L2GameClientPacket
{
	private static String _C__24_REQUESTBBSWRITE = "[C] 24 RequestBBSwrite";
	private String _url;
	private String _arg1;
	private String _arg2;
	private String _arg3;
	private String _arg4;
	private String _arg5;

	/**
	 * Format cSSSSSS
	 * 
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_url = readS();
		_arg1 = readS();
		_arg2 = readS();
		_arg3 = readS();
		_arg4 = readS();
		_arg5 = readS();
	}

	@Override
	public void runImpl()
	{
		CommunityBoard.handleWriteCommands(getClient(), _url, _arg1, _arg2, _arg3, _arg4, _arg5);
	}

	@Override
	public String getType()
	{
		return _C__24_REQUESTBBSWRITE;
	}
}
