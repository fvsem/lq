package l2n.game.network.clientpackets;

public class RequestCreatePledge extends L2GameClientPacket
{
	private static String _C__25_REQUESTCREATEPLEDGE = "[C] 25 RequestCreatePledge";
	// Format: cS
	private String _pledgename;

	@Override
	public void readImpl()
	{
		_pledgename = readS();
	}

	@Override
	public void runImpl()
	{
		System.out.println("Unfinished packet: " + _C__25_REQUESTCREATEPLEDGE);
		System.out.println("  S: " + _pledgename);
	}

	@Override
	public String getType()
	{
		return _C__25_REQUESTCREATEPLEDGE;
	}
}
