package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2TradeList;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.SendTradeDone;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.IllegalPlayerAction;
import l2n.util.Util;

/**
 * This class ...
 * 
 * @version $Revision: 1.6.2.2.2.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class TradeDone extends L2GameClientPacket
{
	private static String _C__1C_TRADEDONE = "[C] 1C TradeDone";

	private int _response;

	@Override
	public void readImpl()
	{
		_response = readD();
	}

	@Override
	public void runImpl()
	{
		synchronized (getClient())
		{
			final L2Player activeChar = getClient().getActiveChar();
			if(activeChar == null)
				return;

			final Transaction transaction = activeChar.getTransaction();
			L2Player requestor;
			if(transaction == null || (requestor = transaction.getOtherPlayer(activeChar)) == null)
			{
				if(transaction != null)
					transaction.cancel();
				activeChar.sendPacket(SendTradeDone.Fail);
				activeChar.sendActionFailed();
				return;
			}

			if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || requestor.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			{
				transaction.cancel();
				activeChar.sendPacket(SendTradeDone.Fail, Msg.ActionFail);
				activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
				requestor.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
				return;
			}

			if(!transaction.isTypeOf(TransactionType.TRADE))
			{
				transaction.cancel();

				Util.handleIllegalPlayerAction(activeChar, "TradeDone[65]", requestor.getName() + " tried a trade dupe [!transaction.isTypeOf(TransactionType.TRADE)]", IllegalPlayerAction.WARNING);

				activeChar.sendPacket(SendTradeDone.Fail);
				activeChar.sendActionFailed();
				activeChar.sendPacket(new SystemMessage("Something wrong. Maybe, cheater?"));

				requestor.sendPacket(SendTradeDone.Fail);
				requestor.sendActionFailed();
				requestor.sendPacket(new SystemMessage("Something wrong. Maybe, cheater?"));
				return;
			}

			if(_response == 1)
			{
				transaction.confirm(activeChar);

				// first party accepted the trade
				requestor.sendPacket(new SystemMessage(SystemMessage.S1_CONFIRMED_TRADE).addString(activeChar.getName()));

				// notify clients that "OK" button has been pressed.
				activeChar.sendPacket(Msg.TradePressOtherOk);
				requestor.sendPacket(Msg.TradePressOtherOk);

				if(!transaction.isConfirmed(activeChar) || !transaction.isConfirmed(requestor))
				{
					activeChar.sendActionFailed();
					return;
				}

				// Can't exchange on a big distance
				if(!activeChar.isInRange(requestor, 1000))
				{
					transaction.cancel();
					activeChar.sendPacket(SendTradeDone.Fail, new SystemMessage(SystemMessage.S1_CANCELED_THE_TRADE).addString(requestor.getName()));
					requestor.sendPacket(SendTradeDone.Fail, new SystemMessage(SystemMessage.S1_CANCELED_THE_TRADE).addString(activeChar.getName()));
					return;
				}

				final boolean trade1Valid = L2TradeList.validateTrade(activeChar, transaction.getExchangeList(activeChar));
				final boolean trade2Valid = L2TradeList.validateTrade(requestor, transaction.getExchangeList(requestor));

				if(trade1Valid && trade2Valid)
				{
					transaction.tradeItems();
					requestor.sendPacket(Msg.YOUR_TRADE_IS_SUCCESSFUL, SendTradeDone.Success);
					activeChar.sendPacket(Msg.YOUR_TRADE_IS_SUCCESSFUL, SendTradeDone.Success);
				}
				else
				{
					if(!trade2Valid)
						Util.handleIllegalPlayerAction(requestor, "l2n.game.network.clientpackets.TradeDone[119]", requestor.getName() + " tried a trade dupe [!trade2Valid]", IllegalPlayerAction.WARNING);

					if(!trade1Valid)
						Util.handleIllegalPlayerAction(activeChar, "l2n.game.network.clientpackets.TradeDone[128]", activeChar.getName() + " tried a trade dupe [!trade1Valid]", IllegalPlayerAction.WARNING);

					activeChar.sendPacket(SendTradeDone.Fail);
					activeChar.sendPacket(new SystemMessage(SystemMessage.THE_ATTEMPT_TO_TRADE_HAS_FAILED));
					requestor.sendPacket(SendTradeDone.Fail);
					requestor.sendPacket(new SystemMessage(SystemMessage.THE_ATTEMPT_TO_TRADE_HAS_FAILED));
				}
			}
			else
			{
				activeChar.sendPacket(SendTradeDone.Fail);
				requestor.sendPacket(SendTradeDone.Fail);
				requestor.sendPacket(new SystemMessage(SystemMessage.S1_CANCELED_THE_TRADE).addString(activeChar.getName()));
			}

			transaction.cancel();
		}
	}

	@Override
	public String getType()
	{
		return _C__1C_TRADEDONE;
	}
}
