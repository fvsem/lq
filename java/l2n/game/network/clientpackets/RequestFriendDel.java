package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.tables.FriendsTable;

public class RequestFriendDel extends L2GameClientPacket
{
	private static final String _C__7A_REQUESTFRIENDDEL = "[C] 7A RequestFriendDel";
	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS(32);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		FriendsTable.getInstance().TryFriendDelete(activeChar, _name);
	}

	@Override
	public String getType()
	{
		return _C__7A_REQUESTFRIENDDEL;
	}
}
