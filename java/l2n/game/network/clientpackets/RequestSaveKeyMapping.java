package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExUISetting;

/**
 * format: (ch)db
 */
public class RequestSaveKeyMapping extends L2GameClientPacket
{
	private static String _C__D0_22_REQUESTSAVEKEYMAPPING = "[C] D0:22 RequestSaveKeyMapping";
	private byte[] _data;

	@Override
	protected void readImpl()
	{
		final int length = readD();
		if(length > _buf.remaining() || length > Short.MAX_VALUE || length < 0)
		{
			_data = null;
			return;
		}
		_data = new byte[length];
		readB(_data);
	}

	@Override
	protected void runImpl()
	{
		if(_data == null)
			return;
		final L2Player player = getClient().getActiveChar();
		if(player != null)
		{
			player.setKeyBindings(_data);
			player.sendPacket(new ExUISetting(_data));
		}
	}

	/**
	 * @return A String with this packet name for debuging purposes
	 */
	@Override
	public String getType()
	{
		return _C__D0_22_REQUESTSAVEKEYMAPPING;
	}
}
