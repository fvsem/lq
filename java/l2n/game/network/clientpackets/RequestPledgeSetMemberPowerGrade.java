package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;

public class RequestPledgeSetMemberPowerGrade extends L2GameClientPacket
{
	private static String _C__D0_15_REQUESTPLEDGESETMEMBERPOWERGRADE = "[C] D0:15 RequestPledgeSetMemberPowerGrade";
	// format: (ch)Sd
	private int _powerGrade;
	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS();
		_powerGrade = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan == null)
			return;

		if((activeChar.getClanPrivileges() & L2Clan.CP_CL_MANAGE_RANKS) == L2Clan.CP_CL_MANAGE_RANKS)
		{
			final L2ClanMember member = activeChar.getClan().getClanMember(_name);
			if(member != null)
			{
				if(clan.isAcademy(member.getPledgeType()))
				{
					activeChar.sendMessage("You cannot change academy member grade");
					return;
				}
				if(_powerGrade > 5 && clan.getAffiliationRank(member.getPledgeType()) != _powerGrade)
					member.setPowerGrade(clan.getAffiliationRank(member.getPledgeType()));
				else
					member.setPowerGrade(_powerGrade);
				if(member.isOnline())
					member.getPlayer().sendUserInfo();
			}
			else
				activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestPledgeSetMemberPowerGrade.NotBelongClan", activeChar));
		}
		else
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestPledgeSetMemberPowerGrade.HaveNotAuthority", activeChar));
	}

	@Override
	public String getType()
	{
		return _C__D0_15_REQUESTPLEDGESETMEMBERPOWERGRADE;
	}
}
