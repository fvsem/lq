package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.tables.ClanTable;

public class RequestReplySurrenderPledgeWar extends L2GameClientPacket
{
	private static final String _C__52_REQUESTREPLYSURRENDERPLEDGEWAR = "[C] 52 RequestReplySurrenderPledgeWar";

	int _answer;

	@Override
	protected void readImpl()
	{
		@SuppressWarnings("unused")
		final String _reqName = readS();
		_answer = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.ALLY))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED);
			activeChar.sendPacket(Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);
		if(requestor == null)
			return;

		if(_answer == 1)
		{
			requestor.deathPenalty(null);
			ClanTable.getInstance().stopClanWar(requestor.getClan(), activeChar.getClan());
		}
		else
		{}

		transaction.cancel();
	}

	@Override
	public String getType()
	{
		return _C__52_REQUESTREPLYSURRENDERPLEDGEWAR;
	}
}
