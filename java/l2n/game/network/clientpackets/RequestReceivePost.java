package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:47:41
 */
public class RequestReceivePost extends L2GameClientPacket
{
	private int postId;

	@Override
	public void readImpl()
	{
		postId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(!cha.isInPeaceZone())
		{
			cha.sendPacket(Msg.YOU_CANNOT_RECEIVE_IN_A_NON_PEACE_ZONE_LOCATION);
			return;
		}

		MailParcelController.getInstance().receivePost(postId, cha);
	}
}
