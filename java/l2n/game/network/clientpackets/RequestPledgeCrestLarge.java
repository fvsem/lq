package l2n.game.network.clientpackets;

import l2n.game.cache.CrestCache;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExPledgeCrestLarge;

import java.util.logging.Logger;

public class RequestPledgeCrestLarge extends L2GameClientPacket
{
	private static Logger _log = Logger.getLogger(RequestPledgeCrestLarge.class.getName());
	// format: chd
	private static String _C__D0_10_REQUESTPLEDGECRESTLARGE = "[C] D0:10 RequestPledgeCrestLarge";
	private int _crestId;

	@Override
	public void readImpl()
	{
		_crestId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_crestId == 0)
			return;

		final byte[] data = CrestCache.getPledgeCrestLarge(_crestId);
		if(data != null)
		{
			final ExPledgeCrestLarge pcl = new ExPledgeCrestLarge(_crestId, data);
			sendPacket(pcl);
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_10_REQUESTPLEDGECRESTLARGE;
	}
}
