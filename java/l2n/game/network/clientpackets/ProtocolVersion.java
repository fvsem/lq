package l2n.game.network.clientpackets;

import com.lameguard.LameGuard;
import com.lameguard.session.ClientSession;
import l2n.Config;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.KeyPacket;
import l2n.game.network.serverpackets.SendStatus;
import l2n.util.HWID;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.logging.Level;

public final class ProtocolVersion extends L2GameClientPacket
{
	private static final String _C__0E_PROTOCOLVERSION = "[C] 0e ProtocolVersion";
	private static final KeyPacket WRONG_PROTOCOL = new KeyPacket(null, 0);

	private int _version;
	private byte[] _data;
	private byte[] _check;

	@Override
	protected void readImpl()
	{
		_version = readD();
		if(_buf.remaining() == 0x104)
		{
			_data = new byte[0x100];
			_check = new byte[4];
			readB(_data);
			readB(_check);
		}
	}

	@Override
	public void runImpl()
	{
		// this packet is never encrypted
		final L2GameClient client = getClient();
		if(_version == -2)
			// this is just a ping attempt from the new C2 client
			client.closeNow(false);
		else if(_version == -3)
			// Проверки рейтинга типа l2top
			client.close(new SendStatus());
		else if(_version < Config.MIN_PROTOCOL_REVISION || _version > Config.MAX_PROTOCOL_REVISION)
		{
			_log.info("Client: " + client.toString() + " -> Protocol Revision: " + _version + " is invalid. Supported protocols: from " + Config.MIN_PROTOCOL_REVISION + " to " + Config.MAX_PROTOCOL_REVISION + ". Closing connection.");
			client.close(WRONG_PROTOCOL);
		}
		else
		{
			client.setRevision(_version); // set client's Protocol version
			if(client.protect_used)
				try
				{
					// Проверяем данные с помощью LameGuard
					if(!LameGuard.getInstance().checkData(_data, _check))
					{
						Log.add("Incorrect data! May be BOT or unprotected client! Client: " + client, "protect");
						client.close(WRONG_PROTOCOL);
						return;
					}

					// тут уже проверяется бан по HWID у LameGuard
					final ClientSession cs = LameGuard.getInstance().checkClient(client.getIpAddr(), _data);
					if(cs != null)
					{
						client.setHWID(cs.getHWID());
						// Проверка уже нашего списка банов
						if(Config.PROTECT_GS_ENABLE_HWID_BANS && HWID.checkHWIDBanned(client.HWID))
						{
							Log.add("Kicking banned HWID: " + client.HWID + " Client: " + client, "protect");
							client.close(WRONG_PROTOCOL);
							return;
						}

						client.setProtected(true);
						client.setInstanceCount(cs.getInstances());
						client.setPatchVersion(cs.getPatch());

						final byte[] key = client.enableCrypt();
						final byte[] answer = LameGuard.getInstance().assembleAnswer(cs, key);
						client.sendPacket(new KeyPacket(answer, 0));
					}
					else
					{
						// закрываем если не получилось проверить ;-(
						Log.add("CRC Check Fail! May be BOT or unprotected client! Client: " + client, "protect");
						client.close(WRONG_PROTOCOL);
					}
				}
				catch(final Exception e)
				{
					_log.log(Level.WARNING, "Filed read ProtocolVersion [" + _buf.remaining() + "]! May be BOT or unprotected client! Client " + client + ". Closing connection.", e);
					client.closeNow(false);
				}
			else
			{
				int obf_key = 0;
				if(Config.PROTOCOL_ENABLE_OBFUSCATION)
				{
					obf_key = Rnd.nextInt();
					client.enableOpcodeObfuscation(obf_key);
				}
				final byte[] key = client.enableCrypt();
				final byte[] answer = new byte[8];
				System.arraycopy(key, 0, answer, 0, 8);
				client.sendPacket(new KeyPacket(answer, obf_key));
				return;
			}
		}
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}

	@Override
	public String getType()
	{
		return _C__0E_PROTOCOLVERSION;
	}
}
