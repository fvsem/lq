package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.tables.FriendsTable;

public class RequestFriendInvite extends L2GameClientPacket
{
	private static final String _C__77_REQUESTFRIENDINVITE = "[C] 77 RequestFriendInvite";

	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS(32);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
		{
            return;
        }
		FriendsTable.getInstance().TryFriendInvite(activeChar, _name);
	}

	@Override
	public String getType()
	{
		return _C__77_REQUESTFRIENDINVITE;
	}
}
