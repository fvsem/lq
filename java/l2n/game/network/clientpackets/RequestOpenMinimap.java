package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShowMiniMap;

public class RequestOpenMinimap extends L2GameClientPacket
{
	private static String _C__6C_REQUESTSHOWMINIMAP = "[C] 6C RequestOpenMinimap";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		sendPacket(new ShowMiniMap(activeChar, 1665));
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.clientpackets.ClientBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__6C_REQUESTSHOWMINIMAP;
	}
}
