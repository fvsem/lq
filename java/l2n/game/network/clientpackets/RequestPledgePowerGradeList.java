package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.L2Clan.RankPrivs;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgePowerGradeList;

public class RequestPledgePowerGradeList extends L2GameClientPacket
{
	private static String _C__D0_13_REQUESTPLEDGEPOWERGRADELIST = "[C] D0:13 RequestPledgePowerGradeList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan != null)
		{
			final RankPrivs[] privs = clan.getAllRankPrivs();
			activeChar.sendPacket(new PledgePowerGradeList(privs));
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_13_REQUESTPLEDGEPOWERGRADELIST;
	}
}
