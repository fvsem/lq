package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.QuestList;

public class RequestQuestList extends L2GameClientPacket
{
	private static String _C__62_REQUESTQUESTLIST = "[C] 62 RequestQuestList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		sendPacket(new QuestList(getClient().getActiveChar()));
	}

	@Override
	public String getType()
	{
		return _C__62_REQUESTQUESTLIST;
	}
}
