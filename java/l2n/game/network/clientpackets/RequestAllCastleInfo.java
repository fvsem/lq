package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.ExShowCastleInfo;

public class RequestAllCastleInfo extends L2GameClientPacket
{
	private static String _C__D0_3F_REQUESTALLCASTLEINFO = "[C] D0:3F RequestAllCastleInfo";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		getClient().getActiveChar().sendPacket(new ExShowCastleInfo());
	}

	@Override
	public String getType()
	{
		return _C__D0_3F_REQUESTALLCASTLEINFO;
	}
}
