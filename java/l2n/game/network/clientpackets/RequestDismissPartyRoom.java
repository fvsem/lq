package l2n.game.network.clientpackets;

import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;

/**
 * Format: (ch) dd
 */
public class RequestDismissPartyRoom extends L2GameClientPacket
{
	private static String _C__D0_0A_REQUESTDISMISSPARTYROOM = "[C] D0:0A RequestDismissPartyRoom";
	private int _roomId;
	@SuppressWarnings("unused")
	private int _data2;

	@Override
	public void readImpl()
	{
		_roomId = readD(); // room id
		_data2 = readD(); // unknown
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final PartyRoom room = PartyRoomManager.getInstance().getRoom(_roomId);
		if(room.getLeader() == null || room.getLeader().equals(activeChar))
			PartyRoomManager.getInstance().removeRoom(_roomId);
		else
			PartyRoomManager.getInstance().getRoom(_roomId).removeMember(activeChar, false);

	}

	@Override
	public String getType()
	{
		return _C__D0_0A_REQUESTDISMISSPARTYROOM;
	}
}
