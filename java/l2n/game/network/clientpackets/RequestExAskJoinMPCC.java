package l2n.game.network.clientpackets;

import l2n.game.model.L2CommandChannel;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.ExAskJoinMPCC;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * Format: (ch) S
 */
public class RequestExAskJoinMPCC extends L2GameClientPacket
{
	private static String _C__D0_06_REQUESTEXMPCCASKJOIN = "[C] D0:06 RequestExMPCCAskJoin";
	private String _name;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_name = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		// цель приглашения
		L2Player target = L2ObjectsStorage.getPlayer(_name);
		if(target == null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THAT_PLAYER_IS_NOT_CURRENTLY_ONLINE));
			return;
		}

		// Сам себя нельзя
		if(activeChar.getObjectId() == target.getObjectId())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!target.isInParty())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_INVITED_WRONG_TARGET));
			return;
		}

		if(target.isInParty() && !target.getParty().isLeader(target))
			target = target.getParty().getPartyLeader();
		if(target == null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THAT_PLAYER_IS_NOT_CURRENTLY_ONLINE));
			return;
		}

		// сопартийцев тоже нельзя
		if(activeChar.isInParty() && target.isInParty() && activeChar.getParty().equals(target.getParty()))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(target.getParty().isInCommandChannel())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_PARTY_IS_ALREADY_A_MEMBER_OF_THE_COMMAND_CHANNEL).addString(target.getName()));
			return;
		}

		if(target.isInTransaction())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(target.getName()));
			return;
		}

		final L2Party activeParty = activeChar.getParty();
		if(activeParty != null && activeParty.isInCommandChannel())
		{
			if(activeParty.getCommandChannel().getChannelLeader() != activeChar)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_AUTHORITY_TO_INVITE_SOMEONE_TO_THE_COMMAND_CHANNEL));
				return;
			}

			sendInvite(activeChar, target);
		}
		else if(L2CommandChannel.checkAuthority(activeChar))
			sendInvite(activeChar, target);
	}

	private void sendInvite(final L2Player requestor, final L2Player target)
	{
		new Transaction(TransactionType.CHANNEL, requestor, target, 30000);
		target.sendPacket(new SystemMessage(SystemMessage.S1_HAS_INVITED_YOU_TO_THE_COMMAND_CHANNEL_DO_YOU_WANT_TO_JOIN).addString(requestor.getName()));
		target.sendPacket(new ExAskJoinMPCC(requestor.getName()));
		requestor.sendMessage("You invited " + target.getName() + " to your Command Channel.");
	}

	@Override
	public String getType()
	{
		return _C__D0_06_REQUESTEXMPCCASKJOIN;
	}
}
