package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.tables.GmListTable;
import l2n.util.Log;

public class RequestGmList extends L2GameClientPacket
{
	private final static String _C__8B_REQUESTGMLIST = "[C] 8B RequestGmList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
		{
			GmListTable.sendListToPlayer(activeChar);
			Log.LogCommand(activeChar, 2, _C__8B_REQUESTGMLIST, 1);
		}
	}

	@Override
	public String getType()
	{
		return _C__8B_REQUESTGMLIST;
	}
}
