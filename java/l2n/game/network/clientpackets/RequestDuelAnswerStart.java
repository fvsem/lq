package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.entity.Duel;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * sample 2a 01 00 00 00 format chddd
 */
public class RequestDuelAnswerStart extends L2GameClientPacket
{
	private static String _C__D0_1C_REQUESTANSWERPARTY = "[C] D0:1C RequestDuelAnswerStart";

	private int _response;
	private int _duelType;
	@SuppressWarnings("unused")
	private int _unk1;

	@Override
	protected void readImpl()
	{
		_duelType = readD();
		_unk1 = readD();
		_response = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final Transaction transaction = player.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.DUEL))
		{
			transaction.cancel();
			player.sendPacket(Msg.TIME_EXPIRED);
			player.sendPacket(Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(player);
		transaction.cancel();

		if(_response == 1 && (_duelType == 1 && requestor.getParty() != null && player.getParty() != null || _duelType == 0))
		{
			SystemMessage msg1, msg2;
			if(_duelType == 1)
			{
				msg1 = new SystemMessage(SystemMessage.YOU_HAVE_ACCEPTED_S1S_CHALLENGE_TO_A_PARTY_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS);
				msg1.addString(requestor.getName());

				msg2 = new SystemMessage(SystemMessage.S1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_DUEL_AGAINST_THEIR_PARTY_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS);
				msg2.addString(player.getName());
			}
			else
			{
				msg1 = new SystemMessage(SystemMessage.YOU_HAVE_ACCEPTED_S1S_CHALLENGE_TO_A_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS);
				msg1.addString(requestor.getName());

				msg2 = new SystemMessage(SystemMessage.S1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_A_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS);
				msg2.addString(player.getName());
			}

			player.sendPacket(msg1);
			requestor.sendPacket(msg2);

			Duel.createDuel(requestor, player, _duelType);
		}
		else
		{
			SystemMessage msg;
			if(_duelType == 1)
				msg = new SystemMessage(SystemMessage.THE_OPPOSING_PARTY_HAS_DECLINED_YOUR_CHALLENGE_TO_A_DUEL);
			else
			{
				msg = new SystemMessage(SystemMessage.S1_HAS_DECLINED_YOUR_CHALLENGE_TO_A_DUEL);
				msg.addString(player.getName());
			}
			requestor.sendPacket(msg);
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_1C_REQUESTANSWERPARTY;
	}
}
