package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;
import l2n.game.network.serverpackets.ExShowReceivedPostList;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:47:58
 */
public class RequestRejectPost extends L2GameClientPacket
{
	private int postId;

	@Override
	protected void readImpl()
	{
		postId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(cha.isOutOfControl())
		{
			cha.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		if(!cha.isInZonePeace())
		{
			cha.sendPacket(Msg.YOU_CANNOT_RECEIVE_IN_A_NON_PEACE_ZONE_LOCATION);
			return;
		}

		MailParcelController.getInstance().returnLetter(postId, 1);

		// Notify players the mail has been returned
		cha.sendPacket(Msg.MAIL_SUCCESSFULLY_RETURNED, new ExShowReceivedPostList(cha.getObjectId()));
	}
}
