package l2n.game.network.clientpackets;

import l2n.commons.text.Strings;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.CompType;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.olympiad.OlympiadGame;
import l2n.game.model.entity.olympiad.OlympiadManager;
import l2n.game.network.serverpackets.NpcHtmlMessage;

public class RequestOlympiadMatchList extends L2GameClientPacket
{
	private static String _C__D0_2F_REQUESTOLYMPIADMATCHLIST = "[C] D0:2F RequestOlympiadMatchList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!activeChar.inObserverMode())
			return;

		final NpcHtmlMessage reply = new NpcHtmlMessage(0);
		final StringBuffer msg = new StringBuffer("");
		msg.append("!Grand Olympiad Game View:<br>");

		final OlympiadManager manager = Olympiad._manager;
		if(manager != null)
			for(int i = 0; i < Olympiad.STADIUMS.length; i++)
			{
				final OlympiadGame game = manager.getOlympiadInstance(i);
				if(game == null || game.getState() <= 0)
					continue;
				if(game.getType() == CompType.TEAM || game.getType() == CompType.TEAM_RANDOM)
				{
					msg.append("<br1>Arena " + i + ":&nbsp;<a action=\"bypass -h oly_" + (i + 1) + "\">Team vs Team:</a>");
					msg.append("<br1>- " + game.getTeam1Title() + "<br1>- " + game.getTeam2Title());
				}
				else
					msg.append("<br1>Arena " + i + ":&nbsp;<a action=\"bypass -h oly_" + (i + 1) + "\">" + manager.getOlympiadInstance(i).getTitle() + "</a>");
				msg.append("<img src=\"L2UI.SquareWhite\" width=270 height=1> <img src=\"L2UI.SquareBlank\" width=1 height=3>");
			}

		reply.setHtml(Strings.bbParse(msg.toString()));
		activeChar.sendPacket(reply);
	}

	@Override
	public String getType()
	{
		return _C__D0_2F_REQUESTOLYMPIADMATCHLIST;
	}
}
