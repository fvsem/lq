package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.math.SafeMath;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;
import l2n.game.model.L2Manor;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ManorManagerInstance;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

public class RequestProcureCrop extends L2GameClientPacket
{
	private static final String _C__CF_REQUESTPROCURECROP = "[C] CF RequestProcureCrop";
	// format: cddb
	private int _count;
	private long[] _items;

	@Override
	protected void readImpl()
	{
		readD();
		_count = readD();
		if(_count * 16 > _buf.remaining() || _count > Short.MAX_VALUE || _count <= 0)
		{
			_count = 0;
			return;
		}
		_items = new long[_count * 2];
		for(int i = 0; i < _count; i++)
		{
			readD(); // service
			_items[i * 2 + 0] = readD();
			final long cnt = readQ();
			if(cnt < 1)
			{
				_count = 0;
				_items = null;
				return;
			}
			_items[i * 2 + 1] = (int) cnt;
		}
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		// Alt game - Karma punishment
		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && player.getKarma() > 0)
			return;

		final L2Object target = player.getTarget();

		if(_count < 1 || _items == null)
		{
			player.sendActionFailed();
			return;
		}

		// Check for buylist validity and calculates summary values
		int slots = 0;
		int weight = 0;
		final L2ManorManagerInstance manor = target != null && target instanceof L2ManorManagerInstance ? (L2ManorManagerInstance) target : null;

		for(int i = 0; i < _count; i++)
		{
			final int itemId = (int) _items[i * 2 + 0];
			final long count = _items[i * 2 + 1];
			if(count < 0)
			{
				sendPacket(Msg.INCORRECT_ITEM_COUNT);
				return;
			}

			final Castle castle = manor.getCastle();
			if(castle == null)
				return;
			final CropProcure crop = castle.getCrop(itemId, CastleManorManager.PERIOD_CURRENT);
			if(crop == null)
				return;

			final int rewardId = L2Manor.getInstance().getRewardItem(itemId, crop.getReward());
			final L2Item template = ItemTable.getInstance().getTemplate(rewardId);
			weight += count * template.getWeight();

			long add_slot = 0;
			if(!template.isStackable())
				add_slot = count;
			else if(player.getInventory().getItemByItemId(itemId) == null)
				add_slot = 1;

			if(add_slot > 0)
				try
				{
					slots = SafeMath.safeAddInt(slots, add_slot);
				}
				catch(final ArithmeticException e)
				{
					sendPacket(Msg.INCORRECT_ITEM_COUNT);
					return;
				}
		}

		if(!player.getInventory().validateWeight(weight))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
			return;
		}

		if(!player.getInventory().validateCapacity(slots))
		{
			sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
			return;
		}

// // Proceed the purchase
// _procureList = manor.getCastle().getCropProcure(CastleManorManager.PERIOD_CURRENT);

		for(int i = 0; i < _count; i++)
		{
			final int itemId = (int) _items[i * 2 + 0];
			long count = _items[i * 2 + 1];
			if(count < 0)
				count = 0;

			final int rewradItemId = L2Manor.getInstance().getRewardItem(itemId, manor.getCastle().getCrop(itemId, CastleManorManager.PERIOD_CURRENT).getReward());

			long rewradItemCount = L2Manor.getInstance().getRewardAmountPerCrop(manor.getCastle().getId(), itemId, manor.getCastle().getCropRewardType(itemId));

			rewradItemCount = count * rewradItemCount;

			// Add item to Inventory and adjust update packet
			final L2ItemInstance item = player.getInventory().addItem(rewradItemId, rewradItemCount, 0, "Manor");
			final L2ItemInstance iteme = player.getInventory().destroyItemByItemId(itemId, count, true);

			if(item == null || iteme == null)
				continue;

			// Send Char Buy Messages
			player.sendPacket(SystemMessage.obtainItems(rewradItemId, rewradItemCount, 0));
		}

		player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
	}

	@Override
	public String getType()
	{
		return _C__CF_REQUESTPROCURECROP;
	}
}
