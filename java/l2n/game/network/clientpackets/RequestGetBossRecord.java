package l2n.game.network.clientpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.RaidBossSpawnManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExGetBossRecord;
import l2n.game.network.serverpackets.ExGetBossRecord.BossRecordInfo;

import java.util.HashMap;


public class RequestGetBossRecord extends L2GameClientPacket
{
	private static String _C__D0_43_REQUESTGETBOSSRECORD = "[C] D0:43 RequestGetBossRecord";
	@SuppressWarnings("unused")
	private int _bossID;

	@Override
	public void readImpl()
	{
		_bossID = readD(); // always 0?
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		int totalPoints = 0;
		int ranking = 0;

		if(activeChar == null)
			return;

		final GArray<BossRecordInfo> list = new GArray<BossRecordInfo>();
		final HashMap<Integer, Integer> points = RaidBossSpawnManager.getInstance().getPointsForOwnerId(activeChar.getObjectId());
		if(points != null && !points.isEmpty())
			for(final int bossId : points.keySet())
				switch (bossId)
				{
					case -1:
						ranking = points.get(bossId);
						break;
					case 0:
						totalPoints = points.get(bossId);
						break;
					default:
						list.add(new BossRecordInfo(bossId, points.get(bossId), 0));
				}

		activeChar.sendPacket(new ExGetBossRecord(ranking, totalPoints, list));
	}

	@Override
	public String getType()
	{
		return _C__D0_43_REQUESTGETBOSSRECORD;
	}
}
