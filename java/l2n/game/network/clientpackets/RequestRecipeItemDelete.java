package l2n.game.network.clientpackets;

import l2n.game.RecipeController;
import l2n.game.model.L2RecipeList;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.RecipeBookItemList;

public class RequestRecipeItemDelete extends L2GameClientPacket
{
	private static String _C__B6_REQUESTRECIPEITEMDELETE = "[C] B6 RequestRecipeItemDelete";
	// Format: cd
	// private static Logger _log = Logger.getLogger(RequestSellItem.class.getName());

	private int _RecipeID;

	@Override
	public void readImpl()
	{
		_RecipeID = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		final L2RecipeList rp = RecipeController.getInstance().getRecipeList(_RecipeID);
		if(rp == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.unregisterRecipe(_RecipeID);

		final RecipeBookItemList response = new RecipeBookItemList(rp.isDwarvenRecipe(), (int) activeChar.getCurrentMp());

		response.setRecipes(activeChar.getDwarvenRecipeBook());

		activeChar.sendPacket(response);
	}

	@Override
	public String getType()
	{
		return _C__B6_REQUESTRECIPEITEMDELETE;
	}
}
