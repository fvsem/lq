package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.handler.VoicedCommandHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.instancemanager.PetitionManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2World;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.entity.olympiad.OlympiadGame;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.MapRegionTable;
import l2n.status.GameStatusThread;
import l2n.status.Status;
import l2n.util.Calculator;
import l2n.util.Log;
import l2n.util.StringUtil;
import l2n.util.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Say2C extends L2GameClientPacket
{
	private static final String _C__49_SAY2C = "[C] 49 Say2C";

	public final static int ALL_CHAT_RANGE = 1250; // Дальность белого чата

	public final static int LOCAL = 0;
	public final static int SHOUT = 1; // !
	public final static int TELL = 2; // \"
	public final static int PARTY = 3; // #
	public final static int CLAN = 4; // @
	public final static int GM = 5; // like local
	public final static int PETITION_PLAYER = 6; // used for petition
	public final static int PETITION_GM = 7; // * used for petition
	public final static int TRADE = 8; // +
	public final static int ALLIANCE = 9; // $
	public final static int ANNOUNCEMENT = 10;
	public final static int FERRY = 11; // id = 11 --> Crashes client // like shout, but talker is D instead of S
	public final static int L2FRIEND = 12; // id = 12
	public final static int MSN = 13; // id = 13
	public final static int PARTYROOM_MATCHING = 14;
	public final static int COMMANDCHANNEL_ALL = 15; // `` (pink) команды лидера СС
	public final static int COMMANDCHANNEL_COMMANDER = 16; // ` (yellow) команды лидеров партий в СС
	public final static int HERO_VOICE = 17; // %
	public final static int CRITICAL_ANNOUNCEMENT = 18; // dark cyan
	public final static int BATTLEFIELD = 20;

	private final static int[] BAN_CHAN = Config.BAN_CHANNEL_LIST;
	private final static int[] INDECENT_CHAN = Config.INDECENT_CHANNEL_LIST;

	/** RegExp для кэширования линков предметов */
	private static final Pattern PATTERN = Pattern.compile("Type=[0-9]+[^0-9]+ID=([0-9]+)[^0-9]+");

	private final static String[] chatNames = {
			//
			"ALL\t", // 0
			"SHOUT", // 1
			"TELL ", // 2
			"PARTY", // 3
			"CLAN ", // 4
			"GM\t ", // 5
			"PETITION_PLAYER", // 6
			"PETITION_GM", // 7
			"TRADE", // 8
			"ALLIANCE", // 9
			"ANNOUNCEMENT", // 10
			"CRASH", // id = 11 --> Crashes client
			"L2FRIEND", // id = 12
			"MSN", // id = 13
			"PARTYROOM_MATCHING", // 14
			"COMMANDCHANNEL_ALL", // 15
			"COMMANDCHANNEL_COMMANDER", // 16
			"HERO_VOICE", // 17
			"CRITICAL_ANNOUNCEMENT", // 18
			"UNKNOWN_19", // 19
			"BATTLEFIELD" // 20
	};

	private static final String[] WALKER_COMMAND_LIST = {
			"USESKILL",
			"USEITEM",
			"BUYITEM",
			"SELLITEM",
			"SAVEITEM",
			"LOADITEM",
			"MSG",
			"SET",
			"DELAY",
			"LABEL",
			"JMP",
			"CALL",
			"RETURN",
			"MOVETO",
			"NPCSEL",
			"NPCDLG",
			"DLGSEL",
			"CHARSTATUS",
			"POSOUTRANGE",
			"POSINRANGE",
			"GOHOME",
			"SAY",
			"EXIT",
			"PAUSE",
			"STRINDLG",
			"STRNOTINDLG",
			"CHANGEWAITTYPE",
			"FORCEATTACK",
			"ISMEMBER",
			"REQUESTJOINPARTY",
			"REQUESTOUTPARTY",
			"QUITPARTY",
			"MEMBERSTATUS",
			"CHARBUFFS",
			"ITEMCOUNT",
			"FOLLOWTELEPORT" };

	public static final String ALT_LETTERS[][] = {
			{ "h", "н" },
			{ "x", "х" },
			{ "a", "а" },
			{ "b", "ь" },
			{ "3", "з" },
			{ "c", "с" },
			{ "r", "я" },
			{ "0", "о" },
			{ "o", "о" },
			{ "m", "м" },
			{ "p", "р" },
			{ "e", "е" },
			{ "ё", "е" },
			{ "jl", "л" },
			{ "6", "б" },
			{ "y", "у" },
			{ "k", "к" },
			{ "9l*", "я" } };

	protected static GArray<String> _banned = new GArray<String>();
	private String _text;
	private int _type;
	private String _target;

	@Override
	public void readImpl()
	{
		_text = readS();
		_text = _text.replaceAll("\\\\n", "");

		_type = readD();
		_target = _type == TELL ? readS() : null;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
        if(!activeChar.getPassCheck())
		{
            return;
        }
		if(_type < 0 || _type > chatNames.length || _text == null || _text.length() == 0)
		{
			_log.warning("Say2C: Invalid type: " + _type + " Player : " + activeChar.getName() + " text: " + String.valueOf(_text));
			activeChar.sendActionFailed();
			return;
		}
        
		// слишком длинные чат-сообщения вызывают крит клиента (возможный эксплойт)
		// Even though the client can handle more characters than it's current limit allows, an overflow (critical error) happens if you pass a huge (1000+) message.
		if(_text.length() > 105)
		{
			activeChar.sendPacket(Msg.DONT_SPAM);
			return;
		}
		
		
		// Chat config by Vitalay
		if(activeChar.getLevel() < Config.MAIN_LEVEL_CHAT && _type != 0)
		{
			//en
			if(activeChar.getLangId() == 0)
			{
				activeChar.sendMessage("You are not permitted to use this chat until " + Config.MAIN_LEVEL_CHAT + " level");
			}
			else if(activeChar.getLangId() == 1) // ru
			{
				activeChar.sendMessage("Вам запрещено использовать - этот чат до " + Config.MAIN_LEVEL_CHAT + " уровня");
			}
			return;
		}
		
		if(Config.ALLOW_L2WALKER_CLIENT == Config.L2WalkerAllowed.False && _type == TELL && checkBot(_text))
		{
			Util.handleIllegalPlayerAction(activeChar, "Say2C[185]", "Client Emulator Detect: Player " + activeChar.getName() + " using l2walker.", Config.DEFAULT_PUNISH);
			return;
		}

		if(Config.LOG_TELNET)
		{
			String line_output;

			if(_type == TELL)
				line_output = chatNames[_type] + "[" + activeChar.getName() + " to " + _target + "] " + _text;
			else
				line_output = chatNames[_type] + "[" + activeChar.getName() + "] " + _text;
			telnet_output(line_output, _type);
		}

		if(_text.startsWith("."))
		{
			final String fullcmd = _text.substring(1).trim();
			final String command = fullcmd.split("\\s+")[0];
			final String args = fullcmd.substring(command.length()).trim();

			if(command.length() > 0)
			{
				// then check for VoicedCommands
				final IVoicedCommandHandler vch = VoicedCommandHandler.getInstance().getVoicedCommandHandler(command);
				if(vch != null)
				{
					vch.useVoicedCommand(command, activeChar, args);
					return;
				}
			}
			activeChar.sendMessage("Такой команды нет");
			return;
		}
		else if(_text.startsWith("==") || _text.startsWith("--"))
		{
			activeChar.sendMessage(Util.formatDouble(Calculator.eval(_text.substring(2).replace(",", ".")), "Wrong equation: result is not a number", false));
			return;
		}

		boolean itemFound = false;
		// Кэширование линков предметов
		final String links[] = _text.split("\u0008");
		int i = 1;
		while (i < links.length)
		{
			final Matcher matcher = PATTERN.matcher(links[i]);
			if(matcher.find())
			{
				int id;
				try
				{
					id = Integer.parseInt(matcher.group(1));
				}
				catch(final Exception e)
				{
					id = 0;
				}
				if(id > 0 && L2ObjectsStorage.findObject(id) != null)
				{
					itemFound = true;
					break;
				}
			}
			i += 2;
		}

		if((_text = checkText(activeChar, _text, itemFound)) == null)
			return;

		if(Config.LOG_CHAT)
		{
			String _logchat = chatNames[_type] + " ";
			if(_type == TELL)
				_logchat += "[" + activeChar.getName() + " to " + _target + "] " + _text;
			else
				_logchat += "[" + activeChar.getName() + "] " + _text;
			Log.add(_logchat, "chat");
		}

		final String translit = activeChar.getVar("translit");
		if(translit != null)
			_text = Strings.fromTranslit(_text, translit.equals("tl") ? 1 : 2);

		CreatureSay cs = new CreatureSay(activeChar.getObjectId(), _type, activeChar.getName(), _text);
		final int mapregion = MapRegionTable.getInstance().getMapRegion(activeChar.getX(), activeChar.getY());
		final long curTime = System.currentTimeMillis();

		if(_type == PETITION_PLAYER && activeChar.isGM())
			_type = PETITION_GM;

		switch (_type)
		{
			case TELL:
				final L2Player receiver = L2ObjectsStorage.getPlayer(_target);
				
				
				if(receiver != null && receiver.isInOfflineMode())
				{
					activeChar.sendMessage("Персонаж находится на Оффлайн торговле");
					activeChar.sendActionFailed();
				}
				else if(receiver != null && !receiver.isInBlockList(activeChar) && !receiver.isBlockAll())
				{
					if(!receiver.getMessageRefusal())
					{
						receiver.sendPacket(cs);
						cs = new CreatureSay(activeChar.getObjectId(), _type, "->" + receiver.getName(), _text);
						activeChar.sendPacket(cs);
					}
					else
						activeChar.sendPacket(Msg.THE_PERSON_IS_IN_A_MESSAGE_REFUSAL_MODE);
				}
				else if(receiver == null)
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_LOGGED_IN).addString(_target));
					activeChar.sendActionFailed();
				}
				else
				{
					activeChar.sendPacket(Msg.YOU_HAVE_BEEN_BLOCKED_FROM_THE_CONTACT_YOU_SELECTED);
					activeChar.sendActionFailed();
				}
				break;
			case SHOUT:
				if(activeChar.isCursedWeaponEquipped())
				{
					activeChar.sendPacket(Msg.SHOUT_AND_TRADE_CHATING_CANNOT_BE_USED_SHILE_POSSESSING_A_CURSED_WEAPON);
					return;
				}

				final Long lastShoutTime = (Long) activeChar.getProperty(PropertyCollection.ShoutChatLaunched);
				if(lastShoutTime != null && lastShoutTime + 5000L > curTime)
				{
					activeChar.sendMessage("Крик в область разрешается один раз в 5 секунд.");
					return;
				}
				activeChar.addProperty(PropertyCollection.ShoutChatLaunched, curTime);

				// отправка пакета наблюдателям и игрокам
				if(activeChar.inObserverMode())
				{
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadObserveId());
					if(game != null)
						game.broadcastPacket(cs, true, true);
					else
						activeChar.sendPacket(Msg.YOU_CANNOT_CHAT_LOCALLY_WHILE_OBSERVING);
					return;
				}

				// отправка пакета наблюдателям
				if(activeChar.isInOlympiadMode())
				{
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadGameId());
					if(game != null)
						game.broadcastPacket(cs, false, true);
				}

				if(activeChar.getLevel() >= Config.GLOBAL_CHAT || activeChar.isGM())
				{
					for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
						if(!player.isInBlockList(activeChar) && !player.isBlockAll())
							player.sendPacket(cs);
				}
				else
				{
					if(Config.SHOUT_CHAT_MODE == 1)
					{
						for(final L2Player player : L2World.getAroundPlayers(activeChar))
							if(!player.isInBlockList(activeChar) && !player.isBlockAll() && player != activeChar && player.getOlympiadObserveId() == -1)
								player.sendPacket(cs);
					}
					else
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							if(MapRegionTable.getInstance().getMapRegion(player.getX(), player.getY()) == mapregion && !player.isInBlockList(activeChar) && !player.isBlockAll() && player != activeChar && player.getOlympiadObserveId() == -1)
								player.sendPacket(cs);
					activeChar.sendPacket(cs);
				}
				break;
			case TRADE:
				if(activeChar.isCursedWeaponEquipped())
				{
					activeChar.sendPacket(Msg.SHOUT_AND_TRADE_CHATING_CANNOT_BE_USED_SHILE_POSSESSING_A_CURSED_WEAPON);
					return;
				}

				final Long lastTradeTime = (Long) activeChar.getProperty(PropertyCollection.TradeChatLaunched);
				if(lastTradeTime != null && lastTradeTime + 5000L > curTime)
				{
					activeChar.sendMessage("Торговый чат. разрешено один раз в 5 секунд.");
					return;
				}
				activeChar.addProperty(PropertyCollection.TradeChatLaunched, curTime);

				// отправка пакета наблюдателям и игрокам
				if(activeChar.inObserverMode())
				{
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadObserveId());
					if(game != null)
						game.broadcastPacket(cs, true, true);
					else
						activeChar.sendPacket(Msg.YOU_CANNOT_CHAT_LOCALLY_WHILE_OBSERVING);
					return;
				}

				// отправка пакета наблюдателям
				if(activeChar.isInOlympiadMode())
				{
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadGameId());
					if(game != null)
						game.broadcastPacket(cs, false, true);
				}

				if(activeChar.getLevel() >= Config.GLOBAL_TRADE_CHAT || activeChar.isGM())
				{
					for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
						if(!player.isInBlockList(activeChar) && !player.isBlockAll())
							player.sendPacket(cs);
				}
				else
				{
					if(Config.TRADE_CHAT_MODE == 1)
					{
						for(final L2Player player : L2World.getAroundPlayers(activeChar))
							if(!player.isInBlockList(activeChar) && !player.isBlockAll() && player != activeChar && player.getOlympiadObserveId() == -1)
								player.sendPacket(cs);
					}
					else
						for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
							if(MapRegionTable.getInstance().getMapRegion(player.getX(), player.getY()) == mapregion && !player.isInBlockList(activeChar) && !player.isBlockAll() && player != activeChar && player.getOlympiadObserveId() == -1)
								player.sendPacket(cs);
					activeChar.sendPacket(cs);
				}
				break;
			case LOCAL:
				if(activeChar.inObserverMode())
				{
					// отправка пакета наблюдателям и игрокам
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadObserveId());
					if(game != null)
						game.broadcastPacket(cs, true, true);
					else
						activeChar.sendPacket(Msg.YOU_CANNOT_CHAT_LOCALLY_WHILE_OBSERVING);
					return;
				}

				if(activeChar.isCursedWeaponEquipped())
					cs = new CreatureSay(activeChar.getObjectId(), _type, activeChar.getVisName(), _text);
				for(final L2Player player : activeChar.getAroundPlayers(ALL_CHAT_RANGE))
					if(!player.isInBlockList(activeChar) && !player.isBlockAll() && player != activeChar && player.getOlympiadObserveId() == -1)
						player.sendPacket(cs);

				// отправка пакета наблюдателям
				if(activeChar.isInOlympiadMode())
				{
					final OlympiadGame game = Olympiad.getOlympiadGame(activeChar.getOlympiadGameId());
					if(game != null)
						game.broadcastPacket(cs, false, true);
				}
				activeChar.sendPacket(cs);
				break;
			case CLAN:
				if(activeChar.getClan() != null)
					activeChar.getClan().broadcastToOnlineMembers(cs);
				else
					activeChar.sendActionFailed();
				break;
			case ALLIANCE:
				if(activeChar.getClan() != null && activeChar.getClan().getAlliance() != null)
					activeChar.getClan().getAlliance().broadcastToOnlineMembers(cs);
				else
					activeChar.sendActionFailed();
				break;
			case PARTY:
				if(activeChar.isInParty())
					activeChar.getParty().broadcastToPartyMembers(cs);
				else
					activeChar.sendActionFailed();
				break;
			case PARTYROOM_MATCHING:
				if(activeChar.getPartyRoom() <= 0)
				{
					activeChar.sendActionFailed();
					return;
				}
				final PartyRoom room = PartyRoomManager.getInstance().getRooms().get(activeChar.getPartyRoom());
				if(room == null)
				{
					activeChar.sendActionFailed();
					return;
				}
				room.broadcastPacket(cs);
				break;
			case COMMANDCHANNEL_ALL:
				if(!activeChar.isInParty() || !activeChar.getParty().isInCommandChannel())
				{
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL);
					return;
				}
				if(activeChar.getParty().getCommandChannel().getChannelLeader() == activeChar)
					activeChar.getParty().getCommandChannel().broadcastToChannelMembers(cs);
				else
					activeChar.sendPacket(Msg.ONLY_CHANNEL_OPENER_CAN_GIVE_ALL_COMMAND);
				break;
			case COMMANDCHANNEL_COMMANDER:
				if(!activeChar.isInParty() || !activeChar.getParty().isInCommandChannel())
				{
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_AUTHORITY_TO_USE_THE_COMMAND_CHANNEL);
					return;
				}
				if(activeChar.getParty().isLeader(activeChar))
					activeChar.getParty().getCommandChannel().broadcastToChannelPartyLeaders(cs);
				else
					activeChar.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_ACCESS_THE_COMMAND_CHANNEL);
				break;
			case HERO_VOICE:
				if(activeChar.isHero() || activeChar.getPlayerAccess().Announcements)
				{
					// Ограничение только для героев, гм-мы пускай говорят.
					if(!activeChar.getPlayerAccess().Announcements)
					{
						final Long lastTime = (Long) activeChar.getProperty(PropertyCollection.HeroChatLaunched);
						if(lastTime != null && lastTime + 10000L > curTime)
						{
							activeChar.sendMessage("Геройский чат. разрешено один раз в 10 секунд.");
							return;
						}
						activeChar.addProperty(PropertyCollection.HeroChatLaunched, curTime);
					}

					for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
						if(!player.isInBlockList(activeChar) && !player.isBlockAll())
							player.sendPacket(cs);
				}
				break;
			case PETITION_PLAYER:
			case PETITION_GM:
			{
				if(!PetitionManager.getInstance().isPlayerInConsultation(activeChar))
				{
					activeChar.sendPacket(Msg.YOU_ARE_CURRENTLY_NOT_IN_A_PETITION_CHAT);
					return;
				}
				PetitionManager.getInstance().sendActivePetitionMessage(activeChar, _text);
				break;
			}
			case BATTLEFIELD:
				if(activeChar.getTerritorySiege() <= -1 || !TerritorySiege.isTerritoryChatAccessible())
					return;
				for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
					if(!player.isInBlockList(activeChar) && !player.isBlockAll() && activeChar.getTerritorySiege() == player.getTerritorySiege())
						player.sendPacket(cs);
				break;
			default:
				_log.warning("Character " + activeChar.getName() + " used unknown chat type: " + _type + ". Cheater?");
		}
	}

	private boolean checkBot(final String text)
	{
		for(final String botCommand : WALKER_COMMAND_LIST)
			if(text.startsWith(botCommand))
				return true;
		return false;
	}

	private String checkText(final L2Player activeChar, final String text, final boolean isItemContains)
	{
		final boolean globalchat = _type != ALLIANCE && _type != CLAN && _type != PARTY;
		boolean chan_banned = false;
		for(int i = 0; i <= Config.MAT_BAN_COUNT_CHANNELS; i++)
			if(_type == BAN_CHAN[i])
				chan_banned = true;

		boolean indecent_block = false;
		for(int i = 0; i <= Config.INDECENT_BLOCK_COUNT_CHANNELS; i++)
			if(_type == INDECENT_CHAN[i])
				indecent_block = true;

		if((globalchat || chan_banned) && activeChar.getNoChannel() != 0)
		{
			if(activeChar.getNoChannelRemained() > 0 || activeChar.getNoChannel() < 0)
			{
				if(activeChar.getNoChannel() > 0)
				{
					final int timeRemained = Math.round(activeChar.getNoChannelRemained() / 60000);
					activeChar.sendMessage(new CustomMessage("common.ChatBanned", activeChar).addNumber(timeRemained));
				}
				else
					activeChar.sendMessage(new CustomMessage("common.ChatBannedPermanently", activeChar));

				activeChar.sendActionFailed();
				return null;
			}
			activeChar.updateNoChannel(0);
		}

		if(globalchat && Config.SIMPLE_MAT_CHECK)
			if(Config.MAT_REPLACE && !Config.MAT_BANCHAT)
			{
				for(final Pattern pattern : Config.OBSCENE_LIST)
					if(pattern.matcher(text).matches())
					{
						activeChar.sendActionFailed();
						return Config.MAT_REPLACE_STRING;
					}
			}
			else if(Config.MAT_BANCHAT)
				for(final Pattern pattern : Config.OBSCENE_LIST)
					if(pattern.matcher(text).matches())
					{
						activeChar.sendMessage("Вам запрещены все чаты. До снятия бан чата: " + Config.UNCHATBANTIME * 60 + " сек.");
						Log.add("Obscene|" + activeChar.getName() + "|" + text + "|" + pattern, "abuse");
						activeChar.updateNoChannel(Config.UNCHATBANTIME * 60000);
						activeChar.increaseKarma(Config.MAT_KARMA);
						activeChar.sendActionFailed();
						return null;
					}

		String _textCheck = text.toLowerCase();
		if(!isItemContains)
			for(final String[] AltLetter : ALT_LETTERS)
				_textCheck = _textCheck.replaceAll(AltLetter[0], AltLetter[1]);

		final String[] arrayText = _textCheck.split(" ");
		final String[] originalText = text.split(" ");
		final int length = arrayText.length;

		if(chan_banned && !activeChar.isGM())
			if(Config.MAT_REPLACE && !Config.MAT_BANCHAT)
			{
				for(final Pattern pattern : Config.OBSCENE_LIST)
					for(int i = 0; i < length; i++)
						if(pattern.matcher(arrayText[i]).matches())
						{
							originalText[i] = arrayText[i] = Config.MAT_REPLACE_STRING;
							activeChar.increaseKarma(Config.MAT_KARMA);
						}
			}
			else if(Config.MAT_BANCHAT)
				for(final Pattern pattern : Config.OBSCENE_LIST)
					for(int i = 0; i < length; i++)
						if(pattern.matcher(arrayText[i]).matches())
						{
							activeChar.sendMessage("Вам запрещены все чаты. До снятия бан чата: " + Config.UNCHATBANTIME * 60 + " сек.");
							Log.add("Obscene|" + activeChar.getName() + "|" + text + "|" + pattern, "abuse");
							activeChar.updateNoChannel(Config.UNCHATBANTIME * 60000);
							activeChar.increaseKarma(Config.MAT_KARMA);
							activeChar.sendActionFailed();
							return null;
						}

		if(indecent_block && Config.INDECENT_BLOCKCHAT && !activeChar.isGM())
			for(final Pattern pattern : Config.INDECENT_LIST)
				for(int i = 0; i < length; i++)
					if(pattern.matcher(arrayText[i]).matches())
					{
						originalText[i] = arrayText[i] = Config.MAT_REPLACE_STRING;
						activeChar.increaseKarma(Config.MAT_KARMA);
					}

		if(_type == SHOUT && Config.SHOUT_FILTER && !activeChar.isGM())
			for(final Pattern pattern : Config.SHOUT_LIST)
				for(int i = 0; i < length; i++)
					if(pattern.matcher(arrayText[i]).matches())
					{
						originalText[i] = arrayText[i] = Config.MAT_REPLACE_STRING;
						activeChar.increaseKarma(Config.MAT_KARMA);
					}

		final StringBuilder clearText = StringUtil.startAppend(length);
		for(int i = 0; i < length; i++)
			StringUtil.append(clearText, originalText[i], " ");

		return clearText.toString();
	}

	private void telnet_output(final String _text, final int type)
	{
		GameStatusThread tinstance = Status.telnetlist;
		while (tinstance != null)
		{
			if(type == TELL && tinstance.LogTell)
				tinstance.write(_text);
			else if(tinstance.LogChat)
				tinstance.write(_text);
			tinstance = tinstance.next;
		}
	}

	@Override
	public String getType()
	{
		return _C__49_SAY2C;
	}

	public static class UnbanTask implements Runnable
	{
		private final String _name;

		public UnbanTask(final String Name)
		{
			_name = Name;
		}

		@Override
		public void run()
		{
			final L2Player plyr = L2ObjectsStorage.getPlayer(_name);
			if(plyr != null)
			{
				plyr.setAccessLevel(0);
				plyr.sendMessage("Nochannel deactivated");
				Log.add("" + plyr + ": unbanchat online", "abuse");
			}
			else
			{
				setCharacterAccessLevel(_name, 0);
				Log.add("Player " + _name + ": unbanchat offline", "abuse");
			}

			_banned.remove(_name);
		}
	}

	public static void setCharacterAccessLevel(final String user, final int banLevel)
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			final String stmt = "UPDATE characters SET characters.accesslevel = ? WHERE characters.char_name=?";
			statement = con.prepareStatement(stmt);
			statement.setInt(1, banLevel);
			statement.setString(2, user);
			statement.executeUpdate();
		}
		catch(final Exception e)
		{
			_log.warning("Could not set accessLevl:" + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}