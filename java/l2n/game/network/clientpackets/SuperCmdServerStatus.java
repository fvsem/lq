package l2n.game.network.clientpackets;

/**
 * Format ch
 * c: (id) 0x39
 * h: (subid) 0x02
 */
class SuperCmdServerStatus extends L2GameClientPacket
{
	private static String _C__39_02_SUPERCMDSERVERSTATUS = "[C] 39:02 SuperCmdServerStatus";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{}

	@Override
	public String getType()
	{
		return _C__39_02_SUPERCMDSERVERSTATUS;
	}
}
