package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.math.SafeMath;
import l2n.game.cache.Msg;
import l2n.game.model.Elementals;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2Multisell.MultiSellListContainer;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2Augmentation;
import l2n.game.model.base.MultiSellEntry;
import l2n.game.model.base.MultiSellIngredient;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.ExPCCafePointInfo;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.PetDataTable;
import l2n.game.templates.L2Item;
import l2n.util.Log;
import l2n.util.Util;

import java.util.logging.Logger;

public class RequestMultiSellChoose extends L2GameClientPacket
{
	private final static String _C__B0_MULTISELLCHOOSE = "[C] B0 MultiSellChoose";

	private final static Logger _log = Logger.getLogger(RequestMultiSellChoose.class.getName());
	private int _listId;
	private int _entryId;
	private long _amount;

	private int _enchant = 0;
	private byte _enchantAttr = -2;
	private int _enchantAttrVal = 0;
	private boolean _keepenchant = false;
	private boolean _notax = false;
	private MultiSellListContainer _list = null;
	private final GArray<ItemData> _items = new GArray<ItemData>();

	private class ItemData
	{
		private final int _id;
		private final long _count;
		private final L2ItemInstance _item;

		public ItemData(final int id, final long count, final L2ItemInstance item)
		{
			_id = id;
			_count = count;
			_item = item;
		}

		public int getId()
		{
			return _id;
		}

		public long getCount()
		{
			return _count;
		}

		public L2ItemInstance getItem()
		{
			return _item;
		}

		@Override
		public boolean equals(final Object obj)
		{
			if(!(obj instanceof ItemData))
				return false;

			final ItemData i = (ItemData) obj;

			return _id == i._id && _count == i._count && _item == i._item;
		}
	}

	@Override
	public void readImpl()
	{
		_listId = readD();
		_entryId = readD();
		_amount = readQ();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && activeChar.getKarma() > 0 && !activeChar.isGM())
		{
			activeChar.sendActionFailed();
			return;
		}

		_list = activeChar.getMultisell();
		// На всякий случай...
		if(_list == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		// Проверяем, не подменили ли id
		if(_list.getListId() != _listId)
		{
			activeChar.sendActionFailed();
			Util.handleIllegalPlayerAction(activeChar, "RequestMultiSellChoose[110]", "Tried to buy from multisell: " + _listId, 1);
			return;
		}

		if(_amount < 1)
		{
			activeChar.sendActionFailed();
			return;
		}

		_keepenchant = _list.isKeepEnchant();
		_notax = _list.isNoTax();

		for(final MultiSellEntry entry : _list.getEntries())
			if(entry.getEntryId() == _entryId)
			{
				doExchange(activeChar, entry);
				break;
			}
	}

	private void doExchange(final L2Player activeChar, final MultiSellEntry entry)
	{
		final PcInventory inv = activeChar.getInventory();

		int totalAdenaCost = 0;
		long tax;
		try
		{
			tax = SafeMath.safeMulLong(entry.getTax(), _amount); // налог
		}
		catch(final ArithmeticException e)
		{
			return;
		}

		// проверка последнего НПЦ с которым общался чар
		final L2NpcInstance merchant = activeChar.getLastNpc();
		final Castle castle = merchant != null ? merchant.getCastle(activeChar) : null;

		final int playerId = activeChar.getObjectId();
		final boolean logExchange = Config.LOG_MULTISELL_ID_LIST.contains(_listId);
		StringBuffer msgb = new StringBuffer();

		final GArray<MultiSellIngredient> productId = entry.getProduction();
		if(_keepenchant)
			for(final MultiSellIngredient p : productId)
				_enchant = Math.max(_enchant, p.getItemEnchant());

		if(logExchange)
			msgb.append("<multisell id=").append(_listId).append(" player=\"").append(activeChar.getName()).append("\" oid=").append(activeChar.getObjectId()).append(">\n");

		synchronized (inv)
		{
			final int slots = inv.slotsLeft();
			if(slots == 0)
			{
				activeChar.sendPacket(Msg.THE_WEIGHT_AND_VOLUME_LIMIT_OF_INVENTORY_MUST_NOT_BE_EXCEEDED);
				return;
			}

			int req = 0;
			long totalLoad = 0;
			for(final MultiSellIngredient i : productId)
			{
				if(i.getItemId() <= 0)
					continue;

				totalLoad += ItemTable.getInstance().getTemplate(i.getItemId()).getWeight() * _amount;
				if(!ItemTable.getInstance().getTemplate(i.getItemId()).isStackable())
					req += _amount;
				else
					req++;
			}
			if(req > slots || !inv.validateWeight(totalLoad))
			{
				activeChar.sendPacket(Msg.THE_WEIGHT_AND_VOLUME_LIMIT_OF_INVENTORY_MUST_NOT_BE_EXCEEDED);
				return;
			}

			if(entry.getIngredients().size() == 0)
			{
				_log.warning("WARNING Ingredients list = 0 multisell id=:" + _listId + " player:" + activeChar.getName());
				activeChar.sendActionFailed();
				return;
			}

			L2Augmentation augmentation = null;
			String source = null;

			// Перебор всех ингридиентов, проверка наличия и создание списка забираемого
			for(final MultiSellIngredient ingridient : entry.getIngredients())
			{
				final int ingridientItemId = ingridient.getItemId();
				final long ingridientItemCount = ingridient.getItemCount();
				long total;
				try
				{
					total = SafeMath.safeMulLong(ingridientItemCount, _amount);
				}
				catch(final ArithmeticException e)
				{
					Util.handleIllegalPlayerAction(activeChar, "RequestMultiSellChoose[214]", "tried an overflow exploit: buy " + _amount + " of " + productId + ", ingridient " + ingridientItemId + " count " + ingridientItemCount, 1);
					activeChar.sendActionFailed();
					return;
				}

				if(ingridientItemId > 0 && !ItemTable.getInstance().getTemplate(ingridientItemId).isStackable())
					for(int i = 0; i < ingridientItemCount * _amount; i++)
					{
						final L2ItemInstance[] list = inv.getAllItemsById(ingridientItemId);

						/** Если энчант имеет значение - то ищем вещи с точно таким энчантом */
						if(_keepenchant)
						{
							L2ItemInstance itemToTake = null;
							for(final L2ItemInstance itm : list)
								// уровень заточки передаётся только если выключено отображение всех предметов, а только тех что можно обменять.
								if((itm.getEnchantLevel() == _enchant || (itm.isArmor() || itm.isWeapon() || itm.isUnderwear()) && _list.isShowAll() || itm.getItem().getType2() > L2Item.TYPE2_ACCESSORY && !_list.isShowAll()) && !_items.contains(new ItemData(itm.getItemId(), itm.getCount(), itm)) && !itm.isShadowItem() && (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE)
								{
									itemToTake = itm;
									if(itm.getAttributeElement() != Elementals.ATTRIBUTE_NONE)
									{
										_enchantAttr = itm.getAttributeElement();
										_enchantAttrVal = itm.getAttributeElementValue();
									}
									break;
								}
							if(itemToTake == null)
							{
								activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
								return;
							}

							if(!checkItem(itemToTake, activeChar))
							{
								activeChar.sendActionFailed();
								return;
							}

							if(itemToTake.getAugmentation() != null)
							{
								itemToTake.setWhFlag(true);
								augmentation = itemToTake.getAugmentation();
								source = itemToTake.getCreateType();
							}
							_items.add(new ItemData(itemToTake.getItemId(), 1, itemToTake));
						}
						// Если энчант не обрабатывается берется вещь с наименьшим энчантом
						else
						{
							L2ItemInstance itemToTake = null;
							for(final L2ItemInstance itm : list)
								if(!_items.contains(new ItemData(itm.getItemId(), itm.getCount(), itm)) && (itemToTake == null || itm.getEnchantLevel() < itemToTake.getEnchantLevel()) && !itm.isShadowItem() && (itm.getCustomFlags() & L2ItemInstance.FLAG_NO_TRADE) != L2ItemInstance.FLAG_NO_TRADE && checkItem(itm, activeChar))
								{
									itemToTake = itm;
									if(itemToTake.getEnchantLevel() == 0)
										break;
								}

							if(itemToTake == null)
							{
								activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
								return;
							}
							if(itemToTake.getAugmentation() != null)
							{
								itemToTake.setWhFlag(true);
								augmentation = itemToTake.getAugmentation();
								source = itemToTake.getCreateType();
							}
							_items.add(new ItemData(itemToTake.getItemId(), 1, itemToTake));
						}
					}
				else if(ingridientItemId == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE)
				{
					if(activeChar.getClan() == null)
					{
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ARE_NOT_A_CLAN_MEMBER));
						return;
					}

					if(activeChar.getClan().getReputationScore() < total)
					{
						activeChar.sendPacket(new SystemMessage(SystemMessage.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW));
						return;
					}

					if(activeChar.getClan().getLeaderId() != activeChar.getObjectId())
					{
						activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_A_CLAN_LEADER).addString(activeChar.getName()));
						return;
					}
					_items.add(new ItemData(ingridientItemId, total, null));
				}
				else if(ingridientItemId == L2Item.ITEM_ID_PC_BANG_POINTS)
				{
					if(activeChar.getPcBangPoints() < total)
					{
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ARE_SHORT_OF_ACCUMULATED_POINTS));
						return;
					}
					_items.add(new ItemData(ingridientItemId, total, null));
				}
				else if(ingridientItemId == L2Item.ITEM_ID_FAME)
				{
					if(activeChar.getFame() < total)
					{
						activeChar.sendPacket(Msg.NOT_ENOUGH_FAME_POINTS);
						return;
					}
					_items.add(new ItemData(ingridientItemId, total, null));
				}
				else
				{
					if(ingridientItemId == 57)
						totalAdenaCost += ingridientItemCount * _amount;
					final L2ItemInstance item = inv.getItemByItemId(ingridientItemId);

					if(item == null || item.getCount() < total)
					{
						activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
						return;
					}

					_items.add(new ItemData(item.getItemId(), total, item));
				}

				if(activeChar.getAdena() < totalAdenaCost)
				{
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
					return;
				}

			} // get ingredients list (items list to take)

			// take ingredients from char
			for(final ItemData id : _items)
			{
				final long count = id.getCount();
				if(count > 0)
				{
					final L2ItemInstance item = id.getItem();
					if(item != null)
					{
						activeChar.sendPacket(SystemMessage.removeItems(item.getItemId(), count));
						if(logExchange)
							msgb.append("\t<destroy id=").append(item.getItemId()).append(" oid=").append(item.getObjectId()).append(" count=").append(id.getCount()).append(">\n");

						if(item.isEquipped())
							inv.unEquipItemInSlot(item.getEquipSlot());
						inv.destroyItem(item, count, true);
					}
					else if(id.getId() == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE)
					{
						final boolean reduceraterepscore = Config.USE_REDUCE_REPSCORE_RATE_MULTISELL;
						activeChar.getClan().incReputation((int) -count, reduceraterepscore, "MultiSell" + _listId);
						activeChar.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(count));
					}
					else if(id.getId() == L2Item.ITEM_ID_PC_BANG_POINTS)
					{
						activeChar.setPcBangPoints(activeChar.getPcBangPoints() - (int) count);
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ARE_USING_S1_POINT).addNumber(count));
						activeChar.sendPacket(new ExPCCafePointInfo(activeChar));
					}
					else if(id.getId() == L2Item.ITEM_ID_FAME)
					{
						final int fameCost = (int) (activeChar.getFame() - count * _amount);
						activeChar.setFame(fameCost);
					}
				}
			}

			if(tax > 0 && !_notax && castle != null)
			{
				// activeChar.reduceAdena(tax);
				if(activeChar.isGM())
					activeChar.sendMessage("Tax: " + tax);
				if(merchant != null && merchant.getReflection().getId() == 0)
				{
					castle.addToTreasury(tax, true, false);
					Log.add(castle.getName() + "|" + tax + "|Multisell[" + _listId + "]|" + activeChar.toString(), "treasury");
				}
			}

			// give production to char
			for(final MultiSellIngredient production : productId)
			{
				L2ItemInstance product;

				if(production.getItemId() <= 0)
				{
					if(production.getItemId() == L2Item.ITEM_ID_CLAN_REPUTATION_SCORE)
					{
						activeChar.getClan().incReputation((int) (production.getItemCount() * _amount), false, "MultiSell" + _listId);
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOUR_CLAN_HAS_ADDED_1S_POINTS_TO_ITS_CLAN_REPUTATION_SCORE).addNumber(production.getItemCount() * _amount));
					}
					else if(production.getItemId() == L2Item.ITEM_ID_PC_BANG_POINTS)
					{
						activeChar.setPcBangPoints(activeChar.getPcBangPoints() + (int) (production.getItemCount() * _amount));
						activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ACQUIRED_S1_PC_BANG_POINT).addNumber(production.getItemCount() * _amount), new ExPCCafePointInfo(activeChar));
					}
					else if(production.getItemId() == L2Item.ITEM_ID_FAME)
					{
						activeChar.setFame(activeChar.getFame() + (int) (production.getItemCount() * _amount));
					}
				}
				else if(ItemTable.getInstance().getTemplate(production.getItemId()).isStackable())
				{
					product = ItemTable.getInstance().createItem(production.getItemId(), playerId, 0, source != null ? source : "RequestMultiSellChoose[" + _listId + "|" + _entryId + "]");
					final long total = production.getItemCount() * _amount;

					if(total < 0 || total > Long.MAX_VALUE)
					{
						Util.handleIllegalPlayerAction(activeChar, "RequestMultiSellChoose[288]", "tried an overflow exploit: buy " + _amount * production.getItemCount() + " of " + product.getItemId() + ", base amount " + _amount, 1);
						return;
					}

					product.setCount(total);
					activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S).addItemName(product.getItemId()).addNumber(product.getCount()));
					if(logExchange)
						msgb.append("\t<add id=").append(product.getItemId()).append(" count=").append(product.getCount()).append(">\n");
					inv.addItem(product);
				}
				else
					for(int i = 0; i < _amount; i++)
						for(int s = 0; s < production.getItemCount(); s++)
						{
							product = inv.addItem(ItemTable.getInstance().createItem(production.getItemId(), playerId, 0, "RequestMultiSellChoose[" + _listId + "|" + _entryId + "]"));
							product.setCount(1);
							if(_keepenchant)
							{
								if(product.isWeapon() || product.isArmor() || product.isAccessory() || product.isUnderwear())
									product.setEnchantLevel(_enchant);
								if(_enchantAttr != Elementals.ATTRIBUTE_NONE)
									product.setAttributeElement(_enchantAttr, _enchantAttrVal, true);
							}

							if(production.getElementValue() > 0)
								product.setAttributeElement(production.getElement(), production.getElementValue(), true);

							if(augmentation != null && product.isEquipable())
							{
								augmentation.setItem(product);
								product.setAugmentation(augmentation);
							}

							if(logExchange)
								msgb.append("\t<add id=").append(product.getItemId()).append(" oid=").append(product.getObjectId()).append(" count=").append(product.getCount()).append(">\n");
							activeChar.sendPacket(SystemMessage.obtainItems(product));
						}
			}
		}

		activeChar.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
		if(logExchange)
		{
			msgb.append("</multisell>\n");
			Log.add(msgb.toString(), "multisell", false);
			msgb = null;
		}

		if(_list == null || !_list.isShowAll()) // Если показывается только то, на что хватает материалов обновить окно у игрока
			L2Multisell.getInstance().separateAndSend(_listId, activeChar, castle == null ? 0 : castle.getTaxRate());
	}

	private boolean checkItem(final L2ItemInstance temp, final L2Player activeChar)
	{
		if(temp == null)
			return false;
		if(temp.isShadowItem())
			return false;
		if(temp.isTemporalItem())
			return false;
		if(PetDataTable.isPetControlItem(temp) && activeChar.isMounted())
			return false;
		if(temp.isEquipped())
			return false;
		if(temp.isWear())
			return false;
		return true;
	}

	@Override
	public String getType()
	{
		return _C__B0_MULTISELLCHOOSE;
	}
}
