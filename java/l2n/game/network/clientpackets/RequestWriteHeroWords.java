package l2n.game.network.clientpackets;

/**
 * Format chS
 * c (id) 0xD0
 * h (subid) 0x05
 * S the hero's words :)
 */
public class RequestWriteHeroWords extends L2GameClientPacket
{
	private static String _C__FE_05_REQUESTWRITEHEROWORDS = "[C] D0:05 RequestWriteHeroWords";
	@SuppressWarnings("unused")
	private String _heroWords;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_heroWords = readS();
	}

	@Override
	public void runImpl()
	{}

	@Override
	public String getType()
	{
		return _C__FE_05_REQUESTWRITEHEROWORDS;
	}
}
