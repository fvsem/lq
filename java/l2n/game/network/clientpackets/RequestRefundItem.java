package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.TradeController;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.*;
import l2n.game.network.serverpackets.ExBuySellList;
import l2n.game.network.serverpackets.ExRefundList;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 18:12:15
 */
public class RequestRefundItem extends L2GameClientPacket
{
	private int _listId;
	private int[] _items;

	@Override
	public void readImpl()
	{
		_listId = readD();
		final int count = readD();
		if(count <= 0 || count > Config.MAX_ITEM_IN_PACKET || count * 4 != _buf.remaining())
			return;

		_items = new int[count];
		for(int i = 0; i < count; i++)
			_items[i] = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		final ConcurrentLinkedQueue<L2ItemInstance> list = activeChar.getInventory().getRefundItemsList();
		if(list == null || list.isEmpty())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2NpcInstance npc = activeChar.getLastNpc();
		final boolean isValidMerchant = npc instanceof L2ClanHallManagerInstance || npc instanceof L2MerchantInstance || npc instanceof L2MercManagerInstance || npc instanceof L2CastleChamberlainInstance || npc instanceof L2NpcFriendInstance;
		if(!activeChar.isGM() && (npc == null || !isValidMerchant || !activeChar.isInRange(npc.getLoc(), L2Character.INTERACTION_DISTANCE)))
		{
			activeChar.sendActionFailed();
			return;
		}

		final GArray<L2ItemInstance> toReturn = new GArray<L2ItemInstance>(_items.length);
		long price = 0;
		long weight = 0;

		for(final int itemId : _items)
			for(final L2ItemInstance item : list)
				if(item.getObjectId() == itemId)
				{
					price += item.getCount() * item.getReferencePrice() / 2;
					weight += item.getCount() * item.getItem().getWeight();
					toReturn.add(item);
				}

		if(toReturn.isEmpty())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getAdena() < price)
		{
			activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA, Msg.ActionFail);
			return;
		}

		if(!activeChar.getInventory().validateWeight(weight))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT, Msg.ActionFail);
			return;
		}

		if(!activeChar.getInventory().validateCapacity(toReturn))
		{
			sendPacket(Msg.YOUR_INVENTORY_IS_FULL, Msg.ActionFail);
			return;
		}

		if(!activeChar.getInventory().validateWeight(weight))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT, Msg.ActionFail);
			return;
		}

		activeChar.reduceAdena(price, true);

		for(final L2ItemInstance itm : toReturn)
		{
			list.remove(itm);
			activeChar.getInventory().addItem(itm);
		}

		double taxRate = 0;
		Castle castle = null;
		if(npc != null)
		{
			castle = npc.getCastle(activeChar);
			if(castle != null)
				taxRate = castle.getTaxRate();
		}

		activeChar.sendPacket(new ExRefundList(activeChar), new ExBuySellList(TradeController.getInstance().getBuyList(_listId), activeChar, taxRate).done());
		activeChar.updateStats();
	}
}
