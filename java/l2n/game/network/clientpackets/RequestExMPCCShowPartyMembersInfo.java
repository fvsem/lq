package l2n.game.network.clientpackets;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExMPCCShowPartyMemberInfo;

/**
 * Format: ch d
 * Пример пакета:
 * D0 2E 00 4D 90 00 10
 */
public class RequestExMPCCShowPartyMembersInfo extends L2GameClientPacket
{
	private static String _C__D0_2d_REQUESTEXMPCCSHOWPARTYMEMBERINFO = "[C] D0:2d RequestExMPCCShowPartyMembersInfo";
	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();

		if(activeChar == null || !activeChar.isInParty() || !activeChar.getParty().isInCommandChannel())
			return;

		final L2Player partyLeader = L2ObjectsStorage.getPlayer(_objectId);
		if(partyLeader != null && partyLeader.getParty() != null)
			activeChar.sendPacket(new ExMPCCShowPartyMemberInfo(partyLeader));
	}

	@Override
	public String getType()
	{
		return _C__D0_2d_REQUESTEXMPCCSHOWPARTYMEMBERINFO;
	}
}
