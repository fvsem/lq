package l2n.game.network.clientpackets;

import l2n.game.instancemanager.FortressManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.network.serverpackets.ExShowFortressSiegeInfo;

/**
 * @author L2System Project
 */
public class RequestFortressSiegeInfo extends L2GameClientPacket
{

	@Override
	public String getType()
	{
		return "[C] D0:42 RequestFortressSiegeInfo";
	}

	@Override
	protected void readImpl()
	{
		// trigger
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();

		if(activeChar == null)
			return;
		for(final Fortress fort : FortressManager.getInstance().getFortresses().values())
			if(fort != null && fort.getSiege().isInProgress())
				activeChar.sendPacket(new ExShowFortressSiegeInfo(fort));
	}

}
