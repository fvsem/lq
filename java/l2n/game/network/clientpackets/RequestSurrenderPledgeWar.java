package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;

import java.util.logging.Logger;

public class RequestSurrenderPledgeWar extends L2GameClientPacket
{
	private static final String _C__51_REQUESTSURRENDERPLEDGEWAR = "[C] 51 RequestSurrenderPledgeWar";
	private static Logger _log = Logger.getLogger(RequestSurrenderPledgeWar.class.getName());

	String _pledgeName;
	L2Clan _clan;
	L2Player _activeChar;

	@Override
	protected void readImpl()
	{
		_pledgeName = readS();
	}

	@Override
	protected void runImpl()
	{
		_activeChar = getClient().getActiveChar();
		if(_activeChar == null)
			return;

		_clan = _activeChar.getClan();
		if(_clan == null)
			return;

		final L2Clan clan = ClanTable.getInstance().getClanByName(_pledgeName);
		if(clan == null)
		{
			_activeChar.sendMessage("No such clan.");
			_activeChar.sendActionFailed();
			return;
		}

		_log.info("RequestSurrenderPledgeWar by " + getClient().getActiveChar().getClan().getName() + " with " + _pledgeName);

		if(!_clan.isAtWarWith(clan.getClanId()))
		{
			_activeChar.sendMessage("You aren't at war with this clan.");
			_activeChar.sendActionFailed();
			return;
		}

		SystemMessage msg = new SystemMessage(SystemMessage.YOU_HAVE_SURRENDERED_TO_THE_S1_CLAN);
		msg.addString(_pledgeName);
		_activeChar.sendPacket(msg);
		msg = null;
		_activeChar.deathPenalty(null);
		ClanTable.getInstance().stopClanWar(_clan, clan);
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTSURRENDERPLEDGEWAR;
	}
}
