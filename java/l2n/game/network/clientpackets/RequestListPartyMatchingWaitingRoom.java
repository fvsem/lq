package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExListPartyMatchingWaitingRoom;

import java.util.logging.Logger;

/**
 * Format: (ch) dddd
 * 
 * @author Crion/kombat
 */

public class RequestListPartyMatchingWaitingRoom extends L2GameClientPacket
{
	protected static final Logger _log = Logger.getLogger(RequestListPartyMatchingWaitingRoom.class.getName());
	private static final String _C__D0_31_REQUESTLISTPARTYMATCHINGWAITINGROOM = "[C] D0:31 RequestListPartyMatchingWaitingRoom";

	private int _minLevel;
	private int _maxLevel;
	private int _page;
	private int _unk;

	@Override
	protected void readImpl()
	{
		_page = readD();
		_minLevel = readD();
		_maxLevel = readD();
		_unk = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExListPartyMatchingWaitingRoom(activeChar, _minLevel, _maxLevel, _page));
	}

	@Override
	public String getType()
	{
		return _C__D0_31_REQUESTLISTPARTYMATCHINGWAITINGROOM;
	}
}
