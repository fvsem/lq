package l2n.game.network.clientpackets;

import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;

public class RequestPartyMatchDetail extends L2GameClientPacket
{
	private static String _C__71_REQUESTPARTYMATCHDETAIL = "[C] 71 RequestPartyMatchDetail";

	private int _roomId;
	private int _mode;
	private int _level;
	private int _unk;

	/**
	 * packet type id 0x71 sample 71 d8 a8 10 41 object id format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_roomId = readD();
		_mode = readD();
		_level = readD();
		_unk = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_roomId > 0)
			PartyRoomManager.getInstance().joinPartyRoom(activeChar, _roomId);
		else
			for(final PartyRoom room : PartyRoomManager.getInstance().getRooms(_mode, _level, activeChar))
				if(room.getMembersSize() < room.getMaxMembers())
					PartyRoomManager.getInstance().joinPartyRoom(activeChar, room.getId());
	}

	@Override
	public String getType()
	{
		return _C__71_REQUESTPARTYMATCHDETAIL;
	}
}
