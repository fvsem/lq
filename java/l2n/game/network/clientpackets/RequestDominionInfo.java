package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.network.serverpackets.ExReplyDominionInfo;
import l2n.game.network.serverpackets.ExShowOwnthingPos;

/**
 * @author L2System
 * @date 19.04.2010
 * @time 3:52:30
 */
public class RequestDominionInfo extends L2GameClientPacket
{
	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExReplyDominionInfo());
		if(TerritorySiege.isInProgress())
			activeChar.sendPacket(new ExShowOwnthingPos());
	}

	@Override
	public void readImpl()
	{}

	@Override
	public String getType()
	{
		return "[C] D0:58 RequestDominionInfo";
	}
}
