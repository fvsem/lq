package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * @author L2System
 * @date 01.03.2010
 * @time 1:16:26
 */
public class RequestHennaUnequip extends L2GameClientPacket
{

	private static final String _C__72_RequestHennaRemove = "[C] 72 RequestHennaUnequip";
	private int _symbolId;

	@Override
	protected void readImpl()
	{
		_symbolId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		for(int i = 1; i <= 3; i++)
		{
			final L2HennaInstance henna = activeChar.getHenna(i);
			if(henna != null && henna.getSymbolId() == _symbolId)
				if(activeChar.getAdena() >= henna.getPrice() / 5)
					activeChar.removeHenna(i);
				else
					sendPacket(new SystemMessage(SystemMessage.YOU_DO_NOT_HAVE_ENOUGH_ADENA));
		}
	}

	@Override
	public String getType()
	{
		return _C__72_RequestHennaRemove;
	}
}
