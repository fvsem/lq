/**
 * 
 */
package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SkillList;

/**
 * @author L2System Project
 * @date 10.12.2009
 * @time 22:18:47
 */
public class RequestSkillList extends L2GameClientPacket
{
	private static final String _C__3F_REQUESTSKILLLIST = "[C] 3F RequestSkillList";

	@Override
	public String getType()
	{
		return _C__3F_REQUESTSKILLLIST;
	}

	@Override
	protected void readImpl()
	{
		// this is just a trigger packet. it has no content
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha != null)
			cha.sendPacket(new SkillList(cha));
	}

}
