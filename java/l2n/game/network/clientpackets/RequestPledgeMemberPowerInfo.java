package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeReceivePowerInfo;

public class RequestPledgeMemberPowerInfo extends L2GameClientPacket
{
	private static String _C__D0_14_REQUESTPLEDGEMEMBERPOWERINFO = "[C] D0:14 RequestPledgeMemberPowerInfo";
	// format: chdS
	@SuppressWarnings("unused")
	private int _not_known;
	private String _target;

	@Override
	public void readImpl()
	{
		_not_known = readD();
		_target = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan != null)
		{
			final L2ClanMember cm = clan.getClanMember(_target);
			if(cm != null)
				activeChar.sendPacket(new PledgeReceivePowerInfo(cm));
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_14_REQUESTPLEDGEMEMBERPOWERINFO;
	}
}
