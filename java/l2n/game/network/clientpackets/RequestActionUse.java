package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.model.*;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.instances.*;
import l2n.game.network.CustomSystemMessageId;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.EffectType;
import l2n.game.tables.GmListTable;
import l2n.game.tables.PetSkillsTable;
import l2n.game.tables.SkillTable;

import java.util.logging.Logger;

public class RequestActionUse extends L2GameClientPacket
{
	private static final String _C__56_REQUESTACTIONUSE = "[C] 56 RequestActionUse";
	private static final Logger _log = Logger.getLogger(RequestActionUse.class.getName());

	private int _actionId;
	private boolean _ctrlPressed;
	private boolean _shiftPressed;

	/**
	 * packet type id 0x56 format: cddc
	 */
	@Override
	public void readImpl()
	{
		_actionId = readD();
		_ctrlPressed = readD() == 1;
		_shiftPressed = readC() == 1;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTransformationId() != 0)
			// в человеческих трансформах, не меняющих внешний вид, можно использовать действия (садиться, ...)
			if(!activeChar.getTransform().isDefaultActionListTransform())
			{
				activeChar.sendPacket(CustomSystemMessageId.L2PLAYER_CANNOT_IN_TRANSFORM.getPacket(), Msg.ActionFail);
				return;
			}

		boolean usePet;
		switch (_actionId)
		{
			case 16:
			case 17:
			case 19:
			case 22:
			case 23:
			case 32:
			case 36:
			case 39:
			case 41:
			case 42:
			case 43:
			case 44:
			case 45:
			case 46:
			case 47:
			case 48:
			case 52:
			case 53:
			case 54:
			case 1003:
			case 1004:
			case 1005:
			case 1006:
			case 1007:
			case 1008:
			case 1009:
			case 1010:
			case 1011:
			case 1012:
			case 1013:
			case 1014:
			case 1015:
			case 1016:
			case 1017:
			case 1031:
			case 1032:
			case 1033:
			case 1034:
			case 1035:
			case 1036:
			case 1037:
			case 1038:
			case 1039:
			case 1040:
			case 1041:
			case 1042:
			case 1043:
			case 1044:
			case 1045:
			case 1046:
			case 1047:
			case 1048:
			case 1049:
			case 1050:
			case 1051:
			case 1052:
			case 1053:
			case 1054:
			case 1055:
			case 1056:
			case 1057:
			case 1058:
			case 1059:
			case 1060:
			case 1061:
			case 1062:
			case 1063:
			case 1064:
			case 1065:
			case 1066:
			case 1067:
			case 1068:
			case 1069:
			case 1070:
			case 1071:
			case 1072:
			case 1073:
			case 1074:
			case 1075:
			case 1076:
			case 1077:
			case 1078:
			case 1079:
			case 1080:
			case 1081:
			case 1082:
			case 1083:
			case 1084:
			case 1085:
			case 1086:
			case 1087:
			case 1088:
				usePet = true;
				break;
			default:
				usePet = false;
		}

		// dont do anything if player is dead or confused
		if(!usePet && (activeChar.isOutOfControl() || activeChar.isActionsDisabled()) && !(activeChar.isFakeDeath() && _actionId == 0))
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Object target = activeChar.getTarget();
		final L2Summon pet = activeChar.getPet();

		if(usePet && pet == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(Config.DEBUG_PACKETS)
			_log.warning("RequestActionUse: actionId " + _actionId);

		switch (_actionId)
		{
			case 0: // Сесть/встать
				// На страйдере нельзя садиться
				if(activeChar.isMounted())
				{
					activeChar.sendActionFailed();
					break;
				}
				final int distance = (int) activeChar.getDistance(activeChar.getTarget());
				if(target != null && !activeChar.isSitting() && target instanceof L2StaticObjectInstance && ((L2StaticObjectInstance) target).getType() == 1 && distance <= L2Character.INTERACTION_DISTANCE)
				{
					final ChairSit cs = new ChairSit(activeChar, ((L2StaticObjectInstance) target).getStaticObjectId());
					activeChar.sendPacket(cs);
					activeChar.sitDown();
					activeChar.broadcastPacket(cs);
					break;
				}

				if(activeChar.isFakeDeath())
				{
					activeChar.getEffectList().stopEffects(EffectType.FakeDeath);
					activeChar.updateEffectIcons();
				}
				else if(activeChar.isSitting())
					activeChar.standUp();
				else
					activeChar.sitDown();
				break;
			case 1: // Изменить тип передвижения, шаг/бег
				if(activeChar.isRunning())
					activeChar.setWalking();
				else
					activeChar.setRunning();
				break;
			case 7: // Next Target
				L2Character nearest_target = null;
				for(final L2Character cha : L2World.getAroundCharacters(activeChar, 400, 200))
					if(cha != null && !cha.isAlikeDead())
						if((nearest_target == null || activeChar.getDistance3D(cha) < activeChar.getDistance3D(nearest_target)) && cha.isAutoAttackable(activeChar))
							nearest_target = cha;
				if(nearest_target != null && activeChar.getTarget() != nearest_target)
				{
					activeChar.setTarget(nearest_target);
					if(activeChar.getTarget() == nearest_target)
						if(nearest_target.isNpc())
						{
							activeChar.sendPacket(new MyTargetSelected(nearest_target.getObjectId(), activeChar.getLevel() - nearest_target.getLevel()));
							activeChar.sendPacket(nearest_target.makeStatusUpdate(StatusUpdate.CUR_HP, StatusUpdate.MAX_HP));
							activeChar.sendPacket(new ValidateLocation(nearest_target), Msg.ActionFail);
						}
						else
							activeChar.sendPacket(new MyTargetSelected(nearest_target.getObjectId(), 0));
					return;
				}
				break;
			case 10: // Запрос на создание приватного магазина продажи
			case 61: // Запрос на создание приватного магазина продажи пакетом
				if(activeChar.isInTransaction())
					activeChar.getTransaction().cancel();
				if(activeChar.getTradeList() != null)
				{
					activeChar.getTradeList().removeAll();
					activeChar.sendPacket(SendTradeDone.Fail);
				}
				else
					activeChar.setTradeList(new L2TradeList(0));

				activeChar.getTradeList().updateSellList(activeChar, activeChar.getSellList());
				activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
				activeChar.standUp();
				activeChar.broadcastCharInfo();

				if(!activeChar.checksForShop(false))
				{
					activeChar.sendActionFailed();
					return;
				}
				activeChar.sendPacket(new PrivateStoreManageList(activeChar, _actionId == 61));
				break;
			// CT2.3 Social Packets
			case 12:
				useSocial(2, activeChar);
				break;
			case 13:
				useSocial(3, activeChar);
				break;
			case 14:
				useSocial(4, activeChar);
				break;
			case 24:
				useSocial(6, activeChar);
				break;
			case 25:
				useSocial(5, activeChar);
				break;
			case 26:
				useSocial(7, activeChar);
				break;
			case 29:
				useSocial(8, activeChar);
				break;
			case 30:
				useSocial(9, activeChar);
				break;
			case 31:
				useSocial(10, activeChar);
				break;
			case 33:
				useSocial(11, activeChar);
				break;
			case 34:
				useSocial(12, activeChar);
				break;
			case 35:
				useSocial(13, activeChar);
				break;
			case 62:
				useSocial(14, activeChar);
				break;
			case 63: // mini game
				break;
			case 66:
				useSocial(15, activeChar);
				break;
			case 28: // Запрос на создание приватного магазина покупки
				if(activeChar.isInTransaction())
					activeChar.getTransaction().cancel();
				if(activeChar.getTradeList() != null)
				{
					activeChar.getTradeList().removeAll();
					activeChar.sendPacket(SendTradeDone.Fail);
				}
				else
					activeChar.setTradeList(new L2TradeList(0));

				activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
				activeChar.standUp();
				activeChar.broadcastCharInfo();

				if(!activeChar.checksForShop(false))
				{
					activeChar.sendActionFailed();
					return;
				}
				activeChar.sendPacket(new PrivateStoreManageListBuy(activeChar));
				break;
			case 15:
			case 21: // Follow для пета
				if(pet != null)
				{
					pet.setFollowTarget(pet.getPlayer());
					pet.setFollowStatus(!pet.isFollow(), true);
				}
				break;
			case 16:
			case 22: // Атака петом
				if(target == null || pet == target || pet.isDead())
				{
					activeChar.sendActionFailed();
					return;
				}
				pet.doAttack(target, _ctrlPressed, _shiftPressed);
				break;
			case 17:
			case 23: // Отмена действия у пета
				pet.setFollowTarget(pet.getPlayer());
				pet.setFollowStatus(pet.isFollow(), true);
				pet.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE, null, null);
				break;
			case 19: // Отзыв пета
				if(pet.isDead())
				{
					activeChar.sendPacket(Msg.A_DEAD_PET_CANNOT_BE_SENT_BACK, Msg.ActionFail);
					return;
				}

				if(pet.isInCombat())
				{
					activeChar.sendPacket(Msg.A_PET_CANNOT_BE_SENT_BACK_DURING_BATTLE, Msg.ActionFail);
					break;
				}

				if(pet.isPet() && pet.isHungry())
				{
					activeChar.sendPacket(Msg.YOU_CANNOT_RESTORE_HUNGRY_PETS, Msg.ActionFail);
					break;
				}

				pet.unSummon();
				break;
			case 32: // Wild Hog Cannon - Mode Change
				useSkill(4230, target);
				break;
			case 36: // Soulless - Toxic Smoke
				useSkill(4259, target);
				break;
			case 37: // Создание магазина Common Craft
			{
				if(activeChar.isInTransaction())
					activeChar.getTransaction().cancel();
				if(activeChar.getCreateList() == null)
					activeChar.setCreateList(new L2ManufactureList());
				activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
				activeChar.standUp();
				activeChar.broadcastCharInfo();

				if(!activeChar.checksForShop(true))
				{
					activeChar.sendActionFailed();
					return;
				}
				activeChar.sendPacket(new RecipeShopManageList(activeChar, true));
				break;
			}
			case 38: // Mount
				// В режиме трансформации нельзя садиться на петов
				if(activeChar.getTransformationId() != 0)
					activeChar.sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
				if(pet != null && pet.isMountable() && !activeChar.isMounted())
				{
					if(activeChar.checksForMount(true, true))
					{
						activeChar.setMount(pet.getTemplate().npcId, pet.getObjectId());
						pet.unSummon();
					}
				}
				else if(activeChar.isMounted())
				{
					if(activeChar.isFlying() && !activeChar.checkLandingState()) // Виверна
					{
						activeChar.sendPacket(Msg.YOU_ARE_NOT_ALLOWED_TO_DISMOUNT_AT_THIS_LOCATION, Msg.ActionFail);
						return;
					}

					if(activeChar.isInVehicle() || activeChar.isCastingNow() || activeChar.isParalyzed())
					{
						activeChar.sendPacket(Msg.YOU_CANNOT_MOUNT_BECAUSE_YOU_DO_NOT_MEET_THE_REQUIREMENTS);
						return;
					}
					activeChar.setMount(0, 0);
				}
				break;
			case 39: // Soulless - Parasite Burst
				useSkill(4138, target);
				break;
			case 41: // Wild Hog Cannon - Attack
				useSkill(4230, target);
				break;
			case 42: // Kai the Cat - Self Damage Shield
				useSkill(4378, activeChar);
				break;
			case 43: // Unicorn Merrow - Hydro Screw
				useSkill(4137, target);
				break;
			case 44: // Big Boom - Boom Attack
				useSkill(4139, target);
				break;
			case 45: // Unicorn Boxer - Master Recharge
				useSkill(4025, activeChar);
				break;
			case 46: // Mew the Cat - Mega Storm Strike
				useSkill(4261, target);
				break;
			case 47: // Silhouette - Steal Blood
				useSkill(4260, target);
				break;
			case 48: // Mechanic Golem - Mech. Cannon
				useSkill(4068, target);
				break;
			case 51: // Создание магазина Dwarven Craft
			{
				if(!activeChar.checksForShop(true))
				{
					activeChar.sendActionFailed();
					return;
				}

				activeChar.standUp();
				if(activeChar.getCreateList() == null)
					activeChar.setCreateList(new L2ManufactureList());

				activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
				activeChar.broadcastCharInfo();
				activeChar.sendPacket(new RecipeShopManageList(activeChar, false));
				break;
			}
			case 52: // unsummon
				if(pet.isInCombat())
				{
					activeChar.sendPacket(Msg.A_PET_CANNOT_BE_SENT_BACK_DURING_BATTLE, Msg.ActionFail);
					break;
				}
				pet.unSummon();
				break;
			case 53: // move to target
			case 54:
				if(target != null && pet != target && !pet.isMovementDisabled())
				{
					pet.setFollowStatus(false, true);
					pet.moveToLocation(target.getLoc(), 100, true);
				}
				break;
			case 64:
			{
				// Книга Телепортов
				break;
			}
			case 65:
				// Bot report Button.
				if(target != null && target.isPlayer())
				{
					GmListTable.broadcastMessageToGMs("Player " + activeChar.getName() + "[" + activeChar.getObjectId() + "]" + " alerted player " + target.getPlayer().getName() + "[" + target.getObjectId() + "]" + " to using Bot!");
					activeChar.sendMessage("Bot informing completed, thanks for you!");
					_log.info("Player " + activeChar.getName() + "[" + activeChar.getObjectId() + "]" + " alerted player " + target.getPlayer().getName() + "[" + target.getObjectId() + "]" + " to using Bot!");
				}
				break;
			case 67: // Steer. Allows you to control the Airship.
				L2AirShip.controlSteer(activeChar);
				activeChar.sendActionFailed();
				break;
			case 68: // Cancel Control. Relinquishes control of the Airship.
				L2AirShip.controlCancel(activeChar);
				activeChar.sendActionFailed();
				break;
			case 69: // Destination Map. Choose from pre-designated locations.
				L2AirShip.controlDestination(activeChar);
				activeChar.sendActionFailed();
				break;
			case 70: // Exit Airship. Disembarks from the Airship.
				L2AirShip.controlExit(activeChar);
				activeChar.sendActionFailed();
				break;
			case 96: // Quit Party Command Channel?
				_log.info("96 Accessed");
				break;
			case 97: // Request Party Command Channel Info?
				_log.info("97 Accessed");
				break;
			case 1000: // Siege Golem - Siege Hammer
				if(target instanceof L2DoorInstance)
					useSkill(4079, target);
				break;
			case 1001: // Sin Eater - Ultimate Bombastic Buster
				break;
			case 1003: // Wind Hatchling/Strider - Wild Stun
				useSkill(4710, target);
				break;
			case 1004: // Wind Hatchling/Strider - Wild Defense
				useSkill(4711, activeChar);
				break;
			case 1005: // Star Hatchling/Strider - Bright Burst
				useSkill(4712, target);
				break;
			case 1006: // Star Hatchling/Strider - Bright Heal
				useSkill(4713, activeChar);
				break;
			case 1007: // Cat Queen - Blessing of Queen
				useSkill(4699, activeChar);
				break;
			case 1008: // Cat Queen - Gift of Queen
				useSkill(4700, activeChar);
				break;
			case 1009: // Cat Queen - Cure of Queen
				useSkill(4701, target);
				break;
			case 1010: // Unicorn Seraphim - Blessing of Seraphim
				useSkill(4702, activeChar);
				break;
			case 1011: // Unicorn Seraphim - Gift of Seraphim
				useSkill(4703, activeChar);
				break;
			case 1012: // Unicorn Seraphim - Cure of Seraphim
				useSkill(4704, target);
				break;
			case 1013: // Nightshade - Curse of Shade
				useSkill(4705, target);
				break;
			case 1014: // Nightshade - Mass Curse of Shade
				useSkill(4706, target);
				break;
			case 1015: // Nightshade - Shade Sacrifice
				useSkill(4707, target);
				break;
			case 1016: // Cursed Man - Cursed Blow
				useSkill(4709, target);
				break;
			case 1017: // Cursed Man - Cursed Strike/Stun
				useSkill(4708, target);
				break;
			case 1031: // Feline King - Slash
				useSkill(5135, target);
				break;
			case 1032: // Feline King - Spinning Slash
				useSkill(5136, target);
				break;
			case 1033: // Feline King - Grip of the Cat
				useSkill(5137, target);
				break;
			case 1034: // Magnus the Unicorn - Whiplash
				useSkill(5138, target);
				break;
			case 1035: // Magnus the Unicorn - Tridal Wave
				useSkill(5139, target);
				break;
			case 1036: // Spectral Lord - Corpse Kaboom
				useSkill(5142, target);
				break;
			case 1037: // Spectral Lord - Dicing Death
				useSkill(5141, target);
				break;
			case 1038: // Spectral Lord - Force Curse
				useSkill(5140, target);
				break;
			case 1039: // Swoop Cannon - Cannon Fodder (не может атаковать двери и флаги)
				if(!(target instanceof L2DoorInstance) && !(target instanceof L2SiegeHeadquarterInstance))
					useSkill(5110, target);
				break;
			case 1040: // Swoop Cannon - Big Bang (не может атаковать двери и флаги)
				if(!(target instanceof L2DoorInstance) && !(target instanceof L2SiegeHeadquarterInstance))
					useSkill(5111, target);
				break;
			case 1041: // Great Wolf - 5442 - Bite Attack
				useSkill(5442, target);
				break;
			case 1042: // Great Wolf - 5444 - Moul
				useSkill(5444, target);
				break;
			case 1043: // Great Wolf - 5443 - Cry of the Wolf
				useSkill(5443, target);
				break;
			case 1044: // Great Wolf - Awakening
				useSkill(5445, target);
				break;
			case 1045: // Wolf Howl
				useSkill(5584, target);
				break;
			case 1046: // Strider Roar
				useSkill(5585, target);
				break;
			case 1047: // Divine Beast Bite
				useSkill(5580, target);
				break;
			case 1048: // Divine Beast Stun Attack
				useSkill(5581, target);
				break;
			case 1049:// Divine Beast Fire Breath
				useSkill(5582, target);
				break;
			case 1050:// Divine Beast Roar
				useSkill(5583, target);
				break;
			case 1051: // Feline Queen - Bless the Body
				useSkill(5638, target);
				break;
			case 1052: // Feline Queen - Bless the Soul
				useSkill(5639, target);
				break;
			case 1053: // Feline Queen - Haste
				useSkill(5640, target);
				break;
			case 1054: // Unicorn Seraphim - Acumen
				useSkill(5643, target);
				break;
			case 1055: // Unicorn Seraphim - Clarity
				useSkill(5647, target);
				break;
			case 1056: // Unicorn Seraphim - Empower
				useSkill(5648, target);
				break;
			case 1057: // Unicorn Seraphim - Wild Magic
				useSkill(5646, target);
				break;
			case 1058: // Nightshade - Death Whisper
				useSkill(5652, target);
				break;
			case 1059: // Nightshade - Focus
				useSkill(5653, target);
				break;
			case 1060: // Nightshade - Guidance
				useSkill(5654, target);
				break;
			case 1061: // CT 2.2 SpecialPet action
				useSkill(5745); // Death blow
				break;
			case 1062: // CT 2.2 SpecialPet action
				useSkill(5746); // Double attack
				break;
			case 1063: // CT 2.2 SpecialPet action
				useSkill(5747); // Spin attack
				break;
			case 1064: // CT 2.2 SpecialPet action
				useSkill(5748); // Meteor Shower
				break;
			case 1065: // CT 2.2 SpecialPet action
				useSkill(5753); // Awakening
				break;
			case 1066: // CT 2.2 SpecialPet action
				useSkill(5749); // Thunder Bolt
				break;
			case 1067: // CT 2.2 SpecialPet action
				useSkill(5750); // Flash
				break;
			case 1068: // CT 2.2 SpecialPet action
				useSkill(5751); // Lightning Wave
				break;
			case 1069: // CT 2.2 SpecialPet action
				useSkill(5752); // Flare
				break;
			case 1070:
				if(pet != null && pet instanceof L2SupportPetInstance)
					((L2SupportPetInstance) pet).toggleSupportCombatMode();
				break;
			case 1071: // CT 2.2 SpecialPet action
				useSkill(5761); // Power Strike
				break;
			case 1072:
				useSkill(6046); // Piercing attack
				break;
			case 1073:
				useSkill(6047); // Whirlwind
				break;
			case 1074:
				useSkill(6048); // Lance Smash
				break;
			case 1075:
				useSkill(6049); // Battle Cry
				break;
			case 1076:
				useSkill(6050); // Power Smash
				break;
			case 1077:
				useSkill(6051); // Energy Burst
				break;
			case 1078:
				useSkill(6052); // Shockwave
				break;
			case 1079:
				useSkill(6053); // Howl
				break;
			case 1080:
				useSkill(6041); // Phoenix Rush
				break;
			case 1081:
				useSkill(6042); // Phoenix Cleanse
				break;
			case 1082:
				useSkill(6043); // Phoenix Flame Feather
				break;
			case 1083:
				useSkill(6044); // Phoenix Flame Beak
				break;
			case 1084: // (Spirit Shaman, Toy Knight, Turtle Ascetic) Switch State - Toggles you between Attack and Support modes.
				if(pet != null && pet instanceof L2SupportPetInstance)
					((L2SupportPetInstance) pet).toggleSupportCombatMode();
				break;
			case 1086:
				useSkill(6094); // Panther Cancel
				break;
			case 1087:
				useSkill(6095); // Panther Dark Claw
				break;
			case 1088:
				useSkill(6096); // Panther Fatal Claw
				break;
			default:
				_log.warning("unhandled action type " + _actionId);
		}
		activeChar.sendActionFailed();
	}

	/**
	 * Cast a skill for active pet/servitor. Target is retrieved from owner' target, then validated by overloaded method useSkill(int, L2Character).
	 */
	private void useSkill(final int skillId)
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		useSkill(skillId, activeChar.getTarget());
	}

	private void useSkill(final int skillId, final L2Object target)
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Summon pet = activeChar.getPet();
		if(pet == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(target == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!target.isCharacter())
		{
			activeChar.sendActionFailed();
			return;
		}

		int skillLevel = 0;
		if(pet.isPet())
			skillLevel = ((L2PetInstance) pet).getPetRegularSkillLevel(skillId);
		else
			skillLevel = PetSkillsTable.getInstance().getAvailableLevel(pet, skillId);

		if(skillLevel == 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(skillId, skillLevel);
		if(skill == null)
		{
			_log.warning("RequestActionUse: cannot find pet/summon skill " + skillId + " for npcId " + pet.getNpcId() + "; check table pet_skills.");
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getLevel() + 20 <= pet.getLevel())
		{
			activeChar.sendPacket(Msg.THE_PET_IS_TOO_HIGH_LEVEL_TO_CONTROL);
			return;
		}

		if(skill.isOffensive() && (target == activeChar || target == pet))
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		pet.setTarget(target);
		final L2Character aimingTarget = skill.getAimingTarget(pet, pet.getTarget());
		if(skill.checkCondition(pet, aimingTarget, _ctrlPressed, _shiftPressed, true))
			pet.getAI().Cast(skill, aimingTarget, _ctrlPressed, _shiftPressed);
		else
			activeChar.sendActionFailed();
	}

	private class SocialTask implements Runnable
	{
		private final L2Player _player;

		public SocialTask(final L2Player player)
		{
			_player = player;
		}

		@Override
		public void run()
		{
			_player.unblock();
		}
	}

	private void useSocial(final int action, final L2Player activeChar)
	{
		if(activeChar.isOutOfControl() || activeChar.getTransformationId() != 0 || activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || activeChar.isInTransaction() || activeChar.isActionsDisabled() || activeChar.isSitting())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
			return;
		}

		activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), action));

		if(Config.ALT_SOCIAL_ACTION_REUSE)
		{
			L2GameThreadPools.getInstance().scheduleAi(new SocialTask(activeChar), 2600, true);
			activeChar.block();
		}
	}

	@Override
	public String getType()
	{
		return _C__56_REQUESTACTIONUSE;
	}
}
