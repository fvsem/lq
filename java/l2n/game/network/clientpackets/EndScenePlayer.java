package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 17:58:34
 */
public class EndScenePlayer extends L2GameClientPacket
{
	private int _moviveId;

	@Override
	protected void readImpl()
	{
		_moviveId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(cha.getMovieId() != _moviveId)
			return;

		cha.setMovieId(0);
	}
}
