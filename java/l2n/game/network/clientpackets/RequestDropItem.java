package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.GmListTable;
import l2n.game.templates.L2EtcItem.EtcItemType;
import l2n.util.Location;
import l2n.util.Log;

public class RequestDropItem extends L2GameClientPacket
{
	private static final String _C__12_REQUESTDROPITEM = "[C] 12 RequestDropItem";

	private int _objectId;
	private long _count;
	private Location _loc;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		_count = readQ();
		_loc = new Location(readD(), readD(), readD());
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || activeChar.isDead())
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(activeChar.isOutOfControl())
		{
			sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		if(_count < 1 || _loc.isNull())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!Config.ALLOW_DISCARDITEM)
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestDropItem.Disallowed", activeChar));
			return;
		}

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		if(activeChar.isInTransaction())
		{
			sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
			return;
		}

		if(activeChar.isActionsDisabled() || activeChar.isSitting() || activeChar.isDropDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2ItemInstance oldItem = activeChar.getInventory().getItemByObjectId(_objectId);
		if(oldItem == null)
		{
			// Util.handleIllegalPlayerAction(activeChar, "RequestDropItem", "tried to drop an item that is not in the inventory", IllegalPlayerAction.CRITICAL);
			_log.warning(activeChar.getName() + ": tried to drop an item that is not in the inventory? objId: " + _objectId);
			return;
		}

		if(oldItem.getItemType() == EtcItemType.QUEST || !oldItem.canBeDropped(activeChar))
		{
			activeChar.sendPacket(Msg.THAT_ITEM_CANNOT_BE_DISCARDED);
			return;
		}

		if(!oldItem.isStackable() && _count > 1)
		{
			activeChar.sendActionFailed();
			return;
		}

		final long oldCount = oldItem.getCount();
		if(Config.DEBUG)
			_log.fine("requested drop item " + _objectId + "(" + oldCount + ") at " + _loc.toString());

		if(oldCount < _count)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!activeChar.isInRangeSq(_loc, 22500) || Math.abs(_loc.z - activeChar.getZ()) > 50)
		{
			activeChar.sendPacket(Msg.TOO_FAR_TO_DISCARD);
			return;
		}

		if(activeChar.getActiveEnchantItem() != null && oldItem.getObjectId() == activeChar.getActiveEnchantItem().getObjectId())
		{
			activeChar.sendActionFailed();
			return;
		}
		if(activeChar.getActiveEnchantSupportItem() != null && oldItem.getObjectId() == activeChar.getActiveEnchantSupportItem().getObjectId())
		{
			activeChar.sendActionFailed();
			return;
		}
		if(activeChar.getActiveEnchantAttrItem() != null && oldItem.getObjectId() == activeChar.getActiveEnchantAttrItem().getObjectId())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(oldItem.isEquipped())
		{
			activeChar.getInventory().unEquipItem(oldItem);
			activeChar.sendUserInfo(true);
		}

		oldItem.setWhFlag(true);
		final L2ItemInstance dropedItem = activeChar.getInventory().dropItem(_objectId, _count);
		oldItem.setWhFlag(false);

		if(dropedItem == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		dropedItem.dropToTheGround(activeChar, _loc);

		activeChar.disableDrop(1000);

		Log.LogItem(activeChar, Log.Drop, dropedItem);

		if(Config.DEBUG)
			if(dropedItem.isAdena() && dropedItem.getCount() >= 1000000)
			{
				final String msg = "Character (" + activeChar.getName() + ") has dropped (" + dropedItem.getCount() + ") " + dropedItem.getItem().getName() + " " + _loc.toString();
				_log.warning(msg);
				GmListTable.broadcastMessageToGMs(msg);
			}

		activeChar.updateStats();
	}

	@Override
	public String getType()
	{
		return _C__12_REQUESTDROPITEM;
	}
}
