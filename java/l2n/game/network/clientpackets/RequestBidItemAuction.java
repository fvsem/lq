package l2n.game.network.clientpackets;

import l2n.game.instancemanager.itemauction.ItemAuction;
import l2n.game.instancemanager.itemauction.ItemAuctionInstance;
import l2n.game.instancemanager.itemauction.ItemAuctionManager;
import l2n.game.model.actor.L2Player;
import l2n.game.templates.L2Item;

/**
 * @<a href="http://L2System/">L2s</a>
 * @L2System Project
 * @date 16.09.2011
 * @time 10:52:26
 */
public class RequestBidItemAuction extends L2GameClientPacket
{
	private int _instanceId;
	private long _bid;

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#readImpl()
	 */
	@Override
	protected void readImpl()
	{
		_instanceId = readD();
		_bid = readQ();
	}

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#runImpl()
	 */
	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_bid < 0 || _bid > L2Item.MAX_ITEM_COUNT)
			return;

		final ItemAuctionInstance instance = ItemAuctionManager.getInstance().getManagerInstance(_instanceId);
		if(instance != null)
		{
			final ItemAuction auction = instance.getCurrentAuction();
			if(auction != null)
				auction.registerBid(activeChar, _bid);
		}
	}

	@Override
	public final String getType()
	{
		return "[C] D0:39 RequestBidItemAuction";
	}
}
