package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExPutIntensiveResultForVariationMake;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Item.Grade;

public class RequestConfirmRefinerItem extends AbstractRefinePacket
{
	private final static String _C__D0_27_REQUESTCONFIRMREFINERITEM = "[C] D0:27 RequestConfirmRefinerItem";

	private int _targetItemObjId;
	private int _refinerItemObjId;

	@Override
	public void readImpl()
	{
		_targetItemObjId = readD();
		_refinerItemObjId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance targetItem = activeChar.getInventory().getItemByObjectId(_targetItemObjId);
		final L2ItemInstance refinerItem = activeChar.getInventory().getItemByObjectId(_refinerItemObjId);

		if(targetItem == null || refinerItem == null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS));
			return;
		}

		if(!isValid(activeChar))
		{
			activeChar.sendActionFailed();
			return;
		}
		if(!isValid(activeChar, targetItem, refinerItem))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THIS_IS_NOT_A_SUITABLE_ITEM));
			activeChar.sendActionFailed();
			return;
		}

		final Grade grade = targetItem.getItem().getCrystalType();
		final int refinerItemId = refinerItem.getItem().getItemId();

		final LifeStone ls = getLifeStone(refinerItemId);
		final int gemStoneId = getGemStoneId(grade);
		final int gemStoneCount = getGemStoneCount(grade, ls.getGrade());
		final SystemMessage sm = new SystemMessage(SystemMessage.REQUIRES_S1_S2);
		sm.addNumber(gemStoneCount);
		sm.addItemName(gemStoneId);

		activeChar.sendPacket(new ExPutIntensiveResultForVariationMake(_refinerItemObjId, refinerItemId, gemStoneId, gemStoneCount));
		activeChar.sendPacket(sm);
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return _C__D0_27_REQUESTCONFIRMREFINERITEM;
	}
}
