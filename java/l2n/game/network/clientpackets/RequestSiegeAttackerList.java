package l2n.game.network.clientpackets;

import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Residence;
import l2n.game.network.serverpackets.SiegeAttackerList;

public class RequestSiegeAttackerList extends L2GameClientPacket
{
	private static String _C__AB_RequestSiegeAttackerList = "[C] AB RequestSiegeAttackerList";
	// format: cd
	// private static Logger _log = Logger.getLogger(RequestJoinParty.class.getName());

	private int _unitId;

	@Override
	public void readImpl()
	{
		_unitId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		Residence unit = CastleManager.getInstance().getCastleByIndex(_unitId);
		if(unit == null)
			unit = FortressManager.getInstance().getFortressByIndex(_unitId);
		if(unit == null)
			unit = ClanHallManager.getInstance().getClanHall(_unitId);
		if(unit != null)
			sendPacket(new SiegeAttackerList(unit));
	}

	@Override
	public String getType()
	{
		return _C__AB_RequestSiegeAttackerList;
	}
}
