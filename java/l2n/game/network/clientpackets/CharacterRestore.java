package l2n.game.network.clientpackets;

import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.CharacterSelectionInfo;

public class CharacterRestore extends L2GameClientPacket
{
	private static String _C__7B_CHARACTERRESTORE = "[C] 7B CharacterRestore";

	// cd
	private int _charSlot;

	@Override
	public void readImpl()
	{
		_charSlot = readD();
	}

	@Override
	public void runImpl()
	{
		final L2GameClient client = getClient();
		try
		{
			client.markRestoredChar(_charSlot);
		}
		catch(final Exception e)
		{}

		final CharacterSelectionInfo cl = new CharacterSelectionInfo(client.getLoginName(), client.getSessionId().playOkID1);
		sendPacket(cl);
		client.setCharSelection(cl.getCharInfo());
	}

	@Override
	public String getType()
	{
		return _C__7B_CHARACTERRESTORE;
	}
}
