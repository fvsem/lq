package l2n.game.network.clientpackets;

import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.actor.L2Player;

/**
 * Format: (ch)
 * this is just a trigger : no data
 */
public class RequestExitPartyMatchingWaitingRoom extends L2GameClientPacket
{
	private static String _C__D0_25_REQUESTEXITPARTYMATCHINGWAITINGROOM = "[C] D0:25 RequestExitPartyMatchingWaitingRoom";

	@Override
	public void readImpl()
	{}

	/**
	 * @see l2n.game.clientpackets.ClientBasePacket#runImpl()
	 */
	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		PartyRoomManager.getInstance().removeFromWaitingList(activeChar);
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__D0_25_REQUESTEXITPARTYMATCHINGWAITINGROOM;
	}
}
