package l2n.game.network.clientpackets;

import l2n.game.instancemanager.FortressManager;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.network.serverpackets.ExShowFortressMapInfo;

/**
 * @author KenM
 */
public class RequestFortressMapInfo extends L2GameClientPacket
{
	private int _fortressId;

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#getType()
	 */
	@Override
	public String getType()
	{
		return "[C] D0:4B RequestFortressMapInfo";
	}

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#readImpl()
	 */
	@Override
	protected void readImpl()
	{
		_fortressId = readD();
	}

	/**
	 * @see l2n.game.network.clientpackets.L2GameClientPacket#runImpl()
	 */
	@Override
	protected void runImpl()
	{
		final Fortress fort = FortressManager.getInstance().getFortressByIndex(_fortressId);
		if(fort != null)
			sendPacket(new ExShowFortressMapInfo(fort));
	}
}
