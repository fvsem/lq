package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestPetUseItem extends L2GameClientPacket
{
	private static String _C__94_REQUESTPETUSEITEM = "[C] 94 RequestPetUseItem";

	private int _objectId;

	/**
	 * packet type id 0x94
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Summon summon = activeChar.getPet();
		if(summon == null || !summon.isPet())
			return;

		final L2PetInstance pet = (L2PetInstance) summon;

		final L2ItemInstance item = pet.getInventory().getItemByObjectId(_objectId);
		if(item == null || item.getCount() <= 0)
			return;

		if(activeChar.isAlikeDead() || pet.isDead())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId()));
			return;
		}

		if(pet.tryEquipItem(item, true))
			return;

		// manual pet feeding
		if(pet.tryFeedItem(item))
			return;

		final L2Skill[] skills = item.getItem().getAttachedSkills();
		if(skills != null && skills.length > 0)
			for(final L2Skill skill : skills)
			{
				// Отсеиваем все что пету не положено
				if(skill.getHitTime() > 1000)
				{
					activeChar.sendPacket(Msg.ITEM_NOT_AVAILABLE_FOR_PETS);
					return;
				}
				switch (skill.getSkillType())
				{
					case BUFF:
					case CANCEL:
					case HEAL:
					case HEAL_PERCENT:
					case HOT:
					case MANAHEAL:
					case MANAHEAL_PERCENT:
					case NEGATE_EFFECTS:
					case NEGATE_STATS:
						final L2Character aimingTarget = skill.getAimingTarget(pet, pet.getTarget());
						if(skill.checkCondition(pet, aimingTarget, false, false, true))
							pet.getAI().Cast(skill, aimingTarget, false, false);
						break;
					default:
						activeChar.sendPacket(Msg.ITEM_NOT_AVAILABLE_FOR_PETS);
						return;
				}
			}
		else
			activeChar.sendPacket(Msg.ITEM_NOT_AVAILABLE_FOR_PETS);
	}

	@Override
	public String getType()
	{
		return _C__94_REQUESTPETUSEITEM;
	}
}
