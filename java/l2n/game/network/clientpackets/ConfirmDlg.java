package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.instancemanager.L2EventManager;
import l2n.game.model.actor.L2Player;

/**
 * Format: cddd
 */
public class ConfirmDlg extends L2GameClientPacket
{
	private final static String _C__C6_DLGANSWER = "[C] C6 ConfirmDlg";

	public static final int SUMMON_CHARACTER_ANSWER = 1;
	public static final int REVIVE_ANSWER = 2;
	public static final int SCRIPT_ANSWER = 3;
	public static final int ENGAGE_ANSWER = 4;
	public static final int OFFLINE_ANSWER = 5;
	public static final int CORE_EVENT_ANSWER = 6;

	private int _messageId;
	private int _answer;
	private int _requestId;

	@Override
	public void readImpl()
	{
		_messageId = readD();
		_answer = readD();
		_requestId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(Config.DEBUG)
			_log.fine(getType() + ": Answer acepted. Message ID " + _messageId + ", asnwer " + _answer + ", request Id " + _requestId);

		switch (_requestId)
		{
			case SUMMON_CHARACTER_ANSWER:
				activeChar.summonCharacterAnswer(_answer);
				break;
			case REVIVE_ANSWER:
				activeChar.reviveAnswer(_answer);
				break;
			case SCRIPT_ANSWER:
				activeChar.scriptAnswer(_answer);
				break;
			case ENGAGE_ANSWER:
				if(Config.ALLOW_WEDDING && activeChar.isEngageRequest())
					activeChar.engageAnswer(_answer);
				break;
			case OFFLINE_ANSWER:
				if(Config.SERVICES_OFFLINE_TRADE_ALLOW)
					activeChar.offlineAnswer(_answer);
				break;
			case CORE_EVENT_ANSWER:
				L2EventManager.getInstance().participateAnswer(activeChar, _answer);
				break;
		}
	}

	@Override
	public String getType()
	{
		return _C__C6_DLGANSWER;
	}
}
