package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.SendTradeRequest;
import l2n.game.network.serverpackets.SystemMessage;

public class TradeRequest extends L2GameClientPacket
{
	private static String TRADEREQUEST__C__1A = "[C] 1A TradeRequest";
	// Format: cd
	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
			return;
        }
		if(!activeChar.getPlayerAccess().UseTrade)
		{
			activeChar.sendPacket(Msg.THIS_ACCOUNT_CANOT_TRADE_ITEMS);
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isDead())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Object target = activeChar.getVisibleObject(_objectId);

		if(target == null || !target.isPlayer() || target.getObjectId() == activeChar.getObjectId())
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		if(!activeChar.knowsObject(target))
		{
			activeChar.sendPacket(Msg.CANNOT_SEE_TARGET);
			return;
		}

		final L2Player pcTarget = (L2Player) target;

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || pcTarget.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		if(!pcTarget.getPlayerAccess().UseTrade)
		{
			activeChar.sendPacket(Msg.THIS_ACCOUNT_CANOT_TRADE_ITEMS);
			activeChar.sendActionFailed();
			return;
		}

		if(pcTarget.getTeam() != 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(pcTarget.isInOlympiadMode() || activeChar.isInOlympiadMode())
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		if(pcTarget.getTradeRefusal() || pcTarget.isInBlockList(activeChar) || pcTarget.isBlockAll())
		{
			activeChar.sendPacket(Msg.YOU_HAVE_BEEN_BLOCKED_FROM_THE_CONTACT_YOU_SELECTED);
			return;
		}

		if(activeChar.isInTransaction())
		{
			activeChar.sendPacket(Msg.ALREADY_TRADING);
			return;
		}

		if(pcTarget.isInTransaction())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(pcTarget.getName()));
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
			return;
		}

		new Transaction(TransactionType.TRADE_REQUEST, activeChar, pcTarget, 10000);
		pcTarget.sendPacket(new SendTradeRequest(activeChar.getObjectId()));
		activeChar.sendPacket(new SystemMessage(SystemMessage.REQUEST_S1_FOR_TRADE).addString(pcTarget.getName()));
	}

	@Override
	public String getType()
	{
		return TRADEREQUEST__C__1A;
	}
}
