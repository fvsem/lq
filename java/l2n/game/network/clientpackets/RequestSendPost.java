package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.database.utils.mysql;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.MailParcelController;
import l2n.game.model.items.MailParcelController.Letter;
import l2n.game.network.serverpackets.ExNoticePostArrived;
import l2n.game.network.serverpackets.ExReplyWritePost;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Util;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:48:53
 */
public class RequestSendPost extends L2GameClientPacket
{
	private int _messageType;
	private String _targetName;
	private String _topic;
	private String _body;

	private int[] _attachmenItems;
	private long[] _attachmenItemsQ;
	private long _price;

	@Override
	protected void readImpl()
	{
		_targetName = readS(35);
		_messageType = readD();
		_topic = readS(30);
		_body = readS(512);

		final int attachCount = readD();
		if(attachCount < 0 || attachCount > Config.MAX_ITEM_IN_PACKET || attachCount * 12 + 8 != _buf.remaining())
			return;

		_attachmenItems = new int[attachCount];
		_attachmenItemsQ = new long[_attachmenItems.length];
		for(int i = 0; i < _attachmenItems.length; i++)
		{
			_attachmenItems[i] = readD();
			_attachmenItemsQ[i] = readQ();
		}

		_price = readQ();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		SystemMessage msg = null;
		if((msg = MailParcelController.canSentPost(cha)) != null)
		{
			cha.sendPacket(msg);
			cha.sendActionFailed();
			return;
		}

		if(cha.getName().equalsIgnoreCase(_targetName)) // проверка на отправку почты самому себе
		{
			cha.sendPacket(Msg.YOU_CANNOT_SEND_A_MAIL_TO_YOURSELF);
			return;
		}

		if(_messageType == 1)
		{
			if(_price == 0)
			{
				cha.sendPacket(new SystemMessage(SystemMessage.WHEN_NOT_ENTERING_THE_AMOUNT_FOR_THE_PAYMENT_REQUEST_YOU_CANNOT_SEND_ANY_MAIL));
				return;
			}
			if(_attachmenItems == null || _attachmenItems.length == 0)
			{
				cha.sendPacket(new SystemMessage(SystemMessage.IT_S_A_PAYMENT_REQUEST_TRANSACTION_PLEASE_ATTACH_THE_ITEM));
				return;
			}
		}

		if(_price > 1) // если цена больше 0, то проверяем бан торговли.
		{
			final String tradeBan = cha.getVar("tradeBan");
			if(tradeBan != null && (tradeBan.equals("-1") || Long.parseLong(tradeBan) >= System.currentTimeMillis()))
			{
				cha.sendMessage("Your trade is banned! Expires: " + (tradeBan.equals("-1") ? "never" : Util.formatTime((Long.parseLong(tradeBan) - System.currentTimeMillis()) / 1000)) + ".");
				return;
			}
		}

		if(cha.isInBlockList(_targetName)) // если тот кому отпр. письмо находится в блок листе
		{
			cha.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_BLOCKED_C1).addString(_targetName));
			return;
		}

		final L2Player target = L2ObjectsStorage.getPlayer(_targetName);
		int targetId;
		if(target != null)
		{
			targetId = target.getObjectId();
			if(target.isInBlockList(cha)) // если тот кому отпр. письмо находится в блок листе (а вдруг имя сменил))))
			{
				cha.sendPacket(new SystemMessage(SystemMessage.S1_HAS_BLOCKED_YOU_YOU_CANNOT_SEND_MAIL_TO_S1_).addString(_targetName));
				return;
			}
		}
		else
		{
			targetId = Util.GetCharIDbyName(_targetName);
			if(targetId > 0 && mysql.simple_get_int("target_Id", "character_blocklist", "obj_Id=" + targetId + " AND target_Id=" + cha.getObjectId()) > 0)
			{
				cha.sendPacket(new SystemMessage(SystemMessage.S1_HAS_BLOCKED_YOU_YOU_CANNOT_SEND_MAIL_TO_S1_).addString(_targetName));
				return;
			}
		}

		if(targetId == 0) // если такого игрока вообще нет на сервере и никгда не было)
		{
			cha.sendPacket(Msg.WHEN_THE_RECIPIENT_DOESN_T_EXIST_OR_THE_CHARACTER_HAS_BEEN_DELETED_SENDING_MAIL_IS_NOT_POSSIBLE);
			return;
		}

		// Проверка на максимальное количество вложений
		if(_attachmenItems != null && _attachmenItems.length > MailParcelController.MAX_ATTACHMENTS)
		{
			cha.sendPacket(new SystemMessage(SystemMessage.ITEM_SELECTION_IS_POSSIBLE_UP_TO_8));
			return;
		}

		// Письма требующие оплаты автоматически аннулируются через 12 часов после отправки
		final int expiretime = (_messageType == 1 ? MailParcelController.LIFE_TIME_REQUIRES_PAYMENT_MAIL : MailParcelController.LIFE_TIME_MAIL) * 3600 + (int) (System.currentTimeMillis() / 1000);

		final long serviceCost = 100 + _attachmenItems.length * 1000; // TODO хардкод цена за почту
		L2ItemInstance itm;
		for(int i = 0; i < _attachmenItems.length; i++)
		{
			itm = cha.getInventory().getItemByObjectId(_attachmenItems[i]);
			if(itm == null || !itm.canBeTraded(cha) || itm.getCount() < _attachmenItemsQ[i])
			{
				cha.sendPacket(Msg.THE_ITEM_THAT_YOU_RE_TRYING_TO_SEND_CANNOT_BE_FORWARDED_BECAUSE_IT_ISN_T_PROPER);
				return;
			}
		}
		if(cha.getAdena() < serviceCost)
		{
			cha.sendPacket(Msg.YOU_CANNOT_FORWARD_BECAUSE_YOU_DON_T_HAVE_ENOUGH_ADENA);
			return;
		}

		cha.reduceAdena(serviceCost, true);

		final Letter letter = new Letter();
		letter.receiverId = targetId;
		letter.receiverName = _targetName;
		letter.senderId = cha.getObjectId();
		letter.senderName = cha.getName();
		letter.topic = _topic;
		letter.body = _body;
		letter.type = _messageType;
		letter.price = _price;
		letter.unread = 1;
		letter.validtime = expiretime;

		MailParcelController.getInstance().sendLetter(letter, _attachmenItems, _attachmenItemsQ, cha);

		cha.sendPacket(new ExReplyWritePost(1));
		if(target != null)
			target.sendPacket(new ExNoticePostArrived(1));
	}
}
