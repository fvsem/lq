package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.math.SafeMath;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.*;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestPreviewItem extends L2GameClientPacket
{
	private static String _C__C7_REQUESTPREVIEWITEM = "[C] C7 RequestPreviewItem";
	// format: cdddb
	protected static Logger _log = Logger.getLogger(RequestPreviewItem.class.getName());

	protected Future<RemoveWearItemsTask> _removeWearItemsTask;

	@SuppressWarnings("unused")
	private int _unknow;
	@SuppressWarnings("unused")
	private int _listId;
	private int _count;
	private int[] _items; // count*2
	protected L2Player _cha;

	private class RemoveWearItemsTask implements Runnable
	{
		@Override
		public void run()
		{
			try
			{
				final L2ItemInstance[] items = _cha.getInventory().getItems();
				for(final L2ItemInstance i : items)
					if(i.isWear())
					{
						if(i.isEquipped())
							_cha.getInventory().unEquipItemInSlot(i.getEquipSlot());
						_cha.getInventory().destroyItem(i.getObjectId(), 1, true);
					}
				_cha.broadcastUserInfo(true);
				_cha.sendPacket(new SystemMessage(SystemMessage.TRYING_ON_MODE_HAS_ENDED));
				sendPacket(new ItemList(_cha, false));
			}
			catch(final Throwable e)
			{
				_log.log(Level.SEVERE, "", e);
			}
		}
	}

	@Override
	public void readImpl()
	{
		_cha = getClient().getActiveChar();
		_unknow = readD();
		_listId = readD();
		_count = readD();
		if(_count * 4 > _buf.remaining() || _count > Short.MAX_VALUE || _count <= 0)
		{
			_count = 0;
			return;
		}
		_items = new int[_count];
		for(int i = 0; i < _count; i++)
			_items[i] = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = _cha;

		if(!Config.WEAR_TEST_ENABLED)
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestPreviewItem.Disabled", activeChar));
			activeChar.sendActionFailed();
			return;
		}

		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && activeChar.getKarma() > 0 && !activeChar.isGM())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2NpcInstance npc = activeChar.getLastNpc();

		final boolean isValidMerchant = npc instanceof L2ClanHallManagerInstance || npc instanceof L2MerchantInstance || npc instanceof L2MercManagerInstance || npc instanceof L2CastleChamberlainInstance;

		if(!activeChar.isGM() && (npc == null || !isValidMerchant || !activeChar.isInRange(npc.getLoc(), L2Character.INTERACTION_DISTANCE)))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_count < 1)
		{
			activeChar.sendActionFailed();
			return;
		}

		final GArray<L2ItemInstance> items = new GArray<L2ItemInstance>(_count);
		for(int i = 0; i < _count; i++)
		{
			final int itemId = _items[i];
			final int cnt = 1;
			final L2ItemInstance inst = ItemTable.getInstance().createItem(itemId, activeChar.getObjectId(), npc.getObjectId(), "RequestWearItem[137]");
			inst.setCount(cnt);
			items.add(inst);
		}

		// TODO check if valid buylist, stackable items ?

		long neededMoney = 0;
		long finalLoad = 0;
		int finalCount = activeChar.getInventory().getSize();
		int needsSpace = 2;
		int weight = 0;
		final long currentMoney = activeChar.getAdena();

		for(final L2ItemInstance item : items)
		{
			final int itemId = item.getItemId();
			final long cnt = item.getCount();
			int price;
			if(item.getItem().isStackable())
			{
				needsSpace = 1;
				if(activeChar.getInventory().getItemByItemId(itemId) != null)
					needsSpace = 0;
			}

			price = 10;
			weight = item.getItem().getWeight();
			try
			{
				neededMoney = SafeMath.safeAddLong(neededMoney, SafeMath.safeMulLong(cnt, price));
				finalLoad = SafeMath.safeAddLong(finalLoad, SafeMath.safeMulLong(cnt, weight));
			}
			catch(final ArithmeticException e)
			{
				for(final L2ItemInstance i : items)
					i.deleteMe();
				_log.warning("Warning!! Character " + activeChar.getName() + " of account " + activeChar.getAccountName() + " tried to purchase over " + Long.MAX_VALUE + " adena worth of goods: " + e.getMessage());
				activeChar.sendActionFailed();
				return;
			}

			if(needsSpace == 2)
				finalCount += cnt;
			else if(needsSpace == 1)
				finalCount += 1;
		}

		if(neededMoney > currentMoney || neededMoney < 0 || currentMoney <= 0)
		{
			sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);

			for(final L2ItemInstance i : items)
				i.deleteMe();

			activeChar.sendActionFailed();
			return;
		}

		if(!activeChar.getInventory().validateWeight(finalLoad))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);

			for(final L2ItemInstance i : items)
				i.deleteMe();

			activeChar.sendActionFailed();
			return;
		}

		if(!activeChar.getInventory().validateCapacity(finalCount))
		{
			sendPacket(Msg.YOUR_INVENTORY_IS_FULL);

			for(final L2ItemInstance i : items)
				i.deleteMe();

			activeChar.sendActionFailed();
			return;
		}

		activeChar.reduceAdena(neededMoney, true);

		for(final L2ItemInstance item : items)
		{
			item.setWear(true);
			activeChar.getInventory().addItem(item);
			activeChar.getInventory().equipItem(item, true);
		}
		activeChar.broadcastUserInfo(true);

		sendPacket(new ItemList(activeChar, false));

		if(_removeWearItemsTask == null)
			_removeWearItemsTask = L2GameThreadPools.getInstance().scheduleAi(new RemoveWearItemsTask(), 10000, true);
	}

	@Override
	public String getType()
	{
		return _C__C7_REQUESTPREVIEWITEM;
	}
}
