package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.L2World;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Location;

/**
 * Format: chdddddc
 * Пример пакета:
 * D0
 * 2F 00
 * E4 35 00 00 x
 * 62 D1 02 00 y
 * 22 F2 FF FF z
 * 90 05 00 00 skill id
 * 00 00 00 00 ctrlPressed
 * 00 shiftPressed
 */
public class RequestExMagicSkillUseGround extends L2GameClientPacket
{
	private final static String _C__D0_47_REQUESTSEXMAGICSKILLGROUND = "[C] D0:47 RequestExMagicSkillUseGround";

	private final Location _loc = new Location();
	private int _skillId;
	private boolean _ctrlPressed;
	private boolean _shiftPressed;

	/**
	 * packet type id 0xd0
	 */
	@Override
	public void readImpl()
	{
		_loc.x = readD();
		_loc.y = readD();
		_loc.z = readD();
		_skillId = readD();
		_ctrlPressed = readD() != 0;
		_shiftPressed = readC() != 0;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(_skillId, activeChar.getSkillLevel(_skillId));
		if(skill != null)
		{
			if(skill.getAddedSkills() == null)
			{
				activeChar.sendActionFailed();
				return;
			}

			// В режиме трансформации доступны только скилы трансформы
			if(activeChar.getTransformationId() != 0 && !activeChar.getAllSkills().contains(skill))
			{
				activeChar.sendActionFailed();
				return;
			}

			// Нельзя ставить купол в мирной зоне
			if(L2World.getTerritoryByZoneType(_loc, ZoneType.peace_zone) != null)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addSkillName(skill.getId(), 1));
				activeChar.sendActionFailed();
				return;
			}

			if(!activeChar.isInRange(_loc, skill.getCastRange()))
			{
				activeChar.sendPacket(Msg.YOUR_TARGET_IS_OUT_OF_RANGE);
				activeChar.sendActionFailed();
				return;
			}

			final L2Character target = skill.getAimingTarget(activeChar, activeChar.getTarget());
			if(skill.checkCondition(activeChar, target, _ctrlPressed, _shiftPressed, true))
			{
				activeChar.setGroundSkillLoc(_loc);
				activeChar.getAI().Cast(skill, target, _ctrlPressed, _shiftPressed);
			}
			else
				activeChar.sendActionFailed();
		}
		else
			activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__D0_47_REQUESTSEXMAGICSKILLGROUND;
	}
}
