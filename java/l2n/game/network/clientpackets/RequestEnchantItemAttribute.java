package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.Elementals;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.ExAttributeEnchantResult;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Item;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

public class RequestEnchantItemAttribute extends AbstractEnchantPacket
{
	private final static String _C__5F_REQUESTENCHANTITEM = "[C] 5F RequestEnchantItem";
	// Format: chd
	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl() || activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_objectId == 0xFFFFFFFF)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.ELEMENTAL_POWER_ENCHANCER_USAGE_HAS_BEEN_CANCELLED));
			activeChar.setActiveEnchantAttrItem(null);
			return;
		}

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_ADD_ELEMENTAL_POWER_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
			activeChar.setActiveEnchantAttrItem(null);
			return;
		}

		final PcInventory inventory = activeChar.getInventory();
		final L2ItemInstance itemToEnchant = inventory.getItemByObjectId(_objectId);
		L2ItemInstance stone = activeChar.getActiveEnchantAttrItem();
		activeChar.setActiveEnchantAttrItem(null);

		if(itemToEnchant == null || stone == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Item item = itemToEnchant.getItem();

		Log.add(activeChar.getName() + "|Trying to attribute enchant|" + itemToEnchant.getItemId() + "|" + itemToEnchant.getObjectId(), "enchants");

		if(!isEnchantable(itemToEnchant, true) || itemToEnchant.getItem().getCrystalType().cry < L2Item.CRYSTAL_S)
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}

		if(itemToEnchant.isWear() || itemToEnchant.isStackable() || (stone = inventory.getItemByObjectId(stone.getObjectId())) == null)
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}

		// can't enchant rods, shadow items, adventurers', PvP items, hero items, cloaks, bracelets, underwear (e.g. shirt), belt, necklace, earring, ring
		if(item.isUnderwear() || item.isCloak() || item.isBracelet() || item.isBelt() || item.isAccessory())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		if (!Config.ENCHANTELEMENT_CAN_PVP && item.isPvP() || activeChar.isAttackingNow() || activeChar.isCastingNow())
		{
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
			return;
		}
		switch (itemToEnchant.getLocation())
		{
			case INVENTORY:
			case PAPERDOLL:
			{
				// Запрет на заточку чужих вещей, баг может вылезти на серверных лагах
				if(itemToEnchant.getOwnerId() != activeChar.getObjectId())
				{
					activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, Msg.ActionFail);
					activeChar.sendActionFailed();
					return;
				}
				break;
			}
			default:
			{
				Util.handleIllegalPlayerAction(activeChar, "RequestEnchantItemAttribute", "tried to use enchant Exploit!", Config.DEFAULT_PUNISH);
				return;
			}
		}

		// ********************* Конец проверок ***************** //

		final int stoneId = stone.getItemId();
		final Elementals oldElement = itemToEnchant.getElementals();
		final int elementValue = oldElement == null ? 0 : oldElement.getValue();
		final int limit = getLimit(stoneId, itemToEnchant);
		int powerToAdd = getPowerToAdd(stoneId, elementValue, itemToEnchant);
		byte elementToAdd = getElementFromItemId(stoneId);

		// Armors have the opposite element
		if(item.isArmor())
			elementToAdd = Elementals.getOppositeElement(elementToAdd);

		int newPower = elementValue + powerToAdd;
		if(newPower > limit)
		{
			newPower = limit;
			powerToAdd = limit - elementValue;
		}

		if(oldElement != null && oldElement.getElement() != elementToAdd && oldElement.getElement() != Elementals.ATTRIBUTE_NONE)
		{
			activeChar.sendPacket(Msg.ANOTHER_ELEMENTAL_POWER_HAS_ALREADY_BEEN_ADDED_THIS_ELEMENTAL_POWER_CANNOT_BE_ADDED, Msg.ActionFail);
			return;
		}

		if(powerToAdd <= 0)
		{
			activeChar.sendPacket(Msg.ELEMENTAL_POWER_ENCHANCER_USAGE_HAS_BEEN_CANCELLED, Msg.ActionFail);
			return;
		}

		L2ItemInstance removedStone;
		synchronized (inventory)
		{
			removedStone = inventory.destroyItem(stone.getObjectId(), 1, true);
		}

		if(removedStone == null)
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestEnchantItemAttribute", "tried to attribute enchant with a stone he doesn't have", Config.DEFAULT_PUNISH);
			activeChar.sendPacket(ExAttributeEnchantResult.FAIL, Msg.ActionFail);
			return;
		}

		if(Rnd.chance(Config.ENCHANT_ATTRIBUTE_CHANCE))
		{
			if(itemToEnchant.getAttributeElementValue() == 0)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.S2_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO_S1);
				sm.addItemName(itemToEnchant.getItemId());
				sm.addString(Elementals.getElementName(elementToAdd));
				activeChar.sendPacket(sm);
			}
			else
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.S3_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO__S1S2);
				sm.addNumber(itemToEnchant.getAttributeElementValue());
				sm.addItemName(itemToEnchant.getItemId());
				sm.addString(Elementals.getElementName(elementToAdd));
				activeChar.sendPacket(sm);
			}

			boolean equipped = false;
			if(equipped = itemToEnchant.isEquipped())
			{
				activeChar.getInventory().setRefreshingListeners(true);
				activeChar.getInventory().unEquipItem(itemToEnchant);
			}

			itemToEnchant.setAttributeElement(elementToAdd, newPower, true);

			if(equipped)
			{
				activeChar.getInventory().equipItem(itemToEnchant, false);
				activeChar.getInventory().setRefreshingListeners(false);
			}

			activeChar.sendPacket(new InventoryUpdate(itemToEnchant, L2ItemInstance.MODIFIED));
			activeChar.sendPacket(new ExAttributeEnchantResult(powerToAdd));

			Log.add(activeChar.getName() + "|Successfully enchanted by attribute|" + itemToEnchant.getItemId() + "|to+" + itemToEnchant.getAttributeElementValue() + "|" + Config.ENCHANT_ATTRIBUTE_CHANCE, "enchants");
			Log.LogItem(activeChar, Log.EnchantItem, itemToEnchant);
		}
		else
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_ADD_ELEMENTAL_POWER));
			Log.add(activeChar.getName() + "|Failed to enchant attribute|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getAttributeElementValue() + "|" + Config.ENCHANT_ATTRIBUTE_CHANCE, "enchants");
		}

		activeChar.setActiveEnchantAttrItem(null);
		activeChar.updateStats();
	}

	private byte getElementFromItemId(final int itemId)
	{
		byte element = 0;
		for(final int id : Elementals.STONES)
		{
			if(id == itemId)
				return element;
			element++;
		}
		element = 0;
		for(final int id : Elementals.CRYSTALS)
		{
			if(id == itemId)
				return element;
			element++;
		}
		element = 0;
		for(final int id : Elementals.JEWELS)
		{
			if(id == itemId)
				return element;
			element++;
		}
		element = 0;
		for(final int id : Elementals.ENERGIES)
		{
			if(id == itemId)
				return element;
			element++;
		}
		element = 0;
		for(final int id : Elementals.ROUGHORES)
		{
			if(id == itemId)
				return element;
			element++;
		}
		return -2;
	}

	private int getLimit(final int itemId, final L2ItemInstance item)
	{
		for(final int id : Elementals.STONES)
			if(id == itemId)
			{
				if(item.isWeapon())
					return Elementals.WEAPON_VALUES[3];
				return Elementals.ARMOR_VALUES[3];
			}
		for(final int id : Elementals.ROUGHORES)
			if(id == itemId)
			{
				if(item.isWeapon())
					return Elementals.WEAPON_VALUES[3];
				return Elementals.ARMOR_VALUES[3];
			}
		for(final int id : Elementals.CRYSTALS)
			if(id == itemId)
			{
				if(item.isWeapon())
					return Elementals.WEAPON_VALUES[6];
				return Elementals.ARMOR_VALUES[6];
			}
		for(final int id : Elementals.JEWELS)
			if(id == itemId)
			{
				if(item.isWeapon())
					return Elementals.WEAPON_VALUES[9];
				return Elementals.ARMOR_VALUES[9];
			}
		for(final int id : Elementals.ENERGIES)
			if(id == itemId)
			{
				if(item.isWeapon())
					return Elementals.WEAPON_VALUES[12];
				return Elementals.ARMOR_VALUES[12];
			}
		return 0;
	}

	private int getPowerToAdd(final int stoneId, final int oldValue, final L2ItemInstance item)
	{
		boolean stone = false;
		for(final int id : Elementals.ALL_MODIFERS)
		{
			if(id != stoneId)
				continue;
			stone = true;
			break;
		}

		if(stone)
			if(item.isWeapon())
			{
				if(oldValue == 0)
					return Config.ENCHANT_ATTRIBUTE_FIRST_WEAPON_BONUS;
				else
					return Config.ENCHANT_ATTRIBUTE_WEAPON_BONUS;
			}
			else if(item.isArmor())
				return Config.ENCHANT_ATTRIBUTE_ARMOR_BONUS;

		return 0;
	}

	@Override
	public String getType()
	{
		return _C__5F_REQUESTENCHANTITEM;
	}
}
