package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ManagePledgePower;

public class RequestPledgePower extends L2GameClientPacket
{
	private final static String _C__CC_REQUESTPLEDGEPOWER = "[C] CC RequestPledgePower";
	// format: cdd
	private int _rank;
	private int _action;
	private int _privs;

	@Override
	public void readImpl()
	{
		_rank = readD();
		_action = readD();
		_privs = _action == 2 ? readD() : 0;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan == null)
			return;

		if(_action == 2)
		{
			if(_rank < L2Clan.RANK_FIRST || _rank > L2Clan.RANK_LAST)
				return;

			if((activeChar.getClanPrivileges() & L2Clan.CP_CL_MANAGE_RANKS) == L2Clan.CP_CL_MANAGE_RANKS)
			{
				if(_rank == L2Clan.RANK_LAST)
					_privs = (_privs & L2Clan.CP_CH_OPEN_DOOR) + (_privs & L2Clan.CP_CS_OPEN_DOOR);
				clan.setRankPrivs(_rank, _privs);
				clan.updatePrivsForRank(_rank);
			}
		}
		else
			activeChar.sendPacket(new ManagePledgePower(activeChar, _action, _rank));
	}

	@Override
	public String getType()
	{
		return _C__CC_REQUESTPLEDGEPOWER;
	}
}
