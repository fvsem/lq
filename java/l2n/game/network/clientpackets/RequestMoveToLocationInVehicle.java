package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.network.serverpackets.MoveToLocationInVehicle;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Location;

public class RequestMoveToLocationInVehicle extends L2GameClientPacket
{
	private final Location _pos = new Location(0, 0, 0);
	private final Location _originPos = new Location(0, 0, 0);
	private int _boatId;
	private static String _C__75_MOVETOLOCATIONINVEHICLE = "[C] 75 MoveToLocationInVehicle";

	/**
	 * @param buf
	 * @param client
	 *            format: cddddddd
	 */
	@Override
	public void readImpl()
	{
		_boatId = readD(); // objectId of boat
		_pos.x = readD();
		_pos.y = readD();
		_pos.z = readD();
		_originPos.x = readD();
		_originPos.y = readD();
		_originPos.z = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getPet() != null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.BECAUSE_PET_OR_SERVITOR_MAY_BE_DROWNED_WHILE_THE_BOAT_MOVES_PLEASE_RELEASE_THE_SUMMON_BEFORE_DEPARTURE));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_BOARD_A_SHIP_WHILE_YOU_ARE_POLYMORPHED));
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isMovementDisabled() || activeChar.isSitting())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Ship boat = (L2Ship) L2VehicleManager.getInstance().getBoat(_boatId);
		if(boat == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.setInVehiclePosition(_pos);
		activeChar.broadcastPacket(new MoveToLocationInVehicle(activeChar, boat, _originPos, _pos));
	}

	@Override
	public String getType()
	{
		return _C__75_MOVETOLOCATIONINVEHICLE;
	}
}
