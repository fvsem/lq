package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExPutItemResultForVariationCancel;

public class RequestConfirmCancelItem extends L2GameClientPacket
{
	private static final String _C__D0_45_REQUESTCONFIRMCANCELITEM = "[C] D0:45 RequestConfirmCancelItem";
	// format: (ch)d
	private int _itemId;

	@Override
	public void readImpl()
	{
		_itemId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_itemId);
		if(item == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!item.isAugmented())
		{
			activeChar.sendPacket(Msg.AUGMENTATION_REMOVAL_CAN_ONLY_BE_DONE_ON_AN_AUGMENTED_ITEM);
			return;
		}

		int price = 0;
		switch (item.getItem().getCrystalType())
		{
			case C:
			{
				if(item.getItem().getCrystalCount() < 1720)
					price = 95000;
				else if(item.getItem().getCrystalCount() < 2452)
					price = 150000;
				else
					price = 210000;
				break;
			}
			case B:
			{
				if(item.getItem().getCrystalCount() < 1746)
					price = 240000;
				else
					price = 270000;
				break;
			}
			case A:
			{
				if(item.getItem().getCrystalCount() < 2160)
					price = 330000;
				else if(item.getItem().getCrystalCount() < 2824)
					price = 390000;
				else
					price = 420000;
				break;
			}
			case S:
			{
				price = 480000;
				break;
			}
			case S80:
			{
				price = 920000;
				break;
			}
			case S84:
			{
				price = 1400000;
				break;
			}
			default:
				return;
		}
		activeChar.sendPacket(new ExPutItemResultForVariationCancel(_itemId, price));
	}

	@Override
	public String getType()
	{
		return _C__D0_45_REQUESTCONFIRMCANCELITEM;
	}
}
