package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.network.ReceivablePacket;
import l2n.game.model.actor.L2Player;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.L2GameServerPacket;

import java.nio.BufferUnderflowException;
import java.util.Collection;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Packets received by the game server from clients
 */
public abstract class L2GameClientPacket extends ReceivablePacket<L2GameClient, L2GameClientPacket, L2GameServerPacket>
{
	protected final static Logger _log = Logger.getLogger(L2GameClientPacket.class.getName());

	@Override
	protected boolean read()
	{
		try
		{
			readImpl();
			return true;
		}
		catch(final BufferUnderflowException bue)
		{
			// no log pls
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Client: " + getClient().toString() + " - Failed reading: " + getType() + " - Version: " + Config.SERVER_REVISION);
			handleIncompletePacket();
		}
		return false;
	}

	protected abstract void readImpl() throws Exception;

	@Override
	public void run()
	{
		final L2GameClient client = getClient();
		try
		{
			runImpl();
		}
		catch(final Exception e)
		{
			_log.log(Level.SEVERE, "Client: " + client.toString() + " - Failed running: " + getType() + " - Version: " + Config.SERVER_REVISION, e);
			handleIncompletePacket();
		}
		client.can_runImpl = true;
	}

	/**
	 * Contains everything that this packet should do. Runs asynchronously in worker threads. <BR>
	 * <BR>
	 * For <i>every</i> client, received packets are processed sequentially in the received order
	 * (using an unbounded FIFO queue). It is guaranteed that for any given client at any given time
	 * at most 1 packet will be in the processing state. <BR>
	 * <BR>
	 * For <i>all</i> clients, received packets are processed completely asynchronously without any
	 * guarantees. Thus synchronization is necessary for all actions that may directly or indirectly
	 * affect interactions between clients.
	 * 
	 * @throws InvalidPacketException
	 *             if this packet turns out to be invalid<br>
	 *             (either by time synchronization issues, either by purposeful exploitation,
	 *             etc...)
	 * @throws RuntimeException
	 *             if a generic failure occurs while processing the packet
	 */
	protected abstract void runImpl() throws Exception;

	protected void sendPacket(final L2GameServerPacket... packets)
	{
		getClient().sendPacket(packets);
	}

	protected void sendPackets(final Collection<L2GameServerPacket> packets)
	{
		getClient().sendPackets(packets);
	}

	protected void sendPackets(final Deque<L2GameServerPacket> packets)
	{
		getClient().sendPackets(packets);
	}

	public boolean checkReadArray(final int expected_elements, final int element_size, final boolean _debug)
	{
		final int expected_size = expected_elements * element_size;
		final boolean result = expected_size < 0 ? false : _buf.remaining() >= expected_size;
		if(!result && _debug)
			_log.severe("Buffer Underflow Risk in [" + getType() + "], Client: " + getClient().toString() + " from IP: " + getClient().getIpAddr() + " - Buffer Size: " + _buf.remaining() + " / Expected Size: " + expected_size);
		return result;
	}

	public boolean checkReadArray(final int expected_elements, final int element_size)
	{
		return checkReadArray(expected_elements, element_size, true);
	}

	private final void handleIncompletePacket()
	{
		final L2GameClient client = getClient();
		final L2Player activeChar = client.getActiveChar();

		if(activeChar == null)
			_log.warning("Packet not completed. Maybe cheater. IP:" + client.getIpAddr() + ", account:" + client.getLoginName());
		else
			_log.warning("Packet not completed. Maybe cheater. IP:" + client.getIpAddr() + ", account:" + client.getLoginName() + ", character:" + activeChar.getName());

		client.onClientPacketFail();
	}

	/**
	 * @return A String with this packet name for debuging purposes
	 */
	public String getType()
	{
		return "[C] " + getClass().getSimpleName();
	}
}
