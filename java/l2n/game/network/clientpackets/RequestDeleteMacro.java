package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestDeleteMacro extends L2GameClientPacket
{
	private int _id;

	private static String _C__CE_REQUESTDELETEMACRO = "[C] CE RequestDeleteMacro";

	/**
	 * packet type id 0xce
	 * sample
	 * ce
	 * d // macro id
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_id = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		activeChar.deleteMacro(_id);
	}

	@Override
	public String getType()
	{
		return _C__CE_REQUESTDELETEMACRO;
	}
}
