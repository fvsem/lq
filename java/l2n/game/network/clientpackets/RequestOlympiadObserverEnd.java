package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.actor.L2Player;

/**
 * format ch
 * c: (id) 0xD0
 * h: (subid) 0x29
 */
public class RequestOlympiadObserverEnd extends L2GameClientPacket
{
	private static final String _C__D0_12_REQUESTOLYMPIADOBSERVEREND = "[C] D0:12 RequestOlympiadObserverEnd";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(Config.ENABLE_OLYMPIAD && activeChar.inObserverMode())
			activeChar.leaveOlympiadObserverMode();
	}

	@Override
	public String getType()
	{
		return _C__D0_12_REQUESTOLYMPIADOBSERVEREND;
	}
}
