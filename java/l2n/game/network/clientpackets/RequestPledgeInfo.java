package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeInfo;
import l2n.game.tables.ClanTable;

/**
 * This class ...
 * 
 * @version $Revision: 1.5.4.3 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestPledgeInfo extends L2GameClientPacket
{
	private static String _C__65_REQUESTPLEDGEINFO = "[C] 65 RequestPledgeInfo";

	private int _clanId;

	/**
	 * packet type id 0x65
	 * format: cd
	 * 
	 * @param rawPacket
	 */
	@Override
	public void readImpl()
	{
		_clanId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		// задействовано как затычка
		if(_clanId < 10000000)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Clan clan = ClanTable.getInstance().getClan(_clanId);
		if(clan == null)
		{
			activeChar.sendActionFailed();
			return; // we have no clan data ?!? should not happen
		}

		activeChar.sendPacket(new PledgeInfo(clan));

	}

	@Override
	public String getType()
	{
		return _C__65_REQUESTPLEDGEINFO;
	}
}
