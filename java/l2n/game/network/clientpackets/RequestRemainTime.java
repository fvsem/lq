package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * @author L2System Project
 * @date 01.03.2010
 * @time 1:51:37
 */
public class RequestRemainTime extends L2GameClientPacket
{
	private static final String _C__B2_REQUESTREMAINTIME = "[C] B2 RequestRemainTime";

	@Override
	protected void readImpl()
	{
		// Trigger packet
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(Config.SERVER_LIST_CLOCK)
			// we do not support limited time/week servers
			activeChar.sendPacket(new SystemMessage(SystemMessage.THIS_WEEKS_USAGE_TIME_HAS_FINISHED));
		else
			// verified
			activeChar.sendPacket(new SystemMessage(SystemMessage.THIS_COMMAND_CAN_ONLY_BE_USED_IN_THE_RELAX_SERVER));
	}

	@Override
	public String getType()
	{
		return _C__B2_REQUESTREMAINTIME;
	}
}
