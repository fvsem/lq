package l2n.game.network.clientpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.ExCursedWeaponList;

/**
 * Format: (ch)
 */
public class RequestCursedWeaponList extends L2GameClientPacket
{
	private static String _C__D0_2A_REQUESTCURSEDWEAPONLIST = "[C] D0:2A RequestCursedWeaponList";

	@Override
	public void readImpl()
	{}

	/**
	 * @see l2n.game.clientpackets.ClientBasePacket#runImpl()
	 */
	@Override
	public void runImpl()
	{
		final L2Character activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		// send a ExCursedWeaponList :p
		final GArray<Integer> list = new GArray<Integer>();
		for(final int id : CursedWeaponsManager.getInstance().getCursedWeaponsIds())
			list.add(id);

		activeChar.sendPacket(new ExCursedWeaponList(list));
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__D0_2A_REQUESTCURSEDWEAPONLIST;
	}
}
