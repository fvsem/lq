package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExUISetting;

public class RequestKeyMapping extends L2GameClientPacket
{
	private static String _C__D0_21_REQUESTKEYMAPPING = "[C] D0:21 RequestKeyMapping";

	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player != null)
			player.sendPacket(new ExUISetting(player.getKeyBindings()));
	}

	/**
	 * @return A String with this packet name for debuging purposes
	 */
	@Override
	public String getType()
	{
		return _C__D0_21_REQUESTKEYMAPPING;
	}
}
