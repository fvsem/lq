package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.network.serverpackets.GetOnVehicle;
import l2n.util.Location;

public class RequestGetOnVehicle extends L2GameClientPacket
{
	private static String _C__53_GETONVEHICLE = "[C] 53 GetOnVehicle";

	private int _id, _x, _y, _z;

	@Override
	public void readImpl()
	{
		_id = readD();
		_x = readD();
		_y = readD();
		_z = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Ship boat = (L2Ship) L2VehicleManager.getInstance().getBoat(_id);
		if(boat == null)
			return;

		activeChar.stopMove();
		activeChar.setVehicle(boat);
		activeChar.setInVehiclePosition(new Location(_x, _y, _z));
		activeChar.setLoc(boat.getLoc());

		final GetOnVehicle Gon = new GetOnVehicle(activeChar, boat, _x, _y, _z);
		activeChar.broadcastPacket(Gon);
	}

	@Override
	public String getType()
	{
		return _C__53_GETONVEHICLE;
	}
}
