package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.StopRotation;

public class FinishRotating extends L2GameClientPacket
{
	private static String _C__5C_FINISHROTATING = "[C] 5c FinishRotating";

	private int _degree;
	@SuppressWarnings("unused")
	private int _unknown;

	/**
	 * packet type id 0x5c
	 * sample
	 * 5c
	 * d // unknown
	 * d // unknown
	 * format: cdd
	 */
	@Override
	public void readImpl()
	{
		_degree = readD();
		_unknown = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		activeChar.broadcastPacket(new StopRotation(activeChar, _degree, 0));
	}

	@Override
	public String getType()
	{
		return _C__5C_FINISHROTATING;
	}
}
