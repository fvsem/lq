package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestRecipeShopManageQuit extends L2GameClientPacket
{
	private static String _C__BC_RequestRecipeShopManageQuit = "[C] BC RequestRecipeShopManageQuit";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getDuel() != null)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
		activeChar.standUp();
		activeChar.broadcastCharInfo();
	}

	@Override
	public String getType()
	{
		return _C__BC_RequestRecipeShopManageQuit;
	}
}
