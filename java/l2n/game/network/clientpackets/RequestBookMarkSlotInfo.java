package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExGetBookMarkInfo;

/**
 * @author L2System
 * @date 15.12.2009
 * @time 23:03:14
 */
public class RequestBookMarkSlotInfo extends L2GameClientPacket
{
	private static final String _C__51_REQUESTBOOKMARKSLOTINFO = "[C] 51 RequestBookMarkSlotInfo";

	@Override
	protected void readImpl()
	{
		// There is nothing to read.
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		player.sendPacket(new ExGetBookMarkInfo(player));
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTBOOKMARKSLOTINFO;
	}
}
