package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.math.SafeMath;
import l2n.game.TradeController;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.*;
import l2n.game.network.serverpackets.ExBuySellList;
import l2n.util.Log;
import l2n.util.Util;

import java.util.concurrent.ConcurrentLinkedQueue;

public class RequestSellItem extends L2GameClientPacket
{
	private final static String _C__37_REQUESTSELLITEM = "[C] 37 RequestSellItem";

	private int _listId;
	private int _count;
	private long[] _items; // count*3

	/**
	 * packet type id 0x37 sample 1e 00 00 00 00 // list id 02 00 00 00 // number of items 71 72 00 10 // object id ea 05 00 00 // item id 01 00 00 00 // item count 76 4b 00 10 // object id 2e 0a 00 00 // item id 01 00 00 00 // item count format:
	 * cddb, b - array if (ddd)
	 */
	@Override
	public void readImpl()
	{
		_listId = readD();
		_count = readD();
		if(_count < 1 || _count > Config.MAX_ITEM_IN_PACKET || _count * 16 != _buf.remaining())
		{
			_items = null;
			return;
		}
		_items = new long[_count * 3];
		for(int i = 0; i < _count; i++)
		{
			_items[i * 3 + 0] = readD();
			_items[i * 3 + 1] = readD();
			_items[i * 3 + 2] = readQ();
			if(_items[i * 3 + 0] < 1 || _items[i * 3 + 1] < 1 || _items[i * 3 + 2] < 1)
			{
				_items = null;
				break;
			}
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(_items == null || _count < 1)
			return;

		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && activeChar.getKarma() > 0 && !activeChar.isGM())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2NpcInstance npc = activeChar.getLastNpc();

		final boolean isValidMerchant = npc instanceof L2ClanHallManagerInstance || npc instanceof L2MerchantInstance || npc instanceof L2MercManagerInstance || npc instanceof L2CastleChamberlainInstance || npc instanceof L2PetManagerInstance;

	//	if(!activeChar.isGM() && (npc == null || !isValidMerchant || !activeChar.isInRange(npc.getLoc(), L2Character.INTERACTION_DISTANCE)))
		//{
			//activeChar.sendActionFailed();
		//	return;
	//	}

		Transaction trans = activeChar.getTransaction();
		if(trans != null && trans.isInProgress() && trans.isTrade())
		{
			activeChar.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED, Msg.ActionFail);
			trans.cancel();
			return;
		}

		L2ItemInstance item;
		for(int i = 0; i < _count; i++)
		{
			final int objectId = (int) _items[i * 3 + 0];
			final int itemId = (int) _items[i * 3 + 1];
			final long cnt = _items[i * 3 + 2];

			if(cnt < 0)
			{
				Util.handleIllegalPlayerAction(activeChar, "Integer overflow", "RequestSellItem[100]", 0);
				continue;
			}
			else if(cnt == 0)
				continue;

			item = activeChar.getInventory().getItemByObjectId(objectId);
			if(item == null || item.isWear() || !item.canBeTraded(activeChar) || !item.getItem().isSellable() || item.isEquipped())
			{
				activeChar.sendPacket(Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
				return;
			}

			if(item.getItemId() != itemId)
			{
				Util.handleIllegalPlayerAction(activeChar, "Fake packet", "RequestSellItem[115]", 0);
				continue;
			}

			if(item.getCount() < cnt)
			{
				Util.handleIllegalPlayerAction(activeChar, "Incorrect item count", "RequestSellItem[121]", 0);
				continue;
			}

			final long price = item.getReferencePrice() * cnt / 2;

			activeChar.addAdena(price);
			Log.LogItem(activeChar, Log.SellItem, item);

			// If player sells the enchant scroll he is using, deactivate it
			if(activeChar.getActiveEnchantItem() != null && item.getObjectId() == activeChar.getActiveEnchantItem().getObjectId())
				activeChar.setActiveEnchantItem(null);

			final L2ItemInstance refund = activeChar.getInventory().dropItem(item, cnt, true);

			refund.setLocation(L2ItemInstance.ItemLocation.VOID);
			final ConcurrentLinkedQueue<L2ItemInstance> refundlist = activeChar.getInventory().getRefundItemsList(true);
			if(refund.isStackable())
			{
				boolean found = false;
				for(final L2ItemInstance ahri : refundlist)
					if(ahri.getItemId() == refund.getItemId())
					{
						ahri.setCount(SafeMath.safeAddLongOrMax(ahri.getCount(), refund.getCount()));
						found = true;
						break;
					}
				if(!found)
					refundlist.add(refund);
			}
			else
				refundlist.add(refund);

			if(refundlist.size() > 12)
				refundlist.poll();
		}

		double taxRate = 0.0D;
		Castle castle = null;
		if(npc != null)
		{
			castle = npc.getCastle();
			if(castle != null)
				taxRate = castle.getTaxRate();
		}

		activeChar.sendPacket(new ExBuySellList(TradeController.getInstance().getBuyList(_listId), activeChar, taxRate).done());
		activeChar.sendChanges();
	}

	@Override
	public String getType()
	{
		return _C__37_REQUESTSELLITEM;
	}
}
