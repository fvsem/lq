package l2n.game.network.clientpackets;

import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;

public class RequestExSetTutorial extends L2GameClientPacket
{
	private static String _C__88_REQUESTTUTORIALCLIENTEVT = "[C] 88 RequestTutorialClientEvent";
	// format: cd
	int event = 0;

	/**
	 * Пакет от клиента, если вы в туториале подергали мышкой как надо - клиент пришлет его со значением 1 ну или нужным ивентом
	 */
	@Override
	public void readImpl()
	{
		event = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final Quest tutorial = QuestManager.getQuest(255);
		if(tutorial != null)
			player.processQuestEvent(tutorial.getName(), "CE" + event, null);
	}

	@Override
	public String getType()
	{
		return _C__88_REQUESTTUTORIALCLIENTEVT;
	}
}
