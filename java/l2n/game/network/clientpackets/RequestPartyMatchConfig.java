package l2n.game.network.clientpackets;

import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PartyMatchDetail;

/**
 * This class ...
 * 
 * @version $Revision: 1.1.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestPartyMatchConfig extends L2GameClientPacket
{
	private static String _C__6F_REQUESTPARTYMATCHCONFIG = "[C] 6F RequestPartyMatchConfig";

	private int _page;
	private int _region;
	private int _allLevels;

	/**
	 * packet type id 0x6f sample 6f 01 00 00 00 00 00 00 00 00 00 00 00 00 00 format: cdddS
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_page = readD();
		_region = readD();
		_allLevels = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		activeChar.setPartyMatchingLevels(_allLevels);
		activeChar.setPartyMatchingRegion(_region);

		PartyRoomManager.getInstance().addToWaitingList(activeChar);

		activeChar.sendPacket(new PartyMatchDetail(_region, _allLevels, _page, activeChar));
	}

	@Override
	public String getType()
	{
		return _C__6F_REQUESTPARTYMATCHCONFIG;
	}
}
