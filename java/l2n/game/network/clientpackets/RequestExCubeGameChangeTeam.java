package l2n.game.network.clientpackets;

import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.model.actor.L2Player;

/**
 * @author L2System
 * @date 10.12.2009
 * @time 1:39:51
 */
public class RequestExCubeGameChangeTeam extends L2GameClientPacket
{
	private static final String _C__D0_5A_00_REQUESTEXCUBEGAMECHANGETEAM = "[C] D0:5A:00 RequestExCubeGameChangeTeam";

	private int _arena;
	private int _team;

	@Override
	protected void readImpl()
	{
		// client sends -1,0,1,2 for arena parameter
		_arena = readD() + 1;
		_team = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
		{
			// do not remove players after start
			if(HandysBlockCheckerManager.getInstance().arenaIsBeingUsed(_arena))
				return;
			switch (_team)
			{
				case 0:
				case 1:
					HandysBlockCheckerManager.getInstance().changePlayerToTeam(activeChar, _arena, _team);
					break;
				case -1:
				{
					final int team = HandysBlockCheckerManager.getInstance().getHolder(_arena).getPlayerTeam(activeChar);
					// client sends two times this packet if click on exit
					// client did not send this packet on restart
					if(team > -1)
						HandysBlockCheckerManager.getInstance().removePlayer(activeChar, _arena, team);
					break;
				}

				default:
					_log.warning("Wrong Cube Game Team ID: " + _team);
					break;
			}
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_5A_00_REQUESTEXCUBEGAMECHANGETEAM;
	}
}
