package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.L2FriendSay;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.FriendsTable;
import l2n.util.Log;

/**
 * Recieve Private (Friend) Message - 0xCC
 * Format: c SS
 * S: Message
 * S: Receiving Player
 */
public class RequestSendL2FriendSay extends L2GameClientPacket
{
	private static final String _C__6B_REQUESTSENDL2FRIENDSAY = "[C] 6B RequestSendL2FriendSay";

	private String _message;
	private String _reciever;

	@Override
	public void readImpl()
	{
		_message = readS(2048);
		_reciever = readS(32);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
        if(!activeChar.getPassCheck())
		{
            return;
        }
		if(activeChar.getNoChannel() != 0)
		{
			if(activeChar.getNoChannelRemained() > 0 || activeChar.getNoChannel() < 0)
			{
				activeChar.sendPacket(Msg.CHATTING_IS_CURRENTLY_PROHIBITED_IF_YOU_TRY_TO_CHAT_BEFORE_THE_PROHIBITION_IS_REMOVED_THE_PROHIBITION_TIME_WILL_BECOME_EVEN_LONGER);
				return;
			}
			activeChar.updateNoChannel(0);
		}

		final L2Player targetPlayer = L2ObjectsStorage.getPlayer(_reciever);
		if(targetPlayer == null)
		{
			activeChar.sendPacket(Msg.TARGET_IS_NOT_FOUND_IN_THE_GAME);
			return;
		}

		if(!FriendsTable.getInstance().checkIsFriends(activeChar.getObjectId(), targetPlayer.getObjectId()))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_ON_YOUR_FRIEND_LIST).addString(_reciever));
			return;
		}

		if(targetPlayer.isBlockAll())
		{
			activeChar.sendPacket(Msg.THE_PERSON_IS_IN_A_MESSAGE_REFUSAL_MODE);
			return;
		}

		if(Config.LOG_CHAT)
			Log.add("FRIENDTELL " + "[" + activeChar.getName() + " to " + _reciever + "] " + _message, "chat");

		targetPlayer.sendPacket(new L2FriendSay(activeChar.getName(), _reciever, _message));
	}

	@Override
	public String getType()
	{
		return _C__6B_REQUESTSENDL2FRIENDSAY;
	}
}
