package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeShowMemberListAll;

/**
 * This class ...
 * 
 * @version $Revision: 1.5.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestPledgeMemberList extends L2GameClientPacket
{
	private static String _C__4D_REQUESTPLEDGEMEMBERLIST = "[C] 4D RequestPledgeMemberList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		final L2Clan clan = activeChar.getClan();

		if(clan != null)
			activeChar.sendPacket(new PledgeShowMemberListAll(clan, activeChar));
	}

	@Override
	public String getType()
	{
		return _C__4D_REQUESTPLEDGEMEMBERLIST;
	}
}
