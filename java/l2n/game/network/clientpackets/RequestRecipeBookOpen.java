package l2n.game.network.clientpackets;

import l2n.game.RecipeController;
import l2n.game.model.actor.L2Player;

public class RequestRecipeBookOpen extends L2GameClientPacket
{
	private static String _C__B5_REQUESTRECIPEBOOKOPEN = "[C] B5 RequestRecipeBookOpen";
	private boolean isDwarvenCraft = true;

	/**
	 * packet type id 0xB5
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		if(_buf.hasRemaining())
			isDwarvenCraft = readD() == 0;
		else
			return;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		RecipeController.getInstance().requestBookOpen(activeChar, isDwarvenCraft);
	}

	@Override
	public String getType()
	{
		return _C__B5_REQUESTRECIPEBOOKOPEN;
	}
}
