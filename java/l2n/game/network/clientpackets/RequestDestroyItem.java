package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Log;

public class RequestDestroyItem extends L2GameClientPacket
{
	private static String _C__5F_REQUESTDESTROYITEM = "[C] 5F RequestDestroyItem";

	private int _objectId;
	private long _count;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		_count = readQ();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(activeChar.isOutOfControl())
		{
			activeChar.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		long count = _count;

		final L2ItemInstance itemToRemove = activeChar.getInventory().getItemByObjectId(_objectId);
		if(itemToRemove == null || itemToRemove.isWear())
			return;

		if(count < 1)
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DESTROY_IT_BECAUSE_THE_NUMBER_IS_INCORRECT);
			return;
		}

		if(itemToRemove.isHeroItem())
		{
			activeChar.sendPacket(Msg.HERO_WEAPONS_CANNOT_BE_DESTROYED);
			return;
		}

		if(!itemToRemove.canBeDestroyed(activeChar))
		{
			activeChar.sendPacket(Msg.THIS_ITEM_CANNOT_BE_DISCARDED);
			return;
		}

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_THAT_WHILE_FISHING);
			return;
		}

		if(activeChar.getPet() != null && activeChar.getPet().getControlItemId() == itemToRemove.getObjectId())
		{
			activeChar.sendPacket(Msg.THE_PET_HAS_BEEN_SUMMONED_AND_CANNOT_BE_DELETED);
			return;
		}

		if(activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_count > itemToRemove.getCount())
			count = itemToRemove.getCount();

		if(itemToRemove.isEquipped())
			activeChar.getInventory().unEquipItemInSlot(itemToRemove.getEquipSlot());

		if(itemToRemove.canBeCrystallized(activeChar, false))
		{
			RequestCrystallizeItem.crystallize(activeChar, itemToRemove);
			return;
		}

		final L2ItemInstance removedItem = activeChar.getInventory().destroyItem(_objectId, count, true);

		Log.LogItem(activeChar, Log.DeleteItem, removedItem);

		activeChar.sendChanges();

		activeChar.sendPacket(SystemMessage.removeItems(removedItem.getItemId(), count));

		L2World.removeObject(removedItem);

		activeChar.updateStats();
	}

	@Override
	public String getType()
	{
		return _C__5F_REQUESTDESTROYITEM;
	}
}
