package l2n.game.network.clientpackets;

import l2n.game.model.L2ShortCut;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ShortCutRegister;

public class RequestShortCutReg extends L2GameClientPacket
{
	private static String _C__3D_REQUESTSHORTCUTREG = "[C] 3D RequestShortCutReg";

	private int _type;
	private int _id;
	private int _slot;
	private int _page;

	/**
	 * packet type id 0x3D
	 * format: cddddd
	 * 
	 * @param rawPacket
	 */
	@Override
	public void readImpl()
	{
		_type = readD();
		final int slot = readD();
		_id = readD();
		readD(); // unknown
		readD(); // unknown

		_slot = slot % 12;
		_page = slot / 12;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_slot < 0 || _slot > 11 || _page < 0 || _page > 10)
		{
			activeChar.sendActionFailed();
			return;
		}

		switch (_type)
		{
			case 0x01: // item
			case 0x03: // action
			case 0x04: // macro
			case 0x05: // recipe
			{
				final L2ShortCut sc = new L2ShortCut(_slot, _page, _type, _id, -1);
				sendPacket(new ShortCutRegister(sc));
				activeChar.registerShortCut(sc);
				break;
			}
			case 0x02: // skill
			{
				final int level = activeChar.getSkillDisplayLevel(_id);
				if(level > 0)
				{
					final L2ShortCut sc = new L2ShortCut(_slot, _page, _type, _id, level);
					sendPacket(new ShortCutRegister(sc));
					activeChar.registerShortCut(sc);
				}
				break;
			}
		}
	}

	@Override
	public String getType()
	{
		return _C__3D_REQUESTSHORTCUTREG;
	}
}
