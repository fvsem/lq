package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.util.Log;
import l2n.util.Rnd;

import java.util.logging.Logger;

public final class RequestExEnchantSkillRouteChange extends L2GameClientPacket
{
	protected static final Logger _log = Logger.getLogger(RequestExEnchantSkillRouteChange.class.getName());
	private int _skillId;
	private int _skillLvl;

	@Override
	protected void readImpl()
	{
		_skillId = readD();
		_skillLvl = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendMessage("You must leave transformation mode first.");
			return;
		}

		if(activeChar.getLevel() < 76)
		{
			sendPacket(Msg.YOU_DONT_MEET_SKILL_LEVEL_REQUIREMENTS);
			return;
		}

		if(activeChar.getClassId().getLevel() < 4)
		{
			sendPacket(Msg.NOT_COMPLETED_QUEST_FOR_SKILL_ACQUISITION);
			return;
		}

		L2Skill skill = SkillTable.getInstance().getInfo(_skillId, _skillLvl);
		if(skill == null)
		{
			_log.warning("RequestExEnchantSkillRouteChange: skillId " + _skillId + " level " + _skillLvl + " not found in Datapack.");
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final L2EnchantSkillLearn sl = SkillTreeTable.getInstance().getSkillEnchant(_skillId, _skillLvl);
		if(sl == null)
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final int currentLevel = activeChar.getSkillLevel(_skillId);
		// do u have this skill enchanted?
		if(currentLevel <= 100)
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final int currentEnchantLevel = currentLevel % 100;
		// is the requested level valid?
		if(currentEnchantLevel != _skillLvl % 100)
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final int[] cost = sl.getCost();
		final int requiredSp = cost[1] * sl.getCostMult() / 5;
		final int requiredAdena = cost[0] * sl.getCostMult() / 5;

		if(activeChar.getSp() < requiredSp)
		{
			activeChar.sendPacket(Msg.SP_REQUIRED_FOR_SKILL_ENCHANT_IS_INSUFFICIENT);
			return;
		}

		if(activeChar.getAdena() < requiredAdena)
		{
			sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		if(!Config.ALT_DISABLE_ENCHANT_BOOKS)
		{
			if(Functions.getItemCount(activeChar, SkillTreeTable.CHANGE_ENCHANT_BOOK) == 0)
			{
				activeChar.sendPacket(Msg.ITEMS_REQUIRED_FOR_SKILL_ENCHANT_ARE_INSUFFICIENT);
				return;
			}
			Functions.removeItem(activeChar, SkillTreeTable.CHANGE_ENCHANT_BOOK, 1);
		}

		Functions.removeItem(activeChar, 57, requiredAdena);
		activeChar.addExpAndSp(0, -1 * requiredSp, false, false);

		final int levelPenalty = Rnd.get(Math.min(4, currentEnchantLevel));

		_skillLvl -= levelPenalty;
		if(_skillLvl % 100 == 0)
			_skillLvl = sl.getBaseLevel();

		skill = SkillTable.getInstance().getInfo(_skillId, _skillLvl);
		if(skill != null)
			activeChar.disableSkill(skill, activeChar.addSkill(skill, true));
		activeChar.sendPacket(ExEnchantSkillResult.SUCCESS);

		if(levelPenalty == 0)
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.ENCHANT_SKILL_ROUTE_CHANGE_WAS_SUCCESSFUL_LV_OF_ENCHANT_SKILL_S1_WILL_REMAIN);
			sm.addString(skill.getName());
			activeChar.sendPacket(sm);
		}
		else
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.ENCHANT_SKILL_ROUTE_CHANGE_WAS_SUCCESSFUL_LV_OF_ENCHANT_SKILL_S1_HAS_BEEN_DECREASED_BY_S2);
			sm.addString(skill.getName());
			sm.addNumber(levelPenalty);
			activeChar.sendPacket(sm);
		}

		Log.add(activeChar.getName() + "[" + activeChar.getObjectId() + "]" + "|Successfully changed route|" + _skillId + "|to+" + _skillLvl + "|" + levelPenalty, "enchant_skills");

		activeChar.sendPacket(new SkillList(activeChar));
		activeChar.sendPacket(new ExEnchantSkillInfo(_skillId, activeChar.getSkillLevel(_skillId)));
		updateSkillShortcuts(activeChar);
	}

	private void updateSkillShortcuts(final L2Player player)
	{
		// update all the shortcuts to this skill
		for(final L2ShortCut sc : player.getAllShortCuts())
			if(sc.id == _skillId && sc.type == L2ShortCut.TYPE_SKILL)
			{
				final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, player.getSkillLevel(_skillId));
				player.sendPacket(new ShortCutRegister(newsc));
				player.registerShortCut(newsc);
			}
	}

	@Override
	public String getType()
	{
		return "[C] D0:34 RequestExEnchantSkillRouteChange";
	}
}
