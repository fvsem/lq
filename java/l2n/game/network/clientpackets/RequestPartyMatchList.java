package l2n.game.network.clientpackets;

import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExPartyRoomMember;
import l2n.game.network.serverpackets.PartyMatchList;

/**
 * This class ...
 * 
 * @version $Revision: 1.1.4.4 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestPartyMatchList extends L2GameClientPacket
{
	private final static String _C__70_REQUESTPARTYMATCHLIST = "[C] 70 RequestPartyMatchList";

	private int _lootDist;
	private int _maxMembers;
	private int _minLevel;
	private int _maxLevel;
	private int _roomId;
	private String _roomTitle;

	/**
	 * packet type id 0x70 sample 70 01 00 00 00 format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_roomId = readD();
		_maxMembers = readD();
		_minLevel = readD();
		_maxLevel = readD();
		_lootDist = readD();
		_roomTitle = readS(64);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		PartyRoom room = null;
		if(activeChar.getPartyRoom() == 0)
			room = PartyRoomManager.getInstance().addRoom(_minLevel, _maxLevel, _maxMembers, _lootDist, _roomTitle, activeChar);
		else if(activeChar.getPartyRoom() == _roomId)
			room = PartyRoomManager.getInstance().changeRoom(Integer.valueOf(_roomId), _minLevel, _maxLevel, _maxMembers, _lootDist, _roomTitle);
		else
			return;
		activeChar.sendPacket(new PartyMatchList(room));
		activeChar.sendPacket(new ExPartyRoomMember(room, activeChar));
	}

	@Override
	public String getType()
	{
		return _C__70_REQUESTPARTYMATCHLIST;
	}
}
