package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.AskJoinParty;
import l2n.game.network.serverpackets.SystemMessage;

/**
 * sample
 * 29
 * 42 00 00 10
 * 01 00 00 00
 * format cdd
 */
public class RequestJoinParty extends L2GameClientPacket
{
	private final static String _C__42_REQUESTJOINPARTY = "[C] 42 RequestJoinParty";
	// Format: cSd

	private String _name;
	private int _itemDistribution;

	@Override
	public void readImpl()
	{
		_name = _buf.hasRemaining() ? readS() : null;
		_itemDistribution = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _name == null)
			return;
        if(!activeChar.getPassCheck())
		{
            return;
        }
		final L2Player target = L2ObjectsStorage.getPlayer(_name);
		if(target == null || target == activeChar || target.isInvisible())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isInTransaction())
		{
			activeChar.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY, Msg.ActionFail);
			return;
		}

		final SystemMessage problem = target.canJoinParty(activeChar);
		if(problem != null)
		{
			activeChar.sendPacket(problem);
			return;
		}

		if(!activeChar.isInParty())
			createNewParty(_itemDistribution, target, activeChar);
		else
			addTargetToParty(_itemDistribution, target, activeChar);
	}

	/**
	 * @param client
	 * @param itemDistribution
	 * @param target
	 * @param activeChar
	 */
	private void addTargetToParty(final int itemDistribution, final L2Player target, final L2Player activeChar)
	{
		if(activeChar.getParty().getMemberCount() >= L2Party.MAX_PARTY_MEMBERS)
		{
			activeChar.sendPacket(Msg.PARTY_IS_FULL);
			return;
		}

		// Только Party Leader может приглашать новых членов
		if(Config.PARTY_LEADER_ONLY_CAN_INVITE && !activeChar.getParty().isLeader(activeChar))
		{
			activeChar.sendPacket(Msg.ONLY_THE_LEADER_CAN_GIVE_OUT_INVITATIONS);
			return;
		}

		if(activeChar.getParty().isInDimensionalRift())
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestJoinParty.InDimensionalRift", activeChar));
			activeChar.sendActionFailed();
		}

		if(!target.isInTransaction())
		{
			new Transaction(TransactionType.PARTY, activeChar, target, 10000);

			target.sendPacket(new AskJoinParty(activeChar.getName(), itemDistribution));
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_INVITED_S1_TO_YOUR_PARTY).addString(target.getName()));
		}
		else
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(target.getName()));

	}

	/**
	 * @param client
	 * @param itemDistribution
	 * @param target
	 * @param requestor
	 */
	private void createNewParty(final int itemDistribution, final L2Player target, final L2Player requestor)
	{
		if(!target.isInTransaction())
		{
			new Transaction(TransactionType.PARTY, requestor, target, 10000).setCreateNewParty(itemDistribution);
			target.sendPacket(new AskJoinParty(requestor.getName(), itemDistribution));
			requestor.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_INVITED_S1_TO_YOUR_PARTY).addString(target.getName()));
		}
		else
			requestor.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(target.getName()));
	}

	@Override
	public String getType()
	{
		return _C__42_REQUESTJOINPARTY;
	}
}
