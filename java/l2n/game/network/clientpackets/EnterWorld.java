package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.captcha.CaptchaValidator;
import l2n.commons.list.GArray;
import l2n.database.utils.mysql;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.Scripts;
import l2n.game.Announcements;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.*;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.L2WorldRegion;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.l2auction.L2AuctionUtils;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Ship;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.model.quest.Quest;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.*;
import l2n.game.skills.AbnormalEffect;
import l2n.game.tables.FriendsTable;
import l2n.util.HWID.HWIDComparator;

import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Enter World Packet Handler
 * <p>
 * <p>
 * 0000: 11
 * <p>
 */
public class EnterWorld extends L2GameClientPacket
{
	private static final String _C__03_ENTERWORLD = "[C] 03 EnterWorld";
	private static final Logger _log = Logger.getLogger(EnterWorld.class.getName());

	private static final Lock lock = new ReentrantLock();

	// private int[][] _hops = new int[5][4];

	@Override
	public void readImpl()
	{
// readB(new byte[32]); // Unknown Byte Array
// readD();
// readD();
// readD();
// readD();
// readB(new byte[32]); // Unknown Byte Array
// readD();

		// for(int i = 0; i < _hops.length; i++)
		// for(int c = 0; c < 4; c++)
		// _hops[i][c] = readC();
	}


	@Override
	public void runImpl()
	{
		final L2GameClient client = getClient();
		final L2Player activeChar = client.getActiveChar();
		if(activeChar == null)
		{
			client.closeNow(false);
			return;
		}

		final int MyObjectId = activeChar.getObjectId();
		final Long MyStoreId = activeChar.getStoredId();
		lock.lock();
		try
		{
			for(final L2Player cha : L2ObjectsStorage.getAllPlayersForIterate())
			{
				if(MyStoreId == cha.getStoredId())
					continue;
				if(cha.getObjectId() == MyObjectId && !cha.isInOfflineMode())
				{
					_log.warning("EnterWorld failed! Character " + activeChar.getName() + "[" + MyObjectId + "] already in the game!");
					cha.logout(false, false, true, false);
				}
			}
		}
		finally
		{
			lock.unlock();
		}
		
		// Проверяем привязку по HWID
		if(activeChar.getNetConnection() != null && activeChar.getNetConnection().protect_used && activeChar.getAllowHWID() != null && Config.LOCK_ACCOUNT_HWID_COMPARATOR.compare(activeChar.getAllowHWID(), activeChar.getHWID()) != HWIDComparator.EQUALS)
		{
			final NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile("data/html/command/lock_denied.htm");
			activeChar.block();
			activeChar.sendPacket(html);
			activeChar.startKickTask(5 * 1000, true);
			//Log.add("Attempting enter to locked account: " + activeChar.toFullString() + ", HWID: " + activeChar.getHWID().Full, "protect");
			return;
		}
		
		final boolean first = activeChar.entering;
		if(first)
		{
			// Включаем неуязвимость при входе в игру
			if(activeChar.isGM() && activeChar.getPlayerAccess().GodMode && Config.GM_STARTUP_INVULNERABLE)
			{
				activeChar.setInvul(true);
				activeChar.startAbnormalEffect(AbnormalEffect.S_INVULNERABLE);
				activeChar.sendMessage("Вы входите в неуязвимом режиме.");
			}
			// Включаем невидимость при входе в игру
			if(activeChar.isGM() && activeChar.getPlayerAccess().GodMode && Config.GM_STARTUP_INVISIBLE)
			{
				activeChar.setInvisible(true);
				activeChar.startAbnormalEffect(AbnormalEffect.STEALTH);
				activeChar.sendMessage("Вы входите в невидимом режиме.");
			}
		}

		if(first)
		{
			activeChar.spawnMe();

			// Restore Vitality
			if(Config.ENABLE_VITALITY && Config.RECOVER_VITALITY_ON_RECONNECT)
				activeChar.restoreVitality();
		}
		else if(activeChar.isTeleporting())
			activeChar.onTeleported();

		if(SevenSigns.getInstance().isSealValidationPeriod())
			activeChar.sendPacket(SSQInfo.currentSSQInfo());
				if(Config.GM_ANNOUNCE_LOGIN)
		        {
		        	if (activeChar.isGM())
		        	{
						Announcements.announceToAll("[Администратор]: "+activeChar.getName()+ " входит в Мир");

		        	}
		        }
				if (Config.ENT_SHOWENTERMESSON)
		        {
				activeChar.sendPacket(new ExShowScreenMessage(Config.ENT_SHOWENTERMESS, 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER));
		        }

		activeChar.getMacroses().sendUpdate();
		sendPacket(new HennaInfo(activeChar), new ItemList(activeChar, false), new ShortCutInit(activeChar), new SkillList(activeChar), Msg.WELCOME);

		Announcements.getInstance().showAnnouncements(activeChar);

		// add char to online characters
		activeChar.setOnlineStatus(true);

		// Вызов всех хэндлеров, определенных в скриптах
		if(first)
		{
			final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_ENTER_WORLD);
			for(final EventScript script : scripts)
				script.notifyEnterWorld(activeChar);
		}

		// Посылаем уведомление о проводимом аукционе
		L2AuctionUtils.playerEntered(activeChar);

		if(Config.ONLINE_PLAYERS_AT_STARTUP)
		{
			final int online = L2ObjectsStorage.getAllPlayersCount();
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.EnterWorld", activeChar).addNumber(online));
		}
		SevenSigns.getInstance().sendCurrentPeriodMsg(activeChar);

		if(Config.PETITIONING_ALLOWED)
			PetitionManager.getInstance().checkPetitionMessages(activeChar);

		final Collection<L2GameServerPacket> packets = new GArray<L2GameServerPacket>();
		if(first && activeChar.getCreateTime() != 0)
		{
			final Calendar create = Calendar.getInstance();
			create.setTimeInMillis(activeChar.getCreateTime());
			final Calendar now = Calendar.getInstance();
			now.setTimeInMillis(System.currentTimeMillis());

			if(create.get(Calendar.MONTH) == now.get(Calendar.MONTH) && create.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH) && create.get(Calendar.YEAR) != now.get(Calendar.YEAR))
			{
				if(create.get(Calendar.YEAR) - now.get(Calendar.YEAR) == 5)
					packets.add(Msg.FIVE_YEARS_HAVE_PASSED_SINCE_THIS_CHARACTERS_CREATION);
				packets.add(Msg.ExNotifyBirthDay);
				packets.add(Msg.YOUR_BIRTHDAY_GIFT_HAS_ARRIVED_YOU_CAN_OBTAIN_IT_FROM_THE_GATEKEEPER_IN_ANY_VILLAGE);
			}

			final int daysDiff = create.get(Calendar.DAY_OF_YEAR) - now.get(Calendar.DAY_OF_YEAR);
			if(daysDiff <= 7 && daysDiff > 1)
				packets.add(new SystemMessage(SystemMessage.THERE_ARE_S1_DAYS_UNTIL_YOUR_CHARACTERS_BIRTHDAY_ON_THAT_DAY_YOU_CAN_OBTAIN_A_SPECIAL_GIFT_FROM_THE_GATEKEEPER_IN_ANY_VILLAGE).addNumber(daysDiff));

			sendPackets(packets);
			packets.clear();
		}

		if(activeChar.getClan() != null)
		{
			notifyClanMembers(activeChar);
			sendPacket(new PledgeShowMemberListAll(activeChar.getClan(), activeChar), new PledgeShowInfoUpdate(activeChar.getClan()), new PledgeSkillList(activeChar.getClan()));

			if(activeChar.getClan().isAttacker())
				activeChar.setSiegeState(1);
			else if(activeChar.getClan().isDefender())
				activeChar.setSiegeState(2);

			// Обновляем инфу о умениях отряда
			activeChar.getClan().showSquadSkillsToPlayer(activeChar);
		}

		// engage and notify Partner
		if(first && Config.ALLOW_WEDDING)
		{
			CoupleManager.getInstance().engage(activeChar);
			CoupleManager.getInstance().notifyPartner(activeChar);

			// Check if player is maried and remove if necessary Cupid's Bow
			if(!activeChar.isMaried())
			{
				final L2ItemInstance item = activeChar.getInventory().getItemByItemId(9140);
				// Remove Cupid's Bow
				if(item != null && !activeChar.isGM())
				{
					activeChar.sendMessage("Удаление Cupid's Bow");
					activeChar.getInventory().destroyItem(item, 1, true);
					activeChar.getInventory().updateDatabase();
					// Log it
					_log.info("Character " + activeChar.getName() + " of account " + activeChar.getAccountName() + " got Cupid's Bow removed.");
				}
			}
		}

		if(first)
		{
			notifyFriends(activeChar, true);
			loadTutorial(activeChar);
			activeChar.restoreDisableSkills();
		}
		else
			packets.add(new L2FriendList(activeChar, false));

		packets.add(new ExStorageMaxCount(activeChar));
		packets.add(new QuestList(activeChar));
		packets.add(new ExBasicActionList());
		packets.add(new SkillCoolTime(activeChar));
		// refresh player info
		packets.add(new EtcStatusUpdate(activeChar));
		if(activeChar.getPcBangPoints() > 0)
			packets.add(new ExPCCafePointInfo(activeChar));
		else
			packets.add(new ExPCCafePointInfo());
		// Send Teleport Bookmark List
		packets.add(new ExGetBookMarkInfo(activeChar));

		sendPackets(packets);
		packets.clear();

		if(activeChar.isTransformed() && !activeChar.getTransform().isDefaultActionListTransform())
			sendPacket(ExBasicActionList.TRANSFORMED_ACTION_LIST);
		else
			sendPacket(ExBasicActionList.DEFAULT_ACTION_LIST);

		CursedWeaponsManager.getInstance().checkPlayer(activeChar);

		// refresh player info
		activeChar.checkHpMessages(activeChar.getMaxHp(), activeChar.getCurrentHp());
		activeChar.checkDayNightMessages();

		if(!first)
		{
			if(activeChar.isCastingNow())
			{
				final L2Character castingTarget = activeChar.getCastingTarget();
				final L2Skill castingSkill = activeChar.getCastingSkill();
				final long animationEndTime = activeChar.getAnimationEndTime();
				if(castingSkill != null && castingTarget != null && castingTarget.isCharacter() && activeChar.getAnimationEndTime() > 0)
					packets.add(new MagicSkillUse(activeChar, castingTarget, castingSkill.getId(), castingSkill.getLevel(), (int) (animationEndTime - System.currentTimeMillis()), 0));
			}

			if(activeChar.isInVehicle() && !activeChar.getVehicle().isClanAirShip())
				if(activeChar.getVehicle().isAirShip())
					packets.add(new ExGetOnAirShip(activeChar, (L2AirShip) activeChar.getVehicle(), activeChar.getInVehiclePosition()));
				else
					packets.add(new GetOnVehicle(activeChar, (L2Ship) activeChar.getVehicle(), activeChar.getInVehiclePosition()));

			if(activeChar.isMoving || activeChar.isFollow)
				packets.add(new CharMoveToLocation(activeChar));

			if(activeChar.getMountNpcId() != 0)
				packets.add(new Ride(activeChar));
		}

		sendPackets(packets);
		packets.clear();

		activeChar.entering = false;
		activeChar.sendUserInfo(true);

		// check if that player is bot
		if(Config.CAPTCHA_ENABLE || !Config.CAPTCHA_SHOW_PLAYERS_WITH_PA && activeChar.hasPremiumAccount())
		{
			CaptchaValidator.getInstance().sendCaptcha(activeChar);
		}
		
		// отображаем рамку вокруг лвла если игрок с премиум аккаунтом
		if(activeChar.hasPremiumAccount())
			packets.add(new ExBR_PremiumState(activeChar.getObjectId(), 1));

		sendPackets(packets);
		packets.clear();

		if(activeChar.isSitting())
			activeChar.sendPacket(new ChangeWaitType(activeChar, ChangeWaitType.WT_SITTING));
		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			if(activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_BUY)
				packets.add(new PrivateStoreMsgBuy(activeChar));
			else if(activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL || activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_SELL_PACKAGE)
				packets.add(new PrivateStoreMsgSell(activeChar));
			else if(activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_MANUFACTURE)
				packets.add(new RecipeShopMsg(activeChar));

		// no broadcast needed since the player will already spawn dead to others
		if(activeChar.isDead())
			packets.add(new Die(activeChar));

		sendPackets(packets);
		packets.clear();

		activeChar.unsetVar("offline");

		// на всякий случай
		activeChar.sendActionFailed();

		// сообщение о времени, которое осталось просидеть в тюрьме (если чар в тюрьме)
		if(activeChar.isInJail())
			if(activeChar.isLangRus())
				activeChar.sendMessage("Вы в тюрьме; осталось " + activeChar.getVar("jailed") + " минут.");
			else
				activeChar.sendMessage("You are in jail " + activeChar.getVar("jailed") + " minutes left.");

		PlayerMessageStack.getInstance().CheckMessages(activeChar);
		if(activeChar != null)
			activeChar.setEnterWorldTime(); // запишем время когда чар уже полностью зашел в игру

		sendPacket(new ClientSetTime(), new ExSetCompassZoneCode(activeChar));
		checkNewMail(activeChar);

		if(activeChar.isReviveRequested())
			sendPacket(new ConfirmDlgPacket(SystemMessage.S1_IS_MAKING_AN_ATTEMPT_AT_RESURRECTION_WITH_$S2_EXPERIENCE_POINTS_DO_YOU_WANT_TO_CONTINUE_WITH_THIS_RESURRECTION, 0, ConfirmDlg.REVIVE_ANSWER).addString("Other player").addString("some"));

		if(!first)
		{
			if(activeChar.getCurrentRegion() != null)
				for(final L2WorldRegion neighbor : activeChar.getCurrentRegion().getNeighbors())
					neighbor.showObjectsToPlayer(activeChar);

			if(activeChar.getPet() != null)
				packets.add(new PetInfo(activeChar.getPet()));

			if(activeChar.isInParty())
			{
				L2Summon member_pet;
				// sends new member party window for all members
				// we do all actions before adding member to a list, this speeds things up a little
				packets.add(new PartySmallWindowAll(activeChar.getParty(), activeChar));

				for(final L2Player member : activeChar.getParty().getPartyMembers())
					if(member != activeChar)
					{
						packets.add(new PartySpelled(member, true));
						if((member_pet = member.getPet()) != null)
							packets.add(new PartySpelled(member_pet, true));
						packets.addAll(RelationChanged.update(member, activeChar));
					}

				// Если партия уже в СС, то вновь прибывшем посылаем пакет открытия окна СС
				if(activeChar.getParty().isInCommandChannel())
					packets.add(Msg.ExMPCCOpen);
			}

			for(final int shotId : activeChar.getAutoSoulShot())
				packets.add(new ExAutoSoulShot(shotId, true));

			sendPackets(packets);
			packets.clear();

			activeChar.broadcastCharInfo();
		}
		else
			activeChar.sendUserInfo(); // Отобразит права в клане
		
		if(Config.CHAR_PASS_ENABLE && !activeChar.isFakePlayer())
		{
            activeChar.setPassParalyzedTrue();
            String htmContent;
			if (RequestBypassToServer.getPassKeyEnable(activeChar))
			{
				htmContent = "data/html/passkey/login.htm";
			}
			else
			{
				htmContent = "data/html/passkey/setup.htm";
			}
            NpcHtmlMessage html = new NpcHtmlMessage(1);
            html.setFile(htmContent);
            activeChar.sendPacket(html);
            html = null;
		}
		else
		{
			activeChar.setPassCheck(true);
		}
                
		if (Config.ANNOUNCE_CASTLE_LORDS)
			notifyCastleOwner(activeChar);
		
		// Проверка корон от замком
		checkItems(activeChar);

		// на всякий случай
		activeChar.updateEffectIcons();
		activeChar.updateStats();
		activeChar.sendActionFailed();
	}

	public static void notifyFriends(final L2Player cha, final boolean login)
	{
		if(login)
			cha.sendPacket(new L2FriendList(cha, false));

		try
		{
			for(final int friend_id : FriendsTable.getInstance().getFriendsList(cha.getObjectId()))
			{
				final L2Player friend = L2ObjectsStorage.getPlayer(friend_id);
				if(friend != null)
					if(login)
						friend.sendPacket(new SystemMessage(SystemMessage.S1_FRIEND_HAS_LOGGED_IN).addString(cha.getName()), new L2FriendStatus(cha, true));
					else
						friend.sendPacket(new L2FriendStatus(cha, false));
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "could not restore friend data:", e);
		}
	}

	/**
	 * @param activeChar
	 */
	private void notifyClanMembers(final L2Player activeChar)
	{
		final L2Clan clan = activeChar.getClan();
		if(clan == null || clan.getClanMember(activeChar.getObjectId()) == null)
			return;

		clan.getClanMember(activeChar.getObjectId()).setPlayerInstance(activeChar);
		final int sponsor = activeChar.getSponsor();
		final int apprentice = activeChar.getApprentice();
		final SystemMessage msg = new SystemMessage(SystemMessage.CLAN_MEMBER_S1_HAS_LOGGED_INTO_GAME).addString(activeChar.getName());
		final PledgeShowMemberListUpdate memberUpdate = new PledgeShowMemberListUpdate(activeChar);
		for(final L2Player clanMember : clan.getOnlineMembers(activeChar.getName()))
		{
			clanMember.sendPacket(memberUpdate);
			if(clanMember.getObjectId() == sponsor)
				clanMember.sendPacket(new SystemMessage(SystemMessage.S1_YOUR_CLAN_ACADEMYS_APPRENTICE_HAS_LOGGED_IN).addString(activeChar.getName()));
			else if(clanMember.getObjectId() == apprentice)
				clanMember.sendPacket(new SystemMessage(SystemMessage.S1_YOUR_CLAN_ACADEMYS_SPONSOR_HAS_LOGGED_IN).addString(activeChar.getName()));
			else
				clanMember.sendPacket(msg);
		}

		// Отправляем клановое уведомление
		if(clan.isNoticeEnabled() && clan.getNotice() != "")
		{
			final NpcHtmlMessage notice = new NpcHtmlMessage(5);
			notice.setHtml("<html><body><center><font color=\"Уровень\">" + activeChar.getClan().getName() + " Уведомление о клане</font></center><br>" + activeChar.getClan().getNotice() + "</body></html>");
			sendPacket(notice);
		}
	}

	private void checkItems(final L2Player activeChar)
	{
		final L2Clan clan = activeChar.getClan();

		final Inventory inv = activeChar.getInventory();
		for(final L2ItemInstance item : inv.getItems())
		{
			final int itemId = item.getItemId();

			// delete The Lord's Crown if owner not castle lords
			if(itemId == 6841 && (clan == null || !activeChar.isClanLeader() || clan.getHasCastle() == 0))
			{
				inv.destroyItem(item, item.getCount(), false);
				continue;
			}

			// delete Castle Lord circlets
			if(Inventory.CASTLE_LORD_CIRCLETS_LIST.contains(itemId))
				if(clan == null || itemId != Inventory.CASTLE_LORD_CIRCLETS[clan.getHasCastle()])
				{
					inv.destroyItem(item, item.getCount(), false);
					continue;
				}
		}

		// Проверяем наличие предметов для SeedOfInfinityManager
		if(activeChar.getVarB("NeedToDeleteSOIItems", false))
		{
			activeChar.destroyItemByItemId(13691, 1, false);
			activeChar.destroyItemByItemId(13692, 1, false);
			activeChar.unsetVar("NeedToDeleteSOIItems");
		}
	}

	private void loadTutorial(final L2Player player)
	{
		final Quest q = QuestManager.getQuest(255);
		if(q != null)
			player.processQuestEvent(q.getName(), "UC", null);
	}

	private void checkNewMail(final L2Player activeChar)
	{
		if(mysql.simple_get_int("messageId", "mail", "unread AND receiver=" + activeChar.getObjectId()) > 0)
			activeChar.sendPacket(new ExNoticePostArrived(0));
	}

	@Override
	public String getType()
	{
		return _C__03_ENTERWORLD;
	}
	
private void notifyCastleOwner(final L2Player activeChar)
{
	L2Clan clan = activeChar.getClan();

	if (clan != null)
	{
		if (clan.getHasCastle() > 0)
		{
			Castle castle = CastleManager.getInstance().getCastleByIndex(clan.getHasCastle());

			if ((castle != null) && (activeChar.getObjectId() == clan.getLeaderId())) {
				Announcements.getInstance();
				Announcements.announceToAll("Лорд " + castle.getName() + " Castle: " + activeChar.getName() + " входит в Мир");

			}

		}
	}
}
}