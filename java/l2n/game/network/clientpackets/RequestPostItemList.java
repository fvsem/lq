package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExReplyPostItemList;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:46:28
 */
public class RequestPostItemList extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!activeChar.isInZonePeace())
		{
			activeChar.sendPacket(Msg.CANNOT_USE_MAIL_OUTSIDE_PEACE_ZONE);
			return;
		}
		activeChar.sendPacket(new ExReplyPostItemList(activeChar));
	}
}
