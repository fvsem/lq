package l2n.game.network.clientpackets;

import l2n.game.handler.ItemHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExAutoSoulShot;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestAutoSoulShot extends L2GameClientPacket
{
	private final static String _C__D0_0D_REQUESTAUTOSOULSHOT = "[C] D0:0D RequestAutoSoulShot";

	private int _itemId;
	private boolean _type; // 1 = on : 0 = off;

	@Override
	public void readImpl()
	{
		_itemId = readD();
		_type = readD() == 1;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE || activeChar.isInTransaction() || activeChar.isDead())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2ItemInstance item = activeChar.getInventory().findItemByItemId(_itemId);
		if(item == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_type)
		{
			activeChar.addAutoSoulShot(_itemId);
			activeChar.sendPacket(new ExAutoSoulShot(_itemId, true), new SystemMessage(SystemMessage.THE_USE_OF_S1_WILL_NOW_BE_AUTOMATED).addString(item.getItem().getName()));
			final IItemHandler handler = ItemHandler.getInstance().getItemHandler(_itemId);
			handler.useItem(activeChar, item);
			return;
		}

		activeChar.removeAutoSoulShot(_itemId);
		activeChar.sendPacket(new ExAutoSoulShot(_itemId, false), new SystemMessage(SystemMessage.THE_AUTOMATIC_USE_OF_S1_WILL_NOW_BE_CANCELLED).addString(item.getItem().getName()));
	}

	@Override
	public String getType()
	{
		return _C__D0_0D_REQUESTAUTOSOULSHOT;
	}
}
