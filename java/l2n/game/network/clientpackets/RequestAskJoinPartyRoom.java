package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.instancemanager.PartyRoomManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.PartyRoom;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.ExAskJoinPartyRoom;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestAskJoinPartyRoom extends L2GameClientPacket
{
	/**
	 * @param buf
	 * @param client
	 *            format: (ch)S
	 */
	// 555
	private final static String _C__D0_2f_REQUESTASKJOINPARTYROOM = "[C] D0:2f RequestAskJoinPartyRoom";
	private String name;

	@Override
	public void readImpl()
	{
		name = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;
		final L2Player target = L2ObjectsStorage.getPlayer(name);

		if(target == null || target == player)
		{
			player.sendActionFailed();
			return;
		}

		if(player.getPartyRoom() <= 0)
		{
			player.sendActionFailed();
			return;
		}

		if(player.isInTransaction())
		{
			player.sendPacket(Msg.WAITING_FOR_ANOTHER_REPLY);
			return;
		}

		if(target.isInTransaction())
		{
			player.sendPacket(new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER).addString(target.getName()));
			return;
		}

		if(target.getPartyRoom() > 0)
		{
			player.sendPacket(new SystemMessage(SystemMessage.S1_IS_A_MEMBER_OF_ANOTHER_PARTY_AND_CANNOT_BE_INVITED).addString(target.getName()));
			return;
		}

		final PartyRoom room = PartyRoomManager.getInstance().getRooms().get(player.getPartyRoom());
		if(room == null)
		{
			player.sendActionFailed();
			return;
		}

		if(room.getMembers().size() >= room.getMaxMembers())
		{
			player.sendPacket(Msg.PARTY_IS_FULL);
			return;
		}

		if(!PartyRoomManager.getInstance().isLeader(player))
		{
			player.sendPacket(Msg.ONLY_THE_LEADER_CAN_GIVE_OUT_INVITATIONS);
			return;
		}

		new Transaction(TransactionType.PARTY_ROOM, player, target, 10000);

		target.sendPacket(new ExAskJoinPartyRoom(player.getName()));
		player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_INVITED_YOU_TO_ENTER_THE_PARTY_ROOM).addString(target.getName()));
	}

	@Override
	public String getType()
	{
		return _C__D0_2f_REQUESTASKJOINPARTYROOM;
	}
}
