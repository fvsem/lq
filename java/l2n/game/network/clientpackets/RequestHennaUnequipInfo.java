package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;
import l2n.game.network.serverpackets.HennaUnequipItemInfo;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.HennaTable;
import l2n.game.templates.L2Henna;

/**
 * @author L2System
 * @date 01.03.2010
 * @time 1:16:12
 */
public class RequestHennaUnequipInfo extends L2GameClientPacket
{
	private static final String _C__BB_RequestHennaItemRemoveInfo = "[C] 71 RequestHennaUnequipInfo";

	private int _symbolId;

	@Override
	protected void readImpl()
	{
		_symbolId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Henna template = HennaTable.getInstance().getTemplate(_symbolId);
		if(template == null)
		{
			sendPacket(new SystemMessage(SystemMessage.THE_SYMBOL_INFORMATION_CANNOT_BE_FOUND));
			return;
		}

		sendPacket(new HennaUnequipItemInfo(new L2HennaInstance(template), activeChar));

	}

	@Override
	public String getType()
	{
		return _C__BB_RequestHennaItemRemoveInfo;
	}
}
