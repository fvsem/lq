package l2n.game.network.clientpackets;

import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.RecipeShopSellList;

/**
 * Возврат к списку из информации о рецепте
 */
public class RequestRecipeShopSellList extends L2GameClientPacket
{
	private static String TYPE = "[C] C0 RequestRecipeShopSellList";

	private int _objectId;

	@Override
	public void readImpl()
	{
		_objectId = _buf.hasRemaining() ? readD() : 0;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
			activeChar.sendActionFailed();
            return;
        }
		final L2Player trader = L2ObjectsStorage.getPlayer(_objectId);
		if(trader != null)
			activeChar.sendPacket(new RecipeShopSellList(activeChar, trader));
		else
			activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return TYPE;
	}
}
