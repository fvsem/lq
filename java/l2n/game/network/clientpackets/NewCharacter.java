package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.base.ClassId;
import l2n.game.network.serverpackets.NewCharacterSuccess;
import l2n.game.tables.CharTemplateTable;

import java.util.logging.Logger;

/**
 * This class ...
 * 
 * @version $Revision: 1.3.4.5 $ $Date: 2005/03/27 15:29:30 $
 */
public class NewCharacter extends L2GameClientPacket
{
	private static String _C__13_NEWCHARACTER = "[C] 13 NewCharacter";
	private static Logger _log = Logger.getLogger(NewCharacter.class.getName());

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		if(Config.DEBUG)
			_log.fine("CreateNewChar");

		final NewCharacterSuccess ct = new NewCharacterSuccess();

		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.fighter, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.mage, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.elvenFighter, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.elvenMage, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.darkFighter, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.darkMage, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.orcFighter, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.orcMage, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.dwarvenFighter, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.maleSoldier, false));
		ct.addChar(CharTemplateTable.getInstance().getTemplate(ClassId.femaleSoldier, false));

		sendPacket(ct);
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.clientpackets.ClientBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__13_NEWCHARACTER;
	}
}
