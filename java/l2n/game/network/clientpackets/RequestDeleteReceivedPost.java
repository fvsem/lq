package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;
import l2n.game.network.serverpackets.ExChangePostState;

/**
 * @author L2System
 * @date 29.07.2010
 * @time 16:45:56
 */
public class RequestDeleteReceivedPost extends L2GameClientPacket
{
	private static final int BATCH_LENGTH = 4; // length of the one item
	private int[] _list;

	@Override
	public void readImpl()
	{
		final int count = readD();
		if(count <= 0 || count > MailParcelController.INBOX_SIZE || count * BATCH_LENGTH != _buf.remaining())
			return;

		_list = new int[count];
		for(int i = 0; i < count; i++)
			_list[i] = readD();
	}

	@Override
	public void runImpl()
	{
		if(_list.length == 0)
			return;

		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(cha.isOutOfControl())
		{
			cha.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		if(!cha.isInZonePeace())
		{
			cha.sendPacket(Msg.CANNOT_USE_MAIL_OUTSIDE_PEACE_ZONE);
			return;
		}

		MailParcelController.getInstance().deleteLetter(_list);
		cha.sendPacket(new ExChangePostState(true, _list, MailParcelController.DELETED));
	}

}
