package l2n.game.network.clientpackets;

import l2n.game.ai.CtrlEvent;
import l2n.game.model.actor.L2Player;
import l2n.util.Location;

public class CannotMoveAnymore extends L2GameClientPacket
{
	private static String _C__47_STOPMOVE = "[C] 47 CannotMoveAnymore";
	private final Location _loc = new Location();

	/**
	 * packet type id 0x47
	 * sample
	 * 36
	 * a8 4f 02 00 // x
	 * 17 85 01 00 // y
	 * a7 00 00 00 // z
	 * 98 90 00 00 // heading?
	 * format: cdddd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_loc.x = readD();
		_loc.y = readD();
		_loc.z = readD();
		_loc.h = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.getAI().notifyEvent(CtrlEvent.EVT_ARRIVED_BLOCKED, _loc, null);
	}

	@Override
	public String getType()
	{
		return _C__47_STOPMOVE;
	}
}
