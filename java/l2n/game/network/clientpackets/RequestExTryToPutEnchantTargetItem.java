package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExPutEnchantTargetItemResult;
import l2n.util.Log;

public class RequestExTryToPutEnchantTargetItem extends AbstractEnchantPacket
{
	private static final String _C__D0_78_REQUESTEXTRYTOPUTENCHANTTARGETITEM = "[C] D0 4F RequestExTryToPutEnchantTargetItem";

	private int _objectId = 0;

	@Override
	protected void readImpl()
	{
		_objectId = readD();
	}

	@Override
	protected void runImpl()
	{
		if(_objectId == 0)
			return;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl() || activeChar.isActionsDisabled())
		{
			activeChar.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			return;
		}

		if(activeChar.isEnchanting())
			return;

		final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
		final L2ItemInstance scroll = activeChar.getActiveEnchantItem();

		if(item == null || scroll == null)
		{
			activeChar.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			return;
		}

		Log.add(activeChar.getName() + "|Trying to put enchant|" + item.getItemId() + "|+" + item.getEnchantLevel() + "|" + item.getObjectId(), "enchants");

		// template for scroll
		final EnchantScroll scrollTemplate = getEnchantScroll(scroll);

		if(!scrollTemplate.isValid(activeChar, item) || !isEnchantable(item, false) && item.getItemId() != 13539)
		{
			activeChar.sendPacket(Msg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			return;
		}

		activeChar.setIsEnchanting(true);
		activeChar.setActiveEnchantTimestamp(System.currentTimeMillis());
		activeChar.sendPacket(new ExPutEnchantTargetItemResult(_objectId));
	}

	@Override
	public String getType()
	{
		return _C__D0_78_REQUESTEXTRYTOPUTENCHANTTARGETITEM;
	}
}
