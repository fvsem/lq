package l2n.game.network.clientpackets;

import l2n.game.RecipeController;
import l2n.game.model.actor.L2Player;

public class RequestRecipeItemMakeSelf extends L2GameClientPacket
{
	private static String _C__B8_REQUESTRECIPEITEMMAKESELF = "[C] B8 RequestRecipeItemMakeSelf";
	// private static Logger _log = Logger.getLogger(RequestSellItem.class.getName());

	private int _id;

	/**
	 * packet type id 0xB8
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_id = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getDuel() != null)
		{
			activeChar.sendActionFailed();
			return;
		}

		RecipeController.getInstance().requestMakeItem(activeChar, _id);
	}

	@Override
	public String getType()
	{
		return _C__B8_REQUESTRECIPEITEMMAKESELF;
	}
}
