package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Alliance;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.ClanTable;

public class RequestDismissAlly extends L2GameClientPacket
{
	private static final String _C__90_REQUESTDISMISSALLY = "[C] 90 RequestDismissAlly";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		final L2Clan clan = activeChar.getClan();
		if(clan == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Alliance alliance = clan.getAlliance();
		if(alliance == null)
		{
			activeChar.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_ALLIED_WITH_ANY_CLANS);
			return;
		}

		if(!activeChar.isAllyLeader())
		{
			activeChar.sendPacket(Msg.FEATURE_AVAILABLE_TO_ALLIANCE_LEADERS_ONLY);
			return;
		}

		if(alliance.getMembersCount() > 1)
		{
			activeChar.sendPacket(Msg.YOU_HAVE_FAILED_TO_DISSOLVE_THE_ALLIANCE);
			return;
		}

		ClanTable.getInstance().dissolveAlly(activeChar);
	}

	@Override
	public String getType()
	{
		return _C__90_REQUESTDISMISSALLY;
	}
}
