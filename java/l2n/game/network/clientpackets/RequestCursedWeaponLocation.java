package l2n.game.network.clientpackets;

import l2n.commons.list.GArray;
import l2n.game.instancemanager.CursedWeaponsManager;
import l2n.game.model.CursedWeapon;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.ExCursedWeaponLocation;
import l2n.game.network.serverpackets.ExCursedWeaponLocation.CursedWeaponInfo;
import l2n.util.Location;

/**
 * Format: (ch)
 */
public class RequestCursedWeaponLocation extends L2GameClientPacket
{
	private static String _C__D0_2B_REQUESTCURSEDWEAPONLOCATION = "[C] D0:2B RequestCursedWeaponLocation";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Character activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final GArray<CursedWeaponInfo> list = new GArray<CursedWeaponInfo>();
		for(final CursedWeapon cw : CursedWeaponsManager.getInstance().getCursedWeapons())
		{
			final Location pos = cw.getWorldPosition();
			if(pos != null)
				list.add(new CursedWeaponInfo(pos, cw.getItemId(), cw.isActivated() ? 1 : 0));
		}

		// send the ExCursedWeaponLocation
		activeChar.sendPacket(new ExCursedWeaponLocation(list));
	}

	@Override
	public String getType()
	{
		return _C__D0_2B_REQUESTCURSEDWEAPONLOCATION;
	}
}
