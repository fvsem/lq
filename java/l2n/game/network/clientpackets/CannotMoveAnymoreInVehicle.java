package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.StopMoveInVehicle;
import l2n.util.Location;

// format: cddddd
public class CannotMoveAnymoreInVehicle extends L2GameClientPacket
{
	private static final String _C__76_CANTMOVEANYMOREVEHICLE = "[C] 76 CanNotMoveAnymore(Vehicle)";
	private final Location _loc = new Location();
	private int _boatid;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_boatid = readD();
		_loc.x = readD();
		_loc.y = readD();
		_loc.z = readD();
		_loc.h = readD();

	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(activeChar.isInVehicle())
			if(activeChar.getVehicle().getObjectId() == _boatid)
			{
				activeChar.setInVehiclePosition(_loc);
				activeChar.setHeading(_loc.h);
				activeChar.broadcastPacket(new StopMoveInVehicle(activeChar, _boatid));
			}
	}

	@Override
	public String getType()
	{
		return _C__76_CANTMOVEANYMOREVEHICLE;
	}
}
