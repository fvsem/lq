package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestEvaluate extends L2GameClientPacket
{
	private static String _C__B9_REQUESTEVALUATE = "[C] B9 RequestEvaluate";

	// private static Logger _log = Logger.getLogger(RequestEvaluate.class.getName());

	@SuppressWarnings("unused")
	private int _targetid;

	@Override
	public void readImpl()
	{
		_targetid = readD();
	}

	@Override
	public void runImpl()
	{
		SystemMessage sm;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPlayerAccess().CanEvaluate)
			return;

		if(activeChar.getTarget() == null)
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		final L2Player target = activeChar.getTarget().getPlayer();

		if(target == null)
		{
			activeChar.sendPacket(Msg.TARGET_IS_INCORRECT);
			return;
		}

		if(activeChar.getLevel() < 10)
		{
			sm = Msg.ONLY_LEVEL_SUP_10_CAN_RECOMMEND;
			activeChar.sendPacket(sm);
			return;
		}

		if(target == activeChar)
		{
			sm = Msg.YOU_CANNOT_RECOMMEND_YOURSELF;

			activeChar.sendPacket(sm);
			return;
		}

		if(activeChar.getRecomLeft() <= 0)
		{
			sm = Msg.NO_MORE_RECOMMENDATIONS_TO_HAVE;
			activeChar.sendPacket(sm);
			return;
		}

		if(target.getRecomHave() >= 255)
		{
			sm = Msg.YOU_NO_LONGER_RECIVE_A_RECOMMENDATION;
			activeChar.sendPacket(sm);
			return;
		}

		if(!activeChar.canRecom(target))
		{
			sm = Msg.THAT_CHARACTER_IS_RECOMMENDED;

			activeChar.sendPacket(sm);
			return;
		}

		activeChar.giveRecom(target);
		sm = new SystemMessage(SystemMessage.YOU_HAVE_RECOMMENDED);
		sm.addString(target.getName());
		sm.addNumber(activeChar.getRecomLeft());
		activeChar.sendPacket(sm);

		sm = new SystemMessage(SystemMessage.YOU_HAVE_BEEN_RECOMMENDED);
		sm.addString(activeChar.getName());
		target.sendPacket(sm);

		activeChar.sendUserInfo(false);
		target.broadcastUserInfo(true);
	}

	@Override
	public String getType()
	{
		return _C__B9_REQUESTEVALUATE;
	}
}
