package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.base.Transaction;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.PetItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.PetDataTable;
import l2n.util.Log;

public class RequestGiveItemToPet extends L2GameClientPacket
{
	private static final String REQUESTCIVEITEMTOPET__C__8B = "[C] 8B RequestGiveItemToPet";

	private int _objectId;
	private long _amount;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		_amount = readQ();
	}

	@Override
	public void runImpl()
	{
		if(_amount < 1)
			return;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Summon summon = activeChar.getPet();
		if(summon == null || !summon.isPet())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2PetInstance pet = (L2PetInstance) summon;
		if(pet.isDead())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.CANNOT_GIVE_ITEMS_TO_A_DEAD_PET));
			return;
		}

		if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		Transaction trans = activeChar.getTransaction();
		if(trans != null && trans.isInProgress() && trans.isTrade())
		{
			activeChar.sendActionFailed();
			trans.cancel();
			return;
		}

		final L2ItemInstance item = activeChar.getInventory().getItemByObjectId(_objectId);
		if(item == null || item.getObjectId() == pet.getControlItemId())
			return;

		if(item.isEquipable() && !item.getItem().isForPet() || PetDataTable.isPetControlItem(item))
		{
			activeChar.sendPacket(Msg.ITEM_NOT_AVAILABLE_FOR_PETS);
			return;
		}

		if(pet.getInventory().getTotalWeight() + item.getItem().getWeight() * _amount >= pet.getMaxLoad())
		{
			activeChar.sendPacket(Msg.EXCEEDED_PET_INVENTORYS_WEIGHT_LIMIT);
			return;
		}

		if(!item.canBeDropped(activeChar))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_amount >= item.getCount())
		{
			activeChar.getInventory().dropItem(_objectId, item.getCount(), false);
			item.setCustomFlags(item.getCustomFlags() | L2ItemInstance.FLAG_PET_EQUIPPED, true);
			pet.getInventory().addItem(item);
		}
		else
		{
			final L2ItemInstance newPetItem = activeChar.getInventory().dropItem(_objectId, _amount, false);
			pet.getInventory().addItem(newPetItem);
		}

		activeChar.sendChanges();
		activeChar.sendPacket(new PetItemList(pet));
		pet.broadcastCharInfo();

		Log.LogItem(activeChar, pet, Log.GiveItemToPet, item);
	}

	@Override
	public String getType()
	{
		return REQUESTCIVEITEMTOPET__C__8B;
	}
}
