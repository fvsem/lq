package l2n.game.network.clientpackets;

/**
 * Format chS
 * c: (id) 0x39
 * h: (subid) 0x01
 * S: the summon name (or maybe cmd string ?)
 */
class SuperCmdSummonCmd extends L2GameClientPacket
{
	private static String _C__39_01_SUPERCMDSUMMONCMD = "[C] 39:01 SuperCmdSummonCmd";
	@SuppressWarnings("unused")
	private String _summonName;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_summonName = readS();
	}

	@Override
	public void runImpl()
	{}

	@Override
	public String getType()
	{
		return _C__39_01_SUPERCMDSUMMONCMD;
	}
}
