package l2n.game.network.clientpackets;

/**
 * Format chS
 * c: (id) 0x39
 * h: (subid) 0x00
 * S: the character name (or maybe cmd string ?)
 */
class SuperCmdCharacterInfo extends L2GameClientPacket
{
	private static String _C__39_00_SUPERCMDCHARACTERINFO = "[C] 39:00 SuperCmdCharacterInfo";
	@SuppressWarnings("unused")
	private String _characterName;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_characterName = readS();
	}

	@Override
	public void runImpl()
	{}

	@Override
	public String getType()
	{
		return _C__39_00_SUPERCMDCHARACTERINFO;
	}
}
