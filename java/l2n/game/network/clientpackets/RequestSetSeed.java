package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.SeedProduction;

/**
 * Format: (ch) dd [ddd] d - manor id d - size [ d - seed id d - sales d - price ]
 */
public class RequestSetSeed extends L2GameClientPacket
{
	private static final String _C__D0_03_REQUESTSETSEED = "[C] D0:03 RequestSetSeed";

	private int _size;
	private int _manorId;
	private long[] _items; // _size*3

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	protected void readImpl()
	{
		_manorId = readD();
		_size = readD();
		if(_size <= 0 || _size > Config.MAX_ITEM_IN_PACKET || _size * 20 != _buf.remaining())
		{
			_size = 0;
			return;
		}
		_items = new long[_size * 3];
		for(int i = 0; i < _size; i++)
		{
			final int itemId = readD();
			final long sales = readQ();
			final long price = readQ();
			if(itemId < 1 || sales < 0 || price < 0)
			{
				_items = null;
				_size = 0;
				return;
			}

			_items[i * 3 + 0] = itemId;
			_items[i * 3 + 1] = sales;
			_items[i * 3 + 2] = price;
		}
	}

	@Override
	protected void runImpl()
	{
		if(_size < 1)
			return;

		final GArray<SeedProduction> seeds = new GArray<SeedProduction>();
		for(int i = 0; i < _size; i++)
		{
			final int id = (int) _items[i * 3 + 0];
			final long sales = _items[i * 3 + 1];
			final long price = _items[i * 3 + 2];
			if(id > 0)
			{
				final SeedProduction s = CastleManorManager.getInstance().getNewSeedProduction(id, sales, price, sales);
				seeds.add(s);
			}
		}

		CastleManager.getInstance().getCastleByIndex(_manorId).setSeedProduction(seeds, CastleManorManager.PERIOD_NEXT);
		if(Config.MANOR_SAVE_ALL_ACTIONS)
			CastleManager.getInstance().getCastleByIndex(_manorId).saveSeedData(CastleManorManager.PERIOD_NEXT);
	}

	@Override
	public String getType()
	{
		return _C__D0_03_REQUESTSETSEED;
	}
}
