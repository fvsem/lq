package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2CommandChannel;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestExAcceptJoinMPCC extends L2GameClientPacket
{
	private final static String _C__D0_07_REQUESTEXMPCCACCEPTJOIN = "[C] D0:07 RequestExMPCCAcceptJoin";
	private int _response;
	@SuppressWarnings("unused")
	private int _unk;

	/**
	 * @param buf
	 * @param client
	 *            format: chdd
	 */
	@Override
	public void readImpl()
	{
		_response = _buf.hasRemaining() ? readD() : 0;
		_unk = _buf.hasRemaining() ? readD() : 0;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(Transaction.TransactionType.CHANNEL))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED);
			activeChar.sendPacket(Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);

		transaction.cancel();

		if(!requestor.isInParty() || !activeChar.isInParty() || activeChar.getParty().isInCommandChannel())
		{
			requestor.sendPacket(new SystemMessage(SystemMessage.NO_USER_HAS_BEEN_INVITED_TO_THE_COMMAND_CHANNEL));
			return;
		}

		if(_response == 1)
		{
			if(activeChar.isTeleporting())
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_JOIN_A_COMMAND_CHANNEL_WHILE_TELEPORTING));
				requestor.sendPacket(new SystemMessage(SystemMessage.NO_USER_HAS_BEEN_INVITED_TO_THE_COMMAND_CHANNEL));
				return;
			}

			if(requestor.getParty().isInCommandChannel())
				requestor.getParty().getCommandChannel().addParty(activeChar.getParty());
			else
			{
				if(!L2CommandChannel.checkAuthority(requestor))
					return;
				final boolean haveSkill = requestor.getSkillLevel(L2CommandChannel.CLAN_IMPERIUM_ID) > 0;

				final boolean haveItem = activeChar.getInventory().getItemByItemId(L2CommandChannel.STRATEGY_GUIDE_ID) != null;

				if(!haveSkill && haveItem)
				{
					requestor.getInventory().destroyItemByItemId(L2CommandChannel.STRATEGY_GUIDE_ID, 1, false);
					requestor.sendPacket(SystemMessage.removeItems(L2CommandChannel.STRATEGY_GUIDE_ID, 1));
				}

				final L2CommandChannel channel = new L2CommandChannel(requestor);
				requestor.sendPacket(new SystemMessage(SystemMessage.THE_COMMAND_CHANNEL_HAS_BEEN_FORMED));
				channel.addParty(activeChar.getParty());
			}
		}
		else
			requestor.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DECLINED_THE_CHANNEL_INVITATION).addString(activeChar.getName()));
	}

	@Override
	public String getType()
	{
		return _C__D0_07_REQUESTEXMPCCACCEPTJOIN;
	}
}
