package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.BeginRotation;

public class StartRotating extends L2GameClientPacket
{
	private static String _C__5B_STARTROTATING = "[C] 5b StartRotating";

	private int _degree;
	private int _side;

	/**
	 * packet type id 0x5b
	 * sample
	 * 5b
	 * fb 0f 00 00 // degree (goes from 0 to 65535)
	 * 01 00 00 00 // side (01 00 00 00 = right, ff ff ff ff = left)
	 * format: cdd
	 */
	@Override
	public void readImpl()
	{
		_degree = readD();
		_side = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.setHeading(_degree);
		activeChar.broadcastPacket(new BeginRotation(activeChar, _degree, _side, 0));
	}

	@Override
	public String getType()
	{
		return _C__5B_STARTROTATING;
	}
}
