package l2n.game.network.clientpackets;

import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.network.serverpackets.ExGetOnAirShip;
import l2n.util.Location;
import l2n.util.Util;

/**
 * @author L2System
 * @date 10.12.2009
 * @time 23:27:30
 */
public class RequestExGetOffAirShip extends L2GameClientPacket
{
	private int _x;
	private int _y;
	private int _z;
	private int _id;

	@Override
	protected void readImpl()
	{
		_x = readD();
		_y = readD();
		_z = readD();
		_id = readD();
	}

	@Override
	protected void runImpl()
	{
		System.out.println("[T1:ExGetOffAirShip] x: " + _x);
		System.out.println("[T1:ExGetOffAirShip] y: " + _y);
		System.out.println("[T1:ExGetOffAirShip] z: " + _z);
		System.out.println("[T1:ExGetOffAirShip] ship ID: " + _id);

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2AirShip boat = (L2AirShip) L2VehicleManager.getInstance().getBoat(_id);
		if(boat == null || boat.isMoving)
			return;
		activeChar.setVehicle(null);

		final double angle = Util.convertHeadingToDegree(activeChar.getHeading());
		final double radian = Math.toRadians(angle - 90.0D);

		final int x = _x - (int) (100.0D * Math.sin(radian));
		final int y = _y + (int) (100.0D * Math.cos(radian));
		final int z = GeoEngine.getHeight(x, y, _z);

		activeChar.setXYZ(x, y, z);
		activeChar.broadcastPacket(new ExGetOnAirShip(activeChar, boat, new Location(x, y, z)));
	}
}
