package l2n.game.network.clientpackets;

import l2n.game.model.L2TradeList;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PrivateStoreManageListBuy;
import l2n.game.network.serverpackets.SendTradeDone;

/**
 * @author L2System Project
 * @date 28.04.2013
 * @time 0:50:28
 */
public class RequestPrivateStoreBuyManage extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isInTransaction())
			activeChar.getTransaction().cancel();
		if(activeChar.getTradeList() != null)
		{
			activeChar.getTradeList().removeAll();
			activeChar.sendPacket(SendTradeDone.Fail);
		}
		else
			activeChar.setTradeList(new L2TradeList(0));

		activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_NONE);
		activeChar.standUp();
		activeChar.broadcastCharInfo();

		if(!activeChar.checksForShop(false))
		{
			activeChar.sendActionFailed();
			return;
		}
		activeChar.sendPacket(new PrivateStoreManageListBuy(activeChar));
	}

	@Override
	public String getType()
	{
		return "[C] 99 RequestPrivateStoreManageBuy";
	}
}
