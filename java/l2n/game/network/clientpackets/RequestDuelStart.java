package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.entity.Duel;
import l2n.game.network.serverpackets.ExDuelAskStart;
import l2n.game.network.serverpackets.SystemMessage;

import java.util.logging.Logger;

public class RequestDuelStart extends L2GameClientPacket
{
	private static String _C__D0_1B_REQUESTDUELSTART = "[C] D0:1B RequestDuelStart";
	// format: (ch)Sd
	private static Logger _log = Logger.getLogger(RequestDuelStart.class.getName());
	private String _name;
	private int _duelType;

	@Override
	public void readImpl()
	{
		_name = readS();
		_duelType = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		final L2Player target = L2ObjectsStorage.getPlayer(_name);
		if(activeChar == null)
			return;

		if(target == null || target == activeChar)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THERE_IS_NO_OPPONENT_TO_RECEIVE_YOUR_CHALLENGE_FOR_A_DUEL));
			return;
		}

		// Check if duel is possible
		if(!Duel.checkIfCanDuel(activeChar, activeChar, true) || !Duel.checkIfCanDuel(activeChar, target, true))
			return;

		// Duel is a party duel
		if(_duelType == 1)
		{
			// Player must be in a party & the party leader
			if(!activeChar.isInParty() || !(activeChar.isInParty() && activeChar.getParty().isLeader(activeChar)))
			{
				activeChar.sendMessage("You have to be the leader of a party in order to request a party duel.");
				return;
			}
			// Target must be in a party
			else if(!target.isInParty())
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.SINCE_THE_PERSON_YOU_CHALLENGED_IS_NOT_CURRENTLY_IN_A_PARTY_THEY_CANNOT_DUEL_AGAINST_YOUR_PARTY));
				return;
			}
			// Target may not be of the same party
			else if(activeChar.getParty().getPartyMembers().contains(target))
			{
				activeChar.sendMessage("This player is a member of your own party.");
				return;
			}

			// Check if every player is ready for a duel
			for(final L2Player temp : activeChar.getParty().getPartyMembers())
			{
				if(!Duel.checkIfCanDuel(activeChar, temp, false))
				{
					activeChar.sendMessage("Not all the members of your party are ready for a duel.");
					return;
				}
				if(temp.getTransformationId() != 0)
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.PARTY_DUEL_CANNOT_BE_INITIATED_DUEL_TO_A_POLYMORPHED_PARTY_MEMBER));
					return;
				}
			}
			L2Player partyLeader = null; // snatch party leader of target's party
			for(final L2Player temp : target.getParty().getPartyMembers())
			{
				if(target.getParty().getPartyLeaderOID() == temp.getObjectId())
					partyLeader = temp;

				if(!Duel.checkIfCanDuel(activeChar, temp, false))
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.THE_OPPOSING_PARTY_IS_CURRENTLY_UNABLE_TO_ACCEPT_A_CHALLENGE_TO_A_DUEL));
					return;
				}
				if(temp.getTransformationId() != 0)
				{
					activeChar.sendPacket(new SystemMessage(SystemMessage.PARTY_DUEL_CANNOT_BE_INITIATED_DUEL_TO_A_POLYMORPHED_PARTY_MEMBER));
					return;
				}
			}

			// Никогда не должно случатся, если случилось то кто-то сломал L2Party
			if(partyLeader == null)
			{
				_log.warning("Some asshole has broken L2Party. Can't get party leader.");
				return;
			}

			// Send request to target's party leader
			if(!partyLeader.isInTransaction())
			{
				new Transaction(TransactionType.DUEL, activeChar, partyLeader, 10000);
				partyLeader.sendPacket(new ExDuelAskStart(activeChar.getName(), _duelType));

				if(Config.DEBUG)
					_log.fine(activeChar.getName() + " requested a duel with " + partyLeader.getName());

				SystemMessage msg = new SystemMessage(SystemMessage.S1S_PARTY_HAS_BEEN_CHALLENGED_TO_A_DUEL);
				msg.addString(partyLeader.getName());
				activeChar.sendPacket(msg);

				msg = new SystemMessage(SystemMessage.S1S_PARTY_HAS_CHALLENGED_YOUR_PARTY_TO_A_DUEL);
				msg.addString(activeChar.getName());
				target.sendPacket(msg);
			}
			else
			{
				final SystemMessage msg = new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER);
				msg.addString(partyLeader.getName());
				activeChar.sendPacket(msg);
			}
		}
		else if(!target.isInTransaction())
		{
			final Transaction transaction = new Transaction(TransactionType.DUEL, activeChar, target, 10000L);
			activeChar.setTransaction(transaction);
			target.setTransaction(transaction);

			SystemMessage msg = new SystemMessage(SystemMessage.S1_HAS_BEEN_CHALLENGED_TO_A_DUEL);
			msg.addString(target.getName());
			activeChar.sendPacket(msg);

			msg = new SystemMessage(SystemMessage.S1_HAS_CHALLENGED_YOU_TO_A_DUEL);
			msg.addString(activeChar.getName());
			target.sendPacket(msg);
			target.sendPacket(new ExDuelAskStart(activeChar.getName(), _duelType));

			if(Config.DEBUG)
				_log.fine(activeChar.getName() + " requested a duel with " + target.getName());
		}
		else
		{
			final SystemMessage msg = new SystemMessage(SystemMessage.S1_IS_BUSY_PLEASE_TRY_AGAIN_LATER);
			msg.addString(target.getName());
			activeChar.sendPacket(msg);
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_1B_REQUESTDUELSTART;
	}
}
