package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.AllyInfo;

/**
 * This class ...
 * 
 * @version $Revision: 1479 $ $Date: 2005-11-09 02:47:42 +0300 (Ср, 09 ноя 2005) $
 */
public class RequestAllyInfo extends L2GameClientPacket
{
	private static String _C__2E_REQUESTALLYINFO = "[C] 2E RequestAllyInfo";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		sendPacket(new AllyInfo(activeChar));
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.clientpackets.ClientBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__2E_REQUESTALLYINFO;
	}
}
