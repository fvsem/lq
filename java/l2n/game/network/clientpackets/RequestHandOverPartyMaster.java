package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestHandOverPartyMaster extends L2GameClientPacket
{
	private final static String _C__0C_REQUESTHANDOVERPARTYMASTER = "[C] 0c RequestHandOverPartyMaster";

	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isInParty() && activeChar.getParty().isLeader(activeChar))
			activeChar.getParty().changePartyLeader(_name);
	}

	@Override
	public String getType()
	{
		return _C__0C_REQUESTHANDOVERPARTYMASTER;
	}
}
