package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.PetitionManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.GmListTable;

/**
 * Format: (c) Sd
 */
public class RequestPetition extends L2GameClientPacket
{
	// format: cSd
	private String _content;
	private int _type; // 1 = on : 0 = off;

	/**
	 * @param buf
	 * @param client
	 */
	@Override
	public void readImpl()
	{
		_content = readS();
		_type = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(GmListTable.getAllVisibleGMs().isEmpty())
		{
			activeChar.sendPacket(Msg.THERE_ARE_NOT_ANY_GMS_THAT_ARE_PROVIDING_CUSTOMER_SERVICE_CURRENTLY);
			return;
		}

		if(!PetitionManager.getInstance().isPetitioningAllowed())
		{
			activeChar.sendPacket(Msg.CANNOT_CONNECT_TO_PETITION_SERVER);
			return;
		}

		if(PetitionManager.getInstance().isPlayerPetitionPending(activeChar))
		{
			activeChar.sendPacket(Msg.ALREADY_APPLIED_FOR_PETITION);
			return;
		}

		if(PetitionManager.getInstance().getPendingPetitionCount() == Config.MAX_PETITIONS_PENDING)
		{
			activeChar.sendPacket(Msg.THE_PETITION_SYSTEM_IS_CURRENTLY_UNAVAILABLE_PLEASE_TRY_AGAIN_LATER);
			return;
		}

		final int totalPetitions = PetitionManager.getInstance().getPlayerTotalPetitionCount(activeChar) + 1;

		if(totalPetitions > Config.MAX_PETITIONS_PER_PLAYER)
		{
			SystemMessage sm = new SystemMessage(SystemMessage.WE_HAVE_RECEIVED_S1_PETITIONS_FROM_YOU_TODAY_AND_THAT_IS_THE_MAXIMUM_THAT_YOU_CAN_SUBMIT_IN_ONE_DAY_YOU_CANNOT_SUBMIT_ANY_MORE_PETITIONS);
			sm.addNumber(totalPetitions);
			activeChar.sendPacket(sm);
			sm = null;
			return;
		}

		if(_content.length() > 255)
		{
			activeChar.sendPacket(Msg.PETITIONS_CANNOT_EXCEED_255_CHARACTERS);
			return;
		}

		final int petitionId = PetitionManager.getInstance().submitPetition(activeChar, _content, _type);

		SystemMessage sm = new SystemMessage(SystemMessage.PETITION_APPLICATION_ACCEPTED_RECEIPT_NO_IS_S1);
		sm.addNumber(petitionId);
		activeChar.sendPacket(sm);

		sm = new SystemMessage(SystemMessage.YOU_HAVE_SUBMITTED_S1_PETITIONS_YOU_MAY_SUBMIT_S2_MORE_PETITIONS_TODAY);
		sm.addNumber(totalPetitions);
		sm.addNumber(Config.MAX_PETITIONS_PER_PLAYER - totalPetitions);
		activeChar.sendPacket(sm);

		sm = new SystemMessage(SystemMessage.THERE_ARE_S1_PETITIONS_PENDING);
		sm.addNumber(PetitionManager.getInstance().getPendingPetitionCount());
		activeChar.sendPacket(sm);
		sm = null;

	}
}
