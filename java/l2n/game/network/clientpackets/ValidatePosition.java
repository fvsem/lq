package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.Universe;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.util.Location;

public class ValidatePosition extends L2GameClientPacket
{
	private static final int MIN_Z = -30000;
	private static final int MAX_Z = 30000;

	private int _boatObjectId;
	private final Location _loc;
	private Location _lastClientPosition;
	private Location _lastServerPosition;

	public ValidatePosition()
	{
		_loc = new Location();
	}

	/**
	 * packet type id 0x48 format: cddddd
	 */
	@Override
	public void readImpl()
	{
		_loc.x = readD();
		_loc.y = readD();
		_loc.z = readD();
		_loc.h = readD();
		_boatObjectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		if(player.isTeleporting() || player.inObserverMode())
			return;

		_lastClientPosition = player.getLastClientPosition();
		_lastServerPosition = player.getLastServerPosition();

		if(_lastClientPosition == null)
			_lastClientPosition = player.getLoc();
		if(_lastServerPosition == null)
			_lastServerPosition = player.getLoc();

		if(player.getX() == 0 && player.getY() == 0 && player.getZ() == 0)
		{
			correctPosition(player);
			return;
		}

		if(player.isInFlyingTransform())
		{
			// В летающей трансформе нельзя находиться на территории Aden
			if(_loc.x > -166168)
			{
				player.stopTransformation();
				return;
			}

			// В летающей трансформе нельзя летать ниже, чем 0, и выше, чем 6000
			if(_loc.z <= 0 || _loc.z >= 6000)
			{
				player.teleToLocation(player.getLoc().setZ(Math.min(5950, Math.max(50, _loc.z))));
				return;
			}
		}
		player.checkTerritoryFlag();

		double diff = player.getDistance(_loc.x, _loc.y);
		int dz = Math.abs(_loc.z - player.getZ());
		int h = _lastServerPosition.z - player.getZ();

		if(Config.ACTIVATE_POSITION_RECORDER && !player.isFlying() && Universe.getInstance().shouldLog(player.getObjectId()))
			Universe.getInstance().registerHeight(player.getX(), player.getY(), _loc.z);

		if(player.isInVehicle())
		{
			player.setLastClientPosition(_loc.setH(player.getHeading()));
			player.setLastServerPosition(player.getLoc());
			return;
		}

		// Если мы уже падаем, то отключаем все валидейты
		if(player.isFalling())
		{
			diff = 0;
			dz = 0;
			h = 0;
		}

		if(h >= 256) // Пока падаем, высоту не корректируем
			player.falling(h);
		else if(!player.isInWater() && dz >= (player.isFlying() ? 1024 : 512))
		{
			if(player.getIncorrectValidateCount() >= 3)
				player.teleToClosestTown();
			else
			{
				player.teleToLocation(player.getLoc());
				player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
			}
		}
		else if(dz >= (player.isFlying() ? 512 : 256))
			player.validateLocation(0);
		else if(_loc.z < MIN_Z || _loc.z > MAX_Z)
		{
			if(player.getIncorrectValidateCount() >= 3)
				player.teleToClosestTown();
			else
			{
				correctPosition(player);
				player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
			}
		}
		else if(diff > 1024)
		{
			if(player.getIncorrectValidateCount() >= 3)
				player.teleToClosestTown();
			else
			{
				player.teleToLocation(player.getLoc());
				player.setIncorrectValidateCount(player.getIncorrectValidateCount() + 1);
			}
		}
		else if(diff > 256)
			// TODO реализовать NetPing и вычислять предельное отклонение исходя из пинга по формуле: 16 + (ping * activeChar.getMoveSpeed()) / 1000
			player.validateLocation(1);
		else
			player.setIncorrectValidateCount(0);

		player.checkWaterState();

		if(player.getPet() != null && !player.getPet().isInRange())
			player.getPet().teleportToOwner();

		player.setLastClientPosition(_loc.setH(player.getHeading()));
		player.setLastServerPosition(player.getLoc());

		if(player.isTerritoryFlagEquipped() && TerritorySiege.isInProgress())
			TerritorySiege.setWardLoc(player.getActiveWeaponInstance().getItemId() - 13559, player.getLoc());
	}

	private void correctPosition(final L2Player player)
	{
		if(player.isGM())
		{
			player.sendMessage("Correcting position...");
			developerLogs(player, player.getLoc());
		}

		if(_lastServerPosition.x != 0 && _lastServerPosition.y != 0 && _lastServerPosition.z != 0)
		{
			if(GeoEngine.getNSWE(_lastServerPosition.x, _lastServerPosition.y, _lastServerPosition.z) == GeoEngine.NSWE_ALL)
				player.teleToLocation(_lastServerPosition);
			else
				player.teleToClosestTown();
		}
		else if(_lastClientPosition.x != 0 && _lastClientPosition.y != 0 && _lastClientPosition.z != 0)
		{
			if(GeoEngine.getNSWE(_lastServerPosition.x, _lastServerPosition.y, _lastServerPosition.z) == GeoEngine.NSWE_ALL)
				player.teleToLocation(_lastClientPosition);
			else
				player.teleToClosestTown();
		}
		else
			player.teleToClosestTown();
	}

	private void developerLogs(final L2Player activeChar, final Location loc)
	{
		_log.info("client pos: " + _loc.toString());
		_log.info("server pos: " + loc.toString());

		activeChar.sendMessage("client pos: " + _loc.toString());
		activeChar.sendMessage("server pos: " + loc.toString());
	}

	@Override
	public String getType()
	{
		return "[C] 48 ValidatePosition";
	}
}
