package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.items.MailParcelController;
import l2n.game.network.serverpackets.ExShowSentPostList;

/**
 * @author L2System
 * @date 29.07.2010
 * @time 16:10:54
 */
public class RequestCancelSentPost extends L2GameClientPacket
{
	private int postId;

	@Override
	protected void readImpl()
	{
		postId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(cha.isOutOfControl())
		{
			cha.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}

		if(!cha.isInZonePeace())
		{
			cha.sendPacket(Msg.CANNOT_USE_MAIL_OUTSIDE_PEACE_ZONE);
			return;
		}

		MailParcelController.getInstance().returnLetter(postId, 2);

		// Notify players the mail has been canceled
		cha.sendPacket(Msg.MAIL_SUCCESSFULLY_CANCELLED, new ExShowSentPostList(cha.getObjectId()));
	}
}
