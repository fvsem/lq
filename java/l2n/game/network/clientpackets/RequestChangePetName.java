package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestChangePetName extends L2GameClientPacket
{
	private static String REQUESTCHANGEPETNAME__C__93 = "[C] 93 RequestChangePetName";

	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		final L2Summon pet = cha.getPet();
		if(pet != null && (pet.getName() == null || pet.getName().isEmpty() || pet.getName().equalsIgnoreCase(pet.getTemplate().name)))
		{
			if(_name.length() < 1 || _name.length() > 8)
			{
				sendPacket(new SystemMessage(SystemMessage.YOUR_PETS_NAME_CAN_BE_UP_TO_8_CHARACTERS));
				return;
			}
			pet.setName(_name);
			pet.broadcastCharInfo();

			if(pet.isPet())
			{
				final L2PetInstance _pet = (L2PetInstance) pet;
				final L2ItemInstance controlItem = _pet.getControlItem();
				if(controlItem != null)
				{
					controlItem.setCustomType2(1);
					controlItem.setPriceToSell(0); // Костыль, иначе CustomType2 = 1 не пишется в базу
					controlItem.updateDatabase();
					_pet.updateControlItem();
				}
			}

		}
	}

	@Override
	public String getType()
	{
		return REQUESTCHANGEPETNAME__C__93;
	}
}
