package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * Appearing Packet Handler
 * <p>
 * <p>
 * 0000: 30
 * <p>
 * <p>
 */
public class Appearing extends L2GameClientPacket
{
	private final static String _C__3A_APPEARING = "[C] 3A Appearing";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isLogoutStarted())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getObserverMode() == 1)
		{
			activeChar.appearObserverMode();
			return;
		}

		if(activeChar.getObserverMode() == 2)
		{
			activeChar.returnFromObserverMode();
			return;
		}

		if(!activeChar.isTeleporting())
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.onTeleported();
	}

	@Override
	public String getType()
	{
		return _C__3A_APPEARING;
	}
}
