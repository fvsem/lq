package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.EnchantResult;

public class RequestExCancelEnchantItem extends L2GameClientPacket
{
	private static final String _C__D0_81_REQUESTEXCANCELENCHANTITEM = "[C] D0 51 RequestExCancelEnchantItem";

	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
		{
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(EnchantResult.CANCEL);
			activeChar.sendActionFailed();
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_81_REQUESTEXCANCELENCHANTITEM;
	}
}
