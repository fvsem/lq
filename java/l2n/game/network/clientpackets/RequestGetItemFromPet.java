package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.model.items.PetInventory;
import l2n.game.network.serverpackets.PetItemList;
import l2n.util.Log;

import java.util.logging.Logger;

public class RequestGetItemFromPet extends L2GameClientPacket
{
	private static final String REQUESTGETITEMFROMPET__C__8C = "[C] 8C RequestGetItemFromPet";
	private static final Logger _log = Logger.getLogger(RequestGetItemFromPet.class.getName());

	private int _objectId;
	private long _amount;

	@Override
	public void readImpl()
	{
		_objectId = readD();
		_amount = readQ();
		readD();// = 0 for most trades
	}

	@Override
	public void runImpl()
	{
		if(_amount < 1)
			return;
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Summon summon = activeChar.getPet();
		if(summon == null || !summon.isPet())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2PetInstance pet = (L2PetInstance) summon;
		final PetInventory petInventory = pet.getInventory();
		final PcInventory playerInventory = activeChar.getInventory();

		final L2ItemInstance petItem = petInventory.getItemByObjectId(_objectId);
		if(petItem == null)
		{
			_log.warning("item requested from pet, but its not there.");
			return;
		}

		if(petItem.isEquipped())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_amount > petItem.getCount())
			_amount = petItem.getCount();

		final long finalLoad = petItem.getItem().getWeight() * _amount;
		if(!activeChar.getInventory().validateWeight(finalLoad))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
			return;
		}

		final L2ItemInstance item = petInventory.dropItem(_objectId, _amount, false);
		item.setCustomFlags(item.getCustomFlags() & ~L2ItemInstance.FLAG_PET_EQUIPPED, true);
		playerInventory.addItem(item);

		activeChar.sendChanges();
		sendPacket(new PetItemList(pet));
		pet.broadcastCharInfo();

		Log.LogItem(activeChar, activeChar.getPet(), Log.GetItemFromPet, petItem);
	}

	@Override
	public String getType()
	{
		return REQUESTGETITEMFROMPET__C__8C;
	}
}
