package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExReplySentPost;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:48:53
 */
public class RequestRequestSentPost extends L2GameClientPacket
{
	private int postId;

	@Override
	public void readImpl()
	{
		postId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(!cha.isInZonePeace())
		{
			cha.sendPacket(Msg.CANNOT_USE_MAIL_OUTSIDE_PEACE_ZONE);
			return;
		}
		cha.sendPacket(new ExReplySentPost(cha, postId));
	}
}
