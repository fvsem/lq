package l2n.game.network.clientpackets;

import java.util.logging.Level;

/**
 * Format: (ch)
 * just a trigger
 */
public class RequestExFishRanking extends L2GameClientPacket
{
	private static final String _C__D0_18_REQUESTEXFISHRANKING = "[C] D0:18 RequestExFishRanking";

	@Override
	public void readImpl()
	{
		// trigger
	}

	/**
	 * @see l2n.game.clientpackets.ClientBasePacket#runImpl()
	 */
	@Override
	public void runImpl()
	{
		_log.log(Level.INFO, "C5: RequestExFishRanking");
	}

	/**
	 * @see l2n.game.BasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__D0_18_REQUESTEXFISHRANKING;
	}
}
