package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.PetitionManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.GmListTable;

public class RequestPetitionCancel extends L2GameClientPacket
{
	private final static String _C__8A_REQUEST_PETITIONCANCEL = "[C] 8A RequestPetitionCancel";

	// private String _text;

	/**
	 * @param buf
	 * @param client
	 *            format: cS
	 */
	@Override
	public void readImpl()
	{
		// _text = readS(4096);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(PetitionManager.getInstance().isPlayerInConsultation(activeChar))
		{
			if(activeChar.isGM())
				PetitionManager.getInstance().endActivePetition(activeChar);
			else
				activeChar.sendPacket(Msg.PETITION_UNDER_PROCESS);
		}
		else if(PetitionManager.getInstance().isPlayerPetitionPending(activeChar))
		{
			if(PetitionManager.getInstance().cancelActivePetition(activeChar))
			{
				final int numRemaining = Config.MAX_PETITIONS_PER_PLAYER - PetitionManager.getInstance().getPlayerTotalPetitionCount(activeChar);
				SystemMessage sm = new SystemMessage(SystemMessage.THE_PETITION_WAS_CANCELED_YOU_MAY_SUBMIT_S1_MORE_PETITIONS_TODAY);
				sm.addString(String.valueOf(numRemaining));
				activeChar.sendPacket(sm);
				sm = null;

				// Notify all GMs that the player's pending petition has been cancelled.
				final String msgContent = activeChar.getName() + " has canceled a pending petition.";
				GmListTable.broadcastToGMs(new CreatureSay(0, Say2C.GM, "Petition System", msgContent));
			}
			else
				activeChar.sendPacket(Msg.FAILED_TO_CANCEL_PETITION_PLEASE_TRY_AGAIN_LATER);
		}
		else
			activeChar.sendPacket(Msg.YOU_HAVE_NOT_SUBMITTED_A_PETITION);
	}

	@Override
	public String getType()
	{
		return _C__8A_REQUEST_PETITIONCANCEL;
	}
}
