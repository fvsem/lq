package l2n.game.network.clientpackets;

import l2n.game.instancemanager.games.HandysBlockCheckerManager;
import l2n.game.model.actor.L2Player;

import java.util.logging.Logger;

/**
 * @author L2System
 * @date 20.12.2010
 * @time 14:56:50
 */
public class RequestExCubeGameReadyAnswer extends L2GameClientPacket
{
	private static final String _C__D0_5C_REQUESTEXCUBEGAMEREADYANSWER = "[C] D0:5C RequestExCubeGameReadyAnswer";
	private static final Logger _log = Logger.getLogger(RequestExCubeGameReadyAnswer.class.getName());

	int _arena;
	int _answer;

	@Override
	protected void readImpl()
	{
		// client sends -1,0,1,2 for arena parameter
		_arena = readD() + 1;
		// client sends 1 if clicked confirm on not clicked, 0 if clicked cancel
		_answer = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player != null)
			switch (_answer)
			{
				case 0:
				{
					// Answer - No
					break;
				}
				case 1:
				{
					HandysBlockCheckerManager.getInstance().increaseArenaVotes(_arena);
					break;
				}
				default:
					_log.warning("Unknown Cube Game Answer ID: " + _answer);
					break;
			}
	}

	@Override
	public String getType()
	{
		return _C__D0_5C_REQUESTEXCUBEGAMEREADYANSWER;
	}
}
