package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.network.serverpackets.SendStatus;

/**
 * @author L2System Project
 * @date 04.10.2010
 * @time 4:17:21
 */
public final class RequestStatus extends L2GameClientPacket
{
	@Override
	protected void readImpl() throws Exception
	{
		readD();
	}

	@Override
	protected void runImpl() throws Exception
	{
		if(Config.USE_OFFEMULATION)
			getClient().close(new SendStatus());
	}

	@Override
	public String getType()
	{
		return "[C] 00 L2TopRequest";
	}
}
