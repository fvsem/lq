package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.EnchantResult;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.IllegalPlayerAction;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.logging.Logger;

/**
 * @author L2System
 * @date 10.03.2010
 * @time 2:48:40
 */
public class RequestEnchantItem extends AbstractEnchantPacket
{
	protected static Logger _log = Logger.getLogger(RequestEnchantItem.class.getName());

	private final static String _C__D0_81_REQUESTENCHANTITEM = "[C] D0 51 RequestEnchantItem";
	// Format: cd
	private int _objectId = 0;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _objectId == 0)
			return;

		if(activeChar.isOutOfControl() || activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2ItemInstance itemToEnchant = activeChar.getInventory().getItemByObjectId(_objectId);
		L2ItemInstance scroll = activeChar.getActiveEnchantItem();
		L2ItemInstance support = activeChar.getActiveEnchantSupportItem();

		if(itemToEnchant == null || scroll == null || !activeChar.isOnline())
		{
			activeChar.setActiveEnchantItem(null);
			activeChar.sendActionFailed();
			return;
		}

		// template for scroll
		final EnchantScroll scrollTemplate = getEnchantScroll(scroll);

		// scroll not found in list
		if(scrollTemplate == null)
			return;

		// template for support item, if exist
		EnchantItem supportTemplate = null;
		if(support != null)
			supportTemplate = getSupportItem(support);

		// first validation check
		if(!scrollTemplate.isValid(activeChar, itemToEnchant, supportTemplate) || !isEnchantable(itemToEnchant, false) && itemToEnchant.getItemId() != 13539)
		{
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, EnchantResult.CANCEL);
			return;
		}

		Log.add(activeChar.getName() + "|Trying to enchant|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getEnchantLevel() + "|" + itemToEnchant.getObjectId(), "enchants");

		// fast auto-enchant cheat check
		if(activeChar.getActiveEnchantTimestamp() == 0 || System.currentTimeMillis() - activeChar.getActiveEnchantTimestamp() < 1000)
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestEnchantItem[90]", "Player " + activeChar.getName() + " use autoenchant program ", IllegalPlayerAction.WARNING);
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(EnchantResult.CANCEL);
			return;
		}

		// attempting to destroy scroll
		scroll = activeChar.getInventory().destroyItem(scroll.getObjectId(), 1, true);
		// tries enchant without scrolls
		if(scroll == null)
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestEnchantItem[102]", "Player " + activeChar.getName() + " tried to enchant with a scroll he doesn't have", IllegalPlayerAction.WARNING);
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT, EnchantResult.CANCEL);
			return;
		}

		// attempting to destroy support if exist
		if(support != null)
		{
			support = activeChar.getInventory().destroyItem(support.getObjectId(), 1, true);
			if(support == null)
			{
				Util.handleIllegalPlayerAction(activeChar, "RequestEnchantItem[115]", "Player " + activeChar.getName() + " tried to enchant with a support item he doesn't have", IllegalPlayerAction.WARNING);
				activeChar.setActiveEnchantItem(null);
				activeChar.sendPacket(Msg.INCORRECT_ITEM_COUNT, EnchantResult.CANCEL);
				return;
			}
		}

		final double chance = scrollTemplate.getChance(activeChar, itemToEnchant, supportTemplate);
		// last validation check
		if(itemToEnchant.getOwnerId() != activeChar.getObjectId() || !isEnchantable(itemToEnchant, false) && itemToEnchant.getItemId() != 13539 || chance < 0)
		{
			activeChar.setActiveEnchantItem(null);
			activeChar.sendPacket(Msg.INAPPROPRIATE_ENCHANT_CONDITIONS, EnchantResult.CANCEL);
			return;
		}

		// success
		if(chance >= 100 || Rnd.chance(chance))
		{
			itemToEnchant.setEnchantLevel(itemToEnchant.getEnchantLevel() + 1);
			itemToEnchant.updateDatabase();

			activeChar.sendPacket(new InventoryUpdate(itemToEnchant, L2ItemInstance.MODIFIED));

			Log.add(activeChar.getName() + "|Successfully enchanted|" + itemToEnchant.getItemId() + "|to+" + itemToEnchant.getEnchantLevel() + "|" + chance, "enchants");
			Log.LogItem(activeChar, Log.EnchantItem, itemToEnchant);
			activeChar.sendPacket(EnchantResult.SUCESS);

			// announce the success
			if(Config.ALT_ENABLE_FIREWORKS)
			{
				final int minEnchantAnnounce = itemToEnchant.getItem().isArmor() ? 6 : 7;
				final int maxEnchantAnnounce = itemToEnchant.getItem().isArmor() ? 0 : 15;
				if(itemToEnchant.getEnchantLevel() == minEnchantAnnounce || itemToEnchant.getEnchantLevel() == maxEnchantAnnounce)
				{
					final SystemMessage sm = new SystemMessage(SystemMessage.C1_SUCCESSFULY_ENCHANTED_A_S2_S3);
					sm.addName(activeChar);
					sm.addNumber(itemToEnchant.getEnchantLevel());
					sm.addItemName(itemToEnchant.getItemId());
					activeChar.broadcastPacket(sm);

					final L2Skill skill = SkillTable.FrequentSkill.FIREWORK.getSkill();
					activeChar.broadcastPacket(new MagicSkillUse(activeChar, activeChar, skill.getId(), skill.getLevel(), skill.getHitTime(), skill.getReuseDelay()));
				}
			}
		}
		else if(scrollTemplate.isSafe())
		{
			Log.add(activeChar.getName() + "|Failed to enchant|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getEnchantLevel() + "|" + chance, "enchants");

			// safe enchant - remain old value
			// need retail message
			activeChar.sendPacket(EnchantResult.SAFE_SCROLL_FAILED);
		}
		else
		// fail
		{
			Log.add(activeChar.getName() + "|Failed to enchant|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getEnchantLevel() + "|" + chance, "enchants");

			int type = 1;
			if(scrollTemplate.isBlessed() && !scrollTemplate.isCrystal())
				type = BLESSED_SCROLL;
			else if(!scrollTemplate.isBlessed() && scrollTemplate.isCrystal())
				type = CRYSTAL_SCROLL;
			else
				type = NORMAL_SCROLL;

			failEnchant(activeChar, itemToEnchant, type);
		}

		activeChar.setActiveEnchantItem(null);
		activeChar.sendChanges();
		activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__D0_81_REQUESTENCHANTITEM;
	}
}
