package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;
import l2n.game.model.L2Manor;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ManorManagerInstance;
import l2n.game.network.serverpackets.StatusUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

/**
 * Format: (ch) d [dddd]
 * d: size
 * [
 * d obj id
 * d item id
 * d manor id
 * d count
 * ]
 */
public class RequestProcureCropList extends L2GameClientPacket
{
	private static final String _C__D0_02_REQUESTPROCURECROPLIST = "[C] D0:02 RequestProcureCropList";
	private int _size;
	private long[] _items; // count*4

	@Override
	protected void readImpl()
	{
		_size = readD();
		if(_size <= 0 || _size > Config.MAX_ITEM_IN_PACKET || _size * 20 != _buf.remaining())
		{
			_size = 0;
			return;
		}
		_items = new long[_size * 4];
		for(int i = 0; i < _size; i++)
		{
			final int objId = readD();
			_items[i * 4 + 0] = objId;
			final int itemId = readD();
			_items[i * 4 + 1] = itemId;
			final int manorId = readD();
			_items[i * 4 + 2] = manorId;
			final long count = readQ();
			_items[i * 4 + 3] = count;
			if(objId < 1 || itemId < 1 || manorId < 0 || count < 0)
			{
				_size = 0;
				_items = null;
				return;
			}
		}
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		if(_size < 1)
		{
			player.sendActionFailed();
			return;
		}

		L2Object target = player.getTarget();

		if(!(target instanceof L2ManorManagerInstance))
			target = player.getLastNpc();

		if(!player.isGM() && (target == null || !(target instanceof L2ManorManagerInstance) || !player.isInRange(target, L2Character.INTERACTION_DISTANCE)))
			return;

		final L2ManorManagerInstance manorManager = (L2ManorManagerInstance) target;

		final int currentManorId = manorManager.getCastle().getId();

		// Calculate summary values
		int slots = 0;
		int weight = 0;

		for(int i = 0; i < _size; i++)
		{
			final int itemId = (int) _items[i * 4 + 1];
			final int manorId = (int) _items[i * 4 + 2];
			final long count = _items[i * 4 + 3];

			if(itemId == 0 || manorId == 0 || count == 0)
				continue;
			if(count < 1)
				continue;
			if(count > Integer.MAX_VALUE)
			{
				sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
				return;
			}

			try
			{
				final CropProcure crop = CastleManager.getInstance().getCastleByIndex(manorId).getCrop(itemId, CastleManorManager.PERIOD_CURRENT);
				final int rewardItemId = L2Manor.getInstance().getRewardItem(itemId, crop.getReward());
				final L2Item template = ItemTable.getInstance().getTemplate(rewardItemId);
				weight += count * template.getWeight();

				if(!template.isStackable())
					slots += count;
				else if(player.getInventory().getItemByItemId(itemId) == null)
					slots++;
			}
			catch(final NullPointerException e)
			{
				continue;
			}
		}

		if(!player.getInventory().validateWeight(weight))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
			return;
		}

		if(!player.getInventory().validateCapacity(slots))
		{
			sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
			return;
		}

		// Proceed the purchase
		for(int i = 0; i < _size; i++)
		{
			final int objId = (int) _items[i * 4 + 0];
			final int cropId = (int) _items[i * 4 + 1];
			final int manorId = (int) _items[i * 4 + 2];
			final long count = _items[i * 4 + 3];

			if(objId == 0 || cropId == 0 || manorId == 0 || count == 0)
				continue;

			if(count < 1)
				continue;

			CropProcure crop = null;

			try
			{
				crop = CastleManager.getInstance().getCastleByIndex(manorId).getCrop(cropId, CastleManorManager.PERIOD_CURRENT);
			}
			catch(final NullPointerException e)
			{
				continue;
			}
			if(crop == null || crop.getId() == 0 || crop.getPrice() == 0)
				continue;

			long fee = 0; // fee for selling to other manors

			final int rewardItem = L2Manor.getInstance().getRewardItem(cropId, crop.getReward());

			if(count > crop.getAmount())
				continue;

			final long sellPrice = count * crop.getPrice();
			final int rewardPrice = ItemTable.getInstance().getTemplate(rewardItem).getReferencePrice();

			if(rewardPrice == 0)
				continue;

			final long rewardItemCount = sellPrice / rewardPrice;
			if(rewardItemCount < 1)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.FAILED_IN_TRADING_S2_OF_CROP_S1);
				sm.addItemName(cropId);
				sm.addNumber(count);
				player.sendPacket(sm);
				continue;
			}

			if(manorId != currentManorId)
				fee = sellPrice * 5 / 100; // 5% fee for selling to other manor

			if(player.getInventory().getAdena() < fee)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.FAILED_IN_TRADING_S2_OF_CROP_S1);
				sm.addItemName(cropId);
				sm.addNumber(count);
				player.sendPacket(sm);
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
				continue;
			}

			// Add item to Inventory and adjust update packet
			L2ItemInstance itemDel = null;
			L2ItemInstance itemAdd = null;
			if(player.getInventory().getItemByObjectId(objId) == null)
				continue;

			// check if player have correct items count
			final L2ItemInstance item = player.getInventory().getItemByObjectId(objId);
			if(item.getCount() < count)
				continue;

			itemDel = player.getInventory().destroyItem(objId, count, true);
			if(itemDel == null)
				continue;

			if(fee > 0)
				player.getInventory().reduceAdena(fee);
			crop.setAmount(crop.getAmount() - count);
			if(Config.MANOR_SAVE_ALL_ACTIONS)
				CastleManager.getInstance().getCastleByIndex(manorId).updateCrop(crop.getId(), crop.getAmount(), CastleManorManager.PERIOD_CURRENT);

			itemAdd = player.getInventory().addItem(rewardItem, rewardItemCount, 0, "Manor: RequestProcureCropList");
			if(itemAdd == null)
				continue;

			// Send System Messages
			SystemMessage sm = new SystemMessage(SystemMessage.TRADED_S2_OF_CROP_S1);
			sm.addItemName(cropId);
			sm.addNumber(count);
			player.sendPacket(sm);

			if(fee > 0)
			{
				sm = new SystemMessage(SystemMessage.S1_ADENA_HAS_BEEN_PAID_FOR_PURCHASING_FEES);
				sm.addNumber(fee);
				player.sendPacket(sm);
			}

			sm = new SystemMessage(SystemMessage.S2_S1_HAS_DISAPPEARED);
			sm.addItemName(cropId);
			sm.addNumber(count);
			player.sendPacket(sm);

			if(fee > 0)
			{
				sm = new SystemMessage(SystemMessage.S1_ADENA_DISAPPEARED);
				sm.addNumber(fee);
				player.sendPacket(sm);
			}

			sm = new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S);
			sm.addItemName(rewardItem);
			sm.addNumber(rewardItemCount);
			player.sendPacket(sm);
		}

		player.sendStatusUpdate(false, StatusUpdate.CUR_LOAD);
	}

	@Override
	public String getType()
	{
		return _C__D0_02_REQUESTPROCURECROPLIST;
	}
}
