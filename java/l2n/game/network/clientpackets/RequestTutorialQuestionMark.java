package l2n.game.network.clientpackets;

import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;

public class RequestTutorialQuestionMark extends L2GameClientPacket
{
	private static String _C__87_REQUESTTUTORIALQMARKPRESSED = "[C] 87 RequestTutorialQuestionMarkPressed";
	// format: cd
	int _number = 0;

	@Override
	public void readImpl()
	{
		_number = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final Quest q = QuestManager.getQuest(255);
		if(q != null)
			player.processQuestEvent(q.getName(), "QM" + _number, null);
	}

	@Override
	public String getType()
	{
		return _C__87_REQUESTTUTORIALQMARKPRESSED;
	}
}
