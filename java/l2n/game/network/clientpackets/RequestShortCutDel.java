package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * This class ...
 * 
 * @version $Revision: 1.3.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestShortCutDel extends L2GameClientPacket
{
	private static String _C__3F_REQUESTSHORTCUTDEL = "[C] 3F RequestShortCutDel";

	private int _slot;
	private int _page;

	/**
	 * packet type id 0x3F format: cd
	 * 
	 * @param rawPacket
	 */
	@Override
	public void readImpl()
	{
		final int id = readD();
		_slot = id % 12;
		_page = id / 12;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.deleteShortCut(_slot, _page);
	}

	/*
	 * (non-Javadoc)
	 * @see l2n.game.clientpackets.ClientBasePacket#getType()
	 */
	@Override
	public String getType()
	{
		return _C__3F_REQUESTSHORTCUTDEL;
	}
}
