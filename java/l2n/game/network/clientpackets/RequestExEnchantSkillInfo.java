package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExEnchantSkillInfo;
import l2n.game.tables.SkillTable;

public class RequestExEnchantSkillInfo extends L2GameClientPacket
{
	private static String _C__D0_0E_REQUESTEXENCHANTSKILLINFO = "[C] D0:0E RequestExEnchantSkillInfo";

	private int _skillId;
	private int _skillLvl;

	@Override
	public void readImpl()
	{
		_skillId = readD();
		_skillLvl = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendMessage("You must leave transformation mode first.");
			return;
		}

		if(activeChar.getLevel() < 76)
		{
			sendPacket(Msg.YOU_DONT_MEET_SKILL_LEVEL_REQUIREMENTS);
			return;
		}

		if(activeChar.getClassId().getLevel() < 4)
		{
			sendPacket(Msg.NOT_COMPLETED_QUEST_FOR_SKILL_ACQUISITION);
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(_skillId, _skillLvl);
		if(skill == null || skill.getId() != _skillId)
		{
			_log.warning("RequestExEnchantSkill: skillId " + _skillId + " level " + _skillLvl + " not found in Datapack.");
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		activeChar.sendPacket(new ExEnchantSkillInfo(_skillId, _skillLvl));
	}

	@Override
	public String getType()
	{
		return _C__D0_0E_REQUESTEXENCHANTSKILLINFO;
	}
}
