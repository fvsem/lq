package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2TradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ChangeWaitType;
import l2n.game.network.serverpackets.PrivateStoreManageListBuy;
import l2n.game.network.serverpackets.PrivateStoreMsgBuy;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;

import java.util.concurrent.ConcurrentLinkedQueue;

public class SetPrivateStoreBuyList extends L2GameClientPacket
{
	private static String _C__9A_SETPRIVATESTORELISTBUY = "[C] 9A SetPrivateStoreListBuy";
	// format: cdb, b - array of (dhhdd)
	private int _count;
	private long[] _items; // count * 3

	private static final int BATCH_LENGTH = 40; // length of the one item

	@Override
	public void readImpl()
	{
		_count = readD();
		if(_count < 1 || _count > Config.MAX_ITEM_IN_PACKET || _count * BATCH_LENGTH != _buf.remaining())
		{
			_items = null;
			return;
		}

		_items = new long[_count * 3];
		for(int i = 0; i < _count; i++)
		{
			final int itemId = readD();
			readH();// TODO analyse this
			readH();// TODO analyse this
			final long cnt = readQ();
			final long price = readQ();

			if(itemId < 1 || cnt < 1 || price < 0)
			{
				_items = null;
				return;
			}

			_items[i * 3 + 0] = itemId;
			_items[i * 3 + 1] = cnt;
			_items[i * 3 + 2] = price;

			readC(); // FE
			readD(); // FF 00 00 00
			readD(); // 00 00 00 00
			readB(new byte[7]); // Completely Unknown
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_items == null || !activeChar.checksForShop(false))
		{
			L2TradeList.cancelStore(activeChar);
			return;
		}

		final int maxSlots = activeChar.getTradeLimit();
		if(_count > maxSlots)
		{
			activeChar.sendPacket(new PrivateStoreManageListBuy(activeChar));
			activeChar.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
			L2TradeList.cancelStore(activeChar);
			return;
		}

		TradeItem temp;
		final ConcurrentLinkedQueue<TradeItem> listbuy = new ConcurrentLinkedQueue<TradeItem>();
		long totalCost = 0;
		int count = 0;

		for(int x = 0; x < _count; x++)
		{
			final int itemId = (int) _items[x * 3 + 0];
			final long itemCount = _items[x * 3 + 1];
			final long itemPrice = _items[x * 3 + 2];
			if(!ItemTable.getInstance().isExists(itemId) || itemCount < 1 || itemPrice < 0 || itemId == L2Item.ITEM_ID_ADENA)
				continue;

			final L2Item item = ItemTable.getInstance().getTemplate(itemId);
			if(item.getReferencePrice() / 2 > itemPrice)
				activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.SetPrivateStoreBuyList.TooLowPrice", activeChar).addItemName(item).addNumber(item.getReferencePrice() / 2));
			else
			{
				temp = new TradeItem();
				temp.setItemId(itemId);
				temp.setCount(itemCount);
				temp.setOwnersPrice(itemPrice);
				totalCost += temp.getOwnersPrice() * temp.getCount();
				listbuy.add(temp);
				count++;
			}
		}

		if(totalCost > activeChar.getAdena())
		{
			activeChar.sendPacket(new PrivateStoreManageListBuy(activeChar));
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_PURCHASE_PRICE_IS_HIGHER_THAN_THE_AMOUNT_OF_MONEY_THAT_YOU_HAVE_AND_SO_YOU_CANNOT_OPEN_A_PERSONAL_STORE));
			L2TradeList.cancelStore(activeChar);
			return;
		}

		if(count > 0)
		{
			activeChar.setBuyList(listbuy);
			activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_BUY);
			activeChar.broadcastPacket(new ChangeWaitType(activeChar, ChangeWaitType.WT_SITTING));
			activeChar.broadcastPacket(new PrivateStoreMsgBuy(activeChar));
			activeChar.sitDown();
			activeChar.broadcastCharInfo();
			return;
		}

		L2TradeList.cancelStore(activeChar);
	}

	@Override
	public String getType()
	{
		return _C__9A_SETPRIVATESTORELISTBUY;
	}
}
