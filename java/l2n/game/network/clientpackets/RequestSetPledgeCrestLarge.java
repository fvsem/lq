package l2n.game.network.clientpackets;

import l2n.game.cache.CrestCache;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestSetPledgeCrestLarge extends L2GameClientPacket
{
	private final static String _C__D0_11_REQUESTSETPLEDGECRESTLARGE = "[C] D0:11 RequestSetPledgeCrestLarge";

	private byte[] _data;

	/**
	 * @param buf
	 * @param client
	 *            format: chd(b)
	 */
	@Override
	public void readImpl()
	{
		final int size = readD();
		if(size == CrestCache.CREST_CLAN_LARGE_SIZE)
		{
			_data = new byte[CrestCache.CREST_CLAN_LARGE_SIZE];
			readB(_data);
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan == null)
			return;

		if((activeChar.getClanPrivileges() & L2Clan.CP_CL_REGISTER_CREST) == L2Clan.CP_CL_REGISTER_CREST)
		{
			if(clan.getHasCastle() == 0 && clan.getHasHideout() == 0)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.THE_CLANS_EMBLEM_WAS_SUCCESSFULLY_REGISTERED__ONLY_A_CLAN_THAT_OWNS_A_CLAN_HALL_OR_A_CASTLE_CAN_GET_THEIR_EMBLEM_DISPLAYED_ON_CLAN_RELATED_ITEMS));
				return;
			}

			if(clan.hasCrestLarge())
				CrestCache.removePledgeCrestLarge(clan);

			if(_data != null)
			{
				CrestCache.savePledgeCrestLarge(clan, _data);
				activeChar.sendPacket(new SystemMessage(SystemMessage.THE_CLANS_EMBLEM_WAS_SUCCESSFULLY_REGISTERED__ONLY_A_CLAN_THAT_OWNS_A_CLAN_HALL_OR_A_CASTLE_CAN_GET_THEIR_EMBLEM_DISPLAYED_ON_CLAN_RELATED_ITEMS));
			}

			clan.broadcastClanStatus(false, true, false);
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_11_REQUESTSETPLEDGECRESTLARGE;
	}
}
