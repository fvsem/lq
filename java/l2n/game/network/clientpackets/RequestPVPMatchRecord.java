package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 28.04.2013
 * @time 3:29:55
 */
public class RequestPVPMatchRecord extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		// TODO UndergroundColiseum.getInstance().recuestInfo(player);
	}

	@Override
	public String getType()
	{
		return "[C] d0:49 - RequestPVPMatchRecord";
	}
}
