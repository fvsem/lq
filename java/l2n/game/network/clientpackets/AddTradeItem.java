package l2n.game.network.clientpackets;

import l2n.commons.math.SafeMath;
import l2n.game.cache.Msg;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.TradeOtherAdd;
import l2n.game.network.serverpackets.TradeOwnAdd;
import l2n.game.network.serverpackets.TradeUpdate;

import java.util.concurrent.ConcurrentLinkedQueue;

public class AddTradeItem extends L2GameClientPacket
{
	private final static String _C__1B_ADDTRADEITEM = "[C] 1B AddTradeItem";

	private int _tradeId;
	private int _objectId;
	private long _amount;

	@Override
	public void readImpl()
	{
		_tradeId = readD();
		_objectId = readD();
		_amount = readQ();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _amount < 1)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
		{
			_log.warning("Character: " + activeChar.getName() + " requested item:" + _objectId + " add without active tradelist: " + _tradeId);
			return;
		}

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.TRADE))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED, Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);
		if(transaction.isConfirmed(activeChar) || transaction.isConfirmed(requestor))
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_MOVE_ADDITIONAL_ITEMS_BECAUSE_TRADE_HAS_BEEN_CONFIRMED, Msg.ActionFail);
			return;
		}

		final L2ItemInstance InvItem = activeChar.getInventory().getItemByObjectId(_objectId);
		if(InvItem == null || !InvItem.canBeTraded(activeChar))
		{
			activeChar.sendPacket(Msg.THIS_ITEM_CANNOT_BE_TRADED_OR_SOLD);
			return;
		}

		if(InvItem.isWear())
			return;

		final long InvItemCount = InvItem.getCount();

		TradeItem tradeItem = getItem(_objectId, transaction.getExchangeList(activeChar));

		long realCount = Math.min(_amount, InvItemCount);
		long leaveCount = InvItemCount - realCount;

		if(tradeItem == null)
		{
			// добавляем новую вещь в список
			tradeItem = new TradeItem(InvItem);
			tradeItem.setCount(realCount);
			transaction.getExchangeList(activeChar).add(tradeItem);
		}
		else
		{
			// меняем количество уже имеющегося
			if(!InvItem.canBeTraded(activeChar))
				return;

			final long TradeItemCount = tradeItem.getCount();
			if(InvItemCount == TradeItemCount) // мы уже предлогаем всё что имеем
				return;

			try
			{
				if(SafeMath.safeAddLong(_amount, TradeItemCount) >= InvItemCount)
					realCount = InvItemCount - TradeItemCount;
			}
			catch(final ArithmeticException e)
			{
				activeChar.sendPacket(Msg.SYSTEM_ERROR, Msg.ActionFail);
				return;
			}

			tradeItem.setCount(realCount + TradeItemCount);
			leaveCount = InvItemCount - realCount - TradeItemCount;
		}

		activeChar.sendPacket(new TradeOwnAdd(InvItem, tradeItem.getCount()));
		activeChar.sendPacket(new TradeUpdate(InvItem, leaveCount));
		requestor.sendPacket(new TradeOtherAdd(InvItem, tradeItem.getCount()));
	}

	private static TradeItem getItem(final int objId, final ConcurrentLinkedQueue<TradeItem> collection)
	{
		for(final TradeItem item : collection)
			if(item.getObjectId() == objId)
				return item;
		return null;
	}

	@Override
	public String getType()
	{
		return _C__1B_ADDTRADEITEM;
	}
}
