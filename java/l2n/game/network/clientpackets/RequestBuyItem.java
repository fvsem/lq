package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.math.SafeMath;
import l2n.game.TradeController;
import l2n.game.TradeController.NpcTradeList;
import l2n.game.cache.Msg;
import l2n.game.model.L2ObjectProcedures;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.*;
import l2n.game.network.serverpackets.ExBuySellList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;
import l2n.util.Util;

import java.util.logging.Logger;

public class RequestBuyItem extends L2GameClientPacket
{
	private static final String _C__40_REQUESTBUYITEM = "[C] 40 RequestBuyItem";
	private static final Logger _log = Logger.getLogger(RequestBuyItem.class.getName());

	private int _listId;
	private int _count;
	private long[] _items; // count*2

	/**
	 * packet type id 0x40
	 * sample
	 * 1f
	 * 44 22 02 01 // list id
	 * 02 00 00 00 // items to buy
	 * 27 07 00 00 // item id
	 * 06 00 00 00 // count
	 * 83 06 00 00
	 * 01 00 00 00
	 * format: cddb, b - array of (dd)
	 */
	@Override
	public void readImpl()
	{
		_listId = readD();
		_count = readD();
		if(_count <= 0 || _count > Config.MAX_ITEM_IN_PACKET || _count * 12 != _buf.remaining())
		{
			_items = null;
			return;
		}

		_items = new long[_count * 2];
		for(int i = 0; i < _count; i++)
		{

			final int itemId = readD();
			final long count = readQ();
			if(itemId <= 0 || count <= 0)
			{
				_items = null;
				return;
			}

			_items[i * 2 + 0] = itemId;
			_items[i * 2 + 1] = count;
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		if(_items == null || _count == 0)
			return;

		if(activeChar.isOutOfControl())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!Config.ALT_GAME_KARMA_PLAYER_CAN_SHOP && activeChar.getKarma() > 0 && !activeChar.isGM())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getBuyListId() != _listId)
			return;

		final L2NpcInstance npc = activeChar.getLastNpc();

		final boolean isValidMerchant = npc instanceof L2SuspiciousMerchantInstance || npc instanceof L2ClanHallManagerInstance || npc instanceof L2MerchantInstance || npc instanceof L2MercManagerInstance || npc instanceof L2CastleChamberlainInstance || npc instanceof L2PetManagerInstance;

		if(!activeChar.isGM() && (npc == null || !isValidMerchant || !activeChar.isInRange(npc.getLoc(), L2Character.INTERACTION_DISTANCE)))
		{
			activeChar.sendActionFailed();
			return;
		}

		L2NpcInstance merchant = null;
		if(npc != null && (npc instanceof L2MerchantInstance || npc instanceof L2ClanHallManagerInstance)) // TODO расширить список?
			merchant = npc;

		final GArray<L2ItemInstance> items = new GArray<L2ItemInstance>(_count);
		L2ItemInstance inst;
		for(int i = 0; i < _count; i++)
		{
			final short itemId = (short) _items[i * 2 + 0];
			final long cnt = _items[i * 2 + 1];
			if(cnt <= 0)
			{
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendActionFailed();
				return;
			}

			inst = ItemTable.getInstance().createItem(itemId, activeChar.getObjectId(), npc != null ? npc.getObjectId() : 0, "RequestBuyItem[122]/" + _listId);
			if(inst == null)
			{
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendActionFailed();
				return;
			}

			if(!inst.isStackable() && cnt != 1)
			{
				Util.handleIllegalPlayerAction(activeChar, "RequestBuyItem[125]", "tried to purchase " + cnt + " non-stackable items: " + itemId + " to 1 slot", 2);
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendActionFailed();
				return;
			}

			inst.setCount(cnt);
			items.add(inst);
		}

		long finalLoad = 0;
		int finalCount = activeChar.getInventory().getSize();
		int needsSpace = 2;
		int weight = 0;
		final long currentMoney = activeChar.getAdena();

		int itemId;
		long cnt, price, tax = 0, totalCost = 0, subTotal = 0;
		double taxRate = 0;

		if(merchant != null && merchant.getCastle() != null)
			taxRate = merchant.getCastle().getTaxRate();

		NpcTradeList list;
		TradeItem tradeItem;
		for(int i = 0; i < items.size(); i++)
		{
			itemId = items.getUnsafe(i).getItemId();
			cnt = items.getUnsafe(i).getCount();
			needsSpace = 2;

			if(items.getUnsafe(i).getItem().isStackable())
			{
				needsSpace = 1;
				if(activeChar.getInventory().getItemByItemId(itemId) != null)
					needsSpace = 0;
			}

			list = TradeController.getInstance().getBuyList(_listId);
			if(list == null)
			{
				Log.add("tried to buy from non-exist list " + _listId, "errors", activeChar);
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendActionFailed();
				return;
			}

			tradeItem = getItemByItemId(itemId, list);
			price = tradeItem == null ? 0 : tradeItem.getOwnersPrice();
			if(itemId >= 3960 && itemId <= 4026 || itemId >= 5205 && itemId <= 5219 || itemId >= 6038 && itemId <= 6306 || itemId >= 6779 && itemId <= 6833 || itemId >= 7918 && itemId <= 8029)
				price *= Config.RATE_SIEGE_GUARDS_PRICE;

			if(price == 0 && !activeChar.getPlayerAccess().UseGMShop)
			{
				Util.handleIllegalPlayerAction(activeChar, "RequestBuyItem[179]", "Tried to buy zero price item, list : " + _listId + " item " + itemId, 0);
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendMessage("Error: zero-price item! Please notify GM.");
				activeChar.sendActionFailed();
				return;
			}

			if(price < 0)
			{
				_log.warning("ERROR, no price found. Wrong buylist?");
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				activeChar.sendActionFailed();
				return;
			}

			weight = items.getUnsafe(i).getItem().getWeight();
			try
			{
				if(cnt < 0)
					throw new ArithmeticException("cnt < 0");
				subTotal = SafeMath.safeAddLong(subTotal, SafeMath.safeMulLong(cnt, price)); // Before tax

				tax = SafeMath.safeMulLong(subTotal, taxRate);
				totalCost = SafeMath.safeAddLong(subTotal, tax);
				if(totalCost < 0)
					throw new ArithmeticException("213: Tried to purchase negative " + totalCost + " adena worth of goods.");

				finalLoad = SafeMath.safeAddLong(finalLoad, SafeMath.safeMulLong(cnt, weight));
				if(finalLoad < 0)
					throw new ArithmeticException("254: Tried to purchase negative " + finalLoad + " adena worth of goods.");
			}
			catch(final ArithmeticException e)
			{
				Util.handleIllegalPlayerAction(activeChar, "RequestBuyItem[157]", "merchant: " + merchant + ": " + e.getMessage(), 1);
				items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
				sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED, Msg.ActionFail);
				return;
			}

			if(needsSpace == 2)
				finalCount += cnt;
			else if(needsSpace == 1)
				finalCount += 1;
		}

		if(subTotal + tax > currentMoney || subTotal < 0 || currentMoney <= 0 && !activeChar.getPlayerAccess().UseGMShop)
		{
			sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA, Msg.ActionFail);
			items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
			return;
		}

		if(!activeChar.getInventory().validateWeight(finalLoad))
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT, Msg.ActionFail);
			items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
			return;
		}

		if(!activeChar.getInventory().validateCapacity(finalCount))
		{
			sendPacket(Msg.YOUR_INVENTORY_IS_FULL, Msg.ActionFail);
			items.forEach(L2ObjectProcedures.PROC_DELETE_ITEMS);
			return;
		}

		// Для магазинов с ограниченным количеством товара число продаваемых предметов уменьшаем после всех проверок
		list = TradeController.getInstance().getBuyList(_listId);
		for(int i = 0; i < items.size(); i++)
		{
			itemId = items.getUnsafe(i).getItemId();
			cnt = items.getUnsafe(i).getCount();
			tradeItem = getItemByItemId(itemId, list);

			if(tradeItem != null && tradeItem.isCountLimited())
				if(cnt > tradeItem.getCurrentValue())
				{
					final int t = (int) (System.currentTimeMillis() / 60000);
					if(tradeItem.getLastRechargeTime() + tradeItem.getRechargeTime() <= t)
					{
						tradeItem.setLastRechargeTime(t);
						tradeItem.setCurrentValue(tradeItem.getCount());
					}
					else
						continue;
				}
				else
					tradeItem.setCurrentValue(tradeItem.getCurrentValue() - cnt);
		}

		if(subTotal + tax > 0)
			activeChar.reduceAdena(subTotal + tax, true);

		for(final L2ItemInstance item : items)
		{
			Log.LogItem(activeChar, merchant, Log.BuyItem, item);
			activeChar.getInventory().addItem(item);
		}

		for(int i = 0; i < items.size(); i++)
		{
			itemId = items.getUnsafe(i).getItemId();
			cnt = items.getUnsafe(i).getCount();
			if(cnt > 1)
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S).addItemName(itemId).addNumber((int) cnt));
			else
				activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addItemName(itemId));
		}

		// Add tax to castle treasury if not owned by npc clan
		if(merchant != null && merchant.getCastle() != null && merchant.getCastle().getOwnerId() > 0)
		{
			merchant.getCastle().addToTreasury(tax, true, false);
			Log.add(merchant.getCastle().getName() + "|" + tax + "|BuyItem", "treasury");
		}

		activeChar.sendPacket(new ExBuySellList(list, activeChar, taxRate).done());
		activeChar.sendChanges();
	}

	private static final TradeItem getItemByItemId(final int itemId, final NpcTradeList list)
	{
		for(final TradeItem ti : list.getItems())
			if(ti.getItemId() == itemId)
				return ti;
		return null;
	}

	@Override
	public String getType()
	{
		return _C__40_REQUESTBUYITEM;
	}
}
