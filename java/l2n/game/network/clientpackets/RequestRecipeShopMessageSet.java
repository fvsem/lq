package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestRecipeShopMessageSet extends L2GameClientPacket
{
	private static String _C__BA_RequestRecipeShopMessageSet = "[C] BA RequestRecipeShopMessageSet";
	// format: cS
	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS(16);
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getDuel() != null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getCreateList() != null)
			activeChar.getCreateList().setStoreName(_name);
	}

	@Override
	public String getType()
	{
		return _C__BA_RequestRecipeShopMessageSet;
	}
}
