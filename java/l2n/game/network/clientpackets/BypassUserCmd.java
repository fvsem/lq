package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.handler.UserCommandHandler;
import l2n.game.handler.interfaces.IUserCommandHandler;
import l2n.game.model.actor.L2Player;

/**
 * Пример пакета по команде /loc:
 * AA 00 00 00 00
 */
public class BypassUserCmd extends L2GameClientPacket
{
	private final static String _C__B3_BYPASSUSERCMD = "[C] B3 BypassUserCmd";

	private int _command;

	/**
	 * packet type id 0xB3
	 * format: cd
	 */
	@Override
	public void readImpl()
	{
		_command = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final IUserCommandHandler handler = UserCommandHandler.getInstance().getUserCommandHandler(_command);
		if(handler == null && activeChar.isGM())
			activeChar.sendMessage(new CustomMessage("common.S1NotImplemented", activeChar).addString(String.valueOf(_command)));
		else if(handler != null)
			handler.useUserCommand(_command, activeChar);
	}

	@Override
	public String getType()
	{
		return _C__B3_BYPASSUSERCMD;
	}
}
