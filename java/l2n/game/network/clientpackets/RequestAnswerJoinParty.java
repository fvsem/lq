package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Party;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.JoinParty;
import l2n.game.network.serverpackets.PartyMemberPosition;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestAnswerJoinParty extends L2GameClientPacket
{
	private final static String _C__2A_REQUESTANSWERPARTY = "[C] 2A RequestAnswerJoinParty";
	// Format: cd
	private boolean _accepted;

	@Override
	public void readImpl()
	{
		if(_buf.hasRemaining())
			_accepted = readD() == 1;
		else
			_accepted = false;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.PARTY))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED, Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);
		transaction.cancel();

		// для определения нужно создавать новую группу или уже есть
		final int itemDistribution = transaction.getCreateNewParty();
		if(itemDistribution < 0 && (requestor.getParty() == null || requestor.getParty().getPartyLeader() == null))
		{
			activeChar.sendPacket(Msg.ActionFail, JoinParty.STATIC_PACKET_DECLINED);
			return;
		}

		final SystemMessage problem = activeChar.canJoinParty(requestor);
		if(problem != null)
		{
			activeChar.sendPacket(problem, Msg.ActionFail, JoinParty.STATIC_PACKET_DECLINED);
			return;
		}

		if(_accepted)
		{

			if(activeChar.isInZone(ZoneType.OlympiadStadia))
			{
				activeChar.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
				requestor.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
				return;
			}

			// значит нужно создать пати
			if(itemDistribution >= 0)
				requestor.setParty(new L2Party(requestor, itemDistribution));

			if(requestor.getParty().getMemberCount() >= L2Party.MAX_PARTY_MEMBERS)
			{
				activeChar.sendPacket(Msg.PARTY_IS_FULL);
				requestor.sendPacket(Msg.PARTY_IS_FULL);
				return;
			}

			requestor.sendPacket(JoinParty.STATIC_PACKET_ACCEPTED);
			activeChar.joinParty(requestor.getParty());
			// force update party position
			activeChar.getParty().broadcastToPartyMembers(activeChar, new PartyMemberPosition());
		}
		else
		{
			requestor.sendPacket(JoinParty.STATIC_PACKET_DECLINED, Msg.THE_PLAYER_DECLINED_TO_JOIN_YOUR_PARTY);
			// activate garbage collection if there are no other members in party (happens when we were creating new one)
			if(requestor.getParty() != null && requestor.getParty().getMemberCount() == 1)
				requestor.setParty(null);
		}
	}

	@Override
	public final String getType()
	{
		return _C__2A_REQUESTANSWERPARTY;
	}
}
