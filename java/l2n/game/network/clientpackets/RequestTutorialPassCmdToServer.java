package l2n.game.network.clientpackets;

import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.quest.Quest;

public class RequestTutorialPassCmdToServer extends L2GameClientPacket
{
	private static String _C__86_REQUESTTUTORIALPASSCMD = "[C] 86 RequestTutorialPassCmdToServer";
	// format: cS

	private String _bypass = null;

	@Override
	public void readImpl()
	{
		_bypass = _buf.hasRemaining() ? readS() : null;
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null || _bypass == null)
			return;

		final Quest tutorial = QuestManager.getQuest(255);
		if(tutorial != null)
			player.processQuestEvent(tutorial.getName(), _bypass, null);
	}

	@Override
	public String getType()
	{
		return _C__86_REQUESTTUTORIALPASSCMD;
	}
}
