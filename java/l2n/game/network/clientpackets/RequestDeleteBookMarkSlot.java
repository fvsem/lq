package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System
 * @date 16.12.2009
 * @time 0:36:18
 */
public class RequestDeleteBookMarkSlot extends L2GameClientPacket
{
	private static final String _C__51_REQUESTDELETEBOOKMARKSLOT = "[C] 51 RequestDeleteBookMarkSlot";

	private int id;

	@Override
	protected void readImpl()
	{
		id = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
            return;
        }
		activeChar.teleportBookmarkDelete(id);
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTDELETEBOOKMARKSLOT;
	}
}
