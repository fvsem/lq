package l2n.game.network.clientpackets;

import l2n.game.cache.CrestCache;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeCrest;

public class RequestPledgeCrest extends L2GameClientPacket
{
	private static final String _C__68_REQUESTPLEDGECREST = "[C] 68 RequestPledgeCrest";
	// format: cd

	private int _crestId;

	@Override
	public void readImpl()
	{
		_crestId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(_crestId == 0)
			return;

		final byte[] data = CrestCache.getPledgeCrest(_crestId);
		if(data != null)
		{
			final PledgeCrest pc = new PledgeCrest(_crestId, data);
			sendPacket(pc);
		}
	}

	@Override
	public String getType()
	{
		return _C__68_REQUESTPLEDGECREST;
	}
}
