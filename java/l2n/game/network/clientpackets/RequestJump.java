package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExJumpToLocation;

/**
 * @author L2System
 * @date 12.07.2010
 * @time 17:11:14
 */
public class RequestJump extends L2GameClientPacket
{
	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		sendPacket(new ExJumpToLocation(activeChar.getObjectId(), activeChar.getLoc(), activeChar.getLoc()));
		System.out.println(getType());
	}

	@Override
	protected void readImpl()
	{}
}
