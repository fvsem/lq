package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.quest.Quest;
import l2n.game.network.L2GameClient;
import l2n.game.network.serverpackets.CharacterSelectionInfo;
import l2n.game.tables.CharNameTable;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2PlayerTemplate;

import java.util.regex.Matcher;

public class CharacterCreate extends L2GameClientPacket
{
	private static final String _C__0B_CHARACTERCREATE = "[C] 0B CharacterCreate";

	// cSdddddddddddd

	private String _name;
	private int _sex;
	private int _classId;
	private int _hairStyle;
	private int _hairColor;
	private int _face;

	/**
	 * @param decrypt
	 */
	@Override
	protected void readImpl()
	{
		_name = readS();
		readD(); // race
		_sex = readD();
		_classId = readD();
		readD(); // int
		readD(); // str
		readD(); // con
		readD(); // men
		readD(); // dex
		readD(); // wit
		_hairStyle = readD();
		_hairColor = readD();
		_face = readD();
	}

	@Override
	public void runImpl()
	{
		for(final ClassId cid : ClassId.values())
			if(cid.getId() == _classId && cid.getLevel() != 1)
				return;

		if(_face > 2 || _face < 0)
		{
			_log.warning("Character Creation Failure: Character face " + _face + " is invalid. Possible client hack. " + getClient());
			sendPacket(Msg.CharacterCreateFail_REASON_CREATION_FAILED);
			return;
		}

		if(_hairStyle < 0 || _sex == 0 && _hairStyle > 4 || _sex != 0 && _hairStyle > 6)
		{
			_log.warning("Character Creation Failure: Character hair style " + _hairStyle + " is invalid. Possible client hack. " + getClient());
			sendPacket(Msg.CharacterCreateFail_REASON_CREATION_FAILED);
			return;
		}

		if(_hairColor > 3 || _hairColor < 0)
		{
			_log.warning("Character Creation Failure: Character hair color " + _hairColor + " is invalid. Possible client hack. " + getClient());
			sendPacket(Msg.CharacterCreateFail_REASON_CREATION_FAILED);
			return;
		}

		if(!Config.CNAME_TEMPLATE.matcher(_name).matches())
		{
			sendPacket(Msg.CharacterCreateFail_REASON_16_ENG_CHARS);
			return;
		}

		// запрещаем использовать знак $ в имени игрока
		_name = Matcher.quoteReplacement(_name);
		if(Config.ENABLE_CHARNAME_BLOCKSPAM)
		{
			for(String nameBlock : Config.CHARNAME_BLOCKSPAM_LIST)
			{
				if(_name.toLowerCase().contains(nameBlock))
				{
					sendPacket(Msg.CharacterCreateFail_REASON_CREATION_FAILED);
					return;
				}
			}
		}
		L2Player newChar = null;

		/*
		 * Since checks for duplicate names are done using SQL, lock must be held until data is written to DB as well.
		 */
		synchronized (CharNameTable.getInstance())
		{
			if(CharNameTable.getInstance().accountCharNumber(getClient().getLoginName()) >= 8)
			{
				sendPacket(Msg.CharacterCreateFail_REASON_TOO_MANY_CHARACTERS);
				return;
			}

			else if(CharNameTable.getInstance().doesCharNameExist(_name))
			{
				sendPacket(Msg.CharacterCreateFail_REASON_NAME_ALREADY_EXISTS);
				return;
			}

			newChar = L2Player.create(_classId, (byte) _sex, getClient().getLoginName(), _name, (byte) _hairStyle, (byte) _hairColor, (byte) _face);
		}

		if(newChar == null)
			return;

		newChar.setConnected(false);

		sendPacket(Msg.CharacterCreateSuccess); // этого одного достаточно

		initNewChar(getClient(), newChar);
	}

	private void initNewChar(final L2GameClient client, final L2Player newChar)
	{
		final L2PlayerTemplate template = newChar.getTemplate();
		L2Player.restoreCharSubClasses(newChar);

				if (Config.SPAWN_CHAR)
			newChar.setXYZInvisible(Config.SPAWN_X, Config.SPAWN_Y, Config.SPAWN_Z);
		else
			newChar.setXYZInvisible(template.spawnLoc);
				
				if (Config.CHAR_TITLE)
			newChar.setTitle(Config.ADD_CHAR_TITLE);
		else
			newChar.setTitle("");
		// add vit points
		newChar.setVitalityPoints(20000, false);

		// Количество адены, дающееся персонажу при его создании
		final ItemTable itemTable = ItemTable.getInstance();
		if(Config.STARTING_ADENA > 0)
			newChar.addAdena(Config.STARTING_ADENA);

		for(final L2Item i : template.getItems())
		{
			final L2ItemInstance item = itemTable.createItem(i.getItemId(), newChar.getObjectId(), 0, "CharacterCreate[178]");
			newChar.getInventory().addItem(item);

			if(item.getItemId() == 5588) // tutorial book
				newChar.registerShortCut(new L2ShortCut(11, 0, L2ShortCut.TYPE_ITEM, item.getObjectId(), -1));

			if(item.isEquipable() && (newChar.getActiveWeaponItem() == null || item.getItem().getType2() != L2Item.TYPE2_WEAPON))
				newChar.getInventory().equipItem(item, false);
		}

		// All classes - Scroll of Escape: Kamael Village
		L2ItemInstance item = itemTable.createItem(9716, newChar.getObjectId(), 0, "CharacterCreate[178]");
		item.setCount(10);
		newChar.getInventory().addItem(item);

		// All classes - Adventurer's Scroll of Escape
		item = itemTable.createItem(10650, newChar.getObjectId(), 0, "CharacterCreate[178]");
		item.setCount(5);
		newChar.getInventory().addItem(item);
		// Стартовые предметы персонажам , маг и воин отдельно
		if(Config.ENABLE_STARTING_ITEM)
			{
		if (newChar.isMageClass())
		{
			for(int i = 0; i < Config.STARTING_ITEM_ID_LIST.size(); i++)
				if(Config.STARTING_ITEM_ID_LIST.getUnsafe(i) != 0)
					if(Config.STARTING_ITEM_COUNT_LIST.getUnsafe(i) != 0)
					{
						item = itemTable.createItem(Config.STARTING_ITEM_ID_LIST.getUnsafe(i), newChar.getObjectId(), 0, "CharacterCreate[178]");
						item.setCount(Config.STARTING_ITEM_COUNT_LIST.getUnsafe(i));
						newChar.getInventory().addItem(item);
			}
			}
					else
			{
						for(int i = 0; i < Config.STARTING_ITEM_ID_LISTM.size(); i++)
				if(Config.STARTING_ITEM_ID_LISTM.getUnsafe(i) != 0)
					if(Config.STARTING_ITEM_COUNT_LISTM.getUnsafe(i) != 0)
					{
						item = itemTable.createItem(Config.STARTING_ITEM_ID_LISTM.getUnsafe(i), newChar.getObjectId(), 0, "CharacterCreate[178]");
						item.setCount(Config.STARTING_ITEM_COUNT_LISTM.getUnsafe(i));
						newChar.getInventory().addItem(item);
			}
			}
		}			

		// ======================================
		for(final L2SkillLearn skill : SkillTreeTable.getInstance().getAvailableSkills(newChar, newChar.getClassId()))
			newChar.addSkill(SkillTable.getInstance().getInfo(skill.id, skill.skillLevel), true);

		if(newChar.getSkillLevel(1001) > 0) // Soul Cry
			newChar.registerShortCut(new L2ShortCut(1, 0, L2ShortCut.TYPE_SKILL, 1001, 1));
		if(newChar.getSkillLevel(1177) > 0) // Wind Strike
			newChar.registerShortCut(new L2ShortCut(1, 0, L2ShortCut.TYPE_SKILL, 1177, 1));
		if(newChar.getSkillLevel(1216) > 0) // Self Heal
			newChar.registerShortCut(new L2ShortCut(2, 0, L2ShortCut.TYPE_SKILL, 1216, 1));

		// add attack, take, sit shortcut
		newChar.registerShortCut(new L2ShortCut(0, 0, L2ShortCut.TYPE_ACTION, 2, -1));
		newChar.registerShortCut(new L2ShortCut(3, 0, L2ShortCut.TYPE_ACTION, 5, -1));
		newChar.registerShortCut(new L2ShortCut(10, 0, L2ShortCut.TYPE_ACTION, 0, -1));

		if(Config.GIVE_NEW_CHARACTER_SS)
		{
			// soulshot magic
			item = itemTable.createItem(3947, newChar.getObjectId(), 0, "Start Soulshot");
			item.setCount(Config.GIVE_NEW_CHARACTER_SS_COUNT);
			newChar.getInventory().addItem(item);
			newChar.registerShortCut(new L2ShortCut(0, 1, L2ShortCut.TYPE_ITEM, item.getObjectId(), -1));
			// soulshot fighter
			item = itemTable.createItem(1835, newChar.getObjectId(), 0, "Start Soulshot");
			item.setCount(Config.GIVE_NEW_CHARACTER_SS_COUNT);
			newChar.getInventory().addItem(item);
			newChar.registerShortCut(new L2ShortCut(1, 1, L2ShortCut.TYPE_ITEM, item.getObjectId(), -1));
		}

		startTutorialQuest(newChar);

		newChar.setCurrentHpMp(newChar.getMaxHp(), newChar.getMaxMp());

		L2GameClient.saveCharToDisk(newChar);
		newChar.deleteMe(); // release the world of this character and it's inventory

		client.setCharSelection(CharacterSelectionInfo.loadCharacterSelectInfo(client.getLoginName()));
	}

	@Override
	public String getType()
	{
		return _C__0B_CHARACTERCREATE;
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}

	public static void startTutorialQuest(final L2Player player)
	{
		final Quest q = QuestManager.getQuest(255);
		if(q != null)
			q.newQuestState(player, Quest.CREATED);
	}
}
