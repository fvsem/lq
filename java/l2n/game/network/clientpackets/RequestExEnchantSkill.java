package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.util.Log;
import l2n.util.Rnd;

/**
 * Format chdd c: (id) 0xD0 h: (subid) 0x0F d: skill id d: skill lvl
 */
public class RequestExEnchantSkill extends L2GameClientPacket
{
	private static String _C__D0_0F_REQUESTEXENCHANTSKILL = "[C] D0:0F RequestExEnchantSkill";
	private int _skillId;
	private int _skillLvl;

	@Override
	public void readImpl()
	{
		_skillId = readD();
		_skillLvl = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendMessage("You must leave transformation mode first.");
			return;
		}

		if(activeChar.getLevel() < 76)
		{
			sendPacket(Msg.YOU_DONT_MEET_SKILL_LEVEL_REQUIREMENTS);
			return;
		}

		if(activeChar.getClassId().getLevel() < 4)
		{
			sendPacket(Msg.NOT_COMPLETED_QUEST_FOR_SKILL_ACQUISITION);
			return;
		}

		L2Skill skill = SkillTable.getInstance().getInfo(_skillId, _skillLvl);
		if(skill == null)
		{
			if(_skillId > 0)
				_log.warning("RequestExEnchantSkill: skillId " + _skillId + " level " + _skillLvl + " not found in Datapack.");
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final L2EnchantSkillLearn sl = SkillTreeTable.getInstance().getSkillEnchant(_skillId, _skillLvl);
		if(sl == null)
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		if(activeChar.getSkillLevel(_skillId) != sl.getMinSkillLevel())
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final int[] cost = sl.getCost();
		final int requiredSp = cost[1] * SkillTreeTable.NORMAL_ENCHANT_COST_MULTIPLIER * sl.getCostMult();
		final int requiredAdena = cost[0] * SkillTreeTable.NORMAL_ENCHANT_COST_MULTIPLIER * sl.getCostMult();
		final int rate = sl.getRate(activeChar);

		if(activeChar.getSp() < requiredSp)
		{
			sendPacket(Msg.SP_REQUIRED_FOR_SKILL_ENCHANT_IS_INSUFFICIENT);
			return;
		}

		if(activeChar.getAdena() < requiredAdena)
		{
			sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		if(!Config.ALT_DISABLE_ENCHANT_BOOKS)
			if(_skillLvl % 100 == 1) // only first lvl requires book (101, 201, 301 ...)
			{
				if(Functions.getItemCount(activeChar, SkillTreeTable.NORMAL_ENCHANT_BOOK) == 0)
				{
					activeChar.sendPacket(Msg.ITEMS_REQUIRED_FOR_SKILL_ENCHANT_ARE_INSUFFICIENT);
					return;
				}
				Functions.removeItem(activeChar, SkillTreeTable.NORMAL_ENCHANT_BOOK, 1);
			}

		if(Rnd.chance(rate))
		{
			activeChar.addExpAndSp(0, -1 * requiredSp, false, false);
			Functions.removeItem(activeChar, 57, requiredAdena);

			// activeChar.sendPacket(new SystemMessage(SystemMessage.SP_HAS_DECREASED_BY_S1).addNumber(requiredSp));
			activeChar.sendPacket(new SystemMessage(SystemMessage.SUCCEEDED_IN_ENCHANTING_SKILL_S1).addSkillName(_skillId, _skillLvl));
			activeChar.sendPacket(ExEnchantSkillResult.SUCCESS);

			Log.add(activeChar.getName() + "[" + activeChar.getObjectId() + "]" + "|Successfully enchanted|" + _skillId + "|to +" + _skillLvl + "|" + rate, "enchant_skills");
		}
		else
		{
			skill = SkillTable.getInstance().getInfo(_skillId, sl.getBaseLevel());
			activeChar.sendPacket(new SystemMessage(SystemMessage.FAILED_IN_ENCHANTING_SKILL_S1).addSkillName(_skillId, _skillLvl));
			activeChar.sendPacket(ExEnchantSkillResult.FAIL);

			Log.add(activeChar.getName() + "[" + activeChar.getObjectId() + "]" + "|Failed to enchant|" + _skillId + "|to +" + _skillLvl + "|" + rate, "enchant_skills");
		}

		activeChar.disableSkill(skill, activeChar.addSkill(skill, true));
		activeChar.sendPacket(new SkillList(activeChar));
		activeChar.sendPacket(new ExEnchantSkillInfo(_skillId, activeChar.getSkillLevel(_skillId)));
		updateSkillShortcuts(activeChar);
	}

	private void updateSkillShortcuts(final L2Player player)
	{
		// update all the shortcuts to this skill
		for(final L2ShortCut sc : player.getAllShortCuts())
			if(sc.id == _skillId && sc.type == L2ShortCut.TYPE_SKILL)
			{
				final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, player.getSkillLevel(_skillId));
				player.sendPacket(new ShortCutRegister(newsc));
				player.registerShortCut(newsc);
			}
	}

	@Override
	public String getType()
	{
		return _C__D0_0F_REQUESTEXENCHANTSKILL;
	}
}
