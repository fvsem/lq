package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExShowReceivedPostList;

/**
 * @author L2System Project
 * @date 29.07.2010
 * @time 16:48:43
 */
public class RequestRequestReceivedPostList extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{
		// trigger packet
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		if(!cha.isInZonePeace())
		{
			cha.sendPacket(Msg.CANNOT_USE_MAIL_OUTSIDE_PEACE_ZONE);
			return;
		}

		cha.sendPacket(new ExShowReceivedPostList(cha.getObjectId()));
	}
}
