package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

public class AttackRequest extends L2GameClientPacket
{
	// cddddc
	private int _objectId;
	private int _originX;
	private int _originY;
	private int _originZ;
	private int _attackId;

	private final static String _C__01_ATTACKREQUEST = "[C] 01 AttackRequest";

	@Override
	public void readImpl()
	{
		try
		{
			_objectId = readD();
			_originX = readD();
			_originY = readD();
			_originZ = readD();
			_attackId = readC(); // 0 for simple click 1 for shift-click
		}
		catch(final Exception e)
		{
			_objectId = 0;
			_originX = 0;
			_originY = 0;
			_originZ = 0;
			_attackId = 0;
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _objectId == 0 && _originX == 0 && _originY == 0 && _originZ == 0)
			return;

		if(activeChar.isOutOfControl())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!activeChar.getPlayerAccess().CanAttack)
		{
			activeChar.sendActionFailed();
			return;
		}

		L2Object target = activeChar.getVisibleObject(_objectId);
		if(target == null && ((target = L2ObjectsStorage.getItemByObjId(_objectId)) == null || !activeChar.isInRange(target, 1000)))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getAggressionTarget() != null && activeChar.getAggressionTarget() != target)
		{
			activeChar.sendPacket(Msg.FAILED_TO_CHANGE_ATTACK_TARGET, Msg.ActionFail);
			return;
		}

		// noinspection ConstantConditions
		if(target.isPlayer() && (activeChar.isInVehicle() || ((L2Player) target).isInVehicle()))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getTarget() != target)
		{
			target.onAction(activeChar, _attackId == 1);
			return;
		}

		if(target.getObjectId() != activeChar.getObjectId() && activeChar.getPrivateStoreType() == L2Player.STORE_PRIVATE_NONE && !activeChar.isInTransaction())
			target.onForcedAttack(activeChar, _attackId == 1);
	}

	@Override
	public String getType()
	{
		return _C__01_ATTACKREQUEST;
	}
}
