package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Alliance;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestAnswerJoinAlly extends L2GameClientPacket
{
	private static String _C__8D_REQUESTANSWERJOINALLY = "[C] 8D RequestAnswerJoinAlly";
	// format: cd

	private int _response;

	@Override
	public void readImpl()
	{
		if(_buf.hasRemaining())
			_response = readD();
		else
			_response = 0;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.ALLY))
		{
			transaction.cancel();
			activeChar.sendPacket(Msg.TIME_EXPIRED);
			activeChar.sendPacket(Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);

		transaction.cancel();

		if(requestor.getAlliance() == null)
			return;
		if(_response == 1)
		{
			final L2Alliance ally = requestor.getAlliance();
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_ACCEPTED_THE_ALLIANCE));
			activeChar.getClan().setAllyId(requestor.getAllyId());
			activeChar.getClan().updateClanInDB();
			ally.addAllyMember(activeChar.getClan(), true);
			ally.broadcastAllyStatus(true);
		}
		else
			requestor.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_FAILED_TO_INVITE_A_CLAN_INTO_THE_ALLIANCE));
	}

	@Override
	public String getType()
	{
		return _C__8D_REQUESTANSWERJOINALLY;
	}
}
