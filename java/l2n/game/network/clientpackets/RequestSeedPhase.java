package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExShowSeedMapInfo;

/**
 * @author L2System Project
 * @date 12.07.2010
 * @time 23:14:50
 */
public class RequestSeedPhase extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExShowSeedMapInfo());
	}
}
