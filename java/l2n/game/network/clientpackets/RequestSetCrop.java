package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.CastleManorManager;
import l2n.game.instancemanager.CastleManorManager.CropProcure;

/**
 * Format: (ch) dd [dddc]
 * d - manor id
 * d - size
 * [
 * d - crop id
 * d - sales
 * d - price
 * c - reward type
 * ]
 */
public class RequestSetCrop extends L2GameClientPacket
{
	private static final String _C__D0_04_REQUESTSETCROP = "[C] D0:04 RequestSetCrop";

	private int _size;

	private int _manorId;

	private long[] _items; // _size*4

	@Override
	protected void readImpl()
	{
		_manorId = readD();
		_size = readD();
		if(_size <= 0 || _size > Config.MAX_ITEM_IN_PACKET || _size * 21 != _buf.remaining())
		{
			_size = 0;
			return;
		}
		_items = new long[_size * 4];
		for(int i = 0; i < _size; i++)
		{
			final int itemId = readD();
			final long sales = readQ();
			final long price = readQ();
			final int type = readC();

			if(itemId < 1 || sales < 0 || price < 0)
			{
				_items = null;
				return;
			}

			_items[i * 4 + 0] = itemId;
			_items[i * 4 + 1] = sales;
			_items[i * 4 + 2] = price;
			_items[i * 4 + 3] = type;
		}
	}

	@Override
	protected void runImpl()
	{
		if(_size < 1)
			return;

		final GArray<CropProcure> crops = new GArray<CropProcure>();
		for(int i = 0; i < _size; i++)
		{
			final int id = (int) _items[i * 4 + 0];
			final long sales = _items[i * 4 + 1];
			final long price = _items[i * 4 + 2];
			final int type = (int) _items[i * 4 + 3];
			if(id > 0)
			{
				final CropProcure s = CastleManorManager.getInstance().getNewCropProcure(id, sales, type, price, sales);
				crops.add(s);
			}
		}

		CastleManager.getInstance().getCastleByIndex(_manorId).setCropProcure(crops, CastleManorManager.PERIOD_NEXT);
		if(Config.MANOR_SAVE_ALL_ACTIONS)
			CastleManager.getInstance().getCastleByIndex(_manorId).saveCropData(CastleManorManager.PERIOD_NEXT);
	}

	@Override
	public String getType()
	{
		return _C__D0_04_REQUESTSETCROP;
	}
}
