package l2n.game.network.clientpackets;

import l2n.game.RecipeController;
import l2n.game.model.L2ManufactureItem;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;

public class RequestRecipeShopMakeDo extends L2GameClientPacket
{
	private static String _C__BF_REQUESTRECIPESHOPMAKEDO = "[C] BF RequestRecipeShopMakeDo";

	private int _id;
	private int _recipeId;
	private long _price;

	/**
	 * packet type id 0xBF format: cddd
	 */
	@Override
	public void readImpl()
	{
		_id = readD();
		_recipeId = readD();
		_price = readQ();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getDuel() != null)
		{
			activeChar.sendActionFailed();
			return;
		}

		final L2Player manufacturer = (L2Player) activeChar.getVisibleObject(_id);
		if(manufacturer == null || manufacturer.getPrivateStoreType() != L2Player.STORE_PRIVATE_MANUFACTURE || manufacturer.getDistance(activeChar) > L2Character.INTERACTION_DISTANCE)
		{
			activeChar.sendActionFailed();
			return;
		}

		for(final L2ManufactureItem i : manufacturer.getCreateList().getList())
			if(i.getRecipeId() == _recipeId)
			{
				if(_price == i.getCost())
					break;
				activeChar.sendActionFailed();
				return;
			}

		RecipeController.getInstance().requestManufactureItem(manufacturer, activeChar, _recipeId);
	}

	@Override
	public String getType()
	{
		return _C__BF_REQUESTRECIPESHOPMAKEDO;
	}
}
