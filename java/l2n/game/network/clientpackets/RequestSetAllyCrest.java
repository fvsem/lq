package l2n.game.network.clientpackets;

import l2n.game.cache.CrestCache;
import l2n.game.model.L2Alliance;
import l2n.game.model.actor.L2Player;

public class RequestSetAllyCrest extends L2GameClientPacket
{
	private final static String _C__91_REQUESTSETALLYCREST = "[C] 91 RequestSetAllyCrest";

	private byte[] _data;

	@Override
	public void readImpl()
	{
		final int size = super.readD();
		if(size == CrestCache.CREST_ALLY_SIZE)
		{
			_data = new byte[CrestCache.CREST_ALLY_SIZE];
			readB(_data);
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Alliance ally = activeChar.getAlliance();
		if(ally != null && activeChar.isAllyLeader())
		{
			if(ally.hasAllyCrest())
				CrestCache.removeAllyCrest(ally);

			if(_data != null)
				CrestCache.saveAllyCrest(ally, _data);

			ally.broadcastAllyStatus(false);
		}
	}

	@Override
	public String getType()
	{
		return _C__91_REQUESTSETALLYCREST;
	}
}
