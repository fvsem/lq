package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.L2FriendList;

public class RequestFriendList extends L2GameClientPacket
{
	private static final String _C__79_REQUESTFRIENDLIST = "[C] 79 RequestFriendList";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		sendPacket(new L2FriendList(getClient().getActiveChar()));
	}

	@Override
	public String getType()
	{
		return _C__79_REQUESTFRIENDLIST;
	}
}
