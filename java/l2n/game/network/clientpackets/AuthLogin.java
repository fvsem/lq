package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.SessionKey;
import l2n.game.network.L2GameClient;

/**
 * This class ...
 * 
 * @version $Revision: 1.9.2.3.2.4 $ $Date: 2005/03/27 15:29:30 $
 */
public class AuthLogin extends L2GameClientPacket
{
	private static final String _C__08_AUTHLOGIN = "[C] 08 AuthLogin";
	// Format: cSddddd

	// loginName + keys must match what the loginserver used.
	private String _loginName;
	private int _playKey1;
	private int _playKey2;
	private int _loginKey1;
	private int _loginKey2;

	@Override
	public void readImpl()
	{
		_loginName = readS().toLowerCase();
		_playKey2 = readD();
		_playKey1 = readD();
		_loginKey1 = readD();
		_loginKey2 = readD();
		// ignore the rest
		_buf.clear();
	}

	@Override
	public void runImpl()
	{
		final SessionKey key = new SessionKey(_loginKey1, _loginKey2, _playKey1, _playKey2);

		final L2GameClient client = getClient();

		client.setSessionId(key);

		if(client.getLoginName() == null)
		{
			client.setLoginName(_loginName);
			LSConnection.getInstance().addWaitingClient(client);
		}

		if(Config.GG_CHECK)
		{
			client.sendPacket(Msg.GameGuardQuery);
			L2GameThreadPools.getInstance().scheduleAi(new GGTest(client), 500, true);
		}
	}

	@Override
	public String getType()
	{
		return _C__08_AUTHLOGIN;
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}

	private class GGTest implements Runnable
	{
		private final L2GameClient targetClient;

		private int attempts = 0;

		public GGTest(final L2GameClient targetClient)
		{
			this.targetClient = targetClient;
		}

		@Override
		public void run()
		{
			if(!targetClient.isGameGuardOk())
				if(attempts < 3)
				{
					targetClient.sendPacket(Msg.GameGuardQuery);
					attempts++;
					L2GameThreadPools.getInstance().scheduleGeneral(this, 500 * attempts);
				}
				else
					targetClient.closeNow(false);
		}
	}
}
