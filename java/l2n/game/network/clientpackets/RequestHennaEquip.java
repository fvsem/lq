package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.HennaTable;
import l2n.game.tables.HennaTreeTable;
import l2n.game.templates.L2Henna;

public class RequestHennaEquip extends L2GameClientPacket
{
	private static String _C__6F_RequestHennaEquip = "[C] 6F RequestHennaEquip";
	private int _symbolId;

	/**
	 * packet type id 0x6F
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_symbolId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Henna template = HennaTable.getInstance().getTemplate(_symbolId);
		if(template == null)
			return;

		final L2HennaInstance temp = new L2HennaInstance(template);

		boolean cheater = true;
		for(final L2HennaInstance h : HennaTreeTable.getInstance().getAvailableHenna(activeChar.getClassId(), activeChar.getSex()))
			if(h.getSymbolId() == temp.getSymbolId())
			{
				cheater = false;
				break;
			}

		if(cheater)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_SYMBOL_CANNOT_BE_DRAWN));
			return;
		}

		final PcInventory inventory = activeChar.getInventory();
		final L2ItemInstance item = inventory.getItemByItemId(temp.getItemIdDye());
		if(item != null && item.getCount() >= temp.getAmountDyeRequire() && activeChar.getAdena() >= temp.getPrice() && activeChar.addHenna(temp))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addString(temp.getName()));
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_SYMBOL_HAS_BEEN_ADDED));
			inventory.reduceAdena(temp.getPrice());
			if(inventory.destroyItemByItemId(temp.getItemIdDye(), temp.getAmountDyeRequire(), true) == null)
				_log.info("RequestHennaEquip[50]: Item not found!!!");
		}
		else
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_SYMBOL_CANNOT_BE_DRAWN));
	}

	@Override
	public String getType()
	{
		return _C__6F_RequestHennaEquip;
	}
}
