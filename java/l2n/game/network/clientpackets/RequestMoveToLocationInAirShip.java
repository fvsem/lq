package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.vehicle.L2AirShip;
import l2n.game.model.entity.vehicle.L2Vehicle;
import l2n.game.model.entity.vehicle.L2VehicleManager;
import l2n.game.network.serverpackets.ExMoveToLocationInAirShip;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.templates.L2Weapon;
import l2n.util.Location;

/**
 * @author L2System Project
 * @date 10.12.2009
 * @time 22:44:58
 */
public class RequestMoveToLocationInAirShip extends L2GameClientPacket
{
	private static final String _C__D0_20_MOVETOLOCATIONINAIRSHIP = "[C] D0:20 MoveToLocationInAirShip";

	private int shipId;
	private Location targetLoc;
	private Location originLoc;

	@Override
	protected void readImpl()
	{
		shipId = readD();
		targetLoc = new Location(readD(), readD(), readD());
		originLoc = new Location(readD(), readD(), readD());
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;
		else if(player.isAttackingNow() && player.getActiveWeaponItem() != null && player.getActiveWeaponItem().getItemType() == L2Weapon.WeaponType.BOW)
			player.sendActionFailed();
		else
		{
			// Проверим координаты по Z
			if(originLoc.getZ() < -16384 || originLoc.getZ() > 16384 || originLoc.getZ() < -16384 || originLoc.getZ() > 16384)
			{
				player.logout(false, false, true, true);
				return;
			}

			if(player.getPet() != null)
			{
				player.sendPacket(new SystemMessage(SystemMessage.BECAUSE_PET_OR_SERVITOR_MAY_BE_DROWNED_WHILE_THE_BOAT_MOVES_PLEASE_RELEASE_THE_SUMMON_BEFORE_DEPARTURE));
				player.sendActionFailed();
				return;
			}

			if(player.getTransformationId() != 0)
			{
				player.sendPacket(new SystemMessage(SystemMessage.YOU_CANNOT_BOARD_A_SHIP_WHILE_YOU_ARE_POLYMORPHED));
				player.sendActionFailed();
				return;
			}

			if(player.isMovementDisabled() || player.isSitting())
			{
				player.sendActionFailed();
				return;
			}

			final L2Vehicle boat = L2VehicleManager.getInstance().getBoat(shipId);
			if(boat == null)
			{
				player.sendActionFailed();
				return;
			}

			if(!boat.isAirShip())
			{
				player.sendActionFailed();
				return;
			}

			if(!player.isInVehicle() || player.getVehicle() != boat)
				player.setVehicle(boat);

			player.setInVehiclePosition(targetLoc);

			player.broadcastPacket(new ExMoveToLocationInAirShip(player, (L2AirShip) boat, originLoc, targetLoc));
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_20_MOVETOLOCATIONINAIRSHIP;
	}
}
