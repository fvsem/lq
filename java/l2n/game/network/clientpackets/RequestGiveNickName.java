package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;

public class RequestGiveNickName extends L2GameClientPacket
{
	private static String _C__0B_REQUESTGIVENICKNAME = "[C] 0B RequestGiveNickName";
	// Format: cSS

	private String _target;
	private String _title;

	@Override
	public void readImpl()
	{
		_target = readS();
		_title = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!_title.equals("") && !Config.CLAN_TITLE_TEMPLATE.matcher(_title).matches())
		{
			activeChar.sendMessage("Incorrect title.");
			return;
		}

		// Дворяне могут устанавливать/менять себе title
		if(activeChar.isNoble() && _target.matches(activeChar.getName()))
		{
			activeChar.setTitle(_title);
			activeChar.sendPacket(Msg.TITLE_HAS_CHANGED);
			activeChar.sendChanges();
			return;
		}
		// Can the player change/give a title?
		else if((activeChar.getClanPrivileges() & L2Clan.CP_CL_GIVE_TITLE) != L2Clan.CP_CL_GIVE_TITLE)
			return;

		if(activeChar.getClan().getLevel() < 3)
		{
			activeChar.sendPacket(Msg.TITLE_ENDOWMENT_IS_ONLY_POSSIBLE_WHEN_CLANS_SKILL_LEVELS_ARE_ABOVE_3);
			return;
		}

		final L2ClanMember member = activeChar.getClan().getClanMember(_target);
		if(member != null)
		{
			member.setTitle(_title);
			if(member.isOnline())
			{
				member.getPlayer().sendPacket(Msg.TITLE_HAS_CHANGED);
				member.getPlayer().sendChanges();
			}
		}
		else
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestGiveNickName.NotInClan", activeChar));
	}

	@Override
	public String getType()
	{
		return _C__0B_REQUESTGIVENICKNAME;
	}
}
