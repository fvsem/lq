package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.network.serverpackets.ExEnchantSkillInfoDetail;
import l2n.game.tables.SkillTreeTable;

import java.util.logging.Logger;

public final class RequestExEnchantSkillInfoDetail extends L2GameClientPacket
{
	protected static final Logger _log = Logger.getLogger(RequestExEnchantSkillInfoDetail.class.getName());

	private static final int TYPE_NORMAL_ENCHANT = 0;
	private static final int TYPE_SAFE_ENCHANT = 1;
	private static final int TYPE_UNTRAIN_ENCHANT = 2;
	private static final int TYPE_CHANGE_ENCHANT = 3;

	private int _type;
	private int _skillId;
	private int _skillLvl;

	@Override
	protected void readImpl()
	{
		_type = readD();
		_skillId = readD();
		_skillLvl = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		int reqSkillLvl = -2;

		if(_type == TYPE_NORMAL_ENCHANT || _type == TYPE_SAFE_ENCHANT)
			reqSkillLvl = _skillLvl - 1; // enchant
		else if(_type == TYPE_UNTRAIN_ENCHANT)
			reqSkillLvl = _skillLvl + 1; // untrain
		else if(_type == TYPE_CHANGE_ENCHANT)
			reqSkillLvl = _skillLvl; // change route

		final int playerSkillLvl = activeChar.getSkillLevel(_skillId);
		if(playerSkillLvl == -1) // dont have such skill
			return;

		// if reqlvl is 100,200,.. check base skill lvl enchant
		if(reqSkillLvl % 100 == 0)
		{
			final L2EnchantSkillLearn esl = SkillTreeTable.getInstance().getSkillEnchant(_skillId, _skillLvl);
			if(esl != null)
			{
				// if player dont have min level to enchant
				if(playerSkillLvl != esl.getBaseLevel())
					return;
			}
			// enchant data dont exist?
			else
				return;
		}
		else if(playerSkillLvl != reqSkillLvl)
			// change route is different skill lvl but same enchant
			if(_type == TYPE_CHANGE_ENCHANT && playerSkillLvl % 100 != _skillLvl % 100)
				return;

		// send skill enchantment detail
		activeChar.sendPacket(new ExEnchantSkillInfoDetail(_type, _skillId, _skillLvl, activeChar));
	}

	@Override
	public String getType()
	{
		return "[C] D0:31 RequestExEnchantSkillInfoDetail";
	}
}
