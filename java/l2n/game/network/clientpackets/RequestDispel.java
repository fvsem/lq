package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.SkillType;
import l2n.game.model.actor.L2Player;

public class RequestDispel extends L2GameClientPacket
{
	private static final String _C__D0_78_REQUESTDISPEL = "[C] D0 4E RequestDispel";

	private int _skillId;
	private int _skillLevel;

	@Override
	protected void readImpl()
	{
		_skillId = readD();
		_skillLevel = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		// нельзя снимать альткликом эффект клансуммона
		// (КЛ должен оставаться недвижим во время работы врат клансуммона)
		if(_skillId == 3632)
			return;

		L2Skill s;
		for(final L2Effect e : cha.getEffectList().getEffects())
		{
			if(e == null)
				continue;
			s = e.getSkill();
			if(e.getDisplayId() == _skillId && e.getDisplayLevel() == _skillLevel)
				if(cha.isGM() || !e.isOffensive() && (!s.isDanceSong() || Config.SKILLS_ALLOW_DISPEL_DANSSONG) && s.getSkillType() != SkillType.TRANSFORMATION)
					e.exit();
				else
					return;
		}
	}

	@Override
	public String getType()
	{
		return _C__D0_78_REQUESTDISPEL;
	}
}
