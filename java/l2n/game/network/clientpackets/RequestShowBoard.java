package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.communitybbs.CommunityBoard;
import l2n.game.model.actor.L2Player;

/**
 * This class ...
 * 
 * @version $Revision: 1.2.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestShowBoard extends L2GameClientPacket
{
	private static String _C__5E_REQUESTSHOWBOARD = "[C] 5E RequestShowBoard";

	@SuppressWarnings("unused")
	private int _unknown;

	/**
	 * packet type id 0x5E
	 * sample
	 * 5E
	 * 01 00 00 00
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_unknown = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		CommunityBoard.handleCommands(getClient(), Config.BBS_DEFAULT);
	}

	@Override
	public String getType()
	{
		return _C__5E_REQUESTSHOWBOARD;
	}
}
