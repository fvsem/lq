package l2n.game.network.clientpackets;

/**
 * format: chS
 */
public class RequestPCCafeCouponUse extends L2GameClientPacket
{
	private static String _C__D0_19_REQUESTPCCAFECOUPONUSE = "[C] D0:19 RequestPCCafeCouponUse";
	// format: (ch)S
	private String _unknown;

	@Override
	public void readImpl()
	{
		_unknown = readS();
	}

	@Override
	public void runImpl()
	{
		System.out.println("Unfinished packet: " + _C__D0_19_REQUESTPCCAFECOUPONUSE);
		System.out.println("  S: " + _unknown);
	}

	@Override
	public String getType()
	{
		return _C__D0_19_REQUESTPCCAFECOUPONUSE;
	}
}
