package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.Elementals;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.PcInventory;
import l2n.game.network.serverpackets.ExShowBaseAttributeCancelWindow;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.util.Log;

public class RequestExRemoveItemAttribute extends L2GameClientPacket
{
	private static String _C__D0_23_REQUESTEXREMOVEITEMATTRIBUTE = "[C] D0:23 RequestExRemoveItemAttribute";
	// Format: chd
	private int _objectId;
	public static final int UNENCHANT_PRICE = 50000;

	@Override
	public void readImpl()
	{
		_objectId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isOutOfControl() || activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		final PcInventory inventory = activeChar.getInventory();
		final L2ItemInstance itemToUnnchant = inventory.getItemByObjectId(_objectId);

		if(itemToUnnchant == null || itemToUnnchant.getAttributeElementValue() == 0 || activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.getAdena() < UNENCHANT_PRICE)
		{
			activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			activeChar.sendActionFailed();
			return;
		}

		activeChar.reduceAdena(UNENCHANT_PRICE, true);

		boolean equipped = false;
		if(equipped = itemToUnnchant.isEquipped())
		{
			activeChar.getInventory().setRefreshingListeners(true);
			activeChar.getInventory().unEquipItem(itemToUnnchant);
		}

		itemToUnnchant.setAttributeElement(Elementals.ATTRIBUTE_NONE, 0, true);

		if(equipped)
		{
			activeChar.getInventory().equipItem(itemToUnnchant, false);
			activeChar.getInventory().setRefreshingListeners(false);
		}

		activeChar.sendPacket(new InventoryUpdate(itemToUnnchant, L2ItemInstance.MODIFIED));
		activeChar.sendPacket(new ExShowBaseAttributeCancelWindow(activeChar, UNENCHANT_PRICE));
		activeChar.updateStats();

		Log.add(activeChar.getName() + "|Successfully unenchanted attribute|" + itemToUnnchant.getItemId(), "enchants");
		Log.LogItem(activeChar, Log.EnchantItem, itemToUnnchant);
	}

	@Override
	public String getType()
	{
		return _C__D0_23_REQUESTEXREMOVEITEMATTRIBUTE;
	}
}
