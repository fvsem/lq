package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExPutCommissionResultForVariationMake;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestConfirmGemStone extends AbstractRefinePacket
{
	private static String _C__D0_28_REQUESTCONFIRMGEMSTONEITEM = "[C] D0:28 RequestConfirmGemStone";
	// format: (ch)dddd
	private int _targetItemObjId;
	private int _refinerItemObjId;
	private int _gemStoneItemObjId;
	private long _gemStoneCount;

	@Override
	public void readImpl()
	{
		_targetItemObjId = readD();
		_refinerItemObjId = readD();
		_gemStoneItemObjId = readD();
		_gemStoneCount = readQ();
	}

	@Override
	public void runImpl()
	{
		if(_gemStoneCount <= 0)
			return;

		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2ItemInstance targetItem = activeChar.getInventory().getItemByObjectId(_targetItemObjId);
		final L2ItemInstance refinerItem = activeChar.getInventory().getItemByObjectId(_refinerItemObjId);
		final L2ItemInstance gemStoneItem = activeChar.getInventory().getItemByObjectId(_gemStoneItemObjId);

		if(targetItem == null || refinerItem == null || gemStoneItem == null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS));
			activeChar.sendActionFailed();
			return;
		}

		if(!isValid(activeChar))
		{
			activeChar.sendActionFailed();
			return;
		}
		// Make sure the item is a gemstone
		if(!isValid(activeChar, targetItem, refinerItem, gemStoneItem))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THIS_IS_NOT_A_SUITABLE_ITEM));
			activeChar.sendActionFailed();
			return;
		}

		// Check for gemstone count
		final LifeStone ls = getLifeStone(refinerItem.getItemId());
		if(ls == null)
			return;

		if(_gemStoneCount != getGemStoneCount(targetItem.getItem().getItemGrade(), ls.getGrade()))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.GEMSTONE_QUANTITY_IS_INCORRECT));
			return;
		}

		activeChar.sendPacket(new ExPutCommissionResultForVariationMake(_gemStoneItemObjId, _gemStoneCount, gemStoneItem.getItemId()));
		activeChar.sendPacket(new SystemMessage(SystemMessage.PRESS_THE_AUGMENT_BUTTON_TO_BEGIN));
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return _C__D0_28_REQUESTCONFIRMGEMSTONEITEM;
	}
}
