package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleManager;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.instancemanager.FortressManager;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.residence.Fortress;
import l2n.game.model.entity.residence.Residence;

public class RequestJoinSiege extends L2GameClientPacket
{
	private static String _C__AD_RequestJoinSiege = "[C] AD RequestJoinSiege";
	// format: cddd

	private int _id;
	private boolean _isAttacker;
	private boolean _isJoining;

	@Override
	public void readImpl()
	{
		_id = _buf.hasRemaining() ? readD() : 0;
		_isAttacker = readD() == 1;
		_isJoining = readD() == 1;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if((activeChar.getClanPrivileges() & L2Clan.CP_CS_MANAGE_SIEGE) != L2Clan.CP_CS_MANAGE_SIEGE)
		{
			activeChar.sendPacket(Msg.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
			return;
		}

		Residence siegeUnit = CastleManager.getInstance().getCastleByIndex(_id);
		if(siegeUnit == null)
			siegeUnit = FortressManager.getInstance().getFortressByIndex(_id);
		if(siegeUnit == null)
			siegeUnit = ClanHallManager.getInstance().getClanHall(_id);

		if(siegeUnit != null && siegeUnit.getSiege() != null)
		{
			if(_isJoining)
			{
				switch (siegeUnit.getType())
				{
					case Clanhall:
						for(final ClanHall temp : ClanHallManager.getInstance().getClanHalls().values())
							if(temp.getSiege() != null && temp.getSiege().checkIsClanRegistered(activeChar.getClan()))
							{
								activeChar.sendPacket(Msg.YOUR_APPLICATION_HAS_BEEN_DENIED_BECAUSE_YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_FOR_ANOTHER_SIEGE_BATTLE);
								return;
							}
						break;
					case Castle:
						for(final Castle temp : CastleManager.getInstance().getCastles().values())
							if(temp.getSiege().checkIsClanRegistered(activeChar.getClan()) && siegeUnit.getSiegeDayOfWeek() == temp.getSiegeDayOfWeek() && siegeUnit.getSiegeHourOfDay() == temp.getSiegeHourOfDay())
							{
								activeChar.sendPacket(Msg.YOUR_APPLICATION_HAS_BEEN_DENIED_BECAUSE_YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_FOR_ANOTHER_SIEGE_BATTLE);
								return;
							}
						break;
					case Fortress:
						for(final Fortress fort : FortressManager.getInstance().getFortresses().values())
							if(fort.getSiege().checkIsClanRegistered(activeChar.getClan()))
							{
								activeChar.sendPacket(Msg.YOUR_APPLICATION_HAS_BEEN_DENIED_BECAUSE_YOU_HAVE_ALREADY_SUBMITTED_A_REQUEST_FOR_ANOTHER_SIEGE_BATTLE);
								return;
							}
						break;
				}

				if(_isAttacker)
					siegeUnit.getSiege().registerAttacker(activeChar);
				else
					siegeUnit.getSiege().registerDefender(activeChar);
			}
			else
				siegeUnit.getSiege().clearSiegeClan(activeChar.getClan(), false);
			siegeUnit.getSiege().listRegisterClan(activeChar);
		}
	}

	@Override
	public String getType()
	{
		return _C__AD_RequestJoinSiege;
	}
}
