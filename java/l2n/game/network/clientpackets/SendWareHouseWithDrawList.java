package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.items.ClanWarehousePool;
import l2n.game.model.items.Warehouse;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.network.serverpackets.ItemList;
import l2n.util.IllegalPlayerAction;
import l2n.util.Util;

import java.util.logging.Level;

public class SendWareHouseWithDrawList extends L2GameClientPacket
{
	private final static String _C__3C_SENDWAREHOUSEWITHDRAWLIST = "[C] 3C SendWareHouseWithDrawList";

	/** Сколько позиций мы берем из хранилища */
	private int _count;
	/** Содержит в себе последовательность идентификатора предмета и количества - сколько надо взять. */
	private long[] _items;
	/** Содержит в себе сколько каждого элемента взять из хранилища */
	private long[] counts;

	@Override
	public void readImpl()
	{
		_count = readD();
		if(_count < 1 || _count > Config.MAX_ITEM_IN_PACKET || _count * 12 != _buf.remaining())
		{
			_items = null;
			return;
		}
		_items = new long[_count * 2];
		counts = new long[_count];
		for(int i = 0; i < _count; i++)
		{

			final int objId = readD();
			final long cnt = readQ();
			if(objId < 1 || cnt < 0)
			{
				_items = null;
				return;
			}
			_items[i * 2 + 0] = objId; // item object id
			_items[i * 2 + 1] = cnt; // count
		}
	}

	@Override
	public void runImpl()
	{
		// ничего не нужно - уходим
		if(_items == null || _count < 1)
			return;

		// а куда подевался игрок - без него никак
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		// Проверяем не торгует ли уже чар.
		if(player.isInStoreMode())
		{
			player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		// Во время обмена невозможно
		if(player.isInTransaction() && player.getTransaction().isTypeOf(TransactionType.TRADE))
		{
			player.sendActionFailed();
			return;
		}

		// а есть ли у нас права запускать свою лапу в хранилище клана? нету - в угол
		if(!Config.ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE && player.getUsingWarehouseType() == WarehouseType.CLAN && player.getClan().getLeaderId() != player.getObjectId())
			return;

		// а есть ли у нас права запускать свою лапу в хранилище клана? нету - в угол
		if(player.getUsingWarehouseType() == WarehouseType.CLAN && !((player.getClanPrivileges() & L2Clan.CP_CL_VIEW_WAREHOUSE) == L2Clan.CP_CL_VIEW_WAREHOUSE))
			return;

		// совокупный вес того, что мы берем из хранилища
		int weight = 0;

		// сколько слотов сейчас занято в сумке
		int finalCount = player.getInventory().getSize();

		// вещи, которые тырим
		final int[] olditems = new int[_count];

		for(int i = 0; i < _count; i++)
		{
			// идентификатор очередного предмета, подлежащего изъятию из хранилища
			final int itemObjId = (int) _items[i * 2 + 0];
			if(itemObjId <= 0)
			{
				_log.log(Level.WARNING, "Error withdrawing a warehouse object for char [" + player + "] (validity check [objectId])");
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
				continue;
			}

			// в каком количестве изымаем
			long count = _items[i * 2 + 1];
			// если количество указано неверное - то посылаем и с чуством выполненного долга завершаем работу
			if(count <= 0)
			{
				Util.handleIllegalPlayerAction(player, "SendWareHouseWithDrawList[105]", "Character " + player.getName() + " at account " + player.getAccountName() + " try store 0 or less items", IllegalPlayerAction.WARNING);
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
				return;
			}

			// получили предмет по его идентификатору.

			// В принципе - в памяти он есть...поскольку перед отдачей списка вещей в хранилище - мы его загрузили туда.
			// Здесь мы грузим последнюю версию на всякий случай, чтобы быть твердо уверенными в том, что у нас последнее
			// состояние объекта. Вдруг это клановое хранилице и пока ты примерялся - все уже расхватали, но где-то был
			// косяк в скриптах и в памяти неккоректный объект лежит.
			final L2ItemInstance oldinst = L2ItemInstance.restoreFromDb(itemObjId, false);
			// если такого предмета нету в игре....
			if(oldinst == null)
			{
				// посылаем игроку сообщение о том, что кто-то изменил клановое хранилище
				player.sendMessage(new CustomMessage("l2n.game.clientpackets.SendWareHouseWithDrawList.Changed", player));
				return;
			}

			// Проверка если человек хочет забрать итем их КВХ после того как вышел из клана
			if(oldinst.getLocation() == ItemLocation.CLANWH && player.getClan() == null || oldinst.getLocation() == ItemLocation.CLANWH && oldinst.getOwnerId() != player.getClan().getClanId())
			{
				Util.handleIllegalPlayerAction(player, "[C] SendWareHouseWithDrawList 180", "Tried to get not owned items", IllegalPlayerAction.CRITICAL);
				return;
			}

			// если количство предметов менее заявленного для изъятия - изымаем столько сколько есть
			if(oldinst.getCount() < count)
				count = oldinst.getCount();

			// сколько будем изымать
			counts[i] = count;
			// что будем изымать
			olditems[i] = oldinst.getObjectId();

			// подсчитываем общий вес для изъятия
			weight += oldinst.getItem().getWeight() * count;

			// виртуально занимаем один из слотов в сумочке
			finalCount++;

			// если вещь кучкуется и такая вещь уже есть в багаже - говорим что были неправы для предыдущей операции
			if(oldinst.getItem().isStackable() && player.getInventory().getItemByItemId(oldinst.getItemId()) != null)
				finalCount--;
		}

		// если у чувака не хватает слотов - в сад
		if(!player.getInventory().validateCapacity(finalCount))
		{
			// посылаем игрока
			player.sendPacket(Msg.YOUR_INVENTORY_IS_FULL);
			return;
		}

		// если чувак не может поднять вес - в сад
		if(!player.getInventory().validateWeight(weight))
		{
			// посылаем игрока
			player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_WEIGHT_LIMIT);
			return;
		}

		Warehouse warehouse = null;
		// если это клановое хранилище - добавляем процесс затаривания из него в отдельный поток и
		// про него забываем. Хотя, мне между делом показалось, что там происходит тот-же процесс что и здесь.
		if(player.getUsingWarehouseType() == WarehouseType.CLAN)
		{
			ClanWarehousePool.getInstance().AddWork(player, olditems, counts);
			return;
		}

		// получаем тип хранилища
		switch (player.getUsingWarehouseType())
		{
			case PRIVATE:
				warehouse = player.getWarehouse();
				break;
			default:
				// Something went wrong!
				_log.warning("Error retrieving a warehouse object for char " + player.getName() + " - using warehouse type: " + player.getUsingWarehouseType());
				return;
		}

		/**
		 * И наконец-то, под звуки фанфар, мы отдаем страждущему желаемое
		 */
		for(int i = 0; i < olditems.length; i++)
		{
			// предмет, который мы забираем
			final L2ItemInstance transfer_item = warehouse.getItemByObj(olditems[i], counts[i]);
			// почему-то не смогли найти такой предмет в хранилище
			if(transfer_item == null)
				continue;

			// добавляем предмет персонажу
			player.getInventory().addItem(transfer_item);
		}

		// посылаем клиенту информацию о том, какой он стал теперь
		player.sendChanges();
		// посылаем клиенту информацию о том, что он имеет, но чтобы окно с репозитарием не открывалось
		player.sendPacket(new ItemList(player, false));
	}

	@Override
	public String getType()
	{
		return _C__3C_SENDWAREHOUSEWITHDRAWLIST;
	}
}
