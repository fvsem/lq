package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.ExSendManorList;

/**
 * Format: ch
 * c (id) 0xD0
 * h (subid) 0x01
 */
public class RequestManorList extends L2GameClientPacket
{
	private static final String _C__D0_01_REQUESTMANORLIST = "[S] D0:01 RequestManorList";

	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		getClient().sendPacket(new ExSendManorList());
	}

	@Override
	public String getType()
	{
		return _C__D0_01_REQUESTMANORLIST;
	}
}
