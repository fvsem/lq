package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.L2TradeList;
import l2n.game.model.TradeItem;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * Список продаваемого в приватный магазин покупки
 */
public class SendPrivateStoreBuyList extends L2GameClientPacket
{
	private final static String _C__9F_SENDPRIVATESTOREBUYBUYLIST = "[C] 9F SendPrivateStoreBuyBuyList";
	// format: cddb, b - array of (ddhhdd)
	private final static Logger _log = Logger.getLogger(SendPrivateStoreBuyList.class.getName());

	private int _buyerID;
	private L2Player _buyer;
	private L2Player _seller;
	private int _count;
	private final ConcurrentLinkedQueue<TradeItem> _sellerlist = new ConcurrentLinkedQueue<TradeItem>();
	private ConcurrentLinkedQueue<TradeItem> _buyerlist = new ConcurrentLinkedQueue<TradeItem>();

	private int _fail = 0;
	private boolean seller_fail = false;

	@Override
	public void readImpl()
	{
		_buyerID = readD();
		_seller = getClient().getActiveChar();
		_buyer = (L2Player) _seller.getVisibleObject(_buyerID);
		_count = readD();

		if(_count <= 0 || _count > Config.MAX_ITEM_IN_PACKET || _count * 28 != _buf.remaining())
		{
			seller_fail = true;
			return;
		}

		if(_seller == null || _buyer == null || _seller.getDistance3D(_buyer) > L2Character.INTERACTION_DISTANCE)
		{
			_fail = 1;
			return;
		}

		if(_buyer.getTradeList() == null)
		{
			_fail = 2;
			return;
		}

		if(!_seller.getPlayerAccess().UseTrade)
		{
			_seller.sendPacket(Msg.THIS_ACCOUNT_CANOT_USE_PRIVATE_STORES);
			_fail = 1;
			return;
		}

		_buyerlist = _buyer.getBuyList();

		long sum = 0;
		TradeItem temp;
		L2ItemInstance SIItem;
		for(int i = 0; i < _count; i++)
		{
			temp = new TradeItem();

			readD(); // ObjectId, не работает

			temp.setItemId(readD());

			readH(); // TODO analyse this
			readH(); // TODO analyse this

			temp.setCount(readQ());
			temp.setOwnersPrice(readQ());

			if(temp.getItemId() < 1 || temp.getCount() < 1 || temp.getOwnersPrice() < 1)
			{
				_seller.sendPacket(Msg.ActionFail, Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
				_fail = 1;
				return;
			}

			sum += temp.getCount() * temp.getOwnersPrice();

			SIItem = _seller.getInventory().getItemByItemId(temp.getItemId());
			if(SIItem == null)
			{
				_log.warning("Player " + _seller.getName() + " tries to sell to PSB:" + _buyer.getName() + " item not in inventory");
				continue;
			}

			if(SIItem.isEquipped())
			{
				_seller.sendPacket(Msg.ActionFail, Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
				_fail = 1;
				return;
			}

			temp.setObjectId(SIItem.getObjectId());

			if(temp.getCount() > SIItem.getCount())
				temp.setCount(SIItem.getCount());

			temp.setEnchantLevel(SIItem.getEnchantLevel());
			temp.setCustomType2(SIItem.getItem().getType2ForPackets());
			temp.setAttackElement(SIItem.getAttackElement());
			temp.setDefenceFire(SIItem.getDefenceFire());
			temp.setDefenceWater(SIItem.getDefenceWater());
			temp.setDefenceWind(SIItem.getDefenceWind());
			temp.setDefenceEarth(SIItem.getDefenceEarth());
			temp.setDefenceHoly(SIItem.getDefenceHoly());
			temp.setDefenceUnholy(SIItem.getDefenceUnholy());

			_sellerlist.add(temp);
		}

		if(sum < 0 || sum > _buyer.getAdena())// если у продавца не хватает денег - снимать с трейда, ибо нефиг
		{
			_seller.sendPacket(Msg.ActionFail, Msg.THE_ATTEMPT_TO_SELL_HAS_FAILED);
			_fail = 2;
			return;
		}

		_fail = 0;
	}

	@Override
	public void runImpl()
	{
		if(seller_fail || _buyer == null)
		{
			if(_seller != null)
				_seller.sendActionFailed();
			return;
		}

		if(_fail == 2)
		{
			L2TradeList.cancelStore(_buyer);
			return;
		}

		_buyer.getTradeList();
		if(_fail == 1 || _buyer.getPrivateStoreType() != L2Player.STORE_PRIVATE_BUY || !_buyer.getTradeList().buySellItems(_buyer, _buyerlist, _seller, _sellerlist))
		{
			_seller.sendActionFailed();
			return;
		}
//
// if(_buyer.getPrivateStoreType() != L2Player.STORE_PRIVATE_BUY)
// {
// L2TradeList.cancelStore(_buyer);
// return;
// }
//
// if(_fail == 1 || _buyer.getPrivateStoreType() != L2Player.STORE_PRIVATE_BUY)
// {
// _seller.sendActionFailed();
// L2TradeList.cancelStore(_buyer);
// return;
// }

		_buyer.saveTradeList();

		// на всякий случай немедленно сохраняем все изменения
		for(final L2ItemInstance i : _buyer.getInventory().getItemsList())
			i.updateDatabase(true);
		for(final L2ItemInstance i : _seller.getInventory().getItemsList())
			i.updateDatabase(true);

		if(_buyer.getBuyList().isEmpty())
			L2TradeList.cancelStore(_buyer);

		_buyer.updateStats();
		_seller.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__9F_SENDPRIVATESTOREBUYBUYLIST;
	}
}
