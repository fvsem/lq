package l2n.game.network.clientpackets;

import l2n.game.cache.CrestCache;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestSetPledgeCrest extends L2GameClientPacket
{
	private final static String _C__09_REQUESTSETPLEDGECREST = "[C] 09 RequestSetPledgeCrest";

	private byte[] _data;

	@Override
	public void readImpl()
	{
		final int size = readD();
		if(size == CrestCache.CREST_CLAN_NORMAL_SIZE)
		{
			_data = new byte[CrestCache.CREST_CLAN_NORMAL_SIZE];
			readB(_data);
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if((activeChar.getClanPrivileges() & L2Clan.CP_CL_REGISTER_CREST) == L2Clan.CP_CL_REGISTER_CREST)
		{
			if(clan.getLevel() < 3)
			{
				activeChar.sendPacket(new SystemMessage(SystemMessage.CLAN_CREST_REGISTRATION_IS_ONLY_POSSIBLE_WHEN_CLANS_SKILL_LEVELS_ARE_ABOVE_3));
				return;
			}

			if(clan.hasCrest())
				CrestCache.removePledgeCrest(clan);

			if(_data != null)
				CrestCache.savePledgeCrest(clan, _data);

			clan.broadcastClanStatus(false, true, false);
		}
	}

	@Override
	public String getType()
	{
		return _C__09_REQUESTSETPLEDGECREST;
	}
}
