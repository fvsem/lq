package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ClanTable;

import java.util.logging.Logger;

public class RequestStartPledgeWar extends L2GameClientPacket
{
	private static String _C__03_REQUESTSTARTPLEDGEWAR = "[C] 03 RequestStartPledgewar";
	// Format: cS
	private static final Logger _log = Logger.getLogger(RequestStartPledgeWar.class.getName());

	private String _pledgeName;
	private L2Clan _clan;

	@Override
	public void readImpl()
	{
		_pledgeName = _buf.hasRemaining() ? readS(32) : null;
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _pledgeName == null)
			return;

		_clan = activeChar.getClan();
		if(_clan == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(!((activeChar.getClanPrivileges() & L2Clan.CP_CL_PLEDGE_WAR) == L2Clan.CP_CL_PLEDGE_WAR))
		{
			activeChar.sendActionFailed();
			return;
		}

		if(_clan.getWarsCount() >= Config.ALT_CLAN_WAR_MAX)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.A_DECLARATION_OF_WAR_AGAINST_MORE_THAN_30_CLANS_CANT_BE_MADE_AT_THE_SAME_TIME));
			activeChar.sendActionFailed();
			return;
		}

		if(_clan.getLevel() < Config.ALT_MIN_CLAN_LVL_FOR_WAR || _clan.getMembersCount() < Config.ALT_CLAN_MEMBERS_FOR_WAR)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.A_CLAN_WAR_CAN_BE_DECLARED_ONLY_IF_THE_CLAN_IS_LEVEL_THREE_OR_ABOVE_AND_THE_NUMBER_OF_CLAN_MEMBERS_IS_FIFTEEN_OR_GREATER));
			activeChar.sendActionFailed();
			return;
		}

		final L2Clan clan = ClanTable.getInstance().getClanByName(_pledgeName);
		if(clan == null)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_DECLARATION_OF_WAR_CANT_BE_MADE_BECAUSE_THE_CLAN_DOES_NOT_EXIST_OR_ACT_FOR_A_LONG_PERIOD));
			activeChar.sendActionFailed();
			return;
		}

		else if(_clan.equals(clan))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.FOOL_YOU_CANNOT_DECLARE_WAR_AGAINST_YOUR_OWN_CLAN));
			activeChar.sendActionFailed();
			return;
		}

		else if(_clan.isAtWarWith(clan.getClanId()))
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.THE_DECLARATION_OF_WAR_HAS_BEEN_ALREADY_MADE_TO_THE_CLAN));
			activeChar.sendActionFailed();
			return;
		}

		else if(_clan.getAllyId() == clan.getAllyId() && _clan.getAllyId() != 0)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.A_DECLARATION_OF_CLAN_WAR_AGAINST_AN_ALLIED_CLAN_CANT_BE_MADE));
			activeChar.sendActionFailed();
			return;
		}

		else if(clan.getLevel() < Config.ALT_MIN_CLAN_LVL_FOR_WAR || clan.getMembersCount() < Config.ALT_CLAN_MEMBERS_FOR_WAR)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.A_CLAN_WAR_CAN_BE_DECLARED_ONLY_IF_THE_CLAN_IS_LEVEL_THREE_OR_ABOVE_AND_THE_NUMBER_OF_CLAN_MEMBERS_IS_FIFTEEN_OR_GREATER));
			activeChar.sendActionFailed();
			return;
		}

		_log.info("RequestStartPledgeWar: By player: " + activeChar.getName() + " of clan: " + _clan.getName() + " to clan: " + _pledgeName);
		ClanTable.getInstance().startClanWar(activeChar.getClan(), clan);
	}

	@Override
	public String getType()
	{
		return _C__03_REQUESTSTARTPLEDGEWAR;
	}
}
