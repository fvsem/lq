/**
 * 
 */
package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 10.12.2009
 * @time 22:24:34
 */
public class MoveWithDelta extends L2GameClientPacket
{

	@Override
	public String getType()
	{
		return "[C] 0x41 MoveWithDelta";
	}

	private int _dx;
	private int _dy;
	private int _dz;

	@Override
	protected void readImpl()
	{
		_dx = readD();
		_dy = readD();
		_dz = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player cha = getClient().getActiveChar();
		if(cha == null)
			return;

		// TODO: implement (no message since this packet is sent while moving?)
		// requestFailed(SystemMessageId.NOT_WORKING_PLEASE_TRY_AGAIN_LATER);
		cha.sendActionFailed();
	}
}
