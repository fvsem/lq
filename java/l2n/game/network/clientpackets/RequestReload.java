package l2n.game.network.clientpackets;

import l2n.game.model.L2WorldRegion;
import l2n.game.model.actor.L2Player;

public class RequestReload extends L2GameClientPacket
{
	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		player.sendUserInfo(false);

		if(player.getCurrentRegion() != null)
			for(final L2WorldRegion neighbor : player.getCurrentRegion().getNeighbors())
				neighbor.showObjectsToPlayer(player);
	}

	/**
	 * just for information and debug purposes
	 * 
	 * @return text for trace message
	 */
	@Override
	public String getType()
	{
		return "[C] CF RequestRecordInfo";
	}
}
