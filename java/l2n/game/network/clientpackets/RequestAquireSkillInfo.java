package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.AcquireSkillInfo;
import l2n.game.network.serverpackets.AcquireSkillList;
import l2n.game.tables.SkillTable;

public class RequestAquireSkillInfo extends L2GameClientPacket
{
	private static final String _C__73_REQUESTACQUIRESKILLINFO = "[C] 73 RequestAquireSkillInfo";
	// format: cddd
	private int _id;
	private int _level;
	private int _skillType;

	@Override
	public void readImpl()
	{
		_id = readD();
		_level = readD();
		_skillType = readD();// normal(0) learn or fisherman(1) clan(2) ? (3) transformation (4)
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || activeChar.getTransformationId() != 0 || SkillTable.getInstance().getInfo(_id, _level) == null)
			return;
		final L2NpcInstance trainer = activeChar.getLastNpc();
		if((trainer == null || activeChar.getDistance(trainer.getX(), trainer.getY()) > L2Character.INTERACTION_DISTANCE) && !activeChar.isGM())
			return;
		if(_skillType == AcquireSkillList.CLAN || _skillType == AcquireSkillList.CLAN_ADDITIONAL)
			sendPacket(new AcquireSkillInfo(_id, _level, activeChar.getClassId(), activeChar.getClan(), _skillType));
		else
			sendPacket(new AcquireSkillInfo(_id, _level, activeChar.getClassId(), null, _skillType));
	}

	@Override
	public String getType()
	{
		return _C__73_REQUESTACQUIRESKILLINFO;
	}
}
