package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.HennaEquipList;
import l2n.game.tables.HennaTreeTable;

public class RequestHennaList extends L2GameClientPacket
{
	private static String _C__C3_RequestHennaList = "[C] C3 RequestHennaList";

	@SuppressWarnings("unused")
	private int _unknown;

	@Override
	public void readImpl()
	{
		_unknown = readD(); // ??
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		activeChar.sendPacket(new HennaEquipList(activeChar, HennaTreeTable.getInstance().getAvailableHenna(activeChar.getClassId(), activeChar.getSex())));
	}

	@Override
	public String getType()
	{
		return _C__C3_RequestHennaList;
	}
}
