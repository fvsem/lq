package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.ExJumpToLocation;

/**
 * @author L2System
 * @date 06.07.2010
 * @time 17:31:45
 */
public class RequestExJump extends L2GameClientPacket
{
	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExJumpToLocation(activeChar.getObjectId(), activeChar.getLoc(), activeChar.getLoc()));
		System.out.println(getType());
	}

	@Override
	public void readImpl()
	{}
}
