package l2n.game.network.clientpackets;

import l2n.game.model.L2Clan;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeReceiveWarList;

public class RequestPledgeWarList extends L2GameClientPacket
{
	private final static String _C__D0_17_REQUESTPLEDGEPOWER = "[C] D0:17 RequestPledgeWarList";
	// format: (ch)dd
	private int _type;
	private int _page;

	@Override
	public void readImpl()
	{
		_page = readD();
		_type = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final L2Clan clan = activeChar.getClan();
		if(clan != null)
			activeChar.sendPacket(new PledgeReceiveWarList(clan, _type, _page));
	}

	@Override
	public String getType()
	{
		return _C__D0_17_REQUESTPLEDGEPOWER;
	}
}
