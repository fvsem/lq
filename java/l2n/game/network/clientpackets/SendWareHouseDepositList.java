package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.items.PcInventory;
import l2n.game.model.items.Warehouse;
import l2n.game.model.items.Warehouse.WarehouseType;
import l2n.game.network.serverpackets.ItemList;

import java.util.HashMap;

/**
 * 31 SendWareHouseDepositList cd (dd)
 */
public class SendWareHouseDepositList extends L2GameClientPacket
{
	private static final String _C__3B_SENDWAREHOUSEDEPOSITLIST = "[C] 3B SendWareHouseDepositList";
	// Format: cdb, b - array of (dd)

	private static final int BATCH_LENGTH = 12; // length of the one item

	private static final int _WAREHOUSE_FEE = 30;
	private HashMap<Integer, Long> _items;

	@Override
	public void readImpl()
	{
		final int itemsCount = readD();
		if(itemsCount <= 0 || itemsCount > Config.MAX_ITEM_IN_PACKET || itemsCount * BATCH_LENGTH != _buf.remaining())
		{
			_items = null;
			return;
		}

		_items = new HashMap<Integer, Long>(itemsCount + 1, 0.999f);
		for(int i = 0; i < itemsCount; i++)
		{
			final int obj_id = readD();
			final long itemQuantity = readQ();
			if(obj_id < 1 || itemQuantity < 0)
			{
				_items = null;
				return;
			}
			_items.put(obj_id, itemQuantity);
		}
	}

	@Override
	public void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null || _items == null)
			return;

		// Проверяем не торгует ли уже чар.
		if(player.isInStoreMode())
		{
			player.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		// Во время обмена невозможно
		Transaction trans = player.getTransaction();
		if(trans != null && trans.isInProgress() && trans.isTrade())
		{
			player.sendActionFailed();
			trans.cancel();
			return;
		}

		Warehouse warehouse;
		final PcInventory inventory = player.getInventory();
		final boolean privatewh = player.getUsingWarehouseType() != WarehouseType.CLAN;
		int slotsleft = 0;
		long adenaDeposit = 0;

		// Список предметов, уже находящихся на складе
		L2ItemInstance[] itemsOnWarehouse;
		if(privatewh)
		{
			warehouse = player.getWarehouse();
			itemsOnWarehouse = warehouse.listItems(ItemClass.ALL);
			slotsleft = player.getWarehouseLimit() - itemsOnWarehouse.length;
		}
		else
		{
			warehouse = player.getClan().getWarehouse();
			itemsOnWarehouse = warehouse.listItems(ItemClass.ALL);
			slotsleft = player.getClan().getWhBonus() + Config.WAREHOUSE_SLOTS_CLAN - itemsOnWarehouse.length;
		}

		// Список стекуемых предметов, уже находящихся на складе
		final GArray<Integer> stackableList = new GArray<Integer>();
		for(final L2ItemInstance i : itemsOnWarehouse)
			if(i.isStackable())
				stackableList.add(i.getItemId());

		// Создаем новый список передаваемых предметов, на основе полученных данных
		final GArray<L2ItemInstance> itemsToStoreList = new GArray<L2ItemInstance>(_items.size() + 1);
		for(final Integer itemObjectId : _items.keySet())
		{
			final L2ItemInstance item = inventory.getItemByObjectId(itemObjectId);
			if(item == null || item.isEquipped() || !item.canBeStored(player, privatewh))
				continue;
			if(!privatewh && !item.canBeDropped(player)) // а его вообще положить можно?
				continue;
			if(!item.isStackable() || !stackableList.contains(item.getItemId())) // вещь требует слота
			{
				if(slotsleft <= 0) // если слоты кончились нестекуемые вещи и отсутствующие стекуемые пропускаем
					continue;
				slotsleft--; // если слот есть то его уже нет
			}

			final long itemQuantity = _items.get(itemObjectId);
			if(itemQuantity <= 0)
			{
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
				return;
			}

			if(item.getItemId() == 57)
				adenaDeposit = _items.get(itemObjectId);

			// Проверяем количество итемов
			if(_items.get(itemObjectId) > item.getCount())
				continue;

			itemsToStoreList.add(item);
		}

		// если список пуст...
		if(itemsToStoreList.isEmpty())
		{
			player.sendActionFailed();
			return;
		}

		// Проверяем, хватит ли у нас денег на уплату налога
		final long fee = itemsToStoreList.size() * _WAREHOUSE_FEE;
		if(fee + adenaDeposit > player.getAdena())
		{
			player.sendPacket(Msg.YOU_LACK_THE_FUNDS_NEEDED_TO_PAY_FOR_THIS_TRANSACTION);
			return;
		}

		// Сообщаем о том, что слоты кончились
		if(slotsleft <= 0)
		{
			player.sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
			return;
		}

		for(final L2ItemInstance itemToStore : itemsToStoreList)
			warehouse.addItem(inventory.dropItem(itemToStore, _items.get(itemToStore.getObjectId()), true), player.getName());

		// Платим налог
		player.reduceAdena(fee, true);

		// Обновляем параметры персонажа
		player.updateStats();
		player.sendPacket(new ItemList(player, false));
	}

	@Override
	public String getType()
	{
		return _C__3B_SENDWAREHOUSEDEPOSITLIST;
	}
}
