package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Skill;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2PledgeSkillLearn;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2VillageMasterInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.SkillSpellbookTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.util.IllegalPlayerAction;
import l2n.util.Util;

import java.util.logging.Level;

public class RequestAquireSkill extends L2GameClientPacket
{
	private final static String _C__7C_REQUESTACQUIRESKILL = "[C] 7C RequestAquireSkill";

	// format: cddd(d)
	private int _id;
	private int _level;
	private int _skillType;
	private int _squadIndex = -1;

	@Override
	public void readImpl()
	{
		_id = readD();
		_level = readD();
		_skillType = readD();
		if(_skillType == 3)
			_squadIndex = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null || activeChar.getTransformationId() != 0)
			return;

		if(_level < 1 || _level > 1000 || _id < 1 || _id > 32000)
		{
			_log.warning("Recived Wrong Packet Data in Aquired Skill - id:" + _id + " level:" + _level);
			return;
		}

		final L2NpcInstance trainer = activeChar.getLastNpc();
		if((trainer == null || activeChar.getDistance(trainer.getX(), trainer.getY()) > L2Character.INTERACTION_DISTANCE) && !activeChar.isGM())
			return;

		if(SkillTable.SubclassSkills.isSubclassSkill(_id))
		{
			Functions.callScripts("services.SubSkills", "learnSkill", new Object[] { activeChar, new Integer(_id) });
			return;
		}

		final L2Skill skill = SkillTable.getInstance().getInfo(_id, _level);
		if(skill == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		// already knows the skill with this level
		if(activeChar.getSkillLevel(_id) >= _level)
		{
			activeChar.sendActionFailed();
			return;
		}

		boolean isTransferSkill = false;
		boolean isSpecial = false;
		switch (_skillType)
		{
			case AcquireSkillList.BISHOP_TRANSFER:
			case AcquireSkillList.ELDER_TRANSFER:
			case AcquireSkillList.SILEN_ELDER_TRANSFER:
				isTransferSkill = true;
				break;
			case AcquireSkillList.COLLECT:
				isSpecial = true;
				break;
		}

		if(isTransferSkill && (activeChar.getLevel() < 76 || activeChar.getClassId().getLevel() < 4))
		{
			activeChar.sendMessage("You must have 3rd class change quest completed.");
			return;
		}

		L2Clan clan = null;
		if(_skillType == AcquireSkillList.CLAN || _skillType == AcquireSkillList.CLAN_ADDITIONAL)
			clan = activeChar.getClan();

		if(_squadIndex == -1 && !isTransferSkill && _level > 1 && (clan == null ? activeChar.getSkillLevel(_id) : clan.getSkillLevel(_id)) != _level - 1)
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestAquireSkill[58]", "tried to increase skill " + _id + " level to " + _level + " while having it's level " + activeChar.getSkillLevel(_id), IllegalPlayerAction.CRITICAL);
			return;
		}

		if(!isSpecial && !skill.isCommon() && !isTransferSkill && !SkillTreeTable.getInstance().isSkillPossible(activeChar, _id, _level))
		{
			Util.handleIllegalPlayerAction(activeChar, "RequestAquireSkill[64]", "tried to learn skill " + _id + " while on class " + activeChar.getActiveClass().getClassId(), IllegalPlayerAction.CRITICAL);
			return;
		}

		final L2SkillLearn skillLearn = SkillTreeTable.getInstance().getSkillLearn(_id, _level, activeChar.getClassId(), clan, isTransferSkill, _squadIndex);
		if(skillLearn == null)
		{
			_log.warning("RequestAquireSkill[95] tried to learn skill " + _id + " level " + _level + " while on class " + activeChar.getActiveClass().getClassId());
			return;
		}

		if(_skillType == AcquireSkillList.CLAN)
			learnClanSkill(skill, activeChar.getClan());
		else if(_skillType == AcquireSkillList.CLAN_ADDITIONAL)
			learnSquadSkill(skill, activeChar.getClan());
		else if(_skillType == AcquireSkillList.TRANSFORMATION)
		{
			final int _requiredSp = skillLearn.getSpCost();
			if(activeChar.getSp() >= _requiredSp)
			{
				if(skillLearn.getItemId() > 0)
				{
					final L2ItemInstance item = activeChar.getInventory().findItemByItemId(skillLearn.getItemId());
					if(item == null || item != null && item.getCount() < skillLearn.getItemCount())
					{
						activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
						return;
					}

					final L2ItemInstance ri = activeChar.getInventory().destroyItem(item, skillLearn.getItemCount(), true);
					activeChar.sendPacket(SystemMessage.removeItems(ri.getItemId(), skillLearn.getItemCount()));
				}

				activeChar.addSkill(skill, true);
				if(_requiredSp > 0)
					activeChar.setSp(activeChar.getSp() - _requiredSp);

				activeChar.sendUserInfo();
				activeChar.updateStats();
			}
			else
			{
				activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_SP_TO_LEARN_SKILLS);
				return;
			}
		}
		else if(_skillType == AcquireSkillList.BISHOP_TRANSFER || _skillType == AcquireSkillList.ELDER_TRANSFER || _skillType == AcquireSkillList.SILEN_ELDER_TRANSFER)
		{
			if(isTransferSkill)
			{
				int item_id = 0;
				switch (activeChar.getClassId())
				{
					case cardinal:
						item_id = 15307;
						break;
					case evaSaint:
						item_id = 15308;
						break;
					case shillienSaint:
						item_id = 15309;
						break;
					default:
						activeChar.sendMessage("There is no skills for your class.");
						return;
				}

				final L2ItemInstance spb = activeChar.getInventory().getItemByItemId(item_id);
				if(spb == null || spb.getCount() < 1)
				{
					activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
					return;
				}

				final L2ItemInstance ri = activeChar.getInventory().destroyItem(spb, 1, true);
				activeChar.sendPacket(SystemMessage.removeItems(ri.getItemId(), 1));

				String var = activeChar.getVar("TransferSkills" + item_id);
				if(var == null)
					var = "";
				if(!var.isEmpty())
					var = var + ";";
				var = var + skill.getId();
				activeChar.setVar("TransferSkills" + item_id, var);
			}

			activeChar.addSkill(skill, true);
			activeChar.sendUserInfo();
			activeChar.updateStats();
		}
		else if(isSpecial && _skillType == AcquireSkillList.COLLECT)
		{
			if(activeChar.getInventory().getCountOf(skillLearn.itemId) >= skillLearn.itemCount)
			{
				activeChar.destroyItemByItemId(skillLearn.itemId, skillLearn.itemCount, true);
				activeChar.sendPacket(SystemMessage.removeItems(skillLearn.itemId, skillLearn.itemCount));
				activeChar.addSkill(skill, true);
			}
			else
			{
				activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
				activeChar.sendActionFailed();
				return;
			}
		}
		else
		{
			final int _requiredSp = SkillTreeTable.getInstance().getSkillCost(activeChar, skill);

			if(activeChar.getSp() >= _requiredSp || skillLearn.common || skillLearn.transformation)
			{
				final int spb_id = SkillSpellbookTable._skillSpellbooks.get(SkillSpellbookTable.hashCode(new int[] { skill.getId(), skill.getLevel() }));
				if(spb_id > 0)
				{
					final L2ItemInstance spb = activeChar.getInventory().findItemByItemId(spb_id);
					if(spb == null || spb != null && spb.getCount() < skillLearn.itemCount)
					{
						activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
						return;
					}

					final L2ItemInstance ri = activeChar.getInventory().destroyItem(spb, skillLearn.itemCount, true);
					activeChar.sendPacket(SystemMessage.removeItems(ri.getItemId(), skillLearn.itemCount));
				}

				if(!skillLearn.common && !skillLearn.transformation)
					activeChar.setSp(activeChar.getSp() - _requiredSp);

				activeChar.addSkill(skill, true);

				activeChar.sendUserInfo();
				activeChar.updateStats();

				// update all the shortcuts to this skill
				if(_level > 1)
					for(final L2ShortCut sc : activeChar.getAllShortCuts())
						if(sc.id == _id && sc.type == L2ShortCut.TYPE_SKILL)
						{
							final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, _level);
							activeChar.sendPacket(new ShortCutRegister(newsc));
							activeChar.registerShortCut(newsc);
						}
			}
			else
			{
				activeChar.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_SP_TO_LEARN_SKILLS);
				return;
			}
		}

		if(skillLearn.common)
			activeChar.sendPacket(new ExStorageMaxCount(activeChar));

		activeChar.sendPacket(new SkillList(activeChar));

		if(trainer != null)
			if(_skillType == AcquireSkillList.USUAL || _skillType == AcquireSkillList.COLLECT)
				trainer.showSkillList(activeChar);
			else if(_skillType == AcquireSkillList.FISHING)
				trainer.showFishingSkillList(activeChar);
			else if(_skillType == AcquireSkillList.CLAN)
				trainer.showClanSkillList(activeChar);
			else if(_skillType == AcquireSkillList.CLAN_ADDITIONAL)
				trainer.showSquadSkillList(activeChar);
			else if(_skillType == AcquireSkillList.TRANSFORMATION)
				trainer.showTransformationSkillList(activeChar);
			else if(_skillType == AcquireSkillList.BISHOP_TRANSFER || _skillType == AcquireSkillList.ELDER_TRANSFER || _skillType == AcquireSkillList.SILEN_ELDER_TRANSFER)
				trainer.showTransferSkillList(activeChar);
	}

	private void learnSquadSkill(final L2Skill skill, final L2Clan clan)
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null || skill == null || clan == null)
			return;

		final L2NpcInstance trainer = player.getLastNpc();
		if(trainer == null)
			return;

		if(skill.getId() < 611 || skill.getId() > 616)
		{
			_log.log(Level.WARNING, "Warning! Player " + player.getName() + " tried to add a non-squad skill to one of his squads! ban him!");
			return;
		}

		final L2PledgeSkillLearn sqSkill = SkillTreeTable.getInstance().getSquadSkill(skill.getId(), skill.getLevel());
		if(player.getClan().getReputationScore() < sqSkill.getRepCost())
		{
			player.sendPacket(Msg.THE_ATTEMPT_TO_ACQUIRE_THE_SKILL_HAS_FAILED_BECAUSE_OF_AN_INSUFFICIENT_CLAN_REPUTATION_SCORE);
			return;
		}

		if(player.getInventory().getCountOf(sqSkill.getItemId()) >= sqSkill.getItemCount() && player.destroyItemByItemId(sqSkill.getItemId(), sqSkill.getItemCount(), true) != null)
		{
			player.getClan().incReputation(-sqSkill.getRepCost(), false, "SquadSkills");
			player.getClan().addNewSkill(skill, true, _squadIndex);
			player.sendPacket(new ExSubPledgeSkillAdd(skill.getId(), skill.getLevel(), _squadIndex));
		}
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	private void learnClanSkill(final L2Skill skill, final L2Clan clan)
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null || skill == null || clan == null)
			return;

		final L2NpcInstance trainer = player.getLastNpc();
		if(trainer == null)
			return;

		if(!(trainer instanceof L2VillageMasterInstance))
		{
			System.out.println("RequestAquireSkill.learnClanSkill, trainer isn't L2VillageMasterInstance");
			System.out.println(trainer.getName() + "[" + trainer.getNpcId() + "] Loc: " + trainer.getLoc());
			return;
		}
		if(!player.isClanLeader())
		{
			player.sendPacket(Msg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return;
		}

		final L2SkillLearn skillLearn = SkillTreeTable.getInstance().getSkillLearn(_id, _level, null, clan, false, _squadIndex);

		final int _requiredRep = SkillTreeTable.getInstance().getSkillRepCost(clan, skill);
		int itemId = 0;

		if(!Config.ALT_DISABLE_EGGS)
			itemId = skillLearn.itemId;

		if(skill.getMinPledgeClass() <= clan.getLevel() && clan.getReputationScore() >= _requiredRep)
		{
			if(itemId > 0)
			{
				final L2ItemInstance spb = player.getInventory().findItemByItemId(itemId);
				if(spb == null)
				{
					// Haven't spellbook
					player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
					return;
				}
				final L2ItemInstance ri = player.getInventory().destroyItem(spb, skillLearn.itemCount, true);
				player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(ri.getItemId()));
			}

			clan.incReputation(-_requiredRep, Config.USE_REDUCE_REPSCORE_RATE_CLAN_SKILL, "AquireSkill");
			player.sendPacket(new SystemMessage(SystemMessage.S1_POINTS_HAVE_BEEN_DEDUCTED_FROM_THE_CLAN_REPUTATION_SCORE).addNumber(_requiredRep));

			clan.addNewSkill(skill, true);
			player.sendPacket(new SystemMessage(SystemMessage.THE_CLAN_SKILL_S1_HAS_BEEN_ADDED).addSkillName(_id));

			((L2VillageMasterInstance) trainer).showClanSkillWindow(player); // Maybe we shoud add a check here...
		}
		else
		{
			player.sendPacket(Msg.THE_ATTEMPT_TO_ACQUIRE_THE_SKILL_HAS_FAILED_BECAUSE_OF_AN_INSUFFICIENT_CLAN_REPUTATION_SCORE);
			return;
		}

		// update all the shortcuts to this skill
		if(_level > 1)
			for(final L2ShortCut sc : player.getAllShortCuts())
				if(sc.id == _id && sc.type == L2ShortCut.TYPE_SKILL)
				{
					final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, _level);
					player.sendPacket(new ShortCutRegister(newsc));
					player.registerShortCut(newsc);
				}

		clan.addAndShowSkillsToPlayer(player);
	}

	@Override
	public String getType()
	{
		return _C__7C_REQUESTACQUIRESKILL;
	}
}
