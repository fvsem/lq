package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

public class RequestResetNickname extends L2GameClientPacket
{
	private static final String _C__D0_83_REQUESTRESETNICKNAME = "[C] D0 53 RequestResetNickname";

	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
			activeChar.sendMessage("RequestResetNickname");
	}

	@Override
	public String getType()
	{
		return _C__D0_83_REQUESTRESETNICKNAME;
	}
}
