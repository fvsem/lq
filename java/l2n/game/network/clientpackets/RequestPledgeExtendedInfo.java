package l2n.game.network.clientpackets;

import l2n.util.Log;

/**
 * @author L2System Project
 * @date 10.12.2009
 * @time 22:36:39
 */
public class RequestPledgeExtendedInfo extends L2GameClientPacket
{
	private String _name;

	@Override
	protected void readImpl()
	{
		_name = readS();
		Log.add("" + getType() + ": " + _name, "unknown_packets");
	}

	@Override
	protected void runImpl()
	{
		// TODO Auto-generated method stub
	}
}
