package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.network.L2GameClient;
import l2n.game.network.L2GameClient.GameClientState;
import l2n.game.network.serverpackets.CharSelected;
import l2n.util.AutoBan;
import l2n.util.Rnd;

public class CharacterSelected extends L2GameClientPacket
{
	private final static String _C__12_CHARACTERSELECTED = "[C] 12 CharacterSelected";
	private int _charSlot;

	/**
	 * @param decrypt
	 *            Format: cdhddd
	 */
	@Override
	public void readImpl()
	{
		_charSlot = readD();
	}

	@Override
	public void runImpl()
	{
		final L2GameClient client = getClient();
		if(client.getActiveChar() != null)
			return;

		final L2Player activeChar = client.loadCharFromDisk(_charSlot);
		if(activeChar == null)
			return;

		if(AutoBan.isBanned(activeChar.getObjectId()))
		{
			activeChar.setAccessLevel(-100);
			activeChar.logout(false, false, true, true);
			return;
		}

		activeChar.setCharSelectTime(); // запишем время когда игрок выбрал чара
		if(activeChar.getAccessLevel() < 0)
			activeChar.setAccessLevel(0);

		client.setState(GameClientState.IN_GAME);

		int obf_key = 0;
		if(Config.PROTOCOL_ENABLE_OBFUSCATION)
		{
			obf_key = Rnd.nextInt();
			// reset opcode obfuscation tables
			getClient().disableOpcodeObfuscation();
			getClient().enableOpcodeObfuscation(obf_key);
		}
		sendPacket(new CharSelected(activeChar, client.getSessionId().playOkID1, obf_key));
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}

	@Override
	public String getType()
	{
		return _C__12_CHARACTERSELECTED;
	}
}
