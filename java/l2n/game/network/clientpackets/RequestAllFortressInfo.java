package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.ExShowFortressInfo;

public class RequestAllFortressInfo extends L2GameClientPacket
{
	private final static String _C__D0_40_REQUESTALLFORTRESSINFO = "[C] D0:40 RequestAllFortressInfo";

	@Override
	public void readImpl()
	{
		// trigger packet
	}

	@Override
	public void runImpl()
	{
		getClient().getActiveChar().sendPacket(new ExShowFortressInfo());
	}

	@Override
	public String getType()
	{
		return _C__D0_40_REQUESTALLFORTRESSINFO;
	}
}
