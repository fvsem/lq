package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.L2Party;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSignsFestival.SevenSignsFestival;
import l2n.game.network.L2GameClient.GameClientState;
import l2n.game.network.serverpackets.CharacterSelectionInfo;
import l2n.game.network.serverpackets.RestartResponse;

public class RequestRestart extends L2GameClientPacket
{
	private static String _C__57_REQUESTRESTART = "[C] 57 RequestRestart";

	/**
	 * packet type id 0x57
	 * format: c
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isLocked())
		{
			activeChar.sendPacket(RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		if(activeChar.isInOlympiadMode())
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestRestart.Olympiad", activeChar));
			activeChar.sendPacket(RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		if(activeChar.inObserverMode())
		{
			activeChar.sendPacket(Msg.OBSERVERS_CANNOT_PARTICIPATE, RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		if(activeChar.isInCombat())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_RESTART_WHILE_IN_COMBAT, RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING, RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		if(activeChar.isBlocked() && !activeChar.isFlying())
		{
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestRestart.OutOfControl", activeChar));
			activeChar.sendPacket(RestartResponse.FAIL, Msg.ActionFail);
			return;
		}

		// Prevent player from restarting if they are a festival participant
		// and it is in progress, otherwise notify party members that the player
		// is not longer a participant.
		if(activeChar.isFestivalParticipant())
		{
			if(SevenSignsFestival.getInstance().isFestivalInitialized())
			{
				activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestRestart.Festival", activeChar));
				activeChar.sendPacket(RestartResponse.FAIL, Msg.ActionFail);
				return;
			}
			final L2Party playerParty = activeChar.getParty();

			if(playerParty != null)
				playerParty.broadcastMessageToPartyMembers(activeChar.getName() + " has been removed from the upcoming festival.");
		}

		if(getClient() != null)
			getClient().setState(GameClientState.AUTHED);
		activeChar.logout(false, true, false, true);

		// send char list
		final CharacterSelectionInfo cl = new CharacterSelectionInfo(getClient().getLoginName(), getClient().getSessionId().playOkID1);
		sendPacket(RestartResponse.OK, cl);
		getClient().setCharSelection(cl.getCharInfo());
	}

	@Override
	public boolean blockReadingUntilExecutionIsFinished()
	{
		return true;
	}

	@Override
	public String getType()
	{
		return _C__57_REQUESTRESTART;
	}
}
