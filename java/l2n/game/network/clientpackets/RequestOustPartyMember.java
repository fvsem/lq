package l2n.game.network.clientpackets;

import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;

/**
 * This class ...
 * 
 * @version $Revision: 1.3.4.2 $ $Date: 2005/03/27 15:29:30 $
 */
public class RequestOustPartyMember extends L2GameClientPacket
{
	private final static String _C__45_REQUESTOUSTPARTYMEMBER = "[C] 45 RequestOustPartyMember";
	// Format: cS

	private String _name;

	@Override
	public void readImpl()
	{
		_name = readS();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(!activeChar.isInParty() || !activeChar.getParty().isLeader(activeChar))
			return;

		if(activeChar.isInOlympiadMode())
		{
			activeChar.sendMessage("Вы не можете сейчас выйти из группы.");
			return;
		}

		final Reflection r = activeChar.getParty().getReflection();
		if(r != null && r.isDimensionalRift() && activeChar.getParty().getPlayerByName(_name).getReflection().equals(r))
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestOustPartyMember.CantOustInRift", activeChar));
		else if(r != null && !r.isDimensionalRift())
			activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestOustPartyMember.CantOustInDungeon", activeChar));
		else
		{
			L2Player member = activeChar.getParty().getPlayerByName(_name);
			if(member == null || !member.canLeaveParty())
				return;
			activeChar.getParty().oustPartyMember(member);
		}
	}

	@Override
	public String getType()
	{
		return _C__45_REQUESTOUSTPARTYMEMBER;
	}
}
