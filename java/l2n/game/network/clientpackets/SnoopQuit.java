package l2n.game.network.clientpackets;

import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

public class SnoopQuit extends L2GameClientPacket
{
	private final static String _C__B4_SNOOPQUIT = "[C] B4 SnoopQuit";
	private int _snoopId;

	/**
	 * @param buf
	 * @param client
	 *            format: cd
	 */
	@Override
	public void readImpl()
	{
		_snoopId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar != null)
		{
			final L2Object obj = L2ObjectsStorage.findObject(_snoopId);
			if(obj != null && obj.isPlayer())
			{
				((L2Player) obj).removeSnooper(activeChar.getStoredId());
				activeChar.removeSnooped(obj.getStoredId());
			}
		}
	}

	@Override
	public String getType()
	{
		return _C__B4_SNOOPQUIT;
	}
}
