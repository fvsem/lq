package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.Msg;
import l2n.game.model.L2ShortCut;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTreeTable;
import l2n.util.Log;

import java.util.logging.Logger;

public final class RequestExEnchantSkillUntrain extends L2GameClientPacket
{
	protected static final Logger _log = Logger.getLogger(RequestExEnchantSkillUntrain.class.getName());
	private int _skillId;
	private int _skillLvl;

	@Override
	protected void readImpl()
	{
		_skillId = readD();
		_skillLvl = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getTransformationId() != 0)
		{
			activeChar.sendMessage("You must leave transformation mode first.");
			return;
		}

		if(activeChar.getLevel() < 76)
		{
			sendPacket(Msg.YOU_DONT_MEET_SKILL_LEVEL_REQUIREMENTS);
			return;
		}

		if(activeChar.getClassId().getLevel() < 4)
		{
			sendPacket(Msg.NOT_COMPLETED_QUEST_FOR_SKILL_ACQUISITION);
			return;
		}

		final int currentLevel = activeChar.getSkillLevel(_skillId);
		final L2EnchantSkillLearn sl = SkillTreeTable.getInstance().getSkillEnchant(_skillId, currentLevel);
		if(sl == null)
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		if(_skillLvl % 100 == 0)
			_skillLvl = sl.getBaseLevel();

		final L2Skill skill = SkillTable.getInstance().getInfo(_skillId, _skillLvl);
		if(skill == null)
		{
			_log.warning("RequestExEnchantSkillUntrain: skillId " + _skillId + " level " + _skillLvl + " not found in Datapack.");
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		if(currentLevel - 1 != _skillLvl && (currentLevel % 100 != 1 || _skillLvl != sl.getBaseLevel()))
		{
			sendPacket(Msg.THERE_IS_NO_SKILL_THAT_ENABLES_ENCHANT);
			return;
		}

		final int[] cost = sl.getCost();
		final int requiredSp = (int) (cost[1] * 0.8 * sl.getCostMult());
		final int requiredAdena = cost[0] * sl.getCostMult();

		if(activeChar.getAdena() < requiredAdena)
		{
			sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		if(!Config.ALT_DISABLE_ENCHANT_BOOKS)
		{
			if(Functions.getItemCount(activeChar, SkillTreeTable.UNTRAIN_ENCHANT_BOOK) == 0)
			{
				activeChar.sendPacket(Msg.ITEMS_REQUIRED_FOR_SKILL_ENCHANT_ARE_INSUFFICIENT);
				return;
			}
			Functions.removeItem(activeChar, SkillTreeTable.UNTRAIN_ENCHANT_BOOK, 1);
		}

		Functions.removeItem(activeChar, 57, requiredAdena);
		activeChar.addExpAndSp(0, requiredSp, false, false);
		activeChar.disableSkill(skill, activeChar.addSkill(skill, true));
		activeChar.sendPacket(ExEnchantSkillResult.SUCCESS);

		if(_skillLvl > 100)
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.Untrain_of_enchant_skill_was_successful_Current_level_of_enchant_skill_S1_has_been_decreased_by_1);
			sm.addSkillName(_skillId, _skillLvl);
			activeChar.sendPacket(sm);
		}
		else
		{
			final SystemMessage sm = new SystemMessage(SystemMessage.Untrain_of_enchant_skill_was_successful_Current_level_of_enchant_skill_S1_became_0_and_enchant_skill_will_be_initialized);
			sm.addSkillName(_skillId, _skillLvl);
			activeChar.sendPacket(sm);
		}

		Log.add(activeChar.getName() + "[" + activeChar.getObjectId() + "]" + "|Successfully untranes|" + _skillId + "|to +" + _skillLvl + "|---", "enchant_skills");

		activeChar.sendPacket(new SkillList(activeChar));
		activeChar.sendPacket(new ExEnchantSkillInfo(_skillId, skill.getLevel()));
		updateSkillShortcuts(activeChar);
	}

	private void updateSkillShortcuts(final L2Player player)
	{
		// update all the shortcuts to this skill
		for(final L2ShortCut sc : player.getAllShortCuts())
			if(sc.id == _skillId && sc.type == L2ShortCut.TYPE_SKILL)
			{
				final L2ShortCut newsc = new L2ShortCut(sc.slot, sc.page, sc.type, sc.id, player.getSkillLevel(_skillId));
				player.sendPacket(new ShortCutRegister(newsc));
				player.registerShortCut(newsc);
			}
	}

	@Override
	public String getType()
	{
		return "[C] D0:33 RequestExEnchantSkillUntrain";
	}
}
