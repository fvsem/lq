package l2n.game.network.clientpackets;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.serverpackets.EnchantResult;
import l2n.game.network.serverpackets.InventoryUpdate;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.game.templates.L2Item.Grade;
import l2n.game.templates.L2Weapon;
import l2n.game.templates.L2Weapon.WeaponType;
import l2n.util.ArrayUtil;
import l2n.util.Log;

import static l2n.game.templates.L2Item.Grade.*;

/**
 * класс для методов в заточке предметов.<br>
 * 
 * @author L2System Project
 * @date 06.03.2010
 * @time 20:28:11
 */
public abstract class AbstractEnchantPacket extends L2GameClientPacket
{
	public static enum EnchantFormulaType
	{
		NORMAL(new NormalEnchantFormula()),
		PVP(new PvPEnchantFormula()),
		FIXED(new FixedEnchantFormula());

		private final EnchantFormula formula;

		private EnchantFormulaType(final EnchantFormula formula)
		{
			this.formula = formula;
		}

		protected EnchantFormula getFormula()
		{
			return formula;
		}
	}

	private static interface EnchantFormula
	{
		double getChance(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem, final EnchantScroll scroll);
	}

	private static final class NormalEnchantFormula implements EnchantFormula
	{
		@Override
		public double getChance(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem, final EnchantScroll scroll)
		{
			final boolean fullBody = enchantItem.getItem().getBodyPart() == L2Item.SLOT_FULL_ARMOR;
			if(enchantItem.getEnchantLevel() < Config.SAFE_ENCHANT_COMMON || fullBody && enchantItem.getEnchantLevel() < Config.SAFE_ENCHANT_FULL_BODY)
				return 100;

			final boolean isAccessory = enchantItem.getItem().getType2() == L2Item.TYPE2_ACCESSORY;
			double chance = 0;
			if(scroll._isBlessed)
			{
				// blessed scrolls does not use support items
				if(supportItem != null)
					return -1;

				if(scroll._isWeapon)
					chance = Config.ENCHANT_CHANCE_BLESSED_WEAPON;
				else if(isAccessory)
					chance = Config.ENCHANT_CHANCE_BLESSED_ACCESSORY;
				else
					chance = Config.ENCHANT_CHANCE_BLESSED_ARMOR;
			}
			else if(scroll._isCrystal)
			{
				if(scroll._isWeapon)
					chance = Config.ENCHANT_CHANCE_CRYSTAL_WEAPON;
				else if(isAccessory)
					chance = Config.ENCHANT_CHANCE_CRYSTAL_ACCESSORY;
				else
					chance = Config.ENCHANT_CHANCE_CRYSTAL_ARMOR;
			}
			else if(scroll._isWeapon)
				chance = Config.ENCHANT_CHANCE_WEAPON;
			else if(isAccessory)
				chance = Config.ENCHANT_CHANCE_ACCESSORY;
			else
				chance = Config.ENCHANT_CHANCE_ARMOR;

			chance += scroll._chanceAdd;

			if(supportItem != null)
				chance += supportItem.getChanceAdd();
			chance = chance - enchantItem.getEnchantLevel() * Config.ENCHANT_MODIFER;

			return chance;
		}
	}

	private static final class PvPEnchantFormula implements EnchantFormula
	{
		@Override
		public double getChance(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem, final EnchantScroll scroll)
		{
			final boolean fullBody = enchantItem.getItem().getBodyPart() == L2Item.SLOT_FULL_ARMOR;
			if(enchantItem.getEnchantLevel() < Config.SAFE_ENCHANT_COMMON || fullBody && enchantItem.getEnchantLevel() < Config.SAFE_ENCHANT_FULL_BODY)
				return 100;

			double chance = 0;

			final int itemType = enchantItem.getItem().getType2();
			int safeEnchantLevel = enchantItem.getItem().getBodyPart() == L2Item.SLOT_FULL_ARMOR ? Config.SAFE_ENCHANT_FULL_BODY : Config.SAFE_ENCHANT_COMMON;
			final int enchlvl = enchantItem.getEnchantLevel();
			final Grade crystaltype = enchantItem.getItem().getCrystalType();

			// для уровнения шансов дуальщиков и остальных на победу в PvP вставка SA в дули халявная
			if(itemType == L2Item.TYPE2_WEAPON && enchantItem.getItemType() == WeaponType.DUAL)
				safeEnchantLevel += 1;

			if(enchlvl > 11)
				chance = 1;
			else
			{
				// Выборка базового шанса
				if(itemType == L2Item.TYPE2_WEAPON)
				{
					final L2Weapon wepToEnchant = (L2Weapon) enchantItem.getItem();
					final boolean magewep = itemType == L2Item.TYPE2_WEAPON && crystaltype.cry >= L2Item.CRYSTAL_C && wepToEnchant.getPDamage() - wepToEnchant.getMDamage() <= wepToEnchant.getPDamage() * 0.4;
					chance = !magewep ? Config.ALT_ENCHANT_CHANCE_W : Config.ALT_ENCHANT_CHANCE_MAGE_W;

					// Штраф на двуручное оружие(немагическое)
					if(enchantItem.getItem().getBodyPart() == L2Item.SLOT_LR_HAND && enchantItem.getItem().getItemType() == WeaponType.BLUNT && !magewep)
						chance -= Config.PENALTY_TO_TWOHANDED_BLUNTS;
				}
				else
					chance = Config.ALT_ENCHANT_CHANCE_ARMOR;

				int DeltaChance = 15;
				// Основная прогрессия
				for(int i = safeEnchantLevel; i < enchlvl; i++)
				{
					if(i == safeEnchantLevel + 2)
						DeltaChance -= 5;
					if(i == safeEnchantLevel + 6)
						DeltaChance -= 5;
					chance -= DeltaChance;
				}

				// Учёт грейда
				int Delta2 = 5;
				for(int in = 0; in < crystaltype.externalOrdinal; in++)
				{
					if(in == L2Item.CRYSTAL_C)
						Delta2 -= 5;
					if(in == L2Item.CRYSTAL_B)
						Delta2 -= 5;
					if(in == L2Item.CRYSTAL_A)
						Delta2 -= 2;
					if(in == L2Item.CRYSTAL_S)
						Delta2--;
				}
				chance += Delta2;

				if(scroll._isBlessed)
					chance += 2;
				if(chance < 1)
					chance = 1;
			}

			chance += scroll._chanceAdd;

			if(supportItem != null)
				chance += supportItem.getChanceAdd();

			return chance;
		}
	}

	private static final class FixedEnchantFormula implements EnchantFormula
	{
		@Override
		public double getChance(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem, final EnchantScroll scroll)
		{
			final boolean fullBody = enchantItem.getItem().getBodyPart() == L2Item.SLOT_FULL_ARMOR;
			final int currentEnchantLevel = enchantItem.getEnchantLevel();

			double chance = 0;
			if(scroll._isWeapon)
				chance = Config.ALT_FIX_ENCHANT_CHANCE_WEAPON[currentEnchantLevel + 1];
			else if(fullBody)
				chance = Config.ALT_FIX_ENCHANT_CHANCE_ARMOR_FULLBODY[currentEnchantLevel + 1];
			else
				chance = Config.ALT_FIX_ENCHANT_CHANCE_ARMOR[currentEnchantLevel + 1];

			chance += scroll._chanceAdd;

			if(supportItem != null)
				chance += supportItem.getChanceAdd();
			return chance;
		}
	}

	public static final TIntObjectHashMap<EnchantScroll> _scrolls = new TIntObjectHashMap<EnchantScroll>();
	public static final TIntObjectHashMap<EnchantItem> _supports = new TIntObjectHashMap<EnchantItem>();
	private static final EnchantFormula enchantFormula = Config.ENCHANT_FORMULA_TYPE.getFormula();

	protected static final int NORMAL_SCROLL = 1;
	protected static final int BLESSED_SCROLL = 2;
	protected static final int CRYSTAL_SCROLL = 3;

	private static final int ENCHANT_FAIL_TYPE_1 = 1;
	private static final int ENCHANT_FAIL_TYPE_2 = 2;
	private static final int ENCHANT_FAIL_TYPE_3 = 3;
	private static final int ENCHANT_FAIL_TYPE_4 = 4;

	public static class EnchantItem
	{
		protected final boolean _isWeapon;
		protected final Grade _grade;
		protected final int _maxEnchantLevel;
		protected final int _chanceAdd;
		protected final int[] _itemIds;

		public EnchantItem(final boolean wep, final Grade type, final int level, final int chance, final int... items)
		{
			_isWeapon = wep;
			_grade = type;
			_maxEnchantLevel = level;
			_chanceAdd = chance;
			_itemIds = items;
		}

		/**
		 * Return true if support item can be used for this item
		 */
		public final boolean isValid(final L2Player activeChar, final L2ItemInstance enchantItem)
		{
			if(enchantItem == null)
				return false;

			if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
			{
				activeChar.sendPacket(Msg.YOU_CANNOT_PRACTICE_ENCHANTING_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_MANUFACTURING_WORKSHOP);
				return false;
			}

			if(activeChar.isDead())
			{
				activeChar.sendMessage("Enchanting items is not allowed during a dead.");
				return false;
			}

			// Запрет на заточку чужих вещей, баг может вылезти на серверных лагах
			if(enchantItem.getOwnerId() != activeChar.getObjectId())
				return false;

			final int type2 = enchantItem.getItem().getType2();

			// checking scroll type and configured maximum enchant level
			if(enchantItem.getItemId() != 13539) // Master Yogi's
				switch (type2)
				{
				// weapon scrolls can enchant only weapons
					case L2Item.TYPE2_WEAPON:
						if(!_isWeapon || Config.ENCHANT_MAX_WEAPON > 0 && enchantItem.getEnchantLevel() >= Config.ENCHANT_MAX_WEAPON)
						{
							activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestEnchantItem.MaxLevel", activeChar));
							return false;
						}
						break;
					// armor scrolls can enchant only accessory and armors
					case L2Item.TYPE2_SHIELD_ARMOR:
						if(_isWeapon || Config.ENCHANT_MAX_ARMOR > 0 && enchantItem.getEnchantLevel() >= Config.ENCHANT_MAX_ARMOR)
						{
							activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestEnchantItem.MaxLevel", activeChar));
							return false;
						}
						break;
					case L2Item.TYPE2_ACCESSORY:
						if(_isWeapon || Config.ENCHANT_MAX_ACCESSORY > 0 && enchantItem.getEnchantLevel() >= Config.ENCHANT_MAX_ACCESSORY)
						{
							activeChar.sendMessage(new CustomMessage("l2n.game.clientpackets.RequestEnchantItem.MaxLevel", activeChar));
							return false;
						}
						break;
					default:
						return false;
				}

			// check for crystal types
			if(_grade != enchantItem.getItem().getItemGradeSPlus())
				return false;

			// check for maximum enchant level
			if(_maxEnchantLevel != 0 && enchantItem.getEnchantLevel() >= _maxEnchantLevel)
				return false;

			if(_itemIds != null && !ArrayUtil.arrayContains(_itemIds, enchantItem.getItemId()))
				return false;

			return true;
		}

		/**
		 * return chance increase
		 */
		public final int getChanceAdd()
		{
			return _chanceAdd;
		}
	}

	public static final class EnchantScroll extends EnchantItem
	{
		private final boolean _isBlessed;
		private final boolean _isCrystal;
		private final boolean _isSafe;

		public EnchantScroll(final boolean wep, final boolean bless, final boolean crystal, final boolean safe, final Grade type, final int level, final int chance, final int... items)
		{
			super(wep, type, level, chance, items);

			_isBlessed = bless;
			_isCrystal = crystal;
			_isSafe = safe;
		}

		/**
		 * Return true for blessed scrolls
		 */
		public final boolean isBlessed()
		{
			return _isBlessed;
		}

		/**
		 * Return true for crystal scrolls
		 */
		public final boolean isCrystal()
		{
			return _isCrystal;
		}

		/**
		 * Return true for safe-enchant scrolls (enchant level will remain on failure)
		 */
		public final boolean isSafe()
		{
			return _isSafe;
		}

		public final boolean isValid(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem)
		{
			// blessed scrolls can't use support items
			if(supportItem != null && (!supportItem.isValid(activeChar, enchantItem) || isBlessed()))
				return false;

			return isValid(activeChar, enchantItem);
		}

		public final double getChance(final L2Player activeChar, final L2ItemInstance enchantItem, final EnchantItem supportItem)
		{
			if(!isValid(activeChar, enchantItem, supportItem))
				return -1;

			return enchantFormula.getChance(activeChar, enchantItem, supportItem, EnchantScroll.this);
		}
	}

	static
	{
		// itemId, (isWeapon, isBlessed, isCrystal, isSafe, grade, max enchant level, chance increase, allowed item IDs)
		// allowed items list must be sorted by ascending order
		_scrolls.put(729, new EnchantScroll(true, false, false, false, A, 0, 0, null));
		_scrolls.put(730, new EnchantScroll(false, false, false, false, A, 0, 0, null));
		_scrolls.put(731, new EnchantScroll(true, false, true, false, A, 0, 0, null));
		_scrolls.put(732, new EnchantScroll(false, false, true, false, A, 0, 0, null));
		_scrolls.put(947, new EnchantScroll(true, false, false, false, B, 0, 0, null));
		_scrolls.put(948, new EnchantScroll(false, false, false, false, B, 0, 0, null));
		_scrolls.put(949, new EnchantScroll(true, false, true, false, B, 0, 0, null));
		_scrolls.put(950, new EnchantScroll(false, false, true, false, B, 0, 0, null));
		_scrolls.put(951, new EnchantScroll(true, false, false, false, C, 0, 0, null));
		_scrolls.put(952, new EnchantScroll(false, false, false, false, C, 0, 0, null));
		_scrolls.put(953, new EnchantScroll(true, false, true, false, C, 0, 0, null));
		_scrolls.put(954, new EnchantScroll(false, false, true, false, C, 0, 0, null));
		_scrolls.put(955, new EnchantScroll(true, false, false, false, D, 0, 0, null));
		_scrolls.put(956, new EnchantScroll(false, false, false, false, D, 0, 0, null));
		_scrolls.put(957, new EnchantScroll(true, false, true, false, D, 0, 0, null));
		_scrolls.put(958, new EnchantScroll(false, false, true, false, D, 0, 0, null));
		_scrolls.put(959, new EnchantScroll(true, false, false, false, S, 0, 0, null));
		_scrolls.put(960, new EnchantScroll(false, false, false, false, S, 0, 0, null));
		_scrolls.put(961, new EnchantScroll(true, false, true, false, S, 0, 0, null));
		_scrolls.put(962, new EnchantScroll(false, false, true, false, S, 0, 0, null));
		_scrolls.put(6569, new EnchantScroll(true, true, false, false, A, 0, 0, null));
		_scrolls.put(6570, new EnchantScroll(false, true, false, false, A, 0, 0, null));
		_scrolls.put(6571, new EnchantScroll(true, true, false, false, B, 0, 0, null));
		_scrolls.put(6572, new EnchantScroll(false, true, false, false, B, 0, 0, null));
		_scrolls.put(6573, new EnchantScroll(true, true, false, false, C, 0, 0, null));
		_scrolls.put(6574, new EnchantScroll(false, true, false, false, C, 0, 0, null));
		_scrolls.put(6575, new EnchantScroll(true, true, false, false, D, 0, 0, null));
		_scrolls.put(6576, new EnchantScroll(false, true, false, false, D, 0, 0, null));
		_scrolls.put(6577, new EnchantScroll(true, true, false, false, S, 0, 0, null));
		_scrolls.put(6578, new EnchantScroll(false, true, false, false, S, 0, 0, null));

		/** Эти свитки имеют 10% бонус шанса заточки */
		_scrolls.put(22006, new EnchantScroll(true, false, false, false, D, 0, 10, null));
		_scrolls.put(22007, new EnchantScroll(true, false, false, false, C, 0, 10, null));
		_scrolls.put(22008, new EnchantScroll(true, false, false, false, B, 0, 10, null));
		_scrolls.put(22009, new EnchantScroll(true, false, false, false, A, 0, 10, null));
		_scrolls.put(20517, new EnchantScroll(true, false, false, false, S, 0, 10, null));
		_scrolls.put(22010, new EnchantScroll(false, false, false, false, D, 0, 10, null));
		_scrolls.put(22011, new EnchantScroll(false, false, false, false, C, 0, 10, null));
		_scrolls.put(22012, new EnchantScroll(false, false, false, false, B, 0, 10, null));
		_scrolls.put(22013, new EnchantScroll(false, false, false, false, A, 0, 10, null));
		_scrolls.put(20518, new EnchantScroll(false, false, false, false, S, 0, 10, null));

		/** При фейле эти свитки не имеют побочных эффектов */
		_scrolls.put(22014, new EnchantScroll(true, false, false, true, B, 16, 10, null)); // Ancient Weapon
		_scrolls.put(22015, new EnchantScroll(true, false, false, true, A, 16, 10, null)); // Ancient Weapon
		_scrolls.put(20519, new EnchantScroll(true, false, false, true, S, 16, 10, null)); // Ancient Weapon
		_scrolls.put(22016, new EnchantScroll(false, false, false, true, B, 16, 10, null)); // Ancient Armor
		_scrolls.put(22017, new EnchantScroll(false, false, false, true, A, 16, 10, null)); // Ancient Armor
		_scrolls.put(20520, new EnchantScroll(false, false, false, true, S, 16, 10, null)); // Ancient Armor

		/** Эти свитки имеют 100% шанс */
		_scrolls.put(22018, new EnchantScroll(true, false, false, false, B, 0, 100, null)); // Divine Weapon
		_scrolls.put(22019, new EnchantScroll(true, false, false, false, A, 0, 100, null)); // Divine Weapon
		_scrolls.put(20521, new EnchantScroll(true, false, false, false, S, 0, 100, null)); // Divine Weapon
		_scrolls.put(22020, new EnchantScroll(false, false, false, false, B, 0, 100, null)); // Divine Armor
		_scrolls.put(22021, new EnchantScroll(false, false, false, false, A, 0, 100, null)); // Divine Armor
		_scrolls.put(20522, new EnchantScroll(false, false, false, false, S, 0, 100, null)); // Divine Armor

		// Master Yogi's Scroll Enchant Weapon (event)
		_scrolls.put(13540, new EnchantScroll(true, false, false, false, NONE, 23, 0, 13539));

		// itemId, (isWeapon, grade, max enchant level, chance increase)
		_supports.put(12362, new EnchantItem(true, D, 9, 20, null));
		_supports.put(12363, new EnchantItem(true, C, 9, 18, null));
		_supports.put(12364, new EnchantItem(true, B, 9, 15, null));
		_supports.put(12365, new EnchantItem(true, A, 9, 12, null));
		_supports.put(12366, new EnchantItem(true, S, 9, 10, null));

		_supports.put(12367, new EnchantItem(false, D, 9, 35, null));
		_supports.put(12368, new EnchantItem(false, C, 9, 27, null));
		_supports.put(12369, new EnchantItem(false, B, 9, 23, null));
		_supports.put(12370, new EnchantItem(false, A, 9, 18, null));
		_supports.put(12371, new EnchantItem(false, S, 9, 15, null));

		_supports.put(14702, new EnchantItem(true, D, 9, 20, null));
		_supports.put(14703, new EnchantItem(true, C, 9, 18, null));
		_supports.put(14704, new EnchantItem(true, B, 9, 15, null));
		_supports.put(14705, new EnchantItem(true, A, 9, 12, null));
		_supports.put(14706, new EnchantItem(true, S, 9, 10, null));

		_supports.put(14707, new EnchantItem(false, D, 9, 35, null));
		_supports.put(14708, new EnchantItem(false, C, 9, 27, null));
		_supports.put(14709, new EnchantItem(false, B, 9, 23, null));
		_supports.put(14710, new EnchantItem(false, A, 9, 18, null));
		_supports.put(14711, new EnchantItem(false, S, 9, 15, null));
	}

	/**
	 * Return enchant template for scroll
	 */
	protected static final EnchantScroll getEnchantScroll(final L2ItemInstance scroll)
	{
		return _scrolls.get(scroll.getItemId());
	}

	public static int[] getItemIds()
	{
		return _scrolls.keys();
	}

	/**
	 * Return enchant template for support item
	 */
	protected static final EnchantItem getSupportItem(final L2ItemInstance item)
	{
		return _supports.get(item.getItemId());
	}

	/**
	 * @param attribute
	 *            TODO
	 * @return true если предмет может быть заточен
	 */
	public static final boolean isEnchantable(final L2ItemInstance item, final boolean attribute)
	{
		if((item.getCustomFlags() & L2ItemInstance.FLAG_NO_ENCHANT) == L2ItemInstance.FLAG_NO_ENCHANT)
			return false;
		if(item.isHeroItem())
			return false;
		if(item.isShadowItem())
			return false;
		if(item.isCommonItem())
			return false;
		if(item.isEtcItem())
			return false;
		if(item.isTemporalItem())
			return false;
		if(item.isWear())
			return false;
		if(item.isCursed())
			return false;
		// rods
		if(item.getItem().getItemType() == WeaponType.ROD)
			return false;
		if(item.getCrystalType().cry == L2Item.CRYSTAL_NONE)
			return false;
		if(item.getItem().getType2() == L2Item.TYPE2_QUEST)
			return false;
		// apprentice and travelers weapons
		if(item.getItemId() >= 7816 && item.getItemId() <= 7831)
			return false;
		// Pailaka предметы
		if(item.getItemId() >= 13293 && item.getItemId() <= 13296)
			return false;
		// bracelets
		if(item.getItem().getBodyPart() == L2Item.SLOT_L_BRACELET && !Config.ENCHANT_BRACELETITEM)
			return false;
		if(item.getItem().getBodyPart() == L2Item.SLOT_R_BRACELET && !Config.ENCHANT_BRACELETITEM)
			return false;
		// Belts canno't be enchanted.
		if(item.getItem().getBodyPart() == L2Item.SLOT_BELT && !Config.ENCHANT_BELTITEM)
			return false;
		if(item.getItem().getBodyPart() == L2Item.SLOT_BACK && !Config.ENCHANT_CLOAKITEM)
			return false;

		// бижутерия с ТВ не точиться на аттрибут
		if(attribute && item.getItemId() >= 14801 && item.getItemId() <= 14809 || item.getItemId() >= 15282 && item.getItemId() <= 15299)
			return false;

		// only items in inventory and equipped can be enchanted
		if(item.getLocation() != L2ItemInstance.ItemLocation.INVENTORY && item.getLocation() != L2ItemInstance.ItemLocation.PAPERDOLL)
			return false;

		return true;
	}

	/**
	 * Обработка неудачи при энчанте предмета
	 */
	protected static final void failEnchant(final L2Player activeChar, final L2ItemInstance enchantItem, final int type)
	{
		final Inventory inventory = activeChar.getInventory();
		synchronized (inventory)
		{
			switch (type)
			{
				case NORMAL_SCROLL: // Обрабатываем обычные скролы
				{
					switch (Config.ENCHANT_FAIL_TYPE)
					{
						case ENCHANT_FAIL_TYPE_1: // предмет ломатеся на кристалы.
						{
							destroyItem(activeChar, enchantItem);
						}
							break;
						case ENCHANT_FAIL_TYPE_2: // уровень заточки предмета сбрасывается на нулевой.
						{
							enchantItem.setEnchantLevel(0);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), Msg.FAILED_IN_BLESSED_ENCHANT_THE_ENCHANT_VALUE_OF_THE_ITEM_BECAME_0, EnchantResult.CANCEL);
						}
							break;
						case ENCHANT_FAIL_TYPE_3: // уровень заточки предмета сбрасывается на указанный.
						{
							enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_LEVEL);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), EnchantResult.SAFE_SCROLL_FAILED);
						}
							break;
						case ENCHANT_FAIL_TYPE_4: // уровень заточки предмета сбрасывается на указанный при наличии у игрока в инвентаре необходимого итема.
						{
							final L2ItemInstance item = inventory.findItemByItemId(Config.ENCHANT_FAIL_ITEM_ID);
							final long count = item != null ? item.getCount() : 0;

							if(count >= Config.ENCHANT_FAIL_ITEM_COUNT)
							{
								enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_LEVEL);
								activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), EnchantResult.SAFE_SCROLL_FAILED);
								inventory.destroyItem(Config.ENCHANT_FAIL_ITEM_ID, Config.ENCHANT_FAIL_ITEM_COUNT, true);
							}
							else
								destroyItem(activeChar, enchantItem);

						}
							break;
						default:
						{
							_log.warning("AbstractEnchantPacket: wrong ENCHANT_FAIL_TYPE in failEnchant(), ENCHANT_FAIL_TYPE = " + Config.ENCHANT_FAIL_TYPE);
							Thread.dumpStack();
						}
							break;
					}
				}
					break;
				case BLESSED_SCROLL: // Обрабатываем blessed скролы
				{
					switch (Config.ENCHANT_FAIL_BLESSED_TYPE)
					{
						case ENCHANT_FAIL_TYPE_1: // предмет ломатеся на кристалы.
						{
							destroyItem(activeChar, enchantItem);
						}
							break;
						case ENCHANT_FAIL_TYPE_2: // уровень заточки предмета сбрасывается на нулевой.
						{
							enchantItem.setEnchantLevel(0);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED));
							activeChar.sendPacket(Msg.FAILED_IN_BLESSED_ENCHANT_THE_ENCHANT_VALUE_OF_THE_ITEM_BECAME_0);
							activeChar.sendPacket(EnchantResult.BLESSED_FAILED);
						}
							break;
						case ENCHANT_FAIL_TYPE_3: // уровень заточки предмета сбрасывается на указанный.
						{
							enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_BLESSED_LEVEL);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED));
							activeChar.sendPacket(EnchantResult.SAFE_SCROLL_FAILED);
						}
							break;
						case ENCHANT_FAIL_TYPE_4: // уровень заточки предмета сбрасывается на указанный при наличии у игрока в инвентаре необходимого итема.
						{
							final L2ItemInstance item = inventory.findItemByItemId(Config.ENCHANT_FAIL_BLESSED_ITEM_ID);
							final long count = item != null ? item.getCount() : 0;

							if(count >= Config.ENCHANT_FAIL_BLESSED_ITEM_COUNT)
							{
								enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_LEVEL);
								activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED));
								activeChar.sendPacket(EnchantResult.SAFE_SCROLL_FAILED);
								inventory.destroyItem(Config.ENCHANT_FAIL_BLESSED_ITEM_ID, Config.ENCHANT_FAIL_BLESSED_ITEM_COUNT, true);
							}
							else
								destroyItem(activeChar, enchantItem);
						}
							break;
						default:
						{
							_log.warning("AbstractEnchantPacket: wrong ENCHANT_FAIL_BLESSED_TYPE in failEnchant(), ENCHANT_FAIL_BLESSED_TYPE = " + Config.ENCHANT_FAIL_BLESSED_TYPE);
							Thread.dumpStack();
						}
							break;
					}
				}
					break;
				case CRYSTAL_SCROLL:// Обрабатываем crystal скролы
				{
					switch (Config.ENCHANT_FAIL_CRYSTAL_TYPE)
					{
						case ENCHANT_FAIL_TYPE_1: // предмет ломатеся на кристалы.
						{
							destroyItem(activeChar, enchantItem);
						}
							break;
						case ENCHANT_FAIL_TYPE_2: // уровень заточки предмета сбрасывается на нулевой.
						{
							enchantItem.setEnchantLevel(0);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), Msg.FAILED_IN_BLESSED_ENCHANT_THE_ENCHANT_VALUE_OF_THE_ITEM_BECAME_0, EnchantResult.CANCEL);
						}
							break;
						case ENCHANT_FAIL_TYPE_3: // уровень заточки предмета сбрасывается на указанный.
						{
							enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_CRYSTAL_LEVEL);
							activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), EnchantResult.SAFE_SCROLL_FAILED);
						}
							break;
						case ENCHANT_FAIL_TYPE_4: // уровень заточки предмета сбрасывается на указанный при наличии у игрока в инвентаре необходимого итема.
						{
							final L2ItemInstance item = inventory.findItemByItemId(Config.ENCHANT_FAIL_CRYSTAL_ITEM_ID);
							final long count = item != null ? item.getCount() : -1;

							if(count >= Config.ENCHANT_FAIL_CRYSTAL_ITEM_COUNT)
							{
								enchantItem.setEnchantLevel(Config.ENCHANT_FAIL_CRYSTAL_LEVEL);
								activeChar.sendPacket(new InventoryUpdate(enchantItem, L2ItemInstance.MODIFIED), EnchantResult.SAFE_SCROLL_FAILED);
								inventory.destroyItem(Config.ENCHANT_FAIL_CRYSTAL_ITEM_ID, Config.ENCHANT_FAIL_CRYSTAL_ITEM_COUNT, true);
							}
							else
								destroyItem(activeChar, enchantItem);
						}
							break;
						default:
						{
							_log.warning("AbstractEnchantPacket: wrong ENCHANT_FAIL_CRYSTAL_TYPE in failEnchant(), ENCHANT_FAIL_CRYSTAL_TYPE = " + Config.ENCHANT_FAIL_CRYSTAL_TYPE);
							Thread.dumpStack();
						}
							break;
					}
				}
					break;
				default:
				{
					_log.warning("AbstractEnchantPacket: wrong type in failEnchant(), type = " + type);
					Thread.dumpStack();
				}
			}
		}
	}

	/**
	 * Удаляет предмет, выдаём кристалы
	 * 
	 * @param activeChar
	 *            - игрок
	 * @param enchantItem
	 *            - затачиваемый предмет
	 */
	private final static void destroyItem(final L2Player activeChar, final L2ItemInstance enchantItem)
	{
		if(enchantItem.isEquipped())
			activeChar.getInventory().unEquipItemInSlot(enchantItem.getEquipSlot());

		final L2ItemInstance destroyedItem = activeChar.getInventory().destroyItem(enchantItem.getObjectId(), 1, true);
		if(destroyedItem == null)
		{
			_log.warning("failed to destroy " + enchantItem.getObjectId() + " after unsuccessful enchant attempt by char " + activeChar.getName());
			activeChar.sendActionFailed();
			return;
		}

		final int crystalId = enchantItem.getCrystalType().cry;
		if(crystalId > 0)
		{
			final L2ItemInstance crystalsToAdd = ItemTable.getInstance().createItem(crystalId, activeChar.getObjectId(), enchantItem.getItemId(), "AbstractEnchantPacket[667]");

			int count = (int) (enchantItem.getItem().getCrystalCount() * 0.87);
			if(destroyedItem.getEnchantLevel() > 3)
				count += enchantItem.getItem().getCrystalCount() * 0.25 * (destroyedItem.getEnchantLevel() - 3);
			if(count < 1)
				count = 1;
			crystalsToAdd.setCount(count);

			activeChar.getInventory().addItem(crystalsToAdd);
			Log.LogItem(activeChar, Log.Sys_GetItem, crystalsToAdd);
			activeChar.sendPacket(new EnchantResult(1, crystalsToAdd.getItemId(), count));
		}
		else
			activeChar.sendPacket(EnchantResult.FAILED_NO_CRYSTALS);

		Log.LogItem(activeChar, Log.EnchantItemFail, enchantItem);
		activeChar.refreshExpertisePenalty();
		activeChar.refreshOverloaded();
	}
}
