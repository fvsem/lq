package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2HennaInstance;
import l2n.game.network.serverpackets.HennaItemInfo;
import l2n.game.tables.HennaTable;
import l2n.game.templates.L2Henna;

public class RequestHennaItemInfo extends L2GameClientPacket
{
	private static String _C__C4_RequestHennaItemInfo = "[C] C4 RequestHennaItemInfo";

	private int SymbolId;

	@Override
	public void readImpl()
	{
		SymbolId = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		final L2Henna template = HennaTable.getInstance().getTemplate(SymbolId);
		if(template != null)
			activeChar.sendPacket(new HennaItemInfo(new L2HennaInstance(template), activeChar));
	}

	@Override
	public String getType()
	{
		return _C__C4_RequestHennaItemInfo;
	}
}
