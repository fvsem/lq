package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.L2ManufactureItem;
import l2n.game.model.L2ManufactureList;
import l2n.game.model.L2TradeList;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.RecipeShopMsg;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestRecipeShopListSet extends L2GameClientPacket
{
	private static String _C__BB_RequestRecipeShopListSet = "[C] BB RequestRecipeShopListSet";
	// format: cdb, b - array of (dd)
	private int _count;
	private final L2ManufactureList createList = new L2ManufactureList();

	@Override
	public void readImpl()
	{
		_count = readD();
		if(_count <= 0 || _count > Config.MAX_ITEM_IN_PACKET || _count * 12 != _buf.remaining())
		{
			_count = 0;
			return;
		}
		for(int x = 0; x < _count; x++)
		{
			final int id = readD();
			final long cost = readQ();
			if(id < 1 || cost < 0)
			{
				_count = 0;
				return;
			}
			createList.add(new L2ManufactureItem(id, cost));
		}
		_count = createList.size();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(!activeChar.getPassCheck())
        {
			L2TradeList.cancelStore(activeChar);
            return;
        }
		if(!activeChar.checksForShop(true))
		{
			L2TradeList.cancelStore(activeChar);
			return;
		}

		if(activeChar.getNoChannel() != 0)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOU_ARE_CURRENTLY_BANNED_FROM_ACTIVITIES_RELATED_TO_THE_PRIVATE_STORE_AND_PRIVATE_WORKSHOP));
			L2TradeList.cancelStore(activeChar);
			return;
		}

		if(_count == 0 || activeChar.getCreateList() == null)
		{
			L2TradeList.cancelStore(activeChar);
			return;
		}

		if(_count > Config.MAX_PVTCRAFT_SLOTS)
		{
			sendPacket(Msg.YOU_HAVE_EXCEEDED_THE_QUANTITY_THAT_CAN_BE_INPUTTED);
			L2TradeList.cancelStore(activeChar);
			return;
		}

		createList.setStoreName(activeChar.getCreateList().getStoreName());
		activeChar.setCreateList(createList);

		activeChar.setPrivateStoreType(L2Player.STORE_PRIVATE_MANUFACTURE);
		activeChar.sitDown();
		activeChar.broadcastCharInfo();
		activeChar.broadcastPacket(new RecipeShopMsg(activeChar));
	}

	@Override
	public String getType()
	{
		return _C__BB_RequestRecipeShopListSet;
	}
}
