package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.templates.L2Item;

public class RequestUnEquipItem extends L2GameClientPacket
{
	private static final String _C__16_REQUESTUNEQUIPITEM = "[C] 16 RequestUnequipItem";
	private int _slot;

	/**
	 * packet type id 0x16
	 * format: cd
	 * 
	 * @param decrypt
	 */
	@Override
	public void readImpl()
	{
		_slot = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		// You cannot do anything else while fishing
		if(activeChar.isFishing())
		{
			activeChar.sendPacket(Msg.YOU_CANNOT_DO_ANYTHING_ELSE_WHILE_FISHING);
			return;
		}

		// Нельзя снимать проклятое оружие
		if((_slot == L2Item.SLOT_R_HAND || _slot == L2Item.SLOT_L_HAND || _slot == L2Item.SLOT_LR_HAND) && (activeChar.isCursedWeaponEquipped() || activeChar.isCombatFlagEquipped() || activeChar.isTerritoryFlagEquipped()))
			return;

		if(_slot == L2Item.SLOT_R_HAND)
		{
			L2ItemInstance weapon = activeChar.getActiveWeaponInstance();
			if(weapon == null)
				return;
			activeChar.abortAttack();
			activeChar.abortCast();
			activeChar.sendDisarmMessage(weapon);
		}

		activeChar.getInventory().unEquipItemInBodySlot(_slot, null);
	}

	@Override
	public String getType()
	{
		return _C__16_REQUESTUNEQUIPITEM;
	}
}
