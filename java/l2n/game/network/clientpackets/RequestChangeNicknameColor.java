package l2n.game.network.clientpackets;

import l2n.Config;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;

public class RequestChangeNicknameColor extends L2GameClientPacket
{
	private static final String _C__D0_4F_REQUESTCHANGENICKNAMECOLOR = "[C] D0:4F RequestChangeNicknameColor";

	public static final int COLOR_NAME_1 = 13021;
	public static final int COLOR_NAME_2 = 13307;

	// colors harvested from client, do not modify
	private static final int COLOR_CHOICES[] = {
			0x9393FF, // Pink
			0x7C49FC, // Rose Pink
			0x97F8FC, // Lemon Yellow
			0xFA9AEE, // Lilac
			0xFF5D93, // Cobalt Violet
			0x00FCA0, // Mint Green
			0xA0A601, // Peacock Green
			0x7898AF, // Yellow Ochre
			0x486295, // Chocolate
			0x999999 // Silver
	};

	// Name Color
	private int _color;
	private String _title;
	private int _itemObjectId;

	@Override
	protected void readImpl()
	{
		_color = readD();
		_title = readS();
		_itemObjectId = readD();
	}

	@Override
	protected void runImpl()
	{
		final L2Player player = getClient().getActiveChar();
		if(player == null)
			return;

		if(player.isInStoreMode() || player.isCastingNow() || player.isInCombat() || player.isOutOfControl() || player.isActionsDisabled() || player.isMounted() || player.isInOlympiadMode() || player.getDuel() != null)
		{
			player.sendActionFailed();
			return;
		}

		final L2ItemInstance item = player.getInventory().getItemByObjectId(_itemObjectId);
		if(item == null)
		{
			player.sendActionFailed();
			return;
		}

		if(item.getItemId() != COLOR_NAME_1 && item.getItemId() != COLOR_NAME_2)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			player.sendActionFailed();
			return;
		}

		if(!_title.equals("") && !Config.CLAN_TITLE_TEMPLATE.matcher(_title).matches())
		{
			player.sendMessage("Incorrect title.");
			player.sendActionFailed();
			return;
		}

		if(player.getInventory().destroyItem(item, 1, true) == null)
		{
			player.sendPacket(Msg.YOU_CANNOT_DESTROY_IT_BECAUSE_THE_NUMBER_IS_INCORRECT);
			player.sendActionFailed();
			return;
		}

		player.setTitle(_title);
		player.sendPacket(Msg.TITLE_HAS_CHANGED);

		if(0 <= _color && _color < COLOR_CHOICES.length)
			player.setTitleColor(COLOR_CHOICES[_color]);
		else
			player.setTitleColor(0xFFFF77);

		player.sendChanges();
		player.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__D0_4F_REQUESTCHANGENICKNAMECOLOR;
	}
}
