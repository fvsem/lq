package l2n.game.network.clientpackets;

import l2n.game.network.serverpackets.ExShowAgitInfo;

public class RequestAllAgitInfo extends L2GameClientPacket
{
	private static String _C__D0_41_REQUESTALLAGITINFO = "[C] D0:41 RequestAllAgitInfo";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		getClient().getActiveChar().sendPacket(new ExShowAgitInfo());
	}

	@Override
	public String getType()
	{
		return _C__D0_41_REQUESTALLAGITINFO;
	}
}
