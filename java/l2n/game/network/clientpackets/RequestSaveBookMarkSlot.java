/**
 * 
 */
package l2n.game.network.clientpackets;

import l2n.game.model.actor.L2Player;

/**
 * @author L2System Project
 * @date 15.12.2009
 * @time 23:03:32
 */
public class RequestSaveBookMarkSlot extends L2GameClientPacket
{
	private static final String _C__51_REQUESTSAVEBOOKMARKSLOT = "[C] 51 RequestSaveBookMarkSlot";

	private int icon;
	private String name, tag;

	@Override
	protected void readImpl()
	{
		readH();
		name = readS();
		icon = readD();
		tag = readS();
	}

	@Override
	protected void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.teleportBookmarkAdd(activeChar.getX(), activeChar.getY(), activeChar.getZ(), icon, tag, name);
		activeChar.sendActionFailed();
	}

	@Override
	public String getType()
	{
		return _C__51_REQUESTSAVEBOOKMARKSLOT;
	}
}
