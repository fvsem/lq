package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.network.serverpackets.TradeStart;

public class AnswerTradeRequest extends L2GameClientPacket
{
	private final static String _C__55_ANSWERTRADEREQUEST = "[C] 55 AnswerTradeRequest";
	// Format: cd
	private int _response;

	@Override
	public void readImpl()
	{
		_response = readD();
	}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		final Transaction transaction = activeChar.getTransaction();
		if(transaction == null)
			return;

		if(!transaction.isValid() || !transaction.isTypeOf(TransactionType.TRADE_REQUEST))
		{
			transaction.cancel();
			if(_response == 1)
				activeChar.sendPacket(Msg.TARGET_IS_NOT_FOUND_IN_THE_GAME, Msg.ActionFail);
			else
				activeChar.sendPacket(Msg.TIME_EXPIRED, Msg.ActionFail);
			return;
		}

		final L2Player requestor = transaction.getOtherPlayer(activeChar);

		if(_response != 1 || activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
		{
			requestor.sendPacket(new SystemMessage(SystemMessage.S1_DENIED_YOUR_REQUEST_FOR_TRADE).addString(activeChar.getName()), Msg.ActionFail);
			transaction.cancel();
			if(activeChar.getPrivateStoreType() != L2Player.STORE_PRIVATE_NONE)
				activeChar.sendPacket(Msg.WHILE_OPERATING_A_PRIVATE_STORE_OR_WORKSHOP_YOU_CANNOT_DISCARD_DESTROY_OR_TRADE_AN_ITEM);
			return;
		}

		transaction.cancel();

		new Transaction(TransactionType.TRADE, activeChar, requestor);

		requestor.sendPacket(new SystemMessage(SystemMessage.BEGIN_TRADING_WITH_S1).addString(activeChar.getName()), new TradeStart(requestor, activeChar));
		activeChar.sendPacket(new SystemMessage(SystemMessage.BEGIN_TRADING_WITH_S1).addString(requestor.getName()), new TradeStart(activeChar, requestor));
	}

	@Override
	public String getType()
	{
		return _C__55_ANSWERTRADEREQUEST;
	}
}
