package l2n.game.network.clientpackets;

import l2n.game.cache.Msg;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.PledgeShowMemberListDelete;
import l2n.game.network.serverpackets.SystemMessage;

public class RequestWithdrawalPledge extends L2GameClientPacket
{
	private static final String _C__28_REQUESTWITHDRAWALPLEDGE = "[C] 28 RequestWithdrawalPledge";

	@Override
	public void readImpl()
	{}

	@Override
	public void runImpl()
	{
		final L2Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		// is the guy in a clan ?
		if(activeChar.getClanId() == 0)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isInCombat())
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.ONE_CANNOT_LEAVE_ONES_CLAN_DURING_COMBAT));
			return;
		}

		final L2Clan clan = activeChar.getClan();
		if(clan == null)
			return;

		final L2ClanMember member = clan.getClanMember(activeChar.getObjectId());
		if(member == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		if(member.isClanLeader())
		{
			activeChar.sendMessage("A clan leader may not be dismissed.");
			return;
		}

		// this also updated the database
		clan.removeClanMember(activeChar.getObjectId());

		// player withdrawed.
		clan.broadcastToOnlineMembers(new SystemMessage(SystemMessage.S1_HAS_WITHDRAWN_FROM_THE_CLAN).addString(activeChar.getName()));

		// Remove the Player From the Member list
		clan.broadcastToOnlineMembers(new PledgeShowMemberListDelete(activeChar.getName()));

		activeChar.sendPacket(Msg.YOU_HAVE_RECENTLY_BEEN_DISMISSED_FROM_A_CLAN_YOU_ARE_NOT_ALLOWED_TO_JOIN_ANOTHER_CLAN_FOR_24_HOURS);

		activeChar.setClan(null);

		if(!activeChar.isNoble())
			activeChar.setTitle("");

		activeChar.setLeaveClanCurTime();
		activeChar.broadcastCharInfo();

		// disable clan tab
		activeChar.sendPacket(Msg.PledgeShowMemberListDeleteAll);
	}

	@Override
	public String getType()
	{
		return _C__28_REQUESTWITHDRAWALPLEDGE;
	}
}
