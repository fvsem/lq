package l2n.game.network;

import com.lameguard.crypt.GameCrypt;
import com.lameguard.session.LameClientV195;
import javolution.text.TextBuilder;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.network.MMOClient;
import l2n.commons.network.MMOConnection;
import l2n.commons.threading.FIFORunnableQueue;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.loginservercon.LSConnection;
import l2n.game.loginservercon.SessionKey;
import l2n.game.loginservercon.send.PlayerLogout;
import l2n.game.model.CharSelectInfoPackage;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.L2GameClientPacket;
import l2n.game.network.model.L2OpcodeObfuscator;
import l2n.game.network.model.PacketLogger;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.tables.ClanTable;
import l2n.util.HWID.HardwareID;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class L2GameClient extends MMOClient<L2GameClient, L2GameClientPacket, L2GameServerPacket> implements LameClientV195
{
	public static enum GameClientState
	{
		CONNECTED,
		AUTHED,
		IN_GAME
	}

	private final static Logger _log = Logger.getLogger(L2GameClient.class.getName());

	public GameCrypt _crypt = null;
	private double _bonus = 1;
	private long _bonus_expire = 0;
	public GameClientState _state;

	private int _upTryes = 0, _upTryesTotal = 0;
	private long _upTryesRefresh = 0;

	private String _loginName;
	private L2Player _activeChar;
	private SessionKey _sessionId = null;

	private int revision = 0;
	private boolean _gameGuardOk = false;

	public boolean protect_used = false;
	public byte client_lang = -1;
	public HardwareID HWID = null, ALLOW_HWID = null;

	private final GArray<Integer> _charSlotMapping = new GArray<Integer>();
	private PacketLogger pktLogger = null;
	private boolean pktLoggerMatch = false;

	private L2OpcodeObfuscator _obfuscator = null;

	public L2GameClient(final MMOConnection<L2GameClient, L2GameClientPacket, L2GameServerPacket> con, final boolean offline)
	{
		super(con);
		if(!offline)
		{
			_state = GameClientState.CONNECTED;
			_sessionId = new SessionKey(-1, -1, -1, -1);
			_crypt = new GameCrypt();

			if(Config.PROTECT_ENABLE)
				protect_used = !Config.PROTECT_UNPROTECTED_IPS.isIpInNets(getIpAddr());

			if(Config.LOG_CLIENT_PACKETS || Config.LOG_SERVER_PACKETS)
			{
				pktLogger = new PacketLogger(this, Config.PACKETLOGGER_FLUSH_SIZE);
				if(Config.PACKETLOGGER_IPS != null)
					if(Config.PACKETLOGGER_IPS.isIpInNets(getIpAddr()))
						pktLoggerMatch = true;
			}
		}
		else
			_state = GameClientState.IN_GAME;
	}

	public L2GameClient(final MMOConnection<L2GameClient, L2GameClientPacket, L2GameServerPacket> con)
	{
		this(con, false);
	}

	public void disconnectOffline()
	{
		onDisconnection();
	}

	@Override
	protected void onDisconnection()
	{
		if(pktLogger != null)
		{
			if(!pktLogger.assigned() && pktLoggerMatch)
				pktLogger.assign();
			pktLogger.close();
			pktLogger = null;
		}

		if(getLoginName() == null || getLoginName().equals("") || _state != GameClientState.IN_GAME && _state != GameClientState.AUTHED)
			return;

		try
		{
			if(_activeChar != null && _activeChar.isInOfflineMode())
				return;

			LSConnection.getInstance().removeAccount(this);
			if(_activeChar != null) // this should only happen on connection loss
			{
				final L2Player player = _activeChar;
				_activeChar = null;

				if(player != null && !player.isLogoutStarted()) // this should only happen on connection loss
				{
					player.scheduleDelete(Config.PLAYER_DISCONNECT_INGAME_TIME);
					if(player.getNetConnection() != null)
					{
						if(!player.isInOfflineMode())
							player.getNetConnection().closeNow(false);
						player.setNetConnection(null);
					}
					player.setConnected(false);
					if(Config.PLAYER_DISCONNECT_INGAME_TIME > 0)
						player.broadcastUserInfo(false);
				}
			}
			setConnection(null);
		}
		catch(final Exception e1)
		{
			_log.log(Level.WARNING, "error while disconnecting client", e1);
		}
		finally
		{
			LSConnection.getInstance().sendPacket(new PlayerLogout(getLoginName()));
		}
		super.onDisconnection();
	}

	public static void saveCharToDisk(final L2Player cha)
	{
		try
		{
			cha.getInventory().updateDatabase(true);
			cha.store(false);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error saving player character: ", e);
		}
	}

	public void deleteFromClan(final L2Player cha)
	{
		final L2Clan clan = cha.getClan();
		if(clan != null)
			clan.removeClanMember(cha.getObjectId());
	}

	public static void deleteFromClan(final int charId, final int clanId)
	{
		if(clanId == 0)
			return;
		final L2Clan clan = ClanTable.getInstance().getClan(clanId);
		if(clan != null)
			clan.removeClanMember(charId);
	}

	public void markRestoredChar(final int charslot) throws Exception
	{
		final int objid = getObjectIdForSlot(charslot);
		if(objid < 0)
			return;

		// have to make sure active character must be nulled
		if(getActiveChar() != null)
		{
			saveCharToDisk(getActiveChar());
			if(Config.DEBUG)
				_log.fine("active Char saved");
			_activeChar = null;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET deletetime=0 WHERE obj_id=?");
			statement.setInt(1, objid);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "data error on restore char:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void markToDeleteChar(final int charslot) throws Exception
	{
		// have to make sure active character must be nulled
		if(getActiveChar() != null)
		{
			saveCharToDisk(getActiveChar());
			if(Config.DEBUG)
				_log.fine("active Char saved");
			_activeChar = null;
		}

		final int objid = getObjectIdForSlot(charslot);
		if(objid < 0)
			return;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE characters SET deletetime=? WHERE obj_id=?");
			statement.setLong(1, (int) (System.currentTimeMillis() / 1000));
			statement.setInt(2, objid);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "data error on update deletime char:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void deleteChar(final int charslot) throws Exception
	{
		// have to make sure active character must be nulled
		if(getActiveChar() != null)
		{
			_activeChar.logout(false, false, true, true);
			_activeChar = null;
		}

		final int objid = getObjectIdForSlot(charslot);
		if(objid == -1)
			return;

		deleteCharByObjId(objid);
	}

	public static void deleteCharByObjId(final int objid)
	{
		if(objid < 0)
			return;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM characters WHERE obj_Id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_shortcuts WHERE char_obj_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_macroses WHERE char_obj_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE pets FROM pets, items WHERE pets.item_obj_id=items.object_id AND items.owner_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM items WHERE owner_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_skills WHERE char_obj_Id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_effects_save WHERE char_obj_Id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_skills_save WHERE char_obj_Id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_quests WHERE char_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_recipebook WHERE char_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_friends WHERE char_id=? or friend_id = ?");
			statement.setInt(1, objid);
			statement.setInt(2, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM seven_signs WHERE char_obj_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);

			statement = con.prepareStatement("DELETE FROM character_subclasses WHERE char_obj_id=?");
			statement.setInt(1, objid);
			statement.execute();
			DbUtils.close(statement);
			statement = null;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Data error on delete char: ", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public boolean hasHWID()
	{
		return HWID != null;
	}

	public L2Player loadCharFromDisk(final int charslot)
	{
		final Integer objectId = getObjectIdForSlot(charslot);
		if(objectId == -1)
			return null;

		L2Player character = null;
		final L2Player old_player = L2ObjectsStorage.getPlayer(objectId);

		if(old_player != null)
			if(old_player.isInOfflineMode() || old_player.isLogoutStarted() || old_player.entering)
				// оффтрейдового чара проще выбУть чем восстанавлУвать
				old_player.logout(false, false, true, true);
			else
			{
				old_player.sendPacket(Msg.ANOTHER_PERSON_HAS_LOGGED_IN_WITH_THE_SAME_ACCOUNT);
				LSConnection.getInstance().sendPacket(new PlayerLogout(getLoginName()));

				if(old_player.getNetConnection() != null)
				{
					old_player.getNetConnection().setActiveChar(null);
					old_player.getNetConnection().closeNow(false);
				}
				old_player.setLogoutStarted(false);
				old_player.setNetConnection(this);
				character = old_player;
			}

		if(character == null)
			character = L2Player.restore(objectId);
		if(character != null)
		{
			// preinit some values for each login
			character.setRunning(); // running is default
			character.standUp(); // standing is default

			character.updateStats();
			character.setOnlineStatus(true);
			setActiveChar(character);
			character.restoreBonus();
			switch (client_lang)
			{
				case 0:
					character.setVar("lang@", "en");
					break;
				case 1:
					character.setVar("lang@", "ru");
					break;
			}

			if(protect_used && Config.PROTECT_GS_STORE_HWID && hasHWID())
				character.storeHWID(HWID.Full);

			if(pktLogger != null)
				if(!pktLogger.assigned())
				{
					if(!pktLoggerMatch)
						if(Config.PACKETLOGGER_CHARACTERS != null)
						{
							final String char_name = character.getName();
							for(int i = 0; i < Config.PACKETLOGGER_CHARACTERS.size(); i++)
							{
								final String s_mask = Config.PACKETLOGGER_CHARACTERS.get(i);
								if(char_name.matches(s_mask))
								{
									pktLoggerMatch = true;
									break;
								}
							}
						}
					if(pktLoggerMatch)
						pktLogger.assign();
					else
						pktLogger = null;
				}
		}
		else
			_log.warning("could not restore obj_id: " + objectId + " in slot:" + charslot);

		return character;
	}

	public int getObjectIdForSlot(final int charslot)
	{
		if(charslot < 0 || charslot >= _charSlotMapping.size())
		{
			_log.warning(getLoginName() + " tried to modify Character in slot " + charslot + " but no characters exits at that slot.");
			return -1;
		}
		return _charSlotMapping.get(charslot);
	}

	public L2Player getActiveChar()
	{
		return _activeChar;
	}

	/**
	 * @return Returns the sessionId.
	 */
	public SessionKey getSessionId()
	{
		return _sessionId;
	}

	public String getLoginName()
	{
		return _loginName;
	}

	private void logHWID()
	{
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(Config.PROTECT_GS_LOG_HWID_QUERY);
			statement.setString(1, _loginName);
			statement.setString(2, getIpAddr());
			statement.setString(3, HWID.Full);
			statement.setInt(4, Config.REQUEST_ID);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not log HWID:", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void setLoginName(final String loginName)
	{
		_loginName = loginName;
		if(protect_used && Config.PROTECT_GS_LOG_HWID && !getIpAddr().equalsIgnoreCase("Disconnected"))
			logHWID();

		if(pktLogger != null && !pktLoggerMatch && Config.PACKETLOGGER_ACCOUNTS != null)
			for(int i = 0; i < Config.PACKETLOGGER_ACCOUNTS.size(); i++)
			{
				final String s_mask = Config.PACKETLOGGER_ACCOUNTS.get(i);
				if(loginName.matches(s_mask))
				{
					pktLoggerMatch = true;
					break;
				}
			}
	}

	public void setActiveChar(final L2Player cha)
	{
		_activeChar = cha;
		if(cha == null)
			return;

		_activeChar.setNetConnection(this);
	}

	public void setSessionId(final SessionKey sessionKey)
	{
		_sessionId = sessionKey;
	}

	public void setCharSelection(final CharSelectInfoPackage[] chars)
	{
		_charSlotMapping.clear();

		for(final CharSelectInfoPackage element : chars)
		{
			final int objectId = element.getObjectId();
			_charSlotMapping.add(objectId);
		}
	}

	public void setCharSelection(final int c)
	{
		_charSlotMapping.clear();
		_charSlotMapping.add(c);
	}

	public int getRevision()
	{
		return revision;
	}

	public void setRevision(final int rev)
	{
		revision = rev;
	}

	public void setGameGuardOk(final boolean gameGuardOk)
	{
		_gameGuardOk = gameGuardOk;
	}

	public boolean isGameGuardOk()
	{
		return _gameGuardOk;
	}

	@Override
	public boolean encrypt(final ByteBuffer buf, final int size)
	{
		if(pktLogger != null && Config.LOG_SERVER_PACKETS)
			pktLogger.log_packet((byte) 1, buf, size);
		_crypt.encrypt(buf.array(), buf.position(), size);
		buf.position(buf.position() + size);
		return true;
	}

	@Override
	public boolean decrypt(final ByteBuffer buf, final int size)
	{
		_crypt.decrypt(buf.array(), buf.position(), size);
		if(pktLogger != null && Config.LOG_CLIENT_PACKETS)
			pktLogger.log_packet((byte) 0, buf, size);
		return true;
	}

	public void sendPacket(final L2GameServerPacket... gsps)
	{
		if(getConnection() == null)
			return;

		getConnection().sendPacket(gsps);
		if(Config.DEBUG_PACKETS)
			debugPackets(getActiveChar(), gsps);
	}

	public void sendPackets(final Collection<L2GameServerPacket> gsps)
	{
		if(getConnection() == null)
			return;

		getConnection().sendPackets(gsps);
		if(Config.DEBUG_PACKETS)
			debugPackets(getActiveChar(), gsps.toArray(new L2GameServerPacket[gsps.size()]));
	}

	public void sendPackets(final Deque<L2GameServerPacket> gsps)
	{
		if(getConnection() == null)
			return;

		getConnection().sendPackets(gsps);
		if(Config.DEBUG_PACKETS)
			debugPackets(getActiveChar(), gsps.toArray(new L2GameServerPacket[gsps.size()]));
	}

	private static void debugPackets(final L2Player player, final L2GameServerPacket... gsps)
	{
		for(final L2GameServerPacket gsp : gsps)
			if(player != null)
			{
				if(!gsp.getClass().getSimpleName().equalsIgnoreCase("SocialAction") && !gsp.getClass().getSimpleName().equalsIgnoreCase("ValidateLocation") && !gsp.getClass().getSimpleName().equalsIgnoreCase("CharMoveToLocation"))
					_log.warning("[S]: " + gsp.getClass().getSimpleName() + " | to " + player.getName() + "[" + player.getObjectId() + "]");
			}
			else
				_log.warning("[S]: " + gsp.getClass().getSimpleName());
	}

	public void close(final L2GameServerPacket gsp)
	{
		getConnection().close(gsp);
	}

	public String getIpAddr()
	{
		try
		{
			return getConnection().getSocket().getInetAddress().getHostAddress();
		}
		catch(final NullPointerException e)
		{
			return "Disconnected";
		}
	}

	public byte[] enableCrypt()
	{
		final byte[] key = isProtected() ? com.lameguard.utils.Rnd.nextBytes(new byte[64]) : BlowFishKeygen.getRandomKey();
		_crypt.setProtected(isProtected());
		_crypt.setKey(key);
		return key;
	}

	public double getBonus()
	{
		return Config.SERVICES_RATE_BONUS_ENABLED ? _bonus : 1;
	}

	public void setBonus(final double bonus)
	{
		_bonus = bonus;
	}

	public void setAllowHWIDs(final String hwid)
	{
		if(hwid != null && !hwid.isEmpty())
			ALLOW_HWID = new HardwareID(hwid);
	}

	public long getBonusExpire()
	{
		return _bonus_expire;
	}

	public void setBonusExpire(final long time)
	{
		if(time < 0)
			return;
		if(time < System.currentTimeMillis() / 1000)
		{
			_bonus = 1;
			return;
		}
		_bonus_expire = time;
	}

	public GameClientState getState()
	{
		return _state;
	}

	@Override
	public boolean stateIsConnected()
	{
		return _state == GameClientState.CONNECTED;
	}

	public void setState(final GameClientState state)
	{
		_state = state;
	}

	public boolean onClientPacketFail()
	{
		if(isPacketsFailed())
			return true;

		if(_upTryesRefresh <= 0)
			_upTryesRefresh = System.currentTimeMillis() + 5000;
		else if(_upTryesRefresh < System.currentTimeMillis())
		{
			_upTryesRefresh = System.currentTimeMillis() + 5000;
			_upTryes = 0;
		}

		_upTryes++;
		_upTryesTotal++;

		if(_upTryes > 4 || _upTryesTotal > 10)
		{
			_log.warning("Too many client packet fails, connection closed. IP: " + getIpAddr() + ", account: " + getLoginName());
			final L2Player activeChar = getActiveChar();
			if(activeChar != null)
				activeChar.logout(false, false, true, true);
			else
				closeNow(true);
			_upTryesTotal = Integer.MAX_VALUE;
			return true;
		}
		return false;
	}

	public boolean isPacketsFailed()
	{
		return _upTryesTotal == Integer.MAX_VALUE;
	}

	@Override
	public String toString()
	{
		final TextBuilder tb = TextBuilder.newInstance();

		tb.append("[State: ").append(getState());

		final String host = getIpAddr();
		if(host != null)
			tb.append(" | IP: ").append(host);

		final String account = getLoginName();
		if(account != null)
			tb.append(" | Account: ").append(account);

		final L2Player player = getActiveChar();
		if(player != null)
			tb.append(" | Character: ").append(player.getName());

		tb.append("]");

		final String toString = tb.toString();

		TextBuilder.recycle(tb);

		return toString;
	}

	public void enableOpcodeObfuscation(final int obfuscationKey)
	{
		if(_obfuscator == null)
			_obfuscator = new L2OpcodeObfuscator();
		else
			_obfuscator.disable();
		_obfuscator.initTables(obfuscationKey);
	}

	public void disableOpcodeObfuscation()
	{
		if(_obfuscator != null)
			_obfuscator.disable();
	}

	public L2OpcodeObfuscator getObfuscator()
	{
		if(_obfuscator == null)
			_obfuscator = new L2OpcodeObfuscator(); // по умолчанУю - не актУвен
		return _obfuscator;
	}

	// Для работы с LameGuard
	private int instances;
	private int patch;
	private boolean isProtected = false;

	@Override
	public String getHWID()
	{
		return HWID != null ? HWID.Full : null;
	}

	@Override
	public int getInstanceCount()
	{
		return instances;
	}

	@Override
	public int getPatchVersion()
	{
		return patch;
	}

	@Override
	public boolean isProtected()
	{
		return isProtected;
	}

	@Override
	public void setHWID(final String hwid)
	{
		HWID = new HardwareID(hwid);
	}

	@Override
	public void setInstanceCount(final int instances)
	{
		this.instances = instances;
	}

	@Override
	public void setPatchVersion(final int patch)
	{
		this.patch = patch;
	}

	@Override
	public void setProtected(final boolean isProtected)
	{
		this.isProtected = isProtected;
	}

	@Override
	public boolean isGameClient()
	{
		return true;
	}

	private FIFORunnableQueue<L2GameClient, L2GameClientPacket, L2GameServerPacket> _packetQueue;

	@Override
	public FIFORunnableQueue<L2GameClient, L2GameClientPacket, L2GameServerPacket> getPacketQueue()
	{
		if(_packetQueue == null)
			_packetQueue = new FIFORunnableQueue<L2GameClient, L2GameClientPacket, L2GameServerPacket>(L2GameThreadPools.getInstance())
			{
				/* Instantiating an abstract class */
			};
		return _packetQueue;
	}

	@Override
	public void executePacket(final L2GameClientPacket packet)
	{
		if(packet.blockReadingUntilExecutionIsFinished())
			getPacketQueue().executeNow(packet);
		else
			getPacketQueue().execute(packet);
	}
}
