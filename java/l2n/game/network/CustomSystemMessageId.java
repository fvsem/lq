package l2n.game.network;

import l2n.game.network.serverpackets.SystemMessage;

public enum CustomSystemMessageId
{
	L2PLAYER_SEVEN_SIGNS_TELEPORTED_BEGINN("Вы были телепортироваться в ближайший город в связи с началом периода Проверки Печати."),
	L2PLAYER_SEVEN_SIGNS_TELEPORTED_NOT_SIGNED("Вы были телепортироваться в ближайший город, потому что вы не подписали соглашение."),
	L2PLAYER_CANNOT_IN_TRANSFORM("Вы не можете использовать это действие в преобразованном виде!"),
	L2CB_BUFFER_AUTOBUFF_DISABLED("Авто-бафф выключен."),
	L2CB_BUFFER_YOUR_LEVEL_NOT_ALLOWED("Ваш уровень не отвечает требованиям!"),
	L2CB_BUFFER_DISABLE_IN_EVENT("Нельзя использовать бафф во время ивента!"),
	L2CB_BUFFER_DISABLE_IN_SIEGE("Нельзя использовать бафф во время осады!"),
	L2CB_BUFFER_NOT_ENTER_SCHEME_NAME("Вы не ввели Имя группы."),
	L2CB_BUFFER_CANT_SAVE_MORE_THAN_4_SCHEME("Вы не можете сохранить более 4 групп."),
	L2SKILL_SKILL_TARGET_NOT_HANDLED("Цель навыка в настоящее время не обрабатывается"),
	DUEL_CANCELED("Поединок был отменен из-за дуэлянта занятого в PvP бою."),
	L2NPCINSTANCE_WRONG_PARAMETER("Неправильные параметры команды"),
	L2NPCINSTANCE_GO_AWAY("Уходи, тебе здесь не рады.");

	private final SystemMessage _packet;

	private CustomSystemMessageId(final String msg)
	{
		_packet = new SystemMessage(SystemMessage.S1);
		_packet.addString(msg);
	}

	public final SystemMessage getPacket()
	{
		return _packet;
	}
}
