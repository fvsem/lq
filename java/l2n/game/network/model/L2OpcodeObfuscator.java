package l2n.game.network.model;

public class L2OpcodeObfuscator
{
	private boolean _enabled;
	private int _rand_seed;
	private int _opcodeSize1;
	private int _opcodeSize2;
	private int _opcodeSize3;

	private int[] _decodeTable1;
	private int[] _decodeTable2;
	private int[] _decodeTable3;

	public L2OpcodeObfuscator()
	{
		disable();
	}

	public void disable()
	{
		_enabled = false;
		_rand_seed = 0;
		_opcodeSize1 = _opcodeSize2 = _opcodeSize3 = 0;
		_decodeTable1 = _decodeTable2 = _decodeTable3 = null;
	}

	public void initTables(int obfuscationKey)
	{

		int i = 0;
		int tmp = 0;
		int position = 0;
		int clientPosition = 0;

		_opcodeSize1 = 0xd0;
		_opcodeSize2 = 0x80;

		_opcodeSize3 = 0x64;

		_decodeTable1 = new int[_opcodeSize1 + 1];
		_decodeTable2 = new int[_opcodeSize2 + 1];
		_decodeTable3 = new int[_opcodeSize3 + 1];

		for(i = 0; i <= _opcodeSize1; i++)
			_decodeTable1[i] = i;
		for(i = 0; i <= _opcodeSize2; i++)
			_decodeTable2[i] = i;
		for(i = 0; i <= _opcodeSize3; i++)
			_decodeTable3[i] = i;

		pseudoRandSeed(obfuscationKey);

		for(i = 1; i <= _opcodeSize1; i++)
		{
			position = pseudoRand() % (i + 1);
			tmp = _decodeTable1[position];
			_decodeTable1[position] = _decodeTable1[i];
			_decodeTable1[i] = tmp;
		}

		for(i = 1; i <= _opcodeSize2; i++)
		{
			position = pseudoRand() % (i + 1);
			tmp = _decodeTable2[position];
			_decodeTable2[position] = _decodeTable2[i];
			_decodeTable2[i] = tmp;
		}

		for(i = 1; i <= _opcodeSize3; i++)
		{
			position = pseudoRand() % (i + 1);
			tmp = _decodeTable3[position];
			_decodeTable3[position] = _decodeTable3[i];
			_decodeTable3[i] = tmp;
		}

		clientPosition = 0;
		while (_decodeTable1[clientPosition] != 0x12)
			clientPosition++;
		tmp = _decodeTable1[0x12];
		_decodeTable1[0x12] = 0x12;
		_decodeTable1[clientPosition] = tmp;

		clientPosition = 0;
		while (_decodeTable1[clientPosition] != 0xB1)
			clientPosition++;
		tmp = _decodeTable1[0xB1];
		_decodeTable1[0xB1] = 0xB1;
		_decodeTable1[clientPosition] = tmp;

		_enabled = true;
	}

	public int decodeSingleOpcode(int opcode)
	{
		if(!_enabled)
			return opcode;

		if(opcode > _opcodeSize1)
		{

			return opcode;
		}
		opcode = _decodeTable1[opcode];
		return opcode;
	}

	public int decodeDoubleOpcode(int opcode)
	{
		if(!_enabled)
			return opcode;

		if(opcode > _opcodeSize2)
		{
			return opcode;
		}
		opcode = _decodeTable2[opcode];
		return opcode;
	}

	public int decodeTripleOpcode(int opcode)
	{
		if(!_enabled)
			return opcode;

		if(opcode > _opcodeSize3)
		{
			return opcode;
		}
		opcode = _decodeTable3[opcode];
		return opcode;
	}

	private void pseudoRandSeed(int seed)
	{
		_rand_seed = seed;
	}

	private int pseudoRand()
	{
		int a = _rand_seed * 0x343FD + 0x269EC3 & 0xFFFFFFFF;
		_rand_seed = a;
		int result = _rand_seed >> 0x10 & 0x7FFF;
		return result;
	}
}
