package l2n.game.network.model;

import gnu.trove.map.hash.TIntIntHashMap;
import l2n.Config;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.NetPing;

public class Pinger extends Thread
{
	private static Pinger instance;

	private TIntIntHashMap pingTimes = new TIntIntHashMap();

	public static Pinger getInstance()
	{
		if(instance == null)
			instance = new Pinger();
		return instance;
	}

	private Pinger()
	{}

	public int getPingTimes(int objId)
	{
		return pingTimes.get(objId);
	}

	public void answerPing(int objId)
	{
		if(!Config.PING_ENABLED)
			return;

		synchronized (pingTimes)
		{
			pingTimes.remove(objId);
		}
	}

	@Override
	public void run()
	{
		for(;;)
		{
			try
			{
				Thread.sleep(Config.PING_INTERVAL);
			}
			catch(final InterruptedException e)
			{}

			try
			{
				TIntIntHashMap newPingTimes = new TIntIntHashMap();
				synchronized (pingTimes)
				{
					for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
					{
						if(player == null || player.isInOfflineMode() || player.isInStoreMode() || player.isInCraftMode())
							continue;

						int objectId = player.getObjectId();
						final int times = getPingTimes(objectId);
						if(times > Config.PING_IGNORED_REQEST_LIMIT - 1)
							kickPlayer(player);
						else
							newPingTimes.put(objectId, times + 1);

						player.sendPacket(new NetPing(player.getObjectId()));
					}
				}
				pingTimes = newPingTimes;
			}
			catch(Exception ignored)
			{};
		}
	}

	private static void kickPlayer(L2Player player)
	{
		if(player != null)
			player.logout(false, false, true, true);
	}
}
