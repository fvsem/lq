package l2n.game.network.model;

import l2n.game.model.instances.L2ItemInstance;

public final class ItemInfo
{
	private final short lastChange;
	private final short type1;
	private final int objectId;
	private final int itemId;
	private final long count;
	private final short type2;
	private final short customType1;
	private final boolean isEquipped;
	private final int bodyPart;
	private final short enchantLevel;
	private final short customType2;
	private final int augmentationId;
	private final int shadowLifeTime;
	private final int[] attackElement;
	private final int defenceFire;
	private final int defenceWater;
	private final int defenceWind;
	private final int defenceEarth;
	private final int defenceHoly;
	private final int defenceUnholy;
	private final int equipSlot;
	private final int temporalLifeTime;

	public ItemInfo(final L2ItemInstance item)
	{
		lastChange = (short) item.getLastChange();
		type1 = (short) item.getItem().getType1();
		objectId = item.getObjectId();
		itemId = item.getItemId();
		count = item.getCount();
		type2 = (short) item.getItem().getType2ForPackets();
		customType1 = (short) item.getCustomType1();
		isEquipped = item.isEquipped();
		bodyPart = item.getItem().getBodyPart();
		enchantLevel = (short) item.getEnchantLevel();
		customType2 = (short) item.getCustomType2();
		augmentationId = item.getAugmentationId();
		shadowLifeTime = item.isShadowItem() ? item.getLifeTimeRemaining() : -1;
		attackElement = item.getAttackElement();
		defenceFire = item.getDefenceFire();
		defenceWater = item.getDefenceWater();
		defenceWind = item.getDefenceWind();
		defenceEarth = item.getDefenceEarth();
		defenceHoly = item.getDefenceHoly();
		defenceUnholy = item.getDefenceUnholy();
		equipSlot = item.getEquipSlot();
		temporalLifeTime = item.isTemporalItem() ? item.getLifeTimeRemaining() : 0x00;
	}

	public short getLastChange()
	{
		return lastChange;
	}

	public short getType1()
	{
		return type1;
	}

	public int getObjectId()
	{
		return objectId;
	}

	public int getItemId()
	{
		return itemId;
	}

	public long getCount()
	{
		return count;
	}

	public short getType2()
	{
		return type2;
	}

	public short getCustomType1()
	{
		return customType1;
	}

	public boolean isEquipped()
	{
		return isEquipped;
	}

	public int getBodyPart()
	{
		return bodyPart;
	}

	public short getEnchantLevel()
	{
		return enchantLevel;
	}

	public int getAugmentationId()
	{
		return augmentationId;
	}

	public int getShadowLifeTime()
	{
		return shadowLifeTime;
	}

	public short getCustomType2()
	{
		return customType2;
	}

	public int[] getAttackElement()
	{
		return attackElement;
	}

	public int getDefenceFire()
	{
		return defenceFire;
	}

	public int getDefenceWater()
	{
		return defenceWater;
	}

	public int getDefenceWind()
	{
		return defenceWind;
	}

	public int getDefenceEarth()
	{
		return defenceEarth;
	}

	public int getDefenceHoly()
	{
		return defenceHoly;
	}

	public int getDefenceUnholy()
	{
		return defenceUnholy;
	}

	public int getEquipSlot()
	{
		return equipSlot;
	}

	public int getTemporalLifeTime()
	{
		return temporalLifeTime;
	}
}
