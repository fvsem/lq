package l2n.game.geodata.matches;

import l2n.Config;
import l2n.game.geodata.GeoEngine;

public class GeoCheckOptimized
{
	public static void main(String[] args) throws Exception
	{
		common.init();
		Config.GEOFILES_PATTERN = "(\\d{2}_\\d{2})\\.l2j";
		Config.ALLOW_DOORS = false;
		Config.COMPACT_MODE = true;
		GeoEngine.loadGeo();
		common.PromptEnterToContinue();
	}
}
