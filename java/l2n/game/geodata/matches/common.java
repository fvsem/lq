package l2n.game.geodata.matches;

import l2n.Config;
import l2n.Server;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class common
{
	public static final Logger log = Logger.getLogger(common.class.getName());
	public static byte[] dummy;

	public static void SetDummyUseMem(int sz)
	{
		dummy = new byte[sz];
	}

	public static void GC()
	{
		System.gc();
		logMem();
	}

	public static void logMem()
	{
		long maxMem = Runtime.getRuntime().maxMemory();
		long freeMem = Runtime.getRuntime().freeMemory() + maxMem - Runtime.getRuntime().totalMemory();
		long usedMem = maxMem - freeMem;
		log.info("Used memory " + (int) (usedMem / 1048576) + " Mb of " + (int) (maxMem / 1048576) + " Mb (" + (int) (freeMem / 1048576) + " Mb is free)");
	}

	public static void init() throws Exception
	{
		Server.SERVER_MODE = Server.MODE_GAMESERVER;

		InputStream is = new FileInputStream(new File("./config/log.ini"));
		LogManager.getLogManager().readConfiguration(is);
		is.close();

		Config.load();
		GC();
	}

	public static boolean YesNoPrompt(String prompt)
	{
		while (true)
		{
			System.out.print(prompt + " [Y/N]: ");
			String s = System.console().readLine();
			if(s.equalsIgnoreCase("Y") || s.equalsIgnoreCase("Yes") || s.equalsIgnoreCase("True"))
				return true;
			if(s.equalsIgnoreCase("N") || s.equalsIgnoreCase("No") || s.equalsIgnoreCase("False"))
				return false;
		}
	}

	public static void PromptEnterToContinue()
	{
		System.out.print("Press Enter to continue...");
		System.console().readLine();
	}
}
