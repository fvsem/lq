package l2n.game.geodata;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.geodata.GeoOptimizer.BlockLink;
import l2n.game.model.L2Object;
import l2n.game.model.L2Territory;
import l2n.game.model.L2World;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.tables.DoorTable;
import l2n.util.Location;
import l2n.util.Log;
import l2n.util.Rnd;
import l2n.util.Util;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeoEngine
{
	private static final Logger log = Logger.getLogger(GeoEngine.class.getName());

	public static final byte EAST = 1, WEST = 2, SOUTH = 4, NORTH = 8, NSWE_ALL = 15, NSWE_NONE = 0;

	public static final byte BLOCKTYPE_FLAT = 0;
	public static final byte BLOCKTYPE_COMPLEX = 1;
	public static final byte BLOCKTYPE_MULTILEVEL = 2;

	public static final int GEODATA_SIZE_X = L2World.WORLD_SIZE_X;
	public static final int GEODATA_SIZE_Y = L2World.WORLD_SIZE_Y;

	public static final byte GEODATA_ARRAY_OFFSET_X = 11;
	public static final byte GEODATA_ARRAY_OFFSET_Y = 10;
	public static final int BLOCKS_IN_MAP = 256 * 256;
	public static int MAX_LAYERS = 1; // меньше 1 быть не должно, что бы создавались временные массивы как минимум

	private static final int DOOR_MAXZDIFF = 256;

	private static final byte[][][][] geodata = new byte[GEODATA_SIZE_X][GEODATA_SIZE_Y][][];

	public static short getType(final int x, final int y)
	{
		return NgetType(x - L2World.MAP_MIN_X >> 4, y - L2World.MAP_MIN_Y >> 4);
	}

	public static int getHeight(final Location loc)
	{
		return getHeight(loc.getX(), loc.getY(), loc.getZ());
	}

	public static int getHeight(final int x, final int y, final int z)
	{
		if(!Config.GEODATA_ENABLED)
			return z;

		return NgetHeight(x - L2World.MAP_MIN_X >> 4, y - L2World.MAP_MIN_Y >> 4, z);
	}

	public static boolean canMoveToCoord(final int x, final int y, final int z, final int tx, final int ty, final int tz)
	{
		return canMove(x, y, z, tx, ty, tz, false) == 0;
	}

	public static byte getNSWE(final int x, final int y, final int z)
	{
		return NgetNSWE(x - L2World.MAP_MIN_X >> 4, y - L2World.MAP_MIN_Y >> 4, z);
	}

	public static Location moveCheck(final int x, final int y, final int z, final int tx, final int ty)
	{
		return moveCheck(x, y, z, tx, ty, false, false, false);
	}

	public static Location moveCheck(final int x, final int y, final int z, final int tx, final int ty, final boolean returnPrev)
	{
		return moveCheck(x, y, z, tx, ty, false, false, returnPrev);
	}

	public static Location moveCheckWithCollision(final int x, final int y, final int z, final int tx, final int ty)
	{
		return moveCheck(x, y, z, tx, ty, true, false, false);
	}

	public static Location moveCheckWithCollision(final int x, final int y, final int z, final int tx, final int ty, final boolean returnPrev)
	{
		return moveCheck(x, y, z, tx, ty, true, false, returnPrev);
	}

	public static Location moveCheckBackward(final int x, final int y, final int z, final int tx, final int ty)
	{
		return moveCheck(x, y, z, tx, ty, false, true, false);
	}

	public static Location moveCheckBackward(final int x, final int y, final int z, final int tx, final int ty, final boolean returnPrev)
	{
		return moveCheck(x, y, z, tx, ty, false, true, returnPrev);
	}

	public static Location moveCheckBackwardWithCollision(final int x, final int y, final int z, final int tx, final int ty)
	{
		return moveCheck(x, y, z, tx, ty, true, true, false);
	}

	public static Location moveCheckBackwardWithCollision(final int x, final int y, final int z, final int tx, final int ty, final boolean returnPrev)
	{
		return moveCheck(x, y, z, tx, ty, true, true, returnPrev);
	}

	public static Location moveInWaterCheck(final int x, final int y, final int z, final int tx, final int ty, final int waterZ, final int tz)
	{
		return MoveInWaterCheck(x - L2World.MAP_MIN_X >> 4, y - L2World.MAP_MIN_Y >> 4, z, tx - L2World.MAP_MIN_X >> 4, ty - L2World.MAP_MIN_Y >> 4, waterZ, tz);
	}

	public static Location moveCheckForAI(final Location loc1, final Location loc2)
	{
		return moveCheckForAI(loc1.getX() - L2World.MAP_MIN_X >> 4, loc1.getY() - L2World.MAP_MIN_Y >> 4, loc1.getZ(), loc2.getX() - L2World.MAP_MIN_X >> 4, loc2.getY() - L2World.MAP_MIN_Y >> 4);
	}

	public static boolean canSeeTarget(final L2Object actor, final L2Object target, final boolean air)
	{
		if(target == null)
			return false;
		// Костыль конечно, но решает кучу проблем с дверьми
		if(target instanceof GeoControl || actor.equals(target))
			return true;

		return canSeeCoord(actor, target.getX(), target.getY(), target.getZ() + (int) target.getColHeight() + 64, air);
	}

	public static boolean canSeeCoord(final L2Object actor, final int tx, final int ty, final int tz, final boolean air)
	{
		return actor != null && canSeeCoord(actor.getX(), actor.getY(), actor.getZ() + (int) actor.getColHeight() + 64, tx, ty, tz, air);
	}

	public static boolean canSeeCoord(final int x, final int y, final int z, final int tx, final int ty, final int tz, final boolean air)
	{
		final int mx = x - L2World.MAP_MIN_X >> 4;
		final int my = y - L2World.MAP_MIN_Y >> 4;
		final int tmx = tx - L2World.MAP_MIN_X >> 4;
		final int tmy = ty - L2World.MAP_MIN_Y >> 4;
		return canSee(mx, my, z, tmx, tmy, tz, air).equals(tmx, tmy, tz) && canSee(tmx, tmy, tz, mx, my, z, air).equals(mx, my, z);
	}

	public static boolean canMoveWithCollision(final int x, final int y, final int z, final int tx, final int ty, final int tz)
	{
		return canMove(x, y, z, tx, ty, tz, true) == 0;
	}

	public static Location moveCheckInAir(final L2Object actor, final int tx, final int ty, final int tz)
	{
		if(actor == null)
			return null;

		return moveCheckInAir(actor.getX(), actor.getY(), actor.getZ(), tx, ty, tz);
	}

	public static Location moveCheckInAir(final int x, final int y, final int z, final int tx, final int ty, int tz)
	{
		final int mx = x - L2World.MAP_MIN_X >> 4;
		final int my = y - L2World.MAP_MIN_Y >> 4;
		final int tmx = tx - L2World.MAP_MIN_X >> 4;
		final int tmy = ty - L2World.MAP_MIN_Y >> 4;

		final int nz = NgetHeight(tmx, tmy, tz);

		// Не даем опуститься ниже, чем пол + 32
		if(tz <= nz + 32)
			tz = nz + 32;

		final Location result = canSee(mx, my, z, tmx, tmy, tz, true);
		if(result.equals(mx, my, z))
			return null;

		return result.geo2world();
	}

	public static boolean checkNSWE(final byte NSWE, final int x, final int y, final int tx, final int ty)
	{
		if(NSWE == NSWE_ALL)
			return true;
		if(NSWE == NSWE_NONE)
			return false;
		if(tx > x)
		{
			if((NSWE & EAST) == 0)
				return false;
		}
		else if(tx < x)
			if((NSWE & WEST) == 0)
				return false;
		if(ty > y)
		{
			if((NSWE & SOUTH) == 0)
				return false;
		}
		else if(ty < y)
			if((NSWE & NORTH) == 0)
				return false;
		return true;
	}

	private static boolean NLOS_WATER(final int x, final int y, final int z, final int next_x, final int next_y, final int next_z)
	{
		final Layer[] layers1 = NGetLayers(x, y);
		final Layer[] layers2 = NGetLayers(next_x, next_y);
		if(layers1.length == 0 || layers2.length == 0)
			return true;

		// Находим ближайший к целевой клетке слой
		short z2 = Short.MIN_VALUE;
		for(final Layer layer : layers2)
			if(Math.abs(next_z - z2) > Math.abs(next_z - layer.height))
				z2 = layer.height;

		// Луч проходит над преградой
		if(next_z + 32 >= z2)
			return true;

		// Либо перед нами стена, либо над нами потолок. Ищем слой пониже, для уточнения
		short z3 = Short.MIN_VALUE;
		for(final Layer layer : layers2)
			if(layer.height < z2 + Config.MIN_LAYER_HEIGHT && Math.abs(next_z - z3) > Math.abs(next_z - layer.height))
				z3 = layer.height;

		// Ниже нет слоев, значит это стена
		if(z3 == Short.MIN_VALUE)
			return false;

		// Собираем данные о предыдущей клетке, игнорируя верхние слои
		short z1 = Short.MIN_VALUE;
		byte NSWE1 = NSWE_ALL;
		for(final Layer layer : layers1)
			if(layer.height < z + Config.MIN_LAYER_HEIGHT && Math.abs(z - z1) > Math.abs(z - layer.height))
			{
				z1 = layer.height;
				NSWE1 = layer.nswe;
			}

		// Невозможная ситуация, но все же...
		if(z1 < -30000)
			return true;

		// Если есть NSWE, то считаем за стену
		return checkNSWE(NSWE1, x, y, next_x, next_y);
	}

	private static int findNearestLowerLayer(final short[] layers, final int z)
	{
		short h, nearest_layer_h = Short.MIN_VALUE;
		int nearest_layer = Integer.MIN_VALUE;
		for(int i = 1; i <= layers[0]; i++)
		{
			h = (short) ((short) (layers[i] & 0x0fff0) >> 1);
			if(h < z && nearest_layer_h < h)
			{
				nearest_layer_h = h;
				nearest_layer = layers[i];
			}
		}
		return nearest_layer;
	}

	private static short CheckNoOneLayerInRangeAndFindNearestLowerLayer(final short[] layers, final int z0, final int z1)
	{
		int z_min, z_max;
		if(z0 > z1)
		{
			z_min = z1;
			z_max = z0;
		}
		else
		{
			z_min = z0;
			z_max = z1;
		}
		short h, nearest_layer = Short.MIN_VALUE, nearest_layer_h = Short.MIN_VALUE;
		for(int i = 1; i <= layers[0]; i++)
		{
			h = (short) ((short) (layers[i] & 0x0fff0) >> 1);
			if(z_min <= h && h <= z_max)
				return Short.MIN_VALUE;
			if(h < z0 && nearest_layer_h < h)
			{
				nearest_layer_h = h;
				nearest_layer = layers[i];
			}
		}
		return nearest_layer;
	}

	public static boolean canSeeWallCheck(final Layer layer, final Layer nearest_lower_neighbor, final byte directionNSWE)
	{
		return (layer.nswe & directionNSWE) != 0 || layer.height <= nearest_lower_neighbor.height || Math.abs(layer.height - nearest_lower_neighbor.height) < Config.MAX_Z_DIFF;
	}

	public static boolean canSeeWallCheck(final short layer, final short nearest_lower_neighbor, final byte directionNSWE, final int curr_z, final boolean air)
	{
		final short nearest_lower_neighborh = (short) ((short) (nearest_lower_neighbor & 0xFFF0) >> 1);
		if(air)
			return nearest_lower_neighborh < curr_z;
		final short layerh = (short) ((short) (layer & 0xFFF0) >> 1);
		final int zdiff = nearest_lower_neighborh - layerh;
		return (layer & 0x0F & directionNSWE) != 0 || zdiff > -Config.MAX_Z_DIFF && zdiff != 0;
	}

	/**
	 * проверка видимости
	 * 
	 * @return возвращает последнюю точку которую видно (в формате геокоординат) в результате (Location) h является кодом, если >= 0 то успешно достигли последней точки, если меньше то не последней
	 */
	public static Location canSee(final int _x, final int _y, final int _z, final int _tx, final int _ty, final int _tz, final boolean air)
	{
		final int diff_x = _tx - _x, diff_y = _ty - _y, diff_z = _tz - _z;
		final int dx = Math.abs(diff_x), dy = Math.abs(diff_y);

		final float steps = Math.max(dx, dy);
		int curr_x = _x, curr_y = _y, curr_z = _z;
		final short[] curr_layers = new short[MAX_LAYERS + 1];
		NGetLayers(curr_x, curr_y, curr_layers);

		final Location result = new Location(_x, _y, _z, -1);

		if(steps == 0)
		{
			if(CheckNoOneLayerInRangeAndFindNearestLowerLayer(curr_layers, curr_z, curr_z + diff_z) != Short.MIN_VALUE)
				result.set(_tx, _ty, _tz, 1);
			return result;
		}

		final float step_x = diff_x / steps, step_y = diff_y / steps, step_z = diff_z / steps;
		final int half_step_z = (int) (step_z / 2);
		float next_x = curr_x, next_y = curr_y, next_z = curr_z;
		int i_next_x, i_next_y, i_next_z, middle_z;
		final short[] tmp_layers = new short[MAX_LAYERS + 1];
		short src_nearest_lower_layer, dst_nearest_lower_layer, tmp_nearest_lower_layer;

		for(int i = 0; i < steps; i++)
		{
			if(curr_layers[0] == 0)
			{
				result.set(_tx, _ty, _tz, 0);
				return result; // Здесь нет геодаты, разрешаем
			}

			next_x += step_x;
			next_y += step_y;
			next_z += step_z;
			i_next_x = (int) (next_x + 0.5f);
			i_next_y = (int) (next_y + 0.5f);
			i_next_z = (int) (next_z + 0.5f);
			middle_z = curr_z + half_step_z;

			if((src_nearest_lower_layer = CheckNoOneLayerInRangeAndFindNearestLowerLayer(curr_layers, curr_z, middle_z)) == Short.MIN_VALUE)
				return result.setH(-10); // либо есть преграждающая поверхность, либо нет снизу слоя и значит это "пустота", то что за стеной или за колоной

			NGetLayers(curr_x, curr_y, curr_layers);
			if(curr_layers[0] == 0)
			{
				result.set(_tx, _ty, _tz, 0);
				return result; // Здесь нет геодаты, разрешаем
			}

			if((dst_nearest_lower_layer = CheckNoOneLayerInRangeAndFindNearestLowerLayer(curr_layers, i_next_z, middle_z)) == Short.MIN_VALUE)
				return result.setH(-11); // либо есть преграда, либо нет снизу слоя и значит это "пустота", то что за стеной или за колоной

			if(curr_x == i_next_x)
			{
				// движемся по вертикали
				if(!canSeeWallCheck(src_nearest_lower_layer, dst_nearest_lower_layer, i_next_y > curr_y ? SOUTH : NORTH, curr_z, air))
					return result.setH(-20);
			}
			else if(curr_y == i_next_y)
			{
				// движемся по горизонтали
				if(!canSeeWallCheck(src_nearest_lower_layer, dst_nearest_lower_layer, i_next_x > curr_x ? EAST : WEST, curr_z, air))
					return result.setH(-21);
			}
			else
			{
				// движемся по диагонали
				NGetLayers(curr_x, i_next_y, tmp_layers);
				if(tmp_layers[0] == 0)
				{
					result.set(_tx, _ty, _tz, 0);
					return result; // Здесь нет геодаты, разрешаем
				}
				if((tmp_nearest_lower_layer = CheckNoOneLayerInRangeAndFindNearestLowerLayer(tmp_layers, i_next_z, middle_z)) == Short.MIN_VALUE)
					return result.setH(-30); // либо есть преграда, либо нет снизу слоя и значит это "пустота", то что за стеной или за колоной

				if(!(canSeeWallCheck(src_nearest_lower_layer, tmp_nearest_lower_layer, i_next_y > curr_y ? SOUTH : NORTH, curr_z, air) && canSeeWallCheck(tmp_nearest_lower_layer, dst_nearest_lower_layer, i_next_x > curr_x ? EAST : WEST, curr_z, air)))
				{
					NGetLayers(i_next_x, curr_y, tmp_layers);
					if(tmp_layers[0] == 0)
					{
						result.set(_tx, _ty, _tz, 0);
						return result; // Здесь нет геодаты, разрешаем
					}
					if((tmp_nearest_lower_layer = CheckNoOneLayerInRangeAndFindNearestLowerLayer(tmp_layers, i_next_z, middle_z)) == Short.MIN_VALUE)
						return result.setH(-31); // либо есть преграда, либо нет снизу слоя и значит это "пустота", то что за стеной или за колоной
					if(!canSeeWallCheck(src_nearest_lower_layer, tmp_nearest_lower_layer, i_next_x > curr_x ? EAST : WEST, curr_z, air))
						return result.setH(-32);
					if(!canSeeWallCheck(tmp_nearest_lower_layer, dst_nearest_lower_layer, i_next_x > curr_x ? EAST : WEST, curr_z, air))
						return result.setH(-33);
				}
			}

			result.set(curr_x, curr_y, curr_z);
			curr_x = i_next_x;
			curr_y = i_next_y;
			curr_z = i_next_z;
		}

		result.set(_tx, _ty, _tz, 0xFF);
		return result;
	}

	private static Location MoveInWaterCheck(int x, int y, int z, final int tx, final int ty, final int tz, final int waterZ)
	{
		int dx = tx - x;
		int dy = ty - y;
		final int dz = tz - z;
		final int inc_x = sign(dx);
		final int inc_y = sign(dy);
		dx = Math.abs(dx);
		dy = Math.abs(dy);
		if(dx + dy == 0)
			return new Location(x, y, z).geo2world();
		final float inc_z_for_x = dx == 0 ? 0 : dz / dx;
		final float inc_z_for_y = dy == 0 ? 0 : dz / dy;
		int prev_x;
		int prev_y;
		int prev_z;
		float next_x = x;
		float next_y = y;
		float next_z = z;
		if(dx >= dy) // dy/dx <= 1
		{
			final int delta_A = 2 * dy;
			int d = delta_A - dx;
			final int delta_B = delta_A - 2 * dx;
			for(int i = 0; i < dx; i++)
			{
				prev_x = x;
				prev_y = y;
				prev_z = z;
				x = (int) next_x;
				y = (int) next_y;
				z = (int) next_z;
				if(d > 0)
				{
					d += delta_B;
					next_x += inc_x;
					next_z += inc_z_for_x;
					next_y += inc_y;
					next_z += inc_z_for_y;
				}
				else
				{
					d += delta_A;
					next_x += inc_x;
					next_z += inc_z_for_x;
				}

				if(next_z >= waterZ || !NLOS_WATER(x, y, z, (int) next_x, (int) next_y, (int) next_z))
					return new Location(prev_x, prev_y, prev_z).geo2world();
			}
		}
		else
		{
			final int delta_A = 2 * dx;
			int d = delta_A - dy;
			final int delta_B = delta_A - 2 * dy;
			for(int i = 0; i < dy; i++)
			{
				prev_x = x;
				prev_y = y;
				prev_z = z;
				x = (int) next_x;
				y = (int) next_y;
				z = (int) next_z;
				if(d > 0)
				{
					d += delta_B;
					next_x += inc_x;
					next_z += inc_z_for_x;
					next_y += inc_y;
					next_z += inc_z_for_y;
				}
				else
				{
					d += delta_A;
					next_y += inc_y;
					next_z += inc_z_for_y;
				}

				if(next_z >= waterZ || !NLOS_WATER(x, y, z, (int) next_x, (int) next_y, (int) next_z))
					return new Location(prev_x, prev_y, prev_z).geo2world();
			}
		}
		return new Location((int) next_x, (int) next_y, (int) next_z).geo2world();
	}

	public static int canMove(final int __x, final int __y, final int _z, final int __tx, final int __ty, final int _tz, final boolean withCollision)
	{
		final int _x = __x - L2World.MAP_MIN_X >> 4;
		final int _y = __y - L2World.MAP_MIN_Y >> 4;
		final int _tx = __tx - L2World.MAP_MIN_X >> 4;
		final int _ty = __ty - L2World.MAP_MIN_Y >> 4;
		final int diff_x = _tx - _x, diff_y = _ty - _y;
		int diff_z = _tz - _z;
		final int dx = Math.abs(diff_x), dy = Math.abs(diff_y);
		int dz = Math.abs(diff_z);
		final float steps = Math.max(dx, dy);

		int curr_x = _x, curr_y = _y, curr_z = _z;
		short[] curr_layers = new short[MAX_LAYERS + 1];
		NGetLayers(curr_x, curr_y, curr_layers);
		if(curr_layers[0] == 0)
			return 0;

		if(steps == 0)
			return -5;

		final float step_x = diff_x / steps, step_y = diff_y / steps;
		float next_x = curr_x, next_y = curr_y;
		int i_next_x, i_next_y;

		short[] next_layers = new short[MAX_LAYERS + 1];
		final short[] temp_layers = new short[MAX_LAYERS + 1];
		short[] curr_next_switcher;

		for(int i = 0; i < steps; i++)
		{
			next_x += step_x;
			next_y += step_y;
			i_next_x = (int) (next_x + 0.5f);
			i_next_y = (int) (next_y + 0.5f);
			NGetLayers(i_next_x, i_next_y, next_layers);
			if((curr_z = NcanMoveNext(curr_x, curr_y, curr_z, curr_layers, i_next_x, i_next_y, next_layers, temp_layers, withCollision)) == Integer.MIN_VALUE)
				return 1;
			curr_next_switcher = curr_layers;
			curr_layers = next_layers;
			next_layers = curr_next_switcher;
			curr_x = i_next_x;
			curr_y = i_next_y;
		}
		diff_z = curr_z - _tz;
		dz = Math.abs(diff_z);
		if(Config.ALLOW_FALL_FROM_WALLS)
			return diff_z < Config.MAX_Z_DIFF ? 0 : diff_z * 10000;
		return dz > Config.MAX_Z_DIFF ? dz * 1000 : 0;
	}

	public static Location moveCheck(final int __x, final int __y, final int _z, final int __tx, final int __ty, final boolean withCollision, final boolean backwardMove, final boolean returnPrev)
	{
		final int _x = __x - L2World.MAP_MIN_X >> 4;
		final int _y = __y - L2World.MAP_MIN_Y >> 4;
		final int _tx = __tx - L2World.MAP_MIN_X >> 4;
		final int _ty = __ty - L2World.MAP_MIN_Y >> 4;

		final int diff_x = _tx - _x, diff_y = _ty - _y;
		final int dx = Math.abs(diff_x), dy = Math.abs(diff_y);
		final float steps = Math.max(dx, dy);
		if(steps == 0)
			return new Location(__x, __y, _z);

		final float step_x = diff_x / steps, step_y = diff_y / steps;
		int curr_x = _x, curr_y = _y, curr_z = _z;
		float next_x = curr_x, next_y = curr_y;
		int i_next_x, i_next_y, i_next_z;

		short[] next_layers = new short[MAX_LAYERS + 1];
		final short[] temp_layers = new short[MAX_LAYERS + 1];
		short[] curr_layers = new short[MAX_LAYERS + 1];

		NGetLayers(curr_x, curr_y, curr_layers);
		int prev_x = curr_x, prev_y = curr_y, prev_z = curr_z;

		for(int i = 0; i < steps; i++)
		{
			next_x += step_x;
			next_y += step_y;
			i_next_x = (int) (next_x + 0.5f);
			i_next_y = (int) (next_y + 0.5f);
			NGetLayers(i_next_x, i_next_y, next_layers);
			if(backwardMove)
			{
				if((i_next_z = NcanMoveNext(i_next_x, i_next_y, curr_z, next_layers, curr_x, curr_y, curr_layers, temp_layers, withCollision)) == Integer.MIN_VALUE)
					break;
			}
			else if((i_next_z = NcanMoveNext(curr_x, curr_y, curr_z, curr_layers, i_next_x, i_next_y, next_layers, temp_layers, withCollision)) == Integer.MIN_VALUE)
				break;
			final short[] curr_next_switcher = curr_layers;
			curr_layers = next_layers;
			next_layers = curr_next_switcher;
			if(returnPrev)
			{
				prev_x = curr_x;
				prev_y = curr_y;
				prev_z = curr_z;
			}
			curr_x = i_next_x;
			curr_y = i_next_y;
			curr_z = i_next_z;
		}

		if(returnPrev)
		{
			curr_x = prev_x;
			curr_y = prev_y;
			curr_z = prev_z;
		}

		if(curr_x == _x && curr_y == _y)
			return new Location(__x, __y, _z);
		return new Location(curr_x, curr_y, curr_z).geo2world();
	}

	public static List<Location> MoveList(final int __x, final int __y, final int _z, final int __tx, final int __ty, final boolean onlyFullPath)
	{
		final int _x = __x - L2World.MAP_MIN_X >> 4;
		final int _y = __y - L2World.MAP_MIN_Y >> 4;
		final int _tx = __tx - L2World.MAP_MIN_X >> 4;
		final int _ty = __ty - L2World.MAP_MIN_Y >> 4;

		final int diff_x = _tx - _x, diff_y = _ty - _y;
		final int dx = Math.abs(diff_x), dy = Math.abs(diff_y);
		final float steps = Math.max(dx, dy);
		if(steps == 0) // Никуда не идем
			return Collections.emptyList();

		final float step_x = diff_x / steps, step_y = diff_y / steps;
		int curr_x = _x, curr_y = _y, curr_z = _z;
		float next_x = curr_x, next_y = curr_y;
		int i_next_x, i_next_y, i_next_z = curr_z;

		short[] next_layers = new short[MAX_LAYERS + 1];
		final short[] temp_layers = new short[MAX_LAYERS + 1];
		short[] curr_layers = new short[MAX_LAYERS + 1];
		short[] curr_next_switcher;

		NGetLayers(curr_x, curr_y, curr_layers);
		if(curr_layers[0] == 0)
			return null;

		final ArrayList<Location> result = new ArrayList<Location>((int) (steps + 1));
		result.add(new Location(curr_x, curr_y, curr_z)); // Первая точка

		for(int i = 0; i < steps; i++)
		{
			next_x += step_x;
			next_y += step_y;
			i_next_x = (int) (next_x + 0.5f);
			i_next_y = (int) (next_y + 0.5f);

			NGetLayers(i_next_x, i_next_y, next_layers);
			if((i_next_z = NcanMoveNext(curr_x, curr_y, curr_z, curr_layers, i_next_x, i_next_y, next_layers, temp_layers, false)) == Integer.MIN_VALUE)
				if(onlyFullPath)
					return null;
				else
					break;

			curr_next_switcher = curr_layers;
			curr_layers = next_layers;
			next_layers = curr_next_switcher;

			curr_x = i_next_x;
			curr_y = i_next_y;
			curr_z = i_next_z;

			result.add(new Location(curr_x, curr_y, curr_z));
		}

		return result;
	}

	private static Location moveCheckForAI(int x, int y, int z, final int tx, final int ty)
	{
		int dx = tx - x;
		int dy = ty - y;
		final int inc_x = sign(dx);
		final int inc_y = sign(dy);
		dx = Math.abs(dx);
		dy = Math.abs(dy);
		if(dx + dy < 2 || dx == 2 && dy == 0 || dx == 0 && dy == 2)
			return new Location(x, y, z).geo2world();
		int prev_x = x;
		int prev_y = y;
		int prev_z = z;
		int next_x = x;
		int next_y = y;
		int next_z = z;
		if(dx >= dy) // dy/dx <= 1
		{
			final int delta_A = 2 * dy;
			int d = delta_A - dx;
			final int delta_B = delta_A - 2 * dx;
			for(int i = 0; i < dx; i++)
			{
				prev_x = x;
				prev_y = y;
				prev_z = z;
				x = next_x;
				y = next_y;
				z = next_z;
				if(d > 0)
				{
					d += delta_B;
					next_x += inc_x;
					next_y += inc_y;
				}
				else
				{
					d += delta_A;
					next_x += inc_x;
				}
				next_z = NcanMoveNextForAI(x, y, z, next_x, next_y);
				if(next_z == 0)
					return new Location(prev_x, prev_y, prev_z).geo2world();
			}
		}
		else
		{
			final int delta_A = 2 * dx;
			int d = delta_A - dy;
			final int delta_B = delta_A - 2 * dy;
			for(int i = 0; i < dy; i++)
			{
				prev_x = x;
				prev_y = y;
				prev_z = z;
				x = next_x;
				y = next_y;
				z = next_z;
				if(d > 0)
				{
					d += delta_B;
					next_x += inc_x;
					next_y += inc_y;
				}
				else
				{
					d += delta_A;
					next_y += inc_y;
				}
				next_z = NcanMoveNextForAI(x, y, z, next_x, next_y);
				if(next_z == 0)
					return new Location(prev_x, prev_y, prev_z).geo2world();
			}
		}
		return new Location(next_x, next_y, next_z).geo2world();
	}

	private static boolean NcanMoveNextExCheck(final int x, final int y, final int h, final int nextx, final int nexty, final int hexth, final short[] temp_layers)
	{
		NGetLayers(x, y, temp_layers);
		if(temp_layers[0] == 0)
			return true;

		int temp_layer;
		if((temp_layer = findNearestLowerLayer(temp_layers, h + Config.MIN_LAYER_HEIGHT)) == Integer.MIN_VALUE)
			return false;
		final short temp_layer_h = (short) ((short) (temp_layer & 0x0fff0) >> 1);
		if(Math.abs(temp_layer_h - hexth) >= Config.MAX_Z_DIFF || Math.abs(temp_layer_h - h) >= Config.MAX_Z_DIFF)
			return false;
		return checkNSWE((byte) (temp_layer & 0x0F), x, y, nextx, nexty);
	}

	public static int NcanMoveNext(final int x, final int y, final int z, final short[] layers, final int next_x, final int next_y, final short[] next_layers, final short[] temp_layers, final boolean withCollision)
	{
		if(layers[0] == 0 || next_layers[0] == 0)
			return z;

		int layer, next_layer;
		if((layer = findNearestLowerLayer(layers, z + Config.MIN_LAYER_HEIGHT)) == Integer.MIN_VALUE)
			return Integer.MIN_VALUE;

		final byte layer_nswe = (byte) (layer & 0x0F);
		if(!checkNSWE(layer_nswe, x, y, next_x, next_y))
			return Integer.MIN_VALUE;

		final short layer_h = (short) ((short) (layer & 0xFFF0) >> 1);
		if((next_layer = findNearestLowerLayer(next_layers, layer_h + Config.MIN_LAYER_HEIGHT)) == Integer.MIN_VALUE)
			return Integer.MIN_VALUE;

		final short next_layer_h = (short) ((short) (next_layer & 0xFFF0) >> 1);
		/*
		 * if(withCollision && next_layer_h + Config.MAX_Z_DIFF < layer_h) return Integer.MIN_VALUE;
		 */

		// если движение не по диагонали
		if(x == next_x || y == next_y)
		{
			if(withCollision)
			{
				// short[] heightNSWE = temp_layers;
				if(x == next_x)
				{
					NgetHeightAndNSWE(x - 1, y, layer_h, temp_layers);
					if(Math.abs(temp_layers[0] - layer_h) > 15 || !checkNSWE(layer_nswe, x - 1, y, x, y) || !checkNSWE((byte) temp_layers[1], x - 1, y, x - 1, next_y))
						return Integer.MIN_VALUE;

					NgetHeightAndNSWE(x + 1, y, layer_h, temp_layers);
					if(Math.abs(temp_layers[0] - layer_h) > 15 || !checkNSWE(layer_nswe, x + 1, y, x, y) || !checkNSWE((byte) temp_layers[1], x + 1, y, x + 1, next_y))
						return Integer.MIN_VALUE;

					return next_layer_h;
				}

				NgetHeightAndNSWE(x, y - 1, layer_h, temp_layers);
				if(Math.abs(temp_layers[0] - layer_h) >= Config.MAX_Z_DIFF || !checkNSWE(layer_nswe, x, y - 1, x, y) || !checkNSWE((byte) temp_layers[1], x, y - 1, next_x, y - 1))
					return Integer.MIN_VALUE;

				NgetHeightAndNSWE(x, y + 1, layer_h, temp_layers);
				if(Math.abs(temp_layers[0] - layer_h) >= Config.MAX_Z_DIFF || !checkNSWE(layer_nswe, x, y + 1, x, y) || !checkNSWE((byte) temp_layers[1], x, y + 1, next_x, y + 1))
					return Integer.MIN_VALUE;
			}

			return next_layer_h;
		}

		if(!NcanMoveNextExCheck(x, next_y, layer_h, next_x, next_y, next_layer_h, temp_layers))
			return Integer.MIN_VALUE;
		if(!NcanMoveNextExCheck(next_x, y, layer_h, next_x, next_y, next_layer_h, temp_layers))
			return Integer.MIN_VALUE;

		// FIXME if(withCollision)

		return next_layer_h;
	}

	public static int NcanMoveNextForAI(final int x, final int y, final int z, final int next_x, final int next_y)
	{
		final Layer[] layers1 = NGetLayers(x, y);
		final Layer[] layers2 = NGetLayers(next_x, next_y);

		if(layers1.length == 0 || layers2.length == 0)
			return z == 0 ? 1 : z;

		short z1 = Short.MIN_VALUE;
		short z2 = Short.MIN_VALUE;
		byte NSWE1 = NSWE_ALL;
		byte NSWE2 = NSWE_ALL;

		for(final Layer layer : layers1)
			if(layer.height < z + Config.MIN_LAYER_HEIGHT && Math.abs(z - z1) > Math.abs(z - layer.height))
			{
				z1 = layer.height;
				NSWE1 = layer.nswe;
			}

		// Вторая попытка с более мягкими условиями
		if(z1 < -30000)
			for(final Layer layer : layers1)
				if(Math.abs(z - z1) > Math.abs(z - layer.height))
				{
					z1 = layer.height;
					NSWE1 = layer.nswe;
				}

		if(z1 < -30000)
			return 0;

		for(final Layer layer : layers2)
			if(layer.height < z1 + Config.MIN_LAYER_HEIGHT && Math.abs(z1 - z2) > Math.abs(z1 - layer.height))
			{
				z2 = layer.height;
				NSWE2 = layer.nswe;
			}

		// Вторая попытка с более мягкими условиями
		if(z2 < -30000)
			for(final Layer layer : layers2)
				if(Math.abs(z1 - z2) > Math.abs(z1 - layer.height))
				{
					z2 = layer.height;
					NSWE2 = layer.nswe;
				}

		if(z2 < -30000)
			return 0;

		if(z1 > z2 && z1 - z2 > Config.MAX_Z_DIFF)
			return 0;

		if(!checkNSWE(NSWE1, x, y, next_x, next_y) || !checkNSWE(NSWE2, next_x, next_y, x, y))
			return 0;

		return z2 == 0 ? 1 : z2;
	}

	public static void NGetLayers(final int geoX, final int geoY, final short[] result)
	{
		result[0] = 0;
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);
		if(block == null)
			return;

		int cellX, cellY;
		int index = 0;
		// Read current block type: 0 - flat, 1 - complex, 2 - multilevel
		final byte type = block[index];
		index++;

		switch (type)
		{
			case BLOCKTYPE_FLAT:
				short height = makeShort(block[index + 1], block[index]);
				height = (short) (height & 0x0fff0);
				result[0]++;
				result[1] = (short) ((short) (height << 1) | NSWE_ALL);
				return;
			case BLOCKTYPE_COMPLEX:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				index += (cellX << 3) + cellY << 1;
				height = makeShort(block[index + 1], block[index]);
				result[0]++;
				result[1] = height;
				return;
			case BLOCKTYPE_MULTILEVEL:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				int offset = (cellX << 3) + cellY;
				while (offset > 0)
				{
					final byte lc = block[index];
					index += (lc << 1) + 1;
					offset--;
				}
				byte layer_count = block[index];
				index++;
				if(layer_count <= 0 || layer_count > MAX_LAYERS)
					return;
				result[0] = layer_count;
				while (layer_count > 0)
				{
					result[layer_count] = makeShort(block[index + 1], block[index]);
					layer_count--;
					index += 2;
				}
				return;
			default:
				log.severe("GeoEngine: Unknown block type");
				return;
		}
	}

	private static Layer[] NGetLayers(final int geoX, final int geoY)
	{
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);

		if(block == null)
			return new Layer[0];

		int cellX, cellY;
		int index = 0;
		// Read current block type: 0 - flat, 1 - complex, 2 - multilevel
		final byte type = block[index];
		index++;

		switch (type)
		{
			case BLOCKTYPE_FLAT:
				short height = makeShort(block[index + 1], block[index]);
				height = (short) (height & 0xFFF0);
				return new Layer[] { new Layer(height, NSWE_ALL) };
			case BLOCKTYPE_COMPLEX:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				index += (cellX << 3) + cellY << 1;
				height = makeShort(block[index + 1], block[index]);
				return new Layer[] { new Layer((short) ((short) (height & 0x0fff0) >> 1), (byte) (height & 0xF)) };
			case BLOCKTYPE_MULTILEVEL:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				int offset = (cellX << 3) + cellY;
				while (offset > 0)
				{
					final byte lc = block[index];
					index += (lc << 1) + 1;
					offset--;
				}
				byte layer_count = block[index];
				index++;
				if(layer_count <= 0 || layer_count > MAX_LAYERS)
					return new Layer[0];
				final Layer[] layers = new Layer[layer_count];
				while (layer_count > 0)
				{
					height = makeShort(block[index + 1], block[index]);
					layer_count--;
					layers[layer_count] = new Layer((short) ((short) (height & 0xFFF0) >> 1), (byte) (height & 0xF));
					index += 2;
				}
				return layers;
			default:
				log.severe("GeoEngine: Unknown block type");
				return new Layer[0];
		}
	}

	private static short NgetType(final int geoX, final int geoY)
	{
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);

		if(block == null)
			return 0;

		return block[0];
	}

	public static int NgetHeight(final int geoX, final int geoY, final int z)
	{
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);

		if(block == null)
			return z;

		int cellX, cellY, index = 0;

		// Read current block type: 0 - flat, 1 - complex, 2 - multilevel
		final byte type = block[index];
		index++;

		short height;
		switch (type)
		{
			case BLOCKTYPE_FLAT:
				height = makeShort(block[index + 1], block[index]);
				return (short) (height & 0x0fff0);
			case BLOCKTYPE_COMPLEX:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				index += (cellX << 3) + cellY << 1;
				height = makeShort(block[index + 1], block[index]);
				return (short) ((short) (height & 0x0fff0) >> 1); // height / 2
			case BLOCKTYPE_MULTILEVEL:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				int offset = (cellX << 3) + cellY;
				while (offset > 0)
				{
					final byte lc = block[index];
					index += (lc << 1) + 1;
					offset--;
				}
				byte layers = block[index];
				index++;
				if(layers <= 0 || layers > MAX_LAYERS)
					return (short) z;

				final int z_nearest_lower_limit = z + Config.MIN_LAYER_HEIGHT;
				int z_nearest_lower = Integer.MIN_VALUE;
				int z_nearest = Integer.MIN_VALUE;

				while (layers > 0)
				{
					height = (short) ((short) (makeShort(block[index + 1], block[index]) & 0x0fff0) >> 1);
					if(height < z_nearest_lower_limit)
						z_nearest_lower = Math.max(z_nearest_lower, height);
					else if(Math.abs(z - height) < Math.abs(z - z_nearest))
						z_nearest = height;
					layers--;
					index += 2;
				}

				return z_nearest_lower != Integer.MIN_VALUE ? z_nearest_lower : z_nearest;
			default:
				log.severe("GeoEngine: Unknown blockType");
				return z;
		}
	}

	public static byte NgetNSWE(final int geoX, final int geoY, final int z)
	{
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);

		if(block == null)
			return NSWE_ALL;

		int cellX, cellY;
		int index = 0;

		// Read current block type: 0 - flat, 1 - complex, 2 - multilevel
		final byte type = block[index];
		index++;

		switch (type)
		{
			case BLOCKTYPE_FLAT:
				return NSWE_ALL;
			case BLOCKTYPE_COMPLEX:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				index += (cellX << 3) + cellY << 1;
				short height = makeShort(block[index + 1], block[index]);
				return (byte) (height & 0x0F);
			case BLOCKTYPE_MULTILEVEL:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				int offset = (cellX << 3) + cellY;
				while (offset > 0)
				{
					final byte lc = block[index];
					index += (lc << 1) + 1;
					offset--;
				}
				byte layers = block[index];
				index++;
				if(layers <= 0 || layers > MAX_LAYERS)
					return NSWE_ALL;

				short tempz1 = Short.MIN_VALUE;
				short tempz2 = Short.MIN_VALUE;
				int index_nswe1 = NSWE_NONE;
				int index_nswe2 = NSWE_NONE;
				final int z_nearest_lower_limit = z + Config.MIN_LAYER_HEIGHT;

				while (layers > 0)
				{
					height = (short) ((short) (makeShort(block[index + 1], block[index]) & 0x0fff0) >> 1); // height / 2

					if(height < z_nearest_lower_limit)
					{
						if(height > tempz1)
						{
							tempz1 = height;
							index_nswe1 = index;
						}
					}
					else if(Math.abs(z - height) < Math.abs(z - tempz2))
					{
						tempz2 = height;
						index_nswe2 = index;
					}

					layers--;
					index += 2;
				}

				if(index_nswe1 > 0)
					return (byte) (makeShort(block[index_nswe1 + 1], block[index_nswe1]) & 0x0F);
				if(index_nswe2 > 0)
					return (byte) (makeShort(block[index_nswe2 + 1], block[index_nswe2]) & 0x0F);

				return NSWE_ALL;
			default:
				log.severe("GeoEngine: Unknown block type.");
				return NSWE_ALL;
		}
	}

	public static void NgetHeightAndNSWE(final int geoX, final int geoY, final short z, final short[] result)
	{
		final byte[] block = getGeoBlockFromGeoCoords(geoX, geoY);

		if(block == null)
		{
			result[0] = z;
			result[1] = NSWE_ALL;
			return;
		}

		int cellX, cellY, index = 0;
		short height, NSWE = NSWE_ALL;

		// Read current block type: 0 - flat, 1 - complex, 2 - multilevel
		final byte type = block[index];
		index++;

		switch (type)
		{
			case BLOCKTYPE_FLAT:
				height = makeShort(block[index + 1], block[index]);
				result[0] = (short) (height & 0x0fff0);
				result[1] = NSWE_ALL;
				return;
			case BLOCKTYPE_COMPLEX:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				index += (cellX << 3) + cellY << 1;
				height = makeShort(block[index + 1], block[index]);
				result[0] = (short) ((short) (height & 0x0fff0) >> 1); // height / 2
				result[1] = (short) (height & 0x0F);
				return;
			case BLOCKTYPE_MULTILEVEL:
				cellX = getCell(geoX);
				cellY = getCell(geoY);
				int offset = (cellX << 3) + cellY;
				while (offset > 0)
				{
					final byte lc = block[index];
					index += (lc << 1) + 1;
					offset--;
				}
				byte layers = block[index];
				index++;
				if(layers <= 0 || layers > MAX_LAYERS)
				{
					result[0] = z;
					result[1] = NSWE_ALL;
					return;
				}

				short tempz1 = Short.MIN_VALUE;
				short tempz2 = Short.MIN_VALUE;
				int index_nswe1 = 0;
				int index_nswe2 = 0;
				final int z_nearest_lower_limit = z + Config.MIN_LAYER_HEIGHT;

				while (layers > 0)
				{
					height = (short) ((short) (makeShort(block[index + 1], block[index]) & 0x0fff0) >> 1); // height / 2

					if(height < z_nearest_lower_limit)
					{
						if(height > tempz1)
						{
							tempz1 = height;
							index_nswe1 = index;
						}
					}
					else if(Math.abs(z - height) < Math.abs(z - tempz2))
					{
						tempz2 = height;
						index_nswe2 = index;
					}

					layers--;
					index += 2;
				}

				if(index_nswe1 > 0)
				{
					NSWE = makeShort(block[index_nswe1 + 1], block[index_nswe1]);
					NSWE = (short) (NSWE & 0x0F);
				}
				else if(index_nswe2 > 0)
				{
					NSWE = makeShort(block[index_nswe2 + 1], block[index_nswe2]);
					NSWE = (short) (NSWE & 0x0F);
				}
				result[0] = tempz1 > Short.MIN_VALUE ? tempz1 : tempz2;
				result[1] = NSWE;
				return;
			default:
				log.severe("GeoEngine: Unknown block type.");
				result[0] = z;
				result[1] = NSWE_ALL;
				return;
		}
	}

	protected static short makeShort(final byte b1, final byte b0)
	{
		return (short) (b1 << 8 | b0 & 0xFF);
	}

	protected static int getBlock(final int geoPos)
	{
		return (geoPos >> 3) % 256;
	}

	protected static int getCell(final int geoPos)
	{
		return geoPos % 8;
	}

	protected static int getBlockIndex(final int blockX, final int blockY)
	{
		return (blockX << 8) + blockY;
	}

	private static byte sign(final int x)
	{
		if(x >= 0)
			return +1;
		return -1;
	}

	private static byte[] getGeoBlockFromGeoCoords(final int geoX, final int geoY)
	{
		final int ix = geoX >> 11;
		final int iy = geoY >> 11;

		if(ix < 0 || ix >= GEODATA_SIZE_X || iy < 0 || iy >= GEODATA_SIZE_Y)
			return null;

		final byte[][] region = geodata[ix][iy];

		if(region == null)
			return null;

		final int blockX = getBlock(geoX);
		final int blockY = getBlock(geoY);

		return region[getBlockIndex(blockX, blockY)];
	}

	public static void loadGeo()
	{
		if(Config.GEODATA_ENABLED)
		{
			log.info("GeoEngine: loading Geodata...");
			final Pattern p = Pattern.compile(Config.GEOFILES_PATTERN);

			String fn;
			final GArray<File> geo_files = new GArray<File>();

			final File f = new File("./data/geodata");
			// папка вообще пустая
			if(!f.exists() || !f.isDirectory() || f.isDirectory() && f.listFiles().length == 0)
			{
				Config.GEODATA_ENABLED = false;
				log.info("GeoEngine: files missing, loading aborted error[0]");
				return;
			}
			else if(f.isDirectory() && f.listFiles().length != 0)
				for(final File file : f.listFiles())
				{
					if(file.isHidden() || file.isDirectory())
						continue;

					fn = file.getName();
					final Matcher m = p.matcher(fn);
					if(m.matches())
						geo_files.add(file);
				}

			// нет ни одного подходящего файла
			if(geo_files.isEmpty())
			{
				Config.GEODATA_ENABLED = false;
				log.info("GeoEngine: files missing, loading aborted error[1]");
				return;
			}

			for(final File q : geo_files)
			{
				fn = q.getName();
				fn = fn.substring(0, 5); // обрезаем .l2j
				final String[] xy = fn.split("_");
				LoadGeodataFile(Byte.parseByte(xy[0]), Byte.parseByte(xy[1]));
			}

			log.info("GeoEngine: " + geo_files.size() + " map(s), max layers: " + MAX_LAYERS);

			if(Config.COMPACT_MODE)
				compact(true);

			if(Config.ALLOW_DOORS)
				for(final L2DoorInstance door : DoorTable.getInstance().getDoors())
				{
					if(door.isOpen() || !door.getGeodata())
						continue;
					applyControl(door);
					door.geoOpen = false;
				}
		}
	}

	public static boolean LoadGeodataFile(final byte rx, final byte ry)
	{
		final String fname = "./data/geodata/" + rx + "_" + ry + ".l2j";
		final int ix = rx - Config.GEO_X_FIRST;
		final int iy = ry - Config.GEO_Y_FIRST;

		if(ix < 0 || iy < 0 || ix > (L2World.MAP_MAX_X >> 15) + Math.abs(L2World.MAP_MIN_X >> 15) || iy > (L2World.MAP_MAX_Y >> 15) + Math.abs(L2World.MAP_MIN_Y >> 15))
		{
			log.info("GeoLoader: File " + fname + " was not loaded!!! ");
			return false;
		}

		final File Geo = new File(fname);
		log.info("Loading " + Geo.getName());
		int size, index = 0, block = 0, flor = 0;
		try
		{
			// Create a read-only memory-mapped file
			final FileChannel roChannel = new RandomAccessFile(Geo, "r").getChannel();
			size = (int) roChannel.size();

			final ByteBuffer buffer = roChannel.map(FileChannel.MapMode.READ_ONLY, 0, size);
			roChannel.close();
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			final byte[] geo = new byte[buffer.remaining()];
			buffer.get(geo, 0, geo.length);
			if(size >= BLOCKS_IN_MAP * 3)
			{
				final byte[][] blocks = new byte[BLOCKS_IN_MAP][]; // 256 * 256 блоков в регионе геодаты

				// Indexing geo files, so we will know where each block starts
				for(block = 0; block < blocks.length; block++)
				{
					final byte type = geo[index];
					index++;

					byte[] geoBlock;
					switch (type)
					{
						case BLOCKTYPE_FLAT:

							// Создаем блок геодаты
							geoBlock = new byte[2 + 1];

							// Читаем нужные даные с геодаты
							geoBlock[0] = type;
							geoBlock[1] = geo[index];
							geoBlock[2] = geo[index + 1];

							// Увеличиваем индекс
							index += 2;

							// Добавляем блок геодаты
							blocks[block] = geoBlock;
							break;

						case BLOCKTYPE_COMPLEX:

							// Создаем блок геодаты
							geoBlock = new byte[128 + 1];

							// Читаем даные с геодаты
							geoBlock[0] = type;
							System.arraycopy(geo, index, geoBlock, 1, 128);

							// Увеличиваем индекс
							index += 128;

							// Добавляем блок геодаты
							blocks[block] = geoBlock;
							break;

						case BLOCKTYPE_MULTILEVEL:
							// Оригинальный индекс
							final int orgIndex = index;

							// Считаем длину блока геодаты
							for(int b = 0; b < 64; b++)
							{
								final byte layers = geo[index];
								MAX_LAYERS = Math.max(MAX_LAYERS, layers);
								index += (layers << 1) + 1;
								if(layers > flor)
									flor = layers;
							}

							// Получаем длину
							final int diff = index - orgIndex;

							// Создаем массив геодаты
							geoBlock = new byte[diff + 1];

							// Читаем даные с геодаты
							geoBlock[0] = type;
							System.arraycopy(geo, orgIndex, geoBlock, 1, diff);

							// Добавляем блок геодаты
							blocks[block] = geoBlock;
							break;
						default:
							log.severe("GeoEngine: invalid block type: " + type);
					}
				}

				geodata[ix][iy] = blocks;
				return true;
			}
			return false;
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			log.warning("GeoLoader: Failed to Load GeoFile at block: " + block + "\n");
			return false;
		}
	}

	public static boolean DumpGeodataFile(final int cx, final int cy)
	{
		final byte rx = (byte) (Math.floor((float) cx / (float) 32768) + 20);
		final byte ry = (byte) (Math.floor((float) cy / (float) 32768) + 18);
		final String name = "./log/" + rx + "_" + ry + ".l2j";
		return DumpGeodataFile(name, rx, ry);
	}

	public static boolean DumpGeodataFile(final String _name, final byte rx, final byte ry)
	{
		final int ix = rx - Config.GEO_X_FIRST;
		final int iy = ry - Config.GEO_Y_FIRST;

		final byte[][] geoblocks = geodata[ix][iy];
		if(geoblocks == null)
			return false;

		FileChannel wChannel;
		try
		{
			final File f = new File(_name);
			if(f.exists())
				f.delete();
			wChannel = new RandomAccessFile(_name, "rw").getChannel();

			for(final byte[] geoblock : geoblocks)
			{
				ByteBuffer buffer = ByteBuffer.wrap(geoblock);
				wChannel.write(buffer);
				buffer = null;
			}
			wChannel.close();
		}
		catch(final Exception e)
		{
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private static void copyBlock(final int ix, final int iy, final int blockIndex)
	{
		final byte[][] region = geodata[ix][iy];
		if(region == null)
		{
			Log.add("door at null region? [" + ix + "][" + iy + "]", "doors");
			return;
		}

		final byte[] block = region[blockIndex];
		final byte blockType = block[0];

		switch (blockType)
		{
			case BLOCKTYPE_FLAT:
				short height = makeShort(block[2], block[1]);
				height &= 0x0fff0;
				height <<= 1;
				height |= NORTH;
				height |= SOUTH;
				height |= WEST;
				height |= EAST;
				final byte[] newblock = new byte[129];
				newblock[0] = BLOCKTYPE_COMPLEX;
				for(int i = 1; i < 129; i += 2)
				{
					newblock[i + 1] = (byte) (height >> 8);
					newblock[i] = (byte) (height & 0x00ff);
				}
				region[blockIndex] = newblock;
				break;
			default:
				if(Config.COMPACT_MODE)
					region[blockIndex] = region[blockIndex].clone();
				break;
		}
	}

	private static boolean check_door_z(final int minZ, final int maxZ, final int geoZ)
	{
		if(minZ <= geoZ && geoZ <= maxZ)
			return true;
		return Math.abs((minZ + maxZ) / 2 - geoZ) <= DOOR_MAXZDIFF;
	}

	private static boolean check_cell_in_door(int geoX, int geoY, final L2Territory pos)
	{
		geoX = (geoX << 4) + L2World.MAP_MIN_X + 8;
		geoY = (geoY << 4) + L2World.MAP_MIN_Y + 8;
		for(int ax = geoX; ax < geoX + 16; ax++)
			for(int ay = geoY; ay < geoY + 16; ay++)
				if(pos.isInside(ax, ay))
					return true;
		return false;
	}

	public static void returnGeoAtControl(final GeoControl control)
	{
		final L2Territory pos = control.getGeoPos();
		final HashMap<Long, Byte> around = control.getGeoAround();

		if(around == null)
		{
			log.warning("GeoEngine: try to open door with doorId: " + pos.getId());
			Log.add("GeoEngine: Attempt to open 'not closed' door, doorId " + pos.getId(), "doors");
			return;
		}

		short height;
		byte old_nswe;

		synchronized (around)
		{
			for(final long geoXY : around.keySet())
			{
				final int geoX = (int) geoXY;
				final int geoY = (int) (geoXY >> 32);

				// Получение мировых координат
				final int ix = geoX >> 11;
				final int iy = geoY >> 11;

				// Получение индекса блока
				final int blockX = getBlock(geoX);
				final int blockY = getBlock(geoY);
				final int blockIndex = getBlockIndex(blockX, blockY);

				final byte[][] region = geodata[ix][iy];
				if(region == null)
				{
					Log.add("GeoEngine: Attempt to open door at block with no geodata", "doors");
					return;
				}

				final byte[] block = region[blockIndex];

				final int cellX = getCell(geoX);
				final int cellY = getCell(geoY);

				int index = 0;
				final byte blockType = block[index];
				index++;

				switch (blockType)
				{
					case BLOCKTYPE_COMPLEX:
						index += (cellX << 3) + cellY << 1;

						// Получаем высоту клетки
						height = makeShort(block[index + 1], block[index]);
						old_nswe = (byte) (height & 0x0F);
						height &= 0xfff0;
						height >>= 1;

						// around
						height <<= 1;
						height &= 0xfff0;
						height |= old_nswe;
						if(control.isGeoCloser())
							height |= around.get(geoXY);
						else
							height &= ~around.get(geoXY);

						// Записываем высоту в массив
						block[index + 1] = (byte) (height >> 8);
						block[index] = (byte) (height & 0x00ff);
						break;
					case BLOCKTYPE_MULTILEVEL:
						// Последний валидный индекс для двери
						int neededIndex = -1;

						// Далее следует стандартный механизм получения высоты
						int offset = (cellX << 3) + cellY;
						while (offset > 0)
						{
							final byte lc = block[index];
							index += (lc << 1) + 1;
							offset--;
						}
						byte layers = block[index];
						index++;
						if(layers <= 0 || layers > MAX_LAYERS)
							break;
						short temph = Short.MIN_VALUE;
						old_nswe = NSWE_ALL;
						while (layers > 0)
						{
							height = makeShort(block[index + 1], block[index]);
							final byte tmp_nswe = (byte) (height & 0x0F);
							height &= 0xfff0;
							height >>= 1;
							final int z_diff_last = Math.abs(pos.getZmin() - temph);
							final int z_diff_curr = Math.abs(pos.getZmin() - height);
							if(z_diff_last > z_diff_curr)
							{
								old_nswe = tmp_nswe;
								temph = height;
								neededIndex = index;
							}
							layers--;
							index += 2;
						}
						// around
						temph <<= 1;
						temph &= 0xfff0;
						temph |= old_nswe;
						if(control.isGeoCloser())
							temph |= around.get(geoXY);
						else
							temph &= ~around.get(geoXY);

						// записываем высоту
						block[neededIndex + 1] = (byte) (temph >> 8);
						block[neededIndex] = (byte) (temph & 0x00ff);
						break;
				}
			}
		}
	}

	public static void applyControl(final GeoControl control)
	{
		final L2Territory pos = control.getGeoPos();
		HashMap<Long, Byte> around = control.getGeoAround();

		final boolean first_time = around == null;

		if(around == null)
		{
			around = new HashMap<Long, Byte>();
			final GArray<Long> around_blocks = new GArray<Long>();
			final int minX = pos.getXmin() - L2World.MAP_MIN_X >> 4;
			final int maxX = pos.getXmax() - L2World.MAP_MIN_X >> 4;
			final int minY = pos.getYmin() - L2World.MAP_MIN_Y >> 4;
			final int maxY = pos.getYmax() - L2World.MAP_MIN_Y >> 4;
			for(int geoX = minX; geoX <= maxX; geoX++)
				for(int geoY = minY; geoY <= maxY; geoY++)
					if(check_cell_in_door(geoX, geoY, pos))
						around_blocks.add(makeLong(geoX, geoY));

			for(final long geoXY : around_blocks)
			{
				final int geoX = (int) geoXY;
				final int geoY = (int) (geoXY >> 32);
				final long aroundN_geoXY = makeLong(geoX, geoY - 1); // close S
				final long aroundS_geoXY = makeLong(geoX, geoY + 1); // close N
				final long aroundW_geoXY = makeLong(geoX - 1, geoY); // close E
				final long aroundE_geoXY = makeLong(geoX + 1, geoY); // close W
				around.put(geoXY, NSWE_ALL);
				byte _nswe;
				if(!around_blocks.contains(aroundN_geoXY))
				{
					_nswe = around.containsKey(aroundN_geoXY) ? around.remove(aroundN_geoXY) : 0;
					_nswe |= SOUTH;
					around.put(aroundN_geoXY, _nswe);
				}
				if(!around_blocks.contains(aroundS_geoXY))
				{
					_nswe = around.containsKey(aroundS_geoXY) ? around.remove(aroundS_geoXY) : 0;
					_nswe |= NORTH;
					around.put(aroundS_geoXY, _nswe);
				}
				if(!around_blocks.contains(aroundW_geoXY))
				{
					_nswe = around.containsKey(aroundW_geoXY) ? around.remove(aroundW_geoXY) : 0;
					_nswe |= EAST;
					around.put(aroundW_geoXY, _nswe);
				}
				if(!around_blocks.contains(aroundE_geoXY))
				{
					_nswe = around.containsKey(aroundE_geoXY) ? around.remove(aroundE_geoXY) : 0;
					_nswe |= WEST;
					around.put(aroundE_geoXY, _nswe);
				}
			}
			around_blocks.clear();
			control.setGeoAround(around);
		}

		short height;
		byte old_nswe, close_nswe;

		synchronized (around)
		{
			Long[] around_keys = around.keySet().toArray(new Long[around.size()]);
			for(final long geoXY : around_keys)
			{
				final int geoX = (int) geoXY;
				final int geoY = (int) (geoXY >> 32);

				// Получение мировых координат
				final int ix = geoX >> 11;
				final int iy = geoY >> 11;

				// Получение индекса блока
				final int blockX = getBlock(geoX);
				final int blockY = getBlock(geoY);
				final int blockIndex = getBlockIndex(blockX, blockY);

				// Попытка скопировать блок геодаты, если уже существует, то не скопируется
				if(first_time)
					copyBlock(ix, iy, blockIndex);

				final byte[][] region = geodata[ix][iy];
				if(region == null)
				{
					Log.add("GeoEngine: Attempt to close door at block with no geodata", "doors");
					return;
				}

				final byte[] block = region[blockIndex];

				final int cellX = getCell(geoX);
				final int cellY = getCell(geoY);

				int index = 0;
				final byte blockType = block[index];
				index++;

				switch (blockType)
				{
					case BLOCKTYPE_COMPLEX:
						index += (cellX << 3) + cellY << 1;

						// Получаем высоту клетки
						height = makeShort(block[index + 1], block[index]);
						old_nswe = (byte) (height & 0x0F);
						height &= 0xfff0;
						height >>= 1;

						if(first_time)
						{
							close_nswe = around.remove(geoXY);
							// подходящий слой не найден
							if(!check_door_z(pos.getZmin(), pos.getZmax(), height))
								break;
							if(control.isGeoCloser())
								close_nswe &= old_nswe;
							else
								close_nswe &= ~old_nswe;
							around.put(geoXY, close_nswe);
						}
						else
							close_nswe = around.get(geoXY);

						// around
						height <<= 1;
						height &= 0xfff0;
						height |= old_nswe;
						if(control.isGeoCloser())
							height &= ~close_nswe;
						else
							height |= close_nswe;

						// Записываем высоту в массив
						block[index + 1] = (byte) (height >> 8);
						block[index] = (byte) (height & 0x00ff);
						break;
					case BLOCKTYPE_MULTILEVEL:
						// Последний валидный индекс для двери
						int neededIndex = -1;

						// Далее следует стандартный механизм получения высоты
						int offset = (cellX << 3) + cellY;
						while (offset > 0)
						{
							final byte lc = block[index];
							index += (lc << 1) + 1;
							offset--;
						}
						byte layers = block[index];
						index++;
						if(layers <= 0 || layers > MAX_LAYERS)
							break;
						short temph = Short.MIN_VALUE;
						old_nswe = NSWE_ALL;
						while (layers > 0)
						{
							height = makeShort(block[index + 1], block[index]);
							final byte tmp_nswe = (byte) (height & 0x0F);
							height &= 0xfff0;
							height >>= 1;
							final int z_diff_last = Math.abs(pos.getZmin() - temph);
							final int z_diff_curr = Math.abs(pos.getZmin() - height);
							if(z_diff_last > z_diff_curr)
							{
								old_nswe = tmp_nswe;
								temph = height;
								neededIndex = index;
							}
							layers--;
							index += 2;
						}

						if(first_time)
						{
							close_nswe = around.remove(geoXY);
							// подходящий слой не найден
							if(temph == Short.MIN_VALUE || !check_door_z(pos.getZmin(), pos.getZmax(), temph))
								break;
							if(control.isGeoCloser())
								close_nswe &= old_nswe;
							else
								close_nswe &= ~old_nswe;
							around.put(geoXY, close_nswe);
						}
						else
							close_nswe = around.get(geoXY);

						// around
						temph <<= 1;
						temph &= 0xfff0;
						temph |= old_nswe;
						if(control.isGeoCloser())
							temph &= ~close_nswe;
						else
							temph |= close_nswe;

						// записываем высоту
						block[neededIndex + 1] = (byte) (temph >> 8);
						block[neededIndex] = (byte) (temph & 0x00ff);
						break;
				}
			}
			around_keys = null;
		}
	}

	public static long makeLong(final int nLo, final int nHi)
	{
		return (long) nHi << 32 | nLo & 0x00000000ffffffffL;
	}

	public static Location findPointToStay(final int x, final int y, final int z, final int j, final int k)
	{
		Location pos;
		for(int i = 0; i < 100; i++)
		{
			pos = Rnd.coordsRandomize(x, y, z, 0, j, k);
			if(canMoveToCoord(x, y, z, pos.x, pos.y, pos.z) && canMoveToCoord(pos.x, pos.y, pos.z, x, y, z))
				break;
		}
		return new Location(x, y, z);
	}

	public static void compact(final boolean andClean)
	{
		long freeMemBefore = 0, freeMemAfter = 0;
		long total = 0, optimized = 0;

		if(andClean)
		{
			Util.gc(2, 100);
			freeMemBefore = Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
		}

		for(int mapX = 0; mapX < GEODATA_SIZE_X; mapX++)
			for(int mapY = 0; mapY < GEODATA_SIZE_Y; mapY++)
			{
				if(geodata[mapX][mapY] == null)
					continue;
				total += BLOCKS_IN_MAP;
				final BlockLink[] links = GeoOptimizer.loadBlockMatches("./data/geodata/matches/" + (mapX + Config.GEO_X_FIRST) + "_" + (mapY + Config.GEO_Y_FIRST) + ".matches");
				if(links == null)
					continue;
				for(int i = 0; i < links.length; i++)
				{
					final byte[][] link_region = geodata[links[i].linkMapX][links[i].linkMapY];
					if(link_region == null)
						continue;
					link_region[links[i].linkBlockIndex] = geodata[mapX][mapY][links[i].blockIndex];
					optimized++;
				}
			}

		String logStr = String.format("GeoEngine: - Compacted %d of %d blocks...", new Object[] { optimized, total });

		if(andClean)
		{
			Util.gc(2, 100);
			freeMemAfter = Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
			logStr = String.format("%s Optimized ~%d Mb of memory", new Object[] { logStr, (freeMemAfter - freeMemBefore) / 1048576 });
		}

		log.info(logStr);
	}

	public static boolean equalsData(final byte[] a1, final byte[] a2)
	{
		if(a1.length != a2.length)
			return false;
		for(int i = 0; i < a1.length; i++)
			if(a1[i] != a2[i])
				return false;
		return true;
	}

	public static boolean compareGeoBlocks(final int mapX1, final int mapY1, final int blockIndex1, final int mapX2, final int mapY2, final int blockIndex2)
	{
		return equalsData(geodata[mapX1][mapY1][blockIndex1], geodata[mapX2][mapY2][blockIndex2]);
	}

	private static void initChecksums()
	{
		log.info("GeoEngine: - Generating Checksums...");
		new File("./data/geodata/checksum").mkdirs();
		final ThreadGroup threads = new ThreadGroup("initChecksums");
		GeoOptimizer.checkSums = new int[GEODATA_SIZE_X][GEODATA_SIZE_Y][];
		for(int mapX = 0; mapX < GEODATA_SIZE_X; mapX++)
			for(int mapY = 0; mapY < GEODATA_SIZE_Y; mapY++)
			{
				final byte[][] region = geodata[mapX][mapY];
				if(region == null)
					continue;
				new GeoOptimizer.CheckSumLoader(threads, mapX, mapY, region).start();
				GeoOptimizer.waitForFreeThreads(threads, 3); // TODO в конфиг
			}
		GeoOptimizer.waitForFreeThreads(threads, 0);
		threads.destroy();
	}

	private static void initBlockMatches(final int maxScanRegions)
	{
		log.info("GeoEngine: - Generating Block Matches...");
		new File("./data/geodata/matches").mkdirs();
		final ThreadGroup threads = new ThreadGroup("initBlockMatches");
		for(int mapX = 0; mapX < GEODATA_SIZE_X; mapX++)
			for(int mapY = 0; mapY < GEODATA_SIZE_Y; mapY++)
			{
				if(geodata[mapX][mapY] == null || GeoOptimizer.checkSums == null || GeoOptimizer.checkSums[mapX][mapY] == null)
					continue;
				new GeoOptimizer.GeoBlocksMatchFinder(threads, mapX, mapY, maxScanRegions).start();
				GeoOptimizer.waitForFreeThreads(threads, 3); // TODO в конфиг
			}
		GeoOptimizer.waitForFreeThreads(threads, 0);
		threads.destroy();
	}

	public static void deleteChecksumFiles()
	{
		for(int mapX = 0; mapX < GEODATA_SIZE_X; mapX++)
			for(int mapY = 0; mapY < GEODATA_SIZE_Y; mapY++)
			{
				if(geodata[mapX][mapY] == null)
					continue;
				new File("./data/geodata/checksum/" + (mapX + Config.GEO_X_FIRST) + "_" + (mapY + Config.GEO_Y_FIRST) + ".crc").delete();
			}
	}

	public static void genBlockMatches(final int maxScanRegions)
	{
		initChecksums();
		initBlockMatches(maxScanRegions);
	}

	public static void unload()
	{
		for(int mapX = 0; mapX < GEODATA_SIZE_X; mapX++)
			for(int mapY = 0; mapY < GEODATA_SIZE_Y; mapY++)
				geodata[mapX][mapY] = null;
	}
}
