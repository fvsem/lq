package l2n.extensions.scripts;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.text.Strings;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.items.Inventory;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.ExShowTrace;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.NpcSay;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.MapRegionTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;
import l2n.util.Rnd;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Functions
{
	protected static final Logger _log = Logger.getLogger(Functions.class.getName());

	public Long self, npc;

	public static final Object[] EMPTY_ARG = new Object[0];

	public L2Object getSelf()
	{
		return L2ObjectsStorage.get(self);
	}
	
	public static void sendDebugMessage(L2Player player, String message)
	{
		if(!player.isGM())
			return;
		player.sendMessage(message);
	}
	public L2Player getSelfPlayer()
	{
		return L2ObjectsStorage.getAsPlayer(self);
	}

	public L2NpcInstance getNpc()
	{
		return L2ObjectsStorage.getAsNpc(npc);
	}

	public static ScheduledFuture<?> executeTask(final L2Object object, final String sClass, final String sMethod, final Object[] args, final HashMap<String, Object> variables, final long delay)
	{
		return L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				if(object != null)
					object.callScripts(sClass, sMethod, args, variables);
			}
		}, delay);
	}

	public static ScheduledFuture<?> executeTask(final String sClass, final String sMethod, final Object[] args, final HashMap<String, Object> variables, final long delay)
	{
		return L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
		{
			@Override
			public void run()
			{
				callScripts(sClass, sMethod, args, variables);
			}
		}, delay);
	}

	public static ScheduledFuture<?> executeTask(final L2Object object, final String sClass, final String sMethod, final Object[] args, final long delay)
	{
		return executeTask(object, sClass, sMethod, args, null, delay);
	}

	public static ScheduledFuture<?> executeTask(final String sClass, final String sMethod, final Object[] args, final long delay)
	{
		return executeTask(sClass, sMethod, args, null, delay);
	}

	public static Object callScripts(final String _class, final String method, final Object[] args)
	{
		return callScripts(_class, method, args, null);
	}

	public static Object callScripts(final String _class, final String method, final Object[] args, final HashMap<String, Object> variables)
	{
		if(Scripts.loading)
			return null;

		final Script scriptClass = Scripts.getInstance().getClasses().get(_class);
		if(scriptClass == null)
			return null;

		ScriptObject o;
		try
		{
			o = scriptClass.newInstance();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Error in callScripts class=" + _class + "/method=" + method, e);
			return null;
		}

		if(variables != null)
			for(final Entry<String, Object> obj : variables.entrySet())
				try
				{
					o.setProperty(obj.getKey(), obj.getValue());
				}
				catch(final Exception e)
				{}

		return o.invokeMethod(method, args);
	}

	public static void show(String text, final L2Player self)
	{
		if(text == null || self == null)
			return;
		NpcHtmlMessage msg;
		if(self.getLastNpc() != null)
			msg = new NpcHtmlMessage(self, self.getLastNpc());
		else
			msg = new NpcHtmlMessage(5);

		if(text.endsWith(".html-ru") || text.endsWith(".htm-ru"))
			text = text.substring(0, text.length() - 3);

		if(text.endsWith(".html") || text.endsWith(".htm"))
			msg.setFile(text);
		else
			msg.setHtml(Strings.bbParse(text));
		self.sendPacket(msg);
	}

	public static void show(final CustomMessage message, final L2Player self)
	{
		show(message.toString(), self);
	}

	public static void sendMessage(final String text, final L2Player self)
	{
		self.sendMessage(text);
	}

	public static void sendMessage(final CustomMessage message, final L2Player self)
	{
		self.sendMessage(message);
	}

	public static void npcSayInRange(final L2NpcInstance npc, final String text, final int range)
	{
		npcSayInRange(npc, Say2C.LOCAL, text, range);
	}

	public static void npcSayInRange(final L2NpcInstance npc, final int messageType, final String text, final int range)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, messageType, text);
		for(final L2Player player : L2World.getAroundPlayers(npc, range, 200))
			if(player != null)
				player.sendPacket(cs);
	}

	public static void npcSay(final L2NpcInstance npc, final String text)
	{
		npcSayInRange(npc, text, 1500);
	}

	public static void npcSayToAll(final L2NpcInstance npc, final String text)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, 1, text);
		if(Config.GLOBAL_CHAT < 81)
		{
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null)
					player.sendPacket(cs);
		}
		else
			for(final L2Player player : L2World.getAroundPlayers(npc))
				if(player != null)
					player.sendPacket(cs);
	}

	public static void npcSayToPlayer(final L2NpcInstance npc, final String text, final L2Player player)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, 0, text);

		player.sendPacket(cs);
	}

	public static void npcSayToRandomPlayerInRange(final L2NpcInstance npc, final String text, final int range)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, 0, text);
		GArray<L2Player> players = npc.getAroundPlayers(range);
		if(players.isEmpty())
			return;
		final L2Player player = players.get(Rnd.get(players.size()));
		if(text.contains("%randomPlayer%"))
			text.replace("%randomPlayer%", player.getName());
		player.sendPacket(cs);
	}

	public static void npcShout(final L2NpcInstance npc, final String text)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, 1, text);
		for(final L2Player player : L2World.getAroundPlayers(npc))
			if(player != null)
				player.sendPacket(cs);
	}

	public static void npcShout(final L2NpcInstance npc, final int range, final String text)
	{
		if(npc == null)
			return;
		final NpcSay cs = new NpcSay(npc, 1, text);
		for(final L2Player player : L2World.getAroundPlayers(npc, range, 1500))
			if(player != null)
				player.sendPacket(cs);
	}

	public static void npcSayInRangeCustomMessage(final L2NpcInstance npc, final int range, final String address)
	{
		if(npc == null)
			return;
		for(final L2Player player : L2World.getAroundPlayers(npc, range, 200))
			if(player != null && !player.isBlockAll())
				player.sendPacket(new NpcSay(npc, 0, new CustomMessage(address, player).toString()));
	}

	public static void npcSayInRangeCustomMessage(final L2NpcInstance npc, final int range, final String address, final Object... replacements)
	{
		if(npc == null)
			return;
		for(final L2Player player : L2World.getAroundPlayers(npc, range, 200))
			if(player != null && !player.isBlockAll())
				player.sendPacket(new NpcSay(npc, 0, new CustomMessage(address, player, replacements).toString()));
	}

	public static void npcSayCustomMessage(final L2NpcInstance npc, final String address)
	{
		npcSayInRangeCustomMessage(npc, 1500, address);
	}

	public static void npcSayCustomMessage(final L2NpcInstance npc, final String address, final Object... replacements)
	{
		npcSayInRangeCustomMessage(npc, 1500, address, replacements);
	}

	public static void npcShoutCustomMessage(final L2NpcInstance npc, final String address, final Object... replacements)
	{
		if(npc == null)
			return;

		if(Config.SHOUT_CHAT_MODE == 1)
		{
			for(final L2Player player : L2World.getAroundPlayers(npc, 10000, 1500))
				if(player != null && !player.isBlockAll())
					player.sendPacket(new NpcSay(npc, 1, new CustomMessage(address, player, replacements).toString()));
		}
		else
		{
			final int mapregion = MapRegionTable.getInstance().getMapRegion(npc.getX(), npc.getY());
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null && MapRegionTable.getInstance().getMapRegion(player.getX(), player.getY()) == mapregion && !player.isBlockAll())
					player.sendPacket(new NpcSay(npc, 1, new CustomMessage(address, player, replacements).toString()));
		}
	}

	public static void addItem(final L2Playable playable, final int item_id, final long count)
	{
		if(playable == null || count < 1)
			return;

		L2Playable player;
		if(playable.isSummon())
			player = playable.getPlayer();
		else
			player = playable;

		final L2ItemInstance item = ItemTable.getInstance().createItem(item_id, player.getObjectId(), 0, "Scripts.addItem");
		if(item.isStackable())
		{
			item.setCount(count);
			player.getInventory().addItem(item);
		}
		else
		{
			player.getInventory().addItem(item);
			for(int i = 1; i < count; i++)
				player.getInventory().addItem(ItemTable.getInstance().createItem(item_id, player.getObjectId(), 0, "Scripts.addItem"));
		}

		player.sendPacket(SystemMessage.obtainItems(item));
	}

	public static long getItemCount(final L2Playable playable, final int item_id)
	{
		long count = 0;
		L2Playable player;
		if(playable != null && playable.isSummon())
			player = playable.getPlayer();
		else
			player = playable;
		final Inventory inv = player.getInventory();
		if(inv == null)
			return 0;
		final L2ItemInstance[] items = inv.getItems();
		for(final L2ItemInstance item : items)
			if(item.getItemId() == item_id)
				count += item.getCount();
		return count;
	}

	public static long removeItem(final L2Playable playable, final int item_id, long count)
	{
		if(playable == null || count < 1)
			return 0;

		L2Playable player;
		if(playable.isSummon())
			player = playable.getPlayer();
		else
			player = playable;
		final Inventory inv = player.getInventory();
		if(inv == null)
			return 0;
		long removed = count;
		final L2ItemInstance[] items = inv.getItems();
		for(final L2ItemInstance item : items)
			if(item.getItemId() == item_id && count > 0)
			{
				final long item_count = item.getCount();
				final long rem = count <= item_count ? count : item_count;
				player.getInventory().destroyItemByItemId(item_id, rem, true);
				count -= rem;
			}
		removed -= count;
		if(removed > 0)
			player.sendPacket(SystemMessage.removeItems(item_id, removed));
		return removed;
	}

	public static boolean ride(final L2Player player, final int pet)
	{
		if(player.isMounted())
			player.setMount(0, 0);

		if(player.getPet() != null)
		{
			player.sendPacket(Msg.YOU_ALREADY_HAVE_A_PET);
			return false;
		}

		player.setMount(pet, 0);
		return true;
	}

	public static void unRide(final L2Player player)
	{
		if(player.isMounted())
			player.setMount(0, 0);
	}

	public static void unSummonPet(final L2Player player)
	{
		final L2Summon pet = player.getPet();
		if(pet == null)
			return;
		pet.unSummon();
	}

	public static L2NpcInstance spawn(final Location loc, final int npcId)
	{
		return spawn(loc, npcId, 0);
	}

	public static L2NpcInstance spawn(final Location loc, final int npcId, final long refId)
	{
		try
		{
			final L2NpcInstance npc = NpcTable.getTemplate(npcId).getNewInstance();
			npc.setReflection(refId);
			npc.setSpawnedLoc(loc.correctGeoZ());
			npc.onSpawn();
			npc.spawnMe(npc.getSpawnedLoc());
			return npc;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Could not spawn Npc " + npcId, e);
		}
		return null;
	}

	public static void spawnNPCs(final int npcId, final int[][] locations, final GArray<L2Spawn> list)
	{
		final L2NpcTemplate template = NpcTable.getTemplate(npcId);
		if(template == null)
		{
			System.out.println("WARNING! events.Helper.SpawnNPCs template is null for npc: " + npcId);
			Thread.dumpStack();
			return;
		}
		for(final int[] location : locations)
			try
			{
				final L2Spawn sp = new L2Spawn(template);
				sp.setLoc(new Location(location));
				sp.setAmount(1);
				sp.setRespawnDelay(0);
				sp.init();
				if(list != null)
					list.add(sp);
			}
			catch(final ClassNotFoundException e)
			{
				_log.log(Level.WARNING, "Could not spawn Npc " + npcId, e);
			}
	}

	public static void deSpawnNPCs(final GArray<L2Spawn> list)
	{
		for(final L2Spawn sp : list)
		{
			sp.stopRespawn();
			if(sp.getLastSpawn() != null)
				sp.getLastSpawn().deleteMe();
		}
		list.clear();
	}

	public static boolean isActive(final String name)
	{
		return ServerVariables.getString(name, "off").equalsIgnoreCase("on");
	}

	public static boolean setActive(final String name, final boolean active)
	{
		if(active == isActive(name))
			return false;
		if(active)
			ServerVariables.set(name, "on");
		else
			ServerVariables.unset(name);
		return true;
	}

	public static boolean SimpleCheckDrop(final L2Character mob, final L2Character killer)
	{
		return mob != null && mob.isMonster() && !mob.isRaid() && killer != null && killer.getPlayer() != null && killer.getLevel() - mob.getLevel() < 10;
	}

	public static void removeItemByObjId(final L2Playable playable, final int item_obj_id, final int count)
	{
		if(playable == null || count < 1)
			return;

		L2Playable player;
		if(playable.isSummon())
			player = playable.getPlayer();
		else
			player = playable;
		final Inventory inv = player.getInventory();
		if(inv == null)
			return;
		final L2ItemInstance[] items = inv.getItems();
		for(final L2ItemInstance item : items)
		{
			if(item.getObjectId() != item_obj_id || count <= 0)
				continue;
			final long item_count = item.getCount();
			final int item_id = item.getItemId();
			final long removed = count > item_count ? item_count : count;
			player.getInventory().destroyItem(item, removed, true);

			if(item_id == 57)
				player.sendPacket(new SystemMessage(SystemMessage.S1_ADENA_DISAPPEARED).addNumber(removed));
			else if(removed > 1)
				player.sendPacket(new SystemMessage(SystemMessage.S2_S1_HAS_DISAPPEARED).addItemName(item_id).addNumber(removed));
			else
				player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(item_id));
		}
	}

	public static String htmlButton(final String value, final String action, final int width)
	{
		return Strings.htmlButton(value, action, width);
	}

	public static String htmlButton(final String value, final String action, final int width, final int height)
	{
		return Strings.htmlButton(value, action, width, height);
	}

	public static ExShowTrace Points2Trace(final GArray<int[]> points, final int step, final boolean auto_compleate, final boolean maxz)
	{
		final ExShowTrace result = new ExShowTrace();

		int[] prev = null;
		int[] first = null;
		for(final int[] p : points)
		{
			if(first == null)
				first = p;
			if(prev != null)
				result.addLine(prev[0], prev[1], maxz ? prev[3] : prev[2], p[0], p[1], maxz ? p[3] : p[2], step, 60000);
			prev = p;
		}

		if(prev == null || first == null)
			return result;
		if(auto_compleate)
			result.addLine(prev[0], prev[1], maxz ? prev[3] : prev[2], first[0], first[1], maxz ? first[3] : first[2], step, 60000);
		return result;
	}
}
