package l2n.extensions.scripts;

public interface ScriptFile
{

	public void onLoad();

	public void onReload();

	public void onShutdown();
}
