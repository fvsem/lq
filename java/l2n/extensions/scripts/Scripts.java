package l2n.extensions.scripts;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.Config;
import l2n.Server;
import l2n.commons.compiler.Compiler;
import l2n.commons.compiler.MemoryClassLoader;
import l2n.commons.list.GArray;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.quest.Quest;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ClassUtils;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("unchecked")
public class Scripts extends ScriptFileLoader
{
	private static final Logger _log = Logger.getLogger(Scripts.class.getName());

	private static final String ON_LOAD = "onLoad";
	private static final String ON_RELOAD = "onReload";
	private static final String ON_SHUTDOWN = "onShutdown";

	public static boolean JAR;

	private final HashMap<String, Script> _classes = new HashMap<String, Script>();
	private final GArray<EventScript>[] _scriptEvents = new GArray[ScriptEventType.values().length];

	private static final HashMap<String, ScriptObject> scriptsObjects = new HashMap<String, ScriptObject>(550);

	public static final TIntObjectHashMap<GArray<ScriptClassAndMethod>> itemHandlers = new TIntObjectHashMap<GArray<ScriptClassAndMethod>>();
	public static final TIntObjectHashMap<GArray<ScriptClassAndMethod>> dialogAppends = new TIntObjectHashMap<GArray<ScriptClassAndMethod>>();

	public static boolean loading;

	public static Scripts getInstance()
	{
		return SingletonHolder._instance;
	}

	public static class ScriptClassAndMethod
	{
		public final Script scriptClass;
		public final Method method;

		public ScriptClassAndMethod(final Script scriptClass, final Method method)
		{
			this.scriptClass = scriptClass;
			this.method = method;
		}

		@Override
		public final String toString()
		{
			return scriptClass.getRawClass().getCanonicalName() + " [" + method.getName() + "]";
		}
	}

	public Scripts()
	{
		load(false);
	}

	public void addScriptEvent(final ScriptEventType eventType, final EventScript script)
	{
		final int ordinal = eventType.ordinal();

		GArray<EventScript> scriptsByEvents = null;
		if((scriptsByEvents = _scriptEvents[ordinal]) == null)
		{
			scriptsByEvents = new GArray<EventScript>();
			scriptsByEvents.add(script);
			_scriptEvents[ordinal] = scriptsByEvents;
		}
		else
		{
			EventScript temp;
			for(int i = scriptsByEvents.size(); i-- > 0;)
			{
				temp = scriptsByEvents.getUnsafe(i);
				if(script.getName().equalsIgnoreCase(temp.getName()))
				{
					scriptsByEvents.setUnsafeVoid(i, script);
					_log.log(Level.WARNING, "Replaced: (" + temp.getName() + ") with a new version (" + script.getName() + ") for type: " + eventType.toString());
					return;
				}
			}

			scriptsByEvents.add(script);
		}
	}

	public void removeScriptEvent(final ScriptEventType eventType, final EventScript script)
	{
		final int ordinal = eventType.ordinal();

		final GArray<EventScript> scriptsByEvents = _scriptEvents[ordinal];
		if(scriptsByEvents == null)
			return;
		else
		{
			EventScript temp;
			for(int i = scriptsByEvents.size(); i-- > 0;)
			{
				temp = scriptsByEvents.getUnsafe(i);
				if(script.getName().equalsIgnoreCase(temp.getName()))
					scriptsByEvents.removeUnsafeVoid(i);
			}

			if(scriptsByEvents.isEmpty())
				_scriptEvents[ordinal] = null;
		}
	}

	public final Iterable<EventScript> getEventScripts(final ScriptEventType eventType)
	{
		final GArray<EventScript> srcipts = _scriptEvents[eventType.ordinal()];
		if(srcipts == null)
			return Collections.emptyList();

		return srcipts;
	}

	public boolean reload()
	{
		loading = true;

		for(final ScriptObject go : scriptsObjects.values())
			try
			{
				go.invokeMethod(ON_RELOAD);
			}
			catch(final Exception f)
			{
				_log.log(Level.WARNING, "Scripts.reload() error: ", f);
			}
		scriptsObjects.clear();

		final boolean error = load(true);
		callOnLoad();

		loading = false;
		return error;
	}

	public void shutdown()
	{
		for(final ScriptObject go : scriptsObjects.values())
			try
			{
				go.invokeMethod(ON_SHUTDOWN);
			}
			catch(final Exception f)
			{
				_log.log(Level.WARNING, "Scripts.shutdown() error: ", f);
			}
		scriptsObjects.clear();
	}

	private synchronized boolean load(final boolean reload)
	{
		_log.info("Scripts loading...");
		boolean error = false;

		final File f = new File("lib/scripts.jar");
		if(f.exists())
		{
			JarInputStream stream = null;
			try
			{
				stream = new JarInputStream(new FileInputStream(f));
				JarEntry entry = null;
				while ((entry = stream.getNextJarEntry()) != null)
				{

					if(entry.getName().contains(ClassUtils.INNER_CLASS_SEPARATOR) || !entry.getName().endsWith(".class"))
						continue;

					final String name = entry.getName().replace(".class", "").replace("/", ".");

					final Class<?> clazz = Class.forName(name);

					if(Modifier.isAbstract(clazz.getModifiers()))
						continue;

					_classes.put(clazz.getName(), new Script(clazz));
				}
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Fail to load scripts.jar! ", e);
				error = true;
				_classes.clear();
			}
			finally
			{
				IOUtils.closeQuietly(stream);
			}
		}
		else
		{
			final GArray<File> scriptFiles = new GArray<File>();
			parseClasses(new File("./data/scripts"), scriptFiles);

			if(Compiler.getInstance().compile(scriptFiles, System.out))
			{
				final MemoryClassLoader classLoader = Compiler.getInstance().getClassLoader();
				for(final String name : classLoader.getClasses())
				{
					if(name.contains(ClassUtils.INNER_CLASS_SEPARATOR))
						continue;
					try
					{
						final Class<?> clazz = classLoader.loadClass(name);

						if(Modifier.isAbstract(clazz.getModifiers()))
							continue;

						_classes.put(name, new Script(clazz));
					}
					catch(final ClassNotFoundException e)
					{
						_log.log(Level.WARNING, "Can't load script class: " + e.getMessage(), e);
						error = true;
					}
				}
				Compiler.getInstance().clearClassLoader();
			}
			else
			{
				_log.warning("|==============================================|");
				_log.warning("| Can't compile scripts!");
				_log.warning("| See details in file 'log/developer/scripts_error.txt'!");
				_log.warning("|==============================================|");
				error = true;
			}
		}

		if(error)
		{
			_log.info("Scripts loaded with errors. Loaded " + _classes.size() + " classes.");
			if(!reload)
				Server.halt(0, "Scripts loaded with errors. Loaded " + _classes.size() + " classes.");
		}
		else
			_log.info("Scripts successfully loaded. Loaded " + _classes.size() + " classes.");
		return error;
	}

	public void callOnLoad()
	{
		final TreeMap<String, Script> temp = new TreeMap<String, Script>(_classes);
		loadAndInitHandlers(temp);
	}

	private void loadAndInitHandlers(final TreeMap<String, Script> temp)
	{
		itemHandlers.clear();
		dialogAppends.clear();

		for(final Script _class : temp.values())
			loadAndInitHandlersForClass(_class);

	}

	private void loadAndInitHandlersForClass(final Script clazz)
	{
		try
		{
			if(!scriptsObjects.containsKey(clazz.getName()) && ClassUtils.isAssignable(clazz.getRawClass(), ScriptFile.class))
			{
				final ScriptObject go = clazz.newInstance();
				scriptsObjects.put(clazz.getName(), go);
				go.invokeMethod(ON_LOAD);
			}

			for(final Method method : clazz.getRawClass().getMethods())
				if(method.getName().equals(ON_LOAD))
				{
					if(Modifier.isStatic(method.getModifiers()))
						method.invoke(null);
				}
				else if(method.getName().contains("ItemHandler_"))
				{
					final Integer id = Integer.parseInt(method.getName().substring(12));
					GArray<ScriptClassAndMethod> handlers = itemHandlers.get(id);
					if(handlers == null)
					{
						handlers = new GArray<ScriptClassAndMethod>();
						itemHandlers.put(id, handlers);
					}
					handlers.add(new ScriptClassAndMethod(clazz, method));
				}
				else if(method.getName().contains("DialogAppend_"))
				{
					final Integer id = Integer.parseInt(method.getName().substring(13));
					GArray<ScriptClassAndMethod> handlers = dialogAppends.get(id);
					if(handlers == null)
					{
						handlers = new GArray<ScriptClassAndMethod>();
						dialogAppends.put(id, handlers);
					}
					handlers.add(new ScriptClassAndMethod(clazz, method));
				}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Scripts.loadAndInitHandlersForClass() error: ", e);
		}
	}

	private void clearHandlersForClass(final Script _class)
	{
		try
		{
			for(final GArray<ScriptClassAndMethod> entry : itemHandlers.valueCollection())
			{
				final GArray<ScriptClassAndMethod> toRemove = new GArray<ScriptClassAndMethod>();
				for(final ScriptClassAndMethod sc : entry)
					if(sc.scriptClass == _class)
						toRemove.add(sc);
				for(final ScriptClassAndMethod sc : toRemove)
					entry.remove(sc);
			}
			for(final GArray<ScriptClassAndMethod> entry : dialogAppends.valueCollection())
			{
				final GArray<ScriptClassAndMethod> toRemove = new GArray<ScriptClassAndMethod>();
				for(final ScriptClassAndMethod sc : entry)
					if(sc.scriptClass == _class)
						toRemove.add(sc);

				for(final ScriptClassAndMethod sc : toRemove)
					entry.remove(sc);
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Scripts.clearHandlersForClass() error: ", e);
		}
	}

	public boolean reloadClass(final String name)
	{
		File f = new File("./data/scripts/" + name.replace(".", "/") + ".java");
		if(f.exists() && f.isFile())
			return reloadClassByName(name.replace("/", "."));

		f = new File("./data/scripts/" + name.replace(".", "/"));
		if(f.exists() && f.isDirectory())
			return reloadClassByPath(f);

		_log.warning("Con't find class or package by path: " + name);
		return true;
	}

	public boolean reloadClassByPath(final File f)
	{
		final GArray<File> scriptFiles = new GArray<File>();
		parseClasses(f, scriptFiles);

		if(Compiler.getInstance().compile(scriptFiles, System.out))
		{
			final MemoryClassLoader classLoader = Compiler.getInstance().getClassLoader();
			final TreeMap<String, Script> temp = new TreeMap<String, Script>();

			for(final String name : classLoader.getClasses())
			{
				if(name.contains("$"))
					continue;
				try
				{
					final Class<?> c = classLoader.loadClass(name);
					final Script s = new Script(c);
					final ScriptObject oldSo = scriptsObjects.remove(name);
					if(oldSo != null)
						oldSo.invokeMethod(ON_RELOAD);
					final Script oldS = _classes.remove(name);
					if(oldS != null)
						clearHandlersForClass(oldS);
					_classes.put(name, s);
					temp.put(name, s);
				}
				catch(final ClassNotFoundException e)
				{
					_log.warning("Can't load script class:" + e.getMessage());
					return true;
				}
			}

			Compiler.getInstance().clearClassLoader();
			loadAndInitHandlers(temp);
		}
		else
		{
			_log.warning("Can't recompile scripts: " + f.getPath());
			return true;
		}
		return false;
	}

	public boolean reloadClassByName(final String name)
	{
		if(Compiler.getInstance().compile(new File("./data/scripts/" + name.replace(".", "/") + ".java"), System.out))
		{
			final MemoryClassLoader classLoader = Compiler.getInstance().getClassLoader();
			try
			{
				final Class<?> c = classLoader.loadClass(name);
				final Script s = new Script(c);
				final ScriptObject so = scriptsObjects.remove(name);
				if(so != null)
					so.invokeMethod(ON_RELOAD);
				final Script oldS = _classes.remove(name);
				if(oldS != null)
					clearHandlersForClass(oldS);
				_classes.put(name, s);
				loadAndInitHandlersForClass(s);
				return false;
			}
			catch(final ClassNotFoundException e)
			{
				_log.warning("Can't load script class:" + e.getMessage());
				Compiler.getInstance().clearClassLoader();
			}
		}
		else
			_log.warning("Can't recompile script: " + name);
		return true;
	}

	public boolean reloadQuest(final String name)
	{
		if(Config.DONTLOADQUEST)
			return true;
		Quest q = QuestManager.getQuest(name);
		File f;
		if(q != null)
		{
			final String path = q.getClass().getPackage().getName().replace(".", "/");
			f = new File("./data/scripts/" + path + "/");
			if(f.isDirectory())
				return reloadClassByPath(f);
		}
		q = QuestManager.getQuest(Integer.parseInt(name));
		if(q != null)
		{
			final String path = q.getClass().getPackage().getName().replace(".", "/");
			f = new File("./data/scripts/" + path + "/");
			if(f.isDirectory())
				return reloadClassByPath(f);
		}
		return reloadClassByPath(new File("./data/scripts/quests/" + name + "/"));
	}

	@Override
	protected boolean checkFileCondition(final File file, final boolean dir)
	{
		if(dir)
		{
			if(file.isHidden() || file.getName().equals(".svn"))
				return false;
			if(Config.DONTLOADQUEST && file.getName().equals("quests") && file.getParentFile().getName().equals("scripts"))
				return false;
			
			if((file.getName().equals("ai") || file.getName().equals("npc")) && file.getParentFile().getName().equals("scripts"))
				return false;
		}
		else if(file.isHidden() || !file.getName().contains(".java"))
			return false;

		return true;
	}

	public HashMap<String, Script> getClasses()
	{
		return _classes;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final Scripts _instance = new Scripts();
	}
}
