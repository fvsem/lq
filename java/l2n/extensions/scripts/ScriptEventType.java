package l2n.extensions.scripts;

public enum ScriptEventType
{
	ON_ACTION, // l2n.extensions.scripts.Events.onAction(L2Player, L2Object, boolean)
	ON_ATTACK, // l2n.game.model.actor.L2Character.onHitTimer(L2Character, int, boolean, boolean, boolean, boolean, boolean)
	ON_DIE, // l2n.game.model.actor.L2Character.doDie(L2Character)
	ON_DISCONNECT, // l2n.game.model.actor.L2Player.prepareToLogout()
	ON_ESCAPE, // l2n.game.tables.MapRegionTable.getTeleToLocation(L2Character, TeleportWhereType)
	ON_ENTER_WORLD, // l2n.game.network.clientpackets.EnterWorld.runImpl()
	ON_ENTER_ZONE, // TODO
	ON_EXIT_ZONE, // TODO
	ON_IS_IN_EVENT_CHECK, // l2n.game.model.actor.L2Player.isInEvent(L2EventType...)
	ON_PARTY_LEAVE, // l2n.game.model.actor.L2Player.canLeaveParty()
	ON_PARTY_INVITE, // l2n.game.model.actor.L2Player.canJoinParty(L2Player)
	ON_LEVEL_INCREASE, // l2n.game.model.actor.L2Player.updatePlayerInfo(boolean)
	ON_REVIVE, // l2n.game.model.actor.L2Player.doRevive()
	ON_SPAWN // l2n.game.model.actor.L2Player.onSpawn()
}
