package l2n.extensions.scripts;

import l2n.game.event.L2EventType;
import l2n.game.model.L2Object;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class EventScript extends Functions
{
	private final String _name;

	public EventScript()
	{
		_name = getClass().getSimpleName();
	}

	public final void addEventId(final ScriptEventType eventType)
	{
		Scripts.getInstance().addScriptEvent(eventType, this);
	}

	public final void addEventId(final ScriptEventType... types)
	{
		for(final ScriptEventType e : types)
			Scripts.getInstance().addScriptEvent(e, this);
	}

	public final void removeEventId(final ScriptEventType eventType)
	{
		Scripts.getInstance().removeScriptEvent(eventType, this);
	}

	public final void removeEventId(final ScriptEventType... types)
	{
		for(final ScriptEventType e : types)
			Scripts.getInstance().removeScriptEvent(e, this);
	}

	public final void notifyOnDie(final L2Character self, final L2Character killer)
	{
		onDie(self, killer);
	}

	public final void notifyOnEscape(final L2Player player)
	{
		onEscape(player);
	}

	public final void notifyEnterWorld(final L2Player player)
	{
		onEnterWorld(player);
	}

	public final void notifyLevelIncreased(final L2Player player)
	{
		onLevelIncreased(player);
	}

	public final void notifySpawned(final L2Player player)
	{
		onSpawn(player);
	}

	public final void notifyRevive(final L2Player player)
	{
		onRevive(player);
	}

	public final boolean notifyPartyLeaveTry(final L2Player player)
	{
		return onPartyLeave(player);
	}

	public final SystemMessage notifyPartyInviteTry(final L2Player inviter, final L2Player target)
	{
		return onPartyInvite(inviter, target);
	}

	public final boolean notifyAction(final L2Player player, final L2Object target, final boolean shift)
	{
		return onAction(player, target, shift);
	}

	public final boolean notifyIsInEventCheck(final L2Player player, final L2EventType type)
	{
		return isInEvent(player, type);
	}

	public final void notifyDisconnect(final L2Player player)
	{
		onDisconnect(player);
	}

	public final void notifyOnAttack(final L2Character victim, final L2Character attacker)
	{
		onAttack(victim, attacker);
	}

	public final void notifyEnterZone(final L2Player player, final L2Zone zone)
	{
		onEnterZone(player, zone);
	}

	public final void notifyExitZone(final L2Player player, final L2Zone zone)
	{
		onExitZone(player, zone);
	}

	protected void onDie(final L2Character self, final L2Character killer)
	{}

	protected void onEscape(final L2Player player)
	{}

	protected void onLevelIncreased(final L2Player player)
	{}

	protected void onEnterWorld(final L2Player player)
	{}

	protected void onSpawn(final L2Player player)
	{}

	protected void onRevive(final L2Player player)
	{}

	protected boolean onPartyLeave(final L2Player player)
	{
		return true;
	}

	protected SystemMessage onPartyInvite(final L2Player inviter, final L2Player target)
	{
		return null;
	}

	protected boolean onAction(final L2Player player, final L2Object target, final boolean shift)
	{
		return false;
	}

	protected boolean isInEvent(final L2Player player, final L2EventType type)
	{
		return false;
	}

	protected void onDisconnect(final L2Player player)
	{}

	protected void onAttack(final L2Character victim, final L2Character attacker)
	{}

	@Deprecated
	protected void onEnterZone(final L2Player player, final L2Zone zone)
	{}

	@Deprecated
	protected void onExitZone(final L2Player player, final L2Zone zone)
	{}

	public String getName()
	{
		return _name;
	}
}
