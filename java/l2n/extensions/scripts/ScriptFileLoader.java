package l2n.extensions.scripts;

import l2n.commons.list.GArray;

import java.io.File;

public abstract class ScriptFileLoader
{
	protected void parseClasses(final File f, final GArray<File> list)
	{
		for(final File z : f.listFiles())
			if(z.isDirectory())
			{
				if(!checkFileCondition(z, true))
					continue;
				parseClasses(z, list);
			}
			else
			{
				if(!checkFileCondition(z, false))
					continue;
				list.add(z);
			}
	}

	protected abstract boolean checkFileCondition(final File file, final boolean dir);
}
