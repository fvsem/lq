package l2n.extensions.scripts;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;

public final class Events
{
	public static boolean onAction(final L2Player player, final L2Object obj, final boolean shift)
	{
		if(shift && player.getVarB("noShift"))
			return false;

		final Iterable<EventScript> scripts = Scripts.getInstance().getEventScripts(ScriptEventType.ON_ACTION);
		for(final EventScript script : scripts)
			if(script.notifyAction(player, obj, shift))
				return true;

		return false;
	}
}
