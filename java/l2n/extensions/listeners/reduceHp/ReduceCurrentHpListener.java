package l2n.extensions.listeners.reduceHp;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public abstract class ReduceCurrentHpListener implements MethodInvokeListener
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		Object[] args = e.getArgs();
		onReduceCurrentHp((L2Character) e.getOwner(), (Double) args[0], (L2Character) args[1], (L2Skill) args[2], (Boolean) args[3], (Boolean) args[4], (Boolean) args[5]);
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		return event.getMethodName().equals(MethodCollection.ReduceCurrentHp);
	}

	public abstract void onReduceCurrentHp(L2Character actor, double damage, L2Character attacker, L2Skill skill, boolean awake, boolean standUp, boolean directHp);
}
