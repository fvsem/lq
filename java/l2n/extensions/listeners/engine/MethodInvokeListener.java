package l2n.extensions.listeners.engine;

import l2n.extensions.listeners.events.MethodEvent;

public interface MethodInvokeListener
{
	public void methodInvoked(MethodEvent e);

	public boolean accept(MethodEvent event);
}
