package l2n.extensions.listeners.engine;

import l2n.extensions.listeners.events.PropertyEvent;

public interface PropertyChangeListener
{

	public void propertyChanged(PropertyEvent event);

	public String getPropery();

	public boolean accept(String property);
}
