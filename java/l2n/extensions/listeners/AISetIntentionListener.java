package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.AbstractAI.AbstractAISetIntention;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.ai.AbstractAI;
import l2n.game.ai.CtrlIntention;

public abstract class AISetIntentionListener implements MethodInvokeListener, MethodCollection
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		AbstractAISetIntention event = (AbstractAISetIntention) e;
		AbstractAI ai = event.getOwner();
		CtrlIntention evt = (CtrlIntention) event.getArgs()[0];
		Object arg0 = event.getArgs()[1];
		Object arg1 = event.getArgs()[2];
		SetIntention(ai, evt, arg0, arg1);
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		String method = event.getMethodName();
		return event instanceof AbstractAISetIntention && method.equals(AbstractAIsetIntention);
	}

	public abstract void SetIntention(AbstractAI ai, CtrlIntention intention, Object arg0, Object arg1);
}
