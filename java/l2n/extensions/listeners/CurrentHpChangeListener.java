package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.extensions.listeners.engine.PropertyChangeListener;
import l2n.extensions.listeners.events.PropertyEvent;
import l2n.game.model.actor.L2Character;

public abstract class CurrentHpChangeListener implements PropertyChangeListener, PropertyCollection
{
	@Override
	public final void propertyChanged(PropertyEvent event)
	{
		onCurrentHpChange((L2Character) event.getObject(), ((Double) event.getOldValue()).doubleValue(), ((Double) event.getNewValue()).doubleValue());
	}

	@Override
	public final boolean accept(String property)
	{
		return HitPoints.equals(property);
	}

	@Override
	public final String getPropery()
	{
		return HitPoints;
	}

	public abstract void onCurrentHpChange(L2Character character, double oldValue, double newValue);
}
