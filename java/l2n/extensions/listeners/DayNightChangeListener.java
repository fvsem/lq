package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.extensions.listeners.engine.PropertyChangeListener;
import l2n.extensions.listeners.events.PropertyEvent;
import l2n.game.GameTimeController;

public abstract class DayNightChangeListener implements PropertyChangeListener, PropertyCollection
{

	@Override
	public final void propertyChanged(PropertyEvent event)
	{
		if(((GameTimeController) event.getObject()).isNowNight())
			switchToNight();
		else
			switchToDay();
	}

	@Override
	public final boolean accept(String property)
	{
		return GameTimeControllerDayNightChange.equals(property);
	}

	@Override
	public final String getPropery()
	{
		return GameTimeControllerDayNightChange;
	}

	public abstract void switchToNight();

	public abstract void switchToDay();
}
