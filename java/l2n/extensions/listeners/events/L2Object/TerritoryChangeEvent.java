package l2n.extensions.listeners.events.L2Object;

import l2n.extensions.listeners.collections.PropertyCollection;
import l2n.extensions.listeners.events.PropertyEvent;
import l2n.game.model.L2Object;
import l2n.game.model.L2Territory;

import java.util.Collection;

public class TerritoryChangeEvent implements PropertyEvent
{
	private final Collection<L2Territory> enter;
	private final Collection<L2Territory> exit;
	private final L2Object object;

	public TerritoryChangeEvent(Collection<L2Territory> enter, Collection<L2Territory> exit, L2Object object)
	{
		this.enter = enter;
		this.exit = exit;
		this.object = object;
	}

	@Override
	public L2Object getObject()
	{
		return object;
	}


	@Override
	public Collection<L2Territory> getOldValue()
	{
		return exit;
	}

	@Override
	public Collection<L2Territory> getNewValue()
	{
		return enter;
	}

	@Override
	public String getProperty()
	{
		return PropertyCollection.TerritoryChanged;
	}
}
