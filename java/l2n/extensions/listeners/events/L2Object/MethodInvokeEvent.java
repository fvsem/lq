package l2n.extensions.listeners.events.L2Object;

import l2n.extensions.listeners.events.DefaultMethodInvokeEvent;
import l2n.game.model.L2Object;

public class MethodInvokeEvent extends DefaultMethodInvokeEvent
{
	public MethodInvokeEvent(String methodName, L2Object owner, Object[] args)
	{
		super(methodName, owner, args);
	}

	public L2Object getObject()
	{
		return (L2Object) getOwner();
	}
}
