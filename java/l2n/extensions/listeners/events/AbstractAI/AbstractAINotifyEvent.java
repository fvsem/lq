package l2n.extensions.listeners.events.AbstractAI;

import l2n.extensions.listeners.events.DefaultMethodInvokeEvent;
import l2n.game.ai.AbstractAI;

public class AbstractAINotifyEvent extends DefaultMethodInvokeEvent
{
	public AbstractAINotifyEvent(String methodName, AbstractAI owner, Object[] args)
	{
		super(methodName, owner, args);
	}

	@Override
	public AbstractAI getOwner()
	{
		return (AbstractAI) super.getOwner();
	}

	@Override
	public Object[] getArgs()
	{
		return super.getArgs();
	}
}
