package l2n.extensions.listeners.events.AbstractAI;

import l2n.extensions.listeners.events.DefaultMethodInvokeEvent;
import l2n.game.ai.AbstractAI;

public class AbstractAISetIntention extends DefaultMethodInvokeEvent
{
	public AbstractAISetIntention(String methodName, AbstractAI owner, Object[] args)
	{
		super(methodName, owner, args);
	}

	@Override
	public AbstractAI getOwner()
	{
		return (AbstractAI) super.getOwner();
	}

	@Override
	public Object[] getArgs()
	{
		return super.getArgs();
	}
}
