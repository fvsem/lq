package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.actor.L2Character;

public abstract class OnAttackedListener implements MethodInvokeListener, MethodCollection
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		OnAttacked((L2Character) e.getArgs()[0], (L2Character) e.getArgs()[1], (Integer) e.getArgs()[2], (Boolean) e.getArgs()[3], (Boolean) e.getArgs()[4], (Boolean) e.getArgs()[5], (Boolean) e.getArgs()[6], (Boolean) e.getArgs()[7]);
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		return event.getMethodName().equals(onAttacked);
	}

	public abstract void OnAttacked(L2Character actor, L2Character target, int damage, boolean crit, boolean miss, boolean soulshot, boolean shld, boolean unchargeSS);
}
