package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.L2Zone.L2ZoneEnterLeaveEvent;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.L2Zone;
import l2n.game.model.actor.L2Character;

public abstract class L2ZoneEnterLeaveListener implements MethodInvokeListener, MethodCollection
{

	@Override
	public final void methodInvoked(final MethodEvent e)
	{
		final L2ZoneEnterLeaveEvent event = (L2ZoneEnterLeaveEvent) e;
		final L2Zone owner = event.getOwner();
		final L2Character actor = event.getArgs()[0];
		if(e.getMethodName().equals(L2ZoneObjectEnter))
			objectEntered(owner, actor);
		else
			objectLeaved(owner, actor);
	}

	@Override
	public final boolean accept(final MethodEvent event)
	{
		final String method = event.getMethodName();
		return event instanceof L2ZoneEnterLeaveEvent && (method.equals(L2ZoneObjectEnter) || method.equals(L2ZoneObjectLeave));
	}

	public abstract void objectEntered(L2Zone zone, L2Character character);

	public abstract void objectLeaved(L2Zone zone, L2Character character);
}
