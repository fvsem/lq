package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;

public abstract class StartAltCastListener implements MethodInvokeListener, MethodCollection
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		Object[] args = e.getArgs();
		onAltCastStart((L2Skill) args[0], (L2Character) args[1]);
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		return event.getMethodName().equals(onStartAltCast);
	}

	public abstract void onAltCastStart(L2Skill skill, L2Character target);
}
