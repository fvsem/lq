package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.model.actor.L2Character;

public abstract class DoDieListener implements MethodInvokeListener
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		onDie((L2Character) e.getOwner());
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		return event.getMethodName().equals(MethodCollection.onDecay);
	}

	public abstract void onDie(L2Character cha);
}
