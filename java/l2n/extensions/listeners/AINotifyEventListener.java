package l2n.extensions.listeners;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.AbstractAI.AbstractAINotifyEvent;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.game.ai.AbstractAI;
import l2n.game.ai.CtrlEvent;

public abstract class AINotifyEventListener implements MethodInvokeListener, MethodCollection
{
	@Override
	public final void methodInvoked(MethodEvent e)
	{
		AbstractAINotifyEvent event = (AbstractAINotifyEvent) e;
		AbstractAI ai = event.getOwner();
		CtrlEvent evt = (CtrlEvent) event.getArgs()[0];
		Object arg0 = event.getArgs()[1];
		Object arg1 = event.getArgs()[2];
		NotifyEvent(ai, evt, arg0, arg1);
	}

	@Override
	public final boolean accept(MethodEvent event)
	{
		String method = event.getMethodName();
		return event instanceof AbstractAINotifyEvent && method.equals(AbstractAInotifyEvent);
	}

	public abstract void NotifyEvent(AbstractAI ai, CtrlEvent evt, Object arg0, Object arg1);
}
