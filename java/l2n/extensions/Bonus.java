package l2n.extensions;

import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.game.model.actor.L2Player;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Bonus
{
	private L2Player _owner = null;

	public double RATE_XP = 1;
	public double RATE_SP = 1;
	public double RATE_QUESTS_REWARD = 1;
	public double RATE_QUESTS_DROP = 1;
	public double RATE_DROP_ADENA = 1;
	public double RATE_DROP_ITEMS = 1;
	public double RATE_DROP_SPOIL = 1;

	public Bonus(final L2Player player)
	{
		if(player == null || player.getNetConnection() == null)
			return;

		_owner = player;
		restore();

		if(player.getNetConnection() == null)
			return;

		final double bonus = player.getNetConnection().getBonus();
		if(player.hasPremiumAccount())
			player.startBonusTask(player.getNetConnection().getBonusExpire() * 1000);

		RATE_XP *= bonus;
		RATE_SP *= bonus;
		RATE_DROP_ADENA *= bonus;
		RATE_DROP_ITEMS *= bonus;
		RATE_DROP_SPOIL *= bonus;
		RATE_QUESTS_REWARD *= bonus;
		RATE_QUESTS_DROP *= bonus;
	}

	private void restore()
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery("SELECT bonus_name, bonus_value FROM bonus WHERE login='" + _owner.getAccountName() + "' AND (bonus_expire_time='-1' OR bonus_expire_time > " + System.currentTimeMillis() / 1000 + ")");
			while (rset.next())
			{
				final String bonus_name = rset.getString("bonus_name");
				final double bonus_value = rset.getDouble("bonus_value");
				final Class<?> cls = getClass();
				try
				{
					final Field fld = cls.getField(bonus_name);
					try
					{
						fld.setDouble(this, bonus_value);
					}
					catch(final IllegalArgumentException e)
					{
						e.printStackTrace();
					}
					catch(final IllegalAccessException e)
					{
						e.printStackTrace();
					}
				}
				catch(final SecurityException e)
				{
					e.printStackTrace();
				}
				catch(final NoSuchFieldException e)
				{
					e.printStackTrace();
				}
			}
		}
		catch(final SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}
}
