package l2n.extensions.multilang;

import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2Item;
import l2n.util.Log;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

public class CustomMessage
{
	private static final String localizationDirSrc = "data/localization/";
	private static final String localizationDirASCII = "data/localization/ascii/";

	private static URLClassLoader loader;

	static
	{
		reload();
	}

	public synchronized static void reload()
	{
		File src = new File(localizationDirSrc);
		for(File prop : src.listFiles(new PropertiesFilter()))
			ASCIIBuilder.createPropASCII(prop);

		try
		{
			loader = new URLClassLoader(new URL[] { new File(localizationDirASCII).toURI().toURL() });
			en = ResourceBundle.getBundle("messages", new Locale("en"), loader);
			ru = ResourceBundle.getBundle("messages", new Locale("ru"), loader);
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace(System.err);
		}
	}

	private static ResourceBundle en;
	private static ResourceBundle ru;

	private String _text;
	private int mark = 0;

	public CustomMessage(String address, L2Object player, Object... args)
	{
		if(player != null && player.isPlayer())
			getString(address, ((L2Player) player).getLang());
		add(args);
	}

	public CustomMessage(String address, String language, Object... args)
	{
		getString(address, language);
		add(args);
	}

	private void getString(String address, String lang)
	{
		if(lang != null)
			lang = lang.toLowerCase();
		else
			lang = "en";

		ResourceBundle rb;
		if(lang.equals("ru"))
			rb = ru;
		else
			rb = en;

		try
		{
			_text = rb.getString(address);
		}
		catch(Exception e)
		{
			Log.addDev("Custom message with address: \"" + address + "\" is unsupported!", "dev_custom_message");
			_text = "Custom message with address: \"" + address + "\" is unsupported!";
		}

		if(_text == null)
		{
			Log.addDev("Custom message with address: \"" + address + "\" not found!", "dev_custom_message");
			_text = "Custom message with address: \"" + address + "\" not found!";
		}
	}

	public CustomMessage add(Object... args)
	{
		for(Object arg : args)
			if(arg instanceof String)
				addString((String) arg);
			else if(arg instanceof Integer)
				addNumber(((Integer) arg).intValue());
			else if(arg instanceof Long)
				addNumber(((Long) arg).longValue());
			else if(arg instanceof L2Item)
				addItemName((L2Item) arg);
			else if(arg instanceof L2ItemInstance)
				addItemName((L2ItemInstance) arg);
			else if(arg instanceof L2Character)
				addCharName((L2Character) arg);
			else if(arg instanceof L2Skill)
				addSkillName((L2Skill) arg);
			else
			{
				System.out.println("unknown CustomMessage arg type: " + arg);
				Thread.dumpStack();
			}
		return this;
	}

	public CustomMessage addNumber(long number)
	{
		_text = _text.replace("{" + mark + "}", String.valueOf(number));
		mark++;
		return this;
	}

	public CustomMessage addString(String str)
	{
		_text = _text.replace("{" + mark + "}", str);
		mark++;
		return this;
	}

	public CustomMessage addSkillName(L2Skill skill)
	{
		_text = _text.replace("{" + mark + "}", skill.getName());
		mark++;
		return this;
	}

	public CustomMessage addSkillName(short skillId, short skillLevel)
	{
		return addSkillName(SkillTable.getInstance().getInfo(skillId, skillLevel));
	}

	public CustomMessage addItemName(L2Item item)
	{
		_text = _text.replace("{" + mark + "}", item.getName());
		mark++;
		return this;
	}

	public CustomMessage addItemName(int itemId)
	{
		return addItemName(ItemTable.getInstance().getTemplate(itemId));
	}

	public CustomMessage addItemName(L2ItemInstance item)
	{
		return addItemName(item.getItem());
	}

	public CustomMessage addCharName(L2Character cha)
	{
		_text = _text.replace("{" + mark + "}", cha.getName());
		mark++;
		return this;
	}

	@Override
	public String toString()
	{
		return _text;
	}
}
