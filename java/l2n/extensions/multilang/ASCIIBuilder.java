package l2n.extensions.multilang;

import javolution.text.TextBuilder;

import java.io.*;
import java.util.HashMap;

public class ASCIIBuilder
{
	private static final String targetDir = "data/localization/ascii";
	private static final String encoding = "latin1";

	public static void createPropASCII(File f)
	{
		HashMap<String, String> map = new HashMap<String, String>();

		try
		{
			LineNumberReader lnr = new LineNumberReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
			TextBuilder tb = new TextBuilder();

			String s;
			String buf;
			int index;
			while ((s = lnr.readLine()) != null)
			{
				try
				{
					tb.append(s);
					if(!tb.toString().endsWith("\\"))
					{
						buf = tb.toString().replace("\\", "");
						index = buf.indexOf("=");
						map.put(buf.substring(0, index), buf.substring(index + 1));
						tb.clear();
					}
				}
				catch(Exception e)
				{
					tb.clear();
				}
			}
			lnr.close();
		}
		catch(Exception e)
		{
			e.printStackTrace(System.err);
		}

		for(String s : map.keySet())
			map.put(s, convertString(map.get(s)));

		File dir = new File(targetDir);
		if(dir.exists())
			dir.delete();
		dir.mkdir();

		File target = new File(targetDir + "/" + f.getName());

		if(!target.exists())
			try
			{
				target.createNewFile();
			}
			catch(IOException e)
			{
				e.printStackTrace(System.err);
			}

		try
		{
	
			FileOutputStream fos = new FileOutputStream(target);
			for(String q : map.keySet())
			{
				fos.write(q.getBytes(encoding));
				fos.write("=".getBytes(encoding));
				fos.write(map.get(q).getBytes(encoding));
				fos.write("\n".getBytes(encoding));
			}
			fos.close();
		}
		catch(IOException e)
		{
			e.printStackTrace(System.err);
		}
	}

	private static String convertString(String s)
	{
		TextBuilder tb = new TextBuilder();

		for(char c : s.toCharArray())
			if(c > 127) 
			{
				tb.append("\\u");
				String hex = Integer.toHexString(c);
				int lenght = hex.length();
				for(int i = lenght; i < 4; i++)
					tb.append("0");
				tb.append(hex);
			}
			else
				tb.append(c);

		return tb.toString();
	}
}
