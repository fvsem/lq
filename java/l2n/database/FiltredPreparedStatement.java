package l2n.database;

import java.sql.*;

public class FiltredPreparedStatement implements FiltredStatementInterface
{
	private transient final PreparedStatement myStatement;

	public FiltredPreparedStatement(final PreparedStatement statement)
	{
		myStatement = statement;
	}

	public ResultSet executeQuery() throws SQLException
	{
		return myStatement.executeQuery();
	}

	@Override
	public void close()
	{
		try
		{
			myStatement.close();
		}
		catch(final SQLException e)
		{
			e.printStackTrace();
		}
	}

	public boolean execute() throws SQLException
	{
		return myStatement.execute();
	}

	public ResultSet executeQuery(final String sql) throws SQLException
	{
		return myStatement.executeQuery(sql);
	}

	public void setInt(final int index, final int val) throws SQLException
	{
		myStatement.setInt(index, val);
	}

	public void setString(final int index, final String val) throws SQLException
	{
		myStatement.setString(index, val);
	}

	public void setLong(final int index, final long val) throws SQLException
	{
		myStatement.setLong(index, val);
	}

	public void setNull(final int index, final int val) throws SQLException
	{
		myStatement.setNull(index, val);
	}

	public void setDouble(final int index, final double val) throws SQLException
	{
		myStatement.setDouble(index, val);
	}

	public void setBytes(final int index, final byte[] data) throws SQLException
	{
		myStatement.setBytes(index, data);
	}

	public int executeUpdate() throws SQLException
	{
		return myStatement.executeUpdate();
	}

	public void setBoolean(final int index, final boolean val) throws SQLException
	{
		myStatement.setBoolean(index, val);
	}

	public void setEscapeProcessing(final boolean val) throws SQLException
	{
		myStatement.setEscapeProcessing(val);
	}

	public void setByte(final int index, final byte val) throws SQLException
	{
		myStatement.setByte(index, val);
	}

	public void setDate(final int index, final Date value) throws SQLException
	{
		myStatement.setDate(index, value);
	}

	public void setTimestamp(final int index, final Timestamp timestamp) throws SQLException
	{
		myStatement.setTimestamp(index, timestamp);
	}

	public ResultSet getGeneratedKeys() throws SQLException
	{
		return myStatement.getGeneratedKeys();
	}

	public void setVars(final Object... vars) throws SQLException
	{
		Number n;
		long long_val;
		double double_val;
		for(int i = 0; i < vars.length; i++)
			if(vars[i] instanceof Number)
			{
				n = (Number) vars[i];
				long_val = n.longValue();
				double_val = n.doubleValue();
				if(long_val == double_val)
					setLong(i + 1, long_val);
				else
					setDouble(i + 1, double_val);
			}
			else if(vars[i] instanceof String)
				setString(i + 1, (String) vars[i]);
	}
}
