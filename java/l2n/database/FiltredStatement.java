package l2n.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class FiltredStatement implements FiltredStatementInterface
{
	private final Statement myStatement;

	public FiltredStatement(final Statement statement)
	{
		myStatement = statement;
	}

	public int executeUpdate(final String sql) throws SQLException
	{
		return myStatement.executeUpdate(sql);
	}

	@Override
	public void close()
	{
		try
		{
			myStatement.close();
		}
		catch(final SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void addBatch(final String sql) throws SQLException
	{
		myStatement.addBatch(sql);
	}

	public int[] executeBatch() throws SQLException
	{
		return myStatement.executeBatch();
	}

	public ResultSet executeQuery(final String sql) throws SQLException
	{
		return myStatement.executeQuery(sql);
	}
}
