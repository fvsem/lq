package l2n.database.connectingpool;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectingPool
{
	public Connection getConnection() throws SQLException;

	public void shutdown();

	public String getStats();
}
