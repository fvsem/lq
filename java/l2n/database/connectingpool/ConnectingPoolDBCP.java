package l2n.database.connectingpool;

import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectingPoolDBCP implements IConnectingPool {
    private static final Logger _log = Logger.getLogger(ConnectingPoolDBCP.class.getName());

    private final BasicDataSource _source;

    public ConnectingPoolDBCP(String poolName, String driver, String url, String login, String password, int poolSize, int idleTimeOut, int idleTestPeriod) throws SQLException {
        _source = new BasicDataSource();
        try {
            Class.forName(driver).newInstance();

            _source.setDriverClassName(driver);
            _source.setUrl(url);
            _source.setUsername(login);
            _source.setPassword(password);

            _source.setInitialSize(1);
            _source.setMaxActive(poolSize);
            _source.setMaxIdle(poolSize);
            _source.setMinIdle(1);
            _source.setMaxWait(-1L);

            _source.setDefaultReadOnly(false);
            _source.setDefaultAutoCommit(true);
            _source.setValidationQuery("SELECT 1");
            _source.setTestOnBorrow(false);
            _source.setTestWhileIdle(false);
            _source.setPoolPreparedStatements(true);

            _source.setRemoveAbandoned(true);
            _source.setRemoveAbandonedTimeout(60);

            _source.setTimeBetweenEvictionRunsMillis(idleTestPeriod * 1000L);
            _source.setNumTestsPerEvictionRun(poolSize);
            _source.setMinEvictableIdleTimeMillis(idleTimeOut * 1000L);

            _source.getConnection().close();
        } catch (Exception e) {
            throw new SQLException("ConnectingPoolDBCP initialization failed: " + e.getMessage() + "!");
        } finally {
            _log.info("ConnectingPoolDBCP: initialized " + poolName + " connection pool.");
        }
    }

    @Override
    public void shutdown() {
        try {
            _source.close();
        } catch (SQLException e) {
            _log.log(Level.INFO, "", e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return _source.getConnection();
    }

    @Override
    public String getStats() {
        String line = "======== Msql DBCP Stats ==========\n";
        line += "InitialSize............ " + getInitialSize() + "\n";
        line += "BusyConnectionCount.... " + getBusyConnectionCount() + "\n";
        line += "IdleConnectionCount....." + getIdleConnectionCount() + "\n";
        line += "NumActive.............. " + getNumActive() + "\n";
        line += "MaxActive.............. " + getMaxActive() + "\n";
        line += "======== Msql DBCP Stats ==========\n";
        return line;
    }

    private int getBusyConnectionCount() {
        return _source.getNumActive();
    }

    private int getInitialSize() {
        return _source.getInitialSize();
    }

    private int getMaxActive() {
        return _source.getMaxActive();
    }

    private int getIdleConnectionCount() {
        return _source.getNumIdle();
    }

    private int getNumActive() {
        return _source.getNumActive();
    }
}
