package l2n.database;

import l2n.Config;
import l2n.Server;
import l2n.database.connectingpool.ConnectingPoolDBCP;
import l2n.database.connectingpool.IConnectingPool;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class L2DatabaseFactory
{
	private static L2DatabaseFactory _instance;
	private static L2DatabaseFactory _instanceLogin;

	private final static Logger _log = Logger.getLogger(L2DatabaseFactory.class.getName());

	private final ConcurrentHashMap<String, ThreadConnection> _connections = new ConcurrentHashMap<String, ThreadConnection>();

	private IConnectingPool _connection_pool;

	public enum ConnectingPoolType
	{
		DBCP;
	}

	public L2DatabaseFactory(final String poolName, final String driver, final String url, final String login, final String pass, final int poolSize, final int idleTimeOut, final int idleTestPeriod) throws SQLException
	{
		switch (Config.DATABASE_TYPE_CONNECTING_POOL)
		{
			case DBCP:
				_connection_pool = new ConnectingPoolDBCP(poolName, driver, url, login, pass, poolSize, idleTimeOut, idleTestPeriod);
				break;
			default:
			{
				Server.exit(0, "L2DatabaseFactory: wrong type connecting pool!");
			}
		}
	}

	public static L2DatabaseFactory getInstance() throws SQLException
	{
		if(_instance == null)
		{
			_instance = new L2DatabaseFactory("GameServer", Config.DATABASE_DRIVER, Config.DATABASE_URL, Config.DATABASE_LOGIN, Config.DATABASE_PASSWORD, Config.DATABASE_MAX_CONNECTIONS, Config.DATABASE_MAX_IDLE_TIMEOUT, Config.DATABASE_IDLE_TEST_PERIOD);
			if(Config.DATABASE_URL.equalsIgnoreCase(Config.ACCOUNTS_DATABASE_URL))
				_instanceLogin = _instance;
		}
		return _instance;
	}

	public static L2DatabaseFactory getInstanceLogin() throws SQLException
	{
		if(_instanceLogin == null)
		{
			if(Config.DATABASE_URL.equalsIgnoreCase(Config.ACCOUNTS_DATABASE_URL))
				return getInstance();
			_instanceLogin = new L2DatabaseFactory("LoginServer", Config.DATABASE_DRIVER, Config.ACCOUNTS_DATABASE_URL, Config.ACCOUNTS_DATABASE_LOGIN, Config.ACCOUNTS_DATABASE_PASSWORD, 2, Config.DATABASE_MAX_IDLE_TIMEOUT, Config.DATABASE_IDLE_TEST_PERIOD);
		}
		return _instanceLogin;
	}

	public ThreadConnection getConnection() throws SQLException
	{
		ThreadConnection connection;
		if(Config.USE_DATABASE_LAYER)
		{
			final String key = generateKey();

			connection = _connections.get(key);
			if(connection == null)
				try
				{
					connection = new ThreadConnection(_connection_pool.getConnection(), this);
				}
				catch(final SQLException e)
				{
					_log.warning("Couldn't create connection. Cause: " + e.getMessage());
				}
			else
				connection.updateCounter();

			if(connection != null)
				synchronized (_connections)
				{
					_connections.put(key, connection);
				}
		}
		else
			connection = new ThreadConnection(_connection_pool.getConnection(), this);
		return connection;
	}

	public ConcurrentHashMap<String, ThreadConnection> getConnections()
	{
		return _connections;
	}

	public void shutdown()
	{
		_connections.clear();
		try
		{
			_connection_pool.shutdown();
			_connection_pool = null;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "", e);
		}
	}

	public String generateKey()
	{
		return String.valueOf(Thread.currentThread().hashCode());
	}

	public String getStats()
	{
		return _connection_pool.getStats();
	}
}
