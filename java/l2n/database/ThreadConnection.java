package l2n.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

public class ThreadConnection
{
	private final static Logger _log = Logger.getLogger(ThreadConnection.class.getName());

	private transient final Connection myConnection;
	private final L2DatabaseFactory myFactory;
	private int counter = 1;

	public ThreadConnection(final Connection con, final L2DatabaseFactory f)
	{
		myConnection = con;
		myFactory = f;
	}

	public void updateCounter()
	{
		counter++;
	}

	public FiltredPreparedStatement prepareStatement(final String sql) throws SQLException
	{
		return new FiltredPreparedStatement(myConnection.prepareStatement(sql));
	}

	public FiltredPreparedStatement prepareStatement(final String sql, final int Statement) throws SQLException
	{
		return new FiltredPreparedStatement(myConnection.prepareStatement(sql, Statement));
	}

	public void close()
	{
		counter--;
		if(counter == 0)
			try
			{
				synchronized (myFactory.getConnections())
				{
					myConnection.close();
					final String key = myFactory.generateKey();
					myFactory.getConnections().remove(key);
				}
			}
			catch(final Exception e)
			{
				_log.warning("Couldn't close connection. Cause: " + e.getMessage());
			}
	}

	public FiltredStatement createStatement() throws SQLException
	{
		return new FiltredStatement(myConnection.createStatement());
	}
}
