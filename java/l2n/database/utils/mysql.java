package l2n.database.utils;

import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public abstract class mysql
{
	private static Logger _log = Logger.getLogger(mysql.class.getName());

	public static boolean set(String query)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate(query);
		}
		catch(Exception e)
		{
			_log.warning("Could not execute update '" + query + "': " + e);
			Thread.dumpStack();
			return false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		return true;
	}

	public static boolean setNoE(String query)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			statement.executeUpdate(query);
		}
		catch(Exception e)
		{
			return false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
		return true;
	}

	public static Object get(String query)
	{
		Object ret = null;
		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rset = statement.executeQuery(query + " LIMIT 1");
			ResultSetMetaData md = rset.getMetaData();

			if(rset.next())
				if(md.getColumnCount() > 1)
				{
					ConcurrentHashMap<String, Object> tmp = new ConcurrentHashMap<String, Object>();
					for(int i = 0; i < md.getColumnCount(); i++)
						tmp.put(md.getColumnName(i + 1), rset.getObject(i + 1));
					ret = tmp;
				}
				else
					ret = rset.getObject(1);

		}
		catch(Exception e)
		{
			_log.warning("Could not execute query '" + query + "': " + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return ret;
	}

	public static GArray<HashMap<String, Object>> getAll(String query)
	{
		GArray<HashMap<String, Object>> ret = new GArray<HashMap<String, Object>>();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(query);
			rset = statement.executeQuery();
			ResultSetMetaData md = rset.getMetaData();

			while (rset.next())
			{
				HashMap<String, Object> tmp = new HashMap<String, Object>();
				for(int i = md.getColumnCount(); i > 0; i--)
					tmp.put(md.getColumnName(i), rset.getObject(i));
				ret.add(tmp);
			}
		}
		catch(Exception e)
		{
			_log.warning("Could not execute query '" + query + "': " + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return ret;
	}

	public static ArrayList<Object> get_array(String query)
	{
		ArrayList<Object> ret = new ArrayList<Object>();
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(query);
			rset = statement.executeQuery();
			ResultSetMetaData md = rset.getMetaData();

			while (rset.next())
				if(md.getColumnCount() > 1)
				{
					ConcurrentHashMap<String, Object> tmp = new ConcurrentHashMap<String, Object>();
					for(int i = 0; i < md.getColumnCount(); i++)
						tmp.put(md.getColumnName(i + 1), rset.getObject(i + 1));
					ret.add(tmp);
				}
				else
					ret.add(rset.getObject(1));
		}
		catch(Exception e)
		{
			_log.warning("Could not execute query '" + query + "': " + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return ret;
	}

	public static int simple_get_int(String ret_field, String table, String where)
	{
		String query = "SELECT " + ret_field + " FROM `" + table + "` WHERE " + where + " LIMIT 1;";

		int res = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(query);
			rset = statement.executeQuery();

			if(rset.next())
				res = rset.getInt(1);
		}
		catch(Exception e)
		{
			_log.warning("mSGI: Error in query '" + query + "':" + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return res;
	}

	public static int[][] simple_get_int_array(String[] ret_fields, String table, String where)
	{
		long start = System.currentTimeMillis();

		String fields = null;
		for(String field : ret_fields)
			if(fields != null)
			{
				fields += ",";
				fields += "`" + field + "`";
			}
			else
				fields = "`" + field + "`";

		String query = "SELECT " + fields + " FROM `" + table + "` WHERE " + where;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;

		int[][] res = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(query);
			rset = statement.executeQuery();

			GArray<int[]> al = new GArray<int[]>();
			int row = 0;
			while (rset.next())
			{
				int[] tmp = new int[ret_fields.length];
				for(int i = 0; i < ret_fields.length; i++)
				{
					Object obj = rset.getObject(i + 1);
					int element = 0;
					if(obj instanceof Integer)
						element = (Integer) obj;
					else if(obj instanceof String)
						element = Integer.parseInt(obj.toString());

					tmp[i] = element;
				}
				al.add(row, tmp);
				row++;
			}

			res = al.toArray(new int[row][ret_fields.length]);
		}
		catch(Exception e)
		{
			_log.warning("mSGIA: Error in query '" + query + "':" + e);
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		_log.fine("Get all rows in query '" + query + "' in " + (System.currentTimeMillis() - start) + "ms");
		return res;
	}

	public static boolean setEx(L2DatabaseFactory db, String query)
	{
		ThreadConnection con = null;
		FiltredStatement statement = null;
		try
		{
			if(db == null)
				db = L2DatabaseFactory.getInstance();
			con = db.getConnection();
			statement = con.createStatement();
			statement.executeUpdate(query);
		}
		catch(Exception e)
		{
			_log.warning("Could not execute update '" + query + "': " + e);
			e.printStackTrace();
			return false;
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}

		return true;
	}
}
