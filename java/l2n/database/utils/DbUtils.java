package l2n.database.utils;

import l2n.Config;
import l2n.commons.text.Strings;
import l2n.database.FiltredStatement;
import l2n.database.FiltredStatementInterface;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbUtils
{
	private static final Logger _log = Logger.getLogger(DbUtils.class.getName());

	public static String mysql_server_version = null;

	public static String getMySqlServerVersion()
	{
		if(mysql_server_version != null)
			return mysql_server_version;

		ThreadConnection con = null;
		FiltredStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			rs = statement.executeQuery("SELECT VERSION() AS mysql_version");

			rs.next();
			mysql_server_version = rs.getString("mysql_version");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "DbUtils: getMySqlServerVersion error: ", e);
			mysql_server_version = "UNKNOW";
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

		return mysql_server_version;
	}

	public static void optimizeTables()
	{
		if(!Config.ALLOW_OPTIMIZE_TABLES)
			return;

		ThreadConnection con = null;
		FiltredStatement st = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.createStatement();;

			final ArrayList<String> tablesList = new ArrayList<String>();

			rs = st.executeQuery("SHOW FULL TABLES");
			while (rs.next())
			{
				final String tableType = rs.getString(2/* "Table_type" */);
				if(tableType.equals("VIEW"))
					continue;

				tablesList.add(rs.getString(1));
			}
			rs.close();

			final String all_tables = Strings.joinStrings(",", tablesList.toArray(new String[tablesList.size()]));

			rs = st.executeQuery("CHECK TABLE " + all_tables);
			while (rs.next())
			{
				final String table = rs.getString("Table");
				final String msgType = rs.getString("Msg_type");
				final String msgText = rs.getString("Msg_text");

				if(msgType.equals("status"))
					if(msgText.equals("OK"))
						continue;

				_log.log(Level.WARNING, "DbUtils: CHECK TABLE " + table + ": " + msgType + " -> " + msgText);
			}
			rs.close();
			_log.info("DbUtils: Database tables have been checked.");

			rs = st.executeQuery("ANALYZE TABLE " + all_tables);
			while (rs.next())
			{
				final String table = rs.getString("Table");
				final String msgType = rs.getString("Msg_type");
				final String msgText = rs.getString("Msg_text");

				if(msgType.equals("status"))
					if(msgText.equals("OK") || msgText.equals("Table is already up to date"))
						continue;

				if(msgType.equals("note"))
					if(msgText.equals("The storage engine for the table doesn't support analyze"))
						continue;

				_log.log(Level.WARNING, "DbUtils: ANALYZE TABLE " + table + ": " + msgType + " -> " + msgText);
			}
			rs.close();
			_log.info("DbUtils: Database tables have been analyzed.");

			rs = st.executeQuery("OPTIMIZE TABLE " + all_tables);
			while (rs.next())
			{
				final String table = rs.getString("Table");
				final String msgType = rs.getString("Msg_type");
				final String msgText = rs.getString("Msg_text");

				if(msgType.equals("status"))
					if(msgText.equals("OK") || msgText.equals("Table is already up to date"))
						continue;

				if(msgType.equals("note"))
					if(msgText.equals("Table does not support optimize, doing recreate + analyze instead"))
						continue;

				_log.log(Level.WARNING, "DbUtils: OPTIMIZE TABLE " + table + ": " + msgType + " -> " + msgText);
			}
			_log.info("DbUtils: Database tables have been optimized.");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "DbUtils: Cannot optimize database tables!", e);
		}
		finally
		{
			closeQuietly(con, st, rs);
		}
	}
	
	public static void closeDatabaseCSR(ThreadConnection conn, FiltredStatementInterface stmt, ResultSet rs)
	{
		closeResultSet(rs);
		closeStatement(stmt);
		closeConnection(conn);
	}
	
	public static void closeResultSet(ResultSet rs)
	{
		if(rs != null)
			try
			{
				rs.close();
			}
			catch(SQLException e)
			{}
	}
	
	public static void closeStatement(FiltredStatementInterface stmt)
	{
		if(stmt != null)
			stmt.close();
	}
	
	public static void closeConnection(ThreadConnection conn)
	{
		if(conn != null)
			conn.close();
	}

	public static void repairTables()
	{
		if(!Config.ALLOW_REPAIR_TABLES)
			return;

		ThreadConnection con = null;
		FiltredStatement st = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.createStatement();;

			final ArrayList<String> tablesList = new ArrayList<String>();

			rs = st.executeQuery("SHOW FULL TABLES");
			while (rs.next())
			{
				final String tableType = rs.getString(2/* "Table_type" */);
				if(tableType.equals("VIEW"))
					continue;

				tablesList.add(rs.getString(1));
			}
			rs.close();

			final String all_tables = Strings.joinStrings(",", tablesList.toArray(new String[tablesList.size()]));

			rs = st.executeQuery("REPAIR TABLE " + all_tables + " EXTENDED");
			while (rs.next())
			{
				final String table = rs.getString("Table");
				final String msgType = rs.getString("Msg_type");
				final String msgText = rs.getString("Msg_text");

				if(msgType.equals("status"))
					if(msgText.equals("OK"))
						continue;

				if(msgType.equals("note"))
					if(msgText.equals("The storage engine for the table doesn't support repair"))
						continue;

				_log.log(Level.WARNING, "DbUtils: REPAIR TABLE " + table + ": " + msgType + " -> " + msgText);
			}
			_log.info("DbUtils: Database tables have been repaired.");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "DbUtils: Cannot optimize database tables!", e);
		}
		finally
		{
			closeQuietly(con, st, rs);
		}
	}

	public static void close(final ThreadConnection conn)
	{
		if(conn == null)
			return;
		conn.close();
	}


	public static void close(final ResultSet rs)
	{
		if(rs == null)
			return;
		try
		{
			rs.close();
		}
		catch(final SQLException e)
		{
			_log.log(Level.WARNING, "Failed to close ResultSet!", e);
		}
	}

	public static void close(final FiltredStatementInterface stmt)
	{
		if(stmt == null)
			return;
		stmt.close();
	}

	public static void close(final FiltredStatementInterface stmt, final ResultSet rs)
	{
		close(rs);
		close(stmt);
	}

	public static void closeQuietly(final ThreadConnection conn)
	{
		close(conn);
	}

	public static void closeQuietly(final ResultSet rs)
	{
		close(rs);
	}

	public static void closeQuietly(final FiltredStatementInterface stmt)
	{
		close(stmt);
	}

	public static void closeQuietly(final ThreadConnection conn, final FiltredStatementInterface stmt)
	{
		closeQuietly(stmt);
		closeQuietly(conn);
	}

	public static void closeQuietly(final FiltredStatementInterface stmt, final ResultSet rs)
	{
		closeQuietly(rs);
		closeQuietly(stmt);
	}

	public static void closeQuietly(final ThreadConnection conn, final FiltredStatementInterface stmt, final ResultSet rs)
	{
		closeQuietly(rs);
		closeQuietly(stmt);
		closeQuietly(conn);
	}
}
