package l2n.util;

import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log
{
	public static final int General = 0;
	public static final int Shout = 1;
	public static final int Whisper = 2;
	public static final int Party = 3;
	public static final int Clan = 4;
	public static final int Petition = 6;
	public static final int GM = 7;
	public static final int Trade = 8;
	public static final int Alliance = 9;
	public static final int CreatePet = 103;
	public static final int DeletePet = 104;
	public static final int WithDrawPet = 105;
	public static final int DepositPet = 106;
	public static final int ChangePetName = 107;
	public static final int DismissPet = 108;
	public static final int PetGetItem = 109;
	public static final int PetDropItem = 110;
	public static final int GiveItemToPet = 111;
	public static final int GetItemFromPet = 112;
	public static final int PetUseItem = 113;
	public static final int PetDie = 114;
	public static final int CreatePledge = 201;
	public static final int JoinPledge = 202;
	public static final int DismissPledge = 203;
	public static final int WithdrawPledge = 204;
	public static final int OustPledge = 205;
	public static final int NicknamePledge = 206;
	public static final int BeginPledgeWar = 207;
	public static final int StopWar = 208;
	public static final int SurrenderWar = 209;
	public static final int WinWar = 210;
	public static final int CrestPledge = 211;
	public static final int FinishWar = 212;
	public static final int UpdateCastleOwner = 213;
	public static final int WriteTax = 214;
	public static final int SetPledgeInfo = 215;
	public static final int DeletePledgeByTimer = 216;
	public static final int SetUserPledgeInfo = 217;
	public static final int ChallengeRejected = 218;
	public static final int InstallBattleCamp = 219;
	public static final int UninstallAllBattleCamp = 220;
	public static final int UninstallBattleCamp = 221;
	public static final int SetNextSiegeTime = 222;
	public static final int RegisterAsAttacter = 223;
	public static final int RegisterAsDefender = 224;
	public static final int UnregisterCastleWar = 225;
	public static final int ConfirmCastleDefence = 226;
	public static final int ResetCastleSiegePledge = 227;
	public static final int TryPossessHolyThing = 228;
	public static final int DoorHpChanged = 229;
	public static final int SetPledgeContribution = 230;
	public static final int SetWinnerPledgeContribution = 231;
	public static final int UpdateAgitOwner = 232;
	public static final int SetDoorOpenClose = 233;
	public static final int SaveCastleIncome = 234;
	public static final int InstallAgitDeco = 235;
	public static final int TryDismissPledge = 236;
	public static final int SetNextCastleSiege = 237;
	public static final int CreateAgitAuction = 238;
	public static final int CreateAgitBid = 239;
	public static final int SetAgitAuction = 240;
	public static final int CancelAgitAuction = 241;
	public static final int CancelAgitBid = 242;
	public static final int AutoAgitAuction = 243;
	public static final int WinSiege = 244;
	public static final int WinSiegeAlliance = 245;
	public static final int AgitCost = 246;
	public static final int UndoDismissPledge = 247;
	public static final int CreateAlliance = 248;
	public static final int JoinAlliance = 249;
	public static final int DismissAlliance = 250;
	public static final int OustAlliance = 251;
	public static final int WithdrawAlliance = 252;
	public static final int BuyItem = 901;
	public static final int SellItem = 902;
	public static final int Deposit = 903;
	public static final int Retrieve = 904;
	public static final int GetItem = 906;
	public static final int DeleteItem = 907;
	public static final int Drop = 908;
	public static final int TradeGive = 909;
	public static final int TradeGet = 910;
	public static final int Use = 911;
	public static final int NPCDrop = 912;
	public static final int NPCShowSellPage = 913;
	public static final int NPCShowBuyPage = 914;
	public static final int DropItemWhenDied = 915;
	public static final int TradeDone = 916;
	public static final int TradeCanceled = 917;
	public static final int BeginTrade = 918;
	public static final int BeginDeposit = 919;
	public static final int DepositDone = 920;
	public static final int DepositCanceled = 921;
	public static final int BeginRetrieval = 922;
	public static final int RetrievalDone = 923;
	public static final int RetrievalCanceled = 924;
	public static final int EnchantItem = 925;
	public static final int EnchantItemFail = 926;
	public static final int DepositToWarehouse = 927;
	public static final int DespositFee = 928;
	public static final int RetrieveFromWarehouse = 929;
	public static final int SetPrivateMsg = 930;
	public static final int PrivateStoreSell = 931;
	public static final int PrivateStoreBuy = 932;
	public static final int CrystalizeItem = 933;
	public static final int RecipeDelete = 934;
	public static final int RecipeCreate = 935;
	public static final int ShipDepart = 936;
	public static final int ShipKicked = 937;
	public static final int UseArrow = 938;
	public static final int UseTelepoter = 939;
	public static final int RelatedItem = 940;
	public static final int SetBuyPrivateMsg = 941;
	public static final int ItemAddFailed = 942;
	public static final int RetrieveFromCastleWarehouse = 943;
	public static final int DepositToCastleWarehouse = 944;
	public static final int DepositFee2 = 945;
	public static final int KeepPackage = 946;
	public static final int KeepPackageFee = 947;
	public static final int BuyItemTax = 948;
	public static final int PrecedenceTax = 949;
	public static final int CastleTax = 950;
	public static final int SearchTax = 951;
	public static final int GetItemByAutoLoot = 952;
	public static final int GetItemInPaty = 953;
	public static final int PickupItem = 954;
	public static final int HarvesterItem = 955;
	public static final int SweepItem = 956;
	public static final int RetrieveFromClanWarehouse = 957;
	public static final int DepositToClanWarehouse = 958;
	public static final int Sys_GetItem = 959;
	public static final int Sys_DeleteItem = 960;
	public static final int LearnSkill = 401;
	public static final int DeleteSkill = 402;
	public static final int CastSkill = 403;
	public static final int CancelSkill = 405;
	public static final int Dispell = 406;
	public static final int DispellAll = 407;
	public static final int Web_CheckCharacter = 601;
	public static final int Web_SetCharLocation = 602;
	public static final int Web_SetBuilderCharacter = 603;
	public static final int Web_ChangeCharName = 604;
	public static final int Web_KickChar = 605;
	public static final int Web_AddSkill = 606;
	public static final int Web_DelSkill = 607;
	public static final int Web_ModSkill = 608;
	public static final int Web_SetOneTimeQuest = 609;
	public static final int Web_SetQuest = 610;
	public static final int Web_DelQuest = 611;
	public static final int Web_AddItem = 612;
	public static final int Web_DelItem = 613;
	public static final int Web_ModItem = 614;
	public static final int Web_ModChar = 615;
	public static final int Web_ModChar2 = 616;
	public static final int Web_ModCharPledge = 617;
	public static final int Web_PunishChar = 618;
	public static final int Web_SetBuilderAccount = 619;
	public static final int Web_DisableChar = 620;
	public static final int Web_EnableChar = 621;
	public static final int Web_GetChars = 622;
	public static final int Web_SetBookmark = 623;
	public static final int Web_DelBookmark = 624;
	public static final int Web_SeizeItem = 625;
	public static final int Web_Modchar3 = 626;
	public static final int Web_MoveItem = 627;
	public static final int Web_MoveChar_DisableChar = 628;
	public static final int Web_MoveChar = 629;
	public static final int Web_WriteComment = 630;
	public static final int Web_DeleteComment = 631;
	public static final int Web_DeleteCharCompletely = 632;
	public static final int Web_RestoreChar = 633;
	public static final int Web_Web_OustPledge = 634;
	public static final int Web_ChangePledgeOwner = 635;
	public static final int Web_DeletePledge = 636;
	public static final int Web_BanChar = 637;
	public static final int Web_AddItem2 = 638;
	public static final int Web_DelItem2 = 639;
	public static final int Web_MoveItem2 = 640;
	public static final int Web_CopyChar = 641;
	public static final int Web_CreatePet = 642;
	public static final int Adm_DebugChar = 501;
	public static final int Adm_SummonNpc = 502;
	public static final int Adm_SummonItem = 503;
	public static final int Adm_SetParam = 504;
	public static final int Adm_SetSkill = 505;
	public static final int Adm_TeleportToBookmark = 506;
	public static final int Adm_SetOneTimeQuest = 507;
	public static final int Adm_SetQuest = 508;
	public static final int Adm_KillMe = 509;
	public static final int Adm_Home = 510;
	public static final int Adm_SetAI = 511;
	public static final int Adm_SetKarma = 512;
	public static final int Adm_StopSay = 513;
	public static final int Adm_StopLogin = 514;
	public static final int Adm_GMListOn = 515;
	public static final int Adm_Petition = 517;
	public static final int Adm_Recall = 518;
	public static final int Adm_TeleportTo = 519;
	public static final int Adm_Kick = 520;
	public static final int Adm_Announce = 521;
	public static final int Adm_SetAnnounce = 522;
	public static final int Adm_DelAnnounce = 523;
	public static final int Adm_SetBuilder = 524;
	public static final int Adm_Summon = 525;
	public static final int Adm_DelSkill = 526;
	public static final int Adm_DelQuest = 527;
	public static final int Adm_UnregisterCastlePledge = 528;
	public static final int Adm_SetDoorHp = 529;
	public static final int Adm_SetPledgeLevel = 530;
	public static final int Adm_SetSiege = 531;
	public static final int Adm_SetQuickSiege = 532;
	public static final int Adm_SetCastleStatus = 533;
	public static final int Adm_Defend = 534;
	public static final int Adm_Attack = 535;
	public static final int Adm_SetCastleOwner = 536;
	public static final int Adm_SetBp = 537;
	public static final int Adm_Polymorph = 538;
	public static final int Adm_SendHome = 539;
	public static final int Adm_AddItem = 540;
	public static final int Adm_DelItem = 541;
	public static final int Authed = 801;
	public static final int Login = 802;
	public static final int EnterWorld = 803;
	public static final int Logout = 804;
	public static final int LeaveWorld = 805;
	public static final int CreateChar = 806;
	public static final int DeleteChar = 807;
	public static final int ChangeName = 808;
	public static final int ChangeCharLevel = 810;
	public static final int Teleport = 811;
	public static final int SaveCharInfo = 812;
	public static final int SaveCharItemInfo = 813;
	public static final int SaveQuest = 814;
	public static final int SaveOnetimeQuest = 815;
	public static final int ChangeCharClass = 816;
	public static final int CreateParty = 817;
	public static final int JoinParty = 818;
	public static final int DismissParty = 819;
	public static final int WithdrawParty = 820;
	public static final int OustParty = 821;
	public static final int Stand = 822;
	public static final int Sit = 823;
	public static final int EquipItem = 824;
	public static final int PCAttackPc = 825;
	public static final int PCAttackNpc = 826;
	public static final int NPCAttackPc = 827;
	public static final int NPCAttackNpc = 828;
	public static final int RestoreDeletedChar = 829;
	public static final int ConfirmDeleteChar = 830;
	public static final int Leaveworld2 = 831;
	public static final int SayCount = 832;
	public static final int EquippedItems = 833;
	public static final int DelCharByDelAccount = 834;
	public static final int CharLimitExceed = 835;
	public static final int PartyCount = 836;
	public static final int CharCount = 837;
	public static final int EndPrivateStore = 838;
	public static final int L2WalkerFound = 839;
	public static final int BugUse = 840;
	public static final int IllegalAction = 841;
	public static final int GetQuestItem = 301;
	public static final int DeleteQuestItem = 302;
	public static final int BeginQuest = 303;
	public static final int UpdateQuest = 304;
	public static final int DelQuest = 305;
	public static final int SetOneTimeQuest = 306;
	public static final int StopQuest = 307;
	public static final int PCDie = 1101;
	public static final int NPCKilledPlayer = 1105;
	public static final int NPCKilledNPC = 1106;
	public static final int PCKilledPlayer = 1111;
	public static final int PCKilledNPC = 1112;
	public static final int KillByDuel = 1113;
	public static final int KillByPK = 1114;
	public static final int Restart = 1115;
	public static final int SummonedNPCKillPC = 1116;
	public static final int GotHeightDamage = 1117;
	public static final int RessurectBy = 1118;
	public static final int DieDropItemCount = 1119;
	public static final int PcDamagedBy = 1120;
	public static final int CacheDAuditItemInfo = 1401;
	public static final int NPCSpawn = 1402;
	public static final int NPCDropItem = 1403;
	public static final int AuditItem = 1404;
	public static final int InvalidAdena = 1405;
	public static final int Login_CreateAcc = 1501;
	public static final int Login_Authed = 1502;
	public static final int Login_IncorrectPass = 1503;
	public static final int Login_Ban = 1504;
	public static final int Login_BanIp = 1505;
	public static final int Login_UnBanIp = 1506;
	public static final int Login_MissingAcc = 1507;
	public static final int Login_HackExcept = 1508;
	public static final int Login_LogoutFromGS = 1509;
	public static final int Login_GSConnect = 1510;
	public static final int Login_GSDisConnect = 1511;
	public static final int GS_start = 1601;
	public static final int GS_started = 1602;
	public static final int GS_SIGTERM = 1603;
	public static final int GS_shutdown = 1604;
	public static final int GS_restart = 1605;
	public static final int GS_aborting = 1606;
	public static final int CMD_FORTH = 1;
	public static final int CMD_ADMH = 2;
	private static final Logger _log = Logger.getLogger(Log.class.getName());
	private static final Logger _logCommand = Logger.getLogger("commands");
	private static final Logger _logGm = Logger.getLogger("gmactions");
	private static final SimpleDateFormat TIME_FORMATER = new SimpleDateFormat("yy.MM.dd HH:mm:ss");
	private static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("yyyy.MM.dd");

	public final static String getCurrentDate()
	{
		return DATE_FORMATER.format(System.currentTimeMillis());
	}

	public final static String getCurrentTime()
	{
		return TIME_FORMATER.format(System.currentTimeMillis());
	}

	private static final ReentrantLock mainLog = new ReentrantLock(), devLog = new ReentrantLock();

	public static void add(final String text, String filename)
	{
		if(filename.equals("items") || filename.equals("chat") || filename.equals("CommunityBoard"))
			filename = filename + getCurrentDate();

		add(text, filename, true, null);
	}

	public static void add(final String text, final String filename, final L2Player activeChar)
	{
		add(text, filename, true, activeChar);
	}

	public static void add(final String text, final String filename, final boolean date)
	{
		add(text, filename, date, null);
	}

	private final static void add(final String string, final String filename, final boolean date, final L2Player activeChar)
	{
		mainLog.lock();
		FileWriter fstream = null;
		BufferedWriter buffer = null;
		try
		{
			fstream = new FileWriter("log/game/" + filename + ".txt", true);
			buffer = new BufferedWriter(fstream);
			buffer.write(StringUtil.concat(date ? "[" + getCurrentTime() + "]: " : null, activeChar != null ? activeChar.toFullString() + " " : null, string));
			buffer.newLine();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Util: Error while logging to File - " + filename, e);
		}
		finally
		{
			IOUtils.closeQuietly(buffer);
			IOUtils.closeQuietly(fstream);
			mainLog.unlock();
		}
	}

	public static void addDev(final String text, final String filename)
	{
		addDev(text, filename, true);
	}

	public final static void addDev(final String string, final String filename, final boolean date)
	{
		devLog.lock();
		FileWriter fstream = null;
		BufferedWriter buffer = null;
		try
		{
			fstream = new FileWriter("log/developer/" + filename + ".txt", true);
			buffer = new BufferedWriter(fstream);
			buffer.write(StringUtil.concat(date ? "[" + getCurrentTime() + "]: " : null, string));
			buffer.newLine();
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Util: Error while logging to File - " + filename, e);
		}
		finally
		{
			IOUtils.closeQuietly(buffer);
			IOUtils.closeQuietly(fstream);
			devLog.unlock();
		}
	}

	private final static String[] dev_logs = new String[]
	{
			"log/developer/sql_skill_missing.txt",
			"log/developer/sql_skill_levels.txt",
			"log/developer/sql_skill_enchant_levels.txt",
			"log/developer/sql_skill_display_levels.txt",
			"log/developer/sql_skill_without_power.txt",
			"log/developer/sql_skill_power.txt",
			"log/developer/dev_item_skills_notexist.txt",
			"log/developer/dev_item_skills_notdone.txt",
			"log/developer/dev_item_unimplemented_sa.txt",
			"log/developer/dev_notdone_npc_skills.txt",
			"log/developer/dev_multisellbug.txt",
			"log/developer/dev_custom_message.txt",
			"log/developer/dev_notdone_pet_skills.txt",
			"log/developer/dev_notdone_support_pet_skills.txt",
			"log/developer/dev_doors_zero_size.txt",
			"log/developer/dev_droplist.txt",
			"log/developer/dev_locations_error.txt",
			"log/developer/dev_zoneskills.txt",
			"log/developer/skills_load_errors.txt"
	};

	public static final void initDevLogs()
	{
		try
		{
			FileUtils.forceMkdir(new File("log/developer"));
		}
		catch(final IOException e)
		{
			_log.log(Level.WARNING, "Util: error while create dev logs folder! ", e);
		}
		// удаялем файлы с логами
		for(final String log : dev_logs)
			Util.deleteFile(log);
	}

	public static void LogCommand(final L2Player activeChar, final int command_type, final String command, final Integer success)
	{
		final StringBuffer msgb = new StringBuffer(160);
		if(activeChar.isGM())
			msgb.append("GM ");
		msgb.append(activeChar.toFullString()).append(" command_type:").append(command_type).append(" success:").append(success).append(" command:").append(command);
		_logCommand.info(msgb.toString());

		if(activeChar.isGM())
		{
			msgb.append(" [target: ").append(activeChar.getTarget()).append("]");
			_logGm.info(msgb.toString());
		}
	}

	public static void LogItem(final L2Character activeObject, final int log_id, final L2ItemInstance Item)
	{
		LogItem(activeObject, null, log_id, Item);
	}

	public static void LogItem(final L2Character activeObject, final L2Character target, final int log_id, final L2ItemInstance Item)
	{
		LogItem(activeObject, target, log_id, Item, 0);
	}

	public static void LogItem(final L2Character activeObject, final int log_id, final L2ItemInstance Item, final long count)
	{
		LogItem(activeObject, null, log_id, Item, count);
	}

	public static void LogItem(final L2Character activeObject, final L2Character target, final int log_id, final L2ItemInstance Item, final long count)
	{
		if((log_id > 1000 || log_id < 900) && log_id != 301 && log_id != 302 && log_id < 111 && log_id > 113)
		{
			_log.log(Level.WARNING, "Incorrect log_id " + log_id + " for LogItem", new Exception("LogItem"));
			return;
		}
		if(activeObject == null)
		{
			_log.log(Level.WARNING, "null activeObject (target: " + target + "; log_id: " + log_id + "; Item: " + Item + "; count: " + count + ") for LogItem", new Exception("LogItem"));
			return;
		}
		if(Item == null)
		{
			_log.log(Level.WARNING, "null Item (activeObject: " + activeObject + "; target: " + target + "; log_id: " + log_id + "; count: " + count + ") for LogItem", new Exception("LogItem"));
			return;
		}
	}

	public static void LogPetition(final L2Player fromChar, final Integer Petition_type, final String Petition_text)
	{
		add(fromChar.toFullString() + "|" + Petition_type + "|" + Petition_text, "petitions.txt");
	}
}
