package l2n.util;

public final class StringUtil
{
	public final static String EMPTY_STRING = "";
	public final static String NOT_CONNECTED = "<not connected>";
	public final static String NO_STACK = "none".intern();
	public final static String EMPTY_ICON = "skill0000";

	public static String concat(final String... strings)
	{
		final StringBuilder builder = new StringBuilder(getLength(strings));
		for(final String string : strings)
			if(string != null)
				builder.append(string);

		return builder.toString();
	}

	public static String concat(final Object... strings)
	{
		final StringBuilder builder = new StringBuilder(getLength(strings));
		for(final Object string : strings)
			if(string != null)
				builder.append(string);

		return builder.toString();
	}

	public static StringBuilder startAppend(final int sizeHint, final String... strings)
	{
		final int length = getLength(strings);
		final StringBuilder builder = new StringBuilder(sizeHint > length ? sizeHint : length);
		for(final String string : strings)
			if(string != null)
				builder.append(string);

		return builder;
	}

	public static StringBuilder startAppend(final int sizeHint, final Object... strings)
	{
		final int length = getLength(strings);
		final StringBuilder builder = new StringBuilder();
		builder.ensureCapacity(sizeHint > length ? sizeHint : length);

		for(final Object string : strings)
			if(string != null)
				builder.append(string);

		return builder;
	}

	public static void append(final StringBuilder builder, final String... strings)
	{
		builder.ensureCapacity(builder.length() + getLength(strings));
		for(final String string : strings)
			if(string != null)
				builder.append(string);
	}

	public static void append(final StringBuilder builder, final Object... strings)
	{
		builder.ensureCapacity(builder.length() + getLength(strings));
		for(final Object string : strings)
			if(string != null)
				builder.append(string);
	}

	private static int getLength(final String... strings)
	{
		int length = 0;
		for(final String string : strings)
			length += string == null ? 4 : string.length();

		return length;
	}

	private static int getLength(final Object... strings)
	{
		int length = 0;
		for(final Object string : strings)
			length += string == null ? 4 : String.valueOf(string).length();

		return length;
	}

	public static String rightPad(final String string, final int length)
	{
		if(string.length() >= length)
			return string;

		final StringBuilder builder = new StringBuilder(length);
		builder.append(string);

		for(int i = string.length(); i < length; i++)
			builder.append(' ');

		return builder.toString();
	}

	public static String leftPad(final String string, final int length)
	{
		if(string.length() >= length)
			return string;

		final StringBuilder builder = new StringBuilder(length);
		for(int i = string.length(); i < length; i++)
			builder.append(' ');

		builder.append(string);

		return builder.toString();
	}

	public static void recycle(StringBuilder instance)
	{
		instance = null;
	}
}
