package l2n.util;

import javolution.text.TypeFormat;
import org.w3c.dom.Node;

public class XMLUtil
{
	public static int readSubNodeIntValue(Node n, String name, int dflt)
	{
		if(name == null)
			return dflt;
		for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
			if(name.equalsIgnoreCase(d.getNodeName()))
				return TypeFormat.parseInt(d.getNodeValue());
		return dflt;
	}

	public static long readSubNodeLongValue(Node n, String name, long dflt)
	{
		if(name == null)
			return dflt;
		for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
			if(name.equalsIgnoreCase(d.getNodeName()))
				return TypeFormat.parseLong(d.getNodeValue());
		return dflt;
	}

	public static String readSubNodeValue(Node n, String name, String dflt)
	{
		if(name == null)
			return dflt;
		for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
			if(name.equalsIgnoreCase(d.getNodeName()))
				return d.getNodeValue();
		return dflt;
	}

	public static String getAttributeValue(Node n, String item)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return "";
		String val = d.getNodeValue();
		if(val == null)
			return "";
		return val;
	}

	public static String getAttributeValue(Node n, String item, String dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return val;
	}

	public static boolean getAttributeBooleanValue(Node n, String item, boolean dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return TypeFormat.parseBoolean(val);
	}

	public static Integer getAttributeIntValue(Node n, String item)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return null;
		String val = d.getNodeValue();
		if(val == null)
			return null;
		return TypeFormat.parseInt(val);
	}

	public static int getAttributeIntValue(Node n, String item, int dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return TypeFormat.parseInt(val);
	}

	public static long getAttributeLongValue(Node n, String item, long dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return TypeFormat.parseLong(val);
	}

	public static double getAttributeDoubleValue(Node n, String item, double dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return TypeFormat.parseDouble(val);
	}

	public static float getAttributeFloatValue(Node n, String item, float dflt)
	{
		Node d = n.getAttributes().getNamedItem(item);
		if(d == null)
			return dflt;
		String val = d.getNodeValue();
		if(val == null)
			return dflt;
		return TypeFormat.parseFloat(val);
	}

	public static Node findSubNode(Node n, String name)
	{
		if(name == null)
			return null;
		for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
			if(name.equalsIgnoreCase(d.getNodeName()))
				return d;
		return null;
	}
}
