package l2n.util;

import javolution.util.FastMap;
import l2n.Config;
import l2n.commons.list.GArray;
import l2n.commons.list.GSArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.FiltredStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.database.utils.mysql;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HWID
{
	private static final Logger _log = Logger.getLogger(HWID.class.getName());

	private static final GArray<HardwareID> banned_hwids = new GArray<HardwareID>();
	private static final GSArray<Entry<HardwareID, FastMap<String, Integer>>> bonus_hwids = new GSArray<Entry<HardwareID, FastMap<String, Integer>>>();

	public static final HWIDComparator DefaultComparator = new HWIDComparator();
	public static final HWIDComparator BAN_Comparator = new HWIDComparator(false);

	public static final String SELECT_HWID = "SELECT HWID FROM " + Config.HWID_BANS_TABLE;
	public static final String REPLACE_HWID = "REPLACE INTO " + Config.HWID_BANS_TABLE + " (hwid,comment) VALUES (?,?)";
	public static final String DELETE_HWID = "DELETE FROM " + Config.HWID_BANS_TABLE + " WHERE hwid=?";

	public static void reloadBannedHWIDs()
	{
		synchronized (banned_hwids)
		{
			banned_hwids.clear();
		}

		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		ResultSet rs = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement(SELECT_HWID);
			rs = st.executeQuery();
			synchronized (banned_hwids)
			{
				while (rs.next())
					banned_hwids.add(new HardwareID(rs.getString("HWID")));
			}

			_log.info("Protection: Loaded " + banned_hwids.size() + " banned HWIDs");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Protection: Failed to load banned HWIDs", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st, rs);
		}
	}

	public static void reloadBonusHWIDs()
	{
		final long now = System.currentTimeMillis() / 1000;
		mysql.set("DELETE FROM `hwid_bonus` WHERE `type`='tradeBan' AND `value` > -1 AND `value` < " + now);
		mysql.set("DELETE FROM `hwid_bonus` WHERE `type` LIKE 'newbie%' AND UNIX_TIMESTAMP(`time`) < " + (now - 60 * 60 * 24 * 7));
		synchronized (bonus_hwids)
		{
			bonus_hwids.clear();

			ThreadConnection con = null;
			FiltredPreparedStatement st = null;
			ResultSet rs = null;
			try
			{
				con = L2DatabaseFactory.getInstance().getConnection();
				st = con.prepareStatement("SELECT * FROM hwid_bonus");
				rs = st.executeQuery();
				while (rs.next())
				{
					final HardwareID hwid = new HardwareID(rs.getString("HWID"));
					final String type = rs.getString("type");
					final int value = rs.getInt("value");

					FastMap<String, Integer> oldMap = null;
					for(final Entry<HardwareID, FastMap<String, Integer>> entry : bonus_hwids)
						if(entry.getKey().equals(hwid))
						{
							oldMap = entry.getValue();
							break;
						}

					if(oldMap != null)
					{
						final Integer oldValue = oldMap.get(type);
						if(oldValue == null || oldValue < value)
							oldMap.put(type, value);
					}
					else
					{
						final FastMap<String, Integer> bonus = new FastMap<String, Integer>();
						bonus.put(type, value);
						bonus_hwids.add(new Entry<HardwareID, FastMap<String, Integer>>(hwid, bonus));
					}
				}

				_log.info("Protection: Loaded " + bonus_hwids.size() + " bonus HWIDs");
			}
			catch(final Exception e)
			{
				_log.log(Level.WARNING, "Protection: Failed to load bonus HWIDs", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, st, rs);
			}
		}
	}

	public static boolean checkHWIDBanned(final HardwareID hwid)
	{
		synchronized (banned_hwids)
		{
			return hwid != null && BAN_Comparator.contains(hwid, banned_hwids);
		}
	}

	public static String handleBanHWID(final String[] argv)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS)
			return "HWID bans feature disabled";

		if(argv == null || argv.length < 2)
			return "USAGE: banhwid char_name|hwid [kick:true|false] [reason]";

		String hwid = argv[1];
		if(hwid.length() != 32)
		{
			final L2Player plyr = L2ObjectsStorage.getPlayer(hwid);
			if(plyr == null)
				return "Player " + hwid + " not found in world";
			if(!plyr.hasHWID())
				return "Player " + hwid + " not connected (offline trade)";
			hwid = plyr.getHWID().Full;
		}

		if(argv.length == 2)
			BanHWID(hwid, "", true);
		else
		{
			boolean kick = true;
			String reason = "";
			if(argv[2].equalsIgnoreCase("true") || argv[2].equalsIgnoreCase("false"))
			{
				kick = Boolean.parseBoolean(argv[2]);
				if(argv.length > 3)
				{
					for(int i = 3; i < argv.length; i++)
						reason += argv[i] + " ";
					reason = reason.trim();
				}
			}
			
			else
			{
				for(int i = 2; i < argv.length; i++)
					reason += argv[i] + " ";
				reason = reason.trim();
			}
			BanHWID(hwid, reason, kick);
		}
		return "HWID " + hwid + " banned";
	}

	public static boolean BanHWID(final String hwid, final String comment)
	{
		return BanHWID(hwid, comment, false);
	}

	public static boolean BanHWID(final String hwid, final String comment, final boolean kick)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS || hwid == null || hwid.isEmpty())
			return false;
		return BanHWID(new HardwareID(hwid), comment, kick);
	}

	public static boolean BanHWID(final HardwareID hwid, final String comment, final boolean kick)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS || hwid == null)
			return false;

		if(checkHWIDBanned(hwid))
		{
			_log.info("Protection: HWID: " + hwid + " already banned");
			return true;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement(REPLACE_HWID);
			st.setString(1, hwid.Full);
			st.setString(2, comment);
			st.execute();

			synchronized (banned_hwids)
			{
				banned_hwids.add(hwid);
			}
			Log.add("Banned HWID: " + hwid, "protect");

			if(kick)
				for(final L2Player cha : getPlayersByHWID(hwid))
					cha.logout(false, false, true, true);
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Protection: Failed to ban HWID: " + hwid, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}

		return checkHWIDBanned(hwid);
	}

	public static boolean UnbanHWID(final String hwid)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS || hwid == null || hwid.isEmpty())
			return false;
		return UnbanHWID(new HardwareID(hwid));
	}

	public static boolean UnbanHWID(final HardwareID hwid)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BANS || hwid == null)
			return false;

		if(!checkHWIDBanned(hwid))
		{
			_log.info("Protection: HWID: " + hwid + " already not banned");
			return true;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.prepareStatement(DELETE_HWID);
			st.setString(1, hwid.Full);
			st.execute();

			synchronized (banned_hwids)
			{
				BAN_Comparator.remove(hwid, banned_hwids);
			}
			Log.add("Unbanned HWID: " + hwid, "protect");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Protection: Failed to unban HWID: " + hwid, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}
		return !checkHWIDBanned(hwid);
	}

	public static GArray<L2Player> getPlayersByHWID(final HardwareID hwid)
	{
		final GArray<L2Player> result = new GArray<L2Player>();
		if(hwid != null)
			for(final L2Player cha : L2ObjectsStorage.getAllPlayers())
				if(!cha.isInOfflineMode() && cha.getNetConnection() != null && cha.getNetConnection().protect_used && hwid.equals(cha.getHWID()))
					result.add(cha);
		return result;
	}

	public static void setBonus(final HardwareID hwid, final String type, final int value)
	{
		setBonus(hwid, type, value, 0);
	}

	public static void setBonus(final HardwareID hwid, final String type, final int value, final long timestamp)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BONUS || hwid == null)
			return;

		ThreadConnection con = null;
		FiltredStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.createStatement();

			st.executeUpdate("REPLACE INTO hwid_bonus (HWID,type,value,time) VALUES ('" + hwid.Full + "','" + type + "'," + value + "," + (timestamp == 0 ? "CURRENT_TIMESTAMP()" : "FROM_UNIXTIME(" + timestamp + ")") + ")");

			FastMap<String, Integer> oldMap = null;
			synchronized (bonus_hwids)
			{
				for(final Entry<HardwareID, FastMap<String, Integer>> entry : bonus_hwids)
					if(entry.getKey().equals(hwid))
					{
						oldMap = entry.getValue();
						break;
					}

				if(oldMap != null)
				{
					final Integer oldValue = oldMap.get(type);
					if(oldValue == null || oldValue < value)
						oldMap.put(type, value);
				}
				else
				{
					final FastMap<String, Integer> bonus = new FastMap<String, Integer>();
					bonus.put(type, value);
					bonus_hwids.add(new Entry<HardwareID, FastMap<String, Integer>>(hwid, bonus));
				}
			}
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Protection: Failed to add bonus HWID: " + hwid, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}
	}

	public static void unsetBonus(final HardwareID hwid, final String type)
	{
		if(!Config.PROTECT_ENABLE || !Config.PROTECT_GS_ENABLE_HWID_BONUS || hwid == null)
			return;

		FastMap<String, Integer> bonus = null;
		synchronized (bonus_hwids)
		{
			for(final Entry<HardwareID, FastMap<String, Integer>> entry : bonus_hwids)
				if(entry.getKey().equals(hwid))
				{
					bonus = entry.getValue();
					break;
				}
		}
		if(bonus == null)
			return;
		final Integer isSet = bonus.get(type);
		if(isSet == null || isSet == 0)
			return;
		bonus.remove(type);

		ThreadConnection con = null;
		FiltredStatement st = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			st = con.createStatement();

			st.executeUpdate("DELETE FROM `hwid_bonus` WHERE `HWID`='" + hwid.Full + "' AND `type`='" + type + "'");
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Protection: Failed to remove bonus HWID: " + hwid, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, st);
		}
	}

	public static int getBonus(final HardwareID hwid, final String type)
	{
		return getBonus(hwid, type, DefaultComparator);
	}

	public static int getBonus(final HardwareID hwid, final String type, final HWIDComparator comparator)
	{
		if(hwid == null)
			return 0;

		final GArray<Entry<HardwareID, FastMap<String, Integer>>> possible = getAllMatches(hwid, comparator);
		if(possible.isEmpty())
			return 0;
		int max = 0;
		for(final Entry<HardwareID, FastMap<String, Integer>> entry : possible)
			if(entry.getValue() != null && !entry.getValue().isEmpty())
			{
				final FastMap<String, Integer> bonuses = entry.getValue();
				final Integer value = bonuses.get(type);
				if(value != null)
					if(max == 0)
						max = value;
					else
						max = Math.max(value, max);
			}
		return max;
	}

	public static GArray<Entry<HardwareID, FastMap<String, Integer>>> getAllMatches(final HardwareID hwid, final HWIDComparator comparator)
	{
		final GArray<Entry<HardwareID, FastMap<String, Integer>>> ret = new GArray<Entry<HardwareID, FastMap<String, Integer>>>();
		synchronized (bonus_hwids)
		{
			for(final Entry<HardwareID, FastMap<String, Integer>> entry : bonus_hwids)
				if(comparator.compare(entry.getKey(), hwid) == HWIDComparator.EQUALS)
					ret.add(entry);
		}
		return ret;
	}

	public static int getBonus(final L2Player player, final String type, final HWIDComparator comparator)
	{
		return player != null && player.hasHWID() ? getBonus(player.getHWID(), type, comparator) : 0;
	}

	public static int getBonus(final L2Player player, final String type)
	{
		return getBonus(player, type, DefaultComparator);
	}

	public static class HardwareID
	{
		public static final boolean COMPARE_CPU = true;
		public static final boolean COMPARE_BIOS = true;
		public static final boolean COMPARE_HDD = true;
		public static final boolean COMPARE_MAC = true;
		public static final boolean COMPARE_WLInternal = false;

		public final int CPU, WLInternal;
		public final long BIOS, HDD, MAC;
		public final String Full;

		public HardwareID(final String s)
		{
			Full = s;
			CPU = Integer.decode("0x" + s.substring(0, 4));
			BIOS = Long.decode("0x" + s.substring(4, 12));
			HDD = Long.decode("0x" + s.substring(12, 20));
			MAC = Long.decode("0x" + s.substring(20, 28));
			WLInternal = Integer.decode("0x" + s.substring(28, 32));
		}

		@Override
		public int hashCode()
		{
			String hwidString = "";
			if(COMPARE_CPU)
				hwidString += Full.substring(0, 4);
			if(COMPARE_BIOS)
				hwidString += Full.substring(4, 12);
			if(COMPARE_HDD)
				hwidString += Full.substring(12, 20);
			if(COMPARE_MAC)
				hwidString += Full.substring(20, 28);
			if(COMPARE_WLInternal)
				hwidString += Full.substring(28, 32);
			return hwidString.hashCode();
		}

		@Override
		public boolean equals(final Object obj)
		{
			if(obj == null || !(obj instanceof HardwareID))
				return false;
			return DefaultComparator.compare(this, (HardwareID) obj) == HWIDComparator.EQUALS;
		}

		@Override
		public String toString()
		{
			return String.format("%s [CPU: %04x, BIOS: %08x, HDD: %08x, MAC: %08x, WLInternal: %04x]", Full, CPU, BIOS, HDD, MAC, WLInternal);
		}
	}

	public static class HWIDComparator implements Comparator<HardwareID>
	{
		public static final int EQUALS = 0;
		public static final int NOT_EQUALS = 1;

		public boolean COMPARE_CPU = HardwareID.COMPARE_CPU;
		public boolean COMPARE_BIOS = HardwareID.COMPARE_BIOS;
		public boolean COMPARE_HDD = HardwareID.COMPARE_HDD;
		public boolean COMPARE_MAC = HardwareID.COMPARE_MAC;
		public boolean COMPARE_WLInternal = HardwareID.COMPARE_WLInternal;

		public HWIDComparator()
		{}

		public HWIDComparator(final boolean _MAC)
		{
			COMPARE_MAC = _MAC;
		}

		public HWIDComparator(final boolean _MAC, final boolean _CPU)
		{
			COMPARE_MAC = _MAC;
			COMPARE_CPU = _CPU;
		}

		public HWIDComparator(final boolean _MAC, final boolean _CPU, final boolean _BIOS)
		{
			COMPARE_MAC = _MAC;
			COMPARE_CPU = _CPU;
			COMPARE_BIOS = _BIOS;
		}

		public HWIDComparator(final boolean _MAC, final boolean _CPU, final boolean _BIOS, final boolean _HDD)
		{
			COMPARE_MAC = _MAC;
			COMPARE_CPU = _CPU;
			COMPARE_HDD = _HDD;
			COMPARE_BIOS = _BIOS;
		}

		public HWIDComparator(final boolean _MAC, final boolean _CPU, final boolean _BIOS, final boolean _HDD, final boolean _WLInternal)
		{
			COMPARE_MAC = _MAC;
			COMPARE_CPU = _CPU;
			COMPARE_HDD = _HDD;
			COMPARE_BIOS = _BIOS;
			COMPARE_WLInternal = _WLInternal;
		}

		@Override
		public int compare(final HardwareID o1, final HardwareID o2)
		{
			if(o1 == null || o2 == null)
				return o1 == o2 ? EQUALS : NOT_EQUALS;
			if(COMPARE_CPU && o1.CPU != o2.CPU)
				return NOT_EQUALS;
			if(COMPARE_BIOS && o1.BIOS != o2.BIOS)
				return NOT_EQUALS;
			if(COMPARE_HDD && o1.HDD != o2.HDD)
				return NOT_EQUALS;
			if(COMPARE_MAC && o1.MAC != o2.MAC)
				return NOT_EQUALS;
			if(COMPARE_WLInternal && o1.WLInternal != o2.WLInternal)
				return NOT_EQUALS;
			return EQUALS;
		}

		public int find(final HardwareID hwid, final List<HardwareID> in)
		{
			for(int i = 0; i < in.size(); i++)
				if(compare(hwid, in.get(i)) == EQUALS)
					return i;
			return -1;
		}

		public boolean contains(final HardwareID hwid, final List<HardwareID> in)
		{
			return find(hwid, in) != -1;
		}

		public boolean remove(final HardwareID hwid, final List<HardwareID> in)
		{
			final int i = find(hwid, in);
			return i == -1 ? false : in.remove(i) != null;
		}

		public int find(final HardwareID hwid, final GArray<HardwareID> in)
		{
			for(int i = 0; i < in.size(); i++)
				if(compare(hwid, in.get(i)) == EQUALS)
					return i;
			return -1;
		}

		public boolean contains(final HardwareID hwid, final GArray<HardwareID> in)
		{
			return find(hwid, in) != -1;
		}

		public boolean remove(final HardwareID hwid, final GArray<HardwareID> in)
		{
			final int i = find(hwid, in);
			return i == -1 ? false : in.remove(i) != null;
		}

		@Override
		public String toString()
		{
			return "HWIDComparator [MAC: " + COMPARE_MAC + "] [CPU: " + COMPARE_CPU + "] [BIOS: " + COMPARE_BIOS + "] [HDD: " + COMPARE_HDD + "]";
		}
	}

	private static final class Entry<K, V> implements Map.Entry<K, V>
	{
		private final K _key;
		private V _value;

		public Entry(final K key, final V value)
		{
			_key = key;
			_value = value;
		}

		@Override
		public K getKey()
		{
			return _key;
		}

		@Override
		public V getValue()
		{
			return _value;
		}

		@Override
		public V setValue(final V value)
		{
			return _value = value;
		}
	}
}
