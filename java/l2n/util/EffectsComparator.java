package l2n.util;

import l2n.game.model.L2Effect;

import java.util.Comparator;

public final class EffectsComparator implements Comparator<L2Effect>
{
	private static final int LESS = -1;
	private static final int EQUALS = 0;
	private static final int GREATER = 1;

	public static final EffectsComparator STATIC_INSTANCE = new EffectsComparator();

	@Override
	public int compare(L2Effect effect1, L2Effect effect2)
	{
		if(effect1 == null || effect2 == null || effect1 == effect2)
			return EQUALS;

		if(effect1.getSkill().isToggle())
			return GREATER;

		if(effect2.getSkill().isToggle())
			return LESS;

		if(isNormalBuff(effect1) && !isNormalBuff(effect2))
			return LESS;

		if(effect1.getSkill().is7Signs() && isNormalBuff(effect2))
			return GREATER;

		if(effect1.getSkill().is7Signs() && (effect2.getSkill().isToggle() || effect2.getSkill().isDanceSong()))
			return LESS;

		if(effect1.getSkill().isToggle() && (effect2.getSkill().is7Signs() || isNormalBuff(effect2)))
			return GREATER;

		if(effect1.getSkill().isToggle() && effect2.getSkill().isDanceSong())
			return LESS;

		if(effect1.getSkill().isDanceSong() && !effect2.getSkill().isDanceSong() && !isOffensive(effect2))
			return GREATER;

		if(effect1.getSkill().isDanceSong() && isOffensive(effect2))
			return LESS;

		if(isOffensive(effect1) && !isOffensive(effect2))
			return GREATER;

		if(effect1.getStartTime() > effect2.getStartTime())
			return GREATER;
		if(effect1.getStartTime() < effect2.getStartTime())
			return LESS;

		return EQUALS;
	}

	private boolean isNormalBuff(L2Effect e)
	{
		return !e.getSkill().isToggle() && !e.getSkill().isDanceSong() && !e.getSkill().is7Signs() && !isOffensive(e);
	}

	private boolean isOffensive(L2Effect e)
	{
		return e.getSkill().isOffensive() || e.getSkill().isDebuff();
	}
}
