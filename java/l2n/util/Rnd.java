package l2n.util;

import l2n.game.model.L2Object;

public class Rnd
{
	private static final MTRandom _rnd = new MTRandom();

	public static float get()
	{
		return _rnd.nextFloat();
	}

	public static int get(final int n)
	{
		return (int) Math.floor(_rnd.nextDouble() * n);
	}

	public static long get(long n)
	{
		return (long) (_rnd.nextDouble() * n);
	}

	public static int get(final int min, final int max)
	{
		return min + (int) Math.floor(_rnd.nextDouble() * (max - min + 1));
	}

	public static long get(final long min, final long max)
	{
		return min + (long) Math.floor(_rnd.nextDouble() * (max - min + 1));
	}

	public static double get(final double min, final double max)
	{
		return min + Math.floor(_rnd.nextDouble() * (max - min + 1));
	}

	public static int nextInt()
	{
		return _rnd.nextInt();
	}

	public static double nextDouble()
	{
		return _rnd.nextDouble();
	}

	public static double nextGaussian()
	{
		return _rnd.nextGaussian();
	}

	public static boolean nextBoolean()
	{
		return _rnd.nextBoolean();
	}

	public static boolean chance(final int chance)
	{
		return chance >= 1 && (chance > 99 || _rnd.nextInt(99) + 1 <= chance);
	}

	public static boolean chance(final double chance)
	{
		return _rnd.nextDouble() <= chance / 100.;
	}

	public static Location coordsRandomize(final L2Object obj, final int min, final int max)
	{
		return obj.getLoc().rnd(min, max, false);
	}

	public static Location coordsRandomize(final L2Object obj, final int radius)
	{
		return coordsRandomize(obj, 0, radius);
	}

	public static Location coordsRandomize(final int x, final int y, final int z, final int heading, final int radius_min, final int radius_max)
	{
		if(radius_max == 0 || radius_max < radius_min)
			return new Location(x, y, z, heading);
		final int radius = get(radius_min, radius_max);
		final double angle = _rnd.nextDouble() * 2 * Math.PI;
		return new Location((int) (x + radius * Math.cos(angle)), (int) (y + radius * Math.sin(angle)), z, heading);
	}

	public static Location coordsRandomize(final Location pos, final int radius_min, final int radius_max)
	{
		return coordsRandomize(pos.x, pos.y, pos.z, pos.h, radius_min, radius_max);
	}
}
