package l2n.util;

import l2n.Config;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.L2Drop;
import l2n.game.tables.SkillTable;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Util
{
	private final static Logger _log = Logger.getLogger(Util.class.getName());

	private static final char[][] CHARSET =
	{
			"abcdefghijklmnopqrstuvwxyz".toCharArray(),
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray(),
			"0123456789".toCharArray()
	};

	private static final String PATTERN = "0.0000000000E00";
	private static final DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
	static
	{
		df.applyPattern(PATTERN);
		df.setPositivePrefix("+");
	}

	public static final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");


	private static final NumberFormat adenaFormatter = NumberFormat.getIntegerInstance(Locale.FRANCE);

	public static boolean isInternalIP(final String ipAddress)
	{
		if(Config.INTERNAL_NETLIST != null)
			return Config.INTERNAL_NETLIST.isIpInNets(ipAddress);

		for(final String s : Config.INTERNAL_IP)
			if(checkIfIpInRange(ipAddress, s))
				return true;
		return false;
	}

	private static boolean checkIfIpInRange(String ip, final String ipRange)
	{
		// DATA FIELD
		// Ip format. 110.20.30.40 - 50.50.30.40
		int userIp1 = -1;
		int userIp2 = -1;
		int userIp3 = -1;
		int userIp4 = -1;
		String firstIp;
		String lastIp;
		// Data field end.

		ip = ip.replace(".", ",");
		for(final String s : ip.split(","))
			if(userIp1 == -1)
				userIp1 = Integer.parseInt(s);
			else if(userIp2 == -1)
				userIp2 = Integer.parseInt(s);
			else if(userIp3 == -1)
				userIp3 = Integer.parseInt(s);
			else
				userIp4 = Integer.parseInt(s);

		int ipMin1 = -1;
		int ipMin2 = -1;
		int ipMin3 = -1;
		int ipMin4 = -1; // IP values for min ip
		int ipMax1 = -1;
		int ipMax2 = -1;
		int ipMax3 = -1;
		int ipMax4 = -1; // Ip values for max ip

		final StringTokenizer st = new StringTokenizer(ipRange, "-"); // Try to split them by "-" symbol

		if(Config.DEBUG)
			System.out.println("Tokens in string " + ipRange + ": " + st.countTokens());

		if(st.countTokens() == 2)
		{
			firstIp = st.nextToken(); // get our first ip string
			lastIp = st.nextToken(); // get out second ip string.

			firstIp = firstIp.replace(".", ",");
			lastIp = lastIp.replace(".", ",");

			// Set our minimum ip
			for(final String s1 : firstIp.split(","))
				if(ipMin1 == -1)
					ipMin1 = Integer.parseInt(s1);
				else if(ipMin2 == -1)
					ipMin2 = Integer.parseInt(s1);
				else if(ipMin3 == -1)
					ipMin3 = Integer.parseInt(s1);
				else
					ipMin4 = Integer.parseInt(s1);

			// set our maximum ip
			for(final String s2 : lastIp.split(","))
				if(ipMax1 == -1)
					ipMax1 = Integer.parseInt(s2);
				else if(ipMax2 == -1)
					ipMax2 = Integer.parseInt(s2);
				else if(ipMax3 == -1)
					ipMax3 = Integer.parseInt(s2);
				else
					ipMax4 = Integer.parseInt(s2);

			// Now we are making some checks with our ips.
			if(userIp1 > ipMin1 && userIp1 < ipMax1)
				return true; // it's internal
			else if(userIp1 < ipMin1 || userIp1 > ipMax1)
				return false; // it's external
			else if(userIp1 == ipMin1 && userIp1 != ipMax1)
			{
				if(userIp2 > ipMin2)
					return true;
				else if(userIp2 < ipMin2)
					return false;
				else if(userIp3 > ipMin3)
					return true;
				else if(userIp3 < ipMin3)
					return false;
				else
					return userIp4 >= ipMin4;
			}
			else if(userIp1 != ipMin1 && userIp1 == ipMax1)
			{
				if(userIp2 < ipMax2)
					return true;
				else if(userIp2 > ipMax2)
					return false;
				else if(userIp3 < ipMax3)
					return true;
				else if(userIp3 > ipMax3)
					return false;
				else
					return userIp4 <= ipMax4;
			}
			else if(userIp2 > ipMin2 && userIp2 < ipMax2)
				return true; // it's internal
			else if(userIp2 < ipMin2 || userIp2 > ipMax2)
				return false; // it's external
			else if(userIp2 == ipMin2 && userIp2 != ipMax2)
			{
				if(userIp3 > ipMin3)
					return true;
				else if(userIp3 < ipMin3)
					return false;
				else
					return userIp4 >= ipMin4;
			}
			else if(userIp2 != ipMin2 && userIp2 == ipMax2)
			{
				if(userIp3 < ipMax3)
					return true;
				else if(userIp3 > ipMax3)
					return false;
				else
					return userIp4 <= ipMax4;
			}
			else if(userIp3 > ipMin3 && userIp3 < ipMax3)
				return true; // it's internal
			else if(userIp3 < ipMin3 || userIp3 > ipMax3)
				return false; // it's external
			else if(userIp3 == ipMin3 && userIp3 != ipMax3)
				return userIp4 >= ipMin4;
			else if(userIp3 != ipMin3 && userIp3 == ipMax3)
				return userIp4 <= ipMax4;
			else if(userIp4 >= ipMin4 && userIp4 <= ipMax4)
				return true; // it's internal
			else if(userIp4 < ipMin4 || userIp4 > ipMax4)
				return false; // it's external
		}
		else if(st.countTokens() == 1)
		{
			if(ip.equalsIgnoreCase(ipRange))
				return true;
		}
		else
			_log.warning("Error in internal ip detection: " + ipRange);
		return false;
	}


	public static boolean isMatchingRegexp(final String text, final String template)
	{
		Pattern pattern = null;
		try
		{
			pattern = Pattern.compile(template);
		}
		catch(final PatternSyntaxException e) // invalid template
		{
			e.printStackTrace();
		}
		if(pattern == null)
			return false;
		final Matcher regexp = pattern.matcher(text);
		return regexp.matches();
	}

	public static String replaceRegexp(String source, final String template, final String replacement)
	{
		Pattern pattern = null;
		try
		{
			pattern = Pattern.compile(template);
		}
		catch(final PatternSyntaxException e) // invalid template
		{
			e.printStackTrace();
		}
		if(pattern != null)
		{
			final Matcher regexp = pattern.matcher(source);
			source = regexp.replaceAll(replacement);
		}
		return source;
	}

	public static String formatDouble(final double x, final String nanString, final boolean forceExponents)
	{
		if(Double.isNaN(x))
			return nanString;
		if(forceExponents)
			return df.format(x);
		if((long) x == x)
			return String.valueOf((long) x);
		return String.valueOf(x);
	}

	public static void handleIllegalPlayerAction(final L2Player actor, final String etc_str1, final String etc_str2, final int isBug)
	{
		L2GameThreadPools.getInstance().scheduleGeneral(new IllegalPlayerAction(actor, etc_str1, etc_str2, isBug), 300);
	}

	public static String getRelativePath(final File base, final File file)
	{
		return file.toURI().getPath().substring(base.toURI().getPath().length());
	}

	/** Return degree value of object 2 to the horizontal line with object 1 being the origin */
	public static double calculateAngleFrom(final L2Object obj1, final L2Object obj2)
	{
		return calculateAngleFrom(obj1.getX(), obj1.getY(), obj2.getX(), obj2.getY());
	}

	/** Return degree value of object 2 to the horizontal line with object 1 being the origin */
	public static double calculateAngleFrom(final int obj1X, final int obj1Y, final int obj2X, final int obj2Y)
	{
		double angleTarget = Math.toDegrees(Math.atan2(obj1Y - obj2Y, obj1X - obj2X));
		if(angleTarget <= 0)
			angleTarget += 360;
		return angleTarget;
	}

	public static boolean checkIfInRange(final double range, final int x1, final int y1, final int x2, final int y2)
	{
		return checkIfInRange(range, x1, y1, 0, x2, y2, 0, false);
	}

	public static boolean checkIfInRange(final double range, final int x1, final int y1, final int z1, final int x2, final int y2, final int z2, final boolean includeZAxis)
	{
		final double dx = (double) x1 - x2;
		final double dy = (double) y1 - y2;
		final double d;

		if(includeZAxis)
		{
			final double dz = (double) z1 - z2;
			d = dx * dx + dy * dy + dz * dz;
		}
		else
			d = dx * dx + dy * dy;

		return d <= range * range;
	}

	public static boolean checkIfInRange(final double range, final L2Object obj1, final L2Object obj2, final boolean includeZAxis)
	{
		if(obj1 == null || obj2 == null)
			return false;

		return checkIfInRange(range, obj1.getX(), obj1.getY(), obj1.getZ(), obj2.getX(), obj2.getY(), obj2.getZ(), includeZAxis);
	}

	public static double convertHeadingToDegree(final int heading)
	{
		double angle = heading / 182.044444444;
		if(angle == 0)
			angle = 360;
		return angle;
	}

	public static double convertHeadingToRadian(final int heading)
	{
		return Math.toRadians(convertHeadingToDegree(heading) - 90);
	}

	public static double calculateDistance(final int x1, final int y1, final int z1, final int x2, final int y2)
	{
		return calculateDistance(x1, y1, 0, x2, y2, 0, false);
	}

	public static double calculateDistance(final int x1, final int y1, final int z1, final int x2, final int y2, final int z2, final boolean includeZAxis)
	{
		final double dx = x1 - x2;
		final double dy = y1 - y2;

		if(includeZAxis)
		{
			final int dz = z1 - z2;
			return Math.sqrt(dx * dx + dy * dy + dz * dz);
		}
		return Math.sqrt(dx * dx + dy * dy);
	}

	public static double calculateDistance(final L2Object obj1, final L2Object obj2, final boolean includeZAxis)
	{
		if(obj1 == null || obj2 == null)
			return 1000000;
		return calculateDistance(obj1.getX(), obj1.getY(), obj1.getZ(), obj2.getX(), obj2.getY(), obj2.getZ(), includeZAxis);
	}


	public static boolean isIntersectHorizontal(final Location p1, final Location p2, final Location intersection, final int x1, final int x2)
	{
		// ���� ������� ������� ����� � ����� ������� �� �����������
		if((p1.y - intersection.y) * (p2.y - intersection.y) > 0)
			return false;

		// �-���������� ����� ����������� ��������
		// ���������� � long ��� ����, ����� � �������� ������� ��� ��������� �� ���� ������������
		intersection.x = p2.x - (intersection.y - p2.y) * (p2.x - p1.x) / (p1.y - p2.y);

		// ����� ����������� ������ ������ �������� ����� ��������
		return ((long) intersection.x - p1.x) * (intersection.x - p2.x) <= 0 && ((long) intersection.x - x1) * (intersection.x - x2) <= 0;
	}

	public static boolean isIntersectVertical(final Location p1, final Location p2, final Location intersection, final int y1, final int y2)
	{
		if((p1.x - intersection.x) * (p2.x - intersection.x) > 0)
			return false;
		intersection.y = p2.y - (intersection.x - p2.x) * (p2.y - p1.y) / (p1.x - p2.x);
		return ((long) intersection.y - p1.y) * (intersection.y - p2.y) <= 0 && ((long) intersection.y - y1) * (intersection.y - y2) <= 0;
	}

	public static int getIntersectionX(final Location p1, final Location p2, final int ys, final int x1, final int x2)
	{
		return p2.x - (ys - p2.y) * (p2.x - p1.x) / (p1.y - p2.y);
	}

	public static int getIntersectionY(final Location p1, final Location p2, final int xs, final int y1, final int y2)
	{
		return p2.y - (xs - p2.x) * (p2.y - p1.y) / (p1.x - p2.x);
	}

	public static double getDistance(final int x1, final int y1, final int x2, final int y2)
	{
		return Math.hypot(x1 - x2, y1 - y2);
	}

	/**
	 * Return amount of adena formatted with " " delimiter
	 * 
	 * @param l
	 * @return String formatted adena amount
	 */
	public static String formatAdena(final long l)
	{
		return adenaFormatter.format(l);
	}

	public static int getPacketLength(final byte first, final byte second)
	{
		int lenght = first & 0xff;
		return lenght |= second << 8 & 0xff00;
	}


	public static byte[] writeLenght(final byte[] data)
	{
		final int newLenght = data.length + 2;
		final byte[] result = new byte[newLenght];
		result[0] = (byte) (newLenght & 0xFF);
		result[1] = (byte) (newLenght >> 8 & 0xFF);
		System.arraycopy(data, 0, result, 2, data.length);
		return result;
	}


	public static String formatTime(long time)
	{
		String ret = "";
		final long numDays = time / 86400;
		time -= numDays * 86400;
		final long numHours = time / 3600;
		time -= numHours * 3600;
		final long numMins = time / 60;
		time -= numMins * 60;
		final long numSeconds = time;
		if(numDays > 0)
			ret += numDays + "d ";
		if(numHours > 0)
			ret += numHours + "h ";
		if(numMins > 0)
			ret += numMins + "m ";
		if(numSeconds > 0)
			ret += numSeconds + "s";
		return ret.trim();
	}

	public static long rollDrop(final long min, final long max, double calcChance, final boolean rate)
	{
		if(calcChance <= 0.0D || min <= 0 || max <= 0)
			return 0;
		int dropmult = 1;
		if(rate)
			calcChance *= Config.RATE_DROP_ITEMS;
		if(calcChance > L2Drop.MAX_CHANCE)
			if(calcChance % L2Drop.MAX_CHANCE == 0)
				dropmult = (int) (calcChance / L2Drop.MAX_CHANCE);
			else
			{
				dropmult = (int) Math.ceil(calcChance / L2Drop.MAX_CHANCE);
				calcChance /= dropmult;
			}
		return Rnd.chance(calcChance / 10000D) ? Rnd.get(min * dropmult, max * dropmult) : 0;
	}

	public static int GetCharIDbyName(final String name)
	{
		int res = 0;
		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT obj_Id FROM characters WHERE char_name=? LIMIT 1");
			statement.setString(1, name);
			rset = statement.executeQuery();
			if(rset.next())
				res = rset.getInt("obj_Id");
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return res;
	}

	public static String GetCharNameById(final int charId)
	{
		String name = null;

		ThreadConnection con = null;
		FiltredPreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name FROM characters WHERE obj_Id = ? LIMIT 1");
			statement.setInt(1, charId);
			rset = statement.executeQuery();
			if(rset.next())
				name = rset.getString("char_name");
		}
		catch(final Exception e)
		{}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return name;
	}

	public static int packInt(final int[] a, final int bits) throws Exception
	{
		final int m = 32 / bits;
		if(a.length > m)
			throw new Exception("Overflow");

		int result = 0;

		final int mval = (int) Math.pow(2.0D, bits);
		for(int i = 0; i < m; ++i)
		{
			result = result << bits;
			if(a.length > i)
			{
				final int next = a[i];
				if(next < mval && next >= 0)
					break;
				throw new Exception("Overload, value is out of range");
			}

			final int next = 0;
			result = result + next;
		}
		return result;
	}

	public static long packLong(final int[] a, final int bits) throws Exception
	{
		final int m = 64 / bits;
		if(a.length > m)
			throw new Exception("Overflow");

		long result = 0L;

		final int mval = (int) Math.pow(2.0D, bits);

		for(int i = 0; i < m; ++i)
		{
			result = result << bits;
			if(a.length > i)
			{
				final long next = a[i];
				if(next < mval && next >= 0)
					break;
				throw new Exception("Overload, value is out of range");
			}

			final int next = 0;
			result = result + next;
		}
		return result;
	}

	public static int[] unpackInt(int a, final int bits)
	{
		final int m = 32 / bits;
		final int mval = (int) Math.pow(2.0D, bits);
		final int[] result = new int[m];

		for(int i = m; i > 0; --i)
		{
			final int next = a;
			a = a >> bits;
			result[i - 1] = next - a * mval;
		}
		return result;
	}

	public static int[] unpackLong(long a, final int bits)
	{
		final int m = 64 / bits;
		final int mval = (int) Math.pow(2.0D, bits);
		final int[] result = new int[m];

		for(int i = m; i > 0; --i)
		{
			final long next = a;
			a = a >> bits;
			result[i - 1] = (int) (next - a * mval);
		}
		return result;
	}


	public static void printSection(String s)
	{
		s = "={ " + s + " }=";

		for(int i = 0; i < 10; i++)
			s = "-" + s + "--";

		System.out.print(s + "\n");
	}


	public static String printCM(final CustomMessage message)
	{
		return message.toString();
	}


	public static boolean isNumber(final String s)
	{
		try
		{
			Double.parseDouble(s);
		}
		catch(final NumberFormatException e)
		{
			return false;
		}
		return true;
	}

	public static long gc(int count, final int delay)
	{
		final long freeMemBefore = Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
		final Runtime rt = Runtime.getRuntime();
		rt.gc();

		while (count-- > 0)
		{
			try
			{
				Thread.sleep(delay);
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}
			rt.gc();
		}

		rt.runFinalization();
		final long freeMem = Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory();
		return freeMem - freeMemBefore;
	}

	public static String[] getMemoryUsageStatistics()
	{
		final long max = Runtime.getRuntime().maxMemory(); // maxMemory is the upper limit the jvm can use
		final long allocated = Runtime.getRuntime().totalMemory(); // totalMemory the size of the current allocation pool
		final long nonAllocated = max - allocated; // non allocated memory till jvm limit
		final long cached = Runtime.getRuntime().freeMemory(); // freeMemory the unused memory in the allocation pool
		final long used = allocated - cached; // really used memory
		final long useable = max - used; // allocated, but non-used and non-allocated memory

		return new String[] {
				"Allowed Memory:" + formatBytes(max),
				"   |= Allocated Memory:" + formatBytes(allocated) + df.format(allocated / max * 100),
				"   |= Non-Allocated Memory:" + formatBytes(nonAllocated) + df.format(nonAllocated / max * 100),
				"Allocated Memory:" + formatBytes(allocated),
				"   |= Used Memory:" + formatBytes(used) + df.format(used / max * 100),
				"   |= Unused (cached) Memory:" + formatBytes(cached) + df.format(cached / max * 100),
				"Useable Memory:" + formatBytes(useable) + df.format(useable / max * 100) };
	}

	public static String[] getCpuInfo()
	{
		return new String[] { "Avaible CPU(s): " + Runtime.getRuntime().availableProcessors(), "CPU Identifier: " + System.getenv("PROCESSOR_IDENTIFIER") };
	}

	public static String getOSInfo()
	{
		return "Operating System: " + System.getProperty("os.name") + " Build: " + System.getProperty("os.version") + ", arch: " + System.getProperty("os.arch");
	}

	public static String getJavaInfo()
	{
		return "JMV: " + System.getProperty("java.vm.name") + " Build: " + System.getProperty("java.runtime.version");
	}

	public static int getHeadingTo(final L2Object actor, final L2Object target)
	{
		if(actor == null || target == null || target == actor)
			return -1;
		return getHeadingTo(actor.getLoc(), target.getLoc());
	}

	public static int getHeadingTo(final Location actor, final Location target)
	{
		if(actor == null || target == null || target.equals(actor))
			return -1;

		final int dx = target.x - actor.x;
		final int dy = target.y - actor.y;
		int heading = target.h - (int) (Math.atan2(-dy, -dx) * 32768. / Math.PI + 32768);

		if(heading < 0)
			heading = heading + 1 + Integer.MAX_VALUE & 0xFFFF;
		else if(heading > 65535)
			heading &= 65535;
		return heading;
	}

	public static final String randomPassword(final int len)
	{
		final char[] pw = new char[len];
		for(int i = len; i-- > 0;)
		{
			final char[] charset = CHARSET[Rnd.get(3)];
			pw[i] = charset[Rnd.get(charset.length)];
		}
		return new String(pw);
	}

	public static void waitForFreePorts(final String host, final int... ports)
	{
		boolean binded = false;
		while (!binded)
			for(final int port : ports)
				try
				{
					final ServerSocket ss = host.equalsIgnoreCase("*") ? new ServerSocket(port) : new ServerSocket(port, 50, InetAddress.getByName(host));
					_log.fine("Port " + port + " binded successfully.");
					ss.close();
					binded = true;
				}
				catch(final Exception e)
				{
					_log.warning("\nPort " + port + " is allready binded. Please free it and restart server.");
					binded = false;
					try
					{
						Thread.sleep(3000);
					}
					catch(final InterruptedException e2)
					{}
				}
	}

	/**
	 * Format bytes into a string to a rounded string representation.
	 * 
	 * @param bytes
	 *            Bytes.
	 * @return
	 *         Rounded string representation of the byte size.
	 */
	public static String formatBytes(final long bytes)
	{
		if(bytes == 1)
			return String.format("%d byte", bytes);
		else if(bytes < 1024)
			return String.format("%d bytes", bytes);
		else if(bytes < 1048576 && bytes % 1024 == 0)
			return String.format("%.0f KB", (double) bytes / 1024);
		else if(bytes < 1048576)
			return String.format("%.1f KB", (double) bytes / 1024);
		else if(bytes % 1048576 == 0 && bytes < 1073741824)
			return String.format("%.0f MB", (double) bytes / 1048576);
		else if(bytes < 1073741824)
			return String.format("%.1f MB", (double) bytes / 1048576);
		else if(bytes % 1073741824 == 0 && bytes < 1099511627776L)
			return String.format("%.0f GB", (double) bytes / 1073741824);
		else if(bytes < 1099511627776L)
			return String.format("%.1f GB", (double) bytes / 1073741824);
		else if(bytes % 1099511627776L == 0 && bytes < 1125899906842624L)
			return String.format("%.0f TB", (double) bytes / 1099511627776L);
		else if(bytes < 1125899906842624L)
			return String.format("%.1f TB", (double) bytes / 1099511627776L);
		else
			return String.format("%d bytes", bytes);
	}

	public static L2Skill[] parseSkills(final String property)
	{
		if(property == null)
			return SkillTable.EMPTY_ARRAY;
		try
		{
			if(property.trim().equals(""))
				return SkillTable.EMPTY_ARRAY;

			final String[] firstSplit = property.split(";");
			final L2Skill[] skills = new L2Skill[firstSplit.length];
			String[] secondSplit = null;

			for(int i = 0; i < skills.length; i++)
			{
				secondSplit = firstSplit[i].split("-");
				skills[i] = SkillTable.getInstance().getInfo(Integer.valueOf(secondSplit[0]), Integer.valueOf(secondSplit[1]));
			}
			return skills;
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "Util: parseSkills error " + e.getMessage(), e);
			return SkillTable.EMPTY_ARRAY;
		}
	}

	/**
	 * ���������� � ���� ����� ������� ������� ����� ���������/���������
	 */
	public static void dumpTranlateQuest()
	{
		final File workingDir = new File("./data/scripts/quests/");
		for(final File quest : workingDir.listFiles())
		{
			int eng_cont = 0, rus_count = 0;
			if(quest.isDirectory())
			{
				final File rus_dir = new File("./data/scripts/quests/" + quest.getName() + "/ru/");
				if(rus_dir != null && rus_dir.exists())
				{
					for(final File f : rus_dir.listFiles())
						if(f.getName().endsWith(".htm"))
							rus_count++;

					if(rus_count == 0)
					{
						Log.add(quest.getName(), "translate_quest", false);
						continue;
					}
				}
				else
				{
					Log.add(quest.getName(), "translate_quest", false);
					continue;
				}

				for(final File f : quest.listFiles())
					if(f.getName().endsWith(".htm"))
						eng_cont++;

				if(eng_cont != rus_count)
					Log.add(quest.getName(), "check_quest", false);
			}
		}
	}

	public static final void deleteFile(final String filename)
	{
		FileUtils.deleteQuietly(new File(filename));
	}

	private static String formatStackTraceElement(final StackTraceElement ste)
	{
		return ste.getClassName() + "." + ste.getMethodName() + (ste.isNativeMethod() ? "(Native Method)" : ste.getFileName() != null && ste.getLineNumber() >= 0 ? "(" + ste.getFileName() + ":" + ste.getLineNumber() + ")" : ste.getFileName() != null ? "(" + ste.getFileName() + ")" : "(Unknown Source)");
	}

	public static void printStackTrace(final StringBuilder builder, final Throwable t)
	{
		final StackTraceElement[] trace = t.getStackTrace();
		for(int ctr = 1; ctr < trace.length; ctr++)
		{
			final StackTraceElement ste = trace[ctr];
			if(ste.getClassName().startsWith("l2n.extensions.network") || ste.getClassName().startsWith("l2n.util.concurrent") || ste.getClassName().contains("L2GameClient") && ste.getMethodName().contains("executePacket"))
				continue;
			StringUtil.append(builder, "     ", formatStackTraceElement(ste), "\n");
		}
	}
}
