package l2n.util;

import l2n.commons.threading.ThreadPoolFactory.NextLinkedBlockingQueue;
import l2n.commons.threading.ThreadPoolFactory.NextPriorityThreadFactory;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RunnableScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public final class ThreadUtil
{
	public static final Logger _log = Logger.getLogger(ThreadUtil.class.getName());

	public static final String[] getStats(final ThreadPoolExecutor tpe, final boolean full)
	{
		tpe.purge();
		final NextPriorityThreadFactory ptf = (NextPriorityThreadFactory) tpe.getThreadFactory();
		final ThreadGroup tg = ptf.getThreadGroup();

		final StringBuilder stats = new StringBuilder(512);
		stats.append(" + Name............... ").append(tg.getName()).append(" [").append(tpe.getPoolSize()).append("/").append(tpe.getLargestPoolSize()).append("]\n");
		stats.append(" | Completed Tasks.... ").append(tpe.getCompletedTaskCount()).append("\n");
		stats.append(" | Tasks Count........ ").append(tpe.getTaskCount()).append("\n");
		stats.append(" | Created Tasks...... ").append(ptf.getCreatedThreadsCount()).append("\n");
		stats.append(" | Scheduled Tasks.... ").append(tpe.getQueue().size()).append("\n");

		final BlockingQueue<Runnable> queue = tpe.getQueue();
		if(queue instanceof NextLinkedBlockingQueue)
			stats.append(" | MaxQueueSize....... ").append(((NextLinkedBlockingQueue) queue).getMaxSize()).append("\n");

		if(full)
		{
			Thread[] activeThreads = new Thread[tg.activeCount() * 2];
			activeThreads = Arrays.copyOf(activeThreads, tg.enumerate(activeThreads));

			stats.append(" | ActiveThreads...... ").append(activeThreads.length).append("\n");
			for(final Thread thread : activeThreads)
			{
				if(thread == null)
					continue;

				final StackTraceElement[] stes = thread.getStackTrace();

				stats.append(" | + Thread name:       ").append(thread.getName()).append(", id: ").append(thread.getId()).append("\n");
				stats.append(" | - Alive:             ").append(thread.isAlive()).append(", interrupted: ").append(thread.isInterrupted()).append("\n");
				stats.append(" | - Stacktrace: length ").append(stes.length).append("\n").append("\n");

				if(stes.length == 0 || stes[0] == null || stes[0].toString().contains("sun.misc.Unsafe.park") || stes[0].toString().contains("sun.misc.Unsafe.$$YJP$$park"))
				{
					stats.append(" | | -------").append("\n");
					continue;
				}
				for(final StackTraceElement ste : stes)
					stats.append(" | |   ").append(ste).append("\n");
				stats.append(" | | -------").append("\n");
			}
		}
		else
			stats.append(" | ActiveThreads...... ").append(tg.activeCount()).append("\n");

		stats.append(" | -------");
		return stats.toString().split("\n");
	}

	public static final String dumpThreadQueue(final ThreadPoolExecutor tpe)
	{
		tpe.purge();
		final NextPriorityThreadFactory ptf = (NextPriorityThreadFactory) tpe.getThreadFactory();
		final ThreadGroup tg = ptf.getThreadGroup();

		final BlockingQueue<Runnable> queue = tpe.getQueue();
		final long time = System.currentTimeMillis();
		int count = 0;
		if(queue.size() > 0)
		{
			RunnableScheduledFuture<?> task;
			for(final Runnable r : queue)
				if(r instanceof RunnableScheduledFuture)
				{
					task = (RunnableScheduledFuture<?>) r;
					Log.addDev("[" + ++count + "] info: [" + getRunnableClass(r) + "] delay=" + task.getDelay(TimeUnit.MILLISECONDS) + " ms periodic=" + task.isPeriodic(), "dev_dumped_" + time + "_" + tg.getName(), false);
				}
		}
		return tg.getName() + ": dumped " + count + " tasks for " + (System.currentTimeMillis() - time) + " ms.";
	}

	public static String getRunnableClass(final Runnable r)
	{
		return r.toString();
	}
}
