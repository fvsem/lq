package l2n.util;

import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.network.serverpackets.L2GameServerPacket;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.MagicSkillUse;

public final class SkillUtil
{
	public static boolean notShowPacket(final L2GameServerPacket packet)
	{
		if(packet instanceof MagicSkillLaunched)
			return !((MagicSkillLaunched) packet).isOffensive();
		if(packet instanceof MagicSkillUse)
		{
			final int id = ((MagicSkillUse) packet).getSkillId();
			return id == 2061 || id == 2160 || id == 2161 || id == 2162 || id == 2163 || id == 2164 || id == 2033 || id == 2008 || id == 2009 || id == 2039 || id == 2150 || id == 2151 || id == 2152 || id == 2153 || id == 2154;
		}
		return false;
	}

	public static void broadcastUseAnimation(final L2Skill skill, final L2Character user, final L2Character... targets)
	{
		if(targets == null || targets.length == 0)
			return;

		int displayId = 0;
		int displayLevel = 0;

		if(skill.getEffectTemplates() != null)
		{
			displayId = skill.getEffectTemplates()[0]._displayId;
			displayLevel = skill.getEffectTemplates()[0]._displayLevel;
		}

		if(displayId == 0)
			displayId = skill.getDisplayId();
		if(displayLevel == 0)
			displayLevel = skill.getDisplayLevel();

		user.broadcastPacket(new MagicSkillUse(user, targets[0], displayId, displayLevel, 10, 0), new MagicSkillLaunched(user.getObjectId(), displayId, displayLevel, skill.isOffensive(), targets));
	}
}
