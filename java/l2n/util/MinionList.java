package l2n.util;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2MinionData;
import l2n.game.model.instances.L2MinionInstance;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

public class MinionList
{
	private static Logger _log = Logger.getLogger(L2MonsterInstance.class.getName());

	private final ConcurrentLinkedQueue<L2MinionInstance> minionReferences;
	private final L2MonsterInstance master;

	public MinionList(L2MonsterInstance master)
	{
		minionReferences = new ConcurrentLinkedQueue<L2MinionInstance>();
		this.master = master;
	}

	public int countSpawnedMinions()
	{
		synchronized (minionReferences)
		{
			return minionReferences.size();
		}
	}

	public byte countSpawnedMinionsById(int minionId)
	{
		byte count = 0;
		synchronized (minionReferences)
		{
			for(L2MinionInstance minion : getSpawnedMinions())
				if(minion.getNpcId() == minionId)
					count++;
		}
		return count;
	}

	public boolean hasMinions()
	{
		return getSpawnedMinions().size() > 0;
	}

	public L2MonsterInstance getMaster()
	{
		return master;
	}

	public ConcurrentLinkedQueue<L2MinionInstance> getSpawnedMinions()
	{
		return minionReferences;
	}

	public void addSpawnedMinion(L2MinionInstance minion)
	{
		synchronized (minionReferences)
		{
			minionReferences.add(minion);
		}
	}

	public int lazyCountSpawnedMinionsGroups()
	{
		GArray<Integer> seenGroups = new GArray<Integer>();
		for(L2MinionInstance minion : getSpawnedMinions())
			seenGroups.add(minion.getNpcId());
		return seenGroups.size();
	}

	public void removeSpawnedMinion(L2MinionInstance minion)
	{
		synchronized (minionReferences)
		{
			minionReferences.remove(minion);
		}
	}

	public void maintainMinions()
	{
		GArray<L2MinionData> minions = master.getTemplate().getMinionData();

		synchronized (minionReferences)
		{
			for(L2MinionData minion : minions)
			{
				int minionCount = minion.getAmount();
				int minionId = minion.getMinionId();

				for(L2MinionInstance m : minionReferences)
					if(m.getNpcId() == minionId)
						minionCount--;

				for(int i = 0; i < minionCount; i++)
					spawnSingleMinion(minionId);
			}
		}
	}
	
	public void maintainLonelyMinions()
	{
		synchronized (minionReferences)
		{
			for(L2MinionInstance minion : getSpawnedMinions())
				if(!minion.isDead())
				{
					removeSpawnedMinion(minion);
					minion.deleteMe();
				}
		}
	}

	public void spawnSingleMinion(int minionid)
	{

		L2NpcTemplate minionTemplate = NpcTable.getTemplate(minionid);

		L2MinionInstance monster = new L2MinionInstance(IdFactory.getInstance().getNextId(), minionTemplate, master);

		monster.setReflection(master.getReflection());

		monster.setCurrentHpMp(monster.getMaxHp(), monster.getMaxMp());
		monster.setHeading(master.getHeading());

		addSpawnedMinion(monster);

		monster.spawnMe(master.getMinionPosition());

		if(Config.DEBUG)
			_log.finest("Spawned minion template " + minionTemplate.npcId + " with objid: " + monster.getObjectId() + " to boss " + master.getObjectId() + " ,at: " + monster.getX() + " x, " + monster.getY() + " y, " + monster.getZ() + " z");
	}

	public void spawnSingleMinionSync(int minionid)
	{
		synchronized (minionReferences)
		{
			spawnSingleMinion(minionid);
		}
	}
}
