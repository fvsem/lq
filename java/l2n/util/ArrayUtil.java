package l2n.util;

import l2n.extensions.listeners.StatsChangeListener;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Skill.AddedSkill;
import l2n.game.model.L2SkillLearn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.base.L2EnchantSkillLearn;
import l2n.game.skills.conditions.Condition;
import l2n.game.skills.effects.EffectTemplate;
import l2n.game.skills.funcs.Func;
import l2n.game.skills.funcs.FuncTemplate;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

public class ArrayUtil
{
	public static final L2Skill[] EMPTY_SKILLS_ARRAY = new L2Skill[0];
	public static final L2Skill[][] EMPTY_SKILLS_ARRAY_2D = new L2Skill[0][];
	public static final AddedSkill[] EMPTY_ADDED_SKILLS = new AddedSkill[0];
	public static final L2Character[] EMPTY_TARGETS = new L2Character[0];

	public final static L2Object[] EMPTY_L2OBJECT_ARRAY = new L2Object[0];

	public static final Func[] EMPTY_FUNCTION_SET = new Func[0];
	public static final FuncTemplate[] EMPTY_FUNCTION_TEMPLATE = new FuncTemplate[0];
	public static final StatsChangeListener[] EMPTY_LISTENERS = new StatsChangeListener[0];

	public static final L2Effect[] EMPTY_EFFECT_ARRAY = new L2Effect[0];
	public static final EffectTemplate[] EMPTY_EFFECT_TEMPLATE_ARRAY = new EffectTemplate[0];
	public static final Condition[] EMPTY_CONDITION_ARRAY = new Condition[0];

	public static final L2SkillLearn[] EMPTY_SKILL_LEARN = new L2SkillLearn[0];
	public static final L2EnchantSkillLearn[] EMPTY_ENCHANT_SKILL_LEARN = new L2EnchantSkillLearn[0];

	public static final int[] EMPTY_INT_ARRAY = new int[0];
	public static final long[] EMPTY_LONG_ARRAY = new long[0];
	public static final float[] EMPTY_FLOAT_ARRAY = new float[0];
	public static final double[] EMPTY_DOUBLE_ARRAY = new double[0];
	public static final String[] EMPTY_STRING_ARRAY = new String[0];

	public static final byte[] arrayAdd(byte[] array, final byte value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final char[] arrayAdd(char[] array, final char value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final double[] arrayAdd(double[] array, final double value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final float[] arrayAdd(float[] array, final float value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final int[] arrayAdd(int[] array, final int value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final long[] arrayAdd(long[] array, final long value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final short[] arrayAdd(short[] array, final short value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static final <T> T[] arrayAdd(T[] array, final T value)
	{
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	@SuppressWarnings("unchecked")
	public static final <T> T[] arrayAdd(T[] array, final T value, final Class<?> c)
	{
		if(array == null)
		{
			array = (T[]) Array.newInstance(c, 1);
			array[0] = value;
			return array;
		}
		final int size = array.length;
		array = Arrays.copyOf(array, size + 1);
		array[size] = value;
		return array;
	}

	public static <T> T[] arrayAddToArray(final T[] arr, final T[] o, final Class<?> c)
	{
		T[] tmp = arr;
		for(final T t : o)
			tmp = arrayAdd(tmp, t, c);
		return tmp;
	}

	public static final byte[] arrayAddIfAbsent(byte[] array, final byte value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final char[] arrayAddIfAbsent(char[] array, final char value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final double[] arrayAddIfAbsent(double[] array, final double value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final float[] arrayAddIfAbsent(float[] array, final float value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final int[] arrayAddIfAbsent(int[] array, final int value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final long[] arrayAddIfAbsent(long[] array, final long value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final short[] arrayAddIfAbsent(short[] array, final short value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final void arraySwap(final int[] array, final int index1, final int index2)
	{
		final int swap = array[index1];
		array[index1] = array[index2];
		array[index2] = swap;
	}

	public static final <T> T[] arrayAddIfAbsent(T[] array, final T value)
	{
		if(!ArrayUtil.arrayContains(array, value))
			return ArrayUtil.arrayAdd(array, value);
		return array;
	}

	public static final boolean arrayContains(final Object array, final Object value)
	{
		return arrayLastIndexOf(array, value) != -1;
	}

	public static final <T> boolean arrayContains(final T[] array, final T value)
	{
		return arrayLastIndexOf(array, value) != -1;
	}

	public static final boolean arrayContains(final int[] array, final int value)
	{
		return arrayFirstIndexOf(array, value) != -1;
	}

	public static final int arrayFirstIndexOf(final byte[] array, final byte value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final char[] array, final char value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final double[] array, final double value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final float[] array, final float value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final int[] array, final int value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final long[] array, final long value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final Object array, final Object value)
	{
		final int length = Array.getLength(array);
		if(value == null)
		{
			for(int i = 0; i < length; i++)
			{
				if(Array.get(array, i) == null)
					return i;
			}
		}
		else
		{
			for(int i = 0; i < length; i++)
			{
				final Object obj = Array.get(array, i);
				if(value == obj || value.equals(obj))
					return i;
			}
		}
		return -1;
	}

	public static final int arrayFirstIndexOf(final short[] array, final short value)
	{
		final int length = array.length;
		for(int i = 0; i < length; i++)
		{
			if(array[i] == value)
				return i;
		}
		return -1;
	}

	public static final <T> int arrayFirstIndexOf(final T[] array, final T value)
	{
		if(value == null)
		{
			for(int i = 0; i < array.length; i++)
			{
				if(array[i] == null)
					return i;
			}
		}
		else
		{
			for(int i = 0; i < array.length; i++)
			{
				if(value == array[i] || value.equals(array[i]))
					return i;
			}
		}
		return -1;
	}

	public static final int arrayIndexOf(final Object array, final Object value)
	{
		return arrayFirstIndexOf(array, value);
	}

	public static final <T> int arrayIndexOf(final T[] array, final T value)
	{
		return arrayFirstIndexOf(array, value);
	}

	public static final byte[] arrayInsert(byte[] array, final byte value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final byte[] temp = new byte[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final char[] arrayInsert(char[] array, final char value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final char[] temp = new char[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final double[] arrayInsert(double[] array, final double value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final double[] temp = new double[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final float[] arrayInsert(float[] array, final float value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final float[] temp = new float[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final int[] arrayInsert(int[] array, final int value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final int[] temp = new int[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final long[] arrayInsert(long[] array, final long value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final long[] temp = new long[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final short[] arrayInsert(short[] array, final short value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		final short[] temp = new short[size + 1];
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final <T> T[] arrayInsert(T[] array, final T value, final int pos)
	{
		final int size = array.length;
		if(pos < 0 || pos >= size)
			throw new IndexOutOfBoundsException("Pos " + pos + " < 0 or >= size");

		@SuppressWarnings("unchecked")
		final T[] temp = array.getClass() == Object[].class ? (T[]) new Object[size + 1] : (T[]) Array.newInstance(array.getClass().getComponentType(), size + 1);
		temp[pos] = value;

		if(pos != 0)
			System.arraycopy(array, 0, temp, 0, pos);
		System.arraycopy(array, pos, temp, pos + 1, size - pos);
		return temp;
	}

	public static final int arrayLastIndexOf(final Object array, final Object value)
	{
		final int length = Array.getLength(array);
		if(value == null)
		{
			for(int i = length; i-- > 0;)
			{
				if(Array.get(array, i) == null)
					return i;
			}
		}
		else
		{
			for(int i = length; i-- > 0;)
			{
				final Object obj = Array.get(array, i);
				if(value == obj || value.equals(obj))
					return i;
			}
		}
		return -1;
	}

	public static final <T> int arrayLastIndexOf(final T[] array, final T value)
	{
		if(value == null)
		{
			for(int i = array.length; i-- > 0;)
			{
				if(array[i] == null)
					return i;
			}
		}
		else
		{
			for(int i = array.length; i-- > 0;)
			{
				if(value == array[i] || value.equals(array[i]))
					return i;
			}
		}
		return -1;
	}

	public static final <T> T[] arrayRemoveAll(T[] array, final T value)
	{
		int index;
		while ((index = arrayLastIndexOf(array, value)) != -1)
		{
			array = arrayRemoveAtUnsafe(array, index);
		}
		return array;
	}

	public static final byte[] arrayRemoveAtUnsafe(final byte[] array, final int index)
	{
		final byte[] newArray = new byte[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final char[] arrayRemoveAtUnsafe(final char[] array, final int index)
	{
		final char[] newArray = new char[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final double[] arrayRemoveAtUnsafe(final double[] array, final int index)
	{
		final double[] newArray = new double[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final float[] arrayRemoveAtUnsafe(final float[] array, final int index)
	{
		final float[] newArray = new float[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final int[] arrayRemoveAtUnsafe(final int[] array, final int index)
	{
		final int[] newArray = new int[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final long[] arrayRemoveAtUnsafe(final long[] array, final int index)
	{
		final long[] newArray = new long[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final short[] arrayRemoveAtUnsafe(final short[] array, final int index)
	{
		final short[] newArray = new short[array.length - 1];
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final <T> T[] arrayRemoveAtUnsafe(final T[] array, final int index)
	{
		@SuppressWarnings("unchecked")
		final T[] newArray = array.getClass() == Object[].class ? (T[]) new Object[array.length - 1] : (T[]) Array.newInstance(array.getClass().getComponentType(), array.length - 1);
		if(index != 0)
			System.arraycopy(array, 0, newArray, 0, index);
		System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
		return newArray;
	}

	public static final byte[] arrayRemoveFirst(final byte[] array, final byte value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final char[] arrayRemoveFirst(final char[] array, final char value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final double[] arrayRemoveFirst(final double[] array, final double value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final float[] arrayRemoveFirst(final float[] array, final float value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final int[] arrayRemoveFirst(final int[] array, final int value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final long[] arrayRemoveFirst(final long[] array, final long value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final short[] arrayRemoveFirst(final short[] array, final short value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final <T> T[] arrayRemoveFirst(final T[] array, final T value)
	{
		final int index = arrayFirstIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);

		return array;
	}

	public static final <T> T[] arrayRemoveLast(final T[] array, final T value)
	{
		final int index = arrayLastIndexOf(array, value);
		if(index != -1)
			return arrayRemoveAtUnsafe(array, index);
		return array;
	}

	public static int[] toIntArray(final String string, final String delimiter)
	{
		final String[] strings = string.split(delimiter);
		if(strings.length <= 0)
			return new int[0];

		final int[] ints = new int[strings.length];
		for(int i = 0; i < strings.length; i++)
			ints[i] = Integer.parseInt(strings[i]);
		return ints;
	}

	public static double[] toDoubleArray(final String string, final String delimiter)
	{
		final String[] strings = string.split(delimiter);
		if(strings.length <= 0)
			return new double[0];

		final double[] ints = new double[strings.length];
		for(int i = 0; i < strings.length; i++)
			ints[i] = Double.parseDouble(strings[i]);
		return ints;
	}

	public static float[] toFloatArray(final String string, final String delimiter)
	{
		final String[] strings = string.split(delimiter);
		if(strings.length <= 0)
			return new float[0];

		final float[] floats = new float[strings.length];
		for(int i = 0; i < strings.length; i++)
			floats[i] = Float.parseFloat(strings[i]);
		return floats;
	}

	public static int[] getIntArrayRange(final int start, final int end)
	{
		return getIntArrayRange(start, end, 1);
	}

	public static int[] getIntArrayRange(final int start, final int end, final int step)
	{
		final int length = (int) Math.floor((double) (end - start) / (double) step + 1.);
		final int[] array = new int[length];
		for(int i = 0; i < length; i++)
		{
			final int element = i == 0 ? start : array[i - 1] + step;
			array[i] = element;
		}

		return array;
	}

	public static final int getC(final byte[] array, final int pos)
	{
		return array[pos] & 0x000000FF;
	}

	public static final int getD(final byte[] array, final int pos)
	{
		return array[pos] & 0x000000FF | array[pos + 1] << 8 & 0x0000FF00 | array[pos + 2] << 16 & 0x00FF0000 | array[pos + 3] << 24 & 0xFF000000;
	}

	public static final double getF(final byte[] array, final int pos)
	{
		return Double.longBitsToDouble(getQ(array, pos));
	}

	public static final int getH(final byte[] array, final int pos)
	{
		return array[pos] & 0x000000FF | array[pos + 1] << 8 & 0x0000FF00;
	}

	public static final long getQ(final byte[] array, final int pos)
	{
		return getD(array, pos) & 0xFFFFFFFFL | (getD(array, pos + 4) & 0xFFFFFFFFL) << 32;
	}

	public static final void putC(final byte[] array, final int pos, final int value)
	{
		array[pos] = (byte) (value & 0x000000FF);
	}

	public static final void putD(final byte[] array, final int pos, final int value)
	{
		array[pos] = (byte) (value & 0x000000FF);
		array[pos + 1] = (byte) (value >> 8 & 0x000000FF);
		array[pos + 2] = (byte) (value >> 16 & 0x000000FF);
		array[pos + 3] = (byte) (value >> 24 & 0x000000FF);
	}

	public static final void putF(final byte[] array, final int pos, final double value)
	{
		putQ(array, pos, Double.doubleToRawLongBits(value));
	}

	public static final void putH(final byte[] array, final int pos, final int value)
	{
		array[pos] = (byte) (value & 0x000000FF);
		array[pos + 1] = (byte) (value >> 8 & 0x000000FF);
	}

	public static final void putQ(final byte[] array, final int pos, final long value)
	{
		array[pos] = (byte) (value & 0x000000FF);
		array[pos + 1] = (byte) (value >> 8 & 0x000000FF);
		array[pos + 2] = (byte) (value >> 16 & 0x000000FF);
		array[pos + 3] = (byte) (value >> 24 & 0x000000FF);
		array[pos + 4] = (byte) (value >> 32 & 0x000000FF);
		array[pos + 5] = (byte) (value >> 40 & 0x000000FF);
		array[pos + 6] = (byte) (value >> 48 & 0x000000FF);
		array[pos + 7] = (byte) (value >> 56 & 0x000000FF);
	}

	private static <T extends Comparable<T>> void eqBrute(final T[] a, final int lo, final int hi)
	{
		if(hi - lo == 1)
		{
			if(a[hi].compareTo(a[lo]) < 0)
			{
				final T e = a[lo];
				a[lo] = a[hi];
				a[hi] = e;
			}
		}
		else if(hi - lo == 2)
		{
			int pmin = a[lo].compareTo(a[lo + 1]) < 0 ? lo : lo + 1;
			pmin = a[pmin].compareTo(a[lo + 2]) < 0 ? pmin : lo + 2;
			if(pmin != lo)
			{
				final T e = a[lo];
				a[lo] = a[pmin];
				a[pmin] = e;
			}
			eqBrute(a, lo + 1, hi);
		}
		else if(hi - lo == 3)
		{
			int pmin = a[lo].compareTo(a[lo + 1]) < 0 ? lo : lo + 1;
			pmin = a[pmin].compareTo(a[lo + 2]) < 0 ? pmin : lo + 2;
			pmin = a[pmin].compareTo(a[lo + 3]) < 0 ? pmin : lo + 3;
			if(pmin != lo)
			{
				final T e = a[lo];
				a[lo] = a[pmin];
				a[pmin] = e;
			}
			int pmax = a[hi].compareTo(a[hi - 1]) > 0 ? hi : hi - 1;
			pmax = a[pmax].compareTo(a[hi - 2]) > 0 ? pmax : hi - 2;
			if(pmax != hi)
			{
				final T e = a[hi];
				a[hi] = a[pmax];
				a[pmax] = e;
			}
			eqBrute(a, lo + 1, hi - 1);
		}
	}

	private static <T extends Comparable<T>> void eqSort(final T[] a, final int lo0, final int hi0)
	{
		int lo = lo0;
		int hi = hi0;
		if(hi - lo <= 3)
		{
			eqBrute(a, lo, hi);
			return;
		}
		final T pivot = a[(lo + hi) / 2];
		a[(lo + hi) / 2] = a[hi];
		a[hi] = pivot;
		while (lo < hi)
		{
			while (a[lo].compareTo(pivot) <= 0 && lo < hi)
				lo++;
			while (pivot.compareTo(a[hi]) <= 0 && lo < hi)
				hi--;
			if(lo < hi)
			{
				final T e = a[lo];
				a[lo] = a[hi];
				a[hi] = e;
			}
		}
		a[hi0] = a[hi];
		a[hi] = pivot;
		eqSort(a, lo0, lo - 1);
		eqSort(a, hi + 1, hi0);
	}

	public static <T extends Comparable<T>> void eqSort(final T[] a)
	{
		eqSort(a, 0, a.length - 1);
	}

	private static <T> void eqBrute(final T[] a, final int lo, final int hi, final Comparator<T> c)
	{
		if(hi - lo == 1)
		{
			if(c.compare(a[hi], a[lo]) < 0)
			{
				final T e = a[lo];
				a[lo] = a[hi];
				a[hi] = e;
			}
		}
		else if(hi - lo == 2)
		{
			int pmin = c.compare(a[lo], a[lo + 1]) < 0 ? lo : lo + 1;
			pmin = c.compare(a[pmin], a[lo + 2]) < 0 ? pmin : lo + 2;
			if(pmin != lo)
			{
				final T e = a[lo];
				a[lo] = a[pmin];
				a[pmin] = e;
			}
			eqBrute(a, lo + 1, hi, c);
		}
		else if(hi - lo == 3)
		{
			int pmin = c.compare(a[lo], a[lo + 1]) < 0 ? lo : lo + 1;
			pmin = c.compare(a[pmin], a[lo + 2]) < 0 ? pmin : lo + 2;
			pmin = c.compare(a[pmin], a[lo + 3]) < 0 ? pmin : lo + 3;
			if(pmin != lo)
			{
				final T e = a[lo];
				a[lo] = a[pmin];
				a[pmin] = e;
			}
			int pmax = c.compare(a[hi], a[hi - 1]) > 0 ? hi : hi - 1;
			pmax = c.compare(a[pmax], a[hi - 2]) > 0 ? pmax : hi - 2;
			if(pmax != hi)
			{
				final T e = a[hi];
				a[hi] = a[pmax];
				a[pmax] = e;
			}
			eqBrute(a, lo + 1, hi - 1, c);
		}
	}

	private static <T> void eqSort(final T[] a, final int lo0, final int hi0, final Comparator<T> c)
	{
		int lo = lo0;
		int hi = hi0;
		if(hi - lo <= 3)
		{
			eqBrute(a, lo, hi, c);
			return;
		}
		final T pivot = a[(lo + hi) / 2];
		a[(lo + hi) / 2] = a[hi];
		a[hi] = pivot;
		while (lo < hi)
		{
			while (c.compare(a[lo], pivot) <= 0 && lo < hi)
				lo++;
			while (c.compare(pivot, a[hi]) <= 0 && lo < hi)
				hi--;
			if(lo < hi)
			{
				final T e = a[lo];
				a[lo] = a[hi];
				a[hi] = e;
			}
		}
		a[hi0] = a[hi];
		a[hi] = pivot;
		eqSort(a, lo0, lo - 1, c);
		eqSort(a, hi + 1, hi0, c);
	}

	public static <T> void eqSort(final T[] a, final Comparator<T> c)
	{
		eqSort(a, 0, a.length - 1, c);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> T[] add(final T[] array, final T element)
	{
		final Class type = array != null ? array.getClass().getComponentType() : element != null ? element.getClass() : Object.class;
		final T[] newArray = (T[]) copyArrayGrow(array, type);
		newArray[newArray.length - 1] = element;
		return newArray;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] remove(final T[] array, final T value)
	{
		if(array == null)
			return null;

		final int index = arrayIndexOf(array, value);
		if(index == -1)
			return array;

		final int length = array.length;

		final T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), length - 1);
		System.arraycopy(array, 0, newArray, 0, index);
		if(index < length - 1)
			System.arraycopy(array, index + 1, newArray, index, length - index - 1);

		return newArray;
	}

	@SuppressWarnings("unchecked")
	private static <T> T[] copyArrayGrow(final T[] array, final Class<? extends T> type)
	{
		if(array != null)
		{
			final int arrayLength = Array.getLength(array);
			final T[] newArray = (T[]) Array.newInstance(array.getClass().getComponentType(), arrayLength + 1);
			System.arraycopy(array, 0, newArray, 0, arrayLength);
			return newArray;
		}
		return (T[]) Array.newInstance(type, 1);
	}
}
