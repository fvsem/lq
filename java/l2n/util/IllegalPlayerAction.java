package l2n.util;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.GmListTable;

public final class IllegalPlayerAction implements Runnable
{
	private String etc_str1;
	private String etc_str2;
	private int isBug;
	private L2Player actor;

	public static final int INFO = 0;
	public static final int WARNING = 1;
	public static final int CRITICAL = 2;

	public IllegalPlayerAction(L2Player _actor, String _etc_str1, String _etc_str2, int _isBug)
	{
		etc_str1 = _etc_str1;
		etc_str2 = _etc_str2;
		isBug = _isBug;
		actor = _actor;
	}

	@Override
	public void run()
	{
		StringBuffer msgb = new StringBuffer(160);
		int punishment = -1;
		msgb.append("IllegalAction: " + (actor == null ? "actor null " : actor.toString()) + " " + etc_str1 + " " + etc_str2);
		Log.add("IllegalAction: " + (actor == null ? "actor null " : actor.toString()) + " " + etc_str1 + " " + etc_str2, "illegal_action");

		switch (isBug)
		{
			case INFO:
				punishment = 0;
				break;
			case WARNING:
				punishment = Config.DEFAULT_PUNISH;
				break;
			case CRITICAL:
				punishment = Config.BUGUSER_PUNISH;
				break;
		}

		if(actor.isGM())
			punishment = 0;

		switch (punishment)
		{
			case 0:
				msgb.append(" punish: none");
				actor.sendMessage(new CustomMessage("l2n.Util.IllegalAction.case0", actor));
				return;
			case 1:
				actor.sendMessage(new CustomMessage("l2n.Util.IllegalAction.case1", actor));
				try
				{
					Thread.sleep(1000);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				actor.logout(false, false, true, true);
				msgb.append(" punish: kicked");
				break;
			case 2:
				actor.sendMessage(new CustomMessage("l2n.Util.IllegalAction.case2", actor));
				actor.setAccessLevel(-100);
				actor.setAccountAccesslevel(-100, "Autoban: " + etc_str2 + " in " + etc_str1, -1);
				try
				{
					Thread.sleep(1000);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
				actor.logout(false, false, true, true);
				msgb.append(" punish: banned");
				Log.add(msgb.toString(), "banned");
				break;
		}
		GmListTable.broadcastMessageToGMs(msgb.toString());
	}
}
