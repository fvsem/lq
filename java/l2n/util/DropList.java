package l2n.util;

import l2n.Config;
import l2n.game.model.actor.L2Player;
import l2n.game.model.drop.L2Drop;
import l2n.game.model.drop.L2DropData;
import l2n.game.model.drop.L2DropGroup;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.tables.DynamicRateTable;
import l2n.game.templates.L2NpcTemplate;

import java.text.NumberFormat;
import java.util.List;

public class DropList
{
	private static NumberFormat df = NumberFormat.getPercentInstance();
	static
	{
		df.setMaximumFractionDigits(4);
	}

	public static String generateDroplist(L2NpcTemplate template, L2MonsterInstance monster, double mod, L2Player player)
	{
		StringBuilder tmp = new StringBuilder();
		tmp.append("<html><body><center><font color=\"LEVEL\">").append("ID: ").append(template.getNpcId()).append("</font>");
		tmp.append("<br><font color=\"00FF00\">Дроп лист</font>");
		tmp.append("</center><table><tr><td></td></tr>");
		boolean emptylist = false;
		boolean overlevel = true;
		boolean icons = player != null ? player.getVarB("DroplistIcons") : true;
		double rateAdena = mod * DynamicRateTable.getRateAdena(player) * (player != null ? player.getRateAdena() : 1);

		double rateDrop = 1;
		if(monster != null)
			rateDrop = mod * monster.getDropRate(player) * (player == null || Config.SERVICES_RATE_BONUS_DISABLE_EFFECT_ON_RB && monster.isRaid() ? 1 : player.getRateItems());
		else
			rateDrop = mod * (template.isRaid ? Config.RATE_DROP_RAIDBOSS : DynamicRateTable.getRateItems(player)) * (player != null ? player.getRateItems() : 1);

		double rateSpoil = mod * DynamicRateTable.getRateSpoil(player) * (player != null ? player.getRateSpoil() : 1);
		if(template.getDropData() != null)
		{
			if(template.getDropData().getNormal() != null)
				for(L2DropGroup g : template.getDropData().getNormal())
				{
					if(g.isAdena() && rateAdena == 0)
						continue;
					else if(!g.isAdena() && rateDrop == 0)
						continue;
					overlevel = false;

					List<L2DropData> items;
					double GCHANCE;
					double dropmult;
					double chancemult;

					if(g.notRate())
					{
						mod = Math.min(1, mod);
						GCHANCE = g.getChance() * mod;
						chancemult = mod;
						dropmult = 1;
						items = g.getDropItems(false);
					}
					else if(g.isAdena())
					{
						if(mod < 10)
						{
							GCHANCE = g.getChance();
							chancemult = 1;
							dropmult = rateAdena;
						}
						else
						{ // чамп
							chancemult = L2Drop.MAX_CHANCE / g.getChance();
							dropmult = rateAdena * g.getChance() / L2Drop.MAX_CHANCE;
							GCHANCE = L2Drop.MAX_CHANCE;
						}
						items = g.getDropItems(false);
					}
					else if(template.isRaid || monster != null && monster.getChampion() > 0 || g.fixedQty())
					{
						GCHANCE = g.getChance() * rateDrop;
						double[] balanced = L2DropGroup.balanceChanceAndMult(GCHANCE);
						chancemult = balanced[0] / g.getChance();
						GCHANCE = balanced[0];
						dropmult = balanced[1];
						items = g.getDropItems(false);
					}
					else
					{
						items = g.getRatedItems(rateDrop);
						dropmult = 1;
						chancemult = 1;
						GCHANCE = 0;
						for(L2DropData i : items)
							GCHANCE += i.getChance();
					}

					tmp.append("</table><br><center>Шанс Группы: ").append(df.format(GCHANCE / L2Drop.MAX_CHANCE));
					if(dropmult > 1 && !g.isAdena())
					{
						tmp.append(" x").append((int) dropmult);
						dropmult = 1;
					}
					tmp.append("</center><table width=100%>");

					for(L2DropData d : items)
					{
						String chance = df.format(d.getChance() * chancemult / L2Drop.MAX_CHANCE);
						if(icons)
						{
							tmp.append("<tr><td width=32><img src=icon.").append(d.getItem().getIcon()).append(" width=32 height=32></td><td width=200>").append(compact(d.getItem().getName())).append("<br1>[");
							tmp.append(Math.round(d.getMinDrop() * dropmult)).append("-").append(Math.round(d.getMaxDrop() * dropmult)).append("]    ");
							tmp.append(chance).append("</td></tr>");
						}
						else
						{
							tmp.append("<tr><td width=80%>").append(compact(d.getItem().getName())).append("</td><td width=10%>");
							tmp.append(Math.min(Math.round((d.getMinDrop() + d.getMaxDrop()) * dropmult / 2f), 9999999)).append("</td><td width=10%>");
							tmp.append(chance).append("</td></tr>");
						}
					}
				}

			if(template.getDropData().getSpoil() != null)
				if(template.getDropData().getSpoil().size() > 0)
					if(rateSpoil > 0)
					{
						overlevel = false;
						tmp.append("</table><center>Спойл:</center><table width=100%>");
						for(L2DropGroup g : template.getDropData().getSpoil())
							for(L2DropData d : g.getDropItems(false))
							{
								double[] e = L2DropGroup.balanceChanceAndMult(d.getChance() * rateSpoil);
								double GCHANCE = e[0] / 1000000;
								double dropmult = e[1];
								if(icons)
								{
									tmp.append("<tr><td width=32><img src=icon.").append(d.getItem().getIcon()).append(" width=32 height=32></td><td width=200>").append(compact(d.getItem().getName())).append("<br1>[");
									tmp.append(d.getMinDrop() * dropmult).append("-").append(d.getMaxDrop() * dropmult).append("]    ");
									tmp.append(df.format(GCHANCE)).append("</td></tr>");
								}
								else
								{
									double qty = (d.getMinDrop() + d.getMaxDrop()) * dropmult / 2.;
									tmp.append("<tr><td width=80%>").append(compact(d.getItem().getName())).append("</td><td width=10%>");
									tmp.append(Math.round(qty)).append("</td><td width=10%>");
									tmp.append(df.format(GCHANCE)).append("</td></tr>");
								}
							}
					}
		}
		else
		emptylist = true;

		tmp.append("</table>");
		if(emptylist)
			tmp.append("<center>Список выпадения пуст</center>");
		else if(overlevel)
			tmp.append("<center>Этот монстр слишком слаб для Вас!</center>");
		tmp.append("</body></html>");
		return tmp.toString();
	}

	public static String compact(String s)
	{
		return s.replaceFirst("Recipe:", "R:").replaceFirst("Common Item - ", "Common ").replaceFirst("Scroll: Enchant", "Enchant").replaceFirst("Compressed Package", "CP");
	}
}
