package l2n.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.regex.Pattern;

public class Calculator
{
	private static final ScriptEngine javaScriptEngine = new ScriptEngineManager().getEngineByName("JavaScript");

	private static final Pattern PATTERN = Pattern.compile("[0-9+-\\\\* \\\\\\\\()\\\\.]{1,}");

	public static double eval(String expr)
	{
		if(!isMatchingRexEg(expr))
		{
			return Double.NaN;
		}

		Object result = null;

		try
		{
			result = javaScriptEngine.eval(expr);
		}
		catch(ScriptException e)
		{
		
		}

		if(result instanceof Double)
		{
			return (Double) result;
		}
		else
		{
			return Double.NaN;
		}
	}

	private static boolean isMatchingRexEg(String expression)
	{
		return PATTERN.matcher(expression).matches();
	}
}
