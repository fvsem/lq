package l2n.util;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.game.model.actor.L2Player;

public class PremiumAccess
{
	public static enum CommunityBoardType
	{
		TELEPORT(1),
		BUFFER(2),
		SHOP(3),
		ENCHANT(4),
		CLASS(5);

		private final int _id;

		private CommunityBoardType(int id)
		{
			_id = id;
		}

		public long mask()
		{
			return 1L << _id;
		}
	}

	public static enum ServiceType
	{
		CLASS_MASTER(1),
		NICK_CHANGE(2),
		CLAN_NAME_CHANGE(3),
		CLAN_BUY_POINTS(4),
		CLAN_BUY_LEVEL(5),
		SEX_CHANGE(6),
		BASE_CHANGE(7),
		NICK_COLOR_CHANGE(8),
		NOBLESS_SELL(9),
		EXPAND_INVENTORY(10),
		EXPAND_WAREHOUSE(11),
		EXPAND_CWH(12),
		DELEVEL(13),
		NOKARMA_PK(14),
		NOKARMA_KARMA(15),
		OFFLINE_TRADE(16),
		LEVEL_UP(17);

		private final int _id;

		private ServiceType(int id)
		{
			_id = id;
		}

		public long mask()
		{
			return 1L << _id;
		}
	}

	public static boolean checkAccessService(L2Player activeChar, ServiceType type)
	{
		if((Config.SERVECES_PREMIUM_ACCESS & type.mask()) != 0 && !activeChar.isGM() && !activeChar.hasPremiumAccount())
		{
			sendPage(activeChar);
			return false;
		}
		return true;
	}

	public static boolean checkAccessCommunity(L2Player activeChar, CommunityBoardType type)
	{
		if((Config.COMMUNITY_BOARD_PREMIUM_ACCESS & type.mask()) != 0 && !activeChar.isGM() && !activeChar.hasPremiumAccount())
		{
			sendPage(activeChar);
			return false;
		}
		return true;
	}

	public static void sendPage(L2Player activeChar)
	{
		activeChar.sendMessage(new CustomMessage("common.PremiumOnly", activeChar));
		Functions.show(Files.read("data/html/common/premium_info.htm", activeChar), activeChar);
	}
}
