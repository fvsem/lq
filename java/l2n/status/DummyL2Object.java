package l2n.status;

import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Character;

public class DummyL2Object extends L2Object
{
	public DummyL2Object()
	{
		super(-1);
	}

	@Override
	public boolean isAutoAttackable(L2Character attacker)
	{
		return false;
	}
}
