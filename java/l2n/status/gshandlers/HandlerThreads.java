package l2n.status.gshandlers;

import l2n.game.L2GameThreadPools;
import l2n.util.Log;
import l2n.util.ThreadUtil;

import java.io.PrintWriter;
import java.lang.management.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HandlerThreads
{
	public static void ThreadDump(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
			_print.println("USAGE: dt packets(p)|general(g)|move(m)|npcAi|dump_allthreads");
		else if(argv[1].equalsIgnoreCase("packets") || argv[1].equalsIgnoreCase("p"))
			printStats(_print, ThreadUtil.dumpThreadQueue(L2GameThreadPools.getInstance().getInGamePacketsThreadPool()));
		else if(argv[1].equalsIgnoreCase("general") || argv[1].equalsIgnoreCase("g"))
			printStats(_print, ThreadUtil.dumpThreadQueue(L2GameThreadPools.getInstance().getGeneralScheduledThreadPool()));
		else if(argv[1].equalsIgnoreCase("move") || argv[1].equalsIgnoreCase("m"))
			printStats(_print, ThreadUtil.dumpThreadQueue(L2GameThreadPools.getInstance().getMoveScheduledThreadPool()));
		else if(argv[1].equalsIgnoreCase("ai"))
			printStats(_print, ThreadUtil.dumpThreadQueue(L2GameThreadPools.getInstance().getAiScheduledThreadPool()));
		else if(argv[1].equalsIgnoreCase("dump_allthreads"))
		{
			final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
			Log.addDev(threadDump(), "thread_lock_dump" + "_" + datetimeFormatter.format(System.currentTimeMillis()), false);
			printStats(_print, "Info about all threads successfully dumped.", "See file {./game/log/developer/thread_lock_dump_%current_date%.txt}");
		}
	}

	private static void printStats(final PrintWriter print, final String... lines)
	{
		for(final String line : lines)
			print.println(line);
		print.flush();
	}

	private final static String INDENT = "    ";

	public static String threadDump()
	{
		final StringBuilder threadDump = new StringBuilder();
		final ThreadMXBean threadMx = ManagementFactory.getThreadMXBean();
		if(threadMx.isObjectMonitorUsageSupported() && threadMx.isSynchronizerUsageSupported())
			// Print lock info if, and only if, both object monitor usage and synchronizer usage are supported.
			dumpThreadInfo(threadDump, true, threadMx);
		else
			dumpThreadInfo(threadDump, false, threadMx);
		return threadDump.toString();
	}

	private static void dumpThreadInfo(final StringBuilder threadDump, final boolean withLocks, final ThreadMXBean threadMx)
	{
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String timestamp = dateFormat.format(new Date());
		threadDump.append(timestamp);
		threadDump.append("\nFull thread dump ");
		threadDump.append("\n");

		if(withLocks)
		{
			final ThreadInfo[] threadInfos = threadMx.dumpAllThreads(true, true);
			for(final ThreadInfo threadInfo : threadInfos)
			{
				printThreadInfo(threadInfo, threadDump);
				final LockInfo[] syncs = threadInfo.getLockedSynchronizers();
				printLockInfo(syncs, threadDump);
			}
			threadDump.append("\n");
		}
		else
		{
			final long[] threadIds = threadMx.getAllThreadIds();
			final ThreadInfo[] threadInfos = threadMx.getThreadInfo(threadIds, Integer.MAX_VALUE);
			for(final ThreadInfo threadInfo : threadInfos)
				printThreadInfo(threadInfo, threadDump);
		}
	}

	private static void printThreadInfo(final ThreadInfo threadInfo, final StringBuilder threadDump)
	{
		// Print thread information
		printThread(threadInfo, threadDump);
		// print stack trace with locks
		final StackTraceElement[] stacktrace = threadInfo.getStackTrace();
		final MonitorInfo[] monitors = threadInfo.getLockedMonitors();
		for(int i = 0; i < stacktrace.length; i++)
		{
			final StackTraceElement ste = stacktrace[i];
			threadDump.append(INDENT + "at " + ste.toString());
			threadDump.append("\n");
			for(int j = 1; j < monitors.length; j++)
			{
				final MonitorInfo mi = monitors[j];
				if(mi.getLockedStackDepth() == i)
				{
					threadDump.append(INDENT + "  - locked " + mi);
					threadDump.append("\n");
				}
			}
		}
		threadDump.append("\n");
	}

	private static void printLockInfo(final LockInfo[] locks, final StringBuilder threadDump)
	{
		threadDump.append(INDENT + "Locked synchronizers: count = " + locks.length);
		threadDump.append("\n");
		for(int i = 0; i < locks.length; i++)
		{
			final LockInfo li = locks[i];
			threadDump.append(INDENT + "  - " + li);
			threadDump.append("\n");
		}
		threadDump.append("\n");
	}

	private static void printThread(final ThreadInfo threadInfo, final StringBuilder threadDump)
	{
		final StringBuilder sb = new StringBuilder("\"" + threadInfo.getThreadName() + "\"" + " nid=" + threadInfo.getThreadId() + " state=" + threadInfo.getThreadState());
		if(threadInfo.getLockName() != null && threadInfo.getThreadState() != Thread.State.BLOCKED)
		{
			final String[] lockInfo = threadInfo.getLockName().split("@");
			sb.append("\n" + INDENT + "- waiting on <0x" + lockInfo[1] + "> (a " + lockInfo[0] + ")");
			sb.append("\n" + INDENT + "- locked <0x" + lockInfo[1] + "> (a " + lockInfo[0] + ")");
		}
		else if(threadInfo.getLockName() != null && threadInfo.getThreadState() == Thread.State.BLOCKED)
		{
			final String[] lockInfo = threadInfo.getLockName().split("@");
			sb.append("\n" + INDENT + "- waiting to lock <0x" + lockInfo[1] + "> (a " + lockInfo[0] + ")");
		}
		if(threadInfo.isSuspended())
			sb.append(" (suspended)");

		if(threadInfo.isInNative())
			sb.append(" (running in native)");

		threadDump.append(sb.toString());
		threadDump.append("\n");
		if(threadInfo.getLockOwnerName() != null)
		{
			threadDump.append(INDENT + " owned by " + threadInfo.getLockOwnerName() + " id=" + threadInfo.getLockOwnerId());
			threadDump.append("\n");
		}
	}
}
