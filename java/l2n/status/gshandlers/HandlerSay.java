package l2n.status.gshandlers;

import l2n.commons.text.Strings;
import l2n.game.Announcements;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.network.clientpackets.Say2C;
import l2n.game.network.serverpackets.CreatureSay;
import l2n.game.tables.GmListTable;
import l2n.status.GameStatusThread;
import l2n.status.Status;

import java.io.PrintWriter;
import java.net.Socket;

public class HandlerSay
{
	public static void Announce(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: announce Text");
		else
		{
			Announcements.announceToAll(Strings.joinStrings(" ", argv, 1));
			_print.println("Announcement Sent!");
		}
	}

	public static void Message(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 3 || argv[1] == null || argv[2] == null || argv[1].isEmpty() || argv[2].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: message Player Text");
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
				_print.println("Unable to find Player: " + argv[1]);
			else
			{
				final String msg = Strings.joinStrings(" ", argv, 2);
				final CreatureSay cs = new CreatureSay(0, Say2C.TELL, "Админ Сайта", msg);
				player.sendPacket(cs);
				_print.println("Private message ->" + argv[1] + ": " + msg);
			}
		}
	}

	public static void GmChat(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: gmchat Text");
		else
		{
			final CreatureSay cs = new CreatureSay(0, Say2C.GM, "Telnet GM Broadcast from " + _csocket.getInetAddress().getHostAddress(), Strings.joinStrings(" ", argv, 1));
			GmListTable.broadcastToGMs(cs);
			_print.println("Your Message Has Been Sent To " + GmListTable.getAllGMs().size() + " GM(s).");
		}
	}

	public static void TelnetTell(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: tell Text");
		else
		{
			GameStatusThread temp = Status.telnetlist;
			if(temp == null)
				_print.println("telnetlist is null!");
			else
				do
					temp.write("Telnet[" + _csocket.getInetAddress().getHostAddress() + "]: " + argv[1]);
				while ((temp = temp.next) != null);
		}
	}
}
