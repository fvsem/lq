package l2n.status.gshandlers;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.Stat;
import l2n.game.GameTimeController;
import l2n.game.Shutdown;
import l2n.game.ai.DefaultAI;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2ItemInstance.ItemLocation;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.GmListTable;
import l2n.game.taskmanager.MemoryWatchDog;
import l2n.util.Log;
import l2n.util.StringUtil;
import l2n.util.Util;

import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HandlerStatus
{
	public static void Version(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		final OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
		final RuntimeMXBean rt = ManagementFactory.getRuntimeMXBean();
		_print.println("JVM .............: " + rt.getVmVendor() + " " + rt.getVmName() + " " + rt.getVmVersion());
		_print.println("OS ..............: " + os.getName() + " " + os.getVersion() + " " + os.getArch() + ", " + os.getAvailableProcessors() + " CPUs, Load Average: " + os.getSystemLoadAverage());
		_print.println("CPU Cores .......: " + Runtime.getRuntime().availableProcessors());
	}

	public static void GmList(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		int gmsCount = 0;
		String gmList = "";

		for(final L2Player player : GmListTable.getAllGMs())
		{
			gmList = gmList + ", " + player.getName();
			gmsCount++;
		}
		_print.print("There are currently " + gmsCount + " GM(s) online");
		_print.println(gmsCount > 0 ? ": " + gmList : "...");
	}

	public static void Database(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		_print.println("Database Usage Status: ");
		_print.println("+... Players operation: ");
		_print.println("-->  Update characters: " + Stat.getUpdatePlayerBase());
		_print.println("+..... Items operation: ");
		_print.println("-->      Insert: " + Stat.getInsertItemCount());
		_print.println("-->      Delete: " + Stat.getDeleteItemCount());
		_print.println("-->      Update: " + Stat.getUpdateItemCount());
		_print.println("--> Lazy Update: " + Stat.getLazyUpdateItem());
		_print.println("+... Lazy items update: " + Config.LAZY_ITEM_UPDATE);
		_print.println("+... Released ObjectId: " + IdFactory.getInstance().getReleasedCount());
	}

	public static void DumpItems(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2)
		{
			_print.println("Set locations for items.");
			return;
		}

		ItemLocation itemLoc = ItemLocation.valueOf(argv[1].toUpperCase());
		final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
		GArray<L2ItemInstance> items = L2ObjectsStorage.getStorageItems().getAll();

		int count = 0;
		StringBuilder builder = new StringBuilder();
		for(L2ItemInstance item : items)
		{
			if(item != null && (item.getLocation() == null || item.getLocation() == itemLoc))
			{
				StringUtil.append(builder, "\n", item.toString(), "\n");
				Util.printStackTrace(builder, item.getDebugException());
				count++;
			}
		}
		_print.println("Successfully dumped " + count + " items.");
		Log.addDev(builder.toString(), "dump_" + itemLoc.toString().toLowerCase() + "_" + datetimeFormatter.format(System.currentTimeMillis()), false);
	}

	public static void Status(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length >= 2 && argv[1] != null && argv[1].equalsIgnoreCase("mobs"))
		{
			final Map<String, Integer> mobs = new HashMap<String, Integer>();
			for(final L2NpcInstance npc : L2ObjectsStorage.getAllNpcsForIterate())
				if(npc.isMonster())
				{
					final String mobType = npc.getTypeName();
					int mobCount = mobs.containsKey(mobType) ? mobs.remove(mobType) : 0;
					mobCount++;
					mobs.put(mobType, mobCount);
				}
			for(final String mob : mobs.keySet())
				_print.println("\t" + mob + ": \t" + mobs.get(mob));
			return;
		}

		final int playerCount = L2ObjectsStorage.getAllPlayersCount();
		final int offtradeCount = L2ObjectsStorage.getAllOfflineCount();
		final int onlineCount = playerCount - offtradeCount;
		final int objectCount = L2ObjectsStorage.getAllObjectsCount();
		final int itemsCount = L2ObjectsStorage.getStorageItems().getRealSize();
		int nullObjectCount = 0;

		int itemsVoid = 0;
		int itemsClan = 0;
		int itemsChar = 0;
		int itemsMail = 0;
		int itemsPet = 0;
		int itemsFreight = 0;
		int itemsCharWh = 0;

		int monsterCount = 0;
		int minionCount = 0;
		int npcCount = 0;
		int charCount = 0;
		int doorCount = 0;
		int summonCount = 0;

		int AICount = 0;
		int extendedAICount = 0;
		int summonAICount = 0;
		int activeAICount = 0;

		int activeReflection = 0;
		ItemLocation itemLoc;

		for(final L2Object obj : L2ObjectsStorage.getAllObjects())
		{
			if(obj == null)
			{
				nullObjectCount++;
				continue;
			}

			if(obj.isCharacter())
			{
				charCount++;
				if(obj.isNpc())
				{
					npcCount++;
					if(obj.isMonster())
					{
						monsterCount++;
						minionCount += ((L2MonsterInstance) obj).getTotalSpawnedMinionsInstances();
					}
				}
				else if(obj.isSummon() || obj.isPet())
					summonCount++;
				else if(obj instanceof L2DoorInstance)
					doorCount++;

				if(obj.hasAI())
				{
					AICount++;
					if(obj.getAI() instanceof DefaultAI && obj.getAI().isActive())
						activeAICount++;
					if(obj.isNpc())
						extendedAICount++;
					else if(obj.isSummon() || obj.isPet())
						summonAICount++;
				}
			}
			else if(obj.isItem())
			{
				itemLoc = ((L2ItemInstance) obj).getLocation();
				if(itemLoc == null)
				{
					itemsVoid++;
					continue;
				}
				switch (itemLoc)
				{
					case VOID:
						itemsVoid++;
						break;
					case INVENTORY:
					case PAPERDOLL:
						itemsChar++;
						break;
					case WAREHOUSE:
						itemsCharWh++;
						break;
					case CLANWH:
						itemsClan++;
						break;
					case PET:
					case PET_EQUIP:
						itemsPet++;
						break;
					case FREIGHT:
						itemsFreight++;
						break;
					case MAIL:
						itemsMail++;
						break;
				}
			}
			else if(obj instanceof Reflection && ((Reflection) obj).getId() > 0)
				activeReflection++;
		}

		_print.println(" + Players");
		_print.println(" | Players...........: " + playerCount + "/" + Config.MAXIMUM_ONLINE_USERS);
		_print.println(" | Online........... : " + onlineCount);
		_print.println(" | Offline.......... : " + offtradeCount);

		_print.println(" + Characters");
		_print.println(" | Characters....... : " + charCount);
		_print.println(" | Npcs............. : " + npcCount);
		_print.println(" | Doors............ : " + doorCount);
		_print.println(" | Summons.......... : " + summonCount);
		_print.println(" | Monsters......... : " + monsterCount);
		_print.println(" | Minions.......... : " + minionCount);

		_print.println(" + Other");
		_print.println(" | Reflections...... : " + activeReflection);
		_print.println(" | Objects.......... : " + objectCount);
		_print.println(" | Null Objects..... : " + nullObjectCount);

		_print.println(" + AI");
		_print.println(" | All AI........... : " + AICount);
		_print.println(" | Active AI Count.. : " + activeAICount);
		_print.println(" | Extended AI...... : " + extendedAICount);
		_print.println(" | Summon AI........ : " + summonAICount);

		_print.println(" + Items............ : " + itemsCount + "/" + (itemsVoid + itemsChar + itemsCharWh + itemsClan + itemsFreight + itemsMail + itemsPet));
		_print.println(" | Ground Items..... : " + itemsVoid);
		_print.println(" | Owned Items...... : " + itemsChar);
		_print.println(" | Owned(WH) Items.. : " + itemsCharWh);
		_print.println(" | ClanWh Items..... : " + itemsClan);
		_print.println(" | Freight Items.... : " + itemsFreight);
		_print.println(" | Mail Items....... : " + itemsMail);
		_print.println(" | Pet Items........ : " + itemsPet);
		_print.println(" ---------------------------------- ");
		_print.println(" + GM............... : " + GmListTable.getAllGMs().size());
		_print.println(" + Game Time........ : " + GameTime());
		_print.println(" + Real Time........ : " + getCurrentTime());
		_print.println(" + Start Time....... : " + getStartTime());
		_print.println(" + Uptime........... : " + getUptime());
		_print.println(" + Shutdown / mode.. : " + Util.formatTime(Shutdown.getInstance().getSeconds()) + " / " + Shutdown.getInstance().getMode());
		_print.println(" + Threads.......... : " + Thread.activeCount());
		_print.println(" + RAM Used......... : " + MemoryWatchDog.getMemUsedMb());
	}

	public static String GameTime()
	{
		final int t = GameTimeController.getInstance().getGameTime();
		final int h = t / 60;
		final int m = t % 60;
		final SimpleDateFormat format = new SimpleDateFormat("H:mm");
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, h);
		cal.set(Calendar.MINUTE, m);
		return format.format(cal.getTime());
	}

	public static String getUptime()
	{
		return Util.formatTime(ManagementFactory.getRuntimeMXBean().getUptime() / 1000);
	}

	public static String getStartTime()
	{
		return new Date(ManagementFactory.getRuntimeMXBean().getStartTime()).toString();
	}

	public static String getCurrentTime()
	{
		return new Date().toString();
	}
}
