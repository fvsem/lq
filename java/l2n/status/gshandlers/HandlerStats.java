package l2n.status.gshandlers;

import l2n.commons.text.StrTable;
import l2n.database.L2DatabaseFactory;
import l2n.game.geodata.PathFindBuffers;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.taskmanager.PlayerTaskManager;
import l2n.game.taskmanager.SpawnTaskManager;

import java.io.PrintWriter;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.sql.SQLException;

public class HandlerStats
{
	public static void Stats(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
			_print.println("USAGE: stat pf|obj|gc|db|p_tasks|spawn");
		else if(argv[1].equalsIgnoreCase("pf") || argv[1].equalsIgnoreCase("pathfind"))
			_print.print(PathFindBuffers.getStats());
		else if(argv[1].equalsIgnoreCase("obj") || argv[1].equalsIgnoreCase("objects"))
			_print.print(L2ObjectsStorage.getStats());
		else if(argv[1].equalsIgnoreCase("gc"))
			_print.print(getGCStats());
		else if(argv[1].equalsIgnoreCase("ptasks"))
		{
			final String memUsage = PlayerTaskManager.getInstance().getReport();
			for(final String line : memUsage.split("\n"))
				_print.println(line);
		}
		else if(argv[1].equalsIgnoreCase("spawn"))
		{
			StringBuilder sb = new StringBuilder();
			sb.append("SpawnTaskManager\n");
			sb.append("=================================================\n");
			sb.append(SpawnTaskManager.getInstance().getStats());
			sb.append("=================================================\n");

			for(final String line : sb.toString().split("\n"))
				_print.println(line);
		}
		else if(argv[1].equalsIgnoreCase("db"))
			try
			{
				final String memUsage = L2DatabaseFactory.getInstance().getStats();
				for(final String line : memUsage.split("\n"))
					_print.println(line);
			}
			catch(final SQLException e)
			{}
	}

	public static StrTable getGCStats()
	{
		final StrTable table = new StrTable("Garbage Collectors Stats");
		int i = 0;
		for(final GarbageCollectorMXBean gc_bean : ManagementFactory.getGarbageCollectorMXBeans())
		{
			table.set(i, "Name", gc_bean.getName());
			table.set(i, "ColCount", Long.valueOf(gc_bean.getCollectionCount()));
			table.set(i, "ColTime (ms)", Long.valueOf(gc_bean.getCollectionTime()));
			++i;
		}
		return table;
	}
}
