package l2n.status.gshandlers;

import l2n.Config;
import l2n.extensions.scripts.Scripts;
import l2n.game.Shutdown;
import l2n.game.communitybbs.handlers.StatisticHandler;
import l2n.game.loginservercon.LSConnection;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SpawnTable;
import l2n.status.GameStatusThread;
import l2n.util.Files;
import l2n.util.HWID;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

public class HandlerWorld
{
	private static final Logger _log = Logger.getLogger(GameStatusThread.class.getName());

	public static void Whois(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: reload skills|npc|gmaccess|scripts|pktlogger|html|spawn|multisell|banhwid|bonushwid");
		else if(argv[1].equalsIgnoreCase("banhwid"))
		{
			HWID.reloadBonusHWIDs();
			_print.println("Bonushwid table reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("banhwid"))
		{
			HWID.reloadBannedHWIDs();
			_print.println("Banhwids table reloaded...");
		}
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
				_print.println("Unable to find Player: " + argv[1]);
			else
			{
				long adenaCWH = 0;
				final long adenaWH = player.getWarehouse().getAdenaCount();
				String clanName = "-", allyName = "-";
				if(player.getClan() != null)
				{
					adenaCWH = player.getClan().getAdenaCount();
					clanName = player.getClan().getName();
					if(player.getAlliance() != null)
						allyName = player.getAlliance().getAllyName();
				}

				_print.println("Account / IP / .............: " + player.getAccountName() + " / " + player.getIP());
				_print.println("Level / EXP / SP ...........: " + player.getLevel() + " / " + player.getExp() + " / " + player.getSp());
				_print.println("Location ...................: " + player.getLoc());
				_print.println("Adena Inv / WH / CWH .......: " + player.getAdena() + " / " + adenaWH + " / " + adenaCWH);
				_print.println("Clan / Ally ................: " + clanName + " / " + allyName);
				// TODO расширить
			}
		}
	}

	public static void ListEnemy(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: enemy Player");
		else
		{
			final L2Player player = L2ObjectsStorage.getPlayer(argv[1]);
			if(player == null)
				_print.println("Unable to find Player: " + argv[1]);
			else
				for(final L2NpcInstance enemy : player.getHateList().keySet())
					_print.println("--> " + enemy.getName() + " <--");
		}
	}

	public static void Reload(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: reload skills|npc|gmaccess|scripts|pktlogger|html|spawn|multisell");
		else if(argv[1].equalsIgnoreCase("skills"))
		{
			SkillTable.getInstance().reload(true);
			_print.println("Skills table reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("npc"))
		{
			NpcTable.getInstance().reloadAllNpc();
			_print.println("Npc table reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("gmaccess"))
		{
			Config.loadGMAccess();
			for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(!Config.EVERYBODY_HAS_ADMIN_RIGHTS)
					player.setPlayerAccess(Config.gmlist.get(player.getObjectId()));
				else
					player.setPlayerAccess(Config.gmlist.get(new Integer(0)));
			_print.println("GMAccess reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("scripts"))
		{
			Scripts.getInstance().reload();
			_print.println("Scripts reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("pktlogger"))
		{
			Config.reloadPacketLoggerConfig();
			_print.println("Packet-logger config reloaded...");
		}
		else if(argv[1].equalsIgnoreCase("html"))
		{
			Files.cacheClean();
			StatisticHandler.clearCache();
			_print.println("HTML cache clearned.");
		}
		else if(argv[1].equalsIgnoreCase("spawn"))
		{
			SpawnTable.getInstance().reloadAll();
			_print.println("All npc respawned.");
		}
		else if(argv[1].equalsIgnoreCase("multisell"))
		{
			L2Multisell.getInstance().reload();
			_print.println("Multisell list reloaded.");
		}
		else
			_print.println("Unknown reload component: " + argv[1]);
	}

	public static void Shutdown(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: shutdown seconds|NOW");
		else if(argv[1].equalsIgnoreCase("NOW"))
		{
			_log.warning("Shutting down via TELNET by host: " + _csocket.getInetAddress().getHostAddress());
			_print.println("Shutting down...");
			System.exit(-1);
		}
		else
		{
			int val = 0;
			try
			{
				val = Integer.parseInt(argv[1]);
			}
			catch(final Exception e)
			{
				_print.println("USAGE: shutdown seconds|NOW");
				return;
			}
			Shutdown.getInstance().startTelnetShutdown(_csocket.getInetAddress().getHostAddress(), val, false);
			_print.println("Server will shutdown in " + val + " seconds!");
			_print.println("Type \"abort\" to abort shutdown!");
		}
	}

	public static void Restart(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: restart seconds");
		else
		{
			int val = 0;
			try
			{
				val = Integer.parseInt(argv[1]);
			}
			catch(final Exception e)
			{
				_print.println("USAGE: restart seconds");
				return;
			}
			Shutdown.getInstance().startTelnetShutdown(_csocket.getInetAddress().getHostAddress(), val, true);
			_print.println("Server will restart in " + val + " seconds!");
			_print.println("Type \"abort\" to abort restart!");
		}
	}

	public static void AbortShutdown(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		Shutdown.getInstance().Telnetabort(_csocket.getInetAddress().getHostAddress());
		_print.println("OK! - Shutdown/Restart aborted.");
	}

	public static void StopLogin(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		Config.MAXIMUM_ONLINE_USERS = 0;
		Config.MAX_PROTOCOL_REVISION = 1;
		Config.MIN_PROTOCOL_REVISION = 0;

		_print.println("Shutdown LSConnection...");
		_print.flush();
		LSConnection.getInstance().shutdown();

		_print.println("Kicking players...");
		_print.flush();
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			player.logout(true, false, false, true);

		_print.println("Forcing gc...");
		_print.flush();
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				for(int i = 0; i < 10; i++)
				{
					System.gc();
					try
					{
						Thread.sleep(1000);
					}
					catch(final InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
