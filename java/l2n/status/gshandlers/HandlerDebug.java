package l2n.status.gshandlers;

import l2n.commons.lang.HeapDumper;
import l2n.commons.text.Strings;
import l2n.game.geodata.GeoEngine;
import l2n.game.geodata.GeoMove;
import l2n.game.model.L2World;
import l2n.game.taskmanager.DecayTaskManager;
import l2n.status.DummyL2Object;
import l2n.util.Location;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class HandlerDebug
{
	public static void Debug(final String fullCmd, final String[] argv, final PrintWriter _print, final Socket _csocket)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty() || argv[1].equalsIgnoreCase("?"))
			_print.println("USAGE: debug decay|geo");
		else if(argv[1].equalsIgnoreCase("decay"))
			_print.print(DecayTaskManager.getInstance());
		else if(argv[1].equalsIgnoreCase("geo"))
		{
			if(argv.length < 3 || argv[2] == null || argv[2].isEmpty() || argv[2].equalsIgnoreCase("?"))
				_print.println("USAGE: debug geo x,y,z=>x,y,z");
			else
				_print.println(DebugGeo(Strings.joinStrings(" ", argv, 2)));
		}
		else
			_print.println("Unknown debug type: " + argv[1]);
	}

	public static void HprofMemDump(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		boolean live = true;
		String dir = "./log/snapshots";
		if(argv.length > 1)
		{
			if(argv[1].equalsIgnoreCase("?"))
			{
				_print.println("USAGE: dumpmem [Directory] [live:true|false]");
				return;
			}
			dir = argv[1];
			if(argv.length > 2)
				live = Boolean.parseBoolean(argv[2]);
		}
		try
		{
			_print.println("Memory snapshot saved: " + HeapDumper.dumpHeap(dir, live));
		}
		catch(final Exception e)
		{
			e.printStackTrace(_print);
		}
	}

	public static String DebugGeo(final String arg)
	{
		final String[] args = arg.split("=>");
		if(args.length < 2)
			return "You should define source & dest location (x,y,z=>x,y,z)";
		final Location src_loc = new Location(args[0]);
		final Location dst_loc = new Location(args[1]);

		String result = "Move checking " + src_loc.toXYZString() + " => " + dst_loc.toXYZString() + "\r\n";
		result += "\t isWater...........................: " + L2World.isWater(dst_loc) + "\r\n";
		result += "\t canMoveToCoord....................: " + GeoEngine.canMoveToCoord(src_loc.x, src_loc.y, src_loc.z, dst_loc.x, dst_loc.y, dst_loc.z) + "\r\n";
		result += "\t moveCheck.........................: " + GeoEngine.moveCheck(src_loc.x, src_loc.y, src_loc.z, dst_loc.x, dst_loc.y).toXYZString() + "\r\n";
		result += "\t moveCheckWithCollision............: " + GeoEngine.moveCheckWithCollision(src_loc.x, src_loc.y, src_loc.z, dst_loc.x, dst_loc.y).toXYZString() + "\r\n";
		result += "\t moveCheckWithCollision[pf_1]......: " + GeoEngine.moveCheckWithCollision(src_loc.x, src_loc.y, src_loc.z, dst_loc.x, dst_loc.y, true).toXYZString() + "\r\n";
		result += "\t moveCheckWithCollision[pf_2]......: " + GeoEngine.moveCheckBackwardWithCollision(dst_loc.x, dst_loc.y, dst_loc.z, src_loc.x, src_loc.y, true).toXYZString() + "\r\n";

		result += "\t findPath: ";
		final List<Location> targets = GeoMove.findPath(src_loc.x, src_loc.y, src_loc.z, dst_loc.clone(), new DummyL2Object(), false);
		if(targets.isEmpty())
			result += "Empty";
		else
			for(int i = 0; i < targets.size(); i++)
				result += "\r\n\t\t\t [" + i + "]: " + targets.get(i).toXYZString();
		result += "\r\n";
		return result;
	}
}
