package l2n.status.gshandlers;

import l2n.Config;
import l2n.commons.util.StatsUtil;
import l2n.game.L2GameThreadPools;
import l2n.util.Log;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;

public class HandlerPerfomance
{
	public static void LazyItems(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		Config.LAZY_ITEM_UPDATE = !Config.LAZY_ITEM_UPDATE;
		_print.println("Lazy items update set to: " + Config.LAZY_ITEM_UPDATE);
	}

	public static void ThreadPool(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		if(argv.length < 2 || argv[1] == null || argv[1].isEmpty())
			_print.println("USAGE: p packets(p)|general(g)|move(m)|ai|effects|mem|threads|purge|perf");
		else if(argv[1].equalsIgnoreCase("packets") || argv[1].equalsIgnoreCase("p"))
			printStats(_print, L2GameThreadPools.getInstance().getGPacketStats());
		else if(argv[1].equalsIgnoreCase("general") || argv[1].equalsIgnoreCase("g"))
			printStats(_print, L2GameThreadPools.getInstance().getGeneralPoolStats());
		else if(argv[1].equalsIgnoreCase("move") || argv[1].equalsIgnoreCase("m"))
			printStats(_print, L2GameThreadPools.getInstance().getMovePoolStats());
		else if(argv[1].equalsIgnoreCase("ai"))
			printStats(_print, L2GameThreadPools.getInstance().getAIPoolStats());
		else if(argv[1].equalsIgnoreCase("m") || argv[1].equalsIgnoreCase("mem"))
		{
			final String memUsage = StatsUtil.getMemUsage();
			for(final String line : memUsage.split("\n"))
				_print.println(line);
			_print.flush();
		}
		else if(argv[1].equalsIgnoreCase("perf"))
		{
			for(final String line : L2GameThreadPools.getInstance().getStats())
				_print.println(line);
			_print.flush();
		}
		else if(argv[1].equalsIgnoreCase("purge"))
		{
			L2GameThreadPools.getInstance().purge();
			_print.println("STATUS OF THREAD POOLS AFTER PURGE COMMAND:");
			_print.println("");
			for(final String line : L2GameThreadPools.getInstance().getStats())
				_print.println(line);
			_print.flush();
		}
		else
		{
			if(!argv[1].equalsIgnoreCase("t") && !argv[1].equalsIgnoreCase("threads"))
				return;
			_print.print(StatsUtil.getThreadStats());
			_print.println("");
			final SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd.MM.yyyy_HH.mm.ss");
			Log.addDev(StatsUtil.getFullThreadStats(), "threads_dump" + "_" + datetimeFormatter.format(System.currentTimeMillis()), false);
			printStats(_print, "Info about all threads successfully dumped.", "See file {./game/log/developer/threads_dump_%current_date%.txt}");
		}
	}

	public static void GC(final String fullCmd, final String[] argv, final PrintWriter _print)
	{
		_print.println("Currently: ");
		_print.print(StatsUtil.getMemUsage());
		_print.println("Forcing gc...");
		Runtime.getRuntime().gc();
		_print.println("Now: ");
		_print.print(StatsUtil.getMemUsage());
	}

	private static void printStats(final PrintWriter print, final String... lines)
	{
		for(final String line : lines)
			print.println(line);
		print.flush();
	}
}
