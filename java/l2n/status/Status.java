package l2n.status;

import l2n.Config;
import l2n.Server;
import l2n.commons.configuration.L2Properties;
import l2n.util.Util;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Status extends Thread
{
	private final static Logger _log = Logger.getLogger(Status.class.getName());
	private ServerSocket _serverSocket;

	private int _mode;
	private String _password;
	public static GameStatusThread telnetlist;

	@Override
	public void run()
	{
		while (true)
		{
			try
			{
				Socket connection = _serverSocket.accept();
				if(_mode == Server.MODE_GAMESERVER)
					new GameStatusThread(connection, _password);
				else if(_mode == Server.MODE_LOGINSERVER)
					new LoginStatusThread(connection, _password);
				if(isInterrupted())
				{
					try
					{
						_serverSocket.close();
					}
					catch(IOException io)
					{
						io.printStackTrace();
					}
					break;
				}
			}
			catch(IOException e)
			{
				if(isInterrupted())
				{
					try
					{
						_serverSocket.close();
					}
					catch(IOException io)
					{
						io.printStackTrace();
					}
					break;
				}
			}
			try
			{
				Thread.sleep(3000); /* Защита от глюков */
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	public Status(int mode) throws IOException
	{
		super("Status");
		_mode = mode;
		L2Properties telnetSettings = new L2Properties(_mode == Server.MODE_GAMESERVER ? Config.TELNET_FILE : Config.LOGIN_TELNET_FILE, false);

		int port = telnetSettings.getInteger("StatusPort", 12345);
		_password = telnetSettings.getProperty("StatusPW");
		if(_password == null || _password.isEmpty())
		{
			_log.warning("Warning: server's Telnet Function Has No Password Defined!");
			_password = Util.randomPassword(10);
			_log.log(Level.INFO, "StatusServer: using generated password " + _password + ".");
		}
		else
			_log.log(Level.INFO, "StatusServer: using setted password " + _password + ".");

		_serverSocket = new ServerSocket(port);
		if(_mode == Server.MODE_LOGINSERVER)
			_log.log(Level.INFO, "StatusServer for LoginServer started on port: " + port);
		else
			_log.log(Level.INFO, "StatusServer for GameServer started on port: " + port);
	}
}
