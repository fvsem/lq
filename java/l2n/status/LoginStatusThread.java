package l2n.status;

import l2n.Config;
import l2n.commons.network.utils.Net;
import l2n.login.GameServerTable;
import l2n.login.L2LoginServer;
import l2n.login.Shutdown;
import l2n.login.protect.BanInformation;
import l2n.login.protect.IpBlockTable;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginStatusThread extends Thread
{
	private static final Logger _log = Logger.getLogger(LoginStatusThread.class.getName());

	private Socket _csocket;

	private final PrintWriter _print;
	private final BufferedReader _read;

	private void telnetOutput(int type, String text)
	{
		if(type == 1)
			_log.fine("LSTELNET | " + text);
		else if(type == 2)
			_log.fine("LSTELNET | " + text);
		else if(type == 3)
			_log.fine(text);
		else if(type == 4)
			_log.fine(text);
		else
			_log.fine("LSTELNET | " + text);
	}

	private boolean isValidIP(Socket client)
	{
		boolean result = false;
		InetAddress ClientIP = client.getInetAddress();

		// convert IP to String, and compare with list
		String clientStringIP = ClientIP.getHostAddress();

		telnetOutput(1, "Connection from: " + clientStringIP);

		// read and loop thru list of IPs, compare with newIP
		try
		{
			Properties telnetSettings = new Properties();
			InputStream telnetIS = new FileInputStream(new File(Config.LOGIN_TELNET_FILE));
			telnetSettings.load(telnetIS);
			telnetIS.close();

			String HostList = telnetSettings.getProperty("ListOfHosts", "127.0.0.1,localhost");

			// compare
			String ipToCompare;
			for(String ip : HostList.split(","))
				if(!result)
				{
					ipToCompare = InetAddress.getByName(ip).getHostAddress();
					if(clientStringIP.equals(ipToCompare))
						result = true;
				}
		}
		catch(IOException e)
		{
			telnetOutput(1, "Error: " + e);
		}

		return result;
	}

	public LoginStatusThread(Socket client, String password) throws IOException
	{
		_csocket = client;

		_print = new PrintWriter(_csocket.getOutputStream());
		_read = new BufferedReader(new InputStreamReader(_csocket.getInputStream()));

		// проверяем IP
		if(!isValidIP(client))
		{
			telnetOutput(1, "Connection attempt from " + client.getInetAddress().getHostAddress() + " rejected.");
			_csocket.close();
			return;
		}

		telnetOutput(1, client.getInetAddress().getHostAddress() + " accepted.");
		_print.println("Welcome to the L2Dream LoginServer Telnet Session.");

		if(password == null || password.isEmpty())
		{
			start();
			return;
		}

		// авторизируемся
		int authAttempts = 0;
		String authPass;
		_print.println("Enter your password, please!");
		while (authAttempts < 5) // TODO maxAuthAttempts
		{
			authAttempts++;
			_print.print("Password: ");
			_print.flush();
			authPass = _read.readLine();

			if(password.equalsIgnoreCase(authPass))
			{
				_print.println("Password accepted!");
				_print.println();
				_print.flush();
				start();
				return;
			}

			try
			{
				Thread.sleep(authAttempts * 1000);
			}
			catch(Exception e)
			{
				_csocket.close();
				return;
			}

			_print.print("Password incorrect! ");
		}

		_print.println();
		_print.println("Maximum auth re-attempts reached! Disconnecting....");
		_print.flush();
		_csocket.close();
	}

	@Override
	public void run()
	{
		String cmd;
		String[] argv;
		try
		{
			for(;;)
			{
				_print.print("login@server> ");
				_print.flush();
				cmd = _read.readLine();

				if(cmd == null)
				{
					_csocket.close();
					break;
				}

				argv = cmd.split(" ");
				try
				{
					if(argv == null || argv.length == 0 || argv[0].isEmpty())
					{
						/* do nothing */
					}
					else if(argv[0].equalsIgnoreCase("?") || argv[0].equalsIgnoreCase("h") || argv[0].equalsIgnoreCase("help"))
						Help(_print);
					else if(argv[0].equals("status") || argv[0].equals("s"))
						for(String str : GameServerTable.getInstance().status())
							_print.println(str);
					else if(argv[0].equals("baniplist"))
						try
						{
							BanInformation[] baniplist = IpBlockTable.getInstance().getBanInformations();
							_print.println("Ban IP List:");
							for(BanInformation temp : baniplist)
								_print.println("Ip:" + temp.ip + " banned by " + temp.admin);
							_print.flush();
						}
						catch(StringIndexOutOfBoundsException e)
						{
							_print.println("Please enter ip to ban");
						}
					else if(argv[0].startsWith("banip"))
						try
						{
							InetAddress address = InetAddress.getByName(argv[1]);
							if(address != null)
								if(IpBlockTable.getInstance().addIPBann(Net.getHashFromAddress(address), address.getHostAddress(), "Telnet: " + _csocket.getInetAddress().getHostAddress(), 0, ""))
								{
									_print.flush();
									_print.println("IP: " + address.getHostAddress() + " already banned.");
								}
								else
								{
									_print.flush();
									_print.println("IP: " + address.getHostAddress() + " successfully baned.");
								}
						}
						catch(Exception e)
						{
							_print.println("Please enter ip to unban");
						}
					else if(argv[0].startsWith("unbanip"))
						try
						{
							InetAddress address = InetAddress.getByName(argv[1]);
							if(address != null)
							{
								BanInformation banInfo;
								if((banInfo = IpBlockTable.getInstance().removeIPBann(Net.getHashFromAddress(address))) != null)
									_print.println("IP: " + banInfo.ip + " successfully unbaned.");
								else
									_print.println("IP: " + address.getHostAddress() + " not banned.");
							}
						}
						catch(Exception e)
						{
							_print.println("Please enter ip to unban");
						}
					else if(argv[0].startsWith("shutdown"))
					{
						_print.println("Shutting down...");
						System.exit(-1);
						_print.println("Bye Bye!");
						_print.flush();
						_csocket.close();
					}
					else if(argv[0].startsWith("restart"))
					{
						_print.println("Restarting...");
						Shutdown.getInstance().startShutdown(10, true);
						_print.println("Bye Bye!");
						_print.flush();
						_csocket.close();
					}
					else if(argv[0].startsWith("setpass"))
						try
						{
							boolean result = L2LoginServer.getInstance().setPassword(argv[1], argv[2]);
							if(result)
								_print.print("Password successfully changed.");
							else
								_print.print("Error while set new password.");
						}
						catch(StringIndexOutOfBoundsException e)
						{
							_print.println("Usage: setpass <account> <password>");
						}
					else if(argv[0].equalsIgnoreCase("quit") || argv[0].equalsIgnoreCase("q") || argv[0].equalsIgnoreCase("exit"))
					{
						_print.println("Bye Bye!");
						_print.flush();
						_csocket.close();
						break;
					}
					else
						_print.println("Unknown command: " + argv[0]);
				}
				catch(Exception e)
				{
					e.printStackTrace(_print);
				}

				_print.println();
				_print.flush();
				try
				{
					Thread.sleep(100);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			telnetOutput(1, "Connection from " + _csocket.getInetAddress().getHostAddress() + " was closed by client.");
		}
		catch(Exception e)
		{
			_log.log(Level.INFO, "Connection lost " + _csocket.getInetAddress().getHostAddress() + ".");
			try
			{
				_csocket.close();
			}
			catch(Exception e1)
			{}
		}
	}

	private static void Help(PrintWriter _print)
	{
		_print.println("The following is a list of all available commands: ");
		_print.println("help                          - shows this help.");
		_print.println("status                        - displays basic server statistics.");
		_print.println("setpass <account> <password>  - set new password.");
		_print.println("----------- IP Filter Commands----------------------."); // TODO
		_print.println("unblockip <ip>                - removes <ip> from hacking protection list.");
		_print.println("baniplist                     - display banip list.");
		_print.println("banip <ip>                    - ban <ip>.");
		_print.println("unban <ip>                    - unban <ip>.");
		_print.println("----------------- The End ---------------------------.");
		_print.println("shutdown                      - shuts down server.");
		_print.println("restart	                      - restarts the server.");
		_print.println("exit, quit(q)                 - closes telnet session.");
		_print.println("");
	}
}
