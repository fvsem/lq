package l2n.status;

import l2n.Config;
import l2n.status.gshandlers.*;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameStatusThread extends Thread
{
	private static final Logger _log = Logger.getLogger(GameStatusThread.class.getName());

	public boolean LogChat = false;
	public boolean LogTell = false;
	public GameStatusThread next;
	private Socket _csocket;
	private PrintWriter _print;
	private BufferedReader _read;

	private void telnetOutput(int type, String text)
	{
		if(type == 1)
			_log.fine("GSTELNET | " + text);
		else if(type == 2)
			_log.fine("GSTELNET | " + text);
		else if(type == 3)
			_log.fine(text);
		else if(type == 4)
			_log.fine(text);
		else
			_log.fine("GSTELNET | " + text);
	}

	private boolean isValidIP(Socket client)
	{
		boolean result = false;
		InetAddress ClientIP = client.getInetAddress();

		// convert IP to String, and compare with list
		String clientStringIP = ClientIP.getHostAddress();

		telnetOutput(1, "Connection from: " + clientStringIP);

		// read and loop thru list of IPs, compare with newIP
		try
		{
			Properties telnetSettings = new Properties();
			InputStream telnetIS = new FileInputStream(new File(Config.TELNET_FILE));
			telnetSettings.load(telnetIS);
			telnetIS.close();

			String HostList = telnetSettings.getProperty("ListOfHosts", "127.0.0.1,localhost");

			// compare
			String ipToCompare;
			for(String ip : HostList.split(","))
				if(!result)
				{
					ipToCompare = InetAddress.getByName(ip).getHostAddress();
					if(clientStringIP.equals(ipToCompare))
						result = true;
				}
		}
		catch(IOException e)
		{
			telnetOutput(1, "Error: " + e);
		}

		return result;
	}

	public GameStatusThread(Socket client, String password) throws IOException
	{
		_csocket = client;

		_print = new PrintWriter(_csocket.getOutputStream());
		_read = new BufferedReader(new InputStreamReader(_csocket.getInputStream()));

		// проверяем IP
		if(!isValidIP(client))
		{
			telnetOutput(1, "Connection attempt from " + client.getInetAddress().getHostAddress() + " rejected.");
			_csocket.close();
			return;
		}

		telnetOutput(1, client.getInetAddress().getHostAddress() + " accepted.");
		_print.println("Welcome to the L2System GameServer Telnet Session.");

		if(password == null || password.isEmpty())
		{
			start();
			return;
		}

		// авторизируемся
		int authAttempts = 0;
		String authPass;
		_print.println("Enter your password, please!");
		while (authAttempts < 5) // TODO maxAuthAttempts
		{
			authAttempts++;
			_print.print("Password: ");
			_print.flush();
			authPass = _read.readLine();

			if(password.equalsIgnoreCase(authPass))
			{
				_print.println("Password accepted!");
				_print.println();
				_print.flush();
				start();
				return;
			}

			try
			{
				Thread.sleep(authAttempts * 1000);
			}
			catch(Exception e)
			{
				_csocket.close();
				return;
			}

			_print.print("Password incorrect! ");
		}

		_print.println();
		_print.println("Maximum auth re-attempts reached! Disconnecting....");
		_print.flush();
		_csocket.close();
	}

	@Override
	public void run()
	{
		next = Status.telnetlist;
		Status.telnetlist = this;

		String cmd;
		String[] argv;
		try
		{
			for(;;)
			{
				_print.print("game@server> ");
				_print.flush();
				cmd = _read.readLine();

				if(cmd == null)
				{
					_csocket.close();
					break;
				}

				argv = cmd.split(" ");

				try
				{
					if(argv == null || argv.length == 0 || argv[0].isEmpty())
					{ /* do nothing */}
					else if(argv[0].equalsIgnoreCase("?") || argv[0].equalsIgnoreCase("h") || argv[0].equalsIgnoreCase("help"))
						Help(_print);
					else if(argv[0].equalsIgnoreCase("status") || argv[0].equalsIgnoreCase("s"))
						HandlerStatus.Status(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("dumpitems"))
						HandlerStatus.DumpItems(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("database") || argv[0].equalsIgnoreCase("db"))
						HandlerStatus.Database(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("gmlist"))
						HandlerStatus.GmList(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("ver") || argv[0].equalsIgnoreCase("version"))
						HandlerStatus.Version(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("lazyitems"))
						HandlerPerfomance.LazyItems(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("performance") || argv[0].equalsIgnoreCase("p"))
						HandlerPerfomance.ThreadPool(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("dump_thread") || argv[0].equalsIgnoreCase("dt"))
						HandlerThreads.ThreadDump(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("garbage") || argv[0].equalsIgnoreCase("gc"))
						HandlerPerfomance.GC(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("stat"))
						HandlerStats.Stats(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("announce") || argv[0].equalsIgnoreCase("!"))
						HandlerSay.Announce(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("message") || argv[0].equalsIgnoreCase("msg"))
						HandlerSay.Message(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("gmchat"))
						HandlerSay.GmChat(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("tell"))
						HandlerSay.TelnetTell(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("baniplist") || argv[0].equalsIgnoreCase("banip"))
						HandlerBan.BanIP(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("unbanip"))
						HandlerBan.UnBanIP(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("banhwid"))
						HandlerBan.BanHWID(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("unbanhwid"))
						HandlerBan.UnBanHWID(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("ban"))
						HandlerBan.Ban(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("unban"))
						HandlerBan.UnBan(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("kick"))
						HandlerBan.Kick(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("whois"))
						HandlerWorld.Whois(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("enemy"))
						HandlerWorld.ListEnemy(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("reload"))
						HandlerWorld.Reload(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("shutdown"))
						HandlerWorld.Shutdown(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("restart"))
						HandlerWorld.Restart(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("abort") || argv[0].equalsIgnoreCase("a"))
						HandlerWorld.AbortShutdown(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("stopLogin"))
						HandlerWorld.StopLogin(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("debug"))
						HandlerDebug.Debug(cmd, argv, _print, _csocket);
					else if(argv[0].equalsIgnoreCase("log_chat"))
					{
						LogChat = !LogChat;
						_print.println("Log chat is turned " + (LogChat ? "on" : "off"));
					}
					else if(argv[0].equalsIgnoreCase("dumpmem") || argv[0].equalsIgnoreCase("memdump"))
						HandlerDebug.HprofMemDump(cmd, argv, _print);
					else if(argv[0].equalsIgnoreCase("log_tell"))
					{
						LogTell = !LogTell;
						_print.println("Log tell is turned " + (LogTell ? "on" : "off"));
					}
					else if(argv[0].equalsIgnoreCase("quit") || argv[0].equalsIgnoreCase("q") || argv[0].equalsIgnoreCase("exit"))
					{
						_print.println("Bye Bye!");
						_print.flush();
						_csocket.close();
						break;
					}
					else
						_print.println("Unknown command: " + argv[0]);
				}
				catch(Exception e)
				{
					e.printStackTrace(_print);
				}

				_print.println();
				_print.flush();
				try
				{
					Thread.sleep(100);
				}
				catch(InterruptedException e)
				{
					e.printStackTrace();
				}
			} /* end for */

			telnetOutput(1, "Connection from " + _csocket.getInetAddress().getHostAddress() + " was closed by client.");
		}
		catch(Exception e)
		{
			_log.log(Level.INFO, "Connection lost " + _csocket.getInetAddress().getHostAddress() + ".");
			try
			{
				_csocket.close();
			}
			catch(Exception e1)
			{

			}
		}

		// begin clean telnetlist
		if(this == Status.telnetlist)
			Status.telnetlist = next;
		else
		{
			GameStatusThread temp = Status.telnetlist;
			while (temp.next != this)
				temp = temp.next;
			temp.next = next;
		}
	}

	private static void Help(PrintWriter _print)
	{
		_print.println("The following is a list of all available commands:");
		_print.println("help(h|?)               - shows this help.");
		_print.println("version(ver)            - displays version in server statistics.");
		_print.println("status(s)               - displays basic server statistics.");
		_print.println("stat                    - displays other server statistics.");
		_print.println("database(db)");
		_print.println("lazyitems");
		_print.println("performance(p)          - shows server performance statistics.");
		_print.println("dump_thread(dt)         - dump ThreadPoolExecutor queue.");
		_print.println("announce(!)             - announces <text> in game.");
		_print.println("message(msg)");
		_print.println("gmlist                  - lists all gms online.");
		_print.println("gmchat                  - Sends a message to all GMs with <text>.");
		_print.println("tell");
		_print.println("whois");
		_print.println("enemy");
		_print.println("baniplist(banip)");
		_print.println("unbanip");
		_print.println("banhwid");
		_print.println("unbanhwid");
		_print.println("ban");
		_print.println("unban");
		_print.println("kick                    - kick player <name> from server.");
		_print.println("reload");
		_print.println("shutdown                - shuts down server in <time> seconds.");
		_print.println("restart                 - restarts down server in <time> seconds.");
		_print.println("abort                   - aborts shutdown/restart.");
		_print.println("stopLogin");
		_print.println("garbage(gc)             - forced garbage collection.");
		_print.println("dumpmem                 - dump memory.");
		_print.println("log_chat");
		_print.println("log_tell");
		_print.println("exit, quit(q)");
	}

	public void write(String _text)
	{
		_print.println(_text);
		_print.flush();
	}
}
