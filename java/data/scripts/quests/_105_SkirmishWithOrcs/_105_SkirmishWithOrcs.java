package quests._105_SkirmishWithOrcs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _105_SkirmishWithOrcs extends Quest implements ScriptFile
{
	// NPC
	private static final int Kendell = 30218;
	// QuestItem
	private static final int Kendells1stOrder = 1836;
	private static final int Kendells2stOrder = 1837;
	private static final int Kendells3stOrder = 1838;
	private static final int Kendells4stOrder = 1839;
	private static final int Kendells5stOrder = 1840;
	private static final int Kendells6stOrder = 1841;
	private static final int Kendells7stOrder = 1842;
	private static final int Kendells8stOrder = 1843;
	private static final int KabooChiefs1stTorque = 1844;
	private static final int KabooChiefs2stTorque = 1845;
	private static final int RED_SUNSET_SWORD = 981;
	private static final int RED_SUNSET_STAFF = 754;
	// Item
	// NPC
	private static final int KabooChiefUoph = 27059;
	private static final int KabooChiefKracha = 27060;
	private static final int KabooChiefBatoh = 27061;
	private static final int KabooChiefTanukia = 27062;
	private static final int KabooChiefTurel = 27064;
	private static final int KabooChiefRoko = 27065;
	private static final int KabooChiefKamut = 27067;
	private static final int KabooChiefMurtika = 27068;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _105_SkirmishWithOrcs()
	{
		super(105, -1);

		addStartNpc(Kendell);

		addTalkId(Kendell);

		addKillId(KabooChiefUoph);
		addKillId(KabooChiefKracha);
		addKillId(KabooChiefBatoh);
		addKillId(KabooChiefTanukia);
		addKillId(KabooChiefTurel);
		addKillId(KabooChiefRoko);
		addKillId(KabooChiefKamut);
		addKillId(KabooChiefMurtika);

		addQuestItem(new int[] {
				Kendells1stOrder,
				Kendells2stOrder,
				Kendells3stOrder,
				Kendells4stOrder,
				Kendells5stOrder,
				Kendells6stOrder,
				Kendells7stOrder,
				Kendells8stOrder,
				KabooChiefs1stTorque,
				KabooChiefs2stTorque });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30218-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			if(st.getQuestItemsCount(Kendells1stOrder) + st.getQuestItemsCount(Kendells2stOrder) + st.getQuestItemsCount(Kendells3stOrder) + st.getQuestItemsCount(Kendells4stOrder) == 0)
			{
				int n = Rnd.get(100);
				if(n < 25)
					st.giveItems(Kendells1stOrder, 1, false);
				else if(n < 50)
					st.giveItems(Kendells2stOrder, 1, false);
				else if(n < 75)
					st.giveItems(Kendells3stOrder, 1, false);
				else
					st.giveItems(Kendells4stOrder, 1, false);
			}
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getRace().ordinal() != 1)
			{
				htmltext = "30218-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 10)
			{
				htmltext = "30218-10.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30218-02.htm";
		}
		else if(cond == 1 && st.getQuestItemsCount(Kendells1stOrder) + st.getQuestItemsCount(Kendells2stOrder) + st.getQuestItemsCount(Kendells3stOrder) + st.getQuestItemsCount(Kendells4stOrder) != 0)
			htmltext = "30218-05.htm";
		else if(cond == 2 && st.getQuestItemsCount(KabooChiefs1stTorque) != 0)
		{
			htmltext = "30218-06.htm";
			if(st.getQuestItemsCount(Kendells1stOrder) > 0)
				st.takeItems(Kendells1stOrder, -1);
			if(st.getQuestItemsCount(Kendells2stOrder) > 0)
				st.takeItems(Kendells2stOrder, -1);
			if(st.getQuestItemsCount(Kendells3stOrder) > 0)
				st.takeItems(Kendells3stOrder, -1);
			if(st.getQuestItemsCount(Kendells4stOrder) > 0)
				st.takeItems(Kendells4stOrder, -1);
			st.takeItems(KabooChiefs1stTorque, 1);
			int n = Rnd.get(100);
			if(n < 25)
				st.giveItems(Kendells5stOrder, 1, false);
			else if(n < 50)
				st.giveItems(Kendells6stOrder, 1, false);
			else if(n < 75)
				st.giveItems(Kendells7stOrder, 1, false);
			else
				st.giveItems(Kendells8stOrder, 1, false);
			st.set("cond", "3");
			st.setState(STARTED);
		}
		else if(cond == 3 && st.getQuestItemsCount(Kendells5stOrder) + st.getQuestItemsCount(Kendells6stOrder) + st.getQuestItemsCount(Kendells7stOrder) + st.getQuestItemsCount(Kendells8stOrder) == 1)
			htmltext = "30218-07.htm";
		else if(cond == 4 && st.getQuestItemsCount(KabooChiefs2stTorque) > 0)
		{
			htmltext = "30218-08.htm";
			if(st.getQuestItemsCount(Kendells5stOrder) > 0)
				st.takeItems(Kendells5stOrder, -1);
			if(st.getQuestItemsCount(Kendells6stOrder) > 0)
				st.takeItems(Kendells6stOrder, -1);
			if(st.getQuestItemsCount(Kendells7stOrder) > 0)
				st.takeItems(Kendells7stOrder, -1);
			if(st.getQuestItemsCount(Kendells8stOrder) > 0)
				st.takeItems(Kendells8stOrder, -1);
			for(int i = 4412; i <= 4417; i++)
				st.giveItems(i, 10, false);
			st.takeItems(KabooChiefs2stTorque, -1);
			if(st.getPlayer().getClassId().isMage())
			{
				st.giveItems(RED_SUNSET_STAFF, 1, false);
				st.giveItems(5790, 3000, false);
				st.playTutorialVoice("tutorial_voice_027");
			}
			else
			{
				st.giveItems(RED_SUNSET_SWORD, 1, false);
				st.giveItems(5789, 7000, false);
				st.playTutorialVoice("tutorial_voice_026");
			}
			st.unset("cond");
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond == 1 && st.getQuestItemsCount(KabooChiefs1stTorque) == 0)
		{
			if(npcId == KabooChiefUoph && st.getQuestItemsCount(Kendells1stOrder) > 0)
				st.giveItems(KabooChiefs1stTorque, 1, false);
			else if(npcId == KabooChiefKracha && st.getQuestItemsCount(Kendells2stOrder) > 0)
				st.giveItems(KabooChiefs1stTorque, 1, false);
			else if(npcId == KabooChiefBatoh && st.getQuestItemsCount(Kendells3stOrder) > 0)
				st.giveItems(KabooChiefs1stTorque, 1, false);
			else if(npcId == KabooChiefTanukia && st.getQuestItemsCount(Kendells4stOrder) > 0)
				st.giveItems(KabooChiefs1stTorque, 1, false);
			if(st.getQuestItemsCount(KabooChiefs1stTorque) > 0)
			{
				st.set("cond", "2");
				st.setState(STARTED);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(cond == 3 && st.getQuestItemsCount(KabooChiefs2stTorque) == 0)
		{
			if(npcId == KabooChiefTurel && st.getQuestItemsCount(Kendells5stOrder) > 0)
				st.giveItems(KabooChiefs2stTorque, 1, false);
			else if(npcId == KabooChiefRoko && st.getQuestItemsCount(Kendells6stOrder) > 0)
				st.giveItems(KabooChiefs2stTorque, 1, false);
			else if(npcId == KabooChiefKamut && st.getQuestItemsCount(Kendells7stOrder) > 0)
				st.giveItems(KabooChiefs2stTorque, 1, false);
			else if(npcId == KabooChiefMurtika && st.getQuestItemsCount(Kendells8stOrder) > 0)
				st.giveItems(KabooChiefs2stTorque, 1, false);
			if(st.getQuestItemsCount(KabooChiefs2stTorque) > 0)
			{
				st.set("cond", "4");
				st.setState(STARTED);
				st.playSound(SOUND_MIDDLE);
			}
		}
		return null;
	}
}
