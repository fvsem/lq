package quests._148_PathtoBecominganExaltedMercenary;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _148_PathtoBecominganExaltedMercenary extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPCs
	private static final int[] MERCENARY_ID = { 36481, 36482, 36483, 36484, 36485, 36486, 36487, 36488, 36489 };
	private static final int[] CATAPULT_ID = { 36499, 36500, 36501, 36502, 36503, 36504, 36505, 36506, 36507 };
	// Items
	private static final int ELITE_MERCENARY_CERTIFICATE = 13767;
	private static final int EXALTED_MERCENARY_CERTIFICATE = 13768;

	public _148_PathtoBecominganExaltedMercenary()
	{
		super(148, PARTY_ALL);
		addStartNpc(MERCENARY_ID);
		addKillId(CATAPULT_ID);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(st == null)
			return htmltext;

		if(ArrayUtil.arrayContains(MERCENARY_ID, npc.getNpcId()))
			if(event.equalsIgnoreCase("exalted-00b.htm"))
			{
				if(st.getQuestItemsCount(ELITE_MERCENARY_CERTIFICATE) == 0)
					st.giveItems(ELITE_MERCENARY_CERTIFICATE, 1);
			}
			else if(event.equalsIgnoreCase("exalted-03.htm"))
			{
				st.setState(STARTED);
				st.setCond(1);
				st.playSound(SOUND_ACCEPT);
				st.addNotifyOfPlayerKill();
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		String htmltext = "noquest";
		int cond = st.getCond();
		if(ArrayUtil.arrayContains(MERCENARY_ID, npc.getNpcId()))
			switch (st.getState())
			{
				case CREATED:
					if(player.getClan() != null && player.getClan().getHasCastle() > 0)
						htmltext = "castle.htm";
					else if(st.getQuestItemsCount(ELITE_MERCENARY_CERTIFICATE) == 1)
						htmltext = "exalted-01.htm";
					else if(player.isQuestCompleted("_147_PathtoBecominganEliteMercenary"))
						htmltext = "exalted-00a.htm";
					else
						htmltext = "exalted-00.htm";
					break;
				case STARTED:
					if(cond < 4)
						htmltext = "elite-04.htm";
					else if(cond == 4)
					{
						st.takeItems(ELITE_MERCENARY_CERTIFICATE, -1);
						st.giveItems(EXALTED_MERCENARY_CERTIFICATE, 1);
						st.playSound(SOUND_FINISH);
						st.exitCurrentQuest(false);
						htmltext = "exalted-05.htm";
					}
					break;
				case COMPLETED:
					htmltext = "completed";
					break;
			}
		return htmltext;
	}

	@Override
	public String onPlayerKill(L2Player killed, QuestState st)
	{
		int cond = st.getInt("cond");
		L2Player killer = st.getPlayer();

		if(killed == null || killer == null || !checkPlayers(killed, killer))
			return null;

		// убейте 30 врагов
		if(cond == 1 || cond == 3)
		{
			// Get
			int _kills = st.getInt("kills");
			// Increase
			_kills++;
			// Save
			st.set("kills", String.valueOf(_kills));
			// Check
			if(_kills >= 30)
			{
				st.setCond(++cond);
				st.removeNotifyOfPlayerKill();
			}
		}
		return null;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getCond();
		if(cond == 1 || cond == 2)
		{
			int catapults = st.getInt("catapults");
			catapults++;
			st.set("catapults", String.valueOf(catapults));
			if(catapults >= 2)
				st.setCond(3);
		}
		return null;
	}

	public static boolean checkPlayers(L2Player killed, L2Player killer)
	{
		if(killer.getTerritorySiege() < 0 || killed.getTerritorySiege() < 0 || killer.getTerritorySiege() == killed.getTerritorySiege())
			return false;
		if(killer.getParty() != null && killer.getParty() == killed.getParty())
			return false;
		if(killer.getClan() != null && killer.getClan() == killed.getClan())
			return false;
		if(killer.getAllyId() > 0 && killer.getAllyId() == killed.getAllyId())
			return false;
		if(killer.getLevel() < 61 || killed.getLevel() < 61)
			return false;
		return true;
	}
}
