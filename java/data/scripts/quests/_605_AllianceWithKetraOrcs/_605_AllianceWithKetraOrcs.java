package quests._605_AllianceWithKetraOrcs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _605_AllianceWithKetraOrcs extends Quest implements ScriptFile
{
	// ketra mobs
	private static final int[] KETRA_NPC_LIST = new int[] {
			21324,
			21325,
			21327,
			21328,
			21329,
			21331,
			21332,
			21334,
			21335,
			21336,
			21338,
			21339,
			21340,
			21342,
			21343,
			21344,
			21345,
			21346,
			21347,
			21348,
			21349 };

	// items
	private static final int MARK_OF_KETRA_ALLIANCE1 = 7211;
	private static final int MARK_OF_KETRA_ALLIANCE2 = 7212;
	private static final int MARK_OF_KETRA_ALLIANCE3 = 7213;
	private static final int MARK_OF_KETRA_ALLIANCE4 = 7214;
	private static final int MARK_OF_KETRA_ALLIANCE5 = 7215;
	private static final int VB_SOLDIER = 7216;
	private static final int VB_CAPTAIN = 7217;
	private static final int VB_GENERAL = 7218;
	private static final int TOTEM_OF_VALOR = 7219;
	private static final int TOTEM_OF_WISDOM = 7220;

	// hunt for soldier
	private static final int[] SOLDIER = new int[] { 21350, 21351, 21353, 21354, 21355 };

	// hunt for captain
	private static final int[] CAPTAIN = new int[] { 21357, 21358, 21360, 21361, 21362, 21369, 21370 };

	// hunt for general
	private static final int[] GENERAL = new int[] { 21364, 21365, 21366, 21368, 21371, 21372, 21373, 21374, 21375 };

	private static final int Wahkan = 31371;

	public _605_AllianceWithKetraOrcs()
	{
		super(605, 0);

		addStartNpc(Wahkan);
		addTalkId(Wahkan);

		// ketra mobs
		addKillId(KETRA_NPC_LIST);
		// hunt for soldier
		addKillId(SOLDIER);
		// hunt for captain
		addKillId(CAPTAIN);
		// hunt for general
		addKillId(GENERAL);

		addQuestItem(VB_SOLDIER);
		addQuestItem(VB_CAPTAIN);
		addQuestItem(VB_GENERAL);
	}

	private boolean isKetraNpc(int npc)
	{
		return ArrayUtil.arrayContains(KETRA_NPC_LIST, npc);
	}

	private boolean isSoldier(int npc)
	{
		return ArrayUtil.arrayContains(SOLDIER, npc);
	}

	private boolean isCaptain(int npc)
	{
		return ArrayUtil.arrayContains(CAPTAIN, npc);
	}

	private boolean isGeneral(int npc)
	{
		return ArrayUtil.arrayContains(GENERAL, npc);
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public void onAbort(QuestState st)
	{
		takeAllMarks(st);
		st.set("cond", "0");
		st.getPlayer().updateKetraVarka();
		st.playSound(SOUND_MIDDLE);
	}

	private static void takeAllMarks(QuestState st)
	{
		st.takeItems(MARK_OF_KETRA_ALLIANCE1, -1);
		st.takeItems(MARK_OF_KETRA_ALLIANCE2, -1);
		st.takeItems(MARK_OF_KETRA_ALLIANCE3, -1);
		st.takeItems(MARK_OF_KETRA_ALLIANCE4, -1);
		st.takeItems(MARK_OF_KETRA_ALLIANCE5, -1);
	}

	private void checkMarks(QuestState st)
	{
		if(st.getInt("cond") == 0)
			return;
		if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE5) > 0)
			st.set("cond", "6");
		else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE4) > 0)
			st.set("cond", "5");
		else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE3) > 0)
			st.set("cond", "4");
		else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE2) > 0)
			st.set("cond", "3");
		else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE1) > 0)
			st.set("cond", "2");
		else
			st.set("cond", "1");
	}

	private static boolean CheckNextLevel(QuestState st, int soilder_count, int capitan_count, int general_count, int other_item, boolean take)
	{
		if(soilder_count > 0 && st.getQuestItemsCount(VB_SOLDIER) < soilder_count)
			return false;
		if(capitan_count > 0 && st.getQuestItemsCount(VB_CAPTAIN) < capitan_count)
			return false;
		if(general_count > 0 && st.getQuestItemsCount(VB_GENERAL) < general_count)
			return false;
		if(other_item > 0 && st.getQuestItemsCount(other_item) < 1)
			return false;

		if(take)
		{
			if(soilder_count > 0)
				st.takeItems(VB_SOLDIER, soilder_count);
			if(capitan_count > 0)
				st.takeItems(VB_CAPTAIN, capitan_count);
			if(general_count > 0)
				st.takeItems(VB_GENERAL, general_count);
			if(other_item > 0)
				st.takeItems(other_item, 1);
			takeAllMarks(st);
		}
		return true;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("first-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			return event;
		}

		checkMarks(st);
		int cond = st.getInt("cond");

		if(event.equalsIgnoreCase("first-have-2.htm") && cond == 1 && CheckNextLevel(st, 100, 0, 0, 0, true))
		{
			st.giveItems(MARK_OF_KETRA_ALLIANCE1, 1);
			st.set("cond", "2");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("second-have-2.htm") && cond == 2 && CheckNextLevel(st, 200, 100, 0, 0, true))
		{
			st.giveItems(MARK_OF_KETRA_ALLIANCE2, 1);
			st.set("cond", "3");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("third-have-2.htm") && cond == 3 && CheckNextLevel(st, 300, 200, 100, 0, true))
		{
			st.giveItems(MARK_OF_KETRA_ALLIANCE3, 1);
			st.set("cond", "4");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("fourth-have-2.htm") && cond == 4 && CheckNextLevel(st, 300, 300, 200, TOTEM_OF_VALOR, true))
		{
			st.giveItems(MARK_OF_KETRA_ALLIANCE4, 1);
			st.set("cond", "5");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("fifth-have-2.htm") && cond == 5 && CheckNextLevel(st, 400, 400, 200, TOTEM_OF_WISDOM, true))
		{
			st.giveItems(MARK_OF_KETRA_ALLIANCE5, 1);
			st.set("cond", "6");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("quit-2.htm"))
		{
			takeAllMarks(st);
			st.set("cond", "0");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		if(st.getPlayer().getVarka() > 0)
		{
			st.exitCurrentQuest(true);
			return "isvarka.htm";
		}
		checkMarks(st);
		if(st.getState() == CREATED)
			st.set("cond", "0");
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 31371)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 74)
				{
					st.exitCurrentQuest(true);
					return "no-level.htm";
				}
				return "first.htm";
			}
			if(cond == 1)
				return CheckNextLevel(st, 100, 0, 0, 0, false) ? "first-have.htm" : "first-havenot.htm";
			if(cond == 2)
				return CheckNextLevel(st, 200, 100, 0, 0, false) ? "second-have.htm" : "second.htm";
			if(cond == 3)
				return CheckNextLevel(st, 300, 200, 100, 0, false) ? "third-have.htm" : "third.htm";
			if(cond == 4)
				return CheckNextLevel(st, 300, 300, 200, TOTEM_OF_VALOR, false) ? "fourth-have.htm" : "fourth.htm";
			if(cond == 5)
				return CheckNextLevel(st, 400, 400, 200, TOTEM_OF_WISDOM, false) ? "fifth-have.htm" : "fifth.htm";
			if(cond == 6)
				return "high.htm";
		}
		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(isKetraNpc(npcId))
			if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE5) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_KETRA_ALLIANCE4, 1);
				st.getPlayer().updateKetraVarka();
			}
			else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE4) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_KETRA_ALLIANCE3, 1);
				st.getPlayer().updateKetraVarka();
			}
			else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE3) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_KETRA_ALLIANCE2, 1);
				st.getPlayer().updateKetraVarka();
			}
			else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE2) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_KETRA_ALLIANCE1, 1);
				st.getPlayer().updateKetraVarka();
			}
			else if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE1) > 0)
			{
				takeAllMarks(st);
				st.getPlayer().updateKetraVarka();
			}
			else if(st.getPlayer().getKetra() > 0)
			{
				st.getPlayer().updateKetraVarka();
				st.exitCurrentQuest(true);
				return "quit-2.htm";
			}

		if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE5) > 0)
			return null;

		if(isSoldier(npcId))
		{
			if(st.getInt("cond") > 0)
			{
				st.rollAndGive(VB_SOLDIER, 1, 60);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(isCaptain(npcId))
		{
			if(st.getInt("cond") > 1)
			{
				st.rollAndGive(VB_CAPTAIN, 1, 70);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(isGeneral(npcId))
			if(st.getInt("cond") > 2)
			{
				st.rollAndGive(VB_GENERAL, 1, 80);
				st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
