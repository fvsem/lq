package quests._691_MatrasSuspiciousRequest;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _691_MatrasSuspiciousRequest extends Quest implements ScriptFile
{
	// NPC
	private final static int MATRAS = 32245;
	private final static int LABYRINTH_CAPTAIN = 22368;

	// Items
	private final static int RED_STONE = 10372;
	private final static int RED_STONES_COUNT = 744;
	private final static int DYNASTIC_ESSENCE_II = 10413;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _691_MatrasSuspiciousRequest()
	{
		super(691, PARTY_ONE);

		addStartNpc(MATRAS);
		addQuestItem(RED_STONE);
		addKillId(new int[] { 22363, 22364, 22365, 22366, 22367, LABYRINTH_CAPTAIN, 22369, 22370, 22371, 22372 });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32245-03.htm"))
			if(st.getState() == CREATED)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
		if(event.equalsIgnoreCase("DynasticEssenceII"))
			if(st.getQuestItemsCount(RED_STONE) >= RED_STONES_COUNT)
			{
				st.takeItems(RED_STONE, RED_STONES_COUNT);
				st.giveItems(DYNASTIC_ESSENCE_II, 1, false);
				htmltext = "32245-05.htm";
			}
			else if(st.getQuestItemsCount(RED_STONE) > 0)
				htmltext = "32245-06.htm";
			else
				htmltext = "32245-07.htm";
		if(event.equalsIgnoreCase("adena"))
		{
			if(st.getQuestItemsCount(RED_STONE) > 0)
				st.giveItems(57, 10000L * st.getQuestItemsCount(RED_STONE), false);
			st.takeItems(RED_STONE, -1);
			htmltext = "32245-07.htm";
		}
		if(event.equalsIgnoreCase("finish"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			return null;
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int _state = st.getState();
		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() < 76)
			{
				htmltext = "32245-00.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "32245-01.htm";
		}
		else if(_state == STARTED && st.getInt("cond") == 1)
			if(st.getQuestItemsCount(RED_STONE) >= RED_STONES_COUNT)
				htmltext = "32245-05.htm";
			else if(st.getQuestItemsCount(RED_STONE) > 0)
				htmltext = "32245-06.htm";
			else
				htmltext = "32245-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1)
			st.rollAndGive(RED_STONE, 1, 1, RED_STONES_COUNT, npc.getNpcId() == LABYRINTH_CAPTAIN ? 50 : 30);
		return null;
	}
}
