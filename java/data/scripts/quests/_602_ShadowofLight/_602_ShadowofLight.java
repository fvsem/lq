package quests._602_ShadowofLight;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Shadowof Light
 */

public class _602_ShadowofLight extends Quest implements ScriptFile
{
	// NPC
	private static final int ARGOS = 31683;
	// Items
	private static final int ADENA = 57;
	// Quest Item
	private static final int EYE_OF_DARKNESS = 7189;
	// Bonus
	private static final int[][] REWARDS = {
			{ 6699, 40000, 120000, 20000, 1, 19 },
			{ 6698, 60000, 110000, 15000, 20, 39 },
			{ 6700, 40000, 150000, 10000, 40, 49 },
			{ 0, 100000, 140000, 11250, 50, 100 } };
	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	private static final int[][] DROPLIST_COND = { { 1, 2, 21304, 0, EYE_OF_DARKNESS, 100, 40, 1 }, { 1, 2, 21299, 0, EYE_OF_DARKNESS, 100, 35, 1 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 602: Shadowof Light");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _602_ShadowofLight()
	{
		super(602, -1);

		addStartNpc(ARGOS);

		addTalkId(ARGOS);

		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);
		addQuestItem(EYE_OF_DARKNESS);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31683-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31683-04.htm"))
		{
			st.takeItems(EYE_OF_DARKNESS, -1);
			int random = Rnd.get(100) + 1;
			for(int i = 0; i < REWARDS.length; i++)
				if(REWARDS[i][4] <= random && random <= REWARDS[i][5])
				{
					st.giveItems(ADENA, REWARDS[i][1]);
					st.addExpAndSp(REWARDS[i][2], REWARDS[i][3]);
					if(REWARDS[i][0] != 0)
						st.giveItems(REWARDS[i][0], 3);
				}
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == ARGOS)
			if(cond == 0)
				if(st.getPlayer().getLevel() < 68)
				{
					htmltext = "31683-00.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31683-01.htm";
			else if(cond == 1)
				htmltext = "31683-02r.htm";
			else if(cond == 2 && st.getQuestItemsCount(EYE_OF_DARKNESS) == 100)
				htmltext = "31683-03.htm";
			else
			{
				htmltext = "31683-02r.htm";
				st.set("cond", "1");
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}
}
