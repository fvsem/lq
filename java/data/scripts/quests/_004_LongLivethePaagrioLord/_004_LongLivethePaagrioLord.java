package quests._004_LongLivethePaagrioLord;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Рейты не учитываются, награда не стекуемая
 */

public class _004_LongLivethePaagrioLord extends Quest implements ScriptFile
{
	int HONEY_KHANDAR = 1541;
	int BEAR_FUR_CLOAK = 1542;
	int BLOODY_AXE = 1543;
	int ANCESTOR_SKULL = 1544;
	int SPIDER_DUST = 1545;
	int DEEP_SEA_ORB = 1546;

	int[][] NPC_GIFTS = { { 30585, BEAR_FUR_CLOAK }, { 30566, HONEY_KHANDAR }, { 30562, BLOODY_AXE }, { 30560, ANCESTOR_SKULL }, { 30559, SPIDER_DUST }, { 30587, DEEP_SEA_ORB } };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _004_LongLivethePaagrioLord()
	{
		super(4, -1);
		addStartNpc(30578);

		addTalkId(new int[] { 30559, 30560, 30562, 30566, 30578, 30585, 30587 });

		addQuestItem(new int[] { SPIDER_DUST, ANCESTOR_SKULL, BLOODY_AXE, HONEY_KHANDAR, BEAR_FUR_CLOAK, DEEP_SEA_ORB });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30578-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 30578)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 3)
				{
					htmltext = "30578-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 2)
					htmltext = "30578-02.htm";
				else
				{
					htmltext = "30578-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30578-04.htm";
			else if(cond == 2)
			{
				htmltext = "30578-06.htm";
				for(int[] item : NPC_GIFTS)
					st.takeItems(item[1], -1);
				st.giveItems(4, 1, false);
				st.unset("cond");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(cond == 1)
			for(int Id[] : NPC_GIFTS)
				if(Id[0] == npcId)
				{
					int item = Id[1];
					if(st.getQuestItemsCount(item) > 0)
						htmltext = npc + "-02.htm";
					else
					{
						st.giveItems(item, 1, false);
						htmltext = npc + "-01.htm";
						int count = 0;
						for(int[] item1 : NPC_GIFTS)
							count += st.getQuestItemsCount(item1[1]);
						if(count == 6)
						{
							st.set("cond", "2");
							st.playSound(SOUND_MIDDLE);
						}
						else
							st.playSound(SOUND_ITEMGET);
					}
					return htmltext;
				}
		return htmltext;
	}
}
