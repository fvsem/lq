package quests._351_BlackSwan;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _351_BlackSwan extends Quest implements ScriptFile
{
	// NPC
	private static final int Gosta = 30916;
	private static final int Heine = 30969;
	private static final int Ferris = 30847;
	// Items
	private static final int ADENA = 57;
	private static final int ORDER_OF_GOSTA = 4296;
	private static final int LIZARD_FANG = 4297;
	private static final int BARREL_OF_LEAGUE = 4298;
	private static final int BILL_OF_IASON_HEINE = 4310;
	// Chances
	private static final int CHANCE = 80;
	private static final int CHANCE2 = 7;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 351: BlackSwan");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _351_BlackSwan()
	{
		super(351, -1);
		addStartNpc(Gosta);
		addTalkId(Gosta);
		addTalkId(Heine);
		addTalkId(Ferris);
		addKillId(20784);
		addKillId(20785);
		addKillId(21639);
		addKillId(21640);
		addKillId(21642);
		addKillId(21643);
		addQuestItem(ORDER_OF_GOSTA, LIZARD_FANG, BARREL_OF_LEAGUE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		long amount = st.getQuestItemsCount(LIZARD_FANG);
		long amount2 = st.getQuestItemsCount(BARREL_OF_LEAGUE);
		if(event.equalsIgnoreCase("30916-03.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.giveItems(ORDER_OF_GOSTA, 1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30969-02a.htm") && amount > 0)
		{
			htmltext = "30969-02.htm";
			st.giveItems(ADENA, amount * 20);
			st.takeItems(LIZARD_FANG, -1);
		}
		else if(event.equalsIgnoreCase("30969-03a.htm") && amount2 > 0)
		{
			htmltext = "30969-03.htm";
			st.set("cond", "2");
			st.giveItems(BILL_OF_IASON_HEINE, amount2);
			st.takeItems(BARREL_OF_LEAGUE, -1);
		}
		else if(event.equalsIgnoreCase("30969-01.htm") && st.getInt("cond") == 2)
			htmltext = "30969-04.htm";
		else if(event.equalsIgnoreCase("5"))
		{
			st.exitCurrentQuest(true);
			st.playSound(SOUND_FINISH);
			htmltext = "";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == Gosta)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 32)
					htmltext = "30916-01.htm";
				else
				{
					htmltext = "30916-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond >= 1)
				htmltext = "30916-04.htm";
		if(npcId == Heine)
		{
			if(cond == 1)
				htmltext = "30969-01.htm";
			if(cond == 2)
				htmltext = "30969-04.htm";
		}
		if(npcId == Ferris)
			htmltext = "30847.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(Rnd.chance(CHANCE))
		{
			st.giveItems(LIZARD_FANG, Rnd.get(1, 5));
			st.playSound(SOUND_ITEMGET);
		}
		if(Rnd.chance(CHANCE2))
			st.giveItems(BARREL_OF_LEAGUE, 1);
		return null;
	}
}
