package quests._133_ThatsBloodyHot;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Thats Bloody Hot
 * 
 * @author PainKiller
 * @Las0Fixes By Felixx
 */
public class _133_ThatsBloodyHot extends Quest implements ScriptFile
{
	// NPC
	private final static int KANIS = 32264;
	private final static int GALATE = 32292;

	// ITEMS
	private final static int CRYSTAL_SAMPLE = 9785;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _133_ThatsBloodyHot()
	{
		super(133, 0);

		addStartNpc(KANIS);
		addTalkId(KANIS);
		addTalkId(GALATE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32264-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32264-07.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.giveItems(CRYSTAL_SAMPLE, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32292-03.htm"))
			st.takeItems(CRYSTAL_SAMPLE, -1);
		else if(event.equalsIgnoreCase("32292-04.htm"))
		{
			st.addExpAndSp(670612, 0);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == KANIS && cond == 0)
		{
			QuestState BirdInACage = st.getPlayer().getQuestState("_131_BirdInACage");
			if(BirdInACage != null)
				if(BirdInACage.isCompleted())
					htmltext = "32264-01.htm";
				else
				{
					htmltext = "32264-00.htm";
					st.exitCurrentQuest(true);
				}
		}
		else if(npcId == GALATE && cond == 2)
			if(st.getQuestItemsCount(CRYSTAL_SAMPLE) >= 1)
				htmltext = "32292-01.htm";
		return htmltext;
	}
}
