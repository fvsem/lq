package quests._017_LightAndDarkness;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _017_LightAndDarkness extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _017_LightAndDarkness()
	{
		super(17, -1);

		addStartNpc(31517);

		addTalkId(31517);

		addTalkId(31508);
		addTalkId(31509);
		addTalkId(31510);
		addTalkId(31511);

		addQuestItem(7168);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31517-04.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.giveItems(7168, 4);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31508-02.htm"))
		{
			st.takeItems(7168, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("31509-02.htm"))
		{
			st.takeItems(7168, 1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("31510-02.htm"))
		{
			st.takeItems(7168, 1);
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("31511-02.htm"))
		{
			st.takeItems(7168, 1);
			st.set("cond", "5");
			st.playSound(SOUND_MIDDLE);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 31517)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 61)
					htmltext = "31517-02.htm";
				else
				{
					htmltext = "31517-03.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond > 0 && cond < 5 && st.getQuestItemsCount(7168) > 0)
				htmltext = "31517-05.htm";
			else if(cond > 0 && cond < 5 && st.getQuestItemsCount(7168) == 0)
			{
				htmltext = "31517-06.htm";
				st.set("cond", "0");
				st.exitCurrentQuest(false);
			}
			else if(cond == 5 && st.getQuestItemsCount(7168) == 0)
			{
				htmltext = "31517-07.htm";
				st.addExpAndSp(105527, 0);
				st.set("cond", "0");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == 31508)
		{
			if(cond == 1)
				if(st.getQuestItemsCount(7168) != 0)
					htmltext = "31508-01.htm";
				else
					htmltext = "31508-03.htm";
			else if(cond == 2)
				htmltext = "31508-04.htm";
		}
		else if(npcId == 31509)
		{
			if(cond == 2)
				if(st.getQuestItemsCount(7168) != 0)
					htmltext = "31509-01.htm";
				else
					htmltext = "31509-03.htm";
			else if(cond == 3)
				htmltext = "31509-04.htm";
		}
		else if(npcId == 31510)
		{
			if(cond == 3)
				if(st.getQuestItemsCount(7168) != 0)
					htmltext = "31510-01.htm";
				else
					htmltext = "31510-03.htm";
			else if(cond == 4)
				htmltext = "31510-04.htm";
		}
		else if(npcId == 31511)
			if(cond == 4)
				if(st.getQuestItemsCount(7168) != 0)
					htmltext = "31511-01.htm";
				else
					htmltext = "31511-03.htm";
			else if(cond == 5)
				htmltext = "31511-04.htm";
		return htmltext;
	}
}
