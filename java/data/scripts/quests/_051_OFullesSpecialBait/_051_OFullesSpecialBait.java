package quests._051_OFullesSpecialBait;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _051_OFullesSpecialBait extends Quest implements ScriptFile
{
	private static int OFulle = 31572;
	private static int FetteredSoul = 20552;

	private static int LostBaitIngredient = 7622;
	private static int IcyAirFishingLure = 7611;

	Integer FishSkill = 1315;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _051_OFullesSpecialBait()
	{
		super(51, -1);

		addStartNpc(OFulle);

		addTalkId(OFulle);

		addKillId(FetteredSoul);

		addQuestItem(LostBaitIngredient);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31572-04.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31572-07.htm"))
			if(st.getQuestItemsCount(LostBaitIngredient) < 100)
				htmltext = "31572-08.htm";
			else
			{
				st.unset("cond");
				st.takeItems(LostBaitIngredient, -1);
				st.giveItems(IcyAirFishingLure, 4);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int id = st.getState();
		if(npcId == OFulle)
			if(id == CREATED)
			{
				if(st.getPlayer().getLevel() < 36)
				{
					htmltext = "31572-03.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getSkillLevel(FishSkill) >= 11)
					htmltext = "31572-01.htm";
				else
				{
					htmltext = "31572-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 || cond == 2)
				if(st.getQuestItemsCount(LostBaitIngredient) < 100)
				{
					htmltext = "31572-06.htm";
					st.set("cond", "1");
				}
				else
					htmltext = "31572-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == FetteredSoul && st.getInt("cond") == 1)
			if(st.getQuestItemsCount(LostBaitIngredient) < 100 && Rnd.chance(30))
			{
				st.giveItems(LostBaitIngredient, 1);
				if(st.getQuestItemsCount(LostBaitIngredient) == 100)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
