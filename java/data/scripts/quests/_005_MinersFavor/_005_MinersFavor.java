package quests._005_MinersFavor;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Рейты не учитываются, награда не стекуемая
 */
public class _005_MinersFavor extends Quest implements ScriptFile
{
	// NPC
	public final int BOLTER = 30554;
	public final int SHARI = 30517;
	public final int GARITA = 30518;
	public final int REED = 30520;
	public final int BRUNON = 30526;
	// QuestItem
	public final int BOLTERS_LIST = 1547;
	public final int MINING_BOOTS = 1548;
	public final int MINERS_PICK = 1549;
	public final int BOOMBOOM_POWDER = 1550;
	public final int REDSTONE_BEER = 1551;
	public final int BOLTERS_SMELLY_SOCKS = 1552;
	// Item
	public final int NECKLACE = 906;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _005_MinersFavor()
	{
		super(5, -1);
		addStartNpc(BOLTER);
		addTalkId(SHARI);
		addTalkId(GARITA);
		addTalkId(REED);
		addTalkId(BRUNON);

		addQuestItem(new int[] { BOLTERS_LIST, BOLTERS_SMELLY_SOCKS, MINING_BOOTS, MINERS_PICK, BOOMBOOM_POWDER, REDSTONE_BEER });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30554-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(BOLTERS_LIST, 1, false);
			st.giveItems(BOLTERS_SMELLY_SOCKS, 1, false);
		}
		else if(event.equalsIgnoreCase("30526-02.htm"))
		{
			st.takeItems(BOLTERS_SMELLY_SOCKS, -1);
			st.giveItems(MINERS_PICK, 1, false);
			if(st.getQuestItemsCount(BOLTERS_LIST) > 0 && st.getQuestItemsCount(MINING_BOOTS) + st.getQuestItemsCount(MINERS_PICK) + st.getQuestItemsCount(BOOMBOOM_POWDER) + st.getQuestItemsCount(REDSTONE_BEER) == 4)
			{
				st.set("cond", "2");
				st.playSound(SOUND_MIDDLE);
			}
			else
				st.playSound(SOUND_ITEMGET);

		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == BOLTER)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 2)
					htmltext = "30554-02.htm";
				else
				{
					htmltext = "30554-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30554-04.htm";
			else if(cond == 2 && st.getQuestItemsCount(MINING_BOOTS) + st.getQuestItemsCount(MINERS_PICK) + st.getQuestItemsCount(BOOMBOOM_POWDER) + st.getQuestItemsCount(REDSTONE_BEER) == 4)
			{
				htmltext = "30554-06.htm";
				st.takeItems(MINING_BOOTS, -1);
				st.takeItems(MINERS_PICK, -1);
				st.takeItems(BOOMBOOM_POWDER, -1);
				st.takeItems(REDSTONE_BEER, -1);
				st.takeItems(BOLTERS_LIST, -1);
				st.giveItems(NECKLACE, 1, false);
				st.unset("cond");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(cond == 1 && st.getQuestItemsCount(BOLTERS_LIST) > 0)
		{
			if(npcId == SHARI)
			{
				if(st.getQuestItemsCount(BOOMBOOM_POWDER) == 0)
				{
					htmltext = "30517-01.htm";
					st.giveItems(BOOMBOOM_POWDER, 1, false);
					st.playSound(SOUND_ITEMGET);
				}
				else
					htmltext = "30517-02.htm";
			}
			else if(npcId == GARITA)
			{
				if(st.getQuestItemsCount(MINING_BOOTS) == 0)
				{
					htmltext = "30518-01.htm";
					st.giveItems(MINING_BOOTS, 1, false);
					st.playSound(SOUND_ITEMGET);
				}
				else
					htmltext = "30518-02.htm";
			}
			else if(npcId == REED)
			{
				if(st.getQuestItemsCount(REDSTONE_BEER) == 0)
				{
					htmltext = "30520-01.htm";
					st.giveItems(REDSTONE_BEER, 1, false);
					st.playSound(SOUND_ITEMGET);
				}
				else
					htmltext = "30520-02.htm";
			}
			else if(npcId == BRUNON && st.getQuestItemsCount(BOLTERS_SMELLY_SOCKS) > 0)
				if(st.getQuestItemsCount(MINERS_PICK) == 0)
					htmltext = "30526-01.htm";
				else
					htmltext = "30526-03.htm";
			if(st.getQuestItemsCount(BOLTERS_LIST) > 0 && st.getQuestItemsCount(MINING_BOOTS) + st.getQuestItemsCount(MINERS_PICK) + st.getQuestItemsCount(BOOMBOOM_POWDER) + st.getQuestItemsCount(REDSTONE_BEER) == 4)
			{
				st.set("cond", "2");
				st.playSound(SOUND_MIDDLE);
			}
		}
		return htmltext;
	}
}
