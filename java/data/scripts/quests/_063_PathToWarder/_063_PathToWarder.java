package quests._063_PathToWarder;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _063_PathToWarder extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int Sione = 32195;
	private static final int Gobie = 32198;
	private static final int Patrol = 20053;
	private static final int Novice = 20782;
	private static final int Bathis = 30332;
	private static final int Tobias = 30297;
	private static final int Tak = 27337;
	private static final int Maille = 20919;
	private static final int Maille_scout = 20920;
	private static final int Maille_guard = 20921;

	private static final int OlMahumOrders = 9762;
	private static final int OlMahumOrganizationChart = 9763;
	private static final int GobiesOrders = 9764;
	private static final int LettertotheHumans = 9765;
	private static final int HumansReply = 9766;
	private static final int LettertotheDarkElves = 9767;
	private static final int DarkElvesReply = 9768;
	private static final int ReporttoSione = 9769;
	private static final int EmptySoulCrystal = 9770; // empty
	private static final int TaksCapturedSoul = 9771;
	private static final int SteelrazorEvaluation = 9772;

	public _063_PathToWarder()
	{
		super(63, -1);
		addStartNpc(Sione);
		addTalkId(Sione);
		addTalkId(Gobie);
		addTalkId(Bathis);
		addTalkId(Tobias);
		addKillId(Patrol);
		addKillId(Novice);
		addKillId(Tak);
		addKillId(Maille);
		addKillId(Maille_scout);
		addKillId(Maille_guard);
		addQuestItem(new int[] { OlMahumOrganizationChart, OlMahumOrders, TaksCapturedSoul, EmptySoulCrystal });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("32195-02.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		if(event.equals("32195-03.htm"))
			st.set("cond", "2");
		else if(event.equals("30332-03.htm"))
		{
			st.takeItems(LettertotheHumans, 1);
			st.giveItems(HumansReply, 1);
			st.set("cond", "6");
		}
		else if(event.equals("32198-05.htm"))
		{
			st.takeItems(HumansReply, 1);
			st.giveItems(LettertotheDarkElves, 1);
			st.set("cond", "7");
		}
		else if(event.equals("30297-04.htm"))
		{
			st.takeItems(LettertotheDarkElves, 1);
			st.giveItems(DarkElvesReply, 1);
			st.set("cond", "8");
		}
		else if(event.equals("32198-07.htm"))
		{
			st.takeItems(DarkElvesReply, 1);
			st.giveItems(ReporttoSione, 1);
			st.set("cond", "9");
		}
		else if(event.equals("32198-10.htm"))
		{
			st.takeItems(EmptySoulCrystal, 1);
			st.set("cond", "11");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int id = st.getState();
		if(npcId == Sione)
			if(id == CREATED)
			{
				if(st.getPlayer().getClassId() != ClassId.femaleSoldier)
				{
					htmltext = "for female kamael warriors only";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 18)
				{
					htmltext = "no level 18";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "32195-01.htm";
			}
			else
			{
				if(cond == 1)
					htmltext = "32195-02.htm";
				if(cond == 3)
					if(st.getQuestItemsCount(OlMahumOrders) < 10 && st.getQuestItemsCount(OlMahumOrganizationChart) < 5)
						htmltext = "32195-no.htm";
					else
					{
						htmltext = "32195-04.htm";
						st.set("cond", "4");
						st.takeItems(OlMahumOrders, -1);
						st.takeItems(OlMahumOrganizationChart, -1);
						st.giveItems(GobiesOrders, 1);
					}
				if(cond == 9)
				{
					st.takeItems(ReporttoSione, 1);
					st.set("cond", "10");
					htmltext = "32195-05.htm";
				}
			}
		if(npcId == Gobie)
		{
			if(cond == 4)
				if(st.getQuestItemsCount(GobiesOrders) < 1)
					htmltext = "no orders from sionee";
				else
				{
					htmltext = "32198-02.htm";
					st.takeItems(GobiesOrders, 1);
					st.giveItems(LettertotheHumans, 1);
					st.set("cond", "5");
				}
			if(cond == 6)
				htmltext = "32198-03.htm";
			if(cond == 8)
				htmltext = "32198-06.htm";
			if(cond == 10)
				htmltext = "32198-08.htm";
			if(cond == 11)
				htmltext = "no soul, kill Tak";
			if(cond == 12)
				if(st.getQuestItemsCount(TaksCapturedSoul) > 0)
				{
					st.takeItems(TaksCapturedSoul, 1);
					if(st.getPlayer().getClassId().getLevel() == 1)
					{
						st.giveItems(SteelrazorEvaluation, 1);
						if(!st.getPlayer().getVarB("prof1"))
						{
							st.getPlayer().setVar("prof1", "1");
							st.addExpAndSp(160267, 11023, true);
                                                        st.giveItems(57, 163800, true);
						}
					}

					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(true);
					htmltext = "32198-11.htm";
				}
				else
					htmltext = "no soul, kill Tak";
		}
		if(npcId == Bathis && cond == 5)
			htmltext = "30332-01.htm";
		if(npcId == Tobias && cond == 7)
			htmltext = "30297-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond == 2)
		{
			if(npcId == Patrol)
			{
				st.giveItems(OlMahumOrganizationChart, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(npcId == Novice)
			{
				st.giveItems(OlMahumOrders, 1);
				st.playSound(SOUND_ITEMGET);
			}
			if(st.getQuestItemsCount(OlMahumOrders) > 9 && st.getQuestItemsCount(OlMahumOrganizationChart) > 4)
			{
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
			}
		}
		if(cond == 11)
			if((npcId == Maille || npcId == Maille_scout || npcId == Maille_guard) && Rnd.chance(20))
				st.addSpawn(Tak);
			else if(npcId == Tak)
			{
				st.takeItems(EmptySoulCrystal, 1);
				st.giveItems(TaksCapturedSoul, 1);
				st.set("cond", "12");
				st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
