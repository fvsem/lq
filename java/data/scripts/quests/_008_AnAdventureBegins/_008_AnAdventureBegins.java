package quests._008_AnAdventureBegins;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _008_AnAdventureBegins extends Quest implements ScriptFile
{
	int JASMINE = 30134;
	int ROSELYN = 30355;
	int HARNE = 30144;

	int ROSELYNS_NOTE = 7573;

	int SCROLL_OF_ESCAPE_GIRAN = 7126;
	int MARK_OF_TRAVELER = 7570;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _008_AnAdventureBegins()
	{
		super(8, -1);

		addStartNpc(JASMINE);

		addTalkId(JASMINE);
		addTalkId(ROSELYN);
		addTalkId(HARNE);

		addQuestItem(ROSELYNS_NOTE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30134-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30355-02.htm"))
		{
			st.giveItems(ROSELYNS_NOTE, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30144-02.htm"))
		{
			st.takeItems(ROSELYNS_NOTE, -1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30134-06.htm"))
		{
			st.giveItems(SCROLL_OF_ESCAPE_GIRAN, 1);
			st.giveItems(MARK_OF_TRAVELER, 1);
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == JASMINE)
		{
			if(cond == 0 && st.getPlayer().getRace().ordinal() == 2)
				if(st.getPlayer().getLevel() >= 3)
					htmltext = "30134-02.htm";
				else
				{
					htmltext = "30134-01.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "30134-04.htm";
			else if(cond == 3)
				htmltext = "30134-05.htm";
		}
		else if(npcId == ROSELYN)
		{
			if(st.getQuestItemsCount(ROSELYNS_NOTE) == 0)
				htmltext = "30355-01.htm";
			else
				htmltext = "30355-03.htm";
		}
		else if(npcId == HARNE)
			if(cond == 2 && st.getQuestItemsCount(ROSELYNS_NOTE) > 0)
				htmltext = "30144-01.htm";
			else if(cond == 2 && st.getQuestItemsCount(ROSELYNS_NOTE) == 0)
				htmltext = "30144-havent.htm";
			else if(cond == 3)
				htmltext = "30144-02r.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		return null;
	}
}
