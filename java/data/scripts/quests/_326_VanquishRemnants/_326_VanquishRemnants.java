package quests._326_VanquishRemnants;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Vanquish Remnants
 */

public class _326_VanquishRemnants extends Quest implements ScriptFile
{
	// NPC
	private static final int Leopold = 30435;
	// Quest Items
	private static final int RedCrossBadge = 1359;
	private static final int BlueCrossBadge = 1360;
	private static final int BlackCrossBadge = 1361;
	// Items
	private static final int BlackLionMark = 1369;
	// MOB
	private static final int OlMahumPatrol = 30425;
	private static final int OlMahumGuard = 20058;
	private static final int OlMahumRecruit = 20437;
	private static final int OlMahumStraggler = 20061;
	private static final int OlMahumShooter = 20063;
	private static final int OlMahumSupplier = 20436;
	private static final int OlMahumCaptain = 20066;
	private static final int OlMahumGeneral = 20438;
	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	public final int[][] DROPLIST_COND = {
			{ 1, 0, OlMahumPatrol, 0, RedCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumGuard, 0, RedCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumRecruit, 0, RedCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumStraggler, 0, BlueCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumShooter, 0, BlueCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumSupplier, 0, BlueCrossBadge, 0, 25, 1 },
			{ 1, 0, OlMahumCaptain, 0, BlackCrossBadge, 0, 35, 1 },
			{ 1, 0, OlMahumGeneral, 0, BlackCrossBadge, 0, 25, 1 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 326: Vanquish Remnants");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _326_VanquishRemnants()
	{
		super(326, -1);
		addStartNpc(Leopold);
		addTalkId(Leopold);
		// Mob Drop
		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);
		addQuestItem(RedCrossBadge);
		addQuestItem(BlueCrossBadge);
		addQuestItem(BlackCrossBadge);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30435-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30435-03.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == Leopold)
			if(st.getPlayer().getLevel() < 21)
			{
				htmltext = "30435-01.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 0)
				htmltext = "30435-02.htm";
			else if(cond == 1 && st.getQuestItemsCount(RedCrossBadge) == 0 && st.getQuestItemsCount(BlueCrossBadge) == 0 && st.getQuestItemsCount(BlackCrossBadge) == 0)
				htmltext = "30435-04.htm";
			else if(cond == 1)
			{
				if(st.getQuestItemsCount(RedCrossBadge) + st.getQuestItemsCount(BlueCrossBadge) + st.getQuestItemsCount(BlackCrossBadge) >= 100)
				{
					if(st.getQuestItemsCount(BlackLionMark) == 0)
					{
						htmltext = "30435-09.htm";
						st.giveItems(BlackLionMark, 1);
					}
					else
						htmltext = "30435-06.htm";
				}
				else
					htmltext = "30435-05.htm";
				long adena = st.getQuestItemsCount(RedCrossBadge) * 60 + st.getQuestItemsCount(BlueCrossBadge) * 65 + st.getQuestItemsCount(BlackCrossBadge) * 70;
				st.takeItems(RedCrossBadge, -1);
				st.takeItems(BlueCrossBadge, -1);
				st.takeItems(BlackCrossBadge, -1);
				st.giveItems(57, adena);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}
}
