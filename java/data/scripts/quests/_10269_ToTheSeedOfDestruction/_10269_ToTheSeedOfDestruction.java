package quests._10269_ToTheSeedOfDestruction;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _10269_ToTheSeedOfDestruction extends Quest implements ScriptFile
{
	// NPCs
	private static final int Keucereus = 32548;
	private static final int Allenos = 32526;
	// ITEMs
	private static final int Introduction = 13812;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10269_ToTheSeedOfDestruction()
	{
		super(10269, -1);

		addStartNpc(Keucereus);
		addTalkId(Allenos);
		addQuestItem(Introduction);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("32548-05.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(Introduction, 1);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int state = st.getState();
		int npcId = npc.getNpcId();
		if(state == COMPLETED)
			if(npcId == Allenos)
				htmltext = "32526-02.htm";
			else
				htmltext = "32548-0a.htm";
		else if(state == CREATED && npcId == Keucereus)
			if(st.getPlayer().getLevel() < 75)
				htmltext = "32548-00.htm";
			else
				htmltext = "32548-01.htm";
		else if(state == STARTED && npcId == Keucereus)
			htmltext = "32548-06.htm";
		else if(state == STARTED && npcId == Allenos)
		{
			htmltext = "32526-01.htm";
			st.giveItems(ADENA_ID, 29174);
			st.addExpAndSp(176121, 17671);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}
}
