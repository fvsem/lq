package quests._130_PathToHellbound;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _130_PathToHellbound extends Quest implements ScriptFile
{
	private static final int GALATE = 32292;
	private static final int CASIAN = 30612;
	private static final int CASIAN_BLUE_CRY = 12823;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _130_PathToHellbound()
	{
		super(130, 1);
		addStartNpc(CASIAN);
		addTalkId(CASIAN);
		addTalkId(GALATE);
		addQuestItem(CASIAN_BLUE_CRY);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30612-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32292-03.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}

		else if(event.equalsIgnoreCase("30612-05.htm"))
		{
			st.set("cond", "3");
			st.giveItems(CASIAN_BLUE_CRY, 1);
			st.playSound(SOUND_MIDDLE);
		}

		else if(event.equalsIgnoreCase("32292-06.htm"))
		{
			st.takeItems(CASIAN_BLUE_CRY, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);

			// 1 Cтадия Хеллбаунда стартует в тот момент когда первый игрок сервера сделал квест Path to Hellbound (берется у нпц Casian в wastelend) и появился на территории Хеллбаунда
			ServerVariables.set("HellboundOpen", true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");

		if(npcId == CASIAN)
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 78)
					htmltext = "30612-01.htm";
				else
				{
					htmltext = "30612-00.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 2)
				htmltext = "30612-04.htm";

		if(npcId == GALATE && cond == 1)
			htmltext = "32292-01.htm";
		if(npcId == GALATE && cond == 3)
			if(st.getQuestItemsCount(CASIAN_BLUE_CRY) == 1)
				htmltext = "32292-04.htm";
			else
				htmltext = "Incorrect item count.";
		return htmltext;
	}
}
