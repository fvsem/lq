package quests._307_ControlDeviceOfTheGiants;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.NpcTable;
import l2n.util.Location;

/**
 * @<!-- L2System -->
 * @date 07.12.2010
 * @time 19:21:56
 */
public class _307_ControlDeviceOfTheGiants extends Quest implements ScriptFile
{
	// npc
	private static final int DROPH = 32711;

	// raid boss
	private static final int GORGOLOS = 25681;
	private static final int LAST_TITAN_UTENUS = 25684;
	private static final int GIANT_MARPANAK = 25680;
	private static final int HEKATON_PRIME = 25687;

	private static final int DROPHS_SUPPORT_ITEMS = 14850;

	private static final int CAVE_EXPLORATION_TEXT_1_SHEET = 14851;
	private static final int CAVE_EXPLORATION_TEXT_2_SHEET = 14852;
	private static final int CAVE_EXPLORATION_TEXT_3_SHEET = 14853;

	private static final long RESPAWN_DELAY = 12 * 3600 * 1000L;

	public _307_ControlDeviceOfTheGiants()
	{
		super(307, PARTY_ALL);

		addStartNpc(DROPH);
		addTalkId(DROPH);
		addKillId(GORGOLOS, LAST_TITAN_UTENUS, GIANT_MARPANAK, HEKATON_PRIME);
		addQuestItem(CAVE_EXPLORATION_TEXT_1_SHEET, CAVE_EXPLORATION_TEXT_2_SHEET, CAVE_EXPLORATION_TEXT_3_SHEET);
	}

	private boolean spawnPrime(final L2Player player)
	{
		// время отката вызова Хекатона Прайма 3 часа
		if(ServerVariables.getLong(_307_ControlDeviceOfTheGiants.class.getSimpleName(), 0) + RESPAWN_DELAY <= System.currentTimeMillis())
			try
			{
				final L2Spawn sp = new L2Spawn(NpcTable.getTemplate(HEKATON_PRIME));
				final Location pos = GeoEngine.findPointToStay(191890, 56535, -7622, 0, 0);
				sp.setLoc(pos);
				sp.doSpawn(true);
				ServerVariables.set(_307_ControlDeviceOfTheGiants.class.getSimpleName(), String.valueOf(System.currentTimeMillis()));
				return true;
			}
			catch(final ClassNotFoundException e)
			{
				return false;
			}
		else
		{
			// TODO какое то сообщение для этого предусмотрено наверно)
			player.sendMessage("Время отката вызова Хекатона Прайма 3 часа");
			return false;
		}
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		final String htmltext = event;
		if(event.equalsIgnoreCase("32711-03.htm"))
		{
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("32711-quit.htm"))
		{
			st.exitCurrentQuest(true);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		String htmltext = "noquest";
		final L2Player player = st.getPlayer();
		final int npcId = npc.getNpcId();
		final int cond = st.getInt("cond");

		if(npcId == DROPH)
			if(cond == 0)
			{
				if(player.getLevel() >= 79)
					htmltext = "32711-01.htm";
				else
				{
					htmltext = "32711-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
			{
				if(st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_1_SHEET) != 0 && st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_2_SHEET) != 0 && st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_3_SHEET) != 0)
				{
					htmltext = "32711-04.htm";

					if(spawnPrime(player))
					{
						st.takeAllItems(CAVE_EXPLORATION_TEXT_1_SHEET, CAVE_EXPLORATION_TEXT_2_SHEET, CAVE_EXPLORATION_TEXT_3_SHEET);
						st.setCond(2);
					}
				}
				else
					htmltext = "32711-10.htm";
			}
			else if(cond == 2)
				htmltext = "32711-04.htm";
			else if(cond == 3)
			{
				htmltext = "32711-09.htm";
				st.giveItems(DROPHS_SUPPORT_ITEMS, 1);
				st.giveItems(5575, 80000, true); // Ancient Adena
				st.exitCurrentQuest(true);
				st.playSound(SOUND_FINISH);
			}
		return htmltext;
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		final int npcId = npc.getNpcId();
		if(st == null || st.getState() != STARTED)
			return null;

		final int cond = st.getInt("cond");
		if(cond == 1)
		{
			if(npcId == GORGOLOS && st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_1_SHEET) == 0)
			{
				st.giveItems(CAVE_EXPLORATION_TEXT_1_SHEET, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(npcId == LAST_TITAN_UTENUS && st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_2_SHEET) == 0)
			{
				st.giveItems(CAVE_EXPLORATION_TEXT_2_SHEET, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(npcId == GIANT_MARPANAK && st.getQuestItemsCount(CAVE_EXPLORATION_TEXT_3_SHEET) == 0)
			{
				st.giveItems(CAVE_EXPLORATION_TEXT_3_SHEET, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(cond == 2 && npcId == HEKATON_PRIME)
		{
			st.setCond(3);
			st.playSound(SOUND_MIDDLE);
		}
		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
