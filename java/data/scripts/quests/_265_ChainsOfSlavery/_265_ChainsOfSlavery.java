package quests._265_ChainsOfSlavery;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.Race;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест проверен и работает. Рейты применены путем увеличения награды за квестовые предметы.
 */
public class _265_ChainsOfSlavery extends Quest implements ScriptFile
{
	private static String qnTutorial = "_255_Tutorial";

	// NPC
	private static final int KRISTIN = 30357;

	// MOBS
	private static final int IMP = 20004;
	private static final int IMP_ELDER = 20005;

	// ITEMS
	private static final int IMP_SHACKLES = 1368;
	private static final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 265: Chains Of Slavery");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _265_ChainsOfSlavery()
	{
		super(265, -1);
		addStartNpc(KRISTIN);

		addKillId(IMP);
		addKillId(IMP_ELDER);

		addQuestItem(IMP_SHACKLES);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30357-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30357-06.htm"))
			st.exitCurrentQuest(true);
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(st.getInt("cond") == 0)
		{
			if(st.getPlayer().getRace() != Race.darkelf)
			{
				htmltext = "30357-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 6)
			{
				htmltext = "30357-01.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30357-02.htm";
		}
		else
		{
			long count = st.getQuestItemsCount(IMP_SHACKLES);
			if(count > 0)
				if(count >= 10)
					st.giveItems(ADENA, 13 * count + 500, true);
				else
					st.giveItems(ADENA, 13 * count, true);
			st.takeItems(IMP_SHACKLES, -1);
			htmltext = "30357-05.htm";
			QuestState qs = st.getPlayer().getQuestState(qnTutorial);
			if(qs != null)
			{
				int Ex = qs.getInt("Ex");
				if(Ex != 10)
					st.showQuestionMark(26);
				qs.set("Ex", "10");
				if(st.getPlayer().getClassId().isMage())
				{
					st.playTutorialVoice("tutorial_voice_027");
					st.giveItems(5790, 3000);
				}
				else
				{
					st.giveItems(5789, 6000);
					st.playTutorialVoice("tutorial_voice_026");
				}
			}
			else
				htmltext = "30357-04.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(st.getInt("cond") == 1 && Rnd.chance(5 + npcId - 20004))
		{
			st.giveItems(IMP_SHACKLES, 1);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
