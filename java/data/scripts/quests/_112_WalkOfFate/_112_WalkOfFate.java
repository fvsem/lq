package quests._112_WalkOfFate;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _112_WalkOfFate extends Quest implements ScriptFile
{
	// NPC
	private static final int Livina = 30572;
	private static final int Karuda = 32017;
	// Items
	private static final int EnchantD = 956;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _112_WalkOfFate()
	{
		super(112, -1);

		addStartNpc(Livina);
		addTalkId(Karuda);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32017-02.htm"))
		{
			st.addExpAndSp(112876, 5774);
			st.giveItems(ADENA_ID, (long) (22308 + 6000 * (st.getRateQuestsReward() - 1)), true);
			st.giveItems(EnchantD, 1, true);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		else if(event.equalsIgnoreCase("30572-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == Livina)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 20)
					htmltext = "30572-01.htm";
				else
				{
					htmltext = "30572-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30572-03.htm";
		}
		else if(npcId == Karuda)
			if(cond == 1)
				htmltext = "32017-01.htm";
		return htmltext;
	}
}
