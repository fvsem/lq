package quests._613_ProveYourCourage;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _613_ProveYourCourage extends Quest implements ScriptFile
{
	private final int DURAI = 31377;
	private final int KETRAS_HERO_HEKATON = 25299;

	// Quest items
	private final short HEAD_OF_HEKATON = 7240;
	private final short FEATHER_OF_VALOR = 7229;

	// etc
	@SuppressWarnings("unused")
	private final short MARK_OF_VARKA_ALLIANCE1 = 7221;
	@SuppressWarnings("unused")
	private final short MARK_OF_VARKA_ALLIANCE2 = 7222;
	private final short MARK_OF_VARKA_ALLIANCE3 = 7223;
	private final short MARK_OF_VARKA_ALLIANCE4 = 7224;
	private final short MARK_OF_VARKA_ALLIANCE5 = 7225;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 613: Prove Your Courage");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _613_ProveYourCourage()
	{
		super(613, 1);

		addStartNpc(DURAI);
		addTalkId(DURAI);
		addKillId(KETRAS_HERO_HEKATON);

		addQuestItem(HEAD_OF_HEKATON);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31377-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31377-4.htm"))
			if(st.getQuestItemsCount(HEAD_OF_HEKATON) >= 1)
			{
				st.takeItems(HEAD_OF_HEKATON, -1);
				st.giveItems(FEATHER_OF_VALOR, 1);
				st.addExpAndSp(0, 10000);
				st.unset("cond");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "31377-2r.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 75)
			{
				if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE3) == 1 || st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE4) == 1 || st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE5) == 1)
					htmltext = "31377-1.htm";
				else
				{
					htmltext = "31377-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
			{
				htmltext = "31377-0.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(cond == 1 && st.getQuestItemsCount(HEAD_OF_HEKATON) == 0)
			htmltext = "31377-2r.htm";
		else if(cond == 2 && st.getQuestItemsCount(HEAD_OF_HEKATON) >= 1)
			htmltext = "31377-3.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == KETRAS_HERO_HEKATON && st.getInt("cond") == 1)
		{
			st.giveItems(HEAD_OF_HEKATON, 1);
			st.set("cond", "2");
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
