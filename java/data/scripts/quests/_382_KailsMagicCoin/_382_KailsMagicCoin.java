package quests._382_KailsMagicCoin;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

import java.util.HashMap;

public class _382_KailsMagicCoin extends Quest implements ScriptFile
{
	// Quest items
	private static final int ROYAL_MEMBERSHIP = 5898;
	// NPCs
	private static final int VERGARA = 30687;
	// MOBs and CHANCES
	private static final HashMap<Integer, int[]> MOBS = new HashMap<Integer, int[]>();
	static
	{
		MOBS.put(21017, new int[] { 5961 });
		MOBS.put(21019, new int[] { 5962 });
		MOBS.put(21020, new int[] { 5963 });
		MOBS.put(21022, new int[] { 5961, 5962, 5963 });
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _382_KailsMagicCoin()
	{
		super(382, -1);

		addStartNpc(VERGARA);
		for(int mobId : MOBS.keySet())
			addKillId(mobId);

		addQuestItem(5961);
		addQuestItem(5962);
		addQuestItem(5963);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30687-03.htm"))
			if(st.getPlayer().getLevel() >= 55 && st.getQuestItemsCount(ROYAL_MEMBERSHIP) > 0)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
			else
			{
				htmltext = "30687-01.htm";
				st.exitCurrentQuest(true);
			}

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");

		if(st.getQuestItemsCount(ROYAL_MEMBERSHIP) == 0 || st.getPlayer().getLevel() < 55)
		{
			htmltext = "30687-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(cond == 0)
			htmltext = "30687-02.htm";
		else
			htmltext = "30687-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED || st.getQuestItemsCount(ROYAL_MEMBERSHIP) == 0)
			return null;

		int[] droplist = MOBS.get(npc.getNpcId());
		st.rollAndGive(droplist[Rnd.get(droplist.length)], 1, 10);
		return null;
	}
}
