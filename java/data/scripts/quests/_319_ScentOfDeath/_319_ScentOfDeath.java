package quests._319_ScentOfDeath;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Scent Of Death
 */
public class _319_ScentOfDeath extends Quest implements ScriptFile
{
	// NPC
	private static final int MINALESS = 30138;
	// Item
	private static final int ADENA = 57;
	private static final int HealingPotion = 1061;
	// Quest Item
	private static final int ZombieSkin = 1045;

	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	private static final int[][] DROPLIST_COND = {
			{ 1, 2, 20015, 0, ZombieSkin, 5, 20, 1 },
			{ 1, 2, 20016, 0, ZombieSkin, 5, 20, 1 },
			{ 1, 2, 20017, 0, ZombieSkin, 5, 20, 1 },
			{ 1, 2, 20018, 0, ZombieSkin, 5, 20, 1 },
			{ 1, 2, 20019, 0, ZombieSkin, 5, 20, 1 },
			{ 1, 2, 20020, 0, ZombieSkin, 5, 20, 1 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 319: Scent Of Death");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _319_ScentOfDeath()
	{
		super(319, -1);

		addStartNpc(MINALESS);
		addTalkId(MINALESS);
		// Mob Drop
		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);

		addQuestItem(ZombieSkin);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30138-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == MINALESS)
			if(cond == 0)
				if(st.getPlayer().getLevel() < 11)
				{
					htmltext = "30138-02.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "30138-03.htm";
			else if(cond == 1)
				htmltext = "30138-05.htm";
			else if(cond == 2 && st.getQuestItemsCount(ZombieSkin) >= 5)
			{
				htmltext = "30138-06.htm";
				st.takeItems(ZombieSkin, -1);
				st.giveItems(ADENA, 3350);
				st.giveItems(HealingPotion, 1);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30138-05.htm";
				st.set("cond", "1");
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}
}
