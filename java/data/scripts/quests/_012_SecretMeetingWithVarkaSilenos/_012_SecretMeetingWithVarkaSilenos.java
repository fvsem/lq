package quests._012_SecretMeetingWithVarkaSilenos;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _012_SecretMeetingWithVarkaSilenos extends Quest implements ScriptFile
{
	int CADMON = 31296;
	int HELMUT = 31258;
	int NARAN_ASHANUK = 31378;

	int MUNITIONS_BOX = 7232;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _012_SecretMeetingWithVarkaSilenos()
	{
		super(12, -1);

		addStartNpc(CADMON);

		addTalkId(CADMON);
		addTalkId(HELMUT);
		addTalkId(NARAN_ASHANUK);

		addQuestItem(MUNITIONS_BOX);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31296-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31258-2.htm"))
		{
			st.giveItems(MUNITIONS_BOX, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("31378-2.htm"))
		{
			st.takeItems(MUNITIONS_BOX, 1);
			st.addExpAndSp(79761, 0);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == CADMON)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 74)
					htmltext = "31296-1.htm";
				else
				{
					htmltext = "31296-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "31296-2r.htm";
		}
		else if(npcId == HELMUT)
		{
			if(cond == 1)
				htmltext = "31258-1.htm";
			else if(cond == 2)
				htmltext = "31258-2r.htm";
		}
		else if(npcId == NARAN_ASHANUK)
			if(cond == 2 && st.getQuestItemsCount(MUNITIONS_BOX) > 0)
				htmltext = "31378-1.htm";
		return htmltext;
	}
}
