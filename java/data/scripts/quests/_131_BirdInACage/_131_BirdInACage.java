// made by XoTTa6bI4
package quests._131_BirdInACage;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Location;
import l2n.util.Rnd;

public class _131_BirdInACage extends Quest implements ScriptFile
{

	// NPCs
	private static final int KANIS = 32264;
	private static final int PARME = 32271;
	// ITEMS
	private static final int PARMES_LETTER = 9784;
	private static final int ECHO_CRYSTAL = 9783;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _131_BirdInACage()
	{
		super(131, -1);
		addStartNpc(KANIS);

		addTalkId(KANIS);
		addTalkId(PARME);

		addQuestItem(PARMES_LETTER);
		addQuestItem(ECHO_CRYSTAL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32264-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32264-08.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}

		else if(event.equalsIgnoreCase("32271-02.htm"))
		{
			if(st.getQuestItemsCount(ECHO_CRYSTAL) == 0)
				st.giveItems(ECHO_CRYSTAL, 1);
			st.playSound(SOUND_MIDDLE);
		}

		else if(event.equalsIgnoreCase("32271-03.htm"))
		{
			st.set("cond", "3");
			st.giveItems(PARMES_LETTER, 1);
			st.playSound(SOUND_MIDDLE);
			Location pos = Rnd.coordsRandomize(143472, 191040, -3696, 0, 0, 200);
			st.getPlayer().teleToLocation(pos);
		}

		else if(event.equalsIgnoreCase("32264-12.htm"))
		{
			if(st.getQuestItemsCount(PARMES_LETTER) == 0)
				st.giveItems(PARMES_LETTER, 1);
			st.playSound(SOUND_MIDDLE);
		}

		else if(event.equalsIgnoreCase("32264-13.htm"))
		{
			st.takeItems(ECHO_CRYSTAL, 1);
			st.addExpAndSp(1304752, 0);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(st.getPlayer().getQuestState("_131_BirdInACage") == null)
			return htmltext;
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == KANIS)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 78)
					htmltext = "32264-01.htm";
				else
				{
					htmltext = "32264-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "32264-02.htm";
			else if(cond == 2)
				htmltext = "32264-08.htm";
			else if(cond == 3)
				if(st.getQuestItemsCount(ECHO_CRYSTAL) == 1 && st.getQuestItemsCount(PARMES_LETTER) == 1)
					htmltext = "32264-11.htm";
				else if(st.getQuestItemsCount(ECHO_CRYSTAL) == 1 && st.getQuestItemsCount(PARMES_LETTER) == 0)
					htmltext = "32264-12.htm";
		if(npcId == PARME)
			if(cond == 2)
				htmltext = "32271-01.htm";
		return htmltext;
	}
}
