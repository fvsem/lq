package quests._10281_MutatedKaneus_Rune;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Rune</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10281_MutatedKaneus_Rune extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10281_MutatedKaneus_Rune()
	{
		super(10281, "_10281_MutatedKaneus_Rune", "Mutated Kaneus - Rune");

		NPC = new int[] { 31340, 31335 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18577, 13840 } // White Allosce
		};
		startLevel = 68;
		registerNPCs();
	}
}
