package quests._10267_JourneyToGracia;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * <hr>
 * <em>Квест</em> <strong>Journey To Gracia</strong>
 * <hr>
 */
public class _10267_JourneyToGracia extends Quest implements ScriptFile
{
	// NPCs
	private static final int Orven = 30857;
	private static final int Keucereus = 32548;
	private static final int Papiku = 32564;
	// ITEMs
	private static final short Letter = 13810;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10267_JourneyToGracia()
	{
		super(10267, -1);

		addStartNpc(Orven);
		addTalkId(Keucereus);
		addTalkId(Papiku);
		addQuestItem(Letter);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30857-06.htm"))
		{
			st.giveItems(Letter, 1);
			st.playSound(SOUND_ACCEPT);
			st.setState(STARTED);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("32564-02.htm"))
		{
			st.playSound(SOUND_MIDDLE);
			st.set("cond", "2");
		}
		else if(event.equalsIgnoreCase("32548-02.htm"))
		{
			st.giveItems(57, 92500);
			st.addExpAndSp(75480, 7570);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(st.getState() == CREATED && npcId == Orven)
		{
			if(st.getPlayer().getLevel() < 75)
				htmltext = "30857-00.htm";
			else
				htmltext = "30857-01.htm";
		}
		else if(st.isStarted())
			switch (npcId)
			{
				case Orven:
				{
					htmltext = "30857-07.htm";
					break;
				}
				case Papiku:
				{
					if(cond == 1)
						htmltext = "32564-01.htm";
					else
						htmltext = "32564-03.htm";
					break;
				}
				case Keucereus:
				{
					if(cond == 2)
						htmltext = "32548-01.htm";
					break;
				}
			}
		else if(st.isCompleted())
			if(npcId == Keucereus)
				htmltext = "32548-03.htm";
			else if(npcId == Orven)
				htmltext = "30857-0a.htm";
		return htmltext;
	}
}
