package quests._188_SealRemoval;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * <hr>
 * <em>Квест</em> <strong>Seal Removal</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version CT2
 */
public class _188_SealRemoval extends Quest implements ScriptFile
{
	// NPCs
	private static final int Lorain = 30673;
	private static final int Nikola = 30621;
	private static final int Dorothy = 30970;
	// ITEMs
	private static final short BrokenMetal = 10369;
	private static final short Certificate = 10362;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _188_SealRemoval()
	{
		super(188, -1);

		addStartNpc(Lorain);
		addTalkId(new int[] { Nikola, Dorothy });
		addQuestItem(new int[] { BrokenMetal });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30673-02.htm"))
		{
			st.giveItems(BrokenMetal, 1);
			st.takeItems(Certificate, -1);
			st.playSound(SOUND_ACCEPT);
			st.setState(STARTED);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("30621-03.htm"))
		{
			st.playSound(SOUND_MIDDLE);
			st.set("cond", "2");
		}
		else if(event.equalsIgnoreCase("30970-03.htm"))
		{
			st.giveItems(57, 98583);
			if(st.getPlayer().getLevel() < 47)
				st.addExpAndSp(285935, 18711);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(st.getState() == CREATED && npcId == Lorain)
		{
			QuestState qs184 = st.getPlayer().getQuestState("_184_NikolasCooperationContract");
			QuestState qs185 = st.getPlayer().getQuestState("_185_NikolasCooperationConsideration");
			if(st.getPlayer().getLevel() < 41)
				htmltext = "30673-00.htm";
			else if(qs184 != null && qs184.isCompleted() || qs185 != null && qs185.isCompleted())
				if(!(st.getQuestItemsCount(Certificate) > 0))
					return "30673-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(st.isStarted())
			switch (npcId)
			{
				case Lorain:
				{
					if(cond == 1)
						htmltext = "30673-03.htm";
					break;
				}
				case Nikola:
				{
					if(cond == 1)
						htmltext = "30621-01.htm";
					else if(cond == 2)
						htmltext = "30621-05.htm";
					break;
				}
				case Dorothy:
				{
					if(cond == 2)
						htmltext = "30970-01.htm";
					break;
				}
			}
		return htmltext;
	}
}
