package quests._617_GatherTheFlames;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _617_GatherTheFlames extends Quest implements ScriptFile
{
	// npc
	private final static int VULCAN = 31539;
	private final static int HILDA = 31271;
	// items
	private final static int TORCH = 7264;
	// DROPLIST (MOB_ID, CHANCE)
	private final static int[][] DROPLIST = {
			// AI monster
			{ 18799, 51 },
			{ 18800, 51 },
			{ 18801, 51 },
			{ 18802, 56 },
			{ 18803, 56 },
			// other
			{ 21652, 48 },
			{ 22634, 48 },
			{ 22635, 48 },
			{ 22638, 48 },
			{ 22641, 51 },
			{ 22642, 51 },
			{ 22643, 51 },
			{ 22644, 53 },
			{ 22646, 56 },
			{ 22645, 56 },
			{ 22647, 56 },
			{ 22648, 56 },
			{ 22649, 58 },
			{ 22640, 60 },
			{ 22637, 60 } };

	public static final int[] Recipes = { 6881, 6883, 6885, 6887, 7580, 6891, 6893, 6895, 6897, 6899 };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _617_GatherTheFlames()
	{
		super(617, 0);

		addStartNpc(VULCAN);
		addStartNpc(HILDA);

		for(int[] element : DROPLIST)
			addKillId(element[0]);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31539-03.htm")) // VULCAN
		{
			if(st.getPlayer().getLevel() < 74)
				return "31539-02.htm";
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("31271-03.htm")) // HILDA
		{
			if(st.getPlayer().getLevel() < 74)
				return "31271-01.htm";
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("31539-08.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.takeItems(TORCH, -1);
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("31539-07.htm"))
		{
			if(st.getQuestItemsCount(TORCH) < 1000)
				return "31539-05.htm";
			st.takeItems(TORCH, 1000);
			st.giveItems(Recipes[Rnd.get(Recipes.length)] + (Config.ALT_100_RECIPES ? 1 : 0), 1);
			st.playSound(SOUND_MIDDLE);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == VULCAN)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 74)
				{
					htmltext = "31539-02.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31539-01.htm";
			}
			else
				htmltext = st.getQuestItemsCount(TORCH) < 1000 ? "31539-05.htm" : "31539-04.htm";
		}
		else if(npcId == HILDA)
			if(cond < 1)
				htmltext = st.getPlayer().getLevel() < 74 ? "31271-01.htm" : "31271-02.htm";
			else
				htmltext = "31271-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		for(int[] element : DROPLIST)
			if(npc.getNpcId() == element[0])
			{
				st.rollAndGive(TORCH, 1, element[1]);
				return null;
			}
		return null;
	}
}
