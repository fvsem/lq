package quests._189_ContractCompletion;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * <hr>
 * <em>Квест</em> <strong>Contract Completion</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version CT2
 */
public class _189_ContractCompletion extends Quest implements ScriptFile
{
	// NPCs
	private static final int Luka = 31437;
	private static final int Kusto = 30512;
	private static final int Lorain = 30673;
	private static final int Shegfield = 30068;
	// ITEMs
	private static final short Metallograph = 10370;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _189_ContractCompletion()
	{
		super(189, -1);

		addStartNpc(Luka);
		addTalkId(new int[] { Kusto, Lorain, Shegfield });
		addQuestItem(new int[] { Metallograph });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("31437-02.htm"))
		{
			st.giveItems(Metallograph, 1);
			st.playSound(SOUND_ACCEPT);
			st.setState(STARTED);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("30673-02.htm"))
		{
			st.takeItems(Metallograph, -1);
			st.playSound("SOUND_MIDDLE");
			st.set("cond", "2");
		}
		else if(event.equalsIgnoreCase("30068-03.htm"))
		{
			st.playSound("SOUND_MIDDLE");
			st.set("cond", "3");
		}
		else if(event.equalsIgnoreCase("30512-02.htm"))
		{
			st.giveItems(57, 121527);
			if(st.getPlayer().getLevel() < 48)
				st.addExpAndSp(309467, 20614);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(st.getState() == CREATED && npcId == Luka)
		{
			QuestState qs186 = st.getPlayer().getQuestState("_186_ContractExecution");
			if(st.getPlayer().getLevel() < 42)
				htmltext = "31437-00.htm";
			else if(qs186 != null && qs186.isCompleted())
				return "31437-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(st.isStarted())
			switch (npcId)
			{
				case Luka:
				{
					if(cond == 1)
						htmltext = "31437-03.htm";
					break;
				}
				case Lorain:
				{
					if(cond == 1)
						htmltext = "30673-01.htm";
					else if(cond == 2)
						htmltext = "30673-03.htm";
					else if(cond == 3)
					{
						htmltext = "30673-04.htm";
						st.playSound("SOUND_MIDDLE");
						st.set("cond", "4");
					}
					else if(cond == 4)
						htmltext = "30673-05.htm";
					break;
				}
				case Shegfield:
				{
					if(cond == 2)
						htmltext = "30068-01.htm";
					else if(cond == 3)
						htmltext = "30068-04.htm";
					break;
				}
				case Kusto:
				{
					if(cond == 4)
						htmltext = "30512-01.htm";
					break;
				}
			}
		return htmltext;
	}
}
