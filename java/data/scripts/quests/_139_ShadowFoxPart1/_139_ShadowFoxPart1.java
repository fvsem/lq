package quests._139_ShadowFoxPart1;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Shadow Fox Part 1
 */
public class _139_ShadowFoxPart1 extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPC
	private final static int MIA = 30896;

	// ITEM
	private final static int FRAGMENT = 10345;
	private final static int CHEST = 10346;

	// MONSTER
	private final static int[] NPC = { 20784, 20785, 21639, 21640 };

	public _139_ShadowFoxPart1()
	{
		super(139, -1);

		addStartNpc(MIA);
		addTalkId(MIA);

		addQuestItem(FRAGMENT);
		addQuestItem(CHEST);

		for(int i : NPC)
			addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30896-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30896-11.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30896-14.htm"))
		{
			st.takeItems(FRAGMENT, -1);
			st.takeItems(CHEST, -1);
			st.set("talk", "1");
		}
		else if(event.equalsIgnoreCase("30896-16.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.unset("cond");
			st.unset("talk");
			st.exitCurrentQuest(false);
			st.giveItems(57, 14050, true);
			if(st.getPlayer().getLevel() >= 37 && st.getPlayer().getLevel() <= 42)
				st.addExpAndSp(30000, 2000);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		if(npcId == MIA)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 37)
				{
					QuestState qs138 = st.getPlayer().getQuestState("_138_TempleChampionPart2");
					if(qs138 != null)
						htmltext = "30896-01.htm";
					else
						htmltext = "need.htm";
				}
				else
				{
					htmltext = "30896-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30896-03.htm";
			else if(cond == 2)
				if(st.getQuestItemsCount(FRAGMENT) >= 10 && st.getQuestItemsCount(CHEST) >= 1)
					htmltext = "30896-13.htm";
				else if(st.getInt("talk") == 1)
					htmltext = "30896-14.htm";
				else
					htmltext = "30896-12.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		if(cond == 2)
		{
			st.giveItems(FRAGMENT, 1);
			st.playSound(SOUND_ITEMGET);
			if(Rnd.get(100) <= 2)
				st.giveItems(CHEST, 1);
		}
		return null;
	}
}
