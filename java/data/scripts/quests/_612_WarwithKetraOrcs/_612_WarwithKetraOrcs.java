package quests._612_WarwithKetraOrcs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _612_WarwithKetraOrcs extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPC
	private static final int DURAI = 31377;

	// Quest items
	private static final int MOLAR_OF_KETRA_ORC = 7234;
	private static final int NEPENTHES_SEED = 7187;
	private static final int MOLAR_OF_KETRA_ORC_DROP_CHANCE = 80;

	private static final int[] KETRA_NPC_LIST = new int[] {
			21324,
			21325,
			21327,
			21328,
			21329,
			21331,
			21332,
			21334,
			21335,
			21336,
			21338,
			21339,
			21340,
			21342,
			21343,
			21344,
			21345,
			21346,
			21347 };

	public _612_WarwithKetraOrcs()
	{
		super(612, 0);

		addStartNpc(DURAI);
		addTalkId(DURAI);
		addKillId(KETRA_NPC_LIST);

		addQuestItem(MOLAR_OF_KETRA_ORC);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31377-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31377-4.htm"))
		{
			if(st.getQuestItemsCount(MOLAR_OF_KETRA_ORC) >= 100)
			{
				st.takeItems(MOLAR_OF_KETRA_ORC, 100);
				st.giveItems(NEPENTHES_SEED, 20);
			}
			else
				htmltext = "31377-havenot.htm";
		}
		else if(event.equalsIgnoreCase("31377-quit.htm"))
		{
			st.takeItems(MOLAR_OF_KETRA_ORC, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 74)
				htmltext = "31377-1.htm";
			else
			{
				htmltext = "31377-0.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(cond == 1 && st.getQuestItemsCount(MOLAR_OF_KETRA_ORC) == 0)
			htmltext = "31377-2r.htm";
		else if(cond == 1 && st.getQuestItemsCount(MOLAR_OF_KETRA_ORC) > 0)
			htmltext = "31377-3.htm";
		return htmltext;
	}

	public boolean isKetraNpc(int npc)
	{
		return ArrayUtil.arrayContains(KETRA_NPC_LIST, npc);
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(isKetraNpc(npc.getNpcId()) && st.getInt("cond") == 1)
			st.rollAndGive(MOLAR_OF_KETRA_ORC, 1, MOLAR_OF_KETRA_ORC_DROP_CHANCE);
		return null;
	}
}
