package quests._015_SweetWhispers;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _015_SweetWhispers extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _015_SweetWhispers()
	{
		super(15, -1);

		addStartNpc(31302);

		addTalkId(31302);
		addTalkId(31517);
		addTalkId(31518);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31302-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31518-02.htm"))
			st.set("cond", "2");
		else if(event.equalsIgnoreCase("31517-02.htm"))
		{
			st.addExpAndSp(88000, 0);
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 31302)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 60)
					htmltext = "31302-01.htm";
				else
				{
					htmltext = "31302-00.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond >= 1)
				htmltext = "31302-02r.htm";
		}
		else if(npcId == 31518)
		{
			if(cond == 1)
				htmltext = "31518-01.htm";
			else if(cond == 2)
				htmltext = "31518-02r.htm";
		}
		else if(npcId == 31517)
			if(cond == 2)
				htmltext = "31517-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		return null;
	}
}
