package quests._283_TheFewTheProudTheBrave;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _283_TheFewTheProudTheBrave extends Quest implements ScriptFile
{
	// NPCs
	private static int PERWAN = 32133;
	// Mobs
	private static int CRIMSON_SPIDER = 22244;
	// Quest Items
	private static int CRIMSON_SPIDER_CLAW = 9747;
	// Items
	private static int ADENA = 57;
	// Chances
	private static int CRIMSON_SPIDER_CLAW_CHANCE = 34;

	public _283_TheFewTheProudTheBrave()
	{
		super(283, -1);
		addStartNpc(PERWAN);
		addKillId(CRIMSON_SPIDER);
		addQuestItem(CRIMSON_SPIDER_CLAW);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("32133-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32133-06.htm") && _state == STARTED)
		{
			long _count = st.getQuestItemsCount(CRIMSON_SPIDER_CLAW);
			if(_count > 0)
			{
				st.takeItems(CRIMSON_SPIDER_CLAW, -1);
				st.giveItems(ADENA, 45 * _count);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(event.equalsIgnoreCase("32133-08.htm") && _state == STARTED)
		{
			st.takeItems(CRIMSON_SPIDER_CLAW, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != PERWAN)
			return htmltext;
		int _state = st.getState();

		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() >= 15)
			{
				htmltext = "32133-01.htm";
				st.set("cond", "0");
			}
			else
			{
				htmltext = "32133-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(_state == STARTED)
			htmltext = st.getQuestItemsCount(CRIMSON_SPIDER_CLAW) > 0 ? "32133-05.htm" : "32133-04.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		if(Rnd.chance(CRIMSON_SPIDER_CLAW_CHANCE))
		{
			qs.giveItems(CRIMSON_SPIDER_CLAW, 1);
			qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 283: The Few, The Proud, The Brave");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
