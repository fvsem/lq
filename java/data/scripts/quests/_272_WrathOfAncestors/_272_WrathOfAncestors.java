package quests._272_WrathOfAncestors;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Wrath Of Ancestors
 */

public class _272_WrathOfAncestors extends Quest implements ScriptFile
{
	// NPC
	private static final int Livina = 30572;
	// Quest Item
	private static final int GraveRobbersHead = 1474;
	// Item
	private static final int Adena = 57;
	// MOB
	private static final int GoblinGraveRobber = 20319;
	private static final int GoblinTombRaiderLeader = 20320;
	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	private static final int[][] DROPLIST_COND = { { 1, 2, GoblinGraveRobber, 0, GraveRobbersHead, 50, 100, 1 }, { 1, 2, GoblinTombRaiderLeader, 0, GraveRobbersHead, 50, 100, 1 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 272: Wrath Of Ancestors");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _272_WrathOfAncestors()
	{
		super(272, -1);
		addStartNpc(Livina);
		// Mob Drop
		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);
		addQuestItem(GraveRobbersHead);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			htmltext = "30572-03.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == Livina)
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 3)
				{
					htmltext = "30572-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 5)
				{
					htmltext = "30572-01.htm";
					st.exitCurrentQuest(true);
				}
				else
				{
					htmltext = "30572-02.htm";
					return htmltext;
				}
			}
			else if(cond == 1)
				htmltext = "30572-04.htm";
			else if(cond == 2)
			{
				st.takeItems(GraveRobbersHead, -1);
				st.giveItems(Adena, 1500);
				htmltext = "30572-05.htm";
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}
}
