package quests._723_ForTheSakeOfTheTerritoryGoddard;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _723_ForTheSakeOfTheTerritoryGoddard extends TerritoryWarSuperClass
{
	public _723_ForTheSakeOfTheTerritoryGoddard()
	{
		super(723);
		_catapultId = 36505;
		_territoryId = 7;
		_leaderIDs = new int[] { 36544, 36546, 36549, 36597 };
		_guardIDs = new int[] { 36545, 36547, 36548 };
		_text = new String[] { "The catapult of Goddard has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
