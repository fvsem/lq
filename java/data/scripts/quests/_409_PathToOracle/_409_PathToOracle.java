package quests._409_PathToOracle;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _409_PathToOracle extends Quest implements ScriptFile
{
	// npc
	private static final int MANUEL = 30293;
	private static final int ALLANA = 30424;
	private static final int PERRIN = 30428;
	// mobs
	private static final int LIZARDMAN_WARRIOR = 27032;
	private static final int LIZARDMAN_SCOUT = 27033;
	private static final int LIZARDMAN = 27034;
	private static final int TAMIL = 27035;
	// items
	private static final int CRYSTAL_MEDALLION_ID = 1231;
	private static final int MONEY_OF_SWINDLER_ID = 1232;
	private static final int DAIRY_OF_ALLANA_ID = 1233;
	private static final int LIZARD_CAPTAIN_ORDER_ID = 1234;
	private static final int LEAF_OF_ORACLE_ID = 1235;
	private static final int HALF_OF_DAIRY_ID = 1236;
	private static final int TAMATOS_NECKLACE_ID = 1275;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _409_PathToOracle()
	{
		super(409, -1);

		addStartNpc(MANUEL);

		addTalkId(MANUEL);
		addTalkId(ALLANA);
		addTalkId(PERRIN);

		addKillId(LIZARDMAN_WARRIOR);
		addKillId(LIZARDMAN_SCOUT);
		addKillId(LIZARDMAN);
		addKillId(TAMIL);

		addQuestItem(new int[] { MONEY_OF_SWINDLER_ID, DAIRY_OF_ALLANA_ID, LIZARD_CAPTAIN_ORDER_ID, CRYSTAL_MEDALLION_ID, HALF_OF_DAIRY_ID, TAMATOS_NECKLACE_ID });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
		{
			if(event.equalsIgnoreCase("1"))
				if(st.getPlayer().getClassId().getId() != 0x19)
				{
					if(st.getPlayer().getClassId().getId() == 0x1d)
						htmltext = "30293-02a.htm";
					else
						htmltext = "30293-02.htm";
				}
				else if(st.getPlayer().getLevel() < 18)
					htmltext = "30293-03.htm";
				else if(st.getQuestItemsCount(LEAF_OF_ORACLE_ID) > 0)
					htmltext = "30293-04.htm";
				else
				{
					st.set("cond", "1");
					st.setState(STARTED);
					st.playSound(SOUND_ACCEPT);
					st.giveItems(CRYSTAL_MEDALLION_ID, 1);
					htmltext = "30293-05.htm";
				}
		}
		else if(event.equalsIgnoreCase("30424-08.htm"))
		{
			st.addSpawn(LIZARDMAN_WARRIOR);
			st.addSpawn(LIZARDMAN_SCOUT);
			st.addSpawn(LIZARDMAN);
			st.set("cond", "2");
		}
		else if(event.equalsIgnoreCase("30424_1"))
			htmltext = "";
		else if(event.equalsIgnoreCase("30428_1"))
			htmltext = "30428-02.htm";
		else if(event.equalsIgnoreCase("30428_2"))
			htmltext = "30428-03.htm";
		else if(event.equalsIgnoreCase("30428_3"))
			st.addSpawn(TAMIL);
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == MANUEL)
		{
			if(cond < 1)
				htmltext = "30293-01.htm";
			else if(st.getQuestItemsCount(CRYSTAL_MEDALLION_ID) > 0)
				if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) < 1 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) < 1 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) < 1 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) < 1)
					htmltext = "30293-09.htm";
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) > 0 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) > 0 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) < 1)
				{
					htmltext = "30293-08.htm";
					st.takeItems(MONEY_OF_SWINDLER_ID, 1);
					st.takeItems(DAIRY_OF_ALLANA_ID, -1);
					st.takeItems(LIZARD_CAPTAIN_ORDER_ID, -1);
					st.takeItems(CRYSTAL_MEDALLION_ID, -1);
					st.playSound(SOUND_FINISH);
					if(!st.getPlayer().getVarB("prof1") && st.getPlayer().getClassId().getLevel() == 1)
					{
						st.getPlayer().setVar("prof1", "1");
						st.giveItems(57, 163800, true);
						st.addExpAndSp(228064, 16455, true);
					}
					st.exitCurrentQuest(true);
					st.giveItems(LEAF_OF_ORACLE_ID, 1);
				}
				else
					htmltext = "30293-07.htm";
		}
		else if(npcId == ALLANA)
		{
			if(st.getQuestItemsCount(CRYSTAL_MEDALLION_ID) > 0)
				if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) < 1 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) < 1 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) < 1 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) < 1)
				{
					if(cond > 2)
						htmltext = "30424-05.htm";
					else
						htmltext = "30424-01.htm";
				}
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) < 1 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) < 1 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) < 1)
				{
					htmltext = "30424-02.htm";
					st.giveItems(HALF_OF_DAIRY_ID, 1);
					st.set("cond", "4");
				}
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) < 1 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) < 1 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) > 0)
				{
					if(st.getQuestItemsCount(TAMATOS_NECKLACE_ID) < 1)
						htmltext = "30424-06.htm";
					else
						htmltext = "30424-03.htm";
				}
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) > 0 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) < 1 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) > 0)
				{
					htmltext = "30424-04.htm";
					st.takeItems(HALF_OF_DAIRY_ID, -1);
					st.giveItems(DAIRY_OF_ALLANA_ID, 1);
					st.set("cond", "7");
				}
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) > 0 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0 && st.getQuestItemsCount(HALF_OF_DAIRY_ID) < 1 && st.getQuestItemsCount(DAIRY_OF_ALLANA_ID) > 0)
					htmltext = "30424-05.htm";
		}
		else if(npcId == PERRIN)
			if(st.getQuestItemsCount(CRYSTAL_MEDALLION_ID) > 0 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) > 0)
				if(st.getQuestItemsCount(TAMATOS_NECKLACE_ID) > 0)
				{
					htmltext = "30428-04.htm";
					st.takeItems(TAMATOS_NECKLACE_ID, -1);
					st.giveItems(MONEY_OF_SWINDLER_ID, 1);
					st.set("cond", "6");
				}
				else if(st.getQuestItemsCount(MONEY_OF_SWINDLER_ID) > 0)
					htmltext = "30428-05.htm";
				else if(cond > 4)
					htmltext = "30428-06.htm";
				else
					htmltext = "30428-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == LIZARDMAN_WARRIOR | npcId == LIZARDMAN_SCOUT | npcId == LIZARDMAN)
		{
			if(cond == 2 && st.getQuestItemsCount(LIZARD_CAPTAIN_ORDER_ID) < 1)
			{
				st.giveItems(LIZARD_CAPTAIN_ORDER_ID, 1);
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "3");
			}
		}
		else if(npcId == TAMIL)
			if(cond == 4 && st.getQuestItemsCount(TAMATOS_NECKLACE_ID) < 1)
			{
				st.giveItems(TAMATOS_NECKLACE_ID, 1);
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "5");
			}
		return null;
	}
}
