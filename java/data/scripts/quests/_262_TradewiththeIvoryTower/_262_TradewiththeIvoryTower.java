package quests._262_TradewiththeIvoryTower;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _262_TradewiththeIvoryTower extends Quest implements ScriptFile
{
	// NPC
	public final int VOLODOS = 30137;
	// MOB
	public final int GREEN_FUNGUS = 20007;
	public final int BLOOD_FUNGUS = 20400;
	// Items
	public final int FUNGUS_SAC = 707;
	public final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 262 : Tradewiththe Ivory Tower");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _262_TradewiththeIvoryTower()
	{
		super(262, -1);

		addStartNpc(VOLODOS);
		addKillId(new int[] { BLOOD_FUNGUS, GREEN_FUNGUS });
		addQuestItem(new int[] { FUNGUS_SAC });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30137-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 8)
			{
				htmltext = "30137-02.htm";
				return htmltext;
			}
			htmltext = "30137-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(cond == 1 && st.getQuestItemsCount(FUNGUS_SAC) < 10)
			htmltext = "30137-04.htm";
		else if(cond == 2 && st.getQuestItemsCount(FUNGUS_SAC) >= 10)
		{
			st.giveItems(ADENA, 3000);
			st.takeItems(FUNGUS_SAC, -1);
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			htmltext = "30137-05.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int random = Rnd.get(10);
		if(st.getInt("cond") == 1 && st.getQuestItemsCount(FUNGUS_SAC) < 10)
			if(npcId == GREEN_FUNGUS && random < 3 || npcId == BLOOD_FUNGUS && random < 4)
			{
				st.giveItems(FUNGUS_SAC, 1);
				if(st.getQuestItemsCount(FUNGUS_SAC) == 10)
				{
					st.set("cond", "2");
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
