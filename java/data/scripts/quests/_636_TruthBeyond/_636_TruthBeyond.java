package quests._636_TruthBeyond;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _636_TruthBeyond extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// Npc
	private static final int ELIYAH = 31329;
	private static final int FLAURON = 32010;

	// Items
	private static final int VISITORSMARK = 8064;
	private static final int FADED_MARK = 8065;
	private static final int PERMANENT_MARK = 8067;

	public _636_TruthBeyond()
	{
		super(636, -1);

		addStartNpc(ELIYAH);
		addTalkId(FLAURON);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equals("31329-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("32010-02.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_FINISH);
			st.giveItems(VISITORSMARK, 1);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == ELIYAH && cond == 0)
		{
			if(st.getQuestItemsCount(VISITORSMARK, FADED_MARK, PERMANENT_MARK) == 0)
			{
				if(st.getPlayer().getLevel() > 72)
					htmltext = "31329-02.htm";
				else
				{
					htmltext = "31329-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
				htmltext = "31329-05.htm";
		}
		else if(npcId == FLAURON)
			if(cond == 1 || st.getQuestItemsCount(VISITORSMARK, FADED_MARK, PERMANENT_MARK) == 0)
				htmltext = "32010-01.htm";
			else
				htmltext = "32010-03.htm";
		return htmltext;
	}
}
