package quests._113_StatusOfTheBeaconTower;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _113_StatusOfTheBeaconTower extends Quest implements ScriptFile
{
	// NPC
	private static final int MOIRA = 31979;
	private static final int TORRANT = 32016;

	// QUEST ITEM
	private static final int BOX = 8086;

	// REWARD ITEM
	private static final int ADENA = 57;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _113_StatusOfTheBeaconTower()
	{
		super(113, -1);
		addStartNpc(MOIRA);
		addTalkId(TORRANT);
		addQuestItem(BOX);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31979-02.htm"))
		{
			st.set("cond", "1");
			st.giveItems(BOX, 1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32016-02.htm"))
		{
			st.addExpAndSp(76655, 5333);
			st.giveItems(ADENA, 21578);
			st.takeItems(BOX, 1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(id == COMPLETED)
			htmltext = "completed";
		else if(npcId == MOIRA)
		{
			if(id == CREATED)
			{
				if(st.getPlayer().getLevel() >= 40)
					htmltext = "31979-01.htm";
				else
				{
					htmltext = "31979-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "31979-03.htm";
		}
		else if(npcId == TORRANT && st.getQuestItemsCount(BOX) == 1)
			htmltext = "32016-01.htm";
		return htmltext;
	}
}
