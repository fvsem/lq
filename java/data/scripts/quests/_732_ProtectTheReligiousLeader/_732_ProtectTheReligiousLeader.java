package quests._732_ProtectTheReligiousLeader;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _732_ProtectTheReligiousLeader extends TerritoryWarSuperClass
{
	public _732_ProtectTheReligiousLeader()
	{
		super(732);
		_npcIDs = new int[] { 36510, 36516, 36522, 36528, 36534, 36540, 36546, 36552, 36558 };
		registerAttackIds();
	}

	@Override
	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return 1 + (npcid - 36510) / 6;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
