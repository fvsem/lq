package quests._616_MagicalPowerofFire2;

import l2n.extensions.listeners.collections.MethodCollection;
import l2n.extensions.listeners.engine.MethodInvokeListener;
import l2n.extensions.listeners.events.MethodEvent;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _616_MagicalPowerofFire2 extends Quest implements ScriptFile
{
	// NPC
	private static final int KETRAS_HOLY_ALTAR = 31558;
	private static final int UDAN = 31379;
	private static final int SOUL_OF_FIRE_NASTRON = 25306;

	// Quest items
	private static final short FIRE_HEART_OF_NASTRON = 7244;
	private static final short RED_TOTEM = 7243;

	private static final int[] VARKA_NPC_LIST = new int[] {
			21350,
			21351,
			21353,
			21354,
			21355,
			21357,
			21358,
			21360,
			21361,
			21362,
			21369,
			21370,
			21364,
			21365,
			21366,
			21368,
			21371,
			21372,
			21373,
			21374,
			21375 };

	private L2NpcInstance SoulOfFireNastronSpawn = null;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _616_MagicalPowerofFire2()
	{
		super(616, -1);

		addStartNpc(UDAN);

		addTalkId(UDAN);
		addTalkId(KETRAS_HOLY_ALTAR);

		addKillId(VARKA_NPC_LIST);

		addKillId(SOUL_OF_FIRE_NASTRON);
		addQuestItem(FIRE_HEART_OF_NASTRON);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(SOUL_OF_FIRE_NASTRON);
		String htmltext = event;
		if(event.equalsIgnoreCase("quest_accept"))
		{
			htmltext = "shaman_udan_q0616_0104.htm";
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("616_1"))
			if(ServerVariables.getLong(_616_MagicalPowerofFire2.class.getSimpleName(), 0) + 3 * 60 * 60 * 1000 > System.currentTimeMillis())
				htmltext = "totem_of_ketra_q0616_0204.htm";
			else if(st.getQuestItemsCount(RED_TOTEM) >= 1 && isQuest == null)
			{
				st.set("cond", "2");
				st.takeItems(RED_TOTEM, 1);
				SoulOfFireNastronSpawn = st.addSpawn(SOUL_OF_FIRE_NASTRON, 142528, -82528, -6496);
				SoulOfFireNastronSpawn.addMethodInvokeListener(MethodCollection.doDie, new DieListener());
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "totem_of_ketra_q0616_0203.htm";
		else if(event.equalsIgnoreCase("616_3"))
			if(st.getQuestItemsCount(FIRE_HEART_OF_NASTRON) >= 1)
			{
				st.takeItems(FIRE_HEART_OF_NASTRON, -1);
				st.playSound(SOUND_FINISH);
				st.addExpAndSp(10000, 0);
				htmltext = "shaman_udan_q0616_0301.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "shaman_udan_q0616_0302.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(SOUL_OF_FIRE_NASTRON);
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		switch (npcId)
		{
			case UDAN:
				if(cond == 0)
					if(st.getPlayer().getLevel() >= 75)
						if(st.getQuestItemsCount(RED_TOTEM) >= 1)
							htmltext = "shaman_udan_q0616_0101.htm";
						else
						{
							htmltext = "shaman_udan_q0616_0102.htm";
							st.exitCurrentQuest(true);
						}
					else
					{
						htmltext = "shaman_udan_q0616_0103.htm";
						st.exitCurrentQuest(true);
					}
				else if(cond == 1)
					htmltext = "shaman_udan_q0616_0105.htm";
				else if(cond == 2)
					htmltext = "shaman_udan_q0616_0202.htm";
				else if(cond == 3 && st.getQuestItemsCount(FIRE_HEART_OF_NASTRON) == 1)
					htmltext = "shaman_udan_q0616_0201.htm";
				break;
			case KETRAS_HOLY_ALTAR:
				if(ServerVariables.getLong(_616_MagicalPowerofFire2.class.getSimpleName(), 0) + 3 * 60 * 60 * 1000 > System.currentTimeMillis())
					htmltext = "totem_of_ketra_q0616_0204.htm";
				else if(npc.isBusy())
					htmltext = "totem_of_ketra_q0616_0202.htm";
				else if(cond == 1)
					htmltext = "totem_of_ketra_q0616_0101.htm";
				else if(cond == 2)
					if(isQuest == null)
					{
						SoulOfFireNastronSpawn = st.addSpawn(SOUL_OF_FIRE_NASTRON, 142528, -82528, -6496);
						SoulOfFireNastronSpawn.addMethodInvokeListener(MethodCollection.doDie, new DieListener());
						st.set("cond", "2");
						htmltext = "totem_of_ketra_q0616_0204.htm";
					}
					else
						htmltext = "<html><body>Already in spawn.</body></html>";
				break;
		}
		return htmltext;
	}

	private static class DieListener implements MethodInvokeListener
	{
		@Override
		public boolean accept(MethodEvent event)
		{
			return true;
		}

		@Override
		public void methodInvoked(MethodEvent e)
		{
			ServerVariables.set(_616_MagicalPowerofFire2.class.getSimpleName(), String.valueOf(System.currentTimeMillis()));
		}
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == SOUL_OF_FIRE_NASTRON && st.getInt("cond") == 2)
		{
			if(st.getQuestItemsCount(FIRE_HEART_OF_NASTRON) == 0)
				st.giveItems(FIRE_HEART_OF_NASTRON, 1);
			st.set("cond", "3");

			if(SoulOfFireNastronSpawn != null)
				SoulOfFireNastronSpawn.deleteMe();
			SoulOfFireNastronSpawn = null;
		}

		if(ArrayUtil.arrayContains(VARKA_NPC_LIST, npcId))
		{
			st.takeItems(FIRE_HEART_OF_NASTRON, -1);
			st.set("cond", "1");
			st.setState(STARTED);
		}

		return null;
	}
}
