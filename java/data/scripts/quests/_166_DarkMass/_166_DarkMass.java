package quests._166_DarkMass;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _166_DarkMass extends Quest implements ScriptFile
{
	private static int UNDRES_LETTER_ID = 1088;
	private static int CEREMONIAL_DAGGER_ID = 1089;
	private static int DREVIANT_WINE_ID = 1090;
	private static int GARMIELS_SCRIPTURE_ID = 1091;
	private static int ADENA_ID = 57;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _166_DarkMass()
	{
		super(166, -1);

		addStartNpc(30130);

		addTalkId(30130);

		addTalkId(30130);
		addTalkId(30135);
		addTalkId(30139);
		addTalkId(30143);

		addQuestItem(new int[] { CEREMONIAL_DAGGER_ID, DREVIANT_WINE_ID, GARMIELS_SCRIPTURE_ID, UNDRES_LETTER_ID });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			st.set("id", "0");
			htmltext = "30130-04.htm";
			st.giveItems(UNDRES_LETTER_ID, 1);
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{

		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
			st.set("id", "0");
		}
		if(npcId == 30130 && st.getInt("cond") == 0)
		{
			if(st.getInt("cond") < 15)
			{
				if(st.getPlayer().getRace().ordinal() != 2 && st.getPlayer().getRace().ordinal() != 0)
					htmltext = "30130-00.htm";
				else if(st.getPlayer().getLevel() >= 2)
				{
					htmltext = "30130-03.htm";
					return htmltext;
				}
				else
				{
					htmltext = "30130-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
			{
				htmltext = "30130-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == 30130 && st.getInt("cond") == 1 && st.getQuestItemsCount(UNDRES_LETTER_ID) == 1 && (st.getQuestItemsCount(GARMIELS_SCRIPTURE_ID) < 1 || st.getQuestItemsCount(DREVIANT_WINE_ID) < 1 || st.getQuestItemsCount(CEREMONIAL_DAGGER_ID) < 1))
			htmltext = "30130-05.htm";
		else if(npcId == 30135 && st.getInt("cond") == 1 && st.getQuestItemsCount(UNDRES_LETTER_ID) == 1 && st.getQuestItemsCount(CEREMONIAL_DAGGER_ID) == 0)
		{
			st.giveItems(CEREMONIAL_DAGGER_ID, 1);
			htmltext = "30135-01.htm";
		}
		else if(npcId == 30135 && st.getInt("cond") == 1 && st.getQuestItemsCount(CEREMONIAL_DAGGER_ID) == 1)
			htmltext = "30135-02.htm";
		else if(npcId == 30139 && st.getInt("cond") == 1 && st.getQuestItemsCount(UNDRES_LETTER_ID) == 1 && st.getQuestItemsCount(DREVIANT_WINE_ID) == 0)
		{
			st.giveItems(DREVIANT_WINE_ID, 1);
			htmltext = "30139-01.htm";
		}
		else if(npcId == 30139 && st.getInt("cond") == 1 && st.getQuestItemsCount(DREVIANT_WINE_ID) == 1)
			htmltext = "30139-02.htm";
		else if(npcId == 30143 && st.getInt("cond") == 1 && st.getQuestItemsCount(UNDRES_LETTER_ID) == 1 && st.getQuestItemsCount(GARMIELS_SCRIPTURE_ID) == 0)
		{
			st.giveItems(GARMIELS_SCRIPTURE_ID, 1);
			htmltext = "30143-01.htm";
		}
		else if(npcId == 30143 && st.getInt("cond") == 1 && st.getQuestItemsCount(GARMIELS_SCRIPTURE_ID) == 1)
			htmltext = "30143-02.htm";
		else if(npcId == 30130 && st.getInt("cond") == 1 && st.getQuestItemsCount(UNDRES_LETTER_ID) == 1 && st.getQuestItemsCount(CEREMONIAL_DAGGER_ID) == 1 && st.getQuestItemsCount(DREVIANT_WINE_ID) == 1 && st.getQuestItemsCount(GARMIELS_SCRIPTURE_ID) == 1)
			if(st.getInt("id") != 166)
			{
				st.set("id", "166");
				htmltext = "30130-06.htm";
				st.takeItems(CEREMONIAL_DAGGER_ID, 1);
				st.takeItems(DREVIANT_WINE_ID, 1);
				st.takeItems(GARMIELS_SCRIPTURE_ID, 1);
				st.takeItems(UNDRES_LETTER_ID, 1);
				st.giveItems(ADENA_ID, 500);
				st.addExpAndSp(500, 0);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}
}
