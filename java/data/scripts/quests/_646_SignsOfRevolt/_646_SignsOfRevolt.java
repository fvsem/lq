package quests._646_SignsOfRevolt;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _646_SignsOfRevolt extends Quest implements ScriptFile
{
	// NPCs
	private static final int TORRANT = 32016;
	// Mobs
	private static final int Ragna_Orc = 22029; // First in Range
	private static final int Ragna_Orc_Sorcerer = 22044; // Last in Range
	private static final int Guardian_of_the_Ghost_Town = 22047;
	private static final int Varangkas_Succubus = 22049;
	// Items
	private static final int ADENA = 57;
	private static final int Steel = 1880;
	private static final int Coarse_Bone_Powder = 1881;
	private static final int Leather = 1882;
	// Quest Items
	private static final int CURSED_DOLL = 8087;
	// Chances
	private static final int CURSED_DOLL_Chance = 75;

	public _646_SignsOfRevolt()
	{
		super(646, -1);
		addStartNpc(TORRANT);
		for(int Ragna_Orc_id = Ragna_Orc; Ragna_Orc_id <= Ragna_Orc_Sorcerer; Ragna_Orc_id++)
			addKillId(Ragna_Orc_id);
		addKillId(Guardian_of_the_Ghost_Town);
		addKillId(Varangkas_Succubus);
		addQuestItem(CURSED_DOLL);
	}

	private static String doReward(QuestState st, int reward_id, int _count)
	{
		if(st.getQuestItemsCount(CURSED_DOLL) < 180)
			return null;
		st.takeItems(CURSED_DOLL, -1);
		st.giveItems(reward_id, _count, true);
		st.playSound(SOUND_FINISH);
		st.exitCurrentQuest(true);
		return "32016-07.htm";
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("32016-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("reward_adena") && _state == STARTED)
			return doReward(st, ADENA, 21600);
		else if(event.equalsIgnoreCase("reward_cbp") && _state == STARTED)
			return doReward(st, Coarse_Bone_Powder, 12);
		else if(event.equalsIgnoreCase("reward_steel") && _state == STARTED)
			return doReward(st, Steel, 9);
		else if(event.equalsIgnoreCase("reward_leather") && _state == STARTED)
			return doReward(st, Leather, 20);

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != TORRANT)
			return htmltext;
		int _state = st.getState();

		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() < 40)
			{
				htmltext = "32017-02.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "32016-01.htm";
				st.set("cond", "0");
			}
		}
		else if(_state == STARTED)
			htmltext = st.getQuestItemsCount(CURSED_DOLL) >= 180 ? "32016-05.htm" : "32016-04.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		L2Player player = qs.getRandomPartyMember(STARTED, Config.ALT_PARTY_DISTRIBUTION_RANGE);
		if(player == null)
			return null;
		QuestState st = player.getQuestState(qs.getQuest().getName());

		long CURSED_DOLL_COUNT = st.getQuestItemsCount(CURSED_DOLL);
		if(CURSED_DOLL_COUNT < 180 && Rnd.chance(CURSED_DOLL_Chance))
		{
			st.rollAndGive(CURSED_DOLL, 1, 100);
			if(CURSED_DOLL_COUNT == 179)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
		}
		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
