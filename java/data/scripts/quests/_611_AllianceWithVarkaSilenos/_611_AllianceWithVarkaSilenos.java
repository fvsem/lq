package quests._611_AllianceWithVarkaSilenos;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _611_AllianceWithVarkaSilenos extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// Varka mobs
	private static final int[] VARKA_NPC_LIST = new int[] {
			21350,
			21351,
			21353,
			21354,
			21355,
			21357,
			21358,
			21360,
			21361,
			21362,
			21369,
			21370,
			21364,
			21365,
			21366,
			21368,
			21371,
			21372,
			21373,
			21374,
			21375 };

	// hunt for soldier
	private static final int[] SOLDIER = new int[] { 21324, 21325, 21327, 21328, 21329 };

	// hunt for captain
	private static final int[] CAPTAIN = new int[] { 21331, 21332, 21334, 21335, 21336, 21338, 21343, 21344 };

	// hunt for general
	private static final int[] GENERAL = new int[] { 21339, 21340, 21342, 21345, 21346, 21347, 21348, 21349 };

	// Items
	private static final int MARK_OF_VARKA_ALLIANCE1 = 7221;
	private static final int MARK_OF_VARKA_ALLIANCE2 = 7222;
	private static final int MARK_OF_VARKA_ALLIANCE3 = 7223;
	private static final int MARK_OF_VARKA_ALLIANCE4 = 7224;
	private static final int MARK_OF_VARKA_ALLIANCE5 = 7225;
	private static final int KB_SOLDIER = 7226;
	private static final int KB_CAPTAIN = 7227;
	private static final int KB_GENERAL = 7228;
	private static final int TOTEM_OF_VALOR = 7229;
	private static final int TOTEM_OF_WISDOM = 7230;

	private static void takeAllMarks(QuestState st)
	{
		st.takeItems(MARK_OF_VARKA_ALLIANCE1, -1);
		st.takeItems(MARK_OF_VARKA_ALLIANCE2, -1);
		st.takeItems(MARK_OF_VARKA_ALLIANCE3, -1);
		st.takeItems(MARK_OF_VARKA_ALLIANCE4, -1);
		st.takeItems(MARK_OF_VARKA_ALLIANCE5, -1);
	}

	public _611_AllianceWithVarkaSilenos()
	{
		super(611, 0);

		addStartNpc(31378);
		addTalkId(31378);

		addKillId(VARKA_NPC_LIST);

		// hunt for soldier
		addKillId(SOLDIER);
		// hunt for captain
		addKillId(CAPTAIN);
		// hunt for general
		addKillId(GENERAL);

		addQuestItem(KB_SOLDIER);
		addQuestItem(KB_CAPTAIN);
		addQuestItem(KB_GENERAL);
	}

	private boolean isVarkaNpc(int npc)
	{
		return ArrayUtil.arrayContains(VARKA_NPC_LIST, npc);
	}

	private boolean isSoldier(int npc)
	{
		return ArrayUtil.arrayContains(SOLDIER, npc);
	}

	private boolean isCaptain(int npc)
	{
		return ArrayUtil.arrayContains(CAPTAIN, npc);
	}

	private boolean isGeneral(int npc)
	{
		return ArrayUtil.arrayContains(GENERAL, npc);
	}

	private static void checkMarks(QuestState st)
	{
		if(st.getInt("cond") == 0)
			return;
		if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE5) > 0)
			st.set("cond", "6");
		else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE4) > 0)
			st.set("cond", "5");
		else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE3) > 0)
			st.set("cond", "4");
		else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE2) > 0)
			st.set("cond", "3");
		else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE1) > 0)
			st.set("cond", "2");
		else
			st.set("cond", "1");
	}

	private static boolean CheckNextLevel(QuestState st, int soilder_count, int capitan_count, int general_count, int other_item, boolean take)
	{
		if(soilder_count > 0 && st.getQuestItemsCount(KB_SOLDIER) < soilder_count)
			return false;
		if(capitan_count > 0 && st.getQuestItemsCount(KB_CAPTAIN) < capitan_count)
			return false;
		if(general_count > 0 && st.getQuestItemsCount(KB_GENERAL) < general_count)
			return false;
		if(other_item > 0 && st.getQuestItemsCount(other_item) < 1)
			return false;

		if(take)
		{
			if(soilder_count > 0)
				st.takeItems(KB_SOLDIER, soilder_count);
			if(capitan_count > 0)
				st.takeItems(KB_CAPTAIN, capitan_count);
			if(general_count > 0)
				st.takeItems(KB_GENERAL, general_count);
			if(other_item > 0)
				st.takeItems(other_item, 1);
			takeAllMarks(st);
		}
		return true;
	}

	@Override
	public void onAbort(QuestState st)
	{
		takeAllMarks(st);
		st.set("cond", "0");
		st.getPlayer().updateKetraVarka();
		st.playSound(SOUND_MIDDLE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("herald_naran_q0611_04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			return event;
		}

		checkMarks(st);
		int cond = st.getInt("cond");

		if(event.equalsIgnoreCase("herald_naran_q0611_12.htm") && cond == 1 && CheckNextLevel(st, 100, 0, 0, 0, true))
		{
			st.giveItems(MARK_OF_VARKA_ALLIANCE1, 1);
			st.set("cond", "2");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("herald_naran_q0611_15.htm") && cond == 2 && CheckNextLevel(st, 200, 100, 0, 0, true))
		{
			st.giveItems(MARK_OF_VARKA_ALLIANCE2, 1);
			st.set("cond", "3");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("herald_naran_q0611_18.htm") && cond == 3 && CheckNextLevel(st, 300, 200, 100, 0, true))
		{
			st.giveItems(MARK_OF_VARKA_ALLIANCE3, 1);
			st.set("cond", "4");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("herald_naran_q0611_21.htm") && cond == 4 && CheckNextLevel(st, 300, 300, 200, TOTEM_OF_VALOR, true))
		{
			st.giveItems(MARK_OF_VARKA_ALLIANCE4, 1);
			st.set("cond", "5");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("herald_naran_q0611_23.htm") && cond == 5 && CheckNextLevel(st, 400, 400, 200, TOTEM_OF_WISDOM, true))
		{
			st.giveItems(MARK_OF_VARKA_ALLIANCE5, 1);
			st.set("cond", "6");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("herald_naran_q0611_26.htm"))
		{
			takeAllMarks(st);
			st.set("cond", "0");
			st.getPlayer().updateKetraVarka();
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		if(st.getPlayer().getKetra() > 0)
		{
			st.exitCurrentQuest(true);
			return "herald_naran_q0611_02.htm";
		}
		int npcId = npc.getNpcId();
		checkMarks(st);
		if(st.getState() == CREATED)
			st.set("cond", "0");
		int cond = st.getInt("cond");
		if(npcId == 31378)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 74)
				{
					st.exitCurrentQuest(true);
					return "herald_naran_q0611_03.htm";
				}
				return "herald_naran_q0611_01.htm";
			}
			if(cond == 1)
				return CheckNextLevel(st, 100, 0, 0, 0, false) ? "herald_naran_q0611_11.htm" : "herald_naran_q0611_10.htm";
			if(cond == 2)
				return CheckNextLevel(st, 200, 100, 0, 0, false) ? "herald_naran_q0611_14.htm" : "herald_naran_q0611_13.htm";
			if(cond == 3)
				return CheckNextLevel(st, 300, 200, 100, 0, false) ? "herald_naran_q0611_17.htm" : "herald_naran_q0611_16.htm";
			if(cond == 4)
				return CheckNextLevel(st, 300, 300, 200, TOTEM_OF_VALOR, false) ? "herald_naran_q0611_20.htm" : "herald_naran_q0611_19.htm";
			if(cond == 5)
				return CheckNextLevel(st, 400, 400, 200, TOTEM_OF_WISDOM, false) ? "herald_naran_q0611_27.htm" : "herald_naran_q0611_22.htm";
			if(cond == 6)
				return "herald_naran_q0611_24.htm";
		}
		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(isVarkaNpc(npcId))
			if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE5) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_VARKA_ALLIANCE4, 1);
				st.getPlayer().updateKetraVarka();
				checkMarks(st);
			}
			else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE4) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_VARKA_ALLIANCE3, 1);
				st.getPlayer().updateKetraVarka();
				checkMarks(st);
			}
			else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE3) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_VARKA_ALLIANCE2, 1);
				st.getPlayer().updateKetraVarka();
				checkMarks(st);
			}
			else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE2) > 0)
			{
				takeAllMarks(st);
				st.giveItems(MARK_OF_VARKA_ALLIANCE1, 1);
				st.getPlayer().updateKetraVarka();
				checkMarks(st);
			}
			else if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE1) > 0)
			{
				takeAllMarks(st);
				st.getPlayer().updateKetraVarka();
				checkMarks(st);
			}
			else if(st.getPlayer().getVarka() > 0)
			{
				st.getPlayer().updateKetraVarka();
				st.exitCurrentQuest(true);
				return "herald_naran_q0611_26.htm";
			}

		if(st.getQuestItemsCount(MARK_OF_VARKA_ALLIANCE5) > 0)
			return null;

		if(isSoldier(npcId))
		{
			if(st.getInt("cond") > 0)
			{
				st.rollAndGive(KB_SOLDIER, 1, 60);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(isCaptain(npcId))
		{
			if(st.getInt("cond") > 1)
			{
				st.rollAndGive(KB_CAPTAIN, 1, 70);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(isGeneral(npcId))
			if(st.getInt("cond") > 2)
			{
				st.rollAndGive(KB_GENERAL, 1, 80);
				st.playSound(SOUND_MIDDLE);
			}
		return null;

	}
}
