package quests._268_TracesOfEvil;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Traces Of Evil
 * 
 * @author Ruslaner
 * @Last_Fixes by Felixx
 */

public class _268_TracesOfEvil extends Quest implements ScriptFile
{
	// NPC
	public final int KUNAI = 30559;
	// MOBS
	public final int SPIDER = 20474;
	public final int FANG_SPIDER = 20476;
	public final int BLADE_SPIDER = 20478;
	// ITEMS
	public final int CONTAMINATED = 10869;
	public final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 268: Traces Of Evil");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _268_TracesOfEvil()
	{
		super(268, -1);
		addStartNpc(KUNAI);
		addTalkId(KUNAI);
		addKillId(SPIDER);
		addKillId(FANG_SPIDER);
		addKillId(BLADE_SPIDER);
		addQuestItem(CONTAMINATED);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30559-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(st.getInt("cond") == 0)
			if(st.getPlayer().getLevel() < 15)
			{
				htmltext = "30559-01.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30559-00.htm";
		else if(st.getQuestItemsCount(CONTAMINATED) >= 30)
		{
			htmltext = "30559-04.htm";
			st.giveItems(ADENA, 2474);
			st.addExpAndSp(8738, 409);
			st.playSound(SOUND_FINISH);
			st.unset("cond");
			st.exitCurrentQuest(true);
		}
		else
			htmltext = "30559-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		st.giveItems(CONTAMINATED, 1);
		if(st.getQuestItemsCount(CONTAMINATED) <= 29)
			st.playSound(SOUND_ITEMGET);
		else if(st.getQuestItemsCount(CONTAMINATED) >= 30)
		{
			st.playSound(SOUND_MIDDLE);
			st.set("cond", "2");
			st.setState(STARTED);
		}
		return null;
	}
}
