package quests._601_WatchingEyes;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _601_WatchingEyes extends Quest implements ScriptFile
{
	// NPC
	private static int EYE_OF_ARGOS = 31683;
	// ITEMS
	private static int PROOF_OF_AVENGER = 7188;
	// CHANCE
	private static int DROP_CHANCE = 50;
	// MOBS
	private static int[] MOBS = { 21306, 21308, 21309, 21310, 21311 };
	private static int[][] REWARDS = { { 6699, 90000, 0, 19 }, { 6698, 80000, 20, 39 }, { 6700, 40000, 40, 49 }, { 0, 230000, 50, 100 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 601: Watching Eyes");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _601_WatchingEyes()
	{
		super(601, -1);

		addStartNpc(EYE_OF_ARGOS);

		addKillId(MOBS);

		addQuestItem(PROOF_OF_AVENGER);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31683-1.htm"))
			if(st.getPlayer().getLevel() < 71)
			{
				htmltext = "31683-0a.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				st.setState(STARTED);
				st.set("cond", "1");
				st.playSound(SOUND_ACCEPT);
			}
		else if(event.equalsIgnoreCase("31683-4.htm"))
		{
			int random = Rnd.get(101);
			int i = 0;
			int item = 0;
			int adena = 0;
			while (i < REWARDS.length)
			{
				item = REWARDS[i][0];
				adena = REWARDS[i][1];
				if(REWARDS[i][2] <= random && random <= REWARDS[i][3])
					break;
				i++;
			}
			st.giveItems(57, adena);
			if(item != 0)
			{
				st.giveItems(item, 5);
				st.addExpAndSp(120000, 10000);
			}
			st.takeItems(PROOF_OF_AVENGER, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");

		if(cond == 0)
			htmltext = "31683-0.htm";
		else if(cond == 1)
			htmltext = "31683-2.htm";
		else if(cond == 2 && st.getQuestItemsCount(PROOF_OF_AVENGER) == 100)
			htmltext = "31683-3.htm";
		else
		{
			htmltext = "31683-4a.htm";
			st.set("cond", "1");
		}

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
			return null;

		if(st.getInt("cond") == 1 && st.getQuestItemsCount(PROOF_OF_AVENGER) < 100)
			if(Rnd.chance(DROP_CHANCE))
			{
				st.giveItems(PROOF_OF_AVENGER, 1);

				if(st.getQuestItemsCount(PROOF_OF_AVENGER) == 100)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
