package quests._328_SenseForBusiness;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _328_SenseForBusiness extends Quest implements ScriptFile
{
	// NPC
	private int SARIEN = 30436;
	// items
	private int MONSTER_EYE_CARCASS = 1347;
	private int MONSTER_EYE_LENS = 1366;
	private int BASILISK_GIZZARD = 1348;
	private int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 328: Sense For Business");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _328_SenseForBusiness()
	{
		super(328, -1);

		addStartNpc(SARIEN);
		addKillId(20055);
		addKillId(20059);
		addKillId(20067);
		addKillId(20068);
		addKillId(20070);
		addKillId(20072);
		addQuestItem(MONSTER_EYE_CARCASS);
		addQuestItem(MONSTER_EYE_LENS);
		addQuestItem(BASILISK_GIZZARD);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30436-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30436-06.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext;
		int id = st.getState();
		if(id == CREATED)
			st.set("cond", "0");
		if(st.getInt("cond") == 0)
		{
			if(st.getPlayer().getLevel() >= 21)
			{
				htmltext = "30436-02.htm";
				return htmltext;
			}
			htmltext = "30436-01.htm";
			st.exitCurrentQuest(true);
		}
		else
		{
			long carcass = st.getQuestItemsCount(MONSTER_EYE_CARCASS);
			long lenses = st.getQuestItemsCount(MONSTER_EYE_LENS);
			long gizzard = st.getQuestItemsCount(BASILISK_GIZZARD);
			if(carcass + lenses + gizzard > 0)
			{
				st.giveItems(ADENA, 30 * carcass + 2000 * lenses + 75 * gizzard);
				st.takeItems(MONSTER_EYE_CARCASS, -1);
				st.takeItems(MONSTER_EYE_LENS, -1);
				st.takeItems(BASILISK_GIZZARD, -1);
				htmltext = "30436-05.htm";
			}
			else
				htmltext = "30436-04.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int n = Rnd.get(1, 100);
		if(npcId == 20055)
		{
			if(n < 47)
			{
				st.giveItems(MONSTER_EYE_CARCASS, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(n < 49)
			{
				st.giveItems(MONSTER_EYE_LENS, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20059)
		{
			if(n < 51)
			{
				st.giveItems(MONSTER_EYE_CARCASS, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(n < 53)
			{
				st.giveItems(MONSTER_EYE_LENS, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20067)
		{
			if(n < 67)
			{
				st.giveItems(MONSTER_EYE_CARCASS, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(n < 69)
			{
				st.giveItems(MONSTER_EYE_LENS, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20068)
		{
			if(n < 75)
			{
				st.giveItems(MONSTER_EYE_CARCASS, 1);
				st.playSound(SOUND_ITEMGET);
			}
			else if(n < 77)
			{
				st.giveItems(MONSTER_EYE_LENS, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20070)
		{
			if(n < 50)
			{
				st.giveItems(BASILISK_GIZZARD, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20072)
			if(n < 51)
			{
				st.giveItems(BASILISK_GIZZARD, 1);
				st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
