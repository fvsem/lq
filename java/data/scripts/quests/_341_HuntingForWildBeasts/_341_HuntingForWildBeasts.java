package quests._341_HuntingForWildBeasts;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _341_HuntingForWildBeasts extends Quest implements ScriptFile
{
	// NPCs
	private static int PANO = 30078;
	// Mobs
	private static int Red_Bear = 20021;
	private static int Dion_Grizzly = 20203;
	private static int Brown_Bear = 20310;
	private static int Grizzly_Bear = 20335;
	// Quest Items
	private static int BEAR_SKIN = 4259;
	// Items
	private static int ADENA = 57;
	// Chances
	private static int BEAR_SKIN_CHANCE = 40;

	public _341_HuntingForWildBeasts()
	{
		super(341, -1);
		addStartNpc(PANO);
		addKillId(Red_Bear);
		addKillId(Dion_Grizzly);
		addKillId(Brown_Bear);
		addKillId(Grizzly_Bear);
		addQuestItem(BEAR_SKIN);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30078-02.htm") && st.getState() == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != PANO)
			return htmltext;
		int _state = st.getState();
		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() >= 20)
			{
				htmltext = "30078-01.htm";
				st.set("cond", "0");
			}
			else
			{
				htmltext = "30078-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(_state == STARTED)
			if(st.getQuestItemsCount(BEAR_SKIN) >= 20)
			{
				htmltext = "30078-04.htm";
				st.takeItems(BEAR_SKIN, -1);
				st.giveItems(ADENA, 3710);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30078-03.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		long BEAR_SKIN_COUNT = qs.getQuestItemsCount(BEAR_SKIN);
		if(BEAR_SKIN_COUNT < 20 && Rnd.chance(BEAR_SKIN_CHANCE))
		{
			qs.giveItems(BEAR_SKIN, 1);
			if(BEAR_SKIN_COUNT == 19)
			{
				qs.set("cond", "2");
				qs.playSound(SOUND_MIDDLE);
			}
			else
				qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 341: Hunting For Wild Beasts");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
