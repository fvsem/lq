package quests._261_CollectorsDream;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _261_CollectorsDream extends Quest implements ScriptFile
{
	// Items
	int GIANT_SPIDER_LEG = 1087;
	int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 261: Collectors Dream");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _261_CollectorsDream()
	{
		super(261, -1);

		addStartNpc(30222);

		addTalkId(30222);

		addKillId(20308);
		addKillId(20460);
		addKillId(20466);

		addQuestItem(GIANT_SPIDER_LEG);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.intern().equalsIgnoreCase("30222-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 15)
			{
				htmltext = "30222-02.htm";
				return htmltext;
			}
			htmltext = "30222-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(cond == 1 || st.getQuestItemsCount(GIANT_SPIDER_LEG) < 8)
			htmltext = "30222-04.htm";
		else if(cond == 2 && st.getQuestItemsCount(GIANT_SPIDER_LEG) >= 8)
		{
			st.giveItems(ADENA, 1000);
			st.addExpAndSp(2000, 0);
			st.takeItems(GIANT_SPIDER_LEG, -1);
			htmltext = "30222-05.htm";
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1 && st.getQuestItemsCount(GIANT_SPIDER_LEG) < 8)
		{
			st.giveItems(GIANT_SPIDER_LEG, 1);
			if(st.getQuestItemsCount(GIANT_SPIDER_LEG) == 8)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
			else
				st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
