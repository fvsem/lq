package quests._295_DreamsOfTheSkies;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _295_DreamsOfTheSkies extends Quest implements ScriptFile
{
	public static int FLOATING_STONE = 1492;
	public static int RING_OF_FIREFLY = 1509;
	public static int ADENA = 57;

	public static int Arin = 30536;
	public static int MagicalWeaver = 20153;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 295: Dreams Of The Skies");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _295_DreamsOfTheSkies()
	{
		super(295, -1);

		addStartNpc(Arin);
		addTalkId(Arin);
		addKillId(MagicalWeaver);

		addQuestItem(FLOATING_STONE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30536-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();

		if(id == CREATED)
			st.set("cond", "0");
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 11)
			{
				htmltext = "30536-02.htm";
				return htmltext;
			}
			htmltext = "30536-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(cond == 1 || st.getQuestItemsCount(FLOATING_STONE) < 50)
			htmltext = "30536-04.htm";
		else if(cond == 2 && st.getQuestItemsCount(FLOATING_STONE) == 50)
		{
			st.addExpAndSp(0, 500);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			if(st.getQuestItemsCount(RING_OF_FIREFLY) < 1)
			{
				htmltext = "30536-05.htm";
				st.giveItems(RING_OF_FIREFLY, 1);
			}
			else
			{
				htmltext = "30536-06.htm";
				st.giveItems(ADENA, 2400);
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1 && st.getQuestItemsCount(FLOATING_STONE) < 50)
			if(Rnd.chance(25))
			{
				st.giveItems(FLOATING_STONE, 1);
				if(st.getQuestItemsCount(FLOATING_STONE) == 50)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
			else if(st.getQuestItemsCount(FLOATING_STONE) >= 48)
			{
				st.giveItems(FLOATING_STONE, 50 - st.getQuestItemsCount(FLOATING_STONE));
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
			else
			{
				st.giveItems(FLOATING_STONE, 2);
				st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
