package quests._001_LettersOfLove;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;

/**
 * Рейты не применяются, награда нестекуемая
 */

public class _001_LettersOfLove extends Quest implements ScriptFile
{
	public final int DARIN = 30048;
	public final int ROXXY = 30006;
	public final int BAULRO = 30033;

	public final short DARINGS_LETTER = 687;
	public final short RAPUNZELS_KERCHIEF = 688;
	public final short DARINGS_RECEIPT = 1079;
	public final short BAULS_POTION = 1080;
	public final short NECKLACE = 906;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _001_LettersOfLove()
	{
		super(1, -1);

		addStartNpc(DARIN);
		addTalkId(ROXXY);
		addTalkId(BAULRO);
		addQuestItem(DARINGS_LETTER);
		addQuestItem(RAPUNZELS_KERCHIEF);
		addQuestItem(DARINGS_RECEIPT);
		addQuestItem(BAULS_POTION);
	}

	@Override
	public String onEvent(String event, QuestState qs, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30048-06.htm"))
		{
			qs.set("cond", "1");
			qs.setState(STARTED);
			qs.giveItems(DARINGS_LETTER, 1, false);
			qs.playSound(SOUND_ACCEPT);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		switch (npcId)
		{
			case DARIN:
				if(cond == 0)
				{
					if(st.getPlayer().getLevel() >= 2)
						htmltext = "30048-02.htm";
					else
					{
						htmltext = "30048-00.htm";
						st.exitCurrentQuest(true);
					}
				}
				else if(cond == 1)
					htmltext = "30048-07.htm";
				else if(cond == 2 && st.getQuestItemsCount(RAPUNZELS_KERCHIEF) == 1)
				{
					htmltext = "30048-08.htm";
					st.takeItems(RAPUNZELS_KERCHIEF, -1);
					st.giveItems(DARINGS_RECEIPT, 1, false);
					st.set("cond", "3");
					st.playSound(SOUND_MIDDLE);
				}
				else if(cond == 3)
					htmltext = "30048-09.htm";
				else if(cond == 4 && st.getQuestItemsCount(BAULS_POTION) == 1)
				{
					htmltext = "30048-10.htm";
					st.takeItems(BAULS_POTION, -1);
					st.giveItems(NECKLACE, 1, false);
					st.getPlayer().sendPacket(new ExShowScreenMessage("  Delivery duty complete.\nGo find the Newbie Guide.", 5000, ScreenMessageAlign.TOP_CENTER, true));
					st.giveItems(ADENA_ID, (int) ((Config.RATE_QUESTS_REWARD - 1) * 1200 + 2466 * Config.RATE_QUESTS_REWARD), false); // T2
					st.addExpAndSp(5672, 446);
					st.unset("cond");
					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(false);
				}
				break;
			case ROXXY:
				if(cond == 1 && st.getQuestItemsCount(RAPUNZELS_KERCHIEF) == 0 && st.getQuestItemsCount(DARINGS_LETTER) > 0)
				{
					htmltext = "30006-01.htm";
					st.takeItems(DARINGS_LETTER, -1);
					st.giveItems(RAPUNZELS_KERCHIEF, 1, false);
					st.set("cond", "2");
					st.playSound(SOUND_MIDDLE);
				}
				else if(cond == 2 && st.getQuestItemsCount(RAPUNZELS_KERCHIEF) > 0)
					htmltext = "30006-02.htm";
				else if(cond > 2 && (st.getQuestItemsCount(BAULS_POTION) > 0 || st.getQuestItemsCount(DARINGS_RECEIPT) > 0))
					htmltext = "30006-03.htm";
				break;
			case BAULRO:
				if(cond == 3 && st.getQuestItemsCount(DARINGS_RECEIPT) == 1)
				{
					htmltext = "30033-01.htm";
					st.takeItems(DARINGS_RECEIPT, -1);
					st.giveItems(BAULS_POTION, 1, false);
					st.set("cond", "4");
					st.playSound(SOUND_MIDDLE);
				}
				else if(cond == 4)
					htmltext = "30033-02.htm";
				break;
		}
		return htmltext;
	}
}
