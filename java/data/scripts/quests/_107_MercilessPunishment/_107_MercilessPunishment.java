package quests._107_MercilessPunishment;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _107_MercilessPunishment extends Quest implements ScriptFile
{
	private static int HATOSS_ORDER1 = 1553;
	private static int HATOSS_ORDER2 = 1554;
	private static int HATOSS_ORDER3 = 1555;
	private static int LETTER_TO_HUMAN = 1557;
	private static int LETTER_TO_DARKELF = 1556;
	private static int LETTER_TO_ELF = 1558;
	private static int ADENA = 57;
	private static int BUTCHER = 1510;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _107_MercilessPunishment()
	{
		super(107, -1);

		addStartNpc(30568);

		addTalkId(30568);
		addTalkId(30580);

		addKillId(27041);

		addQuestItem(new int[] { LETTER_TO_DARKELF, LETTER_TO_HUMAN, LETTER_TO_ELF, HATOSS_ORDER1, HATOSS_ORDER2, HATOSS_ORDER3 });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30568-03.htm"))
		{
			st.giveItems(HATOSS_ORDER1, 1);
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30568-06.htm"))
		{
			st.takeItems(HATOSS_ORDER2, 1);
			st.takeItems(LETTER_TO_DARKELF, 1);
			st.takeItems(LETTER_TO_HUMAN, 1);
			st.takeItems(LETTER_TO_ELF, 1);
			st.takeItems(HATOSS_ORDER1, 1);
			st.takeItems(HATOSS_ORDER2, 1);
			st.takeItems(HATOSS_ORDER3, 1);
			st.giveItems(ADENA, 200);
			st.unset("cond");
			st.playSound(SOUND_GIVEUP);
		}
		else if(event.equalsIgnoreCase("30568-07.htm"))
		{
			st.takeItems(HATOSS_ORDER1, 1);
			if(st.getQuestItemsCount(HATOSS_ORDER2) == 0)
				st.giveItems(HATOSS_ORDER2, 1);
		}
		else if(event.equalsIgnoreCase("30568-09.htm"))
		{
			st.takeItems(HATOSS_ORDER2, 1);
			if(st.getQuestItemsCount(HATOSS_ORDER3) == 0)
				st.giveItems(HATOSS_ORDER3, 1);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		String htmltext = "noquest";
		if(npcId == 30568)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 3)
				{
					htmltext = "30568-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 10)
					htmltext = "30568-02.htm";
				else
				{
					htmltext = "30568-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && st.getQuestItemsCount(HATOSS_ORDER1) > 0)
				htmltext = "30568-04.htm";
			else if(cond == 2 && st.getQuestItemsCount(HATOSS_ORDER1) > 0 && st.getQuestItemsCount(LETTER_TO_HUMAN) == 0)
				htmltext = "30568-04.htm";
			else if(cond == 3 && st.getQuestItemsCount(HATOSS_ORDER1) > 0 && st.getQuestItemsCount(LETTER_TO_HUMAN) >= 1)
			{
				htmltext = "30568-05.htm";
				st.set("cond", "4");
			}
			else if(cond == 4 && st.getQuestItemsCount(HATOSS_ORDER2) > 0 && st.getQuestItemsCount(LETTER_TO_DARKELF) == 0)
				htmltext = "30568-05.htm";
			else if(cond == 5 && st.getQuestItemsCount(HATOSS_ORDER2) > 0 && st.getQuestItemsCount(LETTER_TO_DARKELF) >= 1)
			{
				htmltext = "30568-08.htm";
				st.set("cond", "6");
			}
			else if(cond == 6 && st.getQuestItemsCount(HATOSS_ORDER3) > 0 && st.getQuestItemsCount(LETTER_TO_ELF) == 0)
				htmltext = "30568-08.htm";
			else if(cond == 7 && st.getQuestItemsCount(HATOSS_ORDER3) > 0 && st.getQuestItemsCount(LETTER_TO_ELF) + st.getQuestItemsCount(LETTER_TO_HUMAN) + st.getQuestItemsCount(LETTER_TO_DARKELF) == 3)
			{
				htmltext = "30568-10.htm";
				st.takeItems(LETTER_TO_DARKELF, -1);
				st.takeItems(LETTER_TO_HUMAN, -1);
				st.takeItems(LETTER_TO_ELF, -1);
				st.takeItems(HATOSS_ORDER3, -1);
				for(int item = 4412; item <= 4417; item++)
					st.giveItems(item, 10);
				st.giveItems(1060, 100);
				if(st.getPlayer().getClassId().isMage())
				{
					st.giveItems(5790, 3000);
					st.playTutorialVoice("tutorial_voice_027");
				}
				else
				{
					st.giveItems(5789, 7000);
					st.playTutorialVoice("tutorial_voice_026");
				}
				st.giveItems(BUTCHER, 1);
				st.unset("cond");
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		}
		else if(npcId == 30580 && cond >= 1 && (st.getQuestItemsCount(HATOSS_ORDER1) > 0 || st.getQuestItemsCount(HATOSS_ORDER2) > 0 || st.getQuestItemsCount(HATOSS_ORDER3) > 0))
		{
			if(cond == 1)
				st.set("cond", "2");
			htmltext = "30580-01.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 27041)
			if(cond == 2 && st.getQuestItemsCount(HATOSS_ORDER1) > 0 && st.getQuestItemsCount(LETTER_TO_HUMAN) == 0)
			{
				st.giveItems(LETTER_TO_HUMAN, 1);
				st.set("cond", "3");
				st.playSound(SOUND_ITEMGET);
			}
			else if(cond == 4 && st.getQuestItemsCount(HATOSS_ORDER2) > 0 && st.getQuestItemsCount(LETTER_TO_DARKELF) == 0)
			{
				st.giveItems(LETTER_TO_DARKELF, 1);
				st.set("cond", "5");
				st.playSound(SOUND_ITEMGET);
			}
			else if(cond == 6 && st.getQuestItemsCount(HATOSS_ORDER3) > 0 && st.getQuestItemsCount(LETTER_TO_ELF) == 0)
			{
				st.giveItems(LETTER_TO_ELF, 1);
				st.set("cond", "7");
				st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
