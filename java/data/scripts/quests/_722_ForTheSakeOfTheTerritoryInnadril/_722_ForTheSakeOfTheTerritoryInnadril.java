package quests._722_ForTheSakeOfTheTerritoryInnadril;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _722_ForTheSakeOfTheTerritoryInnadril extends TerritoryWarSuperClass
{
	public _722_ForTheSakeOfTheTerritoryInnadril()
	{
		super(722);
		_catapultId = 36504;
		_territoryId = 6;
		_leaderIDs = new int[] { 36538, 36540, 36543, 36596 };
		_guardIDs = new int[] { 36539, 36541, 36542 };
		_text = new String[] { "The catapult of Innadril has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
