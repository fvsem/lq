package quests._281_HeadForTheHills;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _281_HeadForTheHills extends Quest implements ScriptFile
{
	// NPC
	public final int Marcela = 32173;
	// Mobs
	public final int GreenGoblin = 22234;
	public final int MountainWerewolf = 22235;
	public final int MuertosArcher = 22236;
	public final int MountainFungus = 22237;
	public final int MountainWerewolfChief = 22238;
	public final int MuertosGuard = 22239;
	// QuestItem
	public final int HillsOfGoldMonsterClaw = 9796;
	// Items
	public final int ScrollOfEscape = 736;
	public final int SoulshotNoGradeforBeginners = 5789;

	// Drop Cond
	// # [ID, CHANCE]
	public final int[][] DROPLIST = {
			{ GreenGoblin, 70 },
			{ MountainWerewolf, 75 },
			{ MuertosArcher, 80 },
			{ MountainFungus, 70 },
			{ MountainWerewolfChief, 90 },
			{ MuertosGuard, 90 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 281: Head for The Hills");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _281_HeadForTheHills()
	{
		super(281, -1);
		addStartNpc(Marcela);

		for(int[] element : DROPLIST)
			addKillId(element[0]);

		addQuestItem(HillsOfGoldMonsterClaw);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32173-03.htm"))
		{
			if(st.getInt("cond") == 0)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
		}
		else if(event.equalsIgnoreCase("adena"))
		{
			st.giveItems(57, st.getQuestItemsCount(HillsOfGoldMonsterClaw) * 50);
			st.takeItems(HillsOfGoldMonsterClaw, -1);
			if(!st.getPlayer().getVarB("q281"))
			{
				if(st.getPlayer().getClassId().isMage())
					st.giveItems(5790, 3000);
				else
					st.giveItems(5789, 6000);
				st.getPlayer().setVar("q281", "1");
			}
			htmltext = "32173-05.htm";
		}
		else if(event.equalsIgnoreCase("soe"))
		{
			if(st.getQuestItemsCount(HillsOfGoldMonsterClaw) >= 50)
			{
				st.takeItems(HillsOfGoldMonsterClaw, 50);
				st.giveItems(ScrollOfEscape, 5);
				st.giveItems(SoulshotNoGradeforBeginners, 6000);
				htmltext = "32173-05.htm";
			}
			else
				htmltext = "32173-07.htm";
		}
		else if(event.equalsIgnoreCase("32173-06.htm"))
			st.exitCurrentQuest(true);

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == Marcela)
			if(st.getPlayer().getLevel() < 6)
			{
				htmltext = "32173-01.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 0)
				htmltext = "32173-02.htm";
			else if(cond == 1 && st.getQuestItemsCount(HillsOfGoldMonsterClaw) > 0)
				htmltext = "32173-04.htm";
			else
				htmltext = "32173-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond != 1)
			return null;
		for(int[] element : DROPLIST)
			if(npcId == element[0])
			{
				st.rollAndGive(HillsOfGoldMonsterClaw, 1, element[1]);
				return null;
			}
		return null;
	}

}
