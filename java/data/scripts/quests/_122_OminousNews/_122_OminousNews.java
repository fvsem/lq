package quests._122_OminousNews;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _122_OminousNews extends Quest implements ScriptFile
{
	private static final int MOIRA = 31979;
	private static final int KARUDA = 32017;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _122_OminousNews()
	{
		super(122, -1);

		addStartNpc(MOIRA);
		addTalkId(KARUDA);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getInt("cond");
		htmltext = event;
		if(htmltext.equalsIgnoreCase("31979-03.htm") && cond == 0)
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(htmltext.equalsIgnoreCase("32017-02.htm"))
			// не догнал зачем тут ОК. Логику сохранил
			if(cond == 1 && st.getInt("ok") > 0)
			{
				st.unset("cond");
				st.unset("ok");
				st.giveItems(ADENA_ID, 8923);
				st.addExpAndSp(45151, 2310); // награда соответствует Т2
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else
				htmltext = "noquest";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == MOIRA)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 20)
					htmltext = "31979-02.htm";
				else
				{
					htmltext = "31979-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
				htmltext = "31979-03.htm";
		}
		else if(npcId == KARUDA && cond == 1)
		{
			htmltext = "32017-01.htm";
			st.set("ok", "1");
		}
		return htmltext;
	}
}
