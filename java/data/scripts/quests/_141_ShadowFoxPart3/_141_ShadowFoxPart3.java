package quests._141_ShadowFoxPart3;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Shadow Fox Part 3
 */
public class _141_ShadowFoxPart3 extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPCs
	private final static int NATOOLS = 30894;

	// ITEMs
	private final static int REPORT = 10350;

	// MONSTERs
	private final static int[] NPC = { 20791, 20792, 20135 };

	public _141_ShadowFoxPart3()
	{
		super(141, -1);

		addStartNpc(NATOOLS);

		addTalkId(NATOOLS);

		addQuestItem(REPORT);

		for(int i : NPC)
			addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30894-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30894-04.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30894-15.htm"))
		{
			st.set("cond", "4");
			st.setState(STARTED);
			st.unset("talk");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30894-18.htm"))
		{
			if(st.getInt("reward") != 1)
			{
				st.playSound(SOUND_FINISH);
				st.giveItems(57, 88888, true);
				if(st.getPlayer().getLevel() >= 37 && st.getPlayer().getLevel() <= 42)
					st.addExpAndSp(278005, 17058);
				st.set("reward", "1");
				htmltext = "select.htm";
			}
			else
				htmltext = "select.htm";
		}
		else if(event.equalsIgnoreCase("dawn"))
		{
			QuestState q1 = st.getPlayer().getQuestState("_142_FallenAngelRequestOfDawn");
			if(q1.isStarted() == false)
			{
				q1.setState(STARTED);
				st.unset("cond");
				st.unset("talk");
				st.unset("reward");
				htmltext = null;
				st.exitCurrentQuest(false);
			}
		}
		else if(event.equalsIgnoreCase("dusk"))
		{
			QuestState q2 = st.getPlayer().getQuestState("_143_FallenAngelRequestOfDusk");
			if(q2.isStarted() == false)
			{
				q2.setState(STARTED);
				st.unset("cond");
				st.unset("talk");
				st.unset("reward");
				htmltext = null;
				st.exitCurrentQuest(false);
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		String htmltext = "noquest";
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 37)
			{
				QuestState qs140 = st.getPlayer().getQuestState("_140_ShadowFoxPart2");
				if(qs140 != null)
					htmltext = "30894-01.htm";
				else
					htmltext = "need.htm";
			}
			else
			{
				htmltext = "30894-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(cond == 1)
			htmltext = "30894-02.htm";
		else if(cond == 2)
			htmltext = "30894-05.htm";
		else if(cond == 3)
		{
			if(st.getInt("talk") == 1)
				htmltext = "30894-07.htm";
			else
			{
				htmltext = "30894-06.htm";
				st.takeItems(REPORT, -1);
				st.set("talk", "1");
			}
		}
		else if(cond == 4)
			htmltext = "30894-16.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 2 && Rnd.get(100) <= 80 && st.getQuestItemsCount(REPORT) < 30)
		{
			st.giveItems(REPORT, 1);
			if(st.getQuestItemsCount(REPORT) >= 30)
			{
				st.set("cond", "3");
				st.setState(STARTED);
				st.playSound(SOUND_MIDDLE);
			}
			else
				st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
