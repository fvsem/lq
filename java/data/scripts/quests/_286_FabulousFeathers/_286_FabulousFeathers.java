package quests._286_FabulousFeathers;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _286_FabulousFeathers extends Quest implements ScriptFile
{
	// NPCs
	private static int ERINU = 32164;
	// Mobs
	private static int Shady_Muertos_Captain = 22251;
	private static int Shady_Muertos_Warrior = 22253;
	private static int Shady_Muertos_Archer = 22254;
	private static int Shady_Muertos_Commander = 22255;
	private static int Shady_Muertos_Wizard = 22256;
	// Quest Items
	private static int Commanders_Feather = 9746;
	// Items
	private static int ADENA = 57;
	// Chances
	private static int Commanders_Feather_Chance = 66;

	public _286_FabulousFeathers()
	{
		super(286, -1);
		addStartNpc(ERINU);
		addKillId(Shady_Muertos_Captain);
		addKillId(Shady_Muertos_Warrior);
		addKillId(Shady_Muertos_Archer);
		addKillId(Shady_Muertos_Commander);
		addKillId(Shady_Muertos_Wizard);
		addQuestItem(Commanders_Feather);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("32164-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32164-06.htm") && _state == STARTED)
		{
			st.takeItems(Commanders_Feather, -1);
			st.giveItems(ADENA, 4160);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != ERINU)
			return htmltext;
		int _state = st.getState();

		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() >= 17)
			{
				htmltext = "32164-01.htm";
				st.set("cond", "0");
			}
			else
			{
				htmltext = "32164-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(_state == STARTED)
			htmltext = st.getQuestItemsCount(Commanders_Feather) >= 80 ? "32164-05.htm" : "32164-04.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		long Commanders_Feather_count = qs.getQuestItemsCount(Commanders_Feather);
		if(Commanders_Feather_count < 80 && Rnd.chance(Commanders_Feather_Chance))
		{
			qs.giveItems(Commanders_Feather, 1);
			if(Commanders_Feather_count == 79)
			{
				qs.set("cond", "2");
				qs.playSound(SOUND_MIDDLE);
			}
			else
				qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 286: Fabulous Feathers");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
