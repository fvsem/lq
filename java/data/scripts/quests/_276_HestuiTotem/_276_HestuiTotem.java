package quests._276_HestuiTotem;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _276_HestuiTotem extends Quest implements ScriptFile
{
	// NPCs
	private static int Tanapi = 30571;
	// Mobs
	private static int Kasha_Bear = 20479;
	private static int Kasha_Bear_Totem_Spirit = 27044;
	// Items
	private static int Leather_Pants = 29;
	private static int Totem_of_Hestui = 1500;
	// Quest Items
	private static int Kasha_Parasite = 1480;
	private static int Kasha_Crystal = 1481;

	public _276_HestuiTotem()
	{
		super(276, -1);
		addStartNpc(Tanapi);
		addKillId(Kasha_Bear);
		addKillId(Kasha_Bear_Totem_Spirit);
		addQuestItem(Kasha_Parasite);
		addQuestItem(Kasha_Crystal);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30571-03.htm") && st.getState() == CREATED && st.getPlayer().getRace().ordinal() == 3 && st.getPlayer().getLevel() >= 15)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != Tanapi)
			return htmltext;
		int _state = st.getState();

		if(_state == CREATED)
		{
			if(st.getPlayer().getRace().ordinal() != 3)
			{
				htmltext = "30571-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 15)
			{
				htmltext = "30571-01.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30571-02.htm";
				st.set("cond", "0");
			}
		}
		else if(_state == STARTED)
			if(st.getQuestItemsCount(Kasha_Crystal) > 0)
			{
				htmltext = "30571-05.htm";
				st.takeItems(Kasha_Parasite, -1);
				st.takeItems(Kasha_Crystal, -1);
				st.giveItems(Leather_Pants, 1);
				st.giveItems(Totem_of_Hestui, 1);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30571-04.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;
		int npcId = npc.getNpcId();

		if(npcId == Kasha_Bear && qs.getQuestItemsCount(Kasha_Crystal) == 0)
		{
			if(qs.getQuestItemsCount(Kasha_Parasite) < 50)
			{
				qs.giveItems(Kasha_Parasite, 1);
				qs.playSound(SOUND_ITEMGET);
			}
			else
			{
				qs.takeItems(Kasha_Parasite, -1);
				qs.addSpawn(Kasha_Bear_Totem_Spirit);
			}
		}
		else if(npcId == Kasha_Bear_Totem_Spirit && qs.getQuestItemsCount(Kasha_Crystal) == 0)
		{
			qs.giveItems(Kasha_Crystal, 1);
			qs.playSound(SOUND_MIDDLE);
		}

		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
