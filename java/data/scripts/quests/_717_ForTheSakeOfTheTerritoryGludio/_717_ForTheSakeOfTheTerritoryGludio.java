package quests._717_ForTheSakeOfTheTerritoryGludio;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _717_ForTheSakeOfTheTerritoryGludio extends TerritoryWarSuperClass
{
	public _717_ForTheSakeOfTheTerritoryGludio()
	{
		super(717);
		_catapultId = 36499;
		_territoryId = 1;
		_leaderIDs = new int[] { 36508, 36510, 36513, 36591 };
		_guardIDs = new int[] { 36509, 36511, 36512 };
		_text = new String[] { "The catapult of Gludio has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
