package quests._011_SecretMeetingWithKetraOrcs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _011_SecretMeetingWithKetraOrcs extends Quest implements ScriptFile
{
	int CADMON = 31296;
	int LEON = 31256;
	int WAHKAN = 31371;

	int MUNITIONS_BOX = 7231;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _011_SecretMeetingWithKetraOrcs()
	{
		super(11, -1);

		addStartNpc(CADMON);

		addTalkId(CADMON);
		addTalkId(LEON);
		addTalkId(WAHKAN);

		addQuestItem(MUNITIONS_BOX);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31296-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31256-2.htm"))
		{
			st.giveItems(MUNITIONS_BOX, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("31371-2.htm"))
		{
			st.takeItems(MUNITIONS_BOX, 1);
			st.addExpAndSp(22787, 0);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == CADMON)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 74)
					htmltext = "31296-1.htm";
				else
				{
					htmltext = "31296-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "31296-2r.htm";
		}
		else if(npcId == LEON)
		{
			if(cond == 1)
				htmltext = "31256-1.htm";
			else if(cond == 2)
				htmltext = "31256-2r.htm";
		}
		else if(npcId == WAHKAN)
			if(cond == 2 && st.getQuestItemsCount(MUNITIONS_BOX) > 0)
				htmltext = "31371-1.htm";
		return htmltext;
	}
}
