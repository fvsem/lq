package quests._405_PathToCleric;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _405_PathToCleric extends Quest implements ScriptFile
{
	// npc
	private static final int GALLINT = 30017;
	private static final int ZIGAUNT = 30022;
	private static final int VIVYAN = 30030;
	private static final int SIMPLON = 30253;
	private static final int PRAGA = 30333;
	private static final int LIONEL = 30408;
	// mobs
	private static final int RUIN_ZOMBIE = 20026;
	private static final int RUIN_ZOMBIE_LEADER = 20029;
	// items
	private static final int LETTER_OF_ORDER1 = 1191;
	private static final int LETTER_OF_ORDER2 = 1192;
	private static final int BOOK_OF_LEMONIELL = 1193;
	private static final int BOOK_OF_VIVI = 1194;
	private static final int BOOK_OF_SIMLON = 1195;
	private static final int BOOK_OF_PRAGA = 1196;
	private static final int CERTIFICATE_OF_GALLINT = 1197;
	private static final int PENDANT_OF_MOTHER = 1198;
	private static final int NECKLACE_OF_MOTHER = 1199;
	private static final int LEMONIELLS_COVENANT = 1200;
	private static final int MARK_OF_FAITH = 1201;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _405_PathToCleric()
	{
		super(405, -1);

		addStartNpc(ZIGAUNT);

		addTalkId(GALLINT);
		addTalkId(ZIGAUNT);
		addTalkId(VIVYAN);
		addTalkId(SIMPLON);
		addTalkId(PRAGA);
		addTalkId(LIONEL);

		addKillId(RUIN_ZOMBIE);
		addKillId(RUIN_ZOMBIE_LEADER);

		addQuestItem(new int[] {
				LEMONIELLS_COVENANT,
				LETTER_OF_ORDER2,
				BOOK_OF_PRAGA,
				BOOK_OF_VIVI,
				BOOK_OF_SIMLON,
				LETTER_OF_ORDER1,
				NECKLACE_OF_MOTHER,
				PENDANT_OF_MOTHER,
				CERTIFICATE_OF_GALLINT,
				BOOK_OF_LEMONIELL });
	}

	public void checkBooks(QuestState st)
	{
		if(st.getQuestItemsCount(BOOK_OF_PRAGA) + st.getQuestItemsCount(BOOK_OF_VIVI) + st.getQuestItemsCount(BOOK_OF_SIMLON) >= 5)
			st.set("cond", "2");
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
			if(st.getPlayer().getLevel() >= 18 && st.getPlayer().getClassId().getId() == 0x0a && st.getQuestItemsCount(MARK_OF_FAITH) < 1)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
				st.giveItems(LETTER_OF_ORDER1, 1);
				htmltext = "30022-05.htm";
			}
			else if(st.getPlayer().getClassId().getId() != 0x0a)
			{
				if(st.getPlayer().getClassId().getId() == 0x0f)
					htmltext = "30022-02a.htm";
				else
					htmltext = "30022-02.htm";
			}
			else if(st.getPlayer().getLevel() < 18 && st.getPlayer().getClassId().getId() == 0x0a)
				htmltext = "30022-03.htm";
			else if(st.getPlayer().getLevel() >= 18 && st.getPlayer().getClassId().getId() == 0x0a && st.getQuestItemsCount(MARK_OF_FAITH) > 0)
				htmltext = "30022-04.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == ZIGAUNT)
		{
			if(st.getQuestItemsCount(MARK_OF_FAITH) > 0)
			{
				htmltext = "30022-04.htm";
				st.exitCurrentQuest(true);
			}
			if(cond < 1 && st.getQuestItemsCount(MARK_OF_FAITH) < 1)
				htmltext = "30022-01.htm";
			else if(cond == 1 | cond == 2 && st.getQuestItemsCount(LETTER_OF_ORDER1) > 0)
			{
				if(st.getQuestItemsCount(BOOK_OF_VIVI) > 0 && st.getQuestItemsCount(BOOK_OF_SIMLON) > 2 && st.getQuestItemsCount(BOOK_OF_PRAGA) > 0)
				{
					htmltext = "30022-08.htm";
					st.takeItems(BOOK_OF_PRAGA, -1);
					st.takeItems(BOOK_OF_VIVI, -1);
					st.takeItems(BOOK_OF_SIMLON, -1);
					st.takeItems(LETTER_OF_ORDER1, -1);
					st.giveItems(LETTER_OF_ORDER2, 1);
					st.set("cond", "3");
				}
				else
					htmltext = "30022-06.htm";
			}
			else if(cond < 6 && st.getQuestItemsCount(LETTER_OF_ORDER2) > 0)
				htmltext = "30022-07.htm";
			else if(cond == 6 && st.getQuestItemsCount(LETTER_OF_ORDER2) > 0 && st.getQuestItemsCount(LEMONIELLS_COVENANT) > 0)
			{
				htmltext = "30022-09.htm";
				st.takeItems(LEMONIELLS_COVENANT, -1);
				st.takeItems(LETTER_OF_ORDER2, -1);
				if(!st.getPlayer().getVarB("prof1") && st.getPlayer().getClassId().getLevel() == 1)
				{
					st.getPlayer().setVar("prof1", "1");
					st.addExpAndSp(228064, 16455, true);
                                        st.giveItems(57, 163800, true);
				}
				st.exitCurrentQuest(true);
				st.giveItems(MARK_OF_FAITH, 1);
				st.playSound(SOUND_FINISH);
			}
		}
		else if(npcId == SIMPLON && cond == 1 && st.getQuestItemsCount(LETTER_OF_ORDER1) > 0)
		{
			if(st.getQuestItemsCount(BOOK_OF_SIMLON) < 1)
			{
				htmltext = "30253-01.htm";
				st.giveItems(BOOK_OF_SIMLON, 3);
				checkBooks(st);
			}
			else if(st.getQuestItemsCount(BOOK_OF_SIMLON) > 2)
				htmltext = "30253-02.htm";
		}
		else if(npcId == VIVYAN && cond == 1 && st.getQuestItemsCount(LETTER_OF_ORDER1) > 0)
		{
			if(st.getQuestItemsCount(BOOK_OF_VIVI) < 1)
			{
				htmltext = "30030-01.htm";
				st.giveItems(BOOK_OF_VIVI, 1);
				checkBooks(st);
			}
			else if(st.getQuestItemsCount(BOOK_OF_VIVI) > 0)
				htmltext = "30030-02.htm";
		}
		else if(npcId == PRAGA && cond == 1 && st.getQuestItemsCount(LETTER_OF_ORDER1) > 0)
		{
			if(st.getQuestItemsCount(BOOK_OF_PRAGA) < 1 && st.getQuestItemsCount(NECKLACE_OF_MOTHER) < 1)
			{
				htmltext = "30333-01.htm";
				st.giveItems(NECKLACE_OF_MOTHER, 1);
			}
			else if(st.getQuestItemsCount(BOOK_OF_PRAGA) < 1 && st.getQuestItemsCount(NECKLACE_OF_MOTHER) > 0 && st.getQuestItemsCount(PENDANT_OF_MOTHER) < 1)
				htmltext = "30333-02.htm";
			else if(st.getQuestItemsCount(BOOK_OF_PRAGA) < 1 && st.getQuestItemsCount(NECKLACE_OF_MOTHER) > 0 && st.getQuestItemsCount(PENDANT_OF_MOTHER) > 0)
			{
				htmltext = "30333-03.htm";
				st.takeItems(NECKLACE_OF_MOTHER, -1);
				st.takeItems(PENDANT_OF_MOTHER, -1);
				st.giveItems(BOOK_OF_PRAGA, 1);
				checkBooks(st);
			}
			else if(st.getQuestItemsCount(BOOK_OF_PRAGA) > 0)
				htmltext = "30333-04.htm";
		}
		else if(npcId == LIONEL)
		{
			if(st.getQuestItemsCount(LETTER_OF_ORDER2) < 1)
				htmltext = "30408-02.htm";
			else if(cond == 3 && st.getQuestItemsCount(LETTER_OF_ORDER2) == 1 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) < 1 && st.getQuestItemsCount(LEMONIELLS_COVENANT) < 1 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) < 1)
			{
				htmltext = "30408-01.htm";
				st.giveItems(BOOK_OF_LEMONIELL, 1);
				st.set("cond", "4");
			}
			else if(cond == 4 && st.getQuestItemsCount(LETTER_OF_ORDER2) == 1 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) > 0 && st.getQuestItemsCount(LEMONIELLS_COVENANT) < 1 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) < 1)
				htmltext = "30408-03.htm";
			else if(st.getQuestItemsCount(LETTER_OF_ORDER2) == 1 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) < 1 && st.getQuestItemsCount(LEMONIELLS_COVENANT) < 1 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) > 0)
			{
				htmltext = "30408-04.htm";
				st.takeItems(CERTIFICATE_OF_GALLINT, -1);
				st.giveItems(LEMONIELLS_COVENANT, 1);
				st.set("cond", "6");
			}
			else if(st.getQuestItemsCount(LETTER_OF_ORDER2) == 1 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) < 1 && st.getQuestItemsCount(LEMONIELLS_COVENANT) > 0 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) < 1)
				htmltext = "30408-05.htm";
		}
		else if(npcId == GALLINT && st.getQuestItemsCount(LETTER_OF_ORDER2) > 0)
			if(cond == 4 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) > 0 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) < 1)
			{
				htmltext = "30017-01.htm";
				st.takeItems(BOOK_OF_LEMONIELL, -1);
				st.giveItems(CERTIFICATE_OF_GALLINT, 1);
				st.set("cond", "5");
			}
			else if(cond == 5 && st.getQuestItemsCount(BOOK_OF_LEMONIELL) < 1 && st.getQuestItemsCount(CERTIFICATE_OF_GALLINT) > 0)
				htmltext = "30017-02.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == RUIN_ZOMBIE | npcId == RUIN_ZOMBIE_LEADER)
			if(st.getInt("cond") == 1 && st.getQuestItemsCount(PENDANT_OF_MOTHER) < 1)
			{
				st.giveItems(PENDANT_OF_MOTHER, 1);
				st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
