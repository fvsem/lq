package quests._10277_MutatedKaneus_Dion;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Dion</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10277_MutatedKaneus_Dion extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10277_MutatedKaneus_Dion()
	{
		super(10277, "_10277_MutatedKaneus_Dion", "Mutated Kaneus - Dion");

		NPC = new int[] { 30071, 30461 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18558, 13832 }, // Crimson Hatu Otis
				{ 18559, 13833 } // Seer Flouros
		};
		startLevel = 28;
		registerNPCs();
	}
}
