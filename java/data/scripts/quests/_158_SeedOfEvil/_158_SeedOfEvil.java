package quests._158_SeedOfEvil;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _158_SeedOfEvil extends Quest implements ScriptFile
{
	private static int CLAY_TABLET_ID = 1025;
	private static int ENCHANT_ARMOR_D = 956;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _158_SeedOfEvil()
	{
		super(158, -1);

		addStartNpc(30031);

		addTalkId(30031);

		addKillId(27016);

		addQuestItem(CLAY_TABLET_ID);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			st.set("id", "0");
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			htmltext = "30031-04.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
			st.set("id", "0");
		}
		if(npcId == 30031 && st.getInt("cond") == 0)
		{
			if(st.getInt("cond") < 15)
			{
				if(st.getPlayer().getLevel() >= 21)
				{
					htmltext = "30031-03.htm";
					return htmltext;
				}
				htmltext = "30031-02.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30031-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == 30031 && st.getInt("cond") == 0)
			htmltext = "completed";
		else if(npcId == 30031 && st.getInt("cond") != 0 && st.getQuestItemsCount(CLAY_TABLET_ID) == 0)
			htmltext = "30031-05.htm";
		else if(npcId == 30031 && st.getInt("cond") != 0 && st.getQuestItemsCount(CLAY_TABLET_ID) != 0)
			if(st.getInt("id") != 158)
			{
				st.set("id", "158");
				st.takeItems(CLAY_TABLET_ID, st.getQuestItemsCount(CLAY_TABLET_ID));
				st.playSound(SOUND_FINISH);
				st.giveItems(ENCHANT_ARMOR_D, 1);
				htmltext = "30031-06.htm";
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == 27016)
		{
			st.set("id", "0");
			if(st.getInt("cond") != 0 && st.getQuestItemsCount(CLAY_TABLET_ID) == 0)
			{
				st.giveItems(CLAY_TABLET_ID, 1);
				st.playSound(SOUND_MIDDLE);
			}
		}
		return null;
	}
}
