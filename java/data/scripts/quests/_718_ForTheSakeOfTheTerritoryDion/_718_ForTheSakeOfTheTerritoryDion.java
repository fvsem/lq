package quests._718_ForTheSakeOfTheTerritoryDion;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _718_ForTheSakeOfTheTerritoryDion extends TerritoryWarSuperClass
{
	public _718_ForTheSakeOfTheTerritoryDion()
	{
		super(718);
		_catapultId = 36500;
		_territoryId = 2;
		_leaderIDs = new int[] { 36514, 36516, 36519, 36592 };
		_guardIDs = new int[] { 36515, 36517, 36518 };
		_text = new String[] { "The catapult of Dion has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
