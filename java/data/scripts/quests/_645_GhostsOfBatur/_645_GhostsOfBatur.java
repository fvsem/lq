package quests._645_GhostsOfBatur;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _645_GhostsOfBatur extends Quest implements ScriptFile
{
	// Npc
	private static final int Karuda = 32017;
	// Items
	private static final int CursedGraveGoods = 8089;
	private static final int CursedBurialItems = 14861;
	// Mobs
	private static final int[] MOBS = { 22007, 22009, 22010, 22011, 22012, 22013, 22014, 22015, 22016, 22703, 22704, 22705, 22706, 22707 };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _645_GhostsOfBatur()
	{
		super(645, 0);

		addStartNpc(Karuda);
		addKillId(MOBS);
		addQuestItem(CursedGraveGoods, CursedBurialItems);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("karuda_q0645_0103.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() < 23)
			{
				htmltext = "karuda_q0645_0102.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "karuda_q0645_0101.htm";
		}
		else
		{
			if(cond == 2)
				st.set("cond", "1");

			if(st.getQuestItemsCount(CursedGraveGoods, CursedBurialItems) == 0)
				htmltext = "karuda_q0645_0106.htm";
			else
				htmltext = Config.ALT_100_RECIPES ? "karuda_q0645_0105a.htm" : "karuda_q0645_0105.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") > 0)
			if(npc.getLevel() < 80)
				st.rollAndGive(CursedGraveGoods, Rnd.get(1, 2), 70);
			else
				st.rollAndGive(CursedBurialItems, 1, 5);
		return null;
	}
}
