package quests._018_MeetingwiththeGoldenRam;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _018_MeetingwiththeGoldenRam extends Quest implements ScriptFile
{
	private static final int SUPPLY_BOX = 7245;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _018_MeetingwiththeGoldenRam()
	{
		super(18, -1);

		addStartNpc(31314);

		addTalkId(31314);
		addTalkId(31315);
		addTalkId(31555);

		addQuestItem(SUPPLY_BOX);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31314-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31315-02.htm"))
		{
			st.set("cond", "2");
			st.giveItems(SUPPLY_BOX, 1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31555-02.htm"))
		{
			st.takeItems(SUPPLY_BOX, -1);
			st.giveItems(57, 68686);
			st.addExpAndSp(330194, 326328);
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 31314)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 66)
					htmltext = "31314-01.htm";
				else
				{
					htmltext = "31314-02.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "31314-04.htm";
		}
		else if(npcId == 31315)
		{
			if(cond == 1)
				htmltext = "31315-01.htm";
			else if(cond == 2)
				htmltext = "31315-03.htm";
		}
		else if(npcId == 31555)
			if(cond == 2 && st.getQuestItemsCount(SUPPLY_BOX) == 1)
				htmltext = "31555-01.htm";
		return htmltext;
	}
}
