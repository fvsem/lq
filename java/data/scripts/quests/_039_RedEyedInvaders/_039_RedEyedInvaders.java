package quests._039_RedEyedInvaders;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _039_RedEyedInvaders extends Quest implements ScriptFile
{
	private static int BBN = 7178;
	private static int RBN = 7179;
	private static int IP = 7180;
	private static int GML = 7181;
	private static int[] REW = { 6521, 6529, 6535 };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _039_RedEyedInvaders()
	{
		super(39, -1);

		addStartNpc(30334);

		addTalkId(30334);
		addTalkId(30334);
		addTalkId(30332);
		addTalkId(30334);

		addKillId(20919);
		addKillId(20920);
		addKillId(20921);
		addKillId(20925);

		addQuestItem(new int[] { BBN, IP, RBN, GML });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30334-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30332-02.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30332-04.htm"))
		{
			if(st.getQuestItemsCount(BBN) == 100 && st.getQuestItemsCount(RBN) == 100)
			{
				st.set("cond", "4");
				st.takeItems(BBN, -1);
				st.takeItems(RBN, -1);
				st.playSound(SOUND_ACCEPT);
			}
			else
				htmltext = "30332-02r.htm";
		}
		else if(event.equals("30332-06.htm"))
			if(st.getQuestItemsCount(IP) == 30 && st.getQuestItemsCount(GML) == 30)
			{
				st.takeItems(IP, -1);
				st.takeItems(GML, -1);
				st.giveItems(REW[0], 60);
				st.giveItems(REW[1], 1);
				st.giveItems(REW[2], 500);
				st.set("cond", "0");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else
				htmltext = "30332-04r.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == 30334)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 20)
				{
					htmltext = "30334-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 20)
					htmltext = "30334-01.htm";
			}
			else if(cond == 1)
				htmltext = "30334-02r.htm";
		}
		else if(npcId == 30332)
			if(cond == 1)
				htmltext = "30332-01.htm";
			else if(cond == 2 && (st.getQuestItemsCount(BBN) < 100 || st.getQuestItemsCount(RBN) < 100))
				htmltext = "30332-02r.htm";
			else if(cond == 3 && st.getQuestItemsCount(BBN) == 100 && st.getQuestItemsCount(RBN) == 100)
				htmltext = "30332-03.htm";
			else if(cond == 4 && (st.getQuestItemsCount(IP) < 30 || st.getQuestItemsCount(GML) < 30))
				htmltext = "30332-04r.htm";
			else if(cond == 5 && st.getQuestItemsCount(IP) == 30 && st.getQuestItemsCount(GML) == 30)
				htmltext = "30332-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond == 2 && Rnd.chance(60))
		{
			if((npcId == 20919 || npcId == 20920) && st.getQuestItemsCount(BBN) <= 99)
				st.giveItems(BBN, 1);
			else if(npcId == 20921 && st.getQuestItemsCount(RBN) <= 99)
				st.giveItems(RBN, 1);
			st.playSound(SOUND_ITEMGET);
			if(st.getQuestItemsCount(BBN) + st.getQuestItemsCount(RBN) == 200)
			{
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
			}
		}
		if(cond == 4 && Rnd.chance(60))
		{
			if(npcId == 20920 && st.getQuestItemsCount(IP) <= 29)
				st.giveItems(IP, 1);
			else if(npcId == 20925 && st.getQuestItemsCount(GML) <= 29)
				st.giveItems(GML, 1);
			st.playSound(SOUND_ITEMGET);
			if(st.getQuestItemsCount(IP) + st.getQuestItemsCount(GML) == 60)
			{
				st.set("cond", "5");
				st.playSound(SOUND_MIDDLE);
			}
		}
		return null;
	}
}
