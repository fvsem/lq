package quests._356_DigUpTheSeaOfSpores;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _356_DigUpTheSeaOfSpores extends Quest implements ScriptFile
{
	// NPC
	private static final int GAUEN = 30717;
	// MOBS
	private static final int SPORE_ZOMBIE = 20562;
	private static final int ROTTING_TREE = 20558;
	// QUEST ITEMS
	private static final int CARNIVORE_SPORE = 5865;
	private static final int HERBIBOROUS_SPORE = 5866;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _356_DigUpTheSeaOfSpores()
	{
		super(356, -1);
		addStartNpc(GAUEN);

		addKillId(SPORE_ZOMBIE);
		addKillId(ROTTING_TREE);

		addQuestItem(CARNIVORE_SPORE);
		addQuestItem(HERBIBOROUS_SPORE);

	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		long carn = st.getQuestItemsCount(CARNIVORE_SPORE);
		long herb = st.getQuestItemsCount(HERBIBOROUS_SPORE);
		if(event.equalsIgnoreCase("30717-5.htm"))
		{
			if(st.getPlayer().getLevel() >= 43)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
			else
			{
				htmltext = "30717-4.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if((event.equalsIgnoreCase("30717-10.htm") || event.equalsIgnoreCase("30717-9.htm")) && carn >= 50 && herb >= 50)
		{
			st.takeItems(CARNIVORE_SPORE, -1);
			st.takeItems(HERBIBOROUS_SPORE, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			if(event.equalsIgnoreCase("30717-9.htm"))
				st.giveItems(57, 44000);
			else
				st.addExpAndSp(36000, 2600);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
			htmltext = "30717-0.htm";
		else if(cond != 3)
			htmltext = "30717-6.htm";
		else if(cond == 3)
			htmltext = "30717-7.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		long carn = st.getQuestItemsCount(CARNIVORE_SPORE);
		long herb = st.getQuestItemsCount(HERBIBOROUS_SPORE);
		if(npcId == SPORE_ZOMBIE)
		{
			if(carn < 50)
			{
				st.giveItems(CARNIVORE_SPORE, 1);
				if(carn == 49)
				{
					st.playSound(SOUND_MIDDLE);
					if(herb >= 50)
					{
						st.set("cond", "3");
						st.setState(STARTED);
					}
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == ROTTING_TREE)
			if(herb < 50)
			{
				st.giveItems(HERBIBOROUS_SPORE, 1);
				if(herb == 49)
				{
					st.playSound(SOUND_MIDDLE);
					if(carn >= 50)
					{
						st.set("cond", "3");
						st.setState(STARTED);
					}
				}
				else
					st.playSound(SOUND_ITEMGET);
			}

		return null;
	}
}
