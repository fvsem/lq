package quests._114_ResurrectionOfAnOldManager;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.ai.CtrlEvent;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _114_ResurrectionOfAnOldManager extends Quest implements ScriptFile
{
	// NPC
	private static final int NEWYEAR = 31961;
	private static final int YUMI = 32041;
	private static final int STONES = 32046;
	private static final int WENDY = 32047;
	private static final int BOX = 32050;

	// MOBS
	private static final int GUARDIAN = 27318;

	// QUEST ITEMS
	private static final int DETECTOR = 8090;
	private static final int DETECTOR2 = 8091;
	private static final int STARSTONE = 8287;
	private static final int LETTER = 8288;
	private static final int STARSTONE2 = 8289;

	private L2NpcInstance GUARDIAN_SPAWN = null;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _114_ResurrectionOfAnOldManager()
	{
		super(114, -1);

		addStartNpc(YUMI);

		addTalkId(WENDY);
		addTalkId(BOX);
		addTalkId(STONES);
		addTalkId(NEWYEAR);

		addKillId(GUARDIAN);

		addQuestItem(DETECTOR);
		addQuestItem(DETECTOR2);
		addQuestItem(STARSTONE);
		addQuestItem(LETTER);
		addQuestItem(STARSTONE2);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int choice;

		if(event.equalsIgnoreCase("31961-02.htm"))
		{
			st.set("cond", "22");
			st.takeItems(LETTER, 1);
			st.giveItems(STARSTONE2, 1);
			st.playSound(SOUND_MIDDLE);
		}
		if(event.equalsIgnoreCase("32041-02.htm"))
		{
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.set("cond", "1");
			st.set("talk", "0");
		}
		else if(event.equalsIgnoreCase("32041-06.htm"))
			st.set("talk", "1");
		else if(event.equalsIgnoreCase("32041-07.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
		}
		else if(event.equalsIgnoreCase("32041-10.htm"))
		{
			choice = st.getInt("choice");
			if(choice == 1)
				htmltext = "32041-10.htm";
			else if(choice == 2)
				htmltext = "32041-10a.htm";
			else if(choice == 3)
				htmltext = "32041-10b.htm";
		}
		else if(event.equalsIgnoreCase("32041-11.htm"))
			st.set("talk", "1");
		else if(event.equalsIgnoreCase("32041-18.htm"))
			st.set("talk", "2");
		else if(event.equalsIgnoreCase("32041-20.htm"))
		{
			st.set("cond", "6");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
		}
		else if(event.equalsIgnoreCase("32041-25.htm"))
		{
			st.set("cond", "17");
			st.playSound(SOUND_MIDDLE);
			st.giveItems(DETECTOR, 1);
		}
		else if(event.equalsIgnoreCase("32041-28.htm"))
		{
			st.takeItems(DETECTOR2, 1);
			st.set("talk", "1");
		}
		else if(event.equalsIgnoreCase("32041-31.htm"))
		{
			choice = st.getInt("choice");
			if(choice > 1)
				htmltext = "32041-37.htm";
		}
		else if(event.equalsIgnoreCase("32041-32.htm"))
		{
			st.set("cond", "21");
			st.giveItems(LETTER, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32041-36.htm"))
		{
			st.set("cond", "20");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32046-02.htm"))
		{
			st.set("cond", "19");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32046-06.htm"))
		{
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		else if(event.equalsIgnoreCase("32047-01.htm"))
		{
			if(st.getInt("talk") + st.getInt("talk1") == 2)
				htmltext = "32047-04.htm";
			else if(st.getInt("talk") + st.getInt("talk1") + st.getInt("talk2") == 6)
				htmltext = "32047-08.htm";
		}
		else if(event.equalsIgnoreCase("32047-02.htm"))
		{
			if(st.getInt("talk") == 0)
				st.set("talk", "1");
		}
		else if(event.equalsIgnoreCase("32047-03.htm"))
		{
			if(st.getInt("talk1") == 0)
				st.set("talk1", "1");
		}
		else if(event.equalsIgnoreCase("32047-05.htm"))
		{
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
			st.set("choice", "1");
			st.unset("talk1");
		}
		else if(event.equalsIgnoreCase("32047-06.htm"))
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
			st.set("choice", "2");
			st.unset("talk1");
		}
		else if(event.equalsIgnoreCase("32047-07.htm"))
		{
			st.set("cond", "5");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
			st.set("choice", "3");
			st.unset("talk1");
		}
		else if(event.equalsIgnoreCase("32047-13.htm"))
		{
			st.set("cond", "7");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32047-13a.htm"))
		{
			st.set("cond", "10");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32047-15.htm"))
		{
			if(st.getInt("talk") == 0)
				st.set("talk", "1");
		}
		else if(event.equalsIgnoreCase("32047-15a.htm"))
		{
			if(GUARDIAN_SPAWN == null || !st.getPlayer().knowsObject(GUARDIAN_SPAWN) || !GUARDIAN_SPAWN.isVisible())
			{
				GUARDIAN_SPAWN = st.addSpawn(GUARDIAN, 96977, -110625, -3280, 900000);
				Functions.npcSay(GUARDIAN_SPAWN, "You, " + st.getPlayer().getName() + ", you attacked Wendy. Prepare to die!");
				GUARDIAN_SPAWN.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, st.getPlayer(), 999);
			}
			else
				htmltext = "32047-19a.htm";
		}
		else if(event.equalsIgnoreCase("32047-17a.htm"))
		{
			st.set("cond", "12");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32047-20.htm"))
			st.set("talk", "2");
		else if(event.equalsIgnoreCase("32047-23.htm"))
		{
			st.set("cond", "13");
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
		}
		else if(event.equalsIgnoreCase("32047-25.htm"))
		{
			st.set("cond", "15");
			st.playSound(SOUND_MIDDLE);
			st.takeItems(STARSTONE, 1);
		}
		else if(event.equalsIgnoreCase("32047-30.htm"))
			st.set("talk", "2");
		else if(event.equalsIgnoreCase("32047-33.htm"))
		{
			if(st.getInt("cond") == 7)
			{
				st.set("cond", "8");
				st.set("talk", "0");
				st.playSound(SOUND_MIDDLE);
			}
			else if(st.getInt("cond") == 8)
			{
				st.set("cond", "9");
				st.playSound(SOUND_MIDDLE);
				htmltext = "32047-34.htm";
			}
		}
		else if(event.equalsIgnoreCase("32047-34.htm"))
		{
			st.set("cond", "9");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32047-38.htm"))
		{
			st.giveItems(STARSTONE2, 1);
			st.takeItems(57, 3000);
			st.set("cond", "26");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32050-02.htm"))
		{
			st.playSound(SOUND_ARMOR_WOOD_3);
			st.set("talk", "1");
		}
		else if(event.equalsIgnoreCase("32050-04.htm"))
		{
			st.set("cond", "14");
			st.giveItems(STARSTONE, 1);
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		if(id == COMPLETED)
			return "completed";
		int cond = st.getInt("cond");
		// onFirstTalk.... wtf???
		if(npcId == STONES && cond == 17)
		{
			st.playSound(SOUND_MIDDLE);
			st.takeItems(DETECTOR, 1);
			st.giveItems(DETECTOR2, 1);
			st.set("cond", "18");
			// Don't know how it's in L2Re Development Team - JAVA
			// st.getPlayer().sendPacket(ExShowScreenMessage("The radio signal detector is responding. A suspicious pile of stones catches your eye.",4500));
		}
		// onTalk

		int talk = st.getInt("talk");
		int talk1 = st.getInt("talk1");

		if(npcId == YUMI)
		{
			if(id == CREATED)
			{
				QuestState Pavel = st.getPlayer().getQuestState("121_PavelTheGiants");
				if(Pavel == null)
					return "32041-00.htm";
				if(st.getPlayer().getLevel() >= 49 && Pavel.getState() == COMPLETED)
					htmltext = "32041-01.htm";
				else
				{
					htmltext = "32041-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
			{
				if(talk == 0)
					htmltext = "32041-02.htm";
				else
					htmltext = "32041-06.htm";
			}
			else if(cond == 2)
				htmltext = "32041-08.htm";
			else if(cond == 3 || cond == 4 || cond == 5)
			{
				if(talk == 0)
					htmltext = "32041-09.htm";
				else if(talk == 1)
					htmltext = "32041-11.htm";
				else
					htmltext = "32041-18.htm";
			}
			else if(cond == 6)
				htmltext = "32041-21.htm";
			else if(cond == 9 || cond == 12 || cond == 16)
				htmltext = "32041-22.htm";
			else if(cond == 17)
				htmltext = "32041-26.htm";
			else if(cond == 19)
			{
				if(talk == 0)
					htmltext = "32041-27.htm";
				else
					htmltext = "32041-28.htm";
			}
			else if(cond == 20)
				htmltext = "32041-36.htm";
			else if(cond == 21)
				htmltext = "32041-33.htm";
			else if(cond == 22 || cond == 26)
			{
				htmltext = "32041-34.htm";
				st.set("cond", "27");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 27)
				htmltext = "32041-35.htm";
		}
		else if(npcId == WENDY)
		{
			if(cond == 2)
			{
				if(talk + talk1 < 2)
					htmltext = "32047-01.htm";
				else if(talk + talk1 == 2)
					htmltext = "32047-04.htm";
			}
			else if(cond == 3)
				htmltext = "32047-09.htm";
			else if(cond == 4 || cond == 5)
				htmltext = "32047-09a.htm";
			else if(cond == 6)
			{
				int choice = st.getInt("choice");
				if(choice == 1)
				{
					if(talk == 0)
						htmltext = "32047-10.htm";
					else if(talk == 1)
						htmltext = "32047-20.htm";
					else
						htmltext = "32047-30.htm";
				}
				else if(choice == 2)
					htmltext = "32047-10a.htm";
				else if(choice == 3)
					if(talk == 0)
						htmltext = "32047-14.htm";
					else if(talk == 1)
						htmltext = "32047-15.htm";
					else
						htmltext = "32047-20.htm";
			}
			else if(cond == 7)
			{
				if(talk == 0)
					htmltext = "32047-14.htm";
				else if(talk == 1)
					htmltext = "32047-15.htm";
				else
					htmltext = "32047-20.htm";
			}
			else if(cond == 8)
				htmltext = "32047-30.htm";
			else if(cond == 9)
				htmltext = "32047-27.htm";
			else if(cond == 10)
				htmltext = "32047-14a.htm";
			else if(cond == 11)
				htmltext = "32047-16a.htm";
			else if(cond == 12)
				htmltext = "32047-18a.htm";
			else if(cond == 13)
				htmltext = "32047-23.htm";
			else if(cond == 14)
				htmltext = "32047-24.htm";
			else if(cond == 15)
			{
				htmltext = "32047-26.htm";
				st.set("cond", "16");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 16)
				htmltext = "32047-27.htm";
			else if(cond == 20)
				htmltext = "32047-35.htm";
			else if(cond == 26)
				htmltext = "32047-40.htm";
		}
		else if(npcId == BOX)
		{
			if(cond == 13)
			{
				if(talk == 0)
					htmltext = "32050-01.htm";
				else
					htmltext = "32050-03.htm";
			}
			else if(cond == 14)
				htmltext = "32050-05.htm";
		}
		else if(npcId == STONES)
		{
			if(cond == 18)
				htmltext = "32046-01.htm";
			else if(cond == 19)
				htmltext = "32046-02.htm";
			else if(cond == 27)
				htmltext = "32046-03.htm";
		}
		else if(npcId == NEWYEAR)
			if(cond == 21)
				htmltext = "31961-01.htm";
			else if(cond == 22)
				htmltext = "31961-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
			return null;

		int npcId = npc.getNpcId();
		if(st.getInt("cond") == 10)
			if(npcId == GUARDIAN)
			{
				Functions.npcSay(npc, "This enemy is far too powerful for me to fight. I must withdraw");
				st.set("cond", "11");
				st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
