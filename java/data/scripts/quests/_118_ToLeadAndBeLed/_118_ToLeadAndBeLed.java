package quests._118_ToLeadAndBeLed;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _118_ToLeadAndBeLed extends Quest implements ScriptFile
{
	private static int PINTER = 30298;
	private static int MAILLE_LIZARDMAN = 20919;
	private static int BLOOD_OF_MAILLE_LIZARDMAN = 8062;
	private static int KING_OF_THE_ARANEID = 20927;
	private static int KING_OF_THE_ARANEID_LEG = 8063;
	private static int D_CRY = 1458;
	private static int D_CRY_COUNT_HEAVY = 922;
	private static int D_CRY_COUNT_LIGHT_MAGIC = 771;

	private static int CLAN_OATH_HELM = 7850;

	private static int CLAN_OATH_ARMOR = 7851;
	private static int CLAN_OATH_GAUNTLETS = 7852;
	private static int CLAN_OATH_SABATON = 7853;

	private static int CLAN_OATH_BRIGANDINE = 7854;
	private static int CLAN_OATH_LEATHER_GLOVES = 7855;
	private static int CLAN_OATH_BOOTS = 7856;

	private static int CLAN_OATH_AKETON = 7857;
	private static int CLAN_OATH_PADDED_GLOVES = 7858;
	private static int CLAN_OATH_SANDALS = 7859;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _118_ToLeadAndBeLed()
	{
		super(118, -1);

		addStartNpc(PINTER);
		addTalkId(PINTER);
		addTalkId(PINTER);
		addTalkId(PINTER);

		addKillId(MAILLE_LIZARDMAN);
		addKillId(KING_OF_THE_ARANEID);

		addQuestItem(new int[] { BLOOD_OF_MAILLE_LIZARDMAN, KING_OF_THE_ARANEID_LEG });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30298-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30298-05.htm"))
		{
			st.set("choose", "1");
			st.set("cond", "3");
		}
		else if(event.equals("30298-06.htm"))
		{
			st.set("choose", "2");
			st.set("cond", "4");
		}
		else if(event.equals("30298-07.htm"))
		{
			st.set("choose", "3");
			st.set("cond", "5");
		}
		else if(event.equals("30298-08.htm"))
		{
			int choose = st.getInt("choose");
			int D_CRY_COUNT = D_CRY_COUNT_LIGHT_MAGIC;
			if(choose == 1)
				D_CRY_COUNT = D_CRY_COUNT_HEAVY;
			if(st.getQuestItemsCount(D_CRY) >= D_CRY_COUNT)
			{
				st.set("cond", "7");
				st.takeItems(D_CRY, D_CRY_COUNT);
			}
			else
				htmltext = "<html><body>771 D Cry!</body></html>";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() < 19)
			{
				htmltext = "<html><body>Your level is too low</body></html>";
				return htmltext;
			}
			else if(st.getPlayer().getClanId() == 0)
			{
				htmltext = "<html><body>You are not in clan</body></html>";
				return htmltext;
			}
			else if(st.getPlayer().getSponsor() == 0)
			{
				htmltext = "<html><body>You have no sponsor</body></html>";
				return htmltext;
			}
			else
				htmltext = "30298-00.htm";
		}
		else if(cond == 1)
			htmltext = "<html><body>Bring me 10 Bruin Lizardmen blood.</body></html>";
		else if(cond == 2)
		{
			st.takeItems(BLOOD_OF_MAILLE_LIZARDMAN, 10);
			htmltext = "30298-04.htm";
		}
		else if(cond == 3)
			htmltext = "30298-05.htm";
		else if(cond == 4)
			htmltext = "30298-06.htm";
		else if(cond == 5)
			htmltext = "30298-07.htm";
		else if(cond == 7)
			htmltext = "<html><body>Bring me 8 Picot Legs.</body></html>";
		else if(cond == 8)
		{
			st.takeItems(KING_OF_THE_ARANEID_LEG, 8);
			int choose = st.getInt("choose");
			st.giveItems(CLAN_OATH_HELM, 1);
			if(choose == 1)
			{
				st.giveItems(CLAN_OATH_ARMOR, 1);
				st.giveItems(CLAN_OATH_GAUNTLETS, 1);
				st.giveItems(CLAN_OATH_SABATON, 1);
			}
			else if(choose == 2)
			{
				st.giveItems(CLAN_OATH_BRIGANDINE, 1);
				st.giveItems(CLAN_OATH_LEATHER_GLOVES, 1);
				st.giveItems(CLAN_OATH_BOOTS, 1);
			}
			else if(choose == 3)
			{
				st.giveItems(CLAN_OATH_AKETON, 1);
				st.giveItems(CLAN_OATH_PADDED_GLOVES, 1);
				st.giveItems(CLAN_OATH_SANDALS, 1);
			}
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			htmltext = "<html><body>OK!</body></html>";
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == MAILLE_LIZARDMAN && st.getQuestItemsCount(BLOOD_OF_MAILLE_LIZARDMAN) < 10 && cond == 1 && Rnd.chance(50))
		{
			st.giveItems(BLOOD_OF_MAILLE_LIZARDMAN, 1);
			if(st.getQuestItemsCount(BLOOD_OF_MAILLE_LIZARDMAN) == 10)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
		}
		else if(npcId == KING_OF_THE_ARANEID && st.getQuestItemsCount(KING_OF_THE_ARANEID_LEG) < 8 && cond == 7 && Rnd.chance(50))
		{
			st.giveItems(KING_OF_THE_ARANEID_LEG, 1);
			if(st.getQuestItemsCount(KING_OF_THE_ARANEID_LEG) == 8)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "8");
			}
		}
		return null;
	}
}
