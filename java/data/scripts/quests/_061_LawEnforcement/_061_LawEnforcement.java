package quests._061_LawEnforcement;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * 3-rd class transfer from Inspector to Judicator
 */
public class _061_LawEnforcement extends Quest implements ScriptFile
{
	private static int Liane = 32222;
	private static int Kekropus = 32138;
	private static int Eindburgh = 32469;
	private static int Adena = 57;
	private static short Judicator = 136;

	public _061_LawEnforcement()
	{
		super(61, -1);
		addStartNpc(Liane);
		addTalkId(Kekropus);
		addTalkId(Eindburgh);
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();

		if(event.equalsIgnoreCase("32222-03.htm") && _state == CREATED)
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		if(event.equalsIgnoreCase("32138-08.htm") && cond == 1)
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		if(event.equalsIgnoreCase("32469-07.htm") && cond == 2)
			if(st.getPlayer().getClassId() == ClassId.inspector)
			{
				st.giveItems(Adena, 26000);
				player.setClassId(Judicator, true);
				player.broadcastUserInfo(true);
				st.exitCurrentQuest(true);
				return "32469-07.htm";
			}
			else
				return "You are not an Inspector!!!";
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();

		if(_state == COMPLETED)
			return "completed";

		if(_state == CREATED)
		{
			if(npcId != Liane)
				return "No quest";
			if(st.getPlayer().getLevel() < 76 || st.getPlayer().getClassId() != ClassId.inspector)
			{
				st.exitCurrentQuest(true);
				return "32222-00.htm";
			}
			st.set("cond", "0");
			return "32222-01.htm";
		}

		int cond = st.getInt("cond");

		if(npcId == Kekropus && cond == 1)
			return "32138-00.htm";
		if(npcId == Eindburgh && cond == 2)
			return "32469-00.htm";
		return "noquest";
	}
}
