package quests._648_AnIceMerchantsDream;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _648_AnIceMerchantsDream extends Quest implements ScriptFile
{
	// NPCs
	private static int Rafforty = 32020;
	private static int Ice_Shelf = 32023;
	// Items
	private static int Silver_Hemocyte = 8057;
	private static int Silver_Ice_Crystal = 8077;
	private static int Black_Ice_Crystal = 8078;
	// Chances
	private static int Silver_Hemocyte_Chance = 10;
	private static int Silver2Black_Chance = 30;

	private static GArray<Integer> silver2black = new GArray<Integer>();

	public _648_AnIceMerchantsDream()
	{
		super(648, PARTY_ONE);
		addStartNpc(Rafforty);
		addStartNpc(Ice_Shelf);
		for(int i = 22080; i <= 22098; i++)
			if(i != 22095)
				addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("32020-02.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32020-09.htm") && _state == STARTED)
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		if(_state != STARTED)
			return event;

		long Silver_Ice_Crystal_Count = st.getQuestItemsCount(Silver_Ice_Crystal);
		long Black_Ice_Crystal_Count = st.getQuestItemsCount(Black_Ice_Crystal);

		if(event.equalsIgnoreCase("32020-07.htm"))
		{
			long reward = Silver_Ice_Crystal_Count * 300 + Black_Ice_Crystal_Count * 1200;
			if(reward > 0)
			{
				st.takeItems(Silver_Ice_Crystal, -1);
				st.takeItems(Black_Ice_Crystal, -1);
				st.giveItems(ADENA_ID, reward);
			}
			else
				return "32020-07a.htm";
		}
		else if(event.equalsIgnoreCase("32023-04.htm"))
		{
			int char_obj_id = st.getPlayer().getObjectId();
			synchronized (silver2black)
			{
				if(silver2black.contains(char_obj_id))
					return event;
				else if(Silver_Ice_Crystal_Count > 0)
					silver2black.add(char_obj_id);
				else
					return "cheat.htm";
			}

			st.takeItems(Silver_Ice_Crystal, 1);
			st.playSound(SOUND_BROKEN_KEY);
		}
		else if(event.equalsIgnoreCase("32023-05.htm"))
		{
			Integer char_obj_id = st.getPlayer().getObjectId();
			synchronized (silver2black)
			{
				if(silver2black.contains(char_obj_id))
					while (silver2black.contains(char_obj_id))
						silver2black.remove(char_obj_id);
				else
					return "cheat.htm";
			}

			if(Rnd.chance(Silver2Black_Chance))
			{
				st.giveItems(Black_Ice_Crystal, 1);
				st.playSound(SOUND_ENCHANT_SUCESS);
			}
			else
			{
				st.playSound(SOUND_ENCHANT_FAILED);
				return "32023-06.htm";
			}
		}

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");

		if(_state == CREATED)
		{
			if(npcId == Rafforty)
			{
				if(st.getPlayer().getLevel() >= 53)
				{
					st.set("cond", "0");
					return "32020-01.htm";
				}
				st.exitCurrentQuest(true);
				return "32020-00.htm";
			}
			if(npcId == Ice_Shelf)
				return "32023-00.htm";
		}

		if(_state != STARTED)
			return "noquest";

		long Silver_Ice_Crystal_Count = st.getQuestItemsCount(Silver_Ice_Crystal);
		if(npcId == Ice_Shelf)
			return Silver_Ice_Crystal_Count > 0 ? "32023-02.htm" : "32023-01.htm";

		long Black_Ice_Crystal_Count = st.getQuestItemsCount(Black_Ice_Crystal);
		if(npcId == Rafforty)
		{
			if(cond == 1)
			{
				if(Silver_Ice_Crystal_Count > 0 || Black_Ice_Crystal_Count > 0)
				{
					QuestState st_115 = st.getPlayer().getQuestState("_115_TheOtherSideOfTruth");
					if(st_115 != null && st_115.getState() == COMPLETED)
					{
						st.set("cond", "2");
						st.playSound(SOUND_MIDDLE);
						return "32020-10.htm";
					}
					return "32020-05.htm";
				}
				return "32020-04.htm";
			}
			if(cond == 2)
				return Silver_Ice_Crystal_Count > 0 || Black_Ice_Crystal_Count > 0 ? "32020-10.htm" : "32020-04a.htm";
		}

		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		int cond = qs.getInt("cond");
		if(cond > 0)
		{
			qs.rollAndGive(Silver_Ice_Crystal, 1, npc.getNpcId() - 22050);
			if(cond == 2)
				qs.rollAndGive(Silver_Hemocyte, 1, Silver_Hemocyte_Chance);
		}
		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
