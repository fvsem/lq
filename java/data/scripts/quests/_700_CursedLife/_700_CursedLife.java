package quests._700_CursedLife;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _700_CursedLife extends Quest implements ScriptFile
{
	private static final int ORBYU = 32560;
	private static final int[] MOBS = { 22602, 22603, 22604, 22605 };
	private static final int ROK = 25624;

	private static final int SWALLOWED_SKULL = 13872;
	private static final int SWALLOWED_STERNUM = 13873;
	private static final int SWALLOWED_BONES = 13874;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _700_CursedLife()
	{
		super(700, PARTY_NONE);
		addStartNpc(ORBYU);
		addKillId(MOBS);
		addKillId(ROK);
		addQuestItem(SWALLOWED_SKULL, SWALLOWED_STERNUM, SWALLOWED_BONES);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32560-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32560-quit.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();

		if(npcId == ORBYU)
		{
			if(st.getPlayer().getLevel() < 74 || !player.isQuestCompleted("_10273_GoodDayToFly"))
			{
				htmltext = "32560-quit.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "32560-01.htm";
			if(cond == 1)
			{
				long Skull = st.getQuestItemsCount(SWALLOWED_SKULL);
				long Sternum = st.getQuestItemsCount(SWALLOWED_STERNUM);
				long Bones = st.getQuestItemsCount(SWALLOWED_BONES);
				if(Skull + Sternum + Bones > 0)
				{
					st.giveItems(ADENA_ID, 50 * Skull + 100 * Sternum + 150 * Bones);
					st.takeItems(SWALLOWED_SKULL, -1);
					st.takeItems(SWALLOWED_STERNUM, -1);
					st.takeItems(SWALLOWED_BONES, -1);
					htmltext = "32560-04.htm";
				}
				else
					htmltext = "32560-03.htm";
			}
			else if(cond == 0)
				htmltext = "32560-00.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
			return null;
		int npcId = npc.getNpcId();
		if(npcId == ROK)
		{
			if(st.getQuestItemsCount(SWALLOWED_STERNUM) == 0)
			{
				st.rollAndGive(SWALLOWED_STERNUM, 1, 80);
				st.playSound(SOUND_ITEMGET);
			}
			else if(st.getQuestItemsCount(SWALLOWED_SKULL) == 0)
			{
				st.rollAndGive(SWALLOWED_SKULL, 1, 80);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(ArrayUtil.arrayContains(MOBS, npcId))
		{
			st.rollAndGive(SWALLOWED_BONES, 1, 80);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
