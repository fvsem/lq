package quests._104_SpiritOfMirror;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;

public class _104_SpiritOfMirror extends Quest implements ScriptFile
{
	private static int GALLINS_OAK_WAND = 748;
	private static int WAND_SPIRITBOUND1 = 1135;
	private static int WAND_SPIRITBOUND2 = 1136;
	private static int WAND_SPIRITBOUND3 = 1137;
	private static int WAND_OF_ADEPT = 747;

	private static int[] items = new int[] { 4412, 4413, 4414, 4415, 4416, 5789, 5790, 5791, 1060, 2509 };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _104_SpiritOfMirror()
	{
		super(104, PARTY_NONE);

		addStartNpc(30017);

		addTalkId(30017);

		addTalkId(30017);
		addTalkId(30041);
		addTalkId(30043);
		addTalkId(30045);

		addKillId(27003);
		addKillId(27004);
		addKillId(27005);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30017-03.htm"))
		{
			st.set("cond", "1");
			st.set("id1", "0");
			st.set("id2", "0");
			st.set("id3", "0");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(GALLINS_OAK_WAND, 3);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 30017)
		{
			if(cond == 0)
				if(st.getPlayer().getRace().ordinal() != 0)
				{
					htmltext = "30017-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 10)
				{
					htmltext = "30017-02.htm";
					return htmltext;
				}
				else
				{
					htmltext = "30017-06.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1 && st.getQuestItemsCount(GALLINS_OAK_WAND) >= 1 && (st.getQuestItemsCount(WAND_SPIRITBOUND1) == 0 || st.getQuestItemsCount(WAND_SPIRITBOUND2) == 0 || st.getQuestItemsCount(WAND_SPIRITBOUND3) == 0))
				htmltext = "30017-04.htm";
			else if(cond == 3 && st.getQuestItemsCount(WAND_SPIRITBOUND1) == 1 && st.getQuestItemsCount(WAND_SPIRITBOUND2) == 1 && st.getQuestItemsCount(WAND_SPIRITBOUND3) == 1)
			{
				st.giveItems(WAND_OF_ADEPT, 1);
				st.takeItems(WAND_SPIRITBOUND1, 1);
				st.takeItems(WAND_SPIRITBOUND2, 1);
				st.takeItems(WAND_SPIRITBOUND3, 1);
				st.giveItems(1060, 100);
				for(int item = 4412; item <= 4416; item++)
					st.giveItems(item, 10);
				if(st.getPlayer().getClassId().isMage())
				{
					st.giveItems(5790, 3000);
					st.giveItems(2509, 500);
				}
				else
				{
					st.giveItems(5789, 6000);
					st.giveItems(1835, 1000);
				}
				htmltext = "30017-05.htm";
				st.unset("cond");
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		}
		else if((npcId == 30041 || npcId == 30043 || npcId == 30045) && cond == 1)
		{
			if(npcId == 30041 && st.getInt("id1") == 0)
				st.set("id1", "1");
			if(npcId == 30043 && st.getInt("id2") == 0)
				st.set("id2", "1");
			if(npcId == 30045 && st.getInt("id3") == 0)
				st.set("id3", "1");
			htmltext = npcId + "-01.htm";
			if(st.getInt("id1") + st.getInt("id2") + st.getInt("id3") == 3)
			{
				st.set("cond", "2");
				st.unset("id1");
				st.unset("id2");
				st.unset("id3");
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();

		if((cond == 1 || cond == 2) && st.getPlayer().getActiveWeaponInstance() != null && st.getPlayer().getActiveWeaponInstance().getItemId() == GALLINS_OAK_WAND)
		{
			L2ItemInstance weapon = st.getPlayer().getActiveWeaponInstance();
			if(npcId == 27003 && st.getQuestItemsCount(WAND_SPIRITBOUND1) == 0)
			{
				st.getPlayer().getInventory().destroyItem(weapon, 1, false);
				st.getPlayer().sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(GALLINS_OAK_WAND));
				st.giveItems(WAND_SPIRITBOUND1, 1);
				st.getPlayer().sendPacket(new ItemList(st.getPlayer(), false));
				long Collect = st.getQuestItemsCount(WAND_SPIRITBOUND1) + st.getQuestItemsCount(WAND_SPIRITBOUND2) + st.getQuestItemsCount(WAND_SPIRITBOUND3);
				if(Collect == 3)
				{
					st.set("cond", "3");
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
			else if(npcId == 27004 && st.getQuestItemsCount(WAND_SPIRITBOUND2) == 0)
			{
				st.getPlayer().getInventory().destroyItem(weapon, 1, false);
				st.getPlayer().sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(GALLINS_OAK_WAND));
				st.giveItems(WAND_SPIRITBOUND2, 1);
				st.getPlayer().sendPacket(new ItemList(st.getPlayer(), false));
				long Collect = st.getQuestItemsCount(WAND_SPIRITBOUND1) + st.getQuestItemsCount(WAND_SPIRITBOUND2) + st.getQuestItemsCount(WAND_SPIRITBOUND3);
				if(Collect == 3)
				{
					st.set("cond", "3");
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
			else if(npcId == 27005 && st.getQuestItemsCount(WAND_SPIRITBOUND3) == 0)
			{
				st.getPlayer().getInventory().destroyItem(weapon, 1, false);
				st.getPlayer().sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(GALLINS_OAK_WAND));
				st.giveItems(WAND_SPIRITBOUND3, 1);
				st.getPlayer().sendPacket(new ItemList(st.getPlayer(), false));
				long Collect = st.getQuestItemsCount(WAND_SPIRITBOUND1) + st.getQuestItemsCount(WAND_SPIRITBOUND2) + st.getQuestItemsCount(WAND_SPIRITBOUND3);
				if(Collect == 3)
				{
					st.set("cond", "3");
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		}
		return null;
	}
}
