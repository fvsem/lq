package quests._110_ToThePrimevalIsle;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * @author Felixx
 */
public class _110_ToThePrimevalIsle extends Quest implements ScriptFile
{
	// NPC
	private static int ANTON = 31338;
	private static int MARQUEZ = 32113;

	// QUEST ITEM and REWARD
	private static int ANCIENT_BOOK = 8777;
	private static int ADENA_ID = 57;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _110_ToThePrimevalIsle()
	{
		super(110, -1);

		addStartNpc(ANTON);
		addTalkId(ANTON);

		addTalkId(MARQUEZ);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			htmltext = "1.htm";
			st.set("cond", "1");
			st.giveItems(ANCIENT_BOOK, 1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("2") && st.getQuestItemsCount(ANCIENT_BOOK) > 0)
		{
			htmltext = "3.htm";
			st.playSound(SOUND_FINISH);
			st.giveItems(ADENA_ID, 169380);
			st.takeItems(ANCIENT_BOOK, -1);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(id == CREATED)
			if(st.getPlayer().getLevel() >= 75)
				htmltext = "0.htm";
			else
			{
				st.exitCurrentQuest(true);
				htmltext = "<html><body>This quest can only be taken by characters that have a minimum level of 75. Return when you are more experienced.</body></html>";
			}
		else if(id == STARTED)
			if(npcId == MARQUEZ && cond == 1)
				if(st.getQuestItemsCount(ANCIENT_BOOK) == 0)
					htmltext = "1a.htm";
				else
					htmltext = "2.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		return null;
	}
}
