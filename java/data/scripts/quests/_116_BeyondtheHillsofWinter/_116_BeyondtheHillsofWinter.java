package quests._116_BeyondtheHillsofWinter;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _116_BeyondtheHillsofWinter extends Quest implements ScriptFile
{
	// NPC
	public final int FILAUR = 30535;
	public final int OBI = 32052;
	// Quest Item
	public final int Supplying_Goods_for_Railroad_Worker = 8098;
	// Item
	public final int Bandage = 1833;
	public final int Energy_Stone = 5589;
	public final int Thief_Key = 1661;
	public final int ADENA = 57;
	public final int SSD = 1463;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _116_BeyondtheHillsofWinter()
	{
		super(116, -1);

		addStartNpc(FILAUR);
		addTalkId(FILAUR);
		addTalkId(FILAUR);
		addTalkId(OBI);
		addTalkId(FILAUR);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30535-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30535-06.htm"))
		{
			if(st.getQuestItemsCount(Bandage) >= 20 && st.getQuestItemsCount(Energy_Stone) >= 5 && st.getQuestItemsCount(Thief_Key) >= 10)
			{
				st.takeItems(Bandage, 20);
				st.takeItems(Energy_Stone, 5);
				st.takeItems(Thief_Key, 10);
				st.giveItems(Supplying_Goods_for_Railroad_Worker, 1);
				st.set("cond", "2");
				st.setState(STARTED);
			}
			else
				htmltext = "30535-04.htm";
		}
		else if(event.equalsIgnoreCase("materials"))
		{
			htmltext = "32052-03.htm";
			st.takeItems(Supplying_Goods_for_Railroad_Worker, 1);
			st.giveItems(SSD, 1650);
			st.unset("cond");
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("adena"))
		{
			htmltext = "32052-03.htm";
			st.takeItems(Supplying_Goods_for_Railroad_Worker, 1);
			st.giveItems(ADENA, 16500);
			st.unset("cond");
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == FILAUR)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 4)
				{
					htmltext = "30535-01.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 30)
				{
					htmltext = "30535-02.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "30535-03.htm";
			}
			else if(cond == 1)
				htmltext = "30535-05.htm";
			else if(cond == 2)
				htmltext = "30535-06.htm";
		}
		else if(npcId == OBI)
			if(cond == 2 && st.getQuestItemsCount(Supplying_Goods_for_Railroad_Worker) > 0)
				htmltext = "32052-01.htm";
		return htmltext;
	}
}
