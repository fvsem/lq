package quests._10273_GoodDayToFly;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.SkillTable;

/**
 * <hr>
 * <em>Квест</em> <strong>Good Day to Fly</strong>
 * <hr>
 * 
 * @author
 * @lastfix HellSinger
 * @version GF
 */
public class _10273_GoodDayToFly extends Quest implements ScriptFile
{
	// NPCs
	private static final int Lekon = 32557;
	// MOBs
	private static final int VultureRider1 = 22614;
	private static final int VultureRider2 = 22615;
	// ITEMs
	private static final short Mark = 13856;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10273_GoodDayToFly()
	{
		super(10273, -1);

		addStartNpc(Lekon);
		addKillId(VultureRider1, VultureRider2);
		addQuestItem(Mark);
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		final L2Player player = st.getPlayer();

		if(event.equalsIgnoreCase("32557-06.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32557-09.htm"))
		{
			if(player.isTransformed())
				return "32557-07.htm";
			st.set("transform", "1");
			SkillTable.getInstance().getInfo(5982, 1).getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("32557-10.htm"))
		{
			if(player.isTransformed())
				return "32557-07.htm";
			st.set("transform", "2");
			SkillTable.getInstance().getInfo(5983, 1).getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("32557-13.htm"))
		{
			if(player.isTransformed())
				return "32557-07.htm";

			if(st.getInt("transform") == 1)
				SkillTable.getInstance().getInfo(5982, 1).getEffects(player, player, false, false);
			else if(st.getInt("transform") == 2)
				SkillTable.getInstance().getInfo(5983, 1).getEffects(player, player, false, false);
		}
		return event;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		String htmltext = "noquest";
		final int transform = st.getInt("transform");
		if(st.getState() == CREATED)
		{
			if(st.getPlayer().getLevel() < 75)
				htmltext = "32557-00.htm";
			else
				htmltext = "32557-01.htm";
		}
		else if(st.isStarted())
		{
			if(st.getQuestItemsCount(Mark) >= 5)
			{
				htmltext = "32557-14.htm";
				if(transform == 1)
					st.giveItems(13553, 1);
				else if(transform == 2)
					st.giveItems(13554, 1);
				st.giveItems(13857, 1);
				st.addExpAndSp(25160, 2525);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else if(transform < 1)
				htmltext = "32557-07.htm";
			else
				htmltext = "32557-11.htm";
		}
		else if(st.isCompleted())
			htmltext = "32557-0a.htm";
		return htmltext;
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		if(st.getInt("cond") == 1 && !(st.getQuestItemsCount(Mark) >= 5))
		{
			st.giveItems(Mark, 1);
			if(st.getQuestItemsCount(Mark) >= 5)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
			else
				st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
