package quests._176_StepsForHonor;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _176_StepsForHonor extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int RAPIDUS = 36479;
	private static final int CLOAK = 14603;

	public _176_StepsForHonor()
	{
		super(176, PARTY_ONE);
		addStartNpc(RAPIDUS);
		addQuestItem(CLOAK);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("36479-02.htm"))
		{
			st.setCond(1);
			st.addNotifyOfPlayerKill();
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		int cond = st.getCond();
		int npcId = npc.getNpcId();

		if(npcId == RAPIDUS)
			if(id == STARTED)
			{
				if(TerritorySiege.isInProgress())
					htmltext = "36479-tw.htm";
				else if(cond == 1)
				{
					htmltext = "36479-03.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 2)
				{
					st.setCond(3);
					st.playSound(SOUND_MIDDLE);
					htmltext = "36479-04.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 3)
				{
					htmltext = "36479-05.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 4)
				{
					st.setCond(5);
					st.playSound(SOUND_MIDDLE);
					htmltext = "36479-06.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 5)
				{
					htmltext = "36479-07.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 6)
				{
					st.setCond(7);
					st.playSound(SOUND_MIDDLE);
					htmltext = "36479-08.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 7)
				{
					htmltext = "36479-09.htm";
					st.addNotifyOfPlayerKill();
				}
				else if(cond == 8)
				{
					st.giveItems(CLOAK, 1);
					htmltext = "36479-09.htm";
					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(false);
				}
			}
			else if(id == CREATED)
			{
				if(st.getPlayer().getLevel() >= 80)
				{
					htmltext = "36479-01.htm";
					st.addNotifyOfPlayerKill();
				}
				else
					htmltext = "36479-00.htm";
			}
			else if(id == COMPLETED)
				htmltext = "36479-11.htm";
		return htmltext;
	}

	@Override
	public String onPlayerKill(L2Player killed, QuestState st)
	{
		int kills = 0;
		int cond = st.getCond();
		L2Player killer = st.getPlayer();
		if(killed == null || killer == null || !checkPlayers(killed, killer))
			return null;
		if(cond == 1 || cond == 3 || cond == 5 || cond == 7)
		{
			// Get kills
			kills = st.getInt("kills");
			// Increase
			kills++;
			// Save
			st.set("kills", String.valueOf(kills));
			// Check
			if(cond == 1 && kills >= 9)
			{
				st.setCond(2);
				st.set("kills", "0");
			}
			else if(cond == 3 && kills >= 18)
			{
				st.setCond(4);
				st.set("kills", "0");
			}
			else if(cond == 5 && kills >= 27)
			{
				st.setCond(6);
				st.set("kills", "0");
			}
			else if(cond == 7 && kills >= 36)
			{
				st.setCond(8);
				st.unset("kills");
			}
		}
		return null;
	}

	@Override
	public void onAbort(QuestState st)
	{
		st.removeNotifyOfPlayerKill();
	}

	public static boolean checkPlayers(L2Player killed, L2Player killer)
	{
		if(killer.getTerritorySiege() < 0 || killed.getTerritorySiege() < 0 || killer.getTerritorySiege() == killed.getTerritorySiege())
			return false;
		if(killer.getParty() != null && killer.getParty() == killed.getParty())
			return false;
		if(killer.getClan() != null && killer.getClan() == killed.getClan())
			return false;
		if(killer.getAllyId() > 0 && killer.getAllyId() == killed.getAllyId())
			return false;
		if(killer.getLevel() < 61 || killed.getLevel() < 61)
			return false;
		return true;
	}
}
