package quests._271_ProofOfValor;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Proof Of Valor
 */

public class _271_ProofOfValor extends Quest implements ScriptFile
{
	// NPC
	private static final int RUKAIN = 30577;
	// Quest Item
	private static final int KASHA_WOLF_FANG_ID = 1473;
	private static final int NECKLACE_OF_VALOR_ID = 1507;
	private static final int NECKLACE_OF_COURAGE_ID = 1506;
	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	private static final int[][] DROPLIST_COND = { { 1, 2, 20475, 0, KASHA_WOLF_FANG_ID, 50, 25, 2 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 271: Proof of Valor");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _271_ProofOfValor()
	{
		super(271, -1);

		addStartNpc(RUKAIN);
		addTalkId(RUKAIN);

		// Mob Drop
		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);

		addQuestItem(KASHA_WOLF_FANG_ID);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30577-03.htm"))
		{
			st.playSound(SOUND_ACCEPT);
			if(st.getQuestItemsCount(NECKLACE_OF_COURAGE_ID) > 0 || st.getQuestItemsCount(NECKLACE_OF_VALOR_ID) > 0)
				htmltext = "30577-07.htm";
			st.set("cond", "1");
			st.setState(STARTED);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == RUKAIN)
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 3)
				{
					htmltext = "30577-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 4)
				{
					htmltext = "30577-01.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getQuestItemsCount(NECKLACE_OF_COURAGE_ID) > 0 || st.getQuestItemsCount(NECKLACE_OF_VALOR_ID) > 0)
				{
					htmltext = "30577-06.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "30577-02.htm";
			}
			else if(cond == 1)
				htmltext = "30577-04.htm";
			else if(cond == 2 && st.getQuestItemsCount(KASHA_WOLF_FANG_ID) == 50)
			{
				st.takeItems(KASHA_WOLF_FANG_ID, -1);
				if(Rnd.chance(14))
				{
					st.takeItems(NECKLACE_OF_VALOR_ID, -1);
					st.giveItems(NECKLACE_OF_VALOR_ID, 1);
				}
				else
				{
					st.takeItems(NECKLACE_OF_COURAGE_ID, -1);
					st.giveItems(NECKLACE_OF_COURAGE_ID, 1);
				}
				htmltext = "30577-05.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 2 && st.getQuestItemsCount(KASHA_WOLF_FANG_ID) < 50)
			{
				htmltext = "30577-04.htm";
				st.set("cond", "1");
				st.setState(STARTED);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}
}
