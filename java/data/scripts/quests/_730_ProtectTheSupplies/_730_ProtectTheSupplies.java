package quests._730_ProtectTheSupplies;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _730_ProtectTheSupplies extends TerritoryWarSuperClass
{
	public _730_ProtectTheSupplies()
	{
		super(730);
		_npcIDs = new int[] { 36591, 36592, 36593, 36594, 36595, 36596, 36597, 36598, 36599 };
		registerAttackIds();
	}

	@Override
	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return npcid - 36590;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
