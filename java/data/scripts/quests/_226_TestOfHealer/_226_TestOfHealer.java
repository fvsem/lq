package quests._226_TestOfHealer;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

import java.util.HashMap;

public class _226_TestOfHealer extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int REPORT_OF_PERRIN_ID = 2810;
	private static final int CRISTINAS_LETTER_ID = 2811;
	private static final int PICTURE_OF_WINDY_ID = 2812;
	private static final int GOLDEN_STATUE_ID = 2813;
	private static final int WINDYS_PEBBLES_ID = 2814;
	private static final int ORDER_OF_SORIUS_ID = 2815;
	private static final int SECRET_LETTER1_ID = 2816;
	private static final int SECRET_LETTER2_ID = 2817;
	private static final int SECRET_LETTER3_ID = 2818;
	private static final int SECRET_LETTER4_ID = 2819;
	private static final int MARK_OF_HEALER_ID = 2820;
	private static final int ADENA_ID = 57;

	private static final HashMap<Integer, Integer[]> DROPLIST = new HashMap<Integer, Integer[]>();
	static
	{
		DROPLIST.put(27134, new Integer[] { 1, 1, 0 });
		DROPLIST.put(27123, new Integer[] { 6, 1, SECRET_LETTER1_ID });
		DROPLIST.put(27124, new Integer[] { 9, 1, SECRET_LETTER2_ID });
		DROPLIST.put(27125, new Integer[] { 11, 1, SECRET_LETTER3_ID });
		DROPLIST.put(27127, new Integer[] { 13, 1, SECRET_LETTER4_ID });
	}

	private void radar2(QuestState st)
	{
		st.addRadar(-44358, 79442, -3634);
	}

	private void radar1(QuestState st)
	{
		st.addRadar(-59985, 79234, -3502);
	}

	private void radar3(QuestState st)
	{
		st.addRadar(-14158, 44953, -3556);
	}

	public _226_TestOfHealer()
	{
		super(226, -1);

		addStartNpc(30473);

		addTalkId(30473);

		for(int npcId : new int[] { 30327, 30424, 30428, 30473, 30658, 30659, 30660, 30661, 30662, 30663, 30664, 30665, 30674 })
			addTalkId(npcId);

		for(int mobId : new int[] { 20150, 27123, 27124, 27125, 27127, 27134 })
			addKillId(mobId);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
		{
			htmltext = "30473-04.htm";
			if(!st.getPlayer().getVarB("dd3"))
				st.getPlayer().setVar("dd3", "1");
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(REPORT_OF_PERRIN_ID, 1);
		}
		else if(event.equalsIgnoreCase("30473_1"))
			htmltext = "30473-08.htm";
		else if(event.equalsIgnoreCase("30473_2"))
		{
			htmltext = "30473-09.htm";
			st.takeItems(GOLDEN_STATUE_ID, -1);
			st.giveItems(MARK_OF_HEALER_ID, 1);
			st.giveItems(ADENA_ID, 233490);
			st.giveItems(7562, 60);
			if(!st.getPlayer().getVarB("q226"))
			{
				st.addExpAndSp(738283, 50662, true);
				st.getPlayer().setVar("q226", "1");
			}
			st.playSound(SOUND_FINISH);
			st.unset("cond");
			st.exitCurrentQuest(false);
		}
		else if(event.equalsIgnoreCase("30428_1"))
		{
			htmltext = "30428-02.htm";
			st.addSpawn(27134, -93254, 147559, -2679);
		}
		else if(event.equalsIgnoreCase("30658_1"))
			if(st.getQuestItemsCount(ADENA_ID) >= 100000)
			{
				htmltext = "30658-02.htm";
				st.takeItems(ADENA_ID, 100000);
				st.giveItems(PICTURE_OF_WINDY_ID, 1);
			}
			else
				htmltext = "30658-05.htm";
		else if(event.equalsIgnoreCase("30658_2"))
		{
			htmltext = "30658-03.htm";
			st.set("cond", "5");
		}
		else if(event.equalsIgnoreCase("30660_1"))
			htmltext = "30660-02.htm";
		else if(event.equalsIgnoreCase("30660_2"))
		{
			htmltext = "30660-03.htm";
			st.takeItems(PICTURE_OF_WINDY_ID, 1);
			st.giveItems(WINDYS_PEBBLES_ID, 1);
		}
		else if(event.equalsIgnoreCase("30674_1"))
		{
			htmltext = "30674-02.htm";
			st.takeItems(ORDER_OF_SORIUS_ID, 1);
			st.addSpawn(27122);
			st.addSpawn(27122);
			st.addSpawn(27123);
			st.playSound(SOUND_BEFORE_BATTLE);
		}
		else if(event.equalsIgnoreCase("30665_1"))
		{
			htmltext = "30665-02.htm";
			st.takeItems(SECRET_LETTER1_ID, 1);
			st.takeItems(SECRET_LETTER2_ID, 1);
			st.takeItems(SECRET_LETTER3_ID, 1);
			st.takeItems(SECRET_LETTER4_ID, 1);
			st.giveItems(CRISTINAS_LETTER_ID, 1);
			st.set("cond", "14");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		if(st.getQuestItemsCount(MARK_OF_HEALER_ID) > 0)
		{
			st.exitCurrentQuest(true);
			return "completed";
		}
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
			st.set("id", "0");
		}
		if(npcId == 30473)
		{
			if(cond == 0)
			{
				if(cond < 15)
					if((st.getPlayer().getClassId().getId() == 0x04 || st.getPlayer().getClassId().getId() == 0x0f || st.getPlayer().getClassId().getId() == 0x1d || st.getPlayer().getClassId().getId() == 0x13) && st.getPlayer().getLevel() > 38)
						htmltext = "30473-03.htm";
					else if(st.getPlayer().getClassId().getId() == 0x04 || st.getPlayer().getClassId().getId() == 0x0f || st.getPlayer().getClassId().getId() == 0x1d || st.getPlayer().getClassId().getId() == 0x13)
						htmltext = "30473-01.htm";
					else
					{
						htmltext = "30473-02.htm";
						st.exitCurrentQuest(true);
					}
				else
				{
					htmltext = "30473-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(npcId == 30473 && cond < 10 && cond > 0)
				htmltext = "30473-05.htm";
			else if(cond == 15 && st.getQuestItemsCount(GOLDEN_STATUE_ID) == 0)
			{
				htmltext = "30473-06.htm";
				st.giveItems(MARK_OF_HEALER_ID, 1);
				if(!st.getPlayer().getVarB("prof2.3"))
				{
					st.addExpAndSp(738283, 50662, true);
					st.giveItems(ADENA_ID, 133490, Config.RATE_QUESTS_OCCUPATION_CHANGE);
					st.getPlayer().setVar("prof2.3", "1");
				}
				st.playSound(SOUND_FINISH);
				st.unset("cond");
				st.exitCurrentQuest(false);
			}
			else if(cond == 15 && st.getQuestItemsCount(GOLDEN_STATUE_ID) > 0)
				htmltext = "30473-07.htm";
		}
		else if(npcId == 30428)
		{
			if(cond == 1 && st.getQuestItemsCount(REPORT_OF_PERRIN_ID) > 0)
				htmltext = "30428-01.htm";
			else if(npcId == 30428 && cond == 2)
			{
				htmltext = "30428-03.htm";
				st.set("cond", "3");
				st.takeItems(REPORT_OF_PERRIN_ID, 1);
			}
		}
		else if(npcId == 30428 && cond == 3)
			htmltext = "30428-04.htm";
		else if(npcId == 30659 && cond == 4)
		{
			int n = Rnd.get(5);
			if(n == 0)
				htmltext = "30659-01.htm";
			if(n == 1)
				htmltext = "30659-02.htm";
			if(n == 2)
				htmltext = "30659-03.htm";
			if(n == 3)
				htmltext = "30659-04.htm";
			if(n == 4)
				htmltext = "30659-05.htm";
		}
		else if(npcId == 30424)
		{
			if(cond == 3)
			{
				htmltext = "30424-01.htm";
				st.set("cond", "4");
			}
			else if(npcId == 30424 && cond == 4)
			{
				htmltext = "30424-02.htm";
				st.set("cond", "4");
			}
		}
		else if(npcId == 30658)
		{
			if(cond == 4 && st.getQuestItemsCount(PICTURE_OF_WINDY_ID) == 0 && st.getQuestItemsCount(WINDYS_PEBBLES_ID) == 0 && st.getQuestItemsCount(GOLDEN_STATUE_ID) == 0)
				htmltext = "30658-01.htm";
			else if(cond == 4 && st.getQuestItemsCount(PICTURE_OF_WINDY_ID) > 0)
				htmltext = "30658-04.htm";
			else if(cond == 4 && st.getQuestItemsCount(WINDYS_PEBBLES_ID) > 0)
			{
				htmltext = "30658-06.htm";
				st.giveItems(GOLDEN_STATUE_ID, 1);
				st.takeItems(WINDYS_PEBBLES_ID, 1);
				st.set("cond", "5");
			}
			else if(cond == 5)
				htmltext = "30658-07.htm";
		}
		else if(npcId == 30660)
		{
			if(cond == 4 && st.getQuestItemsCount(PICTURE_OF_WINDY_ID) > 0)
				htmltext = "30660-01.htm";
			else if(cond == 4 && st.getQuestItemsCount(WINDYS_PEBBLES_ID) > 0)
				htmltext = "30660-04.htm";
		}
		else if(npcId == 30327)
		{
			if(cond == 5)
			{
				htmltext = "30327-01.htm";
				st.giveItems(ORDER_OF_SORIUS_ID, 1);
				st.set("cond", "6");
			}
			else if(cond > 5 && cond < 12)
				htmltext = "30327-02.htm";
			else if(cond == 14)
			{
				htmltext = "30327-03.htm";
				st.takeItems(CRISTINAS_LETTER_ID, 1);
				st.set("cond", "15");
			}
		}
		else if(npcId == 30674)
		{
			if(cond == 6 && st.getQuestItemsCount(ORDER_OF_SORIUS_ID) > 0)
			{
				htmltext = "30674-01.htm";
				st.addSpawn(27122, -97547, 106503, -3405);
				st.addSpawn(27122, -97526, 106584, -3405);
				st.addSpawn(27123, -97441, 106585, -3405);
			}
			else if(cond == 6 && st.getQuestItemsCount(SECRET_LETTER1_ID) > 0)
			{
				htmltext = "30674-03.htm";
				st.set("cond", "7");
			}
		}
		else if(npcId == 30662)
		{
			if(cond == 7 || cond == 8)
			{
				htmltext = "30662-01.htm";
				st.set("cond", "8");
				radar1(st);
			}
			else if(cond == 9 || cond == 10)
			{
				htmltext = "30662-03.htm";
				st.set("cond", "10");
				radar2(st);
			}
			else if(cond == 11 || cond == 12)
			{
				htmltext = "30662-02.htm";
				st.set("cond", "12");
				radar3(st);
			}
			else if(cond == 13)
				htmltext = "30662-04.htm";
		}
		else if(npcId == 30663)
		{
			if(cond == 7 || cond == 8)
			{
				htmltext = "30663-01.htm";
				st.set("cond", "8");
				radar1(st);
			}
			else if(cond == 9 || cond == 10)
			{
				htmltext = "30663-03.htm";
				st.set("cond", "10");
				radar2(st);
			}
			else if(cond == 11 || cond == 12)
			{
				htmltext = "30663-02.htm";
				st.set("cond", "12");
				radar3(st);
			}
			else if(cond == 13)
				htmltext = "30663-04.htm";
		}
		else if(npcId == 30664)
		{
			if(cond == 7 || cond == 8)
			{
				htmltext = "30664-01.htm";
				st.set("cond", "8");
				radar1(st);
			}
			else if(cond == 9 || cond == 10)
			{
				htmltext = "30664-03.htm";
				st.set("cond", "10");
				radar2(st);
			}
			else if(cond == 11 || cond == 12)
			{
				htmltext = "30664-02.htm";
				st.set("cond", "12");
				radar3(st);
			}
			else if(cond == 13)
				htmltext = "30664-04.htm";
		}
		else if(npcId == 30661)
		{
			if(cond == 8)
			{
				htmltext = "30661-01.htm";
				st.addSpawn(27124);
				st.addSpawn(27124);
				st.addSpawn(27124);
				st.playSound(SOUND_BEFORE_BATTLE);
				st.set("cond", "9");
			}
			else if(cond == 10)
			{
				htmltext = "30661-02.htm";
				st.addSpawn(27125);
				st.addSpawn(27125);
				st.addSpawn(27125);
				st.playSound(SOUND_BEFORE_BATTLE);
				st.set("cond", "11");
			}
			else if(cond == 12)
			{
				htmltext = "30661-03.htm";
				st.addSpawn(27126);
				st.addSpawn(27126);
				st.addSpawn(27127);
				st.playSound(SOUND_BEFORE_BATTLE);
				st.set("cond", "13");
			}
			else if(cond == 13)
				htmltext = "30661-04.htm";
		}
		else if(npcId == 30665)
			if(cond == 13)
				htmltext = "30665-01.htm";
			else
				htmltext = "30665-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		Integer[] d = DROPLIST.get(npcId);
		int condition = d[0];
		int maxcount = d[1];
		int item = d[2];
		if(st.getInt("cond") == condition && st.getQuestItemsCount(item) < maxcount)
			if(item != 0)
				st.giveItems(item, 1);
		if(npcId == 27134)
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		return null;
	}
}
