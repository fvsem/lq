package quests._324_SweetestVenom;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _324_SweetestVenom extends Quest implements ScriptFile
{
	// NPCs
	private static int ASTARON = 30351;
	// Mobs
	private static int Prowler = 20034;
	private static int Venomous_Spider = 20038;
	private static int Arachnid_Tracker = 20043;
	// Items
	private static int VENOM_SAC = 1077;
	private static int ADENA = 57;
	// Chances
	private static int VENOM_SAC_BASECHANCE = 60;

	public _324_SweetestVenom()
	{
		super(324, -1);
		addStartNpc(ASTARON);
		addKillId(Prowler);
		addKillId(Venomous_Spider);
		addKillId(Arachnid_Tracker);
		addQuestItem(VENOM_SAC);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != ASTARON)
			return htmltext;
		int _state = st.getState();

		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() >= 18)
			{
				htmltext = "30351-03.htm";
				st.set("cond", "0");
			}
			else
			{
				htmltext = "30351-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(_state == STARTED)
		{
			long _count = st.getQuestItemsCount(VENOM_SAC);
			if(_count >= 10)
			{
				htmltext = "30351-06.htm";
				st.takeItems(VENOM_SAC, -1);
				st.giveItems(ADENA, 5810);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30351-05.htm";
		}
		return htmltext;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30351-04.htm") && st.getState() == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		return event;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		long _count = qs.getQuestItemsCount(VENOM_SAC);
		int _chance = VENOM_SAC_BASECHANCE + (npc.getNpcId() - Prowler) / 4 * 12;

		if(_count < 10 && Rnd.chance(_chance))
		{
			qs.giveItems(VENOM_SAC, 1);
			if(_count == 9)
			{
				qs.set("cond", "2");
				qs.playSound(SOUND_MIDDLE);
			}
			else
				qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 324: Sweetest Venom");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
