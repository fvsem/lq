package quests._311_ExpulsionOfEvilSpirits;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.ai.CtrlEvent;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2Multisell;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Rnd;

public class _311_ExpulsionOfEvilSpirits extends Quest implements ScriptFile
{
	// TODO дебаф в зоне с алтарём
	// TODO AI для рб (рандомная телепортация)

	// NPCs
	private final static int CHAIREN = 32655;
	private final static int ALTAR = 18811;
	private final static int DARK_SHAMAN_VARANGKA = 18808;

	// MONSTERs
	private final static int[] MOBS = { 22691, 22692, 22693, 22694, 22695, 22696, 22697, 22698, 22699, 22700, 22701, 22702 };

	// Items
	private final static int SOUL_PENDANT = 14848;
	private final static int SOUL_CORE = 14881;
	private final static int RAGNA_ORCS_AMULET = 14882;

	// Quest Items Drop Chance
	private static int DROP_CHANCE1 = 1;
	private static int DROP_CHANCE2 = 40;

	private static final Location altarloc = new Location(74181, -101944, -967, 32767);

	private static L2NpcInstance _varangka;
	private static L2NpcInstance _altar;

	public _311_ExpulsionOfEvilSpirits()
	{
		super(311, PARTY_ONE);

		addStartNpc(CHAIREN);
		addTalkId(CHAIREN);

		addAttackId(ALTAR);

		addKillId(MOBS);
		addKillId(DARK_SHAMAN_VARANGKA);

		addQuestItem(SOUL_CORE, RAGNA_ORCS_AMULET, SOUL_PENDANT);

		long respawnTime = ServerVariables.getLong(_311_ExpulsionOfEvilSpirits.class.getSimpleName(), 0);
		if(respawnTime == 0 || respawnTime - System.currentTimeMillis() < 0)
			L2GameThreadPools.getInstance().scheduleGeneral(new AltarSpawn(), 5000);
		else
			L2GameThreadPools.getInstance().scheduleGeneral(new AltarSpawn(), respawnTime - System.currentTimeMillis());
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		if(player == null)
			return null;

		if(event.equalsIgnoreCase("32655-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32655-4.htm"))
		{
			if(st.getQuestItemsCount(RAGNA_ORCS_AMULET) >= 0)
			{
				L2Multisell.getInstance().separateAndSend(32655, player, 0);
				return null;
			}
			else
				return "32655-6b.htm";
		}
		else if(event.equalsIgnoreCase("32655-5.htm"))
		{
			if(st.getQuestItemsCount(SOUL_CORE) >= 10)
			{
				st.takeItems(SOUL_CORE, 10);
				st.giveItems(SOUL_PENDANT, 1, false);
				st.playSound(SOUND_ITEMGET);
			}
			else
				return "32655-5a.htm";
		}
		else if(event.equalsIgnoreCase("32655-9.htm"))
		{
			if(st.getQuestItemsCount(RAGNA_ORCS_AMULET) > 0 || st.getQuestItemsCount(SOUL_CORE) > 0)
				return "32655-9.htm";
			else
			{
				st.exitCurrentQuest(true);
				st.playSound(SOUND_FINISH);
				return "32655-10.htm";
			}
		}
		else if(event.equalsIgnoreCase("32655-10.htm"))
		{
			st.unset("cond");
			st.exitCurrentQuest(true);
			st.playSound(SOUND_FINISH);
		}

		return event;
	}

	// сделал
	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");

		if(npcId == CHAIREN)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 80)
					return "32655-0.htm";
				else
				{
					st.exitCurrentQuest(true);
					return "32655-0a.htm";
				}
			}
			else if(st.getState() == STARTED)
				return "32655-3.htm";

		return "noquest";
	}

	@Override
	public String onAttack(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		if(player == null)
			return null;

		if(st.getInt("cond") == 1)
			if(st.getQuestItemsCount(SOUL_PENDANT) > 0 && Rnd.chance(20))
				if(_varangka == null && L2ObjectsStorage.getByNpcId(DARK_SHAMAN_VARANGKA) == null)
				{
					_varangka = addSpawn(DARK_SHAMAN_VARANGKA, altarloc, 0, 0);
					_varangka.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, player, Rnd.get(1, 100));
				}

		return null;
	}

	// сделал
	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();
		if(player == null)
			return null;

		if(cond == 1 && ArrayUtil.arrayContains(MOBS, npcId))
		{
			if(st.getQuestItemsCount(SOUL_CORE) < 10)
				st.rollAndGive(SOUL_CORE, 1, DROP_CHANCE1);
			if(st.getQuestItemsCount(SOUL_CORE) == 10)
				st.playSound(SOUND_MIDDLE);

			st.rollAndGive(RAGNA_ORCS_AMULET, 1, DROP_CHANCE2);
		}
		else if(cond == 1 && npcId == DARK_SHAMAN_VARANGKA)
		{
			st.takeItems(SOUL_PENDANT, 1);
			_altar.doDie(player);
			_altar = null;
			_varangka = null;

			long respawn = Rnd.get(14400000, 28800000);
			ServerVariables.set(_311_ExpulsionOfEvilSpirits.class.getSimpleName(), String.valueOf(System.currentTimeMillis() + respawn));
			L2GameThreadPools.getInstance().scheduleGeneral(new AltarSpawn(), respawn);
		}

		return null;
	}

	/**
	 * спаунит алтарь
	 */
	private class AltarSpawn implements Runnable
	{
		@Override
		public void run()
		{
			if(L2ObjectsStorage.getByNpcId(ALTAR) == null)
			{
				_altar = addSpawn(ALTAR, altarloc, 0, 0);
				_altar.setInvul(true);
				ServerVariables.unset(_311_ExpulsionOfEvilSpirits.class.getSimpleName());
			}
		}
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
