package quests._108_JumbleTumbleDiamondFuss;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _108_JumbleTumbleDiamondFuss extends Quest implements ScriptFile
{
	private static int GOUPHS_CONTRACT = 1559;
	private static int REEPS_CONTRACT = 1560;
	private static int ELVEN_WINE = 1561;
	private static int BRONPS_DICE = 1562;
	private static int BRONPS_CONTRACT = 1563;
	private static int AQUAMARINE = 1564;
	private static int CHRYSOBERYL = 1565;
	private static int GEM_BOX1 = 1566;
	private static int COAL_PIECE = 1567;
	private static int BRONPS_LETTER = 1568;
	private static int BERRY_TART = 1569;
	private static int BAT_DIAGRAM = 1570;
	private static int STAR_DIAMOND = 1571;
	private static int SILVERSMITH_HAMMER = 1511;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _108_JumbleTumbleDiamondFuss()
	{
		super(108, -1);

		addStartNpc(30523);

		addTalkId(30523);
		addTalkId(30523);

		addTalkId(30516);
		addTalkId(30521);
		addTalkId(30522);
		addTalkId(30523);
		addTalkId(30526);
		addTalkId(30529);
		addTalkId(30555);

		addKillId(20323);
		addKillId(20324);
		addKillId(20480);

		addQuestItem(new int[] {
				GEM_BOX1,
				STAR_DIAMOND,
				GOUPHS_CONTRACT,
				REEPS_CONTRACT,
				ELVEN_WINE,
				BRONPS_CONTRACT,
				AQUAMARINE,
				CHRYSOBERYL,
				COAL_PIECE,
				BRONPS_DICE,
				BRONPS_LETTER,
				BERRY_TART,
				BAT_DIAGRAM, });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30523-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.giveItems(GOUPHS_CONTRACT, 1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30555-02.htm"))
		{
			st.takeItems(REEPS_CONTRACT, 1);
			st.giveItems(ELVEN_WINE, 1);
			st.setState(STARTED);
			st.set("cond", "3");
		}
		else if(event.equals("30526-02.htm"))
		{
			st.takeItems(BRONPS_DICE, 1);
			st.giveItems(BRONPS_CONTRACT, 1);
			st.setState(STARTED);
			st.set("cond", "5");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == 30523)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 4)
				{
					htmltext = "30523-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 10)
					htmltext = "30523-02.htm";
				else
				{
					htmltext = "30523-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 0 && st.getQuestItemsCount(GOUPHS_CONTRACT) > 0)
				htmltext = "30523-04.htm";
			else if(cond > 1 && cond < 7 && (st.getQuestItemsCount(REEPS_CONTRACT) > 0 || st.getQuestItemsCount(ELVEN_WINE) > 0 || st.getQuestItemsCount(BRONPS_DICE) > 0 || st.getQuestItemsCount(BRONPS_CONTRACT) > 0))
				htmltext = "30523-05.htm";
			else if(cond == 7 && st.getQuestItemsCount(GEM_BOX1) > 0)
			{
				htmltext = "30523-06.htm";
				st.takeItems(GEM_BOX1, 1);
				st.giveItems(COAL_PIECE, 1);
				st.set("cond", "8");
				st.setState(STARTED);
			}
			else if(cond > 7 && cond < 12 && (st.getQuestItemsCount(BRONPS_LETTER) > 0 || st.getQuestItemsCount(COAL_PIECE) > 0 || st.getQuestItemsCount(BERRY_TART) > 0 || st.getQuestItemsCount(BAT_DIAGRAM) > 0))
				htmltext = "30523-07.htm";
			else if(cond == 12 && st.getQuestItemsCount(STAR_DIAMOND) > 0)
			{
				htmltext = "30523-08.htm";
				st.takeItems(STAR_DIAMOND, 1);
				for(int item = 4412; item <= 4417; item++)
					st.giveItems(item, 10);
				st.giveItems(1060, 100);
				if(st.getPlayer().getLevel() < 25)
					if(st.getPlayer().getClassId().isMage())
					{
						st.playTutorialVoice("tutorial_voice_027");
						st.giveItems(5790, 3000);
					}
					else
					{
						st.playTutorialVoice("tutorial_voice_026");
						st.giveItems(5789, 6000);
					}
				st.giveItems(SILVERSMITH_HAMMER, 1);
				st.playSound(SOUND_FINISH);
				st.unset("cond");
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == 30516)
		{
			if(cond == 1 && st.getQuestItemsCount(GOUPHS_CONTRACT) > 0)
			{
				htmltext = "30516-01.htm";
				st.giveItems(REEPS_CONTRACT, 1);
				st.takeItems(GOUPHS_CONTRACT, 1);
				st.set("cond", "2");
				st.setState(STARTED);
			}
			else if(cond >= 2)
				htmltext = "30516-02.htm";
		}
		else if(npcId == 30555)
		{
			if(cond == 2 && st.getQuestItemsCount(REEPS_CONTRACT) == 1)
				htmltext = "30555-01.htm";
			else if(cond == 3 && st.getQuestItemsCount(ELVEN_WINE) > 0)
				htmltext = "30555-03.htm";
			else if(cond == 7 && st.getQuestItemsCount(GEM_BOX1) == 1)
				htmltext = "30555-04.htm";
			else
				htmltext = "30555-05.htm";
		}
		else if(npcId == 30529)
		{
			if(cond == 3 && st.getQuestItemsCount(ELVEN_WINE) > 0)
			{
				st.takeItems(ELVEN_WINE, 1);
				st.giveItems(BRONPS_DICE, 1);
				htmltext = "30529-01.htm";
				st.set("cond", "4");
				st.setState(STARTED);
			}
			else if(cond == 4)
				htmltext = "30529-02.htm";
			else
				htmltext = "30529-03.htm";
		}
		else if(npcId == 30526)
		{
			if(cond == 4 && st.getQuestItemsCount(BRONPS_DICE) > 0)
				htmltext = "30526-01.htm";
			else if(cond == 5 && st.getQuestItemsCount(BRONPS_CONTRACT) > 0 && (st.getQuestItemsCount(AQUAMARINE) < 10 || st.getQuestItemsCount(CHRYSOBERYL) < 10))
				htmltext = "30526-03.htm";
			else if(cond == 6 && st.getQuestItemsCount(BRONPS_CONTRACT) > 0 && st.getQuestItemsCount(AQUAMARINE) == 10 && st.getQuestItemsCount(CHRYSOBERYL) == 10)
			{
				htmltext = "30526-04.htm";
				st.takeItems(BRONPS_CONTRACT, -1);
				st.takeItems(AQUAMARINE, -1);
				st.takeItems(CHRYSOBERYL, -1);
				st.giveItems(GEM_BOX1, 1);
				st.setState(STARTED);
				st.set("cond", "7");
			}
			else if(cond == 7 && st.getQuestItemsCount(GEM_BOX1) > 0)
				htmltext = "30526-05.htm";
			else if(cond == 8 && st.getQuestItemsCount(COAL_PIECE) > 0)
			{
				htmltext = "30526-06.htm";
				st.takeItems(COAL_PIECE, 1);
				st.giveItems(BRONPS_LETTER, 1);
				st.setState(STARTED);
				st.set("cond", "9");
			}
			else if(cond == 9 && st.getQuestItemsCount(BRONPS_LETTER) > 0)
				htmltext = "30526-07.htm";
			else
				htmltext = "30526-08.htm";
		}
		else if(npcId == 30521)
		{
			if(cond == 9 && st.getQuestItemsCount(BRONPS_LETTER) > 0)
			{
				htmltext = "30521-01.htm";
				st.takeItems(BRONPS_LETTER, 1);
				st.giveItems(BERRY_TART, 1);
				st.set("cond", "10");
				st.setState(STARTED);
			}
			else if(cond == 10 && st.getQuestItemsCount(BERRY_TART) > 0)
				htmltext = "30521-02.htm";
			else
				htmltext = "30521-03.htm";
		}
		else if(npcId == 30522)
			if(cond == 10 && st.getQuestItemsCount(BERRY_TART) > 0)
			{
				htmltext = "30522-01.htm";
				st.takeItems(BERRY_TART, 1);
				st.giveItems(BAT_DIAGRAM, 1);
				st.set("cond", "11");
				st.setState(STARTED);
			}
			else if(cond == 11 && st.getQuestItemsCount(BAT_DIAGRAM) > 0)
				htmltext = "30522-02.htm";
			else if(cond == 12 && st.getQuestItemsCount(STAR_DIAMOND) > 0)
				htmltext = "30522-03.htm";
			else
				htmltext = "30522-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 20323 || npcId == 20324)
		{
			if(cond == 5 && st.getQuestItemsCount(BRONPS_CONTRACT) > 0)
				if(Rnd.chance(25))
				{
					if(st.getQuestItemsCount(AQUAMARINE) < 10)
					{
						st.giveItems(AQUAMARINE, 1);
						if(Rnd.chance(10) && st.getQuestItemsCount(CHRYSOBERYL) < 10)
							st.giveItems(CHRYSOBERYL, 1);
						if(st.getQuestItemsCount(AQUAMARINE) < 10)
							st.playSound(SOUND_ITEMGET);
						else
						{
							st.playSound(SOUND_MIDDLE);
							if(st.getQuestItemsCount(AQUAMARINE) == 10 && st.getQuestItemsCount(CHRYSOBERYL) == 10)
								st.set("cond", "6");
							st.setState(STARTED);
						}
					}
				}
				else if(Rnd.chance(70))
					if(st.getQuestItemsCount(CHRYSOBERYL) < 10)
					{
						st.giveItems(CHRYSOBERYL, 1);
						if(Rnd.chance(90) && st.getQuestItemsCount(AQUAMARINE) < 10)
							st.giveItems(AQUAMARINE, 1);
						if(st.getQuestItemsCount(CHRYSOBERYL) < 10)
							st.playSound(SOUND_ITEMGET);
						else
						{
							st.playSound(SOUND_MIDDLE);
							if(st.getQuestItemsCount(AQUAMARINE) == 10 && st.getQuestItemsCount(CHRYSOBERYL) == 10)
								st.set("cond", "6");
							st.setState(STARTED);
						}
					}
		}
		else if(npcId == 20480)
			if(cond == 11 && st.getQuestItemsCount(BAT_DIAGRAM) > 0 && st.getQuestItemsCount(STAR_DIAMOND) == 0)
				if(Rnd.chance(20))
				{
					st.takeItems(BAT_DIAGRAM, 1);
					st.giveItems(STAR_DIAMOND, 1);
					st.set("cond", "12");
					st.setState(STARTED);
					st.playSound(SOUND_MIDDLE);
				}
		return null;
	}
}
