package quests._10272_LightFragment;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

/**
 * @<!-- L2System -->
 * @date 29.12.2010
 * @time 1:23:28 <br>
 *       <h2>Описание</h2>
 *       <ol>
 *       <li>Поговорите с солдатом Орви на Базе Альянса Кецеруса. Он пошлет вас к Боевому Магу Артиусу с квестовым итемом - документом.</li>
 *       <li>Поговорите с Артиусом. Он попросит подойти попозже.</li>
 *       <li>Поговорите с Артиусом еще раз. Он посоветует Вам обратиться к спрятавшемуся в крепости Жрецу Реликию. Для этого обратитесь к солдату Джинби.</li>
 *       <li>Солдат Джинби отведет Вас к Жрецу Реликию.</li>
 *       <li>Жрец Реликий рассказывает Вам о Монархе Экимусе, который охраняет Семя Бессмертия.</li>
 *       <li>Вернитесь к Артиусу и передайте ему эту информацию.</li>
 *       <li>Артиус пошлет вас убивать мобов в Семени Разрушения и добывать Порошок Тьмы. Необходимо добыть 100 единиц порошка. Мобы для охоты: Боевой Дракон, Дракон-Пельтаст, Дракон-Лекарь, Дракон-Легионер, Дракон-Легат, Молодой Дракон-Маг,
 *       Дракон-Маг, Боевой Дракон-Маг, Дракон-Архимаг, Выживший Дракон-Легат, Выживший Дракон-Легионер, Выживший Белый Дракон, Выживший Боевой Дракон, Выживший Боевой Дракон-Маг, Выживший Дракон-Маг, Выживший Дракон-Лекарь, Выживший Дракон-Пельтаст.
 *       </li>
 *       <li>Набив 100 единиц порошка, возвращайтесь к Артиусу. Он пошлет Вас в Деревню Эльфов, под Тень Дерева Жизни.</li>
 *       <li>Идите на север Эльфийской Деревни, и в тени Древа Жизни очистите Порошок Тьмы, превратив его в Порошок Света. Это делается путем двойного щелчка мышью на предмет в инвентаре.</li>
 *       <li>Отнесите Порошок Света к Инженеру Лекону на Базу Альянса.</li>
 *       <li>Лекон создаст Осколок Святого Света - оружие для борьбы с Тиатой.</li>
 *       <li>Зайдите к Артиусу и поговорите с ним, получите награду за выполнение квеста.</li>
 *       </ol>
 */
public class _10272_LightFragment extends Quest implements ScriptFile
{
	private static final int ORBYU = 32560;
	private static final int ARTIUS = 32559;
	private static final int GINBY = 32566;
	private static final int LELIKIA = 32567;
	private static final int LEKON = 32557;

	private static final int[] MOBS = { 22537, 22538, 22539, 22541, 22542, 22543, 22544, 22547, 22548, 22549, 22559, 22560, 22561, 22562, 22563, 22564, 22566, 22567 };

	private static final int INSPECTOR_MEDIBALS_DOCUMENT = 13852;
	private static final int DESTROYED_DARKNESS_FRAGMENT_POWDER = 13853;
	private static final int DESTROYED_LIGHT_FRAGMENT_POWDER = 13854;
	private static final int SACRED_LIGHT_FRAGMENT = 13855;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10272_LightFragment()
	{
		super(10272, PARTY_ALL);

		addStartNpc(ORBYU);
		addTalkId(ARTIUS, GINBY, LELIKIA, LEKON);
		addKillId(MOBS);
		addQuestItem(INSPECTOR_MEDIBALS_DOCUMENT, DESTROYED_DARKNESS_FRAGMENT_POWDER, DESTROYED_LIGHT_FRAGMENT_POWDER);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		L2Player player = st.getPlayer();
		if(event.equalsIgnoreCase("32560-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.giveItems(INSPECTOR_MEDIBALS_DOCUMENT, 1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32559-03.htm"))
		{
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32559-06.htm"))
		{
			st.set("cond", "5");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32559-09.htm"))
		{
			st.set("cond", "6");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("teleSecret"))
		{
			if(st.getQuestItemsCount(ADENA_ID) >= 10000)
			{
				st.takeItems(ADENA_ID, 10000);
				player.teleToLocation(-23768, -8968, -5412);
				return null;
			}
			htmltext = "32566-02.htm";
		}
		else if(event.equalsIgnoreCase("32567-02.htm"))
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		else
		{
			if(event.equalsIgnoreCase("teleBack"))
			{
				player.teleToLocation(-185095, 242815, 1576);
				return null;
			}
			if(event.equalsIgnoreCase("32557-02.htm"))
			{
				st.set("cond", "8");
				st.takeItems(DESTROYED_LIGHT_FRAGMENT_POWDER, -1);
				st.giveItems(SACRED_LIGHT_FRAGMENT, 1);
				st.playSound(SOUND_MIDDLE);
			}
			else if(event.equalsIgnoreCase("32559-12.htm"))
			{
				st.giveItems(ADENA_ID, 556980);
				st.addExpAndSp(1009016, 91363);
				st.unset("cond");
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(npcId == ORBYU)
		{
			if(id == CREATED)
			{
				L2Player player = st.getPlayer();
				QuestState qs = player.getQuestState("_10271_TheEnvelopingDarkness");
				if(qs != null && qs.isCompleted() && player.getLevel() >= 75 || player.isGM())
					htmltext = "32560-01.htm";
				else
				{
					htmltext = "32560-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "32560-03.htm";
		}
		else if(npcId == ARTIUS)
		{
			if(cond == 1 && st.getQuestItemsCount(INSPECTOR_MEDIBALS_DOCUMENT) != 0)
			{
				st.takeItems(INSPECTOR_MEDIBALS_DOCUMENT, 1);
				htmltext = "32559-01.htm";
				st.set("cond", "2");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 2)
				htmltext = "32559-02.htm";
			else if(cond == 3)
				htmltext = "32559-04.htm";
			else if(cond == 4)
				htmltext = "32559-05.htm";
			else if(cond == 5)
			{
				if(st.getQuestItemsCount(DESTROYED_DARKNESS_FRAGMENT_POWDER) < 100)
					htmltext = "32559-07.htm";
				else
					htmltext = "32559-08.htm";
			}
			else if(cond == 6)
			{
				if(st.getQuestItemsCount(DESTROYED_LIGHT_FRAGMENT_POWDER) >= 100)
				{
					st.set("cond", "7");
					htmltext = "32559-10.htm";
				}
				else
					htmltext = "32559-09.htm";
			}
			else if(cond == 8)
				htmltext = "32559-11.htm";
		}
		else if(npcId == GINBY)
		{
			if(cond == 3)
				htmltext = "32566-01.htm";
		}
		else if(npcId == LELIKIA)
		{
			if(cond == 3)
				htmltext = "32567-01.htm";
		}
		else if(npcId == LEKON)
			if(cond == 7 && st.getQuestItemsCount(DESTROYED_LIGHT_FRAGMENT_POWDER) >= 100)
				htmltext = "32557-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(ArrayUtil.arrayContains(MOBS, npcId))
		{
			int cond = st.getInt("cond");
			if(st != null && cond == 5)
				st.rollAndGive(DESTROYED_DARKNESS_FRAGMENT_POWDER, 1, 80);
		}
		return null;
	}
}
