package quests._214_TrialOfScholar;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _214_TrialOfScholar extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int MARK_OF_SCHOLAR_ID = 2674;
	private static final int MIRIENS_SIGIL1_ID = 2675;
	private static final int MIRIENS_SIGIL2_ID = 2676;
	private static final int MIRIENS_SIGIL3_ID = 2677;
	private static final int MIRIENS_INSTRUCTION_ID = 2678;
	private static final int MARIAS_LETTER1_ID = 2679;
	private static final int SYMBOL_OF_JUREK_ID = 2698;
	private static final int SYMBOL_OF_SYLVAIN_ID = 2693;
	private static final int SYMBOL_OF_CRONOS_ID = 2720;
	private static final int HIGHPRIESTS_SIGIL_ID = 2689;
	private static final int SYLVAINS_LETTER_ID = 2692;
	private static final int CRYSTAL_OF_PURITY1_ID = 2688;
	private static final int LUCILLAS_HANDBAG_ID = 2682;
	private static final int CRETAS_LETTER1_ID = 2683;
	private static final int BROWN_SCROLL_SCRAP_ID = 2687;
	private static final int CRETAS_PAINTING3_ID = 2686;
	private static final int MARIAS_LETTER2_ID = 2680;
	private static final int LUKAS_LETTER_ID = 2681;
	private static final int CRETAS_PAINTING2_ID = 2685;
	private static final int CRETAS_PAINTING1_ID = 2684;
	private static final int CRYSTAL_OF_PURITY2_ID = 2714;
	private static final int VALKONS_REQUEST_ID = 2710;
	private static final int JUREKS_LIST_ID = 2694;
	private static final int GMAGISTERS_SIGIL_ID = 2690;
	private static final int MEYEDESTROYERS_SKIN_ID = 2695;
	private static final int SHAMANS_NECKLACE_ID = 2696;
	private static final int SHACKLES_SCALP_ID = 2697;
	private static final int CRETAS_LETTER2_ID = 2701;
	private static final int DIETERS_KEY_ID = 2700;
	private static final int CRONOS_SIGIL_ID = 2691;
	private static final int CRONOS_LETTER_ID = 2699;
	private static final int SCRIPTURE_CHAPTER_1_ID = 2706;
	private static final int SCRIPTURE_CHAPTER_2_ID = 2707;
	private static final int SCRIPTURE_CHAPTER_3_ID = 2708;
	private static final int SCRIPTURE_CHAPTER_4_ID = 2709;
	private static final int TRIFFS_RING_ID = 2705;
	private static final int DIETERS_DIARY_ID = 2703;
	private static final int DIETERS_LETTER_ID = 2702;
	private static final int RAUTS_LETTER_ENVELOPE_ID = 2704;
	private static final int STRONG_LIQUOR_ID = 2713;
	private static final int POITANS_NOTES_ID = 2711;
	private static final int CASIANS_LIST_ID = 2715;
	private static final int GHOULS_SKIN_ID = 2716;
	private static final int MEDUSAS_BLOOD_ID = 2717;
	private static final int FETTEREDSOULS_ICHOR_ID = 2718;
	private static final int ENCHT_GARGOYLES_NAIL_ID = 2719;

	public _214_TrialOfScholar()
	{
		super(214, -1);

		addStartNpc(30461);
		addTalkId(30070, 30071, 30103, 30111, 30115, 30230, 30316, 30458, 30461, 30608, 30609, 30610, 30611, 30612);
		addKillId(20158, 20201, 20235, 20269, 20552, 20554, 20567, 20580, 20068);
		addQuestItem(BROWN_SCROLL_SCRAP_ID, MEYEDESTROYERS_SKIN_ID, SHAMANS_NECKLACE_ID, SHACKLES_SCALP_ID, SCRIPTURE_CHAPTER_3_ID, GHOULS_SKIN_ID, MEDUSAS_BLOOD_ID, FETTEREDSOULS_ICHOR_ID, ENCHT_GARGOYLES_NAIL_ID);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			htmltext = "30461-04.htm";
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(MIRIENS_SIGIL1_ID, 1);
			{
				if(!st.getPlayer().getVarB("dd1"))
				{
					st.giveItems(7562, 64);
					st.getPlayer().setVar("dd1", "1");
				}
			}
		}
		else if(event.equals("30461_1"))
		{
			if(st.getPlayer().getLevel() < 36)
			{
				htmltext = "30461-09.htm";
				st.takeItems(SYMBOL_OF_JUREK_ID, 1);
				st.giveItems(MIRIENS_INSTRUCTION_ID, 1);
				st.takeItems(MIRIENS_SIGIL2_ID, 1);
			}
			else
			{
				htmltext = "30461-10.htm";
				st.takeItems(SYMBOL_OF_JUREK_ID, 1);
				st.giveItems(MIRIENS_SIGIL3_ID, 1);
				st.takeItems(MIRIENS_SIGIL2_ID, 1);
			}
		}
		else if(event.equals("30070_1"))
		{
			htmltext = "30070-02.htm";
			st.giveItems(HIGHPRIESTS_SIGIL_ID, 1);
			st.giveItems(SYLVAINS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30608_1"))
		{
			htmltext = "30608-02.htm";
			st.giveItems(MARIAS_LETTER1_ID, 1);
			st.takeItems(SYLVAINS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30608_2"))
			htmltext = "30608-07.htm";
		else if(event.equals("30608_3"))
		{
			htmltext = "30608-08.htm";
			st.giveItems(LUCILLAS_HANDBAG_ID, 1);
			st.takeItems(CRETAS_LETTER1_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30608_4"))
		{
			htmltext = "30608-14.htm";
			st.takeItems(BROWN_SCROLL_SCRAP_ID, st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID));
			st.giveItems(CRYSTAL_OF_PURITY1_ID, 1);
			st.takeItems(CRETAS_PAINTING3_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30115_1"))
			htmltext = "30115-02.htm";
		else if(event.equals("30115_2"))
		{
			htmltext = "30115-03.htm";
			st.giveItems(JUREKS_LIST_ID, 1);
			st.giveItems(GMAGISTERS_SIGIL_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30071_1"))
		{
			htmltext = "30071-04.htm";
			st.giveItems(CRETAS_PAINTING3_ID, 1);
			st.takeItems(CRETAS_PAINTING2_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30609_1"))
			htmltext = "30609-02.htm";
		else if(event.equals("30609_2"))
			htmltext = "30609-03.htm";
		else if(event.equals("30609_3"))
			htmltext = "30609-04.htm";
		else if(event.equals("30609_4"))
		{
			htmltext = "30609-05.htm";
			st.giveItems(CRETAS_LETTER1_ID, 1);
			st.takeItems(MARIAS_LETTER2_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30609_5"))
			htmltext = "30609-08.htm";
		else if(event.equals("30609_6"))
		{
			htmltext = "30609-09.htm";
			st.giveItems(CRETAS_PAINTING1_ID, 1);
			st.takeItems(LUCILLAS_HANDBAG_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30609_7"))
			htmltext = "30609-13.htm";
		else if(event.equals("30609_8"))
		{
			htmltext = "30609-14.htm";
			st.giveItems(CRETAS_LETTER2_ID, 1);
			st.takeItems(DIETERS_KEY_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30610_1"))
			htmltext = "30610-02.htm";
		else if(event.equals("30610_2"))
			htmltext = "30610-03.htm";
		else if(event.equals("30610_3"))
			htmltext = "30610-04.htm";
		else if(event.equals("30610_4"))
			htmltext = "30610-05.htm";
		else if(event.equals("30610_5"))
			htmltext = "30610-06.htm";
		else if(event.equals("30610_6"))
			htmltext = "30610-07.htm";
		else if(event.equals("30610_7"))
			htmltext = "30610-08.htm";
		else if(event.equals("30610_8"))
			htmltext = "30610-09.htm";
		else if(event.equals("30610_9"))
		{
			htmltext = "30610-10.htm";
			st.giveItems(CRONOS_SIGIL_ID, 1);
			st.giveItems(CRONOS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30610_10"))
			htmltext = "30610-13.htm";
		else if(event.equals("30610_11"))
		{
			htmltext = "30610-14.htm";
			st.takeItems(SCRIPTURE_CHAPTER_1_ID, 1);
			st.takeItems(SCRIPTURE_CHAPTER_2_ID, 1);
			st.takeItems(SCRIPTURE_CHAPTER_3_ID, 1);
			st.takeItems(SCRIPTURE_CHAPTER_4_ID, 1);
			st.takeItems(CRONOS_SIGIL_ID, 1);
			st.takeItems(TRIFFS_RING_ID, 1);
			st.giveItems(SYMBOL_OF_CRONOS_ID, 1);
			st.takeItems(DIETERS_DIARY_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30111_1"))
			htmltext = "30111-02.htm";
		else if(event.equals("30111_2"))
			htmltext = "30111-03.htm";
		else if(event.equals("30111_3"))
			htmltext = "30111-04.htm";
		else if(event.equals("30111_4"))
		{
			htmltext = "30111-05.htm";
			st.giveItems(DIETERS_KEY_ID, 1);
			st.takeItems(CRONOS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30111_5"))
			htmltext = "30111-08.htm";
		else if(event.equals("30111_6"))
		{
			htmltext = "30111-09.htm";
			st.giveItems(DIETERS_LETTER_ID, 1);
			st.takeItems(CRETAS_LETTER2_ID, 1);
			st.giveItems(DIETERS_DIARY_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30230_1"))
		{
			htmltext = "30230-02.htm";
			st.giveItems(RAUTS_LETTER_ENVELOPE_ID, 1);
			st.takeItems(DIETERS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30316_1"))
		{
			htmltext = "30316-02.htm";
			st.giveItems(SCRIPTURE_CHAPTER_1_ID, 1);
			st.takeItems(RAUTS_LETTER_ENVELOPE_ID, 1);
			st.giveItems(STRONG_LIQUOR_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30611_1"))
			htmltext = "30611-02.htm";
		else if(event.equals("30611_2"))
			htmltext = "30611-03.htm";
		else if(event.equals("30611_3"))
		{
			htmltext = "30611-04.htm";
			st.giveItems(TRIFFS_RING_ID, 1);
			st.takeItems(STRONG_LIQUOR_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30103_1"))
			htmltext = "30103-02.htm";
		else if(event.equals("30103_2"))
			htmltext = "30103-03.htm";
		else if(event.equals("30103_3"))
		{
			htmltext = "30103-04.htm";
			st.giveItems(VALKONS_REQUEST_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30612_1"))
			htmltext = "30612-03.htm";
		else if(event.equals("30612_2"))
		{
			htmltext = "30612-04.htm";
			st.giveItems(CASIANS_LIST_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30612_3"))
		{
			htmltext = "30612-07.htm";
			st.giveItems(SCRIPTURE_CHAPTER_4_ID, 1);
			st.takeItems(CASIANS_LIST_ID, 1);
			st.takeItems(GHOULS_SKIN_ID, st.getQuestItemsCount(GHOULS_SKIN_ID));
			st.takeItems(MEDUSAS_BLOOD_ID, st.getQuestItemsCount(MEDUSAS_BLOOD_ID));
			st.takeItems(FETTEREDSOULS_ICHOR_ID, st.getQuestItemsCount(FETTEREDSOULS_ICHOR_ID));
			st.takeItems(ENCHT_GARGOYLES_NAIL_ID, st.getQuestItemsCount(ENCHT_GARGOYLES_NAIL_ID));
			st.takeItems(POITANS_NOTES_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		if(st.getQuestItemsCount(MARK_OF_SCHOLAR_ID) > 0)
		{
			st.exitCurrentQuest(true);
			return "completed";
		}
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
			st.set("id", "0");
		}
		if(npcId == 30461 && st.getInt("cond") == 0)
		{
			if(st.getPlayer().getClassId().getId() == 0x0b || st.getPlayer().getClassId().getId() == 0x1a || st.getPlayer().getClassId().getId() == 0x27)
			{
				if(st.getPlayer().getLevel() >= 35)
					htmltext = "30461-03.htm";
				else
				{
					htmltext = "30461-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
			{
				htmltext = "30461-01.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) == 1 && st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) == 0)
			htmltext = "30461-05.htm";
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) == 1 && st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) == 1)
		{
			htmltext = "30461-06.htm";
			st.takeItems(SYMBOL_OF_SYLVAIN_ID, 1);
			st.giveItems(MIRIENS_SIGIL2_ID, 1);
			st.takeItems(MIRIENS_SIGIL1_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) == 1 && st.getQuestItemsCount(SYMBOL_OF_JUREK_ID) == 0)
			htmltext = "30461-07.htm";
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) == 1 && st.getQuestItemsCount(SYMBOL_OF_JUREK_ID) == 1)
			htmltext = "30461-08.htm";
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_INSTRUCTION_ID) == 1)
		{
			if(st.getPlayer().getLevel() < 36)
				htmltext = "30461-11.htm";
			else
			{
				htmltext = "30461-12.htm";
				st.giveItems(MIRIENS_SIGIL3_ID, 1);
				st.takeItems(MIRIENS_INSTRUCTION_ID, 1);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 30461 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1)
		{
			if(st.getQuestItemsCount(SYMBOL_OF_CRONOS_ID) == 0)
				htmltext = "30461-13.htm";
			else
			{
				htmltext = "30461-14.htm";
				st.takeItems(SYMBOL_OF_CRONOS_ID, -1);
				st.takeItems(MIRIENS_SIGIL3_ID, -1);
				st.giveItems(MARK_OF_SCHOLAR_ID, 1);
				if(!st.getPlayer().getVarB("prof2.1"))
				{
					st.addExpAndSp(876963, 56877, true);
					st.giveItems(ADENA_ID, 159814, Config.RATE_QUESTS_OCCUPATION_CHANGE);
					st.getPlayer().setVar("prof2.1", "1");
				}
				st.playSound(SOUND_FINISH);
				st.unset("cond");
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == 30070 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) == 1 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) == 0 && st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) == 0)
			htmltext = "30070-01.htm";
		else if(npcId == 30070 && st.getInt("cond") == 1 && st.getQuestItemsCount(CRYSTAL_OF_PURITY1_ID) == 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0)
			htmltext = "30070-03.htm";
		else if(npcId == 30070 && st.getInt("cond") == 1 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(CRYSTAL_OF_PURITY1_ID) > 0)
		{
			htmltext = "30070-04.htm";
			st.giveItems(SYMBOL_OF_SYLVAIN_ID, 1);
			st.takeItems(HIGHPRIESTS_SIGIL_ID, 1);
			st.takeItems(CRYSTAL_OF_PURITY1_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30070 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) == 0)
			htmltext = "30070-05.htm";
		else if(npcId == 30070 && st.getInt("cond") == 1 && (st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0))
			htmltext = "30070-06.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(SYLVAINS_LETTER_ID) > 0)
			htmltext = "30608-01.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MARIAS_LETTER1_ID) > 0)
			htmltext = "30608-03.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(LUKAS_LETTER_ID) > 0)
		{
			htmltext = "30608-04.htm";
			st.giveItems(MARIAS_LETTER2_ID, 1);
			st.takeItems(LUKAS_LETTER_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MARIAS_LETTER2_ID) > 0)
			htmltext = "30608-05.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_LETTER1_ID) > 0)
			htmltext = "30608-06.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(LUCILLAS_HANDBAG_ID) > 0)
			htmltext = "30608-09.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING1_ID) > 0)
		{
			htmltext = "30608-10.htm";
			st.giveItems(CRETAS_PAINTING2_ID, 1);
			st.takeItems(CRETAS_PAINTING1_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING2_ID) > 0)
			htmltext = "30608-11.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING3_ID) > 0 && st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID) < 5)
			htmltext = "30608-12.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING3_ID) > 0 && st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID) >= 5)
			htmltext = "30608-13.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRYSTAL_OF_PURITY1_ID) > 0)
			htmltext = "30608-15.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && (st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0))
			htmltext = "30608-16.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 0)
			htmltext = "30608-17.htm";
		else if(npcId == 30608 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 1)
		{
			htmltext = "30608-18.htm";
			st.giveItems(CRYSTAL_OF_PURITY2_ID, 1);
			st.takeItems(VALKONS_REQUEST_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30115 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) == 1 && st.getQuestItemsCount(GMAGISTERS_SIGIL_ID) == 0 && st.getQuestItemsCount(SYMBOL_OF_JUREK_ID) == 0)
			htmltext = "30115-01.htm";
		else if(npcId == 30115 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) == 1 && st.getQuestItemsCount(JUREKS_LIST_ID) == 1)
		{
			if(st.getQuestItemsCount(MEYEDESTROYERS_SKIN_ID) + st.getQuestItemsCount(SHAMANS_NECKLACE_ID) + st.getQuestItemsCount(SHACKLES_SCALP_ID) < 12)
				htmltext = "30115-04.htm";
			else
			{
				htmltext = "30115-05.htm";
				st.takeItems(JUREKS_LIST_ID, 1);
				st.takeItems(MEYEDESTROYERS_SKIN_ID, st.getQuestItemsCount(MEYEDESTROYERS_SKIN_ID));
				st.takeItems(SHAMANS_NECKLACE_ID, st.getQuestItemsCount(SHAMANS_NECKLACE_ID));
				st.takeItems(SHACKLES_SCALP_ID, st.getQuestItemsCount(SHACKLES_SCALP_ID));
				st.giveItems(SYMBOL_OF_JUREK_ID, 1);
				st.takeItems(GMAGISTERS_SIGIL_ID, 1);
				st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 30115 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 && st.getQuestItemsCount(SYMBOL_OF_JUREK_ID) > 0 && st.getQuestItemsCount(GMAGISTERS_SIGIL_ID) == 0)
			htmltext = "30115-06.htm";
		else if(npcId == 30115 && st.getInt("cond") == 1 && (st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0))
			htmltext = "30115-07.htm";
		else if(npcId == 30071 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MARIAS_LETTER1_ID) > 0)
		{
			htmltext = "30071-01.htm";
			st.giveItems(LUKAS_LETTER_ID, 1);
			st.takeItems(MARIAS_LETTER1_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30071 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && (st.getQuestItemsCount(MARIAS_LETTER2_ID) > 0 || st.getQuestItemsCount(CRETAS_LETTER1_ID) > 0 || st.getQuestItemsCount(LUCILLAS_HANDBAG_ID) > 0 || st.getQuestItemsCount(CRETAS_PAINTING1_ID) > 0 || st.getQuestItemsCount(LUKAS_LETTER_ID) > 0))
			htmltext = "30071-02.htm";
		else if(npcId == 30071 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING2_ID) > 0)
			htmltext = "30071-03.htm";
		else if(npcId == 30071 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING3_ID) > 0)
		{
			if(st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID) < 5)
				htmltext = "30071-05.htm";
			else
				htmltext = "30071-06.htm";
		}
		else if(npcId == 30071 && st.getInt("cond") == 1 && (st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 || st.getQuestItemsCount(CRYSTAL_OF_PURITY1_ID) > 0))
			htmltext = "30071-07.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(MARIAS_LETTER2_ID) > 0)
			htmltext = "30609-01.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_LETTER1_ID) > 0)
			htmltext = "30609-06.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(LUCILLAS_HANDBAG_ID) > 0)
			htmltext = "30609-07.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && (st.getQuestItemsCount(CRETAS_PAINTING1_ID) > 0 || st.getQuestItemsCount(CRETAS_PAINTING2_ID) > 0 || st.getQuestItemsCount(CRETAS_PAINTING3_ID) > 0))
			htmltext = "30609-10.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && (st.getQuestItemsCount(CRYSTAL_OF_PURITY1_ID) > 0 || st.getQuestItemsCount(SYMBOL_OF_SYLVAIN_ID) > 0 || st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0))
			htmltext = "30609-11.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(DIETERS_KEY_ID) > 0)
			htmltext = "30609-12.htm";
		else if(npcId == 30609 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1 && st.getQuestItemsCount(DIETERS_KEY_ID) == 0)
			htmltext = "30609-15.htm";
		else if(npcId == 30610 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1 && st.getQuestItemsCount(CRONOS_SIGIL_ID) == 0 && st.getQuestItemsCount(SYMBOL_OF_CRONOS_ID) == 0)
			htmltext = "30610-01.htm";
		else if(npcId == 30610 && st.getInt("cond") == 1 && (st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 || st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0))
		{
			if(st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_3_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) > 0)
				htmltext = "30610-12.htm";
			else
				htmltext = "30610-11.htm";
		}
		else if(npcId == 30610 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) == 1 && st.getQuestItemsCount(SYMBOL_OF_CRONOS_ID) == 1 && st.getQuestItemsCount(CRONOS_SIGIL_ID) == 0)
			htmltext = "30610-15.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRONOS_LETTER_ID) > 0)
			htmltext = "30111-01.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(DIETERS_KEY_ID) > 0)
			htmltext = "30111-06.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_LETTER2_ID) > 0)
			htmltext = "30111-07.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(DIETERS_LETTER_ID) > 0)
			htmltext = "30111-10.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(RAUTS_LETTER_ENVELOPE_ID) > 0)
			htmltext = "30111-11.htm";
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(DIETERS_LETTER_ID) == 0 && st.getQuestItemsCount(RAUTS_LETTER_ENVELOPE_ID) == 0)
		{
			if(st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_3_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) > 0)
				htmltext = "30111-13.htm";
			else
				htmltext = "30111-12.htm";
		}
		else if(npcId == 30111 && st.getInt("cond") == 1 && st.getQuestItemsCount(SYMBOL_OF_CRONOS_ID) == 1)
			htmltext = "30111-15.htm";
		else if(npcId == 30230 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(DIETERS_LETTER_ID) > 0)
			htmltext = "30230-01.htm";
		else if(npcId == 30230 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(RAUTS_LETTER_ENVELOPE_ID) > 0)
			htmltext = "30230-03.htm";
		else if(npcId == 30230 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) == 1 && (st.getQuestItemsCount(STRONG_LIQUOR_ID) > 0 || st.getQuestItemsCount(TRIFFS_RING_ID) > 0))
			htmltext = "30230-04.htm";
		else if(npcId == 30316 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(RAUTS_LETTER_ENVELOPE_ID) > 0)
			htmltext = "30316-01.htm";
		else if(npcId == 30316 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(STRONG_LIQUOR_ID) > 0)
			htmltext = "30316-04.htm";
		else if(npcId == 30316 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0)
			htmltext = "30316-05.htm";
		else if(npcId == 30611 && st.getInt("cond") == 1 && st.getQuestItemsCount(DIETERS_DIARY_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(STRONG_LIQUOR_ID) > 0)
			htmltext = "30611-01.htm";
		else if(npcId == 30611 && st.getInt("cond") == 1 && (st.getQuestItemsCount(TRIFFS_RING_ID) > 0 || st.getQuestItemsCount(SYMBOL_OF_CRONOS_ID) > 0))
			htmltext = "30611-05.htm";
		else if(npcId == 30103 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 0 && st.getQuestItemsCount(CRYSTAL_OF_PURITY2_ID) == 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) == 0)
			htmltext = "30103-01.htm";
		else if(npcId == 30103 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 1 && st.getQuestItemsCount(CRYSTAL_OF_PURITY2_ID) == 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) == 0)
			htmltext = "30103-05.htm";
		else if(npcId == 30103 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 0 && st.getQuestItemsCount(CRYSTAL_OF_PURITY2_ID) == 1 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) == 0)
		{
			htmltext = "30103-06.htm";
			st.giveItems(SCRIPTURE_CHAPTER_2_ID, 1);
			st.takeItems(CRYSTAL_OF_PURITY2_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30103 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(VALKONS_REQUEST_ID) == 0 && st.getQuestItemsCount(CRYSTAL_OF_PURITY2_ID) == 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) == 1)
			htmltext = "30103-07.htm";
		else if(npcId == 30458 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(POITANS_NOTES_ID) == 0 && st.getQuestItemsCount(CASIANS_LIST_ID) == 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) == 0)
		{
			htmltext = "30458-01.htm";
			st.giveItems(POITANS_NOTES_ID, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(npcId == 30458 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) == 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) == 0)
			htmltext = "30458-02.htm";
		else if(npcId == 30458 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) == 0)
			htmltext = "30458-03.htm";
		else if(npcId == 30458 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) == 0 && st.getQuestItemsCount(CASIANS_LIST_ID) == 0)
			htmltext = "30458-04.htm";
		else if(npcId == 30612 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) == 0)
		{
			if(st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_3_ID) > 0)
				htmltext = "30612-02.htm";
			else
				htmltext = "30612-01.htm";
		}
		else if(npcId == 30612 && st.getInt("cond") == 1 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0)
		{
			if(st.getQuestItemsCount(GHOULS_SKIN_ID) + st.getQuestItemsCount(MEDUSAS_BLOOD_ID) + st.getQuestItemsCount(FETTEREDSOULS_ICHOR_ID) + st.getQuestItemsCount(ENCHT_GARGOYLES_NAIL_ID) < 32)
				htmltext = "30612-05.htm";
			else
				htmltext = "30612-06.htm";
		}
		else if(npcId == 30612 && st.getInt("cond") == 1 && st.getQuestItemsCount(POITANS_NOTES_ID) == 0 && st.getQuestItemsCount(CASIANS_LIST_ID) == 0 && st.getQuestItemsCount(TRIFFS_RING_ID) == 1 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_1_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_2_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_3_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_4_ID) > 0)
			htmltext = "30612-08.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == 20580)
		{
			if(st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL1_ID) > 0 && st.getQuestItemsCount(HIGHPRIESTS_SIGIL_ID) > 0 && st.getQuestItemsCount(CRETAS_PAINTING3_ID) > 0 && st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID) < 5)
				if(Rnd.chance(50))
				{
					st.giveItems(BROWN_SCROLL_SCRAP_ID, 1);

					if(st.getQuestItemsCount(BROWN_SCROLL_SCRAP_ID) < 5)
						st.playSound(SOUND_ITEMGET);
					else
						st.playSound(SOUND_MIDDLE);
				}
		}
		else if(npcId == 20068)
		{
			if(st.getInt("cond") == 1 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 && st.getQuestItemsCount(GMAGISTERS_SIGIL_ID) > 0 && st.getQuestItemsCount(JUREKS_LIST_ID) > 0 && st.getQuestItemsCount(MEYEDESTROYERS_SKIN_ID) < 5)
				if(Rnd.chance(50))
				{
					st.giveItems(MEYEDESTROYERS_SKIN_ID, 1);

					if(st.getQuestItemsCount(MEYEDESTROYERS_SKIN_ID) < 5)
						st.playSound(SOUND_ITEMGET);
					else
						st.playSound(SOUND_MIDDLE);
				}
		}
		else if(npcId == 20269)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 && st.getQuestItemsCount(GMAGISTERS_SIGIL_ID) > 0 && st.getQuestItemsCount(JUREKS_LIST_ID) > 0 && st.getQuestItemsCount(SHAMANS_NECKLACE_ID) < 5)
				if(Rnd.chance(50))
				{
					st.giveItems(SHAMANS_NECKLACE_ID, 1);

					if(st.getQuestItemsCount(SHAMANS_NECKLACE_ID) < 5)
						st.playSound(SOUND_ITEMGET);
					else
						st.playSound(SOUND_MIDDLE);
				}
		}
		else if(npcId == 20235)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(MIRIENS_SIGIL2_ID) > 0 && st.getQuestItemsCount(GMAGISTERS_SIGIL_ID) > 0 && st.getQuestItemsCount(JUREKS_LIST_ID) > 0 && st.getQuestItemsCount(SHACKLES_SCALP_ID) < 2)
			{
				st.giveItems(SHACKLES_SCALP_ID, 1);

				if(st.getQuestItemsCount(SHACKLES_SCALP_ID) < 2)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 20554)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(MIRIENS_SIGIL3_ID) > 0 && st.getQuestItemsCount(CRONOS_SIGIL_ID) > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(SCRIPTURE_CHAPTER_3_ID) == 0)
				if(Rnd.chance(30))
				{
					st.giveItems(SCRIPTURE_CHAPTER_3_ID, 1);
					st.playSound(SOUND_MIDDLE);
				}
		}
		else if(npcId == 20201)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0 && st.getQuestItemsCount(GHOULS_SKIN_ID) < 10)
			{
				st.giveItems(GHOULS_SKIN_ID, 1);

				if(st.getQuestItemsCount(GHOULS_SKIN_ID) < 10)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 20158)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0 && st.getQuestItemsCount(MEDUSAS_BLOOD_ID) < 12)
			{
				st.giveItems(MEDUSAS_BLOOD_ID, 1);

				if(st.getQuestItemsCount(MEDUSAS_BLOOD_ID) < 12)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 20552)
		{
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0 && st.getQuestItemsCount(FETTEREDSOULS_ICHOR_ID) < 5)
			{
				st.giveItems(FETTEREDSOULS_ICHOR_ID, 1);

				if(st.getQuestItemsCount(FETTEREDSOULS_ICHOR_ID) < 5)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == 20567)
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(TRIFFS_RING_ID) > 0 && st.getQuestItemsCount(POITANS_NOTES_ID) > 0 && st.getQuestItemsCount(CASIANS_LIST_ID) > 0 && st.getQuestItemsCount(ENCHT_GARGOYLES_NAIL_ID) < 5)
			{
				st.giveItems(ENCHT_GARGOYLES_NAIL_ID, 1);

				if(st.getQuestItemsCount(ENCHT_GARGOYLES_NAIL_ID) < 5)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}

		return null;
	}
}
