package quests._733_ProtectTheEconomicLeader;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _733_ProtectTheEconomicLeader extends TerritoryWarSuperClass
{
	public _733_ProtectTheEconomicLeader()
	{
		super(733);
		_npcIDs = new int[] { 36513, 36519, 36525, 36531, 36537, 36543, 36549, 36555, 36561 };
		registerAttackIds();
	}

	@Override
	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return 1 + (npcid - 36513) / 6;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
