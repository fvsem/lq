package quests._038_DragonFangs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _038_DragonFangs extends Quest implements ScriptFile
{
	// NPC
	public static final int ROHMER = 30344;
	public static final int LUIS = 30386;
	public static final int IRIS = 30034;
	// QUEST ITEM
	public static final int FEATHER_ORNAMENT = 7173;
	public static final int TOOTH_OF_TOTEM = 7174;
	public static final int LETTER_OF_IRIS = 7176;
	public static final int LETTER_OF_ROHMER = 7177;
	public static final int TOOTH_OF_DRAGON = 7175;
	// MOBS
	public static final int LANGK_LIZARDMAN_LIEUTENANT = 20357;
	public static final int LANGK_LIZARDMAN_SENTINEL = 21100;
	public static final int LANGK_LIZARDMAN_LEADER = 20356;
	public static final int LANGK_LIZARDMAN_SHAMAN = 21101;
	// CHANCE FOR DROP
	public static final int CHANCE_FOR_QUEST_ITEMS = 100; // 100%???
	// REWARD
	public static final int BONE_HELMET = 45;
	public static final int ASSAULT_BOOTS = 1125;
	public static final int BLUE_BUCKSKIN_BOOTS = 1123;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _038_DragonFangs()
	{
		super(38, -1);

		addStartNpc(LUIS);

		addTalkId(IRIS, ROHMER);
		addKillId(LANGK_LIZARDMAN_LEADER, LANGK_LIZARDMAN_SHAMAN, LANGK_LIZARDMAN_SENTINEL, LANGK_LIZARDMAN_LIEUTENANT);
		addQuestItem(TOOTH_OF_TOTEM, LETTER_OF_IRIS, LETTER_OF_ROHMER, TOOTH_OF_DRAGON, FEATHER_ORNAMENT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getInt("cond");
		if(event.equals("30386-3.htm"))
			if(cond == 0)
			{
				st.setState(STARTED);
				st.set("cond", "1");
				st.playSound(SOUND_ACCEPT);
			}
		if(event.equals("30386-5.htm"))
			if(cond == 2)
			{
				st.set("cond", "3");
				st.takeItems(FEATHER_ORNAMENT, 100);
				st.giveItems(TOOTH_OF_TOTEM, 1);
				st.playSound(SOUND_MIDDLE);
			}
		if(event.equals("30034-2.htm"))
			if(cond == 3)
			{
				st.set("cond", "4");
				st.takeItems(TOOTH_OF_TOTEM, 1);
				st.giveItems(LETTER_OF_IRIS, 1);
				st.playSound(SOUND_MIDDLE);
			}
		if(event.equals("30344-2.htm"))
			if(cond == 4)
			{
				st.set("cond", "5");
				st.takeItems(LETTER_OF_IRIS, 1);
				st.giveItems(LETTER_OF_ROHMER, 1);
				st.playSound(SOUND_MIDDLE);
			}
		if(event.equals("30034-6.htm"))
			if(cond == 5)
			{
				st.set("cond", "6");
				st.takeItems(LETTER_OF_ROHMER, 1);
				st.playSound(SOUND_MIDDLE);
			}
		if(event.equals("30034-9.htm"))
			if(cond == 7)
			{
				st.set("cond", "0");
				st.takeItems(TOOTH_OF_DRAGON, 50);
				int luck = Rnd.get(3);
				if(luck == 0)
				{
					st.giveItems(BLUE_BUCKSKIN_BOOTS, 1);
					st.giveItems(ADENA_ID, 1500);
				}
				if(luck == 1)
				{
					st.giveItems(BONE_HELMET, 1);
					st.giveItems(ADENA_ID, 5200);
				}
				if(luck == 2)
				{
					st.giveItems(ASSAULT_BOOTS, 1);
					st.giveItems(ADENA_ID, 1500);
				}
				st.addExpAndSp(435117, 23977);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == LUIS && cond == 0)
			if(st.getPlayer().getLevel() < 19)
			{
				htmltext = "30386-2.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() >= 19)
				htmltext = "30386-1.htm";
		if(npcId == LUIS && cond == 1)
			htmltext = "30386-6.htm";
		if(npcId == LUIS && cond == 2 && st.getQuestItemsCount(FEATHER_ORNAMENT) == 100)
			htmltext = "30386-4.htm";
		if(npcId == LUIS && cond == 3)
			htmltext = "30386-7.htm";
		if(npcId == IRIS && cond == 3 && st.getQuestItemsCount(TOOTH_OF_TOTEM) == 1)
			htmltext = "30034-1.htm";
		if(npcId == IRIS && cond == 4)
			htmltext = "30034-3.htm";
		if(npcId == IRIS && cond == 5 && st.getQuestItemsCount(LETTER_OF_ROHMER) == 1)
			htmltext = "30034-5.htm";
		if(npcId == IRIS && cond == 6)
			htmltext = "30034-7.htm";
		if(npcId == IRIS && cond == 7 && st.getQuestItemsCount(TOOTH_OF_DRAGON) == 50)
			htmltext = "30034-8.htm";
		if(npcId == ROHMER && cond == 4 && st.getQuestItemsCount(LETTER_OF_IRIS) == 1)
			htmltext = "30344-1.htm";
		if(npcId == ROHMER && cond == 5)
			htmltext = "30344-3.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		boolean chance = Rnd.chance(CHANCE_FOR_QUEST_ITEMS);
		int cond = st.getInt("cond");
		if(npcId == 20357 || npcId == 21100)
			if(cond == 1 && chance && st.getQuestItemsCount(FEATHER_ORNAMENT) < 100)
			{
				st.giveItems(FEATHER_ORNAMENT, 1);
				if(st.getQuestItemsCount(FEATHER_ORNAMENT) == 100)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		if(npcId == 20356 || npcId == 21101)
			if(cond == 6 && chance && st.getQuestItemsCount(TOOTH_OF_DRAGON) < 50)
			{
				st.giveItems(TOOTH_OF_DRAGON, 1);
				if(st.getQuestItemsCount(TOOTH_OF_DRAGON) == 50)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "7");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
