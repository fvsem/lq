package quests._020_BringUpWithLove;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _020_BringUpWithLove extends Quest implements ScriptFile
{
	int TUNATUN = 31537;
	int BUFFALO = 21470;
	int KOOKABURRA = 21451;
	int COUGAR = 21489;
	int JEWEL_INNOCENCE = 7185;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _020_BringUpWithLove()
	{
		super(20, -1);

		addStartNpc(TUNATUN);

		addTalkId(TUNATUN);

		addKillId(BUFFALO);
		addKillId(KOOKABURRA);
		addKillId(COUGAR);

		addQuestItem(JEWEL_INNOCENCE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int id = st.getState();
		if(id == CREATED)
		{
			st.set("cond", "0");
			if(event.equals("31537-1.htm") || event.equals("31537-2.htm") || event.equals("31537-3.htm") || event.equals("31537-4.htm") || event.equals("31537-5.htm"))
				return event;
			if(event.equals("31537-6.htm"))
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
				return event;
			}
		}
		else if(event.equals("31537-8.htm"))
		{
			st.takeItems(JEWEL_INNOCENCE, -1);
			st.giveItems(57, 68500);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		int cond = st.getInt("cond");

		if(cond == 0)
		{
			if(id == CREATED)
				if(st.getPlayer().getLevel() <= 64)
				{
					st.exitCurrentQuest(true);
					htmltext = "31537-00.htm";
				}
				else
					htmltext = "31537-0.htm";
		}
		else if(cond == 1 && st.getQuestItemsCount(JEWEL_INNOCENCE) == 0)
			htmltext = "31537-6a.htm";
		else if(cond == 2 && st.getQuestItemsCount(JEWEL_INNOCENCE) >= 1)
			htmltext = "31537-7.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();

		if(st.getQuestItemsCount(JEWEL_INNOCENCE) == 0)
			if(npcId == BUFFALO || npcId == KOOKABURRA || npcId == COUGAR)
				if(Rnd.chance(20))
				{
					st.giveItems(JEWEL_INNOCENCE, 1);
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
		return null;
	}
}
