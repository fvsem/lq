package quests._111_ElrokianHuntersProof;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _111_ElrokianHuntersProof extends Quest implements ScriptFile
{
	// NPC
	private final static int MARQUEZ = 32113;
	private final static int MUSHIKA = 32114;
	private final static int ASAMAH = 32115;
	private final static int KIRIKACHIN = 32116;

	// MONSTER
	private final static int VELOCIRAPTOR = 22196;
	private final static int VELOCIRAPTOR1 = 22198;
	private final static int VELOCIRAPTOR2 = 22118;
	private final static int VELOCIRAPTOR3 = 22223;
	private final static int ORNITHOMIMUS = 22200;
	private final static int ORNITHOMIMUS1 = 22202;
	private final static int ORNITHOMIMUS2 = 22219;
	private final static int ORNITHOMIMUS3 = 22224;
	private final static int DEINONYCHUS = 22203;
	private final static int DEINONYCHUS1 = 22205;
	private final static int DEINONYCHUS2 = 22220;
	private final static int DEINONYCHUS3 = 22225;
	private final static int PACHYCAPHALOSAURUS = 22208;
	private final static int PACHYCAPHALOSAURUS1 = 22210;
	private final static int PACHYCAPHALOSAURUS2 = 22221;
	private final static int PACHYCAPHALOSAURUS3 = 22226;
	// item
	private final static int DIARY_FRAGMENT = 8768;
	private final static int ORNITHOMIMUS_CLAW = 8770;
	private final static int DEINONYCHUS_BONE = 8771;
	private final static int PACHYCAPHALOSAURUS_SKIN = 8772;
	private final static int ADENA = 57;
	private final static int TRAP_STONE = 8764;
	private final static int PRACTICE_ELROKIAN_TRAP = 8773;
	private final static int ELROKIAN_TRAP = 8763;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _111_ElrokianHuntersProof()
	{
		super(111, -1);

		addStartNpc(MARQUEZ);
		addTalkId(MARQUEZ);
		addTalkId(MUSHIKA);
		addTalkId(ASAMAH);
		addTalkId(KIRIKACHIN);
		addKillId(VELOCIRAPTOR);
		addKillId(ORNITHOMIMUS);
		addKillId(DEINONYCHUS);
		addKillId(PACHYCAPHALOSAURUS);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("ok.htm"))
		{
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.set("cond", "1");
		}
		else if(event.equals("nauchi.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "2");
		}
		else if(event.equals("nauchiasamah.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "3");
		}
		else if(event.equals("diarysoberi.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "4");
		}
		else if(event.equals("kirikachin.htm"))
		{
			st.takeItems(ORNITHOMIMUS_CLAW, -1);
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "6");
		}
		else if(event.equals("bloknotprines.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "7");
		}
		else if(event.equals("pesnyaok.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "8");
		}
		else if(event.equals("pesnyapoem.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "9");
		}
		else if(event.equals("lovdino.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "10");
		}
		else if(event.equals("lovuwka.htm"))
		{
			st.playSound(SOUND_ITEMGET);
			st.set("cond", "12");
		}
		else if(event.equals("lovuwkaok.htm"))
		{
			st.playSound("SOUND_FINISH");
			st.giveItems(ADENA, 1022636);
			st.giveItems(ELROKIAN_TRAP, 1);
			st.giveItems(TRAP_STONE, 100);
			st.unset("cond");
			st.setState(COMPLETED);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		// if(st.getQuestItemsCount(DIARY_FRAGMENT) >= 1) // WTF empty statement?
		// ;
		if(npcId == MARQUEZ)
		{
			if(id == CREATED)
			{
				if(st.getPlayer().getLevel() >= 75)
				{
					htmltext = "privetstvie.htm";
					st.exitCurrentQuest(true);
				}
				else
				{
					htmltext = "lvl.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "idikmushika.htm";
			else if(cond == 3)
				htmltext = "druzba.htm";
			else if(cond == 5)
			{
				htmltext = "bloknot.htm";
				st.takeItems(DIARY_FRAGMENT, -1);
			}
		}
		else if(npcId == MUSHIKA)
		{
			if(cond == 1)
				htmltext = "privmushika.htm";
		}
		else if(npcId == ASAMAH)
		{
			if(cond == 2)
				htmltext = "privasamah.htm";
			if(cond == 8)
				htmltext = "pesnyaokkk.htm";
			if(cond == 9)
				htmltext = "drug.htm";
			if(cond == 10)
				htmltext = "matersoberi.htm";
			if(cond == 11)
			{
				htmltext = "materprines.htm";
				st.takeItems(ORNITHOMIMUS_CLAW, -1);
				st.takeItems(DEINONYCHUS_BONE, -1);
				st.takeItems(PACHYCAPHALOSAURUS_SKIN, -1);
				st.giveItems(PRACTICE_ELROKIAN_TRAP, 3);
			}
		}
		else if(npcId == KIRIKACHIN)
		{
			if(cond == 6)
				htmltext = "privkirikachin.htm";
			if(cond == 7)
				htmltext = "pesnya.htm";
			if(cond == 12)
				htmltext = "lovuwkaa.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(st.getInt("cond") == 4)
			if(npcId == VELOCIRAPTOR || npcId == VELOCIRAPTOR1 || npcId == VELOCIRAPTOR2 || npcId == VELOCIRAPTOR3 && Rnd.chance(10 * Config.RATE_QUESTS_DROP))
				if(st.getQuestItemsCount(DIARY_FRAGMENT) == 50)
				{
					st.set("cond", "5");
					st.playSound(SOUND_MIDDLE);
				}
				else
				{
					st.playSound(SOUND_ITEMGET);
					st.giveItems(DIARY_FRAGMENT, 1);
				}
		if(st.getInt("cond") == 10)
		{
			if(npcId == ORNITHOMIMUS || npcId == ORNITHOMIMUS1 || npcId == ORNITHOMIMUS2 || npcId == ORNITHOMIMUS3 && Rnd.chance(10 * Config.RATE_QUESTS_DROP))
				if(st.getQuestItemsCount(ORNITHOMIMUS_CLAW) < 10)
				{
					st.giveItems(ORNITHOMIMUS_CLAW, 1);
					st.playSound(SOUND_ITEMGET);
				}
			if(npcId == DEINONYCHUS || npcId == DEINONYCHUS1 || npcId == DEINONYCHUS2 || npcId == DEINONYCHUS3 && Rnd.chance(10 * Config.RATE_QUESTS_DROP))
				if(st.getQuestItemsCount(DEINONYCHUS_BONE) < 10)
				{
					st.giveItems(DEINONYCHUS_BONE, 1);
					st.playSound(SOUND_ITEMGET);
				}
			if(npcId == PACHYCAPHALOSAURUS || npcId == PACHYCAPHALOSAURUS1 || npcId == PACHYCAPHALOSAURUS2 || npcId == PACHYCAPHALOSAURUS3 && Rnd.chance(10 * Config.RATE_QUESTS_DROP))
				if(st.getQuestItemsCount(PACHYCAPHALOSAURUS_SKIN) < 10)
				{
					st.giveItems(PACHYCAPHALOSAURUS_SKIN, 1);
					st.playSound(SOUND_ITEMGET);
				}
			{
				if(st.getQuestItemsCount(ORNITHOMIMUS_CLAW) == 10 && st.getQuestItemsCount(DEINONYCHUS_BONE) == 10 && st.getQuestItemsCount(PACHYCAPHALOSAURUS_SKIN) == 10)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "11");
				}

			}
		}
		return null;
	}
}
