package quests._174_SupplyCheck;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * @author Felixx
 */
public class _174_SupplyCheck extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private final static int Marcela = 32173;
	private final static int Benis = 32170;
	private final static int Nika = 32167;

	private final static int WarehouseManifest = 9792;
	private final static int GroceryStoreManifest = 9793;

	private final static int WoodenBreastplate = 23;
	private final static int WoodenGaiters = 2386;
	private final static int LeatherTunic = 429;
	private final static int LeatherStockings = 464;
	private final static int WoodenHelmet = 43;
	private final static int LeatherShoes = 37;
	private final static int Gloves = 49;

	public _174_SupplyCheck()
	{
		super(174, -1);

		addStartNpc(Marcela);
		addTalkId(Marcela);
		addTalkId(Benis);
		addTalkId(Nika);
	}

	@Override
	public String onEvent(String event, QuestState qs, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32173-03.htm"))
		{
			qs.set("cond", "1");
			qs.setState(STARTED);
			qs.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");

		if(npcId == Marcela)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() == 1)
				{
					st.exitCurrentQuest(true);
					htmltext = "32173-01.htm";
				}
				else
					htmltext = "32173-02.htm";
			}
			else if(cond == 2)
			{
				st.set("cond", "3");
				st.takeItems(WarehouseManifest, -1);
				htmltext = "32173-04.htm";
			}
			else if(cond == 4)
			{
				st.takeItems(GroceryStoreManifest, -1);
				if(st.getPlayer().getClassId().isMage())
				{
					st.giveItems(LeatherTunic, 1);
					st.giveItems(LeatherStockings, 1);
				}
				else
				{
					st.giveItems(WoodenBreastplate, 1);
					st.giveItems(WoodenGaiters, 1);
				}
				st.giveItems(WoodenHelmet, 1);
				st.giveItems(LeatherShoes, 1);
				st.giveItems(Gloves, 1);
				st.giveItems(ADENA_ID, 2466);
				st.addExpAndSp(5672, 446);
				st.exitCurrentQuest(false);
				htmltext = "32173-05.htm";
			}
		}
		else if(npcId == Benis && cond == 1)
		{
			st.set("cond", "2");
			st.giveItems(WarehouseManifest, 1);
			htmltext = "32170.htm";
		}
		else if(npcId == Nika && cond == 3)
		{
			st.set("cond", "4");
			st.giveItems(GroceryStoreManifest, 1);
			htmltext = "32167.htm";
		}
		return htmltext;
	}
}
