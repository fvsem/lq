package quests_custom._1001_WakeUpBaium;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.boss.BaiumManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class _1001_WakeUpBaium extends Quest implements ScriptFile
{
	private static final int Baium = 29020;
	private static final int BaiumNpc = 29025;
	private static final int Angel = 29021;
	private static final int AngelicVortex = 31862;

	private static final int BloodedFabric = 4295;

	// Skills
	private static final int SPEAR_POUND = 4132;
	private static final int ANGEL_HEAL = 4133;

	private static final Location TELEPORT_POSITION = new Location(113100, 14500, 10077);

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Custom Quest: Wake Up Baium");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _1001_WakeUpBaium()
	{
		super(1001, "_1001_WakeUpBaium", "Wake Up Baium", 1);

		addStartNpc(BaiumNpc);
		addStartNpc(AngelicVortex);
		addAttackId(Baium);
		addAttackId(Angel);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == BaiumNpc)
		{
			if(st.getInt("ok") != 1)
			{
				st.exitCurrentQuest(true);
				return "Conditions are not right to wake up Baium!";
			}
			if(npc.isBusy() || !BaiumManager.isEnableEnter())
				return "Baium is busy!";
			npc.setBusy(true);
			npc.setBusyMessage("Attending another player's request");
			Functions.npcShout(npc, "Вы называете мое имя! Теперь вы умрете!");
			BaiumManager.spawnBaium(npc, st.getPlayer());
			return "<html><head><body>Баюм:<br>Вы назвали мое имя! Теперь вы умрете!</body></html>";
		}
		else if(npcId == AngelicVortex)
		{
			if(st.getQuestItemsCount(BloodedFabric) > 0)
			{
				L2NpcInstance baiumBoss = L2ObjectsStorage.getByNpcId(Baium);
				if(baiumBoss != null)
					return "<html><head><body>Ангельский Вихрь:<br>Баюм уже проснулся! Вы не можете войти!</body></html>";
				L2NpcInstance isbaiumNpc = L2ObjectsStorage.getByNpcId(BaiumNpc);
				if(isbaiumNpc == null)
					return "<html><head><body>Ангельский Вихрь:<br>Баюма сейчас здесь нет!</body></html>";
				st.takeItems(BloodedFabric, 1);
				st.getPlayer().teleToLocation(TELEPORT_POSITION);
				st.set("ok", "1");
				return "";
			}
			return "<html><head><body>Ангельский Вихрь:<br>У вас нехватает Итемов!</body></html>";
		}
		return null;
	}

	@Override
	public String onAttack(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		L2Player player = st.getPlayer();
		L2Skill skill;
		if(npcId == Angel)
		{
			if(Rnd.chance(10))
			{
				skill = SkillTable.getInstance().getInfo(SPEAR_POUND, 1);
				if(skill != null)
				{
					npc.setTarget(player);
					npc.doCast(skill, player, false);
				}
			}
			if(Rnd.chance(5) && npc.getCurrentHpPercents() < 50.0)
			{
				skill = SkillTable.getInstance().getInfo(ANGEL_HEAL, 1);
				if(skill != null)
				{
					npc.setTarget(npc);
					npc.doCast(skill, npc, false);
				}
			}
		}

		return null;
	}
}
