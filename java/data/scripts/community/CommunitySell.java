package community;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.CommunityBoardHandlers;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Transaction.TransactionType;
import l2n.game.network.serverpackets.ExBuySellList;

public class CommunitySell extends AbstractCommunityBoardHandler implements ICommunityBoardHandler, ScriptFile
{
	private static final String[] commands = new String[]
	{
			"_bbs_itemsell"
	};

	@Override
	public void onLoad()
	{
		CommunityBoardHandlers.getInstance().registerHandler(this);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean execute(final L2Player activeChar, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbs_itemsell"))
		{
			activeChar.tempInventoryDisable();
			activeChar.sendPacket(new ExBuySellList(null, activeChar, 0));
		}
		return false;
	}

	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		if(!activeChar.getPlayerAccess().UseShop || activeChar.isInventoryDisabled())
			return false;

		// lil check to prevent enchant exploit
		if(activeChar.getActiveEnchantItem() != null || activeChar.getActiveEnchantAttrItem() != null)
		{
			activeChar.sendActionFailed();
			return false;
		}

		// Проверяем не торгует ли уже чар.
		if(activeChar.isInStoreMode())
		{
			activeChar.sendActionFailed();
			return false;
		}

		// Во время обмена невозможно
		if(activeChar.isInTransaction() && activeChar.getTransaction().isTypeOf(TransactionType.TRADE))
		{
			activeChar.sendActionFailed();
			return false;
		}

		return true;
	}

	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
