package actions;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.game.cache.DropListCache;
import l2n.game.model.L2Effect;
import l2n.game.model.L2Object;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2NpcInstance.AggroInfo;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestEventType;
import l2n.game.skills.Stats;
import l2n.util.DropList;
import l2n.util.Files;

import java.util.Comparator;
import java.util.TreeSet;

public final class OnActionShift extends Functions
{
	public static final boolean checkCondition(final L2Player player, final L2Object object, boolean shift)
	{
		if(player == null || object == null || !shift)
			return false;
		return true;
	}

	public static String getNpcRaceById(final int raceId)
	{
		switch (raceId)
		{
			case 0:
				return "Undead";
			case 1:
				return "Magic Creatures";
			case 2:
				return "Beasts";
			case 3:
				return "Animals";
			case 4:
				return "Plants";
			case 5:
				return "Humanoids";
			case 6:
				return "Spirits";
			case 7:
				return "Angels";
			case 8:
				return "Demons";
			case 9:
				return "Dragons";
			case 10:
				return "Giants";
			case 11:
				return "Bugs";
			case 12:
				return "Fairies";
			case 13:
				return "Humans";
			case 14:
				return "Elves";
			case 15:
				return "Dark Elves";
			case 16:
				return "Orcs";
			case 17:
				return "Dwarves";
			case 18:
				return "Others";
			case 19:
				return "Non-living Beings";
			case 20:
				return "Siege Weapons";
			case 21:
				return "Defending Army";
			case 22:
				return "Mercenaries";
			case 23:
				return "Unknown Creature";
			case 24:
				return "Kamael";
			default:
				return "Not defined";
		}
	}

	public void droplist()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		droplist(player, npc);
	}

	public static final void droplist(final L2Player player, final L2NpcInstance npc)
	{
		if(player == null || npc == null)
			return;

		if(!Config.ALT_GAME_GEN_DROPLIST_ON_DEMAND)
			show(DropListCache.get(npc.getNpcId()), player);
		else
		{
			final int diff = npc.calculateLevelDiffForDrop(player.isInParty() ? player.getParty().getLevel() : player.getLevel(), false);
			double mult = 1;
			if(diff > 0)
				mult = Experience.penaltyModifier(diff, 9);
			mult = npc.calcStat(Stats.DROP, mult, null, null);
			show(DropList.generateDroplist(npc.getTemplate(), npc.isMonster() ? (L2MonsterInstance) npc : null, mult, player), player);
		}
	}

	public void quests()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><center><font color=\"LEVEL\">").append(npc.getName());

		final Quest[] list = npc.getTemplate().getEventQuests(QuestEventType.MOBKILLED);
		if(list != null && list.length != 0)
		{
			dialog.append("<center><img src=\"L2UI.SquareWhite\" width=\"275\" height=\"1\"><table  bgcolor=\"333333\" width=\"275\"><tr><td width=\"275\"><center><font color=\"FFFF00\">").append(player.isLangRus() ? "Убить для Квеста:" : "On Kill:").append("</font></center></td></tr></table><img src=\"L2UI.SquareWhite\" width=\"275\" height=\"1\"></center><br>");
			for(final Quest q : list)
				dialog.append("<img src=\"L2UI_CH3.QuestBtn\" width=32 height=32></td><td width=\"238\"><br><font color=\"LEVEL\">").append(q.getDescr(player)).append("</font><br1>[<font color=\"b09979\">").append(player.isLangRus() ? "ID Квеста: " : "Quest ID: ").append("</font>").append(q.getQuestIntId()).append("]</td></tr>");
		}

		dialog.append("</body></html>");
		show(dialog.toString(), player);
	}

	public void effects()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><body><center><font color=\"LEVEL\">");
		dialog.append(npc.getName()).append("<br></font></center><br>");
		dialog.append("<table width=\"90%\">");

		final L2Effect[] list = npc.getEffectList().getAllEffects();
		if(list != null && list.length > 0)
			for(final L2Effect e : list)
			{

				dialog.append("<tr><td width=\"32\"><img src=\"icon.").append(e.getSkill().getId()).append(" - ").append(e.getSkill().getLevel()).append(" '");
				dialog.append("<tr><td width=\"32\"><img src=\"icon.").append(e.getSkill().getName()).append("'<br1>");
			}

		dialog.append("<br><center><button value=\"");
		dialog.append("Обновить");
		dialog.append("\" action=\"bypass -h scripts_actions.OnActionShift:effects\" width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center></body></html>");

		show(dialog.toString(), player);
	}

	public void skilllist()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><body><center><font color=\"LEVEL\">");
		dialog.append(npc.getName()).append("<br></font></center>");

		if(npc.getAllSkills() != null && !npc.getAllSkills().isEmpty())
		{
			final GArray<L2Skill> passive = new GArray<L2Skill>();
			final GArray<L2Skill> active = new GArray<L2Skill>();
			for(final L2Skill skill : npc.getAllSkills())
				if(skill.isPassive())
					passive.add(skill);
				else
					active.add(skill);
			

			// показываем активные скилы

			dialog.append("<center><font color=\"LEVEL\">Активный Скилл</font></center>");
			dialog.append("<table width=\"90%\">");
			if(!active.isEmpty())
				for(final L2Skill skill : active)
					
			         dialog.append("<tr><td width=\"32\"><img src=\"icon.").append(skill.getIcon()).append("\" width=\"32\" height=\"32\"></td><td width=\"238\"><br><font color=\"LEVEL\">").append(skill.getName()).append("</font><br1>[<font color=\"b09979\">").append(player.isLangRus() ? "Уровень: " : "Level: ").append("</font>").append(skill.getLevel()).append("][<font color=\"b09979\">Id: </font>").append(skill.getId()).append("]</td></tr>");
			else
				dialog.append("<tr><td>Нет Скилов</td></tr>");
			dialog.append("</table><br>");

			// показываем пассиывные скилы
			dialog.append("<center><font color=\"LEVEL\">Пасивный Скилл</font></center>");
			dialog.append("<table width=\"90%\">");
			if(!passive.isEmpty())
				for(final L2Skill skill : passive)
					
			         dialog.append("<tr><td width=\"32\"><img src=\"icon.").append(skill.getIcon()).append("\" width=\"32\" height=\"32\"></td><td width=\"238\"><br><font color=\"LEVEL\">").append(skill.getName()).append("</font><br1>[<font color=\"b09979\">").append(player.isLangRus() ? "Уровень: " : "Level: ").append("</font>").append(skill.getLevel()).append("][<font color=\"b09979\">Id: </font>").append(skill.getId()).append("]</td></tr>");
			else
				dialog.append("<tr><td>Нет Скиллов</td></tr>");
			dialog.append("</table>");

			dialog.append("</body></html>");
		}
		else
			dialog.append("<center>Нет Скиллов</center></body></html>");
		show(dialog.toString(), player);
	}

	public void stats()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		String dialog = Files.read("data/scripts/actions/player.L2NpcInstance.stats.htm");
		dialog = dialog.replaceFirst("%name%", npc.getName());
		dialog = dialog.replaceFirst("%level%", String.valueOf(npc.getLevel()));
		dialog = dialog.replaceFirst("%factionId%", npc.getFactionId());
		dialog = dialog.replaceFirst("%aggro%", String.valueOf(npc.getAggroRange()));
		dialog = dialog.replaceFirst("%race%", getNpcRaceById(npc.getTemplate().getRace().ordinal()));
		dialog = dialog.replaceFirst("%herbs%", String.valueOf(npc.getTemplate().drop_herbs));
		dialog = dialog.replaceFirst("%maxHp%", String.valueOf(npc.getMaxHp()));
		dialog = dialog.replaceFirst("%maxMp%", String.valueOf(npc.getMaxMp()));
		dialog = dialog.replaceFirst("%pDef%", String.valueOf(npc.getPDef(null)));
		dialog = dialog.replaceFirst("%mDef%", String.valueOf(npc.getMDef(null, null)));
		dialog = dialog.replaceFirst("%pAtk%", String.valueOf(npc.getPAtk(null)));
		dialog = dialog.replaceFirst("%mAtk%", String.valueOf(npc.getMAtk(null, null)));
		dialog = dialog.replaceFirst("%accuracy%", String.valueOf(npc.getAccuracy()));
		dialog = dialog.replaceFirst("%evasionRate%", String.valueOf(npc.getEvasionRate(null)));
		dialog = dialog.replaceFirst("%criticalHit%", String.valueOf(npc.getCriticalHit(null, null)));
		dialog = dialog.replaceFirst("%runSpeed%", String.valueOf(npc.getRunSpeed()));
		dialog = dialog.replaceFirst("%walkSpeed%", String.valueOf(npc.getWalkSpeed()));
		dialog = dialog.replaceFirst("%pAtkSpd%", String.valueOf(npc.getPAtkSpd()));
		dialog = dialog.replaceFirst("%mAtkSpd%", String.valueOf(npc.getMAtkSpd()));
		dialog = dialog.replaceFirst("%STR%", String.valueOf(npc.getSTR()));
		dialog = dialog.replaceFirst("%DEX%", String.valueOf(npc.getDEX()));
		dialog = dialog.replaceFirst("%CON%", String.valueOf(npc.getCON()));
		dialog = dialog.replaceFirst("%INT%", String.valueOf(npc.getINT()));
		dialog = dialog.replaceFirst("%WIT%", String.valueOf(npc.getWIT()));
		dialog = dialog.replaceFirst("%MEN%", String.valueOf(npc.getMEN()));
		show(dialog, player);
	}

	public void resists()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><body><center><font color=\"LEVEL\">");
		dialog.append(npc.getName()).append("<br></font></center>");

		dialog.append("<center><button value=\"");
		if(player.getVar("lang@").equalsIgnoreCase("en"))
			dialog.append("Refresh");
		else
			dialog.append("Обновить");
		dialog.append("\" action=\"bypass -h scripts_actions.OnActionShift:resists\" width=100 height=15 back=\"L2UI_CT1.Button_DF\" fore=\"L2UI_CT1.Button_DF\" /></center>");

		// Бонусы элементальной атаки
		dialog.append("<center><font color=\"LEVEL\">Атака Элементами</font></center><br><table width=\"70%\">");
		final int FIRE_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_FIRE, npc.getTemplate().baseFireAttack, null, null);
		if(FIRE_ATTACK != 0)
			dialog.append("<tr><td>Атака Огнем</td><td>").append(FIRE_ATTACK).append("</td></tr>");

		final int WIND_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_WIND, npc.getTemplate().baseWaterAttack, null, null);
		if(WIND_ATTACK != 0)
			dialog.append("<tr><td>Атака Ветром</td><td>").append(WIND_ATTACK).append("</td></tr>");

		final int WATER_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_WATER, npc.getTemplate().baseWindAttack, null, null);
		if(WATER_ATTACK != 0)
			dialog.append("<tr><td>Атака Водой</td><td>").append(WATER_ATTACK).append("</td></tr>");

		final int EARTH_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_EARTH, npc.getTemplate().baseEarthAttack, null, null);
		if(EARTH_ATTACK != 0)
			dialog.append("<tr><td>Атака Землей</td><td>").append(EARTH_ATTACK).append("</td></tr>");

		final int SACRED_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_SACRED, npc.getTemplate().baseSacredAttack, null, null);
		if(SACRED_ATTACK != 0)
			dialog.append("<tr><td>Атака Святостью</td><td>").append(SACRED_ATTACK).append("</td></tr>");

		final int UNHOLY_ATTACK = (int) npc.calcStat(Stats.ATTACK_ELEMENT_UNHOLY, npc.getTemplate().baseUnholyAttack, null, null);
		if(UNHOLY_ATTACK != 0)
			dialog.append("<tr><td>Darkness attack</td><td>").append(UNHOLY_ATTACK).append("</td></tr>");

		if(FIRE_ATTACK == 0 && WIND_ATTACK == 0 && WATER_ATTACK == 0 && EARTH_ATTACK == 0 && SACRED_ATTACK == 0 && UNHOLY_ATTACK == 0)
			dialog.append("</table>Нет Атак Элементами<br>");
		else
			dialog.append("</table>");

		// Бонусы атаки
		dialog.append("<center><font color=\"LEVEL\">Бонус Атаки</font></center><br><table width=\"70%\">");

		final int BLEED_POWER = (int) npc.calcStat(Stats.BLEED_POWER, 0, null, null);
		if(BLEED_POWER != 0)
			dialog.append("<tr><td>Bleed power</td><td>").append(BLEED_POWER).append("</td></tr>");

		final int POISON_POWER = (int) npc.calcStat(Stats.POISON_POWER, 0, null, null);
		if(POISON_POWER != 0)
			dialog.append("<tr><td>Poison power</td><td>").append(POISON_POWER).append("</td></tr>");

		final int STUN_POWER = (int) npc.calcStat(Stats.STUN_POWER, 0, null, null);
		if(STUN_POWER != 0)
			dialog.append("<tr><td>Stun power</td><td>").append(STUN_POWER).append("</td></tr>");

		final int ROOT_POWER = (int) npc.calcStat(Stats.ROOT_POWER, 0, null, null);
		if(ROOT_POWER != 0)
			dialog.append("<tr><td>Root power</td><td>").append(ROOT_POWER).append("</td></tr>");

		final int MENTAL_POWER = (int) npc.calcStat(Stats.MENTAL_POWER, 0, null, null);
		if(MENTAL_POWER != 0)
			dialog.append("<tr><td>Mental power</td><td>").append(MENTAL_POWER).append("</td></tr>");

		final int SLEEP_POWER = (int) npc.calcStat(Stats.SLEEP_POWER, 0, null, null);
		if(SLEEP_POWER != 0)
			dialog.append("<tr><td>Sleep power</td><td>").append(SLEEP_POWER).append("</td></tr>");

		final int PARALYZE_POWER = (int) npc.calcStat(Stats.PARALYZE_POWER, 0, null, null);
		if(PARALYZE_POWER != 0)
			dialog.append("<tr><td>Paralyze power</td><td>").append(PARALYZE_POWER).append("</td></tr>");

		final int CANCEL_POWER = (int) npc.calcStat(Stats.CANCEL_POWER, 0, null, null);
		if(CANCEL_POWER != 0)
			dialog.append("<tr><td>Cancel power</td><td>").append(CANCEL_POWER).append("</td></tr>");

		final int DEBUFF_POWER = (int) npc.calcStat(Stats.DEBUFF_POWER, 0, null, null);
		if(DEBUFF_POWER != 0)
			dialog.append("<tr><td>Debuff power</td><td>").append(DEBUFF_POWER).append("</td></tr>");

		if(BLEED_POWER == 0 && POISON_POWER == 0 && STUN_POWER == 0 && ROOT_POWER == 0 && MENTAL_POWER == 0 && SLEEP_POWER == 0 && PARALYZE_POWER == 0 && CANCEL_POWER == 0 && DEBUFF_POWER == 0)
			dialog.append("</table>Нет Атак<br>");
		else
			dialog.append("</table>");

		dialog.append("<center><font color=\"LEVEL\">Резисты</font></center><br><table width=\"70%\">");
		final int FIRE_RECEPTIVE = (int) npc.calcStat(Stats.FIRE_RECEPTIVE, npc.getTemplate().baseFireDefend, null, null);
		if(FIRE_RECEPTIVE != 0)
			dialog.append("<tr><td>Огонь</td><td>").append(FIRE_RECEPTIVE).append("</td></tr>");

		final int WATER_RECEPTIVE = (int) npc.calcStat(Stats.WATER_RECEPTIVE, npc.getTemplate().baseWaterDefend, null, null);
		if(WATER_RECEPTIVE != 0)
			dialog.append("<tr><td>Вода</td><td>").append(WATER_RECEPTIVE).append("</td></tr>");

		final int WIND_RECEPTIVE = (int) npc.calcStat(Stats.WIND_RECEPTIVE, npc.getTemplate().baseWindDefend, null, null);
		if(WIND_RECEPTIVE != 0)
			dialog.append("<tr><td>Ветер</td><td>").append(WIND_RECEPTIVE).append("</td></tr>");

		final int EARTH_RECEPTIVE = (int) npc.calcStat(Stats.EARTH_RECEPTIVE, npc.getTemplate().baseEarthDefend, null, null);
		if(EARTH_RECEPTIVE != 0)
			dialog.append("<tr><td>Земля</td><td>").append(EARTH_RECEPTIVE).append("</td></tr>");

		final int SACRED_RECEPTIVE = (int) npc.calcStat(Stats.SACRED_RECEPTIVE, npc.getTemplate().baseSacredDefend, null, null);
		if(SACRED_RECEPTIVE != 0)
			dialog.append("<tr><td>Святость</td><td>").append(SACRED_RECEPTIVE).append("</td></tr>");

		final int UNHOLY_RECEPTIVE = (int) npc.calcStat(Stats.UNHOLY_RECEPTIVE, npc.getTemplate().baseUnholyDefend, null, null);
		if(UNHOLY_RECEPTIVE != 0)
			dialog.append("<tr><td>Темная</td><td>").append(UNHOLY_RECEPTIVE).append("</td></tr>");

		final int BLEED_RECEPTIVE = (int) npc.calcStat(Stats.BLEED_RECEPTIVE, 0, null, null);
		if(BLEED_RECEPTIVE != 0)
			dialog.append("<tr><td>Bleed</td><td>").append(BLEED_RECEPTIVE).append("</td></tr>");

		final int POISON_RECEPTIVE = (int) npc.calcStat(Stats.POISON_RECEPTIVE, 0, null, null);
		if(POISON_RECEPTIVE != 0)
			dialog.append("<tr><td>Poison</td><td>").append(POISON_RECEPTIVE).append("</td></tr>");

		final int DEATH_RECEPTIVE = (int) npc.calcStat(Stats.DEATH_RECEPTIVE, 0, null, null);
		if(DEATH_RECEPTIVE != 0)
			dialog.append("<tr><td>Death</td><td>").append(DEATH_RECEPTIVE).append("</td></tr>");

		final int STUN_RECEPTIVE = (int) npc.calcStat(Stats.STUN_RECEPTIVE, 0, null, null);
		if(STUN_RECEPTIVE != 0)
			dialog.append("<tr><td>Stun</td><td>").append(STUN_RECEPTIVE).append("</td></tr>");

		final int ROOT_RECEPTIVE = (int) npc.calcStat(Stats.ROOT_RECEPTIVE, 0, null, null);
		if(ROOT_RECEPTIVE != 0)
			dialog.append("<tr><td>Root</td><td>").append(ROOT_RECEPTIVE).append("</td></tr>");

		final int SLEEP_RECEPTIVE = (int) npc.calcStat(Stats.SLEEP_RECEPTIVE, 0, null, null);
		if(SLEEP_RECEPTIVE != 0)
			dialog.append("<tr><td>Sleep</td><td>").append(SLEEP_RECEPTIVE).append("</td></tr>");

		final int PARALYZE_RECEPTIVE = (int) npc.calcStat(Stats.PARALYZE_RECEPTIVE, 0, null, null);
		if(PARALYZE_RECEPTIVE != 0)
			dialog.append("<tr><td>Paralyze</td><td>").append(PARALYZE_RECEPTIVE).append("</td></tr>");

		final int MENTAL_RECEPTIVE = (int) npc.calcStat(Stats.MENTAL_RECEPTIVE, 0, null, null);
		if(MENTAL_RECEPTIVE != 0)
			dialog.append("<tr><td>Mental</td><td>").append(MENTAL_RECEPTIVE).append("</td></tr>");

		final int DEBUFF_RECEPTIVE = (int) npc.calcStat(Stats.DEBUFF_RECEPTIVE, 0, null, null);
		if(DEBUFF_RECEPTIVE != 0)
			dialog.append("<tr><td>Debuff</td><td>").append(DEBUFF_RECEPTIVE).append("</td></tr>");

		final int CANCEL_RECEPTIVE = (int) npc.calcStat(Stats.CANCEL_RECEPTIVE, 0, null, null);
		if(CANCEL_RECEPTIVE != 0)
			dialog.append("<tr><td>Cancel</td><td>").append(CANCEL_RECEPTIVE).append("</td></tr>");

		final int SWORD_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.SWORD_WPN_RECEPTIVE, 100, null, null);
		if(SWORD_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Sword</td><td>").append(SWORD_WPN_RECEPTIVE).append("%</td></tr>");

		final int DUAL_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.DUAL_WPN_RECEPTIVE, 100, null, null);
		if(DUAL_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Dual Sword</td><td>").append(DUAL_WPN_RECEPTIVE).append("%</td></tr>");

		final int BLUNT_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.BLUNT_WPN_RECEPTIVE, 100, null, null);
		if(BLUNT_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Blunt</td><td>").append(BLUNT_WPN_RECEPTIVE).append("%</td></tr>");

		final int DAGGER_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.DAGGER_WPN_RECEPTIVE, 100, null, null);
		if(DAGGER_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Dagger/rapier</td><td>").append(DAGGER_WPN_RECEPTIVE).append("%</td></tr>");

		final int BOW_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.BOW_WPN_RECEPTIVE, 100, null, null);
		if(BOW_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Bow/Crossbow</td><td>").append(BOW_WPN_RECEPTIVE).append("%</td></tr>");

		final int POLE_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.POLE_WPN_RECEPTIVE, 100, null, null);
		if(POLE_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Polearm</td><td>").append(POLE_WPN_RECEPTIVE).append("%</td></tr>");

		final int FIST_WPN_RECEPTIVE = 100 - (int) npc.calcStat(Stats.FIST_WPN_RECEPTIVE, 100, null, null);
		if(FIST_WPN_RECEPTIVE != 0)
			dialog.append("<tr><td>Fist weapons</td><td>").append(FIST_WPN_RECEPTIVE).append("%</td></tr>");

		final int CRIT_CHANCE_RECEPTIVE = (int) (100 - npc.calcStat(Stats.CRIT_CHANCE_RECEPTIVE, 100, null, null));
		if(CRIT_CHANCE_RECEPTIVE != 0)
			dialog.append("<tr><td>Resist Crit</td><td>").append(CRIT_CHANCE_RECEPTIVE).append("%</td></tr>");

		final int CRIT_DAMAGE_RECEPTIVE = 100 - (int) npc.calcStat(Stats.CRIT_DAMAGE_RECEPTIVE, 100, null, null);
		if(CRIT_DAMAGE_RECEPTIVE != 0)
			dialog.append("<tr><td>Crit get damage</td><td>").append(CRIT_DAMAGE_RECEPTIVE).append("%</td></tr>");

		if(FIRE_RECEPTIVE == 0 && WIND_RECEPTIVE == 0 && WATER_RECEPTIVE == 0 && EARTH_RECEPTIVE == 0 && UNHOLY_RECEPTIVE == 0 && SACRED_RECEPTIVE // primary elements
					== 0 && BLEED_RECEPTIVE == 0 && DEATH_RECEPTIVE == 0 && STUN_RECEPTIVE // phys debuff
					== 0 && POISON_RECEPTIVE == 0 && ROOT_RECEPTIVE == 0 && SLEEP_RECEPTIVE == 0 && PARALYZE_RECEPTIVE == 0 && MENTAL_RECEPTIVE == 0 && DEBUFF_RECEPTIVE == 0 && CANCEL_RECEPTIVE // mag debuff
					== 0 && SWORD_WPN_RECEPTIVE == 0 && DUAL_WPN_RECEPTIVE == 0 && BLUNT_WPN_RECEPTIVE == 0 && DAGGER_WPN_RECEPTIVE == 0 && BOW_WPN_RECEPTIVE == 0 && POLE_WPN_RECEPTIVE == 0 && FIST_WPN_RECEPTIVE // weapons
					== 0 && CRIT_CHANCE_RECEPTIVE == 0 && CRIT_DAMAGE_RECEPTIVE == 0 // other
		)
			dialog.append("</table>Нет Резистов<br>");
		else
			dialog.append("</table>");

		dialog.append("</body></html>");
		show(dialog.toString(), player);
	}

	public void aggro()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		final StringBuilder dialog = new StringBuilder("<html><body><center><font color=\"LEVEL\">");
		dialog.append(npc.getName()).append("<br></font></center>");
		dialog.append("<table width=\"80%\"><tr><td>Ник</td><td>Дамаг</td><td>Агрессия</td></tr>");

		// Сортировка аггролиста по хейту
		final TreeSet<AggroInfo> aggroList = new TreeSet<AggroInfo>(new Comparator<AggroInfo>()
		{
			@Override
			public int compare(final AggroInfo o1, final AggroInfo o2)
			{
				final int hateDiff = o1.hate - o2.hate;
				if(hateDiff != 0)
					return hateDiff;
				return o1.damage - o2.damage;
			}
		});
		aggroList.addAll(npc.getAggroList());

		// Вывод результата
		for(final AggroInfo aggroInfo : aggroList.descendingSet())
			if(aggroInfo.attacker != null && (aggroInfo.attacker.isPlayer() || aggroInfo.attacker.isSummon() || aggroInfo.attacker.isPet()))
				dialog.append("<tr><td>" + aggroInfo.attacker.getName() + "</td><td>" + aggroInfo.damage + "</td><td>" + aggroInfo.hate + "</td></tr>");

		dialog.append("</table><br><center><button value=\"");
		dialog.append(player.isLangRus() ? "Обновить" : "Refresh");
		dialog.append("\" action=\"bypass -h scripts_actions.OnActionShift:aggro\" width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" /></center></body></html>");
		show(dialog.toString(), player);
	}
}
