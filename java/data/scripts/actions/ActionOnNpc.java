package actions;

import l2n.Config;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Files;
import l2n.util.Util;

public class ActionOnNpc extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !object.isNpc())
			return false;

		if(!Config.ALLOW_NPC_SHIFTCLICK && !player.isGM())
		{
			if(Config.ALT_GAME_SHOW_DROPLIST && object.isNpc())
			{
				final L2NpcInstance npc = (L2NpcInstance) object;
				if(npc.isDead())
					return false;
				OnActionShift.droplist(player, npc);
			}
			return false;
		}

		final L2NpcInstance npc = (L2NpcInstance) object;
		// Для мертвых мобов не показываем табличку, иначе спойлеры плачут
		if(npc.isDead())
			return false;

		String dialog;
		if(Config.ALT_FULL_NPC_STATS_PAGE)
		{
			dialog = Files.read("data/scripts/actions/player.L2NpcInstance.onActionShift.full.htm");
			dialog = dialog.replaceFirst("%class%", String.valueOf(npc.getClass().getSimpleName().replaceFirst("L2", "").replaceFirst("Instance", "")));
			dialog = dialog.replaceFirst("%id%", String.valueOf(npc.getNpcId()));
			dialog = dialog.replaceFirst("%spawn%", String.valueOf(npc.getSpawn() != null ? npc.getSpawn().getId() : "0"));
			dialog = dialog.replaceFirst("%respawn%", String.valueOf(npc.getSpawn() != null ? Util.formatTime(npc.getSpawn().getRespawnDelay()) : "0"));
			dialog = dialog.replaceFirst("%walkSpeed%", String.valueOf(npc.getWalkSpeed()));
			dialog = dialog.replaceFirst("%evs%", String.valueOf(npc.getEvasionRate(null)));
			dialog = dialog.replaceFirst("%acc%", String.valueOf(npc.getAccuracy()));
			dialog = dialog.replaceFirst("%crt%", String.valueOf(npc.getCriticalHit(null, null)));
			dialog = dialog.replaceFirst("%aspd%", String.valueOf(npc.getPAtkSpd()));
			dialog = dialog.replaceFirst("%cspd%", String.valueOf(npc.getMAtkSpd()));
			dialog = dialog.replaceFirst("%loc%", String.valueOf(npc.getSpawn() != null ? npc.getSpawn().getLocation() : "0"));
			dialog = dialog.replaceFirst("%dist%", String.valueOf((int) npc.getRealDistance(player)));
			dialog = dialog.replaceFirst("%killed%", String.valueOf(npc.getTemplate().killscount));
			dialog = dialog.replaceFirst("%spReward%", String.valueOf(npc.getSpReward()));
			dialog = dialog.replaceFirst("%STR%", String.valueOf(npc.getSTR()));
			dialog = dialog.replaceFirst("%DEX%", String.valueOf(npc.getDEX()));
			dialog = dialog.replaceFirst("%CON%", String.valueOf(npc.getCON()));
			dialog = dialog.replaceFirst("%INT%", String.valueOf(npc.getINT()));
			dialog = dialog.replaceFirst("%WIT%", String.valueOf(npc.getWIT()));
			dialog = dialog.replaceFirst("%MEN%", String.valueOf(npc.getMEN()));
			dialog = dialog.replaceFirst("%xyz%", npc.getLoc().toXYZString() + " (" + npc.getReflection().getId() + ")");
			dialog = dialog.replaceFirst("%heading%", String.valueOf(npc.getLoc().h));
			dialog = dialog.replaceFirst("%ai_type%", npc.getAI().getL2ClassShortName());
			dialog = dialog.replaceFirst("%storId%", String.valueOf(npc.getStoredId()));
			dialog = dialog.replaceFirst("%objId%", String.valueOf(npc.getObjectId()));
		}
		else
			dialog = Files.read("data/scripts/actions/player.L2NpcInstance.onActionShift.htm");

		dialog = dialog.replaceAll("%name%", npc.getName());
		dialog = dialog.replaceFirst("%level%", String.valueOf(npc.getLevel()));
		dialog = dialog.replaceFirst("%herbs%", String.valueOf(npc.getTemplate().drop_herbs));
		dialog = dialog.replaceFirst("%factionId%", npc.getFactionId().equals("") ? "none" : npc.getFactionId());
		dialog = dialog.replaceFirst("%aggro%", String.valueOf(npc.getAggroRange()));
		dialog = dialog.replaceFirst("%maxHp%", String.valueOf(npc.getMaxHp()));
		dialog = dialog.replaceFirst("%maxMp%", String.valueOf(npc.getMaxMp()));
		dialog = dialog.replaceFirst("%pDef%", String.valueOf(npc.getPDef(null)));
		dialog = dialog.replaceFirst("%mDef%", String.valueOf(npc.getMDef(null, null)));
		dialog = dialog.replaceFirst("%pAtk%", String.valueOf(npc.getPAtk(null)));
		dialog = dialog.replaceFirst("%mAtk%", String.valueOf(npc.getMAtk(null, null)));
		dialog = dialog.replaceFirst("%expReward%", String.valueOf(npc.getExpReward()));
		dialog = dialog.replaceFirst("%runSpeed%", String.valueOf(npc.getRunSpeed()));

		// Дополнительная инфа для ГМов
		if(player.getPlayerAccess().IsGM)
			dialog = dialog.replaceFirst("%AI%", String.valueOf(npc.getAI()) + ",<br1>active: " + npc.getAI().isActive() + ",<br1>intention: " + npc.getAI().getIntention());
		else
			dialog = dialog.replaceFirst("%AI%", "");

		show(dialog, player);

		return true;
	}
}
