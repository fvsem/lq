package actions;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.handler.admincommandhandlers.AdminEditChar;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;

public final class ActionOnPlayer extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, final boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !player.getPlayerAccess().EditChar || !object.isPlayer())
			return false;

		AdminEditChar.showCharacterList(player, (L2Player) object);
		return true;
	}
}
