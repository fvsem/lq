package events.arena;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptEventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;
import l2n.util.Location;

public class DionArena extends ArenaTemplate
{
	public DionArena()
	{

	}

	public static DionArena getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final DionArena _instance = new DionArena();
	}

	public void loadArena()
	{
		_managerId = 20230001;
		_className = "DionArena";
		_status = 0;

		_zoneListener = new ZoneListener();
		_zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 1, true);
		_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

		_team1points = new GArray<Location>();
		_team2points = new GArray<Location>();

		_team1points.add(new Location(12053, 183101, -3563));
		_team1points.add(new Location(12253, 183101, -3563));
		_team1points.add(new Location(12459, 183101, -3563));
		_team1points.add(new Location(12659, 183101, -3563));
		_team1points.add(new Location(12851, 183101, -3563));
		_team2points.add(new Location(12851, 183941, -3563));
		_team2points.add(new Location(12659, 183941, -3563));
		_team2points.add(new Location(12459, 183941, -3563));
		_team2points.add(new Location(12253, 183941, -3563));
		_team2points.add(new Location(12053, 183941, -3563));

		addEventId(ScriptEventType.ON_DIE);
		addEventId(ScriptEventType.ON_DISCONNECT);
		addEventId(ScriptEventType.ON_ESCAPE);
		_log.info("Loaded Event: Dion Arena");
	}

	public void unLoadArena()
	{
		if(_status > 0)
			stop();
		removeEventId(ScriptEventType.ON_DIE);
		removeEventId(ScriptEventType.ON_DISCONNECT);
		removeEventId(ScriptEventType.ON_ESCAPE);
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
	}

	@Override
	public void onLoad()
	{
		getInstance().loadArena();
	}

	@Override
	public void onReload()
	{
		getInstance().unLoadArena();
	}

	@Override
	public void onShutdown()
	{
		onReload();
	}

	public String DialogAppend_20230001(final Integer val)
	{
		if(val == 0)
		{
			final L2Player player = (L2Player) getSelf();
			if(player.isGM())
				return Files.read("data/scripts/events/arena/20230001.html", player) + Files.read("data/scripts/events/arena/20230001-4.html", player);
			return Files.read("data/scripts/events/arena/20230001.html", player);
		}
		return "";
	}

	public String DialogAppend_20230002(final Integer val)
	{
		return DialogAppend_20230001(val);
	}

	public void create1()
	{
		getInstance().template_create1((L2Player) getSelf());
	}

	public void create2()
	{
		getInstance().template_create2((L2Player) getSelf());
	}

	public void register()
	{
		getInstance().template_register((L2Player) getSelf());
	}

	public void check1(final String[] var)
	{
		getInstance().template_check1((L2Player) getSelf(), getNpc(), var);
	}

	public void check2(final String[] var)
	{
		getInstance().template_check2((L2Player) getSelf(), getNpc(), var);
	}

	public void register_check(final String[] var)
	{
		getInstance().template_register_check((L2Player) getSelf(), var);
	}

	public void stop()
	{
		getInstance().template_stop();
	}

	public void announce()
	{
		getInstance().template_announce();
	}

	public void prepare()
	{
		getInstance().template_prepare();
	}

	public void start()
	{
		getInstance().template_start();
	}

	public static void timeOut()
	{
		getInstance().template_timeOut();
	}
}
