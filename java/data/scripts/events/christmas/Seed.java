package events.christmas;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.handler.ItemHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;

public class Seed implements IItemHandler, ScriptFile
{
	public static class DeSpawnScheduleTimerTask implements Runnable
	{
		private final L2Spawn spawnedTree;

		public DeSpawnScheduleTimerTask(L2Spawn spawn)
		{
			spawnedTree = spawn;
		}

		@Override
		public void run()
		{
			try
			{
				spawnedTree.getLastSpawn().decayMe();
				spawnedTree.getLastSpawn().deleteMe();
			}
			catch(Throwable t)
			{}
		}
	}

	private final static int[] _itemIds = {
			5560, // Christmas Tree
			5561 // Special Christmas Tree
	};

	private final static int[] _npcIds = {
			13006, // Christmas Tree
			13007 // Special Christmas Tree
	};

	private static final int DESPAWN_TIME = 600000; // 10 min

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		L2Player activeChar = (L2Player) playable;
		L2NpcTemplate template = null;

		int itemId = item.getItemId();
		for(int i = 0; i < _itemIds.length; i++)
			if(_itemIds[i] == itemId)
			{
				template = NpcTable.getTemplate(_npcIds[i]);
				break;
			}

		if(template == null)
			return;

		try
		{
			L2Spawn spawn = new L2Spawn(template);
			spawn.setId(IdFactory.getInstance().getNextId());
			spawn.setLoc(activeChar.getLoc());
			L2NpcInstance npc = spawn.doSpawn(false);
			npc.setTitle(activeChar.getName()); // FIXME Почему-то не устанавливается
			spawn.respawnNpc(npc);

			// АИ вещающее бафф регена устанавливается только для большой елки
			if(itemId == 5561)
			{
				npc.setAI(new ctreeAI(npc));
				npc.getAI().startAITask();
			}

			L2GameThreadPools.getInstance().scheduleGeneral(new DeSpawnScheduleTimerTask(spawn), DESPAWN_TIME);
			activeChar.getInventory().destroyItem(item.getObjectId(), 1, false);
		}
		catch(Exception e)
		{
			activeChar.sendPacket(Msg.YOUR_TARGET_CANNOT_BE_FOUND);
		}
	}

	@Override
	public int[] getItemIds()
	{
		return _itemIds;
	}

	@Override
	public void onLoad()
	{
		ItemHandler.getInstance().registerItemHandler(this);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
