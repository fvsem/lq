package events.deathmatch;

import l2n.Config;
import l2n.Config.EventInterval;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.event.HWIDChecker;
import l2n.game.event.L2Event;
import l2n.game.event.L2EventTeam;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.*;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.Revive;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorTable;
import l2n.game.tables.HeroSkillTable;
import l2n.game.tables.ReflectionTable;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

import java.util.Calendar;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import static l2n.game.model.L2Zone.ZoneType.OlympiadStadia;

public class DeathMatch extends L2Event implements ScriptFile
{
	private final static Logger _log = Logger.getLogger(DeathMatch.class.getName());

	private static ScheduledFuture<StartTask> _startTask;
	private static ScheduledFuture<?> _endTask;

	private static boolean _active = false;
	private static boolean _addEventType = false;

	private static L2EventTeam participants;
	private static final L2Skill[][] _eventBuffs = new L2Skill[2][];

	private static final HWIDChecker hwid_check = new HWIDChecker(Config.EVENT_DEATHMATCH_CHECK_HWID);

	private static boolean _isRegistrationActive = false;
	private static int _status = 0;
	private static int _time_to_start;
	private static int _category;
	private static int _minLevel;
	private static int _maxLevel;
	private static int _autoContinue = 0;

	private final static L2Zone _zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 4, true);
	private final static ZoneListener _zoneListener = new ZoneListener();

	private static final Reflection _reflection = new Reflection(ReflectionTable.EVENT_DEATHMATCH);
	private static final int[] _doors = new int[] { 24190001, 24190002, 24190003, 24190004 };

	@Override
	public void onLoad()
	{
		_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

		// Если ивент активен, но пробуем зашедулить
		if(isActive("DeathMatch"))
		{
			_active = true;
			scheduleEventStart(true);
		}

		_log.info("Loaded Event: " + getEventType() + "[" + _active + "]");

		// дверьки добавляем)
		for(final int doorId : _doors)
			_reflection.addDoor(DoorTable.getInstance().getDoor(doorId).clone());

		_eventBuffs[0] = Util.parseSkills(Config.EVENT_DEATHMATCH_BUFFS_FIGHTER);
		_eventBuffs[1] = Util.parseSkills(Config.EVENT_DEATHMATCH_BUFFS_MAGE);
	}

	@Override
	public void onReload()
	{
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
		if(_startTask != null)
			_startTask.cancel(true);
	}

	@Override
	public void onShutdown()
	{
		onReload();
	}

	@Override
	public void activateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("DeathMatch", true))
		{
			// при активации ивента, если он не был активирован, то пробуем стартовать. Так как как таск стартует только при загрузке
			if(_startTask == null)
				scheduleEventStart(false);
			_log.info("Event '" + getEventType() + "' activated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStarted", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' already active.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	@Override
	public void deactivateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("DeathMatch", false))
		{
			if(_startTask != null)
			{
				_startTask.cancel(true);
				_startTask = null;
			}
			_log.info("Event '" + getEventType() + "' deactivated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStoped", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' not active.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public boolean isRunned()
	{
		return _isRegistrationActive || _status > 0;
	}


	@Override
	public L2Skill[] getFighterBuffs()
	{
		return _eventBuffs[0];
	}

	@Override
	public L2Skill[] getMageBuffs()
	{
		return _eventBuffs[1];
	}

	@Override
	public void start(final String... var)
	{
		if(isRunned())
		{
			_log.info("DeathMatch: start task already running!");
			return;
		}

		if(!_addEventType)
		{
			_addEventType = true;
			addEventId(ScriptEventType.ON_PARTY_INVITE);
			addEventId(ScriptEventType.ON_DISCONNECT);
			addEventId(ScriptEventType.ON_ESCAPE);
			addEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
			addEventId(ScriptEventType.ON_DIE);
		}

		final L2Player player = (L2Player) getSelf();
		if(var.length != 2)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		Integer category;
		Integer autoContinue;
		try
		{
			category = Integer.valueOf(var[0]);
			autoContinue = Integer.valueOf(var[1]);
		}
		catch(final Exception e)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		_category = category;
		_autoContinue = autoContinue;

		if(_category == -1)
		{
			_minLevel = 1;
			_maxLevel = 85;
		}
		else
		{
			_minLevel = getMinLevelForCategory(_category);
			_maxLevel = getMaxLevelForCategory(_category);
		}

		if(_endTask != null)
		{
			show(new CustomMessage("common.TryLater", player), player);
			return;
		}

		_status = 0;
		_isRegistrationActive = true;
		_time_to_start = Config.EVENT_DEATHMATCH_TIME_TO_START;

		participants = new L2EventTeam(0, "EVENT_DEATHMATCH");

		final String[] param = { String.valueOf(_time_to_start), String.valueOf(_minLevel), String.valueOf(_maxLevel) };
		sayToAll("scripts.events.deathmatch.AnnouncePreStart", param);

		executeTask("events.deathmatch.DeathMatch", "question", EMPTY_ARG, 10000);
		executeTask("events.deathmatch.DeathMatch", "announce", EMPTY_ARG, 60000);
		_log.info("DeathMatch: start event [" + _category + "-" + _autoContinue + "]");
	}

	public static void question()
	{
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getLevel() >= _minLevel && player.getLevel() <= _maxLevel && player.isAllowEventQuestion())
				player.scriptRequest(new CustomMessage("scripts.events.deathmatch.AskPlayer", player).toString(), "events.deathmatch.DeathMatch:addPlayer", EMPTY_ARG);
	}

	public static void announce()
	{
		if(_time_to_start <= 1 && participants.getPlayersCount() < Config.EVENT_DEATHMATCH_MIN_PLAYER_COUNT)
		{
			sayToAll("scripts.events.deathmatch.AnnounceEventCancelled");
			_status = 0;
			_isRegistrationActive = false;
			executeTask("events.deathmatch.DeathMatch", "autoContinue", EMPTY_ARG, 5000);
			return;
		}

		if(_time_to_start > 1)
		{
			_time_to_start--;
			final String[] param = { String.valueOf(_time_to_start), String.valueOf(_minLevel), String.valueOf(_maxLevel) };
			sayToAll("scripts.events.deathmatch.AnnouncePreStart", param);
			executeTask("events.deathmatch.DeathMatch", "announce", EMPTY_ARG, 60000);
		}
		else
		{
			_status = 1;
			_isRegistrationActive = false;
			sayToAll("scripts.events.deathmatch.AnnounceEventStarting");
			executeTask("events.deathmatch.DeathMatch", "prepare", EMPTY_ARG, 5000);
		}
	}

	public void addPlayer()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null || !checkPlayer(player, true))
			return;

		participants.addPlayer(player);

		show(new CustomMessage("scripts.events.deathmatch.Registered", player), player);
	}

	public static boolean checkPlayer(final L2Player player, final boolean first)
	{
		if(first && !_isRegistrationActive)
		{
			show(new CustomMessage("scripts.events.Late", player), player);
			return false;
		}

		if(first && player.isDead())
			return false;

		if(first && participants.isInTeam(player))
		{
			show(new CustomMessage("scripts.events.deathmatch.Cancelled", player), player);
			return false;
		}

		if(player.getLevel() < _minLevel || player.getLevel() > _maxLevel)
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledLevel", player), player);
			return false;
		}

		if(player.getDuel() != null)
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledDuel", player), player);
			return false;
		}

		if(first && player.isInEvent(L2EventType.NONE) || player.getTeam() != 0)
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.isInZone(OlympiadStadia))
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledOlympiad", player), player);
			return false;
		}

		if(player.isInParty() && player.getParty().isInDimensionalRift())
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.isTeleporting())
		{
			show(new CustomMessage("scripts.events.deathmatch.CancelledTeleport", player), player);
			return false;
		}

		if(player.isCursedWeaponEquipped())
		{
			show(new CustomMessage("scripts.events.deathmatch.Cancelled", player), player);
			return false;
		}

		// последним проверяем HardwareID
		if(first && !hwid_check.canParticipate(player))
		{
			show(new CustomMessage("scripts.events.deathmatch.Cancelled", player), player);
			return false;
		}

		return true;
	}

	@Override
	protected boolean isInEvent(final L2Player player, final L2EventType type)
	{
		return participants.isInTeam(player) && (type == L2EventType.NONE || type == getEventType());
	}

	@Override
	protected void onDie(final L2Character victim, final L2Character killer)
	{
		if(_status > 1 && victim != null && victim.isPlayer() && victim.getTeam() > 0)
		{
			// если жертва находиться в команде
			if(participants.isLive(victim.getStoredId()))
				// увеличиваем счётчик смертей игроку
				participants.getPlayerInfo(victim.getStoredId()).incrementDeaths();

			// Выдаём очки убийце и выдаём награду
			if(killer != null && killer.isPlayer())
				if(participants.isInTeam(killer.getStoredId()))
				{
					// Выдаём очки
					participants.getPlayerInfo(killer.getStoredId()).incrementKills();
					if(Config.EVENT_DEATHMATCH_RATE)
						addItem((L2Player) killer, Config.EVENT_DEATHMATCH_ITEM_ID, victim.getLevel() * Config.EVENT_DEATHMATCH_ITEM_COUNT);
					else
						addItem((L2Player) killer, Config.EVENT_DEATHMATCH_ITEM_ID, Config.EVENT_DEATHMATCH_ITEM_COUNT);
				}

			final L2Player player = victim.getPlayer();
			loosePlayer(player, true);
			checkLive();
		}
	}

	public static void prepare()
	{
		cleanPlayers();
		clearArena();

		executeTask("events.deathmatch.DeathMatch", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.deathmatch.DeathMatch", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.deathmatch.DeathMatch", "saveBackCoords", EMPTY_ARG, 3000);
		executeTask("events.deathmatch.DeathMatch", "teleportPlayersToColiseum", EMPTY_ARG, 4000);
		executeTask("events.deathmatch.DeathMatch", "paralyzePlayers", EMPTY_ARG, 5000);
		executeTask("events.deathmatch.DeathMatch", "go", EMPTY_ARG, 60000);

		sayToAll("scripts.events.deathmatch.AnnounceFinalCountdown");
	}

	@Override
	public void go()
	{
		_status = 2;
		upParalyzePlayers();

		checkLive();
		clearArena();
		sayToAll("scripts.events.deathmatch.AnnounceFight");

		// бафаем игроков перед стартом
		if(Config.EVENT_DEATHMATCH_BUFFS_ONSTART)
			participants.buffAllPlayers(this);

		_endTask = executeTask("events.deathmatch.DeathMatch", "endBattle", EMPTY_ARG, Config.EVENT_DEATHMATCH_TIME_FOR_FIGHT * 60 * 1000);
	}

	public static void endBattle()
	{
		_status = 0;
		removeAura();

		// Остался только один)
		if(participants.getLivePlayersCount() == 1)
		{
			for(final L2Player player : participants.getLivePlayers())
				if(player != null)
				{
					player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
					sayToAll("scripts.events.deathmatch.AnnounceWiner", new String[] { player.getName() });

					if(Config.EVENT_DEATHMATCH_GIVE_HERO)
						player.setHero(true, 1);

					if(Config.EVENT_DEATHMATCH_REWARD_ITEM_ID > 0 && Config.EVENT_DEATHMATCH_REWARD_ITEM_COUNT > 0)
						addItem(player, Config.EVENT_DEATHMATCH_REWARD_ITEM_ID, Config.EVENT_DEATHMATCH_REWARD_ITEM_COUNT);
					break;
				}
		}
		// иначе считаем количество убийств(
		else
		{
			final L2Player player = participants.getTopPlayer();
			if(player != null)
			{
				player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
				sayToAll("scripts.events.deathmatch.AnnounceWiner", new String[] { player.getName() });

				if(Config.EVENT_DEATHMATCH_GIVE_HERO)
					player.setHero(true, 1);

				if(Config.EVENT_DEATHMATCH_REWARD_ITEM_ID > 0 && Config.EVENT_DEATHMATCH_REWARD_ITEM_COUNT > 0)
					addItem(player, Config.EVENT_DEATHMATCH_REWARD_ITEM_ID, Config.EVENT_DEATHMATCH_REWARD_ITEM_COUNT);
			}
		}

		// отправляем всем статискику
		participants.sendPacketToAll(getFinitshStatistics(participants, "Deathmatch TOP"));

		sayToAll("scripts.events.deathmatch.AnnounceEnd");
		executeTask("events.deathmatch.DeathMatch", "end", EMPTY_ARG, 60000);
		_isRegistrationActive = false;
		if(_endTask != null)
		{
			_endTask.cancel(false);
			_endTask = null;
		}
	}

	public static void end()
	{
		openColiseumDoors();
		executeTask("events.deathmatch.DeathMatch", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.deathmatch.DeathMatch", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.deathmatch.DeathMatch", "teleportPlayersToSavedCoords", EMPTY_ARG, 3000);
		executeTask("events.deathmatch.DeathMatch", "autoContinue", EMPTY_ARG, 5000);
	}

	public void autoContinue()
	{
		participants.clear();
		hwid_check.clear();

		if(_autoContinue > 0)
		{
			if(_autoContinue >= 6)
			{
				_autoContinue = 0;
				return;
			}
			start(new String[] { "" + (_autoContinue + 1), "" + (_autoContinue + 1) });
		}
		else
		{
			// если ивент был запущен вручную, но не активирон, то удаляем
			if(_addEventType)
			{
				_addEventType = false;
				removeEventId(ScriptEventType.ON_PARTY_INVITE);
				removeEventId(ScriptEventType.ON_DISCONNECT);
				removeEventId(ScriptEventType.ON_ESCAPE);
				removeEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
				removeEventId(ScriptEventType.ON_DIE);
			}

			// если нет, то пробуем зашедулить по времени из конфигов
			scheduleEventStart(true);
		}
	}

	/**
	 * проверяет возможность запуска ивента и стартует такс в указанное в конфигах время
	 * 
	 * @param check
	 *            - проверять активирован ли ивент
	 */
	public void scheduleEventStart(final boolean check)
	{
		// ивент должен быть активированным для автоматического запуска
		if(check && !_active)
			return;

		try
		{
			final Calendar currentTime = Calendar.getInstance();
			int nextCategory = -1;
			Calendar testStartTime = null;
			Calendar nextStartTime = null;

			// перебираем всё варианты старта... находим оптимальный вариант
			for(final EventInterval interval : Config.EVENT_DEATHMATCH_INTERVAL)
			{
				// Creating a Calendar object from the specified interval value
				testStartTime = Calendar.getInstance();
				testStartTime.setLenient(true); // чтоб выдало исключение если время задано не верно в конфигах

				// устанавливаем время предположительно след запуска
				testStartTime.set(Calendar.HOUR_OF_DAY, interval.hour);
				testStartTime.set(Calendar.MINUTE, interval.minute);

				// If the date is in the past, make it the next day (Example: Checking for "1:00", when the time is 23:57.)
				if(testStartTime.getTimeInMillis() < currentTime.getTimeInMillis())
					testStartTime.add(Calendar.DAY_OF_MONTH, 1);

				// сравниваем, если подходит то устанавливаем
				if(nextStartTime == null || testStartTime.getTimeInMillis() < nextStartTime.getTimeInMillis())
				{
					nextStartTime = testStartTime;
					nextCategory = interval.category;
				}
			}

			_log.info("DeathMatch: next start at " + Util.datetimeFormatter.format(nextStartTime.getTime()));
			// запускаем ивент с указанной катерорией и без автопродолжения
			if(_startTask != null)
			{
				_startTask.cancel(false);
				_startTask = null;
			}
			_startTask = L2GameThreadPools.getInstance().scheduleGeneral(new StartTask(nextCategory, 0), nextStartTime.getTimeInMillis() - System.currentTimeMillis());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "DeathMatch.scheduleEventStart(): error figuring out a start time. Check config file.", e);
		}
	}

	public static void saveBackCoords()
	{
		participants.addRestrictions(Config.EVENT_DEATHMATCH_ITEM_RESTRICTION, Config.EVENT_DEATHMATCH_SKILL_RESTRICTION);
		participants.saveBackCoords();
	}

	public static void teleportPlayersToColiseum()
	{
		closeColiseumDoors();

		for(final L2Player player : participants.getAllPlayers())
			if(player != null)
			{
				unRide(player);

				if(!Config.EVENT_DEATHMATCH_ALLOW_SUMMONS)
					unSummonPet(player);

				// Remove Buffs
				if(!Config.EVENT_DEATHMATCH_ALLOW_BUFFS)
				{
					player.stopAllEffects();
					if(player.getPet() != null)
						player.getPet().stopAllEffects();
				}

				// Remove Clan Skills
				if(!Config.EVENT_DEATHMATCH_ALLOW_CLAN_SKILL)
					if(player.getClan() != null)
						for(final L2Skill skill : player.getClan().getAllSkills())
							player.removeSkill(skill, false);

				if(!Config.EVENT_DEATHMATCH_ALLOW_HERO_SKILL)
					// Remove Hero Skills
					if(player.isHero())
						for(final L2Skill sk : HeroSkillTable.getHeroSkills())
							player.removeSkillById(sk.getId());

				// Force the character to be mortal
				if(player.isInvul())
					player.setInvul(false);

				// Force the character to be visible
				if(player.isInvisible())
					player.setInvisible(false);

				if(Config.EVENT_DEATHMATCH_DISPEL_TRANSFORMATION && player.isTransformed() && !player.getTransform().isDefaultActionListTransform())
					player.stopTransformation();

				// выгоняем из пати
				if(Config.EVENT_DEATHMATCH_PARTY_DISABLE && player.getParty() != null)
					player.getParty().oustPartyMember(player);

				final Location pos = Rnd.coordsRandomize(149505, 46719, -3417, 0, 0, 500);
				player.teleToLocation(pos, ReflectionTable.EVENT_DEATHMATCH);
			}
	}

	public static void teleportPlayersToSavedCoords()
	{
		participants.removeRestrictions(Config.EVENT_DEATHMATCH_ITEM_RESTRICTION, Config.EVENT_DEATHMATCH_SKILL_RESTRICTION);
		participants.teleportToSavedCoords(true);
	}

	public static void teleportToColiseumSpawn(final L2Character cha)
	{
		if(cha != null)
			cha.teleToLocation(147451, 46728, -3410);
	}

	public static void paralyzePlayers()
	{
		participants.paralyzePlayers();
	}

	public static void upParalyzePlayers()
	{
		participants.upParalyzePlayers(true);
	}

	public static void ressurectPlayers()
	{
		participants.ressurectPlayers();
	}

	public static void healPlayers()
	{
		participants.healPlayers();
	}

	public static void cleanPlayers()
	{
		for(final L2Player player : participants.getAllPlayers())
			if(!checkPlayer(player, false))
				removePlayer(player);
	}

	public static void checkLive()
	{
		participants.updateLivePlayers();
		for(final L2Player player : participants.getLivePlayers())
			if(player.isInZone(_zone) && player.isConnected() && !player.isLogoutStarted())
				player.setTeam(2, false);
			else
				loosePlayer(player, false);

		if(participants.getLivePlayersCount() <= 1)
			endBattle();
	}

	public static void removeAura()
	{
		participants.removeAura();
	}

	/**
	 * чистим арену от чужих)
	 */
	public static void clearArena()
	{
		for(final L2Object obj : _zone.getObjectsInside())
			if(obj != null && obj.getReflectionId() == ReflectionTable.EVENT_DEATHMATCH)
			{
				final L2Player player = obj.getPlayer();
				if(player != null && !participants.isLive(player.getStoredId()))
					teleportToColiseumSpawn(player);
			}
	}

	@Override
	protected final void onEscape(final L2Player player)
	{
		if(_status > 1 && player != null && player.getTeam() > 0 && participants.isLive(player.getStoredId()))
		{
			removePlayer(player);
			checkLive();
		}
	}

	@Override
	protected final void onDisconnect(final L2Player player)
	{
		if(player == null || player.getTeam() < 1)
			return;

		// Вышел или вылетел во время регистрации
		if(_status == 0 && _isRegistrationActive && participants.isLive(player.getStoredId()))
		{
			removePlayer(player);
			return;
		}

		// Вышел или вылетел во время телепортации
		if(_status == 1 && participants.isLive(player.getStoredId()))
		{
			removePlayer(player);

			try
			{
				final String var = player.getVar("event_backcoords");
				if(var == null || var.equals(""))
					return;
				final String[] coords = var.split(" ");
				if(coords.length != 4)
					return;

				player.teleToLocation(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]), Long.parseLong(coords[3]));
				player.unsetVar("event_backcoords");
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

			return;
		}

		// Вышел или вылетел во время эвента
		onEscape(player);
	}

	@Override
	protected SystemMessage onPartyInvite(final L2Player inviter, final L2Player target)
	{
		if((participants.isInTeam(inviter) || participants.isInTeam(target)) && Config.EVENT_DEATHMATCH_PARTY_DISABLE)
			return Msg.INCORRECT_TARGET;
		return null;
	}

	private static class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 0 && player != null && !participants.isLive(player.getStoredId()) && player.getReflectionId() == ReflectionTable.EVENT_DEATHMATCH)
				teleportToColiseumSpawn(character);
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 1 && player != null && player.getTeam() > 0 && participants.isLive(player.getStoredId()) && player.getReflectionId() == ReflectionTable.EVENT_DEATHMATCH)
			{
				final double angle = Util.convertHeadingToDegree(player.getHeading()); // угол в градусах
				final double radian = Math.toRadians(angle - 90); // угол в радианах
				final int x = (int) (character.getX() + 50 * Math.sin(radian));
				final int y = (int) (character.getY() - 50 * Math.cos(radian));
				final int z = character.getZ();
				player.teleToLocation(x, y, z);
			}
		}
	}

	private static void loosePlayer(final L2Player player, final boolean inc)
	{
		if(player != null)
			if(inc && player.getTeam() > 0)
			{
				show(new CustomMessage("scripts.events.Revive", player).addNumber(Config.EVENT_DEATHMATCH_REVIVE_DELAY), player);
				executeTask("events.deathmatch.DeathMatch", "resurrect", new Object[] { player }, Config.EVENT_DEATHMATCH_REVIVE_DELAY * 1000);
			}
			else
			{
				participants.removeFromLive(player);
				player.setTeam(0, false);
				show(new CustomMessage("scripts.events.deathmatch.YouLose", player), player);
			}
	}

	public void resurrect(final L2Player player)
	{
		if(player.getTeam() <= 0)
			return;

		if(player.isDead())
		{
			player.setCurrentHp(player.getMaxHp(), true);
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentMp(player.getMaxMp());
			player.broadcastPacket(new Revive(player));
		}

		// бафаем
		L2EventTeam.addEventBuffs(player, this);

		final Location pos = Rnd.coordsRandomize(149505, 46719, -3417, 0, 0, 500);
		player.teleToLocation(pos, ReflectionTable.EVENT_DEATHMATCH);
	}

	private static void removePlayer(final L2Player player)
	{
		participants.removePlayer(player);
	}

	private static void openColiseumDoors()
	{
		_reflection.openDoors(_doors);
	}

	private static void closeColiseumDoors()
	{
		_reflection.closeDoors(_doors);
	}

	/**
	 * @see l2n.game.event.L2Event#isActive()
	 */
	@Override
	public boolean isActive()
	{
		return _active;
	}

	/**
	 * @see l2n.game.event.L2Event#getEventType()
	 */
	@Override
	public L2EventType getEventType()
	{
		return L2EventType.DEATHMATCH;
	}
}
