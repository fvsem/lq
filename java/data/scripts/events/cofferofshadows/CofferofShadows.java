package events.cofferofshadows;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.cache.Msg;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Util;

// Эвент Coffer of Shadows
public class CofferofShadows extends EventScript implements ScriptFile
{
	private final static int COFFER_PRICE = 50000; // 50.000 adena at x1 servers
	private final static int COFFER_ID = 8659;
	private final static int EVENT_MANAGER_ID = 32091;
	private final static GArray<L2Spawn> _spawns = new GArray<L2Spawn>();

	private static boolean _active = false;

	/**
	 * Спавнит эвент менеджеров
	 */
	private void spawnEventManagers()
	{
		final int EVENT_MANAGERS[][] = { { -14823, 123567, -3143, 8192 }, // Gludio
				{ -83159, 150914, -3155, 49152 }, // Gludin
				{ 18600, 145971, -3095, 7400 }, // Dion
				{ 82158, 148609, -3493, 60 }, // Giran
				{ 110992, 218753, -3568, 0 }, // Hiene
				{ 116339, 75424, -2738, 0 }, // Hunter Village
				{ 81140, 55218, -1551, 32768 }, // Oren
				{ 147148, 27401, -2231, 2300 }, // Aden
				{ 43532, -46807, -823, 57344 }, // Rune
				{ 87765, -141947, -1367, 6500 }, // Schuttgart
				{ 147154, -55527, -2807, 61300 } // Goddard
		};

		L2NpcTemplate template = NpcTable.getTemplate(EVENT_MANAGER_ID);
		for(int[] element : EVENT_MANAGERS)
			try
			{
				L2Spawn sp = new L2Spawn(template);
				sp.setLocx(element[0]);
				sp.setLocy(element[1]);
				sp.setLocz(element[2]);
				sp.setAmount(1);
				sp.setHeading(element[3]);
				sp.setRespawnDelay(0);
				sp.init();
				_spawns.add(sp);
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
	}

	/**
	 * Удаляет спавн эвент менеджеров
	 */
	private void unSpawnEventManagers()
	{
		for(L2Spawn sp : _spawns)
		{
			sp.stopRespawn();
			sp.getLastSpawn().deleteMe();
		}
		_spawns.clear();
	}

	/**
	 * Запускает эвент
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("CofferofShadows", true))
		{
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Event: Coffer of Shadows started.");
			Announcements.announceByCustomMessage("scripts.events.cofferofshadows.CofferofShadows.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event 'Coffer of Shadows' already started.");

		_active = true;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Останавливает эвент
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("CofferofShadows", false))
		{
			removeEventId(ScriptEventType.ON_ENTER_WORLD);
			unSpawnEventManagers();
			System.out.println("Event: Coffer of Shadows stopped.");
			Announcements.announceByCustomMessage("scripts.events.cofferofshadows.CofferofShadows.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event 'Coffer of Shadows' not started.");

		_active = false;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Продает 1 сундук игроку
	 */
	public void buycoffer(String[] var)
	{
		L2Player player = (L2Player) getSelf();

		if(!player.isQuestContinuationPossible(true))
			return;

		if(player.isActionsDisabled() || player.isSitting() || player.getLastNpc() == null || player.getLastNpc().getDistance(player) > 300)
			return;

		int coffer_count = 1;
		try
		{
			coffer_count = Integer.valueOf(var[0]);
		}
		catch(Exception E)
		{}

		int need_adena = COFFER_PRICE * Config.EVENT_CofferOfShadowsPriceRate * coffer_count;
		if(player.getAdena() < need_adena)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		player.reduceAdena(need_adena, true);
		L2ItemInstance createditem = ItemTable.getInstance().createItem(COFFER_ID, player.getObjectId(), 0, "CofferofShadows");
		createditem.setCount(coffer_count);
		player.getInventory().addItem(createditem);
		player.sendPacket(new ItemList(player, true));
		if(coffer_count > 1)
			player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S).addItemName(COFFER_ID).addNumber(coffer_count));
		else
			player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addItemName(COFFER_ID));
	}

	/**
	 * Добавляет в диалоги эвент менеджеров строчку с байпасом для покупки сундука
	 */

	private static int[] buycoffer_counts = { 1, 5, 10, 50 }; // TODO в конфиг

	public String DialogAppend_32091(Integer val)
	{
		if(val != 0)
			return "";

		String price;
		String append = "";
		for(int cnt : buycoffer_counts)
		{
			price = Util.formatAdena(COFFER_PRICE * Config.EVENT_CofferOfShadowsPriceRate * cnt);
			append += "<br><a action=\"bypass -h scripts_events.cofferofshadows.CofferofShadows:buycoffer " + cnt + "\">";
			if(cnt == 1)
				append += new CustomMessage("scripts.events.cofferofshadows.CofferofShadows.buycoffer", getSelf()).addString(price);
			else
				append += new CustomMessage("scripts.events.cofferofshadows.CofferofShadows.buycoffers", getSelf()).addNumber(cnt).addString(price);
			append += "</a><br>";
		}

		return append;
	}

	@Override
	public void onLoad()
	{
		if(isActive("CofferofShadows"))
		{
			_active = true;
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Loaded Event: Coffer of Shadows [state: activated]");
		}
		else
			System.out.println("Loaded Event: Coffer of Shadows [state: deactivated]");
	}

	@Override
	public void onReload()
	{
		unSpawnEventManagers();
	}

	@Override
	public void onShutdown()
	{
		unSpawnEventManagers();
	}

	@Override
	protected void onEnterWorld(L2Player player)
	{
		if(_active)
			Announcements.announceToPlayerByCustomMessage(player, "scripts.events.cofferofshadows.CofferofShadows.AnnounceEventStarted", null);
	}
}
