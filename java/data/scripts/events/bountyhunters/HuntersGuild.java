package events.bountyhunters;

import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.handler.VoicedCommandHandler;
import l2n.game.handler.interfaces.IVoicedCommandHandler;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.*;
import l2n.game.scripts.npc.KanabionInstance;
import l2n.game.scripts.npc.QueenAntLarvaInstance;
import l2n.game.scripts.npc.SquashInstance;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Rnd;

public class HuntersGuild extends EventScript implements ScriptFile, IVoicedCommandHandler
{
	private static final String[] _commandList = new String[] {
			"gettask",
			"declinetask" };

	@Override
	public void onLoad()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
		System.out.println("Loaded Event: Bounty Hunters Guild");
		addEventId(ScriptEventType.ON_DIE);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static boolean checkTarget(final L2NpcTemplate npc)
	{
		if(!npc.isInstanceOf(L2MonsterInstance.class))
			return false;
		if(npc.revardExp == 0)
			return false;
		if(npc.isInstanceOf(L2RaidBossInstance.class))
			return false;
		if(npc.isInstanceOf(QueenAntLarvaInstance.class))
			return false;
		if(npc.isInstanceOf(SquashInstance.class))
			return false;
		if(npc.isInstanceOf(L2PenaltyMonsterInstance.class))
			return false;
		if(npc.isInstanceOf(L2MinionInstance.class))
			return false;
		if(npc.isInstanceOf(L2FestivalMonsterInstance.class))
			return false;
		if(npc.isInstanceOf(L2TamedBeastInstance.class))
			return false;
		if(npc.isInstanceOf(L2DeadManInstance.class))
			return false;
		if(npc.isInstanceOf(L2ChestInstance.class))
			return false;
		if(npc.isInstanceOf(KanabionInstance.class))
			return false;
		if(npc.title.contains("Quest Monster"))
			return false;
		if(L2ObjectsStorage.getByNpcId(npc.getNpcId()) == null)
			return false;
		return true;
	}

	public void getTask(final L2Player player)
	{
		final GArray<L2NpcTemplate> monsters = NpcTable.getAllOfLevel(player.getLevel());
		if(monsters == null || monsters.isEmpty())
		{
			show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.NoTargets", player), player);
			return;
		}
		final GArray<L2NpcTemplate> targets = new GArray<L2NpcTemplate>();
		for(final L2NpcTemplate npc : monsters)
			if(checkTarget(npc))
				targets.add(npc);
		if(targets.isEmpty())
		{
			show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.NoTargets", player), player);
			return;
		}
		final L2NpcTemplate target = targets.get(Rnd.get(targets.size()));
		final int mobcount = target.level + Rnd.get(25, 50);
		player.setVar("bhMonstersId", String.valueOf(target.getNpcId()));
		player.setVar("bhMonstersNeeded", String.valueOf(mobcount));
		player.setVar("bhMonstersKilled", "0");

		final int fails = player.getVar("bhfails") == null ? 0 : Integer.parseInt(player.getVar("bhfails")) * 5;
		final int success = player.getVar("bhsuccess") == null ? 0 : Integer.parseInt(player.getVar("bhsuccess")) * 5;

		final double reputation = Math.min(Math.max((100 + success - fails) / 100., .5), 2.);

		final int adenarewardvalue = (int) ((target.level * Math.max(Math.log(target.level), 1) * 10 + Math.max((target.level - 60) * 33, 0) + Math.max((target.level - 65) * 50, 0)) * target.rateHp * mobcount * player.getRateAdena() * reputation);
		int rewardid = 0;
		int rewardcount = 0;
		final int random = Rnd.get(1, 100);
		if(random <= 30) // Адена, 30% случаев
		{
			player.setVar("bhRewardId", "57");
			player.setVar("bhRewardCount", String.valueOf(adenarewardvalue));
			rewardcount = adenarewardvalue;
			rewardid = 57;
		}
		else if(random <= 100) // Кристаллы, 70% случаев
		{
			int crystal = 0;
			if(target.level <= 39)
				crystal = 1458; // D
			else if(target.level <= 51)
				crystal = 1459; // C
			else if(target.level <= 60)
				crystal = 1460; // B
			else if(target.level <= 74)
				crystal = 1461; // A
			else
				crystal = 1462; // S
			player.setVar("bhRewardId", String.valueOf(crystal));
			player.setVar("bhRewardCount", String.valueOf(adenarewardvalue / ItemTable.getInstance().getTemplate(crystal).getReferencePrice()));
			rewardcount = adenarewardvalue / ItemTable.getInstance().getTemplate(crystal).getReferencePrice();
			rewardid = crystal;
		}
		show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.TaskGiven", player).addNumber(mobcount).addString(target.name).addNumber(rewardcount).addItemName(rewardid), player);
	}

	@Override
	protected final void onDie(final L2Character cha, final L2Character killer)
	{
		if(cha.isMonster() && !cha.isRaid() && killer != null && killer.getPlayer() != null && killer.getPlayer().getVar("bhMonstersId") != null && Integer.parseInt(killer.getPlayer().getVar("bhMonstersId")) == cha.getNpcId())
		{
			final int count = Integer.parseInt(killer.getPlayer().getVar("bhMonstersKilled")) + 1;
			killer.getPlayer().setVar("bhMonstersKilled", String.valueOf(count));
			final int needed = Integer.parseInt(killer.getPlayer().getVar("bhMonstersNeeded"));
			if(count >= needed)
				doReward(killer.getPlayer());
			else
				sendMessage(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.NotifyKill", killer.getPlayer()).addNumber(needed - count), killer.getPlayer());
		}
	}

	private static void doReward(final L2Player player)
	{
		final int rewardid = Integer.parseInt(player.getVar("bhRewardId"));
		final int rewardcount = (int) Double.parseDouble(player.getVar("bhRewardCount"));
		player.unsetVar("bhMonstersId");
		player.unsetVar("bhMonstersNeeded");
		player.unsetVar("bhMonstersKilled");
		player.unsetVar("bhRewardId");
		player.unsetVar("bhRewardCount");
		if(player.getVar("bhsuccess") != null)
			player.setVar("bhsuccess", String.valueOf(Integer.parseInt(player.getVar("bhsuccess")) + 1));
		else
			player.setVar("bhsuccess", "1");
		addItem(player, rewardid, rewardcount);
		show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.TaskCompleted", player).addNumber(rewardcount).addItemName(rewardid), player);
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(final String command, final L2Player activeChar, final String target)
	{
		if(activeChar == null)
			return false;
		if(activeChar.getLevel() < 20)
		{
			sendMessage(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.TooLowLevel", activeChar), activeChar);
			return true;
		}
		if(command.equalsIgnoreCase("gettask"))
		{
			if(activeChar.getVar("bhMonstersId") != null)
			{
				final int mobid = Integer.parseInt(activeChar.getVar("bhMonstersId"));
				final int mobcount = Integer.parseInt(activeChar.getVar("bhMonstersNeeded")) - Integer.parseInt(activeChar.getVar("bhMonstersKilled"));
				final int rewardid = Integer.parseInt(activeChar.getVar("bhRewardId"));
				final int rewardcount = (int) Double.parseDouble(activeChar.getVar("bhRewardCount"));
				show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.TaskGiven", activeChar).addNumber(mobcount).addString(NpcTable.getTemplate(mobid).name).addNumber(rewardcount).addItemName(rewardid), activeChar);
				return true;
			}
			getTask(activeChar);
			return true;
		}
		if(command.equalsIgnoreCase("declinetask"))
		{
			if(activeChar.getVar("bhMonstersId") == null)
			{
				sendMessage(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.NoTask", activeChar), activeChar);
				return true;
			}
			activeChar.unsetVar("bhMonstersId");
			activeChar.unsetVar("bhMonstersNeeded");
			activeChar.unsetVar("bhMonstersKilled");
			activeChar.unsetVar("bhRewardId");
			activeChar.unsetVar("bhRewardCount");
			if(activeChar.getVar("bhfails") != null)
				activeChar.setVar("bhfails", String.valueOf(Integer.parseInt(activeChar.getVar("bhfails")) + 1));
			else
				activeChar.setVar("bhfails", "1");
			show(new CustomMessage("scripts.events.bountyhunters.HuntersGuild.TaskCanceled", activeChar), activeChar);
			return true;
		}
		return false;
	}
}
