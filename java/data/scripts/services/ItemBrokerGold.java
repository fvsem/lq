package services;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.RecipeController;
import l2n.game.geodata.GeoEngine;
import l2n.game.geodata.GeoMove;
import l2n.game.model.*;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance.ItemClass;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.ExShowTrace;
import l2n.game.network.serverpackets.RadarControl;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Location;
import l2n.util.StringUtil;
import l2n.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ItemBrokerGold extends Functions implements ScriptFile
{
	private final static ConcurrentHashMap<Integer, NpcItemsCache> _npcInfos = new ConcurrentHashMap<Integer, NpcItemsCache>();

	public static class NpcItemsCache
	{
		public long lastUpdate;
		private TreeMap<String, TreeMap<Long, Item>> bestSellItems;
		private TreeMap<String, TreeMap<Long, Item>> bestBuyItems;
		private TreeMap<String, TreeMap<Long, Item>> bestCraftItems;

		public final TreeMap<String, TreeMap<Long, Item>> getItems(final int type)
		{
			switch (type)
			{
				case L2Player.STORE_PRIVATE_SELL:
					return bestSellItems;
				case L2Player.STORE_PRIVATE_BUY:
					return bestBuyItems;
				case L2Player.STORE_PRIVATE_MANUFACTURE:
					return bestCraftItems;
			}
			_log.warning("ItemBrokerGold: wrong type for getItems '" + type + "'.");
			return null;
		}
	}

	public static class Item
	{
		private final int itemId;
		private final int itemObjId;
		private final int type;
		private final long price;
		private final long count;
		private final long enchant;
		private final String name;
		private final String merchantName;
		private final long merchantStoredId;
		private final List<Location> path;

		private Item(final int itemId, final int itemObjId, final int type, final long price, final long count, final long enchant, final String name, final String merchantName, final long merchantStoredId, final List<Location> path)
		{
			this.itemId = itemId;
			this.itemObjId = itemObjId;
			this.type = type;
			this.price = price;
			this.count = count;
			this.enchant = enchant;
			this.name = name;
			this.merchantName = merchantName;
			this.merchantStoredId = merchantStoredId;
			this.path = path;
		}
	}

	public String DialogAppend_32320(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_32321(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public String DialogAppend_32322(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public String getHtmlAppends(final Integer val)
	{
		final L2Player player = getSelfPlayer();
		if(player == null)
			return null;

		final StringBuilder append = StringUtil.startAppend(300);
		int type = 0;
		String typeNameRu = "";
		String typeNameEn = "";

		switch (val)
		{
			case 0:
				if(!player.isLangRus())
					StringUtil.append(append,
							"<br><font color=\"LEVEL\">Search for dealers:</font><br1>",
							"[npc_%objectId%_Chat 11|<font color=\"FF9900\">The list of goods for sale</font>]<br1>",
							"[npc_%objectId%_Chat 13|<font color=\"FF9900\">The list of goods to buy</font>]<br1>",
							"[npc_%objectId%_Chat 15|<font color=\"FF9900\">The list of goods to craft</font>]<br1>");
				else
					StringUtil.append(append,
							"<br><font color=\"LEVEL\">\u041f\u043e\u0438\u0441\u043a \u0442\u043e\u0440\u0433\u043e\u0432\u0446\u0435\u0432:</font><br1>",
							"[npc_%objectId%_Chat 11|<font color=\"FF9900\">\u0421\u043f\u0438\u0441\u043e\u043a \u043f\u0440\u043e\u0434\u0430\u0432\u0430\u0435\u043c\u044b\u0445 \u0442\u043e\u0432\u0430\u0440\u043e\u0432</font>]<br1>",
							"[npc_%objectId%_Chat 13|<font color=\"FF9900\">\u0421\u043f\u0438\u0441\u043e\u043a \u043f\u043e\u043a\u0443\u043f\u0430\u0435\u043c\u044b\u0445 \u0442\u043e\u0432\u0430\u0440\u043e\u0432</font>]<br1>",
							"[npc_%objectId%_Chat 15|<font color=\"FF9900\">\u0421\u043f\u0438\u0441\u043e\u043a \u0441\u043e\u0437\u0434\u0430\u0432\u0430\u0435\u043c\u044b\u0445 \u0442\u043e\u0432\u0430\u0440\u043e\u0432</font>]<br1>");
				break;
			case 11:
				type = L2Player.STORE_PRIVATE_SELL;
				typeNameRu = "\u043f\u0440\u043e\u0434\u0430\u0432\u0430\u0435\u043c\u044b\u0445";
				typeNameEn = "sell";
				break;
			case 13:
				type = L2Player.STORE_PRIVATE_BUY;
				typeNameRu = "\u043f\u043e\u043a\u0443\u043f\u0430\u0435\u043c\u044b\u0445";
				typeNameEn = "buy";
				break;
		}

		if(type > 0)
			if(!player.isLangRus())
			{
				append.append("!The list of goods to ").append(typeNameEn).append(":<br>");
				if(type == L2Player.STORE_PRIVATE_MANUFACTURE)
				{
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 0 1 1 0 0|<font color=\"FF9900\">List all</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 0|<font color=\"FF9900\">Equipment</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 2 1 1 0 0|<font color=\"FF9900\">Consumable</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 3 1 1 0 0|<font color=\"FF9900\">Matherials</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 4 1 1 0 0|<font color=\"FF9900\">Key matherials</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 7 1 1 0 0|<font color=\"FF9900\">Enchant items</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 8 1 1 0 0|<font color=\"FF9900\">Other</font>]<br1>");
				}
				else
				{
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 0 1 1 0 0|<font color=\"FF9900\">List all</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 0|<font color=\"FF9900\">Equipment</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 1 0|<font color=\"FF9900\">Equipment+</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 1|<font color=\"FF9900\">Rare equipment</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 2 1 1 0 0|<font color=\"FF9900\">Consumable</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 3 1 1 0 0|<font color=\"FF9900\">Matherials</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 4 1 1 0 0|<font color=\"FF9900\">Key matherials</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 5 1 1 0 0|<font color=\"FF9900\">Recipies</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 6 1 1 0 0|<font color=\"FF9900\">Books and amulets</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 7 1 1 0 0|<font color=\"FF9900\">Enchant items</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 8 1 1 0 0|<font color=\"FF9900\">Other</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 9 1 1 0 0|<font color=\"FF9900\">Commons</font>]<br1>");
				}

				append.append("<br>[npc_%objectId%_Chat 0|<font color=\"FF9900\">Back</font>]");
			}
			else
			{
				append.append("!\u0421\u043f\u0438\u0441\u043e\u043a ").append(typeNameRu).append(" \u0442\u043e\u0432\u0430\u0440\u043e\u0432:<br>");
				if(type == L2Player.STORE_PRIVATE_MANUFACTURE)
				{
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 0 1 1 0 0|<font color=\"FF9900\">\u0412\u0435\u0441\u044c \u0441\u043f\u0438\u0441\u043e\u043a</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 0|<font color=\"FF9900\">\u0421\u043d\u0430\u0440\u044f\u0436\u0435\u043d\u0438\u0435</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 2 1 1 0 0|<font color=\"FF9900\">\u0420\u0430\u0441\u0445\u043e\u0434\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 3 1 1 0 0|<font color=\"FF9900\">\u0418\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 4 1 1 0 0|<font color=\"FF9900\">\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 7 1 1 0 0|<font color=\"FF9900\">\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b \u0434\u043b\u044f \u0443\u043b\u0443\u0447\u0448\u0435\u043d\u0438\u044f</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 8 1 1 0 0|<font color=\"FF9900\">\u0420\u0430\u0437\u043d\u043e\u0435</font>]<br1>");
				}
				else
				{
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 0 1 1 0 0|<font color=\"FF9900\">\u0412\u0435\u0441\u044c \u0441\u043f\u0438\u0441\u043e\u043a</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 0|<font color=\"FF9900\">\u0421\u043d\u0430\u0440\u044f\u0436\u0435\u043d\u0438\u0435</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 1 0|<font color=\"FF9900\">\u0421\u043d\u0430\u0440\u044f\u0436\u0435\u043d\u0438\u0435+</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 1 1 1 0 1|<font color=\"FF9900\">\u0420\u0435\u0434\u043a\u043e\u0435 \u0441\u043d\u0430\u0440\u044f\u0436\u0435\u043d\u0438\u0435</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 2 1 1 0 0|<font color=\"FF9900\">\u0420\u0430\u0441\u0445\u043e\u0434\u043d\u044b\u0435 \u043c\u0430\u0442\u0435\u0440\u0438\u0430\u043b\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 3 1 1 0 0|<font color=\"FF9900\">\u0418\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 4 1 1 0 0|<font color=\"FF9900\">\u041a\u043b\u044e\u0447\u0435\u0432\u044b\u0435 \u0438\u043d\u0433\u0440\u0435\u0434\u0438\u0435\u043d\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 5 1 1 0 0|<font color=\"FF9900\">\u0420\u0435\u0446\u0435\u043f\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 6 1 1 0 0|<font color=\"FF9900\">\u041a\u043d\u0438\u0433\u0438 \u0438 \u0430\u043c\u0443\u043b\u0435\u0442\u044b</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 7 1 1 0 0|<font color=\"FF9900\">\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b \u0434\u043b\u044f \u0443\u043b\u0443\u0447\u0448\u0435\u043d\u0438\u044f</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 8 1 1 0 0|<font color=\"FF9900\">\u0420\u0430\u0437\u043d\u043e\u0435</font>]<br1>");
					append.append("[scripts_services.ItemBrokerGold:list ").append(type).append(" 9 1 1 0 0|<font color=\"FF9900\">\u0421\u0442\u0430\u043d\u0434\u0430\u0440\u0442\u043d\u044b\u0435 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u044b</font>]<br1>");
				}
				append.append("<br>[npc_%objectId%_Chat 0|<font color=\"FF9900\">\u041d\u0430\u0437\u0430\u0434</font>]");
			}

		return append.toString();
	}

	public void list(final String[] var)
	{
		final int countPerPage = 9;

		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(var.length != 6)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		int type;
		int itemType;
		int min;
		int max;
		int minEnchant;
		int rare;

		try
		{
			type = Integer.valueOf(var[0]);
			itemType = Integer.valueOf(var[1]);
			min = Integer.valueOf(var[2]);
			max = Integer.valueOf(var[3]);
			minEnchant = Integer.valueOf(var[4]);
			rare = Integer.valueOf(var[5]);
		}
		catch(final Exception e)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		if(max < countPerPage)
			max = countPerPage;

		final ItemClass itemClass = itemType > 8 ? null : ItemClass.values()[itemType];

		updateInfo(player, npc);
		final NpcItemsCache info = _npcInfos.get(npc.getObjectId());

		final TreeMap<String, TreeMap<Long, Item>> allItems = info.getItems(type);
		if(allItems == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final GArray<Item> items = new GArray<Item>();
		for(final TreeMap<Long, Item> tempItems : allItems.values())
		{
			final TreeMap<Long, Item> tempItems2 = new TreeMap<Long, Item>();
			for(final Entry<Long, Item> entry : tempItems.entrySet())
			{
				final Item tempItem = entry.getValue();
				if(tempItem == null)
					continue;
				if(tempItem.enchant < minEnchant)
					continue;
				final L2Item temp = ItemTable.getInstance().getTemplate(tempItem.itemId);
				if(temp == null || rare > 0 && !temp.isRare())
					continue;
				if(itemClass == null ? !temp.isCommonItem() : temp.isCommonItem())
					continue;
				if(itemClass != null && itemClass != ItemClass.ALL && temp.getItemClass() != itemClass)
					continue;
				tempItems2.put(entry.getKey(), tempItem);
			}
			if(tempItems2.isEmpty())
				continue;

			final Item item = type == L2Player.STORE_PRIVATE_BUY ? tempItems2.lastEntry().getValue() : tempItems2.firstEntry().getValue();
			if(item != null)
				items.add(item);
		}

		final StringBuilder out = StringUtil.startAppend(300, "[npc_%objectId%_Chat 1", type, "|\u00ab\u00ab Back]&nbsp;");
		final int pages = Math.max(1, items.size() / countPerPage + 1);
		if(pages > 1)
			for(int j = 1; j <= pages; j++)
				if(min == (j - 1) * countPerPage + 1)
					StringUtil.append(out, j, "&nbsp;");
				else
					StringUtil.append(out, "[scripts_services.ItemBrokerGold:list ", type, " ", itemType, " ", (j - 1) * countPerPage + 1, " ", j * countPerPage, " ", minEnchant, " ", rare, "|", j, "]&nbsp;");
		StringUtil.append(out, "<table width=100%>");

		int i = 0;
		for(final Item item : items)
		{
			i++;
			if(i < min || i > max)
				continue;
			final L2Item temp = ItemTable.getInstance().getTemplate(item.itemId);
			if(temp == null)
				continue;

			final String icon = "<img src=icon." + temp.getIcon() + " width=32 height=32>";

			String color = "<font color=\"LEVEL\">";
			if(item.enchant > 0)
				color = "<font color=\"7CFC00\">+" + item.enchant + " ";
			if(temp.isRare())
				color = "<font color=\"0000FF\">Rare ";
			if(temp.isRare() && item.enchant > 0)
				color = "<font color=\"FF0000\">+" + item.enchant + " Rare ";

			out.append("<tr><td>").append(icon);
			out.append("</td><td><table width=100%><tr><td>[scripts_services.ItemBrokerGold:listForItem ").append(type).append(" ").append(item.itemId).append(" ").append(minEnchant).append(" ").append(rare).append(" ").append(itemType).append(" ").append(min).append(" ").append(max).append("|");
			out.append(color).append(item.name).append("</font>]</td></tr><tr><td>price: ").append(Util.formatAdena(item.price));
			if(type == L2Player.STORE_PRIVATE_MANUFACTURE)
				out.append(", rate: ").append(item.count).append("%");
			else if(temp.isStackable())
				out.append(", count: ").append(Util.formatAdena(item.count));

			out.append("</td></tr></table></td></tr>");
		}
		out.append("</table><br>&nbsp;");

		show(out.toString(), player);
	}

	public void listForItem(final String[] var)
	{
		final int maxItems = 20;

		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(var.length != 7)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		int type;
		int itemId;
		int minEnchant;
		int rare;
		// \u043d\u0430 \u043a\u0430\u043a\u0443\u044e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0432\u043e\u0437\u0432\u0440\u0430\u0449\u0430\u0442\u044c\u0441\u044f
		int itemType;
		int min;
		int max;
		try
		{
			type = Integer.valueOf(var[0]);
			itemId = Integer.valueOf(var[1]);
			minEnchant = Integer.valueOf(var[2]);
			rare = Integer.valueOf(var[3]);
			itemType = Integer.valueOf(var[4]);
			min = Integer.valueOf(var[5]);
			max = Integer.valueOf(var[6]);
		}
		catch(final Exception e)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		final L2Item template = ItemTable.getInstance().getTemplate(itemId);
		if(template == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}
		final String itemName = template.getName();

		final NpcItemsCache info = _npcInfos.get(npc.getObjectId());

		final TreeMap<String, TreeMap<Long, Item>> allItems = info.getItems(type);
		if(allItems == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final TreeMap<Long, Item> items = allItems.get(itemName);
		if(items == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final StringBuilder out = StringUtil.startAppend(500, "[scripts_services.ItemBrokerGold:list ", type, " ", itemType, " ", min, " ", max, " ", minEnchant, " ", rare, "|\u00ab\u00ab Back]", "<table width=100%>");

		final NavigableMap<Long, Item> sortedItems = type == L2Player.STORE_PRIVATE_SELL ? items : items.descendingMap();
		if(sortedItems == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		int i = 0;
		for(final Item item : sortedItems.values())
		{
			if(item.enchant < minEnchant)
				continue;
			final L2Item temp = ItemTable.getInstance().getTemplate(item.itemId);
			if(temp == null || rare > 0 && !temp.isRare())
				continue;

			i++;
			if(i > maxItems)
				break;

			final String icon = "<img src=icon." + temp.getIcon() + " width=32 height=32>";

			String color = "<font color=\"LEVEL\">";
			if(item.enchant > 0)
				color = "<font color=\"7CFC00\">+" + item.enchant + " ";
			if(temp.isRare())
				color = "<font color=\"0000FF\">Rare ";
			if(temp.isRare() && item.enchant > 0)
				color = "<font color=\"FF0000\">+" + item.enchant + " Rare ";

			StringUtil.append(out, "<tr><td>", icon, "</td><td><table width=100%><tr><td>[scripts_services.ItemBrokerGold:path ", type, " ", item.itemId, " ", item.itemObjId, "|", color, item.name, "</font>]</td></tr><tr><td>price: ", Util.formatAdena(item.price));

			if(type == L2Player.STORE_PRIVATE_MANUFACTURE)
				StringUtil.append(out, ", rate: ", item.count, "%");
			else if(temp.isStackable())
				StringUtil.append(out, ", count: ", Util.formatAdena(item.count));

			StringUtil.append(out, ", owner: ", item.merchantName, "</td></tr></table></td></tr>");
		}
		StringUtil.append(out, "</table><br>&nbsp;");

		show(out.toString(), player);
	}

	public void path(final String[] var)
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(var.length != 3)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		int type;
		int itemId;
		int itemObjId;

		try
		{
			type = Integer.valueOf(var[0]);
			itemId = Integer.valueOf(var[1]);
			itemObjId = Integer.valueOf(var[2]);
		}
		catch(final Exception e)
		{
			show("\u041d\u0435\u043a\u043e\u0440\u0440\u0435\u043a\u0442\u043d\u044b\u0435 \u0434\u0430\u043d\u043d\u044b\u0435", player);
			return;
		}

		final L2Item temp = ItemTable.getInstance().getTemplate(itemId);
		if(temp == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final String itemName = temp.getName();

		final NpcItemsCache info = _npcInfos.get(npc.getObjectId());
		if(info == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final TreeMap<String, TreeMap<Long, Item>> allItems = info.getItems(type);
		if(allItems == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		final TreeMap<Long, Item> items = allItems.get(itemName);
		if(items == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		Item item = null;
		for(final Item i : items.values())
			if(i.itemObjId == itemObjId)
			{
				item = i;
				break;
			}

		if(item == null)
		{
			show("\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u0430\u044f \u043e\u0448\u0438\u0431\u043a\u0430", player);
			return;
		}

		// player.sendPacket(Points2Trace(player, item.path, 50, 60000));

		final L2Player trader = L2ObjectsStorage.getAsPlayer(item.merchantStoredId);
		if(trader == null)
		{
			show("\u0422\u043e\u0440\u0433\u043e\u0432\u0435\u0446 \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d", player);
			return;
		}

		player.sendPacket(new RadarControl(2, 2, trader.getX(), trader.getY(), trader.getZ()), new RadarControl(0, 1, trader.getX(), trader.getY(), trader.getZ()));
		player.setTarget(trader);
		if(player.getVarB("notraders"))
			player.sendPacket(trader.newCharInfo());
	}

	public void updateInfo(final L2Player player, final L2NpcInstance npc)
	{
		NpcItemsCache info = _npcInfos.get(npc.getObjectId());
		if(info == null || info.lastUpdate < System.currentTimeMillis() - 300000)
		{
			info = new NpcItemsCache();
			info.lastUpdate = System.currentTimeMillis();
			info.bestBuyItems = new TreeMap<String, TreeMap<Long, Item>>();
			info.bestSellItems = new TreeMap<String, TreeMap<Long, Item>>();
			info.bestCraftItems = new TreeMap<String, TreeMap<Long, Item>>();

			int itemObjId = 0; // \u041e\u0431\u044b\u0447\u043d\u044b\u0439 objId \u043d\u0435 \u043f\u043e\u0434\u0445\u043e\u0434\u0438\u0442 \u0434\u043b\u044f \u043f\u043e\u043a\u0443\u043f\u0430\u0435\u043c\u044b\u0445 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u043e\u0432

			ConcurrentLinkedQueue<TradeItem> tradeList = null;
			TreeMap<String, TreeMap<Long, Item>> items = null;
			L2ManufactureList createList = null;
			Item newItem = null;

			for(final L2Player pl : L2World.getAroundPlayers(npc, 4000, 400))
			{
				final int type = pl.getPrivateStoreType();
				if(type == L2Player.STORE_PRIVATE_SELL || type == L2Player.STORE_PRIVATE_BUY || type == L2Player.STORE_PRIVATE_MANUFACTURE)
				{
					List<Location> path = new ArrayList<Location>();
					if(GeoEngine.canMoveToCoord(npc.getX(), npc.getY(), npc.getZ(), pl.getX(), pl.getY(), pl.getZ()))
					{
						path.add(npc.getLoc());
						path.add(pl.getLoc());
					}
					else
						path = GeoMove.findPath(npc.getX(), npc.getY(), npc.getZ(), new Location(pl), player, false);

					if(!path.isEmpty())
						switch (type)
						{
							case L2Player.STORE_PRIVATE_SELL:
							{
								items = info.bestSellItems;
								tradeList = pl.getSellList();
								if(tradeList == null)
									continue;

								for(final TradeItem item : tradeList)
								{
									final L2Item temp = ItemTable.getInstance().getTemplate(item.getItemId());
									if(temp == null)
										continue;
									TreeMap<Long, Item> oldItems = items.get(temp.getName());
									if(oldItems == null)
									{
										oldItems = new TreeMap<Long, Item>();
										items.put(temp.getName(), oldItems);
									}
									// private Item(int itemId, int itemObjId, int type, long price, long count, long enchant, String name, String merchantName, long merchantStoredId, List<Location> path)
									newItem = new Item(item.getItemId(), item.getObjectId(), type, item.getOwnersPrice(), item.getCount(), item.getEnchantLevel(), temp.getName(), pl.getName(), pl.getStoredId(), path);
									long key = newItem.price * 100;
									while (key < newItem.price * 100 + 100 && oldItems.containsKey(key))
										// \u0414\u043e 100 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u043e\u0432 \u0441 \u043e\u0434\u0438\u043d\u0430\u043a\u043e\u0432\u044b\u043c\u0438 \u0446\u0435\u043d\u0430\u043c\u0438
										key++;
									oldItems.put(key, newItem);
								}
								break;
							}
							case L2Player.STORE_PRIVATE_BUY:
							{
								items = info.bestBuyItems;
								tradeList = pl.getBuyList();
								if(tradeList == null)
									continue;

								for(final TradeItem item : tradeList)
								{
									final L2Item temp = ItemTable.getInstance().getTemplate(item.getItemId());
									if(temp == null)
										continue;
									TreeMap<Long, Item> oldItems = items.get(temp.getName());
									if(oldItems == null)
									{
										oldItems = new TreeMap<Long, Item>();
										items.put(temp.getName(), oldItems);
									}
									// private Item(int itemId, int itemObjId, int type, long price, long count, long enchant, String name, String merchantName, long merchantStoredId, List<Location> path)
									newItem = new Item(item.getItemId(), itemObjId++, type, item.getOwnersPrice(), item.getCount(), item.getEnchantLevel(), temp.getName(), pl.getName(), pl.getStoredId(), path);
									long key = newItem.price * 100;
									while (key < newItem.price * 100 + 100 && oldItems.containsKey(key))
										// \u0414\u043e 100 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u043e\u0432 \u0441 \u043e\u0434\u0438\u043d\u0430\u043a\u043e\u0432\u044b\u043c\u0438 \u0446\u0435\u043d\u0430\u043c\u0438
										key++;
									oldItems.put(key, newItem);
								}
								break;
							}
							case L2Player.STORE_PRIVATE_MANUFACTURE:
							{
								items = info.bestCraftItems;
								createList = pl.getCreateList();
								if(createList == null)
									continue;

								for(final L2ManufactureItem mitem : createList.getList())
								{
									final int recipeId = mitem.getRecipeId();
									final L2RecipeList recipe = RecipeController.getInstance().getRecipeList(recipeId);
									if(recipe == null)
										continue;

									final L2Item temp = ItemTable.getInstance().getTemplate(recipe.getItemId());
									if(temp == null)
										continue;

									TreeMap<Long, Item> oldItems = items.get(temp.getName());
									if(oldItems == null)
									{
										oldItems = new TreeMap<Long, Item>();
										items.put(temp.getName(), oldItems);
									}
									// private Item(int itemId, int itemObjId, int type, long price, long count, long enchant, String name, String merchantName, long merchantStoredId, List<Location> path)
									newItem = new Item(recipe.getItemId(), itemObjId++, type, mitem.getCost(), recipe.getSuccessRate(), 0, temp.getName(), pl.getName(), pl.getStoredId(), path);
									long key = newItem.price * 100;
									while (key < newItem.price * 100 + 100 && oldItems.containsKey(key))
										// \u0414\u043e 100 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u043e\u0432 \u0441 \u043e\u0434\u0438\u043d\u0430\u043a\u043e\u0432\u044b\u043c\u0438 \u0446\u0435\u043d\u0430\u043c\u0438
										key++;
									oldItems.put(key, newItem);
								}
								break;
							}
							default:
								continue;
						}
				}
			}
			_npcInfos.put(npc.getObjectId(), info);
		}
	}

	public static ExShowTrace Points2Trace(final L2Player player, final List<Location> points, final int step, final int time)
	{
		final ExShowTrace result = new ExShowTrace();
		Location _prev = null;
		for(final Location p : points)
		{
			if(player.isGM())
				player.sendMessage("(" + p.x + ", " + p.y + ", " + p.z + ")");
			if(_prev != null)
				result.addLine(_prev.x, _prev.y, _prev.z, p.x, p.y, p.z, step, time);
			_prev = p;
		}
		return result;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Item Broker Gold");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
