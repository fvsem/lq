package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.L2Skill;
import l2n.game.model.L2SubClass;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.*;
import l2n.game.tables.SkillTable;
import l2n.game.tables.SkillTable.SubclassSkills;
import l2n.util.ArrayUtil;
import l2n.util.Files;
import l2n.util.StringUtil;
import quests._136_MoreThanMeetsTheEye._136_MoreThanMeetsTheEye;

import java.util.HashMap;

import static l2n.game.tables.SkillTable.SubclassSkills.CertificateEmergentAbility;
import static l2n.game.tables.SkillTable.SubclassSkills.CertificateMasterAbility;

public class SubSkills extends Functions implements ScriptFile
{
	private static final boolean checkPlayer(final L2Player player)
	{
		if(player == null)
			return false;

		if(!player.isSubClassActive())
		{
			show(Files.read("data/html/villagemaster/subclass/skillcert_needsub.htm", player), player);
			return false;
		}

		if(player.getLevel() < 65)
		{
			show(Files.read("data/html/villagemaster/subclass/skillcert_needsub65.htm", player), player);
			return false;
		}
		return true;
	}

	private static final void giveBookToChar(final L2Player player, final int bookItemId)
	{
		player.getInventory().addItem(bookItemId, 1, 0, "By NPC");
		player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S1).addItemName(bookItemId));
	}

	private static final int getSubclassNubmer(final L2Player player)
	{
		final L2SubClass sub = player.getActiveClass();
		if(sub != null && sub.isActive() && !sub.isBase())
			return sub.getNumSub();
		return -1;
	}

	private void certificate_level(final L2Player player, final int certLevel, final int bookId)
	{
		if(!checkPlayer(player))
			return;

		final int subNumber = getSubclassNubmer(player);
		if(subNumber < 2)
		{
			show(Files.read("data/html/villagemaster/subclass/skillcert_needsub.htm", player), player);
			return;
		}

		if(player.getLevel() < certLevel)
		{
			show(Files.read("data/html/villagemaster/subclass/skillcert-1.htm", player), player);
			return;
		}

		final String certificates_var = StringUtil.concat("SubSkillBook_", subNumber); // "SubSkillBook_1"
		final String data = player.getVar(certificates_var);
		if(data == null)
		{
			player.setVar(certificates_var, String.valueOf(certLevel));
			giveBookToChar(player, bookId);
		}
		else
		{
			final int[] certificates = ArrayUtil.toIntArray(data, ";");
			for(final int cert : certificates)
				if(cert == certLevel)
				{
					show(Files.read("data/html/villagemaster/subclass/skillcert_learnedall.htm", player), player);
					return;
				}

			if(certificates.length < 4)
			{
				player.setVar(certificates_var, StringUtil.concat(data, ";", certLevel));
				giveBookToChar(player, bookId);
			}
			else
				show(Files.read("data/html/villagemaster/subclass/skillcert_learnedall.htm", player), player);
		}
	}

	public void certificate65()
	{
		final L2Player player = (L2Player) getSelf();
		certificate_level(player, 65, CertificateEmergentAbility);
	}

	public void certificate70()
	{
		final L2Player player = (L2Player) getSelf();
		certificate_level(player, 70, CertificateEmergentAbility); // na 65 i 70 lvl 1 i tot je item
	}

	public void certificateClassSpecific75()
	{
		final L2Player player = (L2Player) getSelf();
		final int class_id = player.getClassId().getId();

		int item_id = 0;
		for(final int[] tmp : SkillTable.SubclassSkills.classes_certificates)
			if(tmp[0] == class_id)
			{
				item_id = tmp[1];
				break;
			}
		certificate_level(player, 75, item_id);
	}

	public void certificateMaster75()
	{
		final L2Player player = (L2Player) getSelf();
		certificate_level(player, 75, CertificateMasterAbility);
	}

	public void certificate80()
	{
		final L2Player player = (L2Player) getSelf();
		final int class_id = player.getClassId().getId();
		int item_id = 0;
		for(final int[] tmp : SkillTable.SubclassSkills.classes_certificates)
			if(tmp[0] == class_id)
			{
				item_id = tmp[2];
				break;
			}

		certificate_level(player, 80, item_id);
	}

	public void showList()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.ALLOW_LEARN_TRANS_SKILLS_WO_QUEST)
			if(!player.isQuestCompleted(_136_MoreThanMeetsTheEye.class))
			{
				show("Вы должны занать больше, чем кажется квест преобразовани глаз, чтобы получить сертификацию подкласса и правильно приобрести навыки в качестве основного класса.", player);
				return;
			}

		if(player.isSubClassActive())
		{
			player.sendMessage("Вы должны сделать это на основном классе");
			player.sendActionFailed();
			return;
		}

		AcquireSkillList asl = new AcquireSkillList(AcquireSkillList.SUBCLASS_SKILLS);
		int added_skills = 0;

		if(player.getInventory().getCountOf(CertificateEmergentAbility) > 0)
		{
			int Emergent_Ability_Attack = player.getSkillLevel(631);
			int Emergent_Ability_Defense = player.getSkillLevel(632);
			int Emergent_Ability_Empower = player.getSkillLevel(633);
			int Emergent_Ability_Magic_Defense = player.getSkillLevel(634);
			
			if(Emergent_Ability_Attack > 0)
				Emergent_Ability_Attack++;
			else
				Emergent_Ability_Attack = 1;

			if(Emergent_Ability_Defense > 0)
				Emergent_Ability_Defense++;
			else
				Emergent_Ability_Defense = 1;

			if(Emergent_Ability_Empower > 0)
				Emergent_Ability_Empower++;
			else
				Emergent_Ability_Empower = 1;

			if(Emergent_Ability_Magic_Defense > 0)
				Emergent_Ability_Magic_Defense++;
			else
				Emergent_Ability_Magic_Defense = 1;

			if(Emergent_Ability_Attack <= 6)
			{
				asl.addSkill(631, Emergent_Ability_Attack, 6, 0, 0);
				added_skills++;
			}
			if(Emergent_Ability_Defense <= 6)
			{
				asl.addSkill(632, Emergent_Ability_Defense, 6, 0, 0);
				added_skills++;
			}
			if(Emergent_Ability_Empower <= 6)
			{
				asl.addSkill(633, Emergent_Ability_Empower, 6, 0, 0);
				added_skills++;
			}
			if(Emergent_Ability_Magic_Defense <= 6)
			{
				asl.addSkill(634, Emergent_Ability_Magic_Defense, 6, 0, 0);
				added_skills++;
			}

		}

		for(final int[] tmp : SkillTable.SubclassSkills.skills_certificates_levels)
			if(tmp[2] >= 75)
				if(player.getInventory().getCountOf(tmp[1]) > 0)
					if(player.getSkillLevel(tmp[0]) == -1)
					{
						asl.addSkill(tmp[0], 1, 1, 0, 0);
						added_skills++;
					}

		if(added_skills > 0)
			player.sendPacket(asl);
		asl = null;

		if(added_skills == 0)
		{
			player.sendPacket(Msg.THERE_ARE_NO_OTHER_SKILLS_TO_LEARN);
			player.sendPacket(AcquireSkillDone.STATIC);
		}
	}

	public static boolean canLearn(final L2Player player, final int skillId)
	{
		return true;
	}

	public static void learnSkill(final L2Player player, final Integer skill_id)
	{
		if(player.isSubClassActive())
		{
			player.sendPacket(Msg.SKILL_NOT_FOR_SUBCLASS, Msg.ActionFail);
			return;
		}

		final int certificate_id = SkillTable.SubclassSkills.getCertificateForSkill(skill_id);
		final L2ItemInstance certificate = player.getInventory().findItemByItemId(certificate_id);
		if(certificate == null || certificate.getCount() < 1)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ITEMS_TO_LEARN_SKILLS);
			return;
		}

		final L2ItemInstance ri = player.getInventory().destroyItem(certificate, 1, true);
		player.sendPacket(new SystemMessage(SystemMessage.S1_HAS_DISAPPEARED).addItemName(ri.getItemId()));

		final int new_level = player.getSkillLevel(skill_id, 0) + 1;
		final L2Skill skill = SkillTable.getInstance().getInfo(skill_id, new_level);
		if(skill == null)
		{
			System.out.println("Skilltable cannot find skill " + String.valueOf(skill_id));
			return;
		}

		player.addSkill(skill, true); // store in DB
		player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addSkillName(skill_id, new_level), new SkillList(player), new UserInfo(player));

		final HashMap<String, Object> variables = new HashMap<String, Object>();
		variables.put("self", player.getStoredId());
		Functions.callScripts("services.SubSkills", "showList", EMPTY_ARG, variables);
	}

	public void deleteSkills()
	{
		final L2Player player = (L2Player) getSelf();
		if(player.isSubClassActive())
		{
			player.sendActionFailed();
			return;
		}

		unSummonPet(player);

		boolean haveSkills = false;
		for(final SubclassSkills sub_skill : SubclassSkills.values())
			if(player.getSkillLevel(sub_skill.getId()) > 0)
			{
				haveSkills = true;
				break;
			}

		if(!haveSkills)
		{
			player.sendActionFailed();
			player.sendMessage("У вас нет саб навыков для устранения.");
			return;
		}

		if(player.getInventory().getAdena() < 10000000)
		{
			player.sendActionFailed();
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}
		player.getInventory().reduceAdena(10000000);

		for(final L2SubClass subClass : player.getSubClasses().values())
			if(!subClass.isBase())
				player.unsetVar("SubSkillBook_" + subClass.getNumSub());

		L2ItemInstance item;
		for(final int bookId : SubclassSkills.spellbooks)
		{
			final long count = player.getInventory().getCountOf(bookId);
			if(count > 0)
				player.destroyItemByItemId(bookId, count, true);
			item = player.getWarehouse().findItemId(bookId);
			if(item != null && item.getCount() > 0)
				player.getWarehouse().destroyItem(bookId, item.getCount());
		}

		for(final SubclassSkills sub_skill : SubclassSkills.values())
			if(player.getSkillLevel(sub_skill.getId()) > 0)
				player.removeSkill(sub_skill.getId(), true);

		player.sendPacket(new SkillList(player));
		player.sendPacket(new UserInfo(player));
		show(new CustomMessage("scripts.services.SubclassSkills.SkillsDeleted", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Subclass Skills");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
