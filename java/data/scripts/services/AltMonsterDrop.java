package services;

import l2n.Config;
import l2n.Config.L2RewardItem;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.util.Rnd;

import java.util.StringTokenizer;

/**
 * @<!-- L2System -->
 * @date 20.06.2011
 * @time 0:57:48
 */
public class AltMonsterDrop extends EventScript implements ScriptFile
{
	private final static GArray<L2RewardItem> monster_drop = new GArray<L2RewardItem>();
	private final static GArray<L2RewardItem> raid_boss_drop = new GArray<L2RewardItem>();

	@Override
	public void onLoad()
	{
		if(Config.SERVICES_ALL_MOB_DROP_ENABLED)
		{
			final double mod = Config.SERVICES_ALL_MOB_NO_DROP_RATE ? Config.RATE_DROP_ITEMS : 1;
			final StringTokenizer st = new StringTokenizer(Config.ALL_MOB_DROP_ITEMS, ";");
			while (st.hasMoreTokens())
			{
				final String[] data = st.nextToken().split(",");
				final int itemId = Integer.parseInt(data[0]);
				final String[] count = data[1].split("-");
				final long min = (long) (Long.parseLong(count[0]) * mod);
				final long max = (long) (Long.parseLong(count[1]) * mod);
				final float chance = Float.parseFloat(data[2]);
				monster_drop.add(new L2RewardItem(itemId, chance, min, max));
			}
		}

		if(Config.SERVICES_ALL_RB_DROP_ENABLED)
		{
			final double mod = Config.SERVICES_ALL_RB_NO_DROP_RATE ? Config.RATE_DROP_RAIDBOSS : 1;
			final StringTokenizer st = new StringTokenizer(Config.ALL_RB_DROP_ITEMS, ";");
			while (st.hasMoreTokens())
			{
				final String[] data = st.nextToken().split(",");
				final int itemId = Integer.parseInt(data[0]);
				final String[] count = data[1].split("-");
				final long min = (long) (Long.parseLong(count[0]) * mod);
				final long max = (long) (Long.parseLong(count[1]) * mod);
				final float chance = Float.parseFloat(data[2]);
				raid_boss_drop.add(new L2RewardItem(itemId, chance, min, max));
			}
		}

		if(!monster_drop.isEmpty() || !raid_boss_drop.isEmpty())
		{
			_log.info("Loaded Service: Alt Monster Drop");
			addEventId(ScriptEventType.ON_DIE);
		}
	}

	@Override
	protected void onDie(final L2Character self, final L2Character killer)
	{
		if(Config.SERVICES_ALL_MOB_DROP_ENABLED)
			if(killer != null && self != null && self.isMonster() && !self.isRaid() && killer.getPlayer() != null)
			{
				final L2Player player = killer.getPlayer();
				for(final L2RewardItem drop : monster_drop)
					if(drop.calcChance(((L2MonsterInstance) self).calculateLevelDiffForDrop(player.getLevel(), false)))
						((L2MonsterInstance) self).dropItem(player, drop.item_id, Rnd.get(drop.min, drop.max));
			}

		if(Config.SERVICES_ALL_RB_DROP_ENABLED)
			if(killer != null && self != null && self.isRaid() && killer.getPlayer() != null)
			{
				final L2Player player = killer.getPlayer();
				for(final L2RewardItem drop : raid_boss_drop)
					if(drop.calcChance(((L2MonsterInstance) self).calculateLevelDiffForDrop(player.getLevel(), false)))
						((L2MonsterInstance) self).dropItem(player, drop.item_id, Rnd.get(drop.min, drop.max));
			}
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{
		monster_drop.clear();
		raid_boss_drop.clear();
	}
}
