package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Race;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2TerritoryManagerInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class NoblessSell extends Functions implements ScriptFile
{
	public void get()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null || player.isNoble())
			return;

		if(!Config.SERVICES_NOBLESS_SELL_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.NOBLESS_SELL))
			return;

		if(player.getSubLevel() < 75)
		{
			player.sendMessage("Вы должны сделать саб класс 75-го уровня Первый.");
			return;
		}

		L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_NOBLESS_SELL_ITEM);
		L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_NOBLESS_SELL_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_NOBLESS_SELL_PRICE, true);
			Olympiad.addNoble(player);
			player.setNoble(true, 1);
			player.updatePledgeClass();
			player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
			player.broadcastUserInfo(true);
		}
		else if(Config.SERVICES_NOBLESS_SELL_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void getTW()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null || player.isNoble())
			return;

		L2NpcInstance _npc = getNpc();
		if(_npc == null || !(_npc instanceof L2TerritoryManagerInstance))
			return;

		int terr = _npc.getNpcId() - 36489;
		if(terr > 9 || terr < 1)
			return;

		int territoryBadgeId = 13756 + terr;

		L2ItemInstance pay = player.getInventory().getItemByItemId(territoryBadgeId);
		if(pay != null && pay.getCount() >= Config.MIN_TWBADGE_FOR_NOBLESS)
		{
			player.getInventory().destroyItem(pay, Config.MIN_TWBADGE_FOR_NOBLESS, true);

			Quest q = QuestManager.getQuest("_234_FatesWhisper");
			QuestState qs = player.getQuestState("_234_FatesWhisper");
			if(qs != null)
				qs.exitCurrentQuest(true);
			q.newQuestState(player, Quest.COMPLETED);

			if(player.getRace() == Race.kamael)
			{
				q = QuestManager.getQuest("_236_SeedsOfChaos");
				qs = player.getQuestState("_236_SeedsOfChaos");
				if(qs != null)
					qs.exitCurrentQuest(true);
				q.newQuestState(player, Quest.COMPLETED);
			}
			else
			{
				q = QuestManager.getQuest("_235_MimirsElixir");
				qs = player.getQuestState("_235_MimirsElixir");
				if(qs != null)
					qs.exitCurrentQuest(true);
				q.newQuestState(player, Quest.COMPLETED);
			}

			Olympiad.addNoble(player);
			player.setNoble(true, 1);
			player.updatePledgeClass();
			player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
			player.broadcastUserInfo(true);
		}
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Nobless sell");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
