package services.RateBonus;

import l2n.Config;
import l2n.Config.L2RewardItem;
import l2n.database.L2DatabaseFactory;
import l2n.database.utils.mysql;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ExBR_PremiumState;
import l2n.game.tables.ItemTable;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.Rnd;

import java.sql.SQLException;
import java.util.Date;

public class RateBonus extends Functions implements ScriptFile
{
	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_RATE_BONUS_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return;
		}

		String html;
		if(player.getNetConnection().getBonus() == 1)
		{
			html = Files.read("data/scripts/services/RateBonus/RateBonus.htm", player);

			String add = new String();
			for(int i = 0; i < Config.SERVICES_RATE_BONUS_DAYS.length; i++)
				add += "<a action=\"bypass -h scripts_services.RateBonus.RateBonus:get " + i + "\">" //
						+ (int) (Config.SERVICES_RATE_BONUS_VALUE[i] * 100 - 100) + //
						"% на " + Config.SERVICES_RATE_BONUS_DAYS[i] + //
						" дней - " + Config.SERVICES_RATE_BONUS_PRICE[i] + //
						" " + ItemTable.getInstance().getTemplate(Config.SERVICES_RATE_BONUS_ITEM[i]).getName() + "</a><br>";

			html = html.replaceFirst("%toreplace%", add);
		}
		else if(player.getNetConnection().getBonus() > 1)
		{
			long endtime = player.getNetConnection().getBonusExpire();
			if(endtime >= 0)
				html = Files.read("data/scripts/services/RateBonus/RateBonusAlready.htm", player).replaceFirst("endtime", new Date(endtime * 1000).toString());
			else
				html = Files.read("data/scripts/services/RateBonus/RateBonusInfinite.htm", player);
		}
		else
			html = Files.read("data/scripts/services/RateBonus/RateBonusNo.htm", player);
		show(html, player);
	}

	public void get(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_RATE_BONUS_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return;
		}

		int i = Integer.parseInt(param[0]);

		L2ItemInstance pay = player.getInventory().getItemByItemId(Config.SERVICES_RATE_BONUS_ITEM[i]);
		if(pay != null && pay.getCount() >= Config.SERVICES_RATE_BONUS_PRICE[i])
		{
			removeItem(player, Config.SERVICES_RATE_BONUS_ITEM[i], Config.SERVICES_RATE_BONUS_PRICE[i]);

			// Предмет выдаваемый игроку после покупки ПА
			if(Config.SERVICES_RATE_BONUS_GIVE_ITEMID.size() > 0)
				for(L2RewardItem item : Config.SERVICES_RATE_BONUS_GIVE_ITEMID)
					if(Rnd.chance(item.chance))
						addItem(player, item.item_id, Rnd.get(item.min, item.max));

			Log.add(player.getName() + "|" + player.getObjectId() + "|rate bonus|" + Config.SERVICES_RATE_BONUS_VALUE[i] + "|" + Config.SERVICES_RATE_BONUS_DAYS[i] + "|", "services");
			try
			{
				mysql.setEx(L2DatabaseFactory.getInstanceLogin(), "UPDATE `accounts` SET `bonus` = '" + Config.SERVICES_RATE_BONUS_VALUE[i] + "',`bonus_expire`=UNIX_TIMESTAMP()+" + Config.SERVICES_RATE_BONUS_DAYS[i] + "*24*60*60 WHERE `login` = '" + player.getAccountName() + "'");
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
			player.getNetConnection().setBonus(Config.SERVICES_RATE_BONUS_VALUE[i]);
			player.getNetConnection().setBonusExpire(System.currentTimeMillis() / 1000 + Config.SERVICES_RATE_BONUS_DAYS[i] * 24 * 60 * 60);
			player.restoreBonus();
			if(player.getParty() != null)
				player.getParty().recalculatePartyData();

			player.sendPacket(new ExBR_PremiumState(player.getObjectId(), 1));
			show(Files.read("data/scripts/services/RateBonus/RateBonusGet.htm", player), player);
		}
		else if(Config.SERVICES_RATE_BONUS_ITEM[i] == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void howtogetcol()
	{
		show("data/scripts/services/RateBonus/howtogetcol.htm", (L2Player) getSelf());
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Rate bonus");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
