package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.util.Log;
import l2n.util.StringUtil;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * @author bloodshed <a href="http://l2nextgen.ru/">L2NextGen</a>
 * @email rkx.bloodshed@gmail.com
 * @date 02.10.2012
 * @time 0:03:17
 */
public class OlympiadPointsSell extends Functions implements ScriptFile
{
	public final void list()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!isAvailable(player, false))
			return;

		final StringBuilder html = StringUtil.startAppend(500, "<html><head><body>Покупка Очков Олимпиады:<br>");

		if(Config.SERVICES_OLYMPIAD_POINTS_RESET_ENABLED)
		{
			StringUtil.append(html, "Вы можете сбросить очки олимпиады на ", Config.SERVICES_OLYMPIAD_POINTS_RESET_POINTS, " за ", Config.SERVICES_OLYMPIAD_POINTS_RESET_COUNT, " ", ItemTable.getInstance().getTemplate(Config.SERVICES_OLYMPIAD_POINTS_RESET_ITEM_ID).getName(), "<br1>");
			StringUtil.append(html, "<a action=\"bypass -h scripts_services.OlympiadPointsSell:reset\">Сбросить очки олимпиады</a><br><br>");
		}

		for(int i = 0; i < Config.SERVICES_OLYMPIAD_POINTS_SELL_COUNTS.length; i++)
			StringUtil.append(html, "<a action=\"bypass -h scripts_services.OlympiadPointsSell:buy ", i, "\"> Купить ", Config.SERVICES_OLYMPIAD_POINTS_SELL_COUNTS[i], " очков олимпиады - ", Config.SERVICES_OLYMPIAD_POINTS_SELL_PRICES[i], " ", ItemTable.getInstance().getTemplate(Config.SERVICES_OLYMPIAD_POINTS_SELL_ITEMS[i]).getName(), "</a><br>");

		StringUtil.append(html, "</body></html>");

		show(html.toString(), player);
	}

	public final void buy(final String[] args)
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!isAvailable(player, true) || args == null || args.length != 1)
			return;

		final int idx = NumberUtils.toInt(args[0], -1);
		if(idx < 0)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		final int pointToAdd = Config.SERVICES_OLYMPIAD_POINTS_SELL_COUNTS[idx];
		final int itemId = Config.SERVICES_OLYMPIAD_POINTS_SELL_ITEMS[idx];
		final long itemCount = Config.SERVICES_OLYMPIAD_POINTS_SELL_PRICES[idx];

		final L2ItemInstance pay = player.getInventory().getItemByItemId(itemId);
		if(pay != null && pay.getCount() >= itemCount)
		{
			removeItem(player, itemId, itemCount);
			final int curPoints = Olympiad.getNoblePoints(player.getObjectId());
			Olympiad.manualSetNoblePoints(player.getObjectId(), curPoints + pointToAdd);
			final int newPoints = Olympiad.getNoblePoints(player.getObjectId());
			Log.add("Character " + player.getName() +   "|" + player.getObjectId() + "|buy olympiad points|" + curPoints + " -> " + newPoints, "services");
			player.sendMessage("Покупка успешно завершена.");
		}
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	/**
	 * Сброс очков
	 */
	public final void reset()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!isAvailable(player, true))
			return;

		if(!Config.SERVICES_OLYMPIAD_POINTS_RESET_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.notAvailable", player));
			return;
		}

		final L2ItemInstance pay = player.getInventory().getItemByItemId(Config.SERVICES_OLYMPIAD_POINTS_RESET_ITEM_ID);
		if(pay != null && pay.getCount() >= Config.SERVICES_OLYMPIAD_POINTS_RESET_COUNT)
		{
			removeItem(player, Config.SERVICES_OLYMPIAD_POINTS_RESET_ITEM_ID, Config.SERVICES_OLYMPIAD_POINTS_RESET_COUNT);
			final int curPoints = Olympiad.getNoblePoints(player.getObjectId());
			Olympiad.manualSetNoblePoints(player.getObjectId(), Config.SERVICES_OLYMPIAD_POINTS_RESET_POINTS);
			Log.add("Character " + player.getName() + "|" + player.getObjectId() + "|reset olympiad points|" + curPoints + " -> " + Config.SERVICES_OLYMPIAD_POINTS_RESET_POINTS, "services");
		}			
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	private final static boolean isAvailable(final L2Player player, boolean full)
	{
		if(!Config.SERVICES_OLYMPIAD_POINTS_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return false;
		}

		if(!player.isNoble())
		{
			if(player.isLangRus())
				player.sendMessage("Сервис доступен только Дворянам.");
			else
				player.sendMessage("Only for Noble available.");
			return false;
		}

		// период валидации
		if(!Olympiad._inCompPeriod || Olympiad._isOlympiadEnd)
{
			if(player.isLangRus())
				player.sendMessage("Сервис доступен , Ожидайте начало Олимпиады.");
			else
				player.sendMessage("Only for Olympiad available.");
			return false;
	}

		if(full && (player.isInTransaction() || player.isOutOfControl() || player.getTeam() != 0 || player.isInStoreMode() || player.getDuel() != null || player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player)))
		{
			player.sendMessage(new CustomMessage("common.notAvailable", player));
			return false;
		}

		if(full)
		{
			final int curPoints = Olympiad.getNoblePoints(player.getObjectId());
			if(curPoints >= Config.SERVICES_OLYMPIAD_POINTS_SELL_LIMIT)
			{
				player.sendMessage(new CustomMessage("common.notAvailable", player));
				return false;
			}
		}

		return true;
	}

	@Override
	public void onLoad()
	{
        System.out.println("Loaded Service: Olympiad Points Sell [" + (Config.SERVICES_OLYMPIAD_POINTS_SELL_ENABLED ? "enabled]" : "disabled]"));
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
