package services;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.boss.SailrenManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class TeleToSailren extends Functions implements ScriptFile
{
	private static final int GAZKH = 8784;

	public void toSailren()
	{
		L2Player p = (L2Player) getSelf();
		L2NpcInstance n = getNpc();

		if(p.getLevel() < 75)
			n.onBypassFeedback(p, "Chat 3");
		else if(p.getInventory().getCountOf(GAZKH) > 0)
		{
			int check = SailrenManager.canIntoSailrenLair(p);
			if(check == 1)
				n.onBypassFeedback(p, "Chat 4");
			else if(check == 2)
				n.onBypassFeedback(p, "Chat 8");
			else if(check == 3)
				n.onBypassFeedback(p, "Chat 7");
			else if(check == 4)
				n.onBypassFeedback(p, "Chat 1");
			else if(check == 0)
			{
				Functions.removeItem(p, GAZKH, 1);
				SailrenManager.setSailrenSpawnTask();
				SailrenManager.entryToSailrenLair(p);
			}
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	public void fromSailren()
	{
		L2Player player = (L2Player) getSelf();
		int n = Rnd.get(100);
		Location loc = null;
		if(n < 40)
			loc = new Location(10610, -24035, -3676);
		else if(n < 70)
			loc = new Location(10703, -24041, -3673);
		else
			loc = new Location(10769, -24107, -3672);

		if(player.getParty() == null)
			player.teleToLocation(loc);
		else
		{
			GArray<L2Player> members = new GArray<L2Player>();
			for(L2Player mem : player.getParty().getPartyMembers())
				if(mem != null && !mem.isDead() && mem.isInRange(player, 1000))
					members.add(mem);
			for(L2Player mem : members)
				mem.teleToLocation(loc.rnd(0, 80, false));
		}
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Teleport to Sailren");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
