package services;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Hero;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.Log;

public class HeroItems extends Functions implements ScriptFile
{
	private static final String[][] HERO_ITEMS = {
			{
					"6611",
					"weapon_the_sword_of_hero_i00",
					"Клинок Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При Крит. Атк. применяется вредоносное умение, понижающее Физ. Защ. противника. Увеличивает Макс. HP, Макс. MP, Макс. CP и защиту щита. Дает шанс отразить отрицательные эффекты обратно врагу. Наносит дополнительный урон в PvP.",
					"379/169",
					"Мечь/Одноручный" },
			{
					"6612",
					"weapon_the_two_handed_sword_of_hero_i00",
					"Колун Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. Увеличивает шанс успеха/мощность Крит. Атк., Макс.HP, Макс.CP, наносит дополнительный урон в PvP. Также причиняет дополнительный урон при Крит. Атк. и дает шанс отразить отрицательные эффекты обратно врагу.",
					"461/169",
					"Мечь/Двуручный" },
			{
					"6613",
					"weapon_the_axe_of_hero_i00",
					"Топор Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При критической атаке отражает часть урона и дает заклинание, вызывающее у противника внутренний конфликт. Увеличивает шанс защиты щитом, Макс. HP, Макс МР и Макс. CP. Наносит дополнительный урон в PvP.",
					"379/169",
					"Дубина/Одноручный" },
			{
					"6614",
					"weapon_the_mace_of_hero_i00",
					"Жезл Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При использовании благотворного умения на цель увеличивает Восстановление HP до 100%, а также Макс. MP, Макс. CP, Скор. Маг. и Скорость Восстановления MP. Наносит дополнительный урон в PvP.",
					"303/226",
					"Дубина/Одноручный" },
			{
					"6615",
					"weapon_the_hammer_of_hero_i00",
					"Крушитель Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. Увеличивает Макс. HP, Макс. CP и Скор. Атк. Оглушает цель при критической атаке и дает шанс отразить отрицательные эффекты обратно врагу. Наносит дополнительный урон в PvP.",
					"461/169",
					"Дубина/Двуручный" },
			{
					"6616",
					"weapon_the_staff_of_hero_i00",
					"Скипетр Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При использовании благотворного умения есть шанс Восстановления HP до 100%. Увеличивает Макс. MP, Макс. CP и Маг. Атк., уменьшает расход MP, увеличивает шанс магической Крит. Атк. и понижает шанс прерывания колдовства. Наносит дополнительный урон в PvP.",
					"369/226",
					"Дубина/Двуручный" },
			{
					"6617",
					"weapon_the_dagger_of_hero_i00",
					"Жало Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. Увеличивает Макс. MP, Макс. CP, Скор. Атк., Скорость, Скорость Восстановления MP и шанс успеха в случае применения Смертельного Удара со спины. При критической атаке накладывает на цель безмолвие и дает эффект Ярости Вампира. Наносит дополнительный урон в PvP",
					"332/169",
					"Дагер/Одноручный" },
			{
					"6618",
					"weapon_the_fist_of_hero_i00",
					"Клык Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. Увеличивает Макс. HP, Макс MP, Макс. CP и Уклонение. Оглушает цель при критической атаке и дает шанс отразить отрицательные эффекты обратно врагу. Наносит дополнительный урон в PvP.",
					"461/169",
					"Кастеты/Двуручный" },
			{
					"6619",
					"weapon_the_bow_of_hero_i00",
					"Лук Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При критической атаке замедляет цель, дает эффект Экономии, увеличивает скорострельность лука, Макс. MP и Макс. CP. Наносит дополнительный урон в PvP.",
					"707/169",
					"Лук/Двуручный" },
			{
					"6620",
					"weapon_the_dualsword_of_hero_i00",
					"Крыло Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При критической атаке накладывает на цель безмолвие, дает шанс отразить отрицательные эффекты обратно врагу, увеличивает Макс. HP, Макс. MP, Макс. CP и шанс Крит. Атк. Наносит дополнительный урон в PvP.",
					"461/169",
					"Дуалы/Двуручный" },
			{
					"6621",
					"weapon_the_pole_of_hero_i00",
					"Копье Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При критическом ударе прерывает колдовство цели, дает шанс отразить отрицательные эффекты обратно врагу, увеличивает Макс. HP, Макс. CP, Скор. Атк. и Точность. Наносит дополнительный урон в PvP.",
					"379/169",
					"Пика/Двуручный" },
			{
					"9388",
					"weapon_infinity_rapier_i00",
					"Рапира Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При Крит. Атк. применяется вредоносное умение, понижающее Физ. Защ. противника, отражает часть урона, увеличивает Макс HP/Макс MP/Макс. CP. Наносит дополнительный урон в PvP.При Крит. Атк. дает шанс увеличить Физ. Атк. Маг. Атк. и мощность лечения, а также уменьшает расход MP для Вас и членов Вашей группы.",
					"344/169",
					"Рапира/одноручный" },
			{
					"9389",
					"weapon_infinity_sword_i00",
					"Меч Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. Увеличивает шанс успеха/мощность Крит. Атк., Макс.HP, Макс.CP, и урон, причиняемый в PvP. Также причиняет дополнительный урон при Крит. Атк., и дает шанс отразить отрицательные эффекты обратно врагу.",
					"410/169",
					"Мечь/Двуручный" },
			{
					"9390",
					"weapon_infinity_shooter_i00",
					"Ружье Вечности",
					"Священное Оружие Героя. Добавляется Священная Физ. Атк. При Крит. Атк. дает следующие эффекты: замедление цели, уменьшение расхода MP при использовании умений, увеличение Макс.MP/Макс CP. Наносит дополнительный урон в PvP.",
					"405/169",
					"Арбалет/Двуручный" } };

	public void getweapon(String[] var)
	{
		L2Player player = (L2Player) getSelf();
		int item = Integer.parseInt(var[0]);
		if(item < 6611 && item > 6621 || item < 9388 && item > 9390)
		{
			System.out.println(player.getName() + " tried to obtain non hero item using hero weapon service. Ban him!");
			return;
		}

		if(player.isHero() && (Hero.getInstance().isHero(player.getObjectId()) || Config.ALT_ALLOW_TAKE_WEAPON))
		{
			for(String heroItem[] : HERO_ITEMS)
			{
				int itemId = Integer.parseInt(heroItem[0]);
				if(player.getInventory().getItemByItemId(itemId) != null || player.getWarehouse().countOf(itemId) > 0)
					return;
			}
			additem(item);
		}
	}

	public void rendershop(String[] val)
	{
		String htmltext = "";
		if(val[0].equalsIgnoreCase("list"))
		{
			htmltext = "<html><body><font color=\"LEVEL\">Список Геройского Оружия:</font><table border=0 width=270><tr><td>";
			for(int i = 0; i < HERO_ITEMS.length; i++)
			{
				htmltext += "<tr><td width=32 height=45 valign=top>";
				htmltext += "<img src=icon." + HERO_ITEMS[i][1] + " width=32 height=32></td>";
				htmltext += "<td valign=top>[<a action=\"bypass -h scripts_services.HeroItems:rendershop " + i + "\">" + HERO_ITEMS[i][2] + "</a>]<br1>";
				htmltext += "Тип: " + HERO_ITEMS[i][5] + ", Физ.Атк/Маг.Атк: " + HERO_ITEMS[i][4];
				htmltext += "</td></tr>";
			}
			htmltext += "</table>";
		}
		else if(Integer.parseInt(val[0]) >= 0 && Integer.parseInt(val[0]) <= HERO_ITEMS.length)
		{
			int itemIndex = Integer.parseInt(val[0]);

			// Для камаэль оружия сообщение:
			// 2234 Will you use the selected Kamael race-only Hero Weapon?
			// Для всего остального оружия сообщение:
			// 1484 Are you sure this is the Hero weapon you wish to use? Kamael race cannot use this.
			int msgId = itemIndex > 10 ? 2234 : 1484;

			htmltext = "<html><body><font color=\"LEVEL\">Информация:</font><table border=0 width=270><tr><td>";
			htmltext += "<img src=\"L2UI.SquareWhite\" width=270 height=1>";
			htmltext += "<table border=0 width=240>";
			htmltext += "<tr><td width=32 height=45 valign=top>";
			htmltext += "<img src=icon." + HERO_ITEMS[itemIndex][1] + " width=32 height=32></td>";
			htmltext += "<td valign=top>[<a action=\"bypass -h scripts_services.HeroItems:getweapon " + HERO_ITEMS[itemIndex][0] + "\" msg=\"" + msgId + "\">" + HERO_ITEMS[itemIndex][2] + "</a>]<br1>";
			htmltext += "Тип: " + HERO_ITEMS[itemIndex][5] + ", Физ.Атк/Маг.Атк: " + HERO_ITEMS[itemIndex][4] + "<br1>";
			htmltext += "</td></tr></table>";
			htmltext += "<font color=\"B09878\">" + HERO_ITEMS[itemIndex][3] + "</font>";
			htmltext += "</td></tr></table><br>";
			htmltext += "<img src=\"L2UI.SquareWhite\" width=270 height=1><br><br>";
			htmltext += "<CENTER><button value=Назад action=\"bypass -h scripts_services.HeroItems:rendershop list\" width=40 height=15 back=L2UI_CT1.Button_DF fore=L2UI_CT1.Button_DF></CENTER>";

		}
		show(htmltext, (L2Player) getSelf());
	}

	public void additem(int item)
	{
		L2Player player = (L2Player) getSelf();
		L2ItemInstance createditem = ItemTable.getInstance().createItem(item, player.getObjectId(), 0, "HeroItems");
		player.getInventory().addItem(createditem);
		Log.LogItem(player, Log.GetItem, createditem);
		player.sendPacket(new ItemList(player, true));
		player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S1).addItemName(item));
	}

	public String getcir()
	{
		L2Player player = (L2Player) getSelf();
		if(player.isHero() && (Hero.getInstance().isHero(player.getObjectId()) || Config.ALT_ALLOW_TAKE_WEAPON))
		{
			if(player.getInventory().getItemByItemId(6842) != null || player.getWarehouse().countOf(6842) > 0)
				return null;
			additem(6842); // Wings of Destiny Circlet
		}
		return null;
	}

	public String itemz()
	{
		String append = "";
		L2Player player = (L2Player) getSelf();
		if(player.isHero() && (Hero.getInstance().isHero(player.getObjectId()) || Config.ALT_ALLOW_TAKE_WEAPON))
		{
			String lang = player.getVar("lang@");
			if(player.getInventory().getItemByItemId(6842) == null && player.getWarehouse().countOf(6842) == 0)
				if(lang.equalsIgnoreCase("en"))
					append += "<a action=\"bypass -h scripts_services.HeroItems:getcir\">Get Hair Accessory.</a><br1>";
				else
					append += "<a action=\"bypass -h scripts_services.HeroItems:getcir\">Получить корону героя.</a><br1>";
			for(String heroItem[] : HERO_ITEMS)
			{
				int itemId = Integer.parseInt(heroItem[0]);
				if(player.getInventory().getItemByItemId(itemId) != null || player.getWarehouse().countOf(itemId) > 0)
					return append;
			}
			if(lang.equalsIgnoreCase("en"))
				append += "<a action=\"bypass -h scripts_services.HeroItems:rendershop list\">Select a hero weapon.</a>";
			else
				append += "<a action=\"bypass -h scripts_services.HeroItems:rendershop list\">Выбрать оружие героя.</a>";

			if(lang.equalsIgnoreCase("en"))
				append = "<font color=\"LEVEL\">Hero rewards:</font><br1>" + append;
			else
				append = "<font color=\"LEVEL\">Награда героя:</font><br1>" + append;
		}

		return append;
	}

	public String DialogAppend_31690(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	public String DialogAppend_31769(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	public String DialogAppend_31770(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	public String DialogAppend_31771(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	public String DialogAppend_31772(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	public String DialogAppend_31773(Integer val)
	{
		if(val != 0)
			return "";
		return itemz();
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Hero Items");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
