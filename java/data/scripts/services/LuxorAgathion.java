package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;
import l2n.util.Rnd;

/**
 * Используется в Luxor Shop NPC Alexandria (id: 30098), для продажи Agathion-ов.
 */
public class LuxorAgathion extends Functions implements ScriptFile
{
	// Ингридиенты:
	private static final int[][] INGRIDIENTS = { { 6471, 25 }, // 25 Big Red Nimble Fish
			{ 5094, 50 }, // 50 Great Codrans
			{ 9814, 4 }, // 4 Memento Moris
			{ 9816, 5 }, // 5 Earth Eggs
			{ 9817, 5 }, // 5 Nonliving Nuclei
			{ 9815, 3 }, // 3 Dragon Hearts
			{ 57, 7500000 } // 7,500,000 Adena
	};

	private static final int OldAgathion = 10408;
	private static final int ShadowPurpleVikingCirclet = 10315;
	private static final int ShadowGoldenVikingCirclet = 10321;

	// Продукция:
	private static int[] ANGEL_BRACELET_IDS = new int[] { 10316, 10317, 10318, 10319, 10320 };
	private static int[] DEVIL_BRACELET_IDS = new int[] { 10322, 10323, 10324, 10325, 10326 };

	private static int SUCCESS_RATE = 60; // 60% шанс успеха

	@Override
	public void onLoad()
	{}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void angelAgathion()
	{
		agathion(ANGEL_BRACELET_IDS[Rnd.get(ANGEL_BRACELET_IDS.length)], 1);
	}

	public void devilAgathion()
	{
		agathion(DEVIL_BRACELET_IDS[Rnd.get(DEVIL_BRACELET_IDS.length)], 2);
	}

	private void agathion(int braceletId, int type)
	{
		L2Player player = (L2Player) getSelf();

		for(int[] ingridient : INGRIDIENTS)
			if(getItemCount(player, ingridient[0]) < ingridient[1])
			{
				show(Files.read("data/html/merchant/30098-2.htm", player), player);
				return;
			}

		for(int[] ingridient : INGRIDIENTS)
			removeItem(player, ingridient[0], ingridient[1]);

		if(!Rnd.chance(SUCCESS_RATE))
		{
			addItem(player, OldAgathion, 1);
			if(type == 1)
				addItem(player, ShadowPurpleVikingCirclet, 1);
			else
				addItem(player, ShadowGoldenVikingCirclet, 1);
			show(Files.read("data/html/merchant/30098-3.htm", player), player);
			return;
		}

		addItem(player, braceletId, 1);
		show(Files.read("data/html/merchant/30098-4.htm", player), player);
	}
}
