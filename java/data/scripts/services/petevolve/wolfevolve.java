package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.util.Files;

public class wolfevolve extends Functions implements ScriptFile
{
	private static final int WOLF = 12077; // Чтоб было =), проверка на Wolf
	private static final int count = 1; // Чтоб было =), число забираемх ошейников Wolf
	private static final int WOLF_COLLAR = 2375; // Ошейник Wolf
	private static final int GREAT_WOLF_NECKLACE = 9882; // Ожерелье Great Wolf
	private static final int dist = 50; // Дистанция от игрока до пета во избежание юзания каких-нибудь читов или
										// багов. TODO: попозже добавлю

	public void evolve()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(WOLF_COLLAR) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != WOLF)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/no_level.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, WOLF_COLLAR, count);
		addItem(player, GREAT_WOLF_NECKLACE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Evolve Wolf");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
