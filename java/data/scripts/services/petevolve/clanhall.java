package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.tables.PetDataTable;
import l2n.util.Files;

public class clanhall extends Functions implements ScriptFile
{
	// -- Pet ID --
	private static final int GREAT_WOLF = PetDataTable.GREAT_WOLF_ID;
	private static final int FENRIR = PetDataTable.FENRIR_WOLF_ID;
	private static final int WIND_STRIDER = PetDataTable.STRIDER_WIND_ID;
	private static final int STAR_STRIDER = PetDataTable.STRIDER_STAR_ID;
	private static final int TWILING_STRIDER = PetDataTable.STRIDER_TWILIGHT_ID;

	// -- MAX PET CHANG LEVEL --

	// -- First Item ID --
	private static final int GREAT_WOLF_ITEM = PetDataTable.getControlItemId(GREAT_WOLF);
	private static final int FENRIR_ITEM = PetDataTable.getControlItemId(FENRIR);
	private static final int WIND_STRIDER_ITEM = PetDataTable.getControlItemId(WIND_STRIDER);
	private static final int STAR_STRIDER_ITEM = PetDataTable.getControlItemId(STAR_STRIDER);
	private static final int TWILING_STRIDER_ITEM = PetDataTable.getControlItemId(TWILING_STRIDER);

	// -- Second Item ID --
	private static final int GREAT_SNOW_WOLF_ITEM = PetDataTable.getControlItemId(PetDataTable.WGREAT_WOLF_ID);
	private static final int SNOW_FENRIR_ITEM = PetDataTable.getControlItemId(PetDataTable.WFENRIR_WOLF_ID);
	private static final int RED_WS_ITEM = PetDataTable.getControlItemId(PetDataTable.RED_STRIDER_WIND_ID);
	private static final int RED_SS_ITEM = PetDataTable.getControlItemId(PetDataTable.RED_STRIDER_STAR_ID);
	private static final int RED_TW_ITEM = PetDataTable.getControlItemId(PetDataTable.RED_STRIDER_TWILIGHT_ID);

	// -- dist --
	// private int dist = 50; // Дистанция от игрока до пета во избежание юзания каких-нибудь читов или багов. TODO: попозже добавлю

	public void evolve()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		show(Files.read("data/scripts/services/petevolve/chamberlain.htm", player), player);
	}

	public void greatsw()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final L2Summon pl_pet = player.getPet();

		if(pl_pet == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_1.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != GREAT_WOLF)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/error_lvl_greatw.htm", player), player);
			return;
		}
		else if(player.getInventory().getItemByItemId(GREAT_WOLF_ITEM) == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else
		{
			player.getPet().unSummon();
			removeItem(player, GREAT_WOLF_ITEM, 1);
			addItem(player, GREAT_SNOW_WOLF_ITEM, 1);
			show(Files.read("data/scripts/services/petevolve/end_msg3_gwolf.htm", player), player);
		}
	}

	public void fenrir()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final L2Summon pl_pet = player.getPet();

		if(pl_pet == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_1.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != FENRIR)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 70)
		{
			show(Files.read("data/scripts/services/petevolve/error_lvl_fenrir.htm", player), player);
			return;
		}
		else if(player.getInventory().getItemByItemId(FENRIR_ITEM) == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, FENRIR_ITEM, 1);
		addItem(player, SNOW_FENRIR_ITEM, 1);
		show(Files.read("data/scripts/services/petevolve/end_msg2_fenrir.htm", player), player);
	}

	public void wstrider()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final L2Summon pl_pet = player.getPet();

		if(pl_pet == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_1.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != WIND_STRIDER)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/error_lvl_strider.htm", player), player);
			return;
		}
		else if(player.getInventory().getItemByItemId(WIND_STRIDER_ITEM) == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, WIND_STRIDER_ITEM, 1);
		addItem(player, RED_WS_ITEM, 1);
		show(Files.read("data/scripts/services/petevolve/end_msg_strider.htm", player), player);
	}

	public void sstrider()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final L2Summon pl_pet = player.getPet();

		if(pl_pet == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_1.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != STAR_STRIDER)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/error_lvl_strider.htm", player), player);
			return;
		}
		else if(player.getInventory().getItemByItemId(STAR_STRIDER_ITEM) == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, STAR_STRIDER_ITEM, 1);
		addItem(player, RED_SS_ITEM, 1);
		show(Files.read("data/scripts/services/petevolve/end_msg_strider.htm", player), player);
	}

	public void tstrider()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final L2Summon pl_pet = player.getPet();

		if(pl_pet == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_1.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != TWILING_STRIDER)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/error_lvl_strider.htm", player), player);
			return;
		}
		else if(player.getInventory().getItemByItemId(TWILING_STRIDER_ITEM) == null)
		{
			show(Files.read("data/scripts/services/petevolve/error_2.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, TWILING_STRIDER_ITEM, 1);
		addItem(player, RED_TW_ITEM, 1);
		show(Files.read("data/scripts/services/petevolve/end_msg_strider.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: ClanHall Pet Evolution");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
