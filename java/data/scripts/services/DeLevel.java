package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class DeLevel extends Functions implements ScriptFile
{
	public void showDown()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		L2Item price = ItemTable.getInstance().getTemplate(Config.SERVICES_DELEVEL_ITEM_ID);

		String content = Files.read("data/scripts/services/DeLevel.htm", player);
		content = content.replace("%price%", Config.SERVICES_DELEVEL_ITEM_COUNT + " " + price.getName());
		show(content, player);
	}

	public void showUp()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		L2Item price = ItemTable.getInstance().getTemplate(Config.SERVICES_LEVELUP_ITEM_ID);

		String content = Files.read("data/scripts/services/LevelUp.htm", player);
		content = content.replace("%price%", Config.SERVICES_LEVELUP_ITEM_COUNT + " " + price.getName());
		show(content, player);
	}

	public void delevel()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.DELEVEL))
			return;

		if(!checkConditionDown(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		if(player.getInventory().getCountOf(Config.SERVICES_DELEVEL_ITEM_ID) < Config.SERVICES_DELEVEL_ITEM_COUNT)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		removeItem(player, Config.SERVICES_DELEVEL_ITEM_ID, Config.SERVICES_DELEVEL_ITEM_COUNT);
		Log.add(player.getName() + "|" + player.getObjectId() + "|delevel|" + Config.SERVICES_DELEVEL_ITEM_ID + "-" + Config.SERVICES_DELEVEL_ITEM_COUNT + "|", "services");
		long exp_add = Experience.LEVEL[player.getLevel() - 1] - player.getExp();
		player.addExpAndSp(exp_add, 0, false, false);
	}

	public void levelUp()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.LEVEL_UP))
			return;

		if(!checkConditionUp(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		if(player.getInventory().getCountOf(Config.SERVICES_LEVELUP_ITEM_ID) < Config.SERVICES_LEVELUP_ITEM_COUNT)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		removeItem(player, Config.SERVICES_LEVELUP_ITEM_ID, Config.SERVICES_LEVELUP_ITEM_COUNT);
		Log.add(player.getName() + "|" + player.getObjectId() + "|levelup|" + Config.SERVICES_LEVELUP_ITEM_ID + "-" + Config.SERVICES_LEVELUP_ITEM_COUNT + "|", "services");
		long exp_add = Experience.LEVEL[85] - player.getExp();
		player.addExpAndSp(exp_add, 0, false, false);
	}

	private boolean checkConditionDown(L2Player player)
	{
		if(!Config.SERVICES_DELEVEL_ENABLE)
			return false;
		if(player.getLevel() < Config.SERVICES_DELEVEL_MIN_LEVEL || player.getKarma() > 0 || player.getPvpFlag() != 0)
			return false;
		if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isFlying() || player.isCombatFlagEquipped() || player.isInZone(ZoneType.Siege))
			return false;
		if(player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player) || player.getTeam() != 0)
			return false;
		return true;
	}

	private boolean checkConditionUp(L2Player player)
	{
		if(!Config.SERVICES_LEVELUP_ENABLE)
			return false;
		if(player.getLevel() < Config.SERVICES_LEVELUP_MIN_LEVEL || player.getLevel() >= Config.SERVICES_LEVELUP_MAX_LEVEL)
			return false;
		if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isFlying() || player.isCombatFlagEquipped() || player.isInZone(ZoneType.Siege))
			return false;
		if(player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player) || player.getTeam() != 0)
			return false;
		return true;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: DeLevel");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
