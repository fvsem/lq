package services;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.L2SubClass;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.base.PlayerClass;
import l2n.game.model.base.Race;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.CharTemplateTable;
import l2n.game.tables.ClanTable;
import l2n.game.tables.SkillTable.SubclassSkills;
import l2n.util.Log;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;
import l2n.util.Util;

import java.sql.ResultSet;

public class Rename extends Functions implements ScriptFile
{
	public void rename_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		String append = "!Смена Имени Персонажа";
		append += "<br>";
		append += "<font color=\"LEVEL\">" + new CustomMessage("scripts.services.Rename.RenameFor", getSelf()).addString(Util.formatAdena(Config.SERVICES_CHANGE_NICK_PRICE)).addItemName(Config.SERVICES_CHANGE_NICK_ITEM) + "</font>";
		append += "<table>";
		append += "<tr><td>" + new CustomMessage("scripts.services.Rename.NewName", getSelf()) + ": <edit var=\"new_name\" width=80></td></tr>";
		append += "<tr><td></td></tr>";
		append += "<tr><td><button value=\"" + new CustomMessage("scripts.services.Rename.RenameButton", getSelf()) + "\" action=\"bypass -h scripts_services.Rename:rename $new_name\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>";
		append += "</table>";
		show(append, player);
	}

	public void changesex_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		String append = "Смена пола Персонажа";
		append += "<br>";
		append += "<font color=\"LEVEL\">" + new CustomMessage("scripts.services.SexChange.SexChangeFor", player).addString(Util.formatAdena(Config.SERVICES_CHANGE_SEX_PRICE)).addItemName(Config.SERVICES_CHANGE_SEX_ITEM) + "</font>";
		append += "<table>";
		append += "<tr><td><button value=\"" + new CustomMessage("scripts.services.SexChange.Button", player) + "\" action=\"bypass -h scripts_services.Rename:changesex\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>";
		append += "</table>";
		show(append, player);
	}

	public void changebase_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(player.isHero())
		{
			sendMessage("Не доступно для героев.", player);
			return;
		}

		String append = "Изменение Базового класса";
		append += "<br>";
		append += "<font color=\"LEVEL\">" + new CustomMessage("scripts.services.BaseChange.Price", player).addString(Util.formatAdena(Config.SERVICES_CHANGE_BASE_PRICE)).addItemName(Config.SERVICES_CHANGE_BASE_ITEM) + "</font>";
		append += "<table>";

		GArray<L2SubClass> possible = new GArray<L2SubClass>();
		if(player.getActiveClass().isBase())
		{
			possible.addAll(player.getSubClasses().values());
			possible.remove(player.getSubClasses().get(player.getBaseClassId()));

			for(L2SubClass s : player.getSubClasses().values())
				for(L2SubClass s2 : player.getSubClasses().values())
					if(s != s2 && !PlayerClass.areClassesComportable(PlayerClass.values()[s.getClassId()], PlayerClass.values()[s2.getClassId()]) || s2.getLevel() < 75 || s2.getClassId() == 136) // Judicator
						possible.remove(s2);
		}

		if(possible.isEmpty())
			append += "<tr><td width=300>" + new CustomMessage("scripts.services.BaseChange.NotPossible", player) + "</td></tr>";
		else
			for(L2SubClass s : possible)
				append += "<tr><td><button value=\"" + new CustomMessage("scripts.services.BaseChange.Button", player).addString(CharTemplateTable.getClassNameById(s.getClassId())) + "\" action=\"bypass -h scripts_services.Rename:changebase " + s.getClassId() + "\" width=200 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>";
		append += "</table>";
		show(append, player);
	}

	public void changebase(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_BASE_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.BASE_CHANGE))
			return;

		int target = Integer.parseInt(param[0]);
		if(!player.getActiveClass().isBase())
		{
			show("Вы должны быть на вашем базовом классе, чтобы воспользоваться этой услугой,.", player);
			return;
		}
		if(player.isHero())
		{
			show("Не доступно для героев.", player);
			return;
		}

		if(getItemCount(player, Config.SERVICES_CHANGE_BASE_ITEM) < Config.SERVICES_CHANGE_BASE_PRICE)
		{
			if(Config.SERVICES_CHANGE_BASE_ITEM == 57)
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			else
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		// Удаляем из нублесов
		if(player.isNoble())
			Olympiad.removeNoble(player);

		// удаляем данные о выданных книгах для изучения скилов сабклассов
		for(final L2SubClass subClass : player.getSubClasses().values())
			if(!subClass.isBase())
				player.unsetVar("SubSkillBook_" + subClass.getNumSub());

		// удаляем книги
		L2ItemInstance item;
		for(final int bookId : SubclassSkills.spellbooks)
		{
			final long count = player.getInventory().getCountOf(bookId);
			if(count > 0)
				player.destroyItemByItemId(bookId, count, true);
			item = player.getWarehouse().findItemId(bookId);
			if(item != null && item.getCount() > 0)
				player.getWarehouse().destroyItem(bookId, item.getCount());
		}

		// удаляем скилы сабклассов
		for(final SubclassSkills sub_skill : SubclassSkills.values())
			if(player.getSkillLevel(sub_skill.getId()) > 0)
				player.removeSkill(sub_skill.getId(), true);

		L2SubClass baseClass = player.getActiveClass();
		L2SubClass targetClass = player.getSubClasses().get(target);

		int numTarget = targetClass.getNumSub();
		baseClass.setNumSub(numTarget);
		baseClass.setBase(false);
		if(baseClass.getLevel() > Experience.getMaxSubLevel())
			player.setLevel(Experience.getMaxSubLevel());

		player.checkSkills(0);

		targetClass.setNumSub(1);
		targetClass.setBase(true);
		player.setBaseClass(target);

		// Добавляем в нублесы
		if(player.isNoble())
			Olympiad.addNoble(player);

		player.setHairColor(0);
		player.setHairStyle(0);
		player.setFace(0);
		removeItem(player, Config.SERVICES_CHANGE_BASE_ITEM, Config.SERVICES_CHANGE_BASE_PRICE);
		player.logout(false, false, false, true);
		Log.add("Character " + player + " base changed to " + target, "services");
	}

	public void rename(String[] args)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_NICK_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.NICK_CHANGE))
			return;

		if(args.length != 1)
		{
			show(new CustomMessage("scripts.services.Rename.incorrectinput", player), player);
			return;
		}

		String name = args[0];
		if(!Config.SERVICES_CHANGE_NICK_TEMPLATE.matcher(name).matches())
		{
			show(new CustomMessage("scripts.services.Rename.incorrectinput", player), player);
			return;
		}

		if(getItemCount(player, Config.SERVICES_CHANGE_NICK_ITEM) < Config.SERVICES_CHANGE_NICK_PRICE)
		{
			if(Config.SERVICES_CHANGE_NICK_ITEM == 57)
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			else
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement offline = null;
		ResultSet rs = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			offline = con.prepareStatement("SELECT char_name FROM characters WHERE char_name LIKE ?");
			offline.setString(1, name);
			rs = offline.executeQuery();
			if(rs.next())
			{
				show(new CustomMessage("scripts.services.Rename.Thisnamealreadyexists", player), player);
				return;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			show(new CustomMessage("common.Error", player), player);
			return;
		}
		finally
		{
			DbUtils.closeQuietly(con, offline, rs);
		}

		removeItem(player, Config.SERVICES_CHANGE_NICK_ITEM, Config.SERVICES_CHANGE_NICK_PRICE);

		String oldName = player.getName();
		player.reName(name, true);
		Log.add("Character " + oldName + "[" + player.getObjectId() + "] renamed to " + name + "[" + player.getObjectId() + "]", "renames");
		show(new CustomMessage("scripts.services.Rename.changedname", player).addString(oldName).addString(name), player);
	}

	public void changesex()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_SEX_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.SEX_CHANGE))
			return;

		if(player.getRace() == Race.kamael)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		if(getItemCount(player, Config.SERVICES_CHANGE_SEX_ITEM) < Config.SERVICES_CHANGE_SEX_PRICE)
		{
			if(Config.SERVICES_CHANGE_SEX_ITEM == 57)
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			else
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		ThreadConnection con = null;
		FiltredPreparedStatement offline = null;

		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			offline = con.prepareStatement("UPDATE characters SET sex = ? WHERE obj_Id = ?");
			offline.setInt(1, player.getSex() == 1 ? 0 : 1);
			offline.setInt(2, player.getObjectId());
			offline.executeUpdate();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			show(new CustomMessage("common.Error", player), player);
			return;
		}
		finally
		{
			DbUtils.closeQuietly(con, offline);
		}

		player.setHairColor(0);
		player.setHairStyle(0);
		player.setFace(0);
		removeItem(player, Config.SERVICES_CHANGE_SEX_ITEM, Config.SERVICES_CHANGE_SEX_PRICE);
		player.logout(false, false, false, true);
		Log.add("Character " + player + " sex changed to " + (player.getSex() == 1 ? "male" : "female"), "renames");
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Nick change");
	}

	public void rename_clan_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(player.getClan() == null || !player.isClanLeader())
		{
			player.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_A_CLAN_LEADER).addName(player));
			return;
		}

		String append = "!Смена имени Клана";
		append += "<br>";
		append += "<font color=\"LEVEL\">" + new CustomMessage("scripts.services.Rename.RenameFor", getSelf()).addString(Util.formatAdena(Config.SERVICES_CHANGE_CLAN_NAME_PRICE)).addItemName(Config.SERVICES_CHANGE_CLAN_NAME_ITEM) + "</font>";
		append += "<table>";
		append += "<tr><td>" + new CustomMessage("scripts.services.Rename.NewName", getSelf()) + ": <edit var=\"new_name\" width=80></td></tr>";
		append += "<tr><td></td></tr>";
		append += "<tr><td><button value=\"" + new CustomMessage("scripts.services.Rename.RenameButton", getSelf()) + "\" action=\"bypass -h scripts_services.Rename:rename_clan $new_name\" width=80 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>";
		append += "</table>";
		show(append, player);
	}

	public void rename_clan(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null || param == null || param.length == 0)
			return;

		if(!Config.SERVICES_CHANGE_CLAN_NAME_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.CLAN_NAME_CHANGE))
			return;

		if(player.getClan() == null || !player.isClanLeader())
		{
			player.sendPacket(new SystemMessage(SystemMessage.S1_IS_NOT_A_CLAN_LEADER).addName(player));
			return;
		}

		if(player.getSiegeState() != 0)
		{
			show(new CustomMessage("scripts.services.Rename.SiegeNow", player), player);
			return;
		}

		if(!Config.CLAN_NAME_TEMPLATE.matcher(param[0]).matches())
		{
			player.sendPacket(Msg.CLAN_NAME_IS_INCORRECT);
			return;
		}
		if(ClanTable.getInstance().getClanByName(param[0]) != null)
		{
			player.sendPacket(Msg.THIS_NAME_ALREADY_EXISTS);
			return;
		}

		if(getItemCount(player, Config.SERVICES_CHANGE_CLAN_NAME_ITEM) < Config.SERVICES_CHANGE_CLAN_NAME_PRICE)
		{
			if(Config.SERVICES_CHANGE_CLAN_NAME_ITEM == 57)
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			else
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		show(new CustomMessage("scripts.services.Rename.changedname", player).addString(player.getClan().getName()).addString(param[0]), player);
		player.getClan().setName(param[0]);
		player.getClan().updateClanInDB();
		removeItem(player, Config.SERVICES_CHANGE_CLAN_NAME_ITEM, Config.SERVICES_CHANGE_CLAN_NAME_PRICE);
		player.getClan().broadcastClanStatus(true, true, false);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
