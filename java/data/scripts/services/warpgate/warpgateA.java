package services.warpgate;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.util.Files;
import l2n.util.Location;

public class warpgateA extends Functions implements ScriptFile
{
	public void enter()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null || !L2NpcInstance.canBypassCheck(player, npc))
			return;

		if(player.getLevel() < 75)
			show(Files.read("data/scripts/services/warpgate/32319-1.html", player), player);
		if(player.isGM() || player.isQuestCompleted("_130_PathToHellbound") || player.isQuestCompleted("_133_ThatsBloodyHot"))
		{
			player.broadcastPacketToOthers(new MagicSkillUse(player, player, 4671, 1, 500, 0));
			Location pos = GeoEngine.findPointToStay(-11095, 236440, -3232, 50, 100); // точка телепорта на Hellbound Island
			player.teleToLocation(pos);
		}
		else
		{
			if(player.getVar("lang@").equalsIgnoreCase("en"))
				player.sendMessage("The quest 'Path to Hellbound' or 'Thats Bloody Hot' should be finished.");
			else
				player.sendMessage("Квест 'Path to Hellbound' или 'Thats Bloody Hot' должен быть завершен.");
		}
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Warp Gates");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
