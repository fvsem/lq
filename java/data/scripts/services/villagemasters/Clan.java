package services.villagemasters;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2VillageMasterInstance;

public class Clan extends Functions implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Villagemasters [Clan Operations]");
	}

	public void CheckCreateClan()
	{
		L2Player pl = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(pl == null || npc == null)
			return;
		String htmltext = "clan-02.htm";
		// Player less 10 levels, and can not create clan
		if(pl.getLevel() <= 9)
			htmltext = "clan-06.htm";
		// Player already is a clan by leader and can not newly create clan
		else if(pl.isClanLeader())
			htmltext = "clan-07.htm";
		// Player already consists in clan and can not create clan
		else if(pl.getClan() != null)
			htmltext = "clan-09.htm";
		((L2VillageMasterInstance) npc).showChatWindow(pl, "data/html/villagemaster/" + htmltext);
	}

	public void CheckDissolveClan()
	{
		L2Player pl = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(pl == null || npc == null)
			return;
		String htmltext = "clan-01.htm";
		if(pl.isClanLeader())
			htmltext = "clan-04.htm";
		else
		// Player already consists in clan and can not create clan
		if(pl.getClan() != null)
			htmltext = "9000-08.htm";
		// Player not in clan and can not dismiss clan
		else
			htmltext = "9000-11.htm";
		((L2VillageMasterInstance) npc).showChatWindow(pl, "data/html/villagemaster/" + htmltext);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
