package services.villagemasters;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2VillageMasterInstance;

public class Ally extends Functions implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Villagemasters [Alliance Operations]");
	}

	public void CheckCreateAlly()
	{
		L2Player pl = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(pl == null || npc == null)
			return;
		String htmltext = "ally-01.htm";
		if(pl.isClanLeader())
			htmltext = "ally-02.htm";
		((L2VillageMasterInstance) npc).showChatWindow(pl, "data/html/villagemaster/" + htmltext);
	}

	public void CheckDissolveAlly()
	{
		L2Player pl = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(pl == null || npc == null)
			return;
		String htmltext = "ally-01.htm";
		if(pl.isAllyLeader())
			htmltext = "ally-03.htm";
		((L2VillageMasterInstance) npc).showChatWindow(pl, "data/html/villagemaster/" + htmltext);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
