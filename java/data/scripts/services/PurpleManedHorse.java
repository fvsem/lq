package services;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;

public class PurpleManedHorse extends Functions implements ScriptFile
{
	private static boolean Enabled = Config.SERVECES_RIDEHIRE;
	private static final int MultiSellID = 40012;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Purple-Maned Horses");
	}

	public String PetManagersDialogAppend(Integer val)
	{
		if(val == 0 && Enabled)
		{
			String text = isRus() ? "|Приобрести новых питомцев.]" : "|Buy New Pets.]";
			return "</br></br>[npc_%objectId%_Multisell " + MultiSellID + text;
		}
		return "";
	}

	public String DialogAppend_30731(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30827(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30828(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30829(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30830(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30831(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_30869(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_31067(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_31265(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_31309(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	public String DialogAppend_31954(Integer val)
	{
		return PetManagersDialogAppend(val);
	}

	private static boolean isRus(L2Player player)
	{
		String slang = player.getVar("lang@");
		return slang != null && slang.equalsIgnoreCase("ru");
	}

	private boolean isRus()
	{
		return isRus((L2Player) getSelf());
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
