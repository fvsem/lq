package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;

/**
 * Используется NPC Lekon (id: 32557)
 * 
 * @<!-- L2System -->
 * @date 13.04.2011
 * @time 15:48:40
 */
public class AirshipLicense extends Functions implements ScriptFile
{
	private static final int ENERGY_STAR_STONE = 13277;
	private static final int AIRSHIP_SUMMON_LICENSE = 13559;

	public void sell()
	{
		L2Player player = (L2Player) getSelf();

		if(player.getClan() == null || !player.isClanLeader() || player.getClan().getLevel() < 5)
		{
			show("data/html/default/32557-2.htm", player);
			return;
		}

		if(player.getClan().isHaveAirshipLicense() || Functions.getItemCount(player, AIRSHIP_SUMMON_LICENSE) > 0)
		{
			show("data/html/default/32557-4.htm", player);
			return;
		}

		if(Functions.removeItem(player, ENERGY_STAR_STONE, 10) != 10)
		{
			show("data/html/default/32557-3.htm", player);
			return;
		}

		Functions.addItem(player, AIRSHIP_SUMMON_LICENSE, 1);
	}

	@Override
	public void onLoad()
	{}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
