package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.tables.DoorTable;

public class BorderOutpostDoormans extends Functions implements ScriptFile
{
	private static int DoorId = 24170001;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Border Outpost Doormans");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void openDoor()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		L2DoorInstance door = DoorTable.getInstance().getDoor(DoorId);
		if(!door.isOpen())
			door.openMe();
	}

	public void closeDoor()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		L2DoorInstance door = DoorTable.getInstance().getDoor(DoorId);
		if(door.isOpen())
			door.closeMe();
	}
}
