package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Files;

public class RemoveDeathPenalty extends Functions implements ScriptFile
{
	public void showdialog()
	{
		final L2Player player = (L2Player) getSelf();
		String htmltext;
		if(player.getDeathPenalty().getLevel() > 0)
		{
			htmltext = Files.read("data/scripts/services/RemoveDeathPenalty-1.htm", player);
			htmltext += "<a action=\"bypass -h scripts_services.RemoveDeathPenalty:remove\">Remove 1 level of Death Penalty (" + getPrice() + " adena).</a>";
		}
		else
			htmltext = Files.read("data/scripts/services/RemoveDeathPenalty-0.htm", player);

		show(htmltext, (L2Player) getSelf());
	}

	public void remove()
	{
		final L2NpcInstance npc = getNpc();
		if(npc == null)
			return;
		final L2Player player = (L2Player) getSelf();
		if(player.getDeathPenalty().getLevel() > 0)
			if(player.getAdena() >= getPrice())
			{
				player.reduceAdena(getPrice(), true);
				npc.doCast(SkillTable.getInstance().getInfo(5077, 1), player, false);
			}
			else
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			show(Files.read("data/scripts/services/RemoveDeathPenalty-0.htm", player), player);
	}

	public int getPrice()
	{
		final byte playerLvl = ((L2Player) getSelf()).getLevel();
		if(playerLvl <= 19)
			return 3600;
		else if(playerLvl >= 20 && playerLvl <= 39)
			return 16400;
		else if(playerLvl >= 40 && playerLvl <= 51)
			return 36200;
		else if(playerLvl >= 52 && playerLvl <= 60)
			return 50400;
		else if(playerLvl >= 61 && playerLvl <= 75)
			return 78200;
		else
			return 102800;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: NPC RemoveDeathPenalty");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
