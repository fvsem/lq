package services;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Rnd;

public class SummonCorpse extends Functions implements ScriptFile
{
	private final static int SUMMON_PRICE = 200000;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Summon a corpse");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void doSummon()
	{
		L2Player player = (L2Player) getSelf();
		String fail = Files.read("data/html/default/32104-fail.htm", player);
		String success = Files.read("data/html/default/32104-success.htm", player);

		if(!player.isInParty())
		{
			show(fail, player);
			return;
		}

		int counter = 0;
		GArray<L2Player> partyMembers = player.getParty().getPartyMembers();
		for(L2Player partyMember : partyMembers)
			if(partyMember != null && partyMember.isDead())
			{
				counter++;
				if(player.getAdena() < SUMMON_PRICE)
				{
					player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
					return;
				}
				player.reduceAdena(SUMMON_PRICE, true);
				Location coords = new Location(11255 + Rnd.get(-20, 20), -23370 + Rnd.get(-20, 20), -3649);
				partyMember.summonCharacterRequest(player.getName(), coords, 0);
			}

		if(counter == 0)
			show(fail, player);
		else
			show(success, player);
	}
}
