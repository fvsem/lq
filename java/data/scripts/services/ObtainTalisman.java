package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.util.Files;
import l2n.util.Rnd;

import java.util.ArrayList;

public class ObtainTalisman extends Functions implements ScriptFile
{
	public static final int KNIGHT_EPAULETTE = 9912;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Obtain Talisman");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void Obtain()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(player.isActionsDisabled() || player.isSitting() || player.getLastNpc() == null || player.getLastNpc().getDistance(player) > 300)
			return;

		if(!player.isQuestContinuationPossible(true))
		{
			player.sendPacket(new SystemMessage(SystemMessage.YOUR_INVENTORY_IS_FULL));
			return;
		}

		if(getItemCount(player, KNIGHT_EPAULETTE) < 10)
		{
			show(Files.read("data/scripts/services/ObtainTalisman-no.htm", player), player);
			return;
		}

		final ArrayList<Integer> talismans = new ArrayList<Integer>();

		for(int i = 9914; i <= 9966; i++)
			talismans.add(i);
		for(int i = 10416; i <= 10424; i++)
			talismans.add(i);
		for(int i = 10518; i <= 10519; i++)
			talismans.add(i);
		for(int i = 10533; i <= 10543; i++)
			talismans.add(i);
		for(int i = 12815; i <= 12818; i++)
			talismans.add(i);

		removeItem(player, KNIGHT_EPAULETTE, 10);
		addItem(player, talismans.get(Rnd.get(talismans.size())), 1);
		show(Files.read("data/scripts/services/ObtainTalisman.htm", player), player);
	}
}
