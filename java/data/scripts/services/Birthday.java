package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.geodata.GeoEngine;
import l2n.game.model.L2ObjectTasks;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2World;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.PlaySound;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SkillTable;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Util;

import java.util.Calendar;

public class Birthday extends Functions implements ScriptFile
{
	private static final int BIRTHDAY_HAT = 13488;
	private static final int BIRTHDAY_CAKE = 5950;

	private static final int NPC_ALEGRIA = 32600;
	private static final String msgNotToday = "data/scripts/services/Birthday-no.htm";
	private static final String msgAlreadyRecived = "data/scripts/services/Birthday-already.htm";
	private static final String msgSpawned = "data/scripts/services/Birthday-spawned.htm";

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Birthday");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void reciveGift()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();

		if(player == null || npc == null || !L2NpcInstance.canBypassCheck(player, player.getLastNpc()))
			return;

		if(!isBirthdayToday(player))
		{
			show(Files.read(msgNotToday, player), player);
			return;
		}

		if(isGiftRecivedToday(player))
		{
			show(Files.read(msgAlreadyRecived, player), player);
			return;
		}

		for(L2NpcInstance n : L2World.getAroundNpc(npc))
			if(n.getNpcId() == NPC_ALEGRIA)
			{
				show(Files.read(msgSpawned, player), player);
				return;
			}

		player.sendPacket(new PlaySound(1, "HB01", 0, 0, new Location()));

		try
		{
			Location loc = GeoEngine.findPointToStay(npc.getX(), npc.getY(), npc.getZ(), 40, 60);
			loc.setH(Util.getHeadingTo(loc, player.getLoc()));
			L2Spawn spawn = new L2Spawn(NpcTable.getTemplate(NPC_ALEGRIA));
			spawn.setLoc(loc);
			spawn.doSpawn(true);

			L2GameThreadPools.getInstance().scheduleAi(new DeSpawnScheduleTimerTask(spawn), 180000, false);
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public void reciveGiftAlegria()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();

		if(player == null || npc == null || !L2NpcInstance.canBypassCheck(player, player.getLastNpc()) || npc.isBusy())
			return;

		if(!isBirthdayToday(player))
		{
			show(Files.read(msgNotToday, player), player);
			return;
		}

		if(isGiftRecivedToday(player))
		{
			show(Files.read(msgAlreadyRecived, player), player);
			return;
		}

		npc.altUseSkill(SkillTable.getInstance().getInfo(BIRTHDAY_CAKE, 1), player);
		addItem(player, BIRTHDAY_HAT, 1);
		show(Files.read("data/html/default/32600-2.htm ", player), player);

		long now = System.currentTimeMillis() / 1000;
		player.setVar("Birthday", String.valueOf(now));

		L2GameThreadPools.getInstance().scheduleGeneral(new L2ObjectTasks.DeleteTask(npc), 1000);
		npc.setBusy(true);
	}

	private boolean isBirthdayToday(L2Player player)
	{
		if(player.getCreateTime() == 0)
			return false;

		Calendar create = Calendar.getInstance();
		create.setTimeInMillis(player.getCreateTime());
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(System.currentTimeMillis());

		return create.get(Calendar.MONTH) == now.get(Calendar.MONTH) && create.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH) && create.get(Calendar.YEAR) != now.get(Calendar.YEAR);
	}

	private boolean isGiftRecivedToday(L2Player player)
	{
		int lastBirthday = 0;
		try
		{
			String var = player.getVar("Birthday");
			if(var != null && !var.equals("null"))
				lastBirthday = Integer.parseInt(var);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return true;
		}

		Calendar birthday = Calendar.getInstance();
		birthday.setTimeInMillis(lastBirthday * 1000L);
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(System.currentTimeMillis());

		return birthday.get(Calendar.YEAR) == now.get(Calendar.YEAR) && birthday.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR);
	}

	public class DeSpawnScheduleTimerTask implements Runnable
	{
		L2Spawn spawned = null;

		public DeSpawnScheduleTimerTask(L2Spawn spawn)
		{
			spawned = spawn;
		}

		@Override
		public void run()
		{
			try
			{
				spawned.getLastSpawn().decayMe();
				spawned.getLastSpawn().deleteMe();
			}
			catch(Throwable t)
			{}
		}
	}
}
