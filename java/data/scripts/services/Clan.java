package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.CastleSiegeManager;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.L2Clan;
import l2n.game.model.L2ClanMember;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.PledgeShowInfoUpdate;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;
import l2n.util.Util;

public class Clan extends Functions implements ScriptFile
{
	public void getPoints(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CLAN_BUY_POINTS_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		L2Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("Вы должны быть в клане.");
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.CLAN_BUY_POINTS))
			return;

		int n = Integer.parseInt(param[0]);
		int countCRP = Config.SERVICES_CLAN_BUY_POINTS_PRICE[n][0];
		int price = Config.SERVICES_CLAN_BUY_POINTS_PRICE[n][1];

		if(Functions.getItemCount(player, Config.SERVICES_CLAN_BUY_POINTS_ITEM) < price)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		if(player.getInventory().destroyItemByItemId(Config.SERVICES_CLAN_BUY_POINTS_ITEM, price, false) != null)
		{
			player.sendPacket(SystemMessage.removeItems(Config.SERVICES_CLAN_BUY_POINTS_ITEM, price));
			clan.incReputation(countCRP, false, "ClanService");
			clan.broadcastToOnlineMembers(new PledgeShowInfoUpdate(clan));
			player.sendMessage(new CustomMessage("l2n.game.model.instances.L2ClanTraderInstance", player).addNumber(countCRP));
		}
	}

	public void levelUpClan(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CLAN_BUY_LEVEL_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		L2Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("Вы должны быть в клане.");
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.CLAN_BUY_LEVEL))
			return;

		int new_level = Integer.parseInt(param[0]);
		int diff = new_level - clan.getLevel();
		int price = Config.SERVICES_CLAN_BUY_LEVEL_PRICE[new_level - 1];

		if(Functions.getItemCount(player, Config.SERVICES_CLAN_BUY_LEVEL_ITEM) < price)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		if(player.getInventory().destroyItemByItemId(Config.SERVICES_CLAN_BUY_LEVEL_ITEM, price, false) != null)
		{
			player.sendPacket(SystemMessage.removeItems(Config.SERVICES_CLAN_BUY_LEVEL_ITEM, price));
			clan.setLevel((byte) (clan.getLevel() + diff));
			clan.updateClanInDB();
			player.broadcastPacket(new MagicSkillUse(player, player, 5103, 1, 1000, 0));

			if(clan.getLevel() >= CastleSiegeManager.getSiegeClanMinLevel())
				SiegeManager.addSiegeSkills(player);

			if(clan.getLevel() == 5)
				player.sendPacket(Msg.NOW_THAT_YOUR_CLAN_LEVEL_IS_ABOVE_LEVEL_5_IT_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS);

			// notify all the members about it
			final PledgeShowInfoUpdate pu = new PledgeShowInfoUpdate(clan);
			for(L2ClanMember mbr : clan.getMembers())
				if(mbr.isOnline())
				{
					mbr.getPlayer().updatePledgeClass();
					mbr.getPlayer().sendPacket(Msg.CLANS_SKILL_LEVEL_HAS_INCREASED);
					mbr.getPlayer().sendPacket(pu);
					mbr.getPlayer().broadcastUserInfo(true);
				}
		}

	}

	public void points_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CLAN_BUY_POINTS_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		L2Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("Вы должны быть в клане.");
			return;
		}

		// Уровень клана должен быть 5 или больше.
		if(clan.getLevel() < 5)
		{
			show(Util.printCM(new CustomMessage("scripts.services.Clan.MinLvl", player)), player);
			return;
		}

		String item_name = ItemTable.getInstance().getTemplate(Config.SERVICES_CLAN_BUY_POINTS_ITEM).getName();

		String append = "Покупка очков репутации клана:";
		append += "<br>";
		append += "Клан " + clan.getName() + ": уровень <font color=\"LEVEL\">" + clan.getLevel() + "</font>, CRP <font color=\"LEVEL\">" + clan.getReputationScore() + "</font>";
		append += "<br>";
		append += "<table>";
		append += "<tr><td><center>CRP</center></td><td><center>Цена</center></td></tr>";
		for(int i = 0; i < Config.SERVICES_CLAN_BUY_POINTS_PRICE.length; i++)
		{
			append +=
					"<tr>" +
							"<td><font color=\"FF9900\">" + Config.SERVICES_CLAN_BUY_POINTS_PRICE[i][0] + "</font> CRP</td>" +
							"<td><font color=\"FF9900\">" + Config.SERVICES_CLAN_BUY_POINTS_PRICE[i][1] + "</font> " + item_name + "</td>" +
							"<td>" + "<button value=\"Купить\" action=\"bypass -h scripts_services.Clan:getPoints " + i + "\" width=60 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>" +
							"</tr>";
		}

		append += "</table>";
		show(append, player);
	}

	public void level_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CLAN_BUY_LEVEL_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		L2Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("Вы должны быть в клане.");
			return;
		}

		String item_name = ItemTable.getInstance().getTemplate(Config.SERVICES_CLAN_BUY_LEVEL_ITEM).getName();
		int level = clan.getLevel();

		String append = "Покупка уровня клана:";
		append += "<br>";
		append += "Клан " + clan.getName() + ": уровень <font color=\"LEVEL\">" + level + "</font>";
		append += "<br>";
		append += "<table>";
		append += "<tr><td><center>Уровень</center></td><td><center>Цена</center></td></tr>";
		for(int i = level; i < Config.SERVICES_CLAN_BUY_LEVEL_PRICE.length; i++)
		{
			int new_level = i + 1;
			append +=
					"<tr>" +
							"<td>Уровень <font color=\"FF9900\">" + new_level + "</font></td>" +
							"<td><font color=\"FF9900\">" + Config.SERVICES_CLAN_BUY_LEVEL_PRICE[i] + "</font> " + item_name + "</td>" +
							"<td>" + "<button value=\"Купить\" action=\"bypass -h scripts_services.Clan:levelUpClan " + new_level + "\" width=60 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>" +
							"</tr>";
		}

		append += "</table>";
		show(append, player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Clan");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
