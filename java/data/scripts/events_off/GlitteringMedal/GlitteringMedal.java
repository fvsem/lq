package events_off.GlitteringMedal;

import l2n.Config;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.cache.Msg;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Experience;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Rnd;

import java.util.ArrayList;

public class GlitteringMedal extends EventScript implements ScriptFile
{
	private final static int EVENT_MANAGER_ID1 = 31228; // Roy
	private final static int EVENT_MANAGER_ID2 = 31229; // Winnie

	// Шанс выбить медали
	private final static double MEDAL_CHANCE = Config.GLIT_MEDAL_CHANCE;
	private final static double GLITTMEDAL_CHANCE = Config.GLIT_GLITTMEDAL_CHANCE;

	// Медали
	private final static int ITEM_EVENT_MEDAL = 6392;
	private final static int ITEM_EVENT_GLITTERING_MEDAL = 6393;

	private final static int crest_of_rabbit = 6399;
	private final static int crest_of_hyena = 6400;
	private final static int crest_of_fox = 6401;
	private final static int crest_of_wolf = 6402;

	private final static ArrayList<L2Spawn> _spawns = new ArrayList<L2Spawn>();
	private static boolean _active = false;

	@Override
	public void onLoad()
	{
		if(isActive("glitter"))
		{
			_active = true;
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Loaded Event: L2 Medal Collection Event [state: activated]");
			if(MEDAL_CHANCE > 80 || GLITTMEDAL_CHANCE > 50)
				System.out.println("Event L2 Medal Collection: << W A R N I N G >> RATES IS TO HIGH!!!");
		}
		else
			System.out.println("Loaded Event: L2 Medal Collection Event [state: deactivated]");
	}

	/**
	 * Запускает эвент
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("glitter", true))
		{
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Event 'L2 Medal Collection Event' started.");
			Announcements.announceByCustomMessage("scripts.events_off.GlitteringMedal.GlitteringMedal.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event 'L2 Medal Collection Event' already started.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Останавливает эвент
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("glitter", false))
		{
			removeEventId(ScriptEventType.ON_DIE);
			removeEventId(ScriptEventType.ON_ENTER_WORLD);
			unSpawnEventManagers();
			System.out.println("Event 'L2 Medal Collection Event' stopped.");
			Announcements.announceByCustomMessage("scripts.events_off.GlitteringMedal.GlitteringMedal.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event 'L2 Medal Collection Event' not started.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Спавнит эвент менеджеров
	 */
	private void spawnEventManagers()
	{
		// 1й эвент кот
		final int EVENT_MANAGERS1[][] = {
				{ 147893, -56622, -2776, 0 },
				{ -81070, 149960, -3040, 0 },
				{ 82882, 149332, -3464, 49000 },
				{ 44176, -48732, -800, 33000 },
				{ 147920, 25664, -2000, 16384 },
				{ 117498, 76630, -2695, 38000 },
				{ 111776, 221104, -3543, 16384 },
				{ -84516, 242971, -3730, 34000 },
				{ -13073, 122801, -3117, 0 },
				{ -44337, -113669, -224, 0 },
				{ 11281, 15652, -4584, 25000 },
				{ 44122, 50784, -3059, 57344 },
				{ 80986, 54504, -1525, 32768 },
				{ 114733, -178691, -821, 0 },
				{ 18178, 145149, -3054, 7400 }, };

		L2NpcTemplate template = NpcTable.getTemplate(EVENT_MANAGER_ID1);
		for(int[] element : EVENT_MANAGERS1)
			try
			{
				L2Spawn sp = new L2Spawn(template);
				sp.setLocx(element[0]);
				sp.setLocy(element[1]);
				sp.setLocz(element[2]);
				sp.setAmount(1);
				sp.setHeading(element[3]);
				sp.setRespawnDelay(0);
				sp.init();
				_spawns.add(sp);
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}

		// 2й эвент кот
		final int EVENT_MANAGERS2[][] = {
				{ 147960, -56584, -2776, 0 },
				{ -81070, 149860, -3040, 0 },
				{ 82798, 149332, -3464, 49000 },
				{ 44176, -48688, -800, 33000 },
				{ 147985, 25664, -2000, 16384 },
				{ 117459, 76664, -2695, 38000 },
				{ 111724, 221111, -3543, 16384 },
				{ -84516, 243015, -3730, 34000 },
				{ -13073, 122841, -3117, 0 },
				{ -44342, -113726, -240, 0 },
				{ 11327, 15682, -4584, 25000 },
				{ 44157, 50827, -3059, 57344 },
				{ 80986, 54452, -1525, 32768 },
				{ 114719, -178742, -821, 0 },
				{ 18154, 145192, -3054, 7400 }, };

		template = NpcTable.getTemplate(EVENT_MANAGER_ID2);
		for(int[] element : EVENT_MANAGERS2)
			try
			{
				L2Spawn sp = new L2Spawn(template);
				sp.setLocx(element[0]);
				sp.setLocy(element[1]);
				sp.setLocz(element[2]);
				sp.setAmount(1);
				sp.setHeading(element[3]);
				sp.setRespawnDelay(0);
				sp.init();
				_spawns.add(sp);
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
	}

	/**
	 * Удаляет спавн эвент менеджеров
	 */
	private void unSpawnEventManagers()
	{
		for(L2Spawn sp : _spawns)
		{
			sp.stopRespawn();
			sp.getLastSpawn().deleteMe();
		}
		_spawns.clear();
	}

	@Override
	public void onReload()
	{
		unSpawnEventManagers();
	}

	@Override
	public void onShutdown()
	{
		unSpawnEventManagers();
	}

	@Override
	protected void onEnterWorld(L2Player player)
	{
		if(_active)
			Announcements.announceToPlayerByCustomMessage(player, "scripts.events_off.GlitteringMedal.GlitteringMedal.AnnounceEventStarted", null);
	}

	/**
	 * Обработчик смерти мобов, управляющий эвентовым дропом
	 */
	@Override
	protected void onDie(final L2Character cha, final L2Character killer)
	{
		if(_active && cha.isMonster() && !cha.isRaid() && killer != null && killer.getPlayer() != null && Math.abs(cha.getLevel() - killer.getLevel()) < 10)
		{
			// пропускаем
			if(Config.EXCEPTION_MONSTER_LIST.contains(cha.getNpcId()))
				return;

			if(calcChance((L2NpcInstance) cha, killer.getPlayer(), MEDAL_CHANCE))
			{
				L2ItemInstance item = ItemTable.getInstance().createItem(ITEM_EVENT_MEDAL, killer.getObjectId(), 0, "L2MedalEvent");
				((L2NpcInstance) cha).dropItem(killer.getPlayer(), item);
			}
			if(getItemCount(killer.getPlayer(), crest_of_wolf) == 0 && calcChance((L2NpcInstance) cha, killer.getPlayer(), GLITTMEDAL_CHANCE))
			{
				L2ItemInstance item = ItemTable.getInstance().createItem(ITEM_EVENT_GLITTERING_MEDAL, killer.getObjectId(), 0, "L2MedalEvent");
				((L2NpcInstance) cha).dropItem(killer.getPlayer(), item);
			}
		}
	}

	private boolean calcChance(final L2NpcInstance monster, final L2Player player, double chance)
	{
		chance *= player.getRateItems();
		int diff = monster.calculateLevelDiffForDrop(player.getLevel(), false);
		if(diff > 0)
			return Rnd.chance(chance * Experience.penaltyModifier(diff, 9));
		return Rnd.chance(chance);
	}

	public void glitchang()
	{
		L2Player player = (L2Player) getSelf();

		if(getItemCount(player, ITEM_EVENT_MEDAL) >= 1000)
		{
			removeItem(player, ITEM_EVENT_MEDAL, 1000);
			addItem(player, ITEM_EVENT_GLITTERING_MEDAL, 10);
			return;
		}
		player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
	}

	public void medal()
	{
		L2Player player = (L2Player) getSelf();

		if(getItemCount(player, crest_of_wolf) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent1_q0996_05.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_fox) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent1_q0996_04.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_hyena) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent1_q0996_03.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_rabbit) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent1_q0996_02.htm", player), player);
			return;
		}

		show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent1_q0996_01.htm", player), player);
		return;
	}

	public void medalb()
	{
		L2Player player = (L2Player) getSelf();

		if(getItemCount(player, crest_of_wolf) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_05.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_fox) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_04.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_hyena) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_03.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_rabbit) >= 1)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_02.htm", player), player);
			return;
		}

		show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_01.htm", player), player);
		return;
	}

	public void game()
	{
		L2Player player = (L2Player) getSelf();

		if(getItemCount(player, crest_of_fox) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 40)
			{
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_11.htm", player), player);
				return;
			}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_12.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_hyena) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 20)
			{
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_11.htm", player), player);
				return;
			}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_12.htm", player), player);
			return;
		}
		else if(getItemCount(player, crest_of_rabbit) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 10)
			{
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_11.htm", player), player);
				return;
			}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_12.htm", player), player);
			return;
		}

		else if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 5)
		{
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_11.htm", player), player);
			return;
		}

		show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_12.htm", player), player);
		return;
	}

	public void gamea()
	{
		L2Player player = (L2Player) getSelf();
		// Для временного статуса который выдается в игре рандомно либо 0 либо 1
		final int flag = Rnd.get(2);

		if(getItemCount(player, crest_of_fox) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 40)
				if(flag == 1)
				{
					removeItem(player, crest_of_fox, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 40);
					addItem(player, crest_of_wolf, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_24.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 40);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_25.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		else if(getItemCount(player, crest_of_hyena) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 20)
				if(flag == 1)
				{
					removeItem(player, crest_of_hyena, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 20);
					addItem(player, crest_of_fox, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_23.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 20);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_25.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		else if(getItemCount(player, crest_of_rabbit) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 10)
				if(flag == 1)
				{
					removeItem(player, crest_of_rabbit, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 10);
					addItem(player, crest_of_hyena, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_22.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 10);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_25.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 5)
			if(flag == 1)
			{
				removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 5);
				addItem(player, crest_of_rabbit, 1);
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_21.htm", player), player);
				return;
			}
			else if(flag == 0)
			{
				removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 5);
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_25.htm", player), player);
				return;
			}
		show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
		return;
	}

	public void gameb()
	{
		L2Player player = (L2Player) getSelf();
		// Для временного статуса который выдается в игре рандомно либо 0 либо 1
		final int flag = Rnd.get(2);

		if(getItemCount(player, crest_of_fox) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 40)
				if(flag == 1)
				{
					removeItem(player, crest_of_fox, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 40);
					addItem(player, crest_of_wolf, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_34.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 40);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_35.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		else if(getItemCount(player, crest_of_hyena) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 20)
				if(flag == 1)
				{
					removeItem(player, crest_of_hyena, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 20);
					addItem(player, crest_of_fox, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_33.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 20);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_35.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		else if(getItemCount(player, crest_of_rabbit) >= 1)
		{
			if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 10)
				if(flag == 1)
				{
					removeItem(player, crest_of_rabbit, 1);
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 10);
					addItem(player, crest_of_hyena, 1);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_32.htm", player), player);
					return;
				}
				else if(flag == 0)
				{
					removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 10);
					show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_35.htm", player), player);
					return;
				}
			show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
			return;
		}

		if(getItemCount(player, ITEM_EVENT_GLITTERING_MEDAL) >= 5)
			if(flag == 1)
			{
				removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 5);
				addItem(player, crest_of_rabbit, 1);
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_31.htm", player), player);
				return;
			}
			else if(flag == 0)
			{
				removeItem(player, ITEM_EVENT_GLITTERING_MEDAL, 5);
				show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_35.htm", player), player);
				return;
			}
		show(Files.read("data/scripts/events_off/GlitteringMedal/event_col_agent2_q0996_26.htm", player), player);
		return;
	}
}
