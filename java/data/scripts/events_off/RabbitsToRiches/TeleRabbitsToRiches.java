package events_off.RabbitsToRiches;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

public class TeleRabbitsToRiches extends Functions implements ScriptFile
{
	private static final int RABBIT_TRANSFORMATION_SCROLL = 10274;

	@Override
	public void onLoad()
	{}

	public void TeleRabbits()
	{
		L2Player player = (L2Player) getSelf();

		if(player.getInventory().getItemByItemId(RABBIT_TRANSFORMATION_SCROLL) != null && player.getInventory().getItemByItemId(RABBIT_TRANSFORMATION_SCROLL).getCount() >= 1)
			player.teleToLocation(-59703, -56061, -20360);
		else
			show(Files.read("data/scripts/events_off/RabbitsToRiches/NoTele.htm", player), player);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
