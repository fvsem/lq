package events_off.SpooksAndScarecrows;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

/**
 * Хэллоуин не за углом и Lineage II дарит вам пару изюминок!
 * 20 октября во вторник все игроки выше 20 уровня найдут у себя на складе
 * Scarecrow Jack Transformation Stick (Палка трансформации) и
 * 5 Revita Pop (леденцов виталити).
 */
public class SpooksAndScarecrows extends EventScript implements ScriptFile
{
	private static boolean _active = false;

	// Items
	private final static int SHADOW_JACK = 14271;
	private final static int REVIVA_POP = 20034;

	@Override
	public void onLoad()
	{
		if(isActive())
		{
			_active = true;
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			System.out.println("Loaded Event: Spooks & Scarecrows [state: activated]");
		}
		else
			System.out.println("Loaded Event: Spooks & Scarecrows [state: deactivated]");
	}

	private static boolean isActive()
	{
		return isActive("SpooksAndScarecrows");
	}

	/**
	 * Loading Event
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("SpooksAndScarecrows", true))
		{
			System.out.println("Event: 'Spooks & Scarecrows' started.");

			Announcements.announceByCustomMessage("scripts.events_off.SpooksAndScarecrows.SpooksAndScarecrows.AnnounceEventStarted");
			for(L2Player p : L2ObjectsStorage.getAllPlayersForIterate())
			{
				int playerLvl = p.getLevel();
				if(playerLvl >= 20)
				{
					addItem(p, SHADOW_JACK, 1);
					addItem(p, REVIVA_POP, 5);
				}
				else if(playerLvl >= 20 && getItemCount(player, SHADOW_JACK) >= 1 && getItemCount(player, REVIVA_POP) >= 1)
					return;
			}
		}
		else
			player.sendMessage("Event 'Spooks & Scarecrows' already started.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("SpooksAndScarecrows", false))
		{
			removeEventId(ScriptEventType.ON_ENTER_WORLD);
			System.out.println("Event: 'Spooks & Scarecrows' stopped.");
			Announcements.announceByCustomMessage("scripts.events_off.SpooksAndScarecrows.SpooksAndScarecrows.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event: 'Spooks & Scarecrows' not started.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	protected void onEnterWorld(final L2Player player)
	{
		if(_active)
			Announcements.announceToPlayerByCustomMessage(player, "scripts.events_off.SpooksAndScarecrows.SpooksAndScarecrows.AnnounceEventStarted", null);
	}
}
