package events_off.GiftOfVitality;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillLaunched;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.SkillTable;
import l2n.util.Files;

/**
 * Build By Avaro'
 */
public class GiftOfVitality extends Functions implements ScriptFile
{
	// Reuse between buffs
	private static final int _hours = 24;

	private static final int _jack = 4306;

	private static boolean _active = false;
	private final static GArray<L2Spawn> _spawns = new GArray<L2Spawn>();

	private final static int[] fighterBuff = new int[] {//
	5627, // Wind Walk
			5628, // Shield
			5637, // Magic Barrier
			5629, // Bless the Body
			5630, // Vampiric Rage
			5631, // Regeneration
			5632, // Haste
	};
	private final static int[] magicBuff = new int[] {//
	5627, // Wind Walk
			5628, // Shield
			5637, // Magic Barrier
			5633, // Bless the Soul
			5634, // Acumen
			5635, // Concentration
			5636 // Empower
	};
	private final static int[] petBuff = new int[] {//
	5627, // Wind Walk
			5628, // Shield
			5637, // Magic Barrier
			5629, // Bless the Body
			5633, // Bless the Soul
			5630, // Vampiric Rage
			5634, // Acumen
			5631, // Regeneration
			5635, // Concentration
			5632, // Haste
			5636, // Empower
	};

	/**
	 * Спавнит эвент менеджеров
	 */
	private void spawnEventManagers()
	{
		final int EVENT_MANAGERS[][] = { { 82766, 149438, -3464, 33865 }, { 82286, 53291, -1488, 15250 }, { 147060, 25943, -2008, 18774 }, { 148096, -55466, -2728, 40541 },
				{ 87116, -141332, -1336, 52193 }, { 43521, -47542, -792, 31655 }, { 17203, 144949, -3024, 18166 }, { 111164, 221062, -3544, 2714 }, { -13869, 122063, -2984, 18270 },
				{ -83161, 150915, -3120, 17311 }, { 45402, 48355, -3056, 49153 }, { 115616, -177941, -896, 30708 }, { -44928, -113608, -192, 30212 }, { -84037, 243194, -3728, 8992 },
				{ -119690, 44583, 360, 29289 }, { 12084, 16576, -4584, 57345 } };

		spawnNPCs(_jack, EVENT_MANAGERS, _spawns);
	}

	/**
	 * Удаляет спавн эвент менеджеров
	 */
	private void unSpawnEventManagers()
	{
		deSpawnNPCs(_spawns);
	}

	/**
	 * Читает статус эвента из базы.
	 * 
	 * @return
	 */
	private boolean isActive()
	{
		return _active;
	}

	/**
	 * Запускает эвент
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("GiftOfVitality", true))
		{
			spawnEventManagers();
			System.out.println("Event: The Gift of Vitality started.");
			Announcements.announceByCustomMessage("scripts.events_off.GiftOfVitality.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event 'The Gift of Vitality' already started.");

		_active = true;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Останавливает эвент
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("GiftOfVitality", false))
		{
			unSpawnEventManagers();
			System.out.println("Event: The Gift of Vitality stopped.");
			Announcements.announceByCustomMessage("scripts.events_off.GiftOfVitality.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event 'The Gift of Vitality' not started.");

		_active = false;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public void vitality()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		long reuse = 0;
		String streuse = player.getVar("GOVreuse");
		if(streuse != null)
			reuse = Long.parseLong(streuse);
		if(reuse > System.currentTimeMillis())
		{
			long remainingTime = (reuse - System.currentTimeMillis()) / 1000;
			int hours = (int) (remainingTime / 3600);
			int minutes = (int) (remainingTime % 3600 / 60);
			SystemMessage sm = new SystemMessage(SystemMessage.S1_WILL_BE_AVAILABLE_FOR_REUSE_AFTER_S2_HOURS_S3_MINUTES);
			sm.addSkillName(23179);
			sm.addNumber(hours);
			sm.addNumber(minutes);
			player.sendPacket(sm);
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-notime.htm", player), player);
		}
		else
		{
			npc.doCast(SkillTable.getInstance().getInfo(23179, 1), player, true); // Gift of Vitality
			player.setVar("GOVreuse", String.valueOf(System.currentTimeMillis() + _hours * 60 * 60 * 1000));
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-okvitality.htm", player), player);
		}
	}

	public void memoriesPlayer()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(player.getLevel() < 76)
		{
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-nolevel.htm", player), player);
			return;
		}
		else
		{
			if(player.isMageClass())
			{
				int time = 0;
				L2Skill skill;
				for(int skillId : magicBuff)
				{
					skill = SkillTable.getInstance().getInfo(skillId, 1);
					time += skill.getHitTime();
					L2GameThreadPools.getInstance().scheduleGeneral(new BeginBuff(npc, skill, player), time);
					time += 200;
				}
			}
			else
			{
				int time = 0;
				L2Skill skill;
				for(int skillId : fighterBuff)
				{
					skill = SkillTable.getInstance().getInfo(skillId, 1);
					time += skill.getHitTime();
					L2GameThreadPools.getInstance().scheduleGeneral(new BeginBuff(npc, skill, player), time);
					time += 200;
				}
			}
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-okbuff.htm", player), player);
		}
	}

	public void memoriesSummon()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(player.getLevel() < 76)
		{
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-nolevel.htm", player), player);
			return;
		}
		else if(player.getPet() == null || !player.getPet().isPet())
		{
			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-nosummon.htm", player), player);
			return;
		}
		else
		{
			int time = 0;
			L2Skill skill;
			for(int skillId : petBuff)
			{
				skill = SkillTable.getInstance().getInfo(skillId, 1);
				time += skill.getHitTime();
				L2GameThreadPools.getInstance().scheduleGeneral(new BeginBuff(npc, skill, player.getPet()), time);
				time += 200;
			}

			show(Files.read("data/scripts/events_off/GiftOfVitality/4306-okbuff.htm", player), player);
		}
	}

	public void showHtml(String[] var)
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		String html = var[0];
		show(Files.read("data/scripts/events_off/GiftOfVitality/" + html + ".htm", player), player);
	}

	private class BeginBuff implements Runnable
	{
		L2Character _buffer;
		L2Skill _skill;
		L2Character _target;

		public BeginBuff(L2Character buffer, L2Skill skill, L2Character target)
		{
			_buffer = buffer;
			_skill = skill;
			_target = target;
		}

		@Override
		public void run()
		{
			if(_target.isInOlympiadMode())
				return;
			_buffer.broadcastPacket(new MagicSkillUse(_buffer, _target, _skill.getDisplayId(), _skill.getLevel(), _skill.getHitTime(), 0));
			L2GameThreadPools.getInstance().scheduleGeneral(new EndBuff(_buffer, _skill, _target), _skill.getHitTime());
		}
	}

	private class EndBuff implements Runnable
	{
		L2Character _buffer;
		L2Skill _skill;
		L2Character _target;

		public EndBuff(L2Character buffer, L2Skill skill, L2Character target)
		{
			_buffer = buffer;
			_skill = skill;
			_target = target;
		}

		@Override
		public void run()
		{
			_skill.getEffects(_buffer, _target, false, false);
			_buffer.broadcastPacket(new MagicSkillLaunched(_buffer.getObjectId(), _skill.getId(), _skill.getLevel(), _skill.isOffensive(), _target));
		}
	}

	public String DialogAppend_4306(Integer val)
	{
		if(isActive())
			return Files.read("data/scripts/events_off/GiftOfVitality/4306.htm", (L2Player) getSelf());
		return "";
	}

	@Override
	public void onLoad()
	{
		if(isActive("GiftOfVitality"))
		{
			_active = true;
			spawnEventManagers();
			System.out.println("Loaded Event: Gift of Vitality [state: activated]");
		}
		else
			System.out.println("Loaded Event: Gift of Vitality [state: deactivated]");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
