@echo off
@color 0A
title L2System GameServer
:start
echo Starting L2System Game Server.
echo.
REM set PATH="type here your path to java jdk/jre (including bin folder)"
REM Default parameters for a basic server.

REM ======== Optimize memory settings =======
REM Minimal size with geodata is 1.5G, w/o geo 1G
REM Make sure -Xmn value is always 1/4 the size of -Xms and -Xmx.
REM -Xms<size>	set initial Java heap size
REM -Xmx<size>	set maximum Java heap size
REM -Xmn<size>	Size of young generation
REM ===============================
REM Set heap min/max to same size for consistent results
REM одинаковый размер памяти для Xms и Xmx, JVM пытается удержать размер heap'а минимальным, и если его нужно меньше, чем в Xmx - гоняет GC понапрасну
SET java_opts=%java_opts% -Xms4096m
SET java_opts=%java_opts% -Xmx4096m

REM Non Heap memory
SET java_opts=%java_opts% -XX:PermSize=512m
REM Maximum size of the permanent generation.
SET java_opts=%java_opts% -XX:MaxPermSize=640m

REM Garbage collector/Performance Options
SET java_opts=%java_opts% -XX:+UseConcMarkSweepGC
SET java_opts=%java_opts% -XX:+UseParNewGC
SET java_opts=%java_opts% -XX:+CMSIncrementalMode
SET java_opts=%java_opts% -XX:MaxGCPauseMillis=500
SET java_opts=%java_opts% -XX:+DoEscapeAnalysis
SET java_opts=%java_opts% -XX:+UseBiasedLocking
SET java_opts=%java_opts% -XX:+EliminateLocks
REM SET java_opts=%java_opts% -XX:CMSIncrementalSafetyFactor=50
REM Number of garbage collector threads for the parallel young generation collections and for the parallel parts of the old generation collections
SET java_opts=%java_opts% -XX:ParallelGCThreads=10
SET java_opts=%java_opts% -XX:ParallelCMSThreads=5

SET java_opts=%java_opts% -XX:+AggressiveOpts
REM Default size of new generation
REM SET java_opts=%java_opts% -XX:NewSize=512m
REM SET java_opts=%java_opts% -XX:MaxNewSize=1024m
REM instructs the VM to set a 2:1 ratio between young and tenured generations (Ratio of new/old generation sizes)
REM SET java_opts=%java_opts% -XX:NewRatio=2
REM Sets survivor space ratio to 1:8, resulting in larger survivor spaces (the smaller the ratio, the larger the space). Larger survivor spaces allow short lived objects a longer time period to die in the young generation
REM SET java_opts=%java_opts% -XX:SurvivorRatio=8
REM Allows 80% of the survivor spaces to be occupied instead of the default 50%, allowing better utilization of the survivor space memory.
REM SET java_opts=%java_opts% -XX:TargetSurvivorRatio=50
REM SET java_opts=%java_opts% -XX:MaxTenuringThreshold=5

SET java_opts=%java_opts% -XX:+UseCMSInitiatingOccupancyOnly
SET java_opts=%java_opts% -XX:CMSInitiatingOccupancyFraction=80

REM SET java_opts=%java_opts% -XX:+CMSParallelRemarkEnabled
SET java_opts=%java_opts% -XX:+CMSClassUnloadingEnabled

REM The important setting in 64-bits with the Sun JVM is -XX:+UseCompressedOops as it saves memory and improves performance
SET java_opts=%java_opts% -XX:+UseCompressedOops
SET java_opts=%java_opts% -XX:+UseFastAccessorMethods

REM Logging
REM SET java_opts=%java_opts% -XX:+PrintGCDetails
REM SET java_opts=%java_opts% -XX:+PrintGCDateStamps
REM SET java_opts=%java_opts% -XX:+PrintGCApplicationStoppedTime
REM SET java_opts=%java_opts% -XX:+PrintGCTimeStamps
REM SET java_opts=%java_opts% -XX:+PrintGC
REM SET java_opts=%java_opts% -Xloggc:./log/game/garbage_collector.log

SET java_settings=%java_settings% -Dfile.encoding=UTF-8
SET java_settings=%java_settings% -Djava.net.preferIPv4Stack=true

java -server %java_settings% %java_opts% -cp ./lib/*;L2System.jar l2n.game.GameServer

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo L2System Game: Administrator Restarting.
echo.
goto start
:error
echo.
echo L2System Game: Server terminated abnormally.
echo.
:end
echo.
echo L2System Game: Server terminated.
echo.
pause
