@echo off
@color 0A
cls
title L2System  Register Game Server

SET java_settings=%java_settings% -Dfile.encoding=UTF-8
SET java_settings=%java_settings% -Djava.util.logging.config.file=config/console.ini

java %java_settings% -cp ./lib/*;L2System.jar l2n.gsregistering.GameServerRegister
pause
