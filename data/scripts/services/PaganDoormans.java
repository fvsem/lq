package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.tables.DoorTable;
import l2n.util.Files;

public class PaganDoormans extends Functions implements ScriptFile
{
	private static int MainDoorId = 19160001;
	private static int SecondDoor1Id = 19160011;
	private static int SecondDoor2Id = 19160010;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Pagan Doormans");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void openMainDoor()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		if(getItemCount(player, 8064) == 0 && getItemCount(player, 8067) == 0)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
			return;
		}

		if(getItemCount(player, 8064) != 0)
		{
			removeItem(player, 8064, 1);
			addItem(player, 8065, 1);
		}

		openDoor(MainDoorId);
		show(Files.read("data/html/default/32034-1.htm", player), player);
	}

	public void openSecondDoor()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		if(getItemCount(player, 8067) == 0)
		{
			show(Files.read("data/html/default/32036-2.htm", player), player);
			return;
		}

		openDoor(SecondDoor1Id);
		openDoor(SecondDoor2Id);
		show(Files.read("data/html/default/32036-1.htm", player), player);
	}

	public void pressSkull()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		openDoor(MainDoorId);
		show(Files.read("data/html/default/32035-1.htm", player), player);
	}

	public void press2ndSkull()
	{
		L2Player player = (L2Player) getSelf();

		if(player == null || player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		openDoor(SecondDoor1Id);
		openDoor(SecondDoor2Id);
		show(Files.read("data/html/default/32037-1.htm", player), player);
	}

	private static void openDoor(int doorId)
	{
		final int CLOSE_TIME = 10000;
		L2DoorInstance door = DoorTable.getInstance().getDoor(doorId);
		if(!door.isOpen())
		{
			door.openMe();
			door.scheduleCloseMe(CLOSE_TIME);
		}
	}
}
