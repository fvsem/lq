package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class NoKarma extends Functions implements ScriptFile
{
	public void show()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!checkCondition(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		show(Files.read("data/scripts/services/NoKarma.htm", player), player);
	}

	public void delete_pk_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!checkCondition(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		L2Item price = ItemTable.getInstance().getTemplate(Config.SERVICES_PK_ITEM_ID);

		String content = Files.read("data/scripts/services/NoKarmaPK.htm", player);
		content = content.replace("%price%", Config.SERVICES_PK_ITEM_COUNT + " " + price.getName());
		show(content, player);
	}

	public void delete_karma_page()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!checkCondition(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		L2Item price = ItemTable.getInstance().getTemplate(Config.SERVICES_KARMA_ITEM_ID);

		String content = Files.read("data/scripts/services/NoKarmaKarma.htm", player);
		content = content.replace("%price%", Config.SERVICES_KARMA_ITEM_COUNT + " " + price.getName());
		show(content, player);
	}

	public void delete_pk()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!PremiumAccess.checkAccessService(player, ServiceType.NOKARMA_PK))
			return;

		if(!checkCondition(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		if(player.getInventory().getCountOf(Config.SERVICES_PK_ITEM_ID) < Config.SERVICES_PK_ITEM_COUNT)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		if(player.getPkKills() >= 5)
		{
			removeItem(player, Config.SERVICES_PK_ITEM_ID, Config.SERVICES_PK_ITEM_COUNT);
			Log.add(player.getName() + "|" + player.getObjectId() + "|delete_pk|" + Config.SERVICES_PK_ITEM_ID + "-" + Config.SERVICES_PK_ITEM_COUNT + "|", "services");
			player.setPkKills(player.getPkKills() - 5);
			player.sendChanges();
			delete_pk_page();
		}
		else
		{
			player.sendMessage(new CustomMessage("common.notAvailable", player));
			show();
		}
	}

	public void delete_karma()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!PremiumAccess.checkAccessService(player, ServiceType.NOKARMA_KARMA))
			return;

		if(!checkCondition(player))
		{
			show(new CustomMessage("common.notAvailable", player), player);
			return;
		}

		if(player.getInventory().getCountOf(Config.SERVICES_KARMA_ITEM_ID) < Config.SERVICES_KARMA_ITEM_COUNT)
		{
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		if(player.getKarma() > 0)
		{
			removeItem(player, Config.SERVICES_KARMA_ITEM_ID, Config.SERVICES_KARMA_ITEM_COUNT);
			Log.add(player.getName() + "|" + player.getObjectId() + "|delete_karma|" + Config.SERVICES_KARMA_ITEM_ID + "-" + Config.SERVICES_KARMA_ITEM_COUNT + "|", "services");
			player.setKarma(player.getKarma() - 1000);
			delete_karma_page();
		}
		else
		{
			player.sendMessage(new CustomMessage("common.notAvailable", player));
			show();
		}
	}

	private boolean checkCondition(L2Player player)
	{
		if(!Config.SERVICES_KARMA_ENABLE)
			return false;
		if(player.getPvpFlag() != 0)
			return false;
		if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isFlying() || player.isCombatFlagEquipped() || player.isInZone(ZoneType.Siege))
			return false;
		if(player.isInOlympiadMode() || player.getOlympiadGameId() != -1 || Olympiad.isRegisteredInComp(player) || player.getTeam() != 0)
			return false;
		return true;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: NoKarma");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
