package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SystemMessage;

public class ManaRegen extends Functions implements ScriptFile
{
	private static final int ADENA = 57;
	private static final long PRICE = 5; // 5 аден за 1 МП

	public void DoManaRegen()
	{
		L2Player player = (L2Player) getSelf();
		long mp = (long) Math.floor(player.getMaxMp() - player.getCurrentMp());
		long fullCost = mp * PRICE;
		if(fullCost <= 0)
		{
			player.sendPacket(Msg.NOTHING_HAPPENED);
			return;
		}
		if(getItemCount(player, ADENA) >= fullCost)
		{
			removeItem(player, ADENA, fullCost);
			player.sendPacket(new SystemMessage(SystemMessage.S1_MPS_HAVE_BEEN_RESTORED).addNumber(mp));
			player.setCurrentMp(player.getMaxMp());
		}
		else
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Mana Regen");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
