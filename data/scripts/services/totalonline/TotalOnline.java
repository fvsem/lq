package services.totalonline;

import l2n.database.FiltredPreparedStatement;
import l2n.database.L2DatabaseFactory;
import l2n.database.ThreadConnection;
import l2n.database.utils.DbUtils;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.tables.FakePlayersTable;

public class TotalOnline extends Functions implements ScriptFile {
	public void onLoad() {
		L2GameThreadPools.getInstance().scheduleGeneralAtFixedRate(new UpdateOnline(), 60000, 60000);
	}

	private class UpdateOnline implements Runnable {
		public void run() {
			int members = getOnlineMembers();
			int offMembers = getOfflineMembers();
			ThreadConnection con = null;
			FiltredPreparedStatement statement = null;
			try {
				con = L2DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("update online set totalOnline =?, totalOffline = ? where 'id' = '0'");
				statement.setInt(1, members);
				statement.setInt(2, offMembers);
				statement.execute();
				DbUtils.closeQuietly(statement);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				DbUtils.closeQuietly(con, statement);
			}
		}

		//for future possibility of parsing names of players method is taking also name to array for init
		private int getOnlineMembers() {
			int i = 0;
			for (L2Player player : L2ObjectsStorage.getAllPlayersForIterate()) {
				i++;
			}
			i = i + FakePlayersTable.getFakePlayersCount();
			return i;
		}

		private int getOfflineMembers() {
			int i = 0;
			for (L2Player player : L2ObjectsStorage.getAllPlayersForIterate()) {
				if (player.isInOfflineMode()) {
					i++;
				}
			}
			return i;
		}
	}


	public void onReload() {
		//Не поддерживается
	}

	public void onShutdown() {
		//Не поддерживается
	}
}