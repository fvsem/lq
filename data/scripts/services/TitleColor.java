package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public final class TitleColor extends Functions implements ScriptFile
{
	public void list()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_TITLE_COLOR_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		final StringBuilder append = new StringBuilder();
		if(player.hasPremiumAccount() && Config.SERVICES_CHANGE_TITLE_COLOR_FREE_PA)
		{
			append.append("<br>Possible colors:<br>");
			for(final String color : Config.SERVICES_CHANGE_TITLE_COLOR_LIST)
				append.append("<br><a action=\"bypass -h scripts_services.TitleColor:changeFree ").append(color).append("\"><font color=\"").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("\">").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("</font></a>");
		}
		else
		{
			append.append("Вы можете изменить цвет титула за небольшую цену ").append(Config.SERVICES_CHANGE_TITLE_COLOR_PRICE).append(" ").append(ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_TITLE_COLOR_ITEM).getName()).append(".");
			append.append("<br>Возможные цвета:<br>");
			for(final String color : Config.SERVICES_CHANGE_TITLE_COLOR_LIST)
				append.append("<br><a action=\"bypass -h scripts_services.TitleColor:change ").append(color).append("\"><font color=\"").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("\">").append(color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)).append("</font></a>");
		}

		append.append("<br><a action=\"bypass -h scripts_services.TitleColor:change FFFF77\"><font color=\"77FFFF\">Восстановить значения по умолчанию (бесплатно)</font></a>");
		show(append.toString(), player);
	}

	public void change(final String[] param)
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_TITLE_COLOR_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		if(param[0].equalsIgnoreCase("FFFF77"))
		{
			player.setTitleColor(0xFFFF77);
			player.broadcastUserInfo(true);
			return;
		}

		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_TITLE_COLOR_ITEM);
		final L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_CHANGE_TITLE_COLOR_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_CHANGE_TITLE_COLOR_PRICE, true);
			player.setTitleColor(Integer.decode("0x" + param[0]));
			player.broadcastUserInfo(true);
		}
		else if(Config.SERVICES_CHANGE_TITLE_COLOR_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void changeFree(final String[] param)
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_CHANGE_TITLE_COLOR_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		// проверка прав доступа для игроков без ПА
		if(!PremiumAccess.checkAccessService(player, ServiceType.NICK_COLOR_CHANGE))
			return;
			
		if(param[0].equalsIgnoreCase("FFFF77"))
		{
			player.setTitleColor(0xFFFF77);
			player.broadcastUserInfo(true);
			return;
		}

		player.setTitleColor(Integer.decode("0x" + param[0]));
		player.broadcastUserInfo(true);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Title color");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
