package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.util.Files;

public class fenrir extends Functions implements ScriptFile
{

	private static final int BLACK_WOLF = 16025;
	private static final int count = 1;
	private static final int GREAT_WOLF_NECKLACE = 9882;
	private static final int FENRIR_NECKLACE = 10426;
	private static final int dist = 50;

	public void evolve()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(GREAT_WOLF_NECKLACE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != BLACK_WOLF)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 70)
		{
			show(Files.read("data/scripts/services/petevolve/no_level_gw.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, GREAT_WOLF_NECKLACE, count);
		addItem(player, FENRIR_NECKLACE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Evolve Fenrir");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
