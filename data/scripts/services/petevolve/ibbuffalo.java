package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.util.Files;

public class ibbuffalo extends Functions implements ScriptFile
{
	private static final int BABY_BUFFALO = 12780;
	private static final int count = 1;
	private static final int BABY_BUFFALO_PANPIPE = 6648;
	private static final int IN_BABY_BUFFALO_NECKLACE = 10311;
	private static final int dist = 50;

	public void evolve()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(BABY_BUFFALO_PANPIPE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != BABY_BUFFALO)
		{
			show(Files.read("data/scripts/services/petevolve/no_pet.htm", player), player);
			return;
		}
		// else if(player.isMageClass())
		// {
		// show(Files.read("data/scripts/services/petevolve/no_class_w.htm", player), player);
		// return;
		// }
		else if(pl_pet.getLevel() < 55)
		{
			show(Files.read("data/scripts/services/petevolve/no_level.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, BABY_BUFFALO_PANPIPE, count);
		addItem(player, IN_BABY_BUFFALO_NECKLACE, count);
		show(Files.read("data/scripts/services/petevolve/yes_pet.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Evolve Improved Baby Buffalo");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
