package services.petevolve;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2PetInstance;
import l2n.game.network.serverpackets.ItemList;
import l2n.game.tables.ItemTable;
import l2n.game.tables.PetDataTable;
import l2n.game.templates.L2Item;
import l2n.util.Files;
import l2n.util.Util;

public class exchange extends Functions implements ScriptFile
{
	private static final int PEticketB = 7583;
	private static final int PEticketC = 7584;
	private static final int PEticketK = 7585;

	private static final int BbuffaloP = 6648;
	private static final int BcougarC = 6649;
	private static final int BkookaburraO = 6650;

	public void exch_1()
	{
		final L2Player player = (L2Player) getSelf();

		if(getItemCount(player, PEticketB) >= 1)
		{
			removeItem(player, PEticketB, 1);
			addItem(player, BbuffaloP, 1);
			return;
		}

		show(Files.read("data/scripts/services/petevolve/exchange_no.htm", player), player);
	}

	public void exch_2()
	{
		final L2Player player = (L2Player) getSelf();

		if(getItemCount(player, PEticketC) >= 1)
		{
			removeItem(player, PEticketC, 1);
			addItem(player, BcougarC, 1);
			return;
		}

		show(Files.read("data/scripts/services/petevolve/exchange_no.htm", player), player);
	}

	public void exch_3()
	{
		final L2Player player = (L2Player) getSelf();

		if(getItemCount(player, PEticketK) >= 1)
		{
			removeItem(player, PEticketK, 1);
			addItem(player, BkookaburraO, 1);
			return;
		}

		show(Files.read("data/scripts/services/petevolve/exchange_no.htm", player), player);
	}

	public void showBabyPetExchange()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_EXCHANGE_BABY_PET_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXCHANGE_BABY_PET_ITEM);
		String out = "";
		out += "<html><body>Вы можете в любое время обменять вашего Improved Baby пета на другой вид, без потери опыта. Пет при этом должен быть вызван.";
		out += "<br>Стоимость обмена: " + Util.formatAdena(Config.SERVICES_EXCHANGE_BABY_PET_PRICE) + " " + item.getName();
		out += "<br><button width=250 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h scripts_services.petevolve.exchange:exToCougar\" value=\"Обменять на Improved Cougar\">";
		out += "<br1><button width=250 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h scripts_services.petevolve.exchange:exToBuffalo\" value=\"Обменять на Improved Buffalo\">";
		out += "<br1><button width=250 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h scripts_services.petevolve.exchange:exToKookaburra\" value=\"Обменять на Improved Kookaburra\">";
		out += "</body></html>";
		show(out, player);
	}

	public void showErasePetName()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_CHANGE_PET_NAME_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_PET_NAME_ITEM);
		String out = "";
		out += "<html><body>Вы можете обнулить имя у пета, для того чтобы назначить новое. Пет при этом должен быть вызван.";
		out += "<br>Стоимость обнуления: " + Util.formatAdena(Config.SERVICES_CHANGE_PET_NAME_PRICE) + " " + item.getName();
		out += "<br><button width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h scripts_services.petevolve.exchange:erasePetName\" value=\"Обнулить имя\">";
		out += "</body></html>";
		show(out, player);
	}

	public void erasePetName()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_CHANGE_PET_NAME_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Summon pl_pet = player.getPet();
		if(pl_pet == null || !pl_pet.isPet())
		{
			show("Питомец должен быть вызван.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_CHANGE_PET_NAME_ITEM);
		final L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_CHANGE_PET_NAME_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_CHANGE_PET_NAME_PRICE, true);
			pl_pet.setName(pl_pet.getTemplate().name);
			pl_pet.broadcastCharInfo();

			final L2PetInstance _pet = (L2PetInstance) pl_pet;
			final L2ItemInstance controlItem = _pet.getControlItem();
			if(controlItem != null)
			{
				controlItem.setCustomType2(1);
				controlItem.setPriceToSell(0);
				controlItem.updateDatabase();
				_pet.updateControlItem();
			}
			show("Имя стерто.", player);
			pl_pet.unSummon();
		}
		else if(Config.SERVICES_CHANGE_PET_NAME_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void exToCougar()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_EXCHANGE_BABY_PET_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Summon pl_pet = player.getPet();
		if(pl_pet == null || pl_pet.isDead() || !(pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_BUFFALO_ID || pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_KOOKABURRA_ID))
		{
			show("Пет должен быть вызван.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXCHANGE_BABY_PET_ITEM);
		final L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_EXCHANGE_BABY_PET_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_EXCHANGE_BABY_PET_PRICE, true);
			final L2ItemInstance control = player.getInventory().getItemByObjectId(player.getPet().getControlItemId());
			control.setItemId(PetDataTable.getControlItemId(PetDataTable.IMPROVED_BABY_COUGAR_ID));
			control.updateDatabase(true);
			player.sendPacket(new ItemList(player, false));
			player.getPet().unSummon();
			show("Пет изменен.", player);
		}
		else if(Config.SERVICES_EXCHANGE_BABY_PET_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void exToBuffalo()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_EXCHANGE_BABY_PET_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Summon pl_pet = player.getPet();
		if(pl_pet == null || pl_pet.isDead() || !(pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_COUGAR_ID || pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_KOOKABURRA_ID))
		{
			show("Пет должен быть вызван.", player);
			return;
		}
		if(Config.ALT_IMPROVED_PETS_LIMITED_USE && player.isMageClass())
		{
			show("Этот пет только для воинов.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXCHANGE_BABY_PET_ITEM);
		final L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_EXCHANGE_BABY_PET_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_EXCHANGE_BABY_PET_PRICE, true);
			final L2ItemInstance control = player.getInventory().getItemByObjectId(player.getPet().getControlItemId());
			control.setItemId(PetDataTable.getControlItemId(PetDataTable.IMPROVED_BABY_BUFFALO_ID));
			control.updateDatabase(true);
			player.sendPacket(new ItemList(player, false));
			player.getPet().unSummon();
			show("Пет изменен.", player);
		}
		else if(Config.SERVICES_EXCHANGE_BABY_PET_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public void exToKookaburra()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;
		if(!Config.SERVICES_EXCHANGE_BABY_PET_ENABLED)
		{
			show("Сервис отключен.", player);
			return;
		}
		final L2Summon pl_pet = player.getPet();
		if(pl_pet == null || pl_pet.isDead() || !(pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_BUFFALO_ID || pl_pet.getNpcId() == PetDataTable.IMPROVED_BABY_COUGAR_ID))
		{
			show("Пет должен быть вызван.", player);
			return;
		}
		if(Config.ALT_IMPROVED_PETS_LIMITED_USE && !player.isMageClass())
		{
			show("Этот пет только для магов.", player);
			return;
		}
		final L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXCHANGE_BABY_PET_ITEM);
		final L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_EXCHANGE_BABY_PET_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_EXCHANGE_BABY_PET_PRICE, true);
			final L2ItemInstance control = player.getInventory().getItemByObjectId(player.getPet().getControlItemId());
			control.setItemId(PetDataTable.getControlItemId(PetDataTable.IMPROVED_BABY_KOOKABURRA_ID));
			control.updateDatabase(true);
			player.sendPacket(new ItemList(player, false));
			player.getPet().unSummon();
			show("Пет изменен.", player);
		}
		else if(Config.SERVICES_EXCHANGE_BABY_PET_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	public static String DialogAppend_30731(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30827(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30828(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30829(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30830(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30831(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_30869(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_31067(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_31265(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_31309(final Integer val)
	{
		return getHtmlAppends(val);
	}

	public static String DialogAppend_31954(final Integer val)
	{
		return getHtmlAppends(val);
	}

	private static String getHtmlAppends(final Integer val)
	{
		String ret = "";
		if(val != 0)
			return ret;
		if(Config.SERVICES_CHANGE_PET_NAME_ENABLED)
			ret = "<br>[scripts_services.petevolve.exchange:showErasePetName|Обнулить имя у пета]";
		if(Config.SERVICES_EXCHANGE_BABY_PET_ENABLED)
			ret += "<br>[scripts_services.petevolve.exchange:showBabyPetExchange|Обменять Improved Baby пета]";
		return ret;
	}

	@Override
	public void onLoad()
	{}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
