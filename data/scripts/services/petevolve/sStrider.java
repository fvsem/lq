package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.util.Files;

public class sStrider extends Functions implements ScriptFile
{
	private static final int STAR = 12527;
	private static final int count = 1;
	private static final int STAR_BUGLE = 4423;
	private static final int RED_STAR_BUGLE = 10309;
	private static final int dist = 50;

	public void evolve()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(STAR_BUGLE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != STAR)
		{
			show(Files.read("data/scripts/services/petevolve/no_sStrider.htm", player), player);
			return;
		}
		else if(pl_pet.getLevel() < 70)
		{
			show(Files.read("data/scripts/services/petevolve/no_level_strider.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, STAR_BUGLE, count);
		addItem(player, RED_STAR_BUGLE, count);
		show(Files.read("data/scripts/services/petevolve/yes_Strider.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Evolve STAR Strider");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
