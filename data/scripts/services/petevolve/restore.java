package services.petevolve;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.actor.L2Summon;
import l2n.util.Files;

public class restore extends Functions implements ScriptFile
{
	// GREAT_WOLF_NECKLACE
	private static final int GREAT_SNOW_WOLF = 16037;
	private static final int GREAT_SNOW_WOLF_NECKLACE = 10307;
	private static final int GREAT_WOLF_NECKLACE = 9882;

	// FENRIR_NECKLACE
	private static final int SNOW_FENRIR = 16042;
	private static final int SNOW_FENRIR_NECKLACE = 10611;
	private static final int FENRIR_NECKLACE = 10426;

	// WIND_BUGLE
	private static final int RED_WIND = 16038;
	private static final int RED_WIND_BUGLE = 10308;
	private static final int WIND_BUGLE = 4422;

	// STAR_BUGLE
	private static final int RED_STAR = 16039;
	private static final int RED_STAR_BUGLE = 10309;
	private static final int STAR_BUGLE = 4423;

	// TWILIGTHT_BUGLE
	private static final int RED_TWILIGTHT = 16040;
	private static final int RED_TWILIGTHT_BUGLE = 10310;
	private static final int TWILIGTHT_BUGLE = 4424;

	private static final int count = 1;
	private static final int dist = 50;

	public void rest_1()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(GREAT_SNOW_WOLF_NECKLACE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != GREAT_SNOW_WOLF)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, GREAT_SNOW_WOLF_NECKLACE, count);
		addItem(player, GREAT_WOLF_NECKLACE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	public void rest_2()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(SNOW_FENRIR_NECKLACE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != SNOW_FENRIR)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, SNOW_FENRIR_NECKLACE, count);
		addItem(player, FENRIR_NECKLACE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	public void rest_3()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(RED_WIND_BUGLE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != RED_WIND)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, RED_WIND_BUGLE, count);
		addItem(player, WIND_BUGLE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	public void rest_4()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(RED_STAR_BUGLE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != RED_STAR)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, RED_STAR_BUGLE, count);
		addItem(player, STAR_BUGLE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	public void rest_5()
	{
		L2Player player = (L2Player) getSelf();
		L2Summon pl_pet = player.getPet();

		if(player.getInventory().findItemByItemId(RED_TWILIGTHT_BUGLE) == null)
		{
			show(Files.read("data/scripts/services/petevolve/no_item.htm", player), player);
			return;
		}
		else if(player.getPet() == null)
		{
			show(Files.read("data/scripts/services/petevolve/evolve_no.htm", player), player);
			return;
		}
		else if(pl_pet.getNpcId() != RED_TWILIGTHT)
		{
			show(Files.read("data/scripts/services/petevolve/no_wolf.htm", player), player);
			return;
		}
		else if(player.getPet().getX() - player.getX() > dist || player.getPet().getY() - player.getY() > dist || player.getPet().getZ() - player.getZ() > dist)
		{
			show(Files.read("data/scripts/services/petevolve/no_dist.htm", player), player);
			return;
		}
		else
			player.getPet().unSummon();
		removeItem(player, RED_TWILIGTHT_BUGLE, count);
		addItem(player, TWILIGTHT_BUGLE, count);
		show(Files.read("data/scripts/services/petevolve/yes_wolf.htm", player), player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Restore Evolve");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
