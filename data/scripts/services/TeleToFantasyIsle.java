package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;
import l2n.util.Rnd;

public class TeleToFantasyIsle extends Functions implements ScriptFile
{
	private static final Location[] POINTS = {
			new Location(-60695, -56896, -2032),
			new Location(-59716, -55920, -2032),
			new Location(-58752, -56896, -2032),
			new Location(-59716, -57864, -2032) };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Teleport to Fantasy Isle");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void toFantasyIsle()
	{
		L2Player player = (L2Player) getSelf();

		if(!L2NpcInstance.canBypassCheck(player, player.getLastNpc()))
			return;

		player.setVar("backCoords", player.getLoc().toXYZString());
		player.teleToLocation(POINTS[Rnd.get(POINTS.length)]);
	}

	public void fromFantasyIsle()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(!L2NpcInstance.canBypassCheck(player, npc))
			return;

		String var = player.getVar("backCoords");
		if(var == null || var.equals(""))
		{
			teleOut();
			return;
		}
		player.teleToLocation(new Location(var));
	}

	public void teleOut()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		player.teleToLocation(-44316, -113136, -80);
		if(player.getVar("lang@").equalsIgnoreCase("en"))
			show("I don't know from where you came here, but I can teleport you the nearest town.", player);
		else
			show("Я не знаю, как Вы попали сюда, но я могу Вас отправить в ближайший город.", player);
	}
}
