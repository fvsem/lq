package services.RentPet;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.SiegeManager;
import l2n.game.model.actor.L2Player;
import l2n.game.network.serverpackets.SetupGauge;

public class RideHire extends Functions implements ScriptFile
{
	public String DialogAppend_30827(final Integer val)
	{
		if(Config.SERVECES_RIDEHIRE && val == 0)
		{
			final L2Player player = (L2Player) getSelf();
			String lang = player.getVar("lang@");
			if(lang == null)
				lang = "en";
			if(lang.equalsIgnoreCase("en"))
				return "<br>[scripts_services.RentPet.RideHire:ride_prices|Ride hire mountable pet.]";
			return "<br>[scripts_services.RentPet.RideHire:ride_prices|Взять на прокат ездовое животное.]";
		}
		return "";
	}

	public void ride_prices()
	{
		if(Config.SERVECES_RIDEHIRE)
			show("data/scripts/services/RentPet/ride-prices.htm", (L2Player) getSelf());
		else
			show("Сервис проката животных отключен", (L2Player) getSelf());
	}

	public void ride(final String[] args)
	{
		final L2Player player = (L2Player) getSelf();
		String lang = player.getVar("lang@");
		if(lang == null)
			lang = "en";
		if(!Config.SERVECES_RIDEHIRE)
		{
			show("Сервис проката животных отключен", player);
			return;
		}
		if(args.length != 3)
		{
			if(lang.equalsIgnoreCase("en"))
				show("Incorrect input", player);
			else
				show("Некорректные данные", player);
			return;
		}

		if(player.isActionsDisabled() || player.getLastNpc().getDistance(player) > 250)
			return;

		if(!SiegeManager.getCanRide())
		{
			if(lang.equalsIgnoreCase("en"))
				show("Can't ride while Siege in progress.", player);
			else
				show("Прокат не работает во время осады.", player);
			return;
		}

		if(player.getTransformationId() != 0)
		{
			if(lang.equalsIgnoreCase("en"))
				show("Can't ride while in transformation mode.", player);
			else
				show("Вы не можете взять пета в прокат, пока находитесь в режиме трансформации.", player);
			return;
		}

		if(player.getPet() != null || player.isMounted())
		{
			player.sendPacket(Msg.YOU_ALREADY_HAVE_A_PET);
			return;
		}

		final Integer npc_id = Integer.parseInt(args[0]);
		final Integer time = Integer.parseInt(args[1]);
		final Integer price = Integer.parseInt(args[2]);

		if(npc_id != 12621 && npc_id != 12526 && npc_id != 16030)
		{
			if(lang.equalsIgnoreCase("en"))
				show("Unknown pet.", player);
			else
				show("У меня нет таких питомцев!", player);
			return;
		}

		if(time > 1800)
		{
			if(lang.equalsIgnoreCase("en"))
				show("Too long time to ride.", player);
			else
				show("Слишком большое время.", player);
			return;
		}

		if(player.getAdena() < price)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		player.reduceAdena(price, true);

		doLimitedRide(player, npc_id, time);
	}

	public static void doLimitedRide(final L2Player player, final Integer npc_id, final Integer time)
	{
		if(!ride(player, npc_id))
			return;
		player.sendPacket(new SetupGauge(SetupGauge.GREEN, time * 1000));
		executeTask(player, "services.RentPet.RideHire", "rideOver", new Object[0], time * 1000);
	}

	public void rideOver()
	{
		final L2Player player = (L2Player) getSelf();
		unRide(player);
		final String lang = player.getVar("lang@");
		if(lang.equalsIgnoreCase("en"))
			show("Ride time is over.<br><br>Welcome back again!", player);
		else
			show("Время проката закончилось. Приходите еще!", player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Ride Hire");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
