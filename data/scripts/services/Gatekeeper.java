package services;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

public class Gatekeeper extends Functions implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Gatekeeper");
	}

	public void tele30006()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30006-1.htm";
		else
			htmltext = "30006-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30899()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30899-1.htm";
		else
			htmltext = "30899-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30878()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30878-1.htm";
		else
			htmltext = "30878-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30146()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30146-1.htm";
		else
			htmltext = "30146-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30134()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30134-1.htm";
		else
			htmltext = "30134-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30080()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30080-1.htm";
		else
			htmltext = "30080-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30576()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30576-1.htm";
		else
			htmltext = "30576-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30540()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30540-1.htm";
		else
			htmltext = "30540-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele32163()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "32163-1.htm";
		else
			htmltext = "32163-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void stronghold32163()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= 21)
			htmltext = "32163-4.htm";
		else
			htmltext = "32163-5.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30256()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30256-1.htm";
		else
			htmltext = "30256-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30320()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30320-1.htm";
		else
			htmltext = "30320-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30059()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30059-1.htm";
		else
			htmltext = "30059-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30177()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30177-1.htm";
		else
			htmltext = "30177-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30848()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30848-1.htm";
		else
			htmltext = "30848-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31275()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31275-1.htm";
		else
			htmltext = "31275-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31698()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31698-1.htm";
		else
			htmltext = "31698-1-1.htm";
		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31699()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31699-1.htm";
		else
			htmltext = "31699-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31212()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31212-1.htm";
		else
			htmltext = "31212-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31213()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31213-1.htm";
		else
			htmltext = "31213-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31214()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31214-1.htm";
		else
			htmltext = "31214-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31215()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31215-1.htm";
		else
			htmltext = "31215-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31216()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31216-1.htm";
		else
			htmltext = "31216-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele32353()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "32353-1.htm";
		else
			htmltext = "32353-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31218()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31218-1.htm";
		else
			htmltext = "31218-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31219()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31219-1.htm";
		else
			htmltext = "31219-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31220()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31220-1.htm";
		else
			htmltext = "31220-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31221()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31221-1.htm";
		else
			htmltext = "31221-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31222()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31222-1.htm";
		else
			htmltext = "31222-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31223()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31223-1.htm";
		else
			htmltext = "31223-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31224()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31224-1.htm";
		else
			htmltext = "31224-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele30233()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "30233-1.htm";
		else
			htmltext = "30233-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31767()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31767-1.htm";
		else
			htmltext = "31767-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31768()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31768-1.htm";
		else
			htmltext = "31768-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31320()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31320-1.htm";
		else
			htmltext = "31320-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele32048()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "32048-1.htm";
		else
			htmltext = "32048-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31964()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31964-1.htm";
		else
			htmltext = "31964-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	public void tele31217()
	{
		L2Player player = (L2Player) getSelf();

		String htmltext;
		int Level = player.getLevel();

		if(Level >= Config.GATEKEEPER_FREE)
			htmltext = "31217-1.htm";
		else
			htmltext = "31217-1-1.htm";

		show(Files.read("data/html/teleporter/" + htmltext, player), player);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
