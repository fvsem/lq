package services.HeroSell;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.CharVariables;
import l2n.game.model.entity.Hero;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.network.serverpackets.SocialAction;
import l2n.game.tables.ItemTable;
import l2n.util.Files;
import l2n.util.Log;
import l2n.util.StringUtil;
import l2n.util.Util;

public class HeroSell extends Functions implements ScriptFile
{
	public void list()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_HERO_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return;
		}

		if(!player.isNoble())
		{
			if(player.isLangRus())
				player.sendMessage("Только Ноблесс может стать героем");
			else
				player.sendMessage("Only Noble can be a Hero");
			return;
		}

		String html = null;
		CharVariables var = player.getVariable("hero_time");
		// если герой без переменной
		if(player.isHero() && Hero.getInstance().isHero(player.getObjectId()))
			html = Files.read("data/scripts/services/HeroSell/HeroSellInfinite.htm", player);
		else if(var == null)
		{
			html = Files.read("data/scripts/services/HeroSell/HeroSell.htm", player);
			StringBuilder add = StringUtil.startAppend(300);
			for(int i = 0; i < Config.SERVICES_HERO_SELL_DAYS.length; i++)
				StringUtil.append(add, "<a action=\"bypass -h scripts_services.HeroSell.HeroSell:get ", i, "\"> Купить Геройство ", Config.SERVICES_HERO_SELL_DAYS[i], " Дней - ", Config.SERVICES_HERO_SELL_PRICE[i], " ", ItemTable.getInstance().getTemplate(Config.SERVICES_HERO_SELL_ITEM[i]).getName(), "</a><br>");

			html = html.replaceFirst("%toreplace%", add.toString());
		}
		else if(!var.isExpired())
			html = Files.read("data/scripts/services/HeroSell/HeroSellAlready.htm", player).replaceFirst("%endtime%", Util.datetimeFormatter.format(var.getExpireTime() * 1000));
		else
			html = Files.read("data/scripts/services/HeroSell/HeroSellNo.htm", player);

		show(html, player);
	}

	public void get(String[] param)
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_HERO_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled", player));
			return;
		}

		int i = Integer.parseInt(param[0]);

		L2ItemInstance pay = player.getInventory().getItemByItemId(Config.SERVICES_HERO_SELL_ITEM[i]);
		if(pay != null && pay.getCount() >= Config.SERVICES_HERO_SELL_PRICE[i])
		{
			removeItem(player, Config.SERVICES_HERO_SELL_ITEM[i], Config.SERVICES_HERO_SELL_PRICE[i]);
			Log.add(player.getName() + "|" + player.getObjectId() + "|buy hero|" + Config.SERVICES_HERO_SELL_DAYS[i] + " days|", "services");
			player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));

			player.setVar("hero_time", "1", Config.SERVICES_HERO_SELL_DAYS[i] * 24 * 60);
			player.applyTimeVariables(false);
			player.broadcastUserInfo(true);

			show(Files.read("data/scripts/services/HeroSell/HeroSellGet.htm", player), player);
		}
		else if(Config.SERVICES_HERO_SELL_ITEM[i] == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Hero Sell");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
