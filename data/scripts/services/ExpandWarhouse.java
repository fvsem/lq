package services;

import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.tables.ItemTable;
import l2n.game.templates.L2Item;
import l2n.util.PremiumAccess;
import l2n.util.PremiumAccess.ServiceType;

public class ExpandWarhouse extends Functions implements ScriptFile
{
	public void get()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_EXPAND_WAREHOUSE_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		if(!PremiumAccess.checkAccessService(player, ServiceType.EXPAND_WAREHOUSE))
			return;

		L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXPAND_WAREHOUSE_ITEM);
		L2ItemInstance pay = player.getInventory().getItemByItemId(item.getItemId());
		if(pay != null && pay.getCount() >= Config.SERVICES_EXPAND_WAREHOUSE_PRICE)
		{
			player.getInventory().destroyItem(pay, Config.SERVICES_EXPAND_WAREHOUSE_PRICE, true);
			player.setExpandWarehouse(player.getExpandWarehouse() + 10);
			player.setVar("ExpandWarehouse", String.valueOf(player.getExpandWarehouse()));
			player.sendMessage("Warehouse capacity is now " + player.getWarehouseLimit());
		}
		else if(Config.SERVICES_EXPAND_WAREHOUSE_ITEM == 57)
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
		else
			player.sendPacket(Msg.INCORRECT_ITEM_COUNT);

		show();
	}

	public void show()
	{
		L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(!Config.SERVICES_EXPAND_WAREHOUSE_ENABLED)
		{
			show(new CustomMessage("scripts.services.TurnOff", player), player);
			return;
		}

		L2Item item = ItemTable.getInstance().getTemplate(Config.SERVICES_EXPAND_WAREHOUSE_ITEM);

		String out = "";

		out += "<html><body>Расширение склада";
		out += "<br><br><table>";
		out += "<tr><td>Текущий размер:</td><td>" + player.getWarehouseLimit() + "</td></tr>";
		out += "<tr><td>Стоимость расширения:</td><td>" + Config.SERVICES_EXPAND_WAREHOUSE_PRICE + " " + item.getName() + "</td></tr>";
		out += "</table><br><br>";
		out += "<button width=100 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\" action=\"bypass -h scripts_services.ExpandWarhouse:get\" value=\"Расширить\">";
		out += "</body></html>";

		show(out, player);
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Expand Warehouse");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
