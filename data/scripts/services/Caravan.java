package services;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.DoorTable;
import l2n.util.Location;
import l2n.util.Util;
import quests._132_MatrasCuriosity._132_MatrasCuriosity;

public class Caravan extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Service: Caravan");

		DoorTable.getInstance().getDoor(19250001).openMe();
		DoorTable.getInstance().getDoor(19250002).openMe();

		final long confidence = getConfidence();
		if(confidence > 300000)
			DoorTable.getInstance().getDoor(20250002).openMe();
		if(confidence > 600000)
			DoorTable.getInstance().getDoor(20250001).openMe();
		if(confidence > 800000)
		{
			DoorTable.getInstance().getDoor(20260003).openMe();
			DoorTable.getInstance().getDoor(20260004).openMe();
		}

		addEventId(ScriptEventType.ON_DIE);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public static final int NativeTransformationSkill = 3359;
	private static final int FieryDemonBloodSkill = 2357;

	private static final Location TowerofInfinitumLocationPoint = new Location(-22204, 277056, -15045);
	private static final Location TullyEntranceLocationPoint = new Location(17947, 283205, -9696);
	private static final Location TullyFloor1LocationPoint = new Location(-13400, 272827, -15304);

	public static final int FirstMark = 9850;
	public static final int SecondMark = 9851;
	public static final int ThirdMark = 9852;
	public static final int ForthMark = 9853;

	public static final int NativeHelmet = 9669;
	public static final int NativeTunic = 9670;
	public static final int NativePants = 9671;
	public static final int MagicBottle = 9672;
	public static final int HolyWater = 9673;
	public static final int DarionsBadge = 9674;
	public static final int MarkOfBetrayal = 9676;
	public static final int LifeForce = 9681;
	public static final int ContainedLifeForce = 9682;
	public static final int ScorpionPoisonStinger = 10012;
	public static final int MapofHellbound = 9994;

	private static final int ID_TRANSFORM_NATIVE = 101;

	public void getFirstMark()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		if(getItemCount(p, FirstMark) > 0 || getItemCount(p, SecondMark) > 0 || getItemCount(p, ThirdMark) > 0 || getItemCount(p, ForthMark) > 0) // уже есть какая-то
		{
			n.onBypassFeedback(p, "Chat 3");
			return;
		}

		if(getItemCount(p, DarionsBadge) >= 20)
		{
			removeItem(p, DarionsBadge, 20); // Darion's Badge
			addItem(p, FirstMark, 1); // Basic Caravan Certificate
			n.onBypassFeedback(p, "Chat 4");
		}
		else
			n.onBypassFeedback(p, "Chat 2");
	}

	public void getSecondMark()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		if(getConfidence() < 600000)
		{
			n.onBypassFeedback(p, "Chat 3");
			return;
		}

		if(getItemCount(p, FirstMark) == 0)
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		if(getItemCount(p, SecondMark) > 0 || getItemCount(p, ThirdMark) > 0 || getItemCount(p, ForthMark) > 0)
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, MarkOfBetrayal) >= 30 && getItemCount(p, ScorpionPoisonStinger) >= 60)
		{
			removeItem(p, MarkOfBetrayal, 30); // Mark of Betrayal
			removeItem(p, ScorpionPoisonStinger, 60); // Scorpion Poison Stingers
			removeItem(p, FirstMark, 1); // Basic Caravan Certificate
			addItem(p, SecondMark, 1); // Standard Caravan Certificate
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	public void getThirdMark()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		if(getConfidence() < 1000000) // слишком низкий уровень доверия
		{
			n.onBypassFeedback(p, "Chat 4");
			return;
		}

		if(getItemCount(p, SecondMark) == 0) // нет второй марки
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		if(getItemCount(p, ThirdMark) > 0 || getItemCount(p, ForthMark) > 0) // есть третья или выше
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, LifeForce) >= 56 && getItemCount(p, ContainedLifeForce) >= 14)
		{
			removeItem(p, LifeForce, 56); // Life Force
			removeItem(p, ContainedLifeForce, 14); // Contained Life Force
			removeItem(p, SecondMark, 1); // Standard Caravan Certificate
			addItem(p, ThirdMark, 1); // Premium Caravan Certificate
			addItem(p, MapofHellbound, 1); // Map of Hellbound
		}
		else
			n.onBypassFeedback(p, "Chat 4");
	}

	public void tradeAntiHeatBottle()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, SecondMark) == 0 && getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет второй или выше марки
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		n.onBypassFeedback(p, "Multisell 250980014");
	}

	public void tradeS80()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет третьей или выше марки
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		n.onBypassFeedback(p, "Multisell 250980013");
	}

	public void craftNativeHelmet()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, FirstMark) == 0 && getItemCount(p, SecondMark) == 0 && getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет марки
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, DarionsBadge) >= 10)
		{
			removeItem(p, DarionsBadge, 10); // Darion's Badge
			addItem(p, NativeHelmet, 1); // Native Helmet
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	public void craftNativeTunic()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, FirstMark) == 0 && getItemCount(p, SecondMark) == 0 && getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет марки
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, DarionsBadge) >= 10)
		{
			removeItem(p, DarionsBadge, 10); // Darion's Badge
			addItem(p, NativeTunic, 1); // Native Tunic
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	public void craftNativePants()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, FirstMark) == 0 && getItemCount(p, SecondMark) == 0 && getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет марки
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, DarionsBadge) >= 10)
		{
			removeItem(p, DarionsBadge, 10); // Darion's Badge
			addItem(p, NativePants, 1); // Native Pants
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	public void buyMagicBottle()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();
		if(getItemCount(p, SecondMark) == 0 && getItemCount(p, ThirdMark) == 0 && getItemCount(p, ForthMark) == 0) // нет второй или выше марки
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		if(getItemCount(p, ScorpionPoisonStinger) >= 20)
		{
			removeItem(p, ScorpionPoisonStinger, 20); // Scorpion Poison Stingers
			addItem(p, MagicBottle, 1); // Magic Bottle
		}
		else
			n.onBypassFeedback(p, "Chat 1");
	}

	public void buyHolyWater()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		// требуется трансформация в Native
		if(p.getTransformationId() != ID_TRANSFORM_NATIVE)
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		if(getItemCount(p, DarionsBadge) >= 5)
		{
			removeItem(p, DarionsBadge, 5); // Darion's Badge
			addItem(p, HolyWater, 1); // Holy Water
		}
		else
			n.onBypassFeedback(p, "Chat 3");
	}

	@Override
	protected final void onDie(final L2Character cha, final L2Character killer)
	{
		if(cha == null || killer == null || !cha.isMonster() || !killer.isPlayable())
			return;

		switch (cha.getNpcId())
		{
			case 22320: // Junior Watchman
			case 22321: // Junior Summoner
			case 22324: // Blind Huntsman
			case 22325: // Blind Watchman
				changeConfidence(killer, Config.HELLBOUND_NPC_LIST1); // confirmed
				break;
			case 22327: // Arcane Scout
			case 22328: // Arcane Guardian
			case 22329: // Arcane Watchman
			case 22342: // Darion's Enforcer
			case 22343: // Darion's Executioner
				changeConfidence(killer, Config.HELLBOUND_NPC_LIST2); // confirmed
				break;
			case 32299: // Quarry Slave
			case 22322: // Subjugated Native
			case 22323: // Charmed Native
				changeConfidence(killer, Config.HELLBOUND_NPC_LIST3); // high probability
				break;
			case 18463: // Remnant Diabolist
			case 18464: // Remnant Diviner
			case 22330:
				changeConfidence(killer, Config.HELLBOUND_NPC_LIST4); // high probability
				break;
			// FIXME: unknown values
			case 22334: // Sand Scorpion
			case 22337: // Desiccator
			case 22339: // Wandering Caravan
			case 22340: // Sandstorm
			case 22344: // Quarry Supervisor
			case 22345: // Quarry Bowman
			case 22346: // Quarry Foreman
			case 22347: // Quarry Patrolman
			case 22355: // Enceinte Defender
			case 22356: // Enceinte Defender
			case 22357: // Enceinte Defender
			case 22358: // Enceinte Defender
			case 22341: // Keltas
				changeConfidence(killer, Config.HELLBOUND_NPC_LIST5);
				break;
			case 18465: // Derek - First Generation Seer
				changeConfidence(killer, Config.DEREK); // high probability
				break;
			case 22448: // Leodas - Resistance Commander
				changeConfidence(killer, Config.LEODAS);
				break;
			case 22450: // Tortured Native
				changeConfidence(killer, Config.TORTURED_NATIVE);
				break;
			case 22361: // Steel Citadel Keymaster
				changeConfidence(killer, Config.STEEL_CITADEL_KEYMASTER);
				break;
			case 22449: // Amaskari - Torture Expert
				changeConfidence(killer, Config.AMASKARI);
				break;
			case 25536: // Hannibal - Raid Boss
				changeConfidence(killer, Config.HANNIBAL);
				break;
		}
	}

	public static void changeConfidence(final L2Character cha, Long mod)
	{
		final L2Player p = cha.getPlayer();
		final long curr = getConfidence();
		mod = (long) (mod * Config.RATE_HELLBOUND_POINTS);
		final long n = Math.max(0, mod + curr);
		if(curr != n)
		{
			if(p != null && n < 1000000)
				p.sendMessage(new CustomMessage("HellboundConfidence", p).addNumber(n));
			ServerVariables.set("HellboundConfidence", n);

			// Дверь к каравану
			if(curr < 300000 && n > 300000)
				DoorTable.getInstance().getDoor(20250002).openMe();
			else if(curr > 300000 && n < 300000)
				DoorTable.getInstance().getDoor(20250002).closeMe();

			// Дверь к химерам
			if(curr < 600000 && n > 600000)
				DoorTable.getInstance().getDoor(20250001).openMe();
			else if(curr > 600000 && n < 600000)
				DoorTable.getInstance().getDoor(20250001).closeMe();
		}
	}

	private static long getConfidence()
	{
		return ServerVariables.getInt("HellboundConfidence", 0);
	}

	public void badgesToConfidence(final String[] param)
	{
		if(param == null || param.length < 1 || !Util.isNumber(param[0]))
			return;
		long count = Long.parseLong(param[0]);
		if(count <= 0)
			return;
		final L2Player p = (L2Player) getSelf();
		count = Math.min(count, getItemCount(p, DarionsBadge));
		if(count <= 0)
			return;
		removeItem(p, DarionsBadge, count);
		changeConfidence(p, count * 10L);
	}

	public void enterToInfinitumTower()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		// Нету партии или не лидер партии
		if(p.getParty() == null || !p.getParty().isLeader(p))
		{
			n.onBypassFeedback(p, "Chat 1");
			return;
		}

		final GArray<L2Player> members = p.getParty().getPartyMembers();

		// Далеко или нету эффекта херба Fiery Demon Blood
		for(final L2Player member : members)
			if(member == null || !L2NpcInstance.canBypassCheck(member, n) || member.getEffectList().getFirstEffect(FieryDemonBloodSkill) == null)
			{
				n.onBypassFeedback(p, "Chat 2");
				return;
			}

		// Телепортируем партию на 1 этаж Tower of Infinitum
		for(final L2Player member : members)
			member.teleToLocation(TowerofInfinitumLocationPoint);
	}

	public void enterToTullyEntrance()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		if(!L2NpcInstance.canBypassCheck(p, n))
			return;

		// Телепортируем чара в предбанник Tully's Workshop
		if(p.isQuestCompleted(_132_MatrasCuriosity.class))
			p.teleToLocation(TullyEntranceLocationPoint);
		else
			n.onBypassFeedback(p, "Chat 1");
	}

	public void enterToTullyFloor1()
	{
		final L2Player p = (L2Player) getSelf();
		final L2NpcInstance n = getNpc();

		if(!L2NpcInstance.canBypassCheck(p, n))
			return;

		// Нету партии или не лидер партии
		if(p.getParty() == null || !p.getParty().isLeader(p))
		{
			n.onBypassFeedback(p, "Chat 2");
			return;
		}

		final GArray<L2Player> members = p.getParty().getPartyMembers();

		// Далеко или не выполнен 132 квест
		for(final L2Player member : members)
			if(member == null || !L2NpcInstance.canBypassCheck(member, n) || !member.isQuestCompleted(_132_MatrasCuriosity.class))
			{
				n.onBypassFeedback(p, "Chat 1");
				return;
			}

		// Телепортируем партию на 1 этаж Tully's Workshop
		for(final L2Player member : members)
			member.teleToLocation(TullyFloor1LocationPoint);
	}
}
