package services;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.boss.ValakasManager;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.util.Location;

public class TeleToValakas extends Functions implements ScriptFile
{
	// Items
	private static final int FLOATING_STONE = 7267;

	private static final Location TELEPORT_POSITION1 = new Location(183831, -115457, -3296);
	private static final Location TELEPORT_POSITION2 = new Location(203940, -111840, 66);

	public void teleToCorridor()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;
		String var = player.getVar("ValakasEnter");
		boolean canenter = var != null && Long.parseLong(var) > System.currentTimeMillis();
		if(ValakasManager.isEnableEnterToLair())
			if(Functions.getItemCount(player, FLOATING_STONE) > 0 || canenter)
			{
				if(!canenter)
				{
					player.setVar("ValakasEnter", String.valueOf(System.currentTimeMillis() + 1000 * 60 * 60));
					Functions.removeItem(player, FLOATING_STONE, 1);
				}
				player.teleToLocation(TELEPORT_POSITION1);
			}
			else
				show("<html><body>Клеин:<br>У Вас нет Floating Stone. Перейти получить один, а затем вернуться ко мне.</body></html>", player);
		else
			show("<html><body>Клеин:<br>Валакас уже проснулся!<br>Вы не можете войти в Логово Валакаса.</body></html>", player);
	}

	public void teleToValakas()
	{
		L2Player player = (L2Player) getSelf();
		L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;
		String var = player.getVar("ValakasEnter");
		if(var != null)
			if(ValakasManager.isEnableEnterToLair())
			{
				ValakasManager.setValakasSpawnTask();
				player.teleToLocation(TELEPORT_POSITION2);
				player.unsetVar("ValakasEnter");
			}
			else
				show("<html><body>Сердце Вулкана:<br>Валакас Уже проснулся!<br>Вы не можете войти в Логово Валакаса.</body></html>", player);
		else
			show("Условия не выполнены что бы войти в Логово Валакаса.", player);
	}

	@Override
	public void onLoad()
	{}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
