package events_off.TrickTransmutation;

import l2n.Config;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Rnd;

/**
 * Trick or Transmutation Event
 */
public class TrickTransmutation extends EventScript implements ScriptFile
{
	// Эвент Менеджеры
	private static final int EVENT_MANAGER_ID = 32132; // Alchemist's Servitor
	// сундук
	private static final int CHESTS_ID = 13036; // Alchemist's Chest
	// Ключ
	private static final int A_CHEST_KEY = 9205; // Alchemist's Chest Key

	private static GArray<L2Spawn> _em_spawns = new GArray<L2Spawn>();
	private static GArray<L2Spawn> _ch_spawns = new GArray<L2Spawn>();
	private static boolean _active = false;

	@Override
	public void onLoad()
	{
		if(isActive())
		{
			_active = true;
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Loaded Event: Trick of Trnasmutation [state: activated]");
		}
		else
			System.out.println("Loaded Event: Trick of Trnasmutation [state: deactivated]");
	}

	/**
	 * Читает статус эвента из базы.
	 */
	private static boolean isActive()
	{
		return isActive("trickoftrans");
	}

	/**
	 * Запускает эвент
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("trickoftrans", true))
		{
			addEventId(ScriptEventType.ON_DIE);
			addEventId(ScriptEventType.ON_ENTER_WORLD);
			spawnEventManagers();
			System.out.println("Event: 'Trick of Transmutation' started.");
			Announcements.announceByCustomMessage("scripts.events_off.TrickOfTrans.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event: 'Trick of Transmutation' already started.");

		_active = true;
		
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Останавливает эвент
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;
			
		if(setActive("trickoftrans", false))
		{
			removeEventId(ScriptEventType.ON_DIE);
			removeEventId(ScriptEventType.ON_ENTER_WORLD);
			unSpawnEventManagers();
			System.out.println("Event: 'Trick of Transmutation' stopped.");
			Announcements.announceByCustomMessage("scripts.events_off.TrickOfTrans.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event: 'Trick of Transmutation' not started.");

		_active = false;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * Анонсируется при заходе игроком в мир
	 */
	@Override
	protected void onEnterWorld(final L2Player player)
	{
		if(_active)
			Announcements.announceToPlayerByCustomMessage(player, "scripts.events_off.TrickOfTrans.AnnounceEventStarted", null);
	}

	/**
	 * Обработчик смерти мобов, управляющий эвентовым дропом
	 */
	@Override
	protected void onDie(final L2Character cha, final L2Character killer)
	{
		if(_active && SimpleCheckDrop(cha, killer) && Rnd.get(1000) <= Config.EVENT_TRICK_OF_TRANS_CHANCE * killer.getPlayer().getRateItems() * Config.RATE_DROP_ITEMS * ((L2NpcInstance) cha).getTemplate().rateHp)
			((L2NpcInstance) cha).dropItem(killer.getPlayer(), A_CHEST_KEY, 1);
	}

	@Override
	public void onReload()
	{
		unSpawnEventManagers();
	}

	@Override
	public void onShutdown()
	{
		unSpawnEventManagers();
	}

	/**
	 * Спавнит эвент менеджеров
	 */
	private void spawnEventManagers()
	{
		// Эвент Менеджер
		final int EVENT_MANAGERS[][] = { { 147992, 28616, -2295, 0 }, // Aden
				{ 81919, 148290, -3472, 51432 }, // Giran
				{ 18293, 145208, -3081, 6470 }, // Dion
				{ -14694, 122699, -3122, 0 }, // Gludio
				{ -81634, 150275, -3155, 15863 } // Gludin
		};

		spawnNPCs(EVENT_MANAGER_ID, EVENT_MANAGERS, _em_spawns);
		spawnChest();
	}

	private void spawnChest()
	{
		// Сундуки
		final int CHESTS[][] = { { 148081, 28614, -2274, 2059 }, // Aden
				{ 147918, 28615, -2295, 31471 }, // Aden
				{ 147998, 28534, -2274, 49152 }, // Aden
				{ 148053, 28550, -2274, 55621 }, // Aden
				{ 147945, 28563, -2274, 40159 }, // Aden
				{ 82012, 148286, -3472, 61567 }, // Giran
				{ 81822, 148287, -3493, 29413 }, // Giran
				{ 81917, 148207, -3493, 49152 }, // Giran
				{ 81978, 148228, -3472, 53988 }, // Giran
				{ 81851, 148238, -3472, 40960 }, // Giran
				{ 18343, 145253, -3096, 7449 }, // Dion
				{ 18284, 145274, -3090, 19740 }, // Dion
				{ 18351, 145186, -3089, 61312 }, // Dion
				{ 18228, 145265, -3079, 21674 }, // Dion
				{ 18317, 145140, -3078, 55285 }, // Dion
				{ -14584, 122694, -3122, 65082 }, // Gludio
				{ -14610, 122756, -3143, 13029 }, // Gludio
				{ -14628, 122627, -3122, 50632 }, // Gludio
				{ -14697, 122607, -3143, 48408 }, // Gludio
				{ -14686, 122787, -3122, 12416 }, // Gludio
				{ -81745, 150275, -3134, 32768 }, // Gludin
				{ -81520, 150275, -3134, 0 }, // Gludin
				{ -81628, 150379, -3134, 16025 }, // Gludin
				{ -81696, 150347, -3155, 22854 }, // Gludin
				{ -81559, 150332, -3134, 3356 }, // Gludin
		};

		L2NpcTemplate template = NpcTable.getTemplate(CHESTS_ID);
		for(int[] element : CHESTS)
			try
			{
				L2Spawn sp = new L2Spawn(template);
				sp.setLocx(element[0]);
				sp.setLocy(element[1]);
				sp.setLocz(element[2]);
				sp.setAmount(1);
				sp.setHeading(element[3]);
				sp.setRespawnDelay(20);
				sp.init();
				sp.getLastSpawn().setAI(new AlchemistChest(sp.getLastSpawn()));
				_ch_spawns.add(sp);
			}
			catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
	}

	/**
	 * Удаляет спавн эвент менеджеров
	 */
	private void unSpawnEventManagers()
	{
		deSpawnNPCs(_em_spawns);
		deSpawnNPCs(_ch_spawns);
	}
}
