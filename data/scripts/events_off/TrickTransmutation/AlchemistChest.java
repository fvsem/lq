package events_off.TrickTransmutation;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.Fighter;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.SkillTable;
import l2n.util.Files;
import l2n.util.Rnd;

/**
 * @<!-- L2System -->
 * @date 16.12.2010
 * @time 14:46:26
 */
public class AlchemistChest extends Fighter
{
	// Рецепты
	private static final int RED_PSTC = 9162; // Red Philosopher''s Stone Transmutation Circle
	private static final int BLUE_PSTC = 9163; // Blue Philosopher''s Stone Transmutation Circle
	private static final int ORANGE_PSTC = 9164; // Orange Philosopher''s Stone Transmutation Circle
	private static final int BLACK_PSTC = 9165; // Black Philosopher''s Stone Transmutation Circle
	private static final int WHITE_PSTC = 9166; // White Philosopher''s Stone Transmutation Circle
	private static final int GREEN_PSTC = 9167; // Green Philosopher''s Stone Transmutation Circle

	// Ингридиенты
	private static final int PhilosophersStoneOre = 9168; // Philosopher''s Stone Ore
	private static final int PhilosophersStoneOreMax = 17; // Максимальное Кол-во
	private static final int PhilosophersStoneConversionFormula = 9169; // Philosopher''s Stone Conversion Formula
	private static final int MagicReagents = 9170; // Magic Reagents
	private static final int MagicReagentsMax = 30; // Максимальное Кол-во

	private static final int[] lowDrop = new int[] { RED_PSTC, BLUE_PSTC, ORANGE_PSTC, BLACK_PSTC, WHITE_PSTC, GREEN_PSTC, PhilosophersStoneConversionFormula };

	public AlchemistChest(L2Character actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSeeSpell(L2Skill skill, L2Character caster)
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead() || skill == null || !caster.isPlayer())
			return;

		L2Player player = caster.getPlayer();
		int level = player.getLevel();

		if(skill.getId() == 2322)
		{
			actor.doDie(player);
			if(Rnd.chance(30))
			{
				switch (Rnd.get(3))
				{
					case 0:
						Functions.addItem(player, lowDrop[Rnd.get(7)], 1);
						break;
					case 1:
						Functions.addItem(player, MagicReagents, Rnd.get(1, MagicReagentsMax));
						break;
					case 2:
						Functions.addItem(player, PhilosophersStoneOre, Rnd.get(1, PhilosophersStoneOreMax));
						break;
				}
				Functions.show(Files.read("data/scripts/events_off/TrickTransmutation/TrickOfTrans_01.htm", player), player);
			}
			else if(Rnd.chance(70))
			{
				// что за формула? оО
				int count = 20000 + level / 20 * 25000;
				int rr = Rnd.get(count) / 3;
				count = count + rr - count / 6;
				Functions.addItem(player, 57, count);
			}
		}
		else if(level > 60)
		{
			Functions.show(Files.read("data/scripts/events_off/TrickTransmutation/TrickOfTrans_03.htm", player), player);
			actor.doCast(SkillTable.getInstance().getInfo(264, 1), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1068, 2), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1036, 2), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1311, 3), player, true);
		}
		else if(level > 70)
		{
			Functions.show(Files.read("data/scripts/events_off/TrickTransmutation/TrickOfTrans_03.htm", player), player);
			actor.doCast(SkillTable.getInstance().getInfo(1068, 3), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1311, 6), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1310, 2), player, true);
		}
		else if(level > 80)
		{
			Functions.show(Files.read("data/scripts/events_off/TrickTransmutation/TrickOfTrans_03.htm", player), player);
			actor.doCast(SkillTable.getInstance().getInfo(1310, 4), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1308, 3), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(310, 1), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(275, 1), player, true);
			actor.doCast(SkillTable.getInstance().getInfo(1261, 1), player, true);
		}

		super.onEvtSeeSpell(skill, caster);
	}
}
