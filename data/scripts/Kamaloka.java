import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.Config;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.DimensionalRiftManager;
import l2n.game.instancemanager.DimensionalRiftManager.DimensionalRiftRoom;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.EffectList;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.DelusionChamber;
import l2n.game.model.entity.Instance;
import l2n.game.model.entity.KamalokaAbyssLabyrinth;
import l2n.game.model.entity.KamalokaNightmare;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

public class Kamaloka extends Functions implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: Kamaloka Gate Loaded");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public void Gatekeeper(final String[] param)
	{
		if(param.length < 1)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final InstanceManager izm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> izs = izm.getById(Integer.parseInt(param[0]));
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		final Instance iz = izs.get(0);
		assert iz != null;

		final String name = iz.getName();
		final int timelimit = iz.getDuration();
		final boolean dispellBuffs = iz.isRemoveBuffs();
		final int min_level = iz.getMinLevel();
		final int max_level = iz.getMaxLevel();
		final int minParty = iz.getMinParty();
		final int maxParty = iz.getMaxParty();

		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return;
		}

		if(player.getParty().isInReflection())
		{
			if(player.getLevel() < min_level || player.getLevel() > max_level)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			if(player.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			final Reflection old_ref = player.getParty().getReflection();
			if(old_ref != null)
			{
				if(!iz.equals(old_ref.getInstancedZone()))
				{
					player.sendMessage("Ваша партия находится в инстанс зоне уже.");
					return;
				}
				if(!Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("Leader") && izm.getTimeToNextEnterInstance(name, player) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
					return;
				}
				if(player.getLevel() < min_level || player.getLevel() > max_level)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
					return;
				}

				player.teleToLocation(old_ref.getTeleportLoc(), old_ref.getId());

				if(dispellBuffs)
				{
					player.stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
					if(player.getPet() != null)
						player.getPet().stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
				}
				return;
			}
		}

		if(player.getParty().getPartyLeaderOID() != player.getObjectId())
		{
			player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
			return;
		}

		if(player.getParty().getMemberCount() > maxParty)
		{
			player.sendPacket(Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT);
			return;
		}

		for(final L2Player member : player.getParty().getPartyMembers())
		{
			if(member.getLevel() < min_level || member.getLevel() > max_level)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				member.sendPacket(sm);
				player.sendPacket(sm);
				return;
			}
			if(member.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
				return;
			}
			if(!player.isInRange(member, 500))
			{
				member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				return;
			}
		}

		if(Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("Leader"))
		{
			if(izm.getTimeToNextEnterInstance(name, player) > 0)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
				return;
			}
		}
		else if(Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("All"))
			for(final L2Player member : player.getParty().getPartyMembers())
				if(izm.getTimeToNextEnterInstance(name, member) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return;
				}

		final Reflection r = new Reflection(name);

		for(final Instance i : izs.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		if(minParty <= 1)
			player.setVar(name, String.valueOf(System.currentTimeMillis()));
		for(final L2Player member : player.getParty().getPartyMembers())
		{
			if(dispellBuffs)
			{
				member.stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
				if(member.getPet() != null)
					member.getPet().stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
			}

			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.setReflection(r);
			member.teleToLocation(iz.getTeleportCoords());
		}

		r.setEmptyDestroyTime(60 * 1000);
		player.getParty().setReflection(r);
		r.setParty(player.getParty());
		if(timelimit > 0)
		{
			r.startCollapseTimer(timelimit * 60 * 1000);
			player.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));
		}
	}

	public void enterToAbyssLabyrinth(final String[] param)
	{
		if(param.length < 1)
			throw new IllegalArgumentException();

		final L2Player player = getSelfPlayer();
		if(player == null)
			return;

		final int instanceId = Integer.parseInt(param[0]);
		final InstanceManager izm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> izs = izm.getById(instanceId);
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		final Instance iz = izs.get(0);
		assert iz != null;

		final String name = iz.getName();
		final int timelimit = iz.getDuration();
		final boolean dispellBuffs = iz.isRemoveBuffs();
		final int min_level = iz.getMinLevel();
		final int max_level = iz.getMaxLevel();
		final int minParty = iz.getMinParty();
		final int maxParty = iz.getMaxParty();

		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return;
		}

		if(player.getParty().isInReflection())
		{
			if(player.getLevel() < min_level || player.getLevel() > max_level)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			if(player.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			final Reflection old_ref = player.getParty().getReflection();
			if(old_ref != null)
			{
				if(!iz.equals(old_ref.getInstancedZone()))
				{
					player.sendMessage("Ваша партия находится в инстанс зоне уже.");
					return;
				}
				if(!Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("Leader") && izm.getTimeToNextEnterInstance(name, player) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
					return;
				}
				if(player.getLevel() < min_level || player.getLevel() > max_level)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
					return;
				}

				player.teleToLocation(old_ref.getTeleportLoc(), old_ref.getId());

				if(dispellBuffs)
				{
					player.stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
					if(player.getPet() != null)
						player.getPet().stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
				}
				return;
			}
		}

		if(player.getParty().getPartyLeaderOID() != player.getObjectId())
		{
			player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
			return;
		}

		if(player.getParty().getMemberCount() > maxParty)
		{
			player.sendPacket(Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT);
			return;
		}

		for(final L2Player member : player.getParty().getPartyMembers())
		{
			if(member.getLevel() < min_level || member.getLevel() > max_level)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				member.sendPacket(sm);
				player.sendPacket(sm);
				return;
			}
			if(member.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
				return;
			}
			if(!player.isInRange(member, 500))
			{
				member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				return;
			}
		}

		if(Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("Leader"))
		{
			if(izm.getTimeToNextEnterInstance(name, player) > 0)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
				return;
			}
		}
		else if(Config.ALT_KAMALOKA_LIMITS.equalsIgnoreCase("All"))
			for(final L2Player member : player.getParty().getPartyMembers())
				if(izm.getTimeToNextEnterInstance(name, member) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return;
				}

		final KamalokaAbyssLabyrinth r = new KamalokaAbyssLabyrinth(iz, instanceId);
		r.setInstancedZoneId(instanceId);
		r.setEmptyDestroyTime(5 * 60 * 1000); 
		for(final Instance i : izs.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
		}

		if(minParty <= 1)
			player.setVar(name, String.valueOf(System.currentTimeMillis()));
		for(final L2Player member : player.getParty().getPartyMembers())
		{
			if(dispellBuffs)
			{
				member.stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
				if(member.getPet() != null)
					member.getPet().stopEffects(EffectList.PROC_DISPELL_ADVENTURERS_BUFFS);
			}

			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.setReflection(r);
			member.teleToLocation(iz.getTeleportCoords());
		}

		player.getParty().setReflection(r);
		r.setParty(player.getParty());
		if(timelimit > 0)
		{
			r.startCollapseTimer(timelimit * 60 * 1000);
			player.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));
		}
	}

	/**
	 * Метод для обработки входа в соло-камалоки
	 */
	public void enterSoloKamaloka(final String[] param)
	{
		if(param.length < 1)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		if(player.isInParty())
		{
			player.sendPacket(Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT);
			return;
		}

		KamalokaNightmare r = ReflectionTable.getInstance().findSoloKamaloka(player.getObjectId());
		if(r != null)
		{
			player.setVar("backCoords", r.getReturnLoc().toXYZString());
			player.teleToLocation(r.getTeleportLoc(), r.getId());
			return;
		}

		if(param[0].equals("-1"))
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		if(Config.ALT_KAMALOKA_NIGHTMARES_PREMIUM_ONLY && player.getBonus().RATE_XP <= 1)
		{
			player.sendMessage(new CustomMessage("common.PremiumOnly", player));
			return;
		}

		final InstanceManager izm = InstanceManager.getInstance();
		final TIntObjectHashMap<Instance> izs = InstanceManager.getInstance().getById(Integer.parseInt(param[0]));
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		final Instance iz = izs.get(0);
		assert iz != null;

		final String name = iz.getName();
		final int timelimit = iz.getDuration();
		final int min_level = iz.getMinLevel();
		final int max_level = iz.getMaxLevel();

		if(player.getLevel() < min_level || player.getLevel() > max_level)
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		if(izm.getTimeToNextEnterInstance(name, player) > 0)
			if(Functions.getItemCount(player, 13011) > 0)
				Functions.removeItem(player, 13011, 1);
			else
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
				return;
			}

		r = new KamalokaNightmare(player);

		for(final Instance i : izs.valueCollection())
		{
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		player.setVar(name, String.valueOf(System.currentTimeMillis()));
		r.setReturnLoc(player.getLoc());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());
		player.teleToLocation(r.getTeleportLoc(), r.getId());
		r.setEmptyDestroyTime(20 * 60 * 1000);
		ReflectionTable.getInstance().addSoloKamaloka(player.getObjectId(), r);
		if(timelimit > 0)
		{
			r.startCollapseTimer(timelimit * 60 * 1000);
			player.sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));
		}
	}

	public void StaticSoloInstance(final String[] param)
	{
		if(param.length < 1)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final int instanceId = Integer.parseInt(param[0]);

		final TIntObjectHashMap<Instance> izs = InstanceManager.getInstance().getById(instanceId);
		if(izs == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		final Instance iz = izs.get(0);
		assert iz != null;

		if(player.getLevel() < iz.getMinLevel() || player.getLevel() > iz.getMaxLevel() || player.isInFlyingTransform())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		Reflection r = ReflectionTable.SOD_REFLECTION_ID == 0 ? null : ReflectionTable.getInstance().get(ReflectionTable.SOD_REFLECTION_ID);
		if(ReflectionTable.SOD_REFLECTION_ID > 0 && r != null)
		{
			player.setVar("backCoords", r.getReturnLoc().toXYZString());
			player.teleToLocation(r.getTeleportLoc(), r.getId());
			return;
		}
		else
		{
			r = new Reflection(iz.getName());
			r.setInstancedZoneId(instanceId);
			ReflectionTable.SOD_REFLECTION_ID = r.getId();
		}

		final int timelimit = iz.getDuration();
		for(final Instance i : izs.valueCollection())
		{
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		r.setCoreLoc(r.getReturnLoc());
		r.setReturnLoc(player.getLoc());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());
		player.teleToLocation(r.getTeleportLoc(), r.getId());
		if(timelimit > 0)
			r.startCollapseTimer(timelimit * 60 * 1000);
	}

	public void LeaveKamaloka()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(player.getParty() == null || player.getParty().getPartyLeaderOID() != player.getObjectId())
		{
			show("You are not a party leader.", player);
			return;
		}

		player.getReflection().collapse();
	}

	public String DialogAppend_32484(final Integer val)
	{
		final L2Player player = (L2Player) getSelf();
		String ret = "";
		if(player == null || player.getLevel() < 20)
			return ret;
		if(Config.ALT_KAMALOKA_NIGHTMARE_REENTER || Config.ALT_KAMALOKA_ABYSS_REENTER || Config.ALT_KAMALOKA_LAB_REENTER)
		{
			ret += "<br>Ticket price: " + Util.formatAdena(player.getLevel() * 5000) + " adena.";
			if(Config.ALT_KAMALOKA_NIGHTMARE_REENTER)
				ret += "<br>[scripts_Kamaloka:buyTicket 13011|Купить билет в зал кошмаров]";
			if(Config.ALT_KAMALOKA_ABYSS_REENTER)
				ret += "<br>[scripts_Kamaloka:buyTicket 13010|Купить билет в Зал Бездны]";
			if(Config.ALT_KAMALOKA_LAB_REENTER)
				ret += "<br>[scripts_Kamaloka:buyTicket 13012|Купить билет в Лабиринт Бездны]";
		}
		return ret;
	}

	public void buyTicket(final String[] id)
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null || player.getLevel() < 20)
			return;
		final int price = player.getLevel() * 5000;
		if(Functions.getItemCount(player, 57) < price)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}
		Functions.removeItem(player, 57, price);
		Functions.addItem(player, Integer.parseInt(id[0]), 1);
	}

	public void toDC()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(!L2NpcInstance.canBypassCheck(player, npc))
			return;

		player.setVar("DCBackCoords", player.getLoc().toXYZString());
		player.teleToLocation(-114582, -152635, -6742);
	}

	public void fromDC()
	{
		final L2Player player = (L2Player) getSelf();
		final L2NpcInstance npc = getNpc();
		if(player == null || npc == null)
			return;

		if(!L2NpcInstance.canBypassCheck(player, npc))
			return;

		final String var = player.getVar("DCBackCoords");
		if(var == null || var.equals(""))
			return;
		player.teleToLocation(new Location(var), 0);
		player.unsetVar("DCBackCoords");
	}

	public void enterDC(final String[] param)
	{
		if(param.length < 1)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final int type = Integer.parseInt(param[0]);
		final FastMap<Integer, DimensionalRiftRoom> rooms = DimensionalRiftManager.getInstance().getRooms(type);

		if(rooms == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}
		final int min_level = 80;

		if(!player.isInParty())
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return;
		}

		if(player.getParty().isInReflection())
		{
			if(player.getLevel() < min_level)
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			if(player.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
				return;
			}
			final Reflection old_ref = player.getParty().getReflection();
			if(old_ref != null && old_ref instanceof DelusionChamber)
			{
				if((type == 11 || type == 12) && InstanceManager.getInstance().getTimeToNextEnterInstance(DelusionChamber.getNameById(type), player) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
					return;
				}
				player.teleToLocation(old_ref.getTeleportLoc(), old_ref.getId());
				return;
			}
		}

		if(player.getParty().getPartyLeaderOID() != player.getObjectId())
		{
			player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
			return;
		}

		if(type == 11 || type == 12)
			for(final L2Player member : player.getParty().getPartyMembers())
				if(InstanceManager.getInstance().getTimeToNextEnterInstance(DelusionChamber.getNameById(type), member) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return;
				}

		for(final L2Player member : player.getParty().getPartyMembers())
		{
			if(member.getLevel() < min_level)
			{
				final SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
				member.sendPacket(sm);
				player.sendPacket(sm);
				return;
			}
			if(member.isCursedWeaponEquipped())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
				return;
			}
			if(!player.isInRange(member, 500))
			{
				member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
				return;
			}
		}

		new DelusionChamber(player.getParty(), type, Rnd.get(1, rooms.size() - 1));
	}
}
