package quests_custom.DimensionalMerchants;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class DimensionalMerchants extends Quest implements ScriptFile
{
	private static boolean ENABLE_DIMENSIONAL_MERCHANTS = true;

	public DimensionalMerchants()
	{
		super(-1, "DimensionalMerchants", "Dimensional Merchants", -1);
		addStartNpc(32478);
		addTalkId(32478);
		addFirstTalkId(32478);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = "";
		htmltext = event;

		if(event.equalsIgnoreCase("13017") || event.equalsIgnoreCase("13018") || event.equalsIgnoreCase("13019") || event.equalsIgnoreCase("13020"))
		{
			long normalItem = st.getQuestItemsCount(13273);
			long eventItem = st.getQuestItemsCount(13383);
			if(normalItem >= 1)
			{
				st.takeItems(13273, 1);
				st.giveItems(Integer.valueOf(event), 1);
				st.exitCurrentQuest(true);
				return htmltext;
			}
			else if(eventItem >= 1)
			{
				event = event + 286;
				st.takeItems(13383, 1);
				st.giveItems(Integer.valueOf(event), 1);
				st.exitCurrentQuest(true);
				return htmltext;
			}
			else
				htmltext = "32478-11.htm";
		}
		else if(event.equalsIgnoreCase("13548") || event.equalsIgnoreCase("13549") || event.equalsIgnoreCase("13550") || event.equalsIgnoreCase("13551"))
			if(st.getQuestItemsCount(14065) >= 1)
			{
				st.takeItems(14065, 1);
				st.giveItems(Integer.valueOf(event), 1);
				st.exitCurrentQuest(true);
				return htmltext;
			}
			else
			{
				htmltext = "32478-11.htm";
				st.exitCurrentQuest(true);
			}

		return htmltext;
	}

	@Override
	public String onFirstTalk(L2NpcInstance npc, L2Player player)
	{
		String htmltext = "";
		QuestState st = player.getQuestState(getName());

		if(st == null)
			newQuestState(player, STARTED);

		if(ENABLE_DIMENSIONAL_MERCHANTS)
			htmltext = "32478.htm";
		else
			htmltext = "32478-na.htm";

		return htmltext;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Custom Quest: Dimensional Merchants");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
