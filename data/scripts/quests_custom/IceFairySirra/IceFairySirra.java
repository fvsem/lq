package quests_custom.IceFairySirra;

import java.util.concurrent.Future;
import java.util.logging.Level;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.NpcHtmlMessage;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.DoorTable;
import l2n.game.tables.NpcTable;
import l2n.game.tables.SpawnTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;

public class IceFairySirra extends Quest implements ScriptFile
{
	private static final int STEWARD = 32029;
	private static final int ICE_FAIRY_SIRRA = 29056;
	private static final int SILVER_HEMOCYTE = 8057;
	private static L2Player _player = null;
	protected GArray<L2NpcInstance> _allMobs = new GArray<L2NpcInstance>();
	protected Future<?> _onDeadEventTask = null;

	public IceFairySirra()
	{
		super(-1, "IceFairySirra", "Ice Fairy Sirra", -1);
		int[] mob = { STEWARD, 22100, 22102, 22104 };
		registerMobs(mob);
		addTalkId(STEWARD);
		addFirstTalkId(STEWARD);
		addStartNpc(STEWARD);
		addKillId(ICE_FAIRY_SIRRA);
		init();
	}

	private void init()
	{
		L2NpcInstance steward = L2ObjectsStorage.getByNpcId(STEWARD);
		if(steward != null)
			steward.setBusy(false);
		openGates();
	}

	public void cleanUp()
	{
		init();
		cancelQuestTimer("30MinutesRemaining", null, _player);
		cancelQuestTimer("20MinutesRemaining", null, _player);
		cancelQuestTimer("10MinutesRemaining", null, _player);
		cancelQuestTimer("End", null, _player);
		for(L2NpcInstance mob : _allMobs)
		{
			try
			{
				mob.deleteMe();
			}
			catch(Exception e)
			{
				_log.log(Level.WARNING, "IceFairySirraManager: Failed deleting mob.", e);
			}
		}
		_allMobs.clear();
	}

	public L2NpcInstance findTemplate(int npcId)
	{
		L2NpcInstance npc = null;
		for(L2Spawn spawn : SpawnTable.getInstance().getSpawnTable())
		{
			if(spawn != null && spawn.getNpcId() == npcId)
			{
				npc = spawn.getLastSpawn();
				break;
			}
		}
		return npc;
	}

	protected void openGates()
	{
		for(int i = 23140001; i < 23140003; i++)
		{
			try
			{
				L2DoorInstance door = DoorTable.getInstance().getDoor(i);
				if(door != null)
					door.openMe();
				else
					_log.warning("IceFairySirraManager: Attempted to open undefined door. doorId: " + i);
			}
			catch(RuntimeException e)
			{
				_log.warning("IceFairySirraManager: Failed closing door");
				e.printStackTrace();
			}
		}
	}

	protected void closeGates()
	{
		for(int i = 23140001; i < 23140003; i++)
		{
			try
			{
				L2DoorInstance door = DoorTable.getInstance().getDoor(i);
				if(door != null)
					door.closeMe();
				else
					_log.warning("IceFairySirraManager: Attempted to close undefined door. doorId: " + i);
			}
			catch(RuntimeException e)
			{
				_log.warning("IceFairySirraManager: Failed closing door");
				e.printStackTrace();
			}
		}
	}

	public boolean checkItems(L2Player player)
	{
		if(player.getParty() != null)
		{
			for(L2Player pc : player.getParty().getPartyMembers())
			{
				L2ItemInstance i = pc.getInventory().getItemByItemId(SILVER_HEMOCYTE);
				if(i == null || i.getCount() < 10)
					return false;
			}
		}
		else
			return false;
		return true;
	}

	public void destroyItems(L2Player player)
	{
		if(player.getParty() != null)
		{
			for(L2Player pc : player.getParty().getPartyMembers())
			{
				L2ItemInstance i = pc.getInventory().getItemByItemId(SILVER_HEMOCYTE);
				pc.getInventory().destroyItem(i, 10, true);
				pc.sendPacket(new SystemMessage(SystemMessage.S2_S1_HAS_DISAPPEARED).addItemName(SILVER_HEMOCYTE).addNumber(10));
			}
		}
		else
			cleanUp();
	}

	public void teleportInside(L2Player player)
	{
		if(player.getParty() != null)
		{
			for(L2Player pc : player.getParty().getPartyMembers())
				pc.teleToLocation(113533, -126159, -3488);
		}
		else
			cleanUp();
	}

	public void screenMessage(L2Player player, String text, int time)
	{
		if(player.getParty() != null)
		{
			for(L2Player pc : player.getParty().getPartyMembers())
			{
				pc.sendPacket(new ExShowScreenMessage(text, time));
			}
		}
		else
			cleanUp();
	}

	public void doSpawns()
	{
		int[][] mobs = {
				{ 29060, 105546, -127892, -2768 },
				{ ICE_FAIRY_SIRRA, 102779, -125920, -2840 },
				{ 22100, 111719, -126646, -2992 },
				{ 22102, 109509, -128946, -3216 },
				{ 22104, 109680, -125756, -3136 } };
		L2Spawn spawnDat;
		L2NpcTemplate template;
		try
		{
			for(int i = 0; i < 5; i++)
			{
				template = NpcTable.getTemplate(mobs[i][0]);
				if(template != null)
				{
					spawnDat = new L2Spawn(template);
					spawnDat.setAmount(1);
					spawnDat.setLocx(mobs[i][1]);
					spawnDat.setLocy(mobs[i][2]);
					spawnDat.setLocz(mobs[i][3]);
					spawnDat.setHeading(0);
					spawnDat.setRespawnDelay(0);
					_allMobs.add(spawnDat.doSpawn(true));
					spawnDat.stopRespawn();
				}
				else
				{
					_log.warning("IceFairySirraManager: Data missing in NPC table for ID: " + mobs[i][0]);
				}
			}
		}
		catch(Exception e)
		{
			_log.warning("IceFairySirraManager: Spawns could not be initialized: ");
			e.printStackTrace();
		}
	}

	public String getHtmlPath(int val)
	{
		String pom = "";

		pom = "32029-" + val;
		if(val == 0)
			pom = "32029";

		String temp = "data/html/default/" + pom + ".htm";

		if(Files.read(temp) != null)
			return temp;

		return "data/html/npcdefault.htm";
	}

	public void sendHtml(L2NpcInstance npc, L2Player player, String filename)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(player, npc);
		html.setFile(filename);
		html.replace("%objectId%", String.valueOf(npc.getObjectId()));
		player.sendPacket(html);
		player.sendActionFailed();
	}

	@Override
	public String onFirstTalk(L2NpcInstance npc, L2Player player)
	{
		if(player.getQuestState("IceFairySirra") == null)
			newQuestState(player, STARTED);

		String filename = "";
		if(npc.isBusy())
			filename = getHtmlPath(10);
		else
			filename = getHtmlPath(0);
		sendHtml(npc, player, filename);
		return null;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String filename = "";
		L2Player player = st.getPlayer();

		if(event.equalsIgnoreCase("check_condition"))
		{
			if(npc.isBusy())
				return super.onEvent(event, st, npc);
			else
			{
				if(player.isInParty() && player.getParty().getPartyLeaderOID() == player.getObjectId())
				{
					if(checkItems(player))
					{
						startQuestTimer("start", 100000, null, player);
						_player = player;
						destroyItems(player);
						player.getInventory().addItem(8379, 3, 0, "IceFairySirra - Scroll");
						player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_EARNED_S2_S1S).addItemName(8379).addNumber(3));
						npc.setBusy(true);
						screenMessage(player, "Steward: Please wait a moment.", 100000);
						filename = getHtmlPath(3);
					}
					else
						filename = getHtmlPath(2);
				}
				else
					filename = getHtmlPath(1);

				sendHtml(npc, player, filename);
			}
		}
		else if(event.equalsIgnoreCase("start"))
		{
			closeGates();
			doSpawns();
			startQuestTimer("Party_Port", 2000, null, player);
			startQuestTimer("End", 1802000, null, player);
		}
		else if(event.equalsIgnoreCase("Party_Port"))
		{
			teleportInside(player);
			screenMessage(player, "Steward: Please restore the Queen's appearance!", 10000);
			startQuestTimer("30MinutesRemaining", 300000, null, player);
		}
		else if(event.equalsIgnoreCase("30MinutesRemaining"))
		{
			screenMessage(player, "30 minute(s) are remaining.", 10000);
			startQuestTimer("20minutesremaining", 600000, null, player);
		}
		else if(event.equalsIgnoreCase("20MinutesRemaining"))
		{
			screenMessage(player, "20 minute(s) are remaining.", 10000);
			startQuestTimer("10minutesremaining", 600000, null, player);
		}
		else if(event.equalsIgnoreCase("10MinutesRemaining"))
		{
			screenMessage(player, "Steward: Waste no time! Please hurry!", 10000);
		}
		else if(event.equalsIgnoreCase("End"))
		{
			screenMessage(player, "Steward: Was it indeed too much to ask.", 10000);
			cleanUp();
		}
		return super.onEvent(event, st, npc);
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == ICE_FAIRY_SIRRA)
		{
			cleanUp();
			openGates();
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Custom Quest: Ice Fairy Sirra");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
