package quests_custom._1002_Antharas;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.boss.AntharasManager;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Location;

public class _1002_Antharas extends Quest implements ScriptFile
{
	// NPCs
	private static final int HEART = 13001;

	// Items
	private static final int PORTAL_STONE = 3865;

	private static final Location TELEPORT_POSITION = new Location(179892, 114915, -7704);

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Custom Quest: Antharas");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _1002_Antharas()
	{
		super(1002, "_1002_Antharas", "Antharas", 1);
		addStartNpc(HEART);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == HEART)
		{
			if(st.getPlayer().isFlying())
				return "<html><body>Сердце Заключенного:<br>Вы не можете войти во время полета виверны</body></html>";
			if(AntharasManager.isEnableEnterToLair())
			{
				if(st.getQuestItemsCount(PORTAL_STONE) >= 1)
				{
					st.takeItems(PORTAL_STONE, 1);
					AntharasManager.setAntharasSpawnTask();
					st.getPlayer().teleToLocation(TELEPORT_POSITION);
					return null;
				}
				st.exitCurrentQuest(true);
				return "<html><body>Сердце Заключенного:<br>У вас нет надлежащих камней необходимых для телепорта.<br>Для телепорта вам нужно 1 камень.</body></html>";
			}
			st.exitCurrentQuest(true);
			return "<html><body>Сердце Заключенного:<br>Антарас уже проснулися!<br>Вы не можете войти в Логово Антараса.</body></html>";
		}
		return null;
	}
}
