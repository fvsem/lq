package quests._246_PossessorOfaPreciousSoul3;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _246_PossessorOfaPreciousSoul3 extends Quest implements ScriptFile
{
	private final static int CARADINES_LETTER_2_PART = 7678;
	private final static int RING_OF_GODDESS_WATERBINDER = 7591;
	private final static int NECKLACE_OF_GODDESS_EVERGREEN = 7592;
	private final static int STAFF_OF_GODDESS_RAIN_SONG = 7593;
	private final static int CARADINES_LETTER = 7679;
	private final static int RELIC_BOX = 7594;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _246_PossessorOfaPreciousSoul3()
	{
		super(246, PARTY_ALL);

		addStartNpc(31740);
		addTalkId(31741, 30721);
		addKillId(21541, 21544, 25325);
		addQuestItem(RING_OF_GODDESS_WATERBINDER, NECKLACE_OF_GODDESS_EVERGREEN, STAFF_OF_GODDESS_RAIN_SONG);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31740-2.htm"))
		{
			st.set("cond", "1");
			st.takeItems(CARADINES_LETTER_2_PART, 1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31741-2.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("31741-4.htm"))
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("31741-6.htm"))
		{
			st.takeItems(RING_OF_GODDESS_WATERBINDER, 1);
			st.takeItems(NECKLACE_OF_GODDESS_EVERGREEN, 1);
			st.takeItems(STAFF_OF_GODDESS_RAIN_SONG, 1);
			st.set("cond", "6");
			st.giveItems(RELIC_BOX, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equals("30721-2.htm"))
		{
			st.takeItems(RELIC_BOX, 1);
			st.giveItems(CARADINES_LETTER, 1);
			st.addExpAndSp(719843, 0);
			st.unset("cond");
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		if(!st.getPlayer().isSubClassActive())
			return "Subclass only!";

		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 31740)
		{
			if(cond == 0)
			{
				QuestState previous = st.getPlayer().getQuestState("_242_PossessorOfaPreciousSoul2");
				if(previous != null && previous.getState() == COMPLETED && st.getPlayer().getLevel() >= 65)
					htmltext = "31740-1.htm";
				else
				{
					htmltext = "31740-0.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && st.getPlayer().isSubClassActive())
				htmltext = "31740-2r.htm";
		}
		else if(npcId == 31741 && st.getPlayer().isSubClassActive())
		{
			if(cond == 1)
				htmltext = "31741-1.htm";
			else if((cond == 2 || cond == 3) && (st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) < 1 || st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) < 1))
				htmltext = "8743-2r.htm";
			else if(cond == 3 && st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) == 1 && st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) == 1)
				htmltext = "31741-3.htm";
			else if(cond == 4)
				htmltext = "31741-4.htm";
			else if((cond == 4 || cond == 5) && st.getQuestItemsCount(STAFF_OF_GODDESS_RAIN_SONG) < 1)
				htmltext = "31741-4r.htm";
			else if(cond == 5 && st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) == 1 && st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) == 1 && st.getQuestItemsCount(STAFF_OF_GODDESS_RAIN_SONG) == 1)
				htmltext = "31741-5.htm";
			else if(cond == 6)
				htmltext = "31741-6r.htm";
		}
		else if(npcId == 30721 && st.getPlayer().isSubClassActive())
			if(cond == 6 && st.getQuestItemsCount(RELIC_BOX) == 1)
				htmltext = "30721-1.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(cond == 2 && st.getPlayer().isSubClassActive())
		{
			if(Rnd.chance(15))
				if(npcId == 21541 && st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) == 0)
				{
					st.giveItems(RING_OF_GODDESS_WATERBINDER, 1);
					if(st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) == 1 && st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) == 1)
						st.set("cond", "3");
					st.playSound(SOUND_ITEMGET);
				}
				else if(npcId == 21544 && st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) == 0)
				{
					st.giveItems(NECKLACE_OF_GODDESS_EVERGREEN, 1);
					if(st.getQuestItemsCount(RING_OF_GODDESS_WATERBINDER) == 1 && st.getQuestItemsCount(NECKLACE_OF_GODDESS_EVERGREEN) == 1)
						st.set("cond", "3");
					st.playSound(SOUND_ITEMGET);
				}
		}
		else if(st.getPlayer().isSubClassActive())
			if(npcId == 25325 && st.getQuestItemsCount(STAFF_OF_GODDESS_RAIN_SONG) == 0)
			{
				st.giveItems(STAFF_OF_GODDESS_RAIN_SONG, 1);
				st.set("cond", "5");
				st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
