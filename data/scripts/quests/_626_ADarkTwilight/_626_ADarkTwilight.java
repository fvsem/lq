package quests._626_ADarkTwilight;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _626_ADarkTwilight extends Quest implements ScriptFile
{
	// NPC
	private static final int Hierarch = 31517;
	// QuestItem
	private static int BloodOfSaint = 7169;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 626: A Dark Twilight");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _626_ADarkTwilight()
	{
		super(626, -1);
		addStartNpc(Hierarch);
		for(int npcId = 21520; npcId <= 21542; npcId++)
			addKillId(npcId);
		addQuestItem(BloodOfSaint);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31517-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31517-03-choose.htm"))
		{
			if(st.getQuestItemsCount(BloodOfSaint) < 300)
				htmltext = "31517-bug.htm";
		}
		else if(event.equalsIgnoreCase("rew_exp"))
		{
			st.takeItems(BloodOfSaint, -1);
			st.addExpAndSp(162773, 12500);
			htmltext = "31517-reward.htm";
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("rew_adena"))
		{
			st.takeItems(BloodOfSaint, -1);
			st.giveItems(57, 100000);
			htmltext = "31517-reward.htm";
			st.exitCurrentQuest(true);
		}

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		if(npcId == Hierarch)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 60)
				{
					htmltext = "31517-00.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31517-01.htm";
			}
			else if(cond == 1)
				htmltext = "31517-02-rep.htm";
			else if(cond == 2)
				htmltext = "31517-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		if(cond == 1 && st.getQuestItemsCount(BloodOfSaint) < 300)
			if(st.rollAndGive(BloodOfSaint, 1, 1, 300, 70))
			{
				st.set("cond", "2");
				st.setState(STARTED);
			}
		return null;
	}
}
