package quests._652_AnAgedExAdventurer;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _652_AnAgedExAdventurer extends Quest implements ScriptFile
{
	// NPC
	private static final int Tantan = 32012;
	private static final int Sara = 30180;
	// Item
	private static final int SoulshotCgrade = 1464;
	private static final int Adena = 57;
	private static final int ScrollEnchantArmorD = 956;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 652: An Aged Ex-Adventurer");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _652_AnAgedExAdventurer()
	{
		super(652, -1);

		addStartNpc(Tantan);

		addTalkId(Tantan);
		addTalkId(Sara);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32012-02.htm") && st.getQuestItemsCount(SoulshotCgrade) >= 100)
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.takeItems(SoulshotCgrade, 100);
			st.playSound(SOUND_ACCEPT);
		}
		else
		{
			htmltext = "32012-02a.htm";
			st.exitCurrentQuest(true);
			st.playSound(SOUND_GIVEUP);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == Tantan)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() < 46)
				{
					htmltext = "32012-00.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "32012-01.htm";
		}
		else if(npcId == Sara && cond == 1)
		{
			htmltext = "30180-01.htm";
			st.giveItems(Adena, 5026, true);
			if(Rnd.chance(50))
				st.giveItems(ScrollEnchantArmorD, 1, false);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}
}
