package quests._649_ALooterandaRailroadMan;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест A Looteranda Railroad Man
 */

public class _649_ALooterandaRailroadMan extends Quest implements ScriptFile
{
	// NPC
	private static final int OBI = 32052;
	// Quest Item
	private static final int THIEF_GUILD_MARK = 8099;
	// Item
	private static final int ADENA = 57;
	// Main
	// Drop Cond
	// # [COND, NEWCOND, ID, REQUIRED, ITEM, NEED_COUNT, CHANCE, DROP]
	private static final int[][] DROPLIST_COND = {
			{ 1, 2, 22017, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22018, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22019, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22021, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22022, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22023, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22024, 0, THIEF_GUILD_MARK, 200, 50, 1 },
			{ 1, 2, 22026, 0, THIEF_GUILD_MARK, 200, 50, 1 } };

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 649: A Looter and a Railroad Man");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _649_ALooterandaRailroadMan()
	{
		super(649, -1);

		addStartNpc(OBI);
		addTalkId(OBI);
		// Mob Drop
		for(int i = 0; i < DROPLIST_COND.length; i++)
			addKillId(DROPLIST_COND[i][2]);
		addQuestItem(THIEF_GUILD_MARK);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32052-1.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32052-3.htm"))
			if(st.getQuestItemsCount(THIEF_GUILD_MARK) == 200)
			{
				st.takeItems(THIEF_GUILD_MARK, -1);
				st.giveItems(ADENA, 21698);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
			// Проверка сработает если игрок во время диалога удалит марки
			{
				st.set("cond", "1");
				htmltext = "32052-3a.htm";
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == OBI)
			if(cond == 0)
				if(st.getPlayer().getLevel() < 30)
				{
					htmltext = "32052-0a.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "32052-0.htm";
			else if(cond == 1)
				htmltext = "32052-2a.htm";
			else if(cond == 2 && st.getQuestItemsCount(THIEF_GUILD_MARK) == 200)
				htmltext = "32052-2.htm";
			else
			{
				htmltext = "32052-2a.htm";
				st.set("cond", "1");
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		for(int i = 0; i < DROPLIST_COND.length; i++)
			if(cond == DROPLIST_COND[i][0] && npcId == DROPLIST_COND[i][2])
				if(DROPLIST_COND[i][3] == 0 || st.getQuestItemsCount(DROPLIST_COND[i][3]) > 0)
					if(DROPLIST_COND[i][5] == 0)
						st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][6]);
					else if(st.rollAndGive(DROPLIST_COND[i][4], DROPLIST_COND[i][7], DROPLIST_COND[i][7], DROPLIST_COND[i][5], DROPLIST_COND[i][6]))
						if(DROPLIST_COND[i][1] != cond && DROPLIST_COND[i][1] != 0)
						{
							st.set("cond", String.valueOf(DROPLIST_COND[i][1]));
							st.setState(STARTED);
						}
		return null;
	}

}
