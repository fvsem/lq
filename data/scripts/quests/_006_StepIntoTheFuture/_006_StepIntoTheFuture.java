package quests._006_StepIntoTheFuture;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.Race;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Рейты не учитываются, награда специфичная
 */

public class _006_StepIntoTheFuture extends Quest implements ScriptFile
{
	// NPC
	private static final int Roxxy = 30006;
	private static final int Baulro = 30033;
	private static final int Windawood = 30311;
	// Quest Item
	private static final int BaulrosLetter = 7571;
	// Items
	private static final int ScrollOfEscapeGiran = 7126;
	private static final int MarkOfTraveler = 7570;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _006_StepIntoTheFuture()
	{
		super(6, -1);
		addStartNpc(Roxxy);

		addTalkId(Baulro);
		addTalkId(Windawood);

		addQuestItem(BaulrosLetter);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30006-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30033-02.htm"))
		{
			st.giveItems(BaulrosLetter, 1, false);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30311-03.htm"))
		{
			st.takeItems(BaulrosLetter, -1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30006-06.htm"))
		{
			st.giveItems(ScrollOfEscapeGiran, 1, false);
			st.giveItems(MarkOfTraveler, 1, false);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == Roxxy)
		{
			if(cond == 0)
				if(st.getPlayer().getRace() == Race.human && st.getPlayer().getLevel() >= 3)
					htmltext = "30006-02.htm";
				else
				{
					htmltext = "30006-01.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "30006-04.htm";
			else if(cond == 3)
				htmltext = "30006-05.htm";
		}
		else if(npcId == Baulro)
		{
			if(cond == 1)
				htmltext = "30033-01.htm";
			else if(cond == 2 && st.getQuestItemsCount(BaulrosLetter) > 0)
				htmltext = "30033-03.htm";
		}
		else if(npcId == Windawood)
			if(cond == 2 && st.getQuestItemsCount(BaulrosLetter) > 0)
				htmltext = "30311-02.htm";
			else if(cond == 2 && st.getQuestItemsCount(BaulrosLetter) == 0)
				htmltext = "30311-havent.htm";
			else if(cond == 3)
				htmltext = "30311-03r.htm";
		return htmltext;
	}
}
