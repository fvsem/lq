package quests._10278_MutatedKaneus_Heine;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Heine</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10278_MutatedKaneus_Heine extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10278_MutatedKaneus_Heine()
	{
		super(10278, "_10278_MutatedKaneus_Heine", "Mutated Kaneus - Heine");

		NPC = new int[] { 30916, 30907 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18562, 13834 }, // Blade Otis
				{ 18564, 13835 } // Weird Bunei
		};
		startLevel = 38;
		registerNPCs();
	}
}
