package quests._724_ForTheSakeOfTheTerritoryRune;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _724_ForTheSakeOfTheTerritoryRune extends TerritoryWarSuperClass
{
	public _724_ForTheSakeOfTheTerritoryRune()
	{
		super(724);
		_catapultId = 36506;
		_territoryId = 8;
		_leaderIDs = new int[] { 36550, 36552, 36555, 36598 };
		_guardIDs = new int[] { 36551, 36553, 36554 };
		_text = new String[] { "The catapult of Rune has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
