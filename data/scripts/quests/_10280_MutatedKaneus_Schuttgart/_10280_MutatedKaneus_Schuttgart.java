package quests._10280_MutatedKaneus_Schuttgart;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Schuttgart</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10280_MutatedKaneus_Schuttgart extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10280_MutatedKaneus_Schuttgart()
	{
		super(10280, "_10280_MutatedKaneus_Schuttgart", "Mutated Kaneus - Schuttgart");

		NPC = new int[] { 31981, 31972 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18571, 13838 }, // Venomous Storace
				{ 18573, 13839 } // Kel Bilette
		};
		startLevel = 58;
		registerNPCs();
	}
}
