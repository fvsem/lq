package quests._121_PavelTheGiants;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _121_PavelTheGiants extends Quest implements ScriptFile
{
	// NPCs
	private static int NEWYEAR = 31961;
	private static int YUMI = 32041;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _121_PavelTheGiants()
	{
		super(121, -1);

		addStartNpc(NEWYEAR);
		addTalkId(NEWYEAR);
		addTalkId(YUMI);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equals("32041-2.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.addExpAndSp(10000, 0);
			st.unset("cond");
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");

		if(cond == 0)
			htmltext = "completed";
		else if(id == CREATED && npcId == NEWYEAR)
		{
			if(st.getPlayer().getLevel() >= 46)
			{
				htmltext = "31961-1.htm";
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
			else
			{
				htmltext = "31961-1a.htm";
				st.exitCurrentQuest(false);
			}
		}
		else if(id == STARTED)
			if(npcId == YUMI && cond == 1)
				htmltext = "32041-1.htm";
			else
				htmltext = "31961-2.htm";
		return htmltext;
	}
}
