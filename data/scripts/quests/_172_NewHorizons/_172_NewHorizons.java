package quests._172_NewHorizons;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.Race;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _172_NewHorizons extends Quest implements ScriptFile
{
	// NPC
	private static final int Zenya = 32140;
	private static final int Ragara = 32163;
	// Items
	private static final int ScrollOfEscapeGiran = 7126;
	private static final int MarkOfTraveler = 7570;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _172_NewHorizons()
	{
		super(172, -1);

		addStartNpc(Zenya);

		addTalkId(Zenya);
		addTalkId(Ragara);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32140-02.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32163-02.htm"))
		{
			st.giveItems(ScrollOfEscapeGiran, 1);
			st.giveItems(MarkOfTraveler, 1);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == Zenya)
		{
			if(cond == 0)
				if(st.getPlayer().getRace() == Race.kamael && st.getPlayer().getLevel() >= 3)
					htmltext = "32140-01.htm";
				else
				{
					htmltext = "32140-00.htm";
					st.exitCurrentQuest(true);
				}
		}
		else if(npcId == Ragara)
			if(cond == 1)
				htmltext = "32163-01.htm";
		return htmltext;
	}
}
