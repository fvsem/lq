package quests._408_PathToElvenwizard;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _408_PathToElvenwizard extends Quest implements ScriptFile
{
	// npc
	private static final int GREENIS = 30157;
	private static final int THALIA = 30371;
	private static final int ROSELLA = 30414;
	private static final int NORTHWIND = 30423;
	// mobs
	private static final int DRYAD_ELDER = 20019;
	private static final int PINCER_SPIDER = 20466;
	private static final int SUKAR_WERERAT_LEADER = 20047;
	// items
	private static final int ROGELLIAS_LETTER_ID = 1218;
	private static final int RED_DOWN_ID = 1219;
	private static final int MAGICAL_POWERS_RUBY_ID = 1220;
	private static final int PURE_AQUAMARINE_ID = 1221;
	private static final int APPETIZING_APPLE_ID = 1222;
	private static final int GOLD_LEAVES_ID = 1223;
	private static final int IMMORTAL_LOVE_ID = 1224;
	private static final int AMETHYST_ID = 1225;
	private static final int NOBILITY_AMETHYST_ID = 1226;
	private static final int FERTILITY_PERIDOT_ID = 1229;
	private static final int ETERNITY_DIAMOND_ID = 1230;
	private static final int CHARM_OF_GRAIN_ID = 1272;
	private static final int SAP_OF_WORLD_TREE_ID = 1273;
	private static final int LUCKY_POTPOURI_ID = 1274;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 408: Path to Elven Wizard");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _408_PathToElvenwizard()
	{
		super(408, -1);

		addStartNpc(ROSELLA);

		addTalkId(GREENIS);
		addTalkId(THALIA);
		addTalkId(ROSELLA);
		addTalkId(NORTHWIND);

		addKillId(DRYAD_ELDER);
		addKillId(PINCER_SPIDER);
		addKillId(SUKAR_WERERAT_LEADER);

		addQuestItem(new int[] {
				ROGELLIAS_LETTER_ID,
				FERTILITY_PERIDOT_ID,
				IMMORTAL_LOVE_ID,
				APPETIZING_APPLE_ID,
				CHARM_OF_GRAIN_ID,
				MAGICAL_POWERS_RUBY_ID,
				SAP_OF_WORLD_TREE_ID,
				PURE_AQUAMARINE_ID,
				LUCKY_POTPOURI_ID,
				NOBILITY_AMETHYST_ID,
				GOLD_LEAVES_ID,
				RED_DOWN_ID,
				AMETHYST_ID });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
		{
			if(st.getPlayer().getClassId().getId() != 0x19)
			{
				if(st.getPlayer().getClassId().getId() == 0x1a)
					htmltext = "30414-02a.htm";
				else
					htmltext = "30414-03.htm";
			}
			else if(st.getPlayer().getLevel() < 18)
				htmltext = "30414-04.htm";
			else if(st.getQuestItemsCount(ETERNITY_DIAMOND_ID) > 0)
				htmltext = "30414-05.htm";
			else
			{
				st.setState(STARTED);
				st.set("cond", "1");
				st.playSound(SOUND_ACCEPT);
				st.giveItems(FERTILITY_PERIDOT_ID, 1);
				htmltext = "30414-06.htm";
			}
		}
		else if(event.equalsIgnoreCase("408_1"))
		{
			if(st.getQuestItemsCount(MAGICAL_POWERS_RUBY_ID) > 0)
				htmltext = "30414-10.htm";
			else if(st.getQuestItemsCount(MAGICAL_POWERS_RUBY_ID) < 1 && st.getQuestItemsCount(FERTILITY_PERIDOT_ID) > 0)
			{
				st.giveItems(ROGELLIAS_LETTER_ID, 1);
				htmltext = "30414-07.htm";
			}
		}
		else if(event.equalsIgnoreCase("408_4"))
		{
			if(st.getQuestItemsCount(ROGELLIAS_LETTER_ID) > 0)
			{
				st.takeItems(ROGELLIAS_LETTER_ID, -1);
				st.giveItems(CHARM_OF_GRAIN_ID, 1);
				htmltext = "30157-02.htm";
			}
		}
		else if(event.equalsIgnoreCase("408_2"))
		{
			if(st.getQuestItemsCount(PURE_AQUAMARINE_ID) > 0)
				htmltext = "30414-13.htm";
			else if(st.getQuestItemsCount(PURE_AQUAMARINE_ID) < 1 && st.getQuestItemsCount(FERTILITY_PERIDOT_ID) > 0)
			{
				st.giveItems(APPETIZING_APPLE_ID, 1);
				htmltext = "30414-14.htm";
			}
		}
		else if(event.equalsIgnoreCase("408_5"))
		{
			if(st.getQuestItemsCount(APPETIZING_APPLE_ID) > 0)
			{
				st.takeItems(APPETIZING_APPLE_ID, -1);
				st.giveItems(SAP_OF_WORLD_TREE_ID, 1);
				htmltext = "30371-02.htm";
			}
		}
		else if(event.equalsIgnoreCase("408_3"))
			if(st.getQuestItemsCount(NOBILITY_AMETHYST_ID) > 0)
				htmltext = "30414-17.htm";
			else if(st.getQuestItemsCount(NOBILITY_AMETHYST_ID) < 1 && st.getQuestItemsCount(FERTILITY_PERIDOT_ID) > 0)
			{
				st.giveItems(IMMORTAL_LOVE_ID, 1);
				htmltext = "30414-18.htm";
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == ROSELLA)
		{
			if(cond < 1)
				htmltext = "30414-01.htm";
			else if(st.getQuestItemsCount(CHARM_OF_GRAIN_ID) > 0)
			{
				if(st.getQuestItemsCount(RED_DOWN_ID) < 5)
					htmltext = "30414-09.htm";
				else if(st.getQuestItemsCount(RED_DOWN_ID) > 4)
					htmltext = "30414-25.htm";
				else if(st.getQuestItemsCount(GOLD_LEAVES_ID) > 4)
					htmltext = "30414-26.htm";
			}
			else if(st.getQuestItemsCount(APPETIZING_APPLE_ID) > 0)
				htmltext = "30414-15.htm";
			else if(st.getQuestItemsCount(IMMORTAL_LOVE_ID) > 0)
				htmltext = "30414-19.htm";
			else if(st.getQuestItemsCount(SAP_OF_WORLD_TREE_ID) > 0 && st.getQuestItemsCount(GOLD_LEAVES_ID) < 5)
				htmltext = "30414-16.htm";
			else if(st.getQuestItemsCount(LUCKY_POTPOURI_ID) > 0)
			{
				if(st.getQuestItemsCount(AMETHYST_ID) < 2)
					htmltext = "30414-20.htm";
				else
					htmltext = "30414-27.htm";
			}
			else if(st.getQuestItemsCount(ROGELLIAS_LETTER_ID) > 0)
				htmltext = "30414-08.htm";
			else if(st.getQuestItemsCount(ROGELLIAS_LETTER_ID) < 1 && st.getQuestItemsCount(APPETIZING_APPLE_ID) < 1 && st.getQuestItemsCount(IMMORTAL_LOVE_ID) < 1 && st.getQuestItemsCount(CHARM_OF_GRAIN_ID) < 1 && st.getQuestItemsCount(SAP_OF_WORLD_TREE_ID) < 1 && st.getQuestItemsCount(LUCKY_POTPOURI_ID) < 1 && st.getQuestItemsCount(FERTILITY_PERIDOT_ID) > 0)
				if(st.getQuestItemsCount(MAGICAL_POWERS_RUBY_ID) < 1 | st.getQuestItemsCount(NOBILITY_AMETHYST_ID) < 1 | st.getQuestItemsCount(PURE_AQUAMARINE_ID) < 1)
					htmltext = "30414-11.htm";
				else if(st.getQuestItemsCount(MAGICAL_POWERS_RUBY_ID) > 0 && st.getQuestItemsCount(NOBILITY_AMETHYST_ID) > 0 && st.getQuestItemsCount(PURE_AQUAMARINE_ID) > 0)
				{
					st.takeItems(MAGICAL_POWERS_RUBY_ID, -1);
					st.takeItems(PURE_AQUAMARINE_ID, st.getQuestItemsCount(PURE_AQUAMARINE_ID));
					st.takeItems(NOBILITY_AMETHYST_ID, st.getQuestItemsCount(NOBILITY_AMETHYST_ID));
					st.takeItems(FERTILITY_PERIDOT_ID, st.getQuestItemsCount(FERTILITY_PERIDOT_ID));
					htmltext = "30414-24.htm";
					st.playSound(SOUND_FINISH);
					if(!st.getPlayer().getVarB("prof1") && st.getPlayer().getClassId().getLevel() == 1)
					{
						st.getPlayer().setVar("prof1", "1");
						st.addExpAndSp(228064, 16455, true);
                                                st.giveItems(57, 163800, true);
					}
					st.exitCurrentQuest(true);
					st.giveItems(ETERNITY_DIAMOND_ID, 1);
				}
		}
		else if(npcId == GREENIS && cond > 0)
		{
			if(st.getQuestItemsCount(ROGELLIAS_LETTER_ID) > 0)
				htmltext = "30157-01.htm";
			else if(st.getQuestItemsCount(CHARM_OF_GRAIN_ID) > 0)
				if(st.getQuestItemsCount(RED_DOWN_ID) < 5)
					htmltext = "30157-03.htm";
				else
				{
					st.takeItems(RED_DOWN_ID, -1);
					st.takeItems(CHARM_OF_GRAIN_ID, -1);
					st.giveItems(MAGICAL_POWERS_RUBY_ID, 1);
					htmltext = "30157-04.htm";
				}
		}
		else if(npcId == THALIA && cond > 0)
		{
			if(st.getQuestItemsCount(APPETIZING_APPLE_ID) > 0)
				htmltext = "30371-01.htm";
			else if(st.getQuestItemsCount(SAP_OF_WORLD_TREE_ID) > 0)
				if(st.getQuestItemsCount(GOLD_LEAVES_ID) < 5)
					htmltext = "30371-03.htm";
				else
				{
					st.takeItems(GOLD_LEAVES_ID, -1);
					st.takeItems(SAP_OF_WORLD_TREE_ID, -1);
					st.giveItems(PURE_AQUAMARINE_ID, 1);
					htmltext = "30371-04.htm";
				}
		}
		else if(npcId == NORTHWIND && cond > 0)
			if(st.getQuestItemsCount(IMMORTAL_LOVE_ID) > 0)
			{
				st.takeItems(IMMORTAL_LOVE_ID, -1);
				st.giveItems(LUCKY_POTPOURI_ID, 1);
				htmltext = "30423-01.htm";
			}
			else if(st.getQuestItemsCount(LUCKY_POTPOURI_ID) > 0)
				if(st.getQuestItemsCount(AMETHYST_ID) < 2)
					htmltext = "30423-02.htm";
				else
				{
					st.takeItems(AMETHYST_ID, -1);
					st.takeItems(LUCKY_POTPOURI_ID, -1);
					st.giveItems(NOBILITY_AMETHYST_ID, 1);
					htmltext = "30423-03.htm";
				}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == PINCER_SPIDER)
		{
			if(cond > 0 && st.getQuestItemsCount(CHARM_OF_GRAIN_ID) > 0 && st.getQuestItemsCount(RED_DOWN_ID) < 5 && Rnd.chance(70))
			{
				st.giveItems(RED_DOWN_ID, 1);
				if(st.getQuestItemsCount(RED_DOWN_ID) < 5)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == DRYAD_ELDER)
		{
			if(cond > 0 && st.getQuestItemsCount(SAP_OF_WORLD_TREE_ID) > 0 && st.getQuestItemsCount(GOLD_LEAVES_ID) < 5 && Rnd.chance(40))
			{
				st.giveItems(GOLD_LEAVES_ID, 1);
				if(st.getQuestItemsCount(GOLD_LEAVES_ID) < 5)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		}
		else if(npcId == SUKAR_WERERAT_LEADER)
			if(cond > 0 && st.getQuestItemsCount(LUCKY_POTPOURI_ID) > 0 && st.getQuestItemsCount(AMETHYST_ID) < 2 && Rnd.chance(40))
			{
				st.giveItems(AMETHYST_ID, 1);
				if(st.getQuestItemsCount(AMETHYST_ID) < 2)
					st.playSound(SOUND_ITEMGET);
				else
					st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
