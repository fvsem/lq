package quests._10276_MutatedKaneus_Gludio;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Gludio</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10276_MutatedKaneus_Gludio extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10276_MutatedKaneus_Gludio()
	{
		super(10276, "_10276_MutatedKaneus_Gludio", "Mutated Kaneus - Gludio");

		NPC = new int[] { 30332, 30344 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18554, 13830 }, // Tomlan Kamos
				{ 18555, 13831 } // Ol Ariosh
		};
		startLevel = 18;
		registerNPCs();
	}
}
