package quests._109_InSearchOfTheNest;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _109_InSearchOfTheNest extends Quest implements ScriptFile
{
	// NPC
	private static final int PIERCE = 31553;
	private static final int CORPSE = 32015;
	private static final int KAHMAN = 31554;
	// QUEST ITEMS
	private static final int MEMO = 8083;
	// Items
	private static final int GOLDEN_BADGE_RECRUIT = 7246;
	private static final int GOLDEN_BADGE_SOLDIER = 7247;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _109_InSearchOfTheNest()
	{
		super(109, -1);
		addStartNpc(PIERCE);
		addTalkId(CORPSE);
		addTalkId(KAHMAN);

		addQuestItem(MEMO);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getInt("cond");
		if(event.equalsIgnoreCase("Memo") && cond == 1)
		{
			st.giveItems(MEMO, 1);
			st.set("cond", "2");
			st.playSound(SOUND_ITEMGET);
			htmltext = "You've find something...";
		}
		else if(event.equalsIgnoreCase("31553-02.htm") && cond == 2)
		{
			st.takeItems(MEMO, -1);
			st.set("cond", "3");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int id = st.getState();
		if(id == COMPLETED)
			return "completed";
		int cond = st.getInt("cond");
		String htmltext = "noquest";

		if(id == CREATED)
		{
			if(st.getPlayer().getLevel() >= 66 && npcId == PIERCE && (st.getQuestItemsCount(GOLDEN_BADGE_RECRUIT) > 0 || st.getQuestItemsCount(GOLDEN_BADGE_SOLDIER) > 0))
			{
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
				st.set("cond", "1");
				htmltext = "31553-03.htm";
			}
			else
			{
				htmltext = "31553-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(id == STARTED)
			if(npcId == CORPSE)
			{
				if(cond == 1)
					htmltext = "32015-01.htm";
				else if(cond == 2)
					htmltext = "32015-02.htm";
			}
			else if(npcId == PIERCE)
			{
				if(cond == 1)
					htmltext = "31553-04.htm";
				else if(cond == 2)
					htmltext = "31553-01.htm";
				else if(cond == 3)
					htmltext = "31553-05.htm";
			}
			else if(npcId == KAHMAN && cond == 3)
			{
				htmltext = "31554-01.htm";
				st.giveItems(57, 5168);
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		return null;
	}
}
