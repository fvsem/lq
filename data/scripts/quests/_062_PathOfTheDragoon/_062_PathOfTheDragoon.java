package quests._062_PathOfTheDragoon;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.base.ClassId;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _062_PathOfTheDragoon extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int Shubain = 32194;
	private static final int Gwain = 32197;

	private static final int FelimLizardmanWarrior = 20014;
	private static final int VenomousSpider = 20038;
	private static final int TumranBugbear = 20062;

	private static final int FelimHead = 9749;
	private static final int VenomousSpiderLeg = 9750;
	private static final int TumranBugbearHeart = 9751;
	private static final int ShubainsRecommendation = 9752;
	private static final int GwainsRecommendation = 9753;

	public _062_PathOfTheDragoon()
	{
		super(62, -1);

		addStartNpc(Gwain);
		addTalkId(Gwain);
		addTalkId(Shubain);
		addKillId(FelimLizardmanWarrior);
		addKillId(VenomousSpider);
		addKillId(TumranBugbear);
		addQuestItem(FelimHead);
		addQuestItem(VenomousSpiderLeg);
		addQuestItem(ShubainsRecommendation);
		addQuestItem(TumranBugbearHeart);
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		final String htmltext = event;
		if(event.equals("32197-02.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("32194-02.htm"))
			st.set("cond", "2");
		return htmltext;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		String htmltext = "noquest";
		final int npcId = npc.getNpcId();
		final int id = st.getState();
		final int cond = st.getInt("cond");
		if(npcId == Gwain)
		{
			if(id == CREATED)
			{
				if(st.getPlayer().getClassId() != ClassId.maleSoldier)
				{
					htmltext = "for male kamael warriors only";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 18)
				{
					htmltext = "no level 18";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "32197-01.htm";
			}
			else if(cond == 4)
			{
				st.takeItems(ShubainsRecommendation, -1);
				st.set("cond", "5");
				htmltext = "32197-03.htm";
			}
			else if(cond == 5 && st.getQuestItemsCount(TumranBugbearHeart) > 0)
			{
				st.takeItems(TumranBugbearHeart, -1);
				if(st.getPlayer().getClassId().getLevel() == 1)
				{
					st.giveItems(GwainsRecommendation, 1);
					if(!st.getPlayer().getVarB("prof1"))
					{
						st.getPlayer().setVar("prof1", "1");
						st.addExpAndSp(160267, 11023, true);
                                                st.giveItems(57, 163800, true);
					}
				}

				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
				htmltext = "32197-04.htm";
			}
		}
		else if(npcId == Shubain)
			if(cond == 1)
				htmltext = "32194-01.htm";
			else if(cond == 2 && st.getQuestItemsCount(FelimHead) >= 5)
			{
				st.takeItems(FelimHead, -1);
				st.set("cond", "3");
				htmltext = "32194-04.htm";
			}
			else if(cond == 3 && st.getQuestItemsCount(VenomousSpiderLeg) >= 10)
			{
				st.takeItems(VenomousSpiderLeg, -1);
				st.giveItems(ShubainsRecommendation, 1);
				st.set("cond", "4");
				htmltext = "32194-06.htm";
			}
		return htmltext;
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		final int id = npc.getNpcId();
		final int cond = st.getInt("cond");
		if(id == FelimLizardmanWarrior && cond == 2)
		{
			final long count = st.getQuestItemsCount(FelimHead);
			if(count < 5)
			{
				st.giveItems(FelimHead, 1);
				if(count == 4)
					st.playSound(SOUND_MIDDLE);
				else
					st.playSound(SOUND_ITEMGET);
			}
		}
		if(id == VenomousSpider && cond == 3)
		{
			final long count = st.getQuestItemsCount(VenomousSpiderLeg);
			if(count < 10)
			{
				st.giveItems(VenomousSpiderLeg, 1);
				if(count == 9)
					st.playSound(SOUND_MIDDLE);
				else
					st.playSound(SOUND_ITEMGET);
			}
		}
		if(id == TumranBugbear && cond == 5)
			if(st.getQuestItemsCount(TumranBugbearHeart) == 0)
			{
				st.giveItems(TumranBugbearHeart, 1);
				st.playSound(SOUND_MIDDLE);
			}
		return null;
	}
}
