package quests._407_PathToElvenScout;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _407_PathToElvenScout extends Quest implements ScriptFile
{
	// npc
	private static final int REISA = 30328;
	private static final int MORETTI = 30337;
	private static final int PIPPEN = 30426;
	// mobs
	private static final int OL_MAHUM_SENTRY = 27031;
	private static final int OL_MAHUM_PATROL = 20053;
	// items
	private static final int REORIA_LETTER2_ID = 1207;
	private static final int PRIGUNS_TEAR_LETTER1_ID = 1208;
	private static final int PRIGUNS_TEAR_LETTER2_ID = 1209;
	private static final int PRIGUNS_TEAR_LETTER3_ID = 1210;
	private static final int PRIGUNS_TEAR_LETTER4_ID = 1211;
	private static final int MORETTIS_HERB_ID = 1212;
	private static final int MORETTIS_LETTER_ID = 1214;
	private static final int PRIGUNS_LETTER_ID = 1215;
	private static final int REORIA_RECOMMENDATION_ID = 1217;
	private static final int RUSTED_KEY_ID = 1293;
	private static final int HONORARY_GUARD_ID = 1216;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _407_PathToElvenScout()
	{
		super(407, -1);

		addStartNpc(REISA);
		addTalkId(REISA);
		addTalkId(REISA);
		addTalkId(MORETTI);
		addTalkId(PIPPEN);
		addTalkId(REISA);

		addKillId(OL_MAHUM_SENTRY);
		addKillId(OL_MAHUM_PATROL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
		{
			if(st.getPlayer().getClassId().getId() == 0x12)
			{
				if(st.getPlayer().getLevel() >= 18)
				{
					if(st.getQuestItemsCount(REORIA_RECOMMENDATION_ID) > 0)
					{
						htmltext = "30328-04.htm";
						st.exitCurrentQuest(true);
					}
					else
					{
						htmltext = "30328-05.htm";
						st.giveItems(REORIA_LETTER2_ID, 1);
						st.set("cond", "1");
						st.setState(STARTED);
						st.playSound(SOUND_ACCEPT);
					}
				}
				else
				{
					htmltext = "30328-03.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(st.getPlayer().getClassId().getId() == 0x16)
			{
				htmltext = "30328-02a.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30328-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(event.equalsIgnoreCase("30337_1"))
		{
			st.takeItems(REORIA_LETTER2_ID, 1);
			st.set("cond", "2");
			htmltext = "30337-03.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == REISA)
		{
			if(cond == 0)
				htmltext = "30328-01.htm";
			else if(cond == 1)
				htmltext = "30328-06.htm";
			else if(cond > 1 && st.getQuestItemsCount(HONORARY_GUARD_ID) == 0)
				htmltext = "30328-08.htm";
			else if(cond == 8 && st.getQuestItemsCount(HONORARY_GUARD_ID) == 1)
			{
				htmltext = "30328-07.htm";
				st.takeItems(HONORARY_GUARD_ID, 1);
				st.giveItems(REORIA_RECOMMENDATION_ID, 1);
				st.playSound(SOUND_FINISH);
				if(!st.getPlayer().getVarB("prof1") && st.getPlayer().getClassId().getLevel() == 1)
				{
					st.getPlayer().setVar("prof1", "1");
					st.addExpAndSp(228064, 16455, true);
                                        st.giveItems(57, 163800, true);
				}
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == MORETTI)
		{
			if(cond == 1)
				htmltext = "30337-01.htm";
			else if(cond == 2)
				htmltext = "30337-04.htm";
			else if(cond == 3)
			{
				if(st.getQuestItemsCount(PRIGUNS_TEAR_LETTER1_ID) == 1 && st.getQuestItemsCount(PRIGUNS_TEAR_LETTER2_ID) == 1 && st.getQuestItemsCount(PRIGUNS_TEAR_LETTER3_ID) == 1 && st.getQuestItemsCount(PRIGUNS_TEAR_LETTER4_ID) == 1)
				{
					htmltext = "30337-06.htm";
					st.takeItems(PRIGUNS_TEAR_LETTER1_ID, 1);
					st.takeItems(PRIGUNS_TEAR_LETTER2_ID, 1);
					st.takeItems(PRIGUNS_TEAR_LETTER3_ID, 1);
					st.takeItems(PRIGUNS_TEAR_LETTER4_ID, 1);
					st.giveItems(MORETTIS_HERB_ID, 1);
					st.giveItems(MORETTIS_LETTER_ID, 1);
					st.set("cond", "4");
				}
				else
					htmltext = "30337-05.htm";
			}
			else if(cond == 7 && st.getQuestItemsCount(PRIGUNS_LETTER_ID) == 1)
			{
				htmltext = "30337-07.htm";
				st.takeItems(PRIGUNS_LETTER_ID, 1);
				st.giveItems(HONORARY_GUARD_ID, 1);
				st.set("cond", "8");
			}
			else if(cond > 8)
				htmltext = "30337-08.htm";
		}
		else if(npcId == PIPPEN)
			if(cond == 4)
			{
				htmltext = "30426-01.htm";
				st.set("cond", "5");
			}
			else if(cond == 5)
				htmltext = "30426-01.htm";
			else if(cond == 6 && st.getQuestItemsCount(RUSTED_KEY_ID) == 1 && st.getQuestItemsCount(MORETTIS_HERB_ID) == 1 && st.getQuestItemsCount(MORETTIS_LETTER_ID) == 1)
			{
				htmltext = "30426-02.htm";
				st.takeItems(RUSTED_KEY_ID, 1);
				st.takeItems(MORETTIS_HERB_ID, 1);
				st.takeItems(MORETTIS_LETTER_ID, 1);
				st.giveItems(PRIGUNS_LETTER_ID, 1);
				st.set("cond", "7");
			}
			else if(cond == 7)
				htmltext = "30426-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == OL_MAHUM_PATROL && cond == 2)
		{
			if(st.getQuestItemsCount(PRIGUNS_TEAR_LETTER1_ID) == 0)
			{
				st.giveItems(PRIGUNS_TEAR_LETTER1_ID, 1);
				st.playSound(SOUND_ITEMGET);
				return null;
			}
			if(st.getQuestItemsCount(PRIGUNS_TEAR_LETTER2_ID) == 0)
			{
				st.giveItems(PRIGUNS_TEAR_LETTER2_ID, 1);
				st.playSound(SOUND_ITEMGET);
				return null;
			}
			if(st.getQuestItemsCount(PRIGUNS_TEAR_LETTER3_ID) == 0)
			{
				st.giveItems(PRIGUNS_TEAR_LETTER3_ID, 1);
				st.playSound(SOUND_ITEMGET);
				return null;
			}
			if(st.getQuestItemsCount(PRIGUNS_TEAR_LETTER4_ID) == 0)
			{
				st.giveItems(PRIGUNS_TEAR_LETTER4_ID, 1);
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "3");
				return null;
			}
		}
		else if(npcId == OL_MAHUM_SENTRY && cond == 5 && Rnd.chance(60))
		{
			st.giveItems(RUSTED_KEY_ID, 1);
			st.playSound(SOUND_MIDDLE);
			st.set("cond", "6");
		}
		return null;
	}
}
