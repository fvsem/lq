package quests._294_CovertBusiness;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _294_CovertBusiness extends Quest implements ScriptFile
{
	public static int BatFang = 1491;
	public static int RingOfRaccoon = 1508;
	public static int Adena = 57;

	public static int BarbedBat = 20370;
	public static int BladeBat = 20480;

	public static int Keef = 30534;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 294: Covert Business");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _294_CovertBusiness()
	{
		super(294, -1);

		addStartNpc(Keef);
		addTalkId(Keef);

		addKillId(BarbedBat);
		addKillId(BladeBat);

		addQuestItem(BatFang);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30534-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();

		if(id == CREATED)
		{
			if(st.getPlayer().getRace().ordinal() != 4)
			{
				htmltext = "30534-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() >= 10)
			{
				htmltext = "30534-02.htm";
				return htmltext;
			}
			else
			{
				htmltext = "30534-01.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(st.getQuestItemsCount(BatFang) < 100)
			htmltext = "30534-04.htm";
		else if(st.getQuestItemsCount(RingOfRaccoon) < 1)
		{
			htmltext = "30534-05.htm";
			st.addExpAndSp(0, 60);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			st.giveItems(RingOfRaccoon, 1);
		}
		else
		{
			htmltext = "30534-06.htm";
			st.giveItems(Adena, 2400);
			st.addExpAndSp(0, 60);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") != 1)
			return null;
		int npcId = npc.getNpcId();
		if(npcId == BarbedBat)
		{
			long BatFangCount = st.getQuestItemsCount(BatFang);
			if(BatFangCount < 100)
			{
				int n = Rnd.get(10);
				if(n > 5)
				{
					if(BatFangCount == 99)
					{
						st.giveItems(BatFang, 1);
						st.playSound(SOUND_MIDDLE);
					}
					else
					{
						st.giveItems(BatFang, 1);
						st.playSound(SOUND_ITEMGET);
					}
				}
				else if(n > 2)
				{
					if(BatFangCount >= 98)
					{
						st.giveItems(BatFang, 100 - BatFangCount);
						st.playSound(SOUND_MIDDLE);
					}
					else
					{
						st.giveItems(BatFang, 2);
						st.playSound(SOUND_ITEMGET);
					}
				}
				else if(BatFangCount >= 97)
				{
					st.giveItems(BatFang, 100 - BatFangCount);
					st.playSound(SOUND_MIDDLE);
				}
				else
				{
					st.giveItems(BatFang, 3);
					st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == BladeBat)
		{
			long BatFangCount = st.getQuestItemsCount(BatFang);
			if(BatFangCount < 100)
			{
				int n = Rnd.get(10);
				if(n > 6)
				{
					if(BatFangCount == 99)
					{
						st.giveItems(BatFang, 1);
						st.playSound(SOUND_MIDDLE);
					}
					else
					{
						st.giveItems(BatFang, 1);
						st.playSound(SOUND_ITEMGET);
					}
				}
				else if(n > 3)
				{
					if(BatFangCount >= 98)
					{
						st.giveItems(BatFang, 100 - BatFangCount);
						st.playSound(SOUND_MIDDLE);
					}
					else
					{
						st.giveItems(BatFang, 2);
						st.playSound(SOUND_ITEMGET);
					}
				}
				else if(n > 1)
				{
					if(BatFangCount >= 97)
					{
						st.giveItems(BatFang, 100 - BatFangCount);
						st.playSound(SOUND_MIDDLE);
					}
					else
					{
						st.giveItems(BatFang, 3);
						st.playSound(SOUND_ITEMGET);
					}
				}
				else if(BatFangCount >= 96)
				{
					st.giveItems(BatFang, 100 - BatFangCount);
					st.playSound(SOUND_MIDDLE);
				}
				else
				{
					st.giveItems(BatFang, 4);
					st.playSound(SOUND_ITEMGET);
				}
			}
		}
		return null;
	}
}
