package quests._296_SilkOfTarantula;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _296_SilkOfTarantula extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 296: Silk Of Tarantula");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int TARANTULA_SPIDER_SILK = 1493;
	private static final int TARANTULA_SPINNERETTE = 1494;
	private static final int RING_OF_RACCOON = 1508;
	private static final int RING_OF_FIREFLY = 1509;
	private static final int ADENA = 57;

	public _296_SilkOfTarantula()
	{
		super(296, -1);
		addStartNpc(30519);
		addTalkId(30548);

		addKillId(20394);
		addKillId(20403);
		addKillId(20508);

		addQuestItem(TARANTULA_SPIDER_SILK);
		addQuestItem(TARANTULA_SPINNERETTE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30519-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("quit"))
		{
			htmltext = "30519-06.htm";
			st.takeItems(TARANTULA_SPINNERETTE, -1);
			st.exitCurrentQuest(true);
			st.playSound(SOUND_FINISH);
		}
		else if(event.equalsIgnoreCase("exchange"))
			if(st.getQuestItemsCount(TARANTULA_SPINNERETTE) >= 1)
			{
				htmltext = "30548-03.htm";
				st.giveItems(TARANTULA_SPIDER_SILK, 15 + Rnd.get(9));
				st.takeItems(TARANTULA_SPINNERETTE, -1);
			}
			else
				htmltext = "30548-02.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");

		if(npcId == 30519)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 15)
				{
					if(st.getQuestItemsCount(RING_OF_RACCOON) > 0 || st.getQuestItemsCount(RING_OF_FIREFLY) > 0)
						htmltext = "30519-02.htm";
					else
					{
						htmltext = "30519-08.htm";
						return htmltext;
					}
				}
				else
				{
					htmltext = "30519-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				if(st.getQuestItemsCount(TARANTULA_SPIDER_SILK) < 1)
					htmltext = "30519-04.htm";
				else if(st.getQuestItemsCount(TARANTULA_SPIDER_SILK) >= 1)
				{
					htmltext = "30519-05.htm";
					st.giveItems(ADENA, st.getQuestItemsCount(TARANTULA_SPIDER_SILK) * 23);
					st.takeItems(TARANTULA_SPIDER_SILK, -1);
				}
		}
		else if(npcId == 30548 && cond == 1)
			htmltext = "30548-01.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1)
			if(Rnd.chance(50))
				st.rollAndGive(TARANTULA_SPINNERETTE, 1, 45);
			else
				st.rollAndGive(TARANTULA_SPIDER_SILK, 1, 45);
		return null;
	}
}
