package quests._247_PossessorOfaPreciousSoul4;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _247_PossessorOfaPreciousSoul4 extends Quest implements ScriptFile
{
	private static int CARADINE = 31740;
	private static int LADY_OF_LAKE = 31745;

	private static int CARADINE_LETTER_LAST = 7679;
	private static int NOBLESS_TIARA = 7694;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _247_PossessorOfaPreciousSoul4()
	{
		super(247, -1);

		addStartNpc(CARADINE);
		addTalkId(CARADINE);
		addTalkId(LADY_OF_LAKE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getInt("cond");
		if(cond == 0 && event.equals("31740-3.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(cond == 1)
		{
			if(event.equals("31740-4.htm"))
				return htmltext;
			else if(event.equals("31740-5.htm"))
			{
				st.set("cond", "2");
				st.takeItems(CARADINE_LETTER_LAST, 1);
				st.getPlayer().teleToLocation(143230, 44030, -3030);
				return htmltext;
			}
		}
		else if(cond == 2)
			if(event.equals("31740-6.htm"))
				return htmltext;
			else if(event.equals("31740-5.htm"))
			{
				st.getPlayer().teleToLocation(143230, 44030, -3030);
				return htmltext;
			}
			else if(event.equals("31745-2.htm"))
				return htmltext;
			else if(event.equals("31745-3.htm"))
				return htmltext;
			else if(event.equals("31745-4.htm"))
				return htmltext;
			else if(event.equals("31745-5.htm"))
				if(st.getPlayer().getLevel() >= 75)
				{
					st.giveItems(NOBLESS_TIARA, 1);
					st.addExpAndSp(93836, 0);
					st.playSound(SOUND_FINISH);
					st.unset("cond");
					st.exitCurrentQuest(false);
					Olympiad.addNoble(st.getPlayer());
					st.getPlayer().setNoble(true, 0);
					st.getPlayer().updatePledgeClass();
					st.getPlayer().broadcastUserInfo(true);
				}
				else
					htmltext = "31745-6.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(npcId == CARADINE && st.getPlayer().isSubClassActive())
		{
			if(id == CREATED && st.getQuestItemsCount(CARADINE_LETTER_LAST) == 1)
			{
				st.set("cond", "0");
				if(st.getPlayer().getLevel() < 75)
				{
					htmltext = "31740-2.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31740-1.htm";
			}
			else if(cond == 1)
				htmltext = "31740-3.htm";
			else if(cond == 2)
				htmltext = "31740-6.htm";
		}
		else if(npcId == LADY_OF_LAKE && cond == 2 && st.getPlayer().isSubClassActive())
			if(st.getPlayer().getLevel() >= 75)
				htmltext = "31745-1.htm";
			else
				htmltext = "31745-6.htm";
		return htmltext;
	}
}
