package quests._330_AdeptOfTaste;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _330_AdeptOfTaste extends Quest implements ScriptFile
{
	// NPC
	private static final int GLYVKA = 30067;
	private static final int JONAS = 30469;
	private static final int SONIA = 30062;
	private static final int ROLLANT = 30069;
	private static final int JACOB = 30073;
	private static final int PANO = 30078;
	private static final int MIRIEN = 30461;
	// Items
	private static final int INGREDIENT_LIST_ID = 1420;
	private static final int SONIAS_BOTANYBOOK_ID = 1421;
	private static final int RED_MANDRAGORA_ROOT_ID = 1422;
	private static final int WHITE_MANDRAGORA_ROOT_ID = 1423;
	private static final int RED_MANDRAGORA_SAP_ID = 1424;
	private static final int WHITE_MANDRAGORA_SAP_ID = 1425;
	private static final int JAYCUBS_INSECTBOOK_ID = 1426;
	private static final int NECTAR_ID = 1427;
	private static final int ROYAL_JELLY_ID = 1428;
	private static final int HONEY_ID = 1429;
	private static final int GOLDEN_HONEY_ID = 1430;
	private static final int PANOS_CONTRACT_ID = 1431;
	private static final int HOBGOBLIN_AMULET_ID = 1432;
	private static final int DIONIAN_POTATO_ID = 1433;
	private static final int GLYVKAS_BOTANYBOOK_ID = 1434;
	private static final int GREEN_MARSH_MOSS_ID = 1435;
	private static final int BROWN_MARSH_MOSS_ID = 1436;
	private static final int GREEN_MOSS_BUNDLE_ID = 1437;
	private static final int BROWN_MOSS_BUNDLE_ID = 1438;
	private static final int ROLANTS_CREATUREBOOK_ID = 1439;
	private static final int MONSTER_EYE_BODY_ID = 1440;
	private static final int MONSTER_EYE_MEAT_ID = 1441;
	private static final int JONAS_STEAK_DISH1_ID = 1442;
	private static final int JONAS_STEAK_DISH2_ID = 1443;
	private static final int JONAS_STEAK_DISH3_ID = 1444;
	private static final int JONAS_STEAK_DISH4_ID = 1445;
	private static final int JONAS_STEAK_DISH5_ID = 1446;
	private static final int MIRIENS_REVIEW1_ID = 1447;
	private static final int MIRIENS_REVIEW2_ID = 1448;
	private static final int MIRIENS_REVIEW3_ID = 1449;
	private static final int MIRIENS_REVIEW4_ID = 1450;
	private static final int MIRIENS_REVIEW5_ID = 1451;
	private static final int ADENA_ID = 57;
	private static final int JONAS_SALAD_RECIPE_ID = 1455;
	private static final int JONAS_SAUCE_RECIPE_ID = 1456;
	private static final int JONAS_STEAK_RECIPE_ID = 1457;
	// Mobs
	private static final int MOB1 = 20154;
	private static final int MOB2 = 20155;
	private static final int MOB3 = 20156;
	private static final int MOB4 = 20265;
	private static final int MOB5 = 20266;
	private static final int MOB6 = 20223;
	private static final int MOB7 = 20226;
	private static final int MOB8 = 20228;
	private static final int MOB9 = 20229;
	private static final int MOB10 = 20147;
	private static final int MOB11 = 20204;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 330: AdeptOfTaste");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _330_AdeptOfTaste()
	{
		super(330, -1);

		addStartNpc(JONAS);

		addTalkId(JONAS);
		addTalkId(SONIA);
		addTalkId(GLYVKA);
		addTalkId(ROLLANT);
		addTalkId(JACOB);
		addTalkId(PANO);
		addTalkId(MIRIEN);

		addKillId(new int[] { MOB1, MOB2, MOB3, MOB4, MOB5, MOB6, MOB7, MOB8, MOB9, MOB10, MOB11 });

		addQuestItem(new int[] {
				INGREDIENT_LIST_ID,
				SONIAS_BOTANYBOOK_ID,
				RED_MANDRAGORA_ROOT_ID,
				WHITE_MANDRAGORA_ROOT_ID,
				RED_MANDRAGORA_SAP_ID,
				WHITE_MANDRAGORA_SAP_ID,
				JAYCUBS_INSECTBOOK_ID,
				NECTAR_ID,
				ROYAL_JELLY_ID,
				HONEY_ID,
				GOLDEN_HONEY_ID,
				PANOS_CONTRACT_ID,
				HOBGOBLIN_AMULET_ID,
				DIONIAN_POTATO_ID,
				GLYVKAS_BOTANYBOOK_ID,
				GREEN_MARSH_MOSS_ID,
				BROWN_MARSH_MOSS_ID,
				GREEN_MOSS_BUNDLE_ID,
				BROWN_MOSS_BUNDLE_ID,
				ROLANTS_CREATUREBOOK_ID,
				MONSTER_EYE_BODY_ID,
				MONSTER_EYE_MEAT_ID,
				JONAS_STEAK_DISH1_ID,
				JONAS_STEAK_DISH2_ID,
				JONAS_STEAK_DISH3_ID,
				JONAS_STEAK_DISH4_ID,
				JONAS_STEAK_DISH5_ID,
				MIRIENS_REVIEW1_ID,
				MIRIENS_REVIEW2_ID,
				MIRIENS_REVIEW3_ID,
				MIRIENS_REVIEW4_ID,
				MIRIENS_REVIEW5_ID,
				ADENA_ID,
				JONAS_SALAD_RECIPE_ID,
				JONAS_SAUCE_RECIPE_ID,
				JONAS_STEAK_RECIPE_ID });
	}

	private long has_list(QuestState st)
	{
		return st.getQuestItemsCount(INGREDIENT_LIST_ID);
	}

	private long has_review(QuestState st)
	{
		return st.getQuestItemsCount(MIRIENS_REVIEW1_ID) + st.getQuestItemsCount(MIRIENS_REVIEW2_ID) + st.getQuestItemsCount(MIRIENS_REVIEW3_ID) + st.getQuestItemsCount(MIRIENS_REVIEW4_ID) + st.getQuestItemsCount(MIRIENS_REVIEW5_ID);
	}

	private long has_dish(QuestState st)
	{
		return st.getQuestItemsCount(JONAS_STEAK_DISH1_ID) + st.getQuestItemsCount(JONAS_STEAK_DISH2_ID) + st.getQuestItemsCount(JONAS_STEAK_DISH3_ID) + st.getQuestItemsCount(JONAS_STEAK_DISH4_ID) + st.getQuestItemsCount(JONAS_STEAK_DISH5_ID);
	}

	private long special_ingredients(QuestState st)
	{
		return st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(GOLDEN_HONEY_ID) + st.getQuestItemsCount(BROWN_MOSS_BUNDLE_ID);
	}

	private long ingredients_count(QuestState st)
	{
		return st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(HONEY_ID) + st.getQuestItemsCount(DIONIAN_POTATO_ID) + st.getQuestItemsCount(GREEN_MOSS_BUNDLE_ID) + st.getQuestItemsCount(MONSTER_EYE_MEAT_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(GOLDEN_HONEY_ID) + st.getQuestItemsCount(BROWN_MOSS_BUNDLE_ID);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("1"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			htmltext = "30469-03.htm";
			st.giveItems(INGREDIENT_LIST_ID, 1);
		}
		else if(event.equalsIgnoreCase("30062_1"))
		{
			htmltext = "30062-05.htm";
			st.takeItems(SONIAS_BOTANYBOOK_ID, 1);
			st.takeItems(RED_MANDRAGORA_ROOT_ID, -1);
			st.takeItems(WHITE_MANDRAGORA_ROOT_ID, -1);
			st.giveItems(RED_MANDRAGORA_SAP_ID, 1);
		}
		else if(event.equalsIgnoreCase("30073_1"))
		{
			htmltext = "30073-05.htm";
			st.takeItems(JAYCUBS_INSECTBOOK_ID, 1);
			st.takeItems(NECTAR_ID, -1);
			st.takeItems(ROYAL_JELLY_ID, -1);
			st.giveItems(HONEY_ID, 1);
		}
		else if(event.equalsIgnoreCase("30067_1"))
		{
			htmltext = "30067-05.htm";
			st.takeItems(GLYVKAS_BOTANYBOOK_ID, 1);
			st.takeItems(GREEN_MARSH_MOSS_ID, -1);
			st.takeItems(BROWN_MARSH_MOSS_ID, -1);
			st.giveItems(GREEN_MOSS_BUNDLE_ID, 1);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		if(id == CREATED)
			st.set("cond", "0");
		if(npcId == 30469 && cond == 0)
		{
			if(st.getPlayer().getLevel() >= 24)
			{
				htmltext = "30469-02.htm";
				return htmltext;
			}
			htmltext = "30469-01.htm";
			st.exitCurrentQuest(true);
		}
		else if(npcId == 30469 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5)
			htmltext = "30469-04.htm";
		else if(npcId == 30469 && cond > 0 && has_list(st) > 0 && ingredients_count(st) >= 5)
		{
			if(special_ingredients(st) == 0)
			{
				if(Rnd.chance(10))
				{
					htmltext = "30469-05t2.htm";
					st.giveItems(JONAS_STEAK_DISH2_ID, 1);
				}
				else
					htmltext = "30469-05t1.htm";
				st.giveItems(JONAS_STEAK_DISH1_ID, 1);
			}
			else if(special_ingredients(st) == 1)
			{
				if(Rnd.chance(10))
				{
					htmltext = "30469-05t3.htm";
					st.giveItems(JONAS_STEAK_DISH3_ID, 1);
				}
				else
					htmltext = "30469-05t2.htm";
				st.giveItems(JONAS_STEAK_DISH2_ID, 1);
			}
			else if(special_ingredients(st) == 2)
			{
				if(Rnd.chance(10))
				{
					htmltext = "30469-05t4.htm";
					st.giveItems(JONAS_STEAK_DISH4_ID, 1);
				}
				else
					htmltext = "30469-05t3.htm";
				st.giveItems(JONAS_STEAK_DISH3_ID, 1);
			}
			else if(special_ingredients(st) == 3)
			{
				if(Rnd.chance(10))
				{
					htmltext = "30469-05t5.htm";
					st.giveItems(JONAS_STEAK_DISH5_ID, 1);
					st.playSound(SOUND_JACKPOT);
				}
				else
					htmltext = "30469-05t4.htm";
				st.giveItems(JONAS_STEAK_DISH4_ID, 1);
			}
			st.takeItems(INGREDIENT_LIST_ID, 1);
			st.takeItems(RED_MANDRAGORA_SAP_ID, 1);
			st.takeItems(WHITE_MANDRAGORA_SAP_ID, 1);
			st.takeItems(HONEY_ID, 1);
			st.takeItems(GOLDEN_HONEY_ID, 1);
			st.takeItems(DIONIAN_POTATO_ID, 1);
			st.takeItems(GREEN_MOSS_BUNDLE_ID, 1);
			st.takeItems(BROWN_MOSS_BUNDLE_ID, 1);
			st.takeItems(MONSTER_EYE_MEAT_ID, 1);
		}
		else if(npcId == 30469 && cond > 0 && ingredients_count(st) == 0 && has_list(st) == 0 && has_dish(st) > 0 && has_review(st) == 0)
			htmltext = "30469-06.htm";
		else if(npcId == 30469 && cond > 0 && ingredients_count(st) == 0 && has_list(st) == 0 && has_dish(st) == 0 && has_review(st) > 0) // ???
																																			// ??????
		{
			if(st.getQuestItemsCount(MIRIENS_REVIEW1_ID) != 0)
			{
				htmltext = "30469-06t1.htm";
				st.takeItems(MIRIENS_REVIEW1_ID, 1);
				st.giveItems(ADENA_ID, 7500);
				st.addExpAndSp(6000, 0);
			}
			else if(st.getQuestItemsCount(MIRIENS_REVIEW2_ID) != 0)
			{
				htmltext = "30469-06t2.htm";
				st.takeItems(MIRIENS_REVIEW2_ID, 1);
				st.giveItems(ADENA_ID, 9000);
				st.addExpAndSp(7000, 0);
			}
			else if(st.getQuestItemsCount(MIRIENS_REVIEW3_ID) != 0)
			{
				htmltext = "30469-06t3.htm";
				st.takeItems(MIRIENS_REVIEW3_ID, 1);
				st.giveItems(ADENA_ID, 5800);
				st.giveItems(JONAS_SALAD_RECIPE_ID, 1);
				st.addExpAndSp(9000, 0);
			}
			else if(st.getQuestItemsCount(MIRIENS_REVIEW4_ID) != 0)
			{
				htmltext = "30469-06t4.htm";
				st.takeItems(MIRIENS_REVIEW4_ID, 1);
				st.giveItems(ADENA_ID, 6800);
				st.giveItems(JONAS_SAUCE_RECIPE_ID, 1);
				st.addExpAndSp(10500, 0);
			}
			else if(st.getQuestItemsCount(MIRIENS_REVIEW5_ID) != 0)
			{
				htmltext = "30469-06t5.htm";
				st.takeItems(MIRIENS_REVIEW5_ID, 1);
				st.giveItems(ADENA_ID, 7800);
				st.giveItems(JONAS_STEAK_RECIPE_ID, 1);
				st.addExpAndSp(12000, 0);
			}
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		else if(npcId == 30461 && cond > 0 && has_list(st) > 0)
			htmltext = "30461-01.htm";
		else if(npcId == 30461 && cond > 0 && ingredients_count(st) == 0 && has_list(st) == 0 && has_dish(st) > 0 && has_review(st) == 0)
		{
			if(st.getQuestItemsCount(JONAS_STEAK_DISH1_ID) == 1)
			{
				htmltext = "30461-02t1.htm";
				st.takeItems(JONAS_STEAK_DISH1_ID, 1);
				st.giveItems(MIRIENS_REVIEW1_ID, 1);
			}
			else if(st.getQuestItemsCount(JONAS_STEAK_DISH2_ID) == 1)
			{
				htmltext = "30461-02t2.htm";
				st.takeItems(JONAS_STEAK_DISH2_ID, 1);
				st.giveItems(MIRIENS_REVIEW2_ID, 1);
			}
			else if(st.getQuestItemsCount(JONAS_STEAK_DISH3_ID) == 1)
			{
				htmltext = "30461-02t3.htm";
				st.takeItems(JONAS_STEAK_DISH3_ID, 1);
				st.giveItems(MIRIENS_REVIEW3_ID, 1);
			}
			else if(st.getQuestItemsCount(JONAS_STEAK_DISH4_ID) == 1)
			{
				htmltext = "30461-02t4.htm";
				st.takeItems(JONAS_STEAK_DISH4_ID, 1);
				st.giveItems(MIRIENS_REVIEW4_ID, 1);
			}
			else if(st.getQuestItemsCount(JONAS_STEAK_DISH5_ID) == 1)
			{
				htmltext = "30461-02t5.htm";
				st.takeItems(JONAS_STEAK_DISH5_ID, 1);
				st.giveItems(MIRIENS_REVIEW5_ID, 1);
			}
		}
		else if(npcId == 30461 && cond > 0 && ingredients_count(st) == 0 && has_list(st) == 0 && has_dish(st) == 0 && has_review(st) == 0)
			htmltext = "30461-04.htm";
		else if(npcId == 30062 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) == 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
		{
			htmltext = "30062-01.htm";
			st.giveItems(SONIAS_BOTANYBOOK_ID, 1);
		}
		else if(npcId == 30062 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			htmltext = "30062-02.htm";
		else if(npcId == 30062 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) >= 40 && st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			htmltext = "30062-03.htm";
		else if(npcId == 30062 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) >= 40 && st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) >= 40 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
		{
			htmltext = "30062-06.htm";
			st.takeItems(SONIAS_BOTANYBOOK_ID, 1);
			st.takeItems(RED_MANDRAGORA_ROOT_ID, -1);
			st.takeItems(WHITE_MANDRAGORA_ROOT_ID, -1);
			st.giveItems(WHITE_MANDRAGORA_SAP_ID, 1);
		}
		else if(npcId == 30062 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) == 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) > 0)
			htmltext = "30062-07.htm";
		else if(npcId == 30073 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) == 0 && st.getQuestItemsCount(HONEY_ID) + st.getQuestItemsCount(GOLDEN_HONEY_ID) == 0)
		{
			htmltext = "30073-01.htm";
			st.giveItems(JAYCUBS_INSECTBOOK_ID, 1);
		}
		else if(npcId == 30073 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) > 0 && st.getQuestItemsCount(NECTAR_ID) < 20)
			htmltext = "30073-02.htm";
		else if(npcId == 30073 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) > 0 && st.getQuestItemsCount(NECTAR_ID) >= 20 && st.getQuestItemsCount(ROYAL_JELLY_ID) < 10)
			htmltext = "30073-03.htm";
		else if(npcId == 30073 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) > 0 && st.getQuestItemsCount(NECTAR_ID) >= 20 && st.getQuestItemsCount(ROYAL_JELLY_ID) >= 10)
		{
			htmltext = "30073-06.htm";
			st.takeItems(JAYCUBS_INSECTBOOK_ID, 1);
			st.takeItems(NECTAR_ID, -1);
			st.takeItems(ROYAL_JELLY_ID, -1);
			st.giveItems(GOLDEN_HONEY_ID, 1);
		}
		else if(npcId == 30073 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) == 0 && st.getQuestItemsCount(HONEY_ID) + st.getQuestItemsCount(GOLDEN_HONEY_ID) == 1)
			htmltext = "30073-07.htm";
		else if(npcId == 30078 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(PANOS_CONTRACT_ID) == 0 && st.getQuestItemsCount(DIONIAN_POTATO_ID) == 0)
		{
			htmltext = "30078-01.htm";
			st.giveItems(PANOS_CONTRACT_ID, 1);
		}
		else if(npcId == 30078 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(PANOS_CONTRACT_ID) > 0 && st.getQuestItemsCount(HOBGOBLIN_AMULET_ID) < 30)
			htmltext = "30078-02.htm";
		else if(npcId == 30078 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(PANOS_CONTRACT_ID) > 0 && st.getQuestItemsCount(HOBGOBLIN_AMULET_ID) >= 30)
		{
			htmltext = "30078-03.htm";
			st.takeItems(PANOS_CONTRACT_ID, 1);
			st.takeItems(HOBGOBLIN_AMULET_ID, -1);
			st.giveItems(DIONIAN_POTATO_ID, 1);
		}
		else if(npcId == 30078 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(PANOS_CONTRACT_ID) == 0 && st.getQuestItemsCount(DIONIAN_POTATO_ID) > 0)
			htmltext = "30078-04.htm";
		else if(npcId == 30067 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) == 0 && st.getQuestItemsCount(GREEN_MOSS_BUNDLE_ID) + st.getQuestItemsCount(BROWN_MOSS_BUNDLE_ID) == 0)
		{
			htmltext = "30067-01.htm";
			st.giveItems(GLYVKAS_BOTANYBOOK_ID, 1);
		}
		else if(npcId == 30067 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) + st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) < 20)
			htmltext = "30067-02.htm";
		else if(npcId == 30067 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) + st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) >= 20 && st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) < 20)
			htmltext = "30067-03.htm";
		else if(npcId == 30067 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) + st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) >= 20 && st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) >= 20)
		{
			htmltext = "30067-06.htm";
			st.takeItems(GLYVKAS_BOTANYBOOK_ID, 1);
			st.takeItems(GREEN_MARSH_MOSS_ID, -1);
			st.takeItems(BROWN_MARSH_MOSS_ID, -1);
			st.giveItems(BROWN_MOSS_BUNDLE_ID, 1);
		}
		else if(npcId == 30067 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) == 0 && st.getQuestItemsCount(GREEN_MOSS_BUNDLE_ID) + st.getQuestItemsCount(BROWN_MOSS_BUNDLE_ID) == 1)
			htmltext = "30067-07.htm";
		else if(npcId == 30069 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) == 0 && st.getQuestItemsCount(MONSTER_EYE_MEAT_ID) == 0)
		{
			htmltext = "30069-01.htm";
			st.giveItems(ROLANTS_CREATUREBOOK_ID, 1);
		}
		else if(npcId == 30069 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) > 0 && st.getQuestItemsCount(MONSTER_EYE_BODY_ID) < 30)
			htmltext = "30069-02.htm";
		else if(npcId == 30069 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) > 0 && st.getQuestItemsCount(MONSTER_EYE_BODY_ID) >= 30)
		{
			htmltext = "30069-03.htm";
			st.takeItems(ROLANTS_CREATUREBOOK_ID, 1);
			st.takeItems(MONSTER_EYE_BODY_ID, -1);
			st.giveItems(MONSTER_EYE_MEAT_ID, 1);
		}
		else if(npcId == 30069 && cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) == 0 && st.getQuestItemsCount(MONSTER_EYE_MEAT_ID) == 1)
			htmltext = "30069-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int n = 0;
		int cond = st.getInt("cond");
		if(npcId == 20265)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) > 0 && st.getQuestItemsCount(MONSTER_EYE_BODY_ID) < 30)
			{
				n = Rnd.get(100);
				if(n < 75)
				{
					st.giveItems(MONSTER_EYE_BODY_ID, 1);
					if(st.getQuestItemsCount(MONSTER_EYE_BODY_ID) == 30)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(MONSTER_EYE_BODY_ID) == 29)
					{
						st.giveItems(MONSTER_EYE_BODY_ID, 1);
						st.playSound(SOUND_MIDDLE);
					}
					else
						st.giveItems(MONSTER_EYE_BODY_ID, 2);
					st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20266)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(ROLANTS_CREATUREBOOK_ID) > 0 && st.getQuestItemsCount(MONSTER_EYE_BODY_ID) < 30)
			{
				n = Rnd.get(10);
				if(n < 7)
				{
					st.giveItems(MONSTER_EYE_BODY_ID, 1);
					if(st.getQuestItemsCount(MONSTER_EYE_BODY_ID) == 30)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(MONSTER_EYE_BODY_ID) == 29)
					{
						st.giveItems(MONSTER_EYE_BODY_ID, 1);
						st.playSound(SOUND_MIDDLE);
					}
					else
						st.giveItems(MONSTER_EYE_BODY_ID, 2);
					st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20226)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) > 0)
			{
				n = Rnd.get(10);
				if(n < 9)
				{
					if(st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) < 20)
						st.giveItems(GREEN_MARSH_MOSS_ID, 1);
					if(st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) < 20)
						st.giveItems(BROWN_MARSH_MOSS_ID, 1);
					if(st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20228)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(GLYVKAS_BOTANYBOOK_ID) > 0)
			{
				n = Rnd.get(100);
				if(n < 88)
				{
					if(st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) < 20)
						st.giveItems(GREEN_MARSH_MOSS_ID, 1);
					if(st.getQuestItemsCount(GREEN_MARSH_MOSS_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) < 20)
						st.giveItems(BROWN_MARSH_MOSS_ID, 1);
					if(st.getQuestItemsCount(BROWN_MARSH_MOSS_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20147)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(PANOS_CONTRACT_ID) > 0 && st.getQuestItemsCount(HOBGOBLIN_AMULET_ID) < 30)
				st.giveItems(HOBGOBLIN_AMULET_ID, 1);
			if(st.getQuestItemsCount(HOBGOBLIN_AMULET_ID) == 30)
				st.playSound(SOUND_MIDDLE);
			else
				st.playSound(SOUND_ITEMGET);
		}
		else if(npcId == 20204)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) > 0)
			{
				n = Rnd.get(100);
				if(n < 80)
				{
					if(st.getQuestItemsCount(NECTAR_ID) < 20)
						st.giveItems(NECTAR_ID, 1);
					if(st.getQuestItemsCount(NECTAR_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else if(n > 95)
				{
					if(st.getQuestItemsCount(ROYAL_JELLY_ID) < 10)
						st.giveItems(ROYAL_JELLY_ID, 1);
					if(st.getQuestItemsCount(ROYAL_JELLY_ID) == 10)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20229)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(JAYCUBS_INSECTBOOK_ID) > 0)
			{
				n = Rnd.get(100);
				if(n < 92)
				{
					if(st.getQuestItemsCount(NECTAR_ID) < 20)
						st.giveItems(NECTAR_ID, 1);
					if(st.getQuestItemsCount(NECTAR_ID) == 20)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(ROYAL_JELLY_ID) < 10)
						st.giveItems(ROYAL_JELLY_ID, 1);
					if(st.getQuestItemsCount(ROYAL_JELLY_ID) == 10)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20223)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			{
				n = Rnd.get(100);
				if(n < 67)
				{
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(RED_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else if(n > 93)
				{
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(WHITE_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20154)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			{
				n = Rnd.get(100);
				if(n < 74)
				{
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(RED_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else if(n > 92)
				{
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(WHITE_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20155)
		{
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			{
				n = Rnd.get(100);
				if(n < 80)
				{
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(RED_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else if(n > 91)
				{
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(WHITE_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		}
		else if(npcId == 20156)
			if(cond > 0 && has_list(st) > 0 && ingredients_count(st) < 5 && st.getQuestItemsCount(SONIAS_BOTANYBOOK_ID) > 0 && st.getQuestItemsCount(RED_MANDRAGORA_SAP_ID) + st.getQuestItemsCount(WHITE_MANDRAGORA_SAP_ID) == 0)
			{
				n = Rnd.get(100);
				if(n < 90)
				{
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(RED_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(RED_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
				else
				{
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) < 40)
						st.giveItems(WHITE_MANDRAGORA_ROOT_ID, 1);
					if(st.getQuestItemsCount(WHITE_MANDRAGORA_ROOT_ID) == 40)
						st.playSound(SOUND_MIDDLE);
					else
						st.playSound(SOUND_ITEMGET);
				}
			}
		return null;
	}
}
