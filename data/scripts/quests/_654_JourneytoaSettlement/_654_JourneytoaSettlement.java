package quests._654_JourneytoaSettlement;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _654_JourneytoaSettlement extends Quest implements ScriptFile
{
	// NPC
	private static final int SPIRIT = 31453; // Nameless Spirit

	// TARGET
	private static final int TARGET_1 = 21294; // Canyon Antelope
	private static final int TARGET_2 = 21295; // Canyon Antelope Slave

	// ITEM
	private static final int ITEM = 8072; // Antelope Skin

	// REWARD
	private static final int SCROLL = 8073; // Frintezza's Magic Force Field Removal Scroll

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _654_JourneytoaSettlement()
	{
		super(654, PARTY_ALL);

		addStartNpc(SPIRIT);
		addKillId(TARGET_1);
		addKillId(TARGET_2);
		addQuestItem(ITEM);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("31453-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		if(event.equalsIgnoreCase("31453-3.htm"))
			st.set("cond", "2");
		if(event.equalsIgnoreCase("31453-5.htm"))
		{
			st.giveItems(SCROLL, 1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		QuestState q = st.getPlayer().getQuestState("_119_LastImperialPrince");
		if(q == null)
			return htmltext;
		if(st.getPlayer().getLevel() < 74)
		{
			htmltext = "<html><body>Quest for characters level 74 and above.</body></html>";
			st.exitCurrentQuest(true);
			return htmltext;
		}
		else if(!q.isCompleted())
		{
			htmltext = "noquest";
			st.exitCurrentQuest(true);
			return htmltext;
		}
		int cond = st.getInt("cond");
		if(npc.getNpcId() == SPIRIT)
		{
			if(cond == 0)
				return "31453-1.htm";
			if(cond == 1)
				return "31453-2.htm";
			if(cond == 3)
				return "31453-4.htm";
		}
		else
			htmltext = "noquest";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 2 && Rnd.chance(5))
		{
			st.set("cond", "3");
			st.giveItems(ITEM, 1);
			st.playSound(SOUND_MIDDLE);
		}
		return null;
	}
}
