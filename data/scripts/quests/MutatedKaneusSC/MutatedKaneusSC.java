package quests.MutatedKaneusSC;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * <hr>
 * <em>Суперкласс квестов серии</em> <strong>Mutated Kaneus</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public abstract class MutatedKaneusSC extends Quest implements ScriptFile
{
	protected String qn = "MutatedKaneusSC";
	protected String name = "Mutated Kaneus Superclass";
	protected int startLevel = 99;
	protected int[] NPC = new int[] {};
	// dropList statement {mobId, itemId}
	protected int[][] dropList = new int[][] { {} };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public MutatedKaneusSC(int id, String qn, String name)
	{
		super(id, qn, name, -1);
		this.qn = qn;
		this.name = name;
	}

	protected void registerNPCs()
	{
		addStartNpc(NPC[0]);
		addTalkId(NPC[1]);
		for(int[] e : dropList)
		{
			addKillId(e[0]);
			addQuestItem(e[1]);
		}
	}

	public boolean checkItems(QuestState st)
	{
		int count = 0;
		for(int[] e : dropList)
		{
			if(st.getQuestItemsCount(e[1]) > 0)
				count++;
			else
				continue;
			if(count >= dropList.length)
				return true;
		}
		return false;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("0-03.htm"))
		{
			st.playSound(SOUND_ACCEPT);
			st.setState(STARTED);
			st.set("cond", "1");
		}
		else if(event.equalsIgnoreCase("1-02.htm"))
		{
			st.giveItems(57, 360000);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		if(st.getState() == CREATED && npcId == NPC[0])
		{
			if(st.getPlayer().getLevel() < startLevel)
				htmltext = "0-00.htm";
			else
				htmltext = "0-01.htm";
		}
		else if(st.isStarted())
			if(npcId == NPC[0])
			{
				if(checkItems(st))
					htmltext = "0-05.htm";
				else
					htmltext = "0-04.htm";
			}
			else if(npcId == NPC[1])
				if(checkItems(st))
					htmltext = "1-01.htm";
				else
					htmltext = "1-01a.htm";
		/*
		 * else if(st1.isCompleted()) //TODO требуется расширенная обработка состояния if(npcId == NPC[0]) htmltext = "0-0a.htm";
		 */
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st != null && st.getInt("cond") == 1)
			for(int[] e : dropList)
				if(npc.getNpcId() == e[0])
				{
					if(!(st.getQuestItemsCount(e[1]) > 0))
					{
						st.giveItems(e[1], 1);
						st.playSound(SOUND_MIDDLE);
					}
					break;
				}
		return null;
	}
}
