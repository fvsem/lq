package quests._134_TempleMissionary;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест на Adena, EX, SP, Badge - Temple Missionary
 */
public class _134_TempleMissionary extends Quest implements ScriptFile
{
	// NPC
	private static final int GLYVKA = 30067;
	private static final int ROUKE = 31418;
	// Items
	private static final int FRAGMENT = 10335;
	private static final int TOOL = 10336;
	private static final int REPORT = 10337;
	private static final int REPORT2 = 10338;
	private static final int BADGE = 10339;
	// Mobs
	private final static int Cruma_Marshlands_Traitor = 27339;
	private final static int[] mobs = { 20157, 20229, 20230, 20231, 20232, 20233, 20234 };

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _134_TempleMissionary()
	{
		super(134, 0);

		addStartNpc(GLYVKA);
		addTalkId(ROUKE);
		addKillId(mobs);
		addKillId(Cruma_Marshlands_Traitor);
		addQuestItem(FRAGMENT, TOOL, REPORT, REPORT2);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30067-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30067-04.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30067-08.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.unset("cond");
			st.exitCurrentQuest(false);
			st.giveItems(ADENA_ID, 15100);
			st.giveItems(BADGE, 1);
			st.addExpAndSp(30000, 2000);
		}
		else if(event.equalsIgnoreCase("31418-02.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("31418-07.htm"))
		{
			st.set("cond", "5");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
			st.giveItems(REPORT2, 1);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == GLYVKA)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 35)
					htmltext = "30067-01.htm";
				else
					htmltext = "30067-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 1)
				htmltext = "30067-02.htm";
			else if(cond == 2 || cond == 3 || cond == 4)
				htmltext = "30067-05.htm";
			else if(cond == 5)
				if(st.getQuestItemsCount(REPORT2) >= 1)
				{
					htmltext = "30067-06.htm";
					st.takeItems(REPORT2, -1);
				}
				else
					htmltext = "30067-07.htm";
		}
		else if(npcId == ROUKE)
			if(cond == 2)
				htmltext = "31418-01.htm";
			else if(cond == 3)
			{
				htmltext = "31418-04.htm";
				if(st.getQuestItemsCount(FRAGMENT) >= 10)
					while (st.getQuestItemsCount(FRAGMENT) >= 10)
					{
						st.takeItems(FRAGMENT, 10);
						st.giveItems(TOOL, 1);
					}
				else
					htmltext = "31418-03.htm";
			}
			else if(cond == 4)
			{
				if(st.getQuestItemsCount(REPORT) >= 3)
				{
					htmltext = "31418-05.htm";
					st.takeItems(FRAGMENT, -1);
					st.takeItems(TOOL, -1);
					st.takeItems(REPORT, -1);
				}
				else
					htmltext = "31418-06.htm";
			}
			else if(cond == 5)
				htmltext = "31418-08.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(st.getInt("cond") == 3)
			if(npcId == Cruma_Marshlands_Traitor)
			{
				st.giveItems(REPORT, 1);
				if(st.getQuestItemsCount(REPORT) >= 3)
				{
					st.set("cond", "4");
					st.setState(STARTED);
					st.playSound(SOUND_MIDDLE);
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
			else
			{
				if(Rnd.chance(70))
				{
					st.playSound(SOUND_ITEMGET);
					st.giveItems(FRAGMENT, 1);
				}
				if(st.getQuestItemsCount(TOOL) >= 1)
				{
					st.takeItems(TOOL, 1);
					if(Rnd.chance(45))
						st.addSpawn(Cruma_Marshlands_Traitor, npc.getX(), npc.getY(), npc.getZ(), 0, true, 900000);
				}
			}
		return null;
	}
}
