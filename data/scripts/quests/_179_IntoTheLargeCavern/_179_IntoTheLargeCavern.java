package quests._179_IntoTheLargeCavern;

import gnu.trove.map.hash.TIntObjectHashMap;

import java.util.HashMap;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.SpawnInfo;
import l2n.game.model.L2Spawn;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.base.Race;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2DoorInstance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.instances.L2ReflectionBossInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.TerritoryTable;
import l2n.util.Location;

public class _179_IntoTheLargeCavern extends Quest implements ScriptFile
{
	public class World
	{
		public long instanceId;
		public int status;
	}

	private static HashMap<Long, World> worlds = new HashMap<Long, World>();

	private final static int KEKROPUS = 32138;
	private final static int GardenGuard = 25529;

	private final static int GardenGuard1 = 18347;
	private final static int GardenGuard2 = 18348;
	private final static int GardenGuard3 = 18349;

	private final static int Kamael_Guard = 18352;
	private final static int Guardian_of_Records = 18353;
	private final static int Guardian_of_Observation = 18354;
	private final static int Spiculas_Guard = 18355;
	private final static int Harkilgameds_Gatekeeper = 18356;
	private final static int Rodenpiculas_Gatekeeper = 18357;
	private final static int Guardian_of_Secrets = 18358;
	private final static int Guardian_of_Arviterre = 18359;
	private final static int Katenars_Gatekeeper = 18360;
	private final static int Guardian_of_Prediction = 18361;

	private final static int Gate_Key_Kamael = 9703;
	private final static int Gate_Key_Archives = 9704;
	private final static int Gate_Key_Observation = 9705;
	private final static int Gate_Key_Spicula = 9706;
	private final static int Gate_Key_Harkilgamed = 9707;
	private final static int Gate_Key_Rodenpicula = 9708;
	private final static int Gate_Key_Arviterre = 9709;
	private final static int Gate_Key_Katenar = 9710;
	private final static int Gate_Key_Prediction = 9711;
	private final static int Gate_Key_Massive_Cavern = 9712;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _179_IntoTheLargeCavern()
	{
		super(179, 1);

		addStartNpc(KEKROPUS);
		addTalkId(GardenGuard);
		addAttackId(GardenGuard1);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("32138-06.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("EnterNornilsGarden"))
		{
			int cond = st.getInt("cond");
			if(cond != 1 || st.getPlayer().getRace() != Race.kamael)
				return "noquest";
			enterInstance(npc, st.getPlayer());
			return null;
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			htmltext = "32138-01.htm";
			if(player.getLevel() < 17)
			{
				htmltext = "32138-02.htm";
				st.exitCurrentQuest(true);
			}
			else if(player.getLevel() > 20 || player.getClassId().getLevel() > 1)
			{
				htmltext = "32138-02a.htm";
				st.exitCurrentQuest(true);
			}
			else if(!player.isQuestCompleted("_178_IconicTrinity"))
			{
				htmltext = "32138-03.htm";
				st.exitCurrentQuest(true);
			}
			else if(player.getRace() != Race.kamael)
			{
				htmltext = "32138-04.htm";
				st.exitCurrentQuest(true);
			}
		}
		else
			htmltext = "32138-07.htm";
		return htmltext;
	}

	@Override
	public String onAttack(L2NpcInstance npc, QuestState st)
	{
		World world = worlds.get(npc.getReflection().getId());
		if(world != null && world.status == 0)
		{
			world.status = 1;
			addSpawnToInstance(GardenGuard3, new Location(-110016, 74512, -12533, 0), false, world.instanceId);
			addSpawnToInstance(GardenGuard2, new Location(-109729, 74913, -12533, 0), false, world.instanceId);
			addSpawnToInstance(GardenGuard2, new Location(-109981, 74899, -12533, 0), false, world.instanceId);
		}
		return null;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(npc == null || !npc.isNpc())
			return null;
		switch (npc.getNpcId())
		{
			case Kamael_Guard:
				dropItem(npc, Gate_Key_Kamael, 1);
				break;
			case Guardian_of_Records:
				dropItem(npc, Gate_Key_Archives, 1);
				break;
			case Guardian_of_Observation:
				dropItem(npc, Gate_Key_Observation, 1);
				break;
			case Spiculas_Guard:
				dropItem(npc, Gate_Key_Spicula, 1);
				break;
			case Harkilgameds_Gatekeeper:
				dropItem(npc, Gate_Key_Harkilgamed, 1);
				break;
			case Rodenpiculas_Gatekeeper:
				dropItem(npc, Gate_Key_Rodenpicula, 1);
				break;
			case Guardian_of_Arviterre:
				dropItem(npc, Gate_Key_Arviterre, 1);
				break;
			case Katenars_Gatekeeper:
				dropItem(npc, Gate_Key_Katenar, 1);
				break;
			case Guardian_of_Prediction:
				dropItem(npc, Gate_Key_Prediction, 1);
				break;
			case Guardian_of_Secrets:
				dropItem(npc, Gate_Key_Massive_Cavern, 1);
				break;
		}
		return null;
	}

	private void enterInstance(L2NpcInstance npc, L2Player player)
	{
		InstanceManager ilm = InstanceManager.getInstance();
		TIntObjectHashMap<Instance> ils = ilm.getById(ReflectionTable.NORNILS_GARDEN);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);

		assert il != null;

		String name = il.getName();
		int timelimit = il.getDuration();
		int min_level = il.getMinLevel();
		int max_level = il.getMaxLevel();
		int minParty = il.getMinParty();
		int maxParty = il.getMaxParty();

		if(minParty > 1 && !player.isInParty())
		{
			player.sendPacket(Msg.YOU_ARE_NOT_CURRENTLY_IN_A_PARTY_SO_YOU_CANNOT_ENTER);
			return;
		}

		if(player.isInParty())
		{
			if(player.getParty().isInReflection())
			{
				player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(player));
				return;
			}

			for(L2Player member : player.getParty().getPartyMembers())
				if(ilm.getTimeToNextEnterInstance(name, member) > 0)
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1_MAY_NOT_RE_ENTER_YET).addName(member));
					return;
				}

			if(player.getParty().getPartyLeaderOID() != player.getObjectId())
			{
				player.sendPacket(Msg.ONLY_A_PARTY_LEADER_CAN_TRY_TO_ENTER);
				return;
			}

			if(player.getParty().getMemberCount() > maxParty)
			{
				player.sendPacket(Msg.YOU_CANNOT_ENTER_DUE_TO_THE_PARTY_HAVING_EXCEEDED_THE_LIMIT);
				return;
			}

			for(L2Player member : player.getParty().getPartyMembers())
			{
				if(member.getLevel() < min_level || member.getLevel() > max_level)
				{
					SystemMessage sm = new SystemMessage(SystemMessage.C1S_LEVEL_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member);
					member.sendPacket(sm);
					player.sendPacket(sm);
					return;
				}
				if(member.getClassId().getLevel() > 1 || member.isCursedWeaponEquipped())
				{
					player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(member));
					return;
				}
				if(!player.isInRange(member, 500))
				{
					member.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					player.sendPacket(Msg.ITS_TOO_FAR_FROM_THE_NPC_TO_WORK);
					return;
				}
			}
		}

		Reflection r = new Reflection(name);
		r.setInstancedZoneId(ReflectionTable.NORNILS_GARDEN);
		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			FillSpawns(r, i.getSpawnsInfo());
			FillDoors(r, i.getDoors());
		}

		World world = new World();
		world.instanceId = r.getId();
		worlds.put(r.getId(), world);

		for(L2Player member : player.getParty().getPartyMembers())
		{
			npc.makeSupportMagic(player);
			if(member != player)
				newQuestState(member, STARTED);
			member.setReflection(r);
			member.teleToLocation(il.getTeleportCoords());
			member.setVar("backCoords", r.getReturnLoc().toXYZString());
			member.setVar(name, String.valueOf(System.currentTimeMillis()));
		}

		player.getParty().setReflection(r);
		r.setParty(player.getParty());
		r.startCollapseTimer(timelimit * 60 * 1000L);
		player.getParty().broadcastToPartyMembers(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));
	}

	private void FillDoors(Reflection r, GArray<L2DoorInstance> doors)
	{
		if(doors == null || doors.isEmpty())
		{
			System.out.println("Not found doors for Nornil's Garden!!!");
			return;
		}
		for(L2DoorInstance d : doors)
		{
			L2DoorInstance door = d.clone();
			door.setReflection(r);
			r.addDoor(door);
			door.spawnMe();
			if(d.isOpen())
				door.openMe();
		}
	}

	private void FillSpawns(Reflection r, GArray<SpawnInfo> si)
	{
		if(si == null)
			return;
		for(SpawnInfo s : si)
		{
			L2Spawn c;
			GArray<int[]> points = TerritoryTable.getInstance().getLocation(s.getLocationId()).getCoords();
			for(int[] point : points)
			{
				c = s.getSpawn().clone();
				r.addSpawn(c);
				c.setReflection(r.getId());
				c.setRespawnDelay(s.getSpawn().getRespawnDelay(), s.getSpawn().getRespawnDelayRandom());
				c.setLocation(0);
				c.setLoc(new Location(point));
				if(!NpcTable.getTemplate(c.getNpcId()).isInstanceOf(L2ReflectionBossInstance.class))
					c.startRespawn();
				c.doSpawn(true);
				if(s.getSpawn().getNativeRespawnDelay() == 0)
					c.stopRespawn();
			}
		}
	}

	private void dropItem(L2NpcInstance npc, int itemId, int count)
	{
		L2ItemInstance item = ItemTable.getInstance().createItem(itemId, 0, 0, "QuestDrop:179_IntoTheLargeCavern");
		item.setCount(count);
		item.dropMe(npc, npc.getLoc());
	}
}
