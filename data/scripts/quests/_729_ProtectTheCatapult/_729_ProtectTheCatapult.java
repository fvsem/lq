package quests._729_ProtectTheCatapult;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _729_ProtectTheCatapult extends TerritoryWarSuperClass
{
	public _729_ProtectTheCatapult()
	{
		super(729);
		_npcIDs = new int[] { 36499, 36500, 36501, 36502, 36503, 36504, 36505, 36506, 36507 };
		registerAttackIds();
	}

	@Override
	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return npcid - 36498;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
