package quests._350_EnhanceYourWeapon;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _350_EnhanceYourWeapon extends Quest implements ScriptFile
{
	// CRYSTAL
	private static final int RED_SOUL_CRYSTAL0_ID = 4629;
	private static final int GREEN_SOUL_CRYSTAL0_ID = 4640;
	private static final int BLUE_SOUL_CRYSTAL0_ID = 4651;

	// npc
	private final static int WINONIN = 30856;
	private final static int JUREK = 30115;
	private static final int GIDEON = 30194;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 350: Enhance Your Weapon");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _350_EnhanceYourWeapon()
	{
		super(350, -1);
		addStartNpc(JUREK);
		addStartNpc(GIDEON);
		addStartNpc(WINONIN);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase(JUREK + "-04.htm") || event.equalsIgnoreCase(GIDEON + "-04.htm") || event.equalsIgnoreCase(WINONIN + "-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}

		if(event.equalsIgnoreCase("30115-09.htm") || event.equalsIgnoreCase("30856-09.htm"))
			st.giveItems(RED_SOUL_CRYSTAL0_ID, 1);
		if(event.equalsIgnoreCase("30115-10.htm") || event.equalsIgnoreCase("30856-10.htm"))
			st.giveItems(GREEN_SOUL_CRYSTAL0_ID, 1);
		if(event.equalsIgnoreCase("30115-11.htm") || event.equalsIgnoreCase("30856-11.htm"))
			st.giveItems(BLUE_SOUL_CRYSTAL0_ID, 1);
		if(event.equalsIgnoreCase("exit.htm"))
			st.exitCurrentQuest(true);
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String npcId = str(npc.getNpcId());
		String htmltext = "noquest";
		int id = st.getState();
		if(st.getQuestItemsCount(RED_SOUL_CRYSTAL0_ID) == 0 && st.getQuestItemsCount(GREEN_SOUL_CRYSTAL0_ID) == 0 && st.getQuestItemsCount(BLUE_SOUL_CRYSTAL0_ID) == 0)
			if(id == CREATED)
				htmltext = npcId + "-01.htm";
			else
				htmltext = npcId + "-21.htm";
		else
		{
			if(id == CREATED)
			{
				st.set("cond", "1");
				st.setState(STARTED);
			}
			htmltext = npcId + "-03.htm";
		}
		return htmltext;
	}
}
