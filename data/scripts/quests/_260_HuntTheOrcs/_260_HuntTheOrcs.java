package quests._260_HuntTheOrcs;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _260_HuntTheOrcs extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 260: Hunt The Orcs");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// Items
	private static final int ORC_AMULET = 1114;
	private static final int ORC_NECKLACE = 1115;
	private static final int ADENA_ID = 57;

	public _260_HuntTheOrcs()
	{
		super(260, -1);

		addStartNpc(30221);

		addKillId(new int[] { 20468, 20469, 20470, 20471, 20472, 20473 });

		addQuestItem(new int[] { ORC_AMULET, ORC_NECKLACE });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30221-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30221-06.htm"))
		{
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == 30221)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 6 && st.getPlayer().getRace().ordinal() == 1)
				{
					htmltext = "30221-02.htm";
					return htmltext;
				}
				else if(st.getPlayer().getRace().ordinal() != 1)
				{
					htmltext = "30221-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 6)
				{
					htmltext = "30221-01.htm";
					st.exitCurrentQuest(true);
				}
				else if(cond == 1 && st.getQuestItemsCount(ORC_AMULET) == 0 && st.getQuestItemsCount(ORC_NECKLACE) == 0)
					htmltext = "30221-04.htm";
			}
			else if(cond == 1 && (st.getQuestItemsCount(ORC_AMULET) > 0 || st.getQuestItemsCount(ORC_NECKLACE) > 0))
			{
				htmltext = "30221-05.htm";
				int adenaPay = 0;
				if(st.getQuestItemsCount(ORC_AMULET) >= 40)
					adenaPay += st.getQuestItemsCount(ORC_AMULET) * 14;
				else
					adenaPay += st.getQuestItemsCount(ORC_AMULET) * 12;
				if(st.getQuestItemsCount(ORC_NECKLACE) >= 40)
					adenaPay += st.getQuestItemsCount(ORC_NECKLACE) * 40;
				else
				{
					adenaPay += st.getQuestItemsCount(ORC_NECKLACE) * 30;
					st.giveItems(ADENA_ID, adenaPay, false);
					st.takeItems(ORC_AMULET, -1);
					st.takeItems(ORC_NECKLACE, -1);
					if(st.getPlayer().getClassId().isMage())
					{
						st.playTutorialVoice("tutorial_voice_027");
						st.giveItems(5790, 3000, false);
					}
					else
					{
						st.playTutorialVoice("tutorial_voice_026");
						st.giveItems(5789, 6000, false);
					}
				}
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(st.getInt("cond") > 0)
			if(npcId == 20468 || npcId == 20469 || npcId == 20470)
				st.rollAndGive(ORC_AMULET, 1, 14);
			else if(npcId == 20471 || npcId == 20472 || npcId == 20473)
				st.rollAndGive(ORC_NECKLACE, 1, 14);
		return null;
	}
}
