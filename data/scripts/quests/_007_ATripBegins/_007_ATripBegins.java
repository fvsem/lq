package quests._007_ATripBegins;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _007_ATripBegins extends Quest implements ScriptFile
{
	private static final int MIRABEL = 30146;
	private static final int ARIEL = 30148;
	private static final int ASTERIOS = 30154;

	private static final int SCROLL_OF_ESCAPE_GIRAN = 7126;
	private static final int MARK_OF_TRAVELER = 7570;

	private static final int ARIELS_RECOMMENDATION = 7572;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _007_ATripBegins()
	{
		super(7, -1);
		addStartNpc(MIRABEL);
		addTalkId(ARIEL, ASTERIOS);
		addQuestItem(ARIELS_RECOMMENDATION);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30146-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30148-02.htm"))
		{
			st.giveItems(ARIELS_RECOMMENDATION, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30154-02.htm"))
		{
			st.takeItems(ARIELS_RECOMMENDATION, -1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30146-06.htm"))
		{
			st.giveItems(SCROLL_OF_ESCAPE_GIRAN, 1);
			st.giveItems(MARK_OF_TRAVELER, 1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == MIRABEL)
		{
			if(cond == 0)
				if(st.getPlayer().getRace().ordinal() == 1 && st.getPlayer().getLevel() >= 3)
					htmltext = "30146-02.htm";
				else
				{
					htmltext = "30146-01.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "30146-04.htm";
			else if(cond == 3)
				htmltext = "30146-05.htm";
		}
		else if(npcId == ARIEL)
		{
			if(cond == 1 && st.getQuestItemsCount(ARIELS_RECOMMENDATION) == 0)
				htmltext = "30148-01.htm";
			else if(cond == 2)
				htmltext = "30148-03.htm";
		}
		else if(npcId == ASTERIOS)
			if(cond == 2 && st.getQuestItemsCount(ARIELS_RECOMMENDATION) > 0)
				htmltext = "30154-01.htm";
			else if(cond == 2 && st.getQuestItemsCount(ARIELS_RECOMMENDATION) == 0)
				htmltext = "30154-havent.htm";
			else if(cond == 3)
				htmltext = "30154-02r.htm";
		return htmltext;
	}
}
