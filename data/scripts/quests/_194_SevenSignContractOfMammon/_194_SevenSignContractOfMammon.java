package quests._194_SevenSignContractOfMammon;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Skill;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.tables.SkillTable;

public class _194_SevenSignContractOfMammon extends Quest implements ScriptFile
{
	// NPCs
	private final static int SirGustavAthebaldt = 30760;
	private final static int SecretAgentColin = 32571;
	private final static int FrogKing = 32572;
	private final static int GrandmaTess = 32573;
	private final static int VillagerKuta = 32574;
	private final static int ClaudiaAthebaldt = 31001;

	// ITEMS
	private final static int AthebaltsIntroduction = 13818;
	private final static int NativesGlove = 13819;
	private final static int FrogKingsBead = 13820;
	private final static int GrandmaTessCandyPouch = 13821;

	// Transform skills
	private final static L2Skill SkillFrog = SkillTable.getInstance().getInfo(6201, 1);
	private final static L2Skill SkillChild = SkillTable.getInstance().getInfo(6202, 1);
	private final static L2Skill SkillNative = SkillTable.getInstance().getInfo(6203, 1);

	// Transform ids
	private final static int TransformFrog = 111;
	private final static int TransformChild = 112;
	private final static int TransformNative = 124;

	public _194_SevenSignContractOfMammon()
	{
		super(194, -1);

		addStartNpc(SirGustavAthebaldt);
		addTalkId(VillagerKuta, GrandmaTess, FrogKing, SecretAgentColin, ClaudiaAthebaldt);
		addQuestItem(AthebaltsIntroduction, NativesGlove, FrogKingsBead, GrandmaTessCandyPouch);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		String htmltext = event;
		if(event.equalsIgnoreCase("30760-1.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30760-3.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30760-4.htm"))
		{
			player.showQuestMovie(ExStartScenePlayer.SCENE_SSQ_CONTRACT_OF_MAMMON);
			st.startQuestTimer("normal_world", 102000);
			return null;
		}
		else if(event.equalsIgnoreCase("30760-6.htm"))
		{
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
			st.giveItems(AthebaltsIntroduction, 1);
		}
		else if(event.equalsIgnoreCase("32571-3.htm"))
		{
			st.set("cond", "4");
			player.stopTransformation();
			st.playSound(SOUND_MIDDLE);
			st.takeItems(AthebaltsIntroduction, -1);

			SkillFrog.getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("32572-3.htm"))
		{
			st.set("cond", "5");
			st.playSound(SOUND_MIDDLE);
			st.giveItems(FrogKingsBead, 1);
		}
		else if(event.equalsIgnoreCase("32571-5.htm"))
		{
			st.set("cond", "6");
			st.playSound(SOUND_MIDDLE);
			st.takeItems(FrogKingsBead, -1);
			player.stopTransformation();
		}
		else if(event.equalsIgnoreCase("32571-7.htm"))
		{
			st.set("cond", "7");
			st.playSound(SOUND_MIDDLE);
			player.stopTransformation();
			SkillChild.getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("32573-2.htm"))
		{
			st.set("cond", "8");
			st.giveItems(GrandmaTessCandyPouch, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32571-9.htm"))
		{
			st.set("cond", "9");
			st.playSound(SOUND_MIDDLE);
			st.takeItems(GrandmaTessCandyPouch, 1);
			player.stopTransformation();
		}
		else if(event.equalsIgnoreCase("32571-11.htm"))
		{
			st.set("cond", "10");
			st.playSound(SOUND_MIDDLE);
			player.stopTransformation();
			SkillNative.getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("32574-3.htm"))
		{
			st.set("cond", "11");
			st.playSound(SOUND_MIDDLE);
			st.giveItems(NativesGlove, 1);
		}
		else if(event.equalsIgnoreCase("32571-13.htm"))
		{
			st.set("cond", "12");
			st.playSound(SOUND_MIDDLE);
			st.takeItems(NativesGlove, 1);
		}
		else if(event.equalsIgnoreCase("31001-2.htm"))
		{
			// Нельзя сдать квест субклассом
			// FIXME проверить!
			if(player.isSubClassActive())
				return "noquest";

			st.addExpAndSp(52518015, 5817677);
			st.setState(COMPLETED);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		else if(event.equalsIgnoreCase("normal_world"))
		{
			player.teleToLocation(80102, 56532, -1550);
			return "30760-4.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getCond();
		int id = st.getState();
		L2Player player = st.getPlayer();
		if(npcId == SirGustavAthebaldt)
		{
			if(id == CREATED)
			{
				if(player.getLevel() < 79)
				{
					st.exitCurrentQuest(true);
					return "30760-0a.htm";
				}
				QuestState qs = player.getQuestState("_193_SevenSignDyingMessage");
				if((qs == null || !qs.isCompleted()) && !player.isGM())
				{
					st.exitCurrentQuest(true);
					return "30760-0b.htm";
				}
				return "30760-0.htm";
			}
			else if(cond == 1)
				return "30760-1.htm";
			else if(cond == 2)
				return "30760-3.htm";
			else if(cond == 3)
				return "30760-6a.htm";
		}
		else if(npcId == SecretAgentColin)
		{
			if(cond == 3)
				return "32571-0.htm";
			else if(cond == 4)
			{
				if(player.getTransformationId() == 0)
					SkillFrog.getEffects(player, player, false, false);
				return "32571-3a.htm";
			}
			else if(cond == 5)
				return "32571-4.htm";
			else if(cond == 6)
				return "32571-6.htm";
			else if(cond == 7)
			{
				if(player.getTransformationId() == 0)
					SkillChild.getEffects(player, player, false, false);
				return "32571-7a.htm";
			}
			else if(cond == 8)
				return "32571-8.htm";
			else if(cond == 9)
				return "32571-10.htm";
			else if(cond == 10)
			{
				if(player.getTransformationId() == 0)
					SkillNative.getEffects(player, player, false, false);
				return "32571-11a.htm";
			}
			else if(cond == 11)
				return "32571-12.htm";
			else if(cond == 12)
				return "32571-13a.htm";
		}
		else if(npcId == FrogKing)
		{
			if(cond == 4 && player.getTransformationId() != TransformFrog)
				return "32572-0a.htm";
			else if(cond == 4 && player.getTransformationId() == TransformFrog)
				return "32572-0.htm";
		}
		else if(npcId == GrandmaTess)
		{
			if(cond == 7 && player.getTransformationId() != TransformChild)
				return "32573-0a.htm";
			else if(cond == 7 && player.getTransformationId() == TransformChild)
				return "32573-0.htm";
		}
		else if(npcId == VillagerKuta)
		{
			if(cond == 10 && player.getTransformationId() != TransformNative)
				return "32574-0a.htm";
			else if(cond == 10 && player.getTransformationId() == TransformNative)
				return "32574-0.htm";
		}
		else if(npcId == ClaudiaAthebaldt && cond == 12)
			return "31001-0.htm";
		return "noquest";
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
