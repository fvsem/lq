package quests._631_DeliciousTopChoiceMeat;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _631_DeliciousTopChoiceMeat extends Quest implements ScriptFile
{
	// NPC
	public static final int TUNATUN = 31537;
	// MOBS
	public static final int MOB_LIST[] = {
			21460,
			21461,
			21462,
			21463,
			21464,
			21465,
			21466,
			21467,
			21468,
			21469,
			21479,
			21480,
			21481,
			21482,
			21483,
			21484,
			21485,
			21486,
			21487,
			21488,
			21498,
			21499,
			21500,
			21501,
			21502,
			21503,
			21504,
			21505,
			21506,
			21507 };
	// ITEMS
	public static final int TOP_QUALITY_MEAT = 7546;
	// REWARDS
	public static final int MOLD_GLUE = 4039;
	public static final int MOLD_LUBRICANT = 4040;
	public static final int MOLD_HARDENER = 4041;
	public static final int ENRIA = 4042;
	public static final int ASOFE = 4043;
	public static final int THONS = 4044;
	public static final int[][] REWARDS = { { 1, MOLD_GLUE, 15 }, { 2, ASOFE, 15 }, { 3, THONS, 15 }, { 4, MOLD_LUBRICANT, 10 }, { 5, ENRIA, 10 }, { 6, MOLD_HARDENER, 5 } };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _631_DeliciousTopChoiceMeat()
	{
		super(631, -1);

		addStartNpc(TUNATUN);
		addTalkId(TUNATUN);
		addKillId(MOB_LIST);
		addQuestItem(TOP_QUALITY_MEAT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31537-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31537-05.htm") && st.getQuestItemsCount(TOP_QUALITY_MEAT) >= 120)
			st.set("cond", "3");
		for(int[] element : REWARDS)
			if(event.equalsIgnoreCase(String.valueOf(element[0])))
				if(st.getInt("cond") == 3 && st.getQuestItemsCount(TOP_QUALITY_MEAT) >= 120)
				{
					htmltext = "31537-06.htm";
					st.takeItems(TOP_QUALITY_MEAT, -1);
					st.giveItems(element[1], element[2]);
					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(true);
				}
				else
				{
					htmltext = "31537-07.htm";
					st.set("cond", "1");
					st.setState(STARTED);
					st.playSound(SOUND_ACCEPT);
				}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond < 1)
		{
			if(st.getPlayer().getLevel() < 65)
			{
				htmltext = "31537-02.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "31537-01.htm";
		}
		else if(cond == 1)
			htmltext = "31537-01a.htm";
		else if(cond == 2)
		{
			if(st.getQuestItemsCount(TOP_QUALITY_MEAT) < 120)
			{
				htmltext = "31537-01a.htm";
				st.set("cond", "1");
			}
			else
				htmltext = "31537-04.htm";
		}
		else if(cond == 3)
			htmltext = "31537-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1)
			if(st.rollAndGive(TOP_QUALITY_MEAT, 1, 80))
				if(st.getQuestItemsCount(TOP_QUALITY_MEAT) >= 120)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
		return null;
	}
}
