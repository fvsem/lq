package quests._014_WhereaboutsoftheArchaeologist;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _014_WhereaboutsoftheArchaeologist extends Quest implements ScriptFile {
    private static final int LETTER_TO_ARCHAEOLOGIST = 7253;

    @Override
    public void onLoad() {
        printInfo();
    }

    @Override
    public void onReload() {
    }

    @Override
    public void onShutdown() {
    }

    public _014_WhereaboutsoftheArchaeologist() {
        super(14, -1);

        addStartNpc(31263);

        addTalkId(31263);
        addTalkId(31538);

        addQuestItem(LETTER_TO_ARCHAEOLOGIST);
    }

    @Override
    public String onEvent(String event, QuestState st, L2NpcInstance npc) {
        String htmltext = event;
        if (event.equalsIgnoreCase("31263-2.htm")) {
            st.set("cond", "1");
            st.giveItems(LETTER_TO_ARCHAEOLOGIST, 1);
            st.setState(STARTED);
            st.playSound(SOUND_ACCEPT);
        } else if (event.equalsIgnoreCase("31538-2.htm")) {
            st.takeItems(LETTER_TO_ARCHAEOLOGIST, -1);
            st.giveItems(57, 113228);
            st.set("cond", "0");
            st.playSound(SOUND_FINISH);
            st.exitCurrentQuest(false);
            return "31538-2.htm";
        }
        return htmltext;
    }

    @Override
    public String onTalk(L2NpcInstance npc, QuestState st) {
        String htmltext = "noquest";
        int npcId = npc.getNpcId();
        int cond = st.getInt("cond");
        if (npcId == 31263) {
            if (cond == 0)
                if (st.getPlayer().getLevel() >= 74)
                    htmltext = "31263-1.htm";
                else {
                    htmltext = "31263-00.htm";
                    st.exitCurrentQuest(true);
                }
            else if (cond == 1)
                htmltext = "31263-2.htm";
        } else if (npcId == 31538)
            if (cond == 1 && st.getQuestItemsCount(LETTER_TO_ARCHAEOLOGIST) == 1)
                htmltext = "31538-1.htm";
        return htmltext;
    }

    @Override
    public String onKill(L2NpcInstance npc, QuestState st) {
        return null;
    }
}
