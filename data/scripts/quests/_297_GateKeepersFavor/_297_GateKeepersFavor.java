package quests._297_GateKeepersFavor;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _297_GateKeepersFavor extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 297: Gate Keepers Favor");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int STARSTONE = 1573;
	private static final int GATEKEEPER_TOKEN = 1659;

	public _297_GateKeepersFavor()
	{
		super(297, -1);
		addStartNpc(30540);
		addTalkId(30540);
		addKillId(20521);
		addQuestItem(STARSTONE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30540-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 30540)
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 15)
					htmltext = "30540-02.htm";
				else
					htmltext = "30540-01.htm";
			}
			else if(cond == 1 && st.getQuestItemsCount(STARSTONE) < 20)
				htmltext = "30540-04.htm";
			else if(cond == 2 && st.getQuestItemsCount(STARSTONE) < 20)
				htmltext = "30540-04.htm";
			else if(cond == 2 && st.getQuestItemsCount(STARSTONE) >= 20)
			{
				htmltext = "30540-05.htm";
				st.takeItems(STARSTONE, -1);
				st.giveItems(GATEKEEPER_TOKEN, 2);
				st.exitCurrentQuest(true);
				st.playSound(SOUND_FINISH);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		st.rollAndGive(STARSTONE, 1, 1, 20, 33);
		if(st.getQuestItemsCount(STARSTONE) >= 20)
			st.set("cond", "2");
		return null;
	}
}
