package quests._142_FallenAngelRequestOfDawn;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _142_FallenAngelRequestOfDawn extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPC
	private final static int NATOOLS = 30894;
	private final static int RAYMOND = 30289;
	private final static int CASIAN = 30612;
	private final static int ROCK = 32368;

	// ITEM
	private final static int CRYPT = 10351;
	private final static int FRAGMENT = 10352;
	private final static int BLOOD = 10353;

	// MONSTER
	private final static int Fallen_angel = 27338;
	private final static int[] MONSTERS = { 20079, 20080, 20081, 20082, 20084, 20086, 20087, 20088, 20089, 20090, 27338 };

	public _142_FallenAngelRequestOfDawn()
	{
		super(142, -1);

		addTalkId(NATOOLS, RAYMOND, CASIAN, ROCK);
		addQuestItem(CRYPT, FRAGMENT, BLOOD);
		addKillId(MONSTERS);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30894-01.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30894-03.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
			st.giveItems(CRYPT, 1);
		}
		else if(event.equalsIgnoreCase("30289-04.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30612-07.htm"))
		{
			st.set("cond", "4");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32368-02.htm"))
		{
			L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(Fallen_angel);
			if(isQuest == null)
			{
				st.addSpawn(Fallen_angel);
				st.playSound(SOUND_MIDDLE);
				st.startQuestTimer("Angel_fail", 900000);
			}
			else if(st.getQuestTimer("Angel_fail") == null)
				st.startQuestTimer("Angel_fail", 900000);
		}
		else if(event.equalsIgnoreCase("HARKILGAMED_Fail"))
		{
			htmltext = null;
			L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(Fallen_angel);
			if(isQuest != null)
				isQuest.deleteMe();
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		QuestState ok = st.getPlayer().getQuestState("_141_ShadowFoxPart3");
		if(npcId == NATOOLS)
		{
			if(ok != null)
			{
				if(cond == 1)
					htmltext = "30894-01.htm";
				else if(cond == 2)
					htmltext = "30894-04.htm";
			}
			else
				htmltext = "need.htm";
		}
		else if(npcId == RAYMOND)
		{
			if(cond == 2)
				if(st.getInt("talk") == 1)
					htmltext = "30289-02.htm";
				else
				{
					htmltext = "30289-01.htm";
					st.takeItems(CRYPT, -1);
					st.set("talk", "1");
				}
		}
		else if(cond == 3)
			htmltext = "30289-05.htm";
		else if(cond == 6)
		{
			htmltext = "30289-06.htm";
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
			st.giveItems(ADENA_ID, 92676);
			st.takeItems(BLOOD, -1);
			if(st.getPlayer().getLevel() >= 38 && st.getPlayer().getLevel() <= 43)
				st.addExpAndSp(223036, 13091);
		}
		else if(npcId == CASIAN)
		{
			if(cond == 3)
				htmltext = "30612-01.htm";
			else if(cond == 4)
				htmltext = "30612-07.htm";
		}
		else if(npcId == ROCK)
			if(cond == 5)
			{
				htmltext = "32368-01.htm";
				if(st.getInt("talk") != 1)
				{
					st.takeItems(BLOOD, -1);
					st.set("talk", "1");
				}
			}
			else if(cond == 6)
				htmltext = "32368-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		if(npcId == 27338 && cond == 5)
		{
			st.set("cond", "6");
			st.setState(STARTED);
			st.playSound("ItemSound.quest_middle");
			st.giveItems(BLOOD, 1);
		}
		else if(cond == 4 && st.getQuestItemsCount(FRAGMENT) < 30)
		{
			st.rollAndGive(FRAGMENT, 1, 1, 30, 20);
			if(st.getQuestItemsCount(FRAGMENT) >= 30)
			{
				st.set("cond", "5");
				st.setState(STARTED);
			}
		}
		return null;
	}
}
