package quests._618_IntoTheFlame;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;
import l2n.util.Rnd;

public class _618_IntoTheFlame extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPCs
	private final static int KLEIN = 31540;
	private final static int HILDA = 31271;

	// QUEST ITEMS
	private final static int VACUALITE_ORE = 7265;
	private final static int VACUALITE = 7266;
	private final static int FLOATING_STONE = 7267;

	// CHANCE
	private final static int CHANCE_FOR_QUEST_ITEMS = 50;

	public _618_IntoTheFlame()
	{
		super(618, -1);

		addStartNpc(KLEIN);
		addTalkId(KLEIN);
		addTalkId(HILDA);

		addKillId(ArrayUtil.getIntArrayRange(21274, 21276, 1));
		addKillId(ArrayUtil.getIntArrayRange(21282, 21284, 1));
		addKillId(ArrayUtil.getIntArrayRange(21290, 21292, 1));

		addQuestItem(VACUALITE_ORE);
		addQuestItem(VACUALITE);
		addQuestItem(FLOATING_STONE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getInt("cond");
		if(event.equalsIgnoreCase("31540-03.htm") && cond == 0)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31540-05.htm"))
			if(st.getQuestItemsCount(VACUALITE) > 0 && cond == 4)
			{
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
				st.giveItems(FLOATING_STONE, 1);
			}
			else
				htmltext = "31540-03.htm";
		else if(event.equalsIgnoreCase("31271-02.htm") && cond == 1)
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("31271-05.htm"))
			if(cond == 3 && st.getQuestItemsCount(VACUALITE_ORE) == 50)
			{
				st.takeItems(VACUALITE_ORE, -1);
				st.giveItems(VACUALITE, 1);
				st.set("cond", "4");
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "31271-03.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(st.getPlayer().getQuestState("_618_IntoTheFlame") == null)
			return htmltext;
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == KLEIN)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() < 60)
				{
					htmltext = "31540-01.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31540-02.htm";
			}
			else if(cond == 4 && st.getQuestItemsCount(VACUALITE) > 0)
				htmltext = "31540-04.htm";
			else
				htmltext = "31540-03.htm";
		}
		else if(npcId == HILDA)
			if(cond == 1)
				htmltext = "31271-01.htm";
			else if(cond == 3 && st.getQuestItemsCount(VACUALITE_ORE) >= 50)
				htmltext = "31271-04.htm";
			else if(cond == 4)
				htmltext = "31271-06.htm";
			else
				htmltext = "31271-03.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		long count = st.getQuestItemsCount(VACUALITE_ORE);
		if(Rnd.chance(CHANCE_FOR_QUEST_ITEMS) && count < 50)
		{
			st.giveItems(VACUALITE_ORE, 1);
			if(count == 49)
			{
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
			}
			else
				st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
