package quests._198_SevenSignEmbryo;

import gnu.trove.map.hash.TIntObjectHashMap;
import javolution.util.FastMap;
import l2n.commons.list.GArray;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.ai.CtrlIntention;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;

/**
 * @<!-- L2System -->
 * @date 17.11.2010
 * @time 3:17:14
 */
public class _198_SevenSignEmbryo extends Quest implements ScriptFile
{
	// npc
	private static final int WOOD = 32593;
	private static final int FRANZ = 32597;
	private static final int JAINA = 32582;

	// mobs
	private static final int SHILENSEVIL1 = 27346;
	private static final int SHILENSEVIL2 = 27343;

	// items
	private static final int SCULPTURE = 14360;
	private static final int BRACELET = 15312;
	private static final int AA = 5575;

	private static FastMap<Long, GArray<Long>> shilens = new FastMap<Long, GArray<Long>>();

	public _198_SevenSignEmbryo()
	{
		super(198, -1);

		addStartNpc(WOOD);
		addTalkId(WOOD, FRANZ, JAINA);
		addQuestItem(SCULPTURE);
		addKillId(SHILENSEVIL1, SHILENSEVIL2);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		String htmltext = event;
		if(event.equalsIgnoreCase("32593-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32597-10.htm"))
		{
			st.set("cond", "3");
			st.takeItems(SCULPTURE, -1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32597-05.htm"))
		{
			long refId = player.getReflectionId();
			GArray<Long> monsters = new GArray<Long>(3);

			L2NpcInstance monster = addSpawnToInstance(SHILENSEVIL1, new Location(-23775, -8830, -5390), false, refId);
			L2NpcInstance monster1 = addSpawnToInstance(SHILENSEVIL2, new Location(-23833, -9063, -5390), false, refId);
			L2NpcInstance monster2 = addSpawnToInstance(SHILENSEVIL2, new Location(-23834, -8855, -5390), false, refId);
			player.addDamageHate(0, 999, monster, monster1, monster2);

			Functions.npcSayInRangeCustomMessage(monster, 300, "quests._197_SevenSignTheSacredBookOfSeal.ShilenSpawn");
			monster.setRunning();
			monster1.setRunning();
			monster2.setRunning();
			monster.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
			monster1.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
			monster2.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);

			monsters.add(monster.getStoredId());
			monsters.add(monster1.getStoredId());
			monsters.add(monster2.getStoredId());
			shilens.put(player.getStoredId(), monsters);
		}
		else if(event.equalsIgnoreCase("TeleFranz"))
		{
			player.stopAllEffects();
			enterInstance(player);
			return null;
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();
		if(npcId == WOOD)
		{
			if(cond == 0)
			{
				QuestState qs = st.getPlayer().getQuestState("_197_SevenSignTheSacredBookOfSeal");
				if(qs != null && qs.isCompleted() && player.getLevel() >= 79 || player.isGM())
					htmltext = "32593-01.htm";
				else
				{
					htmltext = "32593-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "32593-02.htm";
			else if(cond == 2)
				htmltext = "32593-03.htm";
			else if(cond == 3)
			{
				htmltext = "32593-04.htm";
				st.addExpAndSp(315108096, 34906059);
				st.giveItems(BRACELET, 1, false);
				st.giveItems(AA, 1500000, false);
				st.unset("cond");
				st.takeItems(SCULPTURE, -1);
				st.setState(COMPLETED);
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		}
		else if(npcId == FRANZ)
		{
			if(cond == 1)
				htmltext = "32597-01.htm";
			else if(cond == 2)
				htmltext = "32597-06.htm";
			else if(cond == 3)
				htmltext = "32597-11.htm";
		}
		else if(npcId == JAINA)
		{
			Reflection r = player.getReflection();
			if(r.getReturnLoc() != null)
				player.teleToLocation(r.getReturnLoc(), 0);
			else
				player.setReflection(0);
			player.unsetVar("backCoords");
			return null;
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		if(npc.getNpcId() == SHILENSEVIL1 && st.getInt("cond") == 1)
		{
			L2NpcInstance mob = null;
			GArray<Long> mobs = shilens.remove(player.getStoredId());
			for(long storedId : mobs)
				if((mob = L2ObjectsStorage.getAsNpc(storedId)) != null)
					mob.deleteMe();

			mobs = null;
			Functions.npcSayInRangeCustomMessage(npc, 300, "quests._197_SevenSignTheSacredBookOfSeal.ShilenDie", st.getPlayer().getName());
			st.giveItems(SCULPTURE, 1, false);
			st.set("cond", "2");
			player.showQuestMovie(ExStartScenePlayer.SCENE_SSQ_EMBRYO);
		}
		return null;
	}

	private void enterInstance(L2Player player)
	{
		TIntObjectHashMap<Instance> ils = InstanceManager.getInstance().getById(ReflectionTable.HIDEOUT_OF_THE_DAWN);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);
		assert il != null;

		if(player.isInParty())
		{
			player.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		Reflection r = new Reflection(il.getName());
		r.setInstancedZoneId(ReflectionTable.HIDEOUT_OF_THE_DAWN);
		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		int timelimit = il.getDuration();

		player.setReflection(r);
		player.teleToLocation(il.getTeleportCoords());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());

		r.setEmptyDestroyTime(10 * 60 * 1000);
		r.startCollapseTimer(timelimit * 60 * 1000);
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
