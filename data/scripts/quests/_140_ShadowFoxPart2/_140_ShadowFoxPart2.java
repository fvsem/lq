package quests._140_ShadowFoxPart2;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Shadow Fox Part 2
 */
public class _140_ShadowFoxPart2 extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPCs
	private final static int KLUCK = 30895;
	private final static int XENOVIA = 30912;

	// ITEMs
	private final static int CRYSTAL = 10347;
	private final static int OXYDE = 10348;
	private final static int CRYPT = 10349;

	// MONSTERs
	private final static int[] NPC = { 20789, 20790, 20791, 20792 };

	public _140_ShadowFoxPart2()
	{
		super(140, -1);

		addStartNpc(KLUCK);

		addTalkId(KLUCK);
		addTalkId(XENOVIA);

		addQuestItem(CRYSTAL);
		addQuestItem(OXYDE);
		addQuestItem(CRYPT);

		for(int i : NPC)
			addKillId(i);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30895-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30895-05.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30895-09.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.unset("talk");
			st.unset("cond");
			st.exitCurrentQuest(false);
			st.giveItems(57, 18775, true);
			if(st.getPlayer().getLevel() >= 37 && st.getPlayer().getLevel() <= 42)
				st.addExpAndSp(30000, 2000);
		}
		else if(event.equalsIgnoreCase("30912-07.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30912-09.htm"))
		{
			st.takeItems(CRYSTAL, 5);
			if(Rnd.get(100) <= 60)
			{
				st.giveItems(OXYDE, 1);
				if(st.getQuestItemsCount(OXYDE) >= 3)
				{
					htmltext = "30912-09b.htm";
					st.set("cond", "4");
					st.setState(STARTED);
					st.playSound(SOUND_MIDDLE);
					st.takeItems(CRYSTAL, -1);
					st.takeItems(OXYDE, -1);
					st.giveItems(CRYPT, 1);
				}
			}
			else
				htmltext = "30912-09a.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		String htmltext = "noquest";
		if(npcId == KLUCK)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 37)
				{
					QuestState qs139 = st.getPlayer().getQuestState("_139_ShadowFoxPart1");
					if(qs139 != null)
						htmltext = "30895-01.htm";
					else
						htmltext = "need.htm";
				}
				else
				{
					htmltext = "30895-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30895-02.htm";
			else if(cond == 2 || cond == 3)
				htmltext = "30895-06.htm";
			else if(cond == 4)
				if(st.getInt("talk") == 1)
					htmltext = "30895-08.htm";
				else
				{
					htmltext = "30895-07.htm";
					st.takeItems(CRYPT, -1);
					st.set("talk", "1");
				}
		}
		else if(npcId == XENOVIA)
			if(cond == 2)
				htmltext = "30912-01.htm";
			else if(cond == 3)
			{
				if(st.getQuestItemsCount(CRYSTAL) >= 5)
					htmltext = "30912-08.htm";
				else
					htmltext = "30912-07.htm";
			}
			else if(cond == 4)
				htmltext = "30912-10.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 3 && Rnd.get(100) <= 80)
		{
			st.giveItems(CRYSTAL, 1);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
