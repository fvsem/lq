package quests._003_WilltheSealbeBroken;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Рейты учтены
 */

public class _003_WilltheSealbeBroken extends Quest implements ScriptFile
{
	int StartNpc = 30141;
	int[] Monster = { 20031, 20041, 20046, 20048, 20052, 20057 };

	int OnyxBeastEye = 1081;
	int TaintStone = 1082;
	int SuccubusBlood = 1083;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _003_WilltheSealbeBroken()
	{
		super(3, -1);
		addStartNpc(StartNpc);

		for(int npcId : Monster)
			addKillId(npcId);

		addQuestItem(new int[] { OnyxBeastEye, TaintStone, SuccubusBlood });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30141-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
			if(st.getPlayer().getRace().ordinal() != 2)
			{
				htmltext = "30141-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() >= 16)
			{
				htmltext = "30141-02.htm";
				return htmltext;
			}
			else
			{
				htmltext = "30141-01.htm";
				st.exitCurrentQuest(true);
			}
		else if(id == STARTED)
			if(st.getQuestItemsCount(OnyxBeastEye) > 0 && st.getQuestItemsCount(TaintStone) > 0 && st.getQuestItemsCount(SuccubusBlood) > 0)
			{
				htmltext = "30141-06.htm";
				st.takeItems(OnyxBeastEye, -1);
				st.takeItems(TaintStone, -1);
				st.takeItems(SuccubusBlood, -1);
				st.giveItems(956, 1, true);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else
				htmltext = "30141-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int id = st.getState();
		if(id == STARTED)
		{
			if(npcId == Monster[0] && st.getQuestItemsCount(OnyxBeastEye) == 0)
			{
				st.giveItems(OnyxBeastEye, 1, false);
				st.playSound(SOUND_ITEMGET);
			}
			else if((npcId == Monster[1] || npcId == Monster[2]) && st.getQuestItemsCount(TaintStone) == 0)
			{
				st.giveItems(TaintStone, 1, false);
				st.playSound(SOUND_ITEMGET);
			}
			else if((npcId == Monster[3] || npcId == Monster[4] || npcId == Monster[5]) && st.getQuestItemsCount(SuccubusBlood) == 0)
			{
				st.giveItems(SuccubusBlood, 1, false);
				st.playSound(SOUND_ITEMGET);
			}
			if(st.getQuestItemsCount(OnyxBeastEye) > 0 && st.getQuestItemsCount(TaintStone) > 0 && st.getQuestItemsCount(SuccubusBlood) > 0)
			{
				st.set("cond", "2");
				st.playSound(SOUND_MIDDLE);
			}
		}
		return null;
	}
}
