package quests._101_SwordOfSolidarity;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.util.Rnd;

public class _101_SwordOfSolidarity extends Quest implements ScriptFile
{
	private static int ROIENS_LETTER = 796;
	private static int HOWTOGO_RUINS = 937;
	private static int BROKEN_SWORD_HANDLE = 739;
	private static int BROKEN_BLADE_BOTTOM = 740;
	private static int BROKEN_BLADE_TOP = 741;
	private static int ALLTRANS_NOTE = 742;
	private static int SWORD_OF_SOLIDARITY = 738;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _101_SwordOfSolidarity()
	{
		super(101, -1);

		addStartNpc(30008);
		addTalkId(30008);
		addTalkId(30283);

		addKillId(20361);
		addKillId(20362);

		addQuestItem(ALLTRANS_NOTE, HOWTOGO_RUINS, BROKEN_BLADE_TOP, BROKEN_BLADE_BOTTOM, ROIENS_LETTER, BROKEN_SWORD_HANDLE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30008-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(ROIENS_LETTER, 1);
		}
		else if(event.equalsIgnoreCase("30283-02.htm"))
		{
			st.set("cond", "2");
			st.takeItems(ROIENS_LETTER, -1);
			st.giveItems(HOWTOGO_RUINS, 1);
		}
		else if(event.equalsIgnoreCase("30283-07.htm"))
		{
			st.takeItems(BROKEN_SWORD_HANDLE, -1);

			st.giveItems(SWORD_OF_SOLIDARITY, 1);
			st.giveItems(ADENA_ID, 10981);
			st.addExpAndSp(25747, 2171);

			if(st.getPlayer().getClassId().getLevel() == 1 && !st.getPlayer().getVarB("p1q3"))
			{
				st.getPlayer().setVar("p1q3", "1"); // flag for helper
				st.getPlayer().sendPacket(new ExShowScreenMessage("Now go find the Newbie Guide.", 5000, ScreenMessageAlign.TOP_CENTER, true));
				st.giveItems(1060, 100); // healing potion
				for(int item = 4412; item <= 4417; item++)
					st.giveItems(item, 10); // echo cry
				if(st.getPlayer().getClassId().isMage())
				{
					st.playTutorialVoice("tutorial_voice_027");
					st.giveItems(5790, 3000); // newbie sps
				}
				else
				{
					st.playTutorialVoice("tutorial_voice_026");
					st.giveItems(5789, 6000); // newbie ss
				}
			}

			st.unset("cond");
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 30008)
		{
			if(cond == 0)
			{

				if(st.getPlayer().getRace().ordinal() != 0)
					htmltext = "30008-00.htm";
				else if(st.getPlayer().getLevel() >= 9)
				{
					htmltext = "30008-02.htm";
					return htmltext;
				}
				else
				{
					htmltext = "30008-08.htm";
					st.exitCurrentQuest(true);
				}

			}
			else if(cond == 1 && st.getQuestItemsCount(ROIENS_LETTER) == 1)
				htmltext = "30008-05.htm";
			else if(cond >= 2 && st.getQuestItemsCount(ROIENS_LETTER) == 0 && st.getQuestItemsCount(ALLTRANS_NOTE) == 0)
			{
				if(st.getQuestItemsCount(BROKEN_BLADE_TOP) > 0 && st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) > 0)
					htmltext = "30008-12.htm";
				if(st.getQuestItemsCount(BROKEN_BLADE_TOP) + st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) <= 1)
					htmltext = "30008-11.htm";
				if(st.getQuestItemsCount(BROKEN_SWORD_HANDLE) > 0)
					htmltext = "30008-07.htm";
				if(st.getQuestItemsCount(HOWTOGO_RUINS) == 1)
					htmltext = "30008-10.htm";
			}
			else if(cond == 4 && st.getQuestItemsCount(ALLTRANS_NOTE) > 0)
			{
				htmltext = "30008-06.htm";
				st.set("cond", "5");
				st.takeItems(ALLTRANS_NOTE, -1);
				st.giveItems(BROKEN_SWORD_HANDLE, 1);
			}
		}
		else if(npcId == 30283)
			if(cond == 1 && st.getQuestItemsCount(ROIENS_LETTER) > 0)
				htmltext = "30283-01.htm";
			else if(cond >= 2 && st.getQuestItemsCount(HOWTOGO_RUINS) == 1)
			{
				if(st.getQuestItemsCount(BROKEN_BLADE_TOP) + st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) == 1)
					htmltext = "30283-08.htm";
				else if(st.getQuestItemsCount(BROKEN_BLADE_TOP) + st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) == 0)
					htmltext = "30283-03.htm";
				else if(st.getQuestItemsCount(BROKEN_BLADE_TOP) > 0 && st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) > 0)
				{
					htmltext = "30283-04.htm";
					st.set("cond", "4");
					st.takeItems(HOWTOGO_RUINS, -1);
					st.takeItems(BROKEN_BLADE_TOP, -1);
					st.takeItems(BROKEN_BLADE_BOTTOM, -1);
					st.giveItems(ALLTRANS_NOTE, 1);
				}
				else if(cond == 4 && st.getQuestItemsCount(ALLTRANS_NOTE) > 0)
					htmltext = "30283-05.htm";
			}
			else if(cond == 5 && st.getQuestItemsCount(BROKEN_SWORD_HANDLE) > 0)
				htmltext = "30283-06.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if((npcId == 20361 || npcId == 20362) && st.getQuestItemsCount(HOWTOGO_RUINS) > 0)
		{
			if(st.getQuestItemsCount(BROKEN_BLADE_TOP) == 0 && Rnd.chance(60))
			{
				st.giveItems(BROKEN_BLADE_TOP, 1);
				st.playSound(SOUND_MIDDLE);
			}
			else if(st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) == 0 && Rnd.chance(60))
			{
				st.giveItems(BROKEN_BLADE_BOTTOM, 1);
				st.playSound(SOUND_MIDDLE);
			}
			if(st.getQuestItemsCount(BROKEN_BLADE_TOP) > 0 && st.getQuestItemsCount(BROKEN_BLADE_BOTTOM) > 0)
				st.set("cond", "3");
		}
		return null;
	}
}
