package quests._138_TempleChampionPart2;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Temple Champion Part 2
 */
public class _138_TempleChampionPart2 extends Quest implements ScriptFile
{
	// NPC
	private final static int SYLVAIN = 30070;
	private final static int PUPINA = 30118;
	private final static int ANGUS = 30474;
	private final static int SLA = 30666;

	// ITEMS
	private final static int MANIFESTO = 10341;
	private final static int RELIC = 10342;
	private final static int ANGUS_REC = 10343;
	private final static int PUPINA_REC = 10344;
	private final static int ADENA = 57;

	// MONSTER
	private final static int[] mobs = { 20176, 20550, 20551, 20552 };

	// MINLEVEL
	private final static int MINLEVEL = 35;

	// MAXLEVEL
	private final static int MAXLEVEL = 41;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _138_TempleChampionPart2()
	{
		super(138, 0);

		addStartNpc(SYLVAIN);
		addTalkId(PUPINA);
		addTalkId(ANGUS);
		addTalkId(SLA);
		addKillId(mobs);
		addQuestItem(MANIFESTO);
		addQuestItem(RELIC);
		addQuestItem(ANGUS_REC);
		addQuestItem(PUPINA_REC);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30070-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.giveItems(MANIFESTO, 1);
		}
		else if(event.equalsIgnoreCase("30070-05.htm"))
		{
			st.unset("cond");
			st.giveItems(ADENA, 84593);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
			if(st.getPlayer().getLevel() >= MINLEVEL && st.getPlayer().getLevel() <= MAXLEVEL)
				st.addExpAndSp(187062, 11307);
		}
		else if(event.equalsIgnoreCase("30070-03.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30118-06.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30118-09.htm"))
		{
			st.set("cond", "6");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
			st.set("talk", "0");
			st.giveItems(PUPINA_REC, 1);
		}
		else if(event.equalsIgnoreCase("30474-02.htm"))
		{
			st.set("cond", "4");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30666-02.htm"))
		{
			st.set("talk", "1");
			st.takeItems(PUPINA_REC, -1);
		}
		else if(event.equalsIgnoreCase("30666-03.htm"))
		{
			st.set("talk", "2");
			st.takeItems(MANIFESTO, -1);
		}
		else if(event.equalsIgnoreCase("30666-08.htm"))
		{
			st.set("cond", "7");
			st.setState(STARTED);
			st.playSound(SOUND_MIDDLE);
			st.unset("talk");
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == SYLVAIN)
		{
			if(cond == 0)
			{
				QuestState TempleChampionPart1 = st.getPlayer().getQuestState("_137_TempleChampionPart1");
				if(TempleChampionPart1 != null)
				{
					if(TempleChampionPart1.isCompleted())
					{
						if(st.getPlayer().getLevel() >= MINLEVEL && st.getPlayer().getLevel() <= MAXLEVEL)
							htmltext = "30070-01.htm";
					}
					else
					{
						htmltext = "30070-00.htm";
						st.exitCurrentQuest(true);
					}
				}
				else
					htmltext = "need.htm";
			}
			else if(cond == 1)
				htmltext = "30070-02.htm";
			else if(cond == 2 && cond == 3 && cond == 4 && cond == 5 && cond == 6)
				htmltext = "30070-03.htm";
			else if(cond == 7)
				htmltext = "30070-04.htm";
		}
		else if(npcId == PUPINA)
		{
			if(cond == 2)
				htmltext = "30118-01.htm";
			else if(cond == 2)
				htmltext = "30118-07.htm";
			else if(cond == 4)
				htmltext = "30118-07.htm";
			else if(cond == 5)
			{
				htmltext = "30118-08.htm";
				st.takeItems(ANGUS_REC, -1);
			}
			else if(cond == 5)
				htmltext = "30118-10.htm";
		}
		else if(npcId == ANGUS)
		{
			if(cond == 3)
				htmltext = "30474-01.htm";
			else if(cond == 4)
			{
				if(st.getQuestItemsCount(RELIC) >= 10)
				{
					htmltext = "30474-04.htm";
					st.takeItems(RELIC, -1);
					st.giveItems(ANGUS_REC, 1);
					st.set("cond", "5");
					st.playSound(SOUND_MIDDLE);
				}
				else
					htmltext = "30474-03.htm";
			}
			else if(cond == 5)
				htmltext = "30474-05.htm";
		}
		else if(npcId == SLA)
			if(cond == 6)
			{
				if(st.getInt("talk") == 0)
					htmltext = "30666-01.htm";
				else if(st.getInt("talk") == 1)
					htmltext = "30666-02.htm";
				else if(st.getInt("talk") == 2)
					htmltext = "30666-03.htm";
			}
			else if(cond == 7)
				htmltext = "30666-09.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		if(cond == 4)
			if(st.getQuestItemsCount(RELIC) < 10)
			{
				st.giveItems(RELIC, 1);
				if(st.getQuestItemsCount(RELIC) >= 10)
					st.playSound(SOUND_MIDDLE);
				else
					st.playSound("SOUND_ITEMGET");
			}
		return null;
	}
}
