package quests._721_ForTheSakeOfTheTerritoryAden;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _721_ForTheSakeOfTheTerritoryAden extends TerritoryWarSuperClass
{
	public _721_ForTheSakeOfTheTerritoryAden()
	{
		super(721);
		_catapultId = 36503;
		_territoryId = 5;
		_leaderIDs = new int[] { 36532, 36534, 36537, 36595 };
		_guardIDs = new int[] { 36533, 36535, 36536 };
		_text = new String[] { "The catapult of Aden has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
