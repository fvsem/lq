package quests._694_BreakThroughTheHallOfSuffering;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.instancemanager.SeedOfInfinityManager;
import l2n.game.model.L2Party;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SeedOfInfinity;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;

/**
 * <h2>Описание</h2> Поговорите с <b>Офицером Тефиосом</b>, который находится внутри Зала Ожидания <b>Семени Бессмертия</b>
 * <ol>
 * <li>Уничтожьте угрозу в <b>Зале Страданий</b>:
 * <ol>
 * <li>Лидер группы через Рот Экимуса (слева от Тефиоса) заходит в <b>Зал Страданий</b>.</li>
 * <li>Очистите первые 5 комнат от монстров и Колоний Гнилой Плоти.</li>
 * <li>Уничтожьте Рыцарей Смерти — Близнецов (<b>Йохана Клодекуса</b> и <b>Йохана Кланикуса</b>), которые охраняют последнюю комнату.</li>
 * <li>После победы над близнецами лидер группы говорит с <b>Офицером Тефиосом</b> и все участники рейда получают награду. Сундук открывается двойным щелчком по нему в инвентаре.</li>
 * </ol>
 * 
 * @<!-- L2System -->
 * @date 27.02.2011
 * @time 1:24:06
 */
public class _694_BreakThroughTheHallOfSuffering extends SeedOfInfinity implements ScriptFile
{
	public _694_BreakThroughTheHallOfSuffering()
	{
		super(694, PARTY_NONE);
		addTalkId(MOUTH_OF_EKIMUS);
		addTalkId(TEPIOS_REWARD);
		addKillId(TUMOR_OF_DEATH, YEHAN_KLODEKUS, YEHAN_KLANIKUS);
		addAttackId(YEHAN_KLODEKUS, YEHAN_KLANIKUS);
	}

	@Override
	public String onEvent(final String event, final QuestState qs, final L2NpcInstance npc)
	{
		String htmltext = event;
		final L2Player player = qs.getPlayer();
		if(event.equals("32603-05.htm"))
		{
			final int cycle = SeedOfInfinityManager.getCurrentCycle();
			if(cycle == 1 || cycle == 2)
			{
				qs.setState(STARTED);
				qs.setCond(1);
				qs.playSound(SOUND_ACCEPT);
			}
			else
			{
				htmltext = "32603-03.htm";
				qs.exitCurrentQuest(true);
			}
		}
		else if(event.equals("ai_is_time_attack_reward_q0694_12.htm"))
		{
			final L2Party party = player.getParty();
			final HallofSufferingWorld world = getHallofSufferingWorld(npc.getReflectionId());
			if(world != null && party != null)
			{
				if(party.isLeader(player) && world.status == 5)
				{
					QuestState uqs;
					for(final L2Player p : player.getParty().getPartyMembers())
						if((uqs = getPlayerQuestState(p, getName())) != null)
						{
							if(uqs.getQuestItemsCount(MARK_OF_KEUCEREUS_STAGE_1) == 0)
								uqs.giveItems(MARK_OF_KEUCEREUS_STAGE_1, 1);

							uqs.giveItems(SOE, 1);
							uqs.giveItems(SUPPLIES[world.rewardType], 1);
							uqs.exitCurrentQuest(true);
						}

					htmltext = "ai_is_time_attack_reward_q0694_13.htm";
					endInstance(player);
				}
				else
					// дам награду только Вашему лидеру
					htmltext = "ai_is_time_attack_reward_q0694_12.htm";
			}
			else
				htmltext = "ai_is_time_attack_reward001.htm";
		}
		return htmltext;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		String htmltext = "noquest";
		final int npcId = npc.getNpcId();
		final int cond = st.getCond();
		final L2Player player = st.getPlayer();
		if(npcId == TEPIOS)
		{
			if(player.getLevel() < 75)
			{
				htmltext = "32603-02.htm";
				st.exitCurrentQuest(true);
			}
			else if(player.getLevel() > 82)
			{
				htmltext = "32603-02a.htm";
				st.exitCurrentQuest(true);
			}
			else if(cond == 0)
				htmltext = "32603-01.htm";
			else if(cond == 1)
				htmltext = "32603-07.htm";
			else if(cond == 2)
			{
				st.setState(STARTED);
				st.setCond(1);
				st.playSound(SOUND_ACCEPT);
				htmltext = "32603-05.htm";
			}
		}
		else if(npcId == MOUTH_OF_EKIMUS)
		{
			final int cycle = SeedOfInfinityManager.getCurrentCycle();
			if(cond == 1 && (cycle == 1 || cycle == 2))
			{
				if(checkCondition(player, ReflectionTable.SOI_HALL_OF_SUFFERING_SECTOR1))
				{
					final Reflection r = enterPartyInstance(player, ReflectionTable.SOI_HALL_OF_SUFFERING_SECTOR1, new HallofSufferingWorld());
					if(r != null)
					{
						final L2Party party = player.getParty();
						for(final L2Player pl : party.getPartyMembers())
							pl.setVar("SeedOfInfinityQuest", getName());
					}
					return null;
				}
				else
					htmltext = "Mouth of Ekimus:<br>Your team does not qualify for entry";
			}
			else
			{
				npc.onBypassFeedback(player, "Chat 2");
				return null;
			}
		}
		else if(npcId == TEPIOS_REWARD && cond == 2)
		{
			final HallofSufferingWorld world = getHallofSufferingWorld(npc.getReflectionId());
			if(world != null)
			{
				// 0 мин - 22 мин
				if(world.timer < 22 * 60)
					world.rewardType = 1;
				// 22 мин - 23 мин
				else if(world.timer >= 22 * 60 && world.timer <= 23 * 60)
					world.rewardType = 2;
				// 23 мин - 24 мин
				else if(world.timer > 23 * 60 && world.timer <= 24 * 60)
					world.rewardType = 3;
				// 24 мин - 25 мин
				else if(world.timer > 24 * 60 && world.timer <= 25 * 60)
					world.rewardType = 4;
				// 25 мин - 26 мин
				else if(world.timer > 25 * 60 && world.timer <= 26 * 60)
					world.rewardType = 5;
				// 26 мин - 60 мин
				else if(world.timer > 26 * 60 && world.timer <= 27 * 60)
					world.rewardType = 6;
				// 27 мин - 60 мин
				else if(world.timer > 27 * 60 && world.timer <= 28 * 60)
					world.rewardType = 7;
				// 28 мин - 60 мин
				else if(world.timer > 28 * 60 && world.timer <= 29 * 60)
					world.rewardType = 8;
				// 29 мин - 60 мин
				else if(world.timer > 29 * 60 && world.timer <= 30 * 60)
					world.rewardType = 9;
				// 30 мин - 60 мин
				else if(world.timer > 30 * 60)
					world.rewardType = 10;

				htmltext = "ai_is_time_attack_reward_q0694_0" + world.rewardType + ".htm";
			}
		}

		return htmltext;
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState qs)
	{
		if(!checkQuest(qs))
			return super.onKill(npc, qs);

		final HallofSufferingWorld world = getHallofSufferingWorld(npc.getReflectionId());
		if(world == null)
			return null;

		final L2Player player = qs.getPlayer();
		final int npcId = npc.getNpcId();
		if(!player.isInParty())
		{
			_log.warning(getName() + ": player: " + player.getName() + " Account: " + player.getAccountName() + " Have no party in party-instance, mb cheater?");
			return super.onKill(npc, qs);
		}

		if(npcId == TUMOR_OF_DEATH)
		{
			addSpawnToInstance(DESTROYED_TUMOR, npc.getLoc(), 0, world.instanceId);
			world.status++;
			if(world.status == 5)
				spawnBrothers(world);
		}
		else if((npcId == YEHAN_KLODEKUS || npcId == YEHAN_KLANIKUS) && checkKillProgress(npc, world) && qs.getCond() == 1)
		{
			addSpawnToInstance(TEPIOS_REWARD, new Location(-173704, 218088, -9528, 0), 0, world.instanceId);

			// отменяем спаун гвардов
			world.cancelGuardsSpawn();
			world.timer = System.currentTimeMillis() / 1000 - world.timer;

			SeedOfInfinityManager.addAttackSuffering();
			QuestState uqs;
			for(final L2Player pl : player.getParty().getPartyMembers())
				if((uqs = getPlayerQuestState(pl, getName())) != null)
					uqs.setCond(2);
		}

		return super.onKill(npc, qs);
	}

	@Override
	public String onAttack(final L2NpcInstance npc, final QuestState st)
	{
		final int npcId = npc.getNpcId();
		final HallofSufferingWorld world = getHallofSufferingWorld(npc.getReflectionId());
		if(world == null)
			return null;

		if(npcId == YEHAN_KLODEKUS || npcId == YEHAN_KLANIKUS)
		{
			if(world.guardsSpawnTask == null)
				world.guardsSpawnTask = L2GameThreadPools.getInstance().scheduleGeneral(new SpawnBossGuards(world), BOSS_MINION_SPAWN_TIME);
		}

		return null;
	}

	private void spawnBrothers(final HallofSufferingWorld world)
	{
		// world.createNpcList();
		world.getNpcList().clear();
		world.add(addSpawnToInstance(YEHAN_KLODEKUS, KLODEKUS_LOC, 0, world.instanceId), false);
		world.add(addSpawnToInstance(YEHAN_KLANIKUS, KLANIKUS_LOC, 0, world.instanceId), false);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
