package quests._010_IntoTheWorld;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _010_IntoTheWorld extends Quest implements ScriptFile
{
	int VERY_EXPENSIVE_NECKLACE = 7574;
	int SCROLL_OF_ESCAPE_GIRAN = 7126;
	int MARK_OF_TRAVELER = 7570;

	int BALANKI = 30533;
	int REED = 30520;
	int GERALD = 30650;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _010_IntoTheWorld()
	{
		super(10, -1);

		addStartNpc(BALANKI);

		addTalkId(BALANKI);
		addTalkId(REED);
		addTalkId(GERALD);

		addQuestItem(VERY_EXPENSIVE_NECKLACE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30533-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30520-02.htm"))
		{
			st.giveItems(VERY_EXPENSIVE_NECKLACE, 1);
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30650-02.htm"))
		{
			st.takeItems(VERY_EXPENSIVE_NECKLACE, -1);
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30520-05.htm"))
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30533-06.htm"))
		{
			st.giveItems(SCROLL_OF_ESCAPE_GIRAN, 1);
			st.giveItems(MARK_OF_TRAVELER, 1);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == BALANKI)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() == 4 && st.getPlayer().getLevel() >= 3)
					htmltext = "30533-02.htm";
				else
				{
					htmltext = "30533-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30533-04.htm";
			else if(cond == 4)
				htmltext = "30533-05.htm";
		}
		else if(npcId == REED)
		{
			if(cond == 1)
				htmltext = "30520-01.htm";
			else if(cond == 2)
				htmltext = "30520-03.htm";
			else if(cond == 3)
				htmltext = "30520-04.htm";
			else if(cond == 4)
				htmltext = "30520-06.htm";
		}
		else if(npcId == GERALD)
			if(cond == 2 && st.getQuestItemsCount(VERY_EXPENSIVE_NECKLACE) > 0)
				htmltext = "30650-01.htm";
			else if(cond == 3)
				htmltext = "30650-03.htm";
			else
				htmltext = "30650-04.htm";
		return htmltext;
	}
}
