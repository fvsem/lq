package quests._159_ProtectHeadsprings;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _159_ProtectHeadsprings extends Quest implements ScriptFile
{
	private static int ADENA_ID = 57;
	private static int PLAGUE_DUST_ID = 1035;
	private static int HYACINTH_CHARM1_ID = 1071;
	private static int HYACINTH_CHARM2_ID = 1072;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _159_ProtectHeadsprings()
	{
		super(159, -1);

		addStartNpc(30154);

		addTalkId(30154);

		addKillId(27017);

		addQuestItem(new int[] { PLAGUE_DUST_ID, HYACINTH_CHARM1_ID, HYACINTH_CHARM2_ID });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			st.set("cond", "1");
			st.set("id", "0");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			if(st.getQuestItemsCount(HYACINTH_CHARM1_ID) == 0)
			{
				st.giveItems(HYACINTH_CHARM1_ID, 1);
				htmltext = "30154-04.htm";
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
			st.set("id", "0");
		}
		if(npcId == 30154 && st.getInt("cond") == 0)
		{
			if(st.getInt("cond") < 15)
			{
				if(st.getPlayer().getRace().ordinal() != 1)
					htmltext = "30154-00.htm";
				else if(st.getPlayer().getLevel() >= 12)
				{
					htmltext = "30154-03.htm";
					return htmltext;
				}
				else
					st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30154-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == 30154 && st.getInt("cond") != 0 && st.getQuestItemsCount(HYACINTH_CHARM1_ID) != 0 && st.getQuestItemsCount(PLAGUE_DUST_ID) == 0)
			htmltext = "30154-05.htm";
		else if(npcId == 30154 && st.getInt("cond") != 0 && st.getQuestItemsCount(HYACINTH_CHARM1_ID) != 0 && st.getQuestItemsCount(PLAGUE_DUST_ID) != 0)
		{
			st.takeItems(PLAGUE_DUST_ID, st.getQuestItemsCount(PLAGUE_DUST_ID));
			st.takeItems(HYACINTH_CHARM1_ID, st.getQuestItemsCount(HYACINTH_CHARM1_ID));
			if(st.getQuestItemsCount(HYACINTH_CHARM2_ID) == 0)
				st.giveItems(HYACINTH_CHARM2_ID, 1);
			{
				htmltext = "30154-06.htm";
			}
		}
		else if(npcId == 30154 && st.getInt("cond") != 0 && st.getQuestItemsCount(HYACINTH_CHARM2_ID) != 0 && st.getQuestItemsCount(PLAGUE_DUST_ID) < 5)
			htmltext = "30154-07.htm";
		else if(npcId == 30154 && st.getInt("cond") != 0 && st.getQuestItemsCount(HYACINTH_CHARM2_ID) != 0 && st.getQuestItemsCount(PLAGUE_DUST_ID) >= 5)
			if(st.getInt("id") != 159)
			{
				st.set("id", "159");
				st.takeItems(PLAGUE_DUST_ID, st.getQuestItemsCount(PLAGUE_DUST_ID));
				st.takeItems(HYACINTH_CHARM2_ID, st.getQuestItemsCount(HYACINTH_CHARM2_ID));
				st.giveItems(ADENA_ID, 18250);
				htmltext = "30154-08.htm";
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == 27017)
		{
			st.set("id", "0");
			// думал думал, но смысл условия с рандомом не понял.
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(HYACINTH_CHARM2_ID) > 0 && Rnd.chance(40) && st.getQuestItemsCount(PLAGUE_DUST_ID) < 5)
				if(st.getQuestItemsCount(PLAGUE_DUST_ID) == 4)
				{
					st.giveItems(PLAGUE_DUST_ID, 1);
					st.playSound(SOUND_MIDDLE);
				}
				else
				{
					st.giveItems(PLAGUE_DUST_ID, 1);
					st.playSound(SOUND_ITEMGET);
				}
			if(st.getInt("cond") > 0 && st.getQuestItemsCount(HYACINTH_CHARM1_ID) > 0 && Rnd.chance(40) && st.getQuestItemsCount(PLAGUE_DUST_ID) == 0)
			{
				st.giveItems(PLAGUE_DUST_ID, 1);
				st.playSound(SOUND_MIDDLE);
			}
		}
		return null;
	}
}
