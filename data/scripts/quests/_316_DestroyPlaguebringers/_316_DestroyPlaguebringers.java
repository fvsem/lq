package quests._316_DestroyPlaguebringers;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Destroy Plaguebringers
 * 
 * @author Felixx
 */
public class _316_DestroyPlaguebringers extends Quest implements ScriptFile
{
	// NPCs
	private static int Ellenia = 30155;
	// Mobs
	private static int Sukar_Wererat = 20040;
	private static int Sukar_Wererat_Leader = 20047;
	private static int Varool_Foulclaw = 27020;
	// Items
	private static int ADENA = 57;
	// Quest Items
	private static int Wererats_Fang = 1042;
	private static int Varool_Foulclaws_Fang = 1043;
	// Chances
	private static int Wererats_Fang_Chance = 50;
	private static int Varool_Foulclaws_Fang_Chance = 30;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 316: Destroy Plaguebringers");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _316_DestroyPlaguebringers()
	{
		super(316, -1);
		addStartNpc(Ellenia);
		addTalkId(Ellenia);
		addKillId(Sukar_Wererat);
		addKillId(Sukar_Wererat_Leader);
		addKillId(Varool_Foulclaw);
		addQuestItem(Wererats_Fang);
		addQuestItem(Varool_Foulclaws_Fang);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("30155-04.htm") && st.getPlayer().getRace().ordinal() == 1 && st.getPlayer().getLevel() >= 18)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30155-08.htm"))
		{
			long Reward = st.getQuestItemsCount(Wererats_Fang) * 60 + st.getQuestItemsCount(Varool_Foulclaws_Fang) * 10000;
			st.takeItems(Wererats_Fang, -1);
			st.takeItems(Varool_Foulclaws_Fang, -1);
			if(Reward > 0)
				st.giveItems(ADENA, Reward);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
			st.unset("cond");
		}
		else if(event.equalsIgnoreCase("30155-09.htm"))
		{
			long Reward = st.getQuestItemsCount(Wererats_Fang) * 60 + st.getQuestItemsCount(Varool_Foulclaws_Fang) * 10000;
			st.takeItems(Wererats_Fang, -1);
			st.takeItems(Varool_Foulclaws_Fang, -1);
			if(Reward > 0)
				st.giveItems(ADENA, Reward);
			st.setState(STARTED);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		String htmltext = "noquest";
		if(cond == 0)
		{
			if(st.getPlayer().getRace().ordinal() != 1)
			{
				htmltext = "30155-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 18)
			{
				htmltext = "30155-02.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "30155-03.htm";
		}
		else if(cond == 1)
			htmltext = "30155-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(npc.getNpcId() == Varool_Foulclaw && qs.getQuestItemsCount(Varool_Foulclaws_Fang) == 0)
			qs.rollAndGive(Varool_Foulclaws_Fang, 1, 1, 1, Varool_Foulclaws_Fang_Chance);
		if(npc.getNpcId() == Sukar_Wererat || npc.getNpcId() == Sukar_Wererat_Leader)
			qs.rollAndGive(Wererats_Fang, 1, Wererats_Fang_Chance);
		return null;
	}
}
