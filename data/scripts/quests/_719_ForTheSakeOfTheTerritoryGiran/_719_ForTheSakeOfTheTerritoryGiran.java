package quests._719_ForTheSakeOfTheTerritoryGiran;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _719_ForTheSakeOfTheTerritoryGiran extends TerritoryWarSuperClass
{
	public _719_ForTheSakeOfTheTerritoryGiran()
	{
		super(719);
		_catapultId = 36501;
		_territoryId = 3;
		_leaderIDs = new int[] { 36520, 36522, 36525, 36593 };
		_guardIDs = new int[] { 36521, 36523, 36524 };
		_text = new String[] { "The catapult of Giran has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
