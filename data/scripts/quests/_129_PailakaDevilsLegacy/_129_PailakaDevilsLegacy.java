package quests._129_PailakaDevilsLegacy;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ItemTable;
import l2n.game.tables.ReflectionTable;
import l2n.util.ArrayUtil;
import l2n.util.Location;
import l2n.util.Rnd;

public class _129_PailakaDevilsLegacy extends Quest implements ScriptFile
{
	// NPC
	private static final int DISURVIVOR = 32498;
	private static final int SUPPORTER = 32501;
	private static final int DADVENTURER = 32508;
	private static final int DADVENTURER2 = 32511;
	private static final int CHEST = 32495;
	private static final int[] Pailaka2nd = new int[] { 18623, 18624, 18625, 18626, 18627 };

	// BOSS
	private static final int KAMS = 18629;
	private static final int ALKASO = 18631;
	private static final int LEMATAN = 18633;

	// ITEMS
	private static final int ScrollOfEscape = 736;
	private static final int SWORD = 13042;
	private static final int ENCHSWORD = 13043;
	private static final int LASTSWORD = 13044;
	private static final int KDROP = 13046;
	private static final int ADROP = 13047;
	private static final int KEY = 13150;
	private static final int[] HERBS = new int[] { 8601, 8602, 8604, 8605 };
	private static final int[] CHESTDROP = new int[] { 13033, 13048, 13049 }; // TODO нет скилла для бутылки: , 13059 };

	// REWARDS
	private static int PBRACELET = 13295;
	// private static int PERING = 13293;

	private static TIntLongHashMap _instances = new TIntLongHashMap();

	public _129_PailakaDevilsLegacy()
	{
		super(129, PARTY_NONE);

		addStartNpc(DISURVIVOR);
		addTalkId(SUPPORTER, DADVENTURER, DADVENTURER2);
		addKillId(KAMS, ALKASO, LEMATAN, CHEST);
		addKillId(Pailaka2nd);
		addQuestItem(SWORD, ENCHSWORD, LASTSWORD, KDROP, ADROP, KEY);
		addQuestItem(CHESTDROP);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		String htmltext = event;
		if(event.equalsIgnoreCase("Enter"))
		{
			enterInstance(player);
			return null;
		}
		else if(event.equalsIgnoreCase("32498-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32498-05.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32501-03.htm"))
		{
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
			st.giveItems(SWORD, 1);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int id = st.getState();
		L2Player player = st.getPlayer();
		if(npcId == DISURVIVOR)
		{
			if(cond == 0)
				if((player.getLevel() < 61 || player.getLevel() > 67) && !player.isGM())
				{
					htmltext = "32498-no.htm";
					st.exitCurrentQuest(true);
				}
				else
					return "32498-01.htm";
			else if(id == COMPLETED)
				htmltext = "32498-no.htm";
			else if(cond == 1 || cond == 2)
				htmltext = "32498-06.htm";
			else
				htmltext = "32498-07.htm";
		}
		else if(npcId == SUPPORTER)
		{
			if(cond == 1 || cond == 2)
				htmltext = "32501-01.htm";
			else
				htmltext = "32501-04.htm";
		}
		else if(npcId == DADVENTURER)
		{
			if(st.getQuestItemsCount(SWORD) > 0 && st.getQuestItemsCount(KDROP) == 0)
				htmltext = "32508-01.htm";
			if(st.getQuestItemsCount(ENCHSWORD) > 0 && st.getQuestItemsCount(ADROP) == 0)
				htmltext = "32508-01.htm";
			if(st.getQuestItemsCount(SWORD) == 0 && st.getQuestItemsCount(KDROP) > 0)
				htmltext = "32508-05.htm";
			if(st.getQuestItemsCount(ENCHSWORD) == 0 && st.getQuestItemsCount(ADROP) > 0)
				htmltext = "32508-05.htm";
			if(st.getQuestItemsCount(SWORD) == 0 && st.getQuestItemsCount(ENCHSWORD) == 0)
				htmltext = "32508-05.htm";
			if(st.getQuestItemsCount(KDROP) == 0 && st.getQuestItemsCount(ADROP) == 0)
				htmltext = "32508-01.htm";
			if(player.getPet() != null)
				htmltext = "32508-04.htm";
			if(st.getQuestItemsCount(SWORD) > 0 && st.getQuestItemsCount(KDROP) > 0)
			{
				st.takeItems(SWORD, 1);
				st.takeItems(KDROP, 1);
				st.giveItems(ENCHSWORD, 1);
				htmltext = "32508-02.htm";
			}
			if(st.getQuestItemsCount(ENCHSWORD) > 0 && st.getQuestItemsCount(ADROP) > 0)
			{
				st.takeItems(ENCHSWORD, 1);
				st.takeItems(ADROP, 1);
				st.giveItems(LASTSWORD, 1);
				htmltext = "32508-03.htm";
			}
			if(st.getQuestItemsCount(LASTSWORD) > 0)
				htmltext = "32508-03.htm";
		}
		else if(npcId == DADVENTURER2)
			if(cond == 4)
			{
				if(player.getPet() != null)
					htmltext = "32511-03.htm";
				else if(player.getPet() == null)
				{
					st.giveItems(ScrollOfEscape, 1);
					st.giveItems(PBRACELET, 1);
					st.addExpAndSp(10810000, 950000);
					st.set("cond", "5");
					st.setState(COMPLETED);
					st.playSound(SOUND_FINISH);
					st.exitCurrentQuest(false);
					player.setVitalityPoints(20000, true);
					player.getReflection().startCollapseTimer(60000);
					htmltext = "32511-01.htm";
				}
			}
			else if(id == COMPLETED)
				htmltext = "32511-02.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getPlayer();
		int npcId = npc.getNpcId();
		long refId = player.getReflection().getId();
		if(npcId == KAMS)
		{
			if(st.getQuestItemsCount(KDROP) == 0)
				st.giveItems(KDROP, 1);
			addSpawnToInstance(ALKASO, new Location(82629, -209487, -3585, 30456), false, refId);
		}
		else if(npcId == ALKASO)
		{
			if(st.getQuestItemsCount(ADROP) == 0)
				st.giveItems(ADROP, 1);
			addSpawnToInstance(LEMATAN, new Location(87798, -209142, -3774, 49152), false, refId);
		}
		else if(npcId == LEMATAN)
		{
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
			addSpawnToInstance(DADVENTURER2, new Location(84990, -208376, -3342, 55000), false, refId);
		}
		else if(ArrayUtil.arrayContains(Pailaka2nd, npcId))
		{
			if(Rnd.get(100) < 80)
				dropItem(npc, HERBS[Rnd.get(HERBS.length)], Rnd.get(1, 2));
		}
		else if(npcId == CHEST)
			if(Rnd.get(100) < 80)
				dropItem(npc, CHESTDROP[Rnd.get(CHESTDROP.length)], Rnd.get(1, 10));
			else
				dropItem(npc, KEY, 1);
		return null;
	}

	private void enterInstance(L2Player player)
	{
		int instancedZoneId = ReflectionTable.PAILAKA_DEVILS_LEGACY;
		InstanceManager ilm = InstanceManager.getInstance();
		TIntObjectHashMap<Instance> ils = ilm.getById(instancedZoneId);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);

		assert il != null;

		if(player.isInParty())
		{
			player.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		long old = _instances.get(player.getObjectId());
		if(old != 0)
		{
			Reflection old_r = ReflectionTable.getInstance().get(old);
			if(old_r != null)
			{
				player.setReflection(old_r);
				player.teleToLocation(il.getTeleportCoords());
				player.setVar("backCoords", old_r.getReturnLoc().toXYZString());
				return;
			}
		}

		Reflection r = new Reflection(il.getName());
		r.setInstancedZoneId(instancedZoneId);
		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
		}

		int timelimit = il.getDuration();

		player.setReflection(r);
		player.teleToLocation(il.getTeleportCoords());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());
		player.sendPacket(new SystemMessage(SystemMessage.THIS_DUNGEON_WILL_EXPIRE_IN_S1_MINUTES).addNumber(timelimit));

		r.setEmptyDestroyTime(15 * 60 * 1000);
		r.startCollapseTimer(timelimit * 60 * 1000L);

		_instances.put(player.getObjectId(), r.getId());
	}

	private void dropItem(L2NpcInstance npc, int itemId, int count)
	{
		L2ItemInstance item = ItemTable.getInstance().createItem(itemId, 0, npc.getNpcId(), "PailakaDevilsLegacy.dropItem()");
		if(item.isStackable())
			item.setCount(count);
		item.dropMe(npc, npc.getLoc());
	}

	@Override
	public void onAbort(QuestState qs)
	{
		// при отмене квеста закрываем инстанс если он есть
		L2Player player = qs.getPlayer();
		if(player != null)
		{
			long refId;
			if((refId = _instances.remove(player.getObjectId())) > 0)
			{
				Reflection old_r = ReflectionTable.getInstance().get(refId);
				if(old_r != null)
					old_r.startCollapseTimer(10000);
			}
		}
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
