package quests._300_HuntingLetoLizardman;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _300_HuntingLetoLizardman extends Quest implements ScriptFile
{
	// NPCs
	private static int RATH = 30126;
	// Items
	private static int BRACELET_OF_LIZARDMAN = 7139;
	private static int ANIMAL_BONE = 1872;
	private static int ADENA = 57;
	// Chances
	private static int BRACELET_OF_LIZARDMAN_CHANCE = 70;
	private static int ADENA_CHANCE = 50;

	public _300_HuntingLetoLizardman()
	{
		super(300, -1);
		addStartNpc(RATH);
		for(int lizardman_id = 20577; lizardman_id <= 20582; lizardman_id++)
			addKillId(lizardman_id);
		addQuestItem(BRACELET_OF_LIZARDMAN);
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != RATH)
			return htmltext;
		if(st.getState() == CREATED)
		{
			if(st.getPlayer().getLevel() < 34)
			{
				htmltext = "30126-00.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30126-01.htm";
				st.set("cond", "0");
			}
		}
		else if(st.getQuestItemsCount(BRACELET_OF_LIZARDMAN) < 60)
		{
			htmltext = "30126-02r.htm";
			st.set("cond", "1");
		}
		else
			htmltext = "30126-03.htm";
		return htmltext;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		int _state = st.getState();
		if(event.equalsIgnoreCase("30126-02.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30126-04.htm") && _state == STARTED)
			if(st.getQuestItemsCount(BRACELET_OF_LIZARDMAN) < 60)
			{
				htmltext = "bug.htm";
				st.set("cond", "1");
			}
			else
			{
				st.takeItems(BRACELET_OF_LIZARDMAN, -1);
				if(Rnd.chance(ADENA_CHANCE))
					st.giveItems(ADENA, 30000);
				else
					st.giveItems(ANIMAL_BONE, 50);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		long _count = qs.getQuestItemsCount(BRACELET_OF_LIZARDMAN);
		if(_count < 60 && Rnd.chance(BRACELET_OF_LIZARDMAN_CHANCE))
		{
			qs.giveItems(BRACELET_OF_LIZARDMAN, 1);
			if(_count == 59)
			{
				qs.set("cond", "2");
				qs.playSound(SOUND_MIDDLE);
			}
			else
				qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 300: Hunting Leto Lizardman");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
