package quests._273_InvadersOfHolyland;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _273_InvadersOfHolyland extends Quest implements ScriptFile
{
	public final int BLACK_SOULSTONE = 1475;
	public final int RED_SOULSTONE = 1476;
	public final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 273: Invaders Of Holyland");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _273_InvadersOfHolyland()
	{
		super(273, -1);

		addStartNpc(30566);
		addKillId(new int[] { 20311, 20312, 20313 });
		addQuestItem(new int[] { BLACK_SOULSTONE, RED_SOULSTONE });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30566-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("30566-07.htm"))
		{
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		else if(event.equals("30566-08.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() < 15)
			{
				if(st.getPlayer().getRace().ordinal() != 3)
				{
					htmltext = "30566-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() < 6)
				{
					htmltext = "30566-01.htm";
					st.exitCurrentQuest(true);
				}
				else
				{
					htmltext = "30566-02.htm";
					return htmltext;
				}
			}
			else
			{
				htmltext = "30566-01.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(cond > 0)
			if(st.getQuestItemsCount(BLACK_SOULSTONE) + st.getQuestItemsCount(RED_SOULSTONE) == 0)
				htmltext = "30566-04.htm";
			else if(st.getQuestItemsCount(RED_SOULSTONE) == 0)
			{
				htmltext = "30566-05.htm";
				long n = st.getQuestItemsCount(BLACK_SOULSTONE);
				st.giveItems(ADENA, n * 5);
				st.takeItems(BLACK_SOULSTONE, n);
				st.set("cond", "0");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "30566-06.htm";
				if(st.getQuestItemsCount(BLACK_SOULSTONE) > 0)
				{
					st.giveItems(ADENA, st.getQuestItemsCount(BLACK_SOULSTONE) * 5);
					st.takeItems(BLACK_SOULSTONE, st.getQuestItemsCount(BLACK_SOULSTONE));
				}
				st.giveItems(ADENA, st.getQuestItemsCount(RED_SOULSTONE) * 50);
				st.takeItems(RED_SOULSTONE, st.getQuestItemsCount(RED_SOULSTONE));
				st.unset("cond");
				if(st.getPlayer().getClassId().isMage())
				{
					st.playTutorialVoice("tutorial_voice_027");
					st.giveItems(5790, 3000);
				}
				else
				{
					st.playTutorialVoice("tutorial_voice_026");
					st.giveItems(5789, 6000);
				}
				st.exitCurrentQuest(true);
				st.playSound(SOUND_FINISH);
			}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == 20311)
		{
			if(cond == 1)
			{
				if(Rnd.chance(90))
					st.giveItems(BLACK_SOULSTONE, 1);
				else
					st.giveItems(RED_SOULSTONE, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20312)
		{
			if(cond == 1)
			{
				if(Rnd.chance(87))
					st.giveItems(BLACK_SOULSTONE, 1);
				else
					st.giveItems(RED_SOULSTONE, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		else if(npcId == 20313)
			if(cond == 1)
			{
				if(Rnd.chance(77))
					st.giveItems(BLACK_SOULSTONE, 1);
				else
					st.giveItems(RED_SOULSTONE, 1);
				st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
