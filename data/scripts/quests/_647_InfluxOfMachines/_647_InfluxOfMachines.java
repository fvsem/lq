package quests._647_InfluxOfMachines;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест проверен и работает.
 * Рейты прописаны путем повышения шанса получения квестовых вещей.
 */
public class _647_InfluxOfMachines extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 647: Influx Of Machines");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// Settings: drop chance in %
	private static final int DROP_CHANCE = 60;

	// QUEST ITEMS
	private static final int DESTROYED_GOLEM_SHARD = 8100;

	// REWARDS
	private static final int[] RECIPES_60 = {
			4963,
			4964,
			4965,
			4966,
			4967,
			4968,
			4969,
			4970,
			4971,
			4972,
			5000,
			5001,
			5002,
			5003,
			5004,
			5005,
			5006,
			5007,
			8298,
			8306,
			8310,
			8312,
			8322,
			8324 };

	private static final int[] RECIPES_100 = {
			4182,
			4183,
			4184,
			4185,
			4186,
			4187,
			4188,
			4189,
			4190,
			4191,
			4192,
			4193,
			4194,
			4195,
			4196,
			4197,
			4198,
			4199,
			8297,
			8305,
			8309,
			8311,
			8321,
			8323 };

	public _647_InfluxOfMachines()
	{
		super(647, 0);

		addStartNpc(32069);

		addTalkId(32069);
		addTalkId(32069);
		addTalkId(32069);

		for(int i = 22052; i < 22079; i++)
			addKillId(i);

		addQuestItem(DESTROYED_GOLEM_SHARD);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32069-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32069-06.htm"))
			if(st.getQuestItemsCount(DESTROYED_GOLEM_SHARD) >= 500)
			{
				st.takeItems(DESTROYED_GOLEM_SHARD, 500);
				if(Config.ALT_100_RECIPES)
					st.giveItems(RECIPES_100[Rnd.get(RECIPES_100.length)], 1);
				else
					st.giveItems(RECIPES_60[Rnd.get(RECIPES_60.length)], 1);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "32069-04.htm";
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		long count = st.getQuestItemsCount(DESTROYED_GOLEM_SHARD);
		if(cond == 0)
			if(st.getPlayer().getLevel() >= 46)
				htmltext = "32069-01.htm";
			else
			{
				htmltext = "32069-03.htm";
				st.exitCurrentQuest(true);
			}
		else if(cond == 1 && count < 500)
			htmltext = "32069-04.htm";
		else if(cond == 2 && count >= 500)
			htmltext = "32069-05.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 1 && st.rollAndGive(DESTROYED_GOLEM_SHARD, 1, 1, 500, DROP_CHANCE * npc.getTemplate().rateHp))
			st.set("cond", "2");
		return null;
	}
}
