package quests.qqqqq;

import l2d.extensions.scripts.ScriptFile;
import l2d.game.model.instances.L2NpcInstance;
import l2d.game.model.quest.Quest;
import l2d.game.model.quest.QuestState;

public class qqqqq extends Quest implements ScriptFile
{
	@Override
    public void onLoad()
	{}

	@Override
    public void onReload()
	{}

	@Override
    public void onShutdown()
	{}

	public qqqqq()
	{
		super(false);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getCond();
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		return null;
	}
}