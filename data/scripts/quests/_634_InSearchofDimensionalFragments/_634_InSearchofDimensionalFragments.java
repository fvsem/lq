package quests._634_InSearchofDimensionalFragments;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _634_InSearchofDimensionalFragments extends Quest implements ScriptFile
{
	int DIMENSION_FRAGMENT_ID = 7079;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 634: In Search of Dimensional Fragments");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _634_InSearchofDimensionalFragments()
	{
		super(634, 1);

		for(int npcId = 31494; npcId < 31508; npcId++)
		{
			addTalkId(npcId);
			addStartNpc(npcId);
		}

		for(int mobs = 21208; mobs < 21256; mobs++)
			addKillId(mobs);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("1"))
		{
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			htmltext = "4.htm";
			st.set("cond", "1");
		}
		else if(event.equals("2"))
		{
			htmltext = "5.htm";
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.set("cond", "0");
			htmltext = "4.htm";
		}
		else if(id == STARTED)
		{
			st.set("cond", "1");
			htmltext = "4.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		st.rollAndGive(DIMENSION_FRAGMENT_ID, 1, 60);
		return null;
	}
}
