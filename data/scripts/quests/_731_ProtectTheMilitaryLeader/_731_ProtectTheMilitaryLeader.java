package quests._731_ProtectTheMilitaryLeader;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _731_ProtectTheMilitaryLeader extends TerritoryWarSuperClass
{
	public _731_ProtectTheMilitaryLeader()
	{
		super(731);
		_npcIDs = new int[] { 36508, 36514, 36520, 36526, 36532, 36538, 36544, 36550, 36556 };
		registerAttackIds();
	}

	@Override
	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return 1 + (npcid - 36508) / 6;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
