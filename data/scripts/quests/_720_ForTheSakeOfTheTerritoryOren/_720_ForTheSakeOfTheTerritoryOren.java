package quests._720_ForTheSakeOfTheTerritoryOren;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _720_ForTheSakeOfTheTerritoryOren extends TerritoryWarSuperClass
{
	public _720_ForTheSakeOfTheTerritoryOren()
	{
		super(720);
		_catapultId = 36502;
		_territoryId = 4;
		_leaderIDs = new int[] { 36526, 36528, 36531, 36594 };
		_guardIDs = new int[] { 36527, 36529, 36530 };
		_text = new String[] { "The catapult of Oren has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
