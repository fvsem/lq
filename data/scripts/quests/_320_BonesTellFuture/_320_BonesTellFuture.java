package quests._320_BonesTellFuture;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _320_BonesTellFuture extends Quest implements ScriptFile
{
	// item
	public final int ADENA = 57;
	public final int BONE_FRAGMENT = 809;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 320: Bones Tell Future");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _320_BonesTellFuture()
	{
		super(320, -1);

		addStartNpc(30359);
		addTalkId(30359);

		addKillId(20517);
		addKillId(20518);

		addQuestItem(BONE_FRAGMENT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30359-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getRace().ordinal() != 2)
			{
				htmltext = "30359-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() >= 10)
				htmltext = "30359-03.htm";
			else
			{
				htmltext = "30359-02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(st.getQuestItemsCount(BONE_FRAGMENT) < 10)
			htmltext = "30359-05.htm";
		else
		{
			htmltext = "30359-06.htm";
			st.giveItems(ADENA, 8470, true);
			st.takeItems(BONE_FRAGMENT, -1);
			st.exitCurrentQuest(true);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		st.rollAndGive(BONE_FRAGMENT, 1, 1, 10, 10);
		if(st.getQuestItemsCount(BONE_FRAGMENT) >= 10)
			st.set("cond", "2");
		st.setState(STARTED);
		return null;
	}
}
