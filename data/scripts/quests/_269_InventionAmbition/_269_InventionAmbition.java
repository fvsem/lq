package quests._269_InventionAmbition;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * Квест Invention Ambition
 * 
 * @author Ruslaner
 * @Last_Fixes by Felixx
 */

public class _269_InventionAmbition extends Quest implements ScriptFile
{
	// NPC
	public final int INVENTOR_MARU = 32486;
	// MOBS
	public final int RED_EYE_BARBED_BAT = 21124;
	public final int UNDERGROUND_KOBOLD = 21132;
	// ITEMS
	public final int ENERGY_ORES = 10866;
	public final int ADENA = 57;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 269: Invention Ambition");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _269_InventionAmbition()
	{
		super(269, -1);
		addStartNpc(INVENTOR_MARU);
		addTalkId(INVENTOR_MARU);
		addKillId(RED_EYE_BARBED_BAT);
		addKillId(UNDERGROUND_KOBOLD);
		addQuestItem(ENERGY_ORES);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("32486-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("32486-05.htm"))
		{
			st.exitCurrentQuest(true);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(st.getState() == CREATED)
			if(st.getPlayer().getLevel() < 18)
			{
				htmltext = "32486-00.htm";
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "32486-01.htm";
		else if(st.getQuestItemsCount(ENERGY_ORES) > 0)
			htmltext = "32486-07.htm";
		if(st.getQuestItemsCount(ENERGY_ORES) >= 20)
		{
			int bonus = 2044;
			st.giveItems(ADENA, ENERGY_ORES * 50 + bonus);
			st.takeItems(ENERGY_ORES, -1);
		}
		else
			htmltext = "32486-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED && st.getInt("cond") != 1)
			return null;
		if(Rnd.get(10) < 6)
		{
			st.giveItems(ENERGY_ORES, 1);
			st.playSound(SOUND_ITEMGET);
		}
		return null;
	}
}
