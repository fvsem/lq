package quests._002_WhatWomenWant;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Рейты учтены
 */
public class _002_WhatWomenWant extends Quest implements ScriptFile
{
	private static int ARUJIEN = 30223;
	private static int MIRABEL = 30146;
	private static int HERBIEL = 30150;
	private static int GREENIS = 30157;

	private static int ARUJIENS_LETTER1 = 1092;
	private static int ARUJIENS_LETTER2 = 1093;
	private static int ARUJIENS_LETTER3 = 1094;
	private static int POETRY_BOOK = 689;
	private static int GREENIS_LETTER = 693;

	private static int BEGINNERS_POTION = 1073;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _002_WhatWomenWant()
	{
		super(2, -1);
		addStartNpc(ARUJIEN);

		addTalkId(MIRABEL);
		addTalkId(HERBIEL);
		addTalkId(GREENIS);

		addQuestItem(new int[] { GREENIS_LETTER, ARUJIENS_LETTER3, ARUJIENS_LETTER1, ARUJIENS_LETTER2, POETRY_BOOK });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30223-04.htm"))
		{
			st.giveItems(ARUJIENS_LETTER1, 1, false);
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30223-08.htm"))
		{
			st.takeItems(ARUJIENS_LETTER3, -1);
			st.giveItems(POETRY_BOOK, 1, false);
			st.set("cond", "4");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30223-10.htm"))
		{
			st.takeItems(ARUJIENS_LETTER3, -1);
			st.giveItems(ADENA_ID, 450, true);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == ARUJIEN)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 1 && st.getPlayer().getRace().ordinal() != 0)
					htmltext = "30223-00.htm";
				else if(st.getPlayer().getLevel() >= 2)
					htmltext = "30223-02.htm";
				else
				{
					htmltext = "30223-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && st.getQuestItemsCount(ARUJIENS_LETTER1) > 0)
				htmltext = "30223-05.htm";
			else if(cond == 2 && st.getQuestItemsCount(ARUJIENS_LETTER2) > 0)
				htmltext = "30223-06.htm";
			else if(cond == 3 && st.getQuestItemsCount(ARUJIENS_LETTER3) > 0)
				htmltext = "30223-07.htm";
			else if(cond == 4 && st.getQuestItemsCount(POETRY_BOOK) > 0)
				htmltext = "30223-11.htm";
			else if(cond == 5 && st.getQuestItemsCount(GREENIS_LETTER) > 0)
			{
				htmltext = "30223-10.htm";
				st.takeItems(GREENIS_LETTER, -1);
				st.giveItems(BEGINNERS_POTION, 5, true);
				st.unset("cond");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == MIRABEL)
		{
			if(cond == 1 && st.getQuestItemsCount(ARUJIENS_LETTER1) > 0)
			{
				htmltext = "30146-01.htm";
				st.takeItems(ARUJIENS_LETTER1, -1);
				st.giveItems(ARUJIENS_LETTER2, 1, false);
				st.set("cond", "2");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 2)
				htmltext = "30146-02.htm";
		}
		else if(npcId == HERBIEL)
		{
			if(cond == 2 && st.getQuestItemsCount(ARUJIENS_LETTER2) > 0)
			{
				htmltext = "30150-01.htm";
				st.takeItems(ARUJIENS_LETTER2, -1);
				st.giveItems(ARUJIENS_LETTER3, 1, false);
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 3)
				htmltext = "30150-02.htm";
		}
		else if(npcId == GREENIS)
			if(cond == 4 && st.getQuestItemsCount(POETRY_BOOK) > 0)
			{
				htmltext = "30157-02.htm";
				st.takeItems(POETRY_BOOK, -1);
				st.giveItems(GREENIS_LETTER, 1, false);
				st.set("cond", "5");
				st.playSound(SOUND_MIDDLE);
			}
			else if(cond == 5 && st.getQuestItemsCount(GREENIS_LETTER) > 0)
				htmltext = "30157-03.htm";
			else
				htmltext = "30157-01.htm";
		return htmltext;
	}
}
