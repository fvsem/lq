package quests.TerritoryWarSuperClass;

import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.siege.territory.TerritorySiege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.util.ArrayUtil;

/**
 * <ul type="square">
 * <li>For the Sake of the Territory - Уничтожьте вражеские Territory Catapult, Supplies Safe, Military, Religious и Economic Association Leader'ов</li>
 * <li>Protect the territory catapult - Обороняйте свою баллисту</li>
 * <li>Protect the Supplies Safe - Не дайте врагу уничтожить ваш Склад со снабжением</li>
 * <li>Protect the Military Association Leader - Защитите своего NPC</li>
 * <li>Protect the Religious Association Leader - Защитите своего NPC</li>
 * <li>Protect the Economic Association Leader - Защитите своего NPC</li>
 * <li>Weaken Defense - убейте 7 игроков класса "Knight" 61+</li>
 * <li>Weaken Attack - убейте 14 игроков класса "Warriors & Rogues" 61+</li>
 * <li>Weaken Magic - убейте 13 игроков класса "Wizard & Summoner" 61+</li>
 * <li>Weaken Support - убейте 9 игроков класса "Wizard & Priests" 61+</li>
 * <li>Weaken Professionals - убейте 6 игроков класса "Warsmith & Overlord" 61+</li>
 * </ul>
 * <br>
 * Обычные - это квесты работающие по стандартной схеме. Взять их можно в любой момент у Mercenary Captain и Rapidus. Выполнять их можно только во время территориальных войн. В том случае, если вы не успели завершить квест, он не аннулируется,
 * тоесть, вы сможете продолжить завершение на следующих территориальных войнах.<br>
 * <ul type="square">
 * <li>Path to Becoming an Elite Mercenary - Уничтожьте 10 игроков и 1 баллисту</li>
 * <li>Path to Becoming a Top Elite Mercenary - Уничтожьте 30 игроков и 2 баллисты</li>
 * <li>Steps for honor - Состоит из 4 частей, в каждой части нужно убить по 9, 18, 27, 36 игроков 61+ соотвественно</li>
 * </ul>
 * 
 * @<!-- L2System -->
 * @date 25.01.2011
 * @time 17:23:39
 */
public class TerritoryWarSuperClass extends Quest implements ScriptFile
{
	private final static TIntObjectHashMap<Quest> _forTheSakeScripts = new TIntObjectHashMap<Quest>();
	private final static TIntObjectHashMap<Quest> _protectTheScripts = new TIntObjectHashMap<Quest>();

	// "For the Sake of the Territory ..." quests variables
	protected int _territoryId;
	protected int _catapultId;
	protected int[] _leaderIDs;
	protected int[] _guardIDs;
	protected String[] _text = {};

	// "Protect the ..." quests variables
	protected int[] _npcIDs;

	public TerritoryWarSuperClass(int id)
	{
		super(id, -1);
		// "For The Sake " quests
		if(id >= 717 && id <= 725)
			_forTheSakeScripts.put(id - 716, this);
		// "Protect the " quests
		else if(id >= 729 && id <= 733)
			_protectTheScripts.put(id, this);
	}

	public TerritoryWarSuperClass()
	{
		super(-1, -1);
		addEventId(ScriptEventType.ON_ENTER_WORLD);
	}

	/**
	 * Used to register NPCs "For the Sake of the Territory ..." quests
	 */
	protected void registerKillIds()
	{
		addKillId(_catapultId);
		addKillId(_leaderIDs);
		addKillId(_guardIDs);
	}

	/**
	 * Used to register NPCs "Protect the ..." quests
	 */
	protected void registerAttackIds()
	{
		addAttackId(_npcIDs);
	}

	public int getTerritoryIdForThisNPCId(int npcid)
	{
		return 0;
	}

	// "Protect the" quests
	@Override
	public String onAttack(L2NpcInstance npc, QuestState qs)
	{
		// _log.info("onAttack " + qs.getPlayer());
		if(npc.getCurrentHp() == npc.getMaxHp() && ArrayUtil.arrayContains(_npcIDs, npc.getNpcId()))
		{
			int territoryId = getTerritoryIdForThisNPCId(npc.getNpcId());
			// _log.info("onAttack " + territoryId);
			if(territoryId >= 1 && territoryId <= 9)
				for(L2Player pl : L2ObjectsStorage.getAllPlayersForIterate())
					if(pl.getTerritorySiege() == territoryId)
					{
						QuestState st = pl.getQuestState(getName());
						if(st == null)
							st = newQuestState(pl, Quest.CREATED);
						if(st.getState() != STARTED)
						{
							st.set("cond", 1);
							st.setStateAndNotSave(STARTED);
						}
					}
		}
		return super.onAttack(npc, qs);
	}

	// тупо выдача баджей за убийство
	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(TerritorySiege.isInProgress() && npc != null)
		{
			L2Player killer = qs.getPlayer();
			if(npc.getNpcId() == _catapultId)
			{
				// _log.info("catapultId " + killer.getName() + ": " + killer.getTerritorySiege());
				TerritorySiege.catapultDestroyed(_territoryId);
				TerritorySiege.giveBadges(killer, _territoryId, 2);
				TerritorySiege.announceToPlayer(new ExShowScreenMessage(_text[0], 10000, ExShowScreenMessage.ScreenMessageAlign.MIDDLE_CENTER, true, 1, -1, false), true);
			}
			else if(ArrayUtil.arrayContains(_leaderIDs, npc.getNpcId()))
				// _log.info("leaderIDs " + killer.getName() + ": " + killer.getTerritorySiege());
				TerritorySiege.giveBadges(killer, _territoryId, 1);
		}

		return super.onKill(npc, qs);
	}

	/**
	 * Сбрасываем либо даём заново квесты из серии "For the Sake of the Territory ..."
	 */
	@Override
	protected void onEnterWorld(L2Player player)
	{
		if(TerritorySiege.isInProgress())
			// срабатывает если игроку присвоена терретория
			if(player != null && player.getTerritorySiege() > 0)
			{
				// register Territory Quest
				Quest quest = _forTheSakeScripts.get(player.getTerritorySiege());
				if(quest != null)
				{
					QuestState st = player.getQuestState(quest.getName());
					if(st == null)
						st = quest.newQuestState(player, CREATED);
					st.set("cond", "1");
					st.setStateAndNotSave(STARTED);
				}
			}
	}

	@Override
	public void onChange(boolean end)
	{
		Quest quest;
		QuestState st;
		if(end) // при окончании ТВ
		{
			for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null && player.getTerritorySiege() > -1)
				{
					quest = _forTheSakeScripts.get(player.getTerritorySiege());
					if(quest == null)
						continue;

					st = player.getQuestState(quest.getName());
					if(st != null)
						st.exitCurrentQuest(false);

					for(Quest q : _protectTheScripts.valueCollection())
						if((st = player.getQuestState(q.getName())) != null)
							st.exitCurrentQuest(false);
				}
		}
		else
			for(L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
				if(player != null && player.getTerritorySiege() > -1)
				{
					quest = _forTheSakeScripts.get(player.getTerritorySiege());
					if(quest == null)
						continue;

					st = player.getQuestState(quest.getName());
					if(st == null)
						st = quest.newQuestState(player, Quest.CREATED);
					if(st.getState() != STARTED)
					{
						st.set("cond", 1);
						st.setStateAndNotSave(STARTED);
					}
				}
	}

	public static boolean checkPlayers(L2Player killed, L2Player killer)
	{
		if(killer.getTerritorySiege() < 0 || killed.getTerritorySiege() < 0 || killer.getTerritorySiege() == killed.getTerritorySiege())
			return false;
		if(killer.getParty() != null && killer.getParty() == killed.getParty())
			return false;
		if(killer.getClan() != null && killer.getClan() == killed.getClan())
			return false;
		if(killer.getAllyId() > 0 && killer.getAllyId() == killed.getAllyId())
			return false;
		if(killer.getLevel() < 61 || killed.getLevel() < 61)
			return false;
		return true;
	}

	@Override
	public void onLoad()
	{}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
