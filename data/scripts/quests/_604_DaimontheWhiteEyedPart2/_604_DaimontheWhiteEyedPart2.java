package quests._604_DaimontheWhiteEyedPart2;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ServerVariables;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _604_DaimontheWhiteEyedPart2 extends Quest implements ScriptFile
{
	// NPC
	private static final int EYE = 31683;
	private static final int ALTAR = 31541;
	// MOBS
	private static final int DAIMON = 25290;
	// ITEMS
	private static final int U_SUMMON = 7192;
	private static final int S_SUMMON = 7193;
	private static final int ESSENCE = 7194;

	// REWARDS dye +2int-2men/+2int-2wit/+2men-2int/+2men-2wit/+2wit-2int/+2wit-2men
	private static final int INT_MEN = 4595;
	private static final int INT_WIT = 4596;
	private static final int MEN_INT = 4597;
	private static final int MEN_WIT = 4598;
	private static final int WIT_INT = 4599;
	private static final int WIT_MEN = 4600;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _604_DaimontheWhiteEyedPart2()
	{
		super(604, 1);
		addStartNpc(EYE);
		addTalkId(EYE);
		addTalkId(ALTAR);
		addKillId(DAIMON);
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		final L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(DAIMON);
		String htmltext = event;
		if(event.equalsIgnoreCase("31683-02.htm"))
		{
			if(st.getPlayer().getLevel() < 73)
			{
				htmltext = "31683-00b.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.takeItems(U_SUMMON, 1);
				st.giveItems(S_SUMMON, 1);
				st.playSound(SOUND_ACCEPT);
			}
		}
		else if(event.equalsIgnoreCase("31541-02.htm"))
		{
			if(st.getQuestItemsCount(S_SUMMON) == 0)
				htmltext = "31541-04.htm";
			if(isQuest != null)
				return "31541-03.htm";
			if(ServerVariables.getLong(_604_DaimontheWhiteEyedPart2.class.getSimpleName(), 0) + 3 * 60 * 60 * 1000 > System.currentTimeMillis())
				return "31541-05.htm";
			else
			{
				st.takeItems(S_SUMMON, 1);
				st.addSpawn(DAIMON, 186320, -43904, -3175);
				Functions.npcShout(isQuest, "I will crush you!");
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
				st.setState(STARTED);
				st.getPlayer().sendMessage("Daimon the White-Eyed has despawned in 186320, -43904, -3175");
				st.startQuestTimer("DAIMON_Fail", 12000000);
			}
		}
		else if(event.equalsIgnoreCase("31683-04.htm"))
		{
			if(st.getQuestItemsCount(ESSENCE) >= 1)
				htmltext = "list.htm";
			else
			{
				htmltext = "31683-05.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(event.equalsIgnoreCase("INT_MEN"))
		{
			st.giveItems(INT_MEN, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("INT_WIT"))
		{
			st.giveItems(INT_WIT, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("MEN_INT"))
		{
			st.giveItems(MEN_INT, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("MEN_WIT"))
		{
			st.giveItems(MEN_WIT, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("WIT_INT"))
		{
			st.giveItems(WIT_INT, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("WIT_MEN"))
		{
			st.giveItems(WIT_MEN, 5);
			st.takeItems(ESSENCE, 1);
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(true);
			htmltext = null;
		}
		else if(event.equalsIgnoreCase("DAIMON_Fail"))
			if(isQuest != null)
				isQuest.deleteMe();
		return htmltext;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		final L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(DAIMON);
		String htmltext = "noquest";
		final int npcId = npc.getNpcId();
		final int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(npcId == EYE)
				if(st.getQuestItemsCount(U_SUMMON) >= 1)
					htmltext = "31683-01.htm";
				else
					htmltext = "31683-00a.htm";
		}
		else if(cond == 1)
		{
			if(npcId == EYE)
				htmltext = "31683-02a.htm";
			else if(npcId == ALTAR)
				if(ServerVariables.getLong(_604_DaimontheWhiteEyedPart2.class.getSimpleName(), 0) + 3 * 60 * 60 * 1000 > System.currentTimeMillis())
					htmltext = "31541-05.htm";
				else
					htmltext = "31541-01.htm";
		}
		else if(cond == 2)
		{
			if(npcId == ALTAR)
				if(isQuest != null)
					htmltext = "31541-03.htm";
				else if(ServerVariables.getLong(_604_DaimontheWhiteEyedPart2.class.getSimpleName(), 0) + 3 * 60 * 60 * 1000 > System.currentTimeMillis())
					htmltext = "31541-05.htm";
				else
				{
					st.addSpawn(DAIMON, 186320, -43904, -3175);
					Functions.npcShout(isQuest, "I will crush you!");
					st.playSound(SOUND_MIDDLE);
					st.setState(STARTED);
					st.getPlayer().sendMessage("Daimon the White-Eyed has despawned in 186320, -43904, -3175");
					st.startQuestTimer("DAIMON_Fail", 12000000);
				}
		}
		else if(cond == 3)
		{
			if(npcId == EYE)
				if(st.getQuestItemsCount(ESSENCE) >= 1)
					htmltext = "31683-03.htm";
				else
					htmltext = "31683-06.htm";
			if(npcId == ALTAR)
				htmltext = "31541-05.htm";
		}
		return htmltext;
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		ServerVariables.set(_604_DaimontheWhiteEyedPart2.class.getSimpleName(), String.valueOf(System.currentTimeMillis()));
		if(st.getQuestItemsCount(S_SUMMON) > 0)
		{
			st.takeItems(S_SUMMON, 1);
			st.giveItems(ESSENCE, 1);
			st.set("cond", "3");
			st.setState(STARTED);
			st.playSound("ItemSound.quest_middle");
		}
		return null;
	}
}
