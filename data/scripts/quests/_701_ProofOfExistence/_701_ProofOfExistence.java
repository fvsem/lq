package quests._701_ProofOfExistence;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _701_ProofOfExistence extends Quest implements ScriptFile
{
	// Npc
	private static final int ARTIUS = 32559;

	// Quest_items
	private static final int DEADMANS_REMAINS = 13875;
	private static final int BANSHEE_QUEENS_EYE = 13876;

	// MOBS
	private static final int[] MOBS = { 22606, 22607, 22608, 22609 };
	private static final int ENIRA = 25625;

	// Settings
	private static final int DROP_CHANCE1 = 80;
	private static final int DROP_CHANCE2 = 75;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _701_ProofOfExistence()
	{
		super(701, PARTY_NONE);
		addStartNpc(ARTIUS);
		addKillId(MOBS);
		addKillId(ENIRA);
		addQuestItem(DEADMANS_REMAINS, BANSHEE_QUEENS_EYE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32559-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32559-quit.htm"))
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();

		if(npcId == ARTIUS)
		{
			QuestState qs = player.getQuestState("_10273_GoodDayToFly");
			if(qs != null && qs.isCompleted() && st.getState() == CREATED && player.getLevel() >= 78)
				htmltext = "32559-01.htm";
			else if(cond == 1)
			{
				long Deadman = st.getQuestItemsCount(DEADMANS_REMAINS);
				long Banshee = st.getQuestItemsCount(BANSHEE_QUEENS_EYE);
				if(Deadman + Banshee > 0)
				{
					st.giveItems(ADENA_ID, 2500 * Deadman + 2500 * Banshee);
					st.takeItems(DEADMANS_REMAINS, -1);
					st.takeItems(BANSHEE_QUEENS_EYE, -1);
					htmltext = "32559-04.htm";
				}
				else
					htmltext = "32559-03.htm";
			}
			else if(cond == 0)
			{
				htmltext = "32559-00.htm";
				st.exitCurrentQuest(true);
			}
		}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(ArrayUtil.arrayContains(MOBS, npcId))
			st.rollAndGive(DEADMANS_REMAINS, 1, DROP_CHANCE1);
		else if(npcId == ENIRA && st.getQuestItemsCount(BANSHEE_QUEENS_EYE) == 0)
			st.rollAndGive(BANSHEE_QUEENS_EYE, 1, DROP_CHANCE2);
		return null;
	}
}
