package quests._642_APowerfulPrimevalCreature;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _642_APowerfulPrimevalCreature extends Quest implements ScriptFile
{
	// NPCs
	private final static int Dinn = 32105;
	// Mobs
	private final static int Ancient_Egg = 18344;
	private final static int[] Dino = {
			22196,
			22197,
			22198,
			22199,
			22200,
			22201,
			22202,
			22203,
			22204,
			22205,
			22218,
			22219,
			22220,
			22223,
			22224,
			22225,
			22226,
			22227,
			22742,
			22743,
			22744,
			22745 };
	// Items
	private final static int[] Rewards = { 8690, 8692, 8694, 8696, 8698, 8700, 8702, 8704, 8706, 8708, 8710 };
	private final static int[] Rewards80 = { 9967, 9968, 9969, 9970, 9971, 9972, 9973, 9974, 9975, 10544 };
	// Quest Items
	private final static short Dinosaur_Tissue = 8774;
	private final static short Dinosaur_Egg = 8775;
	// Chances
	private final static int Dinosaur_Tissue_Chance = 33;
	private final static int Dinosaur_Egg_Chance = 1;

	public _642_APowerfulPrimevalCreature()
	{
		super(642, PARTY_ONE);
		addStartNpc(Dinn);
		addKillId(Ancient_Egg);
		addKillId(Dino);
		addQuestItem(Dinosaur_Tissue);
		addQuestItem(Dinosaur_Egg);
	}

	@Override
	public String onEvent(final String event, final QuestState st, final L2NpcInstance npc)
	{
		final int _state = st.getState();
		final long Dinosaur_Tissue_Count = st.getQuestItemsCount(Dinosaur_Tissue);
		if(event.equalsIgnoreCase("dindin_q0642_04.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("dindin_q0642_12.htm") && _state == STARTED)
		{
			if(Dinosaur_Tissue_Count == 0)
				return "dindin_q0642_08a.htm";
			st.takeItems(Dinosaur_Tissue, -1);
			st.giveItems(ADENA_ID, Dinosaur_Tissue_Count * 3000);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("dindin_q0642_450.htm") && _state == STARTED && Config.ALT_100_RECIPES)
			return "dindin_q0642_450.htm";
		else if(event.equalsIgnoreCase("0"))
			return null;
		else if(_state == STARTED)
			try
			{
				final int rew80_id = Integer.valueOf(event);
				if(Dinosaur_Tissue_Count < 150) // || st.getQuestItemsCount(Dinosaur_Egg) < 0)
					return "dindin_q0642_08a.htm";
				for(final int reward80 : Rewards80)
					if(reward80 == rew80_id)
					{
						st.takeItems(Dinosaur_Tissue, 450);
						st.giveItems(Config.ALT_100_RECIPES ? reward80 + 1 : reward80, 1, true);
						st.playSound(SOUND_MIDDLE);
						return "dindin_q0642_12.htm";
					}
				final int rew_id = Integer.valueOf(event);
				for(final int reward : Rewards)
					if(reward == rew_id)
					{
						st.takeItems(Dinosaur_Tissue, 150);
						st.takeItems(Dinosaur_Egg, 1);
						st.giveItems(Config.ALT_100_RECIPES ? reward + 1 : reward, 1, true);
						st.giveItems(ADENA_ID, 44000);
						st.playSound(SOUND_MIDDLE);
						return "dindin_q0642_12.htm";
					}
				return null;

			}
			catch(final Exception E)
			{}

		return event;
	}

	@Override
	public String onTalk(final L2NpcInstance npc, final QuestState st)
	{
		if(npc.getNpcId() != Dinn)
			return "noquest";
		final int _state = st.getState();
		if(_state == CREATED)
		{
			if(st.getPlayer().getLevel() < 75)
			{
				st.exitCurrentQuest(true);
				return "dindin_q0642_01a.htm";
			}
			st.set("cond", "0");
			return "dindin_q0642_01.htm";
		}
		if(_state == STARTED)
		{
			final long Dinosaur_Tissue_Count = st.getQuestItemsCount(Dinosaur_Tissue);
			if(Dinosaur_Tissue_Count == 0)
				return "dindin_q0642_08a.htm";
			if(Dinosaur_Tissue_Count < 150 || st.getQuestItemsCount(Dinosaur_Egg) == 0)
				return "dindin_q0642_07.htm";
			return "dindin_q0642_07a.htm";
		}

		return "noquest";
	}

	@Override
	public String onKill(final L2NpcInstance npc, final QuestState st)
	{
		if(st.getState() != STARTED || st.getInt("cond") != 1)
			return null;
		if(npc.getNpcId() == Ancient_Egg)
			st.rollAndGive(Dinosaur_Egg, 1, Dinosaur_Egg_Chance);
		else
			st.rollAndGive(Dinosaur_Tissue, 1, Dinosaur_Tissue_Chance);
		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
