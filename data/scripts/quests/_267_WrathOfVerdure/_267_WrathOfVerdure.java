package quests._267_WrathOfVerdure;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _267_WrathOfVerdure extends Quest implements ScriptFile
{
	// NPCs
	private static int Treant_Bremec = 31853;
	// Mobs
	private static int Goblin_Raider = 20325;
	// Quest Items
	private static int Goblin_Club = 1335;
	// Items
	private static int Silvery_Leaf = 1340;
	// Chances
	private static int Goblin_Club_Chance = 50;

	public _267_WrathOfVerdure()
	{
		super(267, -1);
		addStartNpc(Treant_Bremec);
		addKillId(Goblin_Raider);
		addQuestItem(Goblin_Club);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("31853-03.htm") && _state == CREATED && st.getPlayer().getRace().ordinal() == 1 && st.getPlayer().getLevel() >= 4)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31853-06.htm") && _state == STARTED)
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		if(npc.getNpcId() != Treant_Bremec)
			return htmltext;
		int _state = st.getState();
		if(_state == CREATED)
		{
			if(st.getPlayer().getRace().ordinal() != 1)
			{
				htmltext = "31853-00.htm";
				st.exitCurrentQuest(true);
			}
			else if(st.getPlayer().getLevel() < 4)
			{
				htmltext = "31853-01.htm";
				st.exitCurrentQuest(true);
			}
			else
			{
				htmltext = "31853-02.htm";
				st.set("cond", "0");
			}
		}
		else if(_state == STARTED)
		{
			long Goblin_Club_Count = st.getQuestItemsCount(Goblin_Club);
			if(Goblin_Club_Count > 0)
			{
				htmltext = "31853-05.htm";
				st.takeItems(Goblin_Club, -1);
				st.giveItems(Silvery_Leaf, Goblin_Club_Count);
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "31853-04.htm";
		}

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;

		if(Rnd.chance(Goblin_Club_Chance))
		{
			qs.giveItems(Goblin_Club, 1);
			qs.playSound(SOUND_ITEMGET);
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 267: Wrath Of Verdure");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
