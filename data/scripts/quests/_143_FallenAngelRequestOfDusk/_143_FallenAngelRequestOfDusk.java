package quests._143_FallenAngelRequestOfDusk;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест Fallen Angel Request Of Dusk
 */
public class _143_FallenAngelRequestOfDusk extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPC
	private final static int NATOOLS = 30894;
	private final static int TOBIAS = 30297;
	private final static int CASIAN = 30612;
	private final static int ROCK = 32368;
	private final static int ANGEL = 32369;

	// ITEM
	private final static int SEALED_PATH = 10354;
	private final static int PATH = 10355;
	private final static int EMPTY_CRYSTAL = 10356;
	private final static int MEDICINE = 10357;
	private final static int MESSAGE = 10358;

	public _143_FallenAngelRequestOfDusk()
	{
		super(143, -1);

		addTalkId(NATOOLS, TOBIAS, CASIAN, ROCK, ANGEL);
		addQuestItem(SEALED_PATH, PATH, EMPTY_CRYSTAL, MEDICINE, MESSAGE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30894-01.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30894-03.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
			st.playSound("ItemSound.quest_middle");
			st.giveItems(SEALED_PATH, 1);
		}
		else if(event.equalsIgnoreCase("30297-04.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
			st.unset("talk");
			st.playSound("ItemSound.quest_middle");
			st.giveItems(PATH, 1);
			st.giveItems(EMPTY_CRYSTAL, 1);
		}
		else if(event.equalsIgnoreCase("30612-07.htm"))
		{
			st.set("cond", "4");
			st.setState(STARTED);
			st.unset("talk");
			st.giveItems(MEDICINE, 1);
			st.playSound("ItemSound.quest_middle");
		}
		else if(event.equalsIgnoreCase("32368-02.htm"))
		{
			L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(ANGEL);
			if(isQuest == null)
			{
				st.addSpawn(ANGEL);
				st.playSound(SOUND_MIDDLE);
				st.startQuestTimer("Angel_fail", 900000);
			}
			else if(st.getQuestTimer("Angel_fail") == null)
				st.startQuestTimer("Angel_fail", 900000);
		}
		else if(event.equalsIgnoreCase("32369-10.htm"))
		{
			st.set("cond", "5");
			st.setState(STARTED);
			st.unset("talk");
			st.takeItems(EMPTY_CRYSTAL, -1);
			st.giveItems(MESSAGE, 1);
			st.playSound("ItemSound.quest_middle");
		}
		else if(event.equalsIgnoreCase("HARKILGAMED_Fail"))
		{
			htmltext = null;
			L2NpcInstance isQuest = L2ObjectsStorage.getByNpcId(ANGEL);
			if(isQuest != null)
				isQuest.deleteMe();
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		QuestState ok = st.getPlayer().getQuestState("_141_ShadowFoxPart3");
		if(npcId == NATOOLS)
		{
			if(ok != null)
			{
				if(cond == 1)
					htmltext = "30894-01.htm";
				else if(cond == 2)
					htmltext = "30894-04.htm";
			}
			else
				htmltext = "need.htm";
		}
		else if(npcId == TOBIAS)
		{
			if(cond == 2)
				if(st.getInt("talk") == 1)
					htmltext = "30297-02.htm";
				else
				{
					htmltext = "30297-01.htm";
					st.takeItems(SEALED_PATH, -1);
					st.set("talk", "1");
				}
		}
		else if(cond == 3)
			htmltext = "30297-05.htm";
		else if(cond == 5)
		{
			htmltext = "30297-06.htm";
			st.playSound("ItemSound.quest_finish");
			st.exitCurrentQuest(false);
			st.giveItems(ADENA_ID, 89046);
			st.takeItems(MESSAGE, -1);
			st.unset("cond");
			if(st.getPlayer().getLevel() >= 38 && st.getPlayer().getLevel() <= 43)
				st.addExpAndSp(223036, 13901);
		}
		else if(npcId == CASIAN)
		{
			if(cond == 3)
			{
				if(st.getInt("talk") == 1)
					htmltext = "30612-02.htm";
				else
				{
					htmltext = "30612-01.htm";
					st.takeItems(PATH, -1);
					st.set("talk", "1");
				}
			}
			else if(cond == 4)
				htmltext = "30612-07.htm";
		}
		else if(npcId == ROCK)
		{
			if(cond == 4)
				htmltext = "32368-01.htm";
		}
		else if(npcId == ANGEL)
		{
			if(cond == 4)
				if(st.getInt("talk") == 1)
					htmltext = "32369-02.htm";
				else
				{
					htmltext = "32369-01.htm";
					st.takeItems(MEDICINE, -1);
					st.set("talk", "1");
				}
		}
		else if(cond == 5)
			htmltext = "32369-10.htm";
		return htmltext;
	}
}
