package quests._196_SevenSignSealOfTheEmperor;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.InstanceWorld;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.util.Location;
import l2n.util.Rnd;

/**
 * @<!-- L2System -->
 * @date 16.11.2010
 * @time 21:32:20
 */
public class _196_SevenSignSealOfTheEmperor extends Quest implements ScriptFile
{
	public static class SealOfTheEmperorWorld extends InstanceWorld
	{
		public long reflection;
	}

	/** Npc **/
	private static final int IasonHeine = 30969;
	private static final int MerchantofMammon = 32584;
	private static final int PromiseofMammon = 32585;
	private static final int Shunaiman = 32586;
	private static final int Leon = 32587;
	private static final int CourtMagician = 32598;
	private static final int DisciplesGatekeeper = 32657;
	private static final int Wood = 32593;

	/** Mob's **/
	private static final int SealDevice = 27384;
	private static final int SealDeviceDestroy = 32591;
	private static final int[] MOBS = { 27371, 27372, 27373, 27374, 27375, 27377, 27378, 27379 };

	/** Item's **/
	private static final int SacredSwordofEinhasad = 15310; // This is Weapon... ;)
	private static final int ElmoredenHolyWater = 13808; // This is Key
	private static final int CourtMagiciansMagicStaff = 13809; // This is Key
	private static final int SealofBinding = 13846;

	/** Options **/
	private static final int roomSpawnOffset = 380; // разброс спауна от центра комнаты
	private static final int roomSpawnOffset2 = 500; // разброс спауна от центра комнаты (для комнат по больше)

	private static final Location[] blocker_locs = new Location[] {
			new Location(-83177, 217353, -7520),
			new Location(-83177, 216137, -7520),
			new Location(-82588, 216754, -7520),
			new Location(-83804, 216754, -7520) };

	private static final Location merchantloc = new Location(109755, 219970, -3518);

	private long mamonStored = 0;

	private static final TIntLongHashMap _instances = new TIntLongHashMap();

	public _196_SevenSignSealOfTheEmperor()
	{
		super(196, -1);
		addStartNpc(IasonHeine);
		addTalkId(MerchantofMammon, PromiseofMammon, Shunaiman, Leon, CourtMagician, DisciplesGatekeeper, Wood);
		addKillId(SealDevice);
		addKillId(MOBS);
		addQuestItem(SealofBinding, CourtMagiciansMagicStaff, ElmoredenHolyWater, SacredSwordofEinhasad);
	}

	private boolean IsMammonSpawned()
	{
		return L2ObjectsStorage.getAsNpc(mamonStored) != null;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		L2Player player = st.getPlayer();
		if(event.equalsIgnoreCase("30969-05.htm"))
		{
			st.setCond(1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30969-06.htm"))
		{
			if(IsMammonSpawned())
				return "30969-06a.htm"; // Если мамон есть то открываем это...
			L2NpcInstance mammon = addSpawn(MerchantofMammon, merchantloc, 0, 60000);
			mamonStored = mammon.getStoredId();
			Functions.npcSayInRange(mammon, player.isLangRus() ? "Кто звал Торговца Маммона?!" : "Who dares summon the Merchant of Mammon?!", 600);
		}
		else if(event.equalsIgnoreCase("30969-11.htm"))
		{
			st.setCond(6);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32584-05.htm"))
		{
			st.setCond(2);
			st.playSound(SOUND_MIDDLE);
			// Убираем мамона чтобы небыло косяков...
			if(npc != null)
			{
				npc.deleteMe();
				mamonStored = 0;
			}
		}
		else if(event.equalsIgnoreCase("32586-06.htm"))
		{
			st.setCond(4);
			st.playSound(SOUND_MIDDLE);
			player.broadcastPacket(new ExShowScreenMessage("", 4000, ScreenMessageAlign.TOP_CENTER, true, 0, 3031, false));
			player.sendPacket(Msg.BY_USING_THE_SKILL_OF_EINHASADS_HOLY_SWORD);
			player.sendPacket(Msg.BY_USING_THE_HOLY_WATER_OF_EINHASAD);
			st.giveItems(ElmoredenHolyWater, 1, false);
			st.giveItems(SacredSwordofEinhasad, 1, false);
		}
		else if(event.equalsIgnoreCase("32586-11.htm"))
		{
			st.setCond(5);
			st.playSound(SOUND_MIDDLE);
			st.takeItems(SacredSwordofEinhasad, -1);
			st.takeItems(ElmoredenHolyWater, -1);
			st.takeItems(CourtMagiciansMagicStaff, -1);
			st.takeItems(SealofBinding, -1);
		}
		else if(event.equalsIgnoreCase("32598-02.htm"))
			st.giveItems(CourtMagiciansMagicStaff, 1, false);
		else if(event.equalsIgnoreCase("30969-10.htm"))
		{
			st.setCond(6);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32593-02.htm"))
		{
			st.addExpAndSp(52518015, 5817677);
			st.setState(COMPLETED);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		final int npcId = npc.getNpcId();
		final int cond = st.getInt("cond");
		final int state = st.getState();
		final L2Player player = st.getPlayer();
		final Reflection r = player.getReflection();
		final SealOfTheEmperorWorld world = InstanceManager.getInstance().getWorld(player.getReflectionId(), SealOfTheEmperorWorld.class);
		if(npcId == IasonHeine)
		{
			if(state == CREATED)
			{
				if(player.getLevel() < 79)
				{
					st.exitCurrentQuest(true);
					return "30969-00.htm";
				}
				QuestState qs = st.getPlayer().getQuestState("_195_SevenSignsSecretRitualPriests");
				if((qs == null || !qs.isCompleted()) && !player.isGM())
				{
					st.exitCurrentQuest(true);
					return "noquest";
				}
				return "30969-01.htm";
			}
			if(cond == 1)
				return "30969-05.htm";
			else if(cond == 2)
			{
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
				return "30969-07.htm";
			}
			else if(cond == 3)
				return "30969-07a.htm";
			else if(cond == 5)
				return "30969-08.htm";
			else if(cond == 6)
				return "30969-11.htm";
		}
		if(npcId == MerchantofMammon)
			if(cond == 1)
				return "32584-01.htm";
		if(npcId == PromiseofMammon)
			if(cond >= 3 && cond <= 5)
			{
				enterInstance(player);
				return null;
			}
		if(npcId == Leon)
			if(cond >= 3)
			{
				if(r.getReturnLoc() != null)
					player.teleToLocation(r.getReturnLoc(), 0);
				else
					player.setReflection(0);
				st.takeItems(SacredSwordofEinhasad, -1);
				player.unsetVar("backCoords");
				return "32587-00.htm";
			}
		if(npcId == Shunaiman)
			if(cond == 3)
				return "32586-01.htm";
			else if(cond == 4)
			{
				if(st.getQuestItemsCount(SealofBinding) >= 4)
					return "32586-07.htm";
				else if(st.getQuestItemsCount(SacredSwordofEinhasad) >= 1)
					return "32586-06a.htm";
				else
				{
					player.broadcastPacket(new ExShowScreenMessage("", 4000, ScreenMessageAlign.TOP_CENTER, true, 0, 3031, false));
					player.sendPacket(Msg.BY_USING_THE_SKILL_OF_EINHASADS_HOLY_SWORD);
					st.giveItems(SacredSwordofEinhasad, 1, false);
					return "32586-06b.htm";
				}
			}
			else if(cond == 5)
				return "32586-11b.htm";
		if(npcId == CourtMagician && cond == 4)
			if(st.getQuestItemsCount(CourtMagiciansMagicStaff) >= 1)
			{
				player.sendPacket(Msg.BY_USING_THE_COURT_MAGICIAN_MAGIC_STAFF);
				return "32598-02a.htm";
			}
			else
			{
				player.sendPacket(Msg.BY_USING_THE_COURT_MAGICIAN_MAGIC_STAFF);
				return "32598-01.htm";
			}
		if(npcId == DisciplesGatekeeper)
			if(cond == 4 && r.getInstancedZoneId() == ReflectionTable.DISCIPLES_NECROPOLIS)
			{
				r.openDoor(17240111);
				player.showQuestMovie(ExStartScenePlayer.SCENE_SSQ_SEALING_EMPEROR_1ST);
				player.sendPacket(Msg.IN_ORDER_TO_HELP_ANAKIM);

				st.takeItems(SealofBinding, -1);

				// Таймер для спауна Анаким и Лилит (Нужен для того чтобы не показывало их во время ролика...)
				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						L2NpcInstance lilith = addSpawnToInstance(32715, new Location(-83176, 217048, -7488, 0), 0, world.reflection);
						// Высталяем миньёнов лилит с разбросом в 200
						addSpawnToInstance(32716, lilith.getLoc(), Rnd.get(100, 300), world.reflection);
						addSpawnToInstance(32717, lilith.getLoc(), Rnd.get(1, 300), world.reflection);
						L2NpcInstance anakim = addSpawnToInstance(32718, new Location(-83176, 216472, -7488, 0), 0, world.reflection);
						// Выставляем миньёнов анаким с разбросом в 200
						addSpawnToInstance(32719, anakim.getLoc(), Rnd.get(1, 300), world.reflection);
						addSpawnToInstance(32720, anakim.getLoc(), Rnd.get(1, 300), world.reflection);
						addSpawnToInstance(32721, anakim.getLoc(), Rnd.get(1, 300), world.reflection);

						// Seals
						for(Location loc : blocker_locs)
							addSpawnToInstance(SealDevice, loc, false, world.reflection);
					}
				}, 18000);
				return null;
			}
		if(npcId == Wood)
			if(cond == 6)
				return "32593-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		final L2Player player = st.getPlayer();
		final SealOfTheEmperorWorld world = InstanceManager.getInstance().getWorld(player.getReflectionId(), SealOfTheEmperorWorld.class);
		if(world == null)
			return null;

		if(npc.getNpcId() == SealDevice)
		{
			st.giveItems(SealofBinding, 1, false);
			addSpawnToInstance(SealDeviceDestroy, npc.getLoc(), 0, player.getReflectionId());
			npc.deleteMe();
			// Если у персонажа 4 или больше предмета то запускаем ролик и таймер для телепортации
			if(st.getQuestItemsCount(SealofBinding) < 4)
				st.playSound(SOUND_ITEMGET);
			else
			{
				st.playSound(SOUND_MIDDLE);
				player.sendPacket(Msg.THE_SEALING_DEVICE_GLITTERS_AND_MOVES);
				player.showQuestMovie(ExStartScenePlayer.SCENE_SSQ_SEALING_EMPEROR_2ND);
				// Телепортирует персонажа через 28 секунд с начала ролика... (ролик идёт 26 секунд)
				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						player.teleToLocation(new Location(-89559, 216030, -7488));
					}
				}, 28000);
			}
		}

		return null;
	}

	private void enterInstance(L2Player player)
	{
		int instancedZoneId = ReflectionTable.DISCIPLES_NECROPOLIS;
		InstanceManager ilm = InstanceManager.getInstance();
		TIntObjectHashMap<Instance> ils = ilm.getById(instancedZoneId);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);
		assert il != null;

		if(player.isInParty())
		{
			player.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		// Првоеряем если инстанс для этого игрока уже был создан
		final SealOfTheEmperorWorld oldworld = InstanceManager.getInstance().getWorld(_instances.get(player.getObjectId()), SealOfTheEmperorWorld.class);
		if(oldworld != null)
		{
			Reflection old_r = ReflectionTable.getInstance().get(oldworld.reflection);
			if(old_r != null)
			{
				player.setReflection(old_r);
				player.teleToLocation(il.getTeleportCoords());
				player.setVar("backCoords", old_r.getReturnLoc().toXYZString());
				return;
			}
		}

		Reflection r = new Reflection(il.getName());
		r.setInstancedZoneId(instancedZoneId);
		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		// init
		SealOfTheEmperorWorld world = new SealOfTheEmperorWorld();
		world.reflection = r.getId();
		InstanceManager.getInstance().addWorld(r.getId(), world);
		runTheSanctum(world);

		int timelimit = il.getDuration();

		player.setReflection(r);
		player.teleToLocation(il.getTeleportCoords());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());

		r.setEmptyDestroyTime(10 * 60 * 1000);
		r.startCollapseTimer(timelimit * 60 * 1000);
		synchronized (_instances)
		{
			_instances.put(player.getObjectId(), r.getId());
		}
	}

	private void runTheSanctum(SealOfTheEmperorWorld world)
	{
		// Комната 1
		/******************************
		 * - Lilim Butcher = 1
		 * - Lilim Magus = 1
		 * - Lilim Slave Knight = 1
		 * - Shilen's Evil Thoughts = 1
		 ******************************/
		Location center = new Location(-89240, 217928, -7517, 0);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		// Комната 2
		/******************************
		 * - Lilim Butcher = 2
		 * - Lilim Magus = 1
		 * - Lilim Slave Knight = 2
		 * - Shilen's Evil Thoughts = 1
		 ******************************/
		center = new Location(-88600, 220264, -7517, 0);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		// Комната 3
		/******************************
		 * - Lilim Butcher = 2
		 * - Lilim Magus = 2
		 * - Lilim Slave Knight = 2
		 * - Shilen's Evil Thoughts = 2
		 ******************************/
		center = new Location(-87032, 220632, -7517, 0);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		// Комната 4
		/******************************
		 * - Lilim Assassin = 1
		 * - Lilim Guard Knight = 1
		 * - Lilim Butcher = 1
		 * - Lilim Magus = 1
		 * - Lilim Great Magus = 1
		 * - Lilim Slave Knight = 2
		 * - Shilen's Evil Thoughts = 2
		 * - Shilen's Evil Thoughts = 1(мелкий)
		 ******************************/
		center = new Location(-85352, 219224, -7517, 0);
		addSpawnToInstance(27371, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27375, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27377, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27378, center, roomSpawnOffset, world.reflection);
		addSpawnToInstance(27379, center, roomSpawnOffset, world.reflection);
		// Комната 5
		/******************************
		 * - Lilim Assassin = 2
		 * - Lilim Guard Knight = 2
		 * - Lilim Butcher = 1
		 * - Lilim Magus = 1
		 * - Lilim Great Magus = 2
		 * - Lilim Slave Knight = 1
		 * - Shilen's Evil Thoughts = 1
		 * - Shilen's Evil Thoughts = 2(мелкий)
		 ******************************/
		center = new Location(-87448, 217608, -7517, 0);
		addSpawnToInstance(27371, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27372, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27373, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27374, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27375, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27375, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27377, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27377, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27378, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27378, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27379, center, roomSpawnOffset2, world.reflection);
		addSpawnToInstance(27379, center, roomSpawnOffset2, world.reflection);
	}

	@Override
	public void onAbort(QuestState qs)
	{
		// при отмене квеста закрываем инстанс если он есть
		L2Player player = qs.getPlayer();
		if(player != null)
		{
			long refId;
			if((refId = _instances.remove(player.getObjectId())) > 0)
			{
				Reflection old_r = ReflectionTable.getInstance().get(refId);
				if(old_r != null)
					old_r.startCollapseTimer(10000);
			}
		}
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

}
