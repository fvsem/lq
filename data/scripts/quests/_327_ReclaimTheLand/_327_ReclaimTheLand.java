package quests._327_ReclaimTheLand;

import javolution.util.FastMap;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Drop;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _327_ReclaimTheLand extends Quest implements ScriptFile
{
	// NPCs
	private static int Piotur = 30597;
	private static int Iris = 30034;
	private static int Asha = 30313;
	// Items
	private static int ADENA = 57;
	// Quest Items
	private static short TUREK_DOGTAG = 1846;
	private static short TUREK_MEDALLION = 1847;
	private static short CLAY_URN_FRAGMENT = 1848;
	private static short BRASS_TRINKET_PIECE = 1849;
	private static short BRONZE_MIRROR_PIECE = 1850;
	private static short JADE_NECKLACE_BEAD = 1851;
	private static short ANCIENT_CLAY_URN = 1852;
	private static short ANCIENT_BRASS_TIARA = 1853;
	private static short ANCIENT_BRONZE_MIRROR = 1854;
	private static short ANCIENT_JADE_NECKLACE = 1855;
	// Chances
	private static int Exchange_Chance = 80;

	private static FastMap<Integer, Drop> DROPLIST = new FastMap<Integer, Drop>();
	private static FastMap<Short, Integer> EXP = new FastMap<Short, Integer>();

	public _327_ReclaimTheLand()
	{
		super(327, -1);
		addStartNpc(Piotur);
		addTalkId(Iris);
		addTalkId(Asha);

		DROPLIST.put(20495, new Drop(1, 0xFFFF, 13).addItem(TUREK_MEDALLION));
		DROPLIST.put(20496, new Drop(1, 0xFFFF, 9).addItem(TUREK_DOGTAG));
		DROPLIST.put(20497, new Drop(1, 0xFFFF, 11).addItem(TUREK_MEDALLION));
		DROPLIST.put(20498, new Drop(1, 0xFFFF, 10).addItem(TUREK_DOGTAG));
		DROPLIST.put(20499, new Drop(1, 0xFFFF, 8).addItem(TUREK_DOGTAG));
		DROPLIST.put(20500, new Drop(1, 0xFFFF, 7).addItem(TUREK_DOGTAG));
		DROPLIST.put(20501, new Drop(1, 0xFFFF, 12).addItem(TUREK_MEDALLION));
		EXP.put(ANCIENT_CLAY_URN, 913);
		EXP.put(ANCIENT_BRASS_TIARA, 1065);
		EXP.put(ANCIENT_BRONZE_MIRROR, 1065);
		EXP.put(ANCIENT_JADE_NECKLACE, 1294);

		for(int kill_id : DROPLIST.keySet())
			addKillId(kill_id);

		addQuestItem(CLAY_URN_FRAGMENT);
		addQuestItem(BRASS_TRINKET_PIECE);
		addQuestItem(BRONZE_MIRROR_PIECE);
		addQuestItem(JADE_NECKLACE_BEAD);
		addQuestItem(TUREK_MEDALLION);
		addQuestItem(TUREK_DOGTAG);
	}

	private static boolean ExpReward(QuestState st, int item_id)
	{
		Integer exp = EXP.get(item_id);
		if(exp == null)
			exp = 182;
		long exp_reward = st.getQuestItemsCount(item_id * exp);
		if(exp_reward == 0)
			return false;
		st.takeItems(item_id, -1);
		st.addExpAndSp(exp_reward, 0);
		st.playSound(SOUND_MIDDLE);
		return true;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		if(event.equalsIgnoreCase("30597-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30597-06.htm") && _state == STARTED)
		{
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		else if(event.equalsIgnoreCase("30313-02.htm") && _state == STARTED && st.getQuestItemsCount(CLAY_URN_FRAGMENT) >= 5)
		{
			st.takeItems(CLAY_URN_FRAGMENT, 5);
			if(!Rnd.chance(Exchange_Chance))
				return "30313-10.htm";
			st.giveItems(ANCIENT_CLAY_URN, 1);
			st.playSound(SOUND_MIDDLE);
			return "30313-03.htm";
		}
		else if(event.equalsIgnoreCase("30313-04.htm") && _state == STARTED && st.getQuestItemsCount(BRASS_TRINKET_PIECE) >= 5)
		{
			st.takeItems(BRASS_TRINKET_PIECE, 5);
			if(!Rnd.chance(Exchange_Chance))
				return "30313-10.htm";
			st.giveItems(ANCIENT_BRASS_TIARA, 1);
			st.playSound(SOUND_MIDDLE);
			return "30313-05.htm";
		}
		else if(event.equalsIgnoreCase("30313-06.htm") && _state == STARTED && st.getQuestItemsCount(BRONZE_MIRROR_PIECE) >= 5)
		{
			st.takeItems(BRONZE_MIRROR_PIECE, 5);
			if(!Rnd.chance(Exchange_Chance))
				return "30313-10.htm";
			st.giveItems(ANCIENT_BRONZE_MIRROR, 1);
			st.playSound(SOUND_MIDDLE);
			return "30313-07.htm";
		}
		else if(event.equalsIgnoreCase("30313-08.htm") && _state == STARTED && st.getQuestItemsCount(JADE_NECKLACE_BEAD) >= 5)
		{
			st.takeItems(JADE_NECKLACE_BEAD, 5);
			if(!Rnd.chance(Exchange_Chance))
				return "30313-09.htm";
			st.giveItems(ANCIENT_JADE_NECKLACE, 1);
			st.playSound(SOUND_MIDDLE);
			return "30313-07.htm";
		}
		else if(event.equalsIgnoreCase("30034-03.htm") && _state == STARTED)
		{
			if(!ExpReward(st, CLAY_URN_FRAGMENT))
				return "30034-02.htm";
		}
		else if(event.equalsIgnoreCase("30034-04.htm") && _state == STARTED)
		{
			if(!ExpReward(st, BRASS_TRINKET_PIECE))
				return "30034-02.htm";
		}
		else if(event.equalsIgnoreCase("30034-05.htm") && _state == STARTED)
		{
			if(!ExpReward(st, BRONZE_MIRROR_PIECE))
				return "30034-02.htm";
		}
		else if(event.equalsIgnoreCase("30034-06.htm") && _state == STARTED)
		{
			if(!ExpReward(st, JADE_NECKLACE_BEAD))
				return "30034-02.htm";
		}
		else if(event.equalsIgnoreCase("30034-07.htm") && _state == STARTED)
			if(!(ExpReward(st, ANCIENT_CLAY_URN) || ExpReward(st, ANCIENT_BRASS_TIARA) || ExpReward(st, ANCIENT_BRONZE_MIRROR) || ExpReward(st, ANCIENT_JADE_NECKLACE)))
				return "30034-02.htm";

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();
		if(_state == CREATED)
		{
			if(npcId != Piotur)
				return "noquest";
			if(st.getPlayer().getLevel() < 25)
			{
				st.exitCurrentQuest(true);
				return "30597-01.htm";
			}
			st.set("cond", "0");
			return "30597-02.htm";
		}

		if(_state != STARTED)
			return "noquest";

		if(npcId == Piotur)
		{
			long reward = st.getQuestItemsCount(TUREK_DOGTAG) * 40 + st.getQuestItemsCount(TUREK_MEDALLION) * 50;
			if(reward == 0)
				return "30597-04.htm";
			st.takeItems(TUREK_DOGTAG, -1);
			st.takeItems(TUREK_MEDALLION, -1);
			st.giveItems(ADENA, reward);
			st.playSound(SOUND_MIDDLE);
			return "30597-05.htm";
		}
		if(npcId == Iris)
			return "30034-01.htm";
		if(npcId == Asha)
			return "30313-01.htm";

		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED)
			return null;
		int npcId = npc.getNpcId();

		Drop _drop = DROPLIST.get(npcId);
		if(_drop == null)
			return null;

		if(Rnd.chance(_drop.chance))
		{
			int n = Rnd.get(100);
			if(n < 25)
				qs.giveItems(CLAY_URN_FRAGMENT, 1);
			else if(n < 50)
				qs.giveItems(BRASS_TRINKET_PIECE, 1);
			else if(n < 75)
				qs.giveItems(BRONZE_MIRROR_PIECE, 1);
			else
				qs.giveItems(JADE_NECKLACE_BEAD, 1);
		}
		qs.giveItems(_drop.itemList.get(0), 1);
		qs.playSound(SOUND_ITEMGET);

		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 327: Reclaim The Land");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
