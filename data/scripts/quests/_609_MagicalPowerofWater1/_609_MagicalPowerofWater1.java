package quests._609_MagicalPowerofWater1;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.tables.SkillTable;

public class _609_MagicalPowerofWater1 extends Quest implements ScriptFile
{
	// NPC
	private static final int WAHKAN = 31371;
	private static final int ASEFA = 31372;
	private static final int UDANS_MARDUI_BOX = 31561;

	// Quest items
	private static final int STOLEN_GREEN_TOTEM = 7237;
	private static final int DIVINE_STONE_OF_WISDOM = 7081;
	private static final int GREEN_TOTEM = 7238;

	// etc
	@SuppressWarnings("unused")
	private static final int MARK_OF_KETRA_ALLIANCE1 = 7211;
	private static final int MARK_OF_KETRA_ALLIANCE2 = 7212;
	private static final int MARK_OF_KETRA_ALLIANCE3 = 7213;
	private static final int MARK_OF_KETRA_ALLIANCE4 = 7214;
	private static final int MARK_OF_KETRA_ALLIANCE5 = 7215;
	private static final int THIEF_KEY = 1661;

	private static int[] VARKA_NPC_LIST = new int[] {
			21350,
			21351,
			21353,
			21354,
			21355,
			21357,
			21358,
			21360,
			21361,
			21362,
			21369,
			21370,
			21364,
			21365,
			21366,
			21368,
			21371,
			21372,
			21373,
			21374,
			21375 };

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _609_MagicalPowerofWater1()
	{
		super(609, -1);

		addStartNpc(WAHKAN);

		addTalkId(WAHKAN);
		addTalkId(ASEFA);
		addTalkId(UDANS_MARDUI_BOX);

		addAttackId(VARKA_NPC_LIST);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31371-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("open"))
			if(st.getInt("cond") == 2)
				if(st.getQuestItemsCount(THIEF_KEY) < 1)
					htmltext = "31561-2.htm";
				else if(st.getInt("proval") == 1)
				{
					htmltext = "31561-4.htm";
					st.takeItems(THIEF_KEY, 1);
				}
				else
				{
					st.takeItems(THIEF_KEY, 1);
					st.giveItems(STOLEN_GREEN_TOTEM, 1);
					htmltext = "31561-3.htm";
					st.set("cond", "3");
				}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int proval = st.getInt("proval");
		if(npcId == WAHKAN)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 74)
					if(st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE2) == 1 || st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE3) == 1 || st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE4) == 1 || st.getQuestItemsCount(MARK_OF_KETRA_ALLIANCE5) == 1)
					{
						if(st.getQuestItemsCount(DIVINE_STONE_OF_WISDOM) == 0)
							htmltext = "31371-1.htm";
						else
						{
							htmltext = "completed";
							st.exitCurrentQuest(true);
						}
					}
					else
					{
						htmltext = "31371-00.htm";
						st.exitCurrentQuest(true);
					}
				else
				{
					htmltext = "31371-0.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "31371-2r.htm";
		}
		else if(npcId == ASEFA)
		{
			if(cond == 1)
			{
				htmltext = "31372-1.htm";
				st.set("cond", "2");
			}
			else if(cond == 2 && proval == 1)
			{
				htmltext = "31372-3.htm";
				npc.doCast(SkillTable.getInstance().getInfo(4548, 1), st.getPlayer(), true);
				st.set("proval", "0");
			}
			else if(cond == 3 && st.getQuestItemsCount(STOLEN_GREEN_TOTEM) == 1)
			{
				htmltext = "31372-4.htm";
				st.takeItems(STOLEN_GREEN_TOTEM, -1);
				st.giveItems(GREEN_TOTEM, 1);
				st.giveItems(DIVINE_STONE_OF_WISDOM, 1);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
		}
		else if(npcId == UDANS_MARDUI_BOX && cond == 2)
			htmltext = "31561-1.htm";
		return htmltext;
	}

	@Override
	public String onAttack(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 2 && st.getInt("proval") == 0)
		{
			npc.doCast(SkillTable.getInstance().getInfo(4547, 1), st.getPlayer(), true);
			st.set("proval", "1");
		}
		return null;
	}
}
