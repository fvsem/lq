package quests._655_AGrandPlanforTamingWildBeasts;

import java.util.Date;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.ClanHallManager;
import l2n.game.model.entity.residence.ClanHall;
import l2n.game.model.entity.siege.clanhall.ClanHallSiege;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

/**
 * Квест на захват Клан Холла Wild Beast Reserve (63)
 * 
 */
// TODO 1) Тип Стартового НПС - L2NPC 2)Квестовые итемы в квест Инвентарь in SQL
// TODO При окончании работ поправить расчет времени до осады и убрать дебаг инфу.
public class _655_AGrandPlanforTamingWildBeasts extends Quest implements ScriptFile
{
	// NPC
	private static final int MESSENGER = 35627;
	// MOBS
	private static int BUFFALO = 16013;
	private static int COUGAR = 16015;
	private static int KUKABURA = 16017;

	// ITEMS
	private static int STONE = 8084;
	private static int TSTONE = 8293;

	// SHANCE
	private static int STONE_SHANCE = 30;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _655_AGrandPlanforTamingWildBeasts()
	{
		super(655, 1);

		addStartNpc(MESSENGER);
		addTalkId(MESSENGER);
		addKillId(BUFFALO);
		addKillId(COUGAR);
		addKillId(KUKABURA);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("35627-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		int npcId = npc.getNpcId();
		if(npcId == MESSENGER)
			if(cond == 0)
			{
				ClanHall ch = ClanHallManager.getInstance().getClanHall(Integer.valueOf(63));
				ClanHallSiege chSiege = ch.getSiege();
				if(chSiege != null)
					if(st.getPlayer().getClan().getLevel() >= 4)
					{
						long NextSiege = chSiege.getSiegeDate().getTimeInMillis() / 1000;
						long CurTime = new Date().getTime() / 1000;
						long TimeToClanHallSiege = (NextSiege - CurTime) / 60;
						System.out.println("Osada Budet: " + NextSiege);
						System.out.println("Seychas: " + CurTime);
						System.out.println("Do Nachala : " + TimeToClanHallSiege + " minut");
						System.out.println("**************************************");
						if(TimeToClanHallSiege != 0)
							htmltext = "35627-01.htm";
						else
						{
							htmltext = "notime.htm";
							st.exitCurrentQuest(true);
						}
					}
					else
						htmltext = "noclan.htm";
			}
			else if(cond == 1)
			{
				if(st.getQuestItemsCount(STONE) <= 10)
					htmltext = "noitem.htm";
			}
			else if(cond == 2)
			{
				if(st.getQuestItemsCount(STONE) >= 10)
				{
					st.takeItems(STONE, -1);
					st.giveItems(TSTONE, 1);
					st.set("cond", "3");
					st.setState(STARTED);
				}
				else
					htmltext = "noitem.htm";
			}
			else if(cond == 3)
				if(st.getQuestItemsCount(TSTONE) >= 1)
				{
					// Показать окно регистрации
				}
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int cond = st.getInt("cond");
		if(cond == 1)
		{
			st.rollAndGive(STONE, 1, 1, 10, STONE_SHANCE);
			if(st.getQuestItemsCount(STONE) == 10)
			{
				st.set("cond", "2");
				st.playSound("SOUND_MIDDLE");
				st.setState(STARTED);
			}

		}
		return null;
	}
}
