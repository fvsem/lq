package quests._117_OceanOfDistantStar;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

/**
 * @author Felixx
 */

public class _117_OceanOfDistantStar extends Quest implements ScriptFile
{
	// NPC
	private static final int Abey = 32053;
	private static final int GhostEngineer = 32055;
	private static final int Obi = 32052;
	private static final int GhostEngineer2 = 32054;
	private static final int Box = 32076;
	// Quest Items
	private static final int BookOfGreyStar = 8495;
	private static final int EngravedHammer = 8488;
	// Mobs
	private static final int BanditWarrior = 22023;
	private static final int BanditInspector = 22024;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _117_OceanOfDistantStar()
	{
		super(117, -1);

		addStartNpc(Abey);

		addTalkId(Abey);
		addTalkId(GhostEngineer);
		addTalkId(Obi);
		addTalkId(Box);
		addTalkId(GhostEngineer2);

		addKillId(BanditWarrior);
		addKillId(BanditInspector);

		addQuestItem(new int[] { BookOfGreyStar, EngravedHammer });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32053-02.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32055-02.htm"))
		{
			st.set("cond", "2");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32052-02.htm"))
		{
			st.set("cond", "3");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32053-04.htm"))
		{
			st.set("cond", "4");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32076-02.htm"))
		{
			st.set("cond", "5");
			st.setState(STARTED);
			st.giveItems(EngravedHammer, 1);
		}
		else if(event.equalsIgnoreCase("32053-06.htm"))
		{
			st.set("cond", "6");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32052-04.htm"))
		{
			st.set("cond", "7");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32052-06.htm"))
		{
			st.takeItems(BookOfGreyStar, -1);
			st.set("cond", "9");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32055-04.htm"))
		{
			st.takeItems(EngravedHammer, -1);
			st.set("cond", "10");
			st.setState(STARTED);
		}
		else if(event.equalsIgnoreCase("32054-03.htm"))
		{
			st.addExpAndSp(63591, 0);
			st.playSound(SOUND_FINISH);
			st.unset("cond");
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == Abey)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 39)
					htmltext = "32053-01.htm";
				else
				{
					htmltext = "32053-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 3)
				htmltext = "32053-03.htm";
			else if(cond == 5 && st.getQuestItemsCount(EngravedHammer) > 0)
				htmltext = "32053-05.htm";
			else if(cond == 6 && st.getQuestItemsCount(EngravedHammer) > 0)
				htmltext = "32053-06.htm";
		}
		else if(npcId == GhostEngineer)
		{
			if(cond == 1)
				htmltext = "32055-01.htm";
			else if(cond == 9 && st.getQuestItemsCount(EngravedHammer) > 0)
				htmltext = "32055-03.htm";
		}
		else if(npcId == Obi)
		{
			if(cond == 2)
				htmltext = "32052-01.htm";
			else if(cond == 6 && st.getQuestItemsCount(EngravedHammer) > 0)
				htmltext = "32052-03.htm";
			else if(cond == 7 && st.getQuestItemsCount(EngravedHammer) > 0)
				htmltext = "32052-04.htm";
			else if(cond == 8 && st.getQuestItemsCount(BookOfGreyStar) > 0)
				htmltext = "32052-06.htm";
		}
		else if(npcId == Box && cond == 4)
			htmltext = "32076-01.htm";
		else if(npcId == GhostEngineer2 && cond == 10)
			htmltext = "32054-01.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getInt("cond") == 7 && Rnd.chance(30))
		{
			if(st.getQuestItemsCount(BookOfGreyStar) < 1)
			{
				st.giveItems(BookOfGreyStar, 1);
				st.playSound(SOUND_ITEMGET);
			}
			st.set("cond", "8");
			st.setState(STARTED);
		}
		return null;
	}
}
