package quests._690_JudesRequest;

import l2n.Config;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _690_JudesRequest extends Quest implements ScriptFile
{
	// NPC's
	private static final int DROP_RATE = 50;
	private static final int EVIL = 10327;

	private static final int[] IcarusRECIPES_60 = { 10373, 10374, 10375, 10376, 10377, 10378, 10379, 10380, 10381 };
	private static final int[] IcarusRECIPES_100 = { 10385, 10386, 10387, 10388, 10389, 10390, 10391, 10392, 10393 };
	private static final int[] IcarusPARTS = { 10397, 10398, 10399, 10400, 10401, 10402, 10403, 10404, 10405 };

	private static final int JUDE = 32356;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _690_JudesRequest()
	{
		super(690, 0);

		addStartNpc(JUDE);
		addTalkId(JUDE);
		addKillId(22398, 22399);
		addQuestItem(EVIL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		long evil_count = st.getQuestItemsCount(EVIL);
		if(event.equals("jude_q0690_03.htm"))
		{
			if(st.getPlayer().getLevel() >= 78)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
			}
			else
			{
				htmltext = "jude_q0690_02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(event.equals("jude_q0690_09.htm"))
		{
			if(evil_count >= 5)
			{
				htmltext = "jude_q0690_09.htm";
				st.takeItems(EVIL, 5);
				st.giveItems(IcarusPARTS[Rnd.get(IcarusPARTS.length)], 3); // даётся 3 одинаковых куска
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "jude_q0690_09a.htm";
		}
		else if(event.equals("jude_q0690_07.htm"))
		{
			if(evil_count >= 200)
			{
				htmltext = "jude_q0690_07.htm";
				st.takeItems(EVIL, 200);
				if(Config.ALT_100_RECIPES)
					st.giveItems(IcarusRECIPES_100[Rnd.get(IcarusRECIPES_100.length)], 1);
				else
					st.giveItems(IcarusRECIPES_60[Rnd.get(IcarusRECIPES_60.length)], 1);
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "jude_q0690_07a.htm";
		}
		else if(event.equals("jude_q0690_08.htm"))
		{
			st.exitCurrentQuest(true);
			st.playSound(SOUND_GIVEUP);
		}

		return htmltext;

	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");
		long evil_count = st.getQuestItemsCount(EVIL);

		if(npcId == JUDE)
			if(id == CREATED)
			{
				htmltext = "jude_q0690_01.htm";
				if(st.getPlayer().getLevel() < 78)
				{
					st.exitCurrentQuest(true);
					htmltext = "jude_q0690_02.htm";
				}
			}
			else if(id == STARTED)
				if(cond == 1 && evil_count >= 200)
					htmltext = "jude_q0690_04.htm";
				else if(cond == 1 && evil_count >= 5 && evil_count <= 200)
					htmltext = "jude_q0690_05.htm";
				else
					htmltext = "jude_q0690_05a.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		L2Player player = st.getRandomPartyMember(STARTED, Config.ALT_PARTY_DISTRIBUTION_RANGE);

		if(st.getState() != STARTED)
			return null;

		if(player != null)
		{
			QuestState sts = player.getQuestState(st.getQuest().getName());
			if(sts != null && Rnd.chance(DROP_RATE))
			{
				st.giveItems(EVIL, 1);
				st.playSound(SOUND_ITEMGET);
			}
		}
		return null;
	}
}
