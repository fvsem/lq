package quests._998_FallenAngelSelect;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.instancemanager.QuestManager;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _998_FallenAngelSelect extends Quest implements ScriptFile
{
	private static final int NATOOLS = 30894;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _998_FallenAngelSelect()
	{
		super(998, "_998_FallenAngelSelect", "Fallen Angel - Select", -1);
		addStartNpc(NATOOLS);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		if(event.equalsIgnoreCase("dawn"))
		{
			QuestState ok = st.getPlayer().getQuestState("_141_ShadowFoxPart3");
			if(ok == null || !ok.isCompleted())
				return "need.htm";

			Quest q1 = QuestManager.getQuest("_142_FallenAngelRequestOfDawn");
			if(q1 != null)
			{
				QuestState qs1 = q1.newQuestState(st.getPlayer(), CREATED);
				qs1.setState(STARTED);
				q1.notifyEvent("30894-01.htm", qs1, npc);
				st.setState(COMPLETED);
			}
			return null;
		}
		else if(event.equalsIgnoreCase("dusk"))
		{
			QuestState ok = st.getPlayer().getQuestState("_141_ShadowFoxPart3");
			if(ok == null || !ok.isCompleted())
				return "need.htm";

			Quest q2 = QuestManager.getQuest("_143_FallenAngelRequestOfDusk");
			if(q2 != null)
			{
				QuestState qs2 = q2.newQuestState(st.getPlayer(), CREATED);
				qs2.setState(STARTED);
				q2.notifyEvent("30894-01.htm", qs2, npc);
				st.setState(COMPLETED);
			}
			return null;
		}
		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "<html><body>You are either not on a quest that involves this NPC, or you don't meet this NPC's minimum quest requirements.</body></html>";

		if(st.getState() == CREATED)
			htmltext = "30894-01.htm";
		return htmltext;
	}
}
