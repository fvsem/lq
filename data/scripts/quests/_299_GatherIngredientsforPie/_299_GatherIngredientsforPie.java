package quests._299_GatherIngredientsforPie;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _299_GatherIngredientsforPie extends Quest implements ScriptFile
{
	// NPCs
	private static int Emily = 30620;
	private static int Lara = 30063;
	private static int Bright = 30466;
	// Mobs
	private static int Wasp_Worker = 20934;
	private static int Wasp_Leader = 20935;
	// Items
	private static int ADENA = 57;
	private static int Varnish = 1865;
	// Quest Items
	private static short Fruit_Basket = 7136;
	private static short Avellan_Spice = 7137;
	private static short Honey_Pouch = 7138;
	// Chances
	private static int Wasp_Worker_Chance = 55;
	private static int Wasp_Leader_Chance = 70;
	private static int Reward_Varnish_Chance = 50;

	public _299_GatherIngredientsforPie()
	{
		super(299, -1);
		addStartNpc(Emily);
		addTalkId(Lara);
		addTalkId(Bright);
		addKillId(Wasp_Worker);
		addKillId(Wasp_Leader);
		addQuestItem(Fruit_Basket);
		addQuestItem(Avellan_Spice);
		addQuestItem(Honey_Pouch);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		int _state = st.getState();
		int cond = st.getInt("cond");

		if(event.equalsIgnoreCase("30620-03.htm") && _state == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30620-06.htm") && _state == STARTED)
		{
			if(st.getQuestItemsCount(Honey_Pouch) < 100)
				return "30620-07.htm";
			st.takeItems(Honey_Pouch, -1);
			st.set("cond", "3");
		}
		else if(event.equalsIgnoreCase("30063-02.htm") && _state == STARTED && cond == 3)
		{
			st.giveItems(Avellan_Spice, 1);
			st.set("cond", "4");
		}
		else if(event.equalsIgnoreCase("30620-10.htm") && _state == STARTED)
		{
			if(st.getQuestItemsCount(Avellan_Spice) < 1)
				return "30620-11.htm";
			st.takeItems(Avellan_Spice, -1);
			st.set("cond", "5");
		}
		else if(event.equalsIgnoreCase("30466-02.htm") && _state == STARTED && cond == 5)
		{
			st.giveItems(Fruit_Basket, 1);
			st.set("cond", "6");
		}
		else if(event.equalsIgnoreCase("30620-14.htm") && _state == STARTED)
		{
			if(st.getQuestItemsCount(Fruit_Basket) < 1)
				return "30620-15.htm";
			st.takeItems(Fruit_Basket, -1);
			if(Rnd.chance(Reward_Varnish_Chance))
				st.giveItems(Varnish, 50, true);
			else
				st.giveItems(ADENA, 25000);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}

		return event;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int _state = st.getState();
		int npcId = npc.getNpcId();
		if(_state == CREATED)
		{
			if(npcId != Emily)
				return "noquest";
			if(st.getPlayer().getLevel() >= 34)
			{
				st.set("cond", "0");
				return "30620-01.htm";
			}
			st.exitCurrentQuest(true);
			return "30620-02.htm";
		}

		int cond = st.getInt("cond");
		if(npcId == Emily && _state == STARTED)
		{
			if(cond == 1 && st.getQuestItemsCount(Honey_Pouch) <= 99)
				return "30620-05.htm";
			if(cond == 2 && st.getQuestItemsCount(Honey_Pouch) >= 100)
				return "30620-04.htm";
			if(cond == 3 && st.getQuestItemsCount(Avellan_Spice) == 0)
				return "30620-08.htm";
			if(cond == 4 && st.getQuestItemsCount(Avellan_Spice) == 1)
				return "30620-09.htm";
			if(cond == 5 && st.getQuestItemsCount(Fruit_Basket) == 0)
				return "30620-12.htm";
			if(cond == 6 && st.getQuestItemsCount(Fruit_Basket) == 1)
				return "30620-13.htm";
		}
		if(npcId == Lara && _state == STARTED && cond == 3)
			return "30063-01.htm";
		if(npcId == Lara && _state == STARTED && cond == 4)
			return "30063-03.htm";
		if(npcId == Bright && _state == STARTED && cond == 5)
			return "30466-01.htm";
		if(npcId == Bright && _state == STARTED && cond == 5)
			return "30466-03.htm";

		return "noquest";
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState qs)
	{
		if(qs.getState() != STARTED || qs.getInt("cond") != 1 || qs.getQuestItemsCount(Honey_Pouch) >= 100)
			return null;

		int npcId = npc.getNpcId();
		if(npcId == Wasp_Worker && Rnd.chance(Wasp_Worker_Chance) || npcId == Wasp_Leader && Rnd.chance(Wasp_Leader_Chance))
		{
			qs.giveItems(Honey_Pouch, 1);
			if(qs.getQuestItemsCount(Honey_Pouch) < 100)
				qs.playSound(SOUND_ITEMGET);
			else
			{
				qs.set("cond", "2");
				qs.playSound(SOUND_MIDDLE);
			}
		}

		return null;
	}

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 299: Gather Ingredients for Pie");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
