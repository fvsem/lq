package quests._627_HeartInSearchOfPower;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _627_HeartInSearchOfPower extends Quest implements ScriptFile
{
	// NPC
	private static final int M_NECROMANCER = 31518;
	private static final int ENFEUX = 31519;

	// ITEMS
	private static final int SEAL_OF_LIGHT = 7170;
	private static final int GEM_OF_SUBMISSION = 7171;
	private static final int GEM_OF_SAINTS = 7172;

	// REWARDS
	private static final int ADENA = 57;
	private static final int MOLD_HARDENER = 4041;
	private static final int ENRIA = 4042;
	private static final int ASOFE = 4043;
	private static final int THONS = 4044;

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _627_HeartInSearchOfPower()
	{
		super(627, -1);

		addStartNpc(M_NECROMANCER);
		addTalkId(ENFEUX);

		for(int mobs = 21520; mobs <= 21541; mobs++)
			addKillId(mobs);

		addQuestItem(GEM_OF_SUBMISSION);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31518-1.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31518-3.htm"))
		{
			st.takeItems(GEM_OF_SUBMISSION, 300);
			st.giveItems(SEAL_OF_LIGHT, 1);
			st.set("cond", "3");
		}
		else if(event.equals("31519-1.htm"))
		{
			st.takeItems(SEAL_OF_LIGHT, 1);
			st.giveItems(GEM_OF_SAINTS, 1);
			st.set("cond", "4");
		}
		else if(event.equals("31518-5.htm"))
			st.takeItems(GEM_OF_SAINTS, 1);
		else
		{
			if(event.equals("31518-6.htm"))
				st.giveItems(ADENA, 100000, true);
			else if(event.equals("31518-7.htm"))
			{
				st.giveItems(ASOFE, 13, true);
				st.giveItems(ADENA, 6400, true);
			}
			else if(event.equals("31518-8.htm"))
			{
				st.giveItems(THONS, 13, true);
				st.giveItems(ADENA, 6400, true);
			}
			else if(event.equals("31518-9.htm"))
			{
				st.giveItems(ENRIA, 6, true);
				st.giveItems(ADENA, 13600, true);
			}
			else if(event.equals("31518-10.htm"))
			{
				st.giveItems(MOLD_HARDENER, 3, true);
				st.giveItems(ADENA, 17200, true);
			}
			st.set("cond", "0");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == M_NECROMANCER)
		{
			if(cond == 0)
				if(st.getPlayer().getLevel() >= 60)
					htmltext = "31518-0.htm";
				else
				{
					htmltext = "31518-0a.htm";
					st.exitCurrentQuest(true);
				}
			else if(cond == 1)
				htmltext = "31518-1a.htm";
			else if(st.getQuestItemsCount(GEM_OF_SUBMISSION) >= 300)
				htmltext = "31518-2.htm";
			else if(st.getQuestItemsCount(GEM_OF_SAINTS) > 0)
				htmltext = "31518-4.htm";
		}
		else if(npcId == ENFEUX && st.getQuestItemsCount(SEAL_OF_LIGHT) > 0)
			htmltext = "31519-0.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		long count = st.getQuestItemsCount(GEM_OF_SUBMISSION);
		if(st.getInt("cond") == 1 && count < 300)
		{
			st.rollAndGive(GEM_OF_SUBMISSION, 1, 100);
			if(st.getQuestItemsCount(GEM_OF_SUBMISSION) >= 300)
			{
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "2");
			}
		}
		return null;
	}
}
