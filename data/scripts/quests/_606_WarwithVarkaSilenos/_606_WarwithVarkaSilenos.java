package quests._606_WarwithVarkaSilenos;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

public class _606_WarwithVarkaSilenos extends Quest implements ScriptFile
{
	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	// NPC
	public static int KADUN_ZU_KETRA = 31370;

	// Quest items
	private static final int VARKAS_MANE = 7233;
	private static final int HORN_OF_BUFFALO = 7186;
	private static final int VARKAS_MANE_DROP_CHANCE = 80;

	private static final int[] VARKA_NPC_LIST = new int[] {
			21350,
			21351,
			21353,
			21354,
			21355,
			21357,
			21358,
			21360,
			21361,
			21362,
			21364,
			21365,
			21366,
			21368,
			21369,
			21370,
			21371,
			21372,
			21373,
			21374 };

	public _606_WarwithVarkaSilenos()
	{
		super(606, 0);

		addStartNpc(KADUN_ZU_KETRA);
		addTalkId(KADUN_ZU_KETRA);

		addKillId(VARKA_NPC_LIST);
		addQuestItem(VARKAS_MANE);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31370-2.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31370-4.htm"))
		{
			if(st.getQuestItemsCount(VARKAS_MANE) >= 100)
			{
				st.takeItems(VARKAS_MANE, 100);
				st.giveItems(HORN_OF_BUFFALO, 20);
			}
			else
				htmltext = "31370-havenot.htm";
		}
		else if(event.equals("31370-quit.htm"))
		{
			st.takeItems(VARKAS_MANE, -1);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(cond == 0)
		{
			if(st.getPlayer().getLevel() >= 74)
				htmltext = "31370-1.htm";
			else
			{
				htmltext = "31370-0.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(cond == 1 && st.getQuestItemsCount(VARKAS_MANE) == 0)
			htmltext = "31370-2r.htm";
		else if(cond == 1 && st.getQuestItemsCount(VARKAS_MANE) > 0)
			htmltext = "31370-3.htm";
		return htmltext;
	}

	public boolean isVarkaNpc(int npc)
	{
		return ArrayUtil.arrayContains(VARKA_NPC_LIST, npc);
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(isVarkaNpc(npc.getNpcId()) && st.getInt("cond") == 1)
			st.rollAndGive(VARKAS_MANE, 1, VARKAS_MANE_DROP_CHANCE);
		return null;
	}
}
