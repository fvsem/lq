package quests._653_WildMaiden;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Spawn;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.tables.SpawnTable;

public class _653_WildMaiden extends Quest implements ScriptFile
{
	// Npc
	public final int SUKI = 32013;
	public final int GALIBREDO = 30181;

	// Items
	public final int SOE = 736;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 653: Wild Maiden");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _653_WildMaiden()
	{
		super(653, -1);

		addStartNpc(SUKI);

		addTalkId(SUKI);
		addTalkId(GALIBREDO);
	}

	private L2NpcInstance findNpc(int npcId, L2Player player)
	{
		L2NpcInstance instance = null;
		GArray<L2NpcInstance> npclist = new GArray<L2NpcInstance>();
		for(L2Spawn spawn : SpawnTable.getInstance().getSpawnTable())
			if(spawn.getNpcId() == npcId)
			{
				instance = spawn.getLastSpawn();
				npclist.add(instance);
			}

		for(L2NpcInstance npc : npclist)
			if(player.isInRange(npc, 1600))
				return npc;

		return instance;
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		L2Player player = st.getPlayer();
		if(event.equalsIgnoreCase("32013-04.htm"))
		{
			if(st.getQuestItemsCount(SOE) > 0)
			{
				st.set("cond", "1");
				st.setState(STARTED);
				st.playSound(SOUND_ACCEPT);
				st.takeItems(SOE, 1);
				htmltext = "32013-03.htm";
				L2NpcInstance npc1 = findNpc(SUKI, player);
				npc1.broadcastPacket(new MagicSkillUse(npc1, npc1, 2013, 1, 20000, 0));
				st.startQuestTimer("suki_timer", 20000);
			}
		}
		else if(event.equalsIgnoreCase("32013-04a.htm"))
		{
			st.exitCurrentQuest(false);
			st.playSound(SOUND_GIVEUP);
		}
		else if(event.equalsIgnoreCase("suki_timer"))
		{
			L2NpcInstance npc1 = findNpc(SUKI, player);
			npc1.deleteMe();
			htmltext = null;
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";

		int npcId = npc.getNpcId();
		int id = st.getState();
		if(npcId == SUKI && id == CREATED)
		{
			if(st.getPlayer().getLevel() >= 36)
				htmltext = "32013-02.htm";
			else
			{
				htmltext = "32013-01.htm";
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == GALIBREDO && st.getInt("cond") == 1)
		{
			htmltext = "30181-01.htm";
			st.giveItems((short) 57, 2883);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}
}
