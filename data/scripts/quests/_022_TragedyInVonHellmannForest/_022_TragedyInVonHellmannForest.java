package quests._022_TragedyInVonHellmannForest;

import java.util.Arrays;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _022_TragedyInVonHellmannForest extends Quest implements ScriptFile
{
	// Npc list
	public static final int Well = 31527;
	public static final int Tifaren = 31334;
	public static final int Innocentin = 31328;
	public static final int SoulofWell = 31217;
	public static final int Ghostofpriest = 31528;
	public static final int GhostofAdventurer = 31529;
	// ~~~~~~~~ Item list ~~~~~~~~
	public static final int ReportBox = 7147;
	public static final int LostSkullofElf = 7142;
	public static final int CrossofEinhasad = 7141;
	public static final int SealedReportBox = 7146;
	public static final int LetterofInnocentin = 7143;
	public static final int JewelofAdventurerRed = 7145;
	public static final int JewelofAdventurerGreen = 7144;
	// ~~~~ Monster list: ~~~~
	private static final int[] monsters = new int[] {
			21547,
			21548,
			21549,
			21550,
			21551,
			21552,
			21553,
			21554,
			21555,
			21556,
			21557,
			21558,
			21559,
			21560,
			21561,
			21562,
			21563,
			21564,
			21565,
			21566,
			21567,
			21568,
			21569,
			21570,
			21571,
			21572,
			21573,
			21574,
			21575,
			21576,
			21577,
			21578 };

	private L2NpcInstance priest;
	private L2NpcInstance soul;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _022_TragedyInVonHellmannForest()
	{
		super(22, -1);

		addStartNpc(Tifaren);
		addTalkId(Ghostofpriest);
		addTalkId(Innocentin);
		addTalkId(GhostofAdventurer);
		addTalkId(Well);
		addAttackId(SoulofWell);
		addKillId(SoulofWell);
		addKillId(monsters);
		addQuestItem(LostSkullofElf);

	}

	private void spawnGhostOfPriest(QuestState st)
	{
		priest = Functions.spawn(st.getPlayer().getLoc().rnd(50, 100, false), Ghostofpriest);
	}

	private void spawnSoulOfWell(QuestState st)
	{
		soul = Functions.spawn(st.getPlayer().getLoc().rnd(50, 100, false), SoulofWell);
	}

	private void despawnGhostOfPriest()
	{
		if(priest != null)
			priest.deleteMe();
	}

	private void despawnSoulOfWell()
	{
		if(soul != null)
			soul.deleteMe();
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31334-03.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "3");
			st.takeItems(CrossofEinhasad, -1);
		}
		else if(event.equalsIgnoreCase("31334-06.htm"))
			st.set("cond", "4");
		else if(event.equalsIgnoreCase("31334-09.htm"))
		{
			st.set("cond", "6");
			st.takeItems(LostSkullofElf, 1);
			despawnGhostOfPriest();
			spawnGhostOfPriest(st);
		}
		else if(event.equalsIgnoreCase("31528-07.htm"))
		{
			despawnGhostOfPriest();
			st.set("cond", "7");
		}
		else if(event.equalsIgnoreCase("31328-06.htm"))
		{
			st.set("cond", "8");
			st.giveItems(LetterofInnocentin, 1);
		}
		else if(event.equalsIgnoreCase("31529-09.htm"))
		{
			st.set("cond", "9");
			st.takeItems(LetterofInnocentin, 1);
			st.giveItems(JewelofAdventurerGreen, 1);
		}
		else if(event.equalsIgnoreCase("explore"))
		{
			despawnSoulOfWell();
			spawnSoulOfWell(st);
			if(soul != null)
			{
				st.getPlayer().addDamageHate(soul, 0, 999999);
				soul.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, st.getPlayer());
			}

			if(st.getQuestTimer("soul_of_well") != null)
				st.getQuestTimer("soul_of_well").cancel();
			st.startQuestTimer("soul_of_well", 120000);

			st.set("cond", "10");
			st.playSound("SkillSound3.antaras_fear");
			htmltext = "<html><body>Attack Soul of Well but do not kill while stone will not change colour...</body></html>";
		}
		else if(event.equalsIgnoreCase("soul_of_well"))
		{
			despawnSoulOfWell();
			st.giveItems(JewelofAdventurerRed, 1);
			st.takeItems(JewelofAdventurerGreen, -1);
			st.set("cond", "11");
		}
		else if(event.equalsIgnoreCase("31328-08.htm"))
		{
			st.startQuestTimer("wait_timer", 600000);
			st.set("cond", "15");
			st.takeItems(ReportBox, 1);
		}
		else if(event.equalsIgnoreCase("wait_timer"))
		{
			st.set("cond", "16");
			htmltext = "<html><body>Innocentin wants with you to speak...</body></html>";
		}
		else if(event.equalsIgnoreCase("31328-16.htm"))
		{
			st.startQuestTimer("next_wait_timer", 300000);
			st.set("cond", "17");
		}
		else if(event.equalsIgnoreCase("next_wait_timer"))
			st.set("cond", "18");
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		String htmltext = "noquest";
		if(npcId == Tifaren)
		{
			if(cond == 0)
			{
				QuestState hiddenTruth = st.getPlayer().getQuestState("_021_HiddenTruth");
				if(hiddenTruth != null)
				{
					if(hiddenTruth.isCompleted())
						htmltext = "31334-01.htm";
					else
						htmltext = "<html><head><body>You not complite quest Hidden Truth...</body></html>";
				}
				else
					htmltext = "<html><head><body>You not complite quest Hidden Truth...</body></html>";
			}
			else if(cond == 3)
				htmltext = "31334-04.htm";
			else if(cond == 4)
				htmltext = "31334-06.htm";
			else if(cond == 5)
			{
				if(st.getQuestItemsCount(LostSkullofElf) != 0)
					htmltext = "31334-07.htm";
				else
				{
					st.set("cond", "4");
					htmltext = "31334-06.htm";
				}
			}
			else if(cond == 6)
			{
				despawnGhostOfPriest();
				spawnGhostOfPriest(st);
				htmltext = "31334-09.htm";
			}
		}
		else if(npcId == Ghostofpriest)
		{
			if(cond == 6)
				htmltext = "31528-00.htm";
			else if(cond == 7)
				htmltext = "31528-07.htm";
		}
		else if(npcId == Innocentin)
		{
			if(cond == 0)
				htmltext = "31328-17.htm";
			if(cond == 7)
				htmltext = "31328-00.htm";
			else if(cond == 8)
				htmltext = "31328-06.htm";
			else if(cond == 14)
			{
				if(st.getQuestItemsCount(ReportBox) != 0)
					htmltext = "31328-07.htm";
				else
				{
					st.set("cond", "13");
					htmltext = "Go away!";
				}
			}
			else if(cond == 15)
			{
				if(st.getQuestTimer("wait_timer") == null)
					st.set("cond", "16");
				htmltext = "31328-09.htm";
			}
			else if(cond == 16)
				htmltext = "31328-08a.htm";
			else if(cond == 17)
			{
				if(st.getQuestTimer("next_wait_timer") == null)
					st.set("cond", "18");
				htmltext = "31328-16a.htm";
			}
			else if(cond == 18)
			{
				htmltext = "31328-17.htm";
				st.addExpAndSp(345966, 31578);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == GhostofAdventurer)
		{
			if(cond == 8)
			{
				if(st.getQuestItemsCount(LetterofInnocentin) != 0)
					htmltext = "31529-00.htm";
				else
					htmltext = "You have no Letter of Innocentin! Are they Please returned to High Priest Innocentin...";
			}
			else if(cond == 9)
				htmltext = "31529-09.htm";
			else if(cond == 11)
			{
				if(st.getQuestItemsCount(JewelofAdventurerRed) != 0)
				{
					htmltext = "31529-10.htm";
					st.takeItems(JewelofAdventurerRed, -1);
					st.set("cond", "12");
				}
				else
				{
					st.set("cond", "9");
					htmltext = "31529-09.htm";
				}
			}
			else if(cond == 13)
				if(st.getQuestItemsCount(SealedReportBox) != 0)
				{
					htmltext = "31529-11.htm";
					st.set("cond", "14");
					st.takeItems(SealedReportBox, 1);
					st.giveItems(ReportBox, 1);
				}
				else
				{
					st.set("cond", "12");
					htmltext = "31529-10.htm";
				}
		}
		else if(npcId == Well)
			if((cond == 9 || cond == 10) && st.getQuestItemsCount(JewelofAdventurerGreen) > 0)
			{
				htmltext = "31527-00.htm";
				st.playSound("AmbSound.dd_horror_01");
			}
			else if(cond == 12)
			{
				htmltext = "31527-01.htm";
				st.set("cond", "13");
				st.giveItems(SealedReportBox, 1);
			}
			else
				htmltext = "<html><body>I'll have nothing more to do with this ominous well.</body></html>";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(Arrays.binarySearch(monsters, npcId) > 0)
			if(cond == 4 && Rnd.chance(99))
			{
				st.giveItems(LostSkullofElf, 1);
				st.playSound(SOUND_MIDDLE);
				st.set("cond", "5");
			}
		if(npcId == SoulofWell)
			if(cond == 10)
			{
				st.set("cond", "9");
				if(st.getQuestTimer("soul_of_well") != null)
					st.getQuestTimer("soul_of_well").cancel();
			}
		return null;
	}
}
