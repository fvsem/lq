package quests._431_WeddingMarch;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _431_WeddingMarch extends Quest implements ScriptFile
{
	private static int MELODY_MAESTRO_KANTABILON = 31042;
	private static int SILVER_CRYSTAL = 7540;
	private static int WEDDING_ECHO_CRYSTAL = 7062;

	@Override
	public void onLoad()
	{
		System.out.println("Loaded Quest: 431: Wedding March");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _431_WeddingMarch()
	{
		super(431, -1);

		addStartNpc(MELODY_MAESTRO_KANTABILON);

		addKillId(20786);
		addKillId(20787);

		addQuestItem(SILVER_CRYSTAL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("31042-02.htm"))
		{
			st.setState(STARTED);
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("31042-05.htm"))
			if(st.getQuestItemsCount(SILVER_CRYSTAL) == 50)
			{
				st.takeItems(SILVER_CRYSTAL, -1);
				st.giveItems(WEDDING_ECHO_CRYSTAL, 25);
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(true);
			}
			else
				htmltext = "31042-06.htm";

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int condition = st.getInt("cond");
		int npcId = npc.getNpcId();
		int id = st.getState();

		if(npcId == MELODY_MAESTRO_KANTABILON)
			if(id != STARTED)
			{
				if(st.getPlayer().getLevel() < 38)
				{
					htmltext = "31042-00.htm";
					st.exitCurrentQuest(true);
				}
				else
					htmltext = "31042-01.htm";
			}
			else if(condition == 1)
				htmltext = "31042-03.htm";
			else if(condition == 2 && st.getQuestItemsCount(SILVER_CRYSTAL) == 50)
				htmltext = "31042-04.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
			return null;
		int npcId = npc.getNpcId();

		if(npcId == 20786 || npcId == 20787)
			if(st.getInt("cond") == 1 && st.getQuestItemsCount(SILVER_CRYSTAL) < 50)
			{
				st.giveItems(SILVER_CRYSTAL, 1);

				if(st.getQuestItemsCount(SILVER_CRYSTAL) == 50)
				{
					st.playSound(SOUND_MIDDLE);
					st.set("cond", "2");
				}
				else
					st.playSound(SOUND_ITEMGET);
			}
		return null;
	}
}
