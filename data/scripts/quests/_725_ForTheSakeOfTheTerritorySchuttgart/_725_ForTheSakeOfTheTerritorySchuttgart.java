package quests._725_ForTheSakeOfTheTerritorySchuttgart;

import quests.TerritoryWarSuperClass.TerritoryWarSuperClass;

public class _725_ForTheSakeOfTheTerritorySchuttgart extends TerritoryWarSuperClass
{
	public _725_ForTheSakeOfTheTerritorySchuttgart()
	{
		super(725);
		_catapultId = 36507;
		_territoryId = 9;
		_leaderIDs = new int[] { 36556, 36558, 36561, 36599 };
		_guardIDs = new int[] { 36557, 36559, 36560 };
		_text = new String[] { "The catapult of Schuttgart has been destroyed!" };
		registerKillIds();
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
