package quests._692_HowtoOpposeEvil;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.ArrayUtil;

/**
 * @<!-- L2System -->
 * @date 20.10.2010
 * @time 13:02:16
 */
public class _692_HowtoOpposeEvil extends Quest implements ScriptFile
{
	// NPCs
	private static final int DILIOS = 32549;
	private static final int KUTRAN = 32550;
	private static final int[] DESTRUCTION_MOBS = new int[] { 22537, 22538, 22539, 22540, 22541, 22542, 22543, 22544, 22546, 22547, 22548, 22549, 22550, 22551, 22552, 22593, 22596, 22597 };
	private static final int[] IMMORTALITY_MOBS = new int[] { 22509, 22510, 22511, 22512, 22513, 22514, 22515 };

	// items
	private static final int LEKONS_CERTIFICATE = 13857;

	// reward
	private static final int NUCLEUS_OF_A_FREED_SOUL = 13796;
	private static final int FLEET_STEED_TROUPS_CHARM = 13841;

	// drop
	private static final int NUCLEUS_OF_A_INCOMP_SOUL = 13863;
	private static final int FLEET_STEED_TROUPS_TOTEM = 13865;

	private static final int DROP_CHANCE = 50;

	public _692_HowtoOpposeEvil()
	{
		super(692, PARTY_ONE);
		addStartNpc(DILIOS);
		addTalkId(DILIOS);
		addTalkId(KUTRAN);

		addKillId(DESTRUCTION_MOBS);
		addKillId(IMMORTALITY_MOBS);
		addQuestItem(NUCLEUS_OF_A_FREED_SOUL, FLEET_STEED_TROUPS_CHARM);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32549-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32550-04.htm"))
			st.set("cond", "3");
		else if(event.equalsIgnoreCase("exchange1"))
		{
			if(st.getQuestItemsCount(NUCLEUS_OF_A_INCOMP_SOUL) >= 5)
			{
				st.takeItems(NUCLEUS_OF_A_INCOMP_SOUL, 5);
				st.giveItems(NUCLEUS_OF_A_FREED_SOUL, 1);
				if(st.getQuestItemsCount(NUCLEUS_OF_A_INCOMP_SOUL) + st.getQuestItemsCount(FLEET_STEED_TROUPS_TOTEM) == 0)
					htmltext = "32550-04.htm";
				else
					htmltext = "32550-05.htm";
			}
			else
				htmltext = "32550-06.htm";
		}
		else if(event.equalsIgnoreCase("exchange2"))
			if(st.getQuestItemsCount(FLEET_STEED_TROUPS_TOTEM) >= 5)
			{
				st.takeItems(FLEET_STEED_TROUPS_TOTEM, 5);
				st.giveItems(FLEET_STEED_TROUPS_CHARM, 1);
				if(st.getQuestItemsCount(NUCLEUS_OF_A_INCOMP_SOUL) + st.getQuestItemsCount(FLEET_STEED_TROUPS_TOTEM) == 0)
					htmltext = "32550-04.htm";
				else
					htmltext = "32550-05.htm";
			}
			else
				htmltext = "32550-06.htm";

		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int state = st.getState();

		if(state == CREATED)
		{
			if(st.getPlayer().getLevel() >= 75)
				htmltext = "32549-01.htm";
			else
				st.exitCurrentQuest(true);
		}
		else if(npcId == DILIOS)
		{
			if(cond == 1 && st.getQuestItemsCount(LEKONS_CERTIFICATE) >= 1)
			{
				htmltext = "32549-04.htm";
				st.set("cond", "2");
			}
			else if(cond == 2)
				htmltext = "32549-05.htm";
		}
		else if(cond == 2)
			htmltext = "32550-01.htm";
		else if(cond == 3)
			if(st.getQuestItemsCount(NUCLEUS_OF_A_INCOMP_SOUL) + st.getQuestItemsCount(FLEET_STEED_TROUPS_TOTEM) == 0)
				htmltext = "32550-04.htm";
			else
				htmltext = "32550-05.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(st.getState() != STARTED)
			return null;

		int npcId = npc.getNpcId();
		if(ArrayUtil.arrayContains(DESTRUCTION_MOBS, npcId))
			st.rollAndGive(FLEET_STEED_TROUPS_TOTEM, 1, DROP_CHANCE);
		else if(ArrayUtil.arrayContains(IMMORTALITY_MOBS, npcId))
			st.rollAndGive(NUCLEUS_OF_A_INCOMP_SOUL, 1, DROP_CHANCE);
		return null;
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
