package quests._195_SevenSignsSecretRitualPriests;

import gnu.trove.map.hash.TIntLongHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.instancemanager.InstanceManager;
import l2n.game.instancemanager.InstanceManager.InstanceWorld;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.Instance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.ExStartScenePlayer;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.scripts.ai.quests.GuardsOfDawn;
import l2n.game.tables.NpcTable;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.SkillTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Location;

public class _195_SevenSignsSecretRitualPriests extends Quest implements ScriptFile
{
	// npcs
	private static final int KLAUDIA = 31001;
	private static final int JOHN = 32576;
	private static final int RAYMOND = 30289;
	private static final int LIGHT = 32575;
	private static final int BACK_LIGHT = 32579;
	private static final int IASON = 30969;
	private static final int PASSDEVICE = 32577;
	private static final int IDENTIFER = 32578;
	private static final int SHKAF = 32580;

	private static final int[][] DOORS = new int[][] { { 17240001, 17240002 }, { 17240003, 17240004 } };
	private static final int[] libdoors = new int[] { 17240005, 17240006 };

	private static final Location[] priest_spawn_locs = new Location[] {
			new Location(-79229, 205782, -7896, 28672),
			new Location(-79362, 205706, -7896, 16383),
			new Location(-79495, 205774, -7896, 4096),
			new Location(-79493, 205930, -7896, 61440),
			new Location(-79362, 206012, -7896, 49152),
			new Location(-79230, 205935, -7896, 36864) };

	// 1 - точка спауна, 2 - куда идти
	private static final Location[] zone01_01 = new Location[] { new Location(-75650, 212107, -7312), new Location(-75373, 212107, -7312) };
	private static final Location[] zone01_02 = new Location[] { new Location(-74854, 212107, -7312), new Location(-75046, 212107, -7312) };
	private static final Location[] zone01_03 = new Location[] { new Location(-74532, 212108, -7312), new Location(-74255, 212108, -7312) };
	private static final Location[] zone01_04 = new Location[] { new Location(-75200, 211178, -7312), new Location(-75200, 211465, -7312) };
	private static final Location[] zone01_05 = new Location[] { new Location(-74701, 211460, -7312), new Location(-74701, 211172, -7312) };
	private static final Location[] zone02_01 = new Location[] { new Location(-75190, 210175, -7408), new Location(-74750, 210174, -7408) };
	private static final Location[] zone02_02 = new Location[] { new Location(-74743, 209820, -7408), new Location(-75168, 209820, -7408) };
	private static final Location[] zone03_01 = new Location[] { new Location(-75559, 207860, -7504), new Location(-75559, 208813, -7504) };
	private static final Location[] zone03_02 = new Location[] { new Location(-74227, 208290, -7504), new Location(-74508, 208290, -7504) };
	private static final Location[] zone03_03 = new Location[] { new Location(-74522, 207063, -7504), new Location(-74206, 207064, -7504) };
	private static final Location[] zone03_04 = new Location[] { new Location(-74956, 206348, -7504), new Location(-74956, 206686, -7504) };
	private static final Location[] zone03_05 = new Location[] { new Location(-75402, 206939, -7504), new Location(-75693, 206939, -7504) };
	private static final Location[] zone04_01 = new Location[] { new Location(-76378, 207852, -7600), new Location(-76628, 207852, -7600) };
	private static final Location[] zone04_02 = new Location[] { new Location(-76628, 208151, -7600), new Location(-76367, 208151, -6400) };
	private static final Location[] zone04_03 = new Location[] { new Location(-76374, 208848, -7600), new Location(-76632, 208848, -7600) };
	private static final Location[] zone04_04 = new Location[] { new Location(-76928, 209189, -7600), new Location(-76928, 209446, -7600) };
	private static final Location[] zone04_05 = new Location[] { new Location(-77183, 209443, -7600), new Location(-77183, 209188, -7600) };
	private static final Location[] zone05_02 = new Location[] { new Location(-78521, 208035, -7696), new Location(-78065, 208036, -7696) };
	private static final Location[] zone05_03 = new Location[] { new Location(-76881, 208037, -7696), new Location(-77340, 208036, -7696) };
	private static final Location[] zone05_06 = new Location[] { new Location(-77336, 207428, -7696), new Location(-77013, 207105, -7696) };
	private static final Location[] zone06_01 = new Location[] { new Location(-78930, 206101, -7888), new Location(-78710, 206306, -7872) };
	private static final Location[] zone06_02 = new Location[] { new Location(-78855, 206443, -7888), new Location(-79066, 206237, -7888) };
	private static final Location[] zone06_03 = new Location[] { new Location(-79357, 206713, -7888), new Location(-79357, 206330, -7888) };
	private static final Location[] zone06_04 = new Location[] { new Location(-79656, 206246, -7888), new Location(-79858, 206454, -7872) };
	private static final Location[] zone06_05 = new Location[] { new Location(-79999, 206302, -7888), new Location(-79787, 206104, -7888) };
	private static final Location[] zone06_06 = new Location[] { new Location(-79781, 205602, -7888), new Location(-79991, 205392, -7888) };
	private static final Location[] zone06_07 = new Location[] { new Location(-79849, 205260, -7888), new Location(-79641, 205466, -7888) };
	private static final Location[] zone06_08 = new Location[] { new Location(-79363, 205379, -7888), new Location(-79364, 204964, -7888) };
	private static final Location[] zone06_09 = new Location[] { new Location(-78870, 205253, -7888), new Location(-79085, 205454, -7888) };
	private static final Location[] zone06_10 = new Location[] { new Location(-78942, 205599, -7888), new Location(-78731, 205386, -7872) };
	private static final Location[] zone03_06 = new Location[] { new Location(-74246, 206515, -7504), new Location(-75668, 206515, -7504) };
	private static final Location[] zone05_01 = new Location[] { new Location(-78054, 208464, -7696), new Location(-77361, 208464, -7696) };
	private static final Location[] zone05_04 = new Location[] { new Location(-78335, 207793, -7696), new Location(-77060, 207793, -7696) };
	private static final Location[] zone05_05 = new Location[] { new Location(-77702, 207414, -7696), new Location(-77702, 208184, -7696) };
	private static final Location[] zone07_01 = new Location[] { new Location(-81938, 205856, -7984), new Location(-81125, 205855, -7984) };

	// items
	private static final int IDENTITY_CARD = 13822;
	private static final int SHUNAI_CONTRACT = 13823;

	// Transformation's skills
	private static final int GuardsOfTheDawn = 6204;

	private final static TIntLongHashMap _instances = new TIntLongHashMap();

	private static final class SecretRitualWorld extends InstanceWorld
	{
		public int status;
	}

	public _195_SevenSignsSecretRitualPriests()
	{
		super(195, -1);
		addStartNpc(KLAUDIA);
		addTalkId(JOHN, RAYMOND, LIGHT, IDENTIFER, PASSDEVICE, SHKAF, IASON, BACK_LIGHT);
		addQuestItem(IDENTITY_CARD, SHUNAI_CONTRACT);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		L2Player player = st.getPlayer();

		if(event.equalsIgnoreCase("31001-05.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("32576-02.htm"))
		{
			st.set("cond", "2");
			st.giveItems(IDENTITY_CARD, 1);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30289-04.htm"))
		{
			if(st.getInt("cond") < 3)
			{
				st.set("cond", "3");
				st.playSound(SOUND_MIDDLE);
			}
			player.stopAllEffects();
			player.stopTransformation();
			SkillTable.getInstance().getInfo(GuardsOfTheDawn, 1).getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("30289-07.htm"))
		{
			player.stopAllEffects();
			player.stopTransformation();
			SkillTable.getInstance().getInfo(GuardsOfTheDawn, 1).getEffects(player, player, false, false);
		}
		else if(event.equalsIgnoreCase("30289-08.htm"))
		{
			player.stopAllEffects();
			player.stopTransformation();
		}
		else if(event.equalsIgnoreCase("30969-03.htm"))
		{
			if(player.isSubClassActive())
				return "noquest";

			st.takeItems(SHUNAI_CONTRACT, 1);
			st.addExpAndSp(52518015, 5817677);
			st.setState(COMPLETED);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
			_instances.remove(player.getObjectId());
		}
		else if(event.equalsIgnoreCase("32580-02.htm"))
			st.giveItems(SHUNAI_CONTRACT, 1, false);
		else if(event.equalsIgnoreCase("32578-03.htm"))
		{
			Reflection r = player.getReflection();
			SecretRitualWorld world = InstanceManager.getInstance().getWorld(r.getId(), SecretRitualWorld.class);
			if(st.getQuestItemsCount(IDENTITY_CARD) > 0 && world != null)
				switch (Math.abs(npc.getX()))
				{
					case 75695: // первый проход
					{
						if(world.status == 0)
						{
							for(int doorId : DOORS[0])
								r.openDoor(doorId);
							player.broadcastPacket(new ExShowScreenMessage("", 4000, ScreenMessageAlign.TOP_CENTER, true, 0, 3033, false));
							player.sendPacket(new SystemMessage(3033));
							player.sendPacket(new SystemMessage(3037));
							player.sendPacket(new SystemMessage(3038));
							world.status = 1;
						}
						return null;
					}
					case 78289: // второй проход, с отображением ролика
					{
						if(world.status == 1)
						{
							for(int doorId : DOORS[1])
								r.openDoor(doorId);
							player.showQuestMovie(ExStartScenePlayer.SCENE_SSQ_RITUAL_OF_PRIEST);
							st.startQuestTimer("spawnPriest", 30000); // TODO
							player.sendPacket(new SystemMessage(3034));
							world.status = 2;
						}
						return null;
					}
				}
			else
			{
				if(r.getReturnLoc() != null)
					player.teleToLocation(r.getReturnLoc(), 0);
				else
					player.setReflection(0);
				player.unsetVar("backCoords");
				return null;
			}
		}
		else
		{
			Reflection r = player.getReflection();
			if(event.equalsIgnoreCase("EnterLibrary"))
			{
				for(int doorId : libdoors)
					r.openDoor(doorId);
				return null;
			}
			if(event.equalsIgnoreCase("spawnPriest"))
			{
				for(Location loc : priest_spawn_locs)
					addSpawnToInstance(18828, loc, false, r.getId());
				return null;
			}
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		int id = st.getState();
		L2Player player = st.getPlayer();
		Reflection r = player.getReflection();

		if(npcId == KLAUDIA)
			if(id == CREATED)
			{
				QuestState qs = st.getPlayer().getQuestState("_194_SevenSignContractOfMammon");
				if(qs != null && qs.isCompleted() && player.getLevel() >= 79 || player.isGM())
					htmltext = "31001-01.htm";
				else
				{
					htmltext = "31001-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "31001-06.htm";
		if(npcId == JOHN)
			if(cond == 1)
				htmltext = "32576-01.htm";
			else if(cond == 2)
				htmltext = "32576-03.htm";
		if(npcId == RAYMOND)
			if(cond == 2)
				htmltext = "30289-01.htm";
			else if(cond == 3)
			{
				if(st.getQuestItemsCount(SHUNAI_CONTRACT) != 0)
				{
					htmltext = "30289-09.htm";
					player.stopAllEffects();
					st.set("cond", "4");
					st.playSound(SOUND_MIDDLE);
				}
				else
					htmltext = "30289-06.htm";
			}
			else if(cond == 4)
				htmltext = "30289-09.htm";
		if(npcId == LIGHT)
			if(player.getTransformationId() == 113)
			{
				htmltext = "32575-02.htm";
				enterInstance(player);
			}
			else
				htmltext = "32575-01.htm";
		if(npcId == PASSDEVICE && player.getTransformationId() == 113)
			htmltext = "32577-01.htm";
		if(npcId == BACK_LIGHT)
		{
			htmltext = "32579-01.htm";
			if(r.getReturnLoc() != null)
				player.teleToLocation(r.getReturnLoc(), 0);
			else
				player.setReflection(0);
			player.unsetVar("backCoords");
		}
		if(npcId == IDENTIFER)
			if(player.getTransformationId() == 113)
				htmltext = "32578-01.htm";
			else
				htmltext = "32578-02.htm";
		if(npcId == SHKAF && st.getQuestItemsCount(SHUNAI_CONTRACT) == 0)
			htmltext = "32580-01.htm";
		if(npcId == IASON && st.getQuestItemsCount(SHUNAI_CONTRACT) != 0)
			htmltext = "30969-01.htm";
		return htmltext;
	}

	private void enterInstance(L2Player player)
	{
		int instancedZoneId = ReflectionTable.SANCTUM_OF_THE_LORDS_OF_DAWN;
		InstanceManager ilm = InstanceManager.getInstance();
		TIntObjectHashMap<Instance> ils = ilm.getById(instancedZoneId);
		if(ils == null)
		{
			player.sendPacket(Msg.SYSTEM_ERROR);
			return;
		}

		Instance il = ils.get(0);
		assert il != null;

		if(player.isInParty())
		{
			player.sendPacket(Msg.A_PARTY_CANNOT_BE_FORMED_IN_THIS_AREA);
			return;
		}

		if(player.isCursedWeaponEquipped())
		{
			player.sendPacket(new SystemMessage(SystemMessage.C1S_QUEST_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
			return;
		}

		// Првоеряем если инстанс для этого игрока уже был создан
		long old = _instances.get(player.getObjectId());
		if(old > 0)
		{
			Reflection old_r = ReflectionTable.getInstance().get(old);
			if(old_r != null)
			{
				player.setReflection(old_r);
				player.teleToLocation(il.getTeleportCoords());
				player.setVar("backCoords", old_r.getReturnLoc().toXYZString());
				return;
			}
		}

		SecretRitualWorld world = new SecretRitualWorld();
		Reflection r = new Reflection(il.getName());
		r.setInstancedZoneId(instancedZoneId);

		InstanceManager.getInstance().addWorld(r.getId(), world);

		for(Instance i : ils.valueCollection())
		{
			if(r.getReturnLoc() == null)
				r.setReturnLoc(i.getReturnCoords());
			if(r.getTeleportLoc() == null)
				r.setTeleportLoc(i.getTeleportCoords());
			r.fillSpawns(i.getSpawnsInfo());
			r.fillDoors(i.getDoors());
		}

		int timelimit = il.getDuration();

		player.setReflection(r);
		player.teleToLocation(il.getTeleportCoords());
		player.setVar("backCoords", r.getReturnLoc().toXYZString());

		r.setEmptyDestroyTime(10 * 60 * 1000);
		r.startCollapseTimer(timelimit * 60 * 1000);
		spawnWalkingGuards(r.getId());

		_instances.put(player.getObjectId(), r.getId());
	}

	@Override
	public void onAbort(QuestState qs)
	{
		// при отмене квеста закрываем инстанс если он есть
		L2Player player = qs.getPlayer();
		if(player != null)
		{
			long refId;
			if((refId = _instances.remove(player.getObjectId())) > 0)
			{
				Reflection old_r = ReflectionTable.getInstance().get(refId);
				if(old_r != null)
					old_r.startCollapseTimer(10000);
			}
		}
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private void spawnWalkingGuards(long refId)
	{
		spawnGuard(27351, 4, 300, refId, zone07_01);
		spawnGuard(18835, 1, 150, refId, zone01_01);
		spawnGuard(18835, 1, 150, refId, zone01_02);
		spawnGuard(18835, 1, 150, refId, zone01_03);
		spawnGuard(18835, 1, 150, refId, zone01_04);
		spawnGuard(18835, 1, 150, refId, zone01_05);
		spawnGuard(18834, 1, 150, refId, zone02_01);
		spawnGuard(18834, 1, 150, refId, zone02_02);
		spawnGuard(18834, 2, 150, refId, zone03_01);
		spawnGuard(18834, 2, 150, refId, zone03_02);
		spawnGuard(18834, 2, 150, refId, zone03_03);
		spawnGuard(18834, 2, 150, refId, zone03_04);
		spawnGuard(18834, 2, 150, refId, zone03_06);
		spawnGuard(18834, 2, 150, refId, zone03_05);
		spawnGuard(18834, 2, 150, refId, zone04_01);
		spawnGuard(18834, 2, 150, refId, zone04_02);
		spawnGuard(18834, 2, 150, refId, zone04_03);
		spawnGuard(18834, 2, 150, refId, zone04_04);
		spawnGuard(18834, 2, 150, refId, zone04_05);
		spawnGuard(18835, 3, 150, refId, zone05_02);
		spawnGuard(18835, 3, 150, refId, zone05_03);
		spawnGuard(18835, 3, 150, refId, zone05_01);
		spawnGuard(18835, 3, 200, refId, zone05_04);
		spawnGuard(18835, 3, 200, refId, zone05_05);
		spawnGuard(18835, 3, 150, refId, zone05_06);
		spawnGuard(18834, 3, 150, refId, zone06_01);
		spawnGuard(18834, 3, 150, refId, zone06_02);
		spawnGuard(18834, 3, 150, refId, zone06_03);
		spawnGuard(18834, 3, 150, refId, zone06_04);
		spawnGuard(18834, 3, 150, refId, zone06_05);
		spawnGuard(18834, 3, 150, refId, zone06_06);
		spawnGuard(18834, 3, 150, refId, zone06_07);
		spawnGuard(18834, 3, 150, refId, zone06_08);
		spawnGuard(18834, 3, 150, refId, zone06_09);
		spawnGuard(18834, 3, 150, refId, zone06_10);

		spawnGuard(18834, 1, 150, refId, new Location(-74934, 213446, -7232, 33334));
		spawnGuard(18835, 1, 150, refId, new Location(-74951, 211629, -7321, 16384));
		spawnGuard(27351, 1, 300, refId, new Location(-74619, 209981, -7418, 30212));
		spawnGuard(27351, 1, 300, refId, new Location(-75301, 209980, -7418, 1722));
		spawnGuard(27351, 2, 300, refId, new Location(-74558, 206625, -7520, 65102));
		spawnGuard(27351, 2, 300, refId, new Location(-75454, 206740, -7520, 34645));
		spawnGuard(18834, 2, 250, refId, new Location(-74955, 207611, -7520, 0));
		spawnGuard(18834, 2, 200, refId, new Location(-75654, 208112, -7520, 2718));
		spawnGuard(18834, 2, 200, refId, new Location(-75428, 208115, -7520, 32768));
		spawnGuard(18835, 3, 150, refId, new Location(-77718, 207512, -7712, 24550));
		spawnGuard(18835, 3, 150, refId, new Location(-77216, 208297, -7712, 35486));
		spawnGuard(18835, 3, 150, refId, new Location(-77558, 207138, -7712, 17906));
		spawnGuard(18835, 3, 150, refId, new Location(-78113, 207384, -7705, 41575));
		spawnGuard(18835, 3, 150, refId, new Location(-78346, 207146, -7706, 8680));
		spawnGuard(18835, 3, 150, refId, new Location(-77159, 207642, -7712, 32460));
		spawnGuard(27351, 3, 200, refId, new Location(-78926, 205432, -7904, 23278));
		spawnGuard(27351, 3, 200, refId, new Location(-79813, 205426, -7904, 9231));
		spawnGuard(27351, 3, 200, refId, new Location(-79814, 206277, -7904, 59013));
		spawnGuard(27351, 3, 200, refId, new Location(-78891, 206272, -7904, 59013));
		spawnGuard(27351, 4, 300, refId, new Location(-81535, 205503, -8000, 16384));
		spawnGuard(27351, 4, 300, refId, new Location(-81536, 206223, -8000, 49151));
		spawnGuard(27351, 3, 300, refId, new Location(-77703, 208320, -7712, 16384));
		spawnGuard(27351, 3, 300, refId, new Location(-77703, 207275, -7712, 49151));
		spawnGuard(27351, 3, 300, refId, new Location(-76962, 207802, -7712, 35928));
		spawnGuard(27351, 2, 300, refId, new Location(-74282, 208784, -7520, 40959));
	}

	private void spawnGuard(int npcId, int position, int agro, long refId, Location... zones)
	{
		L2NpcTemplate template = NpcTable.getTemplate(npcId);
		if(template != null)
		{
			L2NpcInstance npc = NpcTable.getTemplate(npcId).getNewInstance();
			GuardsOfDawn ai = new GuardsOfDawn(npc, zones.length > 1 ? zones[0] : null, zones.length > 1 ? zones[1] : null, position, agro);
			npc.setAI(ai);
			npc.setSpawnedLoc(zones[0]);
			npc.onSpawn();
			npc.spawnMe(npc.getSpawnedLoc());
			npc.setReflection(refId);
			ai.startAITask();
		}
	}
}
