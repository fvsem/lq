package quests._151_SaveMySister;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _151_SaveMySister extends Quest implements ScriptFile
{
	private static int POISON_SAC = 703;
	private static int FEVER_MEDICINE = 704;
	private static int ROUND_SHIELD = 102;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _151_SaveMySister()
	{
		super(151, -1);

		addStartNpc(30050);

		addTalkId(30050);
		addTalkId(30032);

		addKillId(20103);
		addKillId(20106);
		addKillId(20108);

		addQuestItem(FEVER_MEDICINE, POISON_SAC);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30050-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		int cond = 0;
		if(id != CREATED)
			cond = st.getInt("cond");
		if(npcId == 30050)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 15)
					htmltext = "30050-02.htm";
				else
				{
					htmltext = "30050-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && st.getQuestItemsCount(POISON_SAC) == 0 && st.getQuestItemsCount(FEVER_MEDICINE) == 0)
				htmltext = "30050-04.htm";
			else if(cond == 1 && st.getQuestItemsCount(POISON_SAC) == 1)
				htmltext = "30050-05.htm";
			else if(cond == 3 && st.getQuestItemsCount(FEVER_MEDICINE) == 1)
			{
				st.giveItems(ROUND_SHIELD, 1);
				st.takeItems(FEVER_MEDICINE, -1);
				st.addExpAndSp(13106, 613, true);
				htmltext = "30050-06.htm";
				st.unset("cond");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
		}
		else if(npcId == 30032)
			if(cond == 2 && st.getQuestItemsCount(POISON_SAC) > 0)
			{
				st.giveItems(FEVER_MEDICINE, 1);
				st.takeItems(POISON_SAC, -1);
				st.set("cond", "3");
				htmltext = "30032-01.htm";
			}
			else if(cond == 3 && st.getQuestItemsCount(FEVER_MEDICINE) > 0)
				htmltext = "30032-02.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if((npcId == 20103 || npcId == 20106 || npcId == 20108) && st.getQuestItemsCount(POISON_SAC) == 0 && st.getInt("cond") == 1 && Rnd.chance(20))
		{
			st.set("cond", "2");
			st.giveItems(POISON_SAC, 1);
			st.playSound(SOUND_MIDDLE);
		}
		return null;
	}
}
