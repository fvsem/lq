package quests._028_ChestCaughtWithABaitOfIcyAir;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _028_ChestCaughtWithABaitOfIcyAir extends Quest implements ScriptFile
{
	int OFulle = 31572;
	int Kiki = 31442;

	int BigYellowTreasureChest = 6503;
	int KikisLetter = 7626;
	int ElvenRing = 881;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _028_ChestCaughtWithABaitOfIcyAir()
	{
		super(28, -1);

		addStartNpc(OFulle);

		addTalkId(OFulle);
		addTalkId(Kiki);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("31572-04.htm"))
		{
			st.set("cond", "1");
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equals("31572-07.htm"))
		{
			if(st.getQuestItemsCount(BigYellowTreasureChest) > 0)
			{
				st.set("cond", "2");
				st.takeItems(BigYellowTreasureChest, 1);
				st.giveItems(KikisLetter, 1);
				st.playSound(SOUND_MIDDLE);
			}
			else
				htmltext = "31572-08.htm";
		}
		else if(event.equals("31442-02.htm"))
			if(st.getQuestItemsCount(KikisLetter) == 1)
			{
				htmltext = "31442-02.htm";
				st.takeItems(KikisLetter, -1);
				st.giveItems(ElvenRing, 1);
				st.set("cond", "0");
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else
			{
				htmltext = "31442-03.htm";
				st.exitCurrentQuest(true);
			}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			st.setState(STARTED);
			st.set("cond", "0");
		}
		int cond = st.getInt("cond");
		if(npcId == OFulle)
		{
			if(cond == 0 && id == STARTED)
			{
				int PlayerLevel = st.getPlayer().getLevel();
				if(PlayerLevel < 36)
				{
					QuestState OFullesSpecialBait = st.getPlayer().getQuestState("_051_OFullesSpecialBait");
					if(OFullesSpecialBait != null)
					{
						if(OFullesSpecialBait.isCompleted())
							htmltext = "31572-01.htm";
						else
						{
							htmltext = "31572-02.htm";
							st.exitCurrentQuest(true);
						}
					}
					else
					{
						htmltext = "31572-02.htm";
						st.exitCurrentQuest(true);
					}
				}
				else
					htmltext = "31572-01.htm";
			}
			else if(cond == 1)
			{
				htmltext = "31572-05.htm";
				if(st.getQuestItemsCount(BigYellowTreasureChest) == 0)
					htmltext = "31572-06.htm";
			}
			else if(cond == 2)
				htmltext = "31572-09.htm";
		}
		else if(npcId == Kiki)
			if(cond == 2)
				htmltext = "31442-01.htm";
		return htmltext;
	}
}
