package quests._009_IntoTheCityOfHumans;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;

public class _009_IntoTheCityOfHumans extends Quest implements ScriptFile
{
	// NPC
	public final int PETUKAI = 30583;
	public final int TANAPI = 30571;
	public final int TAMIL = 30576;
	// Items
	public final int SCROLL_OF_ESCAPE_GIRAN = 7126;
	// Quest Item
	public final int MARK_OF_TRAVELER = 7570;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _009_IntoTheCityOfHumans()
	{
		super(9, -1);

		addStartNpc(PETUKAI);

		addTalkId(PETUKAI);
		addTalkId(TANAPI);
		addTalkId(TAMIL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("30583-03.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30571-02.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("30576-02.htm"))
		{
			st.giveItems(SCROLL_OF_ESCAPE_GIRAN, 1);
			st.giveItems(MARK_OF_TRAVELER, 1);
			st.unset("cond");
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(false);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		if(npcId == PETUKAI)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() == 3 && st.getPlayer().getLevel() >= 3)
					htmltext = "30583-02.htm";
				else
				{
					htmltext = "30583-01.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "30583-04.htm";
		}
		else if(npcId == TANAPI)
		{
			if(cond == 1)
				htmltext = "30571-01.htm";
			else if(cond == 2)
				htmltext = "30571-03.htm";
		}
		else if(npcId == TAMIL)
			if(cond == 2)
				htmltext = "30576-01.htm";
		return htmltext;
	}
}
