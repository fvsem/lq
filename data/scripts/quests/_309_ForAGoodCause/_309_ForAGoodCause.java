package quests._309_ForAGoodCause;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import quests._239_WontYouJoinUs._239_WontYouJoinUs;
import quests._308_ReedFieldMaintenance._308_ReedFieldMaintenance;

public class _309_ForAGoodCause extends Quest implements ScriptFile
{
	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	private static final int Atra = 32647;

	private static final int MucrokianHide = 14873;
	private static final int FallenMucrokianHide = 14874;

	private static final int MucrokianFanatic = 22650;
	private static final int MucrokianAscetic = 22651;
	private static final int MucrokianSavior = 22652;
	private static final int MucrokianPreacher = 22653;
	private static final int ContaminatedMucrokian = 22654;
	private static final int ChangedMucrokian = 22655;

	public _309_ForAGoodCause()
	{
		super(309, PARTY_NONE);
		addStartNpc(Atra);
		addQuestItem(MucrokianHide, FallenMucrokianHide);
		addKillId(MucrokianFanatic, MucrokianAscetic, MucrokianSavior, MucrokianPreacher, ContaminatedMucrokian, ChangedMucrokian);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("32647-05.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
		}
		if(event.equalsIgnoreCase("32646-14.htm"))
			st.exitCurrentQuest(true);
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int id = st.getState();
		int cond = st.getInt("cond");

		if(npcId == Atra)
			if(id == CREATED)
			{
				QuestState qs1 = st.getPlayer().getQuestState(_308_ReedFieldMaintenance.class);
				if(qs1 != null && qs1.isStarted())
					return "32647-17.htm"; // нельзя брать оба квеста сразу
				if(st.getPlayer().getLevel() < 82)
					return "32647-00.htm";
				return "32647-01.htm";
			}
			else if(cond == 1)
			{
				long fallen = st.takeAllItems(FallenMucrokianHide);
				if(fallen > 0)
					st.giveItems(MucrokianHide, fallen * 2);

				if(st.getQuestItemsCount(MucrokianHide) == 0)
					return "32647-06.htm"; // нечего менять
				else if(!st.getPlayer().isQuestCompleted(_239_WontYouJoinUs.class))
					return "32647-a1.htm"; // обычные цены
				else
					return "32647-a2.htm"; // со скидкой
			}

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		st.rollAndGive(npc.getNpcId() == ContaminatedMucrokian ? FallenMucrokianHide : MucrokianHide, 1, 60);
		return null;
	}
}
