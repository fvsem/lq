package quests._106_ForgottenTruth;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Rnd;

public class _106_ForgottenTruth extends Quest implements ScriptFile
{
	private static int ONYX_TALISMAN1 = 984;
	private static int ONYX_TALISMAN2 = 985;
	private static int ANCIENT_SCROLL = 986;
	private static int ANCIENT_CLAY_TABLET = 987;
	private static int KARTAS_TRANSLATION = 988;
	private static int ELDRITCH_DAGGER = 989;

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _106_ForgottenTruth()
	{
		super(106, -1);

		addStartNpc(30358);
		addTalkId(30358);
		addTalkId(30133);

		addKillId(27070);

		addQuestItem(new int[] { KARTAS_TRANSLATION, ONYX_TALISMAN1, ONYX_TALISMAN2, ANCIENT_SCROLL, ANCIENT_CLAY_TABLET });
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		String htmltext = event;
		if(event.equals("30358-05.htm"))
		{
			st.giveItems(ONYX_TALISMAN1, 1);
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getInt("cond");
		if(npcId == 30358)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getRace().ordinal() != 2)
				{
					htmltext = "30358-00.htm";
					st.exitCurrentQuest(true);
				}
				else if(st.getPlayer().getLevel() >= 10)
					htmltext = "30358-03.htm";
				else
				{
					htmltext = "30358-02.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond > 0 && (st.getQuestItemsCount(ONYX_TALISMAN1) > 0 || st.getQuestItemsCount(ONYX_TALISMAN2) > 0) && st.getQuestItemsCount(KARTAS_TRANSLATION) == 0)
				htmltext = "30358-06.htm";
			else if(cond == 4 && st.getQuestItemsCount(KARTAS_TRANSLATION) > 0)
			{
				htmltext = "30358-07.htm";
				st.takeItems(KARTAS_TRANSLATION, -1);
				st.giveItems(ELDRITCH_DAGGER, 1);
				for(int item = 4412; item <= 4417; item++)
					st.giveItems(item, 10);
				st.giveItems(1060, 100);
				if(st.getPlayer().getClassId().isMage())
				{
					st.giveItems(5790, 3000);
					st.playTutorialVoice("tutorial_voice_027");
					st.giveItems(2509, 500);
				}
				else
				{
					st.giveItems(1835, 1000);
					st.giveItems(5789, 6000);
					st.playTutorialVoice("tutorial_voice_026");
				}
				st.exitCurrentQuest(false);
				st.playSound(SOUND_FINISH);
			}
		}
		else if(npcId == 30133)
			if(cond == 1 && st.getQuestItemsCount(ONYX_TALISMAN1) > 0)
			{
				htmltext = "30133-01.htm";
				st.takeItems(ONYX_TALISMAN1, -1);
				st.giveItems(ONYX_TALISMAN2, 1);
				st.set("cond", "2");
			}
			else if(cond == 2 && st.getQuestItemsCount(ONYX_TALISMAN2) > 0 && (st.getQuestItemsCount(ANCIENT_SCROLL) == 0 || st.getQuestItemsCount(ANCIENT_CLAY_TABLET) == 0))
				htmltext = "30133-02.htm";
			else if(cond == 3 && st.getQuestItemsCount(ANCIENT_SCROLL) > 0 && st.getQuestItemsCount(ANCIENT_CLAY_TABLET) > 0)
			{
				htmltext = "30133-03.htm";
				st.takeItems(ONYX_TALISMAN2, -1);
				st.takeItems(ANCIENT_SCROLL, -1);
				st.takeItems(ANCIENT_CLAY_TABLET, -1);
				st.giveItems(KARTAS_TRANSLATION, 1);
				st.set("cond", "4");
			}
			else if(cond == 4 && st.getQuestItemsCount(KARTAS_TRANSLATION) > 0)
				htmltext = "30133-04.htm";
		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		if(npcId == 27070)
			if(st.getInt("cond") == 2 && st.getQuestItemsCount(ONYX_TALISMAN2) > 0)
				if(Rnd.chance(20) && st.getQuestItemsCount(ANCIENT_SCROLL) == 0)
				{
					st.giveItems(ANCIENT_SCROLL, 1);
					st.playSound(SOUND_MIDDLE);
				}
				else if(Rnd.chance(10) && st.getQuestItemsCount(ANCIENT_CLAY_TABLET) == 0)
				{
					st.giveItems(ANCIENT_CLAY_TABLET, 1);
					st.playSound(SOUND_MIDDLE);
				}
		if(st.getQuestItemsCount(ANCIENT_SCROLL) > 0 && st.getQuestItemsCount(ANCIENT_CLAY_TABLET) > 0)
			st.set("cond", "3");
		return null;
	}
}
