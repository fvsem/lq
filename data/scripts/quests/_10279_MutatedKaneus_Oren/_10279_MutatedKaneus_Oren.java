package quests._10279_MutatedKaneus_Oren;

import l2n.extensions.scripts.ScriptFile;
import quests.MutatedKaneusSC.MutatedKaneusSC;

/**
 * <hr>
 * <em>Квест</em> <strong>Mutated Kaneus - Oren</strong>
 * <hr>
 * 
 * @author HellSinger
 * @lastfix
 * @version GF
 */
public class _10279_MutatedKaneus_Oren extends MutatedKaneusSC implements ScriptFile
{
	@Override
	public void onLoad()
	{
		super.onLoad();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	public _10279_MutatedKaneus_Oren()
	{
		super(10279, "_10279_MutatedKaneus_Oren", "Mutated Kaneus - Oren");

		NPC = new int[] { 30196, 30189 };
		// dropList statement {mobId, itemId}
		dropList = new int[][] { { 18566, 13836 }, // Kaim Abigore
				{ 18568, 13837 } // Knight Montagnar
		};
		startLevel = 48;
		registerNPCs();
	}
}
