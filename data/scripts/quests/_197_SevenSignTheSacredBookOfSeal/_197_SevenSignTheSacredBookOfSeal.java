package quests._197_SevenSignTheSacredBookOfSeal;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.ai.CtrlIntention;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.model.quest.Quest;
import l2n.game.model.quest.QuestState;
import l2n.util.Location;

/**
 * @<!-- L2System -->
 * @date 17.11.2010
 * @time 3:17:03
 */
public class _197_SevenSignTheSacredBookOfSeal extends Quest implements ScriptFile
{
	// npcs
	private static final int WOOD = 32593;
	private static final int ORVEN = 30857;
	private static final int LEOPARD = 32594;
	private static final int LAWRENCE = 32595;
	private static final int SOFIA = 32596;

	// monster
	private static final int SHILENSEVIL = 27343;

	// items
	private static final int TEXT = 13829;
	private static final int SCULPTURE = 14356;

	private static final Location shilenloc = new Location(152520, -57685, -3438);

	private long shilen = 0;

	public _197_SevenSignTheSacredBookOfSeal()
	{
		super(197, -1);

		addStartNpc(WOOD);
		addTalkId(WOOD, ORVEN, LEOPARD, LAWRENCE, SOFIA);
		addQuestItem(TEXT, SCULPTURE);
		addKillId(SHILENSEVIL);
	}

	@Override
	public String onEvent(String event, QuestState st, L2NpcInstance npc)
	{
		L2Player player = st.getPlayer();
		String htmltext = event;
		if(event.equalsIgnoreCase("32593-04.htm"))
		{
			st.set("cond", "1");
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("30857-04.htm"))
		{
			st.set("cond", "2");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32594-03.htm"))
		{
			st.set("cond", "3");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32595-04.htm"))
		{
			if(L2ObjectsStorage.getAsNpc(shilen) == null)
			{
				L2NpcInstance monster = addSpawn(SHILENSEVIL, shilenloc, 0, 60000);
				Functions.npcSayInRangeCustomMessage(monster, 300, "quests._197_SevenSignTheSacredBookOfSeal.ShilenSpawn");
				monster.setRunning();
				player.addDamageHate(monster, 0, 999);
				monster.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, player);
				shilen = monster.getStoredId();
			}
		}
		else if(event.equalsIgnoreCase("32595-08.htm"))
		{
			st.set("cond", "5");
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32596-04.htm"))
		{
			st.set("cond", "6");
			st.giveItems(TEXT, 1, false);
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("32593-08.htm"))
		{
			st.addExpAndSp(52518015, 5817676);
			st.unset("cond");
			st.takeItems(TEXT, -1);
			st.takeItems(SCULPTURE, -1);
			st.setState(COMPLETED);
			st.exitCurrentQuest(false);
			st.playSound(SOUND_FINISH);
		}
		return htmltext;
	}

	@Override
	public String onTalk(L2NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int npcId = npc.getNpcId();
		int cond = st.getInt("cond");
		L2Player player = st.getPlayer();

		if(npcId == WOOD)
		{
			if(cond == 0)
			{
				QuestState qs = st.getPlayer().getQuestState("_196_SevenSignSealOfTheEmperor");
				if(qs != null && qs.isCompleted() && player.getLevel() >= 79 || player.isGM())
					htmltext = "32593-01.htm";
				else
				{
					htmltext = "32593-00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1)
				htmltext = "32593-05.htm";
			else if(cond == 6)
				htmltext = "32593-06.htm";
		}
		else if(npcId == ORVEN)
		{
			if(cond == 1)
				htmltext = "30857-01.htm";
			else if(cond == 2)
				htmltext = "30857-05.htm";
		}
		else if(npcId == LEOPARD)
		{
			if(cond == 2)
				htmltext = "32594-01.htm";
			else if(cond == 3)
				htmltext = "32594-04.htm";
		}
		else if(npcId == LAWRENCE)
		{
			if(cond == 3)
				htmltext = "32595-01.htm";
			else if(cond == 4)
				htmltext = "32595-05.htm";
			else if(cond == 5)
				htmltext = "32595-09.htm";
		}
		else if(npcId == SOFIA)
			if(cond == 5)
				htmltext = "32596-01.htm";
			else if(cond == 6)
				htmltext = "32596-05.htm";

		return htmltext;
	}

	@Override
	public String onKill(L2NpcInstance npc, QuestState st)
	{
		if(npc.getStoredId() == shilen && st.getInt("cond") == 3 && npc.getNpcId() == SHILENSEVIL)
		{
			Functions.npcSayInRangeCustomMessage(npc, 300, "quests._197_SevenSignTheSacredBookOfSeal.ShilenDie", st.getPlayer().getName());
			st.giveItems(SCULPTURE, 1, false);
			st.set("cond", "4");
		}
		return null;
	}

	@Override
	public void onLoad()
	{
		printInfo();
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
