package events.ctf;

import java.util.Calendar;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import l2n.Config;
import l2n.Config.EventInterval;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.event.HWIDChecker;
import l2n.game.event.L2Event;
import l2n.game.event.L2EventTeam;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Territory;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.Revive;
import l2n.game.tables.DoorTable;
import l2n.game.tables.HeroSkillTable;
import l2n.game.tables.ItemTable;
import l2n.game.tables.ReflectionTable;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

/**
 * @<!-- L2System -->
 */
public class CtF extends L2Event implements ScriptFile
{
	private final static Logger _log = Logger.getLogger(CtF.class.getName());

	private static ScheduledFuture<?> _startTask;

	private static L2EventTeam[] teams = new L2EventTeam[2];
	private static final L2Skill[][] _eventBuffs = new L2Skill[2][];

	private static final HWIDChecker hwid_check = new HWIDChecker(Config.EVENT_CTF_CHECK_HWID);

	private static L2NpcInstance redFlag = null;
	private static L2NpcInstance blueFlag = null;

	private static boolean _isRegistrationActive = false;
	private static int _status = 0;
	private static int _time_to_start;
	private static int _category;
	private static int _minLevel;
	private static int _maxLevel;
	private static int _autoContinue = 0;

	private static boolean _active = false;
	private static boolean _addEventType = false;

	private static ScheduledFuture<?> _endTask;

	private final static L2Zone _zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 4, true);
	private final static L2Zone _blueBaseZone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 5, true);
	private final static L2Zone _redBaseZone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 6, true);

	private final static ZoneListener _zoneListener = new ZoneListener();
	private final static RedBaseZoneListener _redBaseZoneListener = new RedBaseZoneListener();
	private final static BlueBaseZoneListener _blueBaseZoneListener = new BlueBaseZoneListener();

	/** <font color=blue>Blue</font> */
	private final static L2Territory team1loc = new L2Territory(11000003);
	/** <font color=red>Red</font> */
	private final static L2Territory team2loc = new L2Territory(11000004);

	private final static Location blueFlagLoc = new Location(150760, 45848, -3408);
	private final static Location redFlagLoc = new Location(148232, 47688, -3408);

	private final static Reflection _reflection = new Reflection(ReflectionTable.EVENT_CTF);
	private final static int[] _doors = new int[] { 24190002, 24190003 };

	@Override
	public void onLoad()
	{
		_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);
		_blueBaseZone.getListenerEngine().addMethodInvokedListener(_blueBaseZoneListener);
		_redBaseZone.getListenerEngine().addMethodInvokedListener(_redBaseZoneListener);

		team1loc.add(149878, 47505, -3408, -3308);
		team1loc.add(150262, 47513, -3408, -3308);
		team1loc.add(150502, 47233, -3408, -3308);
		team1loc.add(150507, 46300, -3408, -3308);
		team1loc.add(150256, 46002, -3408, -3308);
		team1loc.add(149903, 46005, -3408, -3308);

		team2loc.add(149027, 46005, -3408, -3308);
		team2loc.add(148686, 46003, -3408, -3308);
		team2loc.add(148448, 46302, -3408, -3308);
		team2loc.add(148449, 47231, -3408, -3308);
		team2loc.add(148712, 47516, -3408, -3308);
		team2loc.add(149014, 47527, -3408, -3308);

		// Если ивент активен, но пробуем зашедулить
		if(isActive("CtF"))
		{
			_active = true;
			scheduleEventStart(true);
		}

		_log.info("Loaded Event: " + getEventType() + "[" + _active + "]");

		// дверь запили!
		for(final int doorId : _doors)
			_reflection.addDoor(DoorTable.getInstance().getDoor(doorId).clone());
		_eventBuffs[0] = Util.parseSkills(Config.EVENT_CTF_BUFFS_FIGHTER);
		_eventBuffs[1] = Util.parseSkills(Config.EVENT_CTF_BUFFS_MAGE);
	}

	@Override
	public void onReload()
	{
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
		if(_startTask != null)
			_startTask.cancel(true);
	}

	@Override
	public void onShutdown()
	{
		onReload();
	}

	@Override
	public void activateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("CtF", true))
		{
			// при активации ивента, если он не был активирован, то пробуем стартовать. Так как как таск стартует только при загрузке
			if(_startTask == null)
				scheduleEventStart(false);
			_log.info("Event '" + getEventType() + "' activated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStarted", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' already active.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	@Override
	public void deactivateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("CtF", false))
		{
			if(_startTask != null)
			{
				_startTask.cancel(true);
				_startTask = null;
			}
			_log.info("Event '" + getEventType() + "' deactivated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStoped", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' not active.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public boolean isRunned()
	{
		return _isRegistrationActive || _status > 0;
	}


	// Red flag
	public String DialogAppend_35423(final Integer val)
	{
		final L2Player player = (L2Player) getSelf();
		if(player.getTeam() != 1)
			return "";
		if(val == 0)
			return Files.read("data/scripts/events/ctf/35423.html", player).replaceAll("n1", "" + Rnd.get(100, 999)).replaceAll("n2", "" + Rnd.get(100, 999));
		return "";
	}

	// Blue flag
	public String DialogAppend_35426(final Integer val)
	{
		final L2Player player = (L2Player) getSelf();
		if(player.getTeam() != 2)
			return "";
		if(val == 0)
			return Files.read("data/scripts/events/ctf/35426.html", player).replaceAll("n1", "" + Rnd.get(100, 999)).replaceAll("n2", "" + Rnd.get(100, 999));
		return "";
	}

	public void capture(final String[] var)
	{
		final L2Player player = (L2Player) getSelf();
		if(var.length != 4)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		final L2NpcInstance npc = getNpc();

		if(player.isDead() || npc == null || !player.isInRange(npc, 200) || npc.getNpcId() != (player.getTeam() == 1 ? 35423 : 35426))
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		Integer base;
		Integer add1;
		Integer add2;
		Integer summ;
		try
		{
			base = Integer.valueOf(var[0]);
			add1 = Integer.valueOf(var[1]);
			add2 = Integer.valueOf(var[2]);
			summ = Integer.valueOf(var[3]);
		}
		catch(final Exception e)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		if(add1 + add2 != summ)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		if(base == 1 && blueFlag.isVisible()) // Синяя база
		{
			blueFlag.decayMe();
			addFlag(player, 13561);
		}

		if(base == 2 && redFlag.isVisible()) // Красная база
		{
			redFlag.decayMe();
			addFlag(player, 13560);
		}

		if(player.isInvisible())
			player.setInvisible(false, true);
	}
	@Override
	public L2Skill[] getFighterBuffs()
	{
		return _eventBuffs[0];
	}

	@Override
	public L2Skill[] getMageBuffs()
	{
		return _eventBuffs[1];
	}
	@Override
	public void start(final String... var)
	{
		if(isRunned())
		{
			_log.info("CtF: start task already running!");
			return;
		}

		if(!_addEventType)
		{
			_addEventType = true;
			addEventId(ScriptEventType.ON_DISCONNECT);
			addEventId(ScriptEventType.ON_ESCAPE);
			addEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
			addEventId(ScriptEventType.ON_DIE);
		}

		final L2Player player = (L2Player) getSelf();
		if(var.length != 2)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		Integer category;
		Integer autoContinue;
		try
		{
			category = Integer.valueOf(var[0]);
			autoContinue = Integer.valueOf(var[1]);
		}
		catch(final Exception e)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		_category = category;
		_autoContinue = autoContinue;

		if(_category == -1)
		{
			_minLevel = 1;
			_maxLevel = 85;
		}
		else
		{
			_minLevel = getMinLevelForCategory(_category);
			_maxLevel = getMaxLevelForCategory(_category);
		}

		if(_endTask != null)
		{
			show(new CustomMessage("common.TryLater", player), player);
			return;
		}

		_status = 0;
		_isRegistrationActive = true;
		_time_to_start = Config.EVENT_CFT_TIME_TO_START;

		teams = new L2EventTeam[2];
		teams[BLUE_TEAM] = new L2EventTeam(0, "CTF_BLUE");
		teams[RED_TEAM] = new L2EventTeam(1, "CTF_RED");

		if(redFlag != null)
			redFlag.deleteMe();
		if(blueFlag != null)
			blueFlag.deleteMe();

		redFlag = spawn(redFlagLoc, 35423, ReflectionTable.EVENT_CTF);
		blueFlag = spawn(blueFlagLoc, 35426, ReflectionTable.EVENT_CTF);

		redFlag.decayMe();
		blueFlag.decayMe();

		final String[] param = { String.valueOf(_time_to_start), String.valueOf(_minLevel), String.valueOf(_maxLevel) };
		sayToAll("scripts.events.CtF.AnnouncePreStart", param);

		executeTask("events.ctf.CtF", "question", EMPTY_ARG, 10000);
		executeTask("events.ctf.CtF", "announce", EMPTY_ARG, 60000);
		_log.info("CtF: start event [" + _category + "-" + _autoContinue + "]");
	}

	public static void question()
	{
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getLevel() >= _minLevel && player.getLevel() <= _maxLevel && player.isAllowEventQuestion())
				player.scriptRequest(new CustomMessage("scripts.events.CtF.AskPlayer", player).toString(), "events.ctf.CtF:addPlayer", EMPTY_ARG);
	}

	public static void announce()
	{
		if(_time_to_start <= 1 && (teams[BLUE_TEAM].isEmpty() || teams[RED_TEAM].isEmpty()))
		{
			sayToAll("scripts.events.CtF.AnnounceEventCancelled");
			_isRegistrationActive = false;
			_status = 0;
			executeTask("events.ctf.CtF", "autoContinue", EMPTY_ARG, 5000);
			return;
		}

		if(_time_to_start > 1)
		{
			_time_to_start--;
			final String[] param = { String.valueOf(_time_to_start), String.valueOf(_minLevel), String.valueOf(_maxLevel) };
			sayToAll("scripts.events.CtF.AnnouncePreStart", param);
			executeTask("events.ctf.CtF", "announce", EMPTY_ARG, 60000);
		}
		else
		{
			_status = 1;
			_isRegistrationActive = false;
			sayToAll("scripts.events.CtF.AnnounceEventStarting");
			executeTask("events.ctf.CtF", "prepare", EMPTY_ARG, 5000);
		}
	}

	public void addPlayer()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null || !checkPlayer(player, true))
			return;

		int team = 0;
		final int size1 = teams[RED_TEAM].getPlayersCount(), size2 = teams[BLUE_TEAM].getPlayersCount();

		if(size1 > size2)
			team = 2;
		else if(size1 < size2)
			team = 1;
		else
			team = Rnd.get(1, 2);

		if(!checkCountTeam(team))
		{
			show(new CustomMessage("scripts.events.MaxCountTeam", player), player);
			return;
		}

		if(team == 1)
		{
			teams[RED_TEAM].addPlayer(player);
			show(new CustomMessage("scripts.events.CtF.Registered", player), player);
		}
		else if(team == 2)
		{
			teams[BLUE_TEAM].addPlayer(player);
			show(new CustomMessage("scripts.events.CtF.Registered", player), player);
		}
		else
			_log.info("WTF??? Command id 0 in CtF...");
	}

	private static boolean checkCountTeam(final int team)
	{
		if(team == 1 && teams[BLUE_TEAM].getPlayersCount() >= Config.EVENT_CTF_MAX_LENGTH_TEAM)
			return false;
		else if(team == 2 && teams[RED_TEAM].getPlayersCount() >= Config.EVENT_CTF_MAX_LENGTH_TEAM)
			return false;

		return true;
	}

	public static boolean checkPlayer(final L2Player player, final boolean first)
	{
		if(first && !_isRegistrationActive)
		{
			show(new CustomMessage("scripts.events.Late", player), player);
			return false;
		}

		if(first && player.isDead())
			return false;

		if(first && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())))
		{
			show(new CustomMessage("scripts.events.CtF.Cancelled", player), player);
			return false;
		}

		if(player.getLevel() < _minLevel || player.getLevel() > _maxLevel)
		{
			show(new CustomMessage("scripts.events.CtF.CancelledLevel", player), player);
			return false;
		}

		if(player.isMounted())
		{
			show(new CustomMessage("scripts.events.CtF.Cancelled", player), player);
			return false;
		}

		if(player.getDuel() != null)
		{
			show(new CustomMessage("scripts.events.CtF.CancelledDuel", player), player);
			return false;
		}

		if(first && player.isInEvent(L2EventType.NONE) || player.getTeam() != 0)
		{
			show(new CustomMessage("scripts.events.CtF.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.getOlympiadGameId() > 0 || player.isInZoneOlympiad() || first && Olympiad.isRegistered(player))
		{
			show(new CustomMessage("scripts.events.CtF.CancelledOlympiad", player), player);
			return false;
		}

		if(player.isInParty() && player.getParty().isInDimensionalRift())
		{
			show(new CustomMessage("scripts.events.CtF.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.isTeleporting())
		{
			show(new CustomMessage("scripts.events.CtF.CancelledTeleport", player), player);
			return false;
		}

		if(player.isCursedWeaponEquipped())
		{
			show(new CustomMessage("scripts.events.CtF.Cancelled", player), player);
			return false;
		}

		// последним проверяем HardwareID
		if(first && !hwid_check.canParticipate(player))
		{
			show(new CustomMessage("scripts.events.CtF.Cancelled", player), player);
			return false;
		}

		return true;
	}

	@Override
	protected boolean isInEvent(final L2Player player, final L2EventType type)
	{
		return (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())) && (type == L2EventType.NONE || type == getEventType());
	}

	@Override
	protected void onDie(final L2Character self, final L2Character killer)
	{
		if(_status > 1 && self != null && self.isPlayer() && self.getTeam() > 0 && (teams[RED_TEAM].isInTeam(self.getStoredId()) || teams[BLUE_TEAM].isInTeam(self.getStoredId())))
		{
			dropFlag((L2Player) self);
			executeTask("events.ctf.CtF", "resurrectAtBase", new Object[] { (L2Player) self }, Config.EVENT_CTF_REVIVE_DELAY * 1000);
		}
	}

	public static void prepare()
	{
		closeColiseumDoors();

		cleanPlayers();
		clearArena();

		redFlag.spawnMe();
		blueFlag.spawnMe();

		executeTask("events.ctf.CtF", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.ctf.CtF", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.ctf.CtF", "saveBackCoords", EMPTY_ARG, 3000);
		executeTask("events.ctf.CtF", "teleportPlayersToColiseum", EMPTY_ARG, 4000);
		executeTask("events.ctf.CtF", "paralyzePlayers", EMPTY_ARG, 5000);
		executeTask("events.ctf.CtF", "go", EMPTY_ARG, 60000);

		sayToAll("scripts.events.CtF.AnnounceFinalCountdown");
	}

	@Override
	public void go()
	{
		_status = 2;
		upParalyzePlayers();
		clearArena();
		sayToAll("scripts.events.CtF.AnnounceFight");
		_endTask = executeTask("events.ctf.CtF", "endOfTime", EMPTY_ARG, 300000);
	}

	public static void endOfTime()
	{
		endBattle(3); // ничья
	}

	public static void endBattle(final int win)
	{
		if(_endTask != null)
		{
			_endTask.cancel(false);
			_endTask = null;
		}

		removeFlags();

		if(redFlag != null)
		{
			redFlag.deleteMe();
			redFlag = null;
		}

		if(blueFlag != null)
		{
			blueFlag.deleteMe();
			blueFlag = null;
		}

		_status = 0;
		removeAura();

		openColiseumDoors();

		switch (win)
		{
			case 1:
				sayToAll("scripts.events.CtF.AnnounceFinishedRedWins");
				giveItemsToWinner(false, true, 1);
				break;
			case 2:
				sayToAll("scripts.events.CtF.AnnounceFinishedBlueWins");
				giveItemsToWinner(true, false, 1);
				break;
			case 3:
				sayToAll("scripts.events.CtF.AnnounceFinishedDraw");
				giveItemsToWinner(true, true, 0);
				break;
		}

		sayToAll("scripts.events.CtF.AnnounceEnd");
		executeTask("events.ctf.CtF", "end", EMPTY_ARG, 30000);
		_isRegistrationActive = false;
	}

	public static void end()
	{
		executeTask("events.ctf.CtF", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.ctf.CtF", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.ctf.CtF", "teleportPlayersToSavedCoords", EMPTY_ARG, 3000);
		executeTask("events.ctf.CtF", "autoContinue", EMPTY_ARG, 5000);
	}

	public void autoContinue()
	{
		teams[BLUE_TEAM].clear();
		teams[RED_TEAM].clear();
		hwid_check.clear();

		if(_autoContinue > 0)
		{
			if(_autoContinue >= 6)
			{
				_autoContinue = 0;
				return;
			}
			start(new String[] { "" + (_autoContinue + 1), "" + (_autoContinue + 1) });
		}
		else
		{
			// если ивент был запущен вручную, но не активирон, то удаляем
			if(_addEventType)
			{
				_addEventType = false;
				removeEventId(ScriptEventType.ON_DISCONNECT);
				removeEventId(ScriptEventType.ON_ESCAPE);
				removeEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
				removeEventId(ScriptEventType.ON_DIE);
			}

			// если нет, то пробуем зашедулить по времени из конфигов
			scheduleEventStart(true);
		}
	}

	/**
	 * проверяет возможность запуска ивента и стартует такс в указанное в конфигах время
	 * 
	 * @param check
	 *            - проверять активирован ли ивент
	 */
	public void scheduleEventStart(final boolean check)
	{
		// ивент должен быть активированным для автоматического запуска
		if(check && !_active)
			return;

		try
		{
			final Calendar currentTime = Calendar.getInstance();
			int nextCategory = -1;
			Calendar testStartTime = null;
			Calendar nextStartTime = null;

			// перебираем всё варианты старта... находим оптимальный вариант
			for(final EventInterval interval : Config.EVENT_CFT_INTERVAL)
			{
				// Creating a Calendar object from the specified interval value
				testStartTime = Calendar.getInstance();
				testStartTime.setLenient(true); // чтоб выдало исключение если время задано не верно в конфигах

				// устанавливаем время предположительно след запуска
				testStartTime.set(Calendar.HOUR_OF_DAY, interval.hour);
				testStartTime.set(Calendar.MINUTE, interval.minute);

				// If the date is in the past, make it the next day (Example: Checking for "1:00", when the time is 23:57.)
				if(testStartTime.getTimeInMillis() < currentTime.getTimeInMillis())
					testStartTime.add(Calendar.DAY_OF_MONTH, 1);

				// сравниваем, если подходит то устанавливаем
				if(nextStartTime == null || testStartTime.getTimeInMillis() < nextStartTime.getTimeInMillis())
				{
					nextStartTime = testStartTime;
					nextCategory = interval.category;
				}
			}

			_log.info("CtF: next start at " + Util.datetimeFormatter.format(nextStartTime.getTime()) + ", category: " + nextCategory);
			// запускаем ивент с указанной катерорией и без автопродолжения
			if(_startTask != null)
			{
				_startTask.cancel(false);
				_startTask = null;
			}
			_startTask = L2GameThreadPools.getInstance().scheduleGeneral(new StartTask(nextCategory, 0), nextStartTime.getTimeInMillis() - System.currentTimeMillis());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "CtF.scheduleEventStart(): error figuring out a start time. Check config file.", e);
		}
	}

	public static void giveItemsToWinner(final boolean team1, final boolean team2, final double rate)
	{
		if(team1)
		{
			final GArray<L2Player> players = teams[BLUE_TEAM].getAllPlayers();
			for(final L2Player player : players)
				addItem(player, Config.EVENT_CFT_ITEM_ID, Math.round((Config.EVENT_CFT_RATE ? player.getLevel() : 1) * Config.EVENT_CFT_ITEM_COUNT * rate));
		}
		if(team2)
		{
			final GArray<L2Player> players = teams[RED_TEAM].getAllPlayers();
			for(final L2Player player : players)
				addItem(player, Config.EVENT_CFT_ITEM_ID, Math.round((Config.EVENT_CFT_RATE ? player.getLevel() : 1) * Config.EVENT_CFT_ITEM_COUNT * rate));
		}
	}

	public static void saveBackCoords()
	{
		teams[RED_TEAM].saveBackCoords();
		teams[BLUE_TEAM].saveBackCoords();
		teams[RED_TEAM].addRestrictions(Config.EVENT_CFT_ITEM_RESTRICTION, Config.EVENT_CFT_SKILL_RESTRICTION);
		teams[BLUE_TEAM].addRestrictions(Config.EVENT_CFT_ITEM_RESTRICTION, Config.EVENT_CFT_SKILL_RESTRICTION);
	}

	public static void teleportPlayersToColiseum()
	{
		for(final L2Player player : teams[BLUE_TEAM].getAllPlayers())
		{
			unRide(player);

			if(!Config.EVENT_CFT_ALLOW_SUMMONS)
				unSummonPet(player);

			// Remove Buffs
			if(!Config.EVENT_CFT_ALLOW_BUFFS)
			{
				player.stopAllEffects();
				if(player.getPet() != null)
					player.getPet().stopAllEffects();
			}

			// Remove Clan Skills
			if(!Config.EVENT_CFT_ALLOW_CLAN_SKILL)
				if(player.getClan() != null)
					for(final L2Skill skill : player.getClan().getAllSkills())
						player.removeSkill(skill, false);

			if(!Config.EVENT_CFT_ALLOW_HERO_SKILL)
				// Remove Hero Skills
				if(player.isHero())
					for(final L2Skill sk : HeroSkillTable.getHeroSkills())
						player.removeSkillById(sk.getId());

			// Force the character to be mortal
			if(player.isInvul())
				player.setInvul(false);

			// Force the character to be visible
			if(player.isInvisible())
				player.setInvisible(false);

			if(Config.EVENT_CFT_DISPEL_TRANSFORMATION && player.isTransformed() && !player.getTransform().isDefaultActionListTransform())
				player.stopTransformation();

			final int[] pos = team1loc.getRandomPoint();
			player.teleToLocation(pos[0], pos[1], pos[2], ReflectionTable.EVENT_CTF);
		}
		for(final L2Player player : teams[RED_TEAM].getAllPlayers())
		{
			unRide(player);

			if(!Config.EVENT_CFT_ALLOW_SUMMONS)
				unSummonPet(player);

			// Remove Buffs
			if(!Config.EVENT_CFT_ALLOW_BUFFS)
			{
				player.stopAllEffects();
				if(player.getPet() != null)
					player.getPet().stopAllEffects();
			}

			// Remove Clan Skills
			if(!Config.EVENT_CFT_ALLOW_CLAN_SKILL)
				if(player.getClan() != null)
					for(final L2Skill skill : player.getClan().getAllSkills())
						player.removeSkill(skill, false);

			if(!Config.EVENT_CFT_ALLOW_HERO_SKILL)
				// Remove Hero Skills
				if(player.isHero())
					for(final L2Skill sk : HeroSkillTable.getHeroSkills())
						player.removeSkillById(sk.getId());

			// Force the character to be mortal
			if(player.isInvul())
				player.setInvul(false);

			// Force the character to be visible
			if(player.isInvisible())
				player.setInvisible(false);

			if(Config.EVENT_CFT_DISPEL_TRANSFORMATION && player.isTransformed() && !player.getTransform().isDefaultActionListTransform())
				player.stopTransformation();

			final int[] pos = team2loc.getRandomPoint();
			player.teleToLocation(pos[0], pos[1], pos[2], ReflectionTable.EVENT_CTF);
		}
	}

	public static void teleportPlayersToSavedCoords()
	{
		teams[RED_TEAM].removeRestrictions(Config.EVENT_CFT_ITEM_RESTRICTION, Config.EVENT_CFT_SKILL_RESTRICTION);
		teams[BLUE_TEAM].removeRestrictions(Config.EVENT_CFT_ITEM_RESTRICTION, Config.EVENT_CFT_SKILL_RESTRICTION);
		teams[RED_TEAM].teleportToSavedCoords(true);
		teams[BLUE_TEAM].teleportToSavedCoords(true);
	}

	public static void paralyzePlayers()
	{
		teams[BLUE_TEAM].paralyzePlayers();
		teams[RED_TEAM].paralyzePlayers();
	}

	public static void upParalyzePlayers()
	{
		teams[RED_TEAM].upParalyzePlayers(true);
		teams[BLUE_TEAM].upParalyzePlayers(true);
	}

	public static void ressurectPlayers()
	{
		teams[RED_TEAM].ressurectPlayers();
		teams[BLUE_TEAM].ressurectPlayers();
	}

	public static void healPlayers()
	{
		teams[RED_TEAM].healPlayers();
		teams[BLUE_TEAM].healPlayers();
	}

	public static void cleanPlayers()
	{
		for(final L2Player player : teams[BLUE_TEAM].getAllPlayers())
			if(!checkPlayer(player, false))
				removePlayer(player);
			else
				player.setTeam(1, true);
		for(final L2Player player : teams[RED_TEAM].getAllPlayers())
			if(!checkPlayer(player, false))
				removePlayer(player);
			else
				player.setTeam(2, true);
	}

	public static void removeAura()
	{
		teams[RED_TEAM].removeAura();
		teams[BLUE_TEAM].removeAura();
	}

	/**
	 * чистим арену от мусора) чистим только мусор который находиться в отражении для данного эвента
	 */
	public static void clearArena()
	{
		for(final L2Object obj : _zone.getObjectsInside())
			if(obj != null && obj.getReflectionId() == ReflectionTable.EVENT_CTF)
			{
				final L2Player player = obj.getPlayer();
				if(player != null && !teams[RED_TEAM].isInTeam(player.getStoredId()) && !teams[BLUE_TEAM].isInTeam(player.getStoredId()))
					player.teleToLocation(147451, 46728, -3410);
			}
	}

	public void resurrectAtBase(final L2Player player)
	{
		if(player.getTeam() <= 0)
			return;
		
		if(player.isDead())
		{
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentHp(player.getMaxHp(), true);
			player.setCurrentMp(player.getMaxMp());
			player.broadcastPacket(new Revive(player));
		}
		// бафаем
		L2EventTeam.addEventBuffs(player, this);
		
		int[] pos;
		if(player.getTeam() == 1)
			pos = team1loc.getRandomPoint();
		else
			pos = team2loc.getRandomPoint();
		player.teleToLocation(pos[0], pos[1], pos[2], ReflectionTable.EVENT_CTF);
	}

	@Override
	protected final void onEscape(final L2Player player)
	{
		if(_status > 1 && player != null && player.getTeam() > 0 && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())))
			removePlayer(player);
	}

	@Override
	protected final void onDisconnect(final L2Player player)
	{
		if(player == null || player.getTeam() < 1)
			return;

		// Вышел или вылетел во время регистрации
		if(_status == 0 && _isRegistrationActive && player.getTeam() > 0 && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())))
		{
			removePlayer(player);
			return;
		}

		// Вышел или вылетел во время телепортации
		if(_status == 1 && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())))
		{
			removePlayer(player);

			try
			{
				final String var = player.getVar("event_backcoords");
				if(var == null || var.equals(""))
					return;
				final String[] coords = var.split(" ");
				if(coords.length != 4)
					return;
				player.teleToLocation(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]), Long.parseLong(coords[3]));
				player.unsetVar("event_backcoords");
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

			return;
		}

		// Вышел или вылетел во время эвента
		onEscape(player);
	}

	private static class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 0 && player != null && !teams[RED_TEAM].isInTeam(player.getStoredId()) && !teams[BLUE_TEAM].isInTeam(player.getStoredId()) && player.getReflectionId() == ReflectionTable.EVENT_CTF)
				L2GameThreadPools.getInstance().scheduleGeneral(new TeleportTask(character, new Location(147451, 46728, -3410)), 3000);
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 1 && player != null && player.getTeam() > 0 && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())) && player.getReflectionId() == ReflectionTable.EVENT_CTF)
			{
				final double angle = Util.convertHeadingToDegree(character.getHeading()); // угол в градусах
				final double radian = Math.toRadians(angle - 90); // угол в радианах
				final int x = (int) (character.getX() + 50 * Math.sin(radian));
				final int y = (int) (character.getY() - 50 * Math.cos(radian));
				final int z = character.getZ();
				L2GameThreadPools.getInstance().scheduleGeneral(new TeleportTask(character, new Location(x, y, z)), 3000);
			}
		}
	}

	private static class RedBaseZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 0 && player != null && teams[BLUE_TEAM].isInTeam(player.getStoredId()) && player.isTerritoryFlagEquipped() && player.getReflectionId() == ReflectionTable.EVENT_CTF)
				endBattle(2);
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{}
	}

	private static class BlueBaseZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 0 && player != null && teams[RED_TEAM].isInTeam(player.getStoredId()) && player.isTerritoryFlagEquipped() && player.getReflectionId() == ReflectionTable.EVENT_CTF)
				endBattle(1);
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{}
	}

	private static void removePlayer(final L2Player player)
	{
		if(player != null)
		{
			teams[RED_TEAM].removePlayer(player);
			teams[BLUE_TEAM].removePlayer(player);
			dropFlag(player);
		}
	}

	private static void addFlag(final L2Player player, final int flagId)
	{
		final L2ItemInstance item = ItemTable.getInstance().createItem(flagId, 0, 0, "CtF Event");
		item.setCustomType1(77);
		item.setCustomFlags(L2ItemInstance.FLAG_EQUIP_ON_PICKUP | L2ItemInstance.FLAG_NO_DESTROY | L2ItemInstance.FLAG_NO_TRADE | L2ItemInstance.FLAG_NO_UNEQUIP, false);
		player.getInventory().addItem(item);
		player.getInventory().equipItem(item, false);
		player.sendChanges();
		player.sendPacket(Msg.YOU_VE_ACQUIRED_THE_WARD_MOVE_QUICKLY_TO_YOUR_FORCES__OUTPOST);
	}

	private static void removeFlags()
	{
		for(final L2Player player : teams[BLUE_TEAM].getAllPlayers())
			removeFlag(player);
		for(final L2Player player : teams[RED_TEAM].getAllPlayers())
			removeFlag(player);
	}

	private static void removeFlag(final L2Player player)
	{
		if(player != null && player.isTerritoryFlagEquipped())
		{
			final L2ItemInstance flag = player.getActiveWeaponInstance();
			if(flag != null && flag.getCustomType1() == 77) // 77 это эвентовый флаг
			{
				flag.setCustomFlags(0, false);
				player.getInventory().destroyItem(flag, 1, false);
				player.broadcastUserInfo(true);
			}
		}
	}

	private static void dropFlag(final L2Player player)
	{
		if(player != null && player.isTerritoryFlagEquipped())
		{
			final L2ItemInstance flag = player.getActiveWeaponInstance();
			if(flag != null && flag.getCustomType1() == 77) // 77 это эвентовый флаг
			{
				flag.setCustomFlags(0, false);
				player.getInventory().destroyItem(flag, 1, false);
				player.broadcastUserInfo(true);
				if(flag.getItemId() == 13560)
				{
					redFlag.setXYZInvisible(player.getLoc());
					redFlag.spawnMe();
				}
				else if(flag.getItemId() == 13561)
				{
					blueFlag.setXYZInvisible(player.getLoc());
					blueFlag.spawnMe();
				}
			}
		}
	}

	private static void openColiseumDoors()
	{
		_reflection.openDoor(24190003);
		_reflection.openDoor(24190004);
	}

	private static void closeColiseumDoors()
	{
		_reflection.closeDoor(24190003);
		_reflection.closeDoor(24190004);
	}

	@Override
	public boolean isActive()
	{
		return _active;
	}

	/**
	 * @see l2n.game.event.L2Event#getEventType()
	 */
	@Override
	public L2EventType getEventType()
	{
		return L2EventType.CTF;
	}
}
