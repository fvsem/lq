package events.arena;

import l2n.commons.list.GArray;
import l2n.extensions.scripts.ScriptEventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;
import l2n.util.Location;

public class GiranArena extends ArenaTemplate
{
	public GiranArena()
	{

	}

	public static GiranArena getInstance()
	{
		return SingletonHolder._instance;
	}

	@SuppressWarnings("synthetic-access")
	private static class SingletonHolder
	{
		private static final GiranArena _instance = new GiranArena();
	}

	public void loadArena()
	{
		_managerId = 22220001;
		_className = "GiranArena";
		_status = 0;

		_zoneListener = new ZoneListener();
		_zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 3, true);
		_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

		_team1points = new GArray<Location>();
		_team2points = new GArray<Location>();

		_team1points.add(new Location(72609, 142346, -3798));
		_team1points.add(new Location(72809, 142346, -3798));
		_team1points.add(new Location(73015, 142346, -3798));
		_team1points.add(new Location(73215, 142346, -3798));
		_team1points.add(new Location(73407, 142346, -3798));
		_team2points.add(new Location(73407, 143186, -3798));
		_team2points.add(new Location(73215, 143186, -3798));
		_team2points.add(new Location(73015, 143186, -3798));
		_team2points.add(new Location(72809, 143186, -3798));
		_team2points.add(new Location(72609, 143186, -3798));

		addEventId(ScriptEventType.ON_DIE);
		addEventId(ScriptEventType.ON_DISCONNECT);
		addEventId(ScriptEventType.ON_ESCAPE);
		_log.info("Loaded Event: Giran Arena");
	}

	public void unLoadArena()
	{
		if(_status > 0)
			stop();
		removeEventId(ScriptEventType.ON_DIE);
		removeEventId(ScriptEventType.ON_DISCONNECT);
		removeEventId(ScriptEventType.ON_ESCAPE);
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
	}

	@Override
	public void onLoad()
	{
		getInstance().loadArena();
	}

	@Override
	public void onReload()
	{
		getInstance().unLoadArena();
	}

	@Override
	public void onShutdown()
	{
		onReload();
	}

	public String DialogAppend_22220001(final Integer val)
	{
		if(val == 0)
		{
			final L2Player player = (L2Player) getSelf();
			if(player.isGM())
				return Files.read("data/scripts/events/arena/22220001.html", player) + Files.read("data/scripts/events/arena/22220001-4.html", player);
			return Files.read("data/scripts/events/arena/22220001.html", player);
		}
		return "";
	}

	public String DialogAppend_22220002(final Integer val)
	{
		return DialogAppend_22220001(val);
	}

	public void create1()
	{
		getInstance().template_create1((L2Player) getSelf());
	}

	public void create2()
	{
		getInstance().template_create2((L2Player) getSelf());
	}

	public void register()
	{
		getInstance().template_register((L2Player) getSelf());
	}

	public void check1(final String[] var)
	{
		getInstance().template_check1((L2Player) getSelf(), getNpc(), var);
	}

	public void check2(final String[] var)
	{
		getInstance().template_check2((L2Player) getSelf(), getNpc(), var);
	}

	public void register_check(final String[] var)
	{
		getInstance().template_register_check((L2Player) getSelf(), var);
	}

	public void stop()
	{
		getInstance().template_stop();
	}

	public void announce()
	{
		getInstance().template_announce();
	}

	public void prepare()
	{
		getInstance().template_prepare();
	}

	public void start()
	{
		getInstance().template_start();
	}

	public static void timeOut()
	{
		getInstance().template_timeOut();
	}
}
