package events.thefallharvest;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.handler.ItemHandler;
import l2n.game.handler.interfaces.IItemHandler;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2Object;
import l2n.game.model.L2Spawn;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.scripts.npc.SquashInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;

public class Seed implements IItemHandler, ScriptFile
{
	public class DeSpawnScheduleTimerTask implements Runnable
	{
		L2Spawn spawnedPlant = null;

		public DeSpawnScheduleTimerTask(L2Spawn spawn)
		{
			spawnedPlant = spawn;
		}

		@Override
		public void run()
		{
			try
			{
				spawnedPlant.getLastSpawn().decayMe();
				spawnedPlant.getLastSpawn().deleteMe();
			}
			catch(Throwable t)
			{}
		}
	}

	private static int[] _itemIds = { 6389, // small seed
			6390 // large seed
	};

	private static int[] _npcIds = { 12774, // Young Pumpkin
			12777 // Large Young Pumpkin
	};

	@Override
	public void useItem(L2Playable playable, L2ItemInstance item)
	{
		L2Player activeChar = (L2Player) playable;

		if(activeChar.isInZone(ZoneType.Castle))
		{
			activeChar.sendMessage("Нельзя взращивать тыкву в замке");
			return;
		}
		if(activeChar.isInZone(ZoneType.Fortress))
		{
			activeChar.sendMessage("Нельзя взращивать тыкву в форте");
			return;
		}
		if(activeChar.isInZone(ZoneType.OlympiadStadia))
		{
			activeChar.sendMessage("Нельзя взращивать тыкву на стадионе");
			return;
		}

		L2NpcTemplate template = null;

		int itemId = item.getItemId();
		for(int i = 0; i < _itemIds.length; i++)
			if(_itemIds[i] == itemId)
			{
				template = NpcTable.getTemplate(_npcIds[i]);
				break;
			}

		if(template == null)
			return;

		L2Object target = activeChar.getTarget();
		if(target == null)
			target = activeChar;

		try
		{
			L2Spawn spawn = new L2Spawn(template);
			spawn.setId(IdFactory.getInstance().getNextId());
			spawn.setLoc(activeChar.getLoc());
			L2NpcInstance npc = spawn.doSpawn(true);
			npc.setAI(new SquashAI(npc));
			((SquashInstance) npc).setSpawner(activeChar);

			L2GameThreadPools.getInstance().scheduleAi(new DeSpawnScheduleTimerTask(spawn), 180000, false);
			activeChar.getInventory().destroyItem(item.getObjectId(), 1, false);
		}
		catch(Exception e)
		{
			activeChar.sendPacket(new SystemMessage(SystemMessage.YOUR_TARGET_CANNOT_BE_FOUND));
		}
	}

	@Override
	public int[] getItemIds()
	{
		return _itemIds;
	}

	@Override
	public void onLoad()
	{
		ItemHandler.getInstance().registerItemHandler(this);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
