package events.autoannouncer;

import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.model.actor.L2Player;
import l2n.util.Files;

public class Announcer extends Functions implements ScriptFile
{
	private static boolean _active = false;
	private static String message1 = Config.EVENT_message1;
	private static String time1 = Config.EVENT_time1;
	private static String message2 = Config.EVENT_message2;
	private static String time2 = Config.EVENT_time2;
	private static String message3 = Config.EVENT_message3;
	private static String time3 = Config.EVENT_time3;

	private static String[][] text = { { message1, time1 }, { message2, time2 }, { message3, time3 } };

	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("event_Announcer", true))
		{
			announce_run();
			System.out.println("Event: AutoAnnouncer started.");
		}
		else
			player.sendMessage("Event 'AutoAnnouncer' already started.");

		_active = true;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("event_Announcer", false))
		{
			System.out.println("Event: AutoAnnouncer stopped.");
		}
		else
			player.sendMessage("Event 'AutoAnnouncer' not started.");

		_active = false;
		show(Files.read("data/html/admin/events.htm", player), player);
	}

	public static void announce_run()
	{
		if(_active)
			for(String[] element : text)
				executeTask("events.autoannouncer.Announcer", "announce", new Object[] { element[0], Integer.valueOf(element[1]) }, Integer.valueOf(element[1]));
	}

	public static void announce(String text, Integer inter)
	{
		if(_active)
		{
			Announcements.announceToAll(text);
			executeTask("events.autoannouncer.Announcer", "announce", new Object[] { text, inter }, inter);
		}
	}

	@Override
	public void onLoad()
	{
		if(isActive("event_Announcer"))
		{
			_active = true;
			announce_run();
			System.out.println("Loaded Event: AutoAnnouncer [state: activated]");
		}
		else
			System.out.println("Loaded Event: AutoAnnouncer [state: deactivated]");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
