package events.theflowofthehorror;

import l2n.extensions.scripts.Functions;
import l2n.game.ai.CtrlIntention;
import l2n.game.ai.Fighter;
import l2n.game.model.actor.L2Character;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.MagicSkillUse;
import l2n.game.network.serverpackets.StopMove;
import l2n.game.skills.Stats;
import l2n.game.skills.funcs.FuncMul;
import l2n.game.tables.SkillTable;
import l2n.util.Location;
import l2n.util.Rnd;

public class GilmoreAI extends Fighter
{
	private Location[] points_stage1 = new Location[7];
	private Location[] points_stage2 = new Location[1];

	private static final String[] text_stage1 = { "Text1", "Text2", "Text3", "Text4", "Text5", "Text6", "Text7" };

	private static final String[] text_stage2 = { "Готовы?", "Начнем, нельзя терять ни минуты!" };

	private long wait_timeout = 0;
	private boolean wait = false;
	private int index;

	private int step_stage2 = 1;

	public GilmoreAI(L2Character actor)
	{
		super(actor);

		AI_TASK_DELAY = 200;

		points_stage1[0] = new Location(73195, 118483, -3722);
		points_stage1[1] = new Location(73535, 117945, -3754);
		points_stage1[2] = new Location(73446, 117334, -3752);
		points_stage1[3] = new Location(72847, 117311, -3711);
		points_stage1[4] = new Location(72296, 117720, -3694);
		points_stage1[5] = new Location(72463, 118401, -3694);
		points_stage1[6] = new Location(72912, 117895, -3723);

		points_stage2[0] = new Location(73615, 117629, -3765);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected boolean thinkActive()
	{
		L2NpcInstance actor = getActor();
		if(actor == null || actor.isDead())
			return true;

		if(_def_think)
		{
			doTask();
			return true;
		}

		if(System.currentTimeMillis() > wait_timeout)
		{
			if(!wait)
				switch (TheFlowOfTheHorror.getStage())
				{
					case 1:
						if(Rnd.chance(30))
						{
							index = Rnd.get(text_stage1.length);
							Functions.npcShout(actor, text_stage1[index]);
							wait_timeout = System.currentTimeMillis() + 10000;
							wait = true;
							return true;
						}
						break;
					case 2:
						switch (step_stage2)
						{
							case 1:
								Functions.npcShout(actor, text_stage2[0]);
								wait_timeout = System.currentTimeMillis() + 10000;
								wait = true;
								return true;
							case 2:
								break;
						}
						break;
				}

			wait_timeout = 0;
			wait = false;

			actor.setRunning();

			switch (TheFlowOfTheHorror.getStage())
			{
				case 1:
					index = Rnd.get(points_stage1.length);

					// Добавить новое задание
					addTaskMove(points_stage1[index]);
					doTask();
					return true;
				case 2:
					switch (step_stage2)
					{
						case 1:
							Functions.npcShout(actor, text_stage2[1]);

							// Добавить новое задание
							addTaskMove(points_stage2[0]);
							step_stage2 = 2;
							doTask();
							return true;
						case 2:
							actor.setHeading(0);
							actor.broadcastPacket(new StopMove(actor));
							actor.broadcastPacketToOthers(new MagicSkillUse(actor, actor, 454, 1, 3000, 0));
							step_stage2 = 3;
							return true;
						case 3:
							actor.addStatFunc(new FuncMul(Stats.MAGIC_ATTACK_SPEED, 0x40, this, 5));
							actor.addStatFunc(new FuncMul(Stats.MAGIC_DAMAGE, 0x40, this, 10));
							actor.addStatFunc(new FuncMul(Stats.PHYSICAL_DAMAGE, 0x40, this, 10));
							actor.addStatFunc(new FuncMul(Stats.RUN_SPEED, 0x40, this, 3));
							actor.addSkill(SkillTable.getInstance().getInfo(1467, 1));
							actor.broadcastCharInfo();
							step_stage2 = 4;
							return true;
						case 4:
							setIntention(CtrlIntention.AI_INTENTION_ATTACK, null);
							return true;
						case 10:
							actor.removeStatsOwner(this);
							step_stage2 = 11;
							return true;
					}
			}
		}

		return false;
	}

	@Override
	protected boolean createNewTask()
	{
		clearTasks();

		L2NpcInstance actor = getActor();
		if(actor == null)
			return true;

		for(L2NpcInstance npc : actor.getAroundNpc(1000, 400))
			if(Rnd.chance(10) && npc != null && npc.getNpcId() == 20235)
			{
				L2MonsterInstance monster = (L2MonsterInstance) npc;
				if(Rnd.chance(20))
				{
					addTaskCast(monster, actor.getKnownSkill(1467));
					return true;
				}

				addTaskAttack(monster);
				return true;
			}
		return true;
	}
}
