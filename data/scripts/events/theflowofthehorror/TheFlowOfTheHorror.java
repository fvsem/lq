package events.theflowofthehorror;

import java.util.ArrayList;

import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.idfactory.IdFactory;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2MonsterInstance;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.tables.NpcTable;
import l2n.game.templates.L2NpcTemplate;
import l2n.util.Files;
import l2n.util.Location;

public class TheFlowOfTheHorror extends Functions implements ScriptFile
{
	private final static int Gilmore = 30754;
	private final static int Shackle = 20235;

	private static L2NpcInstance oldGilmore;

	private static int _stage = 1;

	private final static ArrayList<L2MonsterInstance> _spawns = new ArrayList<L2MonsterInstance>();

	private final static ArrayList<Location> points11 = new ArrayList<Location>();
	private final static ArrayList<Location> points12 = new ArrayList<Location>();
	private final static ArrayList<Location> points13 = new ArrayList<Location>();
	private final static ArrayList<Location> points21 = new ArrayList<Location>();
	private final static ArrayList<Location> points22 = new ArrayList<Location>();
	private final static ArrayList<Location> points23 = new ArrayList<Location>();
	private final static ArrayList<Location> points31 = new ArrayList<Location>();
	private final static ArrayList<Location> points32 = new ArrayList<Location>();
	private final static ArrayList<Location> points33 = new ArrayList<Location>();

	@Override
	public void onLoad()
	{

		points11.add(new Location(84211, 117965, -3020));
		points11.add(new Location(83389, 117590, -3036));
		points11.add(new Location(82226, 117051, -3150));
		points11.add(new Location(80902, 116155, -3533));
		points11.add(new Location(79832, 115784, -3733));
		points11.add(new Location(78442, 116510, -3823));
		points11.add(new Location(76299, 117355, -3786));
		points11.add(new Location(74244, 117674, -3785));

		points12.add(new Location(84231, 117597, -3020));
		points12.add(new Location(82536, 116986, -3093));
		points12.add(new Location(79428, 116341, -3749));
		points12.add(new Location(76970, 117362, -3771));
		points12.add(new Location(74322, 117845, -3767));


		points13.add(new Location(83962, 118387, -3022));
		points13.add(new Location(81960, 116925, -3216));
		points13.add(new Location(80223, 116059, -3665));
		points13.add(new Location(78214, 116783, -3854));
		points13.add(new Location(76208, 117462, -3791));
		points13.add(new Location(74278, 117454, -3804));


		points21.add(new Location(79192, 111481, -3011));
		points21.add(new Location(79014, 112396, -3090));
		points21.add(new Location(79309, 113692, -3437));
		points21.add(new Location(79350, 115337, -3758));
		points21.add(new Location(78390, 116309, -3772));
		points21.add(new Location(76794, 117092, -3821));
		points21.add(new Location(74451, 117623, -3797));


		points22.add(new Location(79297, 111456, -3017));
		points22.add(new Location(79020, 112217, -3087));
		points22.add(new Location(79167, 113236, -3289));
		points22.add(new Location(79513, 115408, -3752));
		points22.add(new Location(78555, 116816, -3812));
		points22.add(new Location(76932, 117277, -3781));
		points22.add(new Location(75422, 117788, -3755));
		points22.add(new Location(74223, 117898, -3753));



		points23.add(new Location(79635, 110741, -3003));
		points23.add(new Location(78994, 111858, -3061));
		points23.add(new Location(79088, 112949, -3226));
		points23.add(new Location(79424, 114499, -3674));
		points23.add(new Location(78913, 116266, -3779));
		points23.add(new Location(76930, 117137, -3819));
		points23.add(new Location(75533, 117569, -3781));
		points23.add(new Location(74255, 117398, -3804));


		points31.add(new Location(83128, 111358, -3663));
		points31.add(new Location(81538, 111896, -3631));
		points31.add(new Location(80312, 113837, -3752));
		points31.add(new Location(79012, 115998, -3772));
		points31.add(new Location(77377, 117052, -3812));
		points31.add(new Location(75394, 117608, -3772));
		points31.add(new Location(73998, 117647, -3784));


		points32.add(new Location(83245, 110790, -3772));
		points32.add(new Location(81832, 111379, -3641));
		points32.add(new Location(81405, 112403, -3648));
		points32.add(new Location(79827, 114496, -3752));
		points32.add(new Location(78174, 116968, -3821));
		points32.add(new Location(75944, 117653, -3777));
		points32.add(new Location(74379, 117939, -3755));


		points33.add(new Location(82584, 111930, -3568));
		points33.add(new Location(81389, 111989, -3647));
		points33.add(new Location(80129, 114044, -3748));
		points33.add(new Location(79190, 115579, -3743));
		points33.add(new Location(77989, 116811, -3849));
		points33.add(new Location(76009, 117405, -3800));
		points33.add(new Location(74113, 117441, -3797));

		if(isActive("TheFlowOfTheHorror"))
		{
			activateAI();
			System.out.println("Loaded Event: The Flow Of The Horror [state: activated]");
		}
		else
			System.out.println("Loaded Event: The Flow Of The Horror [state: deactivated]");
	}

	public static void spawnNewWave()
	{
		spawn(Shackle, points11);
		spawn(Shackle, points12);
		spawn(Shackle, points13);
		spawn(Shackle, points21);
		spawn(Shackle, points22);
		spawn(Shackle, points23);
		spawn(Shackle, points31);
		spawn(Shackle, points32);
		spawn(Shackle, points33);

		_stage = 2;
	}

	private static void spawn(int id, ArrayList<Location> points)
	{
		L2NpcTemplate template = NpcTable.getTemplate(id);
		L2MonsterInstance monster = new L2MonsterInstance(IdFactory.getInstance().getNextId(), template);
		monster.setCurrentHpMp(monster.getMaxHp(), monster.getMaxMp(), true);
		monster.setXYZ(points.get(0).x, points.get(0).y, points.get(0).z);
		MonstersAI ai = new MonstersAI(monster);
		monster.setAI(ai);
		monster.spawnMe();
		ai.setPoints(points);
		ai.startAITask();
		_spawns.add(monster);
	}

	private void activateAI()
	{
		L2NpcInstance target = L2ObjectsStorage.getByNpcId(Gilmore);
		if(target != null)
		{
			oldGilmore = target;
			target.decayMe();

			L2NpcTemplate template = NpcTable.getTemplate(Gilmore);
			L2MonsterInstance monster = new L2MonsterInstance(IdFactory.getInstance().getNextId(), template);
			monster.setCurrentHpMp(monster.getMaxHp(), monster.getMaxMp(), true);
			monster.setXYZ(73329, 117705, -3741);
			GilmoreAI ai = new GilmoreAI(monster);
			monster.setAI(ai);
			monster.spawnMe();
			ai.startAITask();
			_spawns.add(monster);
		}
	}

	private void deactivateAI()
	{
		for(L2MonsterInstance monster : _spawns)
			if(monster != null)
			{
				monster.getAI().stopAITask();
				monster.deleteMe();
			}

		if(oldGilmore != null)
			oldGilmore.spawnMe();
	}

	/**
	 * ��������� �����
	 */
	public void startEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("TheFlowOfTheHorror", true))
		{
			activateAI();
			System.out.println("Event 'The Flow Of The Horror' started.");
			Announcements.announceByCustomMessage("scripts.events.theflowofthehorror.TheFlowOfTheHorror.AnnounceEventStarted");
		}
		else
			player.sendMessage("Event 'The Flow Of The Horror' already started.");

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	/**
	 * ������������� �����
	 */
	public void stopEvent()
	{
		L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("TheFlowOfTheHorror", false))
		{
			deactivateAI();
			System.out.println("Event 'The Flow Of The Horror' stopped.");
			Announcements.announceByCustomMessage("scripts.events.theflowofthehorror.TheFlowOfTheHorror.AnnounceEventStoped");
		}
		else
			player.sendMessage("Event 'The Flow Of The Horror' not started.");

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	@Override
	public void onReload()
	{
		deactivateAI();
	}

	@Override
	public void onShutdown()
	{
		deactivateAI();
	}

	public static int getStage()
	{
		return _stage;
	}

	public static void setStage(int stage)
	{
		_stage = stage;
	}
}
