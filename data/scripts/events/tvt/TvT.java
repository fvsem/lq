package events.tvt;

import java.util.Calendar;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import l2n.Config;
import l2n.Config.EventInterval;
import l2n.commons.list.GArray;
import l2n.extensions.listeners.L2ZoneEnterLeaveListener;
import l2n.extensions.multilang.CustomMessage;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.Announcements;
import l2n.game.L2GameThreadPools;
import l2n.game.event.HWIDChecker;
import l2n.game.event.L2Event;
import l2n.game.event.L2EventTeam;
import l2n.game.event.L2EventType;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Territory;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.Reflection;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.olympiad.Olympiad;
import l2n.game.network.serverpackets.ExPVPMatchRecord;
import l2n.game.network.serverpackets.ExShowScreenMessage;
import l2n.game.network.serverpackets.ExShowScreenMessage.ScreenMessageAlign;
import l2n.game.network.serverpackets.Revive;
import l2n.game.tables.DoorTable;
import l2n.game.tables.HeroSkillTable;
import l2n.game.tables.ReflectionTable;
import l2n.util.Files;
import l2n.util.Location;
import l2n.util.Rnd;
import l2n.util.Util;

public class TvT extends L2Event implements ScriptFile
{
	private final static Logger _log = Logger.getLogger(TvT.class.getName());

	private static ScheduledFuture<StartTask> _startTask;
	private static ScheduledFuture<?> _endTask;

	private static boolean _active = false;
	private static boolean _addEventType = false;

	private static boolean _isRegistrationActive = false;
	private static int _status = 0;
	private static int _category;
	private static int _minLevel;
	private static int _maxLevel;

	/** используется для автозапуска, увеличивается на 1 для увеличения группы */
	private static int _autoContinue = 0;

	private static int _time_to_start = Config.EVENT_TVT_TIME_TO_START;

	private static L2EventTeam[] teams = new L2EventTeam[2];
	private static final L2Skill[][] _eventBuffs = new L2Skill[2][];

	private final static HWIDChecker hwid_check = new HWIDChecker(Config.EVENT_TVT_CHECK_HWID);

	private final static L2Zone _zone = ZoneManager.getInstance().getZoneByIndex(ZoneType.battle_zone, 4, true);
	private final static ZoneListener _zoneListener = new ZoneListener();

	private final static L2Territory team1loc = new L2Territory(11000001);
	private final static L2Territory team2loc = new L2Territory(11000002);

	private final static Reflection _reflection = new Reflection(ReflectionTable.EVENT_TVT);
	private final static int[] _doors = new int[] { 24190001, 24190002, 24190003, 24190004 };

	@Override
	public void onLoad()
	{
		_zone.getListenerEngine().addMethodInvokedListener(_zoneListener);

		team1loc.add(149878, 47505, -3408, -3308);
		team1loc.add(150262, 47513, -3408, -3308);
		team1loc.add(150502, 47233, -3408, -3308);
		team1loc.add(150507, 46300, -3408, -3308);
		team1loc.add(150256, 46002, -3408, -3308);
		team1loc.add(149903, 46005, -3408, -3308);

		team2loc.add(149027, 46005, -3408, -3308);
		team2loc.add(148686, 46003, -3408, -3308);
		team2loc.add(148448, 46302, -3408, -3308);
		team2loc.add(148449, 47231, -3408, -3308);
		team2loc.add(148712, 47516, -3408, -3308);
		team2loc.add(149014, 47527, -3408, -3308);

		// Если ивент активен, но пробуем зашедулить
		if(isActive("TvT"))
		{
			_active = true;
			scheduleEventStart(true);
		}

		_log.info("Loaded Event: " + getEventType() + "[" + _active + "]");

		// дверь запили!
		for(final int doorId : _doors)
			_reflection.addDoor(DoorTable.getInstance().getDoor(doorId).clone());

		_eventBuffs[0] = Util.parseSkills(Config.EVENT_TVT_BUFFS_FIGHTER);
		_eventBuffs[1] = Util.parseSkills(Config.EVENT_TVT_BUFFS_MAGE);
	}

	@Override
	public void onReload()
	{
		_zone.getListenerEngine().removeMethodInvokedListener(_zoneListener);
		if(_startTask != null)
			_startTask.cancel(true);
	}

	@Override
	public void onShutdown()
	{
		onReload();
	}

	@Override
	public void activateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("TvT", true))
		{
			// при активации ивента, если он не был активирован, то пробуем стартовать. Так как как таск стартует только при загрузке
			if(_startTask == null)
				scheduleEventStart(false);

			_log.info("Event '" + getEventType() + "' activated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStarted", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' already active.");

		_active = true;

		show(Files.read("data/html/admin/events.htm", player), player);
	}

	@Override
	public void deactivateEvent()
	{
		final L2Player player = (L2Player) getSelf();
		if(!player.getPlayerAccess().IsEventGm)
			return;

		if(setActive("TvT", false))
		{
			// отменяем таск
			if(_startTask != null)
			{
				_startTask.cancel(true);
				_startTask = null;
			}

			_log.info("Event '" + getEventType() + "' deactivated.");
			Announcements.announceByCustomMessage("scripts.events.AnnounceEventStoped", new String[] { getEventType().getName(false) });
		}
		else
			player.sendMessage("Event '" + getEventType() + "' not active.");

		_active = false;

		show(Files.read("data/html/admin/events.htm", player), player);
	}


	@Override
	public L2Skill[] getFighterBuffs()
	{
		return _eventBuffs[0];
	}

	@Override
	public L2Skill[] getMageBuffs()
	{
		return _eventBuffs[1];
	}

	/**
	 * 0 - категория уровней<br>
	 * 1 - если больше 0 то автоматически продолжается
	 */
	@Override
	public void start(final String... var)
	{
		if(isRunned())
		{
			_log.info("TvT: start task already running!");
			return;
		}

		// добавляем перед стартом ивента
		if(!_addEventType)
		{
			_addEventType = true;
			addEventId(ScriptEventType.ON_DISCONNECT);
			addEventId(ScriptEventType.ON_ESCAPE);
			addEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
			addEventId(ScriptEventType.ON_DIE);
		}

		final L2Player player = (L2Player) getSelf();
		if(var.length != 2)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		Integer category;
		Integer autoContinue;
		try
		{
			category = Integer.valueOf(var[0]);
			autoContinue = Integer.valueOf(var[1]);
		}
		catch(final Exception e)
		{
			show(new CustomMessage("common.Error", player), player);
			return;
		}

		_category = category;
		_autoContinue = autoContinue;

		if(_category == -1)
		{
			_minLevel = 1;
			_maxLevel = 85;
		}
		else
		{
			_minLevel = getMinLevelForCategory(_category);
			_maxLevel = getMaxLevelForCategory(_category);
		}

		if(_endTask != null)
		{
			show(new CustomMessage("common.TryLater", player), player);
			return;
		}

		_status = 0;
		_isRegistrationActive = true;

		teams = new L2EventTeam[2];
		teams[BLUE_TEAM] = new L2EventTeam(0, "TVT_BLUE");
		teams[RED_TEAM] = new L2EventTeam(1, "TVT_RED");

		_time_to_start = Config.EVENT_TVT_TIME_TO_START;
		final String[] param = { "" + _time_to_start, "" + _minLevel, "" + _maxLevel };
		sayToAll("scripts.events.TvT.AnnouncePreStart", param);

		executeTask("events.tvt.TvT", "question", EMPTY_ARG, 10000);
		executeTask("events.tvt.TvT", "announce", EMPTY_ARG, 60000);
		_log.info("TvT: start event [" + _category + "-" + _autoContinue + "]");
	}

	public static void question()
	{
		for(final L2Player player : L2ObjectsStorage.getAllPlayersForIterate())
			if(player != null && player.getLevel() >= _minLevel && player.getLevel() <= _maxLevel && player.isAllowEventQuestion())
				player.scriptRequest(new CustomMessage("scripts.events.TvT.AskPlayer", player).toString(), "events.tvt.TvT:addPlayer", EMPTY_ARG);
	}

	public static void announce()
	{
		// Ивент отменён так как не участников ;(
		if(_time_to_start <= 1 && (teams[RED_TEAM].isEmpty() || teams[BLUE_TEAM].isEmpty()))
		{
			sayToAll("scripts.events.TvT.AnnounceEventCancelled");
			_isRegistrationActive = false;
			_status = 0;
			executeTask("events.tvt.TvT", "autoContinue", EMPTY_ARG, 5000);
			return;
		}

		if(_time_to_start > 1)
		{
			_time_to_start--;
			final String[] param = { "" + _time_to_start, "" + _minLevel, "" + _maxLevel };
			sayToAll("scripts.events.TvT.AnnouncePreStart", param);
			executeTask("events.tvt.TvT", "announce", EMPTY_ARG, 60000);
		}
		else
		{
			_status = 1;
			_isRegistrationActive = false;
			sayToAll("scripts.events.TvT.AnnounceEventStarting");
			executeTask("events.tvt.TvT", "prepare", EMPTY_ARG, 5000);
		}
	}

	public void addPlayer()
	{
		final L2Player player = (L2Player) getSelf();
		if(player == null || !checkPlayer(player, true))
			return;

		int team = 0;
		final int size1 = teams[RED_TEAM].getPlayersCount(), size2 = teams[BLUE_TEAM].getPlayersCount();

		if(size1 > size2)
			team = 2;
		else if(size1 < size2)
			team = 1;
		else
			team = Rnd.get(1, 2);

		if(!checkCountTeam(team))
		{
			show(new CustomMessage("scripts.events.MaxCountTeam", player), player);
			return;
		}

		if(team == 1)
		{
			teams[RED_TEAM].addPlayer(player);
			show(new CustomMessage("scripts.events.TvT.Registered", player), player);
		}
		else if(team == 2)
		{
			teams[BLUE_TEAM].addPlayer(player);
			show(new CustomMessage("scripts.events.TvT.Registered", player), player);
		}
		else
			_log.info("WTF??? Command id 0 in TvT...");
	}

	private static boolean checkCountTeam(final int team)
	{
		if(team == 1 && teams[RED_TEAM].getPlayersCount() >= Config.EVENT_TVT_MAX_LENGTH_TEAM)
			return false;
		else if(team == 2 && teams[BLUE_TEAM].getPlayersCount() >= Config.EVENT_TVT_MAX_LENGTH_TEAM)
			return false;
		return true;
	}

	public static boolean checkPlayer(final L2Player player, final boolean first)
	{
		if(first && !_isRegistrationActive)
		{
			show(new CustomMessage("scripts.events.Late", player), player);
			return false;
		}

		if(first && player.isDead())
			return false;

		if(first && (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())))
		{
			show(new CustomMessage("scripts.events.TvT.Cancelled", player), player);
			return false;
		}

		if(player.getLevel() < _minLevel || player.getLevel() > _maxLevel)
		{
			show(new CustomMessage("scripts.events.TvT.CancelledLevel", player), player);
			return false;
		}

		if(player.isMounted())
		{
			show(new CustomMessage("scripts.events.TvT.Cancelled", player), player);
			return false;
		}

		if(player.getDuel() != null)
		{
			show(new CustomMessage("scripts.events.TvT.CancelledDuel", player), player);
			return false;
		}

		if(first && player.isInEvent(L2EventType.NONE) || player.getTeam() != 0)
		{
			show(new CustomMessage("scripts.events.TvT.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.isInOlympiadMode() || player.isInZoneOlympiad() || first && Olympiad.isRegisteredInComp(player))
		{
			show(new CustomMessage("scripts.events.TvT.CancelledOlympiad", player), player);
			return false;
		}

		if(player.isInParty() && player.getParty().isInDimensionalRift())
		{
			show(new CustomMessage("scripts.events.TvT.CancelledOtherEvent", player), player);
			return false;
		}

		if(player.isTeleporting())
		{
			show(new CustomMessage("scripts.events.TvT.CancelledTeleport", player), player);
			return false;
		}

		if(player.isCursedWeaponEquipped())
		{
			show(new CustomMessage("scripts.events.TvT.Cancelled", player), player);
			return false;
		}

		// последним проверяем HardwareID
		if(first && !hwid_check.canParticipate(player))
		{
			show(new CustomMessage("scripts.events.TvT.Cancelled", player), player);
			return false;
		}

		return true;
	}

	@Override
	protected boolean isInEvent(final L2Player player, final L2EventType type)
	{
		return (teams[RED_TEAM].isInTeam(player.getStoredId()) || teams[BLUE_TEAM].isInTeam(player.getStoredId())) && (type == L2EventType.NONE || type == getEventType());
	}

	@Override
	protected void onDie(final L2Character victim, final L2Character killer)
	{
		if(_status > 1 && victim != null && victim.isPlayer() && victim.getTeam() > 0)
		{
			// если жертва находиться в одной из команд
			if(teams[RED_TEAM].isLive(victim.getStoredId()) || teams[BLUE_TEAM].isLive(victim.getStoredId()))
			{
				// увеличиваем счётчик смертей игроку
				teams[victim.getTeam() - 1].getPlayerInfo(victim.getStoredId()).incrementDeaths();
				// Добавляем очки противоположной команде
				teams[victim.getTeam() == 1 ? 1 : 0].increaseTeamPoints();
			}
			// увеличиваем счётчик убийств
			if(killer.getPlayer() != null) // вдруг суммоном убил
			{
				final L2Player player = killer.getPlayer();
				// если убийца находиться в одной из команд
				if(teams[RED_TEAM].isLive(player.getStoredId()) || teams[BLUE_TEAM].isLive(player.getStoredId()))
					teams[player.getTeam() - 1].getPlayerInfo(player.getStoredId()).incrementKills();
			}

			loosePlayer(victim.getPlayer(), true);
			checkLive();
		}
	}

	public static void prepare()
	{
		cleanPlayers();
		clearArena();
		closeColiseumDoors();

		executeTask("events.tvt.TvT", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.tvt.TvT", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.tvt.TvT", "saveBackCoords", EMPTY_ARG, 2000);
		executeTask("events.tvt.TvT", "teleportPlayersToColiseum", EMPTY_ARG, 3000);
		executeTask("events.tvt.TvT", "paralyzePlayers", EMPTY_ARG, 5000);
		executeTask("events.tvt.TvT", "go", EMPTY_ARG, 60000);

		sayToAll("scripts.events.TvT.AnnounceFinalCountdown");
	}

	@Override
	public void go()
	{
		_status = 2;
		upParalyzePlayers();
		checkLive();
		clearArena();
		sayToAll("scripts.events.TvT.AnnounceFight");

		// бафаем игроков перед стартом
		if(Config.EVENT_TVT_BUFFS_ONSTART)
		{
			teams[BLUE_TEAM].buffAllPlayers(this);
			teams[RED_TEAM].buffAllPlayers(this);
		}

		_endTask = executeTask("events.tvt.TvT", "endBattle", EMPTY_ARG, Config.EVENT_TVT_TIME_FOR_FIGHT * 60 * 1000);
	}

	public static void endBattle()
	{
		_status = 0;
		removeAura();

		final int blue_score = teams[BLUE_TEAM].getTeamPoints();
		final int red_score = teams[RED_TEAM].getTeamPoints();
		if(blue_score > red_score)
		{
			sayToAll("scripts.events.TvT.AnnounceFinishedBlueWins");
			giveItemsToWinner(false, true, 1);
		}
		else if(red_score > blue_score)
		{
			sayToAll("scripts.events.TvT.AnnounceFinishedRedWins");
			giveItemsToWinner(true, false, 1);
		}
		else if(blue_score == red_score && blue_score > 0)
		{
			sayToAll("scripts.events.TvT.AnnounceFinishedDraw"); // если убийства есть)
			giveItemsToWinner(true, true, 0.5);
		}
		else if(blue_score == red_score && blue_score <= 0) // если вдруг никто никого не убил
			sayToAll("scripts.events.TvT.AnnounceFinishedDraw");

		// отправляем результаты мача
		final ExPVPMatchRecord result = new ExPVPMatchRecord(teams[BLUE_TEAM], teams[RED_TEAM]);
		teams[BLUE_TEAM].sendPacketToAll(result);
		teams[RED_TEAM].sendPacketToAll(result);

		sayToAll("scripts.events.TvT.AnnounceScore", new String[] { "" + red_score, "" + blue_score });

		sayToAll("scripts.events.TvT.AnnounceEnd");
		executeTask("events.tvt.TvT", "end", EMPTY_ARG, 30000);

		_isRegistrationActive = false;
		if(_endTask != null)
		{
			_endTask.cancel(false);
			_endTask = null;
		}
	}

	public static void end()
	{
		openColiseumDoors();

		executeTask("events.tvt.TvT", "ressurectPlayers", EMPTY_ARG, 1000);
		executeTask("events.tvt.TvT", "healPlayers", EMPTY_ARG, 2000);
		executeTask("events.tvt.TvT", "teleportPlayersToSavedCoords", EMPTY_ARG, 3000);
		executeTask("events.tvt.TvT", "autoContinue", EMPTY_ARG, 5000);
	}

	public void autoContinue()
	{
		teams[BLUE_TEAM].clear();
		teams[RED_TEAM].clear();
		hwid_check.clear();

		// если больше, то стартуем автоматом следующие уровни
		if(_autoContinue > 0)
		{
			if(_autoContinue >= 6)
			{
				_autoContinue = 0;
				return;
			}
			start(new String[] { "" + (_autoContinue + 1), "" + (_autoContinue + 1) });
		}
		else
		{
			// удаляем если нет продолжения
			if(_addEventType)
			{
				_addEventType = false;
				removeEventId(ScriptEventType.ON_DISCONNECT);
				removeEventId(ScriptEventType.ON_ESCAPE);
				removeEventId(ScriptEventType.ON_IS_IN_EVENT_CHECK);
				removeEventId(ScriptEventType.ON_DIE);
			}

			// пробуем зашедулить по времени из конфигов
			scheduleEventStart(true);
		}
	}

	/**
	 * проверяет возможность запуска ивента и стартует такс в указанное в конфигах время
	 * 
	 * @param check
	 *            - проверять активирован ли ивент
	 */
	public void scheduleEventStart(final boolean check)
	{
		// ивент должен быть активированным для автоматического запуска
		if(check && !_active)
			return;

		try
		{
			final Calendar currentTime = Calendar.getInstance();
			int nextCategory = -1;
			Calendar testStartTime = null;
			Calendar nextStartTime = null;

			// перебираем всё варианты старта... находим оптимальный вариант
			for(final EventInterval interval : Config.EVENT_TVT_INTERVAL)
			{
				// Creating a Calendar object from the specified interval value
				testStartTime = Calendar.getInstance();
				testStartTime.setLenient(true); // чтоб выдало исключение если время задано не верно в конфигах

				// устанавливаем время предположительно след запуска
				testStartTime.set(Calendar.HOUR_OF_DAY, interval.hour);
				testStartTime.set(Calendar.MINUTE, interval.minute);

				// If the date is in the past, make it the next day (Example: Checking for "1:00", when the time is 23:57.)
				if(testStartTime.getTimeInMillis() < currentTime.getTimeInMillis())
					testStartTime.add(Calendar.DAY_OF_MONTH, 1);

				// сравниваем, если подходит то устанавливаем
				if(nextStartTime == null || testStartTime.getTimeInMillis() < nextStartTime.getTimeInMillis())
				{
					nextStartTime = testStartTime;
					nextCategory = interval.category;
				}
			}

			_log.info("TvT: next start at " + Util.datetimeFormatter.format(nextStartTime.getTime()) + ", category: " + nextCategory);
			// запускаем ивент с указанной катерорией и без автопродолжения
			if(_startTask != null)
			{
				_startTask.cancel(false);
				_startTask = null;
			}
			_startTask = L2GameThreadPools.getInstance().scheduleGeneral(new StartTask(nextCategory, 0), nextStartTime.getTimeInMillis() - System.currentTimeMillis());
		}
		catch(final Exception e)
		{
			_log.log(Level.WARNING, "TvT.scheduleEventStart(): error figuring out a start time. Check config file.", e);
		}
	}

	public static void giveItemsToWinner(final boolean red, final boolean blue, final double rate)
	{
		if(red)
		{
			final GArray<L2Player> players = teams[RED_TEAM].getAllPlayers();
			for(final L2Player player : players)
				addItem(player, Config.EVENT_TVT_ITEM_ID, Math.round((Config.EVENT_TVT_RATE ? player.getLevel() : 1) * Config.EVENT_TVT_ITEM_COUNT * rate));
		}
		if(blue)
		{
			final GArray<L2Player> players = teams[BLUE_TEAM].getAllPlayers();
			for(final L2Player player : players)
				addItem(player, Config.EVENT_TVT_ITEM_ID, Math.round((Config.EVENT_TVT_RATE ? player.getLevel() : 1) * Config.EVENT_TVT_ITEM_COUNT * rate));
		}
	}

	public static void saveBackCoords()
	{
		teams[RED_TEAM].saveBackCoords();
		teams[BLUE_TEAM].saveBackCoords();
		teams[RED_TEAM].addRestrictions(Config.EVENT_TVT_ITEM_RESTRICTION, Config.EVENT_TVT_SKILL_RESTRICTION);
		teams[BLUE_TEAM].addRestrictions(Config.EVENT_TVT_ITEM_RESTRICTION, Config.EVENT_TVT_SKILL_RESTRICTION);
	}

	public static void teleportPlayersToColiseum()
	{
		for(final L2Player player : teams[RED_TEAM].getAllPlayers())
		{
			unRide(player);

			if(!Config.EVENT_TVT_ALLOW_SUMMONS)
				unSummonPet(player);

			// Remove Buffs
			if(!Config.EVENT_TVT_ALLOW_BUFFS)
			{
				player.stopAllEffects();
				if(player.getPet() != null)
					player.getPet().stopAllEffects();
			}

			// Remove Clan Skills
			if(!Config.EVENT_TVT_ALLOW_CLAN_SKILL)
				if(player.getClan() != null)
					for(final L2Skill skill : player.getClan().getAllSkills())
						player.removeSkill(skill, false);

			if(!Config.EVENT_TVT_ALLOW_HERO_SKILL)
				// Remove Hero Skills
				if(player.isHero())
					for(final L2Skill sk : HeroSkillTable.getHeroSkills())
						player.removeSkillById(sk.getId());

			// Force the character to be mortal
			if(player.isInvul())
				player.setInvul(false);

			// Force the character to be visible
			if(player.isInvisible())
				player.setInvisible(false);

			if(Config.EVENT_TVT_DISPEL_TRANSFORMATION && player.isTransformed() && !player.getTransform().isDefaultActionListTransform())
				player.stopTransformation();

			final int[] pos = team1loc.getRandomPoint();
			player.teleToLocation(pos[0], pos[1], pos[2], ReflectionTable.EVENT_TVT);
		}

		for(final L2Player player : teams[BLUE_TEAM].getAllPlayers())
		{
			unRide(player);

			if(!Config.EVENT_TVT_ALLOW_SUMMONS)
				unSummonPet(player);

			// Remove Buffs
			if(!Config.EVENT_TVT_ALLOW_BUFFS)
			{
				player.stopAllEffects();
				if(player.getPet() != null)
					player.getPet().stopAllEffects();
			}

			// Remove Clan Skills
			if(!Config.EVENT_TVT_ALLOW_CLAN_SKILL)
				if(player.getClan() != null)
					for(final L2Skill skill : player.getClan().getAllSkills())
						player.removeSkill(skill, false);

			if(!Config.EVENT_TVT_ALLOW_HERO_SKILL)
				// Remove Hero Skills
				if(player.isHero())
					for(final L2Skill sk : HeroSkillTable.getHeroSkills())
						player.removeSkillById(sk.getId());

			// Force the character to be mortal
			if(player.isInvul())
				player.setInvul(false);

			// Force the character to be visible
			if(player.isInvisible())
				player.setInvisible(false);

			if(Config.EVENT_TVT_DISPEL_TRANSFORMATION && player.isTransformed() && !player.getTransform().isDefaultActionListTransform())
				player.stopTransformation();

			final int[] pos = team2loc.getRandomPoint();
			player.teleToLocation(pos[0], pos[1], pos[2], ReflectionTable.EVENT_TVT);
		}
	}

	public static void teleportPlayersToSavedCoords()
	{
		teams[RED_TEAM].removeRestrictions(Config.EVENT_TVT_ITEM_RESTRICTION, Config.EVENT_TVT_SKILL_RESTRICTION);
		teams[BLUE_TEAM].removeRestrictions(Config.EVENT_TVT_ITEM_RESTRICTION, Config.EVENT_TVT_SKILL_RESTRICTION);
		teams[RED_TEAM].teleportToSavedCoords(true);
		teams[BLUE_TEAM].teleportToSavedCoords(true);
	}

	public static void paralyzePlayers()
	{
		teams[BLUE_TEAM].paralyzePlayers();
		teams[RED_TEAM].paralyzePlayers();
	}

	public static void upParalyzePlayers()
	{
		teams[RED_TEAM].upParalyzePlayers(true);
		teams[BLUE_TEAM].upParalyzePlayers(true);
	}

	public static void ressurectPlayers()
	{
		teams[RED_TEAM].ressurectPlayers();
		teams[BLUE_TEAM].ressurectPlayers();
	}

	public static void healPlayers()
	{
		teams[RED_TEAM].healPlayers();
		teams[BLUE_TEAM].healPlayers();
	}

	public static void cleanPlayers()
	{
		for(final L2Player player : teams[RED_TEAM].getAllPlayers())
			if(!checkPlayer(player, false))
				removePlayer(player);
		for(final L2Player player : teams[BLUE_TEAM].getAllPlayers())
			if(!checkPlayer(player, false))
				removePlayer(player);
	}

	public static void checkLive()
	{
		teams[RED_TEAM].updateLivePlayers();
		teams[BLUE_TEAM].updateLivePlayers();

		for(final L2Player player : teams[RED_TEAM].getLivePlayers())
			if(player.isInZone(_zone) && player.isConnected() && !player.isLogoutStarted())
				player.setTeam(2, true);
			else
				loosePlayer(player, false);

		for(final L2Player player : teams[BLUE_TEAM].getLivePlayers())
			if(player.isInZone(_zone) && player.isConnected() && !player.isLogoutStarted())
				player.setTeam(1, true);
			else
				loosePlayer(player, false);

		if(teams[RED_TEAM].getLivePlayersCount() < 1 || teams[BLUE_TEAM].getLivePlayersCount() < 1)
			endBattle();
	}

	public static void removeAura()
	{
		teams[RED_TEAM].removeAura();
		teams[BLUE_TEAM].removeAura();
	}

	public static void clearArena()
	{
		for(final L2Character obj : _zone.getObjectsInside())
			if(obj != null && obj.getReflectionId() == ReflectionTable.EVENT_TVT)
			{
				final L2Player player = obj.getPlayer();
				if(player != null && !teams[RED_TEAM].isLive(player.getStoredId()) && !teams[BLUE_TEAM].isLive(player.getStoredId()))
					player.teleToLocation(147451, 46728, -3410);
			}
	}

	@Override
	protected final void onEscape(final L2Player player)
	{
		if(_status > 1 && player != null && player.getTeam() > 0 && (teams[RED_TEAM].isLive(player.getStoredId()) || teams[BLUE_TEAM].isLive(player.getStoredId())))
		{
			removePlayer(player);
			checkLive();
		}
	}

	@Override
	protected final void onDisconnect(final L2Player player)
	{
		if(player == null || player.getTeam() < 1)
			return;

		// Вышел или вылетел во время регистрации
		if(_status == 0 && _isRegistrationActive && player.getTeam() > 0 && (teams[RED_TEAM].isLive(player.getStoredId()) || teams[BLUE_TEAM].isLive(player.getStoredId())))
		{
			removePlayer(player);
			return;
		}

		// Вышел или вылетел во время телепортации
		if(_status == 1 && (teams[RED_TEAM].isLive(player.getStoredId()) || teams[BLUE_TEAM].isLive(player.getStoredId())))
		{
			removePlayer(player);
			try
			{
				final String var = player.getVar("event_backcoords");
				if(var == null || var.equals(""))
					return;
				final String[] coords = var.split(" ");
				if(coords.length != 4)
					return;
				player.teleToLocation(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]), Integer.parseInt(coords[2]), Long.parseLong(coords[3]));
				player.unsetVar("event_backcoords");
			}
			catch(final Exception e)
			{
				e.printStackTrace();
			}

			return;
		}

		// Вышел или вылетел во время эвента
		onEscape(player);
	}

	private static class ZoneListener extends L2ZoneEnterLeaveListener
	{
		@Override
		public void objectEntered(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 0 && player != null && !teams[RED_TEAM].isLive(player.getStoredId()) && !teams[BLUE_TEAM].isLive(player.getStoredId()) && player.getReflectionId() == ReflectionTable.EVENT_TVT)
				L2GameThreadPools.getInstance().scheduleGeneral(new TeleportTask(character, new Location(147451, 46728, -3410)), 3000);
		}

		@Override
		public void objectLeaved(final L2Zone zone, final L2Character character)
		{
			if(character == null)
				return;
			final L2Player player = character.getPlayer();
			if(_status > 1 && player != null && player.getTeam() > 0 && (teams[RED_TEAM].isLive(player.getStoredId()) || teams[BLUE_TEAM].isLive(player.getStoredId())) && player.getReflectionId() == ReflectionTable.EVENT_TVT)
			{
				final double angle = Util.convertHeadingToDegree(character.getHeading()); // угол в градусах
				final double radian = Math.toRadians(angle - 90); // угол в радианах
				final int x = (int) (character.getX() + 50 * Math.sin(radian));
				final int y = (int) (character.getY() - 50 * Math.cos(radian));
				final int z = character.getZ();
				L2GameThreadPools.getInstance().scheduleGeneral(new TeleportTask(character, new Location(x, y, z)), 3000);
			}
		}
	}

	private static void loosePlayer(final L2Player player, final boolean inc)
	{
		if(player != null)
			if(inc && player.getTeam() > 0)
			{
				show(new CustomMessage("scripts.events.Revive", player).addNumber(Config.EVENT_TVT_REVIVE_DELAY), player);
				executeTask("events.tvt.TvT", "resurrect", new Object[] { player }, Config.EVENT_TVT_REVIVE_DELAY * 1000);
				executeTask("events.tvt.TvT", "showScore", new Object[] { player }, 500);
			}
			else
			{
				teams[RED_TEAM].removeFromLive(player);
				teams[BLUE_TEAM].removeFromLive(player);
				player.setTeam(0, true);
				show(new CustomMessage("scripts.events.TvT.YouLose", player), player);
			}
	}

	private static void removePlayer(final L2Player player)
	{
		if(player != null)
		{
			teams[RED_TEAM].removePlayer(player);
			teams[BLUE_TEAM].removePlayer(player);
		}
	}

	public void resurrect(final L2Player player)
	{
		if(player.getTeam() <= 0)
			return;

		if(player.isDead())
		{
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentHp(player.getMaxHp(), true);
			player.setCurrentMp(player.getMaxMp());
			player.broadcastPacket(new Revive(player));
		}

		// бафаем
		L2EventTeam.addEventBuffs(player, this);

		int[] pos;
		if(player.getTeam() == 1)
			pos = team2loc.getRandomPoint();
		else
			pos = team1loc.getRandomPoint();
		player.teleToLocation(pos[0], pos[1], pos[2]);
	}

	/**
	 * Отправляет сообщение игрокам о количестве очков
	 */
	public static void showScore(final L2Player player)
	{
		if(player == null || player.getTeam() == 0)
			return;

		switch (player.getTeam() - 1)
		{
		// если умер 'синий' отправляем сообщения красным с количеством очков
			case BLUE_TEAM:
			{
				final ExShowScreenMessage score = new ExShowScreenMessage("Очей: " + teams[RED_TEAM].getTeamPoints(), 3000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, true);
				teams[RED_TEAM].sendPacketToLive(score);
				break;
			}
			// если умер 'красный' отправляем сообщения синим с количеством очков
			case RED_TEAM:
			{
				final ExShowScreenMessage score = new ExShowScreenMessage("Очей: " + teams[BLUE_TEAM].getTeamPoints(), 3000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, true);
				teams[BLUE_TEAM].sendPacketToLive(score);
				break;
			}
		}
	}

	private static void openColiseumDoors()
	{
		_reflection.openDoors(_doors);
	}

	private static void closeColiseumDoors()
	{
		_reflection.closeDoors(_doors);
	}

	public boolean isRunned()
	{
		return _isRegistrationActive || _status > 0;
	}

	@Override
	public boolean isActive()
	{
		return _active;
	}

	/**
	 * @see l2n.game.event.L2Event#getEventType()
	 */
	@Override
	public L2EventType getEventType()
	{
		return L2EventType.TVT;
	}
}
