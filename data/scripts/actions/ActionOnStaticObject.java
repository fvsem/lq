package actions;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2StaticObjectInstance;
import l2n.game.network.serverpackets.NpcHtmlMessage;

public class ActionOnStaticObject extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, final boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !player.getPlayerAccess().Door || !object.isStaticObject())
			return false;

		final L2StaticObjectInstance staticObject = (L2StaticObjectInstance) object;

		final NpcHtmlMessage html = new NpcHtmlMessage(staticObject.getObjectId());
		final StringBuilder html1 = new StringBuilder("<html><body><table border=0>");
		html1.append("<tr><td>X: " + staticObject.getX() + "</td></tr>");
		html1.append("<tr><td>Y: " + staticObject.getY() + "</td></tr>");
		html1.append("<tr><td>Z: " + staticObject.getZ() + "</td></tr>");
		html1.append("<tr><td>Object ID: " + staticObject.getObjectId() + "</td></tr>");
		html1.append("<tr><td>Static Object ID: " + staticObject.getStaticObjectId() + "</td></tr>");
		html1.append("<tr><td>Mesh Index: " + staticObject.getMeshIndex() + "</td></tr>");
		html1.append("<tr><td><br></td></tr>");
		html1.append("<tr><td>Class: " + staticObject.getClass().getName() + "</td></tr>");
		html1.append("<tr><td><br></td></tr>");
		html1.append("</table></body></html>");

		html.setHtml(html1.toString());
		player.sendPacket(html);

		return true;
	}
}
