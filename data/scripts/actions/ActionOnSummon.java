package actions;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Object;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2SummonInstance;
import l2n.util.Files;

public class ActionOnSummon extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, final boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !player.getPlayerAccess().EditChar || !object.isSummon())
			return false;

		String dialog;
		final L2SummonInstance summon = (L2SummonInstance) object;
		dialog = Files.read("data/scripts/actions/admin.L2SummonInstance.onActionShift.htm");
		dialog = dialog.replaceFirst("%name%", String.valueOf(summon.getName()));
		dialog = dialog.replaceFirst("%level%", String.valueOf(summon.getLevel()));
		dialog = dialog.replaceFirst("%class%", String.valueOf(summon.getClass().getSimpleName().replaceFirst("L2", "").replaceFirst("Instance", "")));
		dialog = dialog.replaceFirst("%xyz%", summon.getLoc().toXYZString() + " (" + summon.getReflection().getId() + ")");
		dialog = dialog.replaceFirst("%heading%", String.valueOf(summon.getLoc().h));

		dialog = dialog.replaceFirst("%owner%", String.valueOf(summon.getPlayer().getName()));
		dialog = dialog.replaceFirst("%ownerId%", String.valueOf(summon.getPlayer().getObjectId()));

		dialog = dialog.replaceFirst("%npcId%", String.valueOf(summon.getNpcId()));
		dialog = dialog.replaceFirst("%expPenalty%", String.valueOf(summon.getExpPenalty()));

		dialog = dialog.replaceFirst("%maxHp%", String.valueOf(summon.getMaxHp()));
		dialog = dialog.replaceFirst("%maxMp%", String.valueOf(summon.getMaxMp()));
		dialog = dialog.replaceFirst("%currHp%", String.valueOf((int) summon.getCurrentHp()));
		dialog = dialog.replaceFirst("%currMp%", String.valueOf((int) summon.getCurrentMp()));

		dialog = dialog.replaceFirst("%pDef%", String.valueOf(summon.getPDef(null)));
		dialog = dialog.replaceFirst("%mDef%", String.valueOf(summon.getMDef(null, null)));
		dialog = dialog.replaceFirst("%pAtk%", String.valueOf(summon.getPAtk(null)));
		dialog = dialog.replaceFirst("%mAtk%", String.valueOf(summon.getMAtk(null, null)));
		dialog = dialog.replaceFirst("%accuracy%", String.valueOf(summon.getAccuracy()));
		dialog = dialog.replaceFirst("%evasionRate%", String.valueOf(summon.getEvasionRate(null)));
		dialog = dialog.replaceFirst("%crt%", String.valueOf(summon.getCriticalHit(null, null)));
		dialog = dialog.replaceFirst("%runSpeed%", String.valueOf(summon.getRunSpeed()));
		dialog = dialog.replaceFirst("%walkSpeed%", String.valueOf(summon.getWalkSpeed()));
		dialog = dialog.replaceFirst("%pAtkSpd%", String.valueOf(summon.getPAtkSpd()));
		dialog = dialog.replaceFirst("%mAtkSpd%", String.valueOf(summon.getMAtkSpd()));
		dialog = dialog.replaceFirst("%dist%", String.valueOf((int) summon.getRealDistance(player)));

		dialog = dialog.replaceFirst("%STR%", String.valueOf(summon.getSTR()));
		dialog = dialog.replaceFirst("%DEX%", String.valueOf(summon.getDEX()));
		dialog = dialog.replaceFirst("%CON%", String.valueOf(summon.getCON()));
		dialog = dialog.replaceFirst("%INT%", String.valueOf(summon.getINT()));
		dialog = dialog.replaceFirst("%WIT%", String.valueOf(summon.getWIT()));
		dialog = dialog.replaceFirst("%MEN%", String.valueOf(summon.getMEN()));

		show(dialog, player);

		return true;
	}
}
