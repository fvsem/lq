package actions;

import l2n.extensions.scripts.EventScript;
import l2n.extensions.scripts.ScriptEventType;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.model.L2Object;
import l2n.game.model.L2ObjectsStorage;
import l2n.game.model.actor.L2Player;
import l2n.game.model.instances.L2ItemInstance;
import l2n.game.templates.L2Armor;
import l2n.game.templates.L2Weapon;
import l2n.util.Files;

public final class ActionOnItem extends EventScript implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: " + getClass().getSimpleName());
		addEventId(ScriptEventType.ON_ACTION);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean onAction(final L2Player player, final L2Object object, final boolean shift)
	{
		if(!OnActionShift.checkCondition(player, object, shift) || !player.getPlayerAccess().IsGM || !object.isItem())
			return false;

		String dialog;
		final L2ItemInstance item = (L2ItemInstance) object;
		dialog = Files.read("data/scripts/actions/admin.L2ItemInstance.onActionShift.htm");
		dialog = dialog.replaceFirst("%name%", String.valueOf(item.getItem().getName()));
		dialog = dialog.replaceFirst("%objId%", String.valueOf(item.getObjectId()));
		dialog = dialog.replaceFirst("%itemId%", String.valueOf(item.getItemId()));
		dialog = dialog.replaceFirst("%grade%", String.valueOf(item.getCrystalType()));
		dialog = dialog.replaceFirst("%count%", String.valueOf(item.getCount()));

		dialog = dialog.replaceFirst("%owner%", String.valueOf(L2ObjectsStorage.getAsPlayer(item.getOwnerId())));
		dialog = dialog.replaceFirst("%ownerId%", String.valueOf(item.getOwnerId()));

		if(item.getItem() instanceof L2Weapon)
		{
			dialog = dialog.replaceFirst("%pAtk%", String.valueOf(((L2Weapon) item.getItem()).getPDamage()));
			dialog = dialog.replaceFirst("%mAtk%", String.valueOf(((L2Weapon) item.getItem()).getMDamage()));
		}
		else
		{
			dialog = dialog.replaceFirst("%pAtk%", "none");
			dialog = dialog.replaceFirst("%mAtk%", "none");
		}

		if(item.getItem() instanceof L2Armor)
		{
			dialog = dialog.replaceFirst("%pDef%", String.valueOf(((L2Armor) item.getItem()).getPDefence()));
			dialog = dialog.replaceFirst("%mDef%", String.valueOf(((L2Armor) item.getItem()).getMDefence()));
		}
		else
		{
			dialog = dialog.replaceFirst("%pDef%", "none");
			dialog = dialog.replaceFirst("%mDef%", "none");
		}

		dialog = dialog.replaceFirst("%attrElement%", String.valueOf(item.getAttributeElement()));
		dialog = dialog.replaceFirst("%attrValue%", String.valueOf(item.getAttributeElementValue()));

		dialog = dialog.replaceFirst("%enchLevel%", String.valueOf(item.getEnchantLevel()));
		dialog = dialog.replaceFirst("%type%", String.valueOf(item.getItemType()));
		show(dialog, player);

		return true;
	}
}
