package community;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.CommunityBoardHandlers;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.model.actor.L2Player;


public class ExampleCBHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler, ScriptFile
{
	private static final String[] commands = new String[]
	{
			"_bbs_test"
	};

	/**
	 * @param activeChar
	 *            - игрок L2Player
	 * @param command
	 *            - команда String
	 * @param params
	 *            - параметры String[]
	 * @param source
	 *            - полная команда
	 * @return
	 */
	@Override
	public boolean execute(L2Player activeChar, String command, String[] params, String source)
	{
		if(command.equalsIgnoreCase("_bbs_test"))
		{
			separateAndSend("<html><body><br><br><center>community.ExampleCBHandler execute successfully ;)</center><br><br></body></html>", activeChar);
		}
		return false;
	}

	/**
	 * Проверяется перед использование метода execute(L2Player, String, String[], String)
	 * 
	 * @param activeChar
	 *            - игрок L2Player
	 * @param command
	 *            - команда
	 * @return true если разрешено использовать данный обработчик
	 */
	@Override
	public boolean checkCondition(L2Player activeChar, String command)
	{
		// разрешаем только для гмов
		return activeChar.isGM();
	}

	/**
	 * @return - список команд
	 */
	@Override
	public String[] getCommands()
	{
		return commands;
	}

	@Override
	public void onLoad()
	{
		// добавляем обработчик
		CommunityBoardHandlers.getInstance().registerHandler(this);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}
}
