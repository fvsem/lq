package community;

import l2n.extensions.scripts.ScriptFile;
import l2n.game.cache.Msg;
import l2n.game.communitybbs.AbstractCommunityBoardHandler;
import l2n.game.communitybbs.CommunityBoardHandlers;
import l2n.game.communitybbs.ICommunityBoardHandler;
import l2n.game.model.actor.L2Player;

public final class AugmentHandler extends AbstractCommunityBoardHandler implements ICommunityBoardHandler, ScriptFile
{
	private static final String[] commands = new String[]
	{
			"_bbs_augment",
			"_bbs_delaugment"
	};

	@Override
	public void onLoad()
	{
		CommunityBoardHandlers.getInstance().registerHandler(this);
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	@Override
	public boolean execute(final L2Player player, final String command, final String[] params, final String source)
	{
		if(command.equalsIgnoreCase("_bbs_augment"))
		{
			player.sendPacket(Msg.SELECT_THE_ITEM_TO_BE_AUGMENTED, Msg.ExShowVariationMakeWindow);
			return true;
		}
		else if(command.equalsIgnoreCase("_bbs_delaugment"))
		{
			player.sendPacket(Msg.SELECT_THE_ITEM_FROM_WHICH_YOU_WISH_TO_REMOVE_AUGMENTATION, Msg.ExShowVariationCancelWindow);
			return true;
		}
		return false;
	}

	@Override
	public boolean checkCondition(final L2Player activeChar, final String command)
	{
		return true;
	}

	@Override
	public String[] getCommands()
	{
		return commands;
	}
}
