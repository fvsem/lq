import l2n.Config;
import l2n.extensions.scripts.Functions;
import l2n.extensions.scripts.ScriptFile;
import l2n.game.L2GameThreadPools;
import l2n.game.cache.Msg;
import l2n.game.geodata.GeoEngine;
import l2n.game.instancemanager.TownManager;
import l2n.game.instancemanager.TullyWorkshopManager;
import l2n.game.instancemanager.ZoneManager;
import l2n.game.model.L2Skill;
import l2n.game.model.L2Zone;
import l2n.game.model.L2Zone.ZoneType;
import l2n.game.model.actor.L2Character;
import l2n.game.model.actor.L2Playable;
import l2n.game.model.actor.L2Player;
import l2n.game.model.entity.SevenSigns;
import l2n.game.model.entity.residence.Castle;
import l2n.game.model.instances.L2NpcInstance;
import l2n.game.network.serverpackets.SystemMessage;
import l2n.game.tables.ReflectionTable;
import l2n.game.tables.SkillTable;
import l2n.util.Location;

public class Util extends Functions implements ScriptFile
{
	@Override
	public void onLoad()
	{
		System.out.println("Loaded Script: Utilites");
	}

	@Override
	public void onReload()
	{}

	@Override
	public void onShutdown()
	{}

	/**
	 * Перемещает за плату в аденах
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param price
	 */
	public void Gatekeeper(final String[] param)
	{
		if(param.length < 4)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		if(player == null)
			return;

		final int price = Integer.parseInt(param[3]);

		if(player.isActionsDisabled() || player.isSitting() || player.getLastNpc() == null || player.getLastNpc().getDistance(player) > 300)
			return;

		if(price > 0 && player.getAdena() < price)
		{
			player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			return;
		}

		if(player.isTerritoryFlagEquipped() || player.isCombatFlagEquipped())
		{
			player.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
			return;
		}

		if(player.getMountType() == 2)
		{
			player.sendMessage("Телепортация верхом на виверне невозможна.");
			return;
		}

		/*
		 * Затычка, npc Mozella не ТПшит чаров уровень которых превышает заданный в конфиге
		 * Off Like >= 56 lvl, данные по ограничению lvl'a устанавливаются в alternative.ini.
		 */
		if(player.getLastNpc() != null)
		{
			final int mozella_cruma = 30483; // NPC Mozella id 30483
			if(player.getLastNpc().getNpcId() == mozella_cruma && player.getLevel() >= Config.CRUMA_GATEKEEPER_LVL)
			{
				show("data/html/teleporter/30483-no.htm", player);
				return;
			}
		}

		final int x = Integer.parseInt(param[0]);
		final int y = Integer.parseInt(param[1]);
		final int z = Integer.parseInt(param[2]);

		// Нельзя телепортироваться в города, где идет осада
		// Узнаем, идет ли осада в ближайшем замке к точке телепортации
		final Castle castle = TownManager.getInstance().getClosestTown(x, y).getCastle();
		if(castle != null && castle.getSiege().isInProgress())
		{
			// Определяем, в город ли телепортируется чар
			boolean teleToTown = false;
			int townId = 0;
			for(final L2Zone town : ZoneManager.getInstance().getZoneByType(ZoneType.Town))
				if(town.checkIfInZone(x, y))
				{
					teleToTown = true;
					townId = town.getIndex();
					break;
				}

			if(teleToTown && townId == castle.getTown())
			{
				player.sendPacket(Msg.YOU_CANNOT_TELEPORT_TO_A_VILLAGE_THAT_IS_IN_A_SIEGE);
				return;
			}
		}

		final Location pos = GeoEngine.findPointToStay(x, y, z, 50, 100);

		if(price > 0)
			player.reduceAdena(price, true);
		player.teleToLocation(pos);
	}

	public void SSGatekeeper(final String[] param)
	{
		if(param.length < 4)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		final int type = Integer.parseInt(param[3]);

		if(player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		if(type > 0 && Config.ALT_SEVEN_SIGNS_RESTRICT)
		{
			if(SevenSigns.getInstance().getCurrentPeriod() == SevenSigns.PERIOD_COMPETITION && SevenSigns.getInstance().getPlayerCabal(player) == SevenSigns.CABAL_NULL)
				return;

			if(SevenSigns.getInstance().getCurrentPeriod() == SevenSigns.PERIOD_SEAL_VALIDATION)
			{
				if(type == 1 && SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_AVARICE) != SevenSigns.getInstance().getPlayerCabal(player))
					return;
				if(type == 2 && SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_GNOSIS) != SevenSigns.getInstance().getPlayerCabal(player))
					return;
			}
		}

		player.teleToLocation(Integer.parseInt(param[0]), Integer.parseInt(param[1]), Integer.parseInt(param[2]));
	}

	/**
	 * Перемещает за определенный предмет
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param count
	 * @param item
	 */
	public void QuestGatekeeper(final String[] param)
	{
		if(param.length < 5)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();

		if(player.isTerritoryFlagEquipped())
		{
			player.sendPacket(Msg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
			return;
		}

		if(player.isActionsDisabled() || player.isSitting() || player.getLastNpc().getDistance(player) > 300)
			return;

		final int count = Integer.parseInt(param[3]);
		final int item_id = Integer.parseInt(param[4]);
		if(count > 0 && item_id > 0)
		{
			final long itemCount = getItemCount(player, item_id);
			if(itemCount == 0 || itemCount < count)
			{
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_REQUIRED_ITEMS);
				return;
			}

			removeItem(player, item_id, count);
		}

		final int x = Integer.parseInt(param[0]);
		final int y = Integer.parseInt(param[1]);
		final int z = Integer.parseInt(param[2]);

		final Location pos = GeoEngine.findPointToStay(x, y, z, 20, 70);
		player.teleToLocation(pos);
	}

	public void ReflectionGatekeeper(final String[] param)
	{
		if(param.length < 5)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		player.setReflection(Integer.parseInt(param[4]));

		Gatekeeper(param);
	}

	/**
	 * Используется для телепортации за Newbie Token, проверяет уровень и передает
	 * параметры в QuestGatekeeper
	 */
	public void TokenJump(final String[] param)
	{
		final L2Player player = (L2Player) getSelf();
		if(player.getLevel() <= 19)
			QuestGatekeeper(param);
		else
			show("data/html/default/newbie_teleport_over.htm", player);
	}

	public void NoblessTeleport()
	{
		final L2Player player = (L2Player) getSelf();
		if(player.isNoble() || Config.ALLOW_NOBLE_TP_TO_ALL)
			show("data/scripts/noble.htm", player);
		else
			show("data/scripts/nobleteleporter-no.htm", player);
	}

	public void TakeNewbieWeaponCoupon()
	{
		final L2Player player = (L2Player) getSelf();
		if(player.getLevel() <= 19 && player.getLevel() >= 6 && player.getPkKills() == 0 && player.getClassId().getLevel() == 1)
		{
			if(!player.getVarB("adventurer_coupon1"))
			{
				player.getInventory().addItem(7832, 1, 0, "L2NewbieGuide");
				player.getInventory().addItem(7832, 1, 0, "L2NewbieGuide");
				player.getInventory().addItem(7832, 1, 0, "L2NewbieGuide");
				player.getInventory().addItem(7832, 1, 0, "L2NewbieGuide");
				player.getInventory().addItem(7832, 1, 0, "L2NewbieGuide");
				player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S1).addItemName(7832));
				show("data/html/default/30598-22.htm", player);
				player.setVar("adventurer_coupon1", "1");
			}
			else
				show("data/html/default/30598-21.htm", player);
		}
		else
			show("data/html/default/30598-23.htm", player);
	}

	public void TakeAdventurersArmorCoupon()
	{
		final L2Player player = (L2Player) getSelf();
		if(player.getLevel() <= 39 && player.getLevel() >= 20 && player.getPkKills() == 0 && player.getClassId().getLevel() == 2)
		{
			if(!player.getVarB("adventurer_coupon2"))
			{
				player.getInventory().addItem(7833, 1, 0, "L2NewbieGuide");
				player.sendPacket(new SystemMessage(SystemMessage.YOU_HAVE_OBTAINED_S1).addItemName(7833));
				show("data/html/default/30598-25.htm", player);
				player.setVar("adventurer_coupon2", "1");
			}
			else
				show("data/html/default/30598-24.htm", player);
		}
		else
			show("data/html/default/30598-26.htm", player);
	}

	public void PayPage(final String[] param)
	{
		if(param.length < 2)
			throw new IllegalArgumentException();

		final L2Player player = (L2Player) getSelf();
		final String page = param[0];
		final int item = Integer.parseInt(param[1]);
		final int price = Integer.parseInt(param[2]);

		if(getItemCount(player, item) < price)
		{
			if(item == 57)
				player.sendPacket(Msg.YOU_DO_NOT_HAVE_ENOUGH_ADENA);
			else
				player.sendPacket(Msg.INCORRECT_ITEM_COUNT);
			return;
		}

		removeItem(player, item, price);
		show(page, player);
	}

	public static void CheckPlayerInEpicZone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(enter && object.isPlayable() && ((L2Playable) object).getLevel() > 48 && object.isInZone(zone))
		{
			object.getEffectList().stopEffect(L2Skill.SKILL_MYSTIC_IMMUNITY);
			SkillTable.FrequentSkill.RAID_CURSE2.getSkill().getEffects(object, object, false, false);
			L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
			{
				@Override
				public void run()
				{
					Functions.callScripts("Util", "CheckPlayerInEpicZone", new Object[] { zone, object, enter });
				}
			}, 120000);
		}
	}

	/**
	 * Проверяется при входи и выходе из зоны
	 * 
	 * @param zone
	 * @param object
	 * @param enter
	 */
	public static void CheckPlayerInMultilayerZone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(object.isPlayable() || object.isMonster())
			if(enter && object.getLevel() < 76 && object.isInZone(zone))
				object.setReflection(ReflectionTable.MULTILAYER);
			else if(object.getReflection().getId() == ReflectionTable.MULTILAYER)
				object.setReflection(ReflectionTable.DEFAULT);
	}

	public static void CheckPlayerInTully1Zone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(!enter || !object.isPlayer() || !object.isInZone(zone))
			return;

		final L2Player p = object.getPlayer();

		final int MASTER_ZELOS_ID = 22377;
		boolean teleport = true;

		for(final L2NpcInstance npc : p.getAroundNpc(3000, 256))
			if(npc.getNpcId() == MASTER_ZELOS_ID && !npc.isDead())
				teleport = false;

		if(teleport)
			p.teleToLocation(TullyWorkshopManager.TullyFloor2LocationPoint);
	}

	public static void CheckPlayerInTully2Zone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(!enter || !object.isPlayer() || !object.isInZone(zone))
			return;

		final L2Player p = object.getPlayer();

		final int MASTER_FESTINA_ID = 22380;
		boolean teleport = true;

		for(final L2NpcInstance npc : p.getAroundNpc(3000, 500))
			if(npc.getNpcId() == MASTER_FESTINA_ID && !npc.isDead())
				teleport = false;

		if(teleport)
			p.teleToLocation(TullyWorkshopManager.TullyFloor4LocationPoint);
	}

	public static void CheckPlayerInTully4Zone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(!enter || !object.isPlayer() || !object.isInZone(zone))
			return;

		final L2Player p = object.getPlayer();
		p.teleToLocation(TullyWorkshopManager.TullyFloor5LocationPoint);
	}

	public static void CheckInfinitum5Zone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(enter && object.isPlayer() && object.isInZone(zone))
		{
			final L2Player p = object.getPlayer();

			boolean teleport = true;
			for(final L2NpcInstance npc : p.getAroundNpc(3000, 500))
				if(npc.getNpcId() == 25540 && !npc.isDead()) // ищем Demon Prince
					teleport = false;

			if(teleport)
				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						if(p != null && zone != null && p.isInZone(zone))
							p.teleToLocation(-19017, 277056, -8256);
					}
				}, 60000);
		}
	}

	public static void CheckInfinitum10Zone(final L2Zone zone, final L2Character object, final Boolean enter)
	{
		if(enter && object.isPlayer() && object.isInZone(zone))
		{
			final L2Player p = object.getPlayer();

			boolean teleport = true;
			for(final L2NpcInstance npc : p.getAroundNpc(3000, 500))
				if(npc.getNpcId() == 25542 && !npc.isDead()) // ищем Ranku
					teleport = false;

			if(teleport)
				L2GameThreadPools.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						if(p != null && zone != null && p.isInZone(zone))
							p.teleToLocation(14602, 283179, -7500);
					}
				}, 60000);
		}
	}
}
