SET NAMES 'utf8';
SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for online
-- ----------------------------
DROP TABLE IF EXISTS `online`;
CREATE TABLE `online` (
  `id` int(1) NOT NULL,
  `totalOnline` int(6) NOT NULL,
  `totalOffline` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `online` VALUES ('0', '0', '0');
