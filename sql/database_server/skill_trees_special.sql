DROP TABLE IF EXISTS `skill_trees_special`;
SET NAMES 'utf8';
CREATE TABLE IF NOT EXISTS `skill_trees_special` (
  `skill_id` int(10) NOT NULL default '0',
  `level` int(10) NOT NULL default '0',
  `name` varchar(25) NOT NULL default '',
  `costid` int(10) NOT NULL default '0',
  `cost` int(10) NOT NULL default '0',
  PRIMARY KEY (`skill_id`,`level`)
) ENGINE = MYISAM DEFAULT CHARSET=utf8;

INSERT INTO `skill_trees_special` VALUES
(932,1,'Star Stone',13728,1),
(932,2,'Star Stone',57,400000),
(932,3,'Star Stone',57,1200000),
(5780,1,'Agathion Collection',57,150000),
(5780,2,'Agathion Collection',57,400000),
(5780,3,'Agathion Collection',57,1200000);