@echo off
TITLE L2System ��⠭���� ��� ������
REM ########################################
goto answer%ERRORLEVEL%
:answerTrue
set fastend=yes
goto upgrade_db
:answer0
set fastend=no
REM ================ Configuration Connecting MSQL==============
REM user  MSQL
set user=root

REM Passowrd  MSQL 
set pass=root

REM BASE MSQL
set DBname=l2s

REM DBHOST MSQL
set DBHost=localhost
REM ================ Configuration Connecting MSQL==============

set Generaltables=accounts augmentations clanhall gameservers banned_ips loginserv_log character_friends character_hennas character_macroses character_quests character_recipebook character_shortcuts character_skills character_effects_save character_skills_save character_subclasses characters character_variables clanhall_bids clanhall_data clan_data clanhall_decorations_bids ally_data clan_wars items pets server_variables seven_signs seven_signs_festival siege_clans killcount dropcount craftcount game_log petitions seven_signs_status global_tasks raidboss_status
set Ignore=--ignore-table=%DBname%.game_log --ignore-table=%DBname%.loginserv_log --ignore-table=%DBname%.petitions

REM ########################################
mysql.exe -h %DBHost% -u %user% --password=%pass% --execute="CREATE DATABASE IF NOT EXISTS %DBname%"
if not exist backup (
mkdir backup
)

REM ####################################### :main_menu
:main_menu
cls
echo.   L2System ������� ���� ��⠭���� ��� ������ �ࢥ�
echo.
echo.	#############################################
echo.	# ������� ���� ��⠭���� ��� ������ �ࢥ� #
echo.	#############################################
echo.
echo.   (0) ��⠭����� ⠡���� ����� ��ࢥ�
echo.   (1) �������� ⠡���� ����� ��ࢥ�
echo.   (2) ��⠭����� ⠡���� ���� ��ࢥ�
echo.   (3) �������� ⠡���� ���� ��ࢥ�
echo.   (4) ����� ⠡��� ��ࢥ�
echo.   (5) ���⮭������� ⠡��� ��ࢥ�
echo.   (6) ��᫥���� ��� ���� ������
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮� ?:
if /i %button%==0 goto Install_Login_Server_menu
if /i %button%==1 goto upgrade_login_db
if /i %button%==2 goto Install_Game_Server_menu
if /i %button%==3 goto upgrade_game_db
if /i %button%==4 goto backup_menu
if /i %button%==5 goto restore_menu
if /i %button%==6 goto lost_data_menu
if /i %button%==q goto end
goto main_menu

REM ######################################## :Install_Login_Server_menu
:Install_Login_Server_menu
cls
echo.   L2System ��⠭���� ������ ����� ��ࢥ�
echo.
echo.   ###################################
echo.   # ��⠭���� ������ ����� ��ࢥ�  #
echo.   ###################################
echo.
echo.   (i) ��⠭���� ������ ����� ��ࢥ�.
echo.       ��������:(�������) accounts, gameservers, banned_ips !���� �������!
echo.   (m) ������� ����
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮�  ?:
if /i %button%==i goto Install_Login_Server
if /i %button%==m goto main_menu
if /i %button%==q goto end
goto Install_Login_Server_menu

REM ######################################## :Install_Game_Server_menu
:Install_Game_Server_menu
cls
echo.   L2System ��⠭���� ������ ���� ��ࢥ�
echo.
echo.   ################################
echo.   # ��⠭���� ������ ���� ��ࢥ�  #
echo.   ################################
echo.
echo.   (i) ��⠭���� ������ ���� ��ࢥ�.
echo.       ��������: ��� ���� ���� �ࢥ� �㤥� �������!
echo.   (m) ������� ����
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮�  ?:
if /i %button%==i goto Install_Game_Server
if /i %button%==m goto main_menu
if /i %button%==q goto end
goto Install_Game_Server_menu

REM ######################################## :upgrade_game_db
:upgrade_game_db
cls
echo.   L2System ���������� ������ ���� ��ࢥ�
echo.
echo.   ####################################
echo.   #  ���������� ������ ���� ��ࢥ�  #
echo.   ####################################
echo.
echo.   (u) �������� ������ ���� ��ࢥ�
echo.   (m) ������� ����
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮�  ?:
if /i %button%==u goto upgrade_db
if /i %button%==m goto main_menu
if /i %button%==q goto end
goto upgrade_game_db

REM ######################################## :upgrade_login_db
:upgrade_login_db
cls
echo.   L2System ���������� ������ ����� ��ࢥ�
echo.
echo.   #####################################
echo.   #  ���������� ������ ����� ��ࢥ�  #
echo.   #####################################
echo.
echo.   (u) ���������� ������ ����� ��ࢥ�
echo.   (m) ������� ����
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮� ?:
if /i %button%==u goto upgradelogin_db
if /i %button%==m goto main_menu
if /i %button%==q goto end
goto upgrade_login_db

REM ######################################## :backup_menu
:backup_menu
cls
echo.   L2System ����� ⠡��� ��ࢥ�
echo.
echo.   ############################
echo.   #   ����� ⠡��� ��ࢥ�   #
echo.   ############################
echo.
echo.   (1) ����� ����� 
echo.   (2) ���쪮 ������ ⠡����
echo.   (m) �������� ����
echo.   (q) ��室
echo.
set button=x
set /p button=   ������ ��� �롮�  ?:
if /i %button%==1 goto full_backup
if /i %button%==2 goto general_backup
if /i %button%==m goto setup
if /i %button%==q goto end
goto backup_menu

REM ######################################## :restore_menu
:restore_menu
cls
echo.List all files in dir "/backup" !
echo.
dir backup /B /P
echo.
echo.   L2System ���⮭������� ⠡��� ��ࢥ�
echo.
echo.   ####################################
echo.   #   ���⮭������� ⠡��� ��ࢥ�   #
echo.   ####################################
echo.
echo.   Enter a full filename do you want to restore to the database !
echo.   (m) Main menu
echo.   (q) Quit
echo.
set filename=x
set /p filename=   Enter filename?:
if /i %filename%==m goto main_menu
if /i %filename%==q goto end
if /i %filename%==%filename% goto restore_DB
goto restore_menu

REM ######################################## :lost_data_menu
:lost_data_menu
cls
echo.   L2System Setup
echo.
echo.   ######################
echo.   #   Lost Data Menu   #
echo.   ######################
echo.
echo.   (1) Show lost data
echo.   (2) Delete lost data
echo.   (m) Main menu
echo.   (q) Quit
echo.
set button=x
set /p button=   Select backup type?:
if /i %button%==1 goto show_lost_data
if /i %button%==2 goto delete_lost_data
if /i %button%==m goto main_menu
if /i %button%==q goto end
goto lost_data_menu

:show_lost_data
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < tools/maintenance/lost_data_show.sql
pause
goto lost_data_menu

:delete_lost_data
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < tools/maintenance/lost_data_del.sql
echo.
echo.   ::. All lost data deleted!
echo.
pause
goto lost_data_menu

REM ######################################## :Install_Login_Server
:Install_Login_Server
set ctime=%TIME:~0,2%
if "%ctime:~0,1%" == " " (
set ctime=0%ctime:~1,1%
)
set ctime=%ctime%'%TIME:~3,2%'%TIME:~6,2%
echo.
echo.   ::. Making a full backup into %DATE%-%ctime%_backup_full.sql
echo.
mysqldump.exe %Ignore% --add-drop-table -h %DBHost% -u %user% --password=%pass% %DBname% > backup/%DATE%-%ctime%_backup_full.sql
echo.
echo.   ::. ��⠭���� ⠡��� ����� ��ࢥ� .:: 
echo.
echo.    :.��⠭���� ⠡���� accounts
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_login/accounts.sql
echo.    :.��⠭���� ⠡���� gameservers
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_login/gameservers.sql
echo.    :.��⠭���� ⠡���� banned_ips
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_login/banned_ips.sql
echo.    :.��⠭���� ⠡���� loginserv_log
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_login/loginserv_log.sql
echo.
echo.   :. ��⠭���� ������ ����� ��ࢥ� ����祭� . 
echo.
pause
goto main_menu

REM ######################################## :Install_Game_Server
:Install_Game_Server
set ctime=%TIME:~0,2%
if "%ctime:~0,1%" == " " (
set ctime=0%ctime:~1,1%
)
set ctime=%ctime%'%TIME:~3,2%'%TIME:~6,2%
echo.
echo.   ::. Making a full backup into %DATE%-%ctime%_backup_full.sql
echo.
mysqldump.exe %Ignore% --add-drop-table -h %DBHost% -u %user% --password=%pass% %DBname% > backup/%DATE%-%ctime%_backup_full.sql
echo.
echo.   ::. ��⠭���� ������ ���� ��ࢥ� . 
echo.
echo.    :.��⠭���� ⠡���� castle
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/castle.sql
echo.    :.��⠭���� ⠡���� siege_doorupgrade
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/siege_doorupgrade.sql
echo.    :.��⠭���� ⠡���� siege_territory_members
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/siege_territory_members.sql
echo.    :.��⠭���� ⠡���� community
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/community_buffs_group_skills.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/community_buffs_group.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/community_teleport.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/community_clanbonus.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/community_bonuscolor.sql
echo.    :.��⠭���� ⠡���� character_friends
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_friends.sql
echo.    :.��⠭���� ⠡���� character_blocklist
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_blocklist.sql
echo.    :.��⠭���� ⠡���� character_hennas
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_hennas.sql
echo.    :.��⠭���� ⠡���� character_macroses
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_macroses.sql
echo.    :.��⠭���� ⠡���� character_quests
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_quests.sql
echo.    :.��⠭���� ⠡���� character_recipebook
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_recipebook.sql
echo.    :.��⠭���� ⠡���� character_shortcuts
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_shortcuts.sql
echo.    :.��⠭���� ⠡���� character_skills
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_skills.sql
echo.    :.��⠭���� ⠡���� character_effects_save
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_effects_save.sql
echo.    :.��⠭���� ⠡���� character_skills_save
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_skills_save.sql
echo.    :.��⠭���� ⠡���� character_subclasses
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_subclasses.sql
echo.    :.��⠭���� ⠡���� character_tpbookmark
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_tpbookmark.sql
echo.    :.��⠭���� ⠡���� character_mail
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_mail.sql
echo.    :.��⠭���� ⠡���� characters
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/characters.sql
echo.    :.��⠭���� ⠡���� character_variables
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_variables.sql
echo.    :.��⠭���� ⠡���� character_votes
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_votes_l2top.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/character_votes_mmotop.sql
echo.    :.��⠭���� ⠡���� clan_data
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clan_data.sql
echo.    :.��⠭���� ⠡���� cursed_weapons
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/cursed_weapons.sql
echo.    :.��⠭���� ⠡���� ally_data
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/ally_data.sql
echo.    :.��⠭���� ⠡���� clan_wars
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clan_wars.sql
echo.    :.��⠭���� ⠡���� clan_privs
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clan_privs.sql
echo.    :.��⠭���� ⠡���� clan_skills
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clan_skills.sql
echo.    :.��⠭���� ⠡���� clan_subpledges
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clan_subpledges.sql
echo.    :.��⠭���� ⠡���� epic_boss_spawn
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/epic_boss_spawn.sql
echo.    :.��⠭���� ⠡���� items
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/items.sql
echo.    :.��⠭���� ⠡���� augmentations
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/augmentations.sql
echo.    :.��⠭���� ⠡���� items_delayed
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/items_delayed.sql
echo.    :.��⠭���� ⠡���� pets
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/pets.sql
echo.    :.��⠭���� ⠡���� server_variables
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/server_variables.sql
echo.    :.��⠭���� ⠡���� seven_signs
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/seven_signs.sql
echo.    :.��⠭���� ⠡���� seven_signs_festival
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/seven_signs_festival.sql
echo.    :.��⠭���� ⠡���� siege_clans
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/siege_clans.sql
echo.    :.��⠭���� ⠡���� summon_effects_save
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/summon_effects_save.sql
echo.    :.��⠭���� ⠡���� killcount
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/killcount.sql
echo.    :.��⠭���� ⠡���� lottery
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/lottery.sql
echo.    :.��⠭���� ⠡���� dropcount
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/dropcount.sql
echo.    :.��⠭���� ⠡���� craftcount
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/craftcount.sql
echo.    :.��⠭���� ⠡���� game_log
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/game_log.sql
echo.    :.��⠭���� ⠡���� petitions
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/petitions.sql
echo.    :.��⠭���� ⠡���� seven_signs_status
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/seven_signs_status.sql
echo.    :.��⠭���� ⠡���� global_tasks
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/global_tasks.sql
echo.    :.��⠭���� ⠡���� gracia_seeds
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/gracia_seeds.sql
echo.    :.��⠭���� ⠡���� raidboss_points
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/raidboss_points.sql
echo.    :.��⠭���� ⠡���� raidboss_status
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/raidboss_status.sql
echo.    :.��⠭���� ⠡���� forts
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/forts.sql
echo.    :.��⠭���� ⠡���� forums
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/forums.sql
echo.    :.��⠭���� ⠡���� posts
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/posts.sql
echo.    :.��⠭���� ⠡���� topic
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/topic.sql
echo.    :.��⠭���� ⠡���� mail
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/mail.sql
echo.    :.��⠭���� ⠡���� bans
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/bans.sql
echo.    :.��⠭���� ⠡���� bonus
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/bonus.sql
echo.    :.��⠭���� ⠡���� auction
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/auction.sql
echo.    :.��⠭���� ⠡���� l2_auction
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/l2_auction.sql
echo.    :.��⠭���� ⠡���� auction_bid
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/auction_bid.sql
echo.    :.��⠭���� ⠡���� clanhall
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/clanhall.sql
echo.    :.��⠭���� ⠡���� clanhall_functions
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/residence_functions.sql
echo.    :.��⠭���� ⠡���� couples
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/couples.sql
echo.    :.��⠭���� ⠡���� olympiad_nobles
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/olympiad_nobles.sql
echo.    :.��⠭���� ⠡���� castle_manor_procure
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/castle_manor_procure.sql
echo.    :.��⠭���� ⠡���� castle_manor_production
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/castle_manor_production.sql
echo.    :.��⠭���� ⠡���� tournament_table
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/tournament_table.sql
echo.    :.��⠭���� ⠡���� tournament_teams
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/tournament_teams.sql
echo.    :.��⠭���� ⠡���� tournament_variables
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/tournament_variables.sql
echo.    :.��⠭���� ⠡���� passkey
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/passkey.sql
echo.    :.��⠭���� ⠡���� heroes
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/heroes.sql
echo.    :.��⠭���� ⠡��� HWID
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < rGuard/hwid_bans.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < rGuard/hwid_info.sql
echo.    :.��⠭���� ⠡���� Item Auction
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/item_auction.sql
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < database_user/item_auction_bid.sql
echo.    :.��⠭���� ������ ���� ��ࢥ� ����祭� .:
goto upgrade_db

REM ######################################## :upgrade_db
:upgrade_db
echo.
echo.    ::. ��⠭���� ���������� ������ ���� ��ࢥ� .::
echo.
set action=    :. ���������� �������
for %%i in (	database_server/ai_params.sql
		database_server/armor.sql
		database_server/armor_ex.sql
		database_server/armorsets.sql
		database_server/auto_chat.sql
		database_server/auto_chat_text.sql
		database_server/char_templates.sql
		database_server/class_list.sql
		database_server/dimensional_rift.sql
		database_server/doors.sql
		database_server/droplist.sql
		database_server/etcitem.sql
		database_server/fish.sql
		database_server/fishreward.sql
		database_server/four_sepulchers_spawnlist.sql
		database_server/henna.sql
		database_server/henna_trees.sql
		database_server/lastimperialtomb_spawnlist.sql
		database_server/locations.sql
		database_server/lvlupgain.sql
		database_server/mapregion.sql
		database_server/merchant_areas_list.sql
		database_server/minions.sql
		database_server/npc.sql
		database_server/npc_elementals.sql
		database_server/npcskills.sql
		database_server/pet_data.sql
		database_server/pets_skills.sql
		database_server/random_spawn.sql
		database_server/random_spawn_loc.sql
		database_server/siege_door.sql
		database_server/siege_guards.sql
		database_server/skill_learn.sql
		database_server/skill_spellbooks.sql
		database_server/skill_trees_enchant.sql
		database_server/skill_trees_fishing.sql
		database_server/skill_trees_player.sql
		database_server/skill_trees_pledge.sql
		database_server/skill_trees_special.sql
		database_server/skill_trees_squad.sql
		database_server/skill_trees_transform.sql
		database_server/skills.sql
		database_server/spawnlist.sql
		database_server/territory_spawnlist.sql
		database_server/tournament_class_list.sql
		database_server/vote.sql
		database_server/weapon.sql
		database_server/online.sql
		database_custom/festive_sweeper.sql
		database_custom/skill_trees_player.sql
		database_custom/our_updates_release.sql
		database_server/weapon_ex.sql ) do call :run_query %%i
echo.    :. ������� ��������� .
echo.

REM ########################################
echo.   :. ��⠭����  ���ᮢ ⠡��� .:
echo.
set action=    :. ���� ⠡���
for %%i in (	updates/update_gameserver.sql
		 ) do call :run_query_null %%i
echo.   :. ����� ��⠭������ .
echo.
if /I %fastend%==yes goto :EOF
pause
goto main_menu

REM ########################################
:run_query
echo %action% %~nx1
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < %1
goto :eof

REM ########################################
:run_query_null
echo %action% %~nx1
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < %1 2>>NUL
goto :eof

REM ######################################## :upgradelogin_db
:upgradelogin_db
echo.
echo.   :. ��⠭���� ���������� ����� ��ࢥ� .
echo.
set action=    :. Upgrade
for %%i in (	updates/update_loginserver.sql ) do call :run_query %%i
echo.   :. ��⠭���� ���������� ����� ��ࢥ� �����襭� .
echo.
pause
goto main_menu

REM ######################################## :full_backup
:full_backup
set ctime=%TIME:~0,2%
if "%ctime:~0,1%" == " " (
set ctime=0%ctime:~1,1%
)
set ctime=%ctime%'%TIME:~3,2%'%TIME:~6,2%
echo.
echo.   ::. Making a full backup into %DATE%-%ctime%_backup_full.sql
echo.
mysqldump.exe %Ignore% --add-drop-table -h %DBHost% -u %user% --password=%pass% %DBname% > backup/%DATE%-%ctime%_backup_full.sql
goto end

REM ######################################## :general_backup
:general_backup
set ctime=%TIME:~0,2%
if "%ctime:~0,1%" == " " (
set ctime=0%ctime:~1,1%
)
set ctime=%ctime%'%TIME:~3,2%'%TIME:~6,2%
echo.
echo.   ::. Making a general tables backup into %DATE%-%ctime%_backup_general.sql
echo.
mysqldump.exe %Ignore% --add-drop-table -h %DBHost% -u %user% --password=%pass% %DBname% %Generaltables% > backup/%DATE%-%ctime%_backup_general.sql
goto end

REM ######################################## :restore_DB
:restore_DB
if not exist backup/%filename% (
echo.
echo.   ::. File not found !
echo.
pause
goto restore_menu
)
cls
echo.
echo.Restore from file %filename% !
echo.
pause
mysql.exe -h %DBHost% -u %user% --password=%pass% -D %DBname% < backup/%filename%
goto end

REM ######################################## :not_working_now
:not_working_now
echo.
echo Not working NOW !!!
echo.
pause
goto main_menu

REM ######################################## :end
:end
