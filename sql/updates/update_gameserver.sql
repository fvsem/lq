UPDATE `characters` set `createtime` = FLOOR(UNIX_TIMESTAMP() - 365 * 24 * 60 * 60 + (RAND() * (UNIX_TIMESTAMP() - (UNIX_TIMESTAMP() - 365 * 24 * 60 * 60))))  WHERE `createtime` = 0;
UPDATE `items` `i`
LEFT JOIN `armor` `a` ON `a`.`item_id`=`i`.`item_id`
SET `i`.`shadow_life_time` = UNIX_TIMESTAMP()+`a`.`durability` * 60
WHERE `a`.`temporal`=1 AND `i`.`shadow_life_time`<=0;
UPDATE `items` `i`
LEFT JOIN `weapon` `a` ON `a`.`item_id`=`i`.`item_id`
SET `i`.`shadow_life_time` = UNIX_TIMESTAMP()+`a`.`durability` * 60
WHERE `a`.`temporal`=1 AND `i`.`shadow_life_time`<=0;
UPDATE `items` `i`
LEFT JOIN `etcitem` `a` ON `a`.`item_id`=`i`.`item_id`
SET `i`.`shadow_life_time` = UNIX_TIMESTAMP()+`a`.`durability` * 60
WHERE `a`.`temporal`=1 AND `i`.`shadow_life_time`<=0;
UPDATE items SET loc = 'WAREHOUSE' WHERE loc = 'FREIGHT';
CREATE TABLE IF NOT EXISTS `clan_notices` (
  `clanID` INT(32) NOT NULL,
  `notice` VARCHAR(512) NOT NULL,
  `enabled` VARCHAR(5) NOT NULL,
  PRIMARY KEY (`clanID`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

ALTER TABLE `clanhall` CHANGE `price` `price` bigint(20) DEFAULT '0' NOT NULL;
CREATE TABLE IF NOT EXISTS `gracia_seeds` (
  `var`  VARCHAR(25) NOT NULL DEFAULT '',
  `value` VARCHAR(255) ,
  PRIMARY KEY (`var`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
INSERT IGNORE INTO `gracia_seeds` VALUES
('tiatKilled','0'),
('state','1'),
('lastStateChangeDate','0');
CREATE TABLE IF NOT EXISTS `community_buffs_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) character set utf8 NOT NULL default '',
  `object_id` int(11) NOT NULL DEFAULT 0,
  INDEX (`object_id`),
  PRIMARY KEY (`group_id`, `object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `community_buffs_group_skills` (
  `group_id` int(11) NOT NULL DEFAULT 0,
  `object_id` int(11) NOT NULL DEFAULT 0,
  `skill_id` mediumint unsigned NOT NULL DEFAULT 0,
  `skill_level` tinyint unsigned NOT NULL DEFAULT 0,
 INDEX (`group_id`, `object_id`),
 UNIQUE KEY (`group_id`, `object_id`, `skill_id`, `skill_level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `community_board_teleport` (
  `TpId` int(11) NOT NULL AUTO_INCREMENT,
  `charId` int(11) DEFAULT NULL,
  `Xpos` int(9) NOT NULL default '0', 
  `Ypos` int(9) NOT NULL default '0', 
  `Zpos` int(9) NOT NULL default '0', 
  `name` varchar(250) NOT NULL default '', 
  PRIMARY KEY (`TpId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `summon_effects_save` (
  `char_obj_id` int(11) NOT NULL default '0',
  `npc_id` SMALLINT(5) UNSIGNED NOT NULL default '0',
  `skill_id` mediumint unsigned  NOT NULL default '0',
  `skill_level` tinyint unsigned NOT NULL default '0',
  `effect_count` tinyint unsigned NOT NULL default '0',
  `effect_cur_time` int NOT NULL default '0',
  `duration` int NOT NULL default '0',
  `order` tinyint NOT NULL default '0',
  PRIMARY KEY (`char_obj_id`,`npc_id`,`skill_id`)
) ENGINE=MyISAM;
DROP TABLE IF EXISTS `community_buffs_fixed`;
DELETE FROM `character_quests` WHERE `name` = "_310_OnlyWhatRemains" AND `value` = "Completed";
UPDATE character_quests SET `name` = "_146_TheZeroHour" WHERE `name` = "_640_TheZeroHour";
SET @ALLOW = (SELECT COUNT(*) FROM server_variables WHERE `name` = "HellboundConfidence" > 0);
REPLACE INTO server_variables (`name`, `value`) VALUES ("HellboundOpen",  @ALLOW);
UPDATE server_variables SET `value` = "false" WHERE `name` = "HellboundOpen" AND `value` = 0;
UPDATE server_variables SET `value` = "true" WHERE `name` = "HellboundOpen" AND `value` = 1;
UPDATE auction SET startingBid = '20000000' WHERE id = '22';
UPDATE auction SET startingBid = '20000000' WHERE id = '23';
UPDATE auction SET startingBid = '20000000' WHERE id = '24';
UPDATE auction SET startingBid = '20000000' WHERE id = '25';
UPDATE auction SET startingBid = '20000000' WHERE id = '26';
UPDATE auction SET startingBid = '20000000' WHERE id = '27';
UPDATE auction SET startingBid = '20000000' WHERE id = '28';
UPDATE auction SET startingBid = '20000000' WHERE id = '29';
UPDATE auction SET startingBid = '20000000' WHERE id = '30';
UPDATE auction SET startingBid = '8000000' WHERE id = '31';
UPDATE auction SET startingBid = '8000000' WHERE id = '32';
UPDATE auction SET startingBid = '8000000' WHERE id = '33';
UPDATE auction SET startingBid = '50000000' WHERE id = '36';
UPDATE auction SET startingBid = '50000000' WHERE id = '37';
UPDATE auction SET startingBid = '50000000' WHERE id = '38';
UPDATE auction SET startingBid = '50000000' WHERE id = '39';
UPDATE auction SET startingBid = '50000000' WHERE id = '40';
UPDATE auction SET startingBid = '50000000' WHERE id = '41';
UPDATE auction SET startingBid = '50000000' WHERE id = '42';
UPDATE auction SET startingBid = '50000000' WHERE id = '43';
UPDATE auction SET startingBid = '50000000' WHERE id = '44';
UPDATE auction SET startingBid = '50000000' WHERE id = '45';
UPDATE auction SET startingBid = '50000000' WHERE id = '46';
UPDATE auction SET startingBid = '50000000' WHERE id = '47';
UPDATE auction SET startingBid = '50000000' WHERE id = '48';
UPDATE auction SET startingBid = '50000000' WHERE id = '49';
UPDATE auction SET startingBid = '50000000' WHERE id = '50';
UPDATE auction SET startingBid = '50000000' WHERE id = '51';
UPDATE auction SET startingBid = '50000000' WHERE id = '52';
UPDATE auction SET startingBid = '50000000' WHERE id = '53';
UPDATE auction SET startingBid = '50000000' WHERE id = '54';
UPDATE auction SET startingBid = '50000000' WHERE id = '55';
UPDATE auction SET startingBid = '50000000' WHERE id = '56';
UPDATE auction SET startingBid = '50000000' WHERE id = '57';
UPDATE auction SET startingBid = '50000000' WHERE id = '58';
UPDATE auction SET startingBid = '50000000' WHERE id = '59';
UPDATE auction SET startingBid = '50000000' WHERE id = '60';
UPDATE auction SET startingBid = '50000000' WHERE id = '61';

CREATE TABLE IF NOT EXISTS `character_buff_schemes` (
  `char_obj_id` INT(11) NOT NULL,
  `scheme_id` TINYINT(3) UNSIGNED NOT NULL,
  `scheme_name` varchar(20) character set utf8 NOT NULL default '',
  `skills` VARCHAR(1024) NOT NULL,
  PRIMARY KEY (`char_obj_id`,`scheme_id`)
) ENGINE=MYISAM;
CREATE TABLE IF NOT EXISTS `character_votes_mmotop` (
  `obj_Id` INT UNSIGNED NOT NULL DEFAULT 0,
  `char_name` VARCHAR(120) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `time` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`obj_Id`,`time`),
  KEY `obj_Id` (`obj_Id`),
  KEY `char_name` (`char_name`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `baby_pets_buffs`;

ALTER IGNORE TABLE `castle_manor_procure` CHANGE `can_buy` `can_buy` BIGINT(20) DEFAULT '0' NOT NULL, CHANGE `start_buy` `start_buy` BIGINT(20) DEFAULT '0' NOT NULL, CHANGE `price` `price` BIGINT(20) DEFAULT '0' NOT NULL; 
ALTER IGNORE TABLE `castle_manor_production` CHANGE `can_produce` `can_produce` BIGINT(20) DEFAULT '0' NOT NULL, CHANGE `start_produce` `start_produce` BIGINT(20) DEFAULT '0' NOT NULL, CHANGE `seed_price` `seed_price` BIGINT(20) DEFAULT '0' NOT NULL; 
ALTER TABLE `character_effects_save` CHANGE `skill_level` `skill_level` SMALLINT(6) UNSIGNED DEFAULT '0' NOT NULL;
ALTER TABLE `summon_effects_save` CHANGE `skill_level` `skill_level` SMALLINT(6) UNSIGNED DEFAULT '0' NOT NULL;
ALTER TABLE `seven_signs_festival` CHANGE `date` `date` BIGINT(50) DEFAULT '0' NULL , CHANGE `score` `score` MEDIUMINT(8) DEFAULT '0' NOT NULL;
ALTER TABLE `seven_signs_status` CHANGE `dawn_stone_score` `dawn_stone_score` BIGINT DEFAULT '0' NOT NULL, CHANGE `dawn_festival_score` `dawn_festival_score` BIGINT DEFAULT '0' NOT NULL, CHANGE `dusk_stone_score` `dusk_stone_score` BIGINT DEFAULT '0' NOT NULL, CHANGE `dusk_festival_score` `dusk_festival_score` BIGINT DEFAULT '0' NOT NULL, CHANGE `accumulated_bonus0` `accumulated_bonus0` BIGINT DEFAULT '0' NOT NULL, CHANGE `accumulated_bonus1` `accumulated_bonus1` BIGINT DEFAULT '0' NOT NULL, CHANGE `accumulated_bonus2` `accumulated_bonus2` BIGINT DEFAULT '0' NOT NULL, CHANGE `accumulated_bonus3` `accumulated_bonus3` BIGINT DEFAULT '0' NOT NULL, CHANGE `accumulated_bonus4` `accumulated_bonus4` BIGINT DEFAULT '0' NOT NULL;
CREATE TABLE IF NOT EXISTS `hwid_bans` (
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hwid` VARCHAR(32) NOT NULL DEFAULT '',
  `comment` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY  (`hwid`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `hwid_bonus` (
  `hwid` VARCHAR(32) NOT NULL DEFAULT '',
  `type` VARCHAR(255) NOT NULL DEFAULT '',
  `value` INT(10) DEFAULT NULL,
  `time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY  (`hwid`,`type`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `hwids_log` (
  `server_id` INT(10) NOT NULL,
  `time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account` VARCHAR(16) NOT NULL,
  `ip` VARCHAR(16) NOT NULL,
  `hwid` VARCHAR(32) NOT NULL,
  PRIMARY KEY  (`server_id`,`time`,`account`),
  KEY `account` (`account`),
  KEY `ip` (`ip`),
  KEY `hwid` (`hwid`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
ALTER TABLE `character_skills_save` CHANGE `class_index` `class_index` TINYINT(3) UNSIGNED NOT NULL, CHANGE `end_time` `end_time` BIGINT(13) UNSIGNED NOT NULL;
ALTER TABLE `character_variables` CHANGE `value` `value` VARCHAR(1024) DEFAULT '0' NOT NULL;
INSERT IGNORE INTO `epic_boss_spawn`(`bossId`,`respawnDate`,`state`) VALUES ( '25609','0','0');
INSERT IGNORE INTO `epic_boss_spawn`(`bossId`,`respawnDate`,`state`) VALUES ( '29118','0','0');
UPDATE `epic_boss_spawn` SET `name` = (SELECT `name` FROM `npc` WHERE `id` = `bossId`);
DROP TABLE IF EXISTS `manor_crop`;
DROP TABLE IF EXISTS `manor_seeds`;
CREATE TABLE IF NOT EXISTS `item_auction` (
  `auctionId` int(11) NOT NULL,
  `instanceId` int(11) NOT NULL,
  `auctionItemId` int(11) NOT NULL,
  `startingTime` bigint(20) NOT NULL,
  `endingTime` bigint(20) NOT NULL,
  `auctionStateId` tinyint(1) NOT NULL,
  PRIMARY KEY (`auctionId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `item_auction_bid` (
  `auctionId` int(11) NOT NULL,
  `playerObjId` int(11) NOT NULL,
  `playerBid` bigint(20) NOT NULL,
  PRIMARY KEY (`auctionId`,`playerObjId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
