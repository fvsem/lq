ALTER TABLE `accounts` CHANGE `password` `password` VARCHAR(256) DEFAULT '' NOT NULL;
ALTER TABLE `banned_ips` CHANGE `expiretime` `expiretime` BIGINT(20) UNSIGNED DEFAULT '0' NOT NULL;
