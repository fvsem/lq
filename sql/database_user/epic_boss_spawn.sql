DROP TABLE IF EXISTS `epic_boss_spawn`;
CREATE TABLE `epic_boss_spawn` (
  `bossId` smallint(5) unsigned NOT NULL,
  `respawnDate` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`bossId`)
) ENGINE=MyISAM;

INSERT IGNORE INTO `epic_boss_spawn` (`bossId`,`respawnDate`,`state`,`name`) VALUES
(29019,0,0,'Antharas'),
(29020,0,0,'Baium'),
(29028,0,0,'Valakas'),
(29045,0,0,'Frintezza'),
(29062,0,0,'Andreas Van Halter'),
(29065,0,0,'Sailren'),
(29099,0,0,'Baylor'),
(25609,0,0,'Epidos'),
(29118,0,0,'Beleth');