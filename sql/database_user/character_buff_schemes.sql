DROP TABLE IF EXISTS `character_buff_schemes`;
CREATE TABLE `character_buff_schemes` (
  `char_obj_id` INT(11) NOT NULL,
  `scheme_id` TINYINT(3) UNSIGNED NOT NULL,
  `scheme_name` varchar(20) character set utf8 NOT NULL default '',
  `skills` VARCHAR(1024) NOT NULL,
  PRIMARY KEY (`char_obj_id`,`scheme_id`)
) ENGINE=MYISAM;