DROP TABLE IF EXISTS `character_tpbookmark`;
CREATE TABLE `character_tpbookmark` (
  `char_id` INT(20) NOT NULL,
  `Id` INT(20) NOT NULL,
  `x` INT(20) NOT NULL,
  `y` INT(20) NOT NULL,
  `z` INT(20) NOT NULL,
  `icon` INT(20) NOT NULL,
  `tag` VARCHAR(20) DEFAULT NULL,
  `name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`char_id`,`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
