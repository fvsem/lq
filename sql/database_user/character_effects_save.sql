DROP TABLE IF EXISTS `character_effects_save`;
CREATE TABLE `character_effects_save` (
  `char_obj_id` int(11) NOT NULL DEFAULT '0',
  `skill_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `skill_level` smallint(6) NOT NULL DEFAULT 0,
  `effect_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `effect_cur_time` int(11) NOT NULL DEFAULT '0',
  `duration` int(11) NOT NULL DEFAULT '0',
  `order` tinyint(4) NOT NULL DEFAULT '0',
  `class_index` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`char_obj_id`,`skill_id`,`class_index`),
  KEY `order` (`order`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
