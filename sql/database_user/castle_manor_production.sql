DROP TABLE IF EXISTS `castle_manor_production`;
CREATE TABLE `castle_manor_production` (
  `castle_id` tinyint unsigned NOT NULL DEFAULT 0,
  `seed_id` smallint unsigned NOT NULL DEFAULT 0,
  `can_produce` int NOT NULL DEFAULT 0,
  `start_produce` int NOT NULL DEFAULT 0,
  `seed_price` int NOT NULL DEFAULT 0,
  `period` int NOT NULL DEFAULT 1,
  PRIMARY KEY  (`castle_id`,`seed_id`,`period`)
) ENGINE=MyISAM;
