
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `community_clanbonus`;
SET NAMES 'utf8';
CREATE TABLE `community_clanbonus` (
  `name` text NOT NULL,
  `players_count` int(11) DEFAULT NULL,
  `reward_lvl` int(11) DEFAULT NULL,
  `reward_rep` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`(16))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


