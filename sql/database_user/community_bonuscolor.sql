
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `community_bonuscolor`;
SET NAMES 'utf8';
CREATE TABLE `community_bonuscolor` (
  `name` text NOT NULL,
  PRIMARY KEY (`name`(16))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

