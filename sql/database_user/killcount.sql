DROP TABLE IF EXISTS `killcount`;
CREATE TABLE `killcount` (
  `char_id` INT(11) NOT NULL,
  `npc_id` SMALLINT(5) UNSIGNED NOT NULL,
  `count` INT(11) unsigned default '0',
  UNIQUE KEY `char_id` (`char_id`,`npc_id`),
  KEY `char_id_2` (`char_id`)
) ENGINE=MyISAM;
