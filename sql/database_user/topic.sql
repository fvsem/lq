DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `topic_id` int(11) NOT NULL DEFAULT '0',
  `topic_forum_id` int(11) NOT NULL DEFAULT '0',
  `topic_name` varchar(255) NOT NULL,
  `topic_date` bigint(20) NOT NULL DEFAULT '0',
  `topic_ownername` varchar(255) NOT NULL DEFAULT '0',
  `topic_ownerid` int(11) NOT NULL DEFAULT '0',
  `topic_type` int(11) NOT NULL DEFAULT '0',
  `topic_reply` int(11) NOT NULL DEFAULT '0',
  KEY `topic_forum_id` (`topic_forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;