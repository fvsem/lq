DROP TABLE IF EXISTS `character_macroses`;
CREATE TABLE `character_macroses` (
  `char_obj_id` INT NOT NULL DEFAULT '0',
  `id` INT NOT NULL DEFAULT '0',
  `icon` INT,
  `name` VARCHAR(40) character set utf8,
  `descr` VARCHAR(80) character set utf8,
  `acronym` VARCHAR(4) character set utf8,
  `commands` VARCHAR(1024) character set utf8,
  PRIMARY KEY  (`char_obj_id`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
