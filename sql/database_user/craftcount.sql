DROP TABLE IF EXISTS `craftcount`;
CREATE TABLE `craftcount` (
  `char_id` INT(10) UNSIGNED NOT NULL,
  `item_id` SMALLINT(5) UNSIGNED NOT NULL,
  `count` INT(14) UNSIGNED default '0',
  UNIQUE KEY `char_id` (`char_id`,`item_id`),
  KEY `char_id_2` (`char_id`)
) ENGINE=MyISAM;