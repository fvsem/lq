DROP TABLE IF EXISTS `hwid_bonus`;
CREATE TABLE `hwid_bonus` (
  `hwid` VARCHAR(32) NOT NULL DEFAULT '',
  `type` VARCHAR(255) NOT NULL DEFAULT '',
  `value` INT(10) DEFAULT NULL,
  `time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY  (`hwid`,`type`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;