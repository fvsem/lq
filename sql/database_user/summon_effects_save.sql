DROP TABLE IF EXISTS `summon_effects_save`;
CREATE TABLE `summon_effects_save` (
  `char_obj_id` int(11) NOT NULL default '0',
  `npc_id` SMALLINT(5) UNSIGNED NOT NULL default '0',
  `skill_id` mediumint unsigned  NOT NULL default '0',
  `skill_level` smallint(6) NOT NULL DEFAULT 0,
  `effect_count` tinyint unsigned NOT NULL default '0',
  `effect_cur_time` int NOT NULL default '0',
  `duration` int NOT NULL default '0',
  `order` tinyint NOT NULL default '0',
  PRIMARY KEY (`char_obj_id`,`npc_id`,`skill_id`)
) ENGINE=MyISAM;