DROP TABLE IF EXISTS `passkey`;
CREATE TABLE `passkey` (
  `obj_Id` int(10) unsigned NOT NULL DEFAULT '0',
  `passkey` varchar(256) DEFAULT NULL,
  `question` varchar(55) NOT NULL,
  `answer` varchar(35) NOT NULL,
  PRIMARY KEY (`obj_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;