DROP TABLE IF EXISTS `character_votes_mmotop`;
CREATE TABLE `character_votes_mmotop` (
  `obj_Id` INT UNSIGNED NOT NULL DEFAULT 0,
  `char_name` VARCHAR(120) CHARACTER SET cp1251 NOT NULL DEFAULT '',
  `time` INT(11) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`obj_Id`,`time`),
  KEY `obj_Id` (`obj_Id`),
  KEY `char_name` (`char_name`)
) ENGINE=MYISAM DEFAULT CHARSET=cp1251;