#!/bin/sh
##########################
## L2System Project     #
##########################

# при необходимости указать путь до mysqldump
export MYSQLDUMPPATH=`which mysqldump 2>/dev/null`

# каталог куда будут складываться бекапы (по умолчанию папка где запускается скрипт)
# export DB_BACKUP_PATH="./"`date +%Y-%m-%d`
export DB_BACKUP_PATH="."

# время бекапа, не трогать
export BACKUP_TIME=`date +%Y-%m-%d-%H-%M-%S`

# укажите кодировку если нужно
export DB_CHARACTER="utf8"

export DB_HOST=localhost
# указать пользователя, от которого будет идти бэкап
export DB_USER=root
# указать пароль этого пользователя
export DB_PASS=
# указать базу
export DB_NAME=l2s

zip_files()
{
	# архивируем все файлы
	tar -czf $DB_BACKUP_PATH/backup_$BACKUP_TIME.tar $DB_BACKUP_PATH/*.sql
	# удаляем
	rm $DB_BACKUP_PATH/*.sql
}

do_backup()
{
	for tab in \
		ally_data \
		auction \
		auction_bid \
		augmentations \
		bans \
		bonus \
		castle \
		castle_manor_procure \
		castle_manor_production \
		character_blocklist \
		character_buff_schemes \
		character_effects_save \
		character_friends \
		character_hennas \
		character_macroses \
		character_mail \
		character_quests \
		character_recipebook \
		character_shortcuts \
		character_skills \
		character_skills_save \
		character_subclasses \
		character_tpbookmark \
		character_variables \
		character_votes_l2top \
		character_votes_mmotop \
		characters \
		clan_data \
		clan_notices \
		clan_privs \
		clan_skills \
		clan_subpledges \
		clan_wars \
		clanhall \
		community_board_teleport \
		community_buffs_group \
		community_buffs_group_skills \
		couples \
		cursed_weapons \
		epic_boss_spawn \
		forts \
		forums \
		global_tasks \
		gracia_seeds \
		heroes \
		items \
		items_delayed \
		lottery \
		mail \
		mail_attachments \
		manor_crop \
		manor_seeds \
		olympiad_nobles \
		petitions \
		pets \
		posts \
		raidboss_points \
		raidboss_status \
		residence_functions \
		server_variables \
		seven_signs \
		seven_signs_festival \
		seven_signs_status \
		siege_clans \
		siege_doorupgrade \
		siege_territory_members \
		summon_effects_save \
		topic \
		tournament_table \
		tournament_teams \
		tournament_variables \
		dropcount \
		killcount \
		craftcount \
	; do
		echo backuping $tab...
		$CONNECTION $tab >$DB_BACKUP_PATH/$tab.sql
	done
}

# Clear console
clear

# Open MySQL connections
CONNECTION="$MYSQLDUMPPATH -h $DB_HOST -u $DB_USER --password=$DB_PASS -f --default-character-set=$DB_CHARACTER $DB_NAME"

# Call configure function
do_backup; zip_files;

