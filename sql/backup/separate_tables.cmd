@echo off


REM L2System Project

set user=root
set pass=
set DBname=l2s
set DBHost=localhost

for /f "delims=." %%i in ('wmic.exe OS get LocalDateTime ^| find "."') do set sDateTime=%%i
set BACKUP_PATH=backup_%sDateTime%

:backup
cls
echo.#######################
echo.#     Backup start    #
echo.#######################
md %BACKUP_PATH%
set action=backuping
for %%i in (	ally_data
		auction
		auction_bid
		augmentations
		bans
		bonus
		castle
		castle_manor_procure
		castle_manor_production
		character_blocklist
		character_buff_schemes
		character_effects_save
		character_friends
		character_hennas
		character_macroses
		character_mail
		character_quests
		character_recipebook
		character_shortcuts
		character_skills
		character_skills_save
		character_subclasses
		character_tpbookmark
		character_variables
		character_votes_l2top
		character_votes_mmotop
		characters
		clan_data
		clan_notices
		clan_privs
		clan_skills
		clan_subpledges
		clan_wars
		clanhall
		community_board_teleport
		community_buffs_group
		community_buffs_group_skills
		couples
		cursed_weapons
		epic_boss_spawn
		forts
		forums
		global_tasks
		gracia_seeds
		heroes
		items
		items_delayed
		lottery
		mail
		mail_attachments
		manor_crop
		manor_seeds
		olympiad_nobles
		petitions
		pets
		posts
		raidboss_points
		raidboss_status
		residence_functions
		server_variables
		seven_signs
		seven_signs_festival
		seven_signs_status
		siege_clans
		siege_doorupgrade
		siege_territory_members
		summon_effects_save
		topic
		tournament_table
		tournament_teams
		tournament_variables
		dropcount
		killcount
		craftcount) do call :run_query %%i
echo.#######################
echo.#   Backup finished   #
echo.#######################
echo.
goto end

:run_query
echo %action% %~nx1
mysqldump.exe -h %DBHost% -u %user% --password=%pass% %DBname% %1 > %BACKUP_PATH%\%1.sql
goto :eof

:end