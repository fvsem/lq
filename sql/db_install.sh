#!/bin/bash
##########################
## Based on L2System
##########################

# Configure the database access
configure()
{
	echo "#################################################"
	echo "#				Configuration area"
	echo "#			Please answer to the questions"
	echo "#################################################"
	MYSQLDUMPPATH=`which mysqldump 2>/dev/null`
	MYSQLPATH=`which mysql 2>/dev/null`
	if [ $? -ne 0 ]; then
		echo "Unable to find MySQL binaries on your PATH"
		while :
		do
			echo -ne "\nPlease enter MySQL binaries directory (no trailing slash): "
			read MYSQLBINPATH
			if [ -e "$MYSQLBINPATH" ] && [ -d "$MYSQLBINPATH" ] && [ -e "$MYSQLBINPATH/mysqldump" ] && [ -e "$MYSQLBINPATH/mysql" ]; then
				MYSQLDUMPPATH="$MYSQLBINPATH/mysqldump"
				MYSQLPATH="$MYSQLBINPATH/mysql"
				break
			else
				echo "Invalid data. Please verify and try again."
				exit 1
			fi
		done
	fi

	# LoginServer
	echo -ne "\nEnter MySQL LOGIN SERVER hostname (default localhost): "
	read LSDBHOST
	if [ -z "$LSDBHOST" ]; then
		LSDBHOST="localhost"
	fi
	echo -ne "\nEnter MySQL LOGIN SERVER database name (default l2s): "
	read LSDB
	if [ -z "$LSDB" ]; then
		LSDB="l2s"
	fi
	echo -ne "\nEnter MySQL LOGIN SERVER user (default root): "
	read LSUSER
	if [ -z "$LSUSER" ]; then
		LSUSER="root"
	fi
	echo -ne "\nEnter MySQL LOGIN SERVER $LSUSER's password: "
	read LSPASS
	if [ -z "$LSPASS" ]; then
		echo "Please avoid empty password else you will have a security problem."
	fi

	# GameServer
	echo -ne "\nEnter MySQL GAME SERVER hostname (default $LSDBHOST): "
	read GSDBHOST
	if [ -z "$GSDBHOST" ]; then
		GSDBHOST="$LSDBHOST"
	fi
	echo -ne "\nEnter MySQL GAME SERVER database name (default $LSDB): "
	read GSDB
	if [ -z "$GSDB" ]; then
		GSDB="$LSDB"
	fi
	echo -ne "\nEnter MySQL GAME SERVER user (default $LSUSER): "
	read GSUSER
	if [ -z "$GSUSER" ]; then
		GSUSER="$LSUSER"
	fi
	echo -ne "\nEnter MySQL GAME SERVER $GSUSER's password: "
	read GSPASS
	if [ -z "$GSPASS" ]; then
		echo "Please avoid empty password else you will have a security problem."
	fi
}

# Actions which can be performed
action_type()
{
	echo "#################################################"
	echo "#			L2System	Database Installer Script"
	echo "#################################################"
	echo ""
	echo "What do you want to do?"
	echo "Database backup		[b] (make a backup of the existing tables)"
	echo "Full Installation			[f] (for first installation, this will ERASE all the existing tables)"
	echo "Installation Login DB			[l] (for installation login database, this will ERASE all the existing tables)"
	echo "Installation Game DB			[g] (for installation game database, this will ERASE all the existing tables)"
	echo "Update Data				[u] (Only erase and reinsert tables without players' data)"
	echo "Update Login Data				[ul] (Only erase and reinsert tables without players' data)"
	echo "Insert one table			[t] (Only insert one table in your database)"
	echo "Quit this script			[q]"
	echo -ne "Choice: "
	read ACTION_CHOICE
	case "$ACTION_CHOICE" in
		"b"|"B") backup_db; finish;;
		"f"|"F") full_install; finish;;
		"l"|"L") login_install; finish;;
		"g"|"G") game_install; finish;;
		"u"|"U") update_db; finish;;
		"ul"|"UL") update_login_db; finish;;
		"t"|"T") table_insert;;
		"q"|"Q") finish;;
		*)       action_type;;
	esac
}

# Make a backup of the LS and GS database
backup_db()
{
	echo "#################################################"
	echo "#               L2System Database Backup"
	echo "#################################################"
	echo ""
	echo "LoginServer backup"
	$MYSQLDUMPPATH --ignore-table=${DBNAME}.loginserv_log --add-drop-table -h $LSDBHOST -u $LSUSER --password=$LSPASS $LSDB > loginserver_backup.sql
	echo "GameServer backup"
	$MYSQLDUMPPATH --ignore-table=${DBNAME}.game_log --ignore-table=${DBNAME}.petitions --add-drop-table -h $GSDBHOST -u $GSUSER --password=$GSPASS $GSDB > gameserver_backup.sql
}

# Full installation (erase and insert all tables)
full_install()
{
	echo "#################################################"
	echo "#			L2System	Full Database Installation"
	echo "#################################################"
	echo ""
	echo "[FULL DATABASE INSTALLATION]"
	login_install;
	game_install;
	udpate_install;
	echo "[COMPLETED]"
}

#Login Databese Install
login_install()
{
	echo "#################################################"
	echo "#			L2System Login Server Database Installation	"
	echo "#################################################"
	echo ""
	echo "[LOGINSERVER DATABASE]"
	loginserver_install;
	echo "[COMPLETED]"
}

#Game Databese Install
game_install()
{
	echo "#################################################"
	echo "#		L2System	Game Server Database Installation	"
	echo "#################################################"
	echo ""
	echo "[GAMESERVER DATABASE]"
	serverdata_install;
	userdata_install;
	udpate_install;
	echo "[COMPLETED]"
}

# Database update
update_db()
{
	echo "#################################################"
	echo "#			L2System	Database Update"
	echo "#################################################"
	echo ""
	echo "Please don't forget to make a backup before updating your database"
	echo "Do you want to proceed? (y) yes or (n) no: "
	read ANSWER
	if [ "$ANSWER" = "n" ]; then
		echo "Script aborted, make a backup before each update"
		exit 1
	else
		echo "[UPDATING THE DATABASE]"
		serverdata_install;
		udpate_install;
		echo "[COMPLETED]"
	fi
}

# Loging database update
update_login_db()
{
	echo "#################################################"
	echo "#		L2System	Login Database Update"
	echo "#################################################"
	echo ""
	echo "Please don't forget to make a backup before updating your database"
	echo "Do you want to proceed? (y) yes or (n) no: "
	read ANSWER
	if [ "$ANSWER" = "n" ]; then
		echo "Script aborted, make a backup before each update"
		exit 1
	else
		echo "[UPDATING THE DATABASE]"
		udpatelogin_install;
		echo "[COMPLETED]"
	fi
}

# Insert only one table the user want
table_insert()
{
	echo "#################################################"
	echo "#			L2System	Table insertion"
	echo "#################################################"
	echo ""
	echo -ne "What table do you want to insert? (don't add .sql extension) "
	read TABLE
	echo "Insertion of file $TABLE"
	$MYG < database_user/$TABLE.sql
	$MYG < database_server/$TABLE.sql
	echo "Insertion completed"
	action_type
}

loginserver_install()
{
	for tab in \
		database_login/accounts.sql \
		database_login/banned_ips.sql \
		database_login/gameservers.sql \
		database_login/loginserv_log.sql \
	; do
		echo Loading $tab ...
		$MYL < $tab
	done
}

serverdata_install()
{
	for tab in \
		database_server/ai_params.sql \
		database_server/armor.sql \
		database_server/armor_ex.sql \
		database_server/armorsets.sql \
		database_server/auto_chat.sql \
		database_server/auto_chat_text.sql \
		database_server/char_templates.sql \
		database_server/class_list.sql \
		database_server/dimensional_rift.sql \
		database_server/doors.sql \
		database_server/npc.sql \
		database_server/npc_elementals.sql \
		database_server/droplist.sql \
		database_server/etcitem.sql \
		database_server/fish.sql \
		database_server/fishreward.sql \
		database_server/henna.sql \
		database_server/henna_trees.sql \
		database_server/locations.sql \
		database_server/lvlupgain.sql \
		database_server/mapregion.sql \
		database_server/merchant_areas_list.sql \
		database_server/minions.sql \
		database_server/npcskills.sql \
		database_server/pet_data.sql \
		database_server/pets_skills.sql \
		database_server/random_spawn.sql \
		database_server/random_spawn_loc.sql \
		database_server/siege_door.sql \
		database_server/siege_guards.sql \
		database_server/skill_learn.sql \
		database_server/skill_spellbooks.sql \
		database_server/skill_trees_enchant.sql \
		database_server/skill_trees_player.sql \
		database_server/skill_trees_pledge.sql \
		database_server/skill_trees_squad.sql \
		database_server/skill_trees_transform.sql \
		database_server/skill_trees_fishing.sql \
		database_server/skill_trees_special.sql \
		database_server/skills.sql \
		database_server/spawnlist.sql \
		database_server/territory_spawnlist.sql \
		database_server/tournament_class_list.sql \
		database_server/vote.sql \
		database_server/weapon.sql \
		database_server/weapon_ex.sql \
		database_server/online.sql \
		database_server/lastimperialtomb_spawnlist.sql \
		database_server/four_sepulchers_spawnlist.sql \
		database_custom/festive_sweeper.sql \
		database_custom/skill_trees_player.sql \
	; do
		echo Loading $tab ...
		$MYG < $tab
	done
}

userdata_install()
{
	for tab in \
		database_user/ally_data.sql \
		database_user/auction.sql \
		database_user/auction_bid.sql \
		database_user/l2_auction.sql \
		database_user/augmentations.sql \
		database_user/bans.sql \
		database_user/bonus.sql \
		database_user/castle.sql \
		database_user/castle_manor_procure.sql \
		database_user/castle_manor_production.sql \
		database_user/community_clanbonus.sql \
		database_user/community_bonuscolor.sql \
		database_user/community_buffs_group_skills.sql \
		database_user/community_buffs_group.sql \
		database_user/community_teleport.sql \
		database_user/characters.sql \
		database_user/character_blocklist.sql \
		database_user/character_effects_save.sql \
		database_user/character_friends.sql \
		database_user/character_hennas.sql \
		database_user/character_macroses.sql \
		database_user/character_quests.sql \
		database_user/character_recipebook.sql \
		database_user/character_shortcuts.sql \
		database_user/character_skills.sql \
		database_user/character_skills_save.sql \
		database_user/character_subclasses.sql \
		database_user/character_mail.sql \
		database_user/character_variables.sql \
		database_user/character_tpbookmark.sql \
		database_user/character_votes_l2top.sql \
		database_user/character_votes_mmotop.sql \
		database_user/clanhall.sql \
		database_user/residence_functions.sql \
		database_user/clan_data.sql \
		database_user/clan_privs.sql \
		database_user/clan_skills.sql \
		database_user/clan_subpledges.sql \
		database_user/clan_wars.sql \
		database_user/couples.sql \
		database_user/craftcount.sql \
		database_user/cursed_weapons.sql \
		database_user/dropcount.sql \
		database_user/epic_boss_spawn.sql \
		database_user/forts.sql \
		database_user/forums.sql \
		database_user/game_log.sql \
		database_user/global_tasks.sql \
		database_user/gracia_seeds.sql \
		database_user/heroes.sql \
		database_user/hwid_bans.sql \
		database_user/hwid_bonus.sql \
		database_user/hwids_log.sql \
		database_user/items.sql \
		database_user/items_delayed.sql \
		database_user/item_auction.sql \
		database_user/item_auction_bid.sql \
		database_user/killcount.sql \
		database_user/lottery.sql \
		database_user/mail.sql \
		database_user/olympiad_nobles.sql \
		database_user/petitions.sql \
		database_user/pets.sql \
		database_user/posts.sql \
		database_user/raidboss_points.sql \
		database_user/raidboss_status.sql \
		database_user/server_variables.sql \
		database_user/seven_signs.sql \
		database_user/seven_signs_festival.sql \
		database_user/seven_signs_status.sql \
		database_user/siege_clans.sql \
		database_user/siege_doorupgrade.sql \
		database_user/siege_territory_members.sql \
		database_user/summon_effects_save.sql \
		database_user/topic.sql \
		database_user/tournament_table.sql \
		database_user/tournament_teams.sql \
		database_user/tournament_variables.sql \
		database_user/tpasskey.sql \
	; do
		echo Loading $tab ...
		$MYG < $tab
	done
}

udpate_install()
{
	for tab in \
		updates/update_gameserver.sql \	
	 do
		echo Loading $tab ...
		$MYG < $tab &> /dev/null
	done
}

udpatelogin_install()
{
	for tab in \
		updates/update_loginserver.sql \
	; do
		echo Loading $tab ...
		$MYL < $tab &> /dev/null
	done
}

# End of the script
finish()
{
	echo ""
	echo "Script execution finished."
	exit 0
}

# Clear console
clear

# Call configure function
configure

# Open MySQL connections
MYL="$MYSQLPATH -h $LSDBHOST -u $LSUSER --password=$LSPASS -D $LSDB"
MYG="$MYSQLPATH -h $GSDBHOST -u $GSUSER --password=$GSPASS -D $GSDB"

# Ask action to do
action_type