DROP TABLE IF EXISTS `banned_ips`;
CREATE TABLE `banned_ips` (
  `iphash` BIGINT(20) NOT NULL,
  `ip` VARCHAR(15) NOT NULL,
  `admin` VARCHAR(45) DEFAULT NULL,
  `expiretime` BIGINT(20) NOT NULL DEFAULT '0',
  `comments` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY  (`iphash`),
  UNIQUE KEY `iphash` (`iphash`)
) ENGINE=MyISAM;