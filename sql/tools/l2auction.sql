DROP TABLE IF EXISTS l2_auction;

CREATE TABLE l2_auction (
id serial NOT NULL,
date_start datetime NOT NULL,
date_stop datetime,
date_to_stop datetime,
step int NOT NULL,
status varchar(4) NOT NULL default 'init',
description varchar(256) NOT NULL default '',
PRIMARY KEY auction_primary_key (id)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS l2_auction_lot;

CREATE TABLE l2_auction_lot (
id serial NOT NULL,
auction_id int NOT NULL,
item_id int(11) NOT NULL,
item_count int NOT NULL default 1,
status varchar(4) NOT NULL default 'init',
level varchar(6) NOT NULL default 'normal',
bet int,
bet_cost int,
time_step int,
start_time int,
winner_id int(11),
winner_cost int,
winner_bets int,
date_start datetime,
date_stop datetime,
pos int NOT NULL default 0,
procent int NOT NULL default 100,
PRIMARY KEY lot_primary_key(id)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS l2_lot_member;

CREATE TABLE l2_lot_member (
id serial NOT NULL,
lot_id int NOT NULL,
customer_id int(11) NOT NULL,
status varchar(4) NOT NULL default 'actv',
registered datetime,
PRIMARY KEY member_primary_key(id)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS l2_member_bet;

CREATE TABLE l2_member_bet (
id serial NOT NULL,
bet_time datetime NOT NULL,
member_id int NOT NULL,
lot_id int NOT NULL,
bet int,
status varchar(4) NOT NULL default 'actv',
PRIMARY KEY bet_primary_key(id)
) ENGINE=MyISAM;