-- добавляет N дней ПА аккаунтам, у которых ПА ещё не закончилось
SET @ADD_DAYS = 1; -- количество дней, которые нужно добавить
UPDATE `accounts` SET bonus_expire = bonus_expire + @ADD_DAYS * 86400 WHERE bonus > 1;

-- выдаёт ПА всем аккаунт, у которых ПА нету или закончилось
SET @ADD_DAYS = 1; -- на сколько дней выдать
SET @RATE = 2; -- рейты для ПА
UPDATE `accounts` SET bonus_expire = UNIX_TIMESTAMP() + @ADD_DAYS * 86400, bonus = @RATE WHERE bonus = 1 AND bonus_expire < UNIX_TIMESTAMP();
