-- Update price to 0 for all items
UPDATE `armor` SET price=0 where price > 1;
UPDATE `etcitem` SET price=0 where price > 1;
UPDATE `weapon` SET price=0 where price > 1;

-- L2Dream